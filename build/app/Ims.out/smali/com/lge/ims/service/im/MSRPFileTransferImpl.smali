.class public Lcom/lge/ims/service/im/MSRPFileTransferImpl;
.super Lcom/lge/ims/service/im/FileTransferImpl;
.source "MSRPFileTransferImpl.java"


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;I)V
    .registers 4
    .parameter "fileTransferImplFacade"
    .parameter "context"
    .parameter "nativeObj"

    #@0
    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/ims/service/im/FileTransferImpl;-><init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;I)V

    #@3
    .line 17
    return-void
.end method

.method public constructor <init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;[Ljava/lang/String;)V
    .registers 4
    .parameter "fileTransferImplFacade"
    .parameter "context"
    .parameter "remoteUsers"

    #@0
    .prologue
    .line 12
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/ims/service/im/FileTransferImpl;-><init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;[Ljava/lang/String;)V

    #@3
    .line 13
    return-void
.end method


# virtual methods
.method public accept(Ljava/lang/String;)V
    .registers 5
    .parameter "folderPath"

    #@0
    .prologue
    .line 42
    if-nez p1, :cond_9

    #@2
    .line 43
    const-string v1, "folder path is null"

    #@4
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7
    .line 44
    const-string p1, ""

    #@9
    .line 47
    :cond_9
    const-string v1, "/"

    #@b
    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@e
    move-result v1

    #@f
    if-nez v1, :cond_24

    #@11
    .line 48
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    const-string v2, "/"

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object p1

    #@24
    .line 51
    :cond_24
    new-instance v1, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v2, "folder path = "

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3a
    .line 53
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3d
    move-result-object v0

    #@3e
    .line 55
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x29a7

    #@40
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@43
    .line 56
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@46
    .line 58
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/MSRPFileTransferImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@49
    .line 59
    return-void
.end method

.method public getInterface()I
    .registers 2

    #@0
    .prologue
    .line 20
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 21
    const/16 v0, 0x18

    #@7
    invoke-static {v0}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public sendFile(Lcom/lge/ims/service/im/IMFileInfo;Ljava/lang/String;)V
    .registers 7
    .parameter "imFileInfo"
    .parameter "contributionId"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 25
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v2, "remoteUsers:"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    iget-object v2, p0, Lcom/lge/ims/service/im/FileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@e
    aget-object v2, v2, v3

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v2, "filePath:"

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getFilePath()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    const-string v2, ",contentType:"

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getContentType()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    const-string v2, ",size:"

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getFileSize()I

    #@39
    move-result v2

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    const-string v2, ", Contribution-ID:"

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v1

    #@4c
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4f
    .line 29
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@52
    move-result-object v0

    #@53
    .line 31
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x29a5

    #@55
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@58
    .line 32
    iget-object v1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@5a
    aget-object v1, v1, v3

    #@5c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5f
    .line 33
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getFilePath()Ljava/lang/String;

    #@62
    move-result-object v1

    #@63
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@66
    .line 34
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getContentType()Ljava/lang/String;

    #@69
    move-result-object v1

    #@6a
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@6d
    .line 35
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getFileSize()I

    #@70
    move-result v1

    #@71
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@74
    .line 38
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/MSRPFileTransferImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@77
    .line 39
    return-void
.end method
