.class public Lcom/lge/ims/service/im/IUIMService;
.super Ljava/lang/Object;
.source "IUIMService.java"


# static fields
.field public static final CALL_STATE:Ljava/lang/String; = "callState"

.field public static final CHAT_IMPL:Ljava/lang/String; = "ChatImpl"

.field public static final CONFERENCE_EVENTS:Ljava/lang/String; = "conferenceEvents"

.field public static final CONTENT_TYPE_FILELINK:Ljava/lang/String; = "application/http-ft+xml"

.field public static final CONTENT_TYPE_LOCATION:Ljava/lang/String; = "application/loc"

.field public static final CONTENT_TYPE_TEXT:Ljava/lang/String; = "text/plain"

.field public static final CONTRIBUTION_ID:Ljava/lang/String; = "contributionId"

.field public static final CS:I = 0x1

.field public static final DATE_TIME:Ljava/lang/String; = "dateTime"

.field public static final DELETE_FT:Ljava/lang/String; = "deleteFileTransfer"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "displayName"

.field public static final FILE_CONTENT_TYPE:Ljava/lang/String; = "contentType"

.field public static final FILE_DOWNLOAD_PERIOD:Ljava/lang/String; = "until"

.field public static final FILE_DOWNLOAD_URL:Ljava/lang/String; = "url"

.field public static final FILE_INFO:Ljava/lang/String; = "FileInfo"

.field public static final FILE_NAME:Ljava/lang/String; = "fileName"

.field public static final FILE_SIZE:Ljava/lang/String; = "fileSize"

.field public static final FT_INTERFACE:Ljava/lang/String; = "FileTransferImplFacade"

.field public static final HTTP:Ljava/lang/String; = "HTTP"

.field public static final IDLE:I = 0x0

.field public static final IIMSERVICE:Ljava/lang/String; = "com.lge.ims.service.im.IIMService"

.field public static final IMS_SYSTEMPROPERTY_NAME:Ljava/lang/String; = "net.ims.media.protocol"

.field public static final INTENT_ACTION_CHAT_INVITE_RECEIVED:Ljava/lang/String; = "com.lge.ims.rcsim.CHAT_INVITE_RECEIVED"

.field public static final INTENT_ACTION_CONFERENCE_EVENT_RECEIVED:Ljava/lang/String; = "com.lge.ims.rcsim.CONFERENCE_EVENT_RECEIVED"

.field public static final INTENT_ACTION_CONFERENCE_INVITE_RECEIVED:Ljava/lang/String; = "com.lge.ims.rcsim.CONFERENCE_INVITE_RECEIVED"

.field public static final INTENT_ACTION_CONFERENCE_INVITE_RECEIVED_WITH_FILELINK:Ljava/lang/String; = "com.lge.ims.rcsim.CONFERENCE_INVITE_RECEIVED_WITH_FILELINK"

.field public static final INTENT_ACTION_FILE_TRANSFER_INVITE_RECEIVED:Ljava/lang/String; = "com.lge.ims.rcsim.FILE_TRANSFER_INVITE_RECEIVED"

.field public static final INTENT_ACTION_INVITE_RECEIVED_WITH_FILELINK:Ljava/lang/String; = "com.lge.ims.rcsim.INVITE_RECEIVED_WITH_FILELINK"

.field public static final IS_CONFERENCE:Ljava/lang/String; = "isConference"

.field public static final IS_DEFERRED_MESSAGES:Ljava/lang/String; = "deferredMessages"

.field public static final IS_DISPLAY_NOTIFICATION:Ljava/lang/String; = "displayNotification"

.field public static final MESSAGE_ID:Ljava/lang/String; = "messageId"

.field public static final MIME_TYPE:Ljava/lang/String; = "mimeType"

.field public static final MSRP:Ljava/lang/String; = "MSRP"

.field public static final PARTICIPIENTS:Ljava/lang/String; = "participants"

.field public static final REMOTE_USER:Ljava/lang/String; = "remoteUser"

.field public static final REMOTE_USER_URI:Ljava/lang/String; = "remoteUserUri"

.field public static final SESSION_REPLACES:Ljava/lang/String; = "sessionReplaces"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final THUMBNAIL_CONTENT_TYPE:Ljava/lang/String; = "thumbnailContentType"

.field public static final THUMBNAIL_DOWNLOAD_PERIOD:Ljava/lang/String; = "thumbnailUntil"

.field public static final THUMBNAIL_DOWNLOAD_URL:Ljava/lang/String; = "thumbnailUrl"

.field public static final THUMBNAIL_NAME:Ljava/lang/String; = "thumbnailName"

.field public static final THUMBNAIL_SIZE:Ljava/lang/String; = "thumbnailSize"

.field public static final VT:I = 0x2


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
