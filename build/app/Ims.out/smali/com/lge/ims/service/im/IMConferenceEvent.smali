.class public Lcom/lge/ims/service/im/IMConferenceEvent;
.super Ljava/lang/Object;
.source "IMConferenceEvent.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/im/IMConferenceEvent;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "LGIMS"


# instance fields
.field private mServiceType:Ljava/lang/String;

.field private mStatus:I

.field private mUri:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 53
    new-instance v0, Lcom/lge/ims/service/im/IMConferenceEvent$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/im/IMConferenceEvent$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/im/IMConferenceEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 14
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 15
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 18
    const-string v0, "LGIMS"

    #@5
    const-string v1, "[IMConferenceEvent] Created"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 19
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/im/IMConferenceEvent;->readFromParcel(Landroid/os/Parcel;)V

    #@d
    .line 20
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 50
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getServiceType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mServiceType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getStatus()I
    .registers 2

    #@0
    .prologue
    .line 79
    iget v0, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mStatus:I

    #@2
    return v0
.end method

.method public getUri()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mUri:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 6
    .parameter "source"

    #@0
    .prologue
    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    iput-object v1, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mUri:Ljava/lang/String;

    #@6
    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 25
    .local v0, status:Ljava/lang/String;
    const-string v1, "invitation"

    #@c
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_4e

    #@12
    .line 26
    const/4 v1, 0x1

    #@13
    iput v1, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mStatus:I

    #@15
    .line 32
    :goto_15
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    iput-object v1, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mServiceType:Ljava/lang/String;

    #@1b
    .line 34
    const-string v1, "LGIMS"

    #@1d
    new-instance v2, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v3, "[IMConferenceEvent] Uri : "

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    iget-object v3, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mUri:Ljava/lang/String;

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    const-string v3, " Status : "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    iget v3, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mStatus:I

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    const-string v3, " ServiceType : "

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    iget-object v3, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mServiceType:Ljava/lang/String;

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 35
    return-void

    #@4e
    .line 27
    :cond_4e
    const-string v1, "leaver"

    #@50
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v1

    #@54
    if-eqz v1, :cond_5a

    #@56
    .line 28
    const/4 v1, 0x2

    #@57
    iput v1, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mStatus:I

    #@59
    goto :goto_15

    #@5a
    .line 30
    :cond_5a
    const/4 v1, 0x0

    #@5b
    iput v1, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mStatus:I

    #@5d
    goto :goto_15
.end method

.method public setServiceType(Ljava/lang/String;)V
    .registers 2
    .parameter "serviceType"

    #@0
    .prologue
    .line 91
    iput-object p1, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mServiceType:Ljava/lang/String;

    #@2
    .line 92
    return-void
.end method

.method public setStatus(I)V
    .registers 2
    .parameter "status"

    #@0
    .prologue
    .line 83
    iput p1, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mStatus:I

    #@2
    .line 84
    return-void
.end method

.method public setUri(Ljava/lang/String;)V
    .registers 2
    .parameter "uri"

    #@0
    .prologue
    .line 75
    iput-object p1, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mUri:Ljava/lang/String;

    #@2
    .line 76
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mUri:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 39
    iget v0, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mStatus:I

    #@7
    const/4 v1, 0x1

    #@8
    if-ne v0, v1, :cond_15

    #@a
    .line 40
    const-string v0, "invitation"

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 46
    :goto_f
    iget-object v0, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mServiceType:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 47
    return-void

    #@15
    .line 41
    :cond_15
    iget v0, p0, Lcom/lge/ims/service/im/IMConferenceEvent;->mStatus:I

    #@17
    const/4 v1, 0x2

    #@18
    if-ne v0, v1, :cond_20

    #@1a
    .line 42
    const-string v0, "leaver"

    #@1c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1f
    goto :goto_f

    #@20
    .line 44
    :cond_20
    const-string v0, "none"

    #@22
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@25
    goto :goto_f
.end method
