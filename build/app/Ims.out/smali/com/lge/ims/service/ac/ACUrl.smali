.class public Lcom/lge/ims/service/ac/ACUrl;
.super Ljava/lang/Object;
.source "ACUrl.java"


# static fields
.field public static final PATH_CONFIG:I = 0x0

.field public static final PATH_JOIN:I = 0x1

.field private static final SW_VERSION:Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "ACUrl"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 24
    const-string v0, "ro.lge.swversion"

    #@2
    const-string v1, "1.1.1.1"

    #@4
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    sput-object v0, Lcom/lge/ims/service/ac/ACUrl;->SW_VERSION:Ljava/lang/String;

    #@a
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 27
    return-void
.end method

.method private static createQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 14
    .parameter "vers"
    .parameter "imsi"
    .parameter "clientVendor"
    .parameter "clientVersion"
    .parameter "terminalVendor"
    .parameter "terminalModel"
    .parameter "terminalSwVersion"
    .parameter "imei"

    #@0
    .prologue
    .line 128
    if-nez p0, :cond_b

    #@2
    .line 129
    const-string v3, "ACUrl"

    #@4
    const-string v4, "vers SHOULD be specified"

    #@6
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 130
    const/4 v2, 0x0

    #@a
    .line 171
    :cond_a
    :goto_a
    return-object v2

    #@b
    .line 133
    :cond_b
    const-string v0, "UTF-8"

    #@d
    .line 134
    .local v0, CHARSET:Ljava/lang/String;
    const-string v2, ""

    #@f
    .line 137
    .local v2, query:Ljava/lang/String;
    :try_start_f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "vers="

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    const-string v4, "UTF-8"

    #@1c
    invoke-static {p0, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    .line 139
    if-eqz p1, :cond_47

    #@2a
    .line 140
    new-instance v3, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, "&IMSI="

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    const-string v4, "UTF-8"

    #@3b
    invoke-static {p1, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    .line 143
    :cond_47
    if-eqz p2, :cond_66

    #@49
    .line 144
    new-instance v3, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    const-string v4, "&client_vendor="

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    const-string v4, "UTF-8"

    #@5a
    invoke-static {p2, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v2

    #@66
    .line 147
    :cond_66
    if-eqz p3, :cond_85

    #@68
    .line 148
    new-instance v3, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    const-string v4, "&client_version="

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    const-string v4, "UTF-8"

    #@79
    invoke-static {p3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7c
    move-result-object v4

    #@7d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v2

    #@85
    .line 151
    :cond_85
    if-eqz p4, :cond_a4

    #@87
    .line 152
    new-instance v3, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v3

    #@90
    const-string v4, "&terminal_vendor="

    #@92
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v3

    #@96
    const-string v4, "UTF-8"

    #@98
    invoke-static {p4, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9b
    move-result-object v4

    #@9c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v3

    #@a0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v2

    #@a4
    .line 155
    :cond_a4
    if-eqz p5, :cond_c3

    #@a6
    .line 156
    new-instance v3, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v3

    #@af
    const-string v4, "&terminal_model="

    #@b1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v3

    #@b5
    const-string v4, "UTF-8"

    #@b7
    invoke-static {p5, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@ba
    move-result-object v4

    #@bb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v3

    #@bf
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v2

    #@c3
    .line 159
    :cond_c3
    if-eqz p6, :cond_e2

    #@c5
    .line 160
    new-instance v3, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v3

    #@ce
    const-string v4, "&terminal_sw_version="

    #@d0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v3

    #@d4
    const-string v4, "UTF-8"

    #@d6
    invoke-static {p6, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@d9
    move-result-object v4

    #@da
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v3

    #@de
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v2

    #@e2
    .line 163
    :cond_e2
    if-eqz p7, :cond_a

    #@e4
    .line 164
    new-instance v3, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v3

    #@ed
    const-string v4, "&IMEI="

    #@ef
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v3

    #@f3
    const-string v4, "UTF-8"

    #@f5
    invoke-static {p7, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@f8
    move-result-object v4

    #@f9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v3

    #@fd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_100
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_f .. :try_end_100} :catch_103

    #@100
    move-result-object v2

    #@101
    goto/16 :goto_a

    #@103
    .line 166
    :catch_103
    move-exception v1

    #@104
    .line 167
    .local v1, e:Ljava/io/UnsupportedEncodingException;
    const-string v3, "ACUrl"

    #@106
    new-instance v4, Ljava/lang/StringBuilder;

    #@108
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10b
    const-string v5, "UnsupportedEncodingException :: "

    #@10d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v4

    #@111
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->toString()Ljava/lang/String;

    #@114
    move-result-object v5

    #@115
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v4

    #@119
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v4

    #@11d
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@120
    .line 168
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    #@123
    goto/16 :goto_a
.end method

.method public static getDefaultUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "path"
    .parameter "query"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    .line 30
    const/4 v2, 0x0

    #@3
    .line 32
    .local v2, url:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getConnectionServer()I

    #@6
    move-result v3

    #@7
    if-nez v3, :cond_bf

    #@9
    .line 33
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->hasCommercialNetwork()Z

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_13

    #@f
    .line 34
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getCommercialNetwork()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    .line 42
    :cond_13
    :goto_13
    if-nez v2, :cond_3c

    #@15
    .line 43
    sget-object v3, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@17
    const-string v4, "KT"

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_d9

    #@1f
    .line 44
    invoke-static {v6}, Lcom/lge/ims/PhoneInfo;->getMcc(Z)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .line 45
    .local v0, mcc:Ljava/lang/String;
    invoke-static {v6}, Lcom/lge/ims/PhoneInfo;->getMnc(Z)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    .line 47
    .local v1, mnc:Ljava/lang/String;
    if-nez v0, :cond_2b

    #@29
    .line 48
    const-string v0, "450"

    #@2b
    .line 51
    :cond_2b
    if-nez v1, :cond_cb

    #@2d
    .line 52
    const-string v1, "008"

    #@2f
    .line 59
    :cond_2f
    :goto_2f
    const-string v3, "https://config.rcs.mnc%s.mcc%s.pub.3gppnetwork.org"

    #@31
    new-array v4, v5, [Ljava/lang/Object;

    #@33
    const/4 v5, 0x0

    #@34
    aput-object v1, v4, v5

    #@36
    aput-object v0, v4, v6

    #@38
    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    .line 65
    .end local v0           #mcc:Ljava/lang/String;
    .end local v1           #mnc:Ljava/lang/String;
    :cond_3c
    :goto_3c
    sget-object v3, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@3e
    const-string v4, "SKT"

    #@40
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@43
    move-result v3

    #@44
    if-eqz v3, :cond_4a

    #@46
    .line 66
    invoke-static {v2}, Lcom/lge/ims/service/ac/ACUrl;->replaceHostInfoToResolvedIPAddress(Ljava/lang/String;)Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    .line 69
    :cond_4a
    if-eqz v2, :cond_78

    #@4c
    if-eqz p0, :cond_78

    #@4e
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@51
    move-result v3

    #@52
    if-lez v3, :cond_78

    #@54
    .line 70
    new-instance v3, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    const-string v4, "/"

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v2

    #@67
    .line 71
    new-instance v3, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v3

    #@74
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v2

    #@78
    .line 74
    :cond_78
    if-eqz v2, :cond_a6

    #@7a
    if-eqz p1, :cond_a6

    #@7c
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@7f
    move-result v3

    #@80
    if-lez v3, :cond_a6

    #@82
    .line 75
    new-instance v3, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v3

    #@8b
    const-string v4, "?"

    #@8d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v3

    #@91
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v2

    #@95
    .line 76
    new-instance v3, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v3

    #@9e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v3

    #@a2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v2

    #@a6
    .line 79
    :cond_a6
    const-string v3, "ACUrl"

    #@a8
    new-instance v4, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    const-string v5, "Default URL="

    #@af
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v4

    #@b3
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v4

    #@b7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v4

    #@bb
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@be
    .line 81
    return-object v2

    #@bf
    .line 37
    :cond_bf
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->hasTestbed()Z

    #@c2
    move-result v3

    #@c3
    if-eqz v3, :cond_13

    #@c5
    .line 38
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getTestbed()Ljava/lang/String;

    #@c8
    move-result-object v2

    #@c9
    goto/16 :goto_13

    #@cb
    .line 54
    .restart local v0       #mcc:Ljava/lang/String;
    .restart local v1       #mnc:Ljava/lang/String;
    :cond_cb
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@ce
    move-result v3

    #@cf
    if-ne v3, v5, :cond_2f

    #@d1
    .line 55
    const-string v3, "0"

    #@d3
    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    #@d6
    move-result-object v1

    #@d7
    goto/16 :goto_2f

    #@d9
    .line 60
    .end local v0           #mcc:Ljava/lang/String;
    .end local v1           #mnc:Ljava/lang/String;
    :cond_d9
    sget-object v3, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@db
    const-string v4, "SKT"

    #@dd
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e0
    move-result v3

    #@e1
    if-eqz v3, :cond_3c

    #@e3
    .line 61
    const-string v2, "https://apcs.sktelecom.com"

    #@e5
    goto/16 :goto_3c
.end method

.method public static getPath(I)Ljava/lang/String;
    .registers 4
    .parameter "option"

    #@0
    .prologue
    .line 85
    const/4 v0, 0x0

    #@1
    .line 87
    .local v0, urlPath:Ljava/lang/String;
    sget-object v1, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@3
    const-string v2, "KT"

    #@5
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_16

    #@b
    .line 88
    if-nez p0, :cond_10

    #@d
    .line 89
    const-string v0, "Config"

    #@f
    .line 99
    :cond_f
    :goto_f
    return-object v0

    #@10
    .line 90
    :cond_10
    const/4 v1, 0x1

    #@11
    if-ne p0, v1, :cond_f

    #@13
    .line 91
    const-string v0, "Join"

    #@15
    goto :goto_f

    #@16
    .line 93
    :cond_16
    sget-object v1, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@18
    const-string v2, "SKT"

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_f

    #@20
    .line 94
    if-nez p0, :cond_f

    #@22
    .line 95
    const-string v0, "VoLTE/Config"

    #@24
    goto :goto_f
.end method

.method public static getQuery(Landroid/content/Context;)Ljava/lang/String;
    .registers 12
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 103
    const/4 v10, 0x0

    #@2
    .line 105
    .local v10, urlQuery:Ljava/lang/String;
    sget-object v2, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@4
    const-string v3, "KT"

    #@6
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_35

    #@c
    .line 106
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getVersionFromVersion(Landroid/content/Context;)Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    .line 108
    .local v0, vers:Ljava/lang/String;
    if-eqz v0, :cond_22

    #@12
    .line 109
    const-string v2, "LGE"

    #@14
    const-string v3, "3.3.0"

    #@16
    const-string v4, "LGE"

    #@18
    sget-object v5, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@1a
    sget-object v6, Lcom/lge/ims/service/ac/ACUrl;->SW_VERSION:Ljava/lang/String;

    #@1c
    move-object v7, v1

    #@1d
    invoke-static/range {v0 .. v7}, Lcom/lge/ims/service/ac/ACUrl;->createQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v10

    #@21
    .line 123
    .end local v0           #vers:Ljava/lang/String;
    :cond_21
    :goto_21
    return-object v10

    #@22
    .line 111
    .restart local v0       #vers:Ljava/lang/String;
    :cond_22
    const-string v2, "0.0.0.0"

    #@24
    const-string v4, "LGE"

    #@26
    const-string v5, "3.3.0"

    #@28
    const-string v6, "LGE"

    #@2a
    sget-object v7, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@2c
    sget-object v8, Lcom/lge/ims/service/ac/ACUrl;->SW_VERSION:Ljava/lang/String;

    #@2e
    move-object v3, v1

    #@2f
    move-object v9, v1

    #@30
    invoke-static/range {v2 .. v9}, Lcom/lge/ims/service/ac/ACUrl;->createQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v10

    #@34
    goto :goto_21

    #@35
    .line 113
    .end local v0           #vers:Ljava/lang/String;
    :cond_35
    sget-object v2, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@37
    const-string v3, "SKT"

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v2

    #@3d
    if-eqz v2, :cond_21

    #@3f
    .line 114
    invoke-static {p0}, Lcom/lge/ims/service/ac/ACContentHelper;->getVersionFromVersion(Landroid/content/Context;)Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    .line 116
    .restart local v0       #vers:Ljava/lang/String;
    if-eqz v0, :cond_50

    #@45
    move-object v2, v1

    #@46
    move-object v3, v1

    #@47
    move-object v4, v1

    #@48
    move-object v5, v1

    #@49
    move-object v6, v1

    #@4a
    move-object v7, v1

    #@4b
    .line 117
    invoke-static/range {v0 .. v7}, Lcom/lge/ims/service/ac/ACUrl;->createQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v10

    #@4f
    goto :goto_21

    #@50
    .line 119
    :cond_50
    const-string v2, "0.0.0.0"

    #@52
    move-object v3, v1

    #@53
    move-object v4, v1

    #@54
    move-object v5, v1

    #@55
    move-object v6, v1

    #@56
    move-object v7, v1

    #@57
    move-object v8, v1

    #@58
    move-object v9, v1

    #@59
    invoke-static/range {v2 .. v9}, Lcom/lge/ims/service/ac/ACUrl;->createQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5c
    move-result-object v10

    #@5d
    goto :goto_21
.end method

.method private static replaceHostInfoToResolvedIPAddress(Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "url"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 175
    const/4 v3, 0x0

    #@2
    .line 178
    .local v3, tmpUrl:Ljava/net/URL;
    :try_start_2
    new-instance v4, Ljava/net/URL;

    #@4
    invoke-direct {v4, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_7
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_7} :catch_a0

    #@7
    .line 185
    .end local v3           #tmpUrl:Ljava/net/URL;
    .local v4, tmpUrl:Ljava/net/URL;
    if-eqz v4, :cond_e5

    #@9
    .line 186
    const/4 v0, 0x0

    #@a
    .line 189
    .local v0, address:Ljava/net/InetAddress;
    :try_start_a
    invoke-virtual {v4}, Ljava/net/URL;->getHost()Ljava/lang/String;

    #@d
    move-result-object v7

    #@e
    invoke-static {v7}, Ljava/net/InetAddress;->getByName(Ljava/lang/String;)Ljava/net/InetAddress;
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_11} :catch_c2

    #@11
    move-result-object v0

    #@12
    .line 196
    const-string v1, ""

    #@14
    .line 198
    .local v1, correctedUrl:Ljava/lang/String;
    if-eqz v0, :cond_e5

    #@16
    .line 199
    invoke-virtual {v4}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    .line 200
    new-instance v6, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v6

    #@23
    const-string v7, "://"

    #@25
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    .line 202
    invoke-virtual {v4}, Ljava/net/URL;->getUserInfo()Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    .line 204
    .local v5, userInfo:Ljava/lang/String;
    if-eqz v5, :cond_57

    #@33
    .line 205
    new-instance v6, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    .line 206
    new-instance v6, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    const-string v7, "@"

    #@4f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    .line 209
    :cond_57
    new-instance v6, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v6

    #@60
    invoke-virtual {v0}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    #@63
    move-result-object v7

    #@64
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v6

    #@68
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v1

    #@6c
    .line 211
    invoke-virtual {v4}, Ljava/net/URL;->getPort()I

    #@6f
    move-result v6

    #@70
    if-lez v6, :cond_9e

    #@72
    .line 212
    new-instance v6, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v6

    #@7b
    const-string v7, ":"

    #@7d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v6

    #@81
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v1

    #@85
    .line 213
    new-instance v6, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v6

    #@8e
    invoke-virtual {v4}, Ljava/net/URL;->getPort()I

    #@91
    move-result v7

    #@92
    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@95
    move-result-object v7

    #@96
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v6

    #@9a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v1

    #@9e
    :cond_9e
    move-object v3, v4

    #@9f
    .line 220
    .end local v0           #address:Ljava/net/InetAddress;
    .end local v1           #correctedUrl:Ljava/lang/String;
    .end local v4           #tmpUrl:Ljava/net/URL;
    .end local v5           #userInfo:Ljava/lang/String;
    .restart local v3       #tmpUrl:Ljava/net/URL;
    :goto_9f
    return-object v1

    #@a0
    .line 179
    :catch_a0
    move-exception v2

    #@a1
    .line 180
    .local v2, e:Ljava/net/MalformedURLException;
    const-string v7, "ACUrl"

    #@a3
    new-instance v8, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v9, "MalformedURLException :: "

    #@aa
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v8

    #@ae
    invoke-virtual {v2}, Ljava/net/MalformedURLException;->toString()Ljava/lang/String;

    #@b1
    move-result-object v9

    #@b2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v8

    #@b6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v8

    #@ba
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@bd
    .line 181
    invoke-virtual {v2}, Ljava/net/MalformedURLException;->printStackTrace()V

    #@c0
    move-object v1, v6

    #@c1
    .line 182
    goto :goto_9f

    #@c2
    .line 190
    .end local v2           #e:Ljava/net/MalformedURLException;
    .end local v3           #tmpUrl:Ljava/net/URL;
    .restart local v0       #address:Ljava/net/InetAddress;
    .restart local v4       #tmpUrl:Ljava/net/URL;
    :catch_c2
    move-exception v2

    #@c3
    .line 191
    .local v2, e:Ljava/lang/Exception;
    const-string v7, "ACUrl"

    #@c5
    new-instance v8, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v9, "Exception :: "

    #@cc
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v8

    #@d0
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@d3
    move-result-object v9

    #@d4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v8

    #@d8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v8

    #@dc
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@df
    .line 192
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@e2
    move-object v3, v4

    #@e3
    .end local v4           #tmpUrl:Ljava/net/URL;
    .restart local v3       #tmpUrl:Ljava/net/URL;
    move-object v1, v6

    #@e4
    .line 193
    goto :goto_9f

    #@e5
    .end local v0           #address:Ljava/net/InetAddress;
    .end local v2           #e:Ljava/lang/Exception;
    .end local v3           #tmpUrl:Ljava/net/URL;
    .restart local v4       #tmpUrl:Ljava/net/URL;
    :cond_e5
    move-object v3, v4

    #@e6
    .end local v4           #tmpUrl:Ljava/net/URL;
    .restart local v3       #tmpUrl:Ljava/net/URL;
    move-object v1, v6

    #@e7
    .line 220
    goto :goto_9f
.end method
