.class public Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;
.super Ljava/lang/Object;
.source "OnScreenDebugInfoVideo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "LGIMS"


# instance fields
.field public recvBitRate:I

.field public recvFrameRate:I

.field public sendBitRate:I

.field public sendFrameRate:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 66
    new-instance v0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(IIII)V
    .registers 8
    .parameter "_sendBitRate"
    .parameter "_recvBitRate"
    .parameter "_sendFrameRate"
    .parameter "_recvFrameRate"

    #@0
    .prologue
    .line 25
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 26
    const-string v0, "LGIMS"

    #@5
    const-string v1, "OnScreenDebugInfoVideo()"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 28
    iput p1, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->sendBitRate:I

    #@c
    .line 29
    iput p2, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->recvBitRate:I

    #@e
    .line 30
    iput p3, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->sendFrameRate:I

    #@10
    .line 31
    iput p4, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->recvFrameRate:I

    #@12
    .line 33
    const-string v0, "LGIMS"

    #@14
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "sendBitRate : "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    iget v2, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->sendBitRate:I

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, " recvBitRate : "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    iget v2, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->recvBitRate:I

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string v2, " sendFrameRate : "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    iget v2, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->sendFrameRate:I

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, " recvFrameRate : "

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    iget v2, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->recvFrameRate:I

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v1

    #@4d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 20
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 21
    const-string v0, "LGIMS"

    #@5
    const-string v1, "OnScreenDebugInfoVideo()"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 22
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->readFromParcel(Landroid/os/Parcel;)V

    #@d
    .line 23
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 63
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public logIn()V
    .registers 4

    #@0
    .prologue
    .line 41
    const-string v0, "LGIMS"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "sendBitRate : ["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->sendBitRate:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, "], recvBitRate : ["

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget v2, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->recvBitRate:I

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, "], sendFrameRate : ["

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget v2, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->sendFrameRate:I

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    const-string v2, "], recvFrameRate : ["

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    iget v2, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->recvFrameRate:I

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    const-string v2, "]"

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 46
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "source"

    #@0
    .prologue
    .line 49
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    iput v0, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->sendBitRate:I

    #@6
    .line 50
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->recvBitRate:I

    #@c
    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->sendFrameRate:I

    #@12
    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->recvFrameRate:I

    #@18
    .line 53
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 56
    iget v0, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->sendBitRate:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 57
    iget v0, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->recvBitRate:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 58
    iget v0, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->sendFrameRate:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 59
    iget v0, p0, Lcom/lge/ims/service/uc/OnScreenDebugInfoVideo;->recvFrameRate:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 60
    return-void
.end method
