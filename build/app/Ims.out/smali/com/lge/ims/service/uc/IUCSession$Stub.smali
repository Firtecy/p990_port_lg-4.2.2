.class public abstract Lcom/lge/ims/service/uc/IUCSession$Stub;
.super Landroid/os/Binder;
.source "IUCSession.java"

# interfaces
.implements Lcom/lge/ims/service/uc/IUCSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUCSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.uc.IUCSession"

.field static final TRANSACTION_accept:I = 0x3

.field static final TRANSACTION_acceptUpdate:I = 0xe

.field static final TRANSACTION_dropConf:I = 0xc

.field static final TRANSACTION_expandToConf:I = 0x9

.field static final TRANSACTION_getMediaSession:I = 0x12

.field static final TRANSACTION_getProperty:I = 0x11

.field static final TRANSACTION_hold:I = 0x5

.field static final TRANSACTION_joinConf:I = 0xb

.field static final TRANSACTION_merge:I = 0xa

.field static final TRANSACTION_reject:I = 0x4

.field static final TRANSACTION_rejectUpdate:I = 0xf

.field static final TRANSACTION_resume:I = 0x6

.field static final TRANSACTION_sendDTMF:I = 0x7

.field static final TRANSACTION_setListener:I = 0x10

.field static final TRANSACTION_start:I = 0x1

.field static final TRANSACTION_startConf:I = 0x2

.field static final TRANSACTION_terminate:I = 0x8

.field static final TRANSACTION_update:I = 0xd


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.lge.ims.service.uc.IUCSession"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCSession;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.lge.ims.service.uc.IUCSession"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/uc/IUCSession;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/lge/ims/service/uc/IUCSession;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/IUCSession$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_20a

    #@4
    .line 284
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v5

    #@8
    :goto_8
    return v5

    #@9
    .line 47
    :sswitch_9
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    move v5, v6

    #@f
    .line 48
    goto :goto_8

    #@10
    .line 52
    :sswitch_10
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@12
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18
    move-result v0

    #@19
    .line 56
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    .line 58
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@20
    move-result v5

    #@21
    if-eqz v5, :cond_41

    #@23
    .line 59
    sget-object v5, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@25
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@28
    move-result-object v2

    #@29
    check-cast v2, Lcom/lge/ims/service/uc/MediaInfo;

    #@2b
    .line 65
    .local v2, _arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_2b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2e
    move-result v5

    #@2f
    if-eqz v5, :cond_43

    #@31
    .line 66
    sget-object v5, Lcom/lge/ims/service/uc/SuppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@33
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@36
    move-result-object v3

    #@37
    check-cast v3, Lcom/lge/ims/service/uc/SuppInfo;

    #@39
    .line 71
    .local v3, _arg3:Lcom/lge/ims/service/uc/SuppInfo;
    :goto_39
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/lge/ims/service/uc/IUCSession$Stub;->start(ILjava/lang/String;Lcom/lge/ims/service/uc/MediaInfo;Lcom/lge/ims/service/uc/SuppInfo;)V

    #@3c
    .line 72
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3f
    move v5, v6

    #@40
    .line 73
    goto :goto_8

    #@41
    .line 62
    .end local v2           #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v3           #_arg3:Lcom/lge/ims/service/uc/SuppInfo;
    :cond_41
    const/4 v2, 0x0

    #@42
    .restart local v2       #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_2b

    #@43
    .line 69
    :cond_43
    const/4 v3, 0x0

    #@44
    .restart local v3       #_arg3:Lcom/lge/ims/service/uc/SuppInfo;
    goto :goto_39

    #@45
    .line 77
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v3           #_arg3:Lcom/lge/ims/service/uc/SuppInfo;
    :sswitch_45
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@47
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4a
    .line 79
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v0

    #@4e
    .line 81
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@51
    move-result v5

    #@52
    if-eqz v5, :cond_80

    #@54
    .line 82
    sget-object v5, Lcom/lge/ims/service/uc/ConfInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@56
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@59
    move-result-object v1

    #@5a
    check-cast v1, Lcom/lge/ims/service/uc/ConfInfo;

    #@5c
    .line 88
    .local v1, _arg1:Lcom/lge/ims/service/uc/ConfInfo;
    :goto_5c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v5

    #@60
    if-eqz v5, :cond_82

    #@62
    .line 89
    sget-object v5, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@64
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@67
    move-result-object v2

    #@68
    check-cast v2, Lcom/lge/ims/service/uc/MediaInfo;

    #@6a
    .line 95
    .restart local v2       #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_6a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6d
    move-result v5

    #@6e
    if-eqz v5, :cond_84

    #@70
    .line 96
    sget-object v5, Lcom/lge/ims/service/uc/SuppInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@72
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@75
    move-result-object v3

    #@76
    check-cast v3, Lcom/lge/ims/service/uc/SuppInfo;

    #@78
    .line 101
    .restart local v3       #_arg3:Lcom/lge/ims/service/uc/SuppInfo;
    :goto_78
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/lge/ims/service/uc/IUCSession$Stub;->startConf(ILcom/lge/ims/service/uc/ConfInfo;Lcom/lge/ims/service/uc/MediaInfo;Lcom/lge/ims/service/uc/SuppInfo;)V

    #@7b
    .line 102
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7e
    move v5, v6

    #@7f
    .line 103
    goto :goto_8

    #@80
    .line 85
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/ConfInfo;
    .end local v2           #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v3           #_arg3:Lcom/lge/ims/service/uc/SuppInfo;
    :cond_80
    const/4 v1, 0x0

    #@81
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/ConfInfo;
    goto :goto_5c

    #@82
    .line 92
    :cond_82
    const/4 v2, 0x0

    #@83
    .restart local v2       #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_6a

    #@84
    .line 99
    :cond_84
    const/4 v3, 0x0

    #@85
    .restart local v3       #_arg3:Lcom/lge/ims/service/uc/SuppInfo;
    goto :goto_78

    #@86
    .line 107
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/ConfInfo;
    .end local v2           #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v3           #_arg3:Lcom/lge/ims/service/uc/SuppInfo;
    :sswitch_86
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@88
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8b
    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8e
    move-result v0

    #@8f
    .line 110
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->accept(I)V

    #@92
    .line 111
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@95
    move v5, v6

    #@96
    .line 112
    goto/16 :goto_8

    #@98
    .line 116
    .end local v0           #_arg0:I
    :sswitch_98
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@9a
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9d
    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a0
    move-result v0

    #@a1
    .line 119
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->reject(I)V

    #@a4
    .line 120
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a7
    move v5, v6

    #@a8
    .line 121
    goto/16 :goto_8

    #@aa
    .line 125
    .end local v0           #_arg0:I
    :sswitch_aa
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@ac
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@af
    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b2
    move-result v5

    #@b3
    if-eqz v5, :cond_c6

    #@b5
    .line 128
    sget-object v5, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b7
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@ba
    move-result-object v0

    #@bb
    check-cast v0, Lcom/lge/ims/service/uc/MediaInfo;

    #@bd
    .line 133
    .local v0, _arg0:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_bd
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->hold(Lcom/lge/ims/service/uc/MediaInfo;)V

    #@c0
    .line 134
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c3
    move v5, v6

    #@c4
    .line 135
    goto/16 :goto_8

    #@c6
    .line 131
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_c6
    const/4 v0, 0x0

    #@c7
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_bd

    #@c8
    .line 139
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_c8
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@ca
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cd
    .line 140
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->resume()V

    #@d0
    .line 141
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d3
    move v5, v6

    #@d4
    .line 142
    goto/16 :goto_8

    #@d6
    .line 146
    :sswitch_d6
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@d8
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@db
    .line 148
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@de
    move-result-object v0

    #@df
    .line 150
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@e2
    move-result v1

    #@e3
    .line 151
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSession$Stub;->sendDTMF(Ljava/lang/String;I)V

    #@e6
    .line 152
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e9
    move v5, v6

    #@ea
    .line 153
    goto/16 :goto_8

    #@ec
    .line 157
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_ec
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@ee
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@f1
    .line 159
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f4
    move-result v0

    #@f5
    .line 160
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->terminate(I)V

    #@f8
    .line 161
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fb
    move v5, v6

    #@fc
    .line 162
    goto/16 :goto_8

    #@fe
    .line 166
    .end local v0           #_arg0:I
    :sswitch_fe
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@100
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@103
    .line 168
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@106
    move-result v5

    #@107
    if-eqz v5, :cond_11a

    #@109
    .line 169
    sget-object v5, Lcom/lge/ims/service/uc/ConfInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@10b
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@10e
    move-result-object v0

    #@10f
    check-cast v0, Lcom/lge/ims/service/uc/ConfInfo;

    #@111
    .line 174
    .local v0, _arg0:Lcom/lge/ims/service/uc/ConfInfo;
    :goto_111
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->expandToConf(Lcom/lge/ims/service/uc/ConfInfo;)V

    #@114
    .line 175
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@117
    move v5, v6

    #@118
    .line 176
    goto/16 :goto_8

    #@11a
    .line 172
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    :cond_11a
    const/4 v0, 0x0

    #@11b
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    goto :goto_111

    #@11c
    .line 180
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    :sswitch_11c
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@11e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@121
    .line 181
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->merge()V

    #@124
    .line 182
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@127
    move v5, v6

    #@128
    .line 183
    goto/16 :goto_8

    #@12a
    .line 187
    :sswitch_12a
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@12c
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@12f
    .line 189
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@132
    move-result v5

    #@133
    if-eqz v5, :cond_146

    #@135
    .line 190
    sget-object v5, Lcom/lge/ims/service/uc/ConfInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@137
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@13a
    move-result-object v0

    #@13b
    check-cast v0, Lcom/lge/ims/service/uc/ConfInfo;

    #@13d
    .line 195
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    :goto_13d
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->joinConf(Lcom/lge/ims/service/uc/ConfInfo;)V

    #@140
    .line 196
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@143
    move v5, v6

    #@144
    .line 197
    goto/16 :goto_8

    #@146
    .line 193
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    :cond_146
    const/4 v0, 0x0

    #@147
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    goto :goto_13d

    #@148
    .line 201
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    :sswitch_148
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@14a
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14d
    .line 203
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@150
    move-result v5

    #@151
    if-eqz v5, :cond_164

    #@153
    .line 204
    sget-object v5, Lcom/lge/ims/service/uc/ConfInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@155
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@158
    move-result-object v0

    #@159
    check-cast v0, Lcom/lge/ims/service/uc/ConfInfo;

    #@15b
    .line 209
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    :goto_15b
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->dropConf(Lcom/lge/ims/service/uc/ConfInfo;)V

    #@15e
    .line 210
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@161
    move v5, v6

    #@162
    .line 211
    goto/16 :goto_8

    #@164
    .line 207
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    :cond_164
    const/4 v0, 0x0

    #@165
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    goto :goto_15b

    #@166
    .line 215
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/ConfInfo;
    :sswitch_166
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@168
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16b
    .line 217
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16e
    move-result v0

    #@16f
    .line 219
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@172
    move-result v5

    #@173
    if-eqz v5, :cond_186

    #@175
    .line 220
    sget-object v5, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@177
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@17a
    move-result-object v1

    #@17b
    check-cast v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@17d
    .line 225
    .local v1, _arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_17d
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSession$Stub;->update(ILcom/lge/ims/service/uc/MediaInfo;)V

    #@180
    .line 226
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@183
    move v5, v6

    #@184
    .line 227
    goto/16 :goto_8

    #@186
    .line 223
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_186
    const/4 v1, 0x0

    #@187
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_17d

    #@188
    .line 231
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_188
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@18a
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@18d
    .line 233
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@190
    move-result v0

    #@191
    .line 235
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@194
    move-result v5

    #@195
    if-eqz v5, :cond_1a8

    #@197
    .line 236
    sget-object v5, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@199
    invoke-interface {v5, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@19c
    move-result-object v1

    #@19d
    check-cast v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@19f
    .line 241
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_19f
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSession$Stub;->acceptUpdate(ILcom/lge/ims/service/uc/MediaInfo;)V

    #@1a2
    .line 242
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a5
    move v5, v6

    #@1a6
    .line 243
    goto/16 :goto_8

    #@1a8
    .line 239
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_1a8
    const/4 v1, 0x0

    #@1a9
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_19f

    #@1aa
    .line 247
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_1aa
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@1ac
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1af
    .line 249
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b2
    move-result v0

    #@1b3
    .line 250
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->rejectUpdate(I)V

    #@1b6
    .line 251
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b9
    move v5, v6

    #@1ba
    .line 252
    goto/16 :goto_8

    #@1bc
    .line 256
    .end local v0           #_arg0:I
    :sswitch_1bc
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@1be
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1c1
    .line 258
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1c4
    move-result-object v5

    #@1c5
    invoke-static {v5}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCSessionListener;

    #@1c8
    move-result-object v0

    #@1c9
    .line 259
    .local v0, _arg0:Lcom/lge/ims/service/uc/IUCSessionListener;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->setListener(Lcom/lge/ims/service/uc/IUCSessionListener;)V

    #@1cc
    .line 260
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1cf
    move v5, v6

    #@1d0
    .line 261
    goto/16 :goto_8

    #@1d2
    .line 265
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/IUCSessionListener;
    :sswitch_1d2
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@1d4
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1d7
    .line 267
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1da
    move-result v0

    #@1db
    .line 268
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->getProperty(I)I

    #@1de
    move-result v4

    #@1df
    .line 269
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1e2
    .line 270
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@1e5
    move v5, v6

    #@1e6
    .line 271
    goto/16 :goto_8

    #@1e8
    .line 275
    .end local v0           #_arg0:I
    .end local v4           #_result:I
    :sswitch_1e8
    const-string v5, "com.lge.ims.service.uc.IUCSession"

    #@1ea
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ed
    .line 277
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1f0
    move-result-object v5

    #@1f1
    invoke-static {v5}, Lcom/lge/ims/service/uc/IUCMediaSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCMediaSessionListener;

    #@1f4
    move-result-object v0

    #@1f5
    .line 278
    .local v0, _arg0:Lcom/lge/ims/service/uc/IUCMediaSessionListener;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSession$Stub;->getMediaSession(Lcom/lge/ims/service/uc/IUCMediaSessionListener;)Lcom/lge/ims/service/uc/IUCMediaSession;

    #@1f8
    move-result-object v4

    #@1f9
    .line 279
    .local v4, _result:Lcom/lge/ims/service/uc/IUCMediaSession;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1fc
    .line 280
    if-eqz v4, :cond_208

    #@1fe
    invoke-interface {v4}, Lcom/lge/ims/service/uc/IUCMediaSession;->asBinder()Landroid/os/IBinder;

    #@201
    move-result-object v5

    #@202
    :goto_202
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@205
    move v5, v6

    #@206
    .line 281
    goto/16 :goto_8

    #@208
    .line 280
    :cond_208
    const/4 v5, 0x0

    #@209
    goto :goto_202

    #@20a
    .line 43
    :sswitch_data_20a
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_45
        0x3 -> :sswitch_86
        0x4 -> :sswitch_98
        0x5 -> :sswitch_aa
        0x6 -> :sswitch_c8
        0x7 -> :sswitch_d6
        0x8 -> :sswitch_ec
        0x9 -> :sswitch_fe
        0xa -> :sswitch_11c
        0xb -> :sswitch_12a
        0xc -> :sswitch_148
        0xd -> :sswitch_166
        0xe -> :sswitch_188
        0xf -> :sswitch_1aa
        0x10 -> :sswitch_1bc
        0x11 -> :sswitch_1d2
        0x12 -> :sswitch_1e8
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
