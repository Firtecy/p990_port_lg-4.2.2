.class final Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;
.super Ljava/lang/Object;
.source "ACHttpConnection.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ac/ACHttpConnection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "AuthCredentials"
.end annotation


# instance fields
.field public mNonce:Ljava/lang/String;

.field public mRealm:Ljava/lang/String;

.field public mResponse:Ljava/lang/String;

.field public mUri:Ljava/lang/String;

.field public mUsername:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/ims/service/ac/ACHttpConnection;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/ac/ACHttpConnection;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "uri"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 571
    iput-object p1, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->this$0:Lcom/lge/ims/service/ac/ACHttpConnection;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 565
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mRealm:Ljava/lang/String;

    #@8
    .line 566
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mNonce:Ljava/lang/String;

    #@a
    .line 567
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mUri:Ljava/lang/String;

    #@c
    .line 568
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mResponse:Ljava/lang/String;

    #@e
    .line 569
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mUsername:Ljava/lang/String;

    #@10
    .line 572
    iput-object p2, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mUri:Ljava/lang/String;

    #@12
    .line 573
    return-void
.end method


# virtual methods
.method public getNonce()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 576
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mNonce:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public parse(Ljava/lang/String;)V
    .registers 12
    .parameter "wwwAuthenticate"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 580
    if-nez p1, :cond_5

    #@4
    .line 609
    :cond_4
    return-void

    #@5
    .line 585
    :cond_5
    const/16 v5, 0x20

    #@7
    invoke-virtual {p1, v5}, Ljava/lang/String;->indexOf(I)I

    #@a
    move-result v1

    #@b
    .line 586
    .local v1, index:I
    move-object v3, p1

    #@c
    .line 588
    .local v3, tmp:Ljava/lang/String;
    const/4 v5, -0x1

    #@d
    if-eq v1, v5, :cond_13

    #@f
    .line 589
    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    .line 592
    :cond_13
    const-string v5, "\\p{Space}"

    #@15
    const-string v6, ""

    #@17
    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    .line 594
    const-string v5, ","

    #@1d
    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    .line 596
    .local v4, tokens:[Ljava/lang/String;
    const/4 v0, 0x0

    #@22
    .local v0, i:I
    :goto_22
    array-length v5, v4

    #@23
    if-ge v0, v5, :cond_4

    #@25
    .line 597
    aget-object v5, v4, v0

    #@27
    const-string v6, "="

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    .line 599
    .local v2, nameValue:[Ljava/lang/String;
    array-length v5, v2

    #@2e
    const/4 v6, 0x2

    #@2f
    if-eq v5, v6, :cond_34

    #@31
    .line 596
    :cond_31
    :goto_31
    add-int/lit8 v0, v0, 0x1

    #@33
    goto :goto_22

    #@34
    .line 603
    :cond_34
    const-string v5, "realm"

    #@36
    aget-object v6, v2, v8

    #@38
    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@3b
    move-result v5

    #@3c
    if-eqz v5, :cond_4b

    #@3e
    .line 604
    aget-object v5, v2, v9

    #@40
    const-string v6, "\""

    #@42
    const-string v7, ""

    #@44
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@47
    move-result-object v5

    #@48
    iput-object v5, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mRealm:Ljava/lang/String;

    #@4a
    goto :goto_31

    #@4b
    .line 605
    :cond_4b
    const-string v5, "nonce"

    #@4d
    aget-object v6, v2, v8

    #@4f
    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@52
    move-result v5

    #@53
    if-eqz v5, :cond_31

    #@55
    .line 606
    aget-object v5, v2, v9

    #@57
    const-string v6, "\""

    #@59
    const-string v7, ""

    #@5b
    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@5e
    move-result-object v5

    #@5f
    iput-object v5, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mNonce:Ljava/lang/String;

    #@61
    goto :goto_31
.end method

.method public setResponse(Ljava/lang/String;)V
    .registers 2
    .parameter "response"

    #@0
    .prologue
    .line 612
    iput-object p1, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mResponse:Ljava/lang/String;

    #@2
    .line 613
    return-void
.end method

.method public setUsername(Ljava/lang/String;)V
    .registers 2
    .parameter "username"

    #@0
    .prologue
    .line 616
    iput-object p1, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mUsername:Ljava/lang/String;

    #@2
    .line 617
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 620
    const-string v0, "Digest "

    #@2
    .line 622
    .local v0, authorization:Ljava/lang/String;
    iget-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mUsername:Ljava/lang/String;

    #@4
    if-eqz v1, :cond_52

    #@6
    .line 623
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, "username="

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 624
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    const-string v2, "\""

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    .line 625
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mUsername:Ljava/lang/String;

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    .line 626
    new-instance v1, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    const-string v2, "\""

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v0

    #@52
    .line 629
    :cond_52
    iget-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mRealm:Ljava/lang/String;

    #@54
    if-eqz v1, :cond_bb

    #@56
    .line 630
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@59
    move-result v1

    #@5a
    if-lez v1, :cond_6f

    #@5c
    .line 631
    new-instance v1, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    const-string v2, ","

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v0

    #@6f
    .line 634
    :cond_6f
    new-instance v1, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v1

    #@78
    const-string v2, "realm="

    #@7a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v1

    #@7e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v0

    #@82
    .line 635
    new-instance v1, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v1

    #@8b
    const-string v2, "\""

    #@8d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v1

    #@91
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    .line 636
    new-instance v1, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v1

    #@9e
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mRealm:Ljava/lang/String;

    #@a0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v1

    #@a4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v0

    #@a8
    .line 637
    new-instance v1, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v1

    #@b1
    const-string v2, "\""

    #@b3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v1

    #@b7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v0

    #@bb
    .line 640
    :cond_bb
    iget-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mNonce:Ljava/lang/String;

    #@bd
    if-eqz v1, :cond_124

    #@bf
    .line 641
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@c2
    move-result v1

    #@c3
    if-lez v1, :cond_d8

    #@c5
    .line 642
    new-instance v1, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v1

    #@ce
    const-string v2, ","

    #@d0
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v1

    #@d4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v0

    #@d8
    .line 645
    :cond_d8
    new-instance v1, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v1

    #@e1
    const-string v2, "nonce="

    #@e3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v1

    #@e7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ea
    move-result-object v0

    #@eb
    .line 646
    new-instance v1, Ljava/lang/StringBuilder;

    #@ed
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v1

    #@f4
    const-string v2, "\""

    #@f6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v1

    #@fa
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v0

    #@fe
    .line 647
    new-instance v1, Ljava/lang/StringBuilder;

    #@100
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@103
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v1

    #@107
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mNonce:Ljava/lang/String;

    #@109
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v1

    #@10d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v0

    #@111
    .line 648
    new-instance v1, Ljava/lang/StringBuilder;

    #@113
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@116
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v1

    #@11a
    const-string v2, "\""

    #@11c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v1

    #@120
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@123
    move-result-object v0

    #@124
    .line 651
    :cond_124
    iget-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mUri:Ljava/lang/String;

    #@126
    if-eqz v1, :cond_18d

    #@128
    .line 652
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@12b
    move-result v1

    #@12c
    if-lez v1, :cond_141

    #@12e
    .line 653
    new-instance v1, Ljava/lang/StringBuilder;

    #@130
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@133
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v1

    #@137
    const-string v2, ","

    #@139
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v1

    #@13d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@140
    move-result-object v0

    #@141
    .line 656
    :cond_141
    new-instance v1, Ljava/lang/StringBuilder;

    #@143
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@146
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v1

    #@14a
    const-string v2, "uri="

    #@14c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v1

    #@150
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@153
    move-result-object v0

    #@154
    .line 657
    new-instance v1, Ljava/lang/StringBuilder;

    #@156
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@159
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v1

    #@15d
    const-string v2, "\""

    #@15f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v1

    #@163
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@166
    move-result-object v0

    #@167
    .line 658
    new-instance v1, Ljava/lang/StringBuilder;

    #@169
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v1

    #@170
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mUri:Ljava/lang/String;

    #@172
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@175
    move-result-object v1

    #@176
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@179
    move-result-object v0

    #@17a
    .line 659
    new-instance v1, Ljava/lang/StringBuilder;

    #@17c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17f
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v1

    #@183
    const-string v2, "\""

    #@185
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v1

    #@189
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18c
    move-result-object v0

    #@18d
    .line 662
    :cond_18d
    iget-object v1, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mResponse:Ljava/lang/String;

    #@18f
    if-eqz v1, :cond_1f6

    #@191
    .line 663
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@194
    move-result v1

    #@195
    if-lez v1, :cond_1aa

    #@197
    .line 664
    new-instance v1, Ljava/lang/StringBuilder;

    #@199
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v1

    #@1a0
    const-string v2, ","

    #@1a2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a5
    move-result-object v1

    #@1a6
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a9
    move-result-object v0

    #@1aa
    .line 667
    :cond_1aa
    new-instance v1, Ljava/lang/StringBuilder;

    #@1ac
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1af
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v1

    #@1b3
    const-string v2, "response="

    #@1b5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v1

    #@1b9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1bc
    move-result-object v0

    #@1bd
    .line 668
    new-instance v1, Ljava/lang/StringBuilder;

    #@1bf
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c5
    move-result-object v1

    #@1c6
    const-string v2, "\""

    #@1c8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cb
    move-result-object v1

    #@1cc
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cf
    move-result-object v0

    #@1d0
    .line 669
    new-instance v1, Ljava/lang/StringBuilder;

    #@1d2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v1

    #@1d9
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACHttpConnection$AuthCredentials;->mResponse:Ljava/lang/String;

    #@1db
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1de
    move-result-object v1

    #@1df
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e2
    move-result-object v0

    #@1e3
    .line 670
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e8
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v1

    #@1ec
    const-string v2, "\""

    #@1ee
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v1

    #@1f2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f5
    move-result-object v0

    #@1f6
    .line 673
    :cond_1f6
    return-object v0
.end method
