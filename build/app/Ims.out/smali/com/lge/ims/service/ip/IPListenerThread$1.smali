.class Lcom/lge/ims/service/ip/IPListenerThread$1;
.super Landroid/os/Handler;
.source "IPListenerThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/ims/service/ip/IPListenerThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/ip/IPListenerThread;


# direct methods
.method constructor <init>(Lcom/lge/ims/service/ip/IPListenerThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 78
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPListenerThread$1;->this$0:Lcom/lge/ims/service/ip/IPListenerThread;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "objMessage"

    #@0
    .prologue
    .line 81
    if-nez p1, :cond_8

    #@2
    .line 82
    const-string v4, "[IP][ERROR]Message is null"

    #@4
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@7
    .line 145
    :cond_7
    :goto_7
    return-void

    #@8
    .line 86
    :cond_8
    iget v4, p1, Landroid/os/Message;->what:I

    #@a
    sparse-switch v4, :sswitch_data_a0

    #@d
    goto :goto_7

    #@e
    .line 139
    :sswitch_e
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPListenerThread$1;->this$0:Lcom/lge/ims/service/ip/IPListenerThread;

    #@10
    invoke-static {v4}, Lcom/lge/ims/service/ip/IPListenerThread;->access$000(Lcom/lge/ims/service/ip/IPListenerThread;)Landroid/os/Handler;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Landroid/os/Looper;->quit()V

    #@1b
    goto :goto_7

    #@1c
    .line 89
    :sswitch_1c
    const-string v4, "[IP] IP_MSG_AGENT_LISTENER_NOTIFYQUERYCAPA"

    #@1e
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@21
    .line 90
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@24
    move-result-object v1

    #@25
    .line 91
    .local v1, objBundle:Landroid/os/Bundle;
    const-string v4, "capa"

    #@27
    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@2a
    move-result-object v2

    #@2b
    check-cast v2, Lcom/lge/ims/service/ip/CapaInfo;

    #@2d
    .line 92
    .local v2, objCapaInfo:Lcom/lge/ims/service/ip/CapaInfo;
    const/4 v0, 0x0

    #@2e
    .line 94
    .local v0, nCapaType:I
    if-nez v2, :cond_36

    #@30
    .line 95
    const-string v4, "[IP][ERROR]IP_MSG_AGENT_LISTENER_NOTIFYQUERYCAPA : CapaInfo is null"

    #@32
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@35
    goto :goto_7

    #@36
    .line 99
    :cond_36
    invoke-static {}, Lcom/lge/ims/service/ip/IPContactHelper;->GetInstance()Lcom/lge/ims/service/ip/IPContactHelper;

    #@39
    move-result-object v4

    #@3a
    if-eqz v4, :cond_43

    #@3c
    .line 100
    invoke-static {}, Lcom/lge/ims/service/ip/IPContactHelper;->GetInstance()Lcom/lge/ims/service/ip/IPContactHelper;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateCapability(Lcom/lge/ims/service/ip/CapaInfo;)I

    #@43
    .line 102
    :cond_43
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/CapaInfo;->GetMSISDN()Ljava/lang/String;

    #@4a
    move-result-object v5

    #@4b
    invoke-virtual {v4, v5}, Lcom/lge/ims/service/ip/IPQueryRepository;->RemoveQuery(Ljava/lang/String;)V

    #@4e
    goto :goto_7

    #@4f
    .line 107
    .end local v0           #nCapaType:I
    .end local v1           #objBundle:Landroid/os/Bundle;
    .end local v2           #objCapaInfo:Lcom/lge/ims/service/ip/CapaInfo;
    :sswitch_4f
    const-string v4, "[IP] IP_MSG_AGENT_LISTENER_NOTIFYQUERYPRESENCE"

    #@51
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@54
    .line 108
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@57
    move-result-object v1

    #@58
    .line 109
    .restart local v1       #objBundle:Landroid/os/Bundle;
    const-string v4, "presence"

    #@5a
    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@5d
    move-result-object v3

    #@5e
    check-cast v3, Lcom/lge/ims/service/ip/PresenceInfo;

    #@60
    .line 111
    .local v3, objPresenceInfo:Lcom/lge/ims/service/ip/PresenceInfo;
    if-nez v3, :cond_68

    #@62
    .line 112
    const-string v4, "[IP][ERROR]IP_MSG_AGENT_LISTENER_NOTIFYQUERYPRESENCE PresenceInfo is null"

    #@64
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@67
    goto :goto_7

    #@68
    .line 116
    :cond_68
    invoke-static {}, Lcom/lge/ims/service/ip/IPContactHelper;->GetInstance()Lcom/lge/ims/service/ip/IPContactHelper;

    #@6b
    move-result-object v4

    #@6c
    if-eqz v4, :cond_7

    #@6e
    .line 117
    invoke-static {}, Lcom/lge/ims/service/ip/IPContactHelper;->GetInstance()Lcom/lge/ims/service/ip/IPContactHelper;

    #@71
    move-result-object v4

    #@72
    invoke-virtual {v4, v3}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateBuddyPresenceInformation(Lcom/lge/ims/service/ip/PresenceInfo;)I

    #@75
    goto :goto_7

    #@76
    .line 123
    .end local v1           #objBundle:Landroid/os/Bundle;
    .end local v3           #objPresenceInfo:Lcom/lge/ims/service/ip/PresenceInfo;
    :sswitch_76
    const-string v4, "[IP] IP_MSG_AGENT_LISTENER_GETBUDDYSTATUSICON"

    #@78
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7b
    .line 124
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@7e
    move-result-object v1

    #@7f
    .line 125
    .restart local v1       #objBundle:Landroid/os/Bundle;
    const-string v4, "presence"

    #@81
    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@84
    move-result-object v3

    #@85
    check-cast v3, Lcom/lge/ims/service/ip/PresenceInfo;

    #@87
    .line 127
    .restart local v3       #objPresenceInfo:Lcom/lge/ims/service/ip/PresenceInfo;
    if-nez v3, :cond_90

    #@89
    .line 128
    const-string v4, "[IP][ERROR] IP_MSG_AGENT_LISTENER_GETBUDDYSTATUSICON PresenceInfo is null"

    #@8b
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@8e
    goto/16 :goto_7

    #@90
    .line 132
    :cond_90
    invoke-static {}, Lcom/lge/ims/service/ip/IPContactHelper;->GetInstance()Lcom/lge/ims/service/ip/IPContactHelper;

    #@93
    move-result-object v4

    #@94
    if-eqz v4, :cond_7

    #@96
    .line 133
    invoke-static {}, Lcom/lge/ims/service/ip/IPContactHelper;->GetInstance()Lcom/lge/ims/service/ip/IPContactHelper;

    #@99
    move-result-object v4

    #@9a
    invoke-virtual {v4, v3}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateBuddyStatusIconInformation(Lcom/lge/ims/service/ip/PresenceInfo;)I

    #@9d
    goto/16 :goto_7

    #@9f
    .line 86
    nop

    #@a0
    :sswitch_data_a0
    .sparse-switch
        0xc -> :sswitch_e
        0xd -> :sswitch_1c
        0x14 -> :sswitch_4f
        0x15 -> :sswitch_76
    .end sparse-switch
.end method
