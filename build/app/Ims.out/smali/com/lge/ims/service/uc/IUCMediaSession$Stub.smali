.class public abstract Lcom/lge/ims/service/uc/IUCMediaSession$Stub;
.super Landroid/os/Binder;
.source "IUCMediaSession.java"

# interfaces
.implements Lcom/lge/ims/service/uc/IUCMediaSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUCMediaSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/IUCMediaSession$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.uc.IUCMediaSession"

.field static final TRANSACTION_audioStop:I = 0x2

.field static final TRANSACTION_capture:I = 0x8

.field static final TRANSACTION_changeCameraBrightness:I = 0x5

.field static final TRANSACTION_changeCameraZoom:I = 0x4

.field static final TRANSACTION_changeOrientation:I = 0x10

.field static final TRANSACTION_changeViewSize:I = 0xf

.field static final TRANSACTION_getSessionId:I = 0x1

.field static final TRANSACTION_selectCamera:I = 0x3

.field static final TRANSACTION_startAlternateImage:I = 0xb

.field static final TRANSACTION_startBackground:I = 0x6

.field static final TRANSACTION_startRecording:I = 0x9

.field static final TRANSACTION_stopAlternateImage:I = 0xc

.field static final TRANSACTION_stopBackground:I = 0x7

.field static final TRANSACTION_stopRecording:I = 0xa

.field static final TRANSACTION_swapDisplay:I = 0xd

.field static final TRANSACTION_updateDisplay:I = 0xe


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.lge.ims.service.uc.IUCMediaSession"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCMediaSession;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.lge.ims.service.uc.IUCMediaSession"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/uc/IUCMediaSession;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/lge/ims/service/uc/IUCMediaSession;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/lge/ims/service/uc/IUCMediaSession$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_110

    #@4
    .line 190
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v3

    #@8
    :goto_8
    return v3

    #@9
    .line 47
    :sswitch_9
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@11
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 53
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->getSessionId()I

    #@17
    move-result v2

    #@18
    .line 54
    .local v2, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1b
    .line 55
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    goto :goto_8

    #@1f
    .line 60
    .end local v2           #_result:I
    :sswitch_1f
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@21
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@24
    .line 61
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->audioStop()V

    #@27
    .line 62
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a
    goto :goto_8

    #@2b
    .line 67
    :sswitch_2b
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@2d
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30
    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@33
    move-result v0

    #@34
    .line 70
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->selectCamera(I)V

    #@37
    .line 71
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a
    goto :goto_8

    #@3b
    .line 76
    .end local v0           #_arg0:I
    :sswitch_3b
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@3d
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40
    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v0

    #@44
    .line 79
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->changeCameraZoom(I)V

    #@47
    .line 80
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a
    goto :goto_8

    #@4b
    .line 85
    .end local v0           #_arg0:I
    :sswitch_4b
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@4d
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50
    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@53
    move-result v0

    #@54
    .line 88
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->changeCameraBrightness(I)V

    #@57
    .line 89
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5a
    goto :goto_8

    #@5b
    .line 94
    .end local v0           #_arg0:I
    :sswitch_5b
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@5d
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60
    .line 95
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->startBackground()V

    #@63
    .line 96
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@66
    goto :goto_8

    #@67
    .line 101
    :sswitch_67
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@69
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c
    .line 102
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->stopBackground()V

    #@6f
    .line 103
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@72
    goto :goto_8

    #@73
    .line 108
    :sswitch_73
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@75
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@78
    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7b
    move-result-object v0

    #@7c
    .line 112
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@7f
    move-result v1

    #@80
    .line 113
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->capture(Ljava/lang/String;I)V

    #@83
    .line 114
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@86
    goto :goto_8

    #@87
    .line 119
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_87
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@89
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8c
    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8f
    move-result-object v0

    #@90
    .line 123
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@93
    move-result v1

    #@94
    .line 124
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->startRecording(Ljava/lang/String;I)V

    #@97
    .line 125
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@9a
    goto/16 :goto_8

    #@9c
    .line 130
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_9c
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@9e
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a1
    .line 131
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->stopRecording()V

    #@a4
    .line 132
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a7
    goto/16 :goto_8

    #@a9
    .line 137
    :sswitch_a9
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@ab
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ae
    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@b1
    move-result-object v0

    #@b2
    .line 140
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->startAlternateImage(Ljava/lang/String;)V

    #@b5
    .line 141
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@b8
    goto/16 :goto_8

    #@ba
    .line 146
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_ba
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@bc
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@bf
    .line 147
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->stopAlternateImage()V

    #@c2
    .line 148
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@c5
    goto/16 :goto_8

    #@c7
    .line 153
    :sswitch_c7
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@c9
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@cc
    .line 154
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->swapDisplay()V

    #@cf
    .line 155
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d2
    goto/16 :goto_8

    #@d4
    .line 160
    :sswitch_d4
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@d6
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@d9
    .line 162
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@dc
    move-result v0

    #@dd
    .line 163
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->updateDisplay(I)V

    #@e0
    .line 164
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e3
    goto/16 :goto_8

    #@e5
    .line 169
    .end local v0           #_arg0:I
    :sswitch_e5
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@e7
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ea
    .line 171
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ed
    move-result v0

    #@ee
    .line 173
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@f1
    move-result v1

    #@f2
    .line 174
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->changeViewSize(II)V

    #@f5
    .line 175
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@f8
    goto/16 :goto_8

    #@fa
    .line 180
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_fa
    const-string v4, "com.lge.ims.service.uc.IUCMediaSession"

    #@fc
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ff
    .line 182
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@102
    move-result v0

    #@103
    .line 184
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@106
    move-result v1

    #@107
    .line 185
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCMediaSession$Stub;->changeOrientation(II)V

    #@10a
    .line 186
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@10d
    goto/16 :goto_8

    #@10f
    .line 43
    nop

    #@110
    :sswitch_data_110
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1f
        0x3 -> :sswitch_2b
        0x4 -> :sswitch_3b
        0x5 -> :sswitch_4b
        0x6 -> :sswitch_5b
        0x7 -> :sswitch_67
        0x8 -> :sswitch_73
        0x9 -> :sswitch_87
        0xa -> :sswitch_9c
        0xb -> :sswitch_a9
        0xc -> :sswitch_ba
        0xd -> :sswitch_c7
        0xe -> :sswitch_d4
        0xf -> :sswitch_e5
        0x10 -> :sswitch_fa
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
