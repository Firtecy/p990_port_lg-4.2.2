.class public Lcom/lge/ims/service/cs/ContentShareService;
.super Landroid/app/Service;
.source "ContentShareService.java"


# instance fields
.field private contentShare:Lcom/lge/ims/service/cs/ContentShareServiceImpl;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 21
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/lge/ims/service/cs/ContentShareService;->contentShare:Lcom/lge/ims/service/cs/ContentShareServiceImpl;

    #@6
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 35
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 37
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareService;->contentShare:Lcom/lge/ims/service/cs/ContentShareServiceImpl;

    #@7
    if-nez v0, :cond_10

    #@9
    .line 38
    new-instance v0, Lcom/lge/ims/service/cs/ContentShareServiceImpl;

    #@b
    invoke-direct {v0}, Lcom/lge/ims/service/cs/ContentShareServiceImpl;-><init>()V

    #@e
    iput-object v0, p0, Lcom/lge/ims/service/cs/ContentShareService;->contentShare:Lcom/lge/ims/service/cs/ContentShareServiceImpl;

    #@10
    .line 41
    :cond_10
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareService;->contentShare:Lcom/lge/ims/service/cs/ContentShareServiceImpl;

    #@12
    return-object v0
.end method

.method public onCreate()V
    .registers 2

    #@0
    .prologue
    .line 25
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 26
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@8
    .line 27
    return-void
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 31
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 32
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 45
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 47
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareService;->contentShare:Lcom/lge/ims/service/cs/ContentShareServiceImpl;

    #@7
    if-eqz v0, :cond_11

    #@9
    .line 48
    iget-object v0, p0, Lcom/lge/ims/service/cs/ContentShareService;->contentShare:Lcom/lge/ims/service/cs/ContentShareServiceImpl;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/cs/ContentShareServiceImpl;->terminate()V

    #@e
    .line 49
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Lcom/lge/ims/service/cs/ContentShareService;->contentShare:Lcom/lge/ims/service/cs/ContentShareServiceImpl;

    #@11
    .line 52
    :cond_11
    const/4 v0, 0x1

    #@12
    return v0
.end method
