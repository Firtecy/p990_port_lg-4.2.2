.class public Lcom/lge/ims/service/uc/SessInfo;
.super Ljava/lang/Object;
.source "SessInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/uc/SessInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "LGIMS"


# instance fields
.field public IsConf:Z

.field public ServiceType:I

.field public SessionType:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 78
    new-instance v0, Lcom/lge/ims/service/uc/SessInfo$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/uc/SessInfo$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(IIZ)V
    .registers 7
    .parameter "_ServiceType"
    .parameter "_SessionType"
    .parameter "_IsConf"

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 28
    const-string v0, "LGIMS"

    #@5
    const-string v1, "SessInfo()"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 30
    iput p1, p0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@c
    .line 31
    iput p2, p0, Lcom/lge/ims/service/uc/SessInfo;->SessionType:I

    #@e
    .line 32
    iput-boolean p3, p0, Lcom/lge/ims/service/uc/SessInfo;->IsConf:Z

    #@10
    .line 34
    const-string v0, "LGIMS"

    #@12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v2, "ServiceType : "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget v2, p0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, " SessionType : "

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    iget v2, p0, Lcom/lge/ims/service/uc/SessInfo;->SessionType:I

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    const-string v2, " IsConf : "

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    iget-boolean v2, p0, Lcom/lge/ims/service/uc/SessInfo;->IsConf:Z

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 21
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 22
    const-string v0, "LGIMS"

    #@5
    const-string v1, "SessInfo()"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 23
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/uc/SessInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@d
    .line 24
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 75
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public logIn()V
    .registers 4

    #@0
    .prologue
    .line 43
    const-string v0, "LGIMS"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "ServiceType : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " SessionType : "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget v2, p0, Lcom/lge/ims/service/uc/SessInfo;->SessionType:I

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " IsConf : "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    iget-boolean v2, p0, Lcom/lge/ims/service/uc/SessInfo;->IsConf:Z

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 47
    return-void
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 51
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4
    move-result v0

    #@5
    iput v0, p0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@7
    .line 52
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v0

    #@b
    iput v0, p0, Lcom/lge/ims/service/uc/SessInfo;->SessionType:I

    #@d
    .line 54
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@10
    move-result v0

    #@11
    if-ne v0, v1, :cond_16

    #@13
    .line 55
    iput-boolean v1, p0, Lcom/lge/ims/service/uc/SessInfo;->IsConf:Z

    #@15
    .line 60
    :goto_15
    return-void

    #@16
    .line 58
    :cond_16
    const/4 v0, 0x0

    #@17
    iput-boolean v0, p0, Lcom/lge/ims/service/uc/SessInfo;->IsConf:Z

    #@19
    goto :goto_15
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 64
    iget v0, p0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@5
    .line 65
    iget v0, p0, Lcom/lge/ims/service/uc/SessInfo;->SessionType:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 67
    iget-boolean v0, p0, Lcom/lge/ims/service/uc/SessInfo;->IsConf:Z

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 68
    const/4 v0, 0x1

    #@f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    .line 72
    :goto_12
    return-void

    #@13
    .line 70
    :cond_13
    const/4 v0, 0x0

    #@14
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    goto :goto_12
.end method
