.class public interface abstract Lcom/lge/ims/service/uc/IUCService;
.super Ljava/lang/Object;
.source "IUCService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/IUCService$Stub;
    }
.end annotation


# virtual methods
.method public abstract attachSession(I)Lcom/lge/ims/service/uc/IUCSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract closeSession(Lcom/lge/ims/service/uc/IUCSession;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract openSession(I)Lcom/lge/ims/service/uc/IUCSession;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract setListener(Lcom/lge/ims/service/uc/IUCServiceListener;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
