.class public Lcom/lge/ims/service/cd/ServiceCapabilities;
.super Ljava/lang/Object;
.source "ServiceCapabilities.java"


# static fields
.field public static final MEDIA_ALL:I = 0xff0000

.field public static final MEDIA_CONTACTS:I = 0x100000

.field public static final MEDIA_IMAGE:I = 0x20000

.field public static final MEDIA_MAP:I = 0x80000

.field public static final MEDIA_MEMO:I = 0x200000

.field public static final MEDIA_MM_MEMO:I = 0x400000

.field public static final MEDIA_MUSIC:I = 0x40000

.field public static final MEDIA_VIDEO:I = 0x10000

.field public static final MEDIA_VOICE_RECORD:I = 0x800000

.field public static final SERVICE_ALL_M:I = 0xf

.field public static final SERVICE_CS:I = 0xc

.field public static final SERVICE_DP:I = 0x200

.field public static final SERVICE_FT:I = 0x1

.field public static final SERVICE_FT_ALL:I = 0x1005

.field public static final SERVICE_FT_HTTP:I = 0x1000

.field public static final SERVICE_IM:I = 0x2

.field public static final SERVICE_IS:I = 0x4

.field public static final SERVICE_MIM:I = 0x2000

.field public static final SERVICE_RICH_CHAT:I = 0x3

.field public static final SERVICE_SP:I = 0x100

.field public static final SERVICE_VS:I = 0x8

.field private static final TAG:Ljava/lang/String; = "IMS"


# instance fields
.field private mediaCapabilities:I

.field private phoneNumber:Ljava/lang/String;

.field private serviceCapabilities:I


# direct methods
.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 83
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 69
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->phoneNumber:Ljava/lang/String;

    #@7
    .line 70
    iput v1, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@9
    .line 71
    iput v1, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@b
    .line 84
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->phoneNumber:Ljava/lang/String;

    #@11
    .line 85
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@14
    move-result v0

    #@15
    iput v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@17
    .line 86
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1a
    move-result v0

    #@1b
    iput v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@1d
    .line 88
    const-string v0, "IMS"

    #@1f
    new-instance v1, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v2, "[ServiceCapabilities] "

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@35
    .line 89
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .registers 7
    .parameter "phoneNumber"
    .parameter "serviceCapabilities"
    .parameter "mediaCapabilities"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 69
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->phoneNumber:Ljava/lang/String;

    #@7
    .line 70
    iput v1, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@9
    .line 71
    iput v1, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@b
    .line 76
    iput-object p1, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->phoneNumber:Ljava/lang/String;

    #@d
    .line 77
    iput p2, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@f
    .line 78
    iput p3, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@11
    .line 80
    const-string v0, "IMS"

    #@13
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v2, "[ServiceCapabilities] "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 81
    return-void
.end method


# virtual methods
.method public clearMediaCapabilities(I)V
    .registers 4
    .parameter "mediaCapabilities"

    #@0
    .prologue
    .line 93
    iget v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@2
    xor-int/lit8 v1, p1, -0x1

    #@4
    and-int/2addr v0, v1

    #@5
    iput v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@7
    .line 94
    return-void
.end method

.method public clearServiceCapabilities(I)V
    .registers 4
    .parameter "serviceCapabilities"

    #@0
    .prologue
    .line 98
    iget v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@2
    xor-int/lit8 v1, p1, -0x1

    #@4
    and-int/2addr v0, v1

    #@5
    iput v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@7
    .line 99
    return-void
.end method

.method public getMediaCapabilities()I
    .registers 2

    #@0
    .prologue
    .line 103
    iget v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@2
    return v0
.end method

.method public getPhoneNumber()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->phoneNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getServiceCapabilities()I
    .registers 2

    #@0
    .prologue
    .line 113
    iget v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@2
    return v0
.end method

.method public isMediaSupported(I)Z
    .registers 3
    .parameter "media"

    #@0
    .prologue
    .line 118
    iget v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@2
    and-int/2addr v0, p1

    #@3
    if-ne v0, p1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public isServiceSupported(I)Z
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 123
    iget v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@2
    and-int/2addr v0, p1

    #@3
    if-ne v0, p1, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public setMediaCapabilities(I)V
    .registers 3
    .parameter "mediaCapabilities"

    #@0
    .prologue
    .line 128
    iget v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@5
    .line 129
    return-void
.end method

.method public setServiceCapabilities(I)V
    .registers 3
    .parameter "serviceCapabilities"

    #@0
    .prologue
    .line 133
    iget v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@2
    or-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@5
    .line 134
    return-void
.end method

.method public toCapabilities()Lcom/lge/ims/service/cd/Capabilities;
    .registers 5

    #@0
    .prologue
    .line 138
    new-instance v0, Lcom/lge/ims/service/cd/Capabilities;

    #@2
    iget-object v1, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->phoneNumber:Ljava/lang/String;

    #@4
    iget v2, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@6
    iget v3, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@8
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/ims/service/cd/Capabilities;-><init>(Ljava/lang/String;II)V

    #@b
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "phoneNumber = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->phoneNumber:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", serviceCapabilities = "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->serviceCapabilities:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", mediaCapabilities = "

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/lge/ims/service/cd/ServiceCapabilities;->mediaCapabilities:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    return-object v0
.end method
