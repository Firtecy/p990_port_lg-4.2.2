.class Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;
.super Ljava/lang/Object;
.source "IContentShare.java"

# interfaces
.implements Lcom/lge/ims/service/cs/IContentShare;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cs/IContentShare$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 142
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 143
    iput-object p1, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 144
    return-void
.end method


# virtual methods
.method public addCall(Ljava/lang/String;I)V
    .registers 8
    .parameter "number"
    .parameter "callMode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 175
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 176
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 178
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cs.IContentShare"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 179
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 180
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 181
    iget-object v2, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v3, 0x2

    #@16
    const/4 v4, 0x0

    #@17
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 182
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_24

    #@1d
    .line 185
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 186
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 188
    return-void

    #@24
    .line 185
    :catchall_24
    move-exception v2

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 186
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v2
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 147
    iget-object v0, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public closeImageSession(Lcom/lge/ims/service/cs/IImageSession;)V
    .registers 7
    .parameter "iImageSession"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 285
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 286
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 288
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cs.IContentShare"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 289
    if-eqz p1, :cond_28

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/cs/IImageSession;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 290
    iget-object v2, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x9

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 291
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2a

    #@21
    .line 294
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 295
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 297
    return-void

    #@28
    .line 289
    :cond_28
    const/4 v2, 0x0

    #@29
    goto :goto_13

    #@2a
    .line 294
    :catchall_2a
    move-exception v2

    #@2b
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 295
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    throw v2
.end method

.method public closeVideoSession(Lcom/lge/ims/service/cs/IVideoSession;)V
    .registers 7
    .parameter "iVideoSession"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 253
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 254
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 256
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cs.IContentShare"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 257
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/cs/IVideoSession;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 258
    iget-object v2, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x7

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 259
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 262
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 263
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 265
    return-void

    #@27
    .line 257
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 262
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 263
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 151
    const-string v0, "com.lge.ims.service.cs.IContentShare"

    #@2
    return-object v0
.end method

.method public getServiceNetworkType()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 304
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 305
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 308
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 309
    iget-object v3, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0xa

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 310
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 311
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_23

    #@1b
    move-result v2

    #@1c
    .line 314
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 315
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 317
    return v2

    #@23
    .line 314
    .end local v2           #_result:I
    :catchall_23
    move-exception v3

    #@24
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 315
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2a
    throw v3
.end method

.method public holdCall(Ljava/lang/String;)V
    .registers 7
    .parameter "number"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 206
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 207
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 209
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cs.IContentShare"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 210
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 211
    iget-object v2, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x4

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 212
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 215
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 216
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 218
    return-void

    #@21
    .line 215
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 216
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public openImageSession()Lcom/lge/ims/service/cs/IImageSession;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 268
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 269
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 272
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 273
    iget-object v3, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v4, 0x8

    #@11
    const/4 v5, 0x0

    #@12
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 274
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@18
    .line 275
    invoke-virtual {v1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v3}, Lcom/lge/ims/service/cs/IImageSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IImageSession;
    :try_end_1f
    .catchall {:try_start_8 .. :try_end_1f} :catchall_27

    #@1f
    move-result-object v2

    #@20
    .line 278
    .local v2, _result:Lcom/lge/ims/service/cs/IImageSession;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 279
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 281
    return-object v2

    #@27
    .line 278
    .end local v2           #_result:Lcom/lge/ims/service/cs/IImageSession;
    :catchall_27
    move-exception v3

    #@28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 279
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    throw v3
.end method

.method public openVideoSession()Lcom/lge/ims/service/cs/IVideoSession;
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 236
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 237
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 240
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.cs.IContentShare"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 241
    iget-object v3, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x6

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 242
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 243
    invoke-virtual {v1}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v3}, Lcom/lge/ims/service/cs/IVideoSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IVideoSession;
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_26

    #@1e
    move-result-object v2

    #@1f
    .line 246
    .local v2, _result:Lcom/lge/ims/service/cs/IVideoSession;
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 247
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 249
    return-object v2

    #@26
    .line 246
    .end local v2           #_result:Lcom/lge/ims/service/cs/IVideoSession;
    :catchall_26
    move-exception v3

    #@27
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 247
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    throw v3
.end method

.method public removeCall(Ljava/lang/String;)V
    .registers 7
    .parameter "number"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 191
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 192
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 194
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cs.IContentShare"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 195
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 196
    iget-object v2, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x3

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 197
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 200
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 201
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 203
    return-void

    #@21
    .line 200
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 201
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public setListener(Lcom/lge/ims/service/cs/IContentShareListener;)Z
    .registers 9
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 155
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 156
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 159
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.lge.ims.service.cs.IContentShare"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 160
    if-eqz p1, :cond_2f

    #@11
    invoke-interface {p1}, Lcom/lge/ims/service/cs/IContentShareListener;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 161
    iget-object v4, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1a
    const/4 v5, 0x1

    #@1b
    const/4 v6, 0x0

    #@1c
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 162
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 163
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_25
    .catchall {:try_start_a .. :try_end_25} :catchall_33

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_31

    #@28
    .line 166
    .local v2, _result:Z
    :goto_28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 167
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 169
    return v2

    #@2f
    .line 160
    .end local v2           #_result:Z
    :cond_2f
    const/4 v4, 0x0

    #@30
    goto :goto_15

    #@31
    :cond_31
    move v2, v3

    #@32
    .line 163
    goto :goto_28

    #@33
    .line 166
    :catchall_33
    move-exception v3

    #@34
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 167
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3a
    throw v3
.end method

.method public unholdCall(Ljava/lang/String;)V
    .registers 7
    .parameter "number"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 221
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 222
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 224
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cs.IContentShare"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 225
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 226
    iget-object v2, p0, Lcom/lge/ims/service/cs/IContentShare$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x5

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 227
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 230
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 231
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 233
    return-void

    #@21
    .line 230
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 231
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method
