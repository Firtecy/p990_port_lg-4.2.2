.class public Lcom/lge/ims/service/im/FileTransferImplFacade;
.super Lcom/lge/ims/service/im/IFileTransfer$Stub;
.source "FileTransferImplFacade.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final KEY_FILE_MEDIA_PROTOCOL:Ljava/lang/String; = "ft_media_protocol"

.field private static final RCS_IP_CAPA_URI:Ljava/lang/String; = "content://com.lge.ims.provider.ip/ip_capa"

.field private static final VCARD:Ljava/lang/String; = "text/x-vcard"


# instance fields
.field context:Landroid/content/Context;

.field fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

.field listener:Lcom/lge/ims/service/im/IFileTransferListener;

.field protected nativeObj:I

.field remoteUsers:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 38
    invoke-direct {p0}, Lcom/lge/ims/service/im/IFileTransfer$Stub;-><init>()V

    #@3
    .line 39
    iput-object p1, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->context:Landroid/content/Context;

    #@5
    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 3
    .parameter "context"
    .parameter "nativeObj"

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Lcom/lge/ims/service/im/IFileTransfer$Stub;-><init>()V

    #@3
    .line 43
    iput-object p1, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->context:Landroid/content/Context;

    #@5
    .line 44
    iput p2, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->nativeObj:I

    #@7
    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .registers 3
    .parameter "context"
    .parameter "remoteUsers"

    #@0
    .prologue
    .line 47
    invoke-direct {p0}, Lcom/lge/ims/service/im/IFileTransfer$Stub;-><init>()V

    #@3
    .line 48
    iput-object p1, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->context:Landroid/content/Context;

    #@5
    .line 49
    iput-object p2, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->remoteUsers:[Ljava/lang/String;

    #@7
    .line 50
    return-void
.end method

.method private getFTProtocolType(Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "remoteUserUri"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    .line 170
    const-string v8, "HTTP"

    #@4
    .line 172
    .local v8, ftProtocol:Ljava/lang/String;
    iget-object v1, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->context:Landroid/content/Context;

    #@6
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    .line 174
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v1, 0x2

    #@b
    new-array v2, v1, [Ljava/lang/String;

    #@d
    const-string v1, "nor_msisdn"

    #@f
    aput-object v1, v2, v5

    #@11
    const-string v1, "http_status"

    #@13
    aput-object v1, v2, v10

    #@15
    .line 175
    .local v2, projection:[Ljava/lang/String;
    const-string v3, "nor_msisdn= ?"

    #@17
    .line 176
    .local v3, selection:Ljava/lang/String;
    new-array v4, v10, [Ljava/lang/String;

    #@19
    aput-object p1, v4, v5

    #@1b
    .line 177
    .local v4, selectionArgs:[Ljava/lang/String;
    const-string v1, "content://com.lge.ims.provider.ip/ip_capa"

    #@1d
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@20
    move-result-object v1

    #@21
    const/4 v5, 0x0

    #@22
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@25
    move-result-object v6

    #@26
    .line 179
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_75

    #@28
    .line 181
    :try_start_28
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@2b
    move-result v1

    #@2c
    if-eqz v1, :cond_4d

    #@2e
    .line 182
    const/4 v1, 0x1

    #@2f
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    #@32
    move-result v9

    #@33
    .line 183
    .local v9, isHttpAvailable:I
    new-instance v1, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v5, "isHttpAvailable = "

    #@3a
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@49
    .line 184
    if-eq v9, v10, :cond_4d

    #@4b
    .line 185
    const-string v8, "MSRP"
    :try_end_4d
    .catchall {:try_start_28 .. :try_end_4d} :catchall_70
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_4d} :catch_51

    #@4d
    .line 191
    .end local v9           #isHttpAvailable:I
    :cond_4d
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@50
    .line 197
    :goto_50
    return-object v8

    #@51
    .line 188
    :catch_51
    move-exception v7

    #@52
    .line 189
    .local v7, e:Ljava/lang/Exception;
    :try_start_52
    new-instance v1, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v5, "exception error = "

    #@59
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    invoke-virtual {v7}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V
    :try_end_6c
    .catchall {:try_start_52 .. :try_end_6c} :catchall_70

    #@6c
    .line 191
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@6f
    goto :goto_50

    #@70
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_70
    move-exception v1

    #@71
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@74
    throw v1

    #@75
    .line 194
    :cond_75
    const-string v1, "cursor is null"

    #@77
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@7a
    goto :goto_50
.end method


# virtual methods
.method public accept(Ljava/lang/String;)V
    .registers 3
    .parameter "folderPath"

    #@0
    .prologue
    .line 87
    if-nez p1, :cond_9

    #@2
    .line 88
    const-string v0, "folder path is null"

    #@4
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7
    .line 89
    const-string p1, ""

    #@9
    .line 92
    :cond_9
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@b
    if-nez v0, :cond_13

    #@d
    .line 93
    const-string v0, "fileTransfer is null"

    #@f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@12
    .line 97
    :goto_12
    return-void

    #@13
    .line 96
    :cond_13
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@15
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/im/FileTransferImpl;->accept(Ljava/lang/String;)V

    #@18
    goto :goto_12
.end method

.method public cancel()V
    .registers 2

    #@0
    .prologue
    .line 108
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 109
    const-string v0, "fileTransfer is null"

    #@6
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@9
    .line 113
    :goto_9
    return-void

    #@a
    .line 112
    :cond_a
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/im/FileTransferImpl;->cancel()V

    #@f
    goto :goto_9
.end method

.method public destroy()V
    .registers 2

    #@0
    .prologue
    .line 132
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 133
    const-string v0, "fileTransfer is null"

    #@6
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@9
    .line 138
    :goto_9
    return-void

    #@a
    .line 136
    :cond_a
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/im/FileTransferImpl;->destroy()V

    #@f
    .line 137
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@12
    goto :goto_9
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 12
    .parameter "fileName"
    .parameter "url"
    .parameter "contentType"
    .parameter "fileSize"
    .parameter "folderPath"

    #@0
    .prologue
    .line 117
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@2
    if-nez v0, :cond_13

    #@4
    .line 118
    const-string v0, "HTTP"

    #@6
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/FileTransferImplFacade;->makeFileTransfer(Ljava/lang/String;)Lcom/lge/ims/service/im/FileTransferImpl;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@c
    .line 119
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@e
    iget-object v1, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@10
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/FileTransferImpl;->setListener(Lcom/lge/ims/service/im/IFileTransferListener;)V

    #@13
    .line 121
    :cond_13
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@15
    move-object v1, p1

    #@16
    move-object v2, p2

    #@17
    move-object v3, p3

    #@18
    move v4, p4

    #@19
    move-object v5, p5

    #@1a
    invoke-virtual/range {v0 .. v5}, Lcom/lge/ims/service/im/FileTransferImpl;->download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    #@1d
    .line 122
    return-void
.end method

.method public makeFileTransfer(Ljava/lang/String;)Lcom/lge/ims/service/im/FileTransferImpl;
    .registers 5
    .parameter "fileTransferType"

    #@0
    .prologue
    .line 145
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "FT Protocol = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 147
    const-string v0, "HTTP"

    #@18
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_3f

    #@1e
    .line 149
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->remoteUsers:[Ljava/lang/String;

    #@20
    if-eqz v0, :cond_35

    #@22
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->remoteUsers:[Ljava/lang/String;

    #@24
    array-length v0, v0

    #@25
    if-lez v0, :cond_35

    #@27
    .line 150
    new-instance v0, Lcom/lge/ims/service/im/HttpFileTransferImpl;

    #@29
    iget-object v1, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->context:Landroid/content/Context;

    #@2b
    iget-object v2, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->remoteUsers:[Ljava/lang/String;

    #@2d
    invoke-direct {v0, p0, v1, v2}, Lcom/lge/ims/service/im/HttpFileTransferImpl;-><init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;[Ljava/lang/String;)V

    #@30
    iput-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@32
    .line 166
    :cond_32
    :goto_32
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@34
    return-object v0

    #@35
    .line 153
    :cond_35
    new-instance v0, Lcom/lge/ims/service/im/HttpFileTransferImpl;

    #@37
    iget-object v1, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->context:Landroid/content/Context;

    #@39
    invoke-direct {v0, p0, v1}, Lcom/lge/ims/service/im/HttpFileTransferImpl;-><init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;)V

    #@3c
    iput-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@3e
    goto :goto_32

    #@3f
    .line 155
    :cond_3f
    const-string v0, "MSRP"

    #@41
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v0

    #@45
    if-eqz v0, :cond_32

    #@47
    .line 157
    iget v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->nativeObj:I

    #@49
    if-eqz v0, :cond_57

    #@4b
    .line 158
    new-instance v0, Lcom/lge/ims/service/im/MSRPFileTransferImpl;

    #@4d
    iget-object v1, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->context:Landroid/content/Context;

    #@4f
    iget v2, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->nativeObj:I

    #@51
    invoke-direct {v0, p0, v1, v2}, Lcom/lge/ims/service/im/MSRPFileTransferImpl;-><init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;I)V

    #@54
    iput-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@56
    goto :goto_32

    #@57
    .line 159
    :cond_57
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->remoteUsers:[Ljava/lang/String;

    #@59
    if-eqz v0, :cond_6c

    #@5b
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->remoteUsers:[Ljava/lang/String;

    #@5d
    array-length v0, v0

    #@5e
    if-lez v0, :cond_6c

    #@60
    .line 161
    new-instance v0, Lcom/lge/ims/service/im/MSRPFileTransferImpl;

    #@62
    iget-object v1, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->context:Landroid/content/Context;

    #@64
    iget-object v2, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->remoteUsers:[Ljava/lang/String;

    #@66
    invoke-direct {v0, p0, v1, v2}, Lcom/lge/ims/service/im/MSRPFileTransferImpl;-><init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;[Ljava/lang/String;)V

    #@69
    iput-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@6b
    goto :goto_32

    #@6c
    .line 163
    :cond_6c
    const-string v0, "wrong requirement!!"

    #@6e
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@71
    goto :goto_32
.end method

.method public reject()V
    .registers 2

    #@0
    .prologue
    .line 100
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@2
    if-nez v0, :cond_a

    #@4
    .line 101
    const-string v0, "fileTransfer is null"

    #@6
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@9
    .line 105
    :goto_9
    return-void

    #@a
    .line 104
    :cond_a
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/im/FileTransferImpl;->reject()V

    #@f
    goto :goto_9
.end method

.method public sendFile(Lcom/lge/ims/service/im/IMFileInfo;Ljava/lang/String;)V
    .registers 8
    .parameter "imFileInfo"
    .parameter "contributionId"

    #@0
    .prologue
    .line 53
    iget-object v3, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->remoteUsers:[Ljava/lang/String;

    #@2
    if-eqz v3, :cond_a

    #@4
    iget-object v3, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->remoteUsers:[Ljava/lang/String;

    #@6
    array-length v3, v3

    #@7
    const/4 v4, 0x1

    #@8
    if-ge v3, v4, :cond_10

    #@a
    .line 54
    :cond_a
    const-string v3, "remoteUsers is abnormal !!!"

    #@c
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@f
    .line 84
    :goto_f
    return-void

    #@10
    .line 60
    :cond_10
    const-string v0, "HTTP"

    #@12
    .line 72
    .local v0, ftProtocol:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->context:Landroid/content/Context;

    #@14
    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    #@17
    move-result-object v2

    #@18
    .line 73
    .local v2, sharedPreferences:Landroid/content/SharedPreferences;
    const-string v3, "ft_media_protocol"

    #@1a
    const-string v4, "NONE"

    #@1c
    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    .line 75
    .local v1, hiddenMenuftProtocol:Ljava/lang/String;
    const-string v3, "NONE"

    #@22
    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@25
    move-result v3

    #@26
    if-nez v3, :cond_3f

    #@28
    .line 76
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v4, "the ft protocol of hidden menu = "

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3e
    .line 77
    move-object v0, v1

    #@3f
    .line 81
    :cond_3f
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/FileTransferImplFacade;->makeFileTransfer(Ljava/lang/String;)Lcom/lge/ims/service/im/FileTransferImpl;

    #@42
    move-result-object v3

    #@43
    iput-object v3, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@45
    .line 82
    iget-object v3, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@47
    iget-object v4, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@49
    invoke-virtual {v3, v4}, Lcom/lge/ims/service/im/FileTransferImpl;->setListener(Lcom/lge/ims/service/im/IFileTransferListener;)V

    #@4c
    .line 83
    iget-object v3, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@4e
    invoke-virtual {v3, p1, p2}, Lcom/lge/ims/service/im/FileTransferImpl;->sendFile(Lcom/lge/ims/service/im/IMFileInfo;Ljava/lang/String;)V

    #@51
    goto :goto_f
.end method

.method public setListener(Lcom/lge/ims/service/im/IFileTransferListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 125
    iput-object p1, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@2
    .line 126
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 127
    iget-object v0, p0, Lcom/lge/ims/service/im/FileTransferImplFacade;->fileTransfer:Lcom/lge/ims/service/im/FileTransferImpl;

    #@8
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/im/FileTransferImpl;->setListener(Lcom/lge/ims/service/im/IFileTransferListener;)V

    #@b
    .line 129
    :cond_b
    return-void
.end method
