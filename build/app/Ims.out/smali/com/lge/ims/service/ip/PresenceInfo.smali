.class public Lcom/lge/ims/service/ip/PresenceInfo;
.super Ljava/lang/Object;
.source "PresenceInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/ip/PresenceInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private nStatus:I

.field private strBirthDay:Ljava/lang/String;

.field private strCyworldAccount:Ljava/lang/String;

.field private strDisplayName:Ljava/lang/String;

.field private strEMailAddress:Ljava/lang/String;

.field private strFaceBookAccount:Ljava/lang/String;

.field private strFreeText:Ljava/lang/String;

.field private strHomePage:Ljava/lang/String;

.field private strMSISDN:Ljava/lang/String;

.field private strStatusIcon:Ljava/lang/String;

.field private strStatusIconLink:Ljava/lang/String;

.field private strStatusIconThumb:Ljava/lang/String;

.field private strStatusIconThumbEtag:Ljava/lang/String;

.field private strStatusIconThumbLink:Ljava/lang/String;

.field private strTwitterAccount:Ljava/lang/String;

.field private strWaggleAccount:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 37
    new-instance v0, Lcom/lge/ims/service/ip/PresenceInfo$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/ip/PresenceInfo$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/ip/PresenceInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 53
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strMSISDN:Ljava/lang/String;

    #@6
    .line 54
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strHomePage:Ljava/lang/String;

    #@8
    .line 55
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFreeText:Ljava/lang/String;

    #@a
    .line 56
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strEMailAddress:Ljava/lang/String;

    #@c
    .line 57
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strBirthDay:Ljava/lang/String;

    #@e
    .line 58
    const/4 v0, 0x0

    #@f
    iput v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->nStatus:I

    #@11
    .line 59
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strTwitterAccount:Ljava/lang/String;

    #@13
    .line 60
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFaceBookAccount:Ljava/lang/String;

    #@15
    .line 61
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strCyworldAccount:Ljava/lang/String;

    #@17
    .line 62
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strWaggleAccount:Ljava/lang/String;

    #@19
    .line 63
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIcon:Ljava/lang/String;

    #@1b
    .line 64
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconLink:Ljava/lang/String;

    #@1d
    .line 65
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumb:Ljava/lang/String;

    #@1f
    .line 66
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumbLink:Ljava/lang/String;

    #@21
    .line 67
    iput-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumbEtag:Ljava/lang/String;

    #@23
    .line 68
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 76
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/ip/PresenceInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@6
    .line 78
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/lge/ims/service/ip/PresenceInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/PresenceInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public GetBirthDay()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 184
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strBirthDay:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetCyworldAccount()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 208
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strCyworldAccount:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetDisplayName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 169
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strDisplayName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetEMailAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 176
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strEMailAddress:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetFaceBookAccount()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 200
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFaceBookAccount:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetFreeText()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 161
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFreeText:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetHomePage()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 153
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strHomePage:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetMSISDN()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strMSISDN:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetStatus()I
    .registers 2

    #@0
    .prologue
    .line 255
    iget v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->nStatus:I

    #@2
    return v0
.end method

.method public GetStatusIcon()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 224
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIcon:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetStatusIconLink()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 231
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconLink:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetStatusIconThumb()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumb:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetStatusIconThumbEtag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 249
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumbEtag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetStatusIconThumbLink()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 243
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumbLink:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetTwitterAccount()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 192
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strTwitterAccount:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetWaggleAccount()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 216
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strWaggleAccount:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public IsInvalid()Z
    .registers 2

    #@0
    .prologue
    .line 149
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strMSISDN:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strMSISDN:Ljava/lang/String;

    #@6
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public SetBirthDay(Ljava/lang/String;)V
    .registers 2
    .parameter "_strBirthDay"

    #@0
    .prologue
    .line 188
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strBirthDay:Ljava/lang/String;

    #@2
    .line 189
    return-void
.end method

.method public SetCyworldAccount(Ljava/lang/String;)V
    .registers 2
    .parameter "_strCyworldAccount"

    #@0
    .prologue
    .line 212
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strCyworldAccount:Ljava/lang/String;

    #@2
    .line 213
    return-void
.end method

.method public SetDisplayName(Ljava/lang/String;)V
    .registers 2
    .parameter "_strDisplayName"

    #@0
    .prologue
    .line 173
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strDisplayName:Ljava/lang/String;

    #@2
    .line 174
    return-void
.end method

.method public SetEMailAddress(Ljava/lang/String;)V
    .registers 2
    .parameter "_strEMailAddress"

    #@0
    .prologue
    .line 180
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strEMailAddress:Ljava/lang/String;

    #@2
    .line 181
    return-void
.end method

.method public SetFaceBookAccount(Ljava/lang/String;)V
    .registers 2
    .parameter "_strFaceBookAccount"

    #@0
    .prologue
    .line 204
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFaceBookAccount:Ljava/lang/String;

    #@2
    .line 205
    return-void
.end method

.method public SetFreeText(Ljava/lang/String;)V
    .registers 2
    .parameter "_strFreeText"

    #@0
    .prologue
    .line 165
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFreeText:Ljava/lang/String;

    #@2
    .line 166
    return-void
.end method

.method public SetHomePage(Ljava/lang/String;)V
    .registers 2
    .parameter "_strHomePage"

    #@0
    .prologue
    .line 157
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strHomePage:Ljava/lang/String;

    #@2
    .line 158
    return-void
.end method

.method public SetMSISDN(Ljava/lang/String;)V
    .registers 2
    .parameter "_strMSISDN"

    #@0
    .prologue
    .line 140
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strMSISDN:Ljava/lang/String;

    #@2
    .line 141
    return-void
.end method

.method public SetStatus(I)V
    .registers 2
    .parameter "_nStatus"

    #@0
    .prologue
    .line 259
    iput p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->nStatus:I

    #@2
    .line 260
    return-void
.end method

.method public SetStatusIcon(Ljava/lang/String;)V
    .registers 2
    .parameter "_strStatusIcon"

    #@0
    .prologue
    .line 227
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIcon:Ljava/lang/String;

    #@2
    .line 228
    return-void
.end method

.method public SetStatusIconLink(Ljava/lang/String;)V
    .registers 2
    .parameter "_strStatusIconLink"

    #@0
    .prologue
    .line 234
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconLink:Ljava/lang/String;

    #@2
    .line 235
    return-void
.end method

.method public SetStatusIconThumb(Ljava/lang/String;)V
    .registers 2
    .parameter "_strStatusIconThumb"

    #@0
    .prologue
    .line 240
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumb:Ljava/lang/String;

    #@2
    .line 241
    return-void
.end method

.method public SetStatusIconThumbEtag(Ljava/lang/String;)V
    .registers 2
    .parameter "_strStatusIconThumbEtag"

    #@0
    .prologue
    .line 252
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumbEtag:Ljava/lang/String;

    #@2
    .line 253
    return-void
.end method

.method public SetStatusIconThumbLink(Ljava/lang/String;)V
    .registers 2
    .parameter "_strStatusIconThumbLink"

    #@0
    .prologue
    .line 246
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumbLink:Ljava/lang/String;

    #@2
    .line 247
    return-void
.end method

.method public SetTwitterAccount(Ljava/lang/String;)V
    .registers 2
    .parameter "_strTwitterAccount"

    #@0
    .prologue
    .line 196
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strTwitterAccount:Ljava/lang/String;

    #@2
    .line 197
    return-void
.end method

.method public SetWaggleAccount(Ljava/lang/String;)V
    .registers 2
    .parameter "_strWaggleAccount"

    #@0
    .prologue
    .line 220
    iput-object p1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strWaggleAccount:Ljava/lang/String;

    #@2
    .line 221
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 86
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strMSISDN:Ljava/lang/String;

    #@6
    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strHomePage:Ljava/lang/String;

    #@c
    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFreeText:Ljava/lang/String;

    #@12
    .line 121
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strEMailAddress:Ljava/lang/String;

    #@18
    .line 122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strBirthDay:Ljava/lang/String;

    #@1e
    .line 123
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v0

    #@22
    iput v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->nStatus:I

    #@24
    .line 124
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strTwitterAccount:Ljava/lang/String;

    #@2a
    .line 125
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFaceBookAccount:Ljava/lang/String;

    #@30
    .line 126
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strCyworldAccount:Ljava/lang/String;

    #@36
    .line 127
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@39
    move-result-object v0

    #@3a
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strWaggleAccount:Ljava/lang/String;

    #@3c
    .line 128
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIcon:Ljava/lang/String;

    #@42
    .line 129
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconLink:Ljava/lang/String;

    #@48
    .line 130
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4b
    move-result-object v0

    #@4c
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumb:Ljava/lang/String;

    #@4e
    .line 131
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@51
    move-result-object v0

    #@52
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumbLink:Ljava/lang/String;

    #@54
    .line 132
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@57
    move-result-object v0

    #@58
    iput-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumbEtag:Ljava/lang/String;

    #@5a
    .line 133
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 263
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "PresenceInfo : MSISDN["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strMSISDN:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "], StatusIconFileName["

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIcon:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, "], HomePage["

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strHomePage:Ljava/lang/String;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, "], FreeText["

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFreeText:Ljava/lang/String;

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, "]"

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    const-string v1, ", E-Mail["

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strEMailAddress:Ljava/lang/String;

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    const-string v1, "], BirthDay["

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strBirthDay:Ljava/lang/String;

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    const-string v1, "], Status["

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    iget v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->nStatus:I

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    const-string v1, "], TwitterAccount["

    #@61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v0

    #@65
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strTwitterAccount:Ljava/lang/String;

    #@67
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v0

    #@6b
    const-string v1, "], FaceBookAccount["

    #@6d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v0

    #@71
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFaceBookAccount:Ljava/lang/String;

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v0

    #@77
    const-string v1, "]"

    #@79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v0

    #@7d
    const-string v1, ", CyworldAccount["

    #@7f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v0

    #@83
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strCyworldAccount:Ljava/lang/String;

    #@85
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v0

    #@89
    const-string v1, "], WaggleAccount["

    #@8b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v0

    #@8f
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strWaggleAccount:Ljava/lang/String;

    #@91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v0

    #@95
    const-string v1, "], StatusIconThumb["

    #@97
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v0

    #@9b
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumb:Ljava/lang/String;

    #@9d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v0

    #@a1
    const-string v1, "], strStatusIconLink["

    #@a3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v0

    #@a7
    iget-object v1, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconLink:Ljava/lang/String;

    #@a9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v0

    #@ad
    const-string v1, "]"

    #@af
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v0

    #@b3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v0

    #@b7
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strMSISDN:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 96
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strHomePage:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 97
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFreeText:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 98
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strEMailAddress:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 99
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strBirthDay:Ljava/lang/String;

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@19
    .line 100
    iget v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->nStatus:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 101
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strTwitterAccount:Ljava/lang/String;

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@23
    .line 102
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strFaceBookAccount:Ljava/lang/String;

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@28
    .line 103
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strCyworldAccount:Ljava/lang/String;

    #@2a
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2d
    .line 104
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strWaggleAccount:Ljava/lang/String;

    #@2f
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@32
    .line 105
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIcon:Ljava/lang/String;

    #@34
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@37
    .line 106
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconLink:Ljava/lang/String;

    #@39
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@3c
    .line 107
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumb:Ljava/lang/String;

    #@3e
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@41
    .line 108
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumbLink:Ljava/lang/String;

    #@43
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@46
    .line 109
    iget-object v0, p0, Lcom/lge/ims/service/ip/PresenceInfo;->strStatusIconThumbEtag:Ljava/lang/String;

    #@48
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@4b
    .line 110
    return-void
.end method
