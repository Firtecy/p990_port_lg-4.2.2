.class public Lcom/lge/ims/service/im/IMServiceImpl;
.super Lcom/lge/ims/service/im/IIMService$Stub;
.source "IMServiceImpl.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;
.implements Lcom/lge/ims/service/cd/CapabilitiesListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/lge/ims/service/im/IIMServiceListener;

.field private mNativeObj:I

.field private mNetworkType:I

.field private mRefCount:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 36
    invoke-direct {p0}, Lcom/lge/ims/service/im/IIMService$Stub;-><init>()V

    #@4
    .line 31
    iput v2, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@6
    .line 32
    iput v2, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNetworkType:I

    #@8
    .line 33
    const/4 v1, 0x0

    #@9
    iput-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@b
    .line 34
    iput v2, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@d
    .line 37
    const-string v1, "IMServiceImpl()"

    #@f
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@12
    .line 39
    iput-object p1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@14
    .line 41
    const/16 v1, 0x15

    #@16
    invoke-static {v1}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@19
    move-result v1

    #@1a
    iput v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@1c
    .line 42
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@1e
    if-nez v1, :cond_26

    #@20
    .line 43
    const-string v1, "mNativeObj == null"

    #@22
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@25
    .line 53
    :goto_25
    return-void

    #@26
    .line 47
    :cond_26
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@28
    invoke-static {v1, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@2b
    .line 50
    new-instance v0, Landroid/content/Intent;

    #@2d
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@30
    .line 51
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.lge.ims.rcsim.IMSERVICE_STARTED"

    #@32
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@35
    .line 52
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@38
    goto :goto_25
.end method

.method private openChat(ILjava/lang/String;)Lcom/lge/ims/service/im/ChatImpl;
    .registers 5
    .parameter "nativeObj"
    .parameter "contributionId"

    #@0
    .prologue
    .line 90
    const-string v0, "openChat()"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 92
    iget v0, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@7
    add-int/lit8 v0, v0, 0x1

    #@9
    iput v0, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@b
    .line 94
    new-instance v0, Lcom/lge/ims/service/im/ChatImpl;

    #@d
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@f
    invoke-direct {v0, v1, p1, p2}, Lcom/lge/ims/service/im/ChatImpl;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    #@12
    return-object v0
.end method

.method private openFileTransferToDownload(I)Lcom/lge/ims/service/im/FileTransferImplFacade;
    .registers 4
    .parameter "nativeObj"

    #@0
    .prologue
    .line 146
    const-string v1, "openFileTransfer()"

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 148
    new-instance v0, Lcom/lge/ims/service/im/FileTransferImplFacade;

    #@7
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@9
    invoke-direct {v0, v1, p1}, Lcom/lge/ims/service/im/FileTransferImplFacade;-><init>(Landroid/content/Context;I)V

    #@c
    .line 149
    .local v0, iFileTransfer:Lcom/lge/ims/service/im/FileTransferImplFacade;
    const-string v1, "MSRP"

    #@e
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/FileTransferImplFacade;->makeFileTransfer(Ljava/lang/String;)Lcom/lge/ims/service/im/FileTransferImpl;

    #@11
    .line 150
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@13
    add-int/lit8 v1, v1, 0x1

    #@15
    iput v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@17
    .line 152
    return-object v0
.end method

.method private sendMessageToJNI(Landroid/os/Parcel;)V
    .registers 4
    .parameter "parcel"

    #@0
    .prologue
    .line 321
    if-nez p1, :cond_3

    #@2
    .line 332
    :cond_2
    :goto_2
    return-void

    #@3
    .line 325
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->marshall()[B

    #@6
    move-result-object v0

    #@7
    .line 326
    .local v0, baData:[B
    invoke-virtual {p1}, Landroid/os/Parcel;->recycle()V

    #@a
    .line 327
    const/4 p1, 0x0

    #@b
    .line 329
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@d
    if-eqz v1, :cond_2

    #@f
    .line 330
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@11
    invoke-static {v1, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@14
    goto :goto_2
.end method


# virtual methods
.method public addCapabilityObserver(Ljava/lang/String;)Z
    .registers 5
    .parameter "remoteUser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 270
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "addCapabilityObserver() : remoteUser = "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 272
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityService;->getInstance()Lcom/lge/ims/service/cd/CapabilityService;

    #@19
    move-result-object v0

    #@1a
    .line 274
    .local v0, cs:Lcom/lge/ims/service/cd/CapabilityService;
    if-eqz v0, :cond_21

    #@1c
    .line 275
    invoke-virtual {v0, p1, p0}, Lcom/lge/ims/service/cd/CapabilityService;->addDedicatedListener(Ljava/lang/String;Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@1f
    .line 276
    const/4 v1, 0x1

    #@20
    .line 278
    :goto_20
    return v1

    #@21
    :cond_21
    const/4 v1, 0x0

    #@22
    goto :goto_20
.end method

.method public capabilitiesUpdated(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V
    .registers 6
    .parameter "sc"
    .parameter "resultCode"

    #@0
    .prologue
    .line 765
    const-string v1, "capabilitiesUpdated()"

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 767
    if-nez p1, :cond_8

    #@7
    .line 778
    :cond_7
    :goto_7
    return-void

    #@8
    .line 772
    :cond_8
    :try_start_8
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@a
    if-eqz v1, :cond_7

    #@c
    .line 773
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@e
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/ServiceCapabilities;->toCapabilities()Lcom/lge/ims/service/cd/Capabilities;

    #@11
    move-result-object v2

    #@12
    invoke-interface {v1, v2}, Lcom/lge/ims/service/im/IIMServiceListener;->capabilityNotify(Lcom/lge/ims/service/cd/Capabilities;)V
    :try_end_15
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_15} :catch_16

    #@15
    goto :goto_7

    #@16
    .line 775
    :catch_16
    move-exception v0

    #@17
    .line 776
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@22
    goto :goto_7
.end method

.method public closeChat(Lcom/lge/ims/service/im/IChat;)Z
    .registers 6
    .parameter "chat"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 98
    const-string v3, "closeChat()"

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@6
    .line 100
    if-nez p1, :cond_f

    #@8
    .line 101
    const-string v2, "chat is null"

    #@a
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@d
    .line 102
    const/4 v2, 0x0

    #@e
    .line 118
    :cond_e
    :goto_e
    return v2

    #@f
    :cond_f
    move-object v0, p1

    #@10
    .line 105
    check-cast v0, Lcom/lge/ims/service/im/ChatImpl;

    #@12
    .line 106
    .local v0, chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    invoke-virtual {v0}, Lcom/lge/ims/service/im/ChatImpl;->destroy()V

    #@15
    .line 107
    const/4 v0, 0x0

    #@16
    .line 108
    const/4 p1, 0x0

    #@17
    .line 110
    iget v3, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@19
    add-int/lit8 v3, v3, -0x1

    #@1b
    iput v3, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@1d
    .line 112
    iget v3, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@1f
    if-ge v3, v2, :cond_e

    #@21
    iget v3, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@23
    if-nez v3, :cond_e

    #@25
    .line 113
    new-instance v1, Landroid/content/Intent;

    #@27
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@2a
    .line 114
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "com.lge.ims.rcsim.IMSERVICE_STOPED"

    #@2c
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@2f
    .line 115
    iget-object v3, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@31
    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@34
    goto :goto_e
.end method

.method public closeFileTransfer(Lcom/lge/ims/service/im/IFileTransfer;)Z
    .registers 6
    .parameter "ft"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 156
    const-string v2, "closeFileTransfer()"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@6
    move-object v0, p1

    #@7
    .line 158
    check-cast v0, Lcom/lge/ims/service/im/FileTransferImplFacade;

    #@9
    .line 160
    .local v0, fileTransferImplFacade:Lcom/lge/ims/service/im/FileTransferImplFacade;
    invoke-virtual {v0}, Lcom/lge/ims/service/im/FileTransferImplFacade;->destroy()V

    #@c
    .line 161
    const/4 v0, 0x0

    #@d
    .line 162
    const/4 p1, 0x0

    #@e
    .line 164
    iget v2, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@10
    add-int/lit8 v2, v2, -0x1

    #@12
    iput v2, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@14
    .line 166
    iget v2, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@16
    if-ge v2, v3, :cond_2b

    #@18
    iget v2, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@1a
    if-nez v2, :cond_2b

    #@1c
    .line 167
    new-instance v1, Landroid/content/Intent;

    #@1e
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@21
    .line 168
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "com.lge.ims.rcsim.IMSERVICE_STOPED"

    #@23
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@26
    .line 169
    iget-object v2, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@28
    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@2b
    .line 172
    .end local v1           #intent:Landroid/content/Intent;
    :cond_2b
    return v3
.end method

.method public createCapabilityQuery()Lcom/lge/ims/service/cd/ICapabilityQuery;
    .registers 3

    #@0
    .prologue
    .line 308
    const-string v1, "createCapabilityQuery()"

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 310
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityService;->getInstance()Lcom/lge/ims/service/cd/CapabilityService;

    #@8
    move-result-object v0

    #@9
    .line 312
    .local v0, cs:Lcom/lge/ims/service/cd/CapabilityService;
    if-nez v0, :cond_12

    #@b
    .line 313
    const-string v1, "Creating a CapabilityQuery failed"

    #@d
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@10
    .line 314
    const/4 v1, 0x0

    #@11
    .line 317
    :goto_11
    return-object v1

    #@12
    :cond_12
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityService;->createCapabilityQueryImpl()Lcom/lge/ims/service/cd/CapabilityQueryImpl;

    #@15
    move-result-object v1

    #@16
    goto :goto_11
.end method

.method public destroy()V
    .registers 4

    #@0
    .prologue
    .line 56
    const-string v1, "destroy()"

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 58
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@7
    if-eqz v1, :cond_c

    #@9
    .line 59
    const/4 v1, 0x0

    #@a
    iput-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@c
    .line 62
    :cond_c
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@e
    if-eqz v1, :cond_1d

    #@10
    .line 63
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@12
    invoke-static {v1, p0}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@15
    .line 64
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@17
    invoke-static {v1}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@1a
    .line 65
    const/4 v1, 0x0

    #@1b
    iput v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mNativeObj:I

    #@1d
    .line 68
    :cond_1d
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@1f
    const/4 v2, 0x1

    #@20
    if-ge v1, v2, :cond_31

    #@22
    .line 69
    new-instance v0, Landroid/content/Intent;

    #@24
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@27
    .line 70
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.lge.ims.rcsim.IMSERVICE_STOPED"

    #@29
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@2c
    .line 71
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@2e
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@31
    .line 73
    .end local v0           #intent:Landroid/content/Intent;
    :cond_31
    return-void
.end method

.method public getCallState(Ljava/lang/String;)I
    .registers 3
    .parameter "remoteUri"

    #@0
    .prologue
    .line 781
    invoke-static {p1}, Lcom/lge/ims/service/im/IMFTUtil;->getCallState(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getServiceNetworkType()I
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x3

    #@1
    const/4 v3, 0x1

    #@2
    .line 210
    const-string v2, "getServiceNetworkType()"

    #@4
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@7
    .line 213
    invoke-static {}, Lcom/lge/ims/service/ServiceMngr;->getInstance()Lcom/lge/ims/service/ServiceMngr;

    #@a
    move-result-object v1

    #@b
    .line 214
    .local v1, serviceMngr:Lcom/lge/ims/service/ServiceMngr;
    const/4 v0, 0x0

    #@c
    .line 216
    .local v0, result:I
    if-eqz v1, :cond_15

    #@e
    .line 217
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/ServiceMngr;->isWifiConnected(I)Z

    #@11
    move-result v2

    #@12
    if-ne v2, v3, :cond_2c

    #@14
    .line 218
    const/4 v0, 0x2

    #@15
    .line 224
    :cond_15
    :goto_15
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v3, "network type = "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@2b
    .line 226
    return v0

    #@2c
    .line 219
    :cond_2c
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/ServiceMngr;->isLTEConnected(I)Z

    #@2f
    move-result v2

    #@30
    if-ne v2, v3, :cond_15

    #@32
    .line 220
    const/4 v0, 0x1

    #@33
    goto :goto_15
.end method

.method public onAvailable(I)V
    .registers 5
    .parameter "networkType"

    #@0
    .prologue
    .line 749
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "onAvailable() : networkType = "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 752
    :try_start_16
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@18
    if-eqz v1, :cond_21

    #@1a
    .line 753
    if-lez p1, :cond_22

    #@1c
    .line 754
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@1e
    invoke-interface {v1}, Lcom/lge/ims/service/im/IIMServiceListener;->serviceStarted()V

    #@21
    .line 762
    :cond_21
    :goto_21
    return-void

    #@22
    .line 756
    :cond_22
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@24
    invoke-interface {v1}, Lcom/lge/ims/service/im/IIMServiceListener;->serviceClosed()V
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_27} :catch_28

    #@27
    goto :goto_21

    #@28
    .line 759
    :catch_28
    move-exception v0

    #@29
    .line 760
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@34
    goto :goto_21
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 53
    .parameter "parcel"

    #@0
    .prologue
    .line 335
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v36

    #@4
    .line 337
    .local v36, msg:I
    packed-switch v36, :pswitch_data_b10

    #@7
    .line 740
    :try_start_7
    const-string v47, "NOT HANDLED!!!"

    #@9
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@c
    .line 746
    :cond_c
    :goto_c
    :pswitch_c
    return-void

    #@d
    .line 339
    :pswitch_d
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@10
    move-result v37

    #@11
    .line 340
    .local v37, nativeObj:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14
    move-result-object v42

    #@15
    .line 341
    .local v42, senderURI:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v41

    #@19
    .line 342
    .local v41, senderMSISDN:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1c
    move-result-object v28

    #@1d
    .line 343
    .local v28, displayName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@20
    move-result-object v35

    #@21
    .line 344
    .local v35, mimeType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24
    move-result-object v45

    #@25
    .line 345
    .local v45, subject:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    .line 346
    .local v4, messageId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2c
    move-result-object v26

    #@2d
    .line 347
    .local v26, dateTime:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@30
    move-result-object v21

    #@31
    .line 348
    .local v21, contributionId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v29

    #@35
    .line 349
    .local v29, displayNotification:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@38
    move-result v27

    #@39
    .line 351
    .local v27, deferredMessages:I
    move-object/from16 v0, p0

    #@3b
    move-object/from16 v1, v41

    #@3d
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/IMServiceImpl;->getCallState(Ljava/lang/String;)I

    #@40
    move-result v16

    #@41
    .line 353
    .local v16, callState:I
    new-instance v47, Ljava/lang/StringBuilder;

    #@43
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v48, "RECEIVEDCHAT_IND \n, senderURI = "

    #@48
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v47

    #@4c
    move-object/from16 v0, v47

    #@4e
    move-object/from16 v1, v42

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v47

    #@54
    const-string v48, "\n"

    #@56
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v47

    #@5a
    const-string v48, ", senderMSISDN = "

    #@5c
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v47

    #@60
    move-object/from16 v0, v47

    #@62
    move-object/from16 v1, v41

    #@64
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v47

    #@68
    const-string v48, "\n"

    #@6a
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v47

    #@6e
    const-string v48, ", displayName = "

    #@70
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v47

    #@74
    move-object/from16 v0, v47

    #@76
    move-object/from16 v1, v28

    #@78
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v47

    #@7c
    const-string v48, "\n"

    #@7e
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v47

    #@82
    const-string v48, ", subject = "

    #@84
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v47

    #@88
    move-object/from16 v0, v47

    #@8a
    move-object/from16 v1, v45

    #@8c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v47

    #@90
    const-string v48, "\n"

    #@92
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v47

    #@96
    const-string v48, ", messageId = "

    #@98
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v47

    #@9c
    move-object/from16 v0, v47

    #@9e
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v47

    #@a2
    const-string v48, "\n"

    #@a4
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v47

    #@a8
    const-string v48, ", dateTime = "

    #@aa
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v47

    #@ae
    move-object/from16 v0, v47

    #@b0
    move-object/from16 v1, v26

    #@b2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v47

    #@b6
    const-string v48, "\n"

    #@b8
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v47

    #@bc
    const-string v48, ", contributionId = "

    #@be
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v47

    #@c2
    move-object/from16 v0, v47

    #@c4
    move-object/from16 v1, v21

    #@c6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v47

    #@ca
    const-string v48, "\n"

    #@cc
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v47

    #@d0
    const-string v48, ", displayNotification = "

    #@d2
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v47

    #@d6
    move-object/from16 v0, v47

    #@d8
    move/from16 v1, v29

    #@da
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v47

    #@de
    const-string v48, "\n"

    #@e0
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v47

    #@e4
    const-string v48, ", deferredMessages = "

    #@e6
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v47

    #@ea
    move-object/from16 v0, v47

    #@ec
    move/from16 v1, v27

    #@ee
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v47

    #@f2
    const-string v48, "\n"

    #@f4
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v47

    #@f8
    const-string v48, ", call state = "

    #@fa
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v47

    #@fe
    move-object/from16 v0, v47

    #@100
    move/from16 v1, v16

    #@102
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@105
    move-result-object v47

    #@106
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@109
    move-result-object v47

    #@10a
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@10d
    .line 365
    move-object/from16 v0, p0

    #@10f
    move/from16 v1, v37

    #@111
    move-object/from16 v2, v21

    #@113
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/service/im/IMServiceImpl;->openChat(ILjava/lang/String;)Lcom/lge/ims/service/im/ChatImpl;

    #@116
    move-result-object v17

    #@117
    .line 367
    .local v17, chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    new-instance v34, Landroid/content/Intent;

    #@119
    invoke-direct/range {v34 .. v34}, Landroid/content/Intent;-><init>()V

    #@11c
    .line 368
    .local v34, intent:Landroid/content/Intent;
    const-string v47, "com.lge.ims.rcsim.CHAT_INVITE_RECEIVED"

    #@11e
    move-object/from16 v0, v34

    #@120
    move-object/from16 v1, v47

    #@122
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@125
    .line 369
    new-instance v31, Landroid/os/Bundle;

    #@127
    invoke-direct/range {v31 .. v31}, Landroid/os/Bundle;-><init>()V

    #@12a
    .line 370
    .local v31, extra:Landroid/os/Bundle;
    const-string v47, "ChatImpl"

    #@12c
    move-object/from16 v0, v31

    #@12e
    move-object/from16 v1, v47

    #@130
    move-object/from16 v2, v17

    #@132
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    #@135
    .line 371
    move-object/from16 v0, v34

    #@137
    move-object/from16 v1, v31

    #@139
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@13c
    .line 372
    const-string v47, "remoteUserUri"

    #@13e
    move-object/from16 v0, v34

    #@140
    move-object/from16 v1, v47

    #@142
    move-object/from16 v2, v42

    #@144
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@147
    .line 373
    const-string v47, "remoteUser"

    #@149
    move-object/from16 v0, v34

    #@14b
    move-object/from16 v1, v47

    #@14d
    move-object/from16 v2, v41

    #@14f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@152
    .line 374
    const-string v47, "displayName"

    #@154
    move-object/from16 v0, v34

    #@156
    move-object/from16 v1, v47

    #@158
    move-object/from16 v2, v28

    #@15a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@15d
    .line 375
    const-string v47, "messageId"

    #@15f
    move-object/from16 v0, v34

    #@161
    move-object/from16 v1, v47

    #@163
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@166
    .line 376
    const-string v47, "dateTime"

    #@168
    move-object/from16 v0, v34

    #@16a
    move-object/from16 v1, v47

    #@16c
    move-object/from16 v2, v26

    #@16e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@171
    .line 377
    const-string v47, "subject"

    #@173
    move-object/from16 v0, v34

    #@175
    move-object/from16 v1, v47

    #@177
    move-object/from16 v2, v45

    #@179
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@17c
    .line 378
    const-string v47, "displayNotification"

    #@17e
    move-object/from16 v0, v34

    #@180
    move-object/from16 v1, v47

    #@182
    move/from16 v2, v29

    #@184
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@187
    .line 379
    const-string v47, "deferredMessages"

    #@189
    move-object/from16 v0, v34

    #@18b
    move-object/from16 v1, v47

    #@18d
    move/from16 v2, v27

    #@18f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@192
    .line 380
    const-string v47, "callState"

    #@194
    move-object/from16 v0, v34

    #@196
    move-object/from16 v1, v47

    #@198
    move/from16 v2, v16

    #@19a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@19d
    .line 384
    const-string v47, "mimeType"

    #@19f
    move-object/from16 v0, v34

    #@1a1
    move-object/from16 v1, v47

    #@1a3
    move-object/from16 v2, v35

    #@1a5
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1a8
    .line 385
    const-string v47, "contributionId"

    #@1aa
    move-object/from16 v0, v34

    #@1ac
    move-object/from16 v1, v47

    #@1ae
    move-object/from16 v2, v21

    #@1b0
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1b3
    .line 387
    move-object/from16 v0, p0

    #@1b5
    iget-object v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@1b7
    move-object/from16 v47, v0

    #@1b9
    move-object/from16 v0, v47

    #@1bb
    move-object/from16 v1, v34

    #@1bd
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1c0
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_1c0} :catch_1c2

    #@1c0
    goto/16 :goto_c

    #@1c2
    .line 743
    .end local v4           #messageId:Ljava/lang/String;
    .end local v16           #callState:I
    .end local v17           #chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    .end local v21           #contributionId:Ljava/lang/String;
    .end local v26           #dateTime:Ljava/lang/String;
    .end local v27           #deferredMessages:I
    .end local v28           #displayName:Ljava/lang/String;
    .end local v29           #displayNotification:I
    .end local v31           #extra:Landroid/os/Bundle;
    .end local v34           #intent:Landroid/content/Intent;
    .end local v35           #mimeType:Ljava/lang/String;
    .end local v37           #nativeObj:I
    .end local v41           #senderMSISDN:Ljava/lang/String;
    .end local v42           #senderURI:Ljava/lang/String;
    .end local v45           #subject:Ljava/lang/String;
    :catch_1c2
    move-exception v30

    #@1c3
    .line 744
    .local v30, e:Ljava/lang/Exception;
    invoke-virtual/range {v30 .. v30}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@1c6
    move-result-object v47

    #@1c7
    invoke-virtual/range {v47 .. v47}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1ca
    move-result-object v47

    #@1cb
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@1ce
    goto/16 :goto_c

    #@1d0
    .line 392
    .end local v30           #e:Ljava/lang/Exception;
    :pswitch_1d0
    :try_start_1d0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1d3
    move-result v37

    #@1d4
    .line 393
    .restart local v37       #nativeObj:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1d7
    move-result-object v42

    #@1d8
    .line 394
    .restart local v42       #senderURI:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1db
    move-result-object v41

    #@1dc
    .line 395
    .restart local v41       #senderMSISDN:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1df
    move-result-object v28

    #@1e0
    .line 396
    .restart local v28       #displayName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e3
    move-result-object v35

    #@1e4
    .line 397
    .restart local v35       #mimeType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1e7
    move-result-object v45

    #@1e8
    .line 398
    .restart local v45       #subject:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1eb
    move-result-object v4

    #@1ec
    .line 399
    .restart local v4       #messageId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1ef
    move-result-object v26

    #@1f0
    .line 400
    .restart local v26       #dateTime:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1f3
    move-result-object v21

    #@1f4
    .line 401
    .restart local v21       #contributionId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@1f7
    move-result-object v43

    #@1f8
    .line 402
    .local v43, sessionReplaces:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1fb
    move-result v29

    #@1fc
    .line 403
    .restart local v29       #displayNotification:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1ff
    move-result v27

    #@200
    .line 405
    .restart local v27       #deferredMessages:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@203
    move-result v38

    #@204
    .line 406
    .local v38, numOfParticipants:I
    const/16 v39, 0x0

    #@206
    .line 408
    .local v39, participants:[Ljava/lang/String;
    if-lez v38, :cond_245

    #@208
    .line 409
    move/from16 v0, v38

    #@20a
    new-array v0, v0, [Ljava/lang/String;

    #@20c
    move-object/from16 v39, v0

    #@20e
    .line 410
    const/16 v32, 0x0

    #@210
    .local v32, i:I
    :goto_210
    move/from16 v0, v32

    #@212
    move/from16 v1, v38

    #@214
    if-ge v0, v1, :cond_245

    #@216
    .line 411
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@219
    move-result-object v47

    #@21a
    aput-object v47, v39, v32

    #@21c
    .line 412
    new-instance v47, Ljava/lang/StringBuilder;

    #@21e
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@221
    const-string v48, "participient ["

    #@223
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v47

    #@227
    move-object/from16 v0, v47

    #@229
    move/from16 v1, v32

    #@22b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v47

    #@22f
    const-string v48, "] = "

    #@231
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@234
    move-result-object v47

    #@235
    aget-object v48, v39, v32

    #@237
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v47

    #@23b
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23e
    move-result-object v47

    #@23f
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@242
    .line 410
    add-int/lit8 v32, v32, 0x1

    #@244
    goto :goto_210

    #@245
    .line 416
    .end local v32           #i:I
    :cond_245
    move-object/from16 v0, p0

    #@247
    move-object/from16 v1, v41

    #@249
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/IMServiceImpl;->getCallState(Ljava/lang/String;)I

    #@24c
    move-result v16

    #@24d
    .line 418
    .restart local v16       #callState:I
    new-instance v47, Ljava/lang/StringBuilder;

    #@24f
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@252
    const-string v48, "RECEIVEDCONFERENCE_IND \n, senderURI = "

    #@254
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@257
    move-result-object v47

    #@258
    move-object/from16 v0, v47

    #@25a
    move-object/from16 v1, v42

    #@25c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25f
    move-result-object v47

    #@260
    const-string v48, "\n"

    #@262
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@265
    move-result-object v47

    #@266
    const-string v48, ", senderMSISDN = "

    #@268
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26b
    move-result-object v47

    #@26c
    move-object/from16 v0, v47

    #@26e
    move-object/from16 v1, v41

    #@270
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@273
    move-result-object v47

    #@274
    const-string v48, "\n"

    #@276
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@279
    move-result-object v47

    #@27a
    const-string v48, ", displayName = "

    #@27c
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27f
    move-result-object v47

    #@280
    move-object/from16 v0, v47

    #@282
    move-object/from16 v1, v28

    #@284
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@287
    move-result-object v47

    #@288
    const-string v48, "\n"

    #@28a
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v47

    #@28e
    const-string v48, ", subject = "

    #@290
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@293
    move-result-object v47

    #@294
    move-object/from16 v0, v47

    #@296
    move-object/from16 v1, v45

    #@298
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29b
    move-result-object v47

    #@29c
    const-string v48, "\n"

    #@29e
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a1
    move-result-object v47

    #@2a2
    const-string v48, ", messageId = "

    #@2a4
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a7
    move-result-object v47

    #@2a8
    move-object/from16 v0, v47

    #@2aa
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ad
    move-result-object v47

    #@2ae
    const-string v48, "\n"

    #@2b0
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b3
    move-result-object v47

    #@2b4
    const-string v48, ", dateTime = "

    #@2b6
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b9
    move-result-object v47

    #@2ba
    move-object/from16 v0, v47

    #@2bc
    move-object/from16 v1, v26

    #@2be
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v47

    #@2c2
    const-string v48, "\n"

    #@2c4
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c7
    move-result-object v47

    #@2c8
    const-string v48, ", contributionId = "

    #@2ca
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cd
    move-result-object v47

    #@2ce
    move-object/from16 v0, v47

    #@2d0
    move-object/from16 v1, v21

    #@2d2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d5
    move-result-object v47

    #@2d6
    const-string v48, "\n"

    #@2d8
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2db
    move-result-object v47

    #@2dc
    const-string v48, ", displayNotification = "

    #@2de
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e1
    move-result-object v47

    #@2e2
    move-object/from16 v0, v47

    #@2e4
    move/from16 v1, v29

    #@2e6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e9
    move-result-object v47

    #@2ea
    const-string v48, "\n"

    #@2ec
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ef
    move-result-object v47

    #@2f0
    const-string v48, ", deferredMessages = "

    #@2f2
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f5
    move-result-object v47

    #@2f6
    move-object/from16 v0, v47

    #@2f8
    move/from16 v1, v27

    #@2fa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2fd
    move-result-object v47

    #@2fe
    const-string v48, "\n"

    #@300
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@303
    move-result-object v47

    #@304
    const-string v48, ", numOfRecipients = "

    #@306
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@309
    move-result-object v47

    #@30a
    move-object/from16 v0, v47

    #@30c
    move/from16 v1, v38

    #@30e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@311
    move-result-object v47

    #@312
    const-string v48, "\n"

    #@314
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@317
    move-result-object v47

    #@318
    const-string v48, ", call state = "

    #@31a
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31d
    move-result-object v47

    #@31e
    move-object/from16 v0, v47

    #@320
    move/from16 v1, v16

    #@322
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@325
    move-result-object v47

    #@326
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@329
    move-result-object v47

    #@32a
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@32d
    .line 431
    move-object/from16 v0, p0

    #@32f
    move/from16 v1, v37

    #@331
    move-object/from16 v2, v21

    #@333
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/service/im/IMServiceImpl;->openChat(ILjava/lang/String;)Lcom/lge/ims/service/im/ChatImpl;

    #@336
    move-result-object v17

    #@337
    .line 433
    .restart local v17       #chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    new-instance v34, Landroid/content/Intent;

    #@339
    invoke-direct/range {v34 .. v34}, Landroid/content/Intent;-><init>()V

    #@33c
    .line 434
    .restart local v34       #intent:Landroid/content/Intent;
    const-string v47, "com.lge.ims.rcsim.CONFERENCE_INVITE_RECEIVED"

    #@33e
    move-object/from16 v0, v34

    #@340
    move-object/from16 v1, v47

    #@342
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@345
    .line 435
    new-instance v31, Landroid/os/Bundle;

    #@347
    invoke-direct/range {v31 .. v31}, Landroid/os/Bundle;-><init>()V

    #@34a
    .line 436
    .restart local v31       #extra:Landroid/os/Bundle;
    const-string v47, "ChatImpl"

    #@34c
    move-object/from16 v0, v31

    #@34e
    move-object/from16 v1, v47

    #@350
    move-object/from16 v2, v17

    #@352
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    #@355
    .line 437
    move-object/from16 v0, v34

    #@357
    move-object/from16 v1, v31

    #@359
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@35c
    .line 438
    const-string v47, "remoteUserUri"

    #@35e
    move-object/from16 v0, v34

    #@360
    move-object/from16 v1, v47

    #@362
    move-object/from16 v2, v42

    #@364
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@367
    .line 439
    const-string v47, "remoteUser"

    #@369
    move-object/from16 v0, v34

    #@36b
    move-object/from16 v1, v47

    #@36d
    move-object/from16 v2, v41

    #@36f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@372
    .line 440
    const-string v47, "displayName"

    #@374
    move-object/from16 v0, v34

    #@376
    move-object/from16 v1, v47

    #@378
    move-object/from16 v2, v28

    #@37a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@37d
    .line 441
    const-string v47, "messageId"

    #@37f
    move-object/from16 v0, v34

    #@381
    move-object/from16 v1, v47

    #@383
    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@386
    .line 442
    const-string v47, "dateTime"

    #@388
    move-object/from16 v0, v34

    #@38a
    move-object/from16 v1, v47

    #@38c
    move-object/from16 v2, v26

    #@38e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@391
    .line 443
    const-string v47, "subject"

    #@393
    move-object/from16 v0, v34

    #@395
    move-object/from16 v1, v47

    #@397
    move-object/from16 v2, v45

    #@399
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@39c
    .line 444
    const-string v47, "participants"

    #@39e
    move-object/from16 v0, v34

    #@3a0
    move-object/from16 v1, v47

    #@3a2
    move-object/from16 v2, v39

    #@3a4
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@3a7
    .line 445
    const-string v47, "callState"

    #@3a9
    move-object/from16 v0, v34

    #@3ab
    move-object/from16 v1, v47

    #@3ad
    move/from16 v2, v16

    #@3af
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3b2
    .line 446
    const-string v47, "mimeType"

    #@3b4
    move-object/from16 v0, v34

    #@3b6
    move-object/from16 v1, v47

    #@3b8
    move-object/from16 v2, v35

    #@3ba
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3bd
    .line 447
    const-string v47, "contributionId"

    #@3bf
    move-object/from16 v0, v34

    #@3c1
    move-object/from16 v1, v47

    #@3c3
    move-object/from16 v2, v21

    #@3c5
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3c8
    .line 448
    const-string v47, "sessionReplaces"

    #@3ca
    move-object/from16 v0, v34

    #@3cc
    move-object/from16 v1, v47

    #@3ce
    move-object/from16 v2, v43

    #@3d0
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3d3
    .line 449
    const-string v47, "displayNotification"

    #@3d5
    move-object/from16 v0, v34

    #@3d7
    move-object/from16 v1, v47

    #@3d9
    move/from16 v2, v29

    #@3db
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3de
    .line 450
    const-string v47, "deferredMessages"

    #@3e0
    move-object/from16 v0, v34

    #@3e2
    move-object/from16 v1, v47

    #@3e4
    move/from16 v2, v27

    #@3e6
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3e9
    .line 452
    move-object/from16 v0, p0

    #@3eb
    iget-object v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@3ed
    move-object/from16 v47, v0

    #@3ef
    move-object/from16 v0, v47

    #@3f1
    move-object/from16 v1, v34

    #@3f3
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@3f6
    goto/16 :goto_c

    #@3f8
    .line 457
    .end local v4           #messageId:Ljava/lang/String;
    .end local v16           #callState:I
    .end local v17           #chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    .end local v21           #contributionId:Ljava/lang/String;
    .end local v26           #dateTime:Ljava/lang/String;
    .end local v27           #deferredMessages:I
    .end local v28           #displayName:Ljava/lang/String;
    .end local v29           #displayNotification:I
    .end local v31           #extra:Landroid/os/Bundle;
    .end local v34           #intent:Landroid/content/Intent;
    .end local v35           #mimeType:Ljava/lang/String;
    .end local v37           #nativeObj:I
    .end local v38           #numOfParticipants:I
    .end local v39           #participants:[Ljava/lang/String;
    .end local v41           #senderMSISDN:Ljava/lang/String;
    .end local v42           #senderURI:Ljava/lang/String;
    .end local v43           #sessionReplaces:Ljava/lang/String;
    .end local v45           #subject:Ljava/lang/String;
    :pswitch_3f8
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3fb
    move-result v37

    #@3fc
    .line 458
    .restart local v37       #nativeObj:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3ff
    move-result-object v42

    #@400
    .line 459
    .restart local v42       #senderURI:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@403
    move-result-object v28

    #@404
    .line 460
    .restart local v28       #displayName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@407
    move-result-object v9

    #@408
    .line 461
    .local v9, fileName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@40b
    move-result-object v20

    #@40c
    .line 462
    .local v20, contentType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@40f
    move-result v11

    #@410
    .line 465
    .local v11, fileSize:I
    new-instance v47, Ljava/lang/StringBuilder;

    #@412
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@415
    const-string v48, "RECEIVEDFILETRANSFER_IND: "

    #@417
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41a
    move-result-object v47

    #@41b
    move-object/from16 v0, v47

    #@41d
    move-object/from16 v1, v28

    #@41f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@422
    move-result-object v47

    #@423
    const-string v48, "<"

    #@425
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@428
    move-result-object v47

    #@429
    move-object/from16 v0, v47

    #@42b
    move-object/from16 v1, v42

    #@42d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@430
    move-result-object v47

    #@431
    const-string v48, ">"

    #@433
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@436
    move-result-object v47

    #@437
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43a
    move-result-object v47

    #@43b
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@43e
    .line 467
    new-instance v47, Ljava/lang/StringBuilder;

    #@440
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@443
    const-string v48, "file info : "

    #@445
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@448
    move-result-object v47

    #@449
    move-object/from16 v0, v47

    #@44b
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44e
    move-result-object v47

    #@44f
    const-string v48, " <"

    #@451
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@454
    move-result-object v47

    #@455
    move-object/from16 v0, v47

    #@457
    move-object/from16 v1, v20

    #@459
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45c
    move-result-object v47

    #@45d
    const-string v48, "> "

    #@45f
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@462
    move-result-object v47

    #@463
    move-object/from16 v0, v47

    #@465
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@468
    move-result-object v47

    #@469
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46c
    move-result-object v47

    #@46d
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@470
    .line 471
    move-object/from16 v0, p0

    #@472
    move/from16 v1, v37

    #@474
    invoke-direct {v0, v1}, Lcom/lge/ims/service/im/IMServiceImpl;->openFileTransferToDownload(I)Lcom/lge/ims/service/im/FileTransferImplFacade;

    #@477
    move-result-object v33

    #@478
    .line 474
    .local v33, iFileTransfer:Lcom/lge/ims/service/im/FileTransferImplFacade;
    move-object/from16 v0, p0

    #@47a
    move-object/from16 v1, v42

    #@47c
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/IMServiceImpl;->getCallState(Ljava/lang/String;)I

    #@47f
    move-result v16

    #@480
    .line 476
    .restart local v16       #callState:I
    new-instance v34, Landroid/content/Intent;

    #@482
    invoke-direct/range {v34 .. v34}, Landroid/content/Intent;-><init>()V

    #@485
    .line 477
    .restart local v34       #intent:Landroid/content/Intent;
    const-string v47, "com.lge.ims.rcsim.FILE_TRANSFER_INVITE_RECEIVED"

    #@487
    move-object/from16 v0, v34

    #@489
    move-object/from16 v1, v47

    #@48b
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@48e
    .line 478
    new-instance v31, Landroid/os/Bundle;

    #@490
    invoke-direct/range {v31 .. v31}, Landroid/os/Bundle;-><init>()V

    #@493
    .line 479
    .restart local v31       #extra:Landroid/os/Bundle;
    const-string v47, "FileTransferImpl"

    #@495
    move-object/from16 v0, v31

    #@497
    move-object/from16 v1, v47

    #@499
    move-object/from16 v2, v33

    #@49b
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    #@49e
    .line 480
    move-object/from16 v0, v34

    #@4a0
    move-object/from16 v1, v31

    #@4a2
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@4a5
    .line 481
    const-string v47, "callState"

    #@4a7
    move-object/from16 v0, v34

    #@4a9
    move-object/from16 v1, v47

    #@4ab
    move/from16 v2, v16

    #@4ad
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@4b0
    .line 482
    const-string v47, "remoteUser"

    #@4b2
    move-object/from16 v0, v34

    #@4b4
    move-object/from16 v1, v47

    #@4b6
    move-object/from16 v2, v42

    #@4b8
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4bb
    .line 483
    const-string v47, "displayName"

    #@4bd
    move-object/from16 v0, v34

    #@4bf
    move-object/from16 v1, v47

    #@4c1
    move-object/from16 v2, v28

    #@4c3
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4c6
    .line 484
    const-string v47, "fileName"

    #@4c8
    move-object/from16 v0, v34

    #@4ca
    move-object/from16 v1, v47

    #@4cc
    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4cf
    .line 485
    const-string v47, "contentType"

    #@4d1
    move-object/from16 v0, v34

    #@4d3
    move-object/from16 v1, v47

    #@4d5
    move-object/from16 v2, v20

    #@4d7
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4da
    .line 486
    const-string v47, "fileSize"

    #@4dc
    move-object/from16 v0, v34

    #@4de
    move-object/from16 v1, v47

    #@4e0
    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@4e3
    .line 489
    move-object/from16 v0, p0

    #@4e5
    iget-object v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@4e7
    move-object/from16 v47, v0

    #@4e9
    move-object/from16 v0, v47

    #@4eb
    move-object/from16 v1, v34

    #@4ed
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@4f0
    goto/16 :goto_c

    #@4f2
    .line 494
    .end local v9           #fileName:Ljava/lang/String;
    .end local v11           #fileSize:I
    .end local v16           #callState:I
    .end local v20           #contentType:Ljava/lang/String;
    .end local v28           #displayName:Ljava/lang/String;
    .end local v31           #extra:Landroid/os/Bundle;
    .end local v33           #iFileTransfer:Lcom/lge/ims/service/im/FileTransferImplFacade;
    .end local v34           #intent:Landroid/content/Intent;
    .end local v37           #nativeObj:I
    .end local v42           #senderURI:Ljava/lang/String;
    :pswitch_4f2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4f5
    move-result-object v40

    #@4f6
    .line 495
    .local v40, sender:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4f9
    move-result-object v4

    #@4fa
    .line 496
    .restart local v4       #messageId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@4fd
    move-result v44

    #@4fe
    .line 497
    .local v44, status:I
    new-instance v47, Ljava/lang/StringBuilder;

    #@500
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@503
    const-string v48, "RECEIVEDIMDN_IND : sender = "

    #@505
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@508
    move-result-object v47

    #@509
    move-object/from16 v0, v47

    #@50b
    move-object/from16 v1, v40

    #@50d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@510
    move-result-object v47

    #@511
    const-string v48, ", messageId = "

    #@513
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@516
    move-result-object v47

    #@517
    move-object/from16 v0, v47

    #@519
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51c
    move-result-object v47

    #@51d
    const-string v48, ", status = "

    #@51f
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@522
    move-result-object v47

    #@523
    move-object/from16 v0, v47

    #@525
    move/from16 v1, v44

    #@527
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52a
    move-result-object v47

    #@52b
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52e
    move-result-object v47

    #@52f
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@532
    .line 498
    packed-switch v44, :pswitch_data_b24

    #@535
    goto/16 :goto_c

    #@537
    .line 502
    :pswitch_537
    move-object/from16 v0, p0

    #@539
    iget-object v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@53b
    move-object/from16 v47, v0

    #@53d
    if-eqz v47, :cond_c

    #@53f
    .line 503
    move-object/from16 v0, p0

    #@541
    iget-object v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@543
    move-object/from16 v47, v0

    #@545
    move-object/from16 v0, v47

    #@547
    move-object/from16 v1, v40

    #@549
    invoke-interface {v0, v1, v4}, Lcom/lge/ims/service/im/IIMServiceListener;->messageDelivered(Ljava/lang/String;Ljava/lang/String;)V

    #@54c
    goto/16 :goto_c

    #@54e
    .line 507
    :pswitch_54e
    move-object/from16 v0, p0

    #@550
    iget-object v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@552
    move-object/from16 v47, v0

    #@554
    if-eqz v47, :cond_c

    #@556
    .line 508
    move-object/from16 v0, p0

    #@558
    iget-object v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@55a
    move-object/from16 v47, v0

    #@55c
    move-object/from16 v0, v47

    #@55e
    move-object/from16 v1, v40

    #@560
    invoke-interface {v0, v1, v4}, Lcom/lge/ims/service/im/IIMServiceListener;->messageDisplayed(Ljava/lang/String;Ljava/lang/String;)V

    #@563
    goto/16 :goto_c

    #@565
    .line 518
    .end local v4           #messageId:Ljava/lang/String;
    .end local v40           #sender:Ljava/lang/String;
    .end local v44           #status:I
    :pswitch_565
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@568
    move-result v37

    #@569
    .line 519
    .restart local v37       #nativeObj:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@56c
    move-result-object v42

    #@56d
    .line 520
    .restart local v42       #senderURI:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@570
    move-result-object v41

    #@571
    .line 521
    .restart local v41       #senderMSISDN:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@574
    move-result-object v28

    #@575
    .line 522
    .restart local v28       #displayName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@578
    move-result-object v4

    #@579
    .line 523
    .restart local v4       #messageId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@57c
    move-result-object v26

    #@57d
    .line 524
    .restart local v26       #dateTime:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@580
    move-result-object v21

    #@581
    .line 527
    .restart local v21       #contributionId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@584
    move-result v7

    #@585
    .line 528
    .local v7, thumbnailSize:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@588
    move-result-object v5

    #@589
    .line 529
    .local v5, thumbnailName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@58c
    move-result-object v6

    #@58d
    .line 530
    .local v6, thumbnailContentType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@590
    move-result-object v8

    #@591
    .line 533
    .local v8, thumbnailUrl:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@594
    move-result v11

    #@595
    .line 534
    .restart local v11       #fileSize:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@598
    move-result-object v47

    #@599
    invoke-static/range {v47 .. v47}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@59c
    move-result-object v9

    #@59d
    .line 535
    .restart local v9       #fileName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5a0
    move-result-object v10

    #@5a1
    .line 536
    .local v10, fileContentType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5a4
    move-result-object v12

    #@5a5
    .line 539
    .local v12, fileDownloadUrl:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@5a8
    move-result-object v13

    #@5a9
    .line 540
    .local v13, expiryDateTime:Ljava/lang/String;
    new-instance v47, Ljava/lang/StringBuilder;

    #@5ab
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@5ae
    const-string v48, " : TERMINATED_IND: until="

    #@5b0
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b3
    move-result-object v47

    #@5b4
    move-object/from16 v0, v47

    #@5b6
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b9
    move-result-object v47

    #@5ba
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5bd
    move-result-object v47

    #@5be
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5c1
    .line 542
    const-string v47, "GMT"

    #@5c3
    invoke-static/range {v47 .. v47}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@5c6
    move-result-object v47

    #@5c7
    invoke-static/range {v47 .. v47}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    #@5ca
    move-result-object v15

    #@5cb
    .line 543
    .local v15, cal:Ljava/util/Calendar;
    invoke-virtual {v15}, Ljava/util/Calendar;->getTimeInMillis()J

    #@5ce
    move-result-wide v47

    #@5cf
    const-wide/32 v49, 0x5265c00

    #@5d2
    add-long v23, v47, v49

    #@5d4
    .line 544
    .local v23, currentTime:J
    move-wide/from16 v0, v23

    #@5d6
    invoke-virtual {v15, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@5d9
    .line 545
    invoke-virtual {v15}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@5dc
    move-result-object v22

    #@5dd
    .line 546
    .local v22, currentLocalTime:Ljava/util/Date;
    new-instance v25, Ljava/text/SimpleDateFormat;

    #@5df
    const-string v47, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    #@5e1
    move-object/from16 v0, v25

    #@5e3
    move-object/from16 v1, v47

    #@5e5
    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@5e8
    .line 547
    .local v25, dateFormat:Ljava/text/SimpleDateFormat;
    const-string v47, "GMT"

    #@5ea
    invoke-static/range {v47 .. v47}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@5ed
    move-result-object v47

    #@5ee
    move-object/from16 v0, v25

    #@5f0
    move-object/from16 v1, v47

    #@5f2
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    #@5f5
    .line 548
    move-object/from16 v0, v25

    #@5f7
    move-object/from16 v1, v22

    #@5f9
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@5fc
    move-result-object v13

    #@5fd
    .line 550
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@600
    move-result v27

    #@601
    .line 551
    .restart local v27       #deferredMessages:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@604
    move-result v29

    #@605
    .line 554
    .restart local v29       #displayNotification:I
    move-object/from16 v0, p0

    #@607
    move-object/from16 v1, v41

    #@609
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/IMServiceImpl;->getCallState(Ljava/lang/String;)I

    #@60c
    move-result v16

    #@60d
    .line 556
    .restart local v16       #callState:I
    const/4 v14, 0x0

    #@60e
    .line 557
    .local v14, bDeferredMessages:Z
    if-nez v27, :cond_7c2

    #@610
    .line 558
    const/4 v14, 0x0

    #@611
    .line 563
    :goto_611
    new-instance v47, Ljava/lang/StringBuilder;

    #@613
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@616
    const-string v48, "\tmessageId = "

    #@618
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61b
    move-result-object v47

    #@61c
    move-object/from16 v0, v47

    #@61e
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@621
    move-result-object v47

    #@622
    const-string v48, "\n"

    #@624
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@627
    move-result-object v47

    #@628
    const-string v48, ", senderURI = "

    #@62a
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62d
    move-result-object v47

    #@62e
    move-object/from16 v0, v47

    #@630
    move-object/from16 v1, v42

    #@632
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@635
    move-result-object v47

    #@636
    const-string v48, "\n"

    #@638
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63b
    move-result-object v47

    #@63c
    const-string v48, ", senderMSISDN = "

    #@63e
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@641
    move-result-object v47

    #@642
    move-object/from16 v0, v47

    #@644
    move-object/from16 v1, v41

    #@646
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@649
    move-result-object v47

    #@64a
    const-string v48, "\n"

    #@64c
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64f
    move-result-object v47

    #@650
    const-string v48, ", displayName = "

    #@652
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@655
    move-result-object v47

    #@656
    move-object/from16 v0, v47

    #@658
    move-object/from16 v1, v28

    #@65a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65d
    move-result-object v47

    #@65e
    const-string v48, "\n"

    #@660
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@663
    move-result-object v47

    #@664
    const-string v48, ", dateTime = "

    #@666
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@669
    move-result-object v47

    #@66a
    move-object/from16 v0, v47

    #@66c
    move-object/from16 v1, v26

    #@66e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@671
    move-result-object v47

    #@672
    const-string v48, ", fileName="

    #@674
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@677
    move-result-object v47

    #@678
    move-object/from16 v0, v47

    #@67a
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67d
    move-result-object v47

    #@67e
    const-string v48, "\n"

    #@680
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@683
    move-result-object v47

    #@684
    const-string v48, ", fileContentType = "

    #@686
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@689
    move-result-object v47

    #@68a
    move-object/from16 v0, v47

    #@68c
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68f
    move-result-object v47

    #@690
    const-string v48, "\n"

    #@692
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@695
    move-result-object v47

    #@696
    const-string v48, ", thumbnailUrl = "

    #@698
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69b
    move-result-object v47

    #@69c
    move-object/from16 v0, v47

    #@69e
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a1
    move-result-object v47

    #@6a2
    const-string v48, "\n"

    #@6a4
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a7
    move-result-object v47

    #@6a8
    const-string v48, ", fileDownloadUrl = "

    #@6aa
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6ad
    move-result-object v47

    #@6ae
    move-object/from16 v0, v47

    #@6b0
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b3
    move-result-object v47

    #@6b4
    const-string v48, "\n"

    #@6b6
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b9
    move-result-object v47

    #@6ba
    const-string v48, ", expiryDateTime = "

    #@6bc
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6bf
    move-result-object v47

    #@6c0
    move-object/from16 v0, v47

    #@6c2
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c5
    move-result-object v47

    #@6c6
    const-string v48, "\n"

    #@6c8
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6cb
    move-result-object v47

    #@6cc
    const-string v48, ", deferredMessages = "

    #@6ce
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d1
    move-result-object v47

    #@6d2
    move-object/from16 v0, v47

    #@6d4
    move/from16 v1, v27

    #@6d6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d9
    move-result-object v47

    #@6da
    const-string v48, "\n"

    #@6dc
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6df
    move-result-object v47

    #@6e0
    const-string v48, ", displayNotification = "

    #@6e2
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e5
    move-result-object v47

    #@6e6
    move-object/from16 v0, v47

    #@6e8
    move/from16 v1, v29

    #@6ea
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6ed
    move-result-object v47

    #@6ee
    const-string v48, "\n"

    #@6f0
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f3
    move-result-object v47

    #@6f4
    const-string v48, ", call state = "

    #@6f6
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f9
    move-result-object v47

    #@6fa
    move-object/from16 v0, v47

    #@6fc
    move/from16 v1, v16

    #@6fe
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@701
    move-result-object v47

    #@702
    const-string v48, "\n"

    #@704
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@707
    move-result-object v47

    #@708
    const-string v48, ", contributionId = "

    #@70a
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70d
    move-result-object v47

    #@70e
    move-object/from16 v0, v47

    #@710
    move-object/from16 v1, v21

    #@712
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@715
    move-result-object v47

    #@716
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@719
    move-result-object v47

    #@71a
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@71d
    .line 577
    move-object/from16 v0, p0

    #@71f
    move/from16 v1, v37

    #@721
    move-object/from16 v2, v21

    #@723
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/service/im/IMServiceImpl;->openChat(ILjava/lang/String;)Lcom/lge/ims/service/im/ChatImpl;

    #@726
    move-result-object v17

    #@727
    .line 579
    .restart local v17       #chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    new-instance v34, Landroid/content/Intent;

    #@729
    invoke-direct/range {v34 .. v34}, Landroid/content/Intent;-><init>()V

    #@72c
    .line 580
    .restart local v34       #intent:Landroid/content/Intent;
    const-string v47, "com.lge.ims.rcsim.INVITE_RECEIVED_WITH_FILELINK"

    #@72e
    move-object/from16 v0, v34

    #@730
    move-object/from16 v1, v47

    #@732
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@735
    .line 582
    new-instance v31, Landroid/os/Bundle;

    #@737
    invoke-direct/range {v31 .. v31}, Landroid/os/Bundle;-><init>()V

    #@73a
    .line 583
    .restart local v31       #extra:Landroid/os/Bundle;
    const-string v47, "ChatImpl"

    #@73c
    move-object/from16 v0, v31

    #@73e
    move-object/from16 v1, v47

    #@740
    move-object/from16 v2, v17

    #@742
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    #@745
    .line 584
    move-object/from16 v0, v34

    #@747
    move-object/from16 v1, v31

    #@749
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@74c
    .line 585
    const-string v47, "callState"

    #@74e
    move-object/from16 v0, v34

    #@750
    move-object/from16 v1, v47

    #@752
    move/from16 v2, v16

    #@754
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@757
    .line 586
    const-string v47, "remoteUserUri"

    #@759
    move-object/from16 v0, v34

    #@75b
    move-object/from16 v1, v47

    #@75d
    move-object/from16 v2, v42

    #@75f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@762
    .line 587
    const-string v47, "remoteUser"

    #@764
    move-object/from16 v0, v34

    #@766
    move-object/from16 v1, v47

    #@768
    move-object/from16 v2, v41

    #@76a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@76d
    .line 588
    const-string v47, "displayName"

    #@76f
    move-object/from16 v0, v34

    #@771
    move-object/from16 v1, v47

    #@773
    move-object/from16 v2, v28

    #@775
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@778
    .line 589
    const-string v47, "dateTime"

    #@77a
    move-object/from16 v0, v34

    #@77c
    move-object/from16 v1, v47

    #@77e
    move-object/from16 v2, v26

    #@780
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@783
    .line 590
    const-string v47, "displayNotification"

    #@785
    move-object/from16 v0, v34

    #@787
    move-object/from16 v1, v47

    #@789
    move/from16 v2, v29

    #@78b
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@78e
    .line 591
    const-string v47, "deferredMessages"

    #@790
    move-object/from16 v0, v34

    #@792
    move-object/from16 v1, v47

    #@794
    invoke-virtual {v0, v1, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@797
    .line 592
    const-string v47, "contributionId"

    #@799
    move-object/from16 v0, v34

    #@79b
    move-object/from16 v1, v47

    #@79d
    move-object/from16 v2, v21

    #@79f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@7a2
    .line 595
    new-instance v3, Lcom/lge/ims/service/im/IMFileInfo;

    #@7a4
    invoke-direct {v3}, Lcom/lge/ims/service/im/IMFileInfo;-><init>()V

    #@7a7
    .line 596
    .local v3, imFileInfo:Lcom/lge/ims/service/im/IMFileInfo;
    invoke-virtual/range {v3 .. v13}, Lcom/lge/ims/service/im/IMFileInfo;->setParamOfFileLinkIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@7aa
    .line 599
    const-string v47, "FileInfo"

    #@7ac
    move-object/from16 v0, v34

    #@7ae
    move-object/from16 v1, v47

    #@7b0
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@7b3
    .line 601
    move-object/from16 v0, p0

    #@7b5
    iget-object v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@7b7
    move-object/from16 v47, v0

    #@7b9
    move-object/from16 v0, v47

    #@7bb
    move-object/from16 v1, v34

    #@7bd
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@7c0
    goto/16 :goto_c

    #@7c2
    .line 560
    .end local v3           #imFileInfo:Lcom/lge/ims/service/im/IMFileInfo;
    .end local v17           #chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    .end local v31           #extra:Landroid/os/Bundle;
    .end local v34           #intent:Landroid/content/Intent;
    :cond_7c2
    const/4 v14, 0x1

    #@7c3
    goto/16 :goto_611

    #@7c5
    .line 606
    .end local v4           #messageId:Ljava/lang/String;
    .end local v5           #thumbnailName:Ljava/lang/String;
    .end local v6           #thumbnailContentType:Ljava/lang/String;
    .end local v7           #thumbnailSize:I
    .end local v8           #thumbnailUrl:Ljava/lang/String;
    .end local v9           #fileName:Ljava/lang/String;
    .end local v10           #fileContentType:Ljava/lang/String;
    .end local v11           #fileSize:I
    .end local v12           #fileDownloadUrl:Ljava/lang/String;
    .end local v13           #expiryDateTime:Ljava/lang/String;
    .end local v14           #bDeferredMessages:Z
    .end local v15           #cal:Ljava/util/Calendar;
    .end local v16           #callState:I
    .end local v21           #contributionId:Ljava/lang/String;
    .end local v22           #currentLocalTime:Ljava/util/Date;
    .end local v23           #currentTime:J
    .end local v25           #dateFormat:Ljava/text/SimpleDateFormat;
    .end local v26           #dateTime:Ljava/lang/String;
    .end local v27           #deferredMessages:I
    .end local v28           #displayName:Ljava/lang/String;
    .end local v29           #displayNotification:I
    .end local v37           #nativeObj:I
    .end local v41           #senderMSISDN:Ljava/lang/String;
    .end local v42           #senderURI:Ljava/lang/String;
    :pswitch_7c5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@7c8
    move-result v37

    #@7c9
    .line 607
    .restart local v37       #nativeObj:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7cc
    move-result-object v42

    #@7cd
    .line 608
    .restart local v42       #senderURI:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7d0
    move-result-object v41

    #@7d1
    .line 609
    .restart local v41       #senderMSISDN:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7d4
    move-result-object v28

    #@7d5
    .line 610
    .restart local v28       #displayName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7d8
    move-result-object v4

    #@7d9
    .line 611
    .restart local v4       #messageId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7dc
    move-result-object v26

    #@7dd
    .line 612
    .restart local v26       #dateTime:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7e0
    move-result-object v21

    #@7e1
    .line 613
    .restart local v21       #contributionId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7e4
    move-result-object v43

    #@7e5
    .line 616
    .restart local v43       #sessionReplaces:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@7e8
    move-result v7

    #@7e9
    .line 617
    .restart local v7       #thumbnailSize:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7ec
    move-result-object v5

    #@7ed
    .line 618
    .restart local v5       #thumbnailName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7f0
    move-result-object v6

    #@7f1
    .line 619
    .restart local v6       #thumbnailContentType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7f4
    move-result-object v8

    #@7f5
    .line 622
    .restart local v8       #thumbnailUrl:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@7f8
    move-result v11

    #@7f9
    .line 623
    .restart local v11       #fileSize:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@7fc
    move-result-object v47

    #@7fd
    invoke-static/range {v47 .. v47}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    #@800
    move-result-object v9

    #@801
    .line 624
    .restart local v9       #fileName:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@804
    move-result-object v10

    #@805
    .line 625
    .restart local v10       #fileContentType:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@808
    move-result-object v12

    #@809
    .line 628
    .restart local v12       #fileDownloadUrl:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@80c
    move-result-object v13

    #@80d
    .line 630
    .restart local v13       #expiryDateTime:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@810
    move-result v27

    #@811
    .line 631
    .restart local v27       #deferredMessages:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@814
    move-result v29

    #@815
    .line 633
    .restart local v29       #displayNotification:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@818
    move-result v38

    #@819
    .line 634
    .restart local v38       #numOfParticipants:I
    const/16 v39, 0x0

    #@81b
    .line 635
    .restart local v39       #participants:[Ljava/lang/String;
    if-lez v38, :cond_85a

    #@81d
    .line 636
    move/from16 v0, v38

    #@81f
    new-array v0, v0, [Ljava/lang/String;

    #@821
    move-object/from16 v39, v0

    #@823
    .line 637
    const/16 v32, 0x0

    #@825
    .restart local v32       #i:I
    :goto_825
    move/from16 v0, v32

    #@827
    move/from16 v1, v38

    #@829
    if-ge v0, v1, :cond_85a

    #@82b
    .line 638
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@82e
    move-result-object v47

    #@82f
    aput-object v47, v39, v32

    #@831
    .line 639
    new-instance v47, Ljava/lang/StringBuilder;

    #@833
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@836
    const-string v48, "participient ["

    #@838
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83b
    move-result-object v47

    #@83c
    move-object/from16 v0, v47

    #@83e
    move/from16 v1, v32

    #@840
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@843
    move-result-object v47

    #@844
    const-string v48, "] = "

    #@846
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@849
    move-result-object v47

    #@84a
    aget-object v48, v39, v32

    #@84c
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84f
    move-result-object v47

    #@850
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@853
    move-result-object v47

    #@854
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@857
    .line 637
    add-int/lit8 v32, v32, 0x1

    #@859
    goto :goto_825

    #@85a
    .line 643
    .end local v32           #i:I
    :cond_85a
    move-object/from16 v0, p0

    #@85c
    move-object/from16 v1, v41

    #@85e
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/IMServiceImpl;->getCallState(Ljava/lang/String;)I

    #@861
    move-result v16

    #@862
    .line 645
    .restart local v16       #callState:I
    const/4 v14, 0x0

    #@863
    .line 646
    .restart local v14       #bDeferredMessages:Z
    if-nez v27, :cond_a55

    #@865
    .line 647
    const/4 v14, 0x0

    #@866
    .line 652
    :goto_866
    new-instance v47, Ljava/lang/StringBuilder;

    #@868
    invoke-direct/range {v47 .. v47}, Ljava/lang/StringBuilder;-><init>()V

    #@86b
    const-string v48, "\tmessageId = "

    #@86d
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@870
    move-result-object v47

    #@871
    move-object/from16 v0, v47

    #@873
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@876
    move-result-object v47

    #@877
    const-string v48, ", senderURI = "

    #@879
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87c
    move-result-object v47

    #@87d
    move-object/from16 v0, v47

    #@87f
    move-object/from16 v1, v42

    #@881
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@884
    move-result-object v47

    #@885
    const-string v48, "\n"

    #@887
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88a
    move-result-object v47

    #@88b
    const-string v48, ", senderMSISDN = "

    #@88d
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@890
    move-result-object v47

    #@891
    move-object/from16 v0, v47

    #@893
    move-object/from16 v1, v41

    #@895
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@898
    move-result-object v47

    #@899
    const-string v48, "\n"

    #@89b
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89e
    move-result-object v47

    #@89f
    const-string v48, ", displayName = "

    #@8a1
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a4
    move-result-object v47

    #@8a5
    move-object/from16 v0, v47

    #@8a7
    move-object/from16 v1, v28

    #@8a9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8ac
    move-result-object v47

    #@8ad
    const-string v48, "\n"

    #@8af
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b2
    move-result-object v47

    #@8b3
    const-string v48, ", date time = "

    #@8b5
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b8
    move-result-object v47

    #@8b9
    move-object/from16 v0, v47

    #@8bb
    move-object/from16 v1, v26

    #@8bd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c0
    move-result-object v47

    #@8c1
    const-string v48, ", file name = "

    #@8c3
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c6
    move-result-object v47

    #@8c7
    move-object/from16 v0, v47

    #@8c9
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8cc
    move-result-object v47

    #@8cd
    const-string v48, "\n"

    #@8cf
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d2
    move-result-object v47

    #@8d3
    const-string v48, ", numOfRecipients = "

    #@8d5
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d8
    move-result-object v47

    #@8d9
    move-object/from16 v0, v47

    #@8db
    move/from16 v1, v38

    #@8dd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e0
    move-result-object v47

    #@8e1
    const-string v48, "\n"

    #@8e3
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e6
    move-result-object v47

    #@8e7
    const-string v48, ", call state = "

    #@8e9
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8ec
    move-result-object v47

    #@8ed
    move-object/from16 v0, v47

    #@8ef
    move/from16 v1, v16

    #@8f1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8f4
    move-result-object v47

    #@8f5
    const-string v48, "\n"

    #@8f7
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8fa
    move-result-object v47

    #@8fb
    const-string v48, ", file content type = "

    #@8fd
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@900
    move-result-object v47

    #@901
    move-object/from16 v0, v47

    #@903
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@906
    move-result-object v47

    #@907
    const-string v48, "\n"

    #@909
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90c
    move-result-object v47

    #@90d
    const-string v48, ", thumbnail url = "

    #@90f
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@912
    move-result-object v47

    #@913
    move-object/from16 v0, v47

    #@915
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@918
    move-result-object v47

    #@919
    const-string v48, "\n"

    #@91b
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91e
    move-result-object v47

    #@91f
    const-string v48, ", file download url = "

    #@921
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@924
    move-result-object v47

    #@925
    move-object/from16 v0, v47

    #@927
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92a
    move-result-object v47

    #@92b
    const-string v48, "\n"

    #@92d
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@930
    move-result-object v47

    #@931
    const-string v48, ", expiryDateTime = "

    #@933
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@936
    move-result-object v47

    #@937
    move-object/from16 v0, v47

    #@939
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93c
    move-result-object v47

    #@93d
    const-string v48, "\n"

    #@93f
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@942
    move-result-object v47

    #@943
    const-string v48, ", deferred messages = "

    #@945
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@948
    move-result-object v47

    #@949
    move-object/from16 v0, v47

    #@94b
    move/from16 v1, v27

    #@94d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@950
    move-result-object v47

    #@951
    const-string v48, "\n"

    #@953
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@956
    move-result-object v47

    #@957
    const-string v48, ", display notification = "

    #@959
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95c
    move-result-object v47

    #@95d
    move-object/from16 v0, v47

    #@95f
    move/from16 v1, v29

    #@961
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@964
    move-result-object v47

    #@965
    const-string v48, "\n"

    #@967
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96a
    move-result-object v47

    #@96b
    const-string v48, ", contributionId = "

    #@96d
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@970
    move-result-object v47

    #@971
    move-object/from16 v0, v47

    #@973
    move-object/from16 v1, v21

    #@975
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@978
    move-result-object v47

    #@979
    const-string v48, "\n"

    #@97b
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97e
    move-result-object v47

    #@97f
    const-string v48, ", sessionReplaces = "

    #@981
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@984
    move-result-object v47

    #@985
    move-object/from16 v0, v47

    #@987
    move-object/from16 v1, v43

    #@989
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98c
    move-result-object v47

    #@98d
    const-string v48, "\n"

    #@98f
    invoke-virtual/range {v47 .. v48}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@992
    move-result-object v47

    #@993
    invoke-virtual/range {v47 .. v47}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@996
    move-result-object v47

    #@997
    invoke-static/range {v47 .. v47}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@99a
    .line 667
    move-object/from16 v0, p0

    #@99c
    move/from16 v1, v37

    #@99e
    move-object/from16 v2, v21

    #@9a0
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/service/im/IMServiceImpl;->openChat(ILjava/lang/String;)Lcom/lge/ims/service/im/ChatImpl;

    #@9a3
    move-result-object v17

    #@9a4
    .line 669
    .restart local v17       #chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    new-instance v34, Landroid/content/Intent;

    #@9a6
    invoke-direct/range {v34 .. v34}, Landroid/content/Intent;-><init>()V

    #@9a9
    .line 670
    .restart local v34       #intent:Landroid/content/Intent;
    const-string v47, "com.lge.ims.rcsim.CONFERENCE_INVITE_RECEIVED_WITH_FILELINK"

    #@9ab
    move-object/from16 v0, v34

    #@9ad
    move-object/from16 v1, v47

    #@9af
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@9b2
    .line 672
    new-instance v31, Landroid/os/Bundle;

    #@9b4
    invoke-direct/range {v31 .. v31}, Landroid/os/Bundle;-><init>()V

    #@9b7
    .line 673
    .restart local v31       #extra:Landroid/os/Bundle;
    const-string v47, "ChatImpl"

    #@9b9
    move-object/from16 v0, v31

    #@9bb
    move-object/from16 v1, v47

    #@9bd
    move-object/from16 v2, v17

    #@9bf
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    #@9c2
    .line 674
    move-object/from16 v0, v34

    #@9c4
    move-object/from16 v1, v31

    #@9c6
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@9c9
    .line 675
    const-string v47, "callState"

    #@9cb
    move-object/from16 v0, v34

    #@9cd
    move-object/from16 v1, v47

    #@9cf
    move/from16 v2, v16

    #@9d1
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@9d4
    .line 676
    const-string v47, "remoteUserUri"

    #@9d6
    move-object/from16 v0, v34

    #@9d8
    move-object/from16 v1, v47

    #@9da
    move-object/from16 v2, v42

    #@9dc
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@9df
    .line 677
    const-string v47, "remoteUser"

    #@9e1
    move-object/from16 v0, v34

    #@9e3
    move-object/from16 v1, v47

    #@9e5
    move-object/from16 v2, v41

    #@9e7
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@9ea
    .line 678
    const-string v47, "displayName"

    #@9ec
    move-object/from16 v0, v34

    #@9ee
    move-object/from16 v1, v47

    #@9f0
    move-object/from16 v2, v28

    #@9f2
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@9f5
    .line 679
    const-string v47, "dateTime"

    #@9f7
    move-object/from16 v0, v34

    #@9f9
    move-object/from16 v1, v47

    #@9fb
    move-object/from16 v2, v26

    #@9fd
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@a00
    .line 680
    const-string v47, "participants"

    #@a02
    move-object/from16 v0, v34

    #@a04
    move-object/from16 v1, v47

    #@a06
    move-object/from16 v2, v39

    #@a08
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@a0b
    .line 681
    const-string v47, "displayNotification"

    #@a0d
    move-object/from16 v0, v34

    #@a0f
    move-object/from16 v1, v47

    #@a11
    move/from16 v2, v29

    #@a13
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@a16
    .line 682
    const-string v47, "deferredMessages"

    #@a18
    move-object/from16 v0, v34

    #@a1a
    move-object/from16 v1, v47

    #@a1c
    invoke-virtual {v0, v1, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@a1f
    .line 683
    const-string v47, "contributionId"

    #@a21
    move-object/from16 v0, v34

    #@a23
    move-object/from16 v1, v47

    #@a25
    move-object/from16 v2, v21

    #@a27
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@a2a
    .line 684
    const-string v47, "sessionReplaces"

    #@a2c
    move-object/from16 v0, v34

    #@a2e
    move-object/from16 v1, v47

    #@a30
    move-object/from16 v2, v43

    #@a32
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@a35
    .line 687
    new-instance v3, Lcom/lge/ims/service/im/IMFileInfo;

    #@a37
    invoke-direct {v3}, Lcom/lge/ims/service/im/IMFileInfo;-><init>()V

    #@a3a
    .line 688
    .restart local v3       #imFileInfo:Lcom/lge/ims/service/im/IMFileInfo;
    invoke-virtual/range {v3 .. v13}, Lcom/lge/ims/service/im/IMFileInfo;->setParamOfFileLinkIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@a3d
    .line 691
    const-string v47, "FileInfo"

    #@a3f
    move-object/from16 v0, v34

    #@a41
    move-object/from16 v1, v47

    #@a43
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@a46
    .line 693
    move-object/from16 v0, p0

    #@a48
    iget-object v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@a4a
    move-object/from16 v47, v0

    #@a4c
    move-object/from16 v0, v47

    #@a4e
    move-object/from16 v1, v34

    #@a50
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@a53
    goto/16 :goto_c

    #@a55
    .line 649
    .end local v3           #imFileInfo:Lcom/lge/ims/service/im/IMFileInfo;
    .end local v17           #chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    .end local v31           #extra:Landroid/os/Bundle;
    .end local v34           #intent:Landroid/content/Intent;
    :cond_a55
    const/4 v14, 0x1

    #@a56
    goto/16 :goto_866

    #@a58
    .line 698
    .end local v4           #messageId:Ljava/lang/String;
    .end local v5           #thumbnailName:Ljava/lang/String;
    .end local v6           #thumbnailContentType:Ljava/lang/String;
    .end local v7           #thumbnailSize:I
    .end local v8           #thumbnailUrl:Ljava/lang/String;
    .end local v9           #fileName:Ljava/lang/String;
    .end local v10           #fileContentType:Ljava/lang/String;
    .end local v11           #fileSize:I
    .end local v12           #fileDownloadUrl:Ljava/lang/String;
    .end local v13           #expiryDateTime:Ljava/lang/String;
    .end local v14           #bDeferredMessages:Z
    .end local v16           #callState:I
    .end local v21           #contributionId:Ljava/lang/String;
    .end local v26           #dateTime:Ljava/lang/String;
    .end local v27           #deferredMessages:I
    .end local v28           #displayName:Ljava/lang/String;
    .end local v29           #displayNotification:I
    .end local v37           #nativeObj:I
    .end local v38           #numOfParticipants:I
    .end local v39           #participants:[Ljava/lang/String;
    .end local v41           #senderMSISDN:Ljava/lang/String;
    .end local v42           #senderURI:Ljava/lang/String;
    .end local v43           #sessionReplaces:Ljava/lang/String;
    :pswitch_a58
    const/16 v19, 0x0

    #@a5a
    .line 700
    .local v19, conferenceEvents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/im/IMConferenceEvent;>;"
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@a5d
    move-result v37

    #@a5e
    .line 701
    .restart local v37       #nativeObj:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a61
    move-result-object v21

    #@a62
    .line 702
    .restart local v21       #contributionId:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@a65
    move-result-object v26

    #@a66
    .line 703
    .restart local v26       #dateTime:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@a69
    move-result v38

    #@a6a
    .line 705
    .restart local v38       #numOfParticipants:I
    if-lez v38, :cond_c

    #@a6c
    .line 706
    new-instance v19, Ljava/util/ArrayList;

    #@a6e
    .end local v19           #conferenceEvents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/im/IMConferenceEvent;>;"
    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    #@a71
    .line 707
    .restart local v19       #conferenceEvents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/im/IMConferenceEvent;>;"
    const/16 v18, 0x0

    #@a73
    .line 708
    .local v18, conferenceEvent:Lcom/lge/ims/service/im/IMConferenceEvent;
    const/16 v32, 0x0

    #@a75
    .restart local v32       #i:I
    :goto_a75
    move/from16 v0, v32

    #@a77
    move/from16 v1, v38

    #@a79
    if-ge v0, v1, :cond_a8e

    #@a7b
    .line 709
    new-instance v18, Lcom/lge/ims/service/im/IMConferenceEvent;

    #@a7d
    .end local v18           #conferenceEvent:Lcom/lge/ims/service/im/IMConferenceEvent;
    move-object/from16 v0, v18

    #@a7f
    move-object/from16 v1, p1

    #@a81
    invoke-direct {v0, v1}, Lcom/lge/ims/service/im/IMConferenceEvent;-><init>(Landroid/os/Parcel;)V

    #@a84
    .line 710
    .restart local v18       #conferenceEvent:Lcom/lge/ims/service/im/IMConferenceEvent;
    move-object/from16 v0, v19

    #@a86
    move-object/from16 v1, v18

    #@a88
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@a8b
    .line 708
    add-int/lit8 v32, v32, 0x1

    #@a8d
    goto :goto_a75

    #@a8e
    .line 713
    :cond_a8e
    move-object/from16 v0, p0

    #@a90
    move/from16 v1, v37

    #@a92
    move-object/from16 v2, v21

    #@a94
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/service/im/IMServiceImpl;->openChat(ILjava/lang/String;)Lcom/lge/ims/service/im/ChatImpl;

    #@a97
    move-result-object v17

    #@a98
    .line 715
    .restart local v17       #chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    new-instance v34, Landroid/content/Intent;

    #@a9a
    const-string v47, "com.lge.ims.rcsim.CONFERENCE_EVENT_RECEIVED"

    #@a9c
    move-object/from16 v0, v34

    #@a9e
    move-object/from16 v1, v47

    #@aa0
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@aa3
    .line 717
    .restart local v34       #intent:Landroid/content/Intent;
    new-instance v31, Landroid/os/Bundle;

    #@aa5
    invoke-direct/range {v31 .. v31}, Landroid/os/Bundle;-><init>()V

    #@aa8
    .line 718
    .restart local v31       #extra:Landroid/os/Bundle;
    const-string v47, "ChatImpl"

    #@aaa
    move-object/from16 v0, v31

    #@aac
    move-object/from16 v1, v47

    #@aae
    move-object/from16 v2, v17

    #@ab0
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    #@ab3
    .line 719
    move-object/from16 v0, v34

    #@ab5
    move-object/from16 v1, v31

    #@ab7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@aba
    .line 720
    const-string v47, "contributionId"

    #@abc
    move-object/from16 v0, v34

    #@abe
    move-object/from16 v1, v47

    #@ac0
    move-object/from16 v2, v21

    #@ac2
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@ac5
    .line 721
    const-string v47, "dateTime"

    #@ac7
    move-object/from16 v0, v34

    #@ac9
    move-object/from16 v1, v47

    #@acb
    move-object/from16 v2, v26

    #@acd
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@ad0
    .line 722
    const-string v47, "conferenceEvents"

    #@ad2
    move-object/from16 v0, v34

    #@ad4
    move-object/from16 v1, v47

    #@ad6
    move-object/from16 v2, v19

    #@ad8
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    #@adb
    .line 724
    move-object/from16 v0, p0

    #@add
    iget-object v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@adf
    move-object/from16 v47, v0

    #@ae1
    move-object/from16 v0, v47

    #@ae3
    move-object/from16 v1, v34

    #@ae5
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@ae8
    goto/16 :goto_c

    #@aea
    .line 730
    .end local v17           #chatImpl:Lcom/lge/ims/service/im/ChatImpl;
    .end local v18           #conferenceEvent:Lcom/lge/ims/service/im/IMConferenceEvent;
    .end local v19           #conferenceEvents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/im/IMConferenceEvent;>;"
    .end local v21           #contributionId:Ljava/lang/String;
    .end local v26           #dateTime:Ljava/lang/String;
    .end local v31           #extra:Landroid/os/Bundle;
    .end local v32           #i:I
    .end local v34           #intent:Landroid/content/Intent;
    .end local v37           #nativeObj:I
    .end local v38           #numOfParticipants:I
    :pswitch_aea
    invoke-virtual/range {p0 .. p0}, Lcom/lge/ims/service/im/IMServiceImpl;->getServiceNetworkType()I

    #@aed
    move-result v46

    #@aee
    .line 732
    .local v46, type:I
    move-object/from16 v0, p0

    #@af0
    iget v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mNetworkType:I

    #@af2
    move/from16 v47, v0

    #@af4
    move/from16 v0, v47

    #@af6
    move/from16 v1, v46

    #@af8
    if-eq v0, v1, :cond_c

    #@afa
    .line 733
    move/from16 v0, v46

    #@afc
    move-object/from16 v1, p0

    #@afe
    iput v0, v1, Lcom/lge/ims/service/im/IMServiceImpl;->mNetworkType:I

    #@b00
    .line 734
    move-object/from16 v0, p0

    #@b02
    iget v0, v0, Lcom/lge/ims/service/im/IMServiceImpl;->mNetworkType:I

    #@b04
    move/from16 v47, v0

    #@b06
    move-object/from16 v0, p0

    #@b08
    move/from16 v1, v47

    #@b0a
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/IMServiceImpl;->onAvailable(I)V
    :try_end_b0d
    .catch Ljava/lang/Exception; {:try_start_1d0 .. :try_end_b0d} :catch_1c2

    #@b0d
    goto/16 :goto_c

    #@b0f
    .line 337
    nop

    #@b10
    :pswitch_data_b10
    .packed-switch 0x2973
        :pswitch_d
        :pswitch_1d0
        :pswitch_3f8
        :pswitch_4f2
        :pswitch_565
        :pswitch_7c5
        :pswitch_a58
        :pswitch_aea
    .end packed-switch

    #@b24
    .line 498
    :pswitch_data_b24
    .packed-switch 0x0
        :pswitch_c
        :pswitch_537
        :pswitch_54e
    .end packed-switch
.end method

.method public openChat()Lcom/lge/ims/service/im/IChat;
    .registers 3

    #@0
    .prologue
    .line 82
    const-string v0, "openChat()"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 84
    iget v0, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@7
    add-int/lit8 v0, v0, 0x1

    #@9
    iput v0, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@b
    .line 86
    new-instance v0, Lcom/lge/ims/service/im/ChatImpl;

    #@d
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@f
    invoke-direct {v0, v1}, Lcom/lge/ims/service/im/ChatImpl;-><init>(Landroid/content/Context;)V

    #@12
    return-object v0
.end method

.method public openFileTransfer([Ljava/lang/String;)Lcom/lge/ims/service/im/IFileTransfer;
    .registers 5
    .parameter "recipients"

    #@0
    .prologue
    .line 122
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "openFileTransfer() : recipients = "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 124
    new-instance v0, Lcom/lge/ims/service/im/FileTransferImplFacade;

    #@18
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@1a
    invoke-direct {v0, v1, p1}, Lcom/lge/ims/service/im/FileTransferImplFacade;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    #@1d
    .line 125
    .local v0, iFileTransfer:Lcom/lge/ims/service/im/IFileTransfer;
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@1f
    add-int/lit8 v1, v1, 0x1

    #@21
    iput v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@23
    .line 127
    return-object v0
.end method

.method public openFileTransferToDownload()Lcom/lge/ims/service/im/IFileTransfer;
    .registers 3

    #@0
    .prologue
    .line 136
    const-string v1, "openFileTransfer()"

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 138
    new-instance v0, Lcom/lge/ims/service/im/FileTransferImplFacade;

    #@7
    iget-object v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@9
    invoke-direct {v0, v1}, Lcom/lge/ims/service/im/FileTransferImplFacade;-><init>(Landroid/content/Context;)V

    #@c
    .line 139
    .local v0, iFileTransfer:Lcom/lge/ims/service/im/FileTransferImplFacade;
    const-string v1, "HTTP"

    #@e
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/FileTransferImplFacade;->makeFileTransfer(Ljava/lang/String;)Lcom/lge/ims/service/im/FileTransferImpl;

    #@11
    .line 140
    iget v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@13
    add-int/lit8 v1, v1, 0x1

    #@15
    iput v1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mRefCount:I

    #@17
    .line 142
    return-object v0
.end method

.method public queryCapability(Ljava/lang/String;)V
    .registers 4
    .parameter "contact"

    #@0
    .prologue
    .line 230
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "queryCapability() : contact = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 240
    return-void
.end method

.method public queryCapability(Ljava/lang/String;Z)V
    .registers 5
    .parameter "remoteUser"
    .parameter "localFTCapa"

    #@0
    .prologue
    .line 294
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "queryCapability() : remoteUser = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, ", localFTCapa = "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@20
    .line 305
    return-void
.end method

.method public removeCapabilityObserver(Ljava/lang/String;)V
    .registers 5
    .parameter "remoteUser"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 284
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "removeCapabilityObserver() : remoteUser"

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 286
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityService;->getInstance()Lcom/lge/ims/service/cd/CapabilityService;

    #@19
    move-result-object v0

    #@1a
    .line 288
    .local v0, cs:Lcom/lge/ims/service/cd/CapabilityService;
    if-eqz v0, :cond_1f

    #@1c
    .line 289
    invoke-virtual {v0, p1, p0}, Lcom/lge/ims/service/cd/CapabilityService;->removeDedicatedListener(Ljava/lang/String;Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@1f
    .line 291
    :cond_1f
    return-void
.end method

.method public sendInvitationMessage(Ljava/lang/String;)V
    .registers 6
    .parameter "recipient"

    #@0
    .prologue
    .line 786
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "sendInvitationMessage() : recipient="

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 788
    iget-object v2, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mContext:Landroid/content/Context;

    #@18
    const v3, 0x7f0603aa

    #@1b
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 790
    .local v0, message:Ljava/lang/String;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@22
    move-result-object v1

    #@23
    .line 792
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2972

    #@25
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 793
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2b
    .line 794
    invoke-virtual {v1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@2e
    .line 796
    invoke-direct {p0, v1}, Lcom/lge/ims/service/im/IMServiceImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@31
    .line 797
    return-void
.end method

.method public sendMessageDisplayNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "recipient"
    .parameter "messageId"
    .parameter "dateTime"

    #@0
    .prologue
    .line 187
    if-eqz p1, :cond_6

    #@2
    if-eqz p2, :cond_6

    #@4
    if-nez p3, :cond_c

    #@6
    .line 190
    :cond_6
    const-string v1, "sendMessageDisplayNotification() : Invalid Parameter"

    #@8
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@b
    .line 206
    :goto_b
    return-void

    #@c
    .line 194
    :cond_c
    new-instance v1, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v2, "sendMessageDisplayNotification() : recipient = "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", messageId = "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, ", dateTime = "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@36
    .line 197
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@39
    move-result-object v0

    #@3a
    .line 199
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x2970

    #@3c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@3f
    .line 200
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@42
    .line 201
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@45
    .line 202
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@48
    .line 203
    const/4 v1, 0x2

    #@49
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@4c
    .line 205
    invoke-direct {p0, v0}, Lcom/lge/ims/service/im/IMServiceImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@4f
    goto :goto_b
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .registers 5
    .parameter "displayName"

    #@0
    .prologue
    .line 176
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "setDisplayName() : displayName = "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 178
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@19
    move-result-object v0

    #@1a
    .line 180
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x296f

    #@1c
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@1f
    .line 181
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@22
    .line 183
    invoke-direct {p0, v0}, Lcom/lge/ims/service/im/IMServiceImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@25
    .line 184
    return-void
.end method

.method public setListener(Lcom/lge/ims/service/im/IIMServiceListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 76
    const-string v0, "setListener()"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 78
    iput-object p1, p0, Lcom/lge/ims/service/im/IMServiceImpl;->mListener:Lcom/lge/ims/service/im/IIMServiceListener;

    #@7
    .line 79
    return-void
.end method

.method public subscribeCapability(Ljava/lang/String;)V
    .registers 4
    .parameter "contact"

    #@0
    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "subscribeCapability() : contact = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 253
    return-void
.end method

.method public unsubscribeCapability(Ljava/lang/String;)V
    .registers 4
    .parameter "contact"

    #@0
    .prologue
    .line 256
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "unsubscribeCapability() : contact = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@16
    .line 266
    return-void
.end method
