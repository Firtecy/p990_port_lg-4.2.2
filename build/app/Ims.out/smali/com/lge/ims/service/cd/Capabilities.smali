.class public Lcom/lge/ims/service/cd/Capabilities;
.super Ljava/lang/Object;
.source "Capabilities.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/cd/Capabilities;",
            ">;"
        }
    .end annotation
.end field

.field public static final MEDIA_CONTACTS:I = 0x100000

.field public static final MEDIA_IMAGE:I = 0x20000

.field public static final MEDIA_MAP:I = 0x80000

.field public static final MEDIA_MEMO:I = 0x200000

.field public static final MEDIA_MUSIC:I = 0x40000

.field public static final MEDIA_VIDEO:I = 0x10000

.field public static final SERVICE_FT:I = 0x1

.field public static final SERVICE_FT_HTTP:I = 0x1000

.field public static final SERVICE_IM:I = 0x2

.field public static final SERVICE_IS:I = 0x4

.field public static final SERVICE_MIM:I = 0x2000

.field public static final SERVICE_VS:I = 0x8

.field private static final TAG:Ljava/lang/String; = "LGIMS"


# instance fields
.field private mediaCapabilities:I

.field private phoneNumber:Ljava/lang/String;

.field private serviceCapabilities:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 83
    new-instance v0, Lcom/lge/ims/service/cd/Capabilities$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/cd/Capabilities$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/cd/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 46
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/lge/ims/service/cd/Capabilities;->phoneNumber:Ljava/lang/String;

    #@7
    .line 47
    iput v1, p0, Lcom/lge/ims/service/cd/Capabilities;->serviceCapabilities:I

    #@9
    .line 48
    iput v1, p0, Lcom/lge/ims/service/cd/Capabilities;->mediaCapabilities:I

    #@b
    .line 59
    const-string v0, "LGIMS"

    #@d
    const-string v1, "[Capabilities] Created"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 60
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/cd/Capabilities;->readFromParcel(Landroid/os/Parcel;)V

    #@15
    .line 61
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .registers 6
    .parameter "phoneNumber"
    .parameter "serviceCapabilities"
    .parameter "mediaCapabilities"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 52
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 46
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/lge/ims/service/cd/Capabilities;->phoneNumber:Ljava/lang/String;

    #@7
    .line 47
    iput v1, p0, Lcom/lge/ims/service/cd/Capabilities;->serviceCapabilities:I

    #@9
    .line 48
    iput v1, p0, Lcom/lge/ims/service/cd/Capabilities;->mediaCapabilities:I

    #@b
    .line 53
    iput-object p1, p0, Lcom/lge/ims/service/cd/Capabilities;->phoneNumber:Ljava/lang/String;

    #@d
    .line 54
    iput p2, p0, Lcom/lge/ims/service/cd/Capabilities;->serviceCapabilities:I

    #@f
    .line 55
    iput p3, p0, Lcom/lge/ims/service/cd/Capabilities;->mediaCapabilities:I

    #@11
    .line 56
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 80
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getPeerMSISDN()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 102
    iget-object v0, p0, Lcom/lge/ims/service/cd/Capabilities;->phoneNumber:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public isMediaSupported(I)Z
    .registers 3
    .parameter "media"

    #@0
    .prologue
    .line 107
    iget v0, p0, Lcom/lge/ims/service/cd/Capabilities;->mediaCapabilities:I

    #@2
    and-int/2addr v0, p1

    #@3
    if-eqz v0, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public isServiceSupported(I)Z
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 112
    iget v0, p0, Lcom/lge/ims/service/cd/Capabilities;->serviceCapabilities:I

    #@2
    and-int/2addr v0, p1

    #@3
    if-eqz v0, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/lge/ims/service/cd/Capabilities;->phoneNumber:Ljava/lang/String;

    #@6
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Lcom/lge/ims/service/cd/Capabilities;->serviceCapabilities:I

    #@c
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Lcom/lge/ims/service/cd/Capabilities;->mediaCapabilities:I

    #@12
    .line 68
    const-string v0, "LGIMS"

    #@14
    new-instance v1, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "[Capabilities] phoneNumber : "

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    iget-object v2, p0, Lcom/lge/ims/service/cd/Capabilities;->phoneNumber:Ljava/lang/String;

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, " serviceCapabilities : "

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    iget v2, p0, Lcom/lge/ims/service/cd/Capabilities;->serviceCapabilities:I

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string v2, " mediaCapabilities : "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    iget v2, p0, Lcom/lge/ims/service/cd/Capabilities;->mediaCapabilities:I

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v1

    #@41
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 71
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Lcom/lge/ims/service/cd/Capabilities;->phoneNumber:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 75
    iget v0, p0, Lcom/lge/ims/service/cd/Capabilities;->serviceCapabilities:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 76
    iget v0, p0, Lcom/lge/ims/service/cd/Capabilities;->mediaCapabilities:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 77
    return-void
.end method
