.class public abstract Lcom/lge/ims/service/im/IFileTransferListener$Stub;
.super Landroid/os/Binder;
.source "IFileTransferListener.java"

# interfaces
.implements Lcom/lge/ims/service/im/IFileTransferListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/im/IFileTransferListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/im/IFileTransferListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.im.IFileTransferListener"

.field static final TRANSACTION_fileReceived:I = 0x3

.field static final TRANSACTION_fileSent:I = 0x4

.field static final TRANSACTION_transferFailed:I = 0x6

.field static final TRANSACTION_transferProgress:I = 0x7

.field static final TRANSACTION_transferStartFailed:I = 0x2

.field static final TRANSACTION_transferStarted:I = 0x1

.field static final TRANSACTION_transferTerminated:I = 0x5


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.im.IFileTransferListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/im/IFileTransferListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IFileTransferListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.im.IFileTransferListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/im/IFileTransferListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/im/IFileTransferListener;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/im/IFileTransferListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/im/IFileTransferListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 8
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_78

    #@4
    .line 105
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v1

    #@8
    :goto_8
    return v1

    #@9
    .line 42
    :sswitch_9
    const-string v2, "com.lge.ims.service.im.IFileTransferListener"

    #@b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v2, "com.lge.ims.service.im.IFileTransferListener"

    #@11
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 48
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IFileTransferListener$Stub;->transferStarted()V

    #@17
    .line 49
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1a
    goto :goto_8

    #@1b
    .line 54
    :sswitch_1b
    const-string v2, "com.lge.ims.service.im.IFileTransferListener"

    #@1d
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20
    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@23
    move-result v0

    #@24
    .line 57
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IFileTransferListener$Stub;->transferStartFailed(I)V

    #@27
    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2a
    goto :goto_8

    #@2b
    .line 63
    .end local v0           #_arg0:I
    :sswitch_2b
    const-string v2, "com.lge.ims.service.im.IFileTransferListener"

    #@2d
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@30
    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    .line 66
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IFileTransferListener$Stub;->fileReceived(Ljava/lang/String;)V

    #@37
    .line 67
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3a
    goto :goto_8

    #@3b
    .line 72
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_3b
    const-string v2, "com.lge.ims.service.im.IFileTransferListener"

    #@3d
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@43
    move-result-object v0

    #@44
    .line 75
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IFileTransferListener$Stub;->fileSent(Ljava/lang/String;)V

    #@47
    .line 76
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4a
    goto :goto_8

    #@4b
    .line 81
    .end local v0           #_arg0:Ljava/lang/String;
    :sswitch_4b
    const-string v2, "com.lge.ims.service.im.IFileTransferListener"

    #@4d
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@50
    .line 82
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IFileTransferListener$Stub;->transferTerminated()V

    #@53
    .line 83
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@56
    goto :goto_8

    #@57
    .line 88
    :sswitch_57
    const-string v2, "com.lge.ims.service.im.IFileTransferListener"

    #@59
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@5c
    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@5f
    move-result v0

    #@60
    .line 91
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IFileTransferListener$Stub;->transferFailed(I)V

    #@63
    .line 92
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@66
    goto :goto_8

    #@67
    .line 97
    .end local v0           #_arg0:I
    :sswitch_67
    const-string v2, "com.lge.ims.service.im.IFileTransferListener"

    #@69
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@6c
    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v0

    #@70
    .line 100
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/IFileTransferListener$Stub;->transferProgress(I)V

    #@73
    .line 101
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@76
    goto :goto_8

    #@77
    .line 38
    nop

    #@78
    :sswitch_data_78
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_1b
        0x3 -> :sswitch_2b
        0x4 -> :sswitch_3b
        0x5 -> :sswitch_4b
        0x6 -> :sswitch_57
        0x7 -> :sswitch_67
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
