.class public Lcom/lge/ims/service/ac/content/ACVersion;
.super Ljava/lang/Object;
.source "ACVersion.java"


# instance fields
.field public mValidity:Ljava/lang/String;

.field public mVersion:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 17
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 12
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACVersion;->mVersion:Ljava/lang/String;

    #@6
    .line 13
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACVersion;->mValidity:Ljava/lang/String;

    #@8
    .line 18
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 21
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACVersion;->mVersion:Ljava/lang/String;

    #@3
    .line 22
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACVersion;->mValidity:Ljava/lang/String;

    #@5
    .line 23
    return-void
.end method

.method public isContentPresent()Z
    .registers 2

    #@0
    .prologue
    .line 26
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACVersion;->mVersion:Ljava/lang/String;

    #@2
    if-nez v0, :cond_8

    #@4
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACVersion;->mValidity:Ljava/lang/String;

    #@6
    if-eqz v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 30
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "version="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACVersion;->mVersion:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", validity="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACVersion;->mValidity:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    return-object v0
.end method
