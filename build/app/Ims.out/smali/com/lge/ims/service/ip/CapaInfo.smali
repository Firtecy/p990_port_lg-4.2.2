.class public Lcom/lge/ims/service/ip/CapaInfo;
.super Ljava/lang/Object;
.source "CapaInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CAPAINFO_CAPA_TYPE_POLLING:I = 0x1

.field public static final CAPAINFO_CAPA_TYPE_SYNC:I = 0x0

.field public static final CAPAINFO_USER_INVALID:I = -0x1

.field public static final CAPAINFO_USER_MAX:I = 0x6

.field public static final CAPAINFO_USER_RCSE:I = 0x1

.field public static final CAPAINFO_USER_TEMP:I = 0x4

.field public static final CAPAINFO_USER_UNKNOWN:I

.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/ip/CapaInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private nCapaType:I

.field private nDiscoveryPresenceStatus:I

.field private nFTStatus:I

.field private nHttpStatus:I

.field private nIMStatus:I

.field private nMIMStatus:I

.field private nPresenceStatus:I

.field private nRCSUserType:I

.field private nResponseCode:I

.field private strMSISDN:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 40
    new-instance v0, Lcom/lge/ims/service/ip/CapaInfo$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/ip/CapaInfo$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/ip/CapaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 56
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->strMSISDN:Ljava/lang/String;

    #@7
    .line 57
    iput v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nPresenceStatus:I

    #@9
    .line 58
    iput v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nIMStatus:I

    #@b
    .line 59
    iput v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nFTStatus:I

    #@d
    .line 60
    iput v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nCapaType:I

    #@f
    .line 61
    iput v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nMIMStatus:I

    #@11
    .line 62
    iput v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nHttpStatus:I

    #@13
    .line 63
    iput v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nDiscoveryPresenceStatus:I

    #@15
    .line 64
    iput v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nResponseCode:I

    #@17
    .line 65
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 67
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 68
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/ip/CapaInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@6
    .line 69
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/lge/ims/service/ip/CapaInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/CapaInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public GetCapaType()I
    .registers 2

    #@0
    .prologue
    .line 150
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nCapaType:I

    #@2
    return v0
.end method

.method public GetDiscoveryPresenceCapa()I
    .registers 2

    #@0
    .prologue
    .line 118
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nDiscoveryPresenceStatus:I

    #@2
    return v0
.end method

.method public GetFTCapa()I
    .registers 2

    #@0
    .prologue
    .line 134
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nFTStatus:I

    #@2
    return v0
.end method

.method public GetHTTPCapa()I
    .registers 2

    #@0
    .prologue
    .line 158
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nHttpStatus:I

    #@2
    return v0
.end method

.method public GetIMCapa()I
    .registers 2

    #@0
    .prologue
    .line 126
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nIMStatus:I

    #@2
    return v0
.end method

.method public GetMIMCapa()I
    .registers 2

    #@0
    .prologue
    .line 142
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nMIMStatus:I

    #@2
    return v0
.end method

.method public GetMSISDN()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 98
    iget-object v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->strMSISDN:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetPresenceCapa()I
    .registers 2

    #@0
    .prologue
    .line 110
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nPresenceStatus:I

    #@2
    return v0
.end method

.method public GetResponseCode()I
    .registers 2

    #@0
    .prologue
    .line 166
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nResponseCode:I

    #@2
    return v0
.end method

.method public IsInvalid()Z
    .registers 2

    #@0
    .prologue
    .line 106
    iget-object v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->strMSISDN:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->strMSISDN:Ljava/lang/String;

    #@6
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public SetCapaType(I)V
    .registers 2
    .parameter "_nCapaType"

    #@0
    .prologue
    .line 154
    iput p1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nCapaType:I

    #@2
    .line 155
    return-void
.end method

.method public SetDiscoveryPresenceCapa(I)V
    .registers 2
    .parameter "_bHasnDiscoveryPresenceCapa"

    #@0
    .prologue
    .line 122
    iput p1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nDiscoveryPresenceStatus:I

    #@2
    .line 123
    return-void
.end method

.method public SetFTCapa(I)V
    .registers 2
    .parameter "_bHasFTCapa"

    #@0
    .prologue
    .line 138
    iput p1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nFTStatus:I

    #@2
    .line 139
    return-void
.end method

.method public SetHTTPCapa(I)V
    .registers 2
    .parameter "_nHttpStatus"

    #@0
    .prologue
    .line 162
    iput p1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nHttpStatus:I

    #@2
    .line 163
    return-void
.end method

.method public SetIMCapa(I)V
    .registers 2
    .parameter "_bHasIMCapa"

    #@0
    .prologue
    .line 130
    iput p1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nIMStatus:I

    #@2
    .line 131
    return-void
.end method

.method public SetMIMCapa(I)V
    .registers 2
    .parameter "_bHasMIMCapa"

    #@0
    .prologue
    .line 146
    iput p1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nMIMStatus:I

    #@2
    .line 147
    return-void
.end method

.method public SetMSISDN(Ljava/lang/String;)V
    .registers 2
    .parameter "_strMSISDN"

    #@0
    .prologue
    .line 102
    iput-object p1, p0, Lcom/lge/ims/service/ip/CapaInfo;->strMSISDN:Ljava/lang/String;

    #@2
    .line 103
    return-void
.end method

.method public SetPresenceCapa(I)V
    .registers 2
    .parameter "_bHasPresenceCapa"

    #@0
    .prologue
    .line 114
    iput p1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nPresenceStatus:I

    #@2
    .line 115
    return-void
.end method

.method public SetResponseCode(I)V
    .registers 2
    .parameter "_nResponseCode"

    #@0
    .prologue
    .line 170
    iput p1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nResponseCode:I

    #@2
    .line 171
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 72
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->strMSISDN:Ljava/lang/String;

    #@6
    .line 88
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@9
    move-result v0

    #@a
    iput v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nPresenceStatus:I

    #@c
    .line 89
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@f
    move-result v0

    #@10
    iput v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nDiscoveryPresenceStatus:I

    #@12
    .line 90
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@15
    move-result v0

    #@16
    iput v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nIMStatus:I

    #@18
    .line 91
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v0

    #@1c
    iput v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nFTStatus:I

    #@1e
    .line 92
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@21
    move-result v0

    #@22
    iput v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nMIMStatus:I

    #@24
    .line 93
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@27
    move-result v0

    #@28
    iput v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nHttpStatus:I

    #@2a
    .line 94
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v0

    #@2e
    iput v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nResponseCode:I

    #@30
    .line 95
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "CapaInfo : MSISDN["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->strMSISDN:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "], Status : Presence["

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nPresenceStatus:I

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, "], IM["

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nIMStatus:I

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, "], FT["

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nFTStatus:I

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, "], HTTP["

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    iget v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nHttpStatus:I

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    const-string v1, "], DiscoveryPresence ["

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v0

    #@47
    iget v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nDiscoveryPresenceStatus:I

    #@49
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v0

    #@4d
    const-string v1, "], RCS : ResponseCode["

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    iget v1, p0, Lcom/lge/ims/service/ip/CapaInfo;->nResponseCode:I

    #@55
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v0

    #@59
    const-string v1, "]"

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v0

    #@63
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->strMSISDN:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 77
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nPresenceStatus:I

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@a
    .line 78
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nDiscoveryPresenceStatus:I

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@f
    .line 79
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nIMStatus:I

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 80
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nFTStatus:I

    #@16
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 81
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nMIMStatus:I

    #@1b
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@1e
    .line 82
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nHttpStatus:I

    #@20
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 83
    iget v0, p0, Lcom/lge/ims/service/ip/CapaInfo;->nResponseCode:I

    #@25
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@28
    .line 84
    return-void
.end method
