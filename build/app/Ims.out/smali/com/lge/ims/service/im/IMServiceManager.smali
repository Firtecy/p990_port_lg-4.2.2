.class public Lcom/lge/ims/service/im/IMServiceManager;
.super Ljava/lang/Object;
.source "IMServiceManager.java"


# static fields
.field private static imSvcMgr:Lcom/lge/ims/service/im/IMServiceManager;


# instance fields
.field private imSvc:Lcom/lge/ims/service/im/IMServiceImpl;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 9
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/im/IMServiceManager;->imSvcMgr:Lcom/lge/ims/service/im/IMServiceManager;

    #@3
    return-void
.end method

.method private constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 12
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 10
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/lge/ims/service/im/IMServiceManager;->imSvc:Lcom/lge/ims/service/im/IMServiceImpl;

    #@6
    .line 13
    const-string v0, ""

    #@8
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@b
    .line 14
    return-void
.end method

.method public static getInstance()Lcom/lge/ims/service/im/IMServiceManager;
    .registers 1

    #@0
    .prologue
    .line 17
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 19
    sget-object v0, Lcom/lge/ims/service/im/IMServiceManager;->imSvcMgr:Lcom/lge/ims/service/im/IMServiceManager;

    #@7
    if-nez v0, :cond_10

    #@9
    .line 20
    new-instance v0, Lcom/lge/ims/service/im/IMServiceManager;

    #@b
    invoke-direct {v0}, Lcom/lge/ims/service/im/IMServiceManager;-><init>()V

    #@e
    sput-object v0, Lcom/lge/ims/service/im/IMServiceManager;->imSvcMgr:Lcom/lge/ims/service/im/IMServiceManager;

    #@10
    .line 23
    :cond_10
    sget-object v0, Lcom/lge/ims/service/im/IMServiceManager;->imSvcMgr:Lcom/lge/ims/service/im/IMServiceManager;

    #@12
    return-object v0
.end method


# virtual methods
.method public getService()Lcom/lge/ims/service/im/IMServiceImpl;
    .registers 2

    #@0
    .prologue
    .line 44
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 46
    iget-object v0, p0, Lcom/lge/ims/service/im/IMServiceManager;->imSvc:Lcom/lge/ims/service/im/IMServiceImpl;

    #@7
    return-object v0
.end method

.method public resetService()V
    .registers 3

    #@0
    .prologue
    .line 50
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 52
    iget-object v0, p0, Lcom/lge/ims/service/im/IMServiceManager;->imSvc:Lcom/lge/ims/service/im/IMServiceImpl;

    #@7
    if-eqz v0, :cond_f

    #@9
    .line 53
    iget-object v0, p0, Lcom/lge/ims/service/im/IMServiceManager;->imSvc:Lcom/lge/ims/service/im/IMServiceImpl;

    #@b
    const/4 v1, 0x0

    #@c
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/im/IMServiceImpl;->setListener(Lcom/lge/ims/service/im/IIMServiceListener;)V

    #@f
    .line 55
    :cond_f
    return-void
.end method

.method public startService(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 27
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 29
    iget-object v0, p0, Lcom/lge/ims/service/im/IMServiceManager;->imSvc:Lcom/lge/ims/service/im/IMServiceImpl;

    #@7
    if-nez v0, :cond_10

    #@9
    .line 30
    new-instance v0, Lcom/lge/ims/service/im/IMServiceImpl;

    #@b
    invoke-direct {v0, p1}, Lcom/lge/ims/service/im/IMServiceImpl;-><init>(Landroid/content/Context;)V

    #@e
    iput-object v0, p0, Lcom/lge/ims/service/im/IMServiceManager;->imSvc:Lcom/lge/ims/service/im/IMServiceImpl;

    #@10
    .line 32
    :cond_10
    return-void
.end method

.method public stopService()V
    .registers 2

    #@0
    .prologue
    .line 35
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 37
    iget-object v0, p0, Lcom/lge/ims/service/im/IMServiceManager;->imSvc:Lcom/lge/ims/service/im/IMServiceImpl;

    #@7
    if-eqz v0, :cond_11

    #@9
    .line 38
    iget-object v0, p0, Lcom/lge/ims/service/im/IMServiceManager;->imSvc:Lcom/lge/ims/service/im/IMServiceImpl;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/im/IMServiceImpl;->destroy()V

    #@e
    .line 39
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Lcom/lge/ims/service/im/IMServiceManager;->imSvc:Lcom/lge/ims/service/im/IMServiceImpl;

    #@11
    .line 41
    :cond_11
    return-void
.end method
