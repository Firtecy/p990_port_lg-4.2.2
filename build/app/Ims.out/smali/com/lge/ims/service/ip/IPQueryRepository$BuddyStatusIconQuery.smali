.class Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;
.super Ljava/lang/Object;
.source "IPQueryRepository.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ip/IPQueryRepository;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BuddyStatusIconQuery"
.end annotation


# instance fields
.field private strMSISDN:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/ims/service/ip/IPQueryRepository;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/ip/IPQueryRepository;Ljava/lang/String;)V
    .registers 4
    .parameter
    .parameter "_strMSISDN"

    #@0
    .prologue
    .line 170
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;->this$0:Lcom/lge/ims/service/ip/IPQueryRepository;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 171
    if-eqz p2, :cond_d

    #@7
    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_13

    #@d
    .line 172
    :cond_d
    const-string v0, "[IP][ERROR]MSISDN is null"

    #@f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@12
    .line 176
    :goto_12
    return-void

    #@13
    .line 175
    :cond_13
    iput-object p2, p0, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;->strMSISDN:Ljava/lang/String;

    #@15
    goto :goto_12
.end method


# virtual methods
.method public GetMSISDN()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 179
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;->strMSISDN:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public SetMSISDN(Ljava/lang/String;)V
    .registers 2
    .parameter "_strMSISDN"

    #@0
    .prologue
    .line 183
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPQueryRepository$BuddyStatusIconQuery;->strMSISDN:Ljava/lang/String;

    #@2
    .line 184
    return-void
.end method
