.class public Lcom/lge/ims/service/uc/IUUCSession$Reject_Reason;
.super Ljava/lang/Object;
.source "IUUCSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUUCSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Reject_Reason"
.end annotation


# static fields
.field public static final REJECT_REASON_BUSY_ALERTING:I = 0x7b

.field public static final REJECT_REASON_BUSY_ISCSCALL:I = 0x78

.field public static final REJECT_REASON_BUSY_ISVOIPCALL:I = 0x79

.field public static final REJECT_REASON_BUSY_ISVTCALL:I = 0x7a

.field public static final REJECT_REASON_BUSY_MAXCALL:I = 0x7c

.field public static final REJECT_REASON_BUSY_NORMAL:I = 0x7d

.field public static final REJECT_REASON_DECLINE_NOANSWER:I = 0x65

.field public static final REJECT_REASON_DECLINE_NOBATTERY:I = 0x66

.field public static final REJECT_REASON_DECLINE_NORMAL:I = 0x67

.field public static final REJECT_REASON_DECLINE_USER:I = 0x64

.field public static final REJECT_REASON_MEDIA_FAIL:I = 0xa3

.field public static final REJECT_REASON_MEDIA_FORMFAIL:I = 0xa1

.field public static final REJECT_REASON_MEDIA_NEGOFAIL:I = 0xa0

.field public static final REJECT_REASON_MEDIA_NODATA:I = 0xa2

.field public static final REJECT_REASON_SESSION_FAIL:I = 0x8e

.field public static final REJECT_REASON_SESSION_FAIL_PRECONDITION:I = 0x8f

.field public static final REJECT_REASON_SESSION_NOTACCEPTABLE:I = 0x8d

.field public static final REJECT_REASON_SESSION_NOTSUPPORT:I = 0x8c

.field public static final REJECT_REASON_UNKNOWN:I


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/uc/IUUCSession;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/uc/IUUCSession;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 162
    iput-object p1, p0, Lcom/lge/ims/service/uc/IUUCSession$Reject_Reason;->this$0:Lcom/lge/ims/service/uc/IUUCSession;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method
