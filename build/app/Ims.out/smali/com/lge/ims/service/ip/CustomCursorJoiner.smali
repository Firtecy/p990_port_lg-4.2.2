.class public Lcom/lge/ims/service/ip/CustomCursorJoiner;
.super Ljava/lang/Object;
.source "CustomCursorJoiner.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/ip/CustomCursorJoiner$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Landroid/database/CursorJoiner$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private alResult:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/database/CursorJoiner$Result;",
            ">;"
        }
    .end annotation
.end field

.field private leftCol:[Ljava/lang/String;

.field private leftCur:Landroid/database/Cursor;

.field private rightCol:[Ljava/lang/String;

.field private rightCur:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;[Ljava/lang/String;Landroid/database/Cursor;[Ljava/lang/String;)V
    .registers 6
    .parameter "lCur"
    .parameter "lCol"
    .parameter "rCur"
    .parameter "rCol"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 41
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@6
    .line 42
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@8
    .line 43
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCol:[Ljava/lang/String;

    #@a
    .line 44
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCol:[Ljava/lang/String;

    #@c
    .line 45
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->alResult:Ljava/util/ArrayList;

    #@e
    .line 48
    iput-object p1, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@10
    .line 49
    iput-object p2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCol:[Ljava/lang/String;

    #@12
    .line 50
    iput-object p3, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@14
    .line 51
    iput-object p4, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCol:[Ljava/lang/String;

    #@16
    .line 53
    new-instance v0, Ljava/util/ArrayList;

    #@18
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->alResult:Ljava/util/ArrayList;

    #@1d
    .line 55
    invoke-direct {p0}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->makeJoinList()Z

    #@20
    .line 56
    return-void
.end method

.method private compareCursor()V
    .registers 11

    #@0
    .prologue
    .line 114
    const/4 v1, -0x1

    #@1
    .line 115
    .local v1, iLJoinColIndex:I
    const/4 v4, -0x1

    #@2
    .line 116
    .local v4, iRJoinColIndex:I
    const-wide/16 v2, -0x1

    #@4
    .line 117
    .local v2, iLValue:J
    const-wide/16 v5, -0x1

    #@6
    .line 120
    .local v5, iRValue:J
    :try_start_6
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@8
    iget-object v8, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCol:[Ljava/lang/String;

    #@a
    const/4 v9, 0x0

    #@b
    aget-object v8, v8, v9

    #@d
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@10
    move-result v1

    #@11
    .line 121
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@13
    iget-object v8, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCol:[Ljava/lang/String;

    #@15
    const/4 v9, 0x0

    #@16
    aget-object v8, v8, v9

    #@18
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@1b
    move-result v4

    #@1c
    .line 123
    :goto_1c
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@1e
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    #@21
    move-result v7

    #@22
    if-nez v7, :cond_8d

    #@24
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@26
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    #@29
    move-result v7

    #@2a
    if-nez v7, :cond_8d

    #@2c
    .line 124
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@2e
    invoke-interface {v7, v1}, Landroid/database/Cursor;->getLong(I)J

    #@31
    move-result-wide v2

    #@32
    .line 125
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@34
    invoke-interface {v7, v4}, Landroid/database/Cursor;->getLong(I)J

    #@37
    move-result-wide v5

    #@38
    .line 127
    cmp-long v7, v2, v5

    #@3a
    if-gez v7, :cond_6a

    #@3c
    .line 128
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->alResult:Ljava/util/ArrayList;

    #@3e
    sget-object v8, Landroid/database/CursorJoiner$Result;->LEFT:Landroid/database/CursorJoiner$Result;

    #@40
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@43
    .line 129
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@45
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_48
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_48} :catch_49

    #@48
    goto :goto_1c

    #@49
    .line 150
    :catch_49
    move-exception v0

    #@4a
    .line 151
    .local v0, ex:Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v8, "Exception Msg: "

    #@51
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@58
    move-result-object v8

    #@59
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v7

    #@5d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v7

    #@61
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@64
    .line 152
    const-string v7, "Exception: "

    #@66
    invoke-static {v7, v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@69
    .line 153
    .end local v0           #ex:Ljava/lang/Exception;
    :cond_69
    :goto_69
    return-void

    #@6a
    .line 130
    :cond_6a
    cmp-long v7, v2, v5

    #@6c
    if-lez v7, :cond_7b

    #@6e
    .line 131
    :try_start_6e
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->alResult:Ljava/util/ArrayList;

    #@70
    sget-object v8, Landroid/database/CursorJoiner$Result;->RIGHT:Landroid/database/CursorJoiner$Result;

    #@72
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@75
    .line 132
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@77
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@7a
    goto :goto_1c

    #@7b
    .line 134
    :cond_7b
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->alResult:Ljava/util/ArrayList;

    #@7d
    sget-object v8, Landroid/database/CursorJoiner$Result;->BOTH:Landroid/database/CursorJoiner$Result;

    #@7f
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@82
    .line 135
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@84
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@87
    .line 136
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@89
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@8c
    goto :goto_1c

    #@8d
    .line 141
    :cond_8d
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@8f
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    #@92
    move-result v7

    #@93
    if-nez v7, :cond_9d

    #@95
    .line 142
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@97
    sget-object v8, Landroid/database/CursorJoiner$Result;->LEFT:Landroid/database/CursorJoiner$Result;

    #@99
    invoke-direct {p0, v7, v8}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->makeListAllRowsOfCursor(Landroid/database/Cursor;Landroid/database/CursorJoiner$Result;)V

    #@9c
    goto :goto_69

    #@9d
    .line 143
    :cond_9d
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@9f
    invoke-interface {v7}, Landroid/database/Cursor;->isAfterLast()Z

    #@a2
    move-result v7

    #@a3
    if-nez v7, :cond_69

    #@a5
    .line 144
    iget-object v7, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@a7
    sget-object v8, Landroid/database/CursorJoiner$Result;->RIGHT:Landroid/database/CursorJoiner$Result;

    #@a9
    invoke-direct {p0, v7, v8}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->makeListAllRowsOfCursor(Landroid/database/Cursor;Landroid/database/CursorJoiner$Result;)V
    :try_end_ac
    .catch Ljava/lang/Exception; {:try_start_6e .. :try_end_ac} :catch_49

    #@ac
    goto :goto_69
.end method

.method private makeJoinList()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 60
    :try_start_1
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@3
    if-eqz v2, :cond_9

    #@5
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@7
    if-nez v2, :cond_21

    #@9
    .line 61
    :cond_9
    const-string v2, "Left or Right Cursor is NULL"

    #@b
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_9f
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_e} :catch_5e

    #@e
    .line 83
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@10
    if-eqz v2, :cond_17

    #@12
    .line 84
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@14
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@17
    .line 87
    :cond_17
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@19
    if-eqz v2, :cond_20

    #@1b
    .line 88
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@1d
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@20
    :cond_20
    :goto_20
    return v1

    #@21
    .line 65
    :cond_21
    :try_start_21
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@23
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    #@26
    move-result v2

    #@27
    if-nez v2, :cond_49

    #@29
    .line 66
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@2b
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@2e
    .line 67
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@30
    sget-object v3, Landroid/database/CursorJoiner$Result;->RIGHT:Landroid/database/CursorJoiner$Result;

    #@32
    invoke-direct {p0, v2, v3}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->makeListAllRowsOfCursor(Landroid/database/Cursor;Landroid/database/CursorJoiner$Result;)V
    :try_end_35
    .catchall {:try_start_21 .. :try_end_35} :catchall_9f
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_35} :catch_5e

    #@35
    .line 77
    :goto_35
    const/4 v1, 0x1

    #@36
    .line 83
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@38
    if-eqz v2, :cond_3f

    #@3a
    .line 84
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@3c
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@3f
    .line 87
    :cond_3f
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@41
    if-eqz v2, :cond_20

    #@43
    .line 88
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@45
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@48
    goto :goto_20

    #@49
    .line 68
    :cond_49
    :try_start_49
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@4b
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    #@4e
    move-result v2

    #@4f
    if-nez v2, :cond_91

    #@51
    .line 69
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@53
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@56
    .line 70
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@58
    sget-object v3, Landroid/database/CursorJoiner$Result;->LEFT:Landroid/database/CursorJoiner$Result;

    #@5a
    invoke-direct {p0, v2, v3}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->makeListAllRowsOfCursor(Landroid/database/Cursor;Landroid/database/CursorJoiner$Result;)V
    :try_end_5d
    .catchall {:try_start_49 .. :try_end_5d} :catchall_9f
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_5d} :catch_5e

    #@5d
    goto :goto_35

    #@5e
    .line 78
    :catch_5e
    move-exception v0

    #@5f
    .line 79
    .local v0, ex:Ljava/lang/Exception;
    :try_start_5f
    new-instance v2, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v3, "Exception Msg: "

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v2

    #@72
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v2

    #@76
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@79
    .line 80
    const-string v2, "Exception: "

    #@7b
    invoke-static {v2, v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7e
    .catchall {:try_start_5f .. :try_end_7e} :catchall_9f

    #@7e
    .line 83
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@80
    if-eqz v2, :cond_87

    #@82
    .line 84
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@84
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@87
    .line 87
    :cond_87
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@89
    if-eqz v2, :cond_20

    #@8b
    .line 88
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@8d
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@90
    goto :goto_20

    #@91
    .line 72
    .end local v0           #ex:Ljava/lang/Exception;
    :cond_91
    :try_start_91
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@93
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@96
    .line 73
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@98
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@9b
    .line 74
    invoke-direct {p0}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->compareCursor()V
    :try_end_9e
    .catchall {:try_start_91 .. :try_end_9e} :catchall_9f
    .catch Ljava/lang/Exception; {:try_start_91 .. :try_end_9e} :catch_5e

    #@9e
    goto :goto_35

    #@9f
    .line 83
    :catchall_9f
    move-exception v1

    #@a0
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@a2
    if-eqz v2, :cond_a9

    #@a4
    .line 84
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@a6
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@a9
    .line 87
    :cond_a9
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@ab
    if-eqz v2, :cond_b2

    #@ad
    .line 88
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@af
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@b2
    :cond_b2
    throw v1
.end method

.method private makeListAllRowsOfCursor(Landroid/database/Cursor;Landroid/database/CursorJoiner$Result;)V
    .registers 6
    .parameter "cur"
    .parameter "enumResult"

    #@0
    .prologue
    .line 96
    if-eqz p1, :cond_8

    #@2
    :try_start_2
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_9

    #@8
    .line 110
    :cond_8
    :goto_8
    return-void

    #@9
    .line 100
    :cond_9
    :goto_9
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    #@c
    move-result v1

    #@d
    if-nez v1, :cond_8

    #@f
    .line 101
    iget-object v1, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->alResult:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@14
    .line 102
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_17
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_17} :catch_18

    #@17
    goto :goto_9

    #@18
    .line 106
    :catch_18
    move-exception v0

    #@19
    .line 107
    .local v0, ex:Ljava/lang/Exception;
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v2, "Exception Msg: "

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@33
    .line 108
    const-string v1, "Exception: "

    #@35
    invoke-static {v1, v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    #@38
    goto :goto_8
.end method


# virtual methods
.method public getLeftCursor()Landroid/database/Cursor;
    .registers 2

    #@0
    .prologue
    .line 158
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->leftCur:Landroid/database/Cursor;

    #@2
    return-object v0
.end method

.method public getLength()I
    .registers 2

    #@0
    .prologue
    .line 166
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->alResult:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getResultAt(I)Landroid/database/CursorJoiner$Result;
    .registers 3
    .parameter "index"

    #@0
    .prologue
    .line 170
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->alResult:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/database/CursorJoiner$Result;

    #@8
    return-object v0
.end method

.method public getRightCursor()Landroid/database/Cursor;
    .registers 2

    #@0
    .prologue
    .line 162
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->rightCur:Landroid/database/Cursor;

    #@2
    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Landroid/database/CursorJoiner$Result;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 191
    new-instance v0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;

    #@2
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;-><init>(Lcom/lge/ims/service/ip/CustomCursorJoiner;)V

    #@5
    return-object v0
.end method

.method public printAllJoinList()V
    .registers 5

    #@0
    .prologue
    .line 174
    iget-object v2, p0, Lcom/lge/ims/service/ip/CustomCursorJoiner;->alResult:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v0

    #@6
    .local v0, i$:Ljava/util/Iterator;
    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_30

    #@c
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Landroid/database/CursorJoiner$Result;

    #@12
    .line 175
    .local v1, temp:Landroid/database/CursorJoiner$Result;
    sget-object v2, Lcom/lge/ims/service/ip/CustomCursorJoiner$1;->$SwitchMap$android$database$CursorJoiner$Result:[I

    #@14
    invoke-virtual {v1}, Landroid/database/CursorJoiner$Result;->ordinal()I

    #@17
    move-result v3

    #@18
    aget v2, v2, v3

    #@1a
    packed-switch v2, :pswitch_data_32

    #@1d
    goto :goto_6

    #@1e
    .line 177
    :pswitch_1e
    const-string v2, "Join Reulst: LEFT"

    #@20
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@23
    goto :goto_6

    #@24
    .line 180
    :pswitch_24
    const-string v2, "Join Reulst: RIGHT"

    #@26
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@29
    goto :goto_6

    #@2a
    .line 183
    :pswitch_2a
    const-string v2, "Join Reulst: BOTH"

    #@2c
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@2f
    goto :goto_6

    #@30
    .line 187
    .end local v1           #temp:Landroid/database/CursorJoiner$Result;
    :cond_30
    return-void

    #@31
    .line 175
    nop

    #@32
    :pswitch_data_32
    .packed-switch 0x1
        :pswitch_1e
        :pswitch_24
        :pswitch_2a
    .end packed-switch
.end method
