.class public Lcom/lge/ims/service/uc/UCCallManager;
.super Ljava/lang/Object;
.source "UCCallManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    }
.end annotation


# static fields
.field public static final ACTION_CALL_STATE:Ljava/lang/String; = "com.lge.ims.action.CALL_STATE"

.field public static final ACTION_REG_STATE:Ljava/lang/String; = "com.lge.ims.action.REG_STATE"

.field public static final ACTION_VOLTE_E911_STATUS_CHANGE:Ljava/lang/String; = "com.lge.location.VOLTE_E911_STATUS_CHANGE"

.field private static final EVENT_AC_CONFIGURATION_ACCESS_ALLOWED:I = 0x835

.field private static final EVENT_AC_CONFIGURATION_UPDATE_COMPLETED:I = 0x836

.field public static final PARAM_E911_CALL_STATE:Ljava/lang/String; = "e911_call_state"

.field public static final PARAM_SERVICE_TYPE:Ljava/lang/String; = "svcType"

.field public static final PARAM_STATE:Ljava/lang/String; = "state"

.field public static final PROP_VT_STATE:Ljava/lang/String; = "net.ims.vt.incall"

.field private static final TAG:Ljava/lang/String; = "UC"

.field public static final TYPE_UC:Ljava/lang/String; = "UC"

.field public static final TYPE_VOIP:Ljava/lang/String; = "VOIP"

.field public static final TYPE_VT:Ljava/lang/String; = "VT"

.field private static ucCallMgr:Lcom/lge/ims/service/uc/UCCallManager;


# instance fields
.field public final IMS_VOLTE_E911_CALL_CONNECT:I

.field public final IMS_VOLTE_E911_CALL_END:I

.field public final IMS_VOLTE_E911_CALL_NONE:I

.field public final IMS_VOLTE_E911_CALL_ORIG:I

.field public final IMS_VOLTE_E911_CALL_SRVCC:I

.field public final INIT_VALUE:I

.field public final INVALID_VALUE:I

.field private final MSG_DESTROY_ALL:I

.field private final MSG_NOTIFY_CALL_STATE:I

.field private final MSG_POST_TIME:I

.field private final MSG_START_FAILED:I

.field private final MSG_TERMINATE_ALL:I

.field public final STATE_ACTIVE_CALL:I

.field public final STATE_INACTIVE_CALL:I

.field public final VOLTE_ENABLED:I

.field public final VOLTE_UNSUPPORTED:I

.field private bSvcNotProvisioned:Z

.field private mCallNode:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/ims/service/uc/UCCallManager$CallNode;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mNotifyCS_CSFB:I

.field private mNotifyCallState:I

.field private mUcRegState:I

.field private mVoIPState:I

.field private mVoLTEService:I

.field private mVtState:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 33
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/uc/UCCallManager;->ucCallMgr:Lcom/lge/ims/service/uc/UCCallManager;

    #@3
    return-void
.end method

.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 236
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 49
    iput v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->VOLTE_ENABLED:I

    #@7
    .line 50
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->VOLTE_UNSUPPORTED:I

    #@9
    .line 51
    const/4 v0, -0x1

    #@a
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->INVALID_VALUE:I

    #@c
    .line 52
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->INIT_VALUE:I

    #@e
    .line 54
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->STATE_INACTIVE_CALL:I

    #@10
    .line 55
    iput v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->STATE_ACTIVE_CALL:I

    #@12
    .line 57
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->IMS_VOLTE_E911_CALL_NONE:I

    #@14
    .line 58
    iput v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->IMS_VOLTE_E911_CALL_ORIG:I

    #@16
    .line 59
    const/4 v0, 0x2

    #@17
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->IMS_VOLTE_E911_CALL_CONNECT:I

    #@19
    .line 60
    const/4 v0, 0x3

    #@1a
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->IMS_VOLTE_E911_CALL_END:I

    #@1c
    .line 61
    const/4 v0, 0x4

    #@1d
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->IMS_VOLTE_E911_CALL_SRVCC:I

    #@1f
    .line 63
    const/4 v0, 0x0

    #@20
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@22
    .line 64
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoLTEService:I

    #@24
    .line 65
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVtState:I

    #@26
    .line 66
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoIPState:I

    #@28
    .line 67
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mUcRegState:I

    #@2a
    .line 68
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCallState:I

    #@2c
    .line 69
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCS_CSFB:I

    #@2e
    .line 72
    iput-boolean v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->bSvcNotProvisioned:Z

    #@30
    .line 74
    new-instance v0, Ljava/util/ArrayList;

    #@32
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@35
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@37
    .line 76
    const/16 v0, 0x3e8

    #@39
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->MSG_NOTIFY_CALL_STATE:I

    #@3b
    .line 77
    const/16 v0, 0x3e9

    #@3d
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->MSG_TERMINATE_ALL:I

    #@3f
    .line 78
    const/16 v0, 0x3ea

    #@41
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->MSG_DESTROY_ALL:I

    #@43
    .line 79
    const/16 v0, 0x3eb

    #@45
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->MSG_START_FAILED:I

    #@47
    .line 80
    const/16 v0, 0xa

    #@49
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->MSG_POST_TIME:I

    #@4b
    .line 86
    new-instance v0, Lcom/lge/ims/service/uc/UCCallManager$1;

    #@4d
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/UCCallManager$1;-><init>(Lcom/lge/ims/service/uc/UCCallManager;)V

    #@50
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@52
    .line 237
    const-string v0, "UC"

    #@54
    const-string v1, "UCCallManager()"

    #@56
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@59
    .line 238
    return-void
.end method

.method public static getInstance()Lcom/lge/ims/service/uc/UCCallManager;
    .registers 2

    #@0
    .prologue
    .line 227
    const-string v0, "UC"

    #@2
    const-string v1, "getInstance()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 229
    sget-object v0, Lcom/lge/ims/service/uc/UCCallManager;->ucCallMgr:Lcom/lge/ims/service/uc/UCCallManager;

    #@9
    if-nez v0, :cond_12

    #@b
    .line 230
    new-instance v0, Lcom/lge/ims/service/uc/UCCallManager;

    #@d
    invoke-direct {v0}, Lcom/lge/ims/service/uc/UCCallManager;-><init>()V

    #@10
    sput-object v0, Lcom/lge/ims/service/uc/UCCallManager;->ucCallMgr:Lcom/lge/ims/service/uc/UCCallManager;

    #@12
    .line 233
    :cond_12
    sget-object v0, Lcom/lge/ims/service/uc/UCCallManager;->ucCallMgr:Lcom/lge/ims/service/uc/UCCallManager;

    #@14
    return-object v0
.end method


# virtual methods
.method public acceptSession(Lcom/lge/ims/service/uc/UCSessionImpl;)V
    .registers 9
    .parameter "session"

    #@0
    .prologue
    .line 370
    const/4 v2, 0x0

    #@1
    .line 372
    .local v2, updated:Z
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v3

    #@8
    if-ge v1, v3, :cond_1f

    #@a
    .line 373
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@12
    .line 375
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_17

    #@14
    .line 372
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_2

    #@17
    .line 379
    :cond_17
    iget-object v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@19
    if-ne v3, p1, :cond_14

    #@1b
    .line 380
    const/4 v3, 0x2

    #@1c
    iput v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallState:I

    #@1e
    .line 381
    const/4 v2, 0x1

    #@1f
    .line 386
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    :cond_1f
    const/4 v3, 0x1

    #@20
    if-ne v2, v3, :cond_2b

    #@22
    .line 387
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@24
    const/16 v4, 0x3e8

    #@26
    const-wide/16 v5, 0xa

    #@28
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@2b
    .line 389
    :cond_2b
    return-void
.end method

.method public closeSession(Lcom/lge/ims/service/uc/UCSessionImpl;)V
    .registers 9
    .parameter "session"

    #@0
    .prologue
    .line 308
    if-nez p1, :cond_3

    #@2
    .line 337
    :goto_2
    return-void

    #@3
    .line 312
    :cond_3
    const/4 v2, 0x0

    #@4
    .line 314
    .local v2, updated:Z
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v3

    #@b
    if-ge v1, v3, :cond_28

    #@d
    .line 315
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@15
    .line 317
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_1a

    #@17
    .line 314
    :cond_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_5

    #@1a
    .line 321
    :cond_1a
    iget-object v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@1c
    if-eqz v3, :cond_17

    #@1e
    .line 325
    iget-object v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@20
    if-ne v3, p1, :cond_17

    #@22
    .line 326
    const/4 v2, 0x1

    #@23
    .line 327
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@25
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@28
    .line 332
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    :cond_28
    const/4 v3, 0x1

    #@29
    if-ne v2, v3, :cond_34

    #@2b
    .line 333
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@2d
    const/16 v4, 0x3e8

    #@2f
    const-wide/16 v5, 0xa

    #@31
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@34
    .line 336
    :cond_34
    const-string v3, "UC"

    #@36
    new-instance v4, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v5, "closeSession() :: size = "

    #@3d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    iget-object v5, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@43
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@46
    move-result v5

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@52
    goto :goto_2
.end method

.method public createConferenceSession(II)V
    .registers 9
    .parameter "sessKey"
    .parameter "callType"

    #@0
    .prologue
    .line 293
    if-nez p1, :cond_3

    #@2
    .line 304
    :goto_2
    return-void

    #@3
    .line 297
    :cond_3
    new-instance v1, Lcom/lge/ims/service/uc/UCSessionImpl;

    #@5
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@7
    invoke-direct {v1, v2, p1}, Lcom/lge/ims/service/uc/UCSessionImpl;-><init>(Landroid/content/Context;I)V

    #@a
    .line 299
    .local v1, sessionImpl:Lcom/lge/ims/service/uc/UCSessionImpl;
    new-instance v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@c
    invoke-direct {v0, p0, v1, p1, p2}, Lcom/lge/ims/service/uc/UCCallManager$CallNode;-><init>(Lcom/lge/ims/service/uc/UCCallManager;Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@f
    .line 300
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    const/4 v2, 0x2

    #@10
    invoke-virtual {v0, v2}, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->setCallState(I)V

    #@13
    .line 301
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@18
    .line 303
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@1a
    const/16 v3, 0x3e8

    #@1c
    const-wide/16 v4, 0xa

    #@1e
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@21
    goto :goto_2
.end method

.method public destroyCalls()V
    .registers 11

    #@0
    .prologue
    .line 506
    invoke-static {}, Lcom/lge/ims/service/uc/UCServiceManager;->getInstance()Lcom/lge/ims/service/uc/UCServiceManager;

    #@3
    move-result-object v6

    #@4
    invoke-virtual {v6}, Lcom/lge/ims/service/uc/UCServiceManager;->getService()Lcom/lge/ims/service/uc/UCServiceImpl;

    #@7
    move-result-object v5

    #@8
    .line 507
    .local v5, serviceImpl:Lcom/lge/ims/service/uc/UCServiceImpl;
    const/4 v1, 0x0

    #@9
    .line 509
    .local v1, closed:Z
    if-eqz v5, :cond_57

    #@b
    .line 510
    const/4 v3, 0x0

    #@c
    .local v3, i:I
    :goto_c
    iget-object v6, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@e
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@11
    move-result v6

    #@12
    if-ge v3, v6, :cond_45

    #@14
    .line 511
    iget-object v6, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@1c
    .line 513
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_21

    #@1e
    .line 510
    :cond_1e
    :goto_1e
    add-int/lit8 v3, v3, 0x1

    #@20
    goto :goto_c

    #@21
    .line 517
    :cond_21
    iget-object v6, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@23
    if-eqz v6, :cond_1e

    #@25
    .line 522
    :try_start_25
    iget-object v6, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@27
    invoke-virtual {v5, v6}, Lcom/lge/ims/service/uc/UCServiceImpl;->closeSession(Lcom/lge/ims/service/uc/IUCSession;)V
    :try_end_2a
    .catch Ljava/lang/Exception; {:try_start_25 .. :try_end_2a} :catch_2b

    #@2a
    goto :goto_1e

    #@2b
    .line 523
    :catch_2b
    move-exception v2

    #@2c
    .line 524
    .local v2, e:Ljava/lang/Exception;
    const-string v6, "UC"

    #@2e
    new-instance v7, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v8, "Exception = "

    #@35
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v7

    #@39
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v7

    #@3d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v7

    #@41
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@44
    goto :goto_1e

    #@45
    .line 528
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    .end local v2           #e:Ljava/lang/Exception;
    :cond_45
    const/4 v4, 0x0

    #@46
    .local v4, j:I
    :goto_46
    iget-object v6, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@48
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@4b
    move-result v6

    #@4c
    if-ge v4, v6, :cond_57

    #@4e
    .line 529
    iget-object v6, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@50
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@53
    .line 530
    const/4 v1, 0x1

    #@54
    .line 528
    add-int/lit8 v4, v4, 0x1

    #@56
    goto :goto_46

    #@57
    .line 534
    .end local v3           #i:I
    .end local v4           #j:I
    :cond_57
    const/4 v6, 0x1

    #@58
    if-ne v1, v6, :cond_63

    #@5a
    .line 535
    iget-object v6, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@5c
    const/16 v7, 0x3e8

    #@5e
    const-wide/16 v8, 0xa

    #@60
    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@63
    .line 537
    :cond_63
    return-void
.end method

.method public getProperty(Lcom/lge/ims/service/uc/UCSessionImpl;I)I
    .registers 6
    .parameter "session"
    .parameter "item"

    #@0
    .prologue
    .line 457
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_1f

    #@9
    .line 458
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@11
    .line 460
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_16

    #@13
    .line 457
    :cond_13
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_1

    #@16
    .line 464
    :cond_16
    iget-object v2, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@18
    if-ne v2, p1, :cond_13

    #@1a
    .line 465
    invoke-virtual {v0, p2}, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->getProperty(I)I

    #@1d
    move-result v2

    #@1e
    .line 469
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    :goto_1e
    return v2

    #@1f
    :cond_1f
    const/4 v2, 0x0

    #@20
    goto :goto_1e
.end method

.method public getSession(I)Lcom/lge/ims/service/uc/UCSessionImpl;
    .registers 6
    .parameter "sessKey"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 246
    if-nez p1, :cond_4

    #@3
    .line 262
    :cond_3
    :goto_3
    return-object v2

    #@4
    .line 250
    :cond_4
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v3

    #@b
    if-ge v1, v3, :cond_3

    #@d
    .line 251
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@15
    .line 253
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_1a

    #@17
    .line 250
    :cond_17
    add-int/lit8 v1, v1, 0x1

    #@19
    goto :goto_5

    #@1a
    .line 257
    :cond_1a
    iget v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSessKey:I

    #@1c
    if-ne v3, p1, :cond_17

    #@1e
    .line 258
    iget-object v2, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@20
    goto :goto_3
.end method

.method public incomingSession(Lcom/lge/ims/service/uc/UCSessionImpl;IILjava/lang/String;)V
    .registers 10
    .parameter "session"
    .parameter "sessKey"
    .parameter "callType"
    .parameter "incomingNumber"

    #@0
    .prologue
    .line 278
    if-eqz p1, :cond_4

    #@2
    if-nez p2, :cond_5

    #@4
    .line 289
    :cond_4
    :goto_4
    return-void

    #@5
    .line 282
    :cond_5
    new-instance v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@7
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/lge/ims/service/uc/UCCallManager$CallNode;-><init>(Lcom/lge/ims/service/uc/UCCallManager;Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@a
    .line 283
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    const/4 v1, 0x1

    #@b
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->setCallState(I)V

    #@e
    .line 284
    invoke-virtual {v0, p4}, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->setMTNumber(Ljava/lang/String;)V

    #@11
    .line 286
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@16
    .line 288
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@18
    const/16 v2, 0x3e8

    #@1a
    const-wide/16 v3, 0xa

    #@1c
    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@1f
    goto :goto_4
.end method

.method public intializeUcState()V
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    .line 735
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@3
    if-nez v1, :cond_6

    #@5
    .line 792
    :cond_5
    :goto_5
    return-void

    #@6
    .line 740
    :cond_6
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v0

    #@c
    .line 741
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v7, 0x0

    #@d
    .line 743
    .local v7, cursor:Landroid/database/Cursor;
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/UCCallManager;->registerAC()V

    #@10
    .line 746
    :try_start_10
    sget-object v1, Lcom/lge/ims/provider/uc/UC$Data;->CONTENT_URI:Landroid/net/Uri;

    #@12
    const/4 v2, 0x0

    #@13
    const/4 v3, 0x0

    #@14
    const/4 v4, 0x0

    #@15
    const/4 v5, 0x0

    #@16
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@19
    move-result-object v7

    #@1a
    .line 748
    if-nez v7, :cond_29

    #@1c
    .line 749
    const-string v1, "UC"

    #@1e
    const-string v2, "Cursor is null"

    #@20
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_23
    .catchall {:try_start_10 .. :try_end_23} :catchall_d5
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_23} :catch_b6

    #@23
    .line 774
    if-eqz v7, :cond_5

    #@25
    .line 775
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@28
    goto :goto_5

    #@29
    .line 753
    :cond_29
    :try_start_29
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@2c
    move-result v1

    #@2d
    if-lez v1, :cond_ae

    #@2f
    .line 754
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    #@32
    move-result v1

    #@33
    if-nez v1, :cond_42

    #@35
    .line 755
    const-string v1, "UC"

    #@37
    const-string v2, "first move is null"

    #@39
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3c
    .catchall {:try_start_29 .. :try_end_3c} :catchall_d5
    .catch Ljava/lang/Exception; {:try_start_29 .. :try_end_3c} :catch_b6

    #@3c
    .line 774
    if-eqz v7, :cond_5

    #@3e
    .line 775
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@41
    goto :goto_5

    #@42
    .line 759
    :cond_42
    :try_start_42
    new-instance v10, Landroid/content/ContentValues;

    #@44
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    #@47
    .line 761
    .local v10, newValues:Landroid/content/ContentValues;
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getUCStateStr()[Ljava/lang/String;

    #@4a
    move-result-object v12

    #@4b
    .line 763
    .local v12, strUc:[Ljava/lang/String;
    const/4 v9, 0x0

    #@4c
    .local v9, i:I
    :goto_4c
    array-length v1, v12

    #@4d
    if-ge v9, v1, :cond_60

    #@4f
    .line 764
    aget-object v1, v12, v9

    #@51
    const/4 v2, 0x0

    #@52
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    .line 763
    add-int/lit8 v9, v9, 0x1

    #@5f
    goto :goto_4c

    #@60
    .line 767
    :cond_60
    sget-object v1, Lcom/lge/ims/provider/uc/UC$Data;->CONTENT_URI:Landroid/net/Uri;

    #@62
    const/4 v2, 0x0

    #@63
    const/4 v3, 0x0

    #@64
    invoke-virtual {v0, v1, v10, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_67
    .catchall {:try_start_42 .. :try_end_67} :catchall_d5
    .catch Ljava/lang/Exception; {:try_start_42 .. :try_end_67} :catch_b6

    #@67
    .line 774
    .end local v9           #i:I
    .end local v10           #newValues:Landroid/content/ContentValues;
    .end local v12           #strUc:[Ljava/lang/String;
    :goto_67
    if-eqz v7, :cond_6c

    #@69
    .line 775
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@6c
    .line 779
    :cond_6c
    :goto_6c
    const-string v2, "com.lge.ims.action.CALL_STATE"

    #@6e
    const-string v3, "state"

    #@70
    const-string v5, "svcType"

    #@72
    const-string v6, "VT"

    #@74
    move-object v1, p0

    #@75
    move v4, v13

    #@76
    invoke-virtual/range {v1 .. v6}, Lcom/lge/ims/service/uc/UCCallManager;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@79
    .line 780
    const-string v2, "com.lge.ims.action.CALL_STATE"

    #@7b
    const-string v3, "state"

    #@7d
    const-string v5, "svcType"

    #@7f
    const-string v6, "VOIP"

    #@81
    move-object v1, p0

    #@82
    move v4, v13

    #@83
    invoke-virtual/range {v1 .. v6}, Lcom/lge/ims/service/uc/UCCallManager;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@86
    .line 781
    const-string v2, "com.lge.ims.action.REG_STATE"

    #@88
    const-string v3, "state"

    #@8a
    const-string v5, "svcType"

    #@8c
    const-string v6, "UC"

    #@8e
    move-object v1, p0

    #@8f
    move v4, v13

    #@90
    invoke-virtual/range {v1 .. v6}, Lcom/lge/ims/service/uc/UCCallManager;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@93
    .line 783
    iget v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCallState:I

    #@95
    const-string v2, ""

    #@97
    invoke-virtual {p0, v1, v2}, Lcom/lge/ims/service/uc/UCCallManager;->notifyCallStateToTelephony(ILjava/lang/String;)Z

    #@9a
    .line 786
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@9d
    move-result-object v11

    #@9e
    .line 788
    .local v11, ssm:Lcom/lge/ims/SystemServiceManager;
    if-eqz v11, :cond_5

    #@a0
    .line 789
    iput v13, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCS_CSFB:I

    #@a2
    .line 790
    const/4 v1, 0x1

    #@a3
    const/16 v2, 0xb

    #@a5
    iget v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCS_CSFB:I

    #@a7
    const-string v4, ""

    #@a9
    invoke-virtual {v11, v1, v2, v3, v4}, Lcom/lge/ims/SystemServiceManager;->setSysInfo(IIILjava/lang/String;)V

    #@ac
    goto/16 :goto_5

    #@ae
    .line 769
    .end local v11           #ssm:Lcom/lge/ims/SystemServiceManager;
    :cond_ae
    :try_start_ae
    const-string v1, "UC"

    #@b0
    const-string v2, "Cursor count is invalid"

    #@b2
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_b5
    .catchall {:try_start_ae .. :try_end_b5} :catchall_d5
    .catch Ljava/lang/Exception; {:try_start_ae .. :try_end_b5} :catch_b6

    #@b5
    goto :goto_67

    #@b6
    .line 771
    :catch_b6
    move-exception v8

    #@b7
    .line 772
    .local v8, e:Ljava/lang/Exception;
    :try_start_b7
    const-string v1, "UC"

    #@b9
    new-instance v2, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v3, "Exception = "

    #@c0
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v2

    #@c4
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v2

    #@c8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v2

    #@cc
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_cf
    .catchall {:try_start_b7 .. :try_end_cf} :catchall_d5

    #@cf
    .line 774
    if-eqz v7, :cond_6c

    #@d1
    .line 775
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@d4
    goto :goto_6c

    #@d5
    .line 774
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_d5
    move-exception v1

    #@d6
    if-eqz v7, :cond_db

    #@d8
    .line 775
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@db
    :cond_db
    throw v1
.end method

.method public notifyCallState()V
    .registers 11

    #@0
    .prologue
    .line 557
    const/4 v8, 0x0

    #@1
    .line 558
    .local v8, voipState:I
    const/4 v9, 0x0

    #@2
    .line 560
    .local v9, vtState:I
    const/4 v7, 0x0

    #@3
    .local v7, i:I
    :goto_3
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@8
    move-result v0

    #@9
    if-ge v7, v0, :cond_2e

    #@b
    .line 561
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@d
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v6

    #@11
    check-cast v6, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@13
    .line 563
    .local v6, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v6, :cond_18

    #@15
    .line 560
    :cond_15
    :goto_15
    add-int/lit8 v7, v7, 0x1

    #@17
    goto :goto_3

    #@18
    .line 567
    :cond_18
    iget-object v0, v6, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@1a
    if-eqz v0, :cond_15

    #@1c
    .line 571
    iget v0, v6, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@1e
    const/4 v1, -0x1

    #@1f
    if-eq v0, v1, :cond_15

    #@21
    .line 575
    iget v0, v6, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@23
    if-nez v0, :cond_27

    #@25
    .line 576
    const/4 v8, 0x1

    #@26
    .line 577
    goto :goto_15

    #@27
    .line 580
    :cond_27
    iget v0, v6, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@29
    const/4 v1, 0x2

    #@2a
    if-ne v0, v1, :cond_15

    #@2c
    .line 581
    const/4 v9, 0x1

    #@2d
    goto :goto_15

    #@2e
    .line 585
    .end local v6           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    :cond_2e
    iget v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoIPState:I

    #@30
    if-eq v0, v8, :cond_4b

    #@32
    .line 586
    iput v8, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoIPState:I

    #@34
    .line 587
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getVoIPStateStr()Ljava/lang/String;

    #@37
    move-result-object v0

    #@38
    iget v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoIPState:I

    #@3a
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcProvider(Ljava/lang/String;I)V

    #@3d
    .line 588
    const-string v1, "com.lge.ims.action.CALL_STATE"

    #@3f
    const-string v2, "state"

    #@41
    iget v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoIPState:I

    #@43
    const-string v4, "svcType"

    #@45
    const-string v5, "VOIP"

    #@47
    move-object v0, p0

    #@48
    invoke-virtual/range {v0 .. v5}, Lcom/lge/ims/service/uc/UCCallManager;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 591
    :cond_4b
    iget v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVtState:I

    #@4d
    if-eq v0, v9, :cond_68

    #@4f
    .line 592
    iput v9, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVtState:I

    #@51
    .line 600
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getVtStateStr()Ljava/lang/String;

    #@54
    move-result-object v0

    #@55
    iget v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVtState:I

    #@57
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcProvider(Ljava/lang/String;I)V

    #@5a
    .line 601
    const-string v1, "com.lge.ims.action.CALL_STATE"

    #@5c
    const-string v2, "state"

    #@5e
    iget v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVtState:I

    #@60
    const-string v4, "svcType"

    #@62
    const-string v5, "VT"

    #@64
    move-object v0, p0

    #@65
    invoke-virtual/range {v0 .. v5}, Lcom/lge/ims/service/uc/UCCallManager;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@68
    .line 603
    :cond_68
    return-void
.end method

.method public notifyCallStateToTelephony(ILjava/lang/String;)Z
    .registers 10
    .parameter "callState"
    .parameter "incomingNumber"

    #@0
    .prologue
    .line 651
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getTelephonyRegistry()Lcom/android/internal/telephony/ITelephonyRegistry;

    #@3
    move-result-object v1

    #@4
    .line 652
    .local v1, mRegistry:Lcom/android/internal/telephony/ITelephonyRegistry;
    const/4 v2, 0x0

    #@5
    .line 654
    .local v2, result:Z
    if-nez v1, :cond_9

    #@7
    move v3, v2

    #@8
    .line 669
    .end local v2           #result:Z
    .local v3, result:I
    :goto_8
    return v3

    #@9
    .line 659
    .end local v3           #result:I
    .restart local v2       #result:Z
    :cond_9
    :try_start_9
    invoke-interface {v1, p1, p2}, Lcom/android/internal/telephony/ITelephonyRegistry;->notifyCallState(ILjava/lang/String;)V
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_9 .. :try_end_c} :catch_f
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_c} :catch_2c

    #@c
    .line 660
    const/4 v2, 0x1

    #@d
    :goto_d
    move v3, v2

    #@e
    .line 669
    .restart local v3       #result:I
    goto :goto_8

    #@f
    .line 661
    .end local v3           #result:I
    :catch_f
    move-exception v0

    #@10
    .line 662
    .local v0, e:Landroid/os/RemoteException;
    const-string v4, "UC"

    #@12
    new-instance v5, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v6, "RemoteException ::"

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v5

    #@25
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 663
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->rebindTelephonyRegistry()V

    #@2b
    goto :goto_d

    #@2c
    .line 664
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_2c
    move-exception v0

    #@2d
    .line 665
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "UC"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "Exception ::"

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 666
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->rebindTelephonyRegistry()V

    #@48
    goto :goto_d
.end method

.method public notifyECallState(II)V
    .registers 9
    .parameter "event"
    .parameter "reason"

    #@0
    .prologue
    .line 920
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@2
    if-nez v3, :cond_5

    #@4
    .line 967
    :cond_4
    :goto_4
    return-void

    #@5
    .line 925
    :cond_5
    const/4 v2, 0x0

    #@6
    .line 926
    .local v2, state:I
    sparse-switch p1, :sswitch_data_a0

    #@9
    .line 947
    :goto_9
    :try_start_9
    const-string v3, "UC"

    #@b
    new-instance v4, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "notifyECallState : event="

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    const-string v5, ", reason="

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    const-string v5, ", state="

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@35
    .line 949
    if-eqz v2, :cond_4

    #@37
    .line 950
    new-instance v1, Landroid/content/Intent;

    #@39
    const-string v3, "com.lge.location.VOLTE_E911_STATUS_CHANGE"

    #@3b
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3e
    .line 951
    .local v1, intent:Landroid/content/Intent;
    if-nez v1, :cond_70

    #@40
    .line 952
    const-string v3, "UC"

    #@42
    const-string v4, "Intent is null"

    #@44
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_47
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_47} :catch_48

    #@47
    goto :goto_4

    #@48
    .line 964
    .end local v1           #intent:Landroid/content/Intent;
    :catch_48
    move-exception v0

    #@49
    .line 965
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "UC"

    #@4b
    new-instance v4, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v5, "Exception ::"

    #@52
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v4

    #@56
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v4

    #@5e
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@61
    goto :goto_4

    #@62
    .line 928
    .end local v0           #e:Ljava/lang/Exception;
    :sswitch_62
    const/4 v2, 0x1

    #@63
    .line 929
    goto :goto_9

    #@64
    .line 931
    :sswitch_64
    const/4 v2, 0x2

    #@65
    .line 932
    goto :goto_9

    #@66
    .line 934
    :sswitch_66
    const/4 v2, 0x3

    #@67
    .line 935
    goto :goto_9

    #@68
    .line 937
    :sswitch_68
    const/16 v3, 0x1a2

    #@6a
    if-eq p2, v3, :cond_6e

    #@6c
    .line 938
    const/4 v2, 0x3

    #@6d
    goto :goto_9

    #@6e
    .line 940
    :cond_6e
    const/4 v2, 0x4

    #@6f
    .line 942
    goto :goto_9

    #@70
    .line 956
    .restart local v1       #intent:Landroid/content/Intent;
    :cond_70
    const/16 v3, 0x20

    #@72
    :try_start_72
    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@75
    .line 957
    const-string v3, "e911_call_state"

    #@77
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@7a
    .line 959
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@7c
    invoke-virtual {v3, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@7f
    .line 961
    const-string v3, "UC"

    #@81
    new-instance v4, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v5, "sendBroadcast : com.lge.location.VOLTE_E911_STATUS_CHANGE(e911_call_state="

    #@88
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v4

    #@8c
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v4

    #@90
    const-string v5, ")"

    #@92
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v4

    #@96
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v4

    #@9a
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9d
    .catch Ljava/lang/Exception; {:try_start_72 .. :try_end_9d} :catch_48

    #@9d
    goto/16 :goto_4

    #@9f
    .line 926
    nop

    #@a0
    :sswitch_data_a0
    .sparse-switch
        0x4b1 -> :sswitch_62
        0x515 -> :sswitch_64
        0x516 -> :sswitch_66
        0x51e -> :sswitch_68
    .end sparse-switch
.end method

.method public notifyStartedFail(Lcom/lge/ims/service/uc/UCSessionImpl;)V
    .registers 2
    .parameter "session"

    #@0
    .prologue
    .line 549
    if-nez p1, :cond_3

    #@2
    .line 554
    :goto_2
    return-void

    #@3
    .line 553
    :cond_3
    invoke-virtual {p1}, Lcom/lge/ims/service/uc/UCSessionImpl;->sendStartFailed()V

    #@6
    goto :goto_2
.end method

.method public notifyToCSFB()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 673
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@4
    move-result-object v1

    #@5
    .line 675
    .local v1, pst:Lcom/lge/ims/PhoneStateTracker;
    if-nez v1, :cond_f

    #@7
    .line 676
    const-string v3, "UC"

    #@9
    const-string v4, "notifyToCSFB() :: PhoneStateTracker is invalid"

    #@b
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 706
    :goto_e
    return-void

    #@f
    .line 680
    :cond_f
    invoke-virtual {v1}, Lcom/lge/ims/PhoneStateTracker;->is4G()Z

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_21

    #@15
    iget v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCS_CSFB:I

    #@17
    if-nez v3, :cond_21

    #@19
    .line 681
    const-string v3, "UC"

    #@1b
    const-string v4, "notifyToCSFB() :: IS NOT 4G - None Notify CSFB"

    #@1d
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    goto :goto_e

    #@21
    .line 685
    :cond_21
    const/4 v0, 0x0

    #@22
    .line 687
    .local v0, NotifyCS_CSFB:I
    iget v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVtState:I

    #@24
    if-eq v3, v6, :cond_2a

    #@26
    iget v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoIPState:I

    #@28
    if-ne v3, v6, :cond_2b

    #@2a
    .line 688
    :cond_2a
    const/4 v0, 0x1

    #@2b
    .line 691
    :cond_2b
    iget v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCS_CSFB:I

    #@2d
    if-ne v3, v0, :cond_50

    #@2f
    .line 692
    const-string v3, "UC"

    #@31
    new-instance v4, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v5, "notifyToCSFB() :: No Changed State ["

    #@38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    iget v5, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCS_CSFB:I

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    const-string v5, "]"

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@4f
    goto :goto_e

    #@50
    .line 696
    :cond_50
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getInstance()Lcom/lge/ims/SystemServiceManager;

    #@53
    move-result-object v2

    #@54
    .line 698
    .local v2, ssm:Lcom/lge/ims/SystemServiceManager;
    if-nez v2, :cond_5e

    #@56
    .line 699
    const-string v3, "UC"

    #@58
    const-string v4, "SystemServiceManager is null"

    #@5a
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5d
    goto :goto_e

    #@5e
    .line 703
    :cond_5e
    const-string v3, "UC"

    #@60
    new-instance v4, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v5, "notifyToCSFB() :: Changed State ["

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    iget v5, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCS_CSFB:I

    #@6d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v4

    #@71
    const-string v5, "]"

    #@73
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v4

    #@77
    const-string v5, "->"

    #@79
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v4

    #@7d
    const-string v5, "["

    #@7f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v4

    #@83
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v4

    #@87
    const-string v5, "]"

    #@89
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v4

    #@8d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v4

    #@91
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@94
    .line 704
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCS_CSFB:I

    #@96
    .line 705
    const/16 v3, 0xb

    #@98
    iget v4, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCS_CSFB:I

    #@9a
    const-string v5, ""

    #@9c
    invoke-virtual {v2, v6, v3, v4, v5}, Lcom/lge/ims/SystemServiceManager;->setSysInfo(IIILjava/lang/String;)V

    #@9f
    goto/16 :goto_e
.end method

.method public openSession(Lcom/lge/ims/service/uc/UCSessionImpl;)V
    .registers 4
    .parameter "session"

    #@0
    .prologue
    .line 267
    if-nez p1, :cond_3

    #@2
    .line 274
    :goto_2
    return-void

    #@3
    .line 273
    :cond_3
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@5
    new-instance v1, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@7
    invoke-direct {v1, p0, p1}, Lcom/lge/ims/service/uc/UCCallManager$CallNode;-><init>(Lcom/lge/ims/service/uc/UCCallManager;Lcom/lge/ims/service/uc/UCSessionImpl;)V

    #@a
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d
    goto :goto_2
.end method

.method public processStartedFail(Lcom/lge/ims/service/uc/UCSessionImpl;)V
    .registers 6
    .parameter "session"

    #@0
    .prologue
    .line 540
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 542
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x3eb

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 543
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    .line 545
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@c
    const-wide/16 v2, 0xa

    #@e
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@11
    .line 546
    return-void
.end method

.method public registerAC()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 902
    invoke-static {}, Lcom/lge/ims/service/ac/ACService;->getInstance()Lcom/lge/ims/service/ac/ACService;

    #@4
    move-result-object v0

    #@5
    .line 904
    .local v0, acs:Lcom/lge/ims/service/ac/ACService;
    if-eqz v0, :cond_27

    #@7
    .line 905
    const-string v1, "UC"

    #@9
    const-string v2, "registerAC"

    #@b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 907
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/ACService;->isConfigurationAccessAllowed()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_1f

    #@14
    .line 908
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/UCCallManager;->updateEnableFromAC()V

    #@17
    .line 913
    :goto_17
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@19
    const/16 v2, 0x836

    #@1b
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/ac/ACService;->registerForConfigurationUpdateCompleted(Landroid/os/Handler;ILjava/lang/Object;)V

    #@1e
    .line 917
    :goto_1e
    return-void

    #@1f
    .line 910
    :cond_1f
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@21
    const/16 v2, 0x835

    #@23
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/ac/ACService;->registerForConfigurationAccessAllowed(Landroid/os/Handler;ILjava/lang/Object;)V

    #@26
    goto :goto_17

    #@27
    .line 915
    :cond_27
    const-string v1, "UC"

    #@29
    const-string v2, "acs is null"

    #@2b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    goto :goto_1e
.end method

.method public sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "action"
    .parameter "name1"
    .parameter "value1"
    .parameter "name2"
    .parameter "value2"

    #@0
    .prologue
    .line 846
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 866
    :goto_4
    return-void

    #@5
    .line 850
    :cond_5
    const-string v1, "UC"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "sendIntent ("

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, "), name1="

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, ", value1="

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    const-string v3, ", name2="

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    const-string v3, ", value2="

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 853
    new-instance v0, Landroid/content/Intent;

    #@47
    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@4a
    .line 855
    .local v0, intent:Landroid/content/Intent;
    if-nez v0, :cond_54

    #@4c
    .line 856
    const-string v1, "UC"

    #@4e
    const-string v2, "Intent is null"

    #@50
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    goto :goto_4

    #@54
    .line 861
    :cond_54
    const/16 v1, 0x20

    #@56
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@59
    .line 862
    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@5c
    .line 863
    invoke-virtual {v0, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@5f
    .line 865
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@61
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@64
    goto :goto_4
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 241
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@2
    .line 242
    return-void
.end method

.method public setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V
    .registers 7
    .parameter "session"
    .parameter "item"
    .parameter "value"

    #@0
    .prologue
    .line 440
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_23

    #@9
    .line 441
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@11
    .line 443
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_16

    #@13
    .line 440
    :cond_13
    add-int/lit8 v1, v1, 0x1

    #@15
    goto :goto_1

    #@16
    .line 447
    :cond_16
    iget-object v2, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@18
    if-ne v2, p1, :cond_13

    #@1a
    .line 448
    invoke-virtual {v0, p2}, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->getProperty(I)I

    #@1d
    move-result v2

    #@1e
    if-eq v2, p3, :cond_23

    #@20
    .line 449
    invoke-virtual {v0, p2, p3}, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->setProperty(II)V

    #@23
    .line 454
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    :cond_23
    return-void
.end method

.method public startSession(Lcom/lge/ims/service/uc/UCSessionImpl;I)V
    .registers 10
    .parameter "session"
    .parameter "callType"

    #@0
    .prologue
    .line 342
    const/4 v2, 0x0

    #@1
    .line 344
    .local v2, updated:Z
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v3

    #@8
    if-ge v1, v3, :cond_25

    #@a
    .line 345
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@12
    .line 347
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_17

    #@14
    .line 344
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_2

    #@17
    .line 351
    :cond_17
    iget-object v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@19
    if-eqz v3, :cond_14

    #@1b
    .line 355
    iget-object v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@1d
    if-ne v3, p1, :cond_14

    #@1f
    .line 356
    iput p2, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@21
    .line 357
    const/4 v3, 0x2

    #@22
    iput v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallState:I

    #@24
    .line 358
    const/4 v2, 0x1

    #@25
    .line 363
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    :cond_25
    const/4 v3, 0x1

    #@26
    if-ne v2, v3, :cond_31

    #@28
    .line 364
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@2a
    const/16 v4, 0x3e8

    #@2c
    const-wide/16 v5, 0xa

    #@2e
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@31
    .line 366
    :cond_31
    return-void
.end method

.method public terminateAllSession()V
    .registers 5

    #@0
    .prologue
    .line 473
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@2
    const/16 v1, 0x3e9

    #@4
    const-wide/16 v2, 0xa

    #@6
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@9
    .line 474
    return-void
.end method

.method public terminateCalls()V
    .registers 9

    #@0
    .prologue
    .line 477
    const/4 v3, 0x0

    #@1
    .line 479
    .local v3, teminated:Z
    const/4 v2, 0x0

    #@2
    .local v2, i:I
    :goto_2
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v4

    #@8
    if-ge v2, v4, :cond_43

    #@a
    .line 480
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@12
    .line 482
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_17

    #@14
    .line 479
    :cond_14
    :goto_14
    add-int/lit8 v2, v2, 0x1

    #@16
    goto :goto_2

    #@17
    .line 486
    :cond_17
    iget-object v4, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@19
    if-eqz v4, :cond_14

    #@1b
    .line 491
    :try_start_1b
    iget-object v4, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@1d
    const/4 v5, 0x0

    #@1e
    invoke-virtual {v4, v5}, Lcom/lge/ims/service/uc/UCSessionImpl;->terminate(I)V

    #@21
    .line 492
    iget-object v4, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@23
    const/4 v5, 0x0

    #@24
    invoke-virtual {v4, v5}, Lcom/lge/ims/service/uc/UCSessionImpl;->setListener(Lcom/lge/ims/service/uc/IUCSessionListener;)V
    :try_end_27
    .catch Ljava/lang/Exception; {:try_start_1b .. :try_end_27} :catch_29

    #@27
    .line 497
    :goto_27
    const/4 v3, 0x1

    #@28
    goto :goto_14

    #@29
    .line 493
    :catch_29
    move-exception v1

    #@2a
    .line 494
    .local v1, e:Ljava/lang/Exception;
    const-string v4, "UC"

    #@2c
    new-instance v5, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v6, "Exception = "

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    goto :goto_27

    #@43
    .line 500
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    .end local v1           #e:Ljava/lang/Exception;
    :cond_43
    const/4 v4, 0x1

    #@44
    if-ne v3, v4, :cond_4f

    #@46
    .line 501
    iget-object v4, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@48
    const/16 v5, 0x3ea

    #@4a
    const-wide/16 v6, 0x64

    #@4c
    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@4f
    .line 503
    :cond_4f
    return-void
.end method

.method public updateCallState()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 606
    const/4 v4, 0x0

    #@2
    .line 607
    .local v4, offhook:Z
    const/4 v5, 0x0

    #@3
    .line 608
    .local v5, ringing:Z
    const-string v3, ""

    #@5
    .line 610
    .local v3, number:Ljava/lang/String;
    const/4 v1, 0x0

    #@6
    .local v1, i:I
    :goto_6
    iget-object v6, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v6

    #@c
    if-ge v1, v6, :cond_22

    #@e
    .line 611
    iget-object v6, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@16
    .line 613
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_1b

    #@18
    .line 610
    :goto_18
    add-int/lit8 v1, v1, 0x1

    #@1a
    goto :goto_6

    #@1b
    .line 617
    :cond_1b
    iget v6, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallState:I

    #@1d
    if-ne v6, v8, :cond_45

    #@1f
    .line 618
    const/4 v5, 0x1

    #@20
    .line 619
    iget-object v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mIncomingNumber:Ljava/lang/String;

    #@22
    .line 630
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    :cond_22
    const/4 v2, 0x0

    #@23
    .line 632
    .local v2, notifyState:I
    if-ne v5, v8, :cond_54

    #@25
    .line 633
    const/4 v2, 0x1

    #@26
    .line 640
    :goto_26
    iget v6, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCallState:I

    #@28
    if-ne v6, v2, :cond_5a

    #@2a
    .line 641
    const-string v6, "UC"

    #@2c
    new-instance v7, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v8, "updateCallState() :: Call state is same; state="

    #@33
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v7

    #@37
    iget v8, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCallState:I

    #@39
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v7

    #@3d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v7

    #@41
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@44
    .line 648
    :cond_44
    :goto_44
    return-void

    #@45
    .line 623
    .end local v2           #notifyState:I
    .restart local v0       #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    :cond_45
    iget v6, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallState:I

    #@47
    const/4 v7, 0x2

    #@48
    if-ne v6, v7, :cond_4c

    #@4a
    .line 624
    const/4 v4, 0x1

    #@4b
    goto :goto_18

    #@4c
    .line 626
    :cond_4c
    const-string v6, "UC"

    #@4e
    const-string v7, "updateCallState() :: Call state is IDLE"

    #@50
    invoke-static {v6, v7}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    goto :goto_18

    #@54
    .line 634
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    .restart local v2       #notifyState:I
    :cond_54
    if-ne v4, v8, :cond_58

    #@56
    .line 635
    const/4 v2, 0x2

    #@57
    goto :goto_26

    #@58
    .line 637
    :cond_58
    const/4 v2, 0x0

    #@59
    goto :goto_26

    #@5a
    .line 645
    :cond_5a
    invoke-virtual {p0, v2, v3}, Lcom/lge/ims/service/uc/UCCallManager;->notifyCallStateToTelephony(ILjava/lang/String;)Z

    #@5d
    move-result v6

    #@5e
    if-ne v6, v8, :cond_44

    #@60
    .line 646
    iput v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->mNotifyCallState:I

    #@62
    goto :goto_44
.end method

.method public updateEnableFromAC()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 870
    invoke-static {}, Lcom/lge/ims/service/ac/ACService;->getInstance()Lcom/lge/ims/service/ac/ACService;

    #@5
    move-result-object v0

    #@6
    .line 872
    .local v0, acs:Lcom/lge/ims/service/ac/ACService;
    const-string v2, "UC"

    #@8
    const-string v3, "updateEnableFromAC"

    #@a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 874
    if-eqz v0, :cond_29

    #@f
    .line 875
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/ACService;->getProvisionedServices()I

    #@12
    move-result v1

    #@13
    .line 877
    .local v1, serviceAvailability:I
    and-int/lit8 v2, v1, 0x1

    #@15
    if-eqz v2, :cond_2a

    #@17
    .line 879
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getVoIPEnabledStr()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {p0, v2, v5}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcProvider(Ljava/lang/String;I)V

    #@1e
    .line 886
    :goto_1e
    iget-boolean v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->bSvcNotProvisioned:Z

    #@20
    if-eqz v2, :cond_32

    #@22
    .line 887
    const-string v2, "UC"

    #@24
    const-string v3, "Received the Service not provisioned, value of vt_enabled is not updated."

    #@26
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 899
    .end local v1           #serviceAvailability:I
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 882
    .restart local v1       #serviceAvailability:I
    :cond_2a
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getVoIPEnabledStr()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {p0, v2, v4}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcProvider(Ljava/lang/String;I)V

    #@31
    goto :goto_1e

    #@32
    .line 891
    :cond_32
    and-int/lit16 v2, v1, 0x100

    #@34
    if-eqz v2, :cond_3e

    #@36
    .line 893
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getVTEnabledStr()Ljava/lang/String;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {p0, v2, v5}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcProvider(Ljava/lang/String;I)V

    #@3d
    goto :goto_29

    #@3e
    .line 896
    :cond_3e
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getVTEnabledStr()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {p0, v2, v4}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcProvider(Ljava/lang/String;I)V

    #@45
    goto :goto_29
.end method

.method public updateSession(Lcom/lge/ims/service/uc/UCSessionImpl;I)V
    .registers 10
    .parameter "session"
    .parameter "callType"

    #@0
    .prologue
    .line 393
    const/4 v2, 0x0

    #@1
    .line 395
    .local v2, updated:Z
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v3

    #@8
    if-ge v1, v3, :cond_22

    #@a
    .line 396
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@12
    .line 398
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_17

    #@14
    .line 395
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_2

    #@17
    .line 402
    :cond_17
    iget-object v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@19
    if-ne v3, p1, :cond_14

    #@1b
    .line 403
    iget v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@1d
    if-eq v3, p2, :cond_22

    #@1f
    .line 404
    iput p2, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@21
    .line 405
    const/4 v2, 0x1

    #@22
    .line 411
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    :cond_22
    const/4 v3, 0x1

    #@23
    if-ne v2, v3, :cond_2e

    #@25
    .line 412
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@27
    const/16 v4, 0x3e8

    #@29
    const-wide/16 v5, 0xa

    #@2b
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@2e
    .line 414
    :cond_2e
    return-void
.end method

.method public updateTerminatedSession(Lcom/lge/ims/service/uc/UCSessionImpl;)V
    .registers 9
    .parameter "session"

    #@0
    .prologue
    .line 417
    const/4 v2, 0x0

    #@1
    .line 419
    .local v2, updated:Z
    const/4 v1, 0x0

    #@2
    .local v1, i:I
    :goto_2
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7
    move-result v3

    #@8
    if-ge v1, v3, :cond_22

    #@a
    .line 420
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mCallNode:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;

    #@12
    .line 422
    .local v0, callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    if-nez v0, :cond_17

    #@14
    .line 419
    :cond_14
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_2

    #@17
    .line 426
    :cond_17
    iget-object v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@19
    if-ne v3, p1, :cond_14

    #@1b
    .line 427
    const/4 v2, 0x1

    #@1c
    .line 428
    const/4 v3, -0x1

    #@1d
    iput v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@1f
    .line 429
    const/4 v3, 0x0

    #@20
    iput v3, v0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallState:I

    #@22
    .line 434
    .end local v0           #callNode:Lcom/lge/ims/service/uc/UCCallManager$CallNode;
    :cond_22
    if-eqz v2, :cond_2d

    #@24
    .line 435
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mHandler:Landroid/os/Handler;

    #@26
    const/16 v4, 0x3e8

    #@28
    const-wide/16 v5, 0xa

    #@2a
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@2d
    .line 437
    :cond_2d
    return-void
.end method

.method public updateUcProvider(Ljava/lang/String;I)V
    .registers 12
    .parameter "key"
    .parameter "value"

    #@0
    .prologue
    .line 805
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@2
    if-nez v1, :cond_5

    #@4
    .line 843
    :cond_4
    :goto_4
    return-void

    #@5
    .line 810
    :cond_5
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mContext:Landroid/content/Context;

    #@7
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    .line 811
    .local v0, cr:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@c
    .line 813
    .local v6, cursor:Landroid/database/Cursor;
    const-string v1, "UC"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "updateUcProvider"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, "="

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 816
    :try_start_2e
    sget-object v1, Lcom/lge/ims/provider/uc/UC$Data;->CONTENT_URI:Landroid/net/Uri;

    #@30
    const/4 v2, 0x0

    #@31
    const/4 v3, 0x0

    #@32
    const/4 v4, 0x0

    #@33
    const/4 v5, 0x0

    #@34
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@37
    move-result-object v6

    #@38
    .line 818
    if-nez v6, :cond_47

    #@3a
    .line 819
    const-string v1, "UC"

    #@3c
    const-string v2, "Cursor is null"

    #@3e
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_41
    .catchall {:try_start_2e .. :try_end_41} :catchall_a5
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_41} :catch_85

    #@41
    .line 839
    if-eqz v6, :cond_4

    #@43
    .line 840
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@46
    goto :goto_4

    #@47
    .line 823
    :cond_47
    :try_start_47
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@4a
    move-result v1

    #@4b
    if-lez v1, :cond_7d

    #@4d
    .line 824
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@50
    move-result v1

    #@51
    if-nez v1, :cond_60

    #@53
    .line 825
    const-string v1, "UC"

    #@55
    const-string v2, "first move is null"

    #@57
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5a
    .catchall {:try_start_47 .. :try_end_5a} :catchall_a5
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_5a} :catch_85

    #@5a
    .line 839
    if-eqz v6, :cond_4

    #@5c
    .line 840
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@5f
    goto :goto_4

    #@60
    .line 829
    :cond_60
    :try_start_60
    new-instance v8, Landroid/content/ContentValues;

    #@62
    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    #@65
    .line 830
    .local v8, newValues:Landroid/content/ContentValues;
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@68
    move-result-object v1

    #@69
    invoke-virtual {v1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    #@6c
    move-result-object v1

    #@6d
    invoke-virtual {v8, p1, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@70
    .line 832
    sget-object v1, Lcom/lge/ims/provider/uc/UC$Data;->CONTENT_URI:Landroid/net/Uri;

    #@72
    const/4 v2, 0x0

    #@73
    const/4 v3, 0x0

    #@74
    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_77
    .catchall {:try_start_60 .. :try_end_77} :catchall_a5
    .catch Ljava/lang/Exception; {:try_start_60 .. :try_end_77} :catch_85

    #@77
    .line 839
    .end local v8           #newValues:Landroid/content/ContentValues;
    :goto_77
    if-eqz v6, :cond_4

    #@79
    .line 840
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7c
    goto :goto_4

    #@7d
    .line 834
    :cond_7d
    :try_start_7d
    const-string v1, "UC"

    #@7f
    const-string v2, "Cursor count is invalid"

    #@81
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_84
    .catchall {:try_start_7d .. :try_end_84} :catchall_a5
    .catch Ljava/lang/Exception; {:try_start_7d .. :try_end_84} :catch_85

    #@84
    goto :goto_77

    #@85
    .line 836
    :catch_85
    move-exception v7

    #@86
    .line 837
    .local v7, e:Ljava/lang/Exception;
    :try_start_86
    const-string v1, "UC"

    #@88
    new-instance v2, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v3, "Exception = "

    #@8f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v2

    #@93
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v2

    #@97
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9a
    move-result-object v2

    #@9b
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_9e
    .catchall {:try_start_86 .. :try_end_9e} :catchall_a5

    #@9e
    .line 839
    if-eqz v6, :cond_4

    #@a0
    .line 840
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@a3
    goto/16 :goto_4

    #@a5
    .line 839
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_a5
    move-exception v1

    #@a6
    if-eqz v6, :cond_ab

    #@a8
    .line 840
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@ab
    :cond_ab
    throw v1
.end method

.method public updateUcReg(I)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    .line 795
    iget v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mUcRegState:I

    #@2
    if-ne p1, v0, :cond_5

    #@4
    .line 802
    :goto_4
    return-void

    #@5
    .line 799
    :cond_5
    iput p1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mUcRegState:I

    #@7
    .line 800
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getUcRegStateStr()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mUcRegState:I

    #@d
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcProvider(Ljava/lang/String;I)V

    #@10
    .line 801
    const-string v1, "com.lge.ims.action.REG_STATE"

    #@12
    const-string v2, "state"

    #@14
    iget v3, p0, Lcom/lge/ims/service/uc/UCCallManager;->mUcRegState:I

    #@16
    const-string v4, "svcType"

    #@18
    const-string v5, "UC"

    #@1a
    move-object v0, p0

    #@1b
    invoke-virtual/range {v0 .. v5}, Lcom/lge/ims/service/uc/UCCallManager;->sendIntent(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    #@1e
    goto :goto_4
.end method

.method public updateVTService(I)V
    .registers 4
    .parameter "serviceState"

    #@0
    .prologue
    .line 726
    const/4 v0, 0x2

    #@1
    if-ne p1, v0, :cond_15

    #@3
    .line 727
    const-string v0, "UC"

    #@5
    const-string v1, "Received the Service not provisioned of REGISTER request(403/404/410), value of vt_enabled is changed to 0. It\'ll not be updated until power cycle."

    #@7
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 728
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->bSvcNotProvisioned:Z

    #@d
    .line 729
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getVTEnabledStr()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    const/4 v1, 0x0

    #@12
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcProvider(Ljava/lang/String;I)V

    #@15
    .line 732
    :cond_15
    return-void
.end method

.method public updateVoLTEService(I)V
    .registers 5
    .parameter "ServiceState"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 709
    const/4 v0, 0x2

    #@2
    if-ne p1, v0, :cond_18

    #@4
    .line 710
    const-string v0, "UC"

    #@6
    const-string v1, "updateVoLTEService() :: volte service is Unsupported"

    #@8
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 711
    const/4 v0, 0x0

    #@c
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoLTEService:I

    #@e
    .line 712
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getVoIPEnabledStr()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    iget v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoLTEService:I

    #@14
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcProvider(Ljava/lang/String;I)V

    #@17
    .line 722
    :cond_17
    :goto_17
    return-void

    #@18
    .line 716
    :cond_18
    if-ne p1, v2, :cond_17

    #@1a
    iget v0, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoLTEService:I

    #@1c
    if-nez v0, :cond_17

    #@1e
    .line 717
    const-string v0, "UC"

    #@20
    const-string v1, "updateVoLTEService() :: volte service is Supported"

    #@22
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 718
    iput v2, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoLTEService:I

    #@27
    .line 719
    invoke-static {}, Lcom/lge/ims/provider/uc/UC$Data;->getVoIPEnabledStr()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    iget v1, p0, Lcom/lge/ims/service/uc/UCCallManager;->mVoLTEService:I

    #@2d
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcProvider(Ljava/lang/String;I)V

    #@30
    goto :goto_17
.end method
