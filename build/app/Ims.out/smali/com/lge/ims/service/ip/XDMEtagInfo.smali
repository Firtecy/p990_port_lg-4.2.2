.class public Lcom/lge/ims/service/ip/XDMEtagInfo;
.super Ljava/lang/Object;
.source "XDMEtagInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/ip/XDMEtagInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private szlistEtag:Ljava/lang/String;

.field private szpidfEtag:Ljava/lang/String;

.field private szrlsEtag:Ljava/lang/String;

.field private szruleEtag:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 25
    new-instance v0, Lcom/lge/ims/service/ip/XDMEtagInfo$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/ip/XDMEtagInfo$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/ip/XDMEtagInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 39
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 40
    iput-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szlistEtag:Ljava/lang/String;

    #@6
    .line 41
    iput-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szrlsEtag:Ljava/lang/String;

    #@8
    .line 42
    iput-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szruleEtag:Ljava/lang/String;

    #@a
    .line 43
    iput-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szpidfEtag:Ljava/lang/String;

    #@c
    .line 44
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 45
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@6
    .line 47
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/lge/ims/service/ip/XDMEtagInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public GetListEtag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szlistEtag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetPidfEtag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szpidfEtag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetRLSEtag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szrlsEtag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetRuleEtag()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 85
    iget-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szruleEtag:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public SetListEtag(Ljava/lang/String;)V
    .registers 2
    .parameter "_szlistEtag"

    #@0
    .prologue
    .line 74
    iput-object p1, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szlistEtag:Ljava/lang/String;

    #@2
    .line 75
    return-void
.end method

.method public SetPidfEtag(Ljava/lang/String;)V
    .registers 2
    .parameter "_szpidfEtag"

    #@0
    .prologue
    .line 97
    iput-object p1, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szpidfEtag:Ljava/lang/String;

    #@2
    .line 98
    return-void
.end method

.method public SetRLSEtag(Ljava/lang/String;)V
    .registers 2
    .parameter "_szrlsEtag"

    #@0
    .prologue
    .line 81
    iput-object p1, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szrlsEtag:Ljava/lang/String;

    #@2
    .line 82
    return-void
.end method

.method public SetRuleEtag(Ljava/lang/String;)V
    .registers 2
    .parameter "_szruleEtag"

    #@0
    .prologue
    .line 89
    iput-object p1, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szruleEtag:Ljava/lang/String;

    #@2
    .line 90
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 49
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 64
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szlistEtag:Ljava/lang/String;

    #@6
    .line 65
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szrlsEtag:Ljava/lang/String;

    #@c
    .line 66
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szruleEtag:Ljava/lang/String;

    #@12
    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szpidfEtag:Ljava/lang/String;

    #@18
    .line 68
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "XDMEtagInfo - szlistEtag ["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szlistEtag:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "], szrlsEtag["

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szrlsEtag:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, "], szruleEtag["

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-object v1, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szruleEtag:Ljava/lang/String;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    const-string v1, "], szpidfEtag["

    #@2b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v0

    #@2f
    iget-object v1, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szpidfEtag:Ljava/lang/String;

    #@31
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v0

    #@35
    const-string v1, "]"

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v0

    #@3f
    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szlistEtag:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 53
    iget-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szrlsEtag:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 54
    iget-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szruleEtag:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 55
    iget-object v0, p0, Lcom/lge/ims/service/ip/XDMEtagInfo;->szpidfEtag:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 56
    return-void
.end method
