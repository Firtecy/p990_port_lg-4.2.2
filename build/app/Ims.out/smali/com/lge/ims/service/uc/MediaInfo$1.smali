.class final Lcom/lge/ims/service/uc/MediaInfo$1;
.super Ljava/lang/Object;
.source "MediaInfo.java"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/MediaInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lcom/lge/ims/service/uc/MediaInfo;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 89
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;)Lcom/lge/ims/service/uc/MediaInfo;
    .registers 5
    .parameter "source"

    #@0
    .prologue
    .line 92
    :try_start_0
    new-instance v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@2
    invoke-direct {v1, p1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 96
    :goto_5
    return-object v1

    #@6
    .line 93
    :catch_6
    move-exception v0

    #@7
    .line 94
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "LGIMS"

    #@9
    const-string v2, "createFromParcel() :: Exception occurred when creating MediaInfo from parcel"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    .line 96
    const/4 v1, 0x0

    #@f
    goto :goto_5
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/uc/MediaInfo$1;->createFromParcel(Landroid/os/Parcel;)Lcom/lge/ims/service/uc/MediaInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public newArray(I)[Lcom/lge/ims/service/uc/MediaInfo;
    .registers 3
    .parameter "size"

    #@0
    .prologue
    .line 100
    new-array v0, p1, [Lcom/lge/ims/service/uc/MediaInfo;

    #@2
    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/uc/MediaInfo$1;->newArray(I)[Lcom/lge/ims/service/uc/MediaInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method
