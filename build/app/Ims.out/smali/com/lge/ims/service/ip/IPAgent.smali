.class public Lcom/lge/ims/service/ip/IPAgent;
.super Ljava/lang/Thread;
.source "IPAgent.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;,
        Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;,
        Lcom/lge/ims/service/ip/IPAgent$IPTimer;
    }
.end annotation


# static fields
.field private static final IP_STATE_SERVICE_OFF:I = 0x0

.field private static final IP_STATE_SERVICE_OFF_BG_OFF:I = 0x1

.field private static final IP_STATE_SERVICE_ON:I = 0x3

.field private static final IP_STATE_SERVICE_ON_BG_OFF:I = 0x2

.field public static bBootCompleted:Z

.field private static objAlarmManager:Landroid/app/AlarmManager;

.field private static objIPAgent:Lcom/lge/ims/service/ip/IPAgent;


# instance fields
.field private bIsFirst:Z

.field private bIsObserverRegistered:Z

.field private bSyncFailedReceived:Z

.field private bXDMConnected:Z

.field private listDeleteQueuingMsisdn:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;

.field nCapaInfoExpireTimer:I

.field nLimitationResourceListUserCount:I

.field nListCapanfoExpireTimer:I

.field nOneContactIntervalTimer:I

.field nPollingTimer:I

.field nRLSSubPeriodTimer:I

.field private nState:I

.field nUserPresence:I

.field nUserPresenceSocialInformation:I

.field private objAddressBookObserver:Landroid/database/ContentObserver;

.field private objBGHelper:Lcom/lge/ims/service/ip/IPBGHelper;

.field private objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

.field private objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

.field private objContext:Landroid/content/Context;

.field private objIPAgentHandler:Landroid/os/Handler;

.field public objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

.field objIPBroadcastReceiver:Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;

.field public objIPCapaMngr:Lcom/lge/ims/service/ip/IPCapaMngr;

.field private objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

.field private objObserverCursor:Landroid/database/Cursor;

.field private objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 59
    sput-object v1, Lcom/lge/ims/service/ip/IPAgent;->objIPAgent:Lcom/lge/ims/service/ip/IPAgent;

    #@3
    .line 88
    const/4 v0, 0x0

    #@4
    sput-boolean v0, Lcom/lge/ims/service/ip/IPAgent;->bBootCompleted:Z

    #@6
    .line 1666
    sput-object v1, Lcom/lge/ims/service/ip/IPAgent;->objAlarmManager:Landroid/app/AlarmManager;

    #@8
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "_objContext"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 180
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 61
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContext:Landroid/content/Context;

    #@7
    .line 62
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentHandler:Landroid/os/Handler;

    #@9
    .line 72
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@b
    .line 85
    iput v1, p0, Lcom/lge/ims/service/ip/IPAgent;->nState:I

    #@d
    .line 87
    iput-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsFirst:Z

    #@f
    .line 90
    iput-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@11
    .line 91
    iput-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@13
    .line 105
    new-instance v0, Ljava/util/ArrayList;

    #@15
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@18
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@1a
    .line 1844
    iput-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsObserverRegistered:Z

    #@1c
    .line 1876
    new-instance v0, Lcom/lge/ims/service/ip/IPAgent$2;

    #@1e
    new-instance v1, Landroid/os/Handler;

    #@20
    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    #@23
    invoke-direct {v0, p0, v1}, Lcom/lge/ims/service/ip/IPAgent$2;-><init>(Lcom/lge/ims/service/ip/IPAgent;Landroid/os/Handler;)V

    #@26
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objAddressBookObserver:Landroid/database/ContentObserver;

    #@28
    .line 181
    const-string v0, "IPAgent"

    #@2a
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->setName(Ljava/lang/String;)V

    #@2d
    .line 182
    if-nez p1, :cond_35

    #@2f
    .line 183
    const-string v0, "[IP][ERROR]IPAgent : _objContext is null"

    #@31
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@34
    .line 190
    :goto_34
    return-void

    #@35
    .line 186
    :cond_35
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPAgent;->objContext:Landroid/content/Context;

    #@37
    .line 188
    new-instance v0, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@39
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;-><init>(Lcom/lge/ims/service/ip/IPAgent;)V

    #@3c
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@3e
    .line 189
    new-instance v0, Lcom/lge/ims/service/ip/IPBGHelper;

    #@40
    invoke-direct {v0, p1}, Lcom/lge/ims/service/ip/IPBGHelper;-><init>(Landroid/content/Context;)V

    #@43
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objBGHelper:Lcom/lge/ims/service/ip/IPBGHelper;

    #@45
    goto :goto_34
.end method

.method public static CreateIPAgent(Landroid/content/Context;)V
    .registers 2
    .parameter "_objContext"

    #@0
    .prologue
    .line 120
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 121
    if-nez p0, :cond_d

    #@7
    .line 122
    const-string v0, "[IP][ERROR]CreateIPAgent : context is null"

    #@9
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@c
    .line 151
    :cond_c
    :goto_c
    return-void

    #@d
    .line 125
    :cond_d
    sget-object v0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgent:Lcom/lge/ims/service/ip/IPAgent;

    #@f
    if-nez v0, :cond_c

    #@11
    .line 126
    new-instance v0, Lcom/lge/ims/service/ip/IPAgent;

    #@13
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPAgent;-><init>(Landroid/content/Context;)V

    #@16
    sput-object v0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgent:Lcom/lge/ims/service/ip/IPAgent;

    #@18
    goto :goto_c
.end method

.method private Deinitialize()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 390
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@3
    .line 392
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent;->UnregisterBroadcastReceiver()V

    #@6
    .line 394
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsObserverRegistered:Z

    #@8
    if-eqz v0, :cond_d

    #@a
    .line 395
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent;->DeregisterObserver()V

    #@d
    .line 398
    :cond_d
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@f
    if-eqz v0, :cond_13

    #@11
    .line 399
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@13
    .line 402
    :cond_13
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objBGHelper:Lcom/lge/ims/service/ip/IPBGHelper;

    #@15
    .line 404
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@17
    if-eqz v0, :cond_1b

    #@19
    .line 405
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@1b
    .line 407
    :cond_1b
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1d
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->terminate()V

    #@20
    .line 408
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@22
    .line 410
    const/4 v0, 0x0

    #@23
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@26
    .line 411
    return-void
.end method

.method private DeregisterObserver()V
    .registers 3

    #@0
    .prologue
    .line 1867
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsObserverRegistered:Z

    #@2
    if-eqz v0, :cond_11

    #@4
    .line 1868
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objObserverCursor:Landroid/database/Cursor;

    #@6
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objAddressBookObserver:Landroid/database/ContentObserver;

    #@8
    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@b
    .line 1869
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objObserverCursor:Landroid/database/Cursor;

    #@e
    .line 1870
    const/4 v0, 0x0

    #@f
    iput-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsObserverRegistered:Z

    #@11
    .line 1872
    :cond_11
    return-void
.end method

.method public static GetContext()Landroid/content/Context;
    .registers 1

    #@0
    .prologue
    .line 157
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@3
    move-result-object v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 158
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@9
    move-result-object v0

    #@a
    iget-object v0, v0, Lcom/lge/ims/service/ip/IPAgent;->objContext:Landroid/content/Context;

    #@c
    .line 161
    :goto_c
    return-object v0

    #@d
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_c
.end method

.method private GetIMSI()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 456
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objContext:Landroid/content/Context;

    #@2
    const-string v2, "phone"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Landroid/telephony/TelephonyManager;

    #@a
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@c
    .line 458
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@e
    if-nez v1, :cond_18

    #@10
    .line 459
    const-string v1, "[IP][ERROR] TelephonyManager is null"

    #@12
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@15
    .line 460
    const-string v0, "450000000000000"

    #@17
    .line 469
    .local v0, strIMSI:Ljava/lang/String;
    :goto_17
    return-object v0

    #@18
    .line 462
    .end local v0           #strIMSI:Ljava/lang/String;
    :cond_18
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@1a
    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    #@1d
    move-result-object v0

    #@1e
    .line 463
    .restart local v0       #strIMSI:Ljava/lang/String;
    if-eqz v0, :cond_27

    #@20
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    #@23
    move-result v1

    #@24
    const/4 v2, 0x1

    #@25
    if-ne v1, v2, :cond_2a

    #@27
    .line 464
    :cond_27
    const-string v0, "450000000000000"

    #@29
    goto :goto_17

    #@2a
    .line 466
    :cond_2a
    const-string v1, ":"

    #@2c
    const-string v2, "0"

    #@2e
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@31
    move-result-object v0

    #@32
    goto :goto_17
.end method

.method public static GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;
    .registers 1

    #@0
    .prologue
    .line 153
    sget-object v0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgent:Lcom/lge/ims/service/ip/IPAgent;

    #@2
    return-object v0
.end method

.method private GetMessageWhat(I)Ljava/lang/String;
    .registers 3
    .parameter "what"

    #@0
    .prologue
    .line 530
    packed-switch p1, :pswitch_data_6a

    #@3
    .line 631
    :pswitch_3
    const-string v0, "Unknown Message"

    #@5
    .line 634
    .local v0, szWhat:Ljava/lang/String;
    :goto_5
    return-object v0

    #@6
    .line 532
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_6
    const-string v0, "IP_MSG_IMS_AGENT_CONNECTED"

    #@8
    .line 533
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@9
    .line 535
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_9
    const-string v0, "IP_MSG_IMS_AGENT_DISCONNECTED"

    #@b
    .line 536
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@c
    .line 538
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_c
    const-string v0, "IP_MSG_XDM_AGENT_CONNECTED"

    #@e
    .line 539
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@f
    .line 541
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_f
    const-string v0, "IP_MSG_TIMER_AGENT_POLLING_EXPIRED"

    #@11
    .line 542
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@12
    .line 544
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_12
    const-string v0, "IP_MSG_TIMER_AGENT_SUBRLS_POLLING_EXPIRED"

    #@14
    .line 545
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@15
    .line 547
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_15
    const-string v0, "IP_MSG_ADDRBOOK_AGENT_CHANGED"

    #@17
    .line 548
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@18
    .line 550
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_18
    const-string v0, "IP_MSG_BGHELPER_AGENT_ON"

    #@1a
    .line 551
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@1b
    .line 553
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_1b
    const-string v0, "IP_MSG_BGHELPER_AGENT_OFF"

    #@1d
    .line 554
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@1e
    .line 556
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_1e
    const-string v0, "IP_MSG_QUERYREPO_AGENT_EMPTY"

    #@20
    .line 557
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@21
    .line 559
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_21
    const-string v0, "IP_MSG_QUERYPOLLING_AGENT_EMPTY"

    #@23
    .line 560
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@24
    .line 562
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_24
    const-string v0, "IP_MSG_SVC_AGENT_SYNC"

    #@26
    .line 563
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@27
    .line 565
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_27
    const-string v0, "IP_MSG_XDM_AGENT_GETRLSLIST_FAILED"

    #@29
    .line 566
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@2a
    .line 568
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_2a
    const-string v0, "IP_MSG_XDM_AGENT_GETRLSLIST_SUCCESS"

    #@2c
    .line 569
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@2d
    .line 571
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_2d
    const-string v0, "IP_MSG_CONTACT_CAPA_QUERY"

    #@2f
    .line 572
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@30
    .line 574
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_30
    const-string v0, "IP_MSG_CONTACT_ADD_RCSFRIEND"

    #@32
    .line 575
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@33
    .line 577
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_33
    const-string v0, "IP_MSG_CONTACT_DELETE_RCSFRIEND"

    #@35
    .line 578
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@36
    .line 580
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_36
    const-string v0, "IP_MSG_CONTACT_ADD_NONRCSFRIEND"

    #@38
    .line 581
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@39
    .line 583
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_39
    const-string v0, "IP_MSG_CONTACT_DELETE_RCSFRIENDLIST"

    #@3b
    .line 584
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@3c
    .line 586
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_3c
    const-string v0, "IP_MSG_XDM_AGENT_RLS_SYNCFAILED"

    #@3e
    .line 587
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@3f
    .line 589
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_3f
    const-string v0, "IP_MSG_SAVE_RLS_LIST_URI"

    #@41
    .line 590
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@42
    .line 592
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_42
    const-string v0, "IP_MSG_SAVE_MY_STATUS_INFO"

    #@44
    .line 593
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@45
    .line 595
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_45
    const-string v0, "IP_MSG_SAVE_MY_STATUSICON_INFO"

    #@47
    .line 596
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@48
    .line 598
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_48
    const-string v0, "IP_MSG_SAVE_XDM_DOC"

    #@4a
    .line 599
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@4b
    .line 601
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_4b
    const-string v0, "IP_MSG_SAVE_DELETE_RCSUSER"

    #@4d
    .line 602
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@4e
    .line 604
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_4e
    const-string v0, "IP_MSG_SAVE_ADD_RCSUSER"

    #@50
    .line 605
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@51
    .line 607
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_51
    const-string v0, "IP_MSG_SAVE_ADD_NONRCSUSER"

    #@53
    .line 608
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@54
    .line 610
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_54
    const-string v0, "IP_MSG_XDM_AGENT_GET_MYSTATUSICONTHUMB"

    #@56
    .line 611
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@57
    .line 613
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_57
    const-string v0, "IP_MSG_XDM_AGENT_GET_MYSTATUSICON"

    #@59
    .line 614
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@5a
    .line 616
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_5a
    const-string v0, "IP_MSG_XDM_AGENT_GET_BUDDYSTATUSICONTHUMB"

    #@5c
    .line 617
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@5d
    .line 619
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_5d
    const-string v0, "IP_MSG_XDM_AGENT_GET_BUDDYSTATUSICON"

    #@5f
    .line 620
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@60
    .line 622
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_60
    const-string v0, "IP_MSG_CONTACT_ONEUSER_QUERY"

    #@62
    .line 623
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@63
    .line 625
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_63
    const-string v0, "IP_MSG_SAVE_ONE_CONTACTQUERY_FAILED"

    #@65
    .line 626
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@66
    .line 628
    .end local v0           #szWhat:Ljava/lang/String;
    :pswitch_66
    const-string v0, "IP_MSG_SAVE_ONE_CONTACTQUERY_SUCCESS"

    #@68
    .line 629
    .restart local v0       #szWhat:Ljava/lang/String;
    goto :goto_5

    #@69
    .line 530
    nop

    #@6a
    :pswitch_data_6a
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_12
        :pswitch_2d
        :pswitch_30
        :pswitch_33
        :pswitch_36
        :pswitch_60
        :pswitch_39
        :pswitch_3
        :pswitch_3
        :pswitch_15
        :pswitch_18
        :pswitch_1b
        :pswitch_1e
        :pswitch_21
        :pswitch_24
        :pswitch_3
        :pswitch_3
        :pswitch_27
        :pswitch_2a
        :pswitch_3c
        :pswitch_54
        :pswitch_57
        :pswitch_5a
        :pswitch_5d
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3f
        :pswitch_42
        :pswitch_45
        :pswitch_48
        :pswitch_3
        :pswitch_4b
        :pswitch_4e
        :pswitch_51
        :pswitch_63
        :pswitch_66
    .end packed-switch
.end method

.method private GetProvisioningValue()V
    .registers 5

    #@0
    .prologue
    const v3, 0x15180

    #@3
    const/4 v2, 0x1

    #@4
    const/16 v1, 0x1388

    #@6
    .line 414
    const-string v0, "[IP] : GetProvisioningValue"

    #@8
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@b
    .line 415
    iput v3, p0, Lcom/lge/ims/service/ip/IPAgent;->nPollingTimer:I

    #@d
    .line 416
    iput v1, p0, Lcom/lge/ims/service/ip/IPAgent;->nCapaInfoExpireTimer:I

    #@f
    .line 418
    iput v3, p0, Lcom/lge/ims/service/ip/IPAgent;->nRLSSubPeriodTimer:I

    #@11
    .line 419
    iput v1, p0, Lcom/lge/ims/service/ip/IPAgent;->nListCapanfoExpireTimer:I

    #@13
    .line 420
    iput v1, p0, Lcom/lge/ims/service/ip/IPAgent;->nOneContactIntervalTimer:I

    #@15
    .line 422
    iput v2, p0, Lcom/lge/ims/service/ip/IPAgent;->nUserPresence:I

    #@17
    .line 423
    iput v2, p0, Lcom/lge/ims/service/ip/IPAgent;->nUserPresenceSocialInformation:I

    #@19
    .line 424
    const/16 v0, 0x7d0

    #@1b
    iput v0, p0, Lcom/lge/ims/service/ip/IPAgent;->nLimitationResourceListUserCount:I

    #@1d
    .line 450
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1f
    iget v1, p0, Lcom/lge/ims/service/ip/IPAgent;->nUserPresence:I

    #@21
    iget v2, p0, Lcom/lge/ims/service/ip/IPAgent;->nUserPresenceSocialInformation:I

    #@23
    invoke-virtual {v0, v1, v2}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->setValue(II)V

    #@26
    .line 451
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@28
    iget v1, p0, Lcom/lge/ims/service/ip/IPAgent;->nLimitationResourceListUserCount:I

    #@2a
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->SetLimitationResourceListUserCount(I)Z

    #@2d
    .line 452
    return-void
.end method

.method private GetState()I
    .registers 2

    #@0
    .prologue
    .line 525
    iget v0, p0, Lcom/lge/ims/service/ip/IPAgent;->nState:I

    #@2
    return v0
.end method

.method private GetStateName(I)Ljava/lang/String;
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 639
    packed-switch p1, :pswitch_data_12

    #@3
    .line 653
    const-string v0, "Unknown State"

    #@5
    .line 656
    .local v0, szState:Ljava/lang/String;
    :goto_5
    return-object v0

    #@6
    .line 641
    .end local v0           #szState:Ljava/lang/String;
    :pswitch_6
    const-string v0, "IP_STATE_SERVICE_OFF"

    #@8
    .line 642
    .restart local v0       #szState:Ljava/lang/String;
    goto :goto_5

    #@9
    .line 644
    .end local v0           #szState:Ljava/lang/String;
    :pswitch_9
    const-string v0, "IP_STATE_SERVICE_OFF_BG_OFF"

    #@b
    .line 645
    .restart local v0       #szState:Ljava/lang/String;
    goto :goto_5

    #@c
    .line 647
    .end local v0           #szState:Ljava/lang/String;
    :pswitch_c
    const-string v0, "IP_STATE_SERVICE_ON_BG_OFF"

    #@e
    .line 648
    .restart local v0       #szState:Ljava/lang/String;
    goto :goto_5

    #@f
    .line 650
    .end local v0           #szState:Ljava/lang/String;
    :pswitch_f
    const-string v0, "IP_STATE_SERVICE_ON"

    #@11
    .line 651
    .restart local v0       #szState:Ljava/lang/String;
    goto :goto_5

    #@12
    .line 639
    :pswitch_data_12
    .packed-switch 0x0
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
    .end packed-switch
.end method

.method private Initialize()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 356
    new-instance v1, Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@4
    invoke-direct {v1, p0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;-><init>(Lcom/lge/ims/service/ip/IPAgent;)V

    #@7
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@9
    .line 359
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objContext:Landroid/content/Context;

    #@b
    invoke-static {v1}, Lcom/lge/ims/service/ip/IPContactHelper;->CreateInstance(Landroid/content/Context;)V

    #@e
    .line 360
    invoke-static {}, Lcom/lge/ims/service/ip/IPContactHelper;->GetInstance()Lcom/lge/ims/service/ip/IPContactHelper;

    #@11
    move-result-object v1

    #@12
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@14
    .line 363
    iget-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsObserverRegistered:Z

    #@16
    if-nez v1, :cond_1b

    #@18
    .line 364
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent;->RegisterObserver()V

    #@1b
    .line 367
    :cond_1b
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent;->RegisterBroadcastReceiver()V

    #@1e
    .line 369
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@20
    if-nez v1, :cond_2a

    #@22
    .line 370
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objContext:Landroid/content/Context;

    #@24
    invoke-static {v1}, Lcom/lge/ims/service/ip/IPProvisioningInfo;->GetInstance(Landroid/content/Context;)Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@27
    move-result-object v1

    #@28
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objProvisioningInfo:Lcom/lge/ims/service/ip/IPProvisioningInfo;

    #@2a
    .line 372
    :cond_2a
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@2c
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPContactHelper;->GetFirstPollingLatestTime()J

    #@2f
    move-result-wide v1

    #@30
    const-wide/16 v3, 0x0

    #@32
    cmp-long v1, v1, v3

    #@34
    if-nez v1, :cond_3d

    #@36
    .line 373
    const-string v1, "[IP]First Polling"

    #@38
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3b
    .line 374
    iput-boolean v6, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsFirst:Z

    #@3d
    .line 376
    :cond_3d
    new-instance v0, Ljava/io/File;

    #@3f
    const-string v1, "/data/data/com.lge.ims/image"

    #@41
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@44
    .line 377
    .local v0, copyFile:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    #@47
    .line 378
    invoke-virtual {v0, v6, v5}, Ljava/io/File;->setReadable(ZZ)Z

    #@4a
    .line 379
    invoke-virtual {v0, v6, v5}, Ljava/io/File;->setWritable(ZZ)Z

    #@4d
    .line 380
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@4f
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPContactHelper;->SetFirstTimeRcs()V

    #@52
    .line 381
    invoke-direct {p0, v5}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@55
    .line 382
    return-void
.end method

.method private RegisterBroadcastReceiver()V
    .registers 4

    #@0
    .prologue
    .line 477
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPBroadcastReceiver:Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;

    #@2
    if-nez v1, :cond_58

    #@4
    .line 478
    new-instance v1, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;

    #@6
    invoke-direct {v1, p0}, Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;-><init>(Lcom/lge/ims/service/ip/IPAgent;)V

    #@9
    iput-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPBroadcastReceiver:Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;

    #@b
    .line 480
    new-instance v0, Landroid/content/IntentFilter;

    #@d
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@10
    .line 481
    .local v0, objIntentFilter:Landroid/content/IntentFilter;
    const-string v1, "com.lge.ims.rcs.query_capa"

    #@12
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@15
    .line 482
    const-string v1, "com.lge.ims.rcs.query_capa_id"

    #@17
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1a
    .line 483
    const-string v1, "com.lge.ims.rcs.query_list"

    #@1c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1f
    .line 484
    const-string v1, "com.lge.ims.rcs.my_status_change"

    #@21
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@24
    .line 485
    const-string v1, "com.lge.ims.rcs.my_icon_change"

    #@26
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@29
    .line 486
    const-string v1, "com.lge.ims.rcs.my_icon_delete"

    #@2b
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2e
    .line 487
    const-string v1, "com.lge.ims.rcs.invite_user"

    #@30
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@33
    .line 488
    const-string v1, "com.lge.ims.rcs.get_buddy_icon"

    #@35
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@38
    .line 489
    const-string v1, "com.lge.ims.rcs.photo_update"

    #@3a
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@3d
    .line 491
    const-string v1, "com.lge.ims.action.IMS_EVENT_CONFIG_UPDATE_COMPLETE"

    #@3f
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@42
    .line 492
    const-string v1, "android.intent.action.SCREEN_OFF"

    #@44
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@47
    .line 493
    const-string v1, "android.intent.action.SCREEN_ON"

    #@49
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4c
    .line 495
    const-string v1, "com.lge.ims.ip.polling"

    #@4e
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@51
    .line 496
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objContext:Landroid/content/Context;

    #@53
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPBroadcastReceiver:Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;

    #@55
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@58
    .line 498
    .end local v0           #objIntentFilter:Landroid/content/IntentFilter;
    :cond_58
    return-void
.end method

.method private RegisterObserver()V
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1852
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsObserverRegistered:Z

    #@3
    if-nez v0, :cond_20

    #@5
    .line 1853
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContext:Landroid/content/Context;

    #@7
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@d
    const-string v3, "phones"

    #@f
    invoke-static {v1, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@12
    move-result-object v1

    #@13
    move-object v3, v2

    #@14
    move-object v4, v2

    #@15
    move-object v5, v2

    #@16
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objObserverCursor:Landroid/database/Cursor;

    #@1c
    .line 1856
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objObserverCursor:Landroid/database/Cursor;

    #@1e
    if-nez v0, :cond_21

    #@20
    .line 1862
    :cond_20
    :goto_20
    return-void

    #@21
    .line 1859
    :cond_21
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objObserverCursor:Landroid/database/Cursor;

    #@23
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objAddressBookObserver:Landroid/database/ContentObserver;

    #@25
    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    #@28
    .line 1860
    const/4 v0, 0x1

    #@29
    iput-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsObserverRegistered:Z

    #@2b
    goto :goto_20
.end method

.method private SendNOptions(Z)V
    .registers 9
    .parameter "bPollingTimerExpired"

    #@0
    .prologue
    const/16 v6, 0xa

    #@2
    .line 1638
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "[IP]:bPollingTimerExpired ["

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, "]"

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v4

    #@1b
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1e
    .line 1639
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@20
    if-nez v4, :cond_23

    #@22
    .line 1661
    :goto_22
    return-void

    #@23
    .line 1642
    :cond_23
    const/4 v2, 0x0

    #@24
    .line 1643
    .local v2, nSent:I
    new-instance v3, Ljava/util/ArrayList;

    #@26
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@29
    .line 1644
    .local v3, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->clear()V

    #@2c
    .line 1645
    if-eqz p1, :cond_50

    #@2e
    .line 1646
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@30
    iget v5, p0, Lcom/lge/ims/service/ip/IPAgent;->nPollingTimer:I

    #@32
    invoke-virtual {v4, v3, v6, v5}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSEPollingContacts(Ljava/util/List;II)Z

    #@35
    move-result v4

    #@36
    if-eqz v4, :cond_70

    #@38
    .line 1647
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@3b
    move-result-object v1

    #@3c
    .local v1, i$:Ljava/util/Iterator;
    :goto_3c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@3f
    move-result v4

    #@40
    if-eqz v4, :cond_70

    #@42
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@45
    move-result-object v0

    #@46
    check-cast v0, Ljava/lang/String;

    #@48
    .line 1648
    .local v0, MSISDN:Ljava/lang/String;
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@4a
    invoke-virtual {v4, v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendCapaQuery(Ljava/lang/String;)V

    #@4d
    .line 1649
    add-int/lit8 v2, v2, 0x1

    #@4f
    goto :goto_3c

    #@50
    .line 1653
    .end local v0           #MSISDN:Ljava/lang/String;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_50
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@52
    invoke-virtual {v4, v3, v6}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSETempContacts(Ljava/util/List;I)Z

    #@55
    move-result v4

    #@56
    if-eqz v4, :cond_70

    #@58
    .line 1654
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5b
    move-result-object v1

    #@5c
    .restart local v1       #i$:Ljava/util/Iterator;
    :goto_5c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@5f
    move-result v4

    #@60
    if-eqz v4, :cond_70

    #@62
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@65
    move-result-object v0

    #@66
    check-cast v0, Ljava/lang/String;

    #@68
    .line 1655
    .restart local v0       #MSISDN:Ljava/lang/String;
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@6a
    invoke-virtual {v4, v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendCapaQuery(Ljava/lang/String;)V

    #@6d
    .line 1656
    add-int/lit8 v2, v2, 0x1

    #@6f
    goto :goto_5c

    #@70
    .line 1660
    .end local v0           #MSISDN:Ljava/lang/String;
    .end local v1           #i$:Ljava/util/Iterator;
    :cond_70
    new-instance v4, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v5, "[IP]send "

    #@77
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v4

    #@7b
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v4

    #@7f
    const-string v5, " options"

    #@81
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v4

    #@85
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v4

    #@89
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8c
    goto :goto_22
.end method

.method private SetState(I)V
    .registers 4
    .parameter "_nState"

    #@0
    .prologue
    .line 672
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP]IPAgent: SetState - Old State["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget v1, p0, Lcom/lge/ims/service/ip/IPAgent;->nState:I

    #@d
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ip/IPAgent;->GetStateName(I)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    const-string v1, "], New State["

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->GetStateName(I)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    const-string v1, "]"

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@30
    .line 673
    iput p1, p0, Lcom/lge/ims/service/ip/IPAgent;->nState:I

    #@32
    .line 674
    return-void
.end method

.method private StatePreprocess_IPMsgSvcAgentSync(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 946
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 947
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 948
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPContactHelper;->Sync()Z

    #@e
    .line 950
    :cond_e
    const/4 v0, 0x1

    #@f
    return v0
.end method

.method private StatePreprocess_IPMsgXdmAgentGetRLSListFailed(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 953
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 955
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method private StatePreprocess_IPMsgXdmAgentGetRLSListSuccess(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 958
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 960
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method private StateSERVICEOFFBGOFF_IPMsgAddressBookAgentChanged(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1139
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1144
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method private StateSERVICEOFFBGOFF_IPMsgBGHelperAgentOn(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1147
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1148
    const/4 v0, 0x0

    #@6
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@9
    .line 1149
    const/4 v0, 0x1

    #@a
    return v0
.end method

.method private StateSERVICEOFFBGOFF_IPMsgContactDeleteRcsFriendList(Landroid/os/Message;)Z
    .registers 8
    .parameter "objMessage"

    #@0
    .prologue
    .line 1152
    const-string v4, "[IP]"

    #@2
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1153
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@8
    move-result-object v2

    #@9
    .line 1154
    .local v2, objBundle:Landroid/os/Bundle;
    const-string v4, "delete_rcsuser"

    #@b
    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@e
    move-result-object v3

    #@f
    check-cast v3, Lcom/lge/ims/service/ip/ResourceListUserInfo;

    #@11
    .line 1155
    .local v3, objResourceListUserInfo:Lcom/lge/ims/service/ip/ResourceListUserInfo;
    if-eqz v3, :cond_32

    #@13
    .line 1156
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetCurrentResourceListUserSize()I

    #@16
    move-result v1

    #@17
    .line 1157
    .local v1, nCurrentSize:I
    const/4 v0, 0x0

    #@18
    .local v0, n:I
    :goto_18
    if-ge v0, v1, :cond_32

    #@1a
    .line 1158
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@1c
    invoke-virtual {v3, v0}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetResourceListUser(I)Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@23
    move-result v4

    #@24
    if-nez v4, :cond_2f

    #@26
    .line 1159
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@28
    invoke-virtual {v3, v0}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetResourceListUser(I)Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2f
    .line 1157
    :cond_2f
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_18

    #@32
    .line 1163
    .end local v0           #n:I
    .end local v1           #nCurrentSize:I
    :cond_32
    const/4 v4, 0x1

    #@33
    return v4
.end method

.method private StateSERVICEOFFBGOFF_IPMsgImsAgentConnected(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1075
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1076
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 1077
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPContactHelper;->Sync()Z

    #@f
    .line 1080
    :cond_f
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@11
    if-eq v0, v1, :cond_18

    #@13
    .line 1081
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@15
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->StartXDMAgent()V

    #@18
    .line 1083
    :cond_18
    const/4 v0, 0x2

    #@19
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@1c
    .line 1084
    return v1
.end method

.method private StateSERVICEOFFBGOFF_IPMsgImsAgentDisconnected(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1087
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1089
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 1090
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopPollingTimer()V

    #@f
    .line 1093
    :cond_f
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@11
    if-ne v0, v1, :cond_13

    #@13
    .line 1096
    :cond_13
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@16
    .line 1097
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPQueryRepository;->ClearQueryList()V

    #@1d
    .line 1098
    return v1
.end method

.method private StateSERVICEOFFBGOFF_IPMsgTimerAGentPollingExpired(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1125
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1126
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1127
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopPollingTimer()V

    #@e
    .line 1129
    :cond_e
    const/4 v0, 0x1

    #@f
    return v0
.end method

.method private StateSERVICEOFFBGOFF_IPMsgTimerAGentRLSSubPollingExpired(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1132
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1133
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1134
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopRLSSubPollingTimer()V

    #@e
    .line 1136
    :cond_e
    const/4 v0, 0x1

    #@f
    return v0
.end method

.method private StateSERVICEOFFBGOFF_IPMsgXdmAgentConnected(Landroid/os/Message;)Z
    .registers 6
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1101
    const-string v1, "[IP]"

    #@3
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1102
    iput-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    .line 1103
    iget-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@a
    if-ne v1, v3, :cond_24

    #@c
    .line 1104
    const/4 v1, 0x0

    #@d
    iput-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@f
    .line 1105
    iget-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@11
    if-ne v1, v3, :cond_1e

    #@13
    .line 1106
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@15
    invoke-interface {v1}, Ljava/util/List;->clear()V

    #@18
    .line 1107
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1a
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->AddRCSFriendList()V

    #@1d
    .line 1122
    :goto_1d
    return v3

    #@1e
    .line 1109
    :cond_1e
    const-string v1, "[IP]:xdm is not connected. it is impossible"

    #@20
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@23
    goto :goto_1d

    #@24
    .line 1112
    :cond_24
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@26
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@29
    move-result v1

    #@2a
    if-lez v1, :cond_6c

    #@2c
    .line 1113
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v2, "[IP] listDeleteQueuingMsisdn size[ "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@39
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@3c
    move-result v2

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    const-string v2, " ]"

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4e
    .line 1115
    const/4 v0, 0x0

    #@4f
    .local v0, idx:I
    :goto_4f
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@51
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@54
    move-result v1

    #@55
    if-ge v0, v1, :cond_67

    #@57
    .line 1116
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@59
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@5b
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5e
    move-result-object v1

    #@5f
    check-cast v1, Ljava/lang/String;

    #@61
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->DeleteRCSFriend(Ljava/lang/String;)V

    #@64
    .line 1115
    add-int/lit8 v0, v0, 0x1

    #@66
    goto :goto_4f

    #@67
    .line 1118
    :cond_67
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@69
    invoke-interface {v1}, Ljava/util/List;->clear()V

    #@6c
    .line 1120
    .end local v0           #idx:I
    :cond_6c
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@6e
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SyncRCSFriendList()V

    #@71
    goto :goto_1d
.end method

.method private StateSERVICEOFFBGOFF_IPMsgXdmAgentRLSSyncFailed(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1166
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1167
    iput-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@8
    .line 1168
    return v1
.end method

.method private StateSERVICEOFF_BGOFF_MessageHandler(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 748
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_3c

    #@5
    .line 793
    :pswitch_5
    const-string v0, "[IP]Don`t handle message in Service Off BG Off State"

    #@7
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a
    .line 796
    :goto_a
    const/4 v0, 0x1

    #@b
    return v0

    #@c
    .line 750
    :pswitch_c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFFBGOFF_IPMsgImsAgentConnected(Landroid/os/Message;)Z

    #@f
    goto :goto_a

    #@10
    .line 753
    :pswitch_10
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFFBGOFF_IPMsgImsAgentDisconnected(Landroid/os/Message;)Z

    #@13
    goto :goto_a

    #@14
    .line 756
    :pswitch_14
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFFBGOFF_IPMsgXdmAgentConnected(Landroid/os/Message;)Z

    #@17
    goto :goto_a

    #@18
    .line 759
    :pswitch_18
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFFBGOFF_IPMsgTimerAGentPollingExpired(Landroid/os/Message;)Z

    #@1b
    goto :goto_a

    #@1c
    .line 762
    :pswitch_1c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFFBGOFF_IPMsgTimerAGentRLSSubPollingExpired(Landroid/os/Message;)Z

    #@1f
    goto :goto_a

    #@20
    .line 765
    :pswitch_20
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFFBGOFF_IPMsgAddressBookAgentChanged(Landroid/os/Message;)Z

    #@23
    goto :goto_a

    #@24
    .line 768
    :pswitch_24
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFFBGOFF_IPMsgBGHelperAgentOn(Landroid/os/Message;)Z

    #@27
    goto :goto_a

    #@28
    .line 771
    :pswitch_28
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgSvcAgentSync(Landroid/os/Message;)Z

    #@2b
    goto :goto_a

    #@2c
    .line 774
    :pswitch_2c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgXdmAgentGetRLSListFailed(Landroid/os/Message;)Z

    #@2f
    goto :goto_a

    #@30
    .line 777
    :pswitch_30
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgXdmAgentGetRLSListSuccess(Landroid/os/Message;)Z

    #@33
    goto :goto_a

    #@34
    .line 780
    :pswitch_34
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFFBGOFF_IPMsgContactDeleteRcsFriendList(Landroid/os/Message;)Z

    #@37
    goto :goto_a

    #@38
    .line 783
    :pswitch_38
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFFBGOFF_IPMsgXdmAgentRLSSyncFailed(Landroid/os/Message;)Z

    #@3b
    goto :goto_a

    #@3c
    .line 748
    :pswitch_data_3c
    .packed-switch 0x1
        :pswitch_c
        :pswitch_10
        :pswitch_14
        :pswitch_18
        :pswitch_1c
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_34
        :pswitch_5
        :pswitch_5
        :pswitch_20
        :pswitch_24
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_28
        :pswitch_5
        :pswitch_5
        :pswitch_2c
        :pswitch_30
        :pswitch_38
    .end packed-switch
.end method

.method private StateSERVICEOFF_IPMsgAddressBookAgentChanged(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1031
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1036
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method private StateSERVICEOFF_IPMsgBGHelperAgentOff(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1063
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1065
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 1066
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopPollingTimer()V

    #@f
    .line 1068
    :cond_f
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@12
    .line 1069
    return v1
.end method

.method private StateSERVICEOFF_IPMsgContactDeleteRcsFriendList(Landroid/os/Message;)Z
    .registers 8
    .parameter "objMessage"

    #@0
    .prologue
    .line 963
    const-string v4, "[IP]"

    #@2
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 964
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@8
    move-result-object v2

    #@9
    .line 965
    .local v2, objBundle:Landroid/os/Bundle;
    const-string v4, "delete_rcsuser"

    #@b
    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@e
    move-result-object v3

    #@f
    check-cast v3, Lcom/lge/ims/service/ip/ResourceListUserInfo;

    #@11
    .line 966
    .local v3, objResourceListUserInfo:Lcom/lge/ims/service/ip/ResourceListUserInfo;
    if-eqz v3, :cond_32

    #@13
    .line 967
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetCurrentResourceListUserSize()I

    #@16
    move-result v1

    #@17
    .line 968
    .local v1, nCurrentSize:I
    const/4 v0, 0x0

    #@18
    .local v0, n:I
    :goto_18
    if-ge v0, v1, :cond_32

    #@1a
    .line 969
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@1c
    invoke-virtual {v3, v0}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetResourceListUser(I)Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@23
    move-result v4

    #@24
    if-nez v4, :cond_2f

    #@26
    .line 970
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@28
    invoke-virtual {v3, v0}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetResourceListUser(I)Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2f
    .line 968
    :cond_2f
    add-int/lit8 v0, v0, 0x1

    #@31
    goto :goto_18

    #@32
    .line 974
    .end local v0           #n:I
    .end local v1           #nCurrentSize:I
    :cond_32
    const/4 v4, 0x1

    #@33
    return v4
.end method

.method private StateSERVICEOFF_IPMsgImsAgentConnected(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 985
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 986
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 987
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPContactHelper;->Sync()Z

    #@f
    .line 990
    :cond_f
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@11
    if-eq v0, v1, :cond_18

    #@13
    .line 991
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@15
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->StartXDMAgent()V

    #@18
    .line 994
    :cond_18
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objBGHelper:Lcom/lge/ims/service/ip/IPBGHelper;

    #@1a
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPBGHelper;->GetBGServiceState()Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_3c

    #@20
    .line 995
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@22
    if-ne v0, v1, :cond_37

    #@24
    .line 996
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@27
    move-result-object v0

    #@28
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPQueryRepository;->IsEmpty()Z

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_32

    #@2e
    .line 997
    const/4 v0, 0x0

    #@2f
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SendNOptions(Z)V

    #@32
    .line 999
    :cond_32
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@34
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->CheckPollingTimer()V

    #@37
    .line 1001
    :cond_37
    const/4 v0, 0x3

    #@38
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@3b
    .line 1005
    :goto_3b
    return v1

    #@3c
    .line 1003
    :cond_3c
    const/4 v0, 0x2

    #@3d
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@40
    goto :goto_3b
.end method

.method private StateSERVICEOFF_IPMsgImsAgentDisconnected(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1008
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1009
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v0, v1, :cond_a

    #@a
    .line 1012
    :cond_a
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@d
    .line 1013
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPQueryRepository;->ClearQueryList()V

    #@14
    .line 1014
    return v1
.end method

.method private StateSERVICEOFF_IPMsgTimerAGentPollingExpired(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1017
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1018
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1019
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopPollingTimer()V

    #@e
    .line 1021
    :cond_e
    const/4 v0, 0x1

    #@f
    return v0
.end method

.method private StateSERVICEOFF_IPMsgTimerAGentRLSSubPollingExpired(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1024
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1025
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1026
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopRLSSubPollingTimer()V

    #@e
    .line 1028
    :cond_e
    const/4 v0, 0x1

    #@f
    return v0
.end method

.method private StateSERVICEOFF_IPMsgXdmAgentConnected(Landroid/os/Message;)Z
    .registers 6
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1039
    const-string v1, "[IP]"

    #@3
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1040
    iput-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    .line 1041
    iget-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@a
    if-ne v1, v3, :cond_24

    #@c
    .line 1042
    const/4 v1, 0x0

    #@d
    iput-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@f
    .line 1043
    iget-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@11
    if-ne v1, v3, :cond_1e

    #@13
    .line 1044
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@15
    invoke-interface {v1}, Ljava/util/List;->clear()V

    #@18
    .line 1045
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1a
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->AddRCSFriendList()V

    #@1d
    .line 1060
    :goto_1d
    return v3

    #@1e
    .line 1047
    :cond_1e
    const-string v1, "[IP]:xdm is not connected. it is impossible"

    #@20
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@23
    goto :goto_1d

    #@24
    .line 1050
    :cond_24
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@26
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@29
    move-result v1

    #@2a
    if-lez v1, :cond_6c

    #@2c
    .line 1051
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v2, "[IP] listDeleteQueuingMsisdn size[ "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@39
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@3c
    move-result v2

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    const-string v2, " ]"

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4e
    .line 1053
    const/4 v0, 0x0

    #@4f
    .local v0, idx:I
    :goto_4f
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@51
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@54
    move-result v1

    #@55
    if-ge v0, v1, :cond_67

    #@57
    .line 1054
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@59
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@5b
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5e
    move-result-object v1

    #@5f
    check-cast v1, Ljava/lang/String;

    #@61
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->DeleteRCSFriend(Ljava/lang/String;)V

    #@64
    .line 1053
    add-int/lit8 v0, v0, 0x1

    #@66
    goto :goto_4f

    #@67
    .line 1056
    :cond_67
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@69
    invoke-interface {v1}, Ljava/util/List;->clear()V

    #@6c
    .line 1058
    .end local v0           #idx:I
    :cond_6c
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@6e
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SyncRCSFriendList()V

    #@71
    goto :goto_1d
.end method

.method private StateSERVICEOFF_IPMsgXdmAgentRLSSyncFailed(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 977
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 978
    iput-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@8
    .line 979
    return v1
.end method

.method private StateSERVICEOFF_MessageHandler(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 697
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_3c

    #@5
    .line 742
    :pswitch_5
    const-string v0, "[IP]Don`t handle message in Service Off State"

    #@7
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a
    .line 745
    :goto_a
    const/4 v0, 0x1

    #@b
    return v0

    #@c
    .line 699
    :pswitch_c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_IPMsgImsAgentConnected(Landroid/os/Message;)Z

    #@f
    goto :goto_a

    #@10
    .line 702
    :pswitch_10
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_IPMsgTimerAGentPollingExpired(Landroid/os/Message;)Z

    #@13
    goto :goto_a

    #@14
    .line 705
    :pswitch_14
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_IPMsgTimerAGentRLSSubPollingExpired(Landroid/os/Message;)Z

    #@17
    goto :goto_a

    #@18
    .line 708
    :pswitch_18
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_IPMsgAddressBookAgentChanged(Landroid/os/Message;)Z

    #@1b
    goto :goto_a

    #@1c
    .line 711
    :pswitch_1c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_IPMsgBGHelperAgentOff(Landroid/os/Message;)Z

    #@1f
    goto :goto_a

    #@20
    .line 714
    :pswitch_20
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgSvcAgentSync(Landroid/os/Message;)Z

    #@23
    goto :goto_a

    #@24
    .line 717
    :pswitch_24
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_IPMsgImsAgentDisconnected(Landroid/os/Message;)Z

    #@27
    goto :goto_a

    #@28
    .line 720
    :pswitch_28
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_IPMsgXdmAgentConnected(Landroid/os/Message;)Z

    #@2b
    goto :goto_a

    #@2c
    .line 723
    :pswitch_2c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgXdmAgentGetRLSListFailed(Landroid/os/Message;)Z

    #@2f
    goto :goto_a

    #@30
    .line 726
    :pswitch_30
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgXdmAgentGetRLSListSuccess(Landroid/os/Message;)Z

    #@33
    goto :goto_a

    #@34
    .line 729
    :pswitch_34
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_IPMsgContactDeleteRcsFriendList(Landroid/os/Message;)Z

    #@37
    goto :goto_a

    #@38
    .line 732
    :pswitch_38
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_IPMsgXdmAgentRLSSyncFailed(Landroid/os/Message;)Z

    #@3b
    goto :goto_a

    #@3c
    .line 697
    :pswitch_data_3c
    .packed-switch 0x1
        :pswitch_c
        :pswitch_24
        :pswitch_28
        :pswitch_10
        :pswitch_14
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_34
        :pswitch_5
        :pswitch_5
        :pswitch_18
        :pswitch_5
        :pswitch_1c
        :pswitch_5
        :pswitch_5
        :pswitch_20
        :pswitch_5
        :pswitch_5
        :pswitch_2c
        :pswitch_30
        :pswitch_38
    .end packed-switch
.end method

.method private StateSERVICEONBGOFF_IPMsgAddressBookAgentChanged(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1351
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1352
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1353
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPContactHelper;->Sync()Z

    #@e
    .line 1355
    :cond_e
    const/4 v0, 0x1

    #@f
    return v0
.end method

.method private StateSERVICEONBGOFF_IPMsgBGHelperAgentOn(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1358
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1359
    const/4 v0, 0x3

    #@7
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@a
    .line 1360
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@c
    if-ne v0, v1, :cond_21

    #@e
    .line 1361
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPQueryRepository;->IsEmpty()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1c

    #@18
    .line 1362
    const/4 v0, 0x0

    #@19
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SendNOptions(Z)V

    #@1c
    .line 1364
    :cond_1c
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@1e
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->CheckPollingTimer()V

    #@21
    .line 1366
    :cond_21
    return v1
.end method

.method private StateSERVICEONBGOFF_IPMsgContactAddNonRcsFriend(Landroid/os/Message;)Z
    .registers 5
    .parameter "objMessage"

    #@0
    .prologue
    .line 1255
    const-string v2, "[IP]"

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1257
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@8
    move-result-object v0

    #@9
    .line 1258
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v2, "msisdn"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 1259
    .local v1, szMSISDN:Ljava/lang/String;
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@11
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->AddNonRCSFriend(Ljava/lang/String;)V

    #@14
    .line 1260
    const/4 v2, 0x1

    #@15
    return v2
.end method

.method private StateSERVICEONBGOFF_IPMsgContactAddRcsFriend(Landroid/os/Message;)Z
    .registers 7
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1222
    const-string v3, "[IP]"

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1224
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9
    move-result-object v0

    #@a
    .line 1225
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v3, "msisdn"

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    .line 1226
    .local v2, szMSISDN:Ljava/lang/String;
    const-string v3, "displayname"

    #@12
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 1227
    .local v1, szDisplayName:Ljava/lang/String;
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@18
    if-ne v3, v4, :cond_1f

    #@1a
    .line 1228
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1c
    invoke-virtual {v3, v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->AddRCSFriend(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 1237
    :cond_1f
    return v4
.end method

.method private StateSERVICEONBGOFF_IPMsgContactDeleteRcsFriend(Landroid/os/Message;)Z
    .registers 6
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1240
    const-string v2, "[IP]"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1242
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9
    move-result-object v0

    #@a
    .line 1243
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v2, "msisdn"

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    .line 1244
    .local v1, szMSISDN:Ljava/lang/String;
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@12
    if-ne v2, v3, :cond_19

    #@14
    .line 1245
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@16
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->DeleteRCSFriend(Ljava/lang/String;)V

    #@19
    .line 1252
    :cond_19
    return v3
.end method

.method private StateSERVICEONBGOFF_IPMsgContactDeleteRcsFriendList(Landroid/os/Message;)Z
    .registers 9
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1263
    const-string v4, "[IP]"

    #@3
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1265
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9
    move-result-object v2

    #@a
    .line 1266
    .local v2, objBundle:Landroid/os/Bundle;
    const-string v4, "delete_rcsuser"

    #@c
    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Lcom/lge/ims/service/ip/ResourceListUserInfo;

    #@12
    .line 1267
    .local v3, objResourceListUserInfo:Lcom/lge/ims/service/ip/ResourceListUserInfo;
    if-eqz v3, :cond_1d

    #@14
    .line 1268
    iget-boolean v4, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@16
    if-ne v4, v6, :cond_1e

    #@18
    .line 1269
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1a
    invoke-virtual {v4, v3}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->DeleteRCSFriendList(Lcom/lge/ims/service/ip/ResourceListUserInfo;)V

    #@1d
    .line 1279
    :cond_1d
    return v6

    #@1e
    .line 1271
    :cond_1e
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetCurrentResourceListUserSize()I

    #@21
    move-result v1

    #@22
    .line 1272
    .local v1, nCurrentSize:I
    const/4 v0, 0x0

    #@23
    .local v0, n:I
    :goto_23
    if-ge v0, v1, :cond_1d

    #@25
    .line 1273
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@27
    invoke-virtual {v3, v0}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetResourceListUser(I)Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@2e
    move-result v4

    #@2f
    if-nez v4, :cond_3a

    #@31
    .line 1274
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@33
    invoke-virtual {v3, v0}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetResourceListUser(I)Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@3a
    .line 1272
    :cond_3a
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_23
.end method

.method private StateSERVICEONBGOFF_IPMsgImsAgentDisconnected(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1174
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1175
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@8
    if-eqz v0, :cond_f

    #@a
    .line 1176
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopPollingTimer()V

    #@f
    .line 1179
    :cond_f
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@11
    if-ne v0, v1, :cond_13

    #@13
    .line 1182
    :cond_13
    const/4 v0, 0x0

    #@14
    iput-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@16
    .line 1183
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPQueryRepository;->ClearQueryList()V

    #@1d
    .line 1184
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@20
    .line 1185
    return v1
.end method

.method private StateSERVICEONBGOFF_IPMsgTimerAGentPollingExpired(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1337
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1338
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1339
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopPollingTimer()V

    #@e
    .line 1341
    :cond_e
    const/4 v0, 0x1

    #@f
    return v0
.end method

.method private StateSERVICEONBGOFF_IPMsgTimerAGentRLSSubPollingExpired(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1344
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1345
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1346
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopRLSSubPollingTimer()V

    #@e
    .line 1348
    :cond_e
    const/4 v0, 0x1

    #@f
    return v0
.end method

.method private StateSERVICEONBGOFF_IPMsgXdmAgentConnected(Landroid/os/Message;)Z
    .registers 6
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1188
    const-string v1, "[IP]"

    #@3
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1189
    iput-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    .line 1190
    iget-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@a
    if-ne v1, v3, :cond_24

    #@c
    .line 1191
    const/4 v1, 0x0

    #@d
    iput-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@f
    .line 1192
    iget-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@11
    if-ne v1, v3, :cond_1e

    #@13
    .line 1193
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@15
    invoke-interface {v1}, Ljava/util/List;->clear()V

    #@18
    .line 1194
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1a
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->AddRCSFriendList()V

    #@1d
    .line 1219
    :goto_1d
    return v3

    #@1e
    .line 1196
    :cond_1e
    const-string v1, "[IP]:xdm is not connected. it is impossible"

    #@20
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@23
    goto :goto_1d

    #@24
    .line 1209
    :cond_24
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@26
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@29
    move-result v1

    #@2a
    if-lez v1, :cond_6c

    #@2c
    .line 1210
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v2, "[IP] listDeleteQueuingMsisdn size[ "

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@39
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@3c
    move-result v2

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    const-string v2, " ]"

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4e
    .line 1212
    const/4 v0, 0x0

    #@4f
    .local v0, idx:I
    :goto_4f
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@51
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@54
    move-result v1

    #@55
    if-ge v0, v1, :cond_67

    #@57
    .line 1213
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@59
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@5b
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@5e
    move-result-object v1

    #@5f
    check-cast v1, Ljava/lang/String;

    #@61
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->DeleteRCSFriend(Ljava/lang/String;)V

    #@64
    .line 1212
    add-int/lit8 v0, v0, 0x1

    #@66
    goto :goto_4f

    #@67
    .line 1215
    :cond_67
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@69
    invoke-interface {v1}, Ljava/util/List;->clear()V

    #@6c
    .line 1217
    .end local v0           #idx:I
    :cond_6c
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@6e
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SyncRCSFriendList()V

    #@71
    goto :goto_1d
.end method

.method private StateSERVICEONBGOFF_IPMsgXdmAgentGetBuddyStatusIcon(Landroid/os/Message;)Z
    .registers 7
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1325
    const-string v3, "[IP]"

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1326
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v3, v4, :cond_20

    #@a
    .line 1327
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1328
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v3, "statusIconUrl"

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    .line 1329
    .local v2, szStatusIconUrl:Ljava/lang/String;
    const-string v3, "msisdn"

    #@16
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    .line 1330
    .local v1, szMSISDN:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1c
    invoke-virtual {v3, v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->GetBuddyStatusIcon(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 1334
    .end local v0           #objBundle:Landroid/os/Bundle;
    .end local v1           #szMSISDN:Ljava/lang/String;
    .end local v2           #szStatusIconUrl:Ljava/lang/String;
    :goto_1f
    return v4

    #@20
    .line 1332
    :cond_20
    const-string v3, "[IP]:xdm is not connected. it is impossible"

    #@22
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@25
    goto :goto_1f
.end method

.method private StateSERVICEONBGOFF_IPMsgXdmAgentGetBuddyStatusIconThumb(Landroid/os/Message;)Z
    .registers 7
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1313
    const-string v3, "[IP]"

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1314
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v3, v4, :cond_20

    #@a
    .line 1315
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1316
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v3, "statusIconThumbUrl"

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    .line 1317
    .local v2, szStatusIconThumbUrl:Ljava/lang/String;
    const-string v3, "msisdn"

    #@16
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    .line 1318
    .local v1, szMSISDN:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1c
    invoke-virtual {v3, v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->GetBuddyStatusIconThumb(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 1322
    .end local v0           #objBundle:Landroid/os/Bundle;
    .end local v1           #szMSISDN:Ljava/lang/String;
    .end local v2           #szStatusIconThumbUrl:Ljava/lang/String;
    :goto_1f
    return v4

    #@20
    .line 1320
    :cond_20
    const-string v3, "[IP]:xdm is not connected. it is impossible"

    #@22
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@25
    goto :goto_1f
.end method

.method private StateSERVICEONBGOFF_IPMsgXdmAgentGetMyStatusIcon(Landroid/os/Message;)Z
    .registers 6
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1302
    const-string v2, "[IP]"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1303
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v2, v3, :cond_1a

    #@a
    .line 1304
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1305
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v2, "statusIconUrl"

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 1306
    .local v1, szStatusIconUrl:Ljava/lang/String;
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@16
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->GetMyStatusIcon(Ljava/lang/String;)V

    #@19
    .line 1310
    .end local v0           #objBundle:Landroid/os/Bundle;
    .end local v1           #szStatusIconUrl:Ljava/lang/String;
    :goto_19
    return v3

    #@1a
    .line 1308
    :cond_1a
    const-string v2, "[IP]:xdm is not connected. it is impossible"

    #@1c
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1f
    goto :goto_19
.end method

.method private StateSERVICEONBGOFF_IPMsgXdmAgentGetMyStatusIconThumb(Landroid/os/Message;)Z
    .registers 6
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1291
    const-string v2, "[IP]"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1292
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v2, v3, :cond_1a

    #@a
    .line 1293
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1294
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v2, "statusIconThumbUrl"

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 1295
    .local v1, szStatusIconThumbUrl:Ljava/lang/String;
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@16
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->GetMyStatusIconThumb(Ljava/lang/String;)V

    #@19
    .line 1299
    .end local v0           #objBundle:Landroid/os/Bundle;
    .end local v1           #szStatusIconThumbUrl:Ljava/lang/String;
    :goto_19
    return v3

    #@1a
    .line 1297
    :cond_1a
    const-string v2, "[IP]:xdm is not connected. it is impossible"

    #@1c
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1f
    goto :goto_19
.end method

.method private StateSERVICEONBGOFF_IPMsgXdmAgentRLSSyncFailed(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1282
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1283
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v0, v1, :cond_10

    #@a
    .line 1284
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->AddRCSFriendList()V

    #@f
    .line 1288
    :goto_f
    return v1

    #@10
    .line 1286
    :cond_10
    const-string v0, "[IP]:xdm is not connected. it is impossible"

    #@12
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@15
    goto :goto_f
.end method

.method private StateSERVICEON_BGOFF_MessageHandler(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 799
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_54

    #@5
    .line 860
    :pswitch_5
    const-string v0, "[IP]Don`t handle message in Service On BG Off State"

    #@7
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a
    .line 863
    :goto_a
    const/4 v0, 0x1

    #@b
    return v0

    #@c
    .line 801
    :pswitch_c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgImsAgentDisconnected(Landroid/os/Message;)Z

    #@f
    goto :goto_a

    #@10
    .line 804
    :pswitch_10
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgTimerAGentPollingExpired(Landroid/os/Message;)Z

    #@13
    goto :goto_a

    #@14
    .line 807
    :pswitch_14
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgTimerAGentRLSSubPollingExpired(Landroid/os/Message;)Z

    #@17
    goto :goto_a

    #@18
    .line 810
    :pswitch_18
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgAddressBookAgentChanged(Landroid/os/Message;)Z

    #@1b
    goto :goto_a

    #@1c
    .line 813
    :pswitch_1c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgBGHelperAgentOn(Landroid/os/Message;)Z

    #@1f
    goto :goto_a

    #@20
    .line 816
    :pswitch_20
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgSvcAgentSync(Landroid/os/Message;)Z

    #@23
    goto :goto_a

    #@24
    .line 819
    :pswitch_24
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgXdmAgentConnected(Landroid/os/Message;)Z

    #@27
    goto :goto_a

    #@28
    .line 822
    :pswitch_28
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgXdmAgentGetRLSListFailed(Landroid/os/Message;)Z

    #@2b
    goto :goto_a

    #@2c
    .line 825
    :pswitch_2c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgXdmAgentGetRLSListSuccess(Landroid/os/Message;)Z

    #@2f
    goto :goto_a

    #@30
    .line 828
    :pswitch_30
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgContactAddRcsFriend(Landroid/os/Message;)Z

    #@33
    goto :goto_a

    #@34
    .line 831
    :pswitch_34
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgContactDeleteRcsFriend(Landroid/os/Message;)Z

    #@37
    goto :goto_a

    #@38
    .line 834
    :pswitch_38
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgContactAddNonRcsFriend(Landroid/os/Message;)Z

    #@3b
    goto :goto_a

    #@3c
    .line 837
    :pswitch_3c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgContactDeleteRcsFriendList(Landroid/os/Message;)Z

    #@3f
    goto :goto_a

    #@40
    .line 840
    :pswitch_40
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgXdmAgentRLSSyncFailed(Landroid/os/Message;)Z

    #@43
    goto :goto_a

    #@44
    .line 843
    :pswitch_44
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgXdmAgentGetMyStatusIconThumb(Landroid/os/Message;)Z

    #@47
    goto :goto_a

    #@48
    .line 846
    :pswitch_48
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgXdmAgentGetMyStatusIcon(Landroid/os/Message;)Z

    #@4b
    goto :goto_a

    #@4c
    .line 849
    :pswitch_4c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgXdmAgentGetBuddyStatusIconThumb(Landroid/os/Message;)Z

    #@4f
    goto :goto_a

    #@50
    .line 852
    :pswitch_50
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEONBGOFF_IPMsgXdmAgentGetBuddyStatusIcon(Landroid/os/Message;)Z

    #@53
    goto :goto_a

    #@54
    .line 799
    :pswitch_data_54
    .packed-switch 0x2
        :pswitch_c
        :pswitch_24
        :pswitch_10
        :pswitch_14
        :pswitch_5
        :pswitch_30
        :pswitch_34
        :pswitch_38
        :pswitch_5
        :pswitch_3c
        :pswitch_5
        :pswitch_5
        :pswitch_18
        :pswitch_1c
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_20
        :pswitch_5
        :pswitch_5
        :pswitch_28
        :pswitch_2c
        :pswitch_40
        :pswitch_44
        :pswitch_48
        :pswitch_4c
        :pswitch_50
    .end packed-switch
.end method

.method private StateSERVICEON_IPMsgAddressBookAgentChanged(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1430
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1431
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1432
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPContactHelper;->Sync()Z

    #@e
    .line 1435
    :cond_e
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPQueryRepository;->IsEmpty()Z

    #@15
    move-result v0

    #@16
    if-eqz v0, :cond_1c

    #@18
    .line 1436
    const/4 v0, 0x0

    #@19
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SendNOptions(Z)V

    #@1c
    .line 1438
    :cond_1c
    const/4 v0, 0x1

    #@1d
    return v0
.end method

.method private StateSERVICEON_IPMsgBGHelperAgentOff(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1451
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1453
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 1454
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopPollingTimer()V

    #@e
    .line 1456
    :cond_e
    const/4 v0, 0x2

    #@f
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@12
    .line 1457
    const/4 v0, 0x1

    #@13
    return v0
.end method

.method private StateSERVICEON_IPMsgContactAddNonRcsFriend(Landroid/os/Message;)Z
    .registers 5
    .parameter "objMessage"

    #@0
    .prologue
    .line 1541
    const-string v2, "[IP]"

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1543
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@8
    move-result-object v0

    #@9
    .line 1544
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v2, "msisdn"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 1545
    .local v1, szMSISDN:Ljava/lang/String;
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@11
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->AddNonRCSFriend(Ljava/lang/String;)V

    #@14
    .line 1546
    const/4 v2, 0x1

    #@15
    return v2
.end method

.method private StateSERVICEON_IPMsgContactAddRcsFriend(Landroid/os/Message;)Z
    .registers 7
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1509
    const-string v3, "[IP]"

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1510
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9
    move-result-object v0

    #@a
    .line 1511
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v3, "msisdn"

    #@c
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v2

    #@10
    .line 1512
    .local v2, szMSISDN:Ljava/lang/String;
    const-string v3, "displayname"

    #@12
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 1513
    .local v1, szDisplayName:Ljava/lang/String;
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@18
    if-ne v3, v4, :cond_1f

    #@1a
    .line 1514
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1c
    invoke-virtual {v3, v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->AddRCSFriend(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 1523
    :cond_1f
    return v4
.end method

.method private StateSERVICEON_IPMsgContactCapaQuery(Landroid/os/Message;)Z
    .registers 5
    .parameter "objMessage"

    #@0
    .prologue
    .line 1502
    const-string v2, "[IP]"

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1503
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@8
    move-result-object v0

    #@9
    .line 1504
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v2, "msisdn"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 1505
    .local v1, szMSISDN:Ljava/lang/String;
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@11
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->ContactCapaQuery(Ljava/lang/String;)V

    #@14
    .line 1506
    const/4 v2, 0x1

    #@15
    return v2
.end method

.method private StateSERVICEON_IPMsgContactDeleteRcsFriend(Landroid/os/Message;)Z
    .registers 6
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1526
    const-string v2, "[IP]"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1528
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9
    move-result-object v0

    #@a
    .line 1529
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v2, "msisdn"

    #@c
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v1

    #@10
    .line 1530
    .local v1, szMSISDN:Ljava/lang/String;
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@12
    if-ne v2, v3, :cond_19

    #@14
    .line 1531
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@16
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->DeleteRCSFriend(Ljava/lang/String;)V

    #@19
    .line 1538
    :cond_19
    return v3
.end method

.method private StateSERVICEON_IPMsgContactDeleteRcsFriendList(Landroid/os/Message;)Z
    .registers 9
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1557
    const-string v4, "[IP]"

    #@3
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1559
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9
    move-result-object v2

    #@a
    .line 1560
    .local v2, objBundle:Landroid/os/Bundle;
    const-string v4, "delete_rcsuser"

    #@c
    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@f
    move-result-object v3

    #@10
    check-cast v3, Lcom/lge/ims/service/ip/ResourceListUserInfo;

    #@12
    .line 1561
    .local v3, objResourceListUserInfo:Lcom/lge/ims/service/ip/ResourceListUserInfo;
    if-eqz v3, :cond_1d

    #@14
    .line 1562
    iget-boolean v4, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@16
    if-ne v4, v6, :cond_1e

    #@18
    .line 1563
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1a
    invoke-virtual {v4, v3}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->DeleteRCSFriendList(Lcom/lge/ims/service/ip/ResourceListUserInfo;)V

    #@1d
    .line 1573
    :cond_1d
    return v6

    #@1e
    .line 1565
    :cond_1e
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetCurrentResourceListUserSize()I

    #@21
    move-result v1

    #@22
    .line 1566
    .local v1, nCurrentSize:I
    const/4 v0, 0x0

    #@23
    .local v0, n:I
    :goto_23
    if-ge v0, v1, :cond_1d

    #@25
    .line 1567
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@27
    invoke-virtual {v3, v0}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetResourceListUser(I)Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@2e
    move-result v4

    #@2f
    if-nez v4, :cond_3a

    #@31
    .line 1568
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@33
    invoke-virtual {v3, v0}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->GetResourceListUser(I)Ljava/lang/String;

    #@36
    move-result-object v5

    #@37
    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@3a
    .line 1566
    :cond_3a
    add-int/lit8 v0, v0, 0x1

    #@3c
    goto :goto_23
.end method

.method private StateSERVICEON_IPMsgContactOneuser_Query(Landroid/os/Message;)Z
    .registers 5
    .parameter "objMessage"

    #@0
    .prologue
    .line 1549
    const-string v2, "[IP]"

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1551
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@8
    move-result-object v0

    #@9
    .line 1552
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v2, "msisdn"

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    .line 1553
    .local v1, szMSISDN:Ljava/lang/String;
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@11
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendPresenceCapaQuery(Ljava/lang/String;)V

    #@14
    .line 1554
    const/4 v2, 0x1

    #@15
    return v2
.end method

.method private StateSERVICEON_IPMsgImsAgentDisconnected(Landroid/os/Message;)Z
    .registers 5
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1372
    const-string v0, "[IP]"

    #@4
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7
    .line 1374
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 1375
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@d
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopPollingTimer()V

    #@10
    .line 1377
    :cond_10
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@12
    if-ne v0, v2, :cond_14

    #@14
    .line 1380
    :cond_14
    iput-boolean v1, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@16
    .line 1381
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPQueryRepository;->ClearQueryList()V

    #@1d
    .line 1382
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ip/IPAgent;->SetState(I)V

    #@20
    .line 1383
    return v2
.end method

.method private StateSERVICEON_IPMsgQueryPollingAgentEmpty(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1446
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1447
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ip/IPAgent;->SendNOptions(Z)V

    #@9
    .line 1448
    return v1
.end method

.method private StateSERVICEON_IPMsgQueryRepoAgentEmpty(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 1441
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1442
    const/4 v0, 0x0

    #@6
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPAgent;->SendNOptions(Z)V

    #@9
    .line 1443
    const/4 v0, 0x1

    #@a
    return v0
.end method

.method private StateSERVICEON_IPMsgTimerAGentPollingExpired(Landroid/os/Message;)Z
    .registers 9
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1386
    const-string v2, "[IP]"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1387
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@8
    if-eqz v2, :cond_f

    #@a
    .line 1388
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@c
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopPollingTimer()V

    #@f
    .line 1390
    :cond_f
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@12
    move-result-object v2

    #@13
    if-eqz v2, :cond_42

    #@15
    .line 1391
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPQueryRepository;->IsEmpty()Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_22

    #@1f
    .line 1392
    invoke-direct {p0, v6}, Lcom/lge/ims/service/ip/IPAgent;->SendNOptions(Z)V

    #@22
    .line 1394
    :cond_22
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@24
    if-eqz v2, :cond_42

    #@26
    .line 1395
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@28
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetPollingTime()J

    #@2b
    move-result-wide v2

    #@2c
    const-wide/16 v4, 0x0

    #@2e
    cmp-long v2, v2, v4

    #@30
    if-lez v2, :cond_43

    #@32
    .line 1396
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@34
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdatePollingLatestTime()Z

    #@37
    .line 1397
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@39
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetRestTime()J

    #@3c
    move-result-wide v0

    #@3d
    .line 1398
    .local v0, resetTimer:J
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@3f
    invoke-virtual {v2, v0, v1}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->CheckPollingTimer(J)V

    #@42
    .line 1407
    .end local v0           #resetTimer:J
    :cond_42
    :goto_42
    return v6

    #@43
    .line 1400
    :cond_43
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsFirst:Z

    #@45
    if-eqz v2, :cond_42

    #@47
    .line 1401
    const/4 v2, 0x0

    #@48
    iput-boolean v2, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsFirst:Z

    #@4a
    .line 1402
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@4c
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdatePollingLatestTime()Z

    #@4f
    goto :goto_42
.end method

.method private StateSERVICEON_IPMsgTimerAGentRLSSubPollingExpired(Landroid/os/Message;)Z
    .registers 8
    .parameter "objMessage"

    #@0
    .prologue
    .line 1410
    const-string v2, "[IP]"

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1411
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@7
    if-eqz v2, :cond_e

    #@9
    .line 1412
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@b
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->StopRLSSubPollingTimer()V

    #@e
    .line 1414
    :cond_e
    invoke-virtual {p0}, Lcom/lge/ims/service/ip/IPAgent;->SendRLSListFetch()V

    #@11
    .line 1415
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@13
    if-eqz v2, :cond_31

    #@15
    .line 1416
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@17
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetRLSSubPollingTime()J

    #@1a
    move-result-wide v2

    #@1b
    const-wide/16 v4, 0x0

    #@1d
    cmp-long v2, v2, v4

    #@1f
    if-lez v2, :cond_33

    #@21
    .line 1417
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@23
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateRLSSubPollingLatestTime()Z

    #@26
    .line 1418
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@28
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetRLSSubRestTime()J

    #@2b
    move-result-wide v0

    #@2c
    .line 1419
    .local v0, resetTimer:J
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@2e
    invoke-virtual {v2, v0, v1}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->CheckRLSSubPollingTimer(J)V

    #@31
    .line 1427
    .end local v0           #resetTimer:J
    :cond_31
    :goto_31
    const/4 v2, 0x1

    #@32
    return v2

    #@33
    .line 1421
    :cond_33
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsFirst:Z

    #@35
    if-eqz v2, :cond_31

    #@37
    .line 1422
    const/4 v2, 0x0

    #@38
    iput-boolean v2, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsFirst:Z

    #@3a
    .line 1423
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@3c
    invoke-virtual {v2}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateRLSSubPollingLatestTime()Z

    #@3f
    goto :goto_31
.end method

.method private StateSERVICEON_IPMsgXdmAgentConnected(Landroid/os/Message;)Z
    .registers 11
    .parameter "objMessage"

    #@0
    .prologue
    const-wide/16 v7, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    const/4 v5, 0x1

    #@4
    .line 1460
    const-string v3, "[IP]"

    #@6
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@9
    .line 1461
    iput-boolean v5, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@b
    .line 1462
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@d
    if-ne v3, v5, :cond_61

    #@f
    .line 1463
    iput-boolean v6, p0, Lcom/lge/ims/service/ip/IPAgent;->bSyncFailedReceived:Z

    #@11
    .line 1464
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@13
    if-ne v3, v5, :cond_5b

    #@15
    .line 1465
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@17
    invoke-interface {v3}, Ljava/util/List;->clear()V

    #@1a
    .line 1466
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1c
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->AddRCSFriendList()V

    #@1f
    .line 1481
    :goto_1f
    invoke-static {}, Lcom/lge/ims/service/ip/IPQueryRepository;->GetQueryRepository()Lcom/lge/ims/service/ip/IPQueryRepository;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPQueryRepository;->IsEmpty()Z

    #@26
    move-result v3

    #@27
    if-eqz v3, :cond_2c

    #@29
    .line 1482
    invoke-direct {p0, v6}, Lcom/lge/ims/service/ip/IPAgent;->SendNOptions(Z)V

    #@2c
    .line 1484
    :cond_2c
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@2e
    if-eqz v3, :cond_5a

    #@30
    .line 1485
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@32
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetPollingTime()J

    #@35
    move-result-wide v3

    #@36
    cmp-long v3, v3, v7

    #@38
    if-lez v3, :cond_45

    #@3a
    .line 1486
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@3c
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetRestTime()J

    #@3f
    move-result-wide v1

    #@40
    .line 1487
    .local v1, resetTimer:J
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@42
    invoke-virtual {v3, v1, v2}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->CheckPollingTimer(J)V

    #@45
    .line 1489
    .end local v1           #resetTimer:J
    :cond_45
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@47
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetRLSSubPollingTime()J

    #@4a
    move-result-wide v3

    #@4b
    cmp-long v3, v3, v7

    #@4d
    if-lez v3, :cond_b0

    #@4f
    .line 1490
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@51
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->GetRLSSubRestTime()J

    #@54
    move-result-wide v1

    #@55
    .line 1491
    .restart local v1       #resetTimer:J
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@57
    invoke-virtual {v3, v1, v2}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->CheckRLSSubPollingTimer(J)V

    #@5a
    .line 1499
    .end local v1           #resetTimer:J
    :cond_5a
    :goto_5a
    return v5

    #@5b
    .line 1468
    :cond_5b
    const-string v3, "[IP]:xdm is not connected. it is impossible"

    #@5d
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@60
    goto :goto_1f

    #@61
    .line 1471
    :cond_61
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@63
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@66
    move-result v3

    #@67
    if-lez v3, :cond_a9

    #@69
    .line 1472
    new-instance v3, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v4, "[IP] listDeleteQueuingMsisdn size[ "

    #@70
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v3

    #@74
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@76
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@79
    move-result v4

    #@7a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v3

    #@7e
    const-string v4, " ]"

    #@80
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v3

    #@84
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v3

    #@88
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8b
    .line 1474
    const/4 v0, 0x0

    #@8c
    .local v0, idx:I
    :goto_8c
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@8e
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@91
    move-result v3

    #@92
    if-ge v0, v3, :cond_a4

    #@94
    .line 1475
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@96
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@98
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@9b
    move-result-object v3

    #@9c
    check-cast v3, Ljava/lang/String;

    #@9e
    invoke-virtual {v4, v3}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->DeleteRCSFriend(Ljava/lang/String;)V

    #@a1
    .line 1474
    add-int/lit8 v0, v0, 0x1

    #@a3
    goto :goto_8c

    #@a4
    .line 1477
    :cond_a4
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->listDeleteQueuingMsisdn:Ljava/util/List;

    #@a6
    invoke-interface {v3}, Ljava/util/List;->clear()V

    #@a9
    .line 1479
    .end local v0           #idx:I
    :cond_a9
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@ab
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SyncRCSFriendList()V

    #@ae
    goto/16 :goto_1f

    #@b0
    .line 1493
    :cond_b0
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsFirst:Z

    #@b2
    if-eqz v3, :cond_5a

    #@b4
    .line 1494
    iput-boolean v6, p0, Lcom/lge/ims/service/ip/IPAgent;->bIsFirst:Z

    #@b6
    .line 1495
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@b8
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdatePollingLatestTime()Z

    #@bb
    goto :goto_5a
.end method

.method private StateSERVICEON_IPMsgXdmAgentGetBuddyStatusIcon(Landroid/os/Message;)Z
    .registers 7
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1619
    const-string v3, "[IP]"

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1620
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v3, v4, :cond_20

    #@a
    .line 1621
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1622
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v3, "statusIconUrl"

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    .line 1623
    .local v2, szStatusIconUrl:Ljava/lang/String;
    const-string v3, "msisdn"

    #@16
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    .line 1624
    .local v1, szMSISDN:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1c
    invoke-virtual {v3, v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->GetBuddyStatusIcon(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 1628
    .end local v0           #objBundle:Landroid/os/Bundle;
    .end local v1           #szMSISDN:Ljava/lang/String;
    .end local v2           #szStatusIconUrl:Ljava/lang/String;
    :goto_1f
    return v4

    #@20
    .line 1626
    :cond_20
    const-string v3, "[IP]:xdm is not connected. it is impossible"

    #@22
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@25
    goto :goto_1f
.end method

.method private StateSERVICEON_IPMsgXdmAgentGetBuddyStatusIconThumb(Landroid/os/Message;)Z
    .registers 7
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 1607
    const-string v3, "[IP]"

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1608
    iget-boolean v3, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v3, v4, :cond_20

    #@a
    .line 1609
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1610
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v3, "statusIconThumbUrl"

    #@10
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v2

    #@14
    .line 1611
    .local v2, szStatusIconThumbUrl:Ljava/lang/String;
    const-string v3, "msisdn"

    #@16
    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    .line 1612
    .local v1, szMSISDN:Ljava/lang/String;
    iget-object v3, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@1c
    invoke-virtual {v3, v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->GetBuddyStatusIconThumb(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 1616
    .end local v0           #objBundle:Landroid/os/Bundle;
    .end local v1           #szMSISDN:Ljava/lang/String;
    .end local v2           #szStatusIconThumbUrl:Ljava/lang/String;
    :goto_1f
    return v4

    #@20
    .line 1614
    :cond_20
    const-string v3, "[IP]:xdm is not connected. it is impossible"

    #@22
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@25
    goto :goto_1f
.end method

.method private StateSERVICEON_IPMsgXdmAgentGetMyStatusIcon(Landroid/os/Message;)Z
    .registers 6
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1596
    const-string v2, "[IP]"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1597
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v2, v3, :cond_1a

    #@a
    .line 1598
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1599
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v2, "statusIconUrl"

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 1600
    .local v1, szStatusIconUrl:Ljava/lang/String;
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@16
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->GetMyStatusIcon(Ljava/lang/String;)V

    #@19
    .line 1604
    .end local v0           #objBundle:Landroid/os/Bundle;
    .end local v1           #szStatusIconUrl:Ljava/lang/String;
    :goto_19
    return v3

    #@1a
    .line 1602
    :cond_1a
    const-string v2, "[IP]:xdm is not connected. it is impossible"

    #@1c
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1f
    goto :goto_19
.end method

.method private StateSERVICEON_IPMsgXdmAgentGetMyStatusIconThumb(Landroid/os/Message;)Z
    .registers 6
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 1585
    const-string v2, "[IP]"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1586
    iget-boolean v2, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v2, v3, :cond_1a

    #@a
    .line 1587
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1588
    .local v0, objBundle:Landroid/os/Bundle;
    const-string v2, "statusIconThumbUrl"

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    .line 1589
    .local v1, szStatusIconThumbUrl:Ljava/lang/String;
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@16
    invoke-virtual {v2, v1}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->GetMyStatusIconThumb(Ljava/lang/String;)V

    #@19
    .line 1593
    .end local v0           #objBundle:Landroid/os/Bundle;
    .end local v1           #szStatusIconThumbUrl:Ljava/lang/String;
    :goto_19
    return v3

    #@1a
    .line 1591
    :cond_1a
    const-string v2, "[IP]:xdm is not connected. it is impossible"

    #@1c
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1f
    goto :goto_19
.end method

.method private StateSERVICEON_IPMsgXdmAgentRLSSyncFailed(Landroid/os/Message;)Z
    .registers 4
    .parameter "objMessage"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1576
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1577
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@8
    if-ne v0, v1, :cond_10

    #@a
    .line 1578
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->AddRCSFriendList()V

    #@f
    .line 1582
    :goto_f
    return v1

    #@10
    .line 1580
    :cond_10
    const-string v0, "[IP]:xdm is not connected. it is impossible"

    #@12
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@15
    goto :goto_f
.end method

.method private StateSERVICEON_MessageHandler(Landroid/os/Message;)Z
    .registers 3
    .parameter "objMessage"

    #@0
    .prologue
    .line 867
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_64

    #@5
    .line 937
    :pswitch_5
    const-string v0, "[IP]Don`t handle message in Service On State"

    #@7
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a
    .line 940
    :goto_a
    const/4 v0, 0x1

    #@b
    return v0

    #@c
    .line 869
    :pswitch_c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgImsAgentDisconnected(Landroid/os/Message;)Z

    #@f
    goto :goto_a

    #@10
    .line 872
    :pswitch_10
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgTimerAGentPollingExpired(Landroid/os/Message;)Z

    #@13
    goto :goto_a

    #@14
    .line 875
    :pswitch_14
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgTimerAGentRLSSubPollingExpired(Landroid/os/Message;)Z

    #@17
    goto :goto_a

    #@18
    .line 878
    :pswitch_18
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgAddressBookAgentChanged(Landroid/os/Message;)Z

    #@1b
    goto :goto_a

    #@1c
    .line 881
    :pswitch_1c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgQueryRepoAgentEmpty(Landroid/os/Message;)Z

    #@1f
    goto :goto_a

    #@20
    .line 884
    :pswitch_20
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgQueryPollingAgentEmpty(Landroid/os/Message;)Z

    #@23
    goto :goto_a

    #@24
    .line 887
    :pswitch_24
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgBGHelperAgentOff(Landroid/os/Message;)Z

    #@27
    goto :goto_a

    #@28
    .line 890
    :pswitch_28
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgSvcAgentSync(Landroid/os/Message;)Z

    #@2b
    goto :goto_a

    #@2c
    .line 893
    :pswitch_2c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgXdmAgentConnected(Landroid/os/Message;)Z

    #@2f
    goto :goto_a

    #@30
    .line 896
    :pswitch_30
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgXdmAgentGetRLSListFailed(Landroid/os/Message;)Z

    #@33
    goto :goto_a

    #@34
    .line 899
    :pswitch_34
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StatePreprocess_IPMsgXdmAgentGetRLSListSuccess(Landroid/os/Message;)Z

    #@37
    goto :goto_a

    #@38
    .line 902
    :pswitch_38
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgContactCapaQuery(Landroid/os/Message;)Z

    #@3b
    goto :goto_a

    #@3c
    .line 905
    :pswitch_3c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgContactAddRcsFriend(Landroid/os/Message;)Z

    #@3f
    goto :goto_a

    #@40
    .line 908
    :pswitch_40
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgContactDeleteRcsFriend(Landroid/os/Message;)Z

    #@43
    goto :goto_a

    #@44
    .line 911
    :pswitch_44
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgContactAddNonRcsFriend(Landroid/os/Message;)Z

    #@47
    goto :goto_a

    #@48
    .line 914
    :pswitch_48
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgContactOneuser_Query(Landroid/os/Message;)Z

    #@4b
    goto :goto_a

    #@4c
    .line 917
    :pswitch_4c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgContactDeleteRcsFriendList(Landroid/os/Message;)Z

    #@4f
    goto :goto_a

    #@50
    .line 920
    :pswitch_50
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgXdmAgentRLSSyncFailed(Landroid/os/Message;)Z

    #@53
    goto :goto_a

    #@54
    .line 923
    :pswitch_54
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgXdmAgentGetMyStatusIconThumb(Landroid/os/Message;)Z

    #@57
    goto :goto_a

    #@58
    .line 926
    :pswitch_58
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgXdmAgentGetMyStatusIcon(Landroid/os/Message;)Z

    #@5b
    goto :goto_a

    #@5c
    .line 929
    :pswitch_5c
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgXdmAgentGetBuddyStatusIconThumb(Landroid/os/Message;)Z

    #@5f
    goto :goto_a

    #@60
    .line 932
    :pswitch_60
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_IPMsgXdmAgentGetBuddyStatusIcon(Landroid/os/Message;)Z

    #@63
    goto :goto_a

    #@64
    .line 867
    :pswitch_data_64
    .packed-switch 0x2
        :pswitch_c
        :pswitch_2c
        :pswitch_10
        :pswitch_14
        :pswitch_38
        :pswitch_3c
        :pswitch_40
        :pswitch_44
        :pswitch_48
        :pswitch_4c
        :pswitch_5
        :pswitch_5
        :pswitch_18
        :pswitch_5
        :pswitch_24
        :pswitch_1c
        :pswitch_20
        :pswitch_28
        :pswitch_5
        :pswitch_5
        :pswitch_30
        :pswitch_34
        :pswitch_50
        :pswitch_54
        :pswitch_58
        :pswitch_5c
        :pswitch_60
    .end packed-switch
.end method

.method private UnregisterBroadcastReceiver()V
    .registers 3

    #@0
    .prologue
    .line 506
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPBroadcastReceiver:Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 507
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContext:Landroid/content/Context;

    #@6
    iget-object v1, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPBroadcastReceiver:Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@b
    .line 508
    const/4 v0, 0x0

    #@c
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPBroadcastReceiver:Lcom/lge/ims/service/ip/IPAgent$IPBroadcastReceiver;

    #@e
    .line 510
    :cond_e
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/service/ip/IPAgent;I)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->GetMessageWhat(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(Lcom/lge/ims/service/ip/IPAgent;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent;->GetState()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1000(Lcom/lge/ims/service/ip/IPAgent;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget v0, p0, Lcom/lge/ims/service/ip/IPAgent;->nState:I

    #@2
    return v0
.end method

.method static synthetic access$1100(Lcom/lge/ims/service/ip/IPAgent;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/lge/ims/service/ip/IPAgent;->bXDMConnected:Z

    #@2
    return v0
.end method

.method static synthetic access$1200(Lcom/lge/ims/service/ip/IPAgent;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 57
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent;->getAutoConfigurationDB()V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPBGHelper;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objBGHelper:Lcom/lge/ims/service/ip/IPBGHelper;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/ims/service/ip/IPAgent;I)Ljava/lang/String;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->GetStateName(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$300(Lcom/lge/ims/service/ip/IPAgent;)Lcom/lge/ims/service/ip/IPContactHelper;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContactHelper:Lcom/lge/ims/service/ip/IPContactHelper;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/lge/ims/service/ip/IPAgent;Landroid/os/Message;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_MessageHandler(Landroid/os/Message;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Lcom/lge/ims/service/ip/IPAgent;Landroid/os/Message;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEOFF_BGOFF_MessageHandler(Landroid/os/Message;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(Lcom/lge/ims/service/ip/IPAgent;Landroid/os/Message;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_BGOFF_MessageHandler(Landroid/os/Message;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/lge/ims/service/ip/IPAgent;Landroid/os/Message;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPAgent;->StateSERVICEON_MessageHandler(Landroid/os/Message;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$800()Landroid/app/AlarmManager;
    .registers 1

    #@0
    .prologue
    .line 57
    sget-object v0, Lcom/lge/ims/service/ip/IPAgent;->objAlarmManager:Landroid/app/AlarmManager;

    #@2
    return-object v0
.end method

.method static synthetic access$802(Landroid/app/AlarmManager;)Landroid/app/AlarmManager;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 57
    sput-object p0, Lcom/lge/ims/service/ip/IPAgent;->objAlarmManager:Landroid/app/AlarmManager;

    #@2
    return-object p0
.end method

.method static synthetic access$900(Lcom/lge/ims/service/ip/IPAgent;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 57
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private getAutoConfigurationDB()V
    .registers 3

    #@0
    .prologue
    .line 350
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent;->GetProvisioningValue()V

    #@3
    .line 351
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@5
    iget v1, p0, Lcom/lge/ims/service/ip/IPAgent;->nPollingTimer:I

    #@7
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->setPollingPeriod(I)V

    #@a
    .line 352
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPTimer:Lcom/lge/ims/service/ip/IPAgent$IPTimer;

    #@c
    iget v1, p0, Lcom/lge/ims/service/ip/IPAgent;->nRLSSubPeriodTimer:I

    #@e
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/IPAgent$IPTimer;->setRLSSubPeriod(I)V

    #@11
    .line 353
    return-void
.end method


# virtual methods
.method public ConnectNativeBind()V
    .registers 2

    #@0
    .prologue
    .line 165
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 166
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@7
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->StartNativeConnection()V

    #@a
    .line 167
    return-void
.end method

.method public GetHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 689
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method public SendRLSListFetch()V
    .registers 2

    #@0
    .prologue
    .line 334
    const-string v0, "[IP]"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 335
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@7
    if-nez v0, :cond_a

    #@9
    .line 339
    :goto_9
    return-void

    #@a
    .line 338
    :cond_a
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentImpl:Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent$IPAgentImpl;->SendRLSListQuery()Z

    #@f
    goto :goto_9
.end method

.method public run()V
    .registers 2

    #@0
    .prologue
    .line 198
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent;->Initialize()V

    #@3
    .line 200
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@6
    .line 202
    new-instance v0, Lcom/lge/ims/service/ip/IPAgent$1;

    #@8
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPAgent$1;-><init>(Lcom/lge/ims/service/ip/IPAgent;)V

    #@b
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPAgent;->objIPAgentHandler:Landroid/os/Handler;

    #@d
    .line 328
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@10
    .line 330
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPAgent;->Deinitialize()V

    #@13
    .line 331
    return-void
.end method
