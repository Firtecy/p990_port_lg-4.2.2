.class public Lcom/lge/ims/service/ac/content/ACSubscriber;
.super Ljava/lang/Object;
.source "ACSubscriber.java"


# instance fields
.field public mAuthPassword:Ljava/lang/String;

.field public mAuthRealm:Ljava/lang/String;

.field public mAuthType:Ljava/lang/String;

.field public mAuthUsername:Ljava/lang/String;

.field public mHomeDomainName:Ljava/lang/String;

.field public mIMPI:Ljava/lang/String;

.field public mIMPUs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 20
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 9
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@6
    .line 10
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@8
    .line 11
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@a
    .line 13
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@c
    .line 14
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@e
    .line 15
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@10
    .line 16
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@12
    .line 21
    new-instance v0, Ljava/util/ArrayList;

    #@14
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@17
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@19
    .line 22
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 25
    iput-object v1, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@3
    .line 26
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@8
    .line 27
    iput-object v1, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@a
    .line 29
    iput-object v1, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@c
    .line 30
    iput-object v1, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@e
    .line 31
    iput-object v1, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@10
    .line 32
    iput-object v1, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@12
    .line 33
    return-void
.end method

.method public containsContent(Lcom/lge/ims/service/ac/content/ACSubscriber;)Z
    .registers 8
    .parameter "cache"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 36
    if-nez p1, :cond_5

    #@4
    .line 92
    :cond_4
    :goto_4
    return v2

    #@5
    .line 40
    :cond_5
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@7
    if-eqz v4, :cond_19

    #@9
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@b
    if-eqz v4, :cond_19

    #@d
    .line 41
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@f
    iget-object v5, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v4

    #@15
    if-nez v4, :cond_19

    #@17
    move v2, v3

    #@18
    .line 42
    goto :goto_4

    #@19
    .line 46
    :cond_19
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@1b
    if-eqz v4, :cond_65

    #@1d
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@1f
    if-eqz v4, :cond_65

    #@21
    .line 47
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@26
    move-result v4

    #@27
    if-lez v4, :cond_65

    #@29
    .line 48
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@2e
    move-result v4

    #@2f
    iget-object v5, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@31
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    #@34
    move-result v5

    #@35
    if-eq v4, v5, :cond_39

    #@37
    move v2, v3

    #@38
    .line 49
    goto :goto_4

    #@39
    .line 52
    :cond_39
    const/4 v0, 0x0

    #@3a
    .local v0, i:I
    :goto_3a
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@3f
    move-result v4

    #@40
    if-ge v0, v4, :cond_65

    #@42
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@44
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@47
    move-result v4

    #@48
    if-ge v0, v4, :cond_65

    #@4a
    .line 53
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@4c
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@4f
    move-result-object v1

    #@50
    check-cast v1, Ljava/lang/String;

    #@52
    .line 55
    .local v1, impu:Ljava/lang/String;
    if-eqz v1, :cond_62

    #@54
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@56
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@59
    move-result-object v4

    #@5a
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5d
    move-result v4

    #@5e
    if-nez v4, :cond_62

    #@60
    move v2, v3

    #@61
    .line 56
    goto :goto_4

    #@62
    .line 52
    :cond_62
    add-int/lit8 v0, v0, 0x1

    #@64
    goto :goto_3a

    #@65
    .line 62
    .end local v0           #i:I
    .end local v1           #impu:Ljava/lang/String;
    :cond_65
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@67
    if-eqz v4, :cond_79

    #@69
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@6b
    if-eqz v4, :cond_79

    #@6d
    .line 63
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@6f
    iget-object v5, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@71
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@74
    move-result v4

    #@75
    if-nez v4, :cond_79

    #@77
    move v2, v3

    #@78
    .line 64
    goto :goto_4

    #@79
    .line 68
    :cond_79
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@7b
    if-eqz v4, :cond_8e

    #@7d
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@7f
    if-eqz v4, :cond_8e

    #@81
    .line 69
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@83
    iget-object v5, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@85
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@88
    move-result v4

    #@89
    if-nez v4, :cond_8e

    #@8b
    move v2, v3

    #@8c
    .line 70
    goto/16 :goto_4

    #@8e
    .line 74
    :cond_8e
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@90
    if-eqz v4, :cond_a3

    #@92
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@94
    if-eqz v4, :cond_a3

    #@96
    .line 75
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@98
    iget-object v5, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@9a
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9d
    move-result v4

    #@9e
    if-nez v4, :cond_a3

    #@a0
    move v2, v3

    #@a1
    .line 76
    goto/16 :goto_4

    #@a3
    .line 80
    :cond_a3
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@a5
    if-eqz v4, :cond_b8

    #@a7
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@a9
    if-eqz v4, :cond_b8

    #@ab
    .line 81
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@ad
    iget-object v5, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@af
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b2
    move-result v4

    #@b3
    if-nez v4, :cond_b8

    #@b5
    move v2, v3

    #@b6
    .line 82
    goto/16 :goto_4

    #@b8
    .line 86
    :cond_b8
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@ba
    if-eqz v4, :cond_4

    #@bc
    iget-object v4, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@be
    if-eqz v4, :cond_4

    #@c0
    .line 87
    iget-object v4, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@c2
    iget-object v5, p1, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@c4
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c7
    move-result v4

    #@c8
    if-nez v4, :cond_4

    #@ca
    move v2, v3

    #@cb
    .line 88
    goto/16 :goto_4
.end method

.method public isContentPresent()Z
    .registers 2

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@2
    if-nez v0, :cond_20

    #@4
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_20

    #@c
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@e
    if-nez v0, :cond_20

    #@10
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@12
    if-nez v0, :cond_20

    #@14
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@16
    if-nez v0, :cond_20

    #@18
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@1a
    if-nez v0, :cond_20

    #@1c
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@1e
    if-eqz v0, :cond_22

    #@20
    :cond_20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method public toString()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 103
    .local v0, builder:Ljava/lang/StringBuilder;
    const-string v2, "impi="

    #@7
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    iget-object v3, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPI:Ljava/lang/String;

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    .line 104
    const-string v2, ", home_domain_name="

    #@13
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget-object v3, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mHomeDomainName:Ljava/lang/String;

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    .line 106
    const/4 v1, 0x0

    #@1e
    .local v1, i:I
    :goto_1e
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@20
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@23
    move-result v2

    #@24
    if-ge v1, v2, :cond_45

    #@26
    .line 107
    const-string v2, ", impu("

    #@28
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, ")="

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mIMPUs:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@3b
    move-result-object v2

    #@3c
    check-cast v2, Ljava/lang/String;

    #@3e
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    .line 106
    add-int/lit8 v1, v1, 0x1

    #@44
    goto :goto_1e

    #@45
    .line 110
    :cond_45
    const-string v2, ", auth_type="

    #@47
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    iget-object v3, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthType:Ljava/lang/String;

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    const-string v3, ", auth_realm="

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    iget-object v3, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthRealm:Ljava/lang/String;

    #@59
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v0

    #@5d
    .line 111
    const-string v2, ", auth_username="

    #@5f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v2

    #@63
    iget-object v3, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthUsername:Ljava/lang/String;

    #@65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    const-string v3, ", auth_password="

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    iget-object v3, p0, Lcom/lge/ims/service/ac/content/ACSubscriber;->mAuthPassword:Ljava/lang/String;

    #@71
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v0

    #@75
    .line 113
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v2

    #@79
    return-object v2
.end method
