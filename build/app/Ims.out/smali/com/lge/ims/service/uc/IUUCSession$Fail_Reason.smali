.class public Lcom/lge/ims/service/uc/IUUCSession$Fail_Reason;
.super Ljava/lang/Object;
.source "IUUCSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUUCSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Fail_Reason"
.end annotation


# static fields
.field public static final CODE_1XRETRY_NONE:I = 0x0

.field public static final CODE_1XRETRY_NORMAL:I = 0x1

.field public static final CODE_1XRETRY_PRIORITY_SET:I = 0x3

.field public static final CODE_1XRETRY_SILENT_REDIAL:I = 0x2

.field public static final FAIL_REASON_CONF_BUSY:I = 0x21e

.field public static final FAIL_REASON_CONF_INTERNAL_ERROR:I = 0x221

.field public static final FAIL_REASON_CONF_NOT_ACCEPTABLE:I = 0x21d

.field public static final FAIL_REASON_CONF_NOT_ACCEPTABLE_HERE:I = 0x21f

.field public static final FAIL_REASON_CONF_TIMEOUT:I = 0x220

.field public static final FAIL_REASON_CONF_UNKNWON:I = 0x222

.field public static final FAIL_REASON_FORBIDDEN_BARRING:I = 0x1f5

.field public static final FAIL_REASON_FORBIDDEN_EXPIRATION:I = 0x1f7

.field public static final FAIL_REASON_FORBIDDEN_NOPROFILE:I = 0x1f6

.field public static final FAIL_REASON_FORBIDDEN_NOSUBSCRIBER:I = 0x1f8

.field public static final FAIL_REASON_MEDIA_INITFAIL:I = 0x231

.field public static final FAIL_REASON_MEDIA_NODATA:I = 0x233

.field public static final FAIL_REASON_MEDIA_NOMATCH:I = 0x232

.field public static final FAIL_REASON_MEDIA_UNKNOWN:I = 0x234

.field public static final FAIL_REASON_NETWORK_IPCHANGE:I = 0xcb

.field public static final FAIL_REASON_NETWORK_OUTOFCOVERAGE:I = 0xc9

.field public static final FAIL_REASON_NETWORK_ROAMING:I = 0xca

.field public static final FAIL_REASON_SERVICE_INCSCALL:I = 0x68

.field public static final FAIL_REASON_SERVICE_INVTCALL:I = 0x69

.field public static final FAIL_REASON_SERVICE_LOWBATTERY:I = 0x67

.field public static final FAIL_REASON_SERVICE_MAXCALL:I = 0x6a

.field public static final FAIL_REASON_SERVICE_NOTREGISTERED:I = 0x6b

.field public static final FAIL_REASON_SERVICE_NOTSUPPORTCALL:I = 0x66

.field public static final FAIL_REASON_SERVICE_UNAVAILABLE:I = 0x65

.field public static final FAIL_REASON_SESSION_BADADDRESS:I = 0x196

.field public static final FAIL_REASON_SESSION_BADRESPONSE:I = 0x19b

.field public static final FAIL_REASON_SESSION_BUSY:I = 0x197

.field public static final FAIL_REASON_SESSION_CANCELED:I = 0x199

.field public static final FAIL_REASON_SESSION_FORBIDDEN:I = 0x192

.field public static final FAIL_REASON_SESSION_NONE:I = 0x0

.field public static final FAIL_REASON_SESSION_NOTACCEPTABLE:I = 0x19c

.field public static final FAIL_REASON_SESSION_NOTFOUND:I = 0x193

.field public static final FAIL_REASON_SESSION_NOTREACHABLE:I = 0x19e

.field public static final FAIL_REASON_SESSION_NOTSUPPORTED:I = 0x194

.field public static final FAIL_REASON_SESSION_PRECONDITION:I = 0x1a4

.field public static final FAIL_REASON_SESSION_REDIRECTION:I = 0x19f

.field public static final FAIL_REASON_SESSION_REJECTED:I = 0x198

.field public static final FAIL_REASON_SESSION_RETRY1X:I = 0x1a1

.field public static final FAIL_REASON_SESSION_RETRYVOLTE:I = 0x1a3

.field public static final FAIL_REASON_SESSION_SERVERERROR:I = 0x191

.field public static final FAIL_REASON_SESSION_SETUPFAILED:I = 0x1a0

.field public static final FAIL_REASON_SESSION_SRVCC:I = 0x1a2

.field public static final FAIL_REASON_SESSION_TEMPUNAVAILABLE:I = 0x195

.field public static final FAIL_REASON_SESSION_TERMINATED:I = 0x19a

.field public static final FAIL_REASON_SESSION_TIMEOUT:I = 0x19d

.field public static final FAIL_REASON_SESSION_UNKNOWN:I = 0x1

.field public static final FAIL_REASON_TO_MO_PROGRESSING:I = 0x209

.field public static final FAIL_REASON_TO_MO_STARTED:I = 0x20a

.field public static final FAIL_REASON_TO_MO_UPDATE:I = 0x20b

.field public static final FAIL_REASON_TO_MT_NOANSWER:I = 0x20c

.field public static final FAIL_REASON_TO_MT_UPDATE:I = 0x20d


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/uc/IUUCSession;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/uc/IUUCSession;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 93
    iput-object p1, p0, Lcom/lge/ims/service/uc/IUUCSession$Fail_Reason;->this$0:Lcom/lge/ims/service/uc/IUUCSession;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method
