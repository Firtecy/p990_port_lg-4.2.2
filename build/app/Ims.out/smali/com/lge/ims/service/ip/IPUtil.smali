.class public Lcom/lge/ims/service/ip/IPUtil;
.super Ljava/lang/Object;
.source "IPUtil.java"


# static fields
.field static final CONTENT_URI:Ljava/lang/String; = "content://com.lge.ims.provisioning/settings"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 25
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static CastGlobalNumber(Ljava/lang/String;)Ljava/lang/String;
    .registers 1
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 37
    return-object p0
.end method

.method public static IsEqualsNumber(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "strANumber"
    .parameter "strBNumber"

    #@0
    .prologue
    .line 56
    invoke-virtual {p0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_8

    #@6
    .line 57
    const/4 v0, 0x1

    #@7
    .line 60
    :goto_7
    return v0

    #@8
    .line 59
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "[IP][ERROR]IsEqualsNumber is false - A number [ "

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    const-string v1, " ], B number [ "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    const-string v1, " ]"

    #@23
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@2e
    .line 60
    const/4 v0, 0x0

    #@2f
    goto :goto_7
.end method

.method public static IsGlobalNumber(Ljava/lang/String;)Z
    .registers 2
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 33
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public static IsRCSService(Landroid/content/Context;)Z
    .registers 2
    .parameter "objContext"

    #@0
    .prologue
    .line 29
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public static IsValidMSISDN(Ljava/lang/String;)Z
    .registers 8
    .parameter "strMSISDN"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 41
    if-eqz p0, :cond_9

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    #@6
    move-result v5

    #@7
    if-eqz v5, :cond_f

    #@9
    .line 42
    :cond_9
    const-string v5, "[IP][ERROR]strMSISDN is null"

    #@b
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e
    .line 52
    :cond_e
    :goto_e
    return v4

    #@f
    .line 45
    :cond_f
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    #@12
    move-result v1

    #@13
    .line 46
    .local v1, bEmergencyNumber:Z
    const/4 v2, 0x0

    #@14
    .line 47
    .local v2, bLength:Z
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@17
    move-result v5

    #@18
    const/16 v6, 0x9

    #@1a
    if-le v5, v6, :cond_1d

    #@1c
    .line 48
    const/4 v2, 0x1

    #@1d
    .line 50
    :cond_1d
    const/4 v5, 0x2

    #@1e
    invoke-virtual {p0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    .line 51
    .local v3, szTemp:Ljava/lang/String;
    const-string v5, "01"

    #@24
    invoke-virtual {v3, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@27
    move-result v0

    #@28
    .line 52
    .local v0, b01x:Z
    if-nez v1, :cond_e

    #@2a
    if-eqz v2, :cond_e

    #@2c
    if-eqz v0, :cond_e

    #@2e
    const/4 v4, 0x1

    #@2f
    goto :goto_e
.end method

.method public static NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "strMSISDN"

    #@0
    .prologue
    .line 73
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_d

    #@6
    .line 74
    const-string v1, "[IP][ERROR]MSISDN is empty or invalid"

    #@8
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@b
    .line 75
    const/4 v0, 0x0

    #@c
    .line 78
    :goto_c
    return-object v0

    #@d
    .line 77
    :cond_d
    invoke-static {p0}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    .line 78
    .local v0, strModifiedMSISDN:Ljava/lang/String;
    goto :goto_c
.end method
