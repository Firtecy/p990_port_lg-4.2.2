.class public Lcom/lge/ims/service/im/HttpFileTransferImpl;
.super Lcom/lge/ims/service/im/FileTransferImpl;
.source "HttpFileTransferImpl.java"


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;)V
    .registers 4
    .parameter "fileTransferImplFacade"
    .parameter "context"

    #@0
    .prologue
    .line 14
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/service/im/FileTransferImpl;-><init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;)V

    #@3
    .line 15
    const-string v0, ""

    #@5
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public constructor <init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;I)V
    .registers 5
    .parameter "fileTransferImplFacade"
    .parameter "context"
    .parameter "nativeObj"

    #@0
    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/ims/service/im/FileTransferImpl;-><init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;I)V

    #@3
    .line 25
    const-string v0, ""

    #@5
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8
    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;[Ljava/lang/String;)V
    .registers 5
    .parameter "fileTransferImplFacade"
    .parameter "context"
    .parameter "remoteUsers"

    #@0
    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/ims/service/im/FileTransferImpl;-><init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;[Ljava/lang/String;)V

    #@3
    .line 20
    const-string v0, ""

    #@5
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method


# virtual methods
.method public download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 12
    .parameter "fileName"
    .parameter "url"
    .parameter "contentType"
    .parameter "fileSize"
    .parameter "folderPath"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 110
    new-instance v3, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v4, "file name = "

    #@8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@17
    .line 111
    new-instance v3, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v4, "file url = "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2d
    .line 112
    new-instance v3, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v4, "file contentType = "

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v3

    #@3c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v3

    #@40
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@43
    .line 113
    new-instance v3, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v4, "folder path for download = "

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@59
    .line 115
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    #@5c
    move-result v3

    #@5d
    add-int/lit8 v1, v3, -0x1

    #@5f
    .line 116
    .local v1, length:I
    const-string v3, "/"

    #@61
    invoke-virtual {p5, v3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@64
    move-result v3

    #@65
    if-nez v3, :cond_7a

    #@67
    .line 117
    new-instance v3, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    const-string v4, "/"

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object p5

    #@7a
    .line 120
    :cond_7a
    if-eqz p1, :cond_92

    #@7c
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@7f
    move-result v3

    #@80
    if-lt v3, v5, :cond_92

    #@82
    if-eqz p2, :cond_92

    #@84
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@87
    move-result v3

    #@88
    if-lt v3, v5, :cond_92

    #@8a
    if-eqz p3, :cond_92

    #@8c
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    #@8f
    move-result v3

    #@90
    if-ge v3, v5, :cond_a2

    #@92
    .line 123
    :cond_92
    const-string v3, "wrong requirement to download : transferStartFailed(-2)"

    #@94
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@97
    .line 125
    :try_start_97
    iget-object v3, p0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@99
    const/4 v4, -0x2

    #@9a
    invoke-interface {v3, v4}, Lcom/lge/ims/service/im/IFileTransferListener;->transferStartFailed(I)V
    :try_end_9d
    .catch Landroid/os/RemoteException; {:try_start_97 .. :try_end_9d} :catch_9e

    #@9d
    .line 148
    :goto_9d
    return-void

    #@9e
    .line 127
    :catch_9e
    move-exception v0

    #@9f
    .line 129
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@a2
    .line 133
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_a2
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a5
    move-result-object v2

    #@a6
    .line 135
    .local v2, parcel:Landroid/os/Parcel;
    const/16 v3, 0x29a9

    #@a8
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@ab
    .line 137
    if-eqz p5, :cond_b3

    #@ad
    invoke-virtual {p5}, Ljava/lang/String;->length()I

    #@b0
    move-result v3

    #@b1
    if-ge v3, v5, :cond_b5

    #@b3
    .line 138
    :cond_b3
    const-string p5, ""

    #@b5
    .line 141
    :cond_b5
    invoke-virtual {v2, p5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@b8
    .line 142
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@bb
    .line 143
    invoke-virtual {v2, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@be
    .line 144
    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@c1
    .line 145
    invoke-virtual {v2, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@c4
    .line 147
    invoke-virtual {p0, v2}, Lcom/lge/ims/service/im/HttpFileTransferImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@c7
    goto :goto_9d
.end method

.method public getInterface()I
    .registers 2

    #@0
    .prologue
    .line 29
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 30
    const/16 v0, 0x19

    #@7
    invoke-static {v0}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public sendFile(Lcom/lge/ims/service/im/IMFileInfo;Ljava/lang/String;)V
    .registers 16
    .parameter "fileInfo"
    .parameter "contributionId"

    #@0
    .prologue
    .line 34
    iget-object v10, p0, Lcom/lge/ims/service/im/FileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@2
    if-nez v10, :cond_5

    #@4
    .line 106
    :cond_4
    :goto_4
    return-void

    #@5
    .line 38
    :cond_5
    iget-object v10, p0, Lcom/lge/ims/service/im/FileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@7
    if-eqz v10, :cond_f

    #@9
    iget-object v10, p0, Lcom/lge/ims/service/im/FileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@b
    array-length v10, v10

    #@c
    const/4 v11, 0x1

    #@d
    if-lt v10, v11, :cond_4

    #@f
    .line 43
    :cond_f
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getFilePath()Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    .line 45
    .local v3, filePath:Ljava/lang/String;
    if-eqz v3, :cond_1c

    #@15
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@18
    move-result v10

    #@19
    const/4 v11, 0x1

    #@1a
    if-ge v10, v11, :cond_45

    #@1c
    .line 46
    :cond_1c
    new-instance v10, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v11, "filePath = "

    #@23
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v10

    #@27
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v10

    #@2b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v10

    #@2f
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@32
    .line 47
    const-string v10, "transferStartFailed(-2)"

    #@34
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@37
    .line 49
    :try_start_37
    iget-object v10, p0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@39
    const/4 v11, -0x2

    #@3a
    invoke-interface {v10, v11}, Lcom/lge/ims/service/im/IFileTransferListener;->transferStartFailed(I)V
    :try_end_3d
    .catchall {:try_start_37 .. :try_end_3d} :catchall_43
    .catch Landroid/os/RemoteException; {:try_start_37 .. :try_end_3d} :catch_3e

    #@3d
    goto :goto_4

    #@3e
    .line 50
    :catch_3e
    move-exception v0

    #@3f
    .line 52
    .local v0, e:Landroid/os/RemoteException;
    :try_start_3f
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_42
    .catchall {:try_start_3f .. :try_end_42} :catchall_43

    #@42
    goto :goto_4

    #@43
    .line 53
    .end local v0           #e:Landroid/os/RemoteException;
    :catchall_43
    move-exception v10

    #@44
    throw v10

    #@45
    .line 59
    :cond_45
    const-string v10, "/"

    #@47
    invoke-virtual {v3, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@4a
    move-result v10

    #@4b
    add-int/lit8 v10, v10, 0x1

    #@4d
    invoke-virtual {v3, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    .line 60
    .local v2, fileName:Ljava/lang/String;
    invoke-static {v2}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v2

    #@55
    .line 62
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getContentType()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    .line 63
    .local v1, fileContentType:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getFileSize()I

    #@5c
    move-result v4

    #@5d
    .line 65
    .local v4, fileSize:I
    new-instance v10, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v11, "remoteUser[0]:"

    #@64
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v10

    #@68
    iget-object v11, p0, Lcom/lge/ims/service/im/FileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@6a
    const/4 v12, 0x0

    #@6b
    aget-object v11, v11, v12

    #@6d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v10

    #@71
    const-string v11, "\n"

    #@73
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v10

    #@77
    const-string v11, ", fileName:"

    #@79
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v10

    #@7d
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v10

    #@81
    const-string v11, "\n"

    #@83
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v10

    #@87
    const-string v11, ", filePath:"

    #@89
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v10

    #@8d
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v10

    #@91
    const-string v11, "\n"

    #@93
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v10

    #@97
    const-string v11, ", contentType:"

    #@99
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v10

    #@9d
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v10

    #@a1
    const-string v11, "\n"

    #@a3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v10

    #@a7
    const-string v11, ", size:"

    #@a9
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v10

    #@ad
    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v10

    #@b1
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v10

    #@b5
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@b8
    .line 72
    const-string v7, ""

    #@ba
    .line 73
    .local v7, thumbnailName:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getThumbnailFilePath()Ljava/lang/String;

    #@bd
    move-result-object v8

    #@be
    .line 74
    .local v8, thumbnailPath:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getThumbnailContentType()Ljava/lang/String;

    #@c1
    move-result-object v6

    #@c2
    .line 75
    .local v6, thumbnailContentType:Ljava/lang/String;
    invoke-virtual {p1}, Lcom/lge/ims/service/im/IMFileInfo;->getThumbnailSize()I

    #@c5
    move-result v9

    #@c6
    .line 77
    .local v9, thumbnailSize:I
    if-eqz v8, :cond_cf

    #@c8
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    #@cb
    move-result v10

    #@cc
    const/4 v11, 0x1

    #@cd
    if-ge v10, v11, :cond_140

    #@cf
    .line 78
    :cond_cf
    const-string v8, ""

    #@d1
    .line 79
    const-string v6, ""

    #@d3
    .line 80
    const/4 v9, 0x0

    #@d4
    .line 86
    :goto_d4
    new-instance v10, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v11, "thumbnail name : "

    #@db
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v10

    #@df
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v10

    #@e3
    const-string v11, "\n"

    #@e5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v10

    #@e9
    const-string v11, ", filePath:"

    #@eb
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v10

    #@ef
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v10

    #@f3
    const-string v11, "\n"

    #@f5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v10

    #@f9
    const-string v11, ", contentType:"

    #@fb
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v10

    #@ff
    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v10

    #@103
    const-string v11, "\n"

    #@105
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v10

    #@109
    const-string v11, ", size:"

    #@10b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10e
    move-result-object v10

    #@10f
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@112
    move-result-object v10

    #@113
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v10

    #@117
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@11a
    .line 91
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@11d
    move-result-object v5

    #@11e
    .line 93
    .local v5, parcel:Landroid/os/Parcel;
    const/16 v10, 0x29aa

    #@120
    invoke-virtual {v5, v10}, Landroid/os/Parcel;->writeInt(I)V

    #@123
    .line 95
    invoke-virtual {v5, v7}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@126
    .line 96
    invoke-virtual {v5, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@129
    .line 97
    invoke-virtual {v5, v6}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12c
    .line 98
    invoke-virtual {v5, v9}, Landroid/os/Parcel;->writeInt(I)V

    #@12f
    .line 100
    invoke-virtual {v5, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@132
    .line 101
    invoke-virtual {v5, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@135
    .line 102
    invoke-virtual {v5, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@138
    .line 103
    invoke-virtual {v5, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@13b
    .line 105
    invoke-virtual {p0, v5}, Lcom/lge/ims/service/im/HttpFileTransferImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@13e
    goto/16 :goto_4

    #@140
    .line 82
    .end local v5           #parcel:Landroid/os/Parcel;
    :cond_140
    const-string v10, "/"

    #@142
    invoke-virtual {v8, v10}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@145
    move-result v10

    #@146
    add-int/lit8 v10, v10, 0x1

    #@148
    invoke-virtual {v8, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@14b
    move-result-object v7

    #@14c
    .line 83
    invoke-static {v7}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@14f
    move-result-object v7

    #@150
    goto :goto_d4
.end method
