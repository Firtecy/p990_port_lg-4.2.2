.class Lcom/lge/ims/service/uc/UCCallManager$1;
.super Landroid/os/Handler;
.source "UCCallManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/UCCallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/uc/UCCallManager;


# direct methods
.method constructor <init>(Lcom/lge/ims/service/uc/UCCallManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 86
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCCallManager$1;->this$0:Lcom/lge/ims/service/uc/UCCallManager;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 88
    const-string v0, "UC"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "msg : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p1, Landroid/os/Message;->what:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 90
    iget v0, p1, Landroid/os/Message;->what:I

    #@1c
    sparse-switch v0, :sswitch_data_4c

    #@1f
    .line 117
    :goto_1f
    return-void

    #@20
    .line 92
    :sswitch_20
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager$1;->this$0:Lcom/lge/ims/service/uc/UCCallManager;

    #@22
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCCallManager;->notifyCallState()V

    #@25
    .line 93
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager$1;->this$0:Lcom/lge/ims/service/uc/UCCallManager;

    #@27
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCCallManager;->updateCallState()V

    #@2a
    .line 94
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager$1;->this$0:Lcom/lge/ims/service/uc/UCCallManager;

    #@2c
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCCallManager;->notifyToCSFB()V

    #@2f
    goto :goto_1f

    #@30
    .line 98
    :sswitch_30
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager$1;->this$0:Lcom/lge/ims/service/uc/UCCallManager;

    #@32
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCCallManager;->terminateCalls()V

    #@35
    goto :goto_1f

    #@36
    .line 102
    :sswitch_36
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager$1;->this$0:Lcom/lge/ims/service/uc/UCCallManager;

    #@38
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCCallManager;->destroyCalls()V

    #@3b
    goto :goto_1f

    #@3c
    .line 106
    :sswitch_3c
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCCallManager$1;->this$0:Lcom/lge/ims/service/uc/UCCallManager;

    #@3e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@40
    check-cast v0, Lcom/lge/ims/service/uc/UCSessionImpl;

    #@42
    invoke-virtual {v1, v0}, Lcom/lge/ims/service/uc/UCCallManager;->notifyStartedFail(Lcom/lge/ims/service/uc/UCSessionImpl;)V

    #@45
    goto :goto_1f

    #@46
    .line 111
    :sswitch_46
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager$1;->this$0:Lcom/lge/ims/service/uc/UCCallManager;

    #@48
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCCallManager;->updateEnableFromAC()V

    #@4b
    goto :goto_1f

    #@4c
    .line 90
    :sswitch_data_4c
    .sparse-switch
        0x3e8 -> :sswitch_20
        0x3e9 -> :sswitch_30
        0x3ea -> :sswitch_36
        0x3eb -> :sswitch_3c
        0x835 -> :sswitch_46
        0x836 -> :sswitch_46
    .end sparse-switch
.end method
