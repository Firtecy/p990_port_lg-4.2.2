.class public Lcom/lge/ims/service/im/IMMessage;
.super Ljava/lang/Object;
.source "IMMessage.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/im/IMMessage;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private content:Ljava/lang/String;

.field private contentType:Ljava/lang/String;

.field private messageId:Ljava/lang/String;

.field private timeStamp:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 33
    new-instance v0, Lcom/lge/ims/service/im/IMMessage$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/im/IMMessage$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/im/IMMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 12
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 3
    .parameter "in"

    #@0
    .prologue
    .line 15
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 16
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->messageId:Ljava/lang/String;

    #@9
    .line 17
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->content:Ljava/lang/String;

    #@f
    .line 18
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->contentType:Ljava/lang/String;

    #@15
    .line 19
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    iput-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->timeStamp:Ljava/lang/String;

    #@1b
    .line 20
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 30
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getContent()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->content:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getContentType()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 61
    iget-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->contentType:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getMessageId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 45
    iget-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->messageId:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getTimeStamp()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 69
    iget-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->timeStamp:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public setContent(Ljava/lang/String;)V
    .registers 2
    .parameter "content"

    #@0
    .prologue
    .line 57
    iput-object p1, p0, Lcom/lge/ims/service/im/IMMessage;->content:Ljava/lang/String;

    #@2
    .line 58
    return-void
.end method

.method public setContentType(Ljava/lang/String;)V
    .registers 2
    .parameter "contentType"

    #@0
    .prologue
    .line 65
    iput-object p1, p0, Lcom/lge/ims/service/im/IMMessage;->contentType:Ljava/lang/String;

    #@2
    .line 66
    return-void
.end method

.method public setMessageId(Ljava/lang/String;)V
    .registers 2
    .parameter "messageId"

    #@0
    .prologue
    .line 49
    iput-object p1, p0, Lcom/lge/ims/service/im/IMMessage;->messageId:Ljava/lang/String;

    #@2
    .line 50
    return-void
.end method

.method public setTimeStamp(Ljava/lang/String;)V
    .registers 2
    .parameter "timeStamp"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/lge/ims/service/im/IMMessage;->timeStamp:Ljava/lang/String;

    #@2
    .line 74
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 4
    .parameter "out"
    .parameter "flags"

    #@0
    .prologue
    .line 23
    iget-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->messageId:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@5
    .line 24
    iget-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->content:Ljava/lang/String;

    #@7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@a
    .line 25
    iget-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->contentType:Ljava/lang/String;

    #@c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    .line 26
    iget-object v0, p0, Lcom/lge/ims/service/im/IMMessage;->timeStamp:Ljava/lang/String;

    #@11
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 27
    return-void
.end method
