.class public interface abstract Lcom/lge/ims/service/cs/IContentShareListener;
.super Ljava/lang/Object;
.source "IContentShareListener.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cs/IContentShareListener$Stub;
    }
.end annotation


# virtual methods
.method public abstract onReceivedImageSession(Lcom/lge/ims/service/cs/IImageSession;Ljava/lang/String;Ljava/lang/String;I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onReceivedVideoSession(Lcom/lge/ims/service/cs/IVideoSession;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onReconfigured(I)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract onUpdateCapability(Lcom/lge/ims/service/cd/Capabilities;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
