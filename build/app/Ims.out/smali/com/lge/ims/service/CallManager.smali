.class public Lcom/lge/ims/service/CallManager;
.super Ljava/lang/Object;
.source "CallManager.java"

# interfaces
.implements Lcom/lge/ims/service/cd/CapabilitiesListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/CallManager$CallManagerHandler;,
        Lcom/lge/ims/service/CallManager$CallNode;,
        Lcom/lge/ims/service/CallManager$CallInfoListener;
    }
.end annotation


# static fields
.field public static final CALL_TYPE_CS:I = 0x1

.field public static final CALL_TYPE_VOIP:I = 0x3

.field public static final CALL_TYPE_VT:I = 0x2

.field private static final MSG_EXIT:I = 0x1

.field private static final MSG_UPDATE_FAKE_CAPABILITIES:I = 0x32

.field private static final ONE_TO_ONE_ONLY_SUPPORTED:Z = true

.field private static mCallMngr:Lcom/lge/ims/service/CallManager;


# instance fields
.field private mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

.field private mCallNodes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/lge/ims/service/CallManager$CallNode;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mIsServiceCapable:Z

.field private mListener:Lcom/lge/ims/service/CallManager$CallInfoListener;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 41
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/CallManager;->mCallMngr:Lcom/lge/ims/service/CallManager;

    #@3
    return-void
.end method

.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 159
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 43
    iput-object v2, p0, Lcom/lge/ims/service/CallManager;->mHandler:Landroid/os/Handler;

    #@6
    .line 44
    iput-object v2, p0, Lcom/lge/ims/service/CallManager;->mHandlerThread:Landroid/os/HandlerThread;

    #@8
    .line 47
    new-instance v1, Ljava/util/HashMap;

    #@a
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@d
    iput-object v1, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@f
    .line 48
    iput-object v2, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@11
    .line 50
    iput-object v2, p0, Lcom/lge/ims/service/CallManager;->mListener:Lcom/lge/ims/service/CallManager$CallInfoListener;

    #@13
    .line 55
    const/4 v1, 0x1

    #@14
    iput-boolean v1, p0, Lcom/lge/ims/service/CallManager;->mIsServiceCapable:Z

    #@16
    .line 160
    new-instance v1, Landroid/os/HandlerThread;

    #@18
    const-string v2, "CallManager"

    #@1a
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@1d
    iput-object v1, p0, Lcom/lge/ims/service/CallManager;->mHandlerThread:Landroid/os/HandlerThread;

    #@1f
    .line 162
    iget-object v1, p0, Lcom/lge/ims/service/CallManager;->mHandlerThread:Landroid/os/HandlerThread;

    #@21
    if-eqz v1, :cond_37

    #@23
    .line 163
    iget-object v1, p0, Lcom/lge/ims/service/CallManager;->mHandlerThread:Landroid/os/HandlerThread;

    #@25
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@28
    .line 165
    iget-object v1, p0, Lcom/lge/ims/service/CallManager;->mHandlerThread:Landroid/os/HandlerThread;

    #@2a
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@2d
    move-result-object v0

    #@2e
    .line 167
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_37

    #@30
    .line 168
    new-instance v1, Lcom/lge/ims/service/CallManager$CallManagerHandler;

    #@32
    invoke-direct {v1, p0, v0}, Lcom/lge/ims/service/CallManager$CallManagerHandler;-><init>(Lcom/lge/ims/service/CallManager;Landroid/os/Looper;)V

    #@35
    iput-object v1, p0, Lcom/lge/ims/service/CallManager;->mHandler:Landroid/os/Handler;

    #@37
    .line 171
    .end local v0           #looper:Landroid/os/Looper;
    :cond_37
    return-void
.end method

.method static synthetic access$002(Lcom/lge/ims/service/CallManager;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 27
    iput-object p1, p0, Lcom/lge/ims/service/CallManager;->mHandler:Landroid/os/Handler;

    #@2
    return-object p1
.end method

.method public static getInstance()Lcom/lge/ims/service/CallManager;
    .registers 1

    #@0
    .prologue
    .line 174
    sget-object v0, Lcom/lge/ims/service/CallManager;->mCallMngr:Lcom/lge/ims/service/CallManager;

    #@2
    if-nez v0, :cond_16

    #@4
    .line 175
    new-instance v0, Lcom/lge/ims/service/CallManager;

    #@6
    invoke-direct {v0}, Lcom/lge/ims/service/CallManager;-><init>()V

    #@9
    sput-object v0, Lcom/lge/ims/service/CallManager;->mCallMngr:Lcom/lge/ims/service/CallManager;

    #@b
    .line 177
    sget-object v0, Lcom/lge/ims/service/CallManager;->mCallMngr:Lcom/lge/ims/service/CallManager;

    #@d
    if-nez v0, :cond_16

    #@f
    .line 178
    const-string v0, "Instantiating a CallManager failed"

    #@11
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@14
    .line 179
    const/4 v0, 0x0

    #@15
    .line 183
    :goto_15
    return-object v0

    #@16
    :cond_16
    sget-object v0, Lcom/lge/ims/service/CallManager;->mCallMngr:Lcom/lge/ims/service/CallManager;

    #@18
    goto :goto_15
.end method

.method private notifyFakeCapabilities(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V
    .registers 7
    .parameter "sc"
    .parameter "resultCode"

    #@0
    .prologue
    .line 493
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 495
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x32

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 496
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    .line 497
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@c
    .line 499
    iget-object v1, p0, Lcom/lge/ims/service/CallManager;->mHandler:Landroid/os/Handler;

    #@e
    if-eqz v1, :cond_18

    #@10
    .line 500
    iget-object v1, p0, Lcom/lge/ims/service/CallManager;->mHandler:Landroid/os/Handler;

    #@12
    const-wide/16 v2, 0x64

    #@14
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@17
    .line 504
    :goto_17
    return-void

    #@18
    .line 502
    :cond_18
    const-string v1, "CallManagerHandler is null"

    #@1a
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@1d
    goto :goto_17
.end method


# virtual methods
.method public addCall(Ljava/lang/String;I)V
    .registers 10
    .parameter "number"
    .parameter "callType"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 227
    invoke-static {}, Lcom/lge/ims/Configuration;->isRCSeOn()Z

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_1f

    #@8
    .line 228
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "addCall :: RCSe is not supported; number="

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@1e
    .line 286
    :goto_1e
    return-void

    #@1f
    .line 232
    :cond_1f
    iget-object v3, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@21
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_3e

    #@27
    .line 233
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v4, "addCall :: duplicated number="

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v3

    #@3a
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3d
    goto :goto_1e

    #@3e
    .line 237
    :cond_3e
    new-instance v3, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v4, "addCall :: number="

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    const-string v4, ", call_type= "

    #@4f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v3

    #@5b
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5e
    .line 239
    new-instance v0, Lcom/lge/ims/service/CallManager$CallNode;

    #@60
    invoke-direct {v0, p0, p1, p2}, Lcom/lge/ims/service/CallManager$CallNode;-><init>(Lcom/lge/ims/service/CallManager;Ljava/lang/String;I)V

    #@63
    .line 241
    .local v0, callNode:Lcom/lge/ims/service/CallManager$CallNode;
    if-nez v0, :cond_6b

    #@65
    .line 242
    const-string v3, "Instantiating a CallNode failed"

    #@67
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@6a
    goto :goto_1e

    #@6b
    .line 246
    :cond_6b
    iget-object v3, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@6d
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@70
    move-result v3

    #@71
    if-nez v3, :cond_a8

    #@73
    .line 247
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityService;->getInstance()Lcom/lge/ims/service/cd/CapabilityService;

    #@76
    move-result-object v2

    #@77
    .line 249
    .local v2, cs:Lcom/lge/ims/service/cd/CapabilityService;
    if-eqz v2, :cond_7c

    #@79
    .line 250
    invoke-virtual {v2, p0}, Lcom/lge/ims/service/cd/CapabilityService;->addBroadcastListener(Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@7c
    .line 254
    :cond_7c
    const/4 v3, 0x2

    #@7d
    if-ne p2, v3, :cond_87

    #@7f
    .line 255
    new-instance v3, Lcom/lge/ims/service/cd/ServiceCapabilities;

    #@81
    invoke-direct {v3, p1, v5, v5}, Lcom/lge/ims/service/cd/ServiceCapabilities;-><init>(Ljava/lang/String;II)V

    #@84
    invoke-direct {p0, v3, v5}, Lcom/lge/ims/service/CallManager;->notifyFakeCapabilities(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V

    #@87
    .line 261
    .end local v2           #cs:Lcom/lge/ims/service/cd/CapabilityService;
    :cond_87
    :goto_87
    iget-object v3, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@89
    invoke-virtual {v3, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8c
    .line 263
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->getInstance()Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@8f
    move-result-object v1

    #@90
    .line 265
    .local v1, capStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;
    iget-object v3, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@92
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@95
    move-result v3

    #@96
    if-ne v3, v6, :cond_b3

    #@98
    .line 267
    if-eqz v1, :cond_a1

    #@9a
    .line 268
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->getCapabilityCallType()I

    #@9d
    move-result v3

    #@9e
    invoke-virtual {v1, v3, p1}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->setActiveCallNumber(ILjava/lang/String;)V

    #@a1
    .line 271
    :cond_a1
    iput-object v0, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@a3
    .line 285
    :cond_a3
    :goto_a3
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->queryCapabilities()Z

    #@a6
    goto/16 :goto_1e

    #@a8
    .line 257
    .end local v1           #capStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;
    :cond_a8
    iget-object v3, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@aa
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@ad
    move-result v3

    #@ae
    if-ne v3, v6, :cond_87

    #@b0
    .line 258
    iput-boolean v5, p0, Lcom/lge/ims/service/CallManager;->mIsServiceCapable:Z

    #@b2
    goto :goto_87

    #@b3
    .line 274
    .restart local v1       #capStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;
    :cond_b3
    if-eqz v1, :cond_be

    #@b5
    .line 275
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->getCapabilityCallType()I

    #@b8
    move-result v3

    #@b9
    const-string v4, ""

    #@bb
    invoke-virtual {v1, v3, v4}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->setActiveCallNumber(ILjava/lang/String;)V

    #@be
    .line 279
    :cond_be
    iget-object v3, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@c0
    if-eqz v3, :cond_a3

    #@c2
    .line 280
    iget-object v3, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@c4
    invoke-virtual {v3}, Lcom/lge/ims/service/CallManager$CallNode;->queryCapabilities()Z

    #@c7
    .line 281
    const/4 v3, 0x0

    #@c8
    iput-object v3, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@ca
    goto :goto_a3
.end method

.method public capabilitiesUpdated(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V
    .registers 7
    .parameter "sc"
    .parameter "resultCode"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 467
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v2, "capabilitiesUpdated :: "

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@17
    .line 469
    iget-object v1, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@19
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/ServiceCapabilities;->getPhoneNumber()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@20
    move-result v1

    #@21
    if-nez v1, :cond_3e

    #@23
    .line 470
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/ServiceCapabilities;->getPhoneNumber()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    const-string v2, " is not in the call node"

    #@32
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v1

    #@36
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39
    move-result-object v1

    #@3a
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@3d
    .line 490
    :goto_3d
    return-void

    #@3e
    .line 474
    :cond_3e
    iget-object v1, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@40
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/ServiceCapabilities;->getPhoneNumber()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@47
    move-result-object v0

    #@48
    check-cast v0, Lcom/lge/ims/service/CallManager$CallNode;

    #@4a
    .line 476
    .local v0, callNode:Lcom/lge/ims/service/CallManager$CallNode;
    if-eqz v0, :cond_58

    #@4c
    .line 477
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->isCapabilityQuerying()Z

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_58

    #@52
    .line 480
    const-string v1, "CapabilityQuery is in progress; so, ignore it ..."

    #@54
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@57
    goto :goto_3d

    #@58
    .line 485
    :cond_58
    invoke-virtual {p0}, Lcom/lge/ims/service/CallManager;->isServiceCapable()Z

    #@5b
    move-result v1

    #@5c
    if-eqz v1, :cond_66

    #@5e
    .line 486
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/ServiceCapabilities;->toCapabilities()Lcom/lge/ims/service/cd/Capabilities;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {p0, v1}, Lcom/lge/ims/service/CallManager;->notifyCapabilities(Lcom/lge/ims/service/cd/Capabilities;)V

    #@65
    goto :goto_3d

    #@66
    .line 488
    :cond_66
    new-instance v1, Lcom/lge/ims/service/cd/Capabilities;

    #@68
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/ServiceCapabilities;->getPhoneNumber()Ljava/lang/String;

    #@6b
    move-result-object v2

    #@6c
    invoke-direct {v1, v2, v3, v3}, Lcom/lge/ims/service/cd/Capabilities;-><init>(Ljava/lang/String;II)V

    #@6f
    invoke-virtual {p0, v1}, Lcom/lge/ims/service/CallManager;->notifyCapabilities(Lcom/lge/ims/service/cd/Capabilities;)V

    #@72
    goto :goto_3d
.end method

.method public holdCall(Ljava/lang/String;)V
    .registers 6
    .parameter "number"

    #@0
    .prologue
    .line 359
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "holdCall :: number="

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 361
    iget-object v2, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@18
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@1b
    move-result v2

    #@1c
    if-nez v2, :cond_24

    #@1e
    .line 362
    const-string v2, "holdCall :: not found"

    #@20
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@23
    .line 393
    :cond_23
    :goto_23
    return-void

    #@24
    .line 366
    :cond_24
    iget-object v2, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@26
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@29
    move-result v2

    #@2a
    const/4 v3, 0x1

    #@2b
    if-le v2, v3, :cond_4a

    #@2d
    .line 367
    new-instance v2, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v3, "holdCall :: size="

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    iget-object v3, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@3a
    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    #@3d
    move-result v3

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@49
    goto :goto_23

    #@4a
    .line 371
    :cond_4a
    const/4 v2, 0x0

    #@4b
    iput-boolean v2, p0, Lcom/lge/ims/service/CallManager;->mIsServiceCapable:Z

    #@4d
    .line 373
    iget-object v2, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@4f
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@52
    move-result-object v0

    #@53
    check-cast v0, Lcom/lge/ims/service/CallManager$CallNode;

    #@55
    .line 375
    .local v0, callNode:Lcom/lge/ims/service/CallManager$CallNode;
    if-eqz v0, :cond_23

    #@57
    iget-object v2, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@59
    if-ne v0, v2, :cond_23

    #@5b
    .line 377
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->getInstance()Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@5e
    move-result-object v1

    #@5f
    .line 379
    .local v1, capStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;
    if-eqz v1, :cond_6a

    #@61
    .line 380
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->getCapabilityCallType()I

    #@64
    move-result v2

    #@65
    const-string v3, ""

    #@67
    invoke-virtual {v1, v2, v3}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->setActiveCallNumber(ILjava/lang/String;)V

    #@6a
    .line 383
    :cond_6a
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->isCapabilityQuerying()Z

    #@6d
    move-result v2

    #@6e
    if-eqz v2, :cond_73

    #@70
    .line 384
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->abortCapabilityQuery()V

    #@73
    .line 387
    :cond_73
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->queryCapabilities()Z

    #@76
    .line 390
    const/4 v2, 0x0

    #@77
    iput-object v2, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@79
    goto :goto_23
.end method

.method public isActiveCallNumber(Ljava/lang/String;)Z
    .registers 4
    .parameter "number"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 437
    iget-object v1, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@3
    if-eqz v1, :cond_7

    #@5
    if-nez p1, :cond_8

    #@7
    .line 445
    :cond_7
    :goto_7
    return v0

    #@8
    .line 441
    :cond_8
    iget-object v1, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@a
    invoke-virtual {v1}, Lcom/lge/ims/service/CallManager$CallNode;->getNumber()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_7

    #@14
    .line 445
    const/4 v0, 0x1

    #@15
    goto :goto_7
.end method

.method public isServiceCapable()Z
    .registers 2

    #@0
    .prologue
    .line 449
    iget-boolean v0, p0, Lcom/lge/ims/service/CallManager;->mIsServiceCapable:Z

    #@2
    return v0
.end method

.method public notifyCapabilities(Lcom/lge/ims/service/cd/Capabilities;)V
    .registers 4
    .parameter "capabilities"

    #@0
    .prologue
    .line 453
    if-nez p1, :cond_8

    #@2
    .line 454
    const-string v0, "notifyCapabilities :: Capabilities is null"

    #@4
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@7
    .line 463
    :cond_7
    :goto_7
    return-void

    #@8
    .line 458
    :cond_8
    new-instance v0, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v1, "notifyCapabilities :: number="

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/Capabilities;->getPeerMSISDN()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@22
    .line 460
    iget-object v0, p0, Lcom/lge/ims/service/CallManager;->mListener:Lcom/lge/ims/service/CallManager$CallInfoListener;

    #@24
    if-eqz v0, :cond_7

    #@26
    .line 461
    iget-object v0, p0, Lcom/lge/ims/service/CallManager;->mListener:Lcom/lge/ims/service/CallManager$CallInfoListener;

    #@28
    invoke-interface {v0, p1}, Lcom/lge/ims/service/CallManager$CallInfoListener;->notifyCapabilities(Lcom/lge/ims/service/cd/Capabilities;)V

    #@2b
    goto :goto_7
.end method

.method public removeAllCalls()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 187
    const-string v5, ""

    #@3
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 189
    iget-object v5, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@8
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@b
    move-result v5

    #@c
    if-lez v5, :cond_67

    #@e
    .line 190
    iget-object v5, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@10
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@13
    move-result-object v2

    #@14
    .line 191
    .local v2, collection:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/lge/ims/service/CallManager$CallNode;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@17
    move-result-object v4

    #@18
    .line 193
    .local v4, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/lge/ims/service/CallManager$CallNode;>;"
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@1b
    move-result v5

    #@1c
    if-eqz v5, :cond_33

    #@1e
    .line 194
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Lcom/lge/ims/service/CallManager$CallNode;

    #@24
    .line 197
    .local v0, callNode:Lcom/lge/ims/service/CallManager$CallNode;
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->getInstance()Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@27
    move-result-object v1

    #@28
    .line 199
    .local v1, capStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;
    if-eqz v1, :cond_33

    #@2a
    .line 200
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->getCapabilityCallType()I

    #@2d
    move-result v5

    #@2e
    const-string v6, ""

    #@30
    invoke-virtual {v1, v5, v6}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->setActiveCallNumber(ILjava/lang/String;)V

    #@33
    .line 204
    .end local v0           #callNode:Lcom/lge/ims/service/CallManager$CallNode;
    .end local v1           #capStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;
    :cond_33
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityService;->getInstance()Lcom/lge/ims/service/cd/CapabilityService;

    #@36
    move-result-object v3

    #@37
    .line 206
    .local v3, cs:Lcom/lge/ims/service/cd/CapabilityService;
    if-eqz v3, :cond_3c

    #@39
    .line 207
    invoke-virtual {v3, p0}, Lcom/lge/ims/service/cd/CapabilityService;->removeBroadcastListener(Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@3c
    .line 211
    :cond_3c
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@3f
    move-result-object v4

    #@40
    .line 213
    :goto_40
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@43
    move-result v5

    #@44
    if-eqz v5, :cond_67

    #@46
    .line 214
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@49
    move-result-object v0

    #@4a
    check-cast v0, Lcom/lge/ims/service/CallManager$CallNode;

    #@4c
    .line 215
    .restart local v0       #callNode:Lcom/lge/ims/service/CallManager$CallNode;
    new-instance v5, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v6, "removeAllCalls :: number="

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->getNumber()Ljava/lang/String;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v5

    #@63
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@66
    goto :goto_40

    #@67
    .line 219
    .end local v0           #callNode:Lcom/lge/ims/service/CallManager$CallNode;
    .end local v2           #collection:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/lge/ims/service/CallManager$CallNode;>;"
    .end local v3           #cs:Lcom/lge/ims/service/cd/CapabilityService;
    .end local v4           #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/lge/ims/service/CallManager$CallNode;>;"
    :cond_67
    iget-object v5, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@69
    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    #@6c
    .line 221
    iput-object v7, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@6e
    .line 222
    iput-object v7, p0, Lcom/lge/ims/service/CallManager;->mListener:Lcom/lge/ims/service/CallManager$CallInfoListener;

    #@70
    .line 223
    const/4 v5, 0x1

    #@71
    iput-boolean v5, p0, Lcom/lge/ims/service/CallManager;->mIsServiceCapable:Z

    #@73
    .line 224
    return-void
.end method

.method public removeCall(Ljava/lang/String;)V
    .registers 10
    .parameter "number"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 289
    iget-object v5, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@3
    invoke-virtual {v5, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@6
    move-result v5

    #@7
    if-nez v5, :cond_20

    #@9
    .line 290
    new-instance v5, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v6, "removeCall :: not found, number="

    #@10
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v5

    #@18
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v5

    #@1c
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1f
    .line 356
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 294
    :cond_20
    iget-object v5, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@22
    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    move-result-object v0

    #@26
    check-cast v0, Lcom/lge/ims/service/CallManager$CallNode;

    #@28
    .line 296
    .local v0, callNode:Lcom/lge/ims/service/CallManager$CallNode;
    if-eqz v0, :cond_1f

    #@2a
    .line 300
    new-instance v5, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v6, "removeCall :: number="

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v5

    #@39
    const-string v6, ", call_type="

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->getCapabilityCallType()I

    #@42
    move-result v6

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v5

    #@4b
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4e
    .line 303
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->abortCapabilityQuery()V

    #@51
    .line 305
    iget-object v5, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@53
    invoke-virtual {v5, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@56
    .line 307
    iget-object v5, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@58
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@5b
    move-result v5

    #@5c
    if-le v5, v7, :cond_7b

    #@5e
    .line 308
    new-instance v5, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v6, "removeCall :: no action - size="

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    iget-object v6, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@6b
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@6e
    move-result v6

    #@6f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v5

    #@73
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v5

    #@77
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7a
    goto :goto_1f

    #@7b
    .line 312
    :cond_7b
    iget-object v5, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@7d
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@80
    move-result v5

    #@81
    if-ne v5, v7, :cond_99

    #@83
    .line 313
    iget-object v5, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@85
    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@88
    move-result-object v2

    #@89
    .line 314
    .local v2, collection:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/lge/ims/service/CallManager$CallNode;>;"
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@8c
    move-result-object v4

    #@8d
    .line 316
    .local v4, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/lge/ims/service/CallManager$CallNode;>;"
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@90
    move-result v5

    #@91
    if-eqz v5, :cond_99

    #@93
    .line 317
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@96
    move-result-object v0

    #@97
    .end local v0           #callNode:Lcom/lge/ims/service/CallManager$CallNode;
    check-cast v0, Lcom/lge/ims/service/CallManager$CallNode;

    #@99
    .line 339
    .end local v2           #collection:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/lge/ims/service/CallManager$CallNode;>;"
    .end local v4           #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/lge/ims/service/CallManager$CallNode;>;"
    .restart local v0       #callNode:Lcom/lge/ims/service/CallManager$CallNode;
    :cond_99
    iget-object v5, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@9b
    invoke-virtual {v5}, Ljava/util/HashMap;->size()I

    #@9e
    move-result v5

    #@9f
    if-nez v5, :cond_1f

    #@a1
    .line 341
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->getInstance()Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@a4
    move-result-object v1

    #@a5
    .line 343
    .local v1, capStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;
    if-eqz v1, :cond_b0

    #@a7
    .line 344
    invoke-virtual {v0}, Lcom/lge/ims/service/CallManager$CallNode;->getCapabilityCallType()I

    #@aa
    move-result v5

    #@ab
    const-string v6, ""

    #@ad
    invoke-virtual {v1, v5, v6}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->setActiveCallNumber(ILjava/lang/String;)V

    #@b0
    .line 347
    :cond_b0
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityService;->getInstance()Lcom/lge/ims/service/cd/CapabilityService;

    #@b3
    move-result-object v3

    #@b4
    .line 349
    .local v3, cs:Lcom/lge/ims/service/cd/CapabilityService;
    if-eqz v3, :cond_b9

    #@b6
    .line 350
    invoke-virtual {v3, p0}, Lcom/lge/ims/service/cd/CapabilityService;->removeBroadcastListener(Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@b9
    .line 353
    :cond_b9
    const/4 v5, 0x0

    #@ba
    iput-object v5, p0, Lcom/lge/ims/service/CallManager;->mActiveCallNode:Lcom/lge/ims/service/CallManager$CallNode;

    #@bc
    .line 354
    iput-boolean v7, p0, Lcom/lge/ims/service/CallManager;->mIsServiceCapable:Z

    #@be
    goto/16 :goto_1f
.end method

.method public setListener(Lcom/lge/ims/service/CallManager$CallInfoListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 433
    iput-object p1, p0, Lcom/lge/ims/service/CallManager;->mListener:Lcom/lge/ims/service/CallManager$CallInfoListener;

    #@2
    .line 434
    return-void
.end method

.method public unholdCall(Ljava/lang/String;)V
    .registers 4
    .parameter "number"

    #@0
    .prologue
    .line 396
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "unholdCall :: number="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 398
    iget-object v0, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@18
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_24

    #@1e
    .line 399
    const-string v0, "unholdCall :: not found"

    #@20
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@23
    .line 430
    :cond_23
    :goto_23
    return-void

    #@24
    .line 403
    :cond_24
    iget-object v0, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@26
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@29
    move-result v0

    #@2a
    const/4 v1, 0x1

    #@2b
    if-le v0, v1, :cond_23

    #@2d
    .line 404
    new-instance v0, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v1, "unholdCall :: size="

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    iget-object v1, p0, Lcom/lge/ims/service/CallManager;->mCallNodes:Ljava/util/HashMap;

    #@3a
    invoke-virtual {v1}, Ljava/util/HashMap;->size()I

    #@3d
    move-result v1

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v0

    #@42
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v0

    #@46
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@49
    goto :goto_23
.end method
