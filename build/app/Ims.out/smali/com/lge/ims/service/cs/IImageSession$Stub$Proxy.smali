.class Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;
.super Ljava/lang/Object;
.source "IImageSession.java"

# interfaces
.implements Lcom/lge/ims/service/cs/IImageSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cs/IImageSession$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 112
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 113
    iput-object p1, p0, Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 114
    return-void
.end method


# virtual methods
.method public accept(Ljava/lang/String;)I
    .registers 8
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 200
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 201
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 204
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.cs.IImageSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 205
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 206
    iget-object v3, p0, Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v4, 0x5

    #@13
    const/4 v5, 0x0

    #@14
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 207
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1a
    .line 208
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    .line 211
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 212
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 214
    return v2

    #@25
    .line 211
    .end local v2           #_result:I
    :catchall_25
    move-exception v3

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 212
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v3
.end method

.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 117
    iget-object v0, p0, Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 121
    const-string v0, "com.lge.ims.service.cs.IImageSession"

    #@2
    return-object v0
.end method

.method public getSessionType()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 143
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 144
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 147
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.cs.IImageSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 148
    iget-object v3, p0, Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x2

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 149
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 150
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 153
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 154
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 156
    return v2

    #@22
    .line 153
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 154
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public reject()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 218
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 219
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 222
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.cs.IImageSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 223
    iget-object v3, p0, Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x6

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 224
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 225
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 228
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 229
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 231
    return v2

    #@22
    .line 228
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 229
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method

.method public setListener(Lcom/lge/ims/service/cs/IImageSessionListener;)Z
    .registers 9
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 125
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 126
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 129
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.lge.ims.service.cs.IImageSession"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 130
    if-eqz p1, :cond_2f

    #@11
    invoke-interface {p1}, Lcom/lge/ims/service/cs/IImageSessionListener;->asBinder()Landroid/os/IBinder;

    #@14
    move-result-object v4

    #@15
    :goto_15
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@18
    .line 131
    iget-object v4, p0, Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1a
    const/4 v5, 0x1

    #@1b
    const/4 v6, 0x0

    #@1c
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1f
    .line 132
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@22
    .line 133
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_25
    .catchall {:try_start_a .. :try_end_25} :catchall_33

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_31

    #@28
    .line 136
    .local v2, _result:Z
    :goto_28
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2b
    .line 137
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 139
    return v2

    #@2f
    .line 130
    .end local v2           #_result:Z
    :cond_2f
    const/4 v4, 0x0

    #@30
    goto :goto_15

    #@31
    :cond_31
    move v2, v3

    #@32
    .line 133
    goto :goto_28

    #@33
    .line 136
    :catchall_33
    move-exception v3

    #@34
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@37
    .line 137
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3a
    throw v3
.end method

.method public start(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .registers 11
    .parameter "peerPhoneNumber"
    .parameter "filePath"
    .parameter "fileName"
    .parameter "fileSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 162
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 163
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 166
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.cs.IImageSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 167
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@10
    .line 168
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@13
    .line 169
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 170
    invoke-virtual {v0, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 171
    iget-object v3, p0, Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@1b
    const/4 v4, 0x3

    #@1c
    const/4 v5, 0x0

    #@1d
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@20
    .line 172
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@23
    .line 173
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_26
    .catchall {:try_start_8 .. :try_end_26} :catchall_2e

    #@26
    move-result v2

    #@27
    .line 176
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2a
    .line 177
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 179
    return v2

    #@2e
    .line 176
    .end local v2           #_result:I
    :catchall_2e
    move-exception v3

    #@2f
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 177
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    throw v3
.end method

.method public stop()I
    .registers 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 183
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 184
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 187
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v3, "com.lge.ims.service.cs.IImageSession"

    #@a
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 188
    iget-object v3, p0, Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/4 v4, 0x4

    #@10
    const/4 v5, 0x0

    #@11
    invoke-interface {v3, v4, v0, v1, v5}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@14
    .line 189
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@17
    .line 190
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_22

    #@1a
    move-result v2

    #@1b
    .line 193
    .local v2, _result:I
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 194
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 196
    return v2

    #@22
    .line 193
    .end local v2           #_result:I
    :catchall_22
    move-exception v3

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 194
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v3
.end method
