.class public Lcom/lge/ims/service/ip/IPListenerThread;
.super Ljava/lang/Thread;
.source "IPListenerThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;
    }
.end annotation


# static fields
.field private static objIPListenerThread:Lcom/lge/ims/service/ip/IPListenerThread;


# instance fields
.field private objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

.field private objContext:Landroid/content/Context;

.field public objIPCapalistener:Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;

.field private objIPListenerHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 32
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/ip/IPListenerThread;->objIPListenerThread:Lcom/lge/ims/service/ip/IPListenerThread;

    #@3
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "_objContext"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 41
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@4
    .line 34
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread;->objContext:Landroid/content/Context;

    #@6
    .line 35
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@8
    .line 42
    const-string v0, "IPListenerThread"

    #@a
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/ip/IPListenerThread;->setName(Ljava/lang/String;)V

    #@d
    .line 43
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPListenerThread;->objContext:Landroid/content/Context;

    #@f
    .line 44
    invoke-virtual {p0}, Lcom/lge/ims/service/ip/IPListenerThread;->ConnectCapaListener()V

    #@12
    .line 45
    return-void
.end method

.method public static CreateIPListenerThread(Landroid/content/Context;)V
    .registers 2
    .parameter "_objContext"

    #@0
    .prologue
    .line 53
    sget-object v0, Lcom/lge/ims/service/ip/IPListenerThread;->objIPListenerThread:Lcom/lge/ims/service/ip/IPListenerThread;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 54
    new-instance v0, Lcom/lge/ims/service/ip/IPListenerThread;

    #@6
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPListenerThread;-><init>(Landroid/content/Context;)V

    #@9
    sput-object v0, Lcom/lge/ims/service/ip/IPListenerThread;->objIPListenerThread:Lcom/lge/ims/service/ip/IPListenerThread;

    #@b
    .line 56
    :cond_b
    return-void
.end method

.method public static GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;
    .registers 1

    #@0
    .prologue
    .line 59
    sget-object v0, Lcom/lge/ims/service/ip/IPListenerThread;->objIPListenerThread:Lcom/lge/ims/service/ip/IPListenerThread;

    #@2
    return-object v0
.end method

.method static synthetic access$000(Lcom/lge/ims/service/ip/IPListenerThread;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 30
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread;->objIPListenerHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method


# virtual methods
.method public ConnectCapaListener()V
    .registers 2

    #@0
    .prologue
    .line 67
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 68
    new-instance v0, Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;

    #@7
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;-><init>(Lcom/lge/ims/service/ip/IPListenerThread;)V

    #@a
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread;->objIPCapalistener:Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;

    #@c
    .line 69
    return-void
.end method

.method public GetHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread;->objIPListenerHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method public run()V
    .registers 2

    #@0
    .prologue
    .line 76
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@3
    .line 78
    new-instance v0, Lcom/lge/ims/service/ip/IPListenerThread$1;

    #@5
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPListenerThread$1;-><init>(Lcom/lge/ims/service/ip/IPListenerThread;)V

    #@8
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread;->objIPListenerHandler:Landroid/os/Handler;

    #@a
    .line 147
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@d
    .line 148
    return-void
.end method
