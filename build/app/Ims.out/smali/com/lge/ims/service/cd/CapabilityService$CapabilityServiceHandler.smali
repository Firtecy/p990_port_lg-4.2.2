.class final Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;
.super Landroid/os/Handler;
.source "CapabilityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cd/CapabilityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CapabilityServiceHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/cd/CapabilityService;


# direct methods
.method private constructor <init>(Lcom/lge/ims/service/cd/CapabilityService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 447
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/service/cd/CapabilityService;Lcom/lge/ims/service/cd/CapabilityService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 447
    invoke-direct {p0, p1}, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;-><init>(Lcom/lge/ims/service/cd/CapabilityService;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 13
    .parameter "msg"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/4 v9, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 451
    if-nez p1, :cond_6

    #@5
    .line 542
    :cond_5
    :goto_5
    return-void

    #@6
    .line 455
    :cond_6
    const-string v5, "CapService"

    #@8
    new-instance v6, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v7, "CapabilityServiceHandler :: handleMessage - "

    #@f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v6

    #@13
    iget v7, p1, Landroid/os/Message;->what:I

    #@15
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v6

    #@19
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 457
    iget v5, p1, Landroid/os/Message;->what:I

    #@22
    sparse-switch v5, :sswitch_data_10e

    #@25
    .line 539
    const-string v5, "CapService"

    #@27
    new-instance v6, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v7, "Unhandled Message :: "

    #@2e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v6

    #@32
    iget v7, p1, Landroid/os/Message;->what:I

    #@34
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v6

    #@38
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v6

    #@3c
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3f
    goto :goto_5

    #@40
    .line 460
    :sswitch_40
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@42
    check-cast v0, Landroid/os/AsyncResult;

    #@44
    .line 462
    .local v0, ar:Landroid/os/AsyncResult;
    if-eqz v0, :cond_5

    #@46
    .line 463
    iget-object v3, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@48
    check-cast v3, Lcom/lge/ims/service/cd/CapabilityQuery;

    #@4a
    .line 465
    .local v3, cq:Lcom/lge/ims/service/cd/CapabilityQuery;
    if-eqz v3, :cond_5

    #@4c
    .line 466
    invoke-virtual {v3}, Lcom/lge/ims/service/cd/CapabilityQuery;->destroy()V

    #@4f
    goto :goto_5

    #@50
    .line 474
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v3           #cq:Lcom/lge/ims/service/cd/CapabilityQuery;
    :sswitch_50
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@52
    check-cast v0, Landroid/os/AsyncResult;

    #@54
    .line 476
    .restart local v0       #ar:Landroid/os/AsyncResult;
    if-eqz v0, :cond_5

    #@56
    .line 477
    iget-object v2, v0, Landroid/os/AsyncResult;->result:Ljava/lang/Object;

    #@58
    check-cast v2, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;

    #@5a
    .line 479
    .local v2, callInfo:Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;
    if-eqz v2, :cond_5

    #@5c
    .line 480
    const-string v5, "CapService"

    #@5e
    new-instance v6, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v7, "CallInfo :: "

    #@65
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v6

    #@6d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v6

    #@71
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@74
    .line 481
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@76
    invoke-virtual {v2}, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;->getCallType()I

    #@79
    move-result v6

    #@7a
    invoke-virtual {v2}, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;->getNumber()Ljava/lang/String;

    #@7d
    move-result-object v7

    #@7e
    invoke-static {v5, v6, v7}, Lcom/lge/ims/service/cd/CapabilityService;->access$100(Lcom/lge/ims/service/cd/CapabilityService;ILjava/lang/String;)Z

    #@81
    goto :goto_5

    #@82
    .line 489
    .end local v0           #ar:Landroid/os/AsyncResult;
    .end local v2           #callInfo:Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;
    :sswitch_82
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@84
    invoke-static {v5}, Lcom/lge/ims/service/cd/CapabilityService;->access$200(Lcom/lge/ims/service/cd/CapabilityService;)Z

    #@87
    move-result v5

    #@88
    if-nez v5, :cond_5

    #@8a
    .line 490
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@8c
    const/16 v6, 0x1005

    #@8e
    invoke-static {v5, v10, v6, v8}, Lcom/lge/ims/service/cd/CapabilityService;->access$300(Lcom/lge/ims/service/cd/CapabilityService;III)Z

    #@91
    goto/16 :goto_5

    #@93
    .line 498
    :sswitch_93
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@95
    invoke-static {v5}, Lcom/lge/ims/service/cd/CapabilityService;->access$400(Lcom/lge/ims/service/cd/CapabilityService;)Z

    #@98
    move-result v5

    #@99
    if-nez v5, :cond_b8

    #@9b
    .line 500
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@9d
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@9f
    invoke-static {v6}, Lcom/lge/ims/service/cd/CapabilityService;->access$500(Lcom/lge/ims/service/cd/CapabilityService;)Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@a2
    move-result-object v6

    #@a3
    invoke-virtual {v6}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->isExternalStorageAvailable()Z

    #@a6
    move-result v6

    #@a7
    invoke-static {v5, v6}, Lcom/lge/ims/service/cd/CapabilityService;->access$202(Lcom/lge/ims/service/cd/CapabilityService;Z)Z

    #@aa
    .line 501
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@ac
    invoke-static {v5, v9}, Lcom/lge/ims/service/cd/CapabilityService;->access$402(Lcom/lge/ims/service/cd/CapabilityService;Z)Z

    #@af
    .line 503
    const/16 v5, 0x65

    #@b1
    const-wide/16 v6, 0x7d0

    #@b3
    invoke-virtual {p0, v5, v6, v7}, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@b6
    goto/16 :goto_5

    #@b8
    .line 507
    :cond_b8
    const/4 v1, 0x0

    #@b9
    .line 508
    .local v1, blockedServices:I
    const/4 v4, 0x0

    #@ba
    .line 510
    .local v4, unblockedServices:I
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@bc
    invoke-static {v5}, Lcom/lge/ims/service/cd/CapabilityService;->access$200(Lcom/lge/ims/service/cd/CapabilityService;)Z

    #@bf
    move-result v5

    #@c0
    if-nez v5, :cond_f2

    #@c2
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@c4
    invoke-static {v5}, Lcom/lge/ims/service/cd/CapabilityService;->access$500(Lcom/lge/ims/service/cd/CapabilityService;)Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@c7
    move-result-object v5

    #@c8
    invoke-virtual {v5}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->isExternalStorageAvailable()Z

    #@cb
    move-result v5

    #@cc
    if-eqz v5, :cond_f2

    #@ce
    .line 512
    const/16 v4, 0x1005

    #@d0
    .line 514
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@d2
    invoke-static {v5, v9}, Lcom/lge/ims/service/cd/CapabilityService;->access$202(Lcom/lge/ims/service/cd/CapabilityService;Z)Z

    #@d5
    .line 526
    :goto_d5
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@d7
    invoke-static {v5, v10, v1, v4}, Lcom/lge/ims/service/cd/CapabilityService;->access$300(Lcom/lge/ims/service/cd/CapabilityService;III)Z

    #@da
    move-result v5

    #@db
    if-eqz v5, :cond_5

    #@dd
    .line 532
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@df
    invoke-static {v5}, Lcom/lge/ims/service/cd/CapabilityService;->access$600(Lcom/lge/ims/service/cd/CapabilityService;)Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;

    #@e2
    move-result-object v5

    #@e3
    if-eqz v5, :cond_5

    #@e5
    .line 533
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@e7
    invoke-static {v5}, Lcom/lge/ims/service/cd/CapabilityService;->access$600(Lcom/lge/ims/service/cd/CapabilityService;)Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;

    #@ea
    move-result-object v5

    #@eb
    const/16 v6, 0xc8

    #@ed
    invoke-virtual {v5, v6}, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;->sendEmptyMessage(I)Z

    #@f0
    goto/16 :goto_5

    #@f2
    .line 516
    :cond_f2
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@f4
    invoke-static {v5}, Lcom/lge/ims/service/cd/CapabilityService;->access$200(Lcom/lge/ims/service/cd/CapabilityService;)Z

    #@f7
    move-result v5

    #@f8
    if-eqz v5, :cond_5

    #@fa
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@fc
    invoke-static {v5}, Lcom/lge/ims/service/cd/CapabilityService;->access$500(Lcom/lge/ims/service/cd/CapabilityService;)Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@ff
    move-result-object v5

    #@100
    invoke-virtual {v5}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->isExternalStorageAvailable()Z

    #@103
    move-result v5

    #@104
    if-nez v5, :cond_5

    #@106
    .line 518
    const/16 v1, 0x1005

    #@108
    .line 520
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@10a
    invoke-static {v5, v8}, Lcom/lge/ims/service/cd/CapabilityService;->access$202(Lcom/lge/ims/service/cd/CapabilityService;Z)Z

    #@10d
    goto :goto_d5

    #@10e
    .line 457
    :sswitch_data_10e
    .sparse-switch
        0x32 -> :sswitch_40
        0x64 -> :sswitch_50
        0x65 -> :sswitch_82
        0x66 -> :sswitch_93
    .end sparse-switch
.end method
