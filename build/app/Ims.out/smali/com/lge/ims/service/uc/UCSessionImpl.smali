.class public Lcom/lge/ims/service/uc/UCSessionImpl;
.super Lcom/lge/ims/service/uc/IUCSession$Stub;
.source "UCSessionImpl.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "UC"


# instance fields
.field private mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

.field private mCallType:I

.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

.field private mMediaSession:Lcom/lge/ims/service/uc/UCMediaSessionImpl;

.field private mNativeSession:I

.field private mRemoteUserId:Ljava/lang/String;

.field private mTerminated:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 35
    invoke-direct {p0}, Lcom/lge/ims/service/uc/IUCSession$Stub;-><init>()V

    #@5
    .line 22
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mContext:Landroid/content/Context;

    #@7
    .line 23
    iput v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@9
    .line 24
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@b
    .line 25
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@d
    .line 26
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mRemoteUserId:Ljava/lang/String;

    #@f
    .line 27
    iput-boolean v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mTerminated:Z

    #@11
    .line 29
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mMediaSession:Lcom/lge/ims/service/uc/UCMediaSessionImpl;

    #@13
    .line 31
    iput v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallType:I

    #@15
    .line 36
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mContext:Landroid/content/Context;

    #@17
    .line 37
    const/16 v0, 0xc9

    #@19
    invoke-static {v0}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@1c
    move-result v0

    #@1d
    iput v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@1f
    .line 38
    invoke-static {}, Lcom/lge/ims/service/uc/UCCallManager;->getInstance()Lcom/lge/ims/service/uc/UCCallManager;

    #@22
    move-result-object v0

    #@23
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@25
    .line 40
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@27
    if-eqz v0, :cond_30

    #@29
    .line 41
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@2b
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mContext:Landroid/content/Context;

    #@2d
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->setContext(Landroid/content/Context;)V

    #@30
    .line 44
    :cond_30
    iget v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@32
    if-nez v0, :cond_3c

    #@34
    .line 45
    const-string v0, "UC"

    #@36
    const-string v1, "native session is null"

    #@38
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 50
    :goto_3b
    return-void

    #@3c
    .line 49
    :cond_3c
    iget v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@3e
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@41
    goto :goto_3b
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .registers 6
    .parameter "context"
    .parameter "sessionKey"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 83
    invoke-direct {p0}, Lcom/lge/ims/service/uc/IUCSession$Stub;-><init>()V

    #@5
    .line 22
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mContext:Landroid/content/Context;

    #@7
    .line 23
    iput v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@9
    .line 24
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@b
    .line 25
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@d
    .line 26
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mRemoteUserId:Ljava/lang/String;

    #@f
    .line 27
    iput-boolean v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mTerminated:Z

    #@11
    .line 29
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mMediaSession:Lcom/lge/ims/service/uc/UCMediaSessionImpl;

    #@13
    .line 31
    iput v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallType:I

    #@15
    .line 84
    const-string v0, "UC"

    #@17
    new-instance v1, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v2, "UCSessionImpl() :: sessionKey="

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 86
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mContext:Landroid/content/Context;

    #@2f
    .line 87
    iput p2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@31
    .line 88
    invoke-static {}, Lcom/lge/ims/service/uc/UCCallManager;->getInstance()Lcom/lge/ims/service/uc/UCCallManager;

    #@34
    move-result-object v0

    #@35
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@37
    .line 90
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@39
    if-eqz v0, :cond_42

    #@3b
    .line 91
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@3d
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mContext:Landroid/content/Context;

    #@3f
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->setContext(Landroid/content/Context;)V

    #@42
    .line 94
    :cond_42
    iget v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@44
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@47
    .line 95
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .registers 6
    .parameter "context"
    .parameter "callType"
    .parameter "bConf"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 52
    invoke-direct {p0}, Lcom/lge/ims/service/uc/IUCSession$Stub;-><init>()V

    #@5
    .line 22
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mContext:Landroid/content/Context;

    #@7
    .line 23
    iput v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@9
    .line 24
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@b
    .line 25
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@d
    .line 26
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mRemoteUserId:Ljava/lang/String;

    #@f
    .line 27
    iput-boolean v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mTerminated:Z

    #@11
    .line 29
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mMediaSession:Lcom/lge/ims/service/uc/UCMediaSessionImpl;

    #@13
    .line 31
    iput v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallType:I

    #@15
    .line 53
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mContext:Landroid/content/Context;

    #@17
    .line 54
    iput p2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallType:I

    #@19
    .line 56
    packed-switch p2, :pswitch_data_5a

    #@1c
    .line 64
    const/16 v0, 0xc9

    #@1e
    invoke-static {v0}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@21
    move-result v0

    #@22
    iput v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@24
    .line 69
    :goto_24
    invoke-static {}, Lcom/lge/ims/service/uc/UCCallManager;->getInstance()Lcom/lge/ims/service/uc/UCCallManager;

    #@27
    move-result-object v0

    #@28
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@2a
    .line 71
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@2c
    if-eqz v0, :cond_35

    #@2e
    .line 72
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@30
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mContext:Landroid/content/Context;

    #@32
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->setContext(Landroid/content/Context;)V

    #@35
    .line 75
    :cond_35
    iget v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@37
    if-nez v0, :cond_53

    #@39
    .line 76
    const-string v0, "UC"

    #@3b
    const-string v1, "native session is null"

    #@3d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@40
    .line 81
    :goto_40
    return-void

    #@41
    .line 58
    :pswitch_41
    const/16 v0, 0xca

    #@43
    invoke-static {v0}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@46
    move-result v0

    #@47
    iput v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@49
    goto :goto_24

    #@4a
    .line 61
    :pswitch_4a
    const/16 v0, 0xcb

    #@4c
    invoke-static {v0}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@4f
    move-result v0

    #@50
    iput v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@52
    goto :goto_24

    #@53
    .line 80
    :cond_53
    iget v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@55
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@58
    goto :goto_40

    #@59
    .line 56
    nop

    #@5a
    :pswitch_data_5a
    .packed-switch 0x1
        :pswitch_41
        :pswitch_4a
    .end packed-switch
.end method


# virtual methods
.method public accept(I)V
    .registers 7
    .parameter "sessionType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 223
    const-string v2, "UC"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "accept() :: sessionType = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 225
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1b
    move-result-object v1

    #@1c
    .line 227
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_1f

    #@1e
    .line 244
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 231
    :cond_1f
    const/16 v2, 0x4b3

    #@21
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 232
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 234
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@2a
    move-result-object v0

    #@2b
    .line 235
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 236
    const/4 v1, 0x0

    #@2f
    .line 238
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@31
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@34
    .line 240
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@36
    if-eqz v2, :cond_1e

    #@38
    .line 241
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@3a
    invoke-virtual {v2, p0, p1}, Lcom/lge/ims/service/uc/UCCallManager;->updateSession(Lcom/lge/ims/service/uc/UCSessionImpl;I)V

    #@3d
    .line 242
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@3f
    invoke-virtual {v2, p0}, Lcom/lge/ims/service/uc/UCCallManager;->acceptSession(Lcom/lge/ims/service/uc/UCSessionImpl;)V

    #@42
    goto :goto_1e
.end method

.method public acceptUpdate(ILcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessionType"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 452
    const-string v2, "UC"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "acceptUpdateSession() :: sessionType = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 453
    invoke-virtual {p2}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@1b
    .line 455
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1e
    move-result-object v1

    #@1f
    .line 457
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_22

    #@21
    .line 475
    :cond_21
    :goto_21
    return-void

    #@22
    .line 461
    :cond_22
    const/16 v2, 0x4be

    #@24
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 462
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 464
    const/4 v2, 0x1

    #@2b
    invoke-virtual {p2, v1, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@2e
    .line 466
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@31
    move-result-object v0

    #@32
    .line 467
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 468
    const/4 v1, 0x0

    #@36
    .line 470
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@38
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@3b
    .line 472
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@3d
    if-eqz v2, :cond_21

    #@3f
    .line 473
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@41
    invoke-virtual {v2, p0, p1}, Lcom/lge/ims/service/uc/UCCallManager;->updateSession(Lcom/lge/ims/service/uc/UCSessionImpl;I)V

    #@44
    goto :goto_21
.end method

.method public attach()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 508
    const-string v2, "UC"

    #@2
    const-string v3, "attach"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 510
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 512
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 524
    :goto_d
    return-void

    #@e
    .line 516
    :cond_e
    const/16 v2, 0x512

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 518
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@16
    move-result-object v0

    #@17
    .line 519
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 520
    const/4 v1, 0x0

    #@1b
    .line 522
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@1d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@20
    goto :goto_d
.end method

.method public close()V
    .registers 2

    #@0
    .prologue
    .line 98
    iget v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@2
    if-nez v0, :cond_5

    #@4
    .line 107
    :goto_4
    return-void

    #@5
    .line 102
    :cond_5
    iget v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@7
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@a
    .line 103
    iget v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@c
    invoke-static {v0}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@f
    .line 105
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@12
    .line 106
    const/4 v0, 0x0

    #@13
    iput v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@15
    goto :goto_4
.end method

.method public dropConf(Lcom/lge/ims/service/uc/ConfInfo;)V
    .registers 6
    .parameter "confInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 409
    const-string v2, "UC"

    #@2
    const-string v3, "dropConf()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 410
    invoke-virtual {p1}, Lcom/lge/ims/service/uc/ConfInfo;->logIn()V

    #@a
    .line 412
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@d
    move-result-object v1

    #@e
    .line 414
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_11

    #@10
    .line 427
    :goto_10
    return-void

    #@11
    .line 418
    :cond_11
    const/16 v2, 0x4bc

    #@13
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 420
    const/4 v2, 0x1

    #@17
    invoke-virtual {p1, v1, v2}, Lcom/lge/ims/service/uc/ConfInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 422
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1d
    move-result-object v0

    #@1e
    .line 423
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 424
    const/4 v1, 0x0

    #@22
    .line 426
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@24
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@27
    goto :goto_10
.end method

.method public expandToConf(Lcom/lge/ims/service/uc/ConfInfo;)V
    .registers 7
    .parameter "confInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 349
    const-string v2, "UC"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "expandToConf() : "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget-object v4, p1, Lcom/lge/ims/service/uc/ConfInfo;->participants:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v4

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 350
    invoke-virtual {p1}, Lcom/lge/ims/service/uc/ConfInfo;->logIn()V

    #@21
    .line 352
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@24
    move-result-object v1

    #@25
    .line 354
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_28

    #@27
    .line 367
    :goto_27
    return-void

    #@28
    .line 358
    :cond_28
    const/16 v2, 0x4b9

    #@2a
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2d
    .line 360
    const/4 v2, 0x1

    #@2e
    invoke-virtual {p1, v1, v2}, Lcom/lge/ims/service/uc/ConfInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@31
    .line 362
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@34
    move-result-object v0

    #@35
    .line 363
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@38
    .line 364
    const/4 v1, 0x0

    #@39
    .line 366
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@3b
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@3e
    goto :goto_27
.end method

.method public getMediaSession(Lcom/lge/ims/service/uc/IUCMediaSessionListener;)Lcom/lge/ims/service/uc/IUCMediaSession;
    .registers 4
    .parameter "listener"

    #@0
    .prologue
    .line 533
    new-instance v0, Lcom/lge/ims/service/uc/UCMediaSessionImpl;

    #@2
    iget v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@4
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/UCMediaSessionImpl;-><init>(I)V

    #@7
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mMediaSession:Lcom/lge/ims/service/uc/UCMediaSessionImpl;

    #@9
    .line 534
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mMediaSession:Lcom/lge/ims/service/uc/UCMediaSessionImpl;

    #@b
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->setListenerThread(Lcom/lge/ims/service/uc/IUCMediaSessionListener;)V

    #@e
    .line 536
    const-string v0, "UC"

    #@10
    const-string v1, "getMediaSession() success."

    #@12
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 538
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mMediaSession:Lcom/lge/ims/service/uc/UCMediaSessionImpl;

    #@17
    return-object v0
.end method

.method public getNativeObject()I
    .registers 2

    #@0
    .prologue
    .line 110
    iget v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@2
    return v0
.end method

.method public getProperty(I)I
    .registers 5
    .parameter "item"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 527
    const-string v0, "UC"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getProperty() :: item["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "]"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 529
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@20
    invoke-virtual {v0, p0, p1}, Lcom/lge/ims/service/uc/UCCallManager;->getProperty(Lcom/lge/ims/service/uc/UCSessionImpl;I)I

    #@23
    move-result v0

    #@24
    return v0
.end method

.method public hold(Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 6
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 266
    const-string v2, "UC"

    #@2
    const-string v3, "hold()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 267
    invoke-virtual {p1}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@a
    .line 269
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@d
    move-result-object v1

    #@e
    .line 271
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_11

    #@10
    .line 284
    :goto_10
    return-void

    #@11
    .line 275
    :cond_11
    const/16 v2, 0x4b5

    #@13
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 277
    const/4 v2, 0x1

    #@17
    invoke-virtual {p1, v1, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 279
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1d
    move-result-object v0

    #@1e
    .line 280
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 281
    const/4 v1, 0x0

    #@22
    .line 283
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@24
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@27
    goto :goto_10
.end method

.method public isTerminated()Z
    .registers 2

    #@0
    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mTerminated:Z

    #@2
    return v0
.end method

.method public joinConf(Lcom/lge/ims/service/uc/ConfInfo;)V
    .registers 6
    .parameter "confInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 388
    const-string v2, "UC"

    #@2
    const-string v3, "joinConf()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 389
    invoke-virtual {p1}, Lcom/lge/ims/service/uc/ConfInfo;->logIn()V

    #@a
    .line 391
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@d
    move-result-object v1

    #@e
    .line 393
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_11

    #@10
    .line 406
    :goto_10
    return-void

    #@11
    .line 397
    :cond_11
    const/16 v2, 0x4bb

    #@13
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 399
    const/4 v2, 0x1

    #@17
    invoke-virtual {p1, v1, v2}, Lcom/lge/ims/service/uc/ConfInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 401
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1d
    move-result-object v0

    #@1e
    .line 402
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 403
    const/4 v1, 0x0

    #@22
    .line 405
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@24
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@27
    goto :goto_10
.end method

.method public merge()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 370
    const-string v2, "UC"

    #@2
    const-string v3, "merge()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 372
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 374
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 385
    :goto_d
    return-void

    #@e
    .line 378
    :cond_e
    const/16 v2, 0x4ba

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 380
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@16
    move-result-object v0

    #@17
    .line 381
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 382
    const/4 v1, 0x0

    #@1b
    .line 384
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@1d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@20
    goto :goto_d
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 30
    .parameter "parcel"

    #@0
    .prologue
    .line 542
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v17

    #@4
    .line 544
    .local v17, msg:I
    const-string v25, "UC"

    #@6
    new-instance v26, Ljava/lang/StringBuilder;

    #@8
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v27, "onMessage() :: msg = "

    #@d
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v26

    #@11
    move-object/from16 v0, v26

    #@13
    move/from16 v1, v17

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v26

    #@19
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v26

    #@1d
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 546
    const/16 v25, 0x516

    #@22
    move/from16 v0, v17

    #@24
    move/from16 v1, v25

    #@26
    if-eq v0, v1, :cond_30

    #@28
    const/16 v25, 0x51e

    #@2a
    move/from16 v0, v17

    #@2c
    move/from16 v1, v25

    #@2e
    if-ne v0, v1, :cond_3f

    #@30
    .line 547
    :cond_30
    const-string v25, "UC"

    #@32
    const-string v26, "session is terminated"

    #@34
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 548
    const/16 v25, 0x1

    #@39
    move/from16 v0, v25

    #@3b
    move-object/from16 v1, p0

    #@3d
    iput-boolean v0, v1, Lcom/lge/ims/service/uc/UCSessionImpl;->mTerminated:Z

    #@3f
    .line 552
    :cond_3f
    :try_start_3f
    move-object/from16 v0, p0

    #@41
    iget v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallType:I

    #@43
    move/from16 v25, v0

    #@45
    const/16 v26, 0x1

    #@47
    move/from16 v0, v25

    #@49
    move/from16 v1, v26

    #@4b
    if-ne v0, v1, :cond_7f

    #@4d
    move-object/from16 v0, p0

    #@4f
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@51
    move-object/from16 v25, v0

    #@53
    if-eqz v25, :cond_7f

    #@55
    .line 553
    const/16 v19, 0x0

    #@57
    .line 554
    .local v19, reason:I
    const/16 v25, 0x51e

    #@59
    move/from16 v0, v17

    #@5b
    move/from16 v1, v25

    #@5d
    if-ne v0, v1, :cond_70

    #@5f
    .line 555
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@62
    move-result v19

    #@63
    .line 556
    const/16 v25, 0x0

    #@65
    move-object/from16 v0, p1

    #@67
    move/from16 v1, v25

    #@69
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@6c
    .line 557
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@6f
    move-result v17

    #@70
    .line 559
    :cond_70
    move-object/from16 v0, p0

    #@72
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@74
    move-object/from16 v25, v0

    #@76
    move-object/from16 v0, v25

    #@78
    move/from16 v1, v17

    #@7a
    move/from16 v2, v19

    #@7c
    invoke-virtual {v0, v1, v2}, Lcom/lge/ims/service/uc/UCCallManager;->notifyECallState(II)V
    :try_end_7f
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_7f} :catch_aa

    #@7f
    .line 565
    .end local v19           #reason:I
    :cond_7f
    :goto_7f
    move-object/from16 v0, p0

    #@81
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@83
    move-object/from16 v25, v0

    #@85
    if-nez v25, :cond_c6

    #@87
    .line 566
    const-string v25, "UC"

    #@89
    const-string v26, "listener is null"

    #@8b
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@8e
    .line 568
    invoke-virtual/range {p0 .. p0}, Lcom/lge/ims/service/uc/UCSessionImpl;->isTerminated()Z

    #@91
    move-result v25

    #@92
    if-eqz v25, :cond_a9

    #@94
    move-object/from16 v0, p0

    #@96
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@98
    move-object/from16 v25, v0

    #@9a
    if-eqz v25, :cond_a9

    #@9c
    .line 569
    move-object/from16 v0, p0

    #@9e
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@a0
    move-object/from16 v25, v0

    #@a2
    move-object/from16 v0, v25

    #@a4
    move-object/from16 v1, p0

    #@a6
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->updateTerminatedSession(Lcom/lge/ims/service/uc/UCSessionImpl;)V

    #@a9
    .line 946
    :cond_a9
    :goto_a9
    return-void

    #@aa
    .line 561
    :catch_aa
    move-exception v10

    #@ab
    .line 562
    .local v10, e:Ljava/lang/Exception;
    const-string v25, "UC"

    #@ad
    new-instance v26, Ljava/lang/StringBuilder;

    #@af
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v27, "Exception ::"

    #@b4
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v26

    #@b8
    move-object/from16 v0, v26

    #@ba
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v26

    #@be
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v26

    #@c2
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@c5
    goto :goto_7f

    #@c6
    .line 574
    .end local v10           #e:Ljava/lang/Exception;
    :cond_c6
    move-object/from16 v0, p0

    #@c8
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@ca
    move-object/from16 v25, v0

    #@cc
    if-nez v25, :cond_d6

    #@ce
    .line 575
    const-string v25, "UC"

    #@d0
    const-string v26, "UCCallMngr is null"

    #@d2
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d5
    goto :goto_a9

    #@d6
    .line 580
    :cond_d6
    packed-switch v17, :pswitch_data_934

    #@d9
    .line 923
    :try_start_d9
    invoke-virtual/range {p0 .. p0}, Lcom/lge/ims/service/uc/UCSessionImpl;->isTerminated()Z

    #@dc
    move-result v25

    #@dd
    if-eqz v25, :cond_90c

    #@df
    const/16 v25, 0x590

    #@e1
    move/from16 v0, v17

    #@e3
    move/from16 v1, v25

    #@e5
    if-ne v0, v1, :cond_90c

    #@e7
    .line 924
    const-string v25, "UC"

    #@e9
    const-string v26, "AUDIO_STARTED_IND ignored : session is terminated... "

    #@eb
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_ee
    .catch Landroid/os/RemoteException; {:try_start_d9 .. :try_end_ee} :catch_1bf
    .catch Ljava/lang/Exception; {:try_start_d9 .. :try_end_ee} :catch_220

    #@ee
    .line 945
    :cond_ee
    :goto_ee
    const-string v25, "UC"

    #@f0
    new-instance v26, Ljava/lang/StringBuilder;

    #@f2
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@f5
    const-string v27, "onMessage() :: msg = "

    #@f7
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v26

    #@fb
    move-object/from16 v0, v26

    #@fd
    move/from16 v1, v17

    #@ff
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@102
    move-result-object v26

    #@103
    const-string v27, " , MESSAGE_HANDELED"

    #@105
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v26

    #@109
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v26

    #@10d
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@110
    goto :goto_a9

    #@111
    .line 583
    :pswitch_111
    :try_start_111
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@114
    move-result v5

    #@115
    .line 584
    .local v5, VMS:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@118
    move-result v6

    #@119
    .line 586
    .local v6, bEnableConf:I
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@11b
    move-object/from16 v0, v22

    #@11d
    move-object/from16 v1, p1

    #@11f
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@122
    .line 587
    .local v22, sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@124
    move-object/from16 v0, v16

    #@126
    move-object/from16 v1, p1

    #@128
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@12b
    .line 589
    .local v16, mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@12d
    new-instance v26, Ljava/lang/StringBuilder;

    #@12f
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@132
    const-string v27, "STARTED :: VMS = "

    #@134
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v26

    #@138
    move-object/from16 v0, v26

    #@13a
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v26

    #@13e
    const-string v27, " , EnableConf = "

    #@140
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v26

    #@144
    move-object/from16 v0, v26

    #@146
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@149
    move-result-object v26

    #@14a
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14d
    move-result-object v26

    #@14e
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@151
    .line 590
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@154
    .line 591
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@157
    .line 593
    move-object/from16 v0, p0

    #@159
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@15b
    move-object/from16 v25, v0

    #@15d
    const/16 v26, 0x1

    #@15f
    move-object/from16 v0, v25

    #@161
    move-object/from16 v1, p0

    #@163
    move/from16 v2, v26

    #@165
    invoke-virtual {v0, v1, v2, v5}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@168
    .line 594
    move-object/from16 v0, p0

    #@16a
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@16c
    move-object/from16 v25, v0

    #@16e
    const/16 v26, 0x32

    #@170
    move-object/from16 v0, v25

    #@172
    move-object/from16 v1, p0

    #@174
    move/from16 v2, v26

    #@176
    invoke-virtual {v0, v1, v2, v6}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@179
    .line 596
    move-object/from16 v0, v22

    #@17b
    iget v0, v0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@17d
    move/from16 v25, v0

    #@17f
    const/16 v26, 0x3

    #@181
    move/from16 v0, v25

    #@183
    move/from16 v1, v26

    #@185
    if-ne v0, v1, :cond_1df

    #@187
    const/4 v7, 0x1

    #@188
    .line 597
    .local v7, callModeChangeable:I
    :goto_188
    move-object/from16 v0, p0

    #@18a
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@18c
    move-object/from16 v25, v0

    #@18e
    const/16 v26, 0x33

    #@190
    move-object/from16 v0, v25

    #@192
    move-object/from16 v1, p0

    #@194
    move/from16 v2, v26

    #@196
    invoke-virtual {v0, v1, v2, v7}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@199
    .line 599
    move-object/from16 v0, p0

    #@19b
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@19d
    move-object/from16 v25, v0

    #@19f
    move-object/from16 v0, v25

    #@1a1
    move-object/from16 v1, v22

    #@1a3
    move-object/from16 v2, v16

    #@1a5
    invoke-interface {v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener;->started(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@1a8
    .line 600
    move-object/from16 v0, p0

    #@1aa
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@1ac
    move-object/from16 v25, v0

    #@1ae
    move-object/from16 v0, v22

    #@1b0
    iget v0, v0, Lcom/lge/ims/service/uc/SessInfo;->SessionType:I

    #@1b2
    move/from16 v26, v0

    #@1b4
    move-object/from16 v0, v25

    #@1b6
    move-object/from16 v1, p0

    #@1b8
    move/from16 v2, v26

    #@1ba
    invoke-virtual {v0, v1, v2}, Lcom/lge/ims/service/uc/UCCallManager;->updateSession(Lcom/lge/ims/service/uc/UCSessionImpl;I)V
    :try_end_1bd
    .catch Landroid/os/RemoteException; {:try_start_111 .. :try_end_1bd} :catch_1bf
    .catch Ljava/lang/Exception; {:try_start_111 .. :try_end_1bd} :catch_220

    #@1bd
    goto/16 :goto_ee

    #@1bf
    .line 937
    .end local v5           #VMS:I
    .end local v6           #bEnableConf:I
    .end local v7           #callModeChangeable:I
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    :catch_1bf
    move-exception v10

    #@1c0
    .line 938
    .local v10, e:Landroid/os/RemoteException;
    const-string v25, "UC"

    #@1c2
    new-instance v26, Ljava/lang/StringBuilder;

    #@1c4
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@1c7
    const-string v27, "RemoteException ::"

    #@1c9
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1cc
    move-result-object v26

    #@1cd
    move-object/from16 v0, v26

    #@1cf
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v26

    #@1d3
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d6
    move-result-object v26

    #@1d7
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1da
    .line 939
    invoke-virtual {v10}, Landroid/os/RemoteException;->printStackTrace()V

    #@1dd
    goto/16 :goto_ee

    #@1df
    .line 596
    .end local v10           #e:Landroid/os/RemoteException;
    .restart local v5       #VMS:I
    .restart local v6       #bEnableConf:I
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    :cond_1df
    const/4 v7, 0x0

    #@1e0
    goto :goto_188

    #@1e1
    .line 605
    .end local v5           #VMS:I
    .end local v6           #bEnableConf:I
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    :pswitch_1e1
    :try_start_1e1
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1e4
    move-result v19

    #@1e5
    .line 606
    .restart local v19       #reason:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1e8
    move-result v8

    #@1e9
    .line 607
    .local v8, code:I
    const-string v25, "UC"

    #@1eb
    new-instance v26, Ljava/lang/StringBuilder;

    #@1ed
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@1f0
    const-string v27, "START_FAILED :: reason = "

    #@1f2
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v26

    #@1f6
    move-object/from16 v0, v26

    #@1f8
    move/from16 v1, v19

    #@1fa
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1fd
    move-result-object v26

    #@1fe
    const-string v27, " , code = "

    #@200
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@203
    move-result-object v26

    #@204
    move-object/from16 v0, v26

    #@206
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@209
    move-result-object v26

    #@20a
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20d
    move-result-object v26

    #@20e
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@211
    .line 609
    move-object/from16 v0, p0

    #@213
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@215
    move-object/from16 v25, v0

    #@217
    move-object/from16 v0, v25

    #@219
    move/from16 v1, v19

    #@21b
    invoke-interface {v0, v1, v8}, Lcom/lge/ims/service/uc/IUCSessionListener;->startFailed(II)V
    :try_end_21e
    .catch Landroid/os/RemoteException; {:try_start_1e1 .. :try_end_21e} :catch_1bf
    .catch Ljava/lang/Exception; {:try_start_1e1 .. :try_end_21e} :catch_220

    #@21e
    goto/16 :goto_ee

    #@220
    .line 940
    .end local v8           #code:I
    .end local v19           #reason:I
    :catch_220
    move-exception v10

    #@221
    .line 941
    .local v10, e:Ljava/lang/Exception;
    const-string v25, "UC"

    #@223
    new-instance v26, Ljava/lang/StringBuilder;

    #@225
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@228
    const-string v27, "Exception ::"

    #@22a
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v26

    #@22e
    move-object/from16 v0, v26

    #@230
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@233
    move-result-object v26

    #@234
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@237
    move-result-object v26

    #@238
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@23b
    .line 942
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    #@23e
    goto/16 :goto_ee

    #@240
    .line 614
    .end local v10           #e:Ljava/lang/Exception;
    :pswitch_240
    :try_start_240
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@243
    move-result v5

    #@244
    .line 616
    .restart local v5       #VMS:I
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@246
    move-object/from16 v0, v16

    #@248
    move-object/from16 v1, p1

    #@24a
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@24d
    .line 618
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@24f
    new-instance v26, Ljava/lang/StringBuilder;

    #@251
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@254
    const-string v27, "PROGRESSING :: , VMS = "

    #@256
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@259
    move-result-object v26

    #@25a
    move-object/from16 v0, v26

    #@25c
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25f
    move-result-object v26

    #@260
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@263
    move-result-object v26

    #@264
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@267
    .line 619
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@26a
    .line 621
    move-object/from16 v0, p0

    #@26c
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@26e
    move-object/from16 v25, v0

    #@270
    const/16 v26, 0x1

    #@272
    move-object/from16 v0, v25

    #@274
    move-object/from16 v1, p0

    #@276
    move/from16 v2, v26

    #@278
    invoke-virtual {v0, v1, v2, v5}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@27b
    .line 623
    move-object/from16 v0, p0

    #@27d
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@27f
    move-object/from16 v25, v0

    #@281
    move-object/from16 v0, v25

    #@283
    move-object/from16 v1, v16

    #@285
    invoke-interface {v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener;->progressing(Lcom/lge/ims/service/uc/MediaInfo;)V

    #@288
    goto/16 :goto_ee

    #@28a
    .line 629
    .end local v5           #VMS:I
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    :pswitch_28a
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@28c
    move-object/from16 v0, v22

    #@28e
    move-object/from16 v1, p1

    #@290
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@293
    .line 630
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@295
    move-object/from16 v0, v16

    #@297
    move-object/from16 v1, p1

    #@299
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@29c
    .line 632
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@29e
    const-string v26, "HELD ::"

    #@2a0
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2a3
    .line 633
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@2a6
    .line 634
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@2a9
    .line 637
    move-object/from16 v0, v22

    #@2ab
    iget v0, v0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@2ad
    move/from16 v25, v0

    #@2af
    const/16 v26, 0x3

    #@2b1
    move/from16 v0, v25

    #@2b3
    move/from16 v1, v26

    #@2b5
    if-ne v0, v1, :cond_2da

    #@2b7
    const/4 v7, 0x1

    #@2b8
    .line 638
    .restart local v7       #callModeChangeable:I
    :goto_2b8
    move-object/from16 v0, p0

    #@2ba
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@2bc
    move-object/from16 v25, v0

    #@2be
    const/16 v26, 0x33

    #@2c0
    move-object/from16 v0, v25

    #@2c2
    move-object/from16 v1, p0

    #@2c4
    move/from16 v2, v26

    #@2c6
    invoke-virtual {v0, v1, v2, v7}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@2c9
    .line 640
    move-object/from16 v0, p0

    #@2cb
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@2cd
    move-object/from16 v25, v0

    #@2cf
    move-object/from16 v0, v25

    #@2d1
    move-object/from16 v1, v22

    #@2d3
    move-object/from16 v2, v16

    #@2d5
    invoke-interface {v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener;->held(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@2d8
    goto/16 :goto_ee

    #@2da
    .line 637
    .end local v7           #callModeChangeable:I
    :cond_2da
    const/4 v7, 0x0

    #@2db
    goto :goto_2b8

    #@2dc
    .line 645
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    :pswitch_2dc
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@2df
    move-result v19

    #@2e0
    .line 646
    .restart local v19       #reason:I
    const-string v25, "UC"

    #@2e2
    new-instance v26, Ljava/lang/StringBuilder;

    #@2e4
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@2e7
    const-string v27, "HOLD_FAILED :: reason = "

    #@2e9
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ec
    move-result-object v26

    #@2ed
    move-object/from16 v0, v26

    #@2ef
    move/from16 v1, v19

    #@2f1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f4
    move-result-object v26

    #@2f5
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f8
    move-result-object v26

    #@2f9
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2fc
    .line 647
    move-object/from16 v0, p0

    #@2fe
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@300
    move-object/from16 v25, v0

    #@302
    move-object/from16 v0, v25

    #@304
    move/from16 v1, v19

    #@306
    invoke-interface {v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener;->holdFailed(I)V

    #@309
    goto/16 :goto_ee

    #@30b
    .line 653
    .end local v19           #reason:I
    :pswitch_30b
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@30d
    move-object/from16 v0, v22

    #@30f
    move-object/from16 v1, p1

    #@311
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@314
    .line 654
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@316
    move-object/from16 v0, v16

    #@318
    move-object/from16 v1, p1

    #@31a
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@31d
    .line 656
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@31f
    const-string v26, "HELD_BY ::"

    #@321
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@324
    .line 657
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@327
    .line 658
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@32a
    .line 661
    move-object/from16 v0, v22

    #@32c
    iget v0, v0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@32e
    move/from16 v25, v0

    #@330
    const/16 v26, 0x3

    #@332
    move/from16 v0, v25

    #@334
    move/from16 v1, v26

    #@336
    if-ne v0, v1, :cond_35b

    #@338
    const/4 v7, 0x1

    #@339
    .line 662
    .restart local v7       #callModeChangeable:I
    :goto_339
    move-object/from16 v0, p0

    #@33b
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@33d
    move-object/from16 v25, v0

    #@33f
    const/16 v26, 0x33

    #@341
    move-object/from16 v0, v25

    #@343
    move-object/from16 v1, p0

    #@345
    move/from16 v2, v26

    #@347
    invoke-virtual {v0, v1, v2, v7}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@34a
    .line 664
    move-object/from16 v0, p0

    #@34c
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@34e
    move-object/from16 v25, v0

    #@350
    move-object/from16 v0, v25

    #@352
    move-object/from16 v1, v22

    #@354
    move-object/from16 v2, v16

    #@356
    invoke-interface {v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener;->heldBy(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@359
    goto/16 :goto_ee

    #@35b
    .line 661
    .end local v7           #callModeChangeable:I
    :cond_35b
    const/4 v7, 0x0

    #@35c
    goto :goto_339

    #@35d
    .line 670
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    :pswitch_35d
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@35f
    move-object/from16 v0, v22

    #@361
    move-object/from16 v1, p1

    #@363
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@366
    .line 671
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@368
    move-object/from16 v0, v16

    #@36a
    move-object/from16 v1, p1

    #@36c
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@36f
    .line 673
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@371
    const-string v26, "RESUMED"

    #@373
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@376
    .line 674
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@379
    .line 675
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@37c
    .line 678
    move-object/from16 v0, v22

    #@37e
    iget v0, v0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@380
    move/from16 v25, v0

    #@382
    const/16 v26, 0x3

    #@384
    move/from16 v0, v25

    #@386
    move/from16 v1, v26

    #@388
    if-ne v0, v1, :cond_3ad

    #@38a
    const/4 v7, 0x1

    #@38b
    .line 679
    .restart local v7       #callModeChangeable:I
    :goto_38b
    move-object/from16 v0, p0

    #@38d
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@38f
    move-object/from16 v25, v0

    #@391
    const/16 v26, 0x33

    #@393
    move-object/from16 v0, v25

    #@395
    move-object/from16 v1, p0

    #@397
    move/from16 v2, v26

    #@399
    invoke-virtual {v0, v1, v2, v7}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@39c
    .line 681
    move-object/from16 v0, p0

    #@39e
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@3a0
    move-object/from16 v25, v0

    #@3a2
    move-object/from16 v0, v25

    #@3a4
    move-object/from16 v1, v22

    #@3a6
    move-object/from16 v2, v16

    #@3a8
    invoke-interface {v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener;->resumed(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@3ab
    goto/16 :goto_ee

    #@3ad
    .line 678
    .end local v7           #callModeChangeable:I
    :cond_3ad
    const/4 v7, 0x0

    #@3ae
    goto :goto_38b

    #@3af
    .line 686
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    :pswitch_3af
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3b2
    move-result v19

    #@3b3
    .line 687
    .restart local v19       #reason:I
    const-string v25, "UC"

    #@3b5
    new-instance v26, Ljava/lang/StringBuilder;

    #@3b7
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@3ba
    const-string v27, "RESUME_FAILED :: reason = "

    #@3bc
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3bf
    move-result-object v26

    #@3c0
    move-object/from16 v0, v26

    #@3c2
    move/from16 v1, v19

    #@3c4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c7
    move-result-object v26

    #@3c8
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3cb
    move-result-object v26

    #@3cc
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@3cf
    .line 688
    move-object/from16 v0, p0

    #@3d1
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@3d3
    move-object/from16 v25, v0

    #@3d5
    move-object/from16 v0, v25

    #@3d7
    move/from16 v1, v19

    #@3d9
    invoke-interface {v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener;->resumeFailed(I)V

    #@3dc
    goto/16 :goto_ee

    #@3de
    .line 694
    .end local v19           #reason:I
    :pswitch_3de
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@3e0
    move-object/from16 v0, v22

    #@3e2
    move-object/from16 v1, p1

    #@3e4
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@3e7
    .line 695
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@3e9
    move-object/from16 v0, v16

    #@3eb
    move-object/from16 v1, p1

    #@3ed
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@3f0
    .line 697
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@3f2
    const-string v26, "RESUMED_BY"

    #@3f4
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@3f7
    .line 698
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@3fa
    .line 699
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@3fd
    .line 702
    move-object/from16 v0, v22

    #@3ff
    iget v0, v0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@401
    move/from16 v25, v0

    #@403
    const/16 v26, 0x3

    #@405
    move/from16 v0, v25

    #@407
    move/from16 v1, v26

    #@409
    if-ne v0, v1, :cond_42e

    #@40b
    const/4 v7, 0x1

    #@40c
    .line 703
    .restart local v7       #callModeChangeable:I
    :goto_40c
    move-object/from16 v0, p0

    #@40e
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@410
    move-object/from16 v25, v0

    #@412
    const/16 v26, 0x33

    #@414
    move-object/from16 v0, v25

    #@416
    move-object/from16 v1, p0

    #@418
    move/from16 v2, v26

    #@41a
    invoke-virtual {v0, v1, v2, v7}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@41d
    .line 705
    move-object/from16 v0, p0

    #@41f
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@421
    move-object/from16 v25, v0

    #@423
    move-object/from16 v0, v25

    #@425
    move-object/from16 v1, v22

    #@427
    move-object/from16 v2, v16

    #@429
    invoke-interface {v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener;->resumedBy(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@42c
    goto/16 :goto_ee

    #@42e
    .line 702
    .end local v7           #callModeChangeable:I
    :cond_42e
    const/4 v7, 0x0

    #@42f
    goto :goto_40c

    #@430
    .line 710
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    :pswitch_430
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@433
    move-result v19

    #@434
    .line 711
    .restart local v19       #reason:I
    const-string v25, "UC"

    #@436
    new-instance v26, Ljava/lang/StringBuilder;

    #@438
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@43b
    const-string v27, "TERMINATED :: reason = "

    #@43d
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@440
    move-result-object v26

    #@441
    move-object/from16 v0, v26

    #@443
    move/from16 v1, v19

    #@445
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@448
    move-result-object v26

    #@449
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44c
    move-result-object v26

    #@44d
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@450
    .line 712
    move-object/from16 v0, p0

    #@452
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@454
    move-object/from16 v25, v0

    #@456
    move-object/from16 v0, v25

    #@458
    move/from16 v1, v19

    #@45a
    invoke-interface {v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener;->terminated(I)V

    #@45d
    goto/16 :goto_ee

    #@45f
    .line 718
    .end local v19           #reason:I
    :pswitch_45f
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@462
    move-result v23

    #@463
    .line 720
    .local v23, sessionKey:I
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@465
    move-object/from16 v0, v22

    #@467
    move-object/from16 v1, p1

    #@469
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@46c
    .line 721
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@46e
    move-object/from16 v0, v16

    #@470
    move-object/from16 v1, p1

    #@472
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@475
    .line 723
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@477
    new-instance v26, Ljava/lang/StringBuilder;

    #@479
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@47c
    const-string v27, "EXPANDED_CONF :: sessionKey="

    #@47e
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@481
    move-result-object v26

    #@482
    move-object/from16 v0, v26

    #@484
    move/from16 v1, v23

    #@486
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@489
    move-result-object v26

    #@48a
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48d
    move-result-object v26

    #@48e
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@491
    .line 724
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@494
    .line 725
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@497
    .line 728
    move-object/from16 v0, p0

    #@499
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@49b
    move-object/from16 v25, v0

    #@49d
    const/16 v26, 0x33

    #@49f
    const/16 v27, 0x0

    #@4a1
    move-object/from16 v0, v25

    #@4a3
    move-object/from16 v1, p0

    #@4a5
    move/from16 v2, v26

    #@4a7
    move/from16 v3, v27

    #@4a9
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@4ac
    .line 730
    move-object/from16 v0, p0

    #@4ae
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@4b0
    move-object/from16 v25, v0

    #@4b2
    move-object/from16 v0, v25

    #@4b4
    move/from16 v1, v23

    #@4b6
    move-object/from16 v2, v22

    #@4b8
    move-object/from16 v3, v16

    #@4ba
    invoke-interface {v0, v1, v2, v3}, Lcom/lge/ims/service/uc/IUCSessionListener;->expandedConf(ILcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@4bd
    goto/16 :goto_ee

    #@4bf
    .line 735
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    .end local v23           #sessionKey:I
    :pswitch_4bf
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@4c2
    move-result v19

    #@4c3
    .line 736
    .restart local v19       #reason:I
    const-string v25, "UC"

    #@4c5
    new-instance v26, Ljava/lang/StringBuilder;

    #@4c7
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@4ca
    const-string v27, "EXPAND_CONF_FAILED :: reason = "

    #@4cc
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4cf
    move-result-object v26

    #@4d0
    move-object/from16 v0, v26

    #@4d2
    move/from16 v1, v19

    #@4d4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d7
    move-result-object v26

    #@4d8
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4db
    move-result-object v26

    #@4dc
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@4df
    .line 737
    move-object/from16 v0, p0

    #@4e1
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@4e3
    move-object/from16 v25, v0

    #@4e5
    move-object/from16 v0, v25

    #@4e7
    move/from16 v1, v19

    #@4e9
    invoke-interface {v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener;->expandConfFailed(I)V

    #@4ec
    goto/16 :goto_ee

    #@4ee
    .line 743
    .end local v19           #reason:I
    :pswitch_4ee
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@4f1
    move-result v23

    #@4f2
    .line 745
    .restart local v23       #sessionKey:I
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@4f4
    move-object/from16 v0, v22

    #@4f6
    move-object/from16 v1, p1

    #@4f8
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@4fb
    .line 746
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@4fd
    move-object/from16 v0, v16

    #@4ff
    move-object/from16 v1, p1

    #@501
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@504
    .line 748
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@506
    new-instance v26, Ljava/lang/StringBuilder;

    #@508
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@50b
    const-string v27, "EXPANDED_CONF_BY :: sessionKey="

    #@50d
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@510
    move-result-object v26

    #@511
    move-object/from16 v0, v26

    #@513
    move/from16 v1, v23

    #@515
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@518
    move-result-object v26

    #@519
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51c
    move-result-object v26

    #@51d
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@520
    .line 749
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@523
    .line 750
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@526
    .line 753
    move-object/from16 v0, p0

    #@528
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@52a
    move-object/from16 v25, v0

    #@52c
    const/16 v26, 0x33

    #@52e
    const/16 v27, 0x0

    #@530
    move-object/from16 v0, v25

    #@532
    move-object/from16 v1, p0

    #@534
    move/from16 v2, v26

    #@536
    move/from16 v3, v27

    #@538
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@53b
    .line 755
    move-object/from16 v0, p0

    #@53d
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@53f
    move-object/from16 v25, v0

    #@541
    move-object/from16 v0, v25

    #@543
    move/from16 v1, v23

    #@545
    move-object/from16 v2, v22

    #@547
    move-object/from16 v3, v16

    #@549
    invoke-interface {v0, v1, v2, v3}, Lcom/lge/ims/service/uc/IUCSessionListener;->expandedConfBy(ILcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@54c
    goto/16 :goto_ee

    #@54e
    .line 760
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    .end local v23           #sessionKey:I
    :pswitch_54e
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@551
    move-result v23

    #@552
    .line 762
    .restart local v23       #sessionKey:I
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@554
    move-object/from16 v0, v22

    #@556
    move-object/from16 v1, p1

    #@558
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@55b
    .line 763
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@55d
    move-object/from16 v0, v16

    #@55f
    move-object/from16 v1, p1

    #@561
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@564
    .line 765
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@566
    new-instance v26, Ljava/lang/StringBuilder;

    #@568
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@56b
    const-string v27, "MERGED :: sessionKey="

    #@56d
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@570
    move-result-object v26

    #@571
    move-object/from16 v0, v26

    #@573
    move/from16 v1, v23

    #@575
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@578
    move-result-object v26

    #@579
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57c
    move-result-object v26

    #@57d
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@580
    .line 766
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@583
    .line 767
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@586
    .line 769
    move-object/from16 v0, p0

    #@588
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@58a
    move-object/from16 v25, v0

    #@58c
    const/16 v26, 0x0

    #@58e
    move-object/from16 v0, v25

    #@590
    move/from16 v1, v23

    #@592
    move/from16 v2, v26

    #@594
    invoke-virtual {v0, v1, v2}, Lcom/lge/ims/service/uc/UCCallManager;->createConferenceSession(II)V

    #@597
    .line 770
    move-object/from16 v0, p0

    #@599
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@59b
    move-object/from16 v25, v0

    #@59d
    move-object/from16 v0, v25

    #@59f
    move/from16 v1, v23

    #@5a1
    move-object/from16 v2, v22

    #@5a3
    move-object/from16 v3, v16

    #@5a5
    invoke-interface {v0, v1, v2, v3}, Lcom/lge/ims/service/uc/IUCSessionListener;->merged(ILcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@5a8
    goto/16 :goto_ee

    #@5aa
    .line 775
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    .end local v23           #sessionKey:I
    :pswitch_5aa
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@5ad
    move-result v19

    #@5ae
    .line 776
    .restart local v19       #reason:I
    const-string v25, "UC"

    #@5b0
    new-instance v26, Ljava/lang/StringBuilder;

    #@5b2
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@5b5
    const-string v27, "MERGE_FAILED :: reason = "

    #@5b7
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5ba
    move-result-object v26

    #@5bb
    move-object/from16 v0, v26

    #@5bd
    move/from16 v1, v19

    #@5bf
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c2
    move-result-object v26

    #@5c3
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c6
    move-result-object v26

    #@5c7
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@5ca
    .line 777
    move-object/from16 v0, p0

    #@5cc
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@5ce
    move-object/from16 v25, v0

    #@5d0
    move-object/from16 v0, v25

    #@5d2
    move/from16 v1, v19

    #@5d4
    invoke-interface {v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener;->mergeFailed(I)V

    #@5d7
    goto/16 :goto_ee

    #@5d9
    .line 782
    .end local v19           #reason:I
    :pswitch_5d9
    const-string v25, "UC"

    #@5db
    const-string v26, "JOINED_CONF"

    #@5dd
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@5e0
    .line 783
    move-object/from16 v0, p0

    #@5e2
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@5e4
    move-object/from16 v25, v0

    #@5e6
    invoke-interface/range {v25 .. v25}, Lcom/lge/ims/service/uc/IUCSessionListener;->joinedConf()V

    #@5e9
    goto/16 :goto_ee

    #@5eb
    .line 788
    :pswitch_5eb
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@5ee
    move-result v19

    #@5ef
    .line 789
    .restart local v19       #reason:I
    const-string v25, "UC"

    #@5f1
    new-instance v26, Ljava/lang/StringBuilder;

    #@5f3
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@5f6
    const-string v27, "JOIN_CONF_FAILED :: reason = "

    #@5f8
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5fb
    move-result-object v26

    #@5fc
    move-object/from16 v0, v26

    #@5fe
    move/from16 v1, v19

    #@600
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@603
    move-result-object v26

    #@604
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@607
    move-result-object v26

    #@608
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@60b
    .line 790
    move-object/from16 v0, p0

    #@60d
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@60f
    move-object/from16 v25, v0

    #@611
    move-object/from16 v0, v25

    #@613
    move/from16 v1, v19

    #@615
    invoke-interface {v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener;->joinConfFailed(I)V

    #@618
    goto/16 :goto_ee

    #@61a
    .line 795
    .end local v19           #reason:I
    :pswitch_61a
    const-string v25, "UC"

    #@61c
    const-string v26, "DROPED_CONF"

    #@61e
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@621
    .line 796
    move-object/from16 v0, p0

    #@623
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@625
    move-object/from16 v25, v0

    #@627
    invoke-interface/range {v25 .. v25}, Lcom/lge/ims/service/uc/IUCSessionListener;->dropedConf()V

    #@62a
    goto/16 :goto_ee

    #@62c
    .line 801
    :pswitch_62c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@62f
    move-result v19

    #@630
    .line 802
    .restart local v19       #reason:I
    const-string v25, "UC"

    #@632
    new-instance v26, Ljava/lang/StringBuilder;

    #@634
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@637
    const-string v27, "DROP_CONF_FAILED :: reason = "

    #@639
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63c
    move-result-object v26

    #@63d
    move-object/from16 v0, v26

    #@63f
    move/from16 v1, v19

    #@641
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@644
    move-result-object v26

    #@645
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@648
    move-result-object v26

    #@649
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@64c
    .line 803
    move-object/from16 v0, p0

    #@64e
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@650
    move-object/from16 v25, v0

    #@652
    move-object/from16 v0, v25

    #@654
    move/from16 v1, v19

    #@656
    invoke-interface {v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener;->dropConfFailed(I)V

    #@659
    goto/16 :goto_ee

    #@65b
    .line 808
    .end local v19           #reason:I
    :pswitch_65b
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@65e
    move-result v15

    #@65f
    .line 810
    .local v15, infoSize:I
    const/16 v25, 0x1

    #@661
    move/from16 v0, v25

    #@663
    if-ge v15, v0, :cond_66e

    #@665
    .line 811
    const-string v25, "UC"

    #@667
    const-string v26, "info size is invalid"

    #@669
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@66c
    goto/16 :goto_a9

    #@66e
    .line 815
    :cond_66e
    const-string v25, "UC"

    #@670
    new-instance v26, Ljava/lang/StringBuilder;

    #@672
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@675
    const-string v27, "NOTIFY_CONF_INFO :: info size = "

    #@677
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67a
    move-result-object v26

    #@67b
    move-object/from16 v0, v26

    #@67d
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@680
    move-result-object v26

    #@681
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@684
    move-result-object v26

    #@685
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@688
    .line 817
    new-array v14, v15, [I

    #@68a
    .line 818
    .local v14, infoArray:[I
    new-array v0, v15, [Ljava/lang/String;

    #@68c
    move-object/from16 v21, v0

    #@68e
    .line 820
    .local v21, recipients:[Ljava/lang/String;
    const/4 v12, 0x0

    #@68f
    .local v12, i:I
    :goto_68f
    if-ge v12, v15, :cond_6d4

    #@691
    .line 821
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@694
    move-result v13

    #@695
    .line 822
    .local v13, info:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@698
    move-result-object v20

    #@699
    .line 824
    .local v20, recipient:Ljava/lang/String;
    const-string v25, "UC"

    #@69b
    new-instance v26, Ljava/lang/StringBuilder;

    #@69d
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@6a0
    const-string v27, "index = "

    #@6a2
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a5
    move-result-object v26

    #@6a6
    move-object/from16 v0, v26

    #@6a8
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6ab
    move-result-object v26

    #@6ac
    const-string v27, " , infoType = "

    #@6ae
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b1
    move-result-object v26

    #@6b2
    move-object/from16 v0, v26

    #@6b4
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b7
    move-result-object v26

    #@6b8
    const-string v27, " , recipients = "

    #@6ba
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6bd
    move-result-object v26

    #@6be
    move-object/from16 v0, v26

    #@6c0
    move-object/from16 v1, v20

    #@6c2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c5
    move-result-object v26

    #@6c6
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c9
    move-result-object v26

    #@6ca
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@6cd
    .line 825
    aput v13, v14, v12

    #@6cf
    .line 826
    aput-object v20, v21, v12

    #@6d1
    .line 820
    add-int/lit8 v12, v12, 0x1

    #@6d3
    goto :goto_68f

    #@6d4
    .line 829
    .end local v13           #info:I
    .end local v20           #recipient:Ljava/lang/String;
    :cond_6d4
    move-object/from16 v0, p0

    #@6d6
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@6d8
    move-object/from16 v25, v0

    #@6da
    move-object/from16 v0, v25

    #@6dc
    move-object/from16 v1, v21

    #@6de
    invoke-interface {v0, v14, v1}, Lcom/lge/ims/service/uc/IUCSessionListener;->notifyConfInfo([I[Ljava/lang/String;)V

    #@6e1
    goto/16 :goto_ee

    #@6e3
    .line 835
    .end local v12           #i:I
    .end local v14           #infoArray:[I
    .end local v15           #infoSize:I
    .end local v21           #recipients:[Ljava/lang/String;
    :pswitch_6e3
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@6e5
    move-object/from16 v0, v22

    #@6e7
    move-object/from16 v1, p1

    #@6e9
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@6ec
    .line 836
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@6ee
    move-object/from16 v0, v16

    #@6f0
    move-object/from16 v1, p1

    #@6f2
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@6f5
    .line 838
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@6f7
    const-string v26, "INCOMING_UPDATE :: "

    #@6f9
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@6fc
    .line 839
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@6ff
    .line 840
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@702
    .line 842
    move-object/from16 v0, p0

    #@704
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@706
    move-object/from16 v26, v0

    #@708
    const/16 v27, 0x0

    #@70a
    move-object/from16 v0, v22

    #@70c
    iget-boolean v0, v0, Lcom/lge/ims/service/uc/SessInfo;->IsConf:Z

    #@70e
    move/from16 v25, v0

    #@710
    if-eqz v25, :cond_730

    #@712
    const/16 v25, 0x1

    #@714
    :goto_714
    move-object/from16 v0, v26

    #@716
    move-object/from16 v1, p0

    #@718
    move/from16 v2, v27

    #@71a
    move/from16 v3, v25

    #@71c
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@71f
    .line 844
    move-object/from16 v0, p0

    #@721
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@723
    move-object/from16 v25, v0

    #@725
    move-object/from16 v0, v25

    #@727
    move-object/from16 v1, v22

    #@729
    move-object/from16 v2, v16

    #@72b
    invoke-interface {v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener;->incomingUpdate(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@72e
    goto/16 :goto_ee

    #@730
    .line 842
    :cond_730
    const/16 v25, 0x0

    #@732
    goto :goto_714

    #@733
    .line 850
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    :pswitch_733
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@736
    move-result v4

    #@737
    .line 852
    .local v4, EnableConf:I
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@739
    move-object/from16 v0, v22

    #@73b
    move-object/from16 v1, p1

    #@73d
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@740
    .line 853
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@742
    move-object/from16 v0, v16

    #@744
    move-object/from16 v1, p1

    #@746
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@749
    .line 855
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@74b
    new-instance v26, Ljava/lang/StringBuilder;

    #@74d
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@750
    const-string v27, "UPDATED :: EnableConf = "

    #@752
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@755
    move-result-object v26

    #@756
    move-object/from16 v0, v26

    #@758
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75b
    move-result-object v26

    #@75c
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75f
    move-result-object v26

    #@760
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@763
    .line 856
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@766
    .line 857
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@769
    .line 859
    move-object/from16 v0, p0

    #@76b
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@76d
    move-object/from16 v26, v0

    #@76f
    const/16 v27, 0x0

    #@771
    move-object/from16 v0, v22

    #@773
    iget-boolean v0, v0, Lcom/lge/ims/service/uc/SessInfo;->IsConf:Z

    #@775
    move/from16 v25, v0

    #@777
    if-eqz v25, :cond_7dd

    #@779
    const/16 v25, 0x1

    #@77b
    :goto_77b
    move-object/from16 v0, v26

    #@77d
    move-object/from16 v1, p0

    #@77f
    move/from16 v2, v27

    #@781
    move/from16 v3, v25

    #@783
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@786
    .line 860
    move-object/from16 v0, p0

    #@788
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@78a
    move-object/from16 v25, v0

    #@78c
    const/16 v26, 0x32

    #@78e
    move-object/from16 v0, v25

    #@790
    move-object/from16 v1, p0

    #@792
    move/from16 v2, v26

    #@794
    invoke-virtual {v0, v1, v2, v4}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@797
    .line 863
    move-object/from16 v0, v22

    #@799
    iget v0, v0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@79b
    move/from16 v25, v0

    #@79d
    const/16 v26, 0x3

    #@79f
    move/from16 v0, v25

    #@7a1
    move/from16 v1, v26

    #@7a3
    if-ne v0, v1, :cond_7e0

    #@7a5
    const/4 v7, 0x1

    #@7a6
    .line 864
    .restart local v7       #callModeChangeable:I
    :goto_7a6
    move-object/from16 v0, p0

    #@7a8
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@7aa
    move-object/from16 v25, v0

    #@7ac
    const/16 v26, 0x33

    #@7ae
    move-object/from16 v0, v25

    #@7b0
    move-object/from16 v1, p0

    #@7b2
    move/from16 v2, v26

    #@7b4
    invoke-virtual {v0, v1, v2, v7}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@7b7
    .line 866
    move-object/from16 v0, p0

    #@7b9
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@7bb
    move-object/from16 v25, v0

    #@7bd
    move-object/from16 v0, v25

    #@7bf
    move-object/from16 v1, v22

    #@7c1
    move-object/from16 v2, v16

    #@7c3
    invoke-interface {v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener;->updated(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@7c6
    .line 867
    move-object/from16 v0, p0

    #@7c8
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@7ca
    move-object/from16 v25, v0

    #@7cc
    move-object/from16 v0, v22

    #@7ce
    iget v0, v0, Lcom/lge/ims/service/uc/SessInfo;->SessionType:I

    #@7d0
    move/from16 v26, v0

    #@7d2
    move-object/from16 v0, v25

    #@7d4
    move-object/from16 v1, p0

    #@7d6
    move/from16 v2, v26

    #@7d8
    invoke-virtual {v0, v1, v2}, Lcom/lge/ims/service/uc/UCCallManager;->updateSession(Lcom/lge/ims/service/uc/UCSessionImpl;I)V

    #@7db
    goto/16 :goto_ee

    #@7dd
    .line 859
    .end local v7           #callModeChangeable:I
    :cond_7dd
    const/16 v25, 0x0

    #@7df
    goto :goto_77b

    #@7e0
    .line 863
    :cond_7e0
    const/4 v7, 0x0

    #@7e1
    goto :goto_7a6

    #@7e2
    .line 872
    .end local v4           #EnableConf:I
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    :pswitch_7e2
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@7e5
    move-result v19

    #@7e6
    .line 873
    .restart local v19       #reason:I
    const-string v25, "UC"

    #@7e8
    new-instance v26, Ljava/lang/StringBuilder;

    #@7ea
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@7ed
    const-string v27, "UPDATE_FAILED :: reason = "

    #@7ef
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f2
    move-result-object v26

    #@7f3
    move-object/from16 v0, v26

    #@7f5
    move/from16 v1, v19

    #@7f7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7fa
    move-result-object v26

    #@7fb
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7fe
    move-result-object v26

    #@7ff
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@802
    .line 874
    move-object/from16 v0, p0

    #@804
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@806
    move-object/from16 v25, v0

    #@808
    move-object/from16 v0, v25

    #@80a
    move/from16 v1, v19

    #@80c
    invoke-interface {v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener;->updateFailed(I)V

    #@80f
    goto/16 :goto_ee

    #@811
    .line 880
    .end local v19           #reason:I
    :pswitch_811
    new-instance v22, Lcom/lge/ims/service/uc/SessInfo;

    #@813
    move-object/from16 v0, v22

    #@815
    move-object/from16 v1, p1

    #@817
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@81a
    .line 881
    .restart local v22       #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    new-instance v16, Lcom/lge/ims/service/uc/MediaInfo;

    #@81c
    move-object/from16 v0, v16

    #@81e
    move-object/from16 v1, p1

    #@820
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@823
    .line 883
    .restart local v16       #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    const-string v25, "UC"

    #@825
    const-string v26, "UPDATED_BY :: "

    #@827
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@82a
    .line 884
    invoke-virtual/range {v22 .. v22}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@82d
    .line 885
    invoke-virtual/range {v16 .. v16}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@830
    .line 887
    move-object/from16 v0, p0

    #@832
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@834
    move-object/from16 v26, v0

    #@836
    const/16 v27, 0x0

    #@838
    move-object/from16 v0, v22

    #@83a
    iget-boolean v0, v0, Lcom/lge/ims/service/uc/SessInfo;->IsConf:Z

    #@83c
    move/from16 v25, v0

    #@83e
    if-eqz v25, :cond_887

    #@840
    const/16 v25, 0x1

    #@842
    :goto_842
    move-object/from16 v0, v26

    #@844
    move-object/from16 v1, p0

    #@846
    move/from16 v2, v27

    #@848
    move/from16 v3, v25

    #@84a
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@84d
    .line 890
    move-object/from16 v0, v22

    #@84f
    iget v0, v0, Lcom/lge/ims/service/uc/SessInfo;->ServiceType:I

    #@851
    move/from16 v25, v0

    #@853
    const/16 v26, 0x3

    #@855
    move/from16 v0, v25

    #@857
    move/from16 v1, v26

    #@859
    if-ne v0, v1, :cond_88a

    #@85b
    const/4 v7, 0x1

    #@85c
    .line 892
    .restart local v7       #callModeChangeable:I
    :goto_85c
    move-object/from16 v0, v22

    #@85e
    iget-boolean v0, v0, Lcom/lge/ims/service/uc/SessInfo;->IsConf:Z

    #@860
    move/from16 v25, v0

    #@862
    if-nez v25, :cond_865

    #@864
    .line 893
    const/4 v7, 0x0

    #@865
    .line 896
    :cond_865
    move-object/from16 v0, p0

    #@867
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@869
    move-object/from16 v25, v0

    #@86b
    const/16 v26, 0x33

    #@86d
    move-object/from16 v0, v25

    #@86f
    move-object/from16 v1, p0

    #@871
    move/from16 v2, v26

    #@873
    invoke-virtual {v0, v1, v2, v7}, Lcom/lge/ims/service/uc/UCCallManager;->setProperty(Lcom/lge/ims/service/uc/UCSessionImpl;II)V

    #@876
    .line 898
    move-object/from16 v0, p0

    #@878
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@87a
    move-object/from16 v25, v0

    #@87c
    move-object/from16 v0, v25

    #@87e
    move-object/from16 v1, v22

    #@880
    move-object/from16 v2, v16

    #@882
    invoke-interface {v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener;->updatedBy(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@885
    goto/16 :goto_ee

    #@887
    .line 887
    .end local v7           #callModeChangeable:I
    :cond_887
    const/16 v25, 0x0

    #@889
    goto :goto_842

    #@88a
    .line 890
    :cond_88a
    const/4 v7, 0x0

    #@88b
    goto :goto_85c

    #@88c
    .line 903
    .end local v16           #mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;
    .end local v22           #sessInfo:Lcom/lge/ims/service/uc/SessInfo;
    :pswitch_88c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@88f
    move-result-object v9

    #@890
    .line 904
    .local v9, connection:Ljava/lang/String;
    const-string v25, "UC"

    #@892
    new-instance v26, Ljava/lang/StringBuilder;

    #@894
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@897
    const-string v27, "connection = "

    #@899
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89c
    move-result-object v26

    #@89d
    move-object/from16 v0, v26

    #@89f
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a2
    move-result-object v26

    #@8a3
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a6
    move-result-object v26

    #@8a7
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8aa
    .line 906
    const-string v25, "com.lge.ims.action.REMOTE_MEDIA"

    #@8ac
    const-string v26, "connection"

    #@8ae
    move-object/from16 v0, p0

    #@8b0
    move-object/from16 v1, v25

    #@8b2
    move-object/from16 v2, v26

    #@8b4
    invoke-virtual {v0, v1, v2, v9}, Lcom/lge/ims/service/uc/UCSessionImpl;->sendIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@8b7
    goto/16 :goto_ee

    #@8b9
    .line 912
    .end local v9           #connection:Ljava/lang/String;
    :pswitch_8b9
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@8bc
    move-result v11

    #@8bd
    .line 913
    .local v11, eType:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@8c0
    move-result v18

    #@8c1
    .line 914
    .local v18, nValue:I
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@8c4
    move-result-object v24

    #@8c5
    .line 916
    .local v24, strValue:Ljava/lang/String;
    const-string v25, "UC"

    #@8c7
    new-instance v26, Ljava/lang/StringBuilder;

    #@8c9
    invoke-direct/range {v26 .. v26}, Ljava/lang/StringBuilder;-><init>()V

    #@8cc
    const-string v27, "NOTIFY_INFO : eType = "

    #@8ce
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d1
    move-result-object v26

    #@8d2
    move-object/from16 v0, v26

    #@8d4
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8d7
    move-result-object v26

    #@8d8
    const-string v27, ", nValue = "

    #@8da
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8dd
    move-result-object v26

    #@8de
    move-object/from16 v0, v26

    #@8e0
    move/from16 v1, v18

    #@8e2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e5
    move-result-object v26

    #@8e6
    const-string v27, ", strValue = "

    #@8e8
    invoke-virtual/range {v26 .. v27}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8eb
    move-result-object v26

    #@8ec
    move-object/from16 v0, v26

    #@8ee
    move-object/from16 v1, v24

    #@8f0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f3
    move-result-object v26

    #@8f4
    invoke-virtual/range {v26 .. v26}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f7
    move-result-object v26

    #@8f8
    invoke-static/range {v25 .. v26}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8fb
    .line 918
    move-object/from16 v0, p0

    #@8fd
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@8ff
    move-object/from16 v25, v0

    #@901
    move-object/from16 v0, v25

    #@903
    move/from16 v1, v18

    #@905
    move-object/from16 v2, v24

    #@907
    invoke-interface {v0, v11, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener;->notifyInfo(IILjava/lang/String;)V

    #@90a
    goto/16 :goto_ee

    #@90c
    .line 927
    .end local v11           #eType:I
    .end local v18           #nValue:I
    .end local v24           #strValue:Ljava/lang/String;
    :cond_90c
    const/16 v25, 0x578

    #@90e
    move/from16 v0, v17

    #@910
    move/from16 v1, v25

    #@912
    if-lt v0, v1, :cond_ee

    #@914
    .line 928
    move-object/from16 v0, p0

    #@916
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mMediaSession:Lcom/lge/ims/service/uc/UCMediaSessionImpl;

    #@918
    move-object/from16 v25, v0

    #@91a
    if-eqz v25, :cond_ee

    #@91c
    .line 929
    const/16 v25, 0x0

    #@91e
    move-object/from16 v0, p1

    #@920
    move/from16 v1, v25

    #@922
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@925
    .line 930
    move-object/from16 v0, p0

    #@927
    iget-object v0, v0, Lcom/lge/ims/service/uc/UCSessionImpl;->mMediaSession:Lcom/lge/ims/service/uc/UCMediaSessionImpl;

    #@929
    move-object/from16 v25, v0

    #@92b
    move-object/from16 v0, v25

    #@92d
    move-object/from16 v1, p1

    #@92f
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/uc/UCMediaSessionImpl;->onMessage(Landroid/os/Parcel;)V
    :try_end_932
    .catch Landroid/os/RemoteException; {:try_start_240 .. :try_end_932} :catch_1bf
    .catch Ljava/lang/Exception; {:try_start_240 .. :try_end_932} :catch_220

    #@932
    goto/16 :goto_ee

    #@934
    .line 580
    :pswitch_data_934
    .packed-switch 0x515
        :pswitch_111
        :pswitch_1e1
        :pswitch_240
        :pswitch_28a
        :pswitch_2dc
        :pswitch_30b
        :pswitch_35d
        :pswitch_3af
        :pswitch_3de
        :pswitch_430
        :pswitch_45f
        :pswitch_4bf
        :pswitch_4ee
        :pswitch_54e
        :pswitch_5aa
        :pswitch_5d9
        :pswitch_5eb
        :pswitch_61a
        :pswitch_62c
        :pswitch_65b
        :pswitch_6e3
        :pswitch_733
        :pswitch_7e2
        :pswitch_811
        :pswitch_88c
        :pswitch_8b9
    .end packed-switch
.end method

.method public reject(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 247
    const-string v2, "UC"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "reject() :: reason = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 249
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1b
    move-result-object v1

    #@1c
    .line 251
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_1f

    #@1e
    .line 263
    :goto_1e
    return-void

    #@1f
    .line 255
    :cond_1f
    const/16 v2, 0x4b4

    #@21
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 256
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 258
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@2a
    move-result-object v0

    #@2b
    .line 259
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 260
    const/4 v1, 0x0

    #@2f
    .line 262
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@31
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@34
    goto :goto_1e
.end method

.method public rejectUpdate(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 478
    const-string v2, "UC"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "rejectUpdateSession() :: reason = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 480
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1b
    move-result-object v1

    #@1c
    .line 482
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_1f

    #@1e
    .line 494
    :goto_1e
    return-void

    #@1f
    .line 486
    :cond_1f
    const/16 v2, 0x4bf

    #@21
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@24
    .line 487
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 489
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@2a
    move-result-object v0

    #@2b
    .line 490
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 491
    const/4 v1, 0x0

    #@2f
    .line 493
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@31
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@34
    goto :goto_1e
.end method

.method public resume()V
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 287
    const-string v2, "UC"

    #@2
    const-string v3, "resume()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 289
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@a
    move-result-object v1

    #@b
    .line 291
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_e

    #@d
    .line 302
    :goto_d
    return-void

    #@e
    .line 295
    :cond_e
    const/16 v2, 0x4b6

    #@10
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 297
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@16
    move-result-object v0

    #@17
    .line 298
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1a
    .line 299
    const/4 v1, 0x0

    #@1b
    .line 301
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@1d
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@20
    goto :goto_d
.end method

.method public sendDTMF(Ljava/lang/String;I)V
    .registers 8
    .parameter "signal"
    .parameter "duration"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 305
    const-string v2, "UC"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "sendDTMF() :: signal = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, " , duration ="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 307
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@25
    move-result-object v1

    #@26
    .line 309
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_29

    #@28
    .line 322
    :goto_28
    return-void

    #@29
    .line 313
    :cond_29
    const/16 v2, 0x4b7

    #@2b
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2e
    .line 314
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@31
    .line 315
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 317
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@37
    move-result-object v0

    #@38
    .line 318
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 319
    const/4 v1, 0x0

    #@3c
    .line 321
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@3e
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@41
    goto :goto_28
.end method

.method public sendIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "action"
    .parameter "name"
    .parameter "value"

    #@0
    .prologue
    .line 949
    const-string v1, "UC"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "sendIntent ("

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, "), name="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ", value="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 951
    new-instance v0, Landroid/content/Intent;

    #@2e
    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@31
    .line 953
    .local v0, intent:Landroid/content/Intent;
    if-nez v0, :cond_3b

    #@33
    .line 954
    const-string v1, "UC"

    #@35
    const-string v2, "Intent is null"

    #@37
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 963
    :goto_3a
    return-void

    #@3b
    .line 959
    :cond_3b
    const/16 v1, 0x20

    #@3d
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@40
    .line 960
    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@43
    .line 962
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mContext:Landroid/content/Context;

    #@45
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@48
    goto :goto_3a
.end method

.method public sendStartFailed()V
    .registers 5

    #@0
    .prologue
    .line 118
    const-string v1, "UC"

    #@2
    const-string v2, "sendStartFailed :: exception handled"

    #@4
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 121
    :try_start_7
    iget v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallType:I

    #@9
    const/4 v2, 0x1

    #@a
    if-ne v1, v2, :cond_18

    #@c
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@e
    if-eqz v1, :cond_18

    #@10
    .line 122
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@12
    const/16 v2, 0x516

    #@14
    const/4 v3, 0x0

    #@15
    invoke-virtual {v1, v2, v3}, Lcom/lge/ims/service/uc/UCCallManager;->notifyECallState(II)V
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_18} :catch_1d

    #@18
    .line 128
    :cond_18
    :goto_18
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@1a
    if-nez v1, :cond_37

    #@1c
    .line 139
    :goto_1c
    return-void

    #@1d
    .line 124
    :catch_1d
    move-exception v0

    #@1e
    .line 125
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "UC"

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "Exception ::"

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    goto :goto_18

    #@37
    .line 133
    .end local v0           #e:Ljava/lang/Exception;
    :cond_37
    :try_start_37
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@39
    const/16 v2, 0x199

    #@3b
    const/4 v3, 0x0

    #@3c
    invoke-interface {v1, v2, v3}, Lcom/lge/ims/service/uc/IUCSessionListener;->startFailed(II)V
    :try_end_3f
    .catch Landroid/os/RemoteException; {:try_start_37 .. :try_end_3f} :catch_40
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_3f} :catch_5a

    #@3f
    goto :goto_1c

    #@40
    .line 134
    :catch_40
    move-exception v0

    #@41
    .line 135
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "UC"

    #@43
    new-instance v2, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v3, "RemoteException ::"

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v2

    #@56
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@59
    goto :goto_1c

    #@5a
    .line 136
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_5a
    move-exception v0

    #@5b
    .line 137
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "UC"

    #@5d
    new-instance v2, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v3, "Exception ::"

    #@64
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v2

    #@68
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v2

    #@70
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    goto :goto_1c
.end method

.method public setListener(Lcom/lge/ims/service/uc/IUCSessionListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 497
    const-string v0, "UC"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setListener() :: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 499
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mListener:Lcom/lge/ims/service/uc/IUCSessionListener;

    #@1a
    .line 501
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/UCSessionImpl;->isTerminated()Z

    #@1d
    move-result v0

    #@1e
    const/4 v1, 0x1

    #@1f
    if-ne v0, v1, :cond_2a

    #@21
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@23
    if-eqz v0, :cond_2a

    #@25
    .line 502
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@27
    invoke-virtual {v0, p0}, Lcom/lge/ims/service/uc/UCCallManager;->processStartedFail(Lcom/lge/ims/service/uc/UCSessionImpl;)V

    #@2a
    .line 504
    :cond_2a
    return-void
.end method

.method public start(ILjava/lang/String;Lcom/lge/ims/service/uc/MediaInfo;Lcom/lge/ims/service/uc/SuppInfo;)V
    .registers 13
    .parameter "sessionType"
    .parameter "target"
    .parameter "mediaInfo"
    .parameter "suppInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/16 v7, 0x4b1

    #@2
    const/4 v6, 0x1

    #@3
    .line 144
    const-string v3, "UC"

    #@5
    new-instance v4, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v5, "start() :: sessionType = "

    #@c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v4

    #@10
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    const-string v5, " , target = "

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 146
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@28
    move-result-object v2

    #@29
    .line 148
    .local v2, parcel:Landroid/os/Parcel;
    if-nez v2, :cond_2c

    #@2b
    .line 180
    :cond_2b
    :goto_2b
    return-void

    #@2c
    .line 152
    :cond_2c
    invoke-virtual {v2, v7}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    .line 153
    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 154
    invoke-virtual {v2, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@35
    .line 156
    invoke-virtual {p3}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@38
    .line 157
    invoke-virtual {p3, v2, v6}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@3b
    .line 159
    invoke-virtual {p4}, Lcom/lge/ims/service/uc/SuppInfo;->logIn()V

    #@3e
    .line 160
    invoke-virtual {p4, v2, v6}, Lcom/lge/ims/service/uc/SuppInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@41
    .line 162
    invoke-virtual {v2}, Landroid/os/Parcel;->marshall()[B

    #@44
    move-result-object v0

    #@45
    .line 163
    .local v0, baData:[B
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    #@48
    .line 164
    const/4 v2, 0x0

    #@49
    .line 166
    iget v3, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@4b
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@4e
    .line 168
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@50
    if-eqz v3, :cond_57

    #@52
    .line 169
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@54
    invoke-virtual {v3, p0, p1}, Lcom/lge/ims/service/uc/UCCallManager;->startSession(Lcom/lge/ims/service/uc/UCSessionImpl;I)V

    #@57
    .line 174
    :cond_57
    :try_start_57
    iget v3, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallType:I

    #@59
    if-ne v3, v6, :cond_2b

    #@5b
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@5d
    if-eqz v3, :cond_2b

    #@5f
    .line 175
    iget-object v3, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@61
    const/16 v4, 0x4b1

    #@63
    const/4 v5, 0x0

    #@64
    invoke-virtual {v3, v4, v5}, Lcom/lge/ims/service/uc/UCCallManager;->notifyECallState(II)V
    :try_end_67
    .catch Ljava/lang/Exception; {:try_start_57 .. :try_end_67} :catch_68

    #@67
    goto :goto_2b

    #@68
    .line 177
    :catch_68
    move-exception v1

    #@69
    .line 178
    .local v1, e:Ljava/lang/Exception;
    const-string v3, "UC"

    #@6b
    new-instance v4, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v5, "Exception ::"

    #@72
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v4

    #@76
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v4

    #@7e
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@81
    goto :goto_2b
.end method

.method public startConf(ILcom/lge/ims/service/uc/ConfInfo;Lcom/lge/ims/service/uc/MediaInfo;Lcom/lge/ims/service/uc/SuppInfo;)V
    .registers 11
    .parameter "sessionType"
    .parameter "confInfo"
    .parameter "mediaInfo"
    .parameter "suppInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 185
    const-string v2, "UC"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "startConf() :: sessionType = "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 187
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1c
    move-result-object v1

    #@1d
    .line 189
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_20

    #@1f
    .line 220
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 193
    :cond_20
    invoke-virtual {p2}, Lcom/lge/ims/service/uc/ConfInfo;->getSize()I

    #@23
    move-result v2

    #@24
    if-gtz v2, :cond_2e

    #@26
    .line 194
    const-string v2, "UC"

    #@28
    const-string v3, "startConf() :: participants\'s size is 0"

    #@2a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    goto :goto_1f

    #@2e
    .line 198
    :cond_2e
    const/16 v2, 0x4b2

    #@30
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@33
    .line 199
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@36
    .line 201
    invoke-virtual {p2}, Lcom/lge/ims/service/uc/ConfInfo;->logIn()V

    #@39
    .line 202
    invoke-virtual {p2, v1, v5}, Lcom/lge/ims/service/uc/ConfInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@3c
    .line 204
    invoke-virtual {p3}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@3f
    .line 205
    invoke-virtual {p3, v1, v5}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@42
    .line 207
    invoke-virtual {p4}, Lcom/lge/ims/service/uc/SuppInfo;->logIn()V

    #@45
    .line 208
    invoke-virtual {p4, v1, v5}, Lcom/lge/ims/service/uc/SuppInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@48
    .line 210
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@4b
    move-result-object v0

    #@4c
    .line 211
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@4f
    .line 212
    const/4 v1, 0x0

    #@50
    .line 214
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@52
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@55
    .line 216
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@57
    if-eqz v2, :cond_1f

    #@59
    .line 217
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@5b
    invoke-virtual {v2, p0, p1}, Lcom/lge/ims/service/uc/UCCallManager;->startSession(Lcom/lge/ims/service/uc/UCSessionImpl;I)V

    #@5e
    goto :goto_1f
.end method

.method public terminate(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 325
    const-string v2, "UC"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "terminate() :: reason = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 327
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@1a
    if-nez v2, :cond_24

    #@1c
    .line 328
    const-string v2, "UC"

    #@1e
    const-string v3, "Session is already closed."

    #@20
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 346
    :cond_23
    :goto_23
    return-void

    #@24
    .line 332
    :cond_24
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@27
    move-result-object v1

    #@28
    .line 334
    .local v1, parcel:Landroid/os/Parcel;
    if-eqz v1, :cond_23

    #@2a
    .line 338
    const/16 v2, 0x4b8

    #@2c
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2f
    .line 339
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@32
    .line 341
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@35
    move-result-object v0

    #@36
    .line 342
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@39
    .line 343
    const/4 v1, 0x0

    #@3a
    .line 345
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@3c
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@3f
    goto :goto_23
.end method

.method public update(ILcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessionType"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 430
    const-string v2, "UC"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "updateSession() :: sessionType = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 431
    invoke-virtual {p2}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@1b
    .line 433
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@1e
    move-result-object v1

    #@1f
    .line 435
    .local v1, parcel:Landroid/os/Parcel;
    if-nez v1, :cond_22

    #@21
    .line 449
    :goto_21
    return-void

    #@22
    .line 439
    :cond_22
    const/16 v2, 0x4bd

    #@24
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@27
    .line 440
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@2a
    .line 442
    const/4 v2, 0x1

    #@2b
    invoke-virtual {p2, v1, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@2e
    .line 444
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@31
    move-result-object v0

    #@32
    .line 445
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 446
    const/4 v1, 0x0

    #@36
    .line 448
    iget v2, p0, Lcom/lge/ims/service/uc/UCSessionImpl;->mNativeSession:I

    #@38
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@3b
    goto :goto_21
.end method
