.class public abstract Lcom/lge/ims/service/cs/IImageSession$Stub;
.super Landroid/os/Binder;
.source "IImageSession.java"

# interfaces
.implements Lcom/lge/ims/service/cs/IImageSession;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cs/IImageSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.cs.IImageSession"

.field static final TRANSACTION_accept:I = 0x5

.field static final TRANSACTION_getSessionType:I = 0x2

.field static final TRANSACTION_reject:I = 0x6

.field static final TRANSACTION_setListener:I = 0x1

.field static final TRANSACTION_start:I = 0x3

.field static final TRANSACTION_stop:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.cs.IImageSession"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/cs/IImageSession$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IImageSession;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.cs.IImageSession"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/cs/IImageSession;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/cs/IImageSession;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/cs/IImageSession$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 12
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_92

    #@4
    .line 106
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v6

    #@8
    :goto_8
    return v6

    #@9
    .line 42
    :sswitch_9
    const-string v5, "com.lge.ims.service.cs.IImageSession"

    #@b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v5, "com.lge.ims.service.cs.IImageSession"

    #@11
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17
    move-result-object v5

    #@18
    invoke-static {v5}, Lcom/lge/ims/service/cs/IImageSessionListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IImageSessionListener;

    #@1b
    move-result-object v0

    #@1c
    .line 50
    .local v0, _arg0:Lcom/lge/ims/service/cs/IImageSessionListener;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IImageSession$Stub;->setListener(Lcom/lge/ims/service/cs/IImageSessionListener;)Z

    #@1f
    move-result v4

    #@20
    .line 51
    .local v4, _result:Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@23
    .line 52
    if-eqz v4, :cond_2a

    #@25
    move v5, v6

    #@26
    :goto_26
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@29
    goto :goto_8

    #@2a
    :cond_2a
    const/4 v5, 0x0

    #@2b
    goto :goto_26

    #@2c
    .line 57
    .end local v0           #_arg0:Lcom/lge/ims/service/cs/IImageSessionListener;
    .end local v4           #_result:Z
    :sswitch_2c
    const-string v5, "com.lge.ims.service.cs.IImageSession"

    #@2e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 58
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IImageSession$Stub;->getSessionType()I

    #@34
    move-result v4

    #@35
    .line 59
    .local v4, _result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@38
    .line 60
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@3b
    goto :goto_8

    #@3c
    .line 65
    .end local v4           #_result:I
    :sswitch_3c
    const-string v5, "com.lge.ims.service.cs.IImageSession"

    #@3e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@41
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    .line 69
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    .line 71
    .local v1, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    .line 73
    .local v2, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@50
    move-result v3

    #@51
    .line 74
    .local v3, _arg3:I
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/lge/ims/service/cs/IImageSession$Stub;->start(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I

    #@54
    move-result v4

    #@55
    .line 75
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@58
    .line 76
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@5b
    goto :goto_8

    #@5c
    .line 81
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:Ljava/lang/String;
    .end local v2           #_arg2:Ljava/lang/String;
    .end local v3           #_arg3:I
    .end local v4           #_result:I
    :sswitch_5c
    const-string v5, "com.lge.ims.service.cs.IImageSession"

    #@5e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@61
    .line 82
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IImageSession$Stub;->stop()I

    #@64
    move-result v4

    #@65
    .line 83
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@68
    .line 84
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@6b
    goto :goto_8

    #@6c
    .line 89
    .end local v4           #_result:I
    :sswitch_6c
    const-string v5, "com.lge.ims.service.cs.IImageSession"

    #@6e
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@71
    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@74
    move-result-object v0

    #@75
    .line 92
    .restart local v0       #_arg0:Ljava/lang/String;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IImageSession$Stub;->accept(Ljava/lang/String;)I

    #@78
    move-result v4

    #@79
    .line 93
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@7c
    .line 94
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@7f
    goto :goto_8

    #@80
    .line 99
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v4           #_result:I
    :sswitch_80
    const-string v5, "com.lge.ims.service.cs.IImageSession"

    #@82
    invoke-virtual {p2, v5}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@85
    .line 100
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IImageSession$Stub;->reject()I

    #@88
    move-result v4

    #@89
    .line 101
    .restart local v4       #_result:I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@8c
    .line 102
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    #@8f
    goto/16 :goto_8

    #@91
    .line 38
    nop

    #@92
    :sswitch_data_92
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_3c
        0x4 -> :sswitch_5c
        0x5 -> :sswitch_6c
        0x6 -> :sswitch_80
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
