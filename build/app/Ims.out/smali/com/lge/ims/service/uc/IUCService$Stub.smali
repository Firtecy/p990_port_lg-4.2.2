.class public abstract Lcom/lge/ims/service/uc/IUCService$Stub;
.super Landroid/os/Binder;
.source "IUCService.java"

# interfaces
.implements Lcom/lge/ims/service/uc/IUCService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUCService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/IUCService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.uc.IUCService"

.field static final TRANSACTION_attachSession:I = 0x2

.field static final TRANSACTION_closeSession:I = 0x3

.field static final TRANSACTION_openSession:I = 0x1

.field static final TRANSACTION_setListener:I = 0x4


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.lge.ims.service.uc.IUCService"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/uc/IUCService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCService;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.lge.ims.service.uc.IUCService"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/uc/IUCService;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/lge/ims/service/uc/IUCService;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/lge/ims/service/uc/IUCService$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/IUCService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 43
    sparse-switch p1, :sswitch_data_72

    #@5
    .line 89
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v2

    #@9
    :goto_9
    return v2

    #@a
    .line 47
    :sswitch_a
    const-string v2, "com.lge.ims.service.uc.IUCService"

    #@c
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    move v2, v3

    #@10
    .line 48
    goto :goto_9

    #@11
    .line 52
    :sswitch_11
    const-string v4, "com.lge.ims.service.uc.IUCService"

    #@13
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@16
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@19
    move-result v0

    #@1a
    .line 55
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCService$Stub;->openSession(I)Lcom/lge/ims/service/uc/IUCSession;

    #@1d
    move-result-object v1

    #@1e
    .line 56
    .local v1, _result:Lcom/lge/ims/service/uc/IUCSession;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@21
    .line 57
    if-eqz v1, :cond_27

    #@23
    invoke-interface {v1}, Lcom/lge/ims/service/uc/IUCSession;->asBinder()Landroid/os/IBinder;

    #@26
    move-result-object v2

    #@27
    :cond_27
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@2a
    move v2, v3

    #@2b
    .line 58
    goto :goto_9

    #@2c
    .line 62
    .end local v0           #_arg0:I
    .end local v1           #_result:Lcom/lge/ims/service/uc/IUCSession;
    :sswitch_2c
    const-string v4, "com.lge.ims.service.uc.IUCService"

    #@2e
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@31
    .line 64
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@34
    move-result v0

    #@35
    .line 65
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCService$Stub;->attachSession(I)Lcom/lge/ims/service/uc/IUCSession;

    #@38
    move-result-object v1

    #@39
    .line 66
    .restart local v1       #_result:Lcom/lge/ims/service/uc/IUCSession;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3c
    .line 67
    if-eqz v1, :cond_42

    #@3e
    invoke-interface {v1}, Lcom/lge/ims/service/uc/IUCSession;->asBinder()Landroid/os/IBinder;

    #@41
    move-result-object v2

    #@42
    :cond_42
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@45
    move v2, v3

    #@46
    .line 68
    goto :goto_9

    #@47
    .line 72
    .end local v0           #_arg0:I
    .end local v1           #_result:Lcom/lge/ims/service/uc/IUCSession;
    :sswitch_47
    const-string v2, "com.lge.ims.service.uc.IUCService"

    #@49
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4c
    .line 74
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@4f
    move-result-object v2

    #@50
    invoke-static {v2}, Lcom/lge/ims/service/uc/IUCSession$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCSession;

    #@53
    move-result-object v0

    #@54
    .line 75
    .local v0, _arg0:Lcom/lge/ims/service/uc/IUCSession;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCService$Stub;->closeSession(Lcom/lge/ims/service/uc/IUCSession;)V

    #@57
    .line 76
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5a
    move v2, v3

    #@5b
    .line 77
    goto :goto_9

    #@5c
    .line 81
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/IUCSession;
    :sswitch_5c
    const-string v2, "com.lge.ims.service.uc.IUCService"

    #@5e
    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@61
    .line 83
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@64
    move-result-object v2

    #@65
    invoke-static {v2}, Lcom/lge/ims/service/uc/IUCServiceListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCServiceListener;

    #@68
    move-result-object v0

    #@69
    .line 84
    .local v0, _arg0:Lcom/lge/ims/service/uc/IUCServiceListener;
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCService$Stub;->setListener(Lcom/lge/ims/service/uc/IUCServiceListener;)V

    #@6c
    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6f
    move v2, v3

    #@70
    .line 86
    goto :goto_9

    #@71
    .line 43
    nop

    #@72
    :sswitch_data_72
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_2c
        0x3 -> :sswitch_47
        0x4 -> :sswitch_5c
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
