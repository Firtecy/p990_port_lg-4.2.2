.class public abstract Lcom/lge/ims/service/im/FileTransferImpl;
.super Ljava/lang/Object;
.source "FileTransferImpl.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;
.implements Ljava/io/Serializable;


# instance fields
.field private fileTransferImplFacade:Lcom/lge/ims/service/im/FileTransferImplFacade;

.field listener:Lcom/lge/ims/service/im/IFileTransferListener;

.field private mContext:Landroid/content/Context;

.field protected nativeObj:I

.field remoteUsers:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;)V
    .registers 5
    .parameter "fileTransferImplFacade"
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 34
    const/4 v0, 0x0

    #@5
    iput v0, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@7
    .line 35
    iput-object v1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@9
    .line 36
    iput-object v1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@b
    .line 39
    const-string v0, ""

    #@d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@10
    .line 40
    iput-object p2, p0, Lcom/lge/ims/service/im/FileTransferImpl;->mContext:Landroid/content/Context;

    #@12
    .line 41
    iput-object p1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->fileTransferImplFacade:Lcom/lge/ims/service/im/FileTransferImplFacade;

    #@14
    .line 42
    invoke-virtual {p0}, Lcom/lge/ims/service/im/FileTransferImpl;->getInterface()I

    #@17
    move-result v0

    #@18
    iput v0, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@1a
    .line 43
    iget v0, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@1c
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@1f
    .line 44
    return-void
.end method

.method public constructor <init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;I)V
    .registers 6
    .parameter "fileTransferImplFacade"
    .parameter "context"
    .parameter "nativeObj"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 34
    const/4 v0, 0x0

    #@5
    iput v0, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@7
    .line 35
    iput-object v1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@9
    .line 36
    iput-object v1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@b
    .line 56
    iput-object p2, p0, Lcom/lge/ims/service/im/FileTransferImpl;->mContext:Landroid/content/Context;

    #@d
    .line 57
    iput-object p1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->fileTransferImplFacade:Lcom/lge/ims/service/im/FileTransferImplFacade;

    #@f
    .line 58
    iput p3, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@11
    .line 59
    invoke-static {p3, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@14
    .line 60
    return-void
.end method

.method public constructor <init>(Lcom/lge/ims/service/im/FileTransferImplFacade;Landroid/content/Context;[Ljava/lang/String;)V
    .registers 6
    .parameter "fileTransferImplFacade"
    .parameter "context"
    .parameter "remoteUsers"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 34
    const/4 v0, 0x0

    #@5
    iput v0, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@7
    .line 35
    iput-object v1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@9
    .line 36
    iput-object v1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@b
    .line 47
    const-string v0, ""

    #@d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@10
    .line 48
    iput-object p2, p0, Lcom/lge/ims/service/im/FileTransferImpl;->mContext:Landroid/content/Context;

    #@12
    .line 49
    iput-object p1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->fileTransferImplFacade:Lcom/lge/ims/service/im/FileTransferImplFacade;

    #@14
    .line 50
    invoke-virtual {p0}, Lcom/lge/ims/service/im/FileTransferImpl;->getInterface()I

    #@17
    move-result v0

    #@18
    iput v0, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@1a
    .line 51
    iget v0, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@1c
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@1f
    .line 52
    iput-object p3, p0, Lcom/lge/ims/service/im/FileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@21
    .line 53
    return-void
.end method


# virtual methods
.method public accept(Ljava/lang/String;)V
    .registers 3
    .parameter "folderPath"

    #@0
    .prologue
    .line 210
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 211
    return-void
.end method

.method final cancel()V
    .registers 3

    #@0
    .prologue
    .line 67
    const-string v1, ": to Send BYE"

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 69
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v0

    #@9
    .line 71
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x29a6

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 73
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/FileTransferImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@11
    .line 74
    return-void
.end method

.method final destroy()V
    .registers 3

    #@0
    .prologue
    .line 87
    const-string v0, ": release FT Interface"

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 89
    iget v0, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@7
    const/4 v1, 0x0

    #@8
    invoke-static {v0, v1}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@b
    .line 90
    iget v0, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@d
    invoke-static {v0}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@10
    .line 91
    const/4 v0, 0x0

    #@11
    iput v0, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@13
    .line 92
    return-void
.end method

.method public download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .registers 7
    .parameter "fileName"
    .parameter "url"
    .parameter "contentType"
    .parameter "fileSize"
    .parameter "folderPath"

    #@0
    .prologue
    .line 215
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 216
    return-void
.end method

.method public abstract getInterface()I
.end method

.method public final onMessage(Landroid/os/Parcel;)V
    .registers 25
    .parameter "parcel"

    #@0
    .prologue
    .line 107
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v13

    #@4
    .line 108
    .local v13, msg:I
    new-instance v19, Ljava/lang/StringBuilder;

    #@6
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v20, " : msg="

    #@b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v19

    #@f
    move-object/from16 v0, v19

    #@11
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v19

    #@15
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v19

    #@19
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1c
    .line 110
    move-object/from16 v0, p0

    #@1e
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@20
    move-object/from16 v19, v0

    #@22
    if-nez v19, :cond_2a

    #@24
    .line 111
    const-string v19, " : NO LISTENER!!!"

    #@26
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@29
    .line 206
    :goto_29
    return-void

    #@2a
    .line 116
    :cond_2a
    packed-switch v13, :pswitch_data_254

    #@2d
    .line 190
    :try_start_2d
    const-string v19, " : NOT HANDLED!!!"

    #@2f
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_32} :catch_33

    #@32
    goto :goto_29

    #@33
    .line 193
    :catch_33
    move-exception v7

    #@34
    .line 194
    .local v7, e:Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    #@37
    .line 195
    const-string v19, "RemoteException!!!"

    #@39
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@3c
    .line 197
    new-instance v12, Landroid/content/Intent;

    #@3e
    invoke-direct {v12}, Landroid/content/Intent;-><init>()V

    #@41
    .line 198
    .local v12, intent:Landroid/content/Intent;
    const-string v19, "com.lge.ims.service.im.IIMService"

    #@43
    move-object/from16 v0, v19

    #@45
    invoke-virtual {v12, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@48
    .line 199
    const-string v19, "deleteFileTransfer"

    #@4a
    const/16 v20, 0x1

    #@4c
    move-object/from16 v0, v19

    #@4e
    move/from16 v1, v20

    #@50
    invoke-virtual {v12, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@53
    .line 200
    new-instance v9, Landroid/os/Bundle;

    #@55
    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    #@58
    .line 201
    .local v9, extra:Landroid/os/Bundle;
    const-string v19, "FileTransferImplFacade"

    #@5a
    move-object/from16 v0, p0

    #@5c
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->fileTransferImplFacade:Lcom/lge/ims/service/im/FileTransferImplFacade;

    #@5e
    move-object/from16 v20, v0

    #@60
    move-object/from16 v0, v19

    #@62
    move-object/from16 v1, v20

    #@64
    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    #@67
    .line 202
    invoke-virtual {v12, v9}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    #@6a
    .line 204
    move-object/from16 v0, p0

    #@6c
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->mContext:Landroid/content/Context;

    #@6e
    move-object/from16 v19, v0

    #@70
    move-object/from16 v0, v19

    #@72
    invoke-virtual {v0, v12}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@75
    goto :goto_29

    #@76
    .line 118
    .end local v7           #e:Ljava/lang/Exception;
    .end local v9           #extra:Landroid/os/Bundle;
    .end local v12           #intent:Landroid/content/Intent;
    :pswitch_76
    :try_start_76
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@79
    move-result v16

    #@7a
    .line 119
    .local v16, result:I
    new-instance v19, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v20, " : ESTABLISH_IND: result="

    #@81
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v19

    #@85
    move-object/from16 v0, v19

    #@87
    move/from16 v1, v16

    #@89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v19

    #@8d
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v19

    #@91
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@94
    .line 120
    if-nez v16, :cond_a0

    #@96
    .line 121
    move-object/from16 v0, p0

    #@98
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@9a
    move-object/from16 v19, v0

    #@9c
    invoke-interface/range {v19 .. v19}, Lcom/lge/ims/service/im/IFileTransferListener;->transferStarted()V

    #@9f
    goto :goto_29

    #@a0
    .line 123
    :cond_a0
    move-object/from16 v0, p0

    #@a2
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@a4
    move-object/from16 v19, v0

    #@a6
    move-object/from16 v0, v19

    #@a8
    move/from16 v1, v16

    #@aa
    invoke-interface {v0, v1}, Lcom/lge/ims/service/im/IFileTransferListener;->transferStartFailed(I)V

    #@ad
    goto/16 :goto_29

    #@af
    .line 128
    .end local v16           #result:I
    :pswitch_af
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@b2
    move-result v15

    #@b3
    .line 130
    .local v15, reason:I
    if-eqz v15, :cond_da

    #@b5
    .line 131
    new-instance v19, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v20, " : TERMINATED_IND: reason="

    #@bc
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v19

    #@c0
    move-object/from16 v0, v19

    #@c2
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v19

    #@c6
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v19

    #@ca
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@cd
    .line 132
    move-object/from16 v0, p0

    #@cf
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@d1
    move-object/from16 v19, v0

    #@d3
    move-object/from16 v0, v19

    #@d5
    invoke-interface {v0, v15}, Lcom/lge/ims/service/im/IFileTransferListener;->transferFailed(I)V

    #@d8
    goto/16 :goto_29

    #@da
    .line 134
    :cond_da
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@dd
    move-result-object v17

    #@de
    .line 135
    .local v17, thumbnailUploadUrl:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e1
    move-result-object v18

    #@e2
    .line 136
    .local v18, uploadUrl:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e5
    move-result-object v8

    #@e6
    .line 137
    .local v8, expiryDateTime:Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e9
    move-result-object v10

    #@ea
    .line 139
    .local v10, fileLinkInfo:Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v20, " : TERMINATED_IND: until="

    #@f1
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v19

    #@f5
    move-object/from16 v0, v19

    #@f7
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v19

    #@fb
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fe
    move-result-object v19

    #@ff
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@102
    .line 141
    const-string v19, "GMT"

    #@104
    invoke-static/range {v19 .. v19}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@107
    move-result-object v19

    #@108
    invoke-static/range {v19 .. v19}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    #@10b
    move-result-object v2

    #@10c
    .line 142
    .local v2, cal:Ljava/util/Calendar;
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    #@10f
    move-result-wide v19

    #@110
    const-wide/32 v21, 0x5265c00

    #@113
    add-long v4, v19, v21

    #@115
    .line 143
    .local v4, currentTime:J
    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    #@118
    .line 144
    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    #@11b
    move-result-object v3

    #@11c
    .line 145
    .local v3, currentLocalTime:Ljava/util/Date;
    new-instance v6, Ljava/text/SimpleDateFormat;

    #@11e
    const-string v19, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    #@120
    move-object/from16 v0, v19

    #@122
    invoke-direct {v6, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@125
    .line 146
    .local v6, dateFormat:Ljava/text/SimpleDateFormat;
    const-string v19, "GMT"

    #@127
    invoke-static/range {v19 .. v19}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    #@12a
    move-result-object v19

    #@12b
    move-object/from16 v0, v19

    #@12d
    invoke-virtual {v6, v0}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    #@130
    .line 147
    invoke-virtual {v6, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@133
    move-result-object v8

    #@134
    .line 149
    new-instance v19, Ljava/lang/StringBuilder;

    #@136
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@139
    const-string v20, " : TERMINATED_IND: reason="

    #@13b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v19

    #@13f
    move-object/from16 v0, v19

    #@141
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@144
    move-result-object v19

    #@145
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@148
    move-result-object v19

    #@149
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@14c
    .line 150
    new-instance v19, Ljava/lang/StringBuilder;

    #@14e
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@151
    const-string v20, " : TERMINATED_IND: thumbnailUploadUrl="

    #@153
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v19

    #@157
    move-object/from16 v0, v19

    #@159
    move-object/from16 v1, v17

    #@15b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v19

    #@15f
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@162
    move-result-object v19

    #@163
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@166
    .line 151
    new-instance v19, Ljava/lang/StringBuilder;

    #@168
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@16b
    const-string v20, " : TERMINATED_IND: uploadUrl="

    #@16d
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v19

    #@171
    move-object/from16 v0, v19

    #@173
    move-object/from16 v1, v18

    #@175
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@178
    move-result-object v19

    #@179
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17c
    move-result-object v19

    #@17d
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@180
    .line 152
    new-instance v19, Ljava/lang/StringBuilder;

    #@182
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@185
    const-string v20, " : TERMINATED_IND: until="

    #@187
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v19

    #@18b
    move-object/from16 v0, v19

    #@18d
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    move-result-object v19

    #@191
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@194
    move-result-object v19

    #@195
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@198
    .line 153
    new-instance v19, Ljava/lang/StringBuilder;

    #@19a
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@19d
    const-string v20, " : TERMINATED_IND: fileLinkInfo="

    #@19f
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v19

    #@1a3
    move-object/from16 v0, v19

    #@1a5
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v19

    #@1a9
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v19

    #@1ad
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1b0
    .line 155
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    #@1b3
    move-result v19

    #@1b4
    const/16 v20, 0x1

    #@1b6
    move/from16 v0, v19

    #@1b8
    move/from16 v1, v20

    #@1ba
    if-ge v0, v1, :cond_1be

    #@1bc
    .line 156
    const/16 v17, 0x0

    #@1be
    .line 160
    :cond_1be
    move-object/from16 v0, p0

    #@1c0
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@1c2
    move-object/from16 v19, v0

    #@1c4
    move-object/from16 v0, v19

    #@1c6
    invoke-interface {v0, v10}, Lcom/lge/ims/service/im/IFileTransferListener;->fileSent(Ljava/lang/String;)V

    #@1c9
    goto/16 :goto_29

    #@1cb
    .line 165
    .end local v2           #cal:Ljava/util/Calendar;
    .end local v3           #currentLocalTime:Ljava/util/Date;
    .end local v4           #currentTime:J
    .end local v6           #dateFormat:Ljava/text/SimpleDateFormat;
    .end local v8           #expiryDateTime:Ljava/lang/String;
    .end local v10           #fileLinkInfo:Ljava/lang/String;
    .end local v15           #reason:I
    .end local v17           #thumbnailUploadUrl:Ljava/lang/String;
    .end local v18           #uploadUrl:Ljava/lang/String;
    :pswitch_1cb
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@1ce
    move-result v15

    #@1cf
    .line 166
    .restart local v15       #reason:I
    new-instance v19, Ljava/lang/StringBuilder;

    #@1d1
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1d4
    const-string v20, " : TERMINATED_IND: reason="

    #@1d6
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v19

    #@1da
    move-object/from16 v0, v19

    #@1dc
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v19

    #@1e0
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e3
    move-result-object v19

    #@1e4
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1e7
    .line 168
    if-nez v15, :cond_1f4

    #@1e9
    .line 169
    move-object/from16 v0, p0

    #@1eb
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@1ed
    move-object/from16 v19, v0

    #@1ef
    invoke-interface/range {v19 .. v19}, Lcom/lge/ims/service/im/IFileTransferListener;->transferTerminated()V

    #@1f2
    goto/16 :goto_29

    #@1f4
    .line 171
    :cond_1f4
    move-object/from16 v0, p0

    #@1f6
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@1f8
    move-object/from16 v19, v0

    #@1fa
    move-object/from16 v0, v19

    #@1fc
    invoke-interface {v0, v15}, Lcom/lge/ims/service/im/IFileTransferListener;->transferFailed(I)V

    #@1ff
    goto/16 :goto_29

    #@201
    .line 177
    .end local v15           #reason:I
    :pswitch_201
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readInt()I

    #@204
    move-result v14

    #@205
    .line 178
    .local v14, nBytesTransferred:I
    new-instance v19, Ljava/lang/StringBuilder;

    #@207
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@20a
    const-string v20, " : TRANSFERPROGRESS_IND: "

    #@20c
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20f
    move-result-object v19

    #@210
    move-object/from16 v0, v19

    #@212
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@215
    move-result-object v19

    #@216
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@219
    move-result-object v19

    #@21a
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@21d
    .line 179
    move-object/from16 v0, p0

    #@21f
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@221
    move-object/from16 v19, v0

    #@223
    move-object/from16 v0, v19

    #@225
    invoke-interface {v0, v14}, Lcom/lge/ims/service/im/IFileTransferListener;->transferProgress(I)V

    #@228
    goto/16 :goto_29

    #@22a
    .line 183
    .end local v14           #nBytesTransferred:I
    :pswitch_22a
    invoke-virtual/range {p1 .. p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@22d
    move-result-object v11

    #@22e
    .line 184
    .local v11, filePath:Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    #@230
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@233
    const-string v20, "FT TRANSFERCOMPLETED / file path = "

    #@235
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@238
    move-result-object v19

    #@239
    move-object/from16 v0, v19

    #@23b
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23e
    move-result-object v19

    #@23f
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@242
    move-result-object v19

    #@243
    invoke-static/range {v19 .. v19}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@246
    .line 185
    move-object/from16 v0, p0

    #@248
    iget-object v0, v0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@24a
    move-object/from16 v19, v0

    #@24c
    move-object/from16 v0, v19

    #@24e
    invoke-interface {v0, v11}, Lcom/lge/ims/service/im/IFileTransferListener;->fileReceived(Ljava/lang/String;)V
    :try_end_251
    .catch Ljava/lang/Exception; {:try_start_76 .. :try_end_251} :catch_33

    #@251
    goto/16 :goto_29

    #@253
    .line 116
    nop

    #@254
    :pswitch_data_254
    .packed-switch 0x29af
        :pswitch_76
        :pswitch_1cb
        :pswitch_201
        :pswitch_22a
        :pswitch_af
    .end packed-switch
.end method

.method final reject()V
    .registers 3

    #@0
    .prologue
    .line 77
    const-string v1, ""

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 79
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@8
    move-result-object v0

    #@9
    .line 81
    .local v0, parcel:Landroid/os/Parcel;
    const/16 v1, 0x29a8

    #@b
    invoke-virtual {v0, v1}, Landroid/os/Parcel;->writeInt(I)V

    #@e
    .line 83
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/im/FileTransferImpl;->sendMessageToJNI(Landroid/os/Parcel;)V

    #@11
    .line 84
    return-void
.end method

.method public abstract sendFile(Lcom/lge/ims/service/im/IMFileInfo;Ljava/lang/String;)V
.end method

.method final sendMessageToJNI(Landroid/os/Parcel;)V
    .registers 4
    .parameter "parcel"

    #@0
    .prologue
    .line 95
    if-eqz p1, :cond_13

    #@2
    .line 96
    invoke-virtual {p1}, Landroid/os/Parcel;->marshall()[B

    #@5
    move-result-object v0

    #@6
    .line 97
    .local v0, baData:[B
    invoke-virtual {p1}, Landroid/os/Parcel;->recycle()V

    #@9
    .line 98
    const/4 p1, 0x0

    #@a
    .line 100
    iget v1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@c
    if-eqz v1, :cond_13

    #@e
    .line 101
    iget v1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->nativeObj:I

    #@10
    invoke-static {v1, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@13
    .line 104
    .end local v0           #baData:[B
    :cond_13
    return-void
.end method

.method final setListener(Lcom/lge/ims/service/im/IFileTransferListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 63
    iput-object p1, p0, Lcom/lge/ims/service/im/FileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@2
    .line 64
    return-void
.end method
