.class final Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilityNotifierHandler;
.super Landroid/os/Handler;
.source "CapabilityNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cd/CapabilityNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CapabilityNotifierHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/cd/CapabilityNotifier;


# direct methods
.method private constructor <init>(Lcom/lge/ims/service/cd/CapabilityNotifier;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 269
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilityNotifierHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/service/cd/CapabilityNotifier;Lcom/lge/ims/service/cd/CapabilityNotifier$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 269
    invoke-direct {p0, p1}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilityNotifierHandler;-><init>(Lcom/lge/ims/service/cd/CapabilityNotifier;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 6
    .parameter "msg"

    #@0
    .prologue
    .line 273
    if-nez p1, :cond_3

    #@2
    .line 300
    :goto_2
    return-void

    #@3
    .line 277
    :cond_3
    const-string v1, "CapNotifier"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "handleMessage :: "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    iget v3, p1, Landroid/os/Message;->what:I

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 279
    iget v1, p1, Landroid/os/Message;->what:I

    #@1f
    const/4 v2, 0x1

    #@20
    if-ne v1, v2, :cond_32

    #@22
    .line 280
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@25
    move-result-object v0

    #@26
    .line 282
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_2b

    #@28
    .line 283
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    #@2b
    .line 285
    :cond_2b
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilityNotifierHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@2d
    const/4 v2, 0x0

    #@2e
    invoke-static {v1, v2}, Lcom/lge/ims/service/cd/CapabilityNotifier;->access$002(Lcom/lge/ims/service/cd/CapabilityNotifier;Landroid/os/Handler;)Landroid/os/Handler;

    #@31
    goto :goto_2

    #@32
    .line 289
    .end local v0           #looper:Landroid/os/Looper;
    :cond_32
    iget v1, p1, Landroid/os/Message;->what:I

    #@34
    packed-switch v1, :pswitch_data_5e

    #@37
    .line 297
    const-string v1, "CapNotifier"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "Unhandled Message :: "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    iget v3, p1, Landroid/os/Message;->what:I

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@51
    goto :goto_2

    #@52
    .line 292
    :pswitch_52
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilityNotifierHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@54
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@56
    check-cast v1, Lcom/lge/ims/service/cd/ServiceCapabilities;

    #@58
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@5a
    invoke-static {v2, v1, v3}, Lcom/lge/ims/service/cd/CapabilityNotifier;->access$100(Lcom/lge/ims/service/cd/CapabilityNotifier;Lcom/lge/ims/service/cd/ServiceCapabilities;I)V

    #@5d
    goto :goto_2

    #@5e
    .line 289
    :pswitch_data_5e
    .packed-switch 0x64
        :pswitch_52
    .end packed-switch
.end method
