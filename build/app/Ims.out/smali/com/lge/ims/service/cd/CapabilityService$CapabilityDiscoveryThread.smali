.class final Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;
.super Ljava/lang/Thread;
.source "CapabilityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cd/CapabilityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CapabilityDiscoveryThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/cd/CapabilityService;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/cd/CapabilityService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 579
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@2
    .line 580
    const-string v0, "CapabilityDiscovery"

    #@4
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@7
    .line 581
    return-void
.end method


# virtual methods
.method public run()V
    .registers 5

    #@0
    .prologue
    .line 584
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@3
    .line 586
    const-string v0, "CapService"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "CapabilityDiscoveryThread is running ... ("

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-static {}, Landroid/os/Process;->myTid()I

    #@13
    move-result v2

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, ")"

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 588
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@27
    new-instance v1, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;

    #@29
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@2b
    const/4 v3, 0x0

    #@2c
    invoke-direct {v1, v2, v3}, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;-><init>(Lcom/lge/ims/service/cd/CapabilityService;Lcom/lge/ims/service/cd/CapabilityService$1;)V

    #@2f
    invoke-static {v0, v1}, Lcom/lge/ims/service/cd/CapabilityService;->access$602(Lcom/lge/ims/service/cd/CapabilityService;Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;)Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;

    #@32
    .line 590
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@35
    .line 591
    return-void
.end method
