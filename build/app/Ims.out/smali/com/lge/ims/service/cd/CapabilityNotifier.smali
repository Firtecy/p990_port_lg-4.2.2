.class public Lcom/lge/ims/service/cd/CapabilityNotifier;
.super Ljava/lang/Object;
.source "CapabilityNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cd/CapabilityNotifier$1;,
        Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;,
        Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilityNotifierThread;,
        Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilityNotifierHandler;
    }
.end annotation


# static fields
.field private static final MSG_EXIT:I = 0x1

.field private static final MSG_UPDATE_CAPABILITIES:I = 0x64

.field private static final TAG:Ljava/lang/String; = "CapNotifier"

.field private static final TEST_MODE:Z


# instance fields
.field private mBroadcastListeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;",
            ">;"
        }
    .end annotation
.end field

.field private mDedicatedListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;",
            ">;>;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field private mThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 26
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mHandler:Landroid/os/Handler;

    #@6
    .line 27
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mThread:Ljava/lang/Thread;

    #@8
    .line 28
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@a
    .line 29
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@c
    .line 34
    new-instance v0, Ljava/util/ArrayList;

    #@e
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@11
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@13
    .line 35
    new-instance v0, Ljava/util/HashMap;

    #@15
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@18
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@1a
    .line 37
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilityNotifierThread;

    #@1c
    invoke-direct {v0, p0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilityNotifierThread;-><init>(Lcom/lge/ims/service/cd/CapabilityNotifier;)V

    #@1f
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mThread:Ljava/lang/Thread;

    #@21
    .line 39
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mThread:Ljava/lang/Thread;

    #@23
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@26
    .line 40
    return-void
.end method

.method static synthetic access$002(Lcom/lge/ims/service/cd/CapabilityNotifier;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 18
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mHandler:Landroid/os/Handler;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Lcom/lge/ims/service/cd/CapabilityNotifier;Lcom/lge/ims/service/cd/ServiceCapabilities;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/service/cd/CapabilityNotifier;->notifyCapabilities(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V

    #@3
    return-void
.end method

.method private hasBroadcastListener()Z
    .registers 4

    #@0
    .prologue
    .line 217
    const/4 v0, 0x0

    #@1
    .line 219
    .local v0, hasListener:Z
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@3
    monitor-enter v2

    #@4
    .line 220
    :try_start_4
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_f

    #@c
    const/4 v0, 0x1

    #@d
    .line 221
    :goto_d
    monitor-exit v2

    #@e
    .line 223
    return v0

    #@f
    .line 220
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_d

    #@11
    .line 221
    :catchall_11
    move-exception v1

    #@12
    monitor-exit v2
    :try_end_13
    .catchall {:try_start_4 .. :try_end_13} :catchall_11

    #@13
    throw v1
.end method

.method private notifyCapabilities(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V
    .registers 10
    .parameter "sc"
    .parameter "resultCode"

    #@0
    .prologue
    .line 227
    if-nez p1, :cond_a

    #@2
    .line 228
    const-string v4, "CapNotifier"

    #@4
    const-string v5, "ServiceCapabilities is null"

    #@6
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 267
    :cond_9
    :goto_9
    return-void

    #@a
    .line 232
    :cond_a
    const-string v4, "CapNotifier"

    #@c
    new-instance v5, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "CapabilityNotifier :: "

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    const-string v6, ", resultCode = "

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 234
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@2e
    monitor-enter v5

    #@2f
    .line 235
    :try_start_2f
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@31
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/ServiceCapabilities;->getPhoneNumber()Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    move-result-object v1

    #@39
    check-cast v1, Ljava/util/ArrayList;

    #@3b
    .line 237
    .local v1, dedicatedContainers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;>;"
    if-eqz v1, :cond_58

    #@3d
    .line 238
    const/4 v2, 0x0

    #@3e
    .local v2, i:I
    :goto_3e
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@41
    move-result v4

    #@42
    if-ge v2, v4, :cond_58

    #@44
    .line 239
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@47
    move-result-object v0

    #@48
    check-cast v0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;

    #@4a
    .line 241
    .local v0, container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    if-eqz v0, :cond_55

    #@4c
    .line 242
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->getListener()Lcom/lge/ims/service/cd/CapabilitiesListener;

    #@4f
    move-result-object v3

    #@50
    .line 244
    .local v3, listener:Lcom/lge/ims/service/cd/CapabilitiesListener;
    if-eqz v3, :cond_55

    #@52
    .line 245
    invoke-interface {v3, p1, p2}, Lcom/lge/ims/service/cd/CapabilitiesListener;->capabilitiesUpdated(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V

    #@55
    .line 238
    .end local v3           #listener:Lcom/lge/ims/service/cd/CapabilitiesListener;
    :cond_55
    add-int/lit8 v2, v2, 0x1

    #@57
    goto :goto_3e

    #@58
    .line 250
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .end local v2           #i:I
    :cond_58
    monitor-exit v5
    :try_end_59
    .catchall {:try_start_2f .. :try_end_59} :catchall_81

    #@59
    .line 252
    invoke-direct {p0}, Lcom/lge/ims/service/cd/CapabilityNotifier;->hasBroadcastListener()Z

    #@5c
    move-result v4

    #@5d
    if-eqz v4, :cond_9

    #@5f
    .line 253
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@61
    monitor-enter v5

    #@62
    .line 254
    const/4 v2, 0x0

    #@63
    .restart local v2       #i:I
    :goto_63
    :try_start_63
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@65
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    #@68
    move-result v4

    #@69
    if-ge v2, v4, :cond_84

    #@6b
    .line 255
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@6d
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@70
    move-result-object v0

    #@71
    check-cast v0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;

    #@73
    .line 257
    .restart local v0       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    if-eqz v0, :cond_7e

    #@75
    .line 258
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->getListener()Lcom/lge/ims/service/cd/CapabilitiesListener;

    #@78
    move-result-object v3

    #@79
    .line 260
    .restart local v3       #listener:Lcom/lge/ims/service/cd/CapabilitiesListener;
    if-eqz v3, :cond_7e

    #@7b
    .line 261
    invoke-interface {v3, p1, p2}, Lcom/lge/ims/service/cd/CapabilitiesListener;->capabilitiesUpdated(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V
    :try_end_7e
    .catchall {:try_start_63 .. :try_end_7e} :catchall_86

    #@7e
    .line 254
    .end local v3           #listener:Lcom/lge/ims/service/cd/CapabilitiesListener;
    :cond_7e
    add-int/lit8 v2, v2, 0x1

    #@80
    goto :goto_63

    #@81
    .line 250
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .end local v1           #dedicatedContainers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;>;"
    .end local v2           #i:I
    :catchall_81
    move-exception v4

    #@82
    :try_start_82
    monitor-exit v5
    :try_end_83
    .catchall {:try_start_82 .. :try_end_83} :catchall_81

    #@83
    throw v4

    #@84
    .line 265
    .restart local v1       #dedicatedContainers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;>;"
    .restart local v2       #i:I
    :cond_84
    :try_start_84
    monitor-exit v5

    #@85
    goto :goto_9

    #@86
    :catchall_86
    move-exception v4

    #@87
    monitor-exit v5
    :try_end_88
    .catchall {:try_start_84 .. :try_end_88} :catchall_86

    #@88
    throw v4
.end method


# virtual methods
.method public addBroadcastListener(Lcom/lge/ims/service/cd/CapabilitiesListener;)V
    .registers 9
    .parameter "listener"

    #@0
    .prologue
    .line 43
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 44
    const/4 v0, 0x0

    #@4
    .line 46
    .local v0, container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    move-object v1, v0

    #@6
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .local v1, container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    :goto_6
    :try_start_6
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@b
    move-result v3

    #@c
    if-ge v2, v3, :cond_53

    #@e
    .line 47
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@10
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    :try_end_16
    .catchall {:try_start_6 .. :try_end_16} :catchall_af

    #@16
    .line 49
    .end local v1           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .restart local v0       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    if-nez v0, :cond_1c

    #@18
    .line 46
    :cond_18
    add-int/lit8 v2, v2, 0x1

    #@1a
    move-object v1, v0

    #@1b
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .restart local v1       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    goto :goto_6

    #@1c
    .line 53
    .end local v1           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .restart local v0       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    :cond_1c
    :try_start_1c
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->getListener()Lcom/lge/ims/service/cd/CapabilitiesListener;

    #@1f
    move-result-object v3

    #@20
    if-ne v3, p1, :cond_18

    #@22
    .line 54
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->addReference()I

    #@25
    .line 56
    const-string v3, "CapNotifier"

    #@27
    new-instance v5, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v6, "Broadcast listener :: add="

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->getRefCount()I

    #@35
    move-result v6

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    const-string v6, ":"

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@42
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@45
    move-result v6

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v5

    #@4e
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@51
    .line 58
    monitor-exit v4
    :try_end_52
    .catchall {:try_start_1c .. :try_end_52} :catchall_8d

    #@52
    .line 71
    :goto_52
    return-void

    #@53
    .line 62
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .restart local v1       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    :cond_53
    :try_start_53
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;

    #@55
    invoke-direct {v0, p0, p1}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;-><init>(Lcom/lge/ims/service/cd/CapabilityNotifier;Lcom/lge/ims/service/cd/CapabilitiesListener;)V
    :try_end_58
    .catchall {:try_start_53 .. :try_end_58} :catchall_af

    #@58
    .line 64
    .end local v1           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .restart local v0       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    if-eqz v0, :cond_90

    #@5a
    .line 65
    :try_start_5a
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@5c
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5f
    .line 66
    const-string v3, "CapNotifier"

    #@61
    new-instance v5, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v6, "Broadcast listener :: add="

    #@68
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->getRefCount()I

    #@6f
    move-result v6

    #@70
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    const-string v6, ":"

    #@76
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@7c
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@7f
    move-result v6

    #@80
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v5

    #@84
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v5

    #@88
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@8b
    .line 70
    :goto_8b
    monitor-exit v4

    #@8c
    goto :goto_52

    #@8d
    :catchall_8d
    move-exception v3

    #@8e
    :goto_8e
    monitor-exit v4
    :try_end_8f
    .catchall {:try_start_5a .. :try_end_8f} :catchall_8d

    #@8f
    throw v3

    #@90
    .line 68
    :cond_90
    :try_start_90
    const-string v3, "CapNotifier"

    #@92
    new-instance v5, Ljava/lang/StringBuilder;

    #@94
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@97
    const-string v6, "Broadcast listener :: add=null:"

    #@99
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v5

    #@9d
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@9f
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@a2
    move-result v6

    #@a3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v5

    #@a7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v5

    #@ab
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_ae
    .catchall {:try_start_90 .. :try_end_ae} :catchall_8d

    #@ae
    goto :goto_8b

    #@af
    .line 70
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .restart local v1       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    :catchall_af
    move-exception v3

    #@b0
    move-object v0, v1

    #@b1
    .end local v1           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .restart local v0       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    goto :goto_8e
.end method

.method public addDedicatedListener(Ljava/lang/String;Lcom/lge/ims/service/cd/CapabilitiesListener;)V
    .registers 10
    .parameter "number"
    .parameter "listener"

    #@0
    .prologue
    .line 74
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@2
    monitor-enter v4

    #@3
    .line 75
    :try_start_3
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@5
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Ljava/util/ArrayList;

    #@b
    .line 77
    .local v1, containerList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;>;"
    if-nez v1, :cond_3d

    #@d
    .line 78
    new-instance v1, Ljava/util/ArrayList;

    #@f
    .end local v1           #containerList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;>;"
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@12
    .line 80
    .restart local v1       #containerList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;>;"
    if-eqz v1, :cond_3b

    #@14
    .line 81
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;

    #@16
    invoke-direct {v0, p0, p2}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;-><init>(Lcom/lge/ims/service/cd/CapabilityNotifier;Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@19
    .line 83
    .local v0, container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    if-eqz v0, :cond_1e

    #@1b
    .line 84
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e
    .line 87
    :cond_1e
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@20
    invoke-virtual {v3, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@23
    .line 89
    const-string v3, "CapNotifier"

    #@25
    new-instance v5, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v6, "Dedicated listener :: add="

    #@2c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 119
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    :cond_3b
    :goto_3b
    monitor-exit v4

    #@3c
    .line 120
    :goto_3c
    return-void

    #@3d
    .line 92
    :cond_3d
    const/4 v0, 0x0

    #@3e
    .line 94
    .restart local v0       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    const/4 v2, 0x0

    #@3f
    .local v2, i:I
    :goto_3f
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@42
    move-result v3

    #@43
    if-ge v2, v3, :cond_94

    #@45
    .line 95
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@48
    move-result-object v0

    #@49
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    check-cast v0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;

    #@4b
    .line 97
    .restart local v0       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    if-nez v0, :cond_50

    #@4d
    .line 94
    :cond_4d
    add-int/lit8 v2, v2, 0x1

    #@4f
    goto :goto_3f

    #@50
    .line 101
    :cond_50
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->getListener()Lcom/lge/ims/service/cd/CapabilitiesListener;

    #@53
    move-result-object v3

    #@54
    if-ne v3, p2, :cond_4d

    #@56
    .line 102
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->addReference()I

    #@59
    .line 104
    const-string v3, "CapNotifier"

    #@5b
    new-instance v5, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v6, "Dedicated listener :: add(r)="

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    const-string v6, ":"

    #@6c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v5

    #@70
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->getRefCount()I

    #@73
    move-result v6

    #@74
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    const-string v6, ":"

    #@7a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v5

    #@7e
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@80
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@83
    move-result v6

    #@84
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@87
    move-result-object v5

    #@88
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@8f
    .line 106
    monitor-exit v4

    #@90
    goto :goto_3c

    #@91
    .line 119
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .end local v1           #containerList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;>;"
    .end local v2           #i:I
    :catchall_91
    move-exception v3

    #@92
    monitor-exit v4
    :try_end_93
    .catchall {:try_start_3 .. :try_end_93} :catchall_91

    #@93
    throw v3

    #@94
    .line 110
    .restart local v0       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .restart local v1       #containerList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;>;"
    .restart local v2       #i:I
    :cond_94
    :try_start_94
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;

    #@96
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    invoke-direct {v0, p0, p2}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;-><init>(Lcom/lge/ims/service/cd/CapabilityNotifier;Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@99
    .line 112
    .restart local v0       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    if-eqz v0, :cond_9e

    #@9b
    .line 113
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@9e
    .line 116
    :cond_9e
    const-string v3, "CapNotifier"

    #@a0
    new-instance v5, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v6, "Dedicated listener :: add(n)="

    #@a7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v5

    #@ab
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v5

    #@af
    const-string v6, ":"

    #@b1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v5

    #@b5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@b8
    move-result v6

    #@b9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v5

    #@bd
    const-string v6, ":"

    #@bf
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v5

    #@c3
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@c5
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@c8
    move-result v6

    #@c9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v5

    #@cd
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v5

    #@d1
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_d4
    .catchall {:try_start_94 .. :try_end_d4} :catchall_91

    #@d4
    goto/16 :goto_3b
.end method

.method public clearAllListeners()V
    .registers 3

    #@0
    .prologue
    .line 178
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v1

    #@3
    .line 179
    :try_start_3
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@5
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@8
    .line 180
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_13

    #@9
    .line 182
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@b
    monitor-enter v1

    #@c
    .line 183
    :try_start_c
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@e
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@11
    .line 184
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_c .. :try_end_12} :catchall_16

    #@12
    .line 185
    return-void

    #@13
    .line 180
    :catchall_13
    move-exception v0

    #@14
    :try_start_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_13

    #@15
    throw v0

    #@16
    .line 184
    :catchall_16
    move-exception v0

    #@17
    :try_start_17
    monitor-exit v1
    :try_end_18
    .catchall {:try_start_17 .. :try_end_18} :catchall_16

    #@18
    throw v0
.end method

.method public hasListener()Z
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 188
    const/4 v0, 0x0

    #@3
    .line 189
    .local v0, hasBroadcastListener:Z
    const/4 v1, 0x0

    #@4
    .line 191
    .local v1, hasDedicatedListener:Z
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@6
    monitor-enter v4

    #@7
    .line 192
    :try_start_7
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    #@c
    move-result v5

    #@d
    if-nez v5, :cond_24

    #@f
    move v0, v3

    #@10
    .line 193
    :goto_10
    monitor-exit v4
    :try_end_11
    .catchall {:try_start_7 .. :try_end_11} :catchall_26

    #@11
    .line 195
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@13
    monitor-enter v4

    #@14
    .line 196
    :try_start_14
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@16
    invoke-virtual {v5}, Ljava/util/HashMap;->isEmpty()Z

    #@19
    move-result v5

    #@1a
    if-nez v5, :cond_29

    #@1c
    move v1, v3

    #@1d
    .line 197
    :goto_1d
    monitor-exit v4
    :try_end_1e
    .catchall {:try_start_14 .. :try_end_1e} :catchall_2b

    #@1e
    .line 199
    if-nez v0, :cond_22

    #@20
    if-eqz v1, :cond_23

    #@22
    :cond_22
    move v2, v3

    #@23
    :cond_23
    return v2

    #@24
    :cond_24
    move v0, v2

    #@25
    .line 192
    goto :goto_10

    #@26
    .line 193
    :catchall_26
    move-exception v2

    #@27
    :try_start_27
    monitor-exit v4
    :try_end_28
    .catchall {:try_start_27 .. :try_end_28} :catchall_26

    #@28
    throw v2

    #@29
    :cond_29
    move v1, v2

    #@2a
    .line 196
    goto :goto_1d

    #@2b
    .line 197
    :catchall_2b
    move-exception v2

    #@2c
    :try_start_2c
    monitor-exit v4
    :try_end_2d
    .catchall {:try_start_2c .. :try_end_2d} :catchall_2b

    #@2d
    throw v2
.end method

.method public removeBroadcastListener(Lcom/lge/ims/service/cd/CapabilitiesListener;)V
    .registers 9
    .parameter "listener"

    #@0
    .prologue
    .line 123
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@2
    monitor-enter v4

    #@3
    .line 124
    const/4 v1, 0x0

    #@4
    .line 126
    .local v1, container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    const/4 v2, 0x0

    #@5
    .local v2, i:I
    :goto_5
    :try_start_5
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@7
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@a
    move-result v3

    #@b
    if-ge v2, v3, :cond_4d

    #@d
    .line 127
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@f
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@12
    move-result-object v3

    #@13
    move-object v0, v3

    #@14
    check-cast v0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;

    #@16
    move-object v1, v0

    #@17
    .line 129
    if-nez v1, :cond_1c

    #@19
    .line 126
    :cond_19
    add-int/lit8 v2, v2, 0x1

    #@1b
    goto :goto_5

    #@1c
    .line 133
    :cond_1c
    invoke-virtual {v1}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->getListener()Lcom/lge/ims/service/cd/CapabilitiesListener;

    #@1f
    move-result-object v3

    #@20
    if-ne v3, p1, :cond_19

    #@22
    .line 134
    invoke-virtual {v1}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->removeReference()I

    #@25
    move-result v3

    #@26
    if-gtz v3, :cond_4b

    #@28
    .line 135
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@2a
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@2d
    .line 137
    const-string v3, "CapNotifier"

    #@2f
    new-instance v5, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v6, "Broadcast listener :: remove="

    #@36
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mBroadcastListeners:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@3f
    move-result v6

    #@40
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v5

    #@48
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 139
    :cond_4b
    monitor-exit v4

    #@4c
    .line 143
    :goto_4c
    return-void

    #@4d
    .line 142
    :cond_4d
    monitor-exit v4

    #@4e
    goto :goto_4c

    #@4f
    :catchall_4f
    move-exception v3

    #@50
    monitor-exit v4
    :try_end_51
    .catchall {:try_start_5 .. :try_end_51} :catchall_4f

    #@51
    throw v3
.end method

.method public removeDedicatedListener(Ljava/lang/String;Lcom/lge/ims/service/cd/CapabilitiesListener;)V
    .registers 10
    .parameter "number"
    .parameter "listener"

    #@0
    .prologue
    .line 146
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@2
    monitor-enter v4

    #@3
    .line 147
    :try_start_3
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@5
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v1

    #@9
    check-cast v1, Ljava/util/ArrayList;

    #@b
    .line 149
    .local v1, containerList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;>;"
    if-eqz v1, :cond_72

    #@d
    .line 150
    const/4 v0, 0x0

    #@e
    .line 152
    .local v0, container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    const/4 v2, 0x0

    #@f
    .local v2, i:I
    :goto_f
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@12
    move-result v3

    #@13
    if-ge v2, v3, :cond_72

    #@15
    .line 153
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    check-cast v0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;

    #@1b
    .line 155
    .restart local v0       #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    if-nez v0, :cond_20

    #@1d
    .line 152
    :cond_1d
    add-int/lit8 v2, v2, 0x1

    #@1f
    goto :goto_f

    #@20
    .line 159
    :cond_20
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->getListener()Lcom/lge/ims/service/cd/CapabilitiesListener;

    #@23
    move-result-object v3

    #@24
    if-ne v3, p2, :cond_1d

    #@26
    .line 160
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->removeReference()I

    #@29
    move-result v3

    #@2a
    if-gtz v3, :cond_70

    #@2c
    .line 161
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@2f
    .line 163
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    #@32
    move-result v3

    #@33
    if-eqz v3, :cond_3a

    #@35
    .line 164
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@37
    invoke-virtual {v3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3a
    .line 167
    :cond_3a
    const-string v3, "CapNotifier"

    #@3c
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "Dedicated listener :: remove="

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    const-string v6, ":"

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@54
    move-result v6

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    const-string v6, ":"

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mDedicatedListeners:Ljava/util/HashMap;

    #@61
    invoke-virtual {v6}, Ljava/util/HashMap;->size()I

    #@64
    move-result v6

    #@65
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v5

    #@69
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v5

    #@6d
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@70
    .line 170
    :cond_70
    monitor-exit v4

    #@71
    .line 175
    .end local v0           #container:Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
    .end local v2           #i:I
    :goto_71
    return-void

    #@72
    .line 174
    :cond_72
    monitor-exit v4

    #@73
    goto :goto_71

    #@74
    .end local v1           #containerList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;>;"
    :catchall_74
    move-exception v3

    #@75
    monitor-exit v4
    :try_end_76
    .catchall {:try_start_3 .. :try_end_76} :catchall_74

    #@76
    throw v3
.end method

.method public updateCapabilities(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V
    .registers 6
    .parameter "sc"
    .parameter "resultCode"

    #@0
    .prologue
    .line 203
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 205
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x64

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 206
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    .line 207
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@c
    .line 209
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mHandler:Landroid/os/Handler;

    #@e
    if-eqz v1, :cond_16

    #@10
    .line 210
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityNotifier;->mHandler:Landroid/os/Handler;

    #@12
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@15
    .line 214
    :goto_15
    return-void

    #@16
    .line 212
    :cond_16
    const-string v1, "CapNotifier"

    #@18
    const-string v2, "CapabilityNotifierHandler is null"

    #@1a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    goto :goto_15
.end method
