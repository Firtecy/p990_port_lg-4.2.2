.class public Lcom/lge/ims/service/ip/XDMConstant;
.super Ljava/lang/Object;
.source "XDMConstant.java"


# static fields
.field public static final INDEX_KEY_LIST_ETAG:I = 0x0

.field public static final INDEX_KEY_PIDF_ETAG:I = 0x3

.field public static final INDEX_KEY_RLS_ETAG:I = 0x1

.field public static final INDEX_KEY_RLS_URI:I = 0x4

.field public static final INDEX_KEY_RULE_ETAG:I = 0x2

.field public static final KEY_LIST_ETAG:Ljava/lang/String; = "list_etag"

.field public static final KEY_PIDF_ETAG:Ljava/lang/String; = "pidf_etag"

.field public static final KEY_RLS_ETAG:Ljava/lang/String; = "rls_etag"

.field public static final KEY_RLS_URI:Ljava/lang/String; = "rls_uri"

.field public static final KEY_RULE_ETAG:Ljava/lang/String; = "rule_etag"

.field public static XDM_CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 21
    const-string v0, "content://com.lge.ims.provider.xdm/ims_xdm"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/ims/service/ip/XDMConstant;->XDM_CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 18
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
