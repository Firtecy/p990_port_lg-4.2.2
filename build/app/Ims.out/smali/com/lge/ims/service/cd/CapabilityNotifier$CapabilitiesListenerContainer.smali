.class final Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;
.super Ljava/lang/Object;
.source "CapabilityNotifier.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cd/CapabilityNotifier;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CapabilitiesListenerContainer"
.end annotation


# instance fields
.field private mListener:Lcom/lge/ims/service/cd/CapabilitiesListener;

.field private mRefCount:I

.field final synthetic this$0:Lcom/lge/ims/service/cd/CapabilityNotifier;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/cd/CapabilityNotifier;Lcom/lge/ims/service/cd/CapabilitiesListener;)V
    .registers 4
    .parameter
    .parameter "listener"

    #@0
    .prologue
    .line 325
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->this$0:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 322
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mListener:Lcom/lge/ims/service/cd/CapabilitiesListener;

    #@8
    .line 323
    const/4 v0, 0x0

    #@9
    iput v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mRefCount:I

    #@b
    .line 326
    iput-object p2, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mListener:Lcom/lge/ims/service/cd/CapabilitiesListener;

    #@d
    .line 327
    const/4 v0, 0x1

    #@e
    iput v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mRefCount:I

    #@10
    .line 328
    return-void
.end method


# virtual methods
.method public addReference()I
    .registers 2

    #@0
    .prologue
    .line 331
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mRefCount:I

    #@2
    add-int/lit8 v0, v0, 0x1

    #@4
    iput v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mRefCount:I

    #@6
    .line 332
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mRefCount:I

    #@8
    return v0
.end method

.method public getListener()Lcom/lge/ims/service/cd/CapabilitiesListener;
    .registers 2

    #@0
    .prologue
    .line 341
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mListener:Lcom/lge/ims/service/cd/CapabilitiesListener;

    #@2
    return-object v0
.end method

.method public getRefCount()I
    .registers 2

    #@0
    .prologue
    .line 345
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mRefCount:I

    #@2
    return v0
.end method

.method public isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 349
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mRefCount:I

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method public removeReference()I
    .registers 2

    #@0
    .prologue
    .line 336
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mRefCount:I

    #@2
    add-int/lit8 v0, v0, -0x1

    #@4
    iput v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mRefCount:I

    #@6
    .line 337
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityNotifier$CapabilitiesListenerContainer;->mRefCount:I

    #@8
    return v0
.end method
