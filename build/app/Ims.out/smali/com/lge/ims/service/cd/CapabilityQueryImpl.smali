.class public Lcom/lge/ims/service/cd/CapabilityQueryImpl;
.super Lcom/lge/ims/service/cd/ICapabilityQuery$Stub;
.source "CapabilityQueryImpl.java"

# interfaces
.implements Lcom/lge/ims/service/cd/CapabilityQueryListener;


# instance fields
.field private mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

.field private mListener:Lcom/lge/ims/service/cd/ICapabilityQueryListener;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/cd/CapabilityQuery;)V
    .registers 3
    .parameter "capQuery"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 15
    invoke-direct {p0}, Lcom/lge/ims/service/cd/ICapabilityQuery$Stub;-><init>()V

    #@4
    .line 10
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQueryImpl;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@6
    .line 11
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQueryImpl;->mListener:Lcom/lge/ims/service/cd/ICapabilityQueryListener;

    #@8
    .line 16
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityQueryImpl;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@a
    .line 18
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQueryImpl;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 19
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQueryImpl;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@10
    invoke-virtual {v0, p0}, Lcom/lge/ims/service/cd/CapabilityQuery;->setListener(Lcom/lge/ims/service/cd/CapabilityQueryListener;)V

    #@13
    .line 21
    :cond_13
    return-void
.end method


# virtual methods
.method public capabilityQueryDelivered(Lcom/lge/ims/service/cd/CapabilityQuery;Lcom/lge/ims/service/cd/Capabilities;I)V
    .registers 5
    .parameter "cq"
    .parameter "capabilities"
    .parameter "resultCode"

    #@0
    .prologue
    .line 38
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQueryImpl;->mListener:Lcom/lge/ims/service/cd/ICapabilityQueryListener;

    #@2
    if-nez v0, :cond_8

    #@4
    .line 39
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/CapabilityQuery;->close()V

    #@7
    .line 52
    :cond_7
    :goto_7
    return-void

    #@8
    .line 44
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQueryImpl;->mListener:Lcom/lge/ims/service/cd/ICapabilityQueryListener;

    #@a
    invoke-interface {v0, p0, p2, p3}, Lcom/lge/ims/service/cd/ICapabilityQueryListener;->capabilityQueryDelivered(Lcom/lge/ims/service/cd/ICapabilityQuery;Lcom/lge/ims/service/cd/Capabilities;I)V
    :try_end_d
    .catchall {:try_start_8 .. :try_end_d} :catchall_1a
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_d} :catch_13

    #@d
    .line 48
    if-eqz p1, :cond_7

    #@f
    .line 49
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/CapabilityQuery;->close()V

    #@12
    goto :goto_7

    #@13
    .line 45
    :catch_13
    move-exception v0

    #@14
    .line 48
    if-eqz p1, :cond_7

    #@16
    .line 49
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/CapabilityQuery;->close()V

    #@19
    goto :goto_7

    #@1a
    .line 48
    :catchall_1a
    move-exception v0

    #@1b
    if-eqz p1, :cond_20

    #@1d
    .line 49
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/CapabilityQuery;->close()V

    #@20
    :cond_20
    throw v0
.end method

.method public send(Ljava/lang/String;)Z
    .registers 3
    .parameter "target"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 25
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQueryImpl;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 26
    const/4 v0, 0x0

    #@5
    .line 28
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityQueryImpl;->mCapQuery:Lcom/lge/ims/service/cd/CapabilityQuery;

    #@8
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/cd/CapabilityQuery;->send(Ljava/lang/String;)Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public setListener(Lcom/lge/ims/service/cd/ICapabilityQueryListener;)V
    .registers 2
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 33
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityQueryImpl;->mListener:Lcom/lge/ims/service/cd/ICapabilityQueryListener;

    #@2
    .line 34
    return-void
.end method
