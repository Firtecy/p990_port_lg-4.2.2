.class public final Lcom/lge/ims/service/ip/IPContactHelper;
.super Ljava/lang/Object;
.source "IPContactHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/ip/IPContactHelper$1;,
        Lcom/lge/ims/service/ip/IPContactHelper$IPBuddyImageData;,
        Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;,
        Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;,
        Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    }
.end annotation


# static fields
.field private static final INVALID_ID:I = -0x1

.field static final IP_BUDDY_IMAGE_PROJECTION:[Ljava/lang/String;

.field static final IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

.field static final IP_FULL_MYSTATUS_PROJECTION:[Ljava/lang/String;

.field static final IP_FULL_PRESENCE_PROJECTION:[Ljava/lang/String;

.field static final IP_PROVISIONING_PROJECTION:[Ljava/lang/String;

.field static final XDM_FULL_ETAG_PROJECTION:[Ljava/lang/String;

.field private static bDescending:Z

.field private static isExcessMaxUserSize:Z

.field private static isSynchronizing:Z

.field private static isToSync:Z

.field private static nLimitationResourceListUserCount:I

.field private static nPollingLatestTime:J

.field private static nSubRLSPollingLatestTime:J

.field private static objInstance:Lcom/lge/ims/service/ip/IPContactHelper;


# instance fields
.field private nMaxSizeKeepingFullImage:I

.field private objContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 57
    const/4 v0, 0x0

    #@6
    sput-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->objInstance:Lcom/lge/ims/service/ip/IPContactHelper;

    #@8
    .line 67
    sput v3, Lcom/lge/ims/service/ip/IPContactHelper;->nLimitationResourceListUserCount:I

    #@a
    .line 68
    const-wide/16 v0, 0x0

    #@c
    sput-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J

    #@e
    .line 69
    const-wide/16 v0, 0x0

    #@10
    sput-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nSubRLSPollingLatestTime:J

    #@12
    .line 72
    sput-boolean v3, Lcom/lge/ims/service/ip/IPContactHelper;->bDescending:Z

    #@14
    .line 74
    sput-boolean v3, Lcom/lge/ims/service/ip/IPContactHelper;->isExcessMaxUserSize:Z

    #@16
    .line 150
    const/16 v0, 0x15

    #@18
    new-array v0, v0, [Ljava/lang/String;

    #@1a
    const-string v1, "_id"

    #@1c
    aput-object v1, v0, v3

    #@1e
    const-string v1, "data_id"

    #@20
    aput-object v1, v0, v4

    #@22
    const-string v1, "msisdn"

    #@24
    aput-object v1, v0, v5

    #@26
    const-string v1, "nor_msisdn"

    #@28
    aput-object v1, v0, v6

    #@2a
    const-string v1, "rcs_status"

    #@2c
    aput-object v1, v0, v7

    #@2e
    const/4 v1, 0x5

    #@2f
    const-string v2, "im_status"

    #@31
    aput-object v2, v0, v1

    #@33
    const/4 v1, 0x6

    #@34
    const-string v2, "ft_status"

    #@36
    aput-object v2, v0, v1

    #@38
    const/4 v1, 0x7

    #@39
    const-string v2, "time_stamp"

    #@3b
    aput-object v2, v0, v1

    #@3d
    const/16 v1, 0x8

    #@3f
    const-string v2, "capa_type"

    #@41
    aput-object v2, v0, v1

    #@43
    const/16 v1, 0x9

    #@45
    const-string v2, "first_time_rcs"

    #@47
    aput-object v2, v0, v1

    #@49
    const/16 v1, 0xa

    #@4b
    const-string v2, "mim_status"

    #@4d
    aput-object v2, v0, v1

    #@4f
    const/16 v1, 0xb

    #@51
    const-string v2, "presence_status"

    #@53
    aput-object v2, v0, v1

    #@55
    const/16 v1, 0xc

    #@57
    const-string v2, "uri_id"

    #@59
    aput-object v2, v0, v1

    #@5b
    const/16 v1, 0xd

    #@5d
    const-string v2, "http_status"

    #@5f
    aput-object v2, v0, v1

    #@61
    const/16 v1, 0xe

    #@63
    const-string v2, "response_code"

    #@65
    aput-object v2, v0, v1

    #@67
    const/16 v1, 0xf

    #@69
    const-string v2, "rcs_user_list"

    #@6b
    aput-object v2, v0, v1

    #@6d
    const/16 v1, 0x10

    #@6f
    const-string v2, "nonrcs_user_list"

    #@71
    aput-object v2, v0, v1

    #@73
    const/16 v1, 0x11

    #@75
    const-string v2, "image_uri_id"

    #@77
    aput-object v2, v0, v1

    #@79
    const/16 v1, 0x12

    #@7b
    const-string v2, "raw_contact_id"

    #@7d
    aput-object v2, v0, v1

    #@7f
    const/16 v1, 0x13

    #@81
    const-string v2, "photo_update"

    #@83
    aput-object v2, v0, v1

    #@85
    const/16 v1, 0x14

    #@87
    const-string v2, "display_name"

    #@89
    aput-object v2, v0, v1

    #@8b
    sput-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@8d
    .line 174
    const/16 v0, 0x12

    #@8f
    new-array v0, v0, [Ljava/lang/String;

    #@91
    const-string v1, "_id"

    #@93
    aput-object v1, v0, v3

    #@95
    const-string v1, "nor_msisdn"

    #@97
    aput-object v1, v0, v4

    #@99
    const-string v1, "homepage"

    #@9b
    aput-object v1, v0, v5

    #@9d
    const-string v1, "free_text"

    #@9f
    aput-object v1, v0, v6

    #@a1
    const-string v1, "e_mail"

    #@a3
    aput-object v1, v0, v7

    #@a5
    const/4 v1, 0x5

    #@a6
    const-string v2, "birthday"

    #@a8
    aput-object v2, v0, v1

    #@aa
    const/4 v1, 0x6

    #@ab
    const-string v2, "status"

    #@ad
    aput-object v2, v0, v1

    #@af
    const/4 v1, 0x7

    #@b0
    const-string v2, "twitter_account"

    #@b2
    aput-object v2, v0, v1

    #@b4
    const/16 v1, 0x8

    #@b6
    const-string v2, "facebook_account"

    #@b8
    aput-object v2, v0, v1

    #@ba
    const/16 v1, 0x9

    #@bc
    const-string v2, "cyworld_account"

    #@be
    aput-object v2, v0, v1

    #@c0
    const/16 v1, 0xa

    #@c2
    const-string v2, "waggle_account"

    #@c4
    aput-object v2, v0, v1

    #@c6
    const/16 v1, 0xb

    #@c8
    const-string v2, "status_icon"

    #@ca
    aput-object v2, v0, v1

    #@cc
    const/16 v1, 0xc

    #@ce
    const-string v2, "status_icon_link"

    #@d0
    aput-object v2, v0, v1

    #@d2
    const/16 v1, 0xd

    #@d4
    const-string v2, "status_icon_thumb"

    #@d6
    aput-object v2, v0, v1

    #@d8
    const/16 v1, 0xe

    #@da
    const-string v2, "status_icon_thumb_link"

    #@dc
    aput-object v2, v0, v1

    #@de
    const/16 v1, 0xf

    #@e0
    const-string v2, "status_icon_thumb_etag"

    #@e2
    aput-object v2, v0, v1

    #@e4
    const/16 v1, 0x10

    #@e6
    const-string v2, "time_stamp"

    #@e8
    aput-object v2, v0, v1

    #@ea
    const/16 v1, 0x11

    #@ec
    const-string v2, "statusicon_update"

    #@ee
    aput-object v2, v0, v1

    #@f0
    sput-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_PRESENCE_PROJECTION:[Ljava/lang/String;

    #@f2
    .line 194
    const/16 v0, 0xf

    #@f4
    new-array v0, v0, [Ljava/lang/String;

    #@f6
    const-string v1, "homepage"

    #@f8
    aput-object v1, v0, v3

    #@fa
    const-string v1, "free_text"

    #@fc
    aput-object v1, v0, v4

    #@fe
    const-string v1, "e_mail"

    #@100
    aput-object v1, v0, v5

    #@102
    const-string v1, "birthday"

    #@104
    aput-object v1, v0, v6

    #@106
    const-string v1, "status"

    #@108
    aput-object v1, v0, v7

    #@10a
    const/4 v1, 0x5

    #@10b
    const-string v2, "twitter_account"

    #@10d
    aput-object v2, v0, v1

    #@10f
    const/4 v1, 0x6

    #@110
    const-string v2, "facebook_account"

    #@112
    aput-object v2, v0, v1

    #@114
    const/4 v1, 0x7

    #@115
    const-string v2, "cyworld_account"

    #@117
    aput-object v2, v0, v1

    #@119
    const/16 v1, 0x8

    #@11b
    const-string v2, "waggle_account"

    #@11d
    aput-object v2, v0, v1

    #@11f
    const/16 v1, 0x9

    #@121
    const-string v2, "status_icon"

    #@123
    aput-object v2, v0, v1

    #@125
    const/16 v1, 0xa

    #@127
    const-string v2, "status_icon_link"

    #@129
    aput-object v2, v0, v1

    #@12b
    const/16 v1, 0xb

    #@12d
    const-string v2, "status_icon_thumb"

    #@12f
    aput-object v2, v0, v1

    #@131
    const/16 v1, 0xc

    #@133
    const-string v2, "status_icon_thumb_link"

    #@135
    aput-object v2, v0, v1

    #@137
    const/16 v1, 0xd

    #@139
    const-string v2, "status_icon_thumb_etag"

    #@13b
    aput-object v2, v0, v1

    #@13d
    const/16 v1, 0xe

    #@13f
    const-string v2, "display_name"

    #@141
    aput-object v2, v0, v1

    #@143
    sput-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_MYSTATUS_PROJECTION:[Ljava/lang/String;

    #@145
    .line 211
    const/4 v0, 0x5

    #@146
    new-array v0, v0, [Ljava/lang/String;

    #@148
    const-string v1, "list_etag"

    #@14a
    aput-object v1, v0, v3

    #@14c
    const-string v1, "rls_etag"

    #@14e
    aput-object v1, v0, v4

    #@150
    const-string v1, "rule_etag"

    #@152
    aput-object v1, v0, v5

    #@154
    const-string v1, "pidf_etag"

    #@156
    aput-object v1, v0, v6

    #@158
    const-string v1, "rls_uri"

    #@15a
    aput-object v1, v0, v7

    #@15c
    sput-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->XDM_FULL_ETAG_PROJECTION:[Ljava/lang/String;

    #@15e
    .line 218
    new-array v0, v7, [Ljava/lang/String;

    #@160
    const-string v1, "_id"

    #@162
    aput-object v1, v0, v3

    #@164
    const-string v1, "nor_msisdn"

    #@166
    aput-object v1, v0, v4

    #@168
    const-string v1, "file_path"

    #@16a
    aput-object v1, v0, v5

    #@16c
    const-string v1, "time_stamp"

    #@16e
    aput-object v1, v0, v6

    #@170
    sput-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->IP_BUDDY_IMAGE_PROJECTION:[Ljava/lang/String;

    #@172
    .line 224
    new-array v0, v6, [Ljava/lang/String;

    #@174
    const-string v1, "latest_polling"

    #@176
    aput-object v1, v0, v3

    #@178
    const-string v1, "latest_rlssub_polling"

    #@17a
    aput-object v1, v0, v4

    #@17c
    const-string v1, "excess_max_user"

    #@17e
    aput-object v1, v0, v5

    #@180
    sput-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->IP_PROVISIONING_PROJECTION:[Ljava/lang/String;

    #@182
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "_objContext"

    #@0
    .prologue
    .line 247
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 70
    const/16 v0, 0xc8

    #@5
    iput v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->nMaxSizeKeepingFullImage:I

    #@7
    .line 248
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@9
    .line 250
    const/4 v0, 0x0

    #@a
    sput-boolean v0, Lcom/lge/ims/service/ip/IPContactHelper;->isSynchronizing:Z

    #@c
    .line 251
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPContactHelper;->IsExceedMaxUserSize()Z

    #@f
    move-result v0

    #@10
    sput-boolean v0, Lcom/lge/ims/service/ip/IPContactHelper;->isExcessMaxUserSize:Z

    #@12
    .line 252
    return-void
.end method

.method private BuddyImageDB_CheckSameUser(Lcom/lge/ims/service/ip/PresenceInfo;)Z
    .registers 11
    .parameter "objPresenceInfo"

    #@0
    .prologue
    .line 3624
    const-string v3, "nor_msisdn=?"

    #@2
    .line 3625
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@3
    new-array v4, v0, [Ljava/lang/String;

    #@5
    const/4 v0, 0x0

    #@6
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    aput-object v1, v4, v0

    #@c
    .line 3626
    .local v4, astrSelectionArg:[Ljava/lang/String;
    const/4 v8, 0x0

    #@d
    .line 3627
    .local v8, objCursor:Landroid/database/Cursor;
    new-instance v0, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v1, "[IP]msisdn : "

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v0

    #@24
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@27
    .line 3628
    const/4 v6, 0x0

    #@28
    .line 3630
    .local v6, bHasSameUser:Z
    :try_start_28
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2d
    move-result-object v0

    #@2e
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_BUDDY_IMAGE_CONTENT_URI:Landroid/net/Uri;

    #@30
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_BUDDY_IMAGE_PROJECTION:[Ljava/lang/String;

    #@32
    const/4 v5, 0x0

    #@33
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_36
    .catchall {:try_start_28 .. :try_end_36} :catchall_69
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_36} :catch_4b

    #@36
    move-result-object v8

    #@37
    .line 3632
    if-nez v8, :cond_41

    #@39
    .line 3633
    const/4 v6, 0x0

    #@3a
    .line 3642
    :goto_3a
    if-eqz v8, :cond_40

    #@3c
    .line 3643
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@3f
    .line 3644
    const/4 v8, 0x0

    #@40
    .line 3647
    :cond_40
    :goto_40
    return v6

    #@41
    .line 3634
    :cond_41
    :try_start_41
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_44
    .catchall {:try_start_41 .. :try_end_44} :catchall_69
    .catch Ljava/lang/Exception; {:try_start_41 .. :try_end_44} :catch_4b

    #@44
    move-result v0

    #@45
    if-nez v0, :cond_49

    #@47
    .line 3635
    const/4 v6, 0x0

    #@48
    goto :goto_3a

    #@49
    .line 3637
    :cond_49
    const/4 v6, 0x1

    #@4a
    goto :goto_3a

    #@4b
    .line 3639
    :catch_4b
    move-exception v7

    #@4c
    .line 3640
    .local v7, e:Ljava/lang/Exception;
    :try_start_4c
    new-instance v0, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v1, "[IP][ERROR]exception : "

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_62
    .catchall {:try_start_4c .. :try_end_62} :catchall_69

    #@62
    .line 3642
    if-eqz v8, :cond_40

    #@64
    .line 3643
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@67
    .line 3644
    const/4 v8, 0x0

    #@68
    goto :goto_40

    #@69
    .line 3642
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_69
    move-exception v0

    #@6a
    if-eqz v8, :cond_70

    #@6c
    .line 3643
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@6f
    .line 3644
    const/4 v8, 0x0

    #@70
    :cond_70
    throw v0
.end method

.method private BuddyImageDB_Delete(Ljava/lang/String;)Z
    .registers 9
    .parameter "strMSISDN"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 3677
    if-eqz p1, :cond_a

    #@4
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@7
    move-result v5

    #@8
    if-eqz v5, :cond_11

    #@a
    .line 3678
    :cond_a
    const-string v3, "[IP][ERROR]strMSISDN is not valid"

    #@c
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@f
    move v3, v4

    #@10
    .line 3685
    :cond_10
    :goto_10
    return v3

    #@11
    .line 3681
    :cond_11
    const-string v2, "nor_msisdn=?"

    #@13
    .line 3682
    .local v2, strSelection:Ljava/lang/String;
    new-array v0, v3, [Ljava/lang/String;

    #@15
    aput-object p1, v0, v4

    #@17
    .line 3683
    .local v0, astrSelectionArg:[Ljava/lang/String;
    iget-object v5, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@19
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1c
    move-result-object v5

    #@1d
    sget-object v6, Lcom/lge/ims/service/ip/IPConstant;->IP_BUDDY_IMAGE_CONTENT_URI:Landroid/net/Uri;

    #@1f
    invoke-virtual {v5, v6, v2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@22
    move-result v1

    #@23
    .line 3685
    .local v1, delete:I
    const/4 v5, -0x1

    #@24
    if-ne v1, v5, :cond_10

    #@26
    move v3, v4

    #@27
    goto :goto_10
.end method

.method private BuddyImageDB_Insert(Lcom/lge/ims/service/ip/PresenceInfo;)Z
    .registers 12
    .parameter "objPresenceInfo"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 3650
    new-instance v1, Landroid/content/ContentValues;

    #@4
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@7
    .line 3651
    .local v1, objContentValues:Landroid/content/ContentValues;
    const-string v3, "nor_msisdn=?"

    #@9
    .line 3652
    .local v3, strSelection:Ljava/lang/String;
    new-array v0, v6, [Ljava/lang/String;

    #@b
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@e
    move-result-object v7

    #@f
    aput-object v7, v0, v5

    #@11
    .line 3653
    .local v0, astrSelectionArg:[Ljava/lang/String;
    const-string v7, "nor_msisdn"

    #@13
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@16
    move-result-object v8

    #@17
    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    .line 3654
    const-string v7, "file_path"

    #@1c
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIcon()Ljava/lang/String;

    #@1f
    move-result-object v8

    #@20
    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 3655
    const-string v7, "time_stamp"

    #@25
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@28
    move-result-wide v8

    #@29
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2c
    move-result-object v8

    #@2d
    invoke-virtual {v1, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@30
    .line 3656
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->BuddyImageDB_CheckSameUser(Lcom/lge/ims/service/ip/PresenceInfo;)Z

    #@33
    move-result v7

    #@34
    if-ne v7, v6, :cond_50

    #@36
    .line 3657
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@38
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3b
    move-result-object v7

    #@3c
    sget-object v8, Lcom/lge/ims/service/ip/IPConstant;->IP_BUDDY_IMAGE_CONTENT_URI:Landroid/net/Uri;

    #@3e
    invoke-virtual {v7, v8, v1, v3, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@41
    move-result v4

    #@42
    .line 3658
    .local v4, update:I
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@45
    .line 3659
    const/4 v7, -0x1

    #@46
    if-ne v4, v7, :cond_4e

    #@48
    .line 3660
    const-string v6, "[IP][ERROR]BuddyImageDB_Insert : buddy image DB update failed"

    #@4a
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@4d
    .line 3672
    .end local v4           #update:I
    :goto_4d
    return v5

    #@4e
    .restart local v4       #update:I
    :cond_4e
    move v5, v6

    #@4f
    .line 3663
    goto :goto_4d

    #@50
    .line 3666
    .end local v4           #update:I
    :cond_50
    iget-object v7, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@52
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@55
    move-result-object v7

    #@56
    sget-object v8, Lcom/lge/ims/service/ip/IPConstant;->IP_BUDDY_IMAGE_CONTENT_URI:Landroid/net/Uri;

    #@58
    invoke-virtual {v7, v8, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@5b
    move-result-object v2

    #@5c
    .line 3667
    .local v2, objUri:Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@5f
    .line 3668
    if-nez v2, :cond_67

    #@61
    .line 3669
    const-string v6, "[IP][ERROR]BuddyImageDB_Insert : buddy image DB insert failed"

    #@63
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@66
    goto :goto_4d

    #@67
    :cond_67
    move v5, v6

    #@68
    .line 3672
    goto :goto_4d
.end method

.method private BuddyImageDB_Update(Lcom/lge/ims/service/ip/PresenceInfo;)Z
    .registers 16
    .parameter "objPresenceInfo"

    #@0
    .prologue
    .line 3550
    const/4 v9, 0x0

    #@1
    .line 3552
    .local v9, objCursor:Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_BUDDY_IMAGE_CONTENT_URI:Landroid/net/Uri;

    #@9
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_BUDDY_IMAGE_PROJECTION:[Ljava/lang/String;

    #@b
    const/4 v3, 0x0

    #@c
    const/4 v4, 0x0

    #@d
    const/4 v5, 0x0

    #@e
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@11
    move-result-object v9

    #@12
    .line 3553
    if-nez v9, :cond_21

    #@14
    .line 3554
    const-string v0, "[IP][ERROR] BuddyImageDB_Update : the result is null"

    #@16
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_19
    .catchall {:try_start_1 .. :try_end_19} :catchall_19d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_19} :catch_17b

    #@19
    .line 3555
    const/4 v0, 0x0

    #@1a
    .line 3616
    if-eqz v9, :cond_20

    #@1c
    .line 3617
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@1f
    .line 3618
    const/4 v9, 0x0

    #@20
    .line 3621
    :cond_20
    :goto_20
    return v0

    #@21
    .line 3556
    :cond_21
    :try_start_21
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    #@24
    move-result v0

    #@25
    iget v1, p0, Lcom/lge/ims/service/ip/IPContactHelper;->nMaxSizeKeepingFullImage:I

    #@27
    if-lt v0, v1, :cond_199

    #@29
    .line 3557
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->BuddyImageDB_CheckSameUser(Lcom/lge/ims/service/ip/PresenceInfo;)Z

    #@2c
    move-result v0

    #@2d
    const/4 v1, 0x1

    #@2e
    if-ne v0, v1, :cond_87

    #@30
    .line 3558
    new-instance v8, Landroid/content/ContentValues;

    #@32
    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    #@35
    .line 3559
    .local v8, objContentValues:Landroid/content/ContentValues;
    const-string v11, "nor_msisdn=?"

    #@37
    .line 3560
    .local v11, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@38
    new-array v6, v0, [Ljava/lang/String;

    #@3a
    const/4 v0, 0x0

    #@3b
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    aput-object v1, v6, v0

    #@41
    .line 3561
    .local v6, astrSelectionArg:[Ljava/lang/String;
    const-string v0, "nor_msisdn"

    #@43
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    .line 3562
    const-string v0, "file_path"

    #@4c
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIcon()Ljava/lang/String;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    .line 3563
    const-string v0, "time_stamp"

    #@55
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@58
    move-result-wide v1

    #@59
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5c
    move-result-object v1

    #@5d
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@60
    .line 3565
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@62
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@65
    move-result-object v0

    #@66
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_BUDDY_IMAGE_CONTENT_URI:Landroid/net/Uri;

    #@68
    invoke-virtual {v0, v1, v8, v11, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@6b
    move-result v13

    #@6c
    .line 3566
    .local v13, update:I
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    #@6f
    .line 3567
    const/4 v0, -0x1

    #@70
    if-ne v13, v0, :cond_7f

    #@72
    .line 3568
    const-string v0, "[IP][ERROR]BuddyImageDB_Update : buddy image DB insert failed"

    #@74
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_77
    .catchall {:try_start_21 .. :try_end_77} :catchall_19d
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_77} :catch_17b

    #@77
    .line 3569
    const/4 v0, 0x0

    #@78
    .line 3616
    if-eqz v9, :cond_20

    #@7a
    .line 3617
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@7d
    .line 3618
    const/4 v9, 0x0

    #@7e
    goto :goto_20

    #@7f
    .line 3571
    :cond_7f
    const/4 v0, 0x1

    #@80
    .line 3616
    if-eqz v9, :cond_20

    #@82
    .line 3617
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@85
    .line 3618
    const/4 v9, 0x0

    #@86
    goto :goto_20

    #@87
    .line 3574
    .end local v6           #astrSelectionArg:[Ljava/lang/String;
    .end local v8           #objContentValues:Landroid/content/ContentValues;
    .end local v11           #strSelection:Ljava/lang/String;
    .end local v13           #update:I
    :cond_87
    const/4 v10, 0x0

    #@88
    .line 3576
    .local v10, objTempCursor:Landroid/database/Cursor;
    :try_start_88
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@8a
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8d
    move-result-object v0

    #@8e
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_BUDDY_IMAGE_CONTENT_URI:Landroid/net/Uri;

    #@90
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_BUDDY_IMAGE_PROJECTION:[Ljava/lang/String;

    #@92
    const/4 v3, 0x0

    #@93
    const/4 v4, 0x0

    #@94
    const-string v5, "time_stamp asc"

    #@96
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@99
    move-result-object v10

    #@9a
    .line 3577
    if-nez v10, :cond_b0

    #@9c
    .line 3578
    const-string v0, "[IP][ERROR] BuddyImageDB_Update : the result is null"

    #@9e
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_a1
    .catchall {:try_start_88 .. :try_end_a1} :catchall_173
    .catch Ljava/lang/Exception; {:try_start_88 .. :try_end_a1} :catch_14d

    #@a1
    .line 3579
    const/4 v0, 0x0

    #@a2
    .line 3604
    if-eqz v10, :cond_a8

    #@a4
    .line 3605
    :try_start_a4
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_a7
    .catchall {:try_start_a4 .. :try_end_a7} :catchall_19d
    .catch Ljava/lang/Exception; {:try_start_a4 .. :try_end_a7} :catch_17b

    #@a7
    .line 3606
    const/4 v10, 0x0

    #@a8
    .line 3616
    :cond_a8
    if-eqz v9, :cond_20

    #@aa
    .line 3617
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@ad
    .line 3618
    const/4 v9, 0x0

    #@ae
    goto/16 :goto_20

    #@b0
    .line 3580
    :cond_b0
    :try_start_b0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@b3
    move-result v0

    #@b4
    if-nez v0, :cond_ca

    #@b6
    .line 3581
    const-string v0, "[IP][ERROR]BuddyImageDB_Update : cursor move to next is false"

    #@b8
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_bb
    .catchall {:try_start_b0 .. :try_end_bb} :catchall_173
    .catch Ljava/lang/Exception; {:try_start_b0 .. :try_end_bb} :catch_14d

    #@bb
    .line 3582
    const/4 v0, 0x0

    #@bc
    .line 3604
    if-eqz v10, :cond_c2

    #@be
    .line 3605
    :try_start_be
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_c1
    .catchall {:try_start_be .. :try_end_c1} :catchall_19d
    .catch Ljava/lang/Exception; {:try_start_be .. :try_end_c1} :catch_17b

    #@c1
    .line 3606
    const/4 v10, 0x0

    #@c2
    .line 3616
    :cond_c2
    if-eqz v9, :cond_20

    #@c4
    .line 3617
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@c7
    .line 3618
    const/4 v9, 0x0

    #@c8
    goto/16 :goto_20

    #@ca
    .line 3584
    :cond_ca
    :try_start_ca
    new-instance v0, Ljava/lang/StringBuilder;

    #@cc
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@cf
    const-string v1, "[IP]file delete [ "

    #@d1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v0

    #@d5
    const/4 v1, 0x2

    #@d6
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@d9
    move-result-object v1

    #@da
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v0

    #@de
    const-string v1, " ]"

    #@e0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v0

    #@e4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e7
    move-result-object v0

    #@e8
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@eb
    .line 3585
    new-instance v12, Ljava/io/File;

    #@ed
    const/4 v0, 0x2

    #@ee
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@f1
    move-result-object v0

    #@f2
    invoke-direct {v12, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@f5
    .line 3586
    .local v12, tempFile:Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->delete()Z

    #@f8
    .line 3588
    const-string v11, "nor_msisdn=?"

    #@fa
    .line 3589
    .restart local v11       #strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@fb
    new-array v6, v0, [Ljava/lang/String;

    #@fd
    const/4 v0, 0x0

    #@fe
    const/4 v1, 0x1

    #@ff
    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@102
    move-result-object v1

    #@103
    aput-object v1, v6, v0

    #@105
    .line 3590
    .restart local v6       #astrSelectionArg:[Ljava/lang/String;
    new-instance v8, Landroid/content/ContentValues;

    #@107
    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    #@10a
    .line 3591
    .restart local v8       #objContentValues:Landroid/content/ContentValues;
    const-string v1, "status_icon_thumb_etag"

    #@10c
    const/4 v0, 0x0

    #@10d
    check-cast v0, Ljava/lang/String;

    #@10f
    invoke-virtual {v8, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@112
    .line 3592
    const-string v1, "status_icon"

    #@114
    const/4 v0, 0x0

    #@115
    check-cast v0, Ljava/lang/String;

    #@117
    invoke-virtual {v8, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@11a
    .line 3593
    const-string v0, "statusicon_update"

    #@11c
    const/4 v1, 0x0

    #@11d
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@120
    move-result-object v1

    #@121
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@124
    .line 3594
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@126
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@129
    move-result-object v0

    #@12a
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@12c
    invoke-virtual {v0, v1, v8, v11, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@12f
    move-result v13

    #@130
    .line 3595
    .restart local v13       #update:I
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V

    #@133
    .line 3597
    const/4 v0, 0x1

    #@134
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@137
    move-result-object v0

    #@138
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPContactHelper;->BuddyImageDB_Delete(Ljava/lang/String;)Z

    #@13b
    .line 3598
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->BuddyImageDB_Insert(Lcom/lge/ims/service/ip/PresenceInfo;)Z
    :try_end_13e
    .catchall {:try_start_ca .. :try_end_13e} :catchall_173
    .catch Ljava/lang/Exception; {:try_start_ca .. :try_end_13e} :catch_14d

    #@13e
    .line 3599
    const/4 v0, 0x1

    #@13f
    .line 3604
    if-eqz v10, :cond_145

    #@141
    .line 3605
    :try_start_141
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_144
    .catchall {:try_start_141 .. :try_end_144} :catchall_19d
    .catch Ljava/lang/Exception; {:try_start_141 .. :try_end_144} :catch_17b

    #@144
    .line 3606
    const/4 v10, 0x0

    #@145
    .line 3616
    :cond_145
    if-eqz v9, :cond_20

    #@147
    .line 3617
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@14a
    .line 3618
    const/4 v9, 0x0

    #@14b
    goto/16 :goto_20

    #@14d
    .line 3601
    .end local v6           #astrSelectionArg:[Ljava/lang/String;
    .end local v8           #objContentValues:Landroid/content/ContentValues;
    .end local v11           #strSelection:Ljava/lang/String;
    .end local v12           #tempFile:Ljava/io/File;
    .end local v13           #update:I
    :catch_14d
    move-exception v7

    #@14e
    .line 3602
    .local v7, e:Ljava/lang/Exception;
    :try_start_14e
    new-instance v0, Ljava/lang/StringBuilder;

    #@150
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@153
    const-string v1, "[IP][ERROR]exception : "

    #@155
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v0

    #@159
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v0

    #@15d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v0

    #@161
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_164
    .catchall {:try_start_14e .. :try_end_164} :catchall_173

    #@164
    .line 3604
    if-eqz v10, :cond_16a

    #@166
    .line 3605
    :try_start_166
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_169
    .catchall {:try_start_166 .. :try_end_169} :catchall_19d
    .catch Ljava/lang/Exception; {:try_start_166 .. :try_end_169} :catch_17b

    #@169
    .line 3606
    const/4 v10, 0x0

    #@16a
    .line 3616
    .end local v7           #e:Ljava/lang/Exception;
    .end local v10           #objTempCursor:Landroid/database/Cursor;
    :cond_16a
    :goto_16a
    if-eqz v9, :cond_170

    #@16c
    .line 3617
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@16f
    .line 3618
    const/4 v9, 0x0

    #@170
    .line 3621
    :cond_170
    :goto_170
    const/4 v0, 0x1

    #@171
    goto/16 :goto_20

    #@173
    .line 3604
    .restart local v10       #objTempCursor:Landroid/database/Cursor;
    :catchall_173
    move-exception v0

    #@174
    if-eqz v10, :cond_17a

    #@176
    .line 3605
    :try_start_176
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@179
    .line 3606
    const/4 v10, 0x0

    #@17a
    :cond_17a
    throw v0
    :try_end_17b
    .catchall {:try_start_176 .. :try_end_17b} :catchall_19d
    .catch Ljava/lang/Exception; {:try_start_176 .. :try_end_17b} :catch_17b

    #@17b
    .line 3613
    .end local v10           #objTempCursor:Landroid/database/Cursor;
    :catch_17b
    move-exception v7

    #@17c
    .line 3614
    .restart local v7       #e:Ljava/lang/Exception;
    :try_start_17c
    new-instance v0, Ljava/lang/StringBuilder;

    #@17e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@181
    const-string v1, "[IP][ERROR]exception : "

    #@183
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@186
    move-result-object v0

    #@187
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v0

    #@18b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18e
    move-result-object v0

    #@18f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_192
    .catchall {:try_start_17c .. :try_end_192} :catchall_19d

    #@192
    .line 3616
    if-eqz v9, :cond_170

    #@194
    .line 3617
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@197
    .line 3618
    const/4 v9, 0x0

    #@198
    goto :goto_170

    #@199
    .line 3611
    .end local v7           #e:Ljava/lang/Exception;
    :cond_199
    :try_start_199
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->BuddyImageDB_Insert(Lcom/lge/ims/service/ip/PresenceInfo;)Z
    :try_end_19c
    .catchall {:try_start_199 .. :try_end_19c} :catchall_19d
    .catch Ljava/lang/Exception; {:try_start_199 .. :try_end_19c} :catch_17b

    #@19c
    goto :goto_16a

    #@19d
    .line 3616
    :catchall_19d
    move-exception v0

    #@19e
    if-eqz v9, :cond_1a4

    #@1a0
    .line 3617
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@1a3
    .line 3618
    const/4 v9, 0x0

    #@1a4
    :cond_1a4
    throw v0
.end method

.method private ContactData_Delete(J)Z
    .registers 10
    .parameter "_UriId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2893
    iget-object v5, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4
    if-nez v5, :cond_7

    #@6
    .line 2905
    :goto_6
    return v4

    #@7
    .line 2897
    :cond_7
    const-string v2, "_id=? AND mimetype=?"

    #@9
    .line 2898
    .local v2, strContactSelection:Ljava/lang/String;
    const/4 v5, 0x2

    #@a
    new-array v0, v5, [Ljava/lang/String;

    #@c
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@f
    move-result-object v5

    #@10
    aput-object v5, v0, v4

    #@12
    sget-object v5, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_MIMETYPE:Ljava/lang/String;

    #@14
    aput-object v5, v0, v3

    #@16
    .line 2902
    .local v0, astrContactSelectionArg:[Ljava/lang/String;
    iget-object v5, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@18
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b
    move-result-object v5

    #@1c
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@1e
    invoke-virtual {v5, v6, v2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@21
    move-result v1

    #@22
    .line 2903
    .local v1, delete:I
    new-instance v5, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v6, "[IP]ContactData_Delete : _UriId [ "

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    const-string v6, " ], delete result["

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    const-string v6, "]"

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@48
    .line 2905
    const/4 v5, -0x1

    #@49
    if-eq v1, v5, :cond_4d

    #@4b
    :goto_4b
    move v4, v3

    #@4c
    goto :goto_6

    #@4d
    :cond_4d
    move v3, v4

    #@4e
    goto :goto_4b
.end method

.method private ContactData_Insert(JJ)J
    .registers 15
    .parameter "_id"
    .parameter "raw_contact_id"

    #@0
    .prologue
    const-wide/16 v6, -0x1

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v9, 0x0

    #@4
    .line 2863
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@6
    if-nez v4, :cond_a

    #@8
    move-wide v4, v6

    #@9
    .line 2889
    :goto_9
    return-wide v4

    #@a
    .line 2866
    :cond_a
    new-instance v4, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v8, "[IP]: _id["

    #@11
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v8, "], raw_contact_id ["

    #@1b
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    const-string v8, "]"

    #@25
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@30
    .line 2867
    new-instance v1, Landroid/content/ContentValues;

    #@32
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@35
    .line 2868
    .local v1, mValues:Landroid/content/ContentValues;
    const-string v4, "raw_contact_id"

    #@37
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3a
    move-result-object v8

    #@3b
    invoke-virtual {v1, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@3e
    .line 2869
    const-string v4, "mimetype"

    #@40
    sget-object v8, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_MIMETYPE:Ljava/lang/String;

    #@42
    invoke-virtual {v1, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 2870
    const-string v4, "data1"

    #@47
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4a
    move-result-object v8

    #@4b
    invoke-virtual {v1, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@4e
    .line 2871
    const-string v4, "data2"

    #@50
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@53
    move-result-object v8

    #@54
    invoke-virtual {v1, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@57
    .line 2872
    const-string v4, "data3"

    #@59
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v1, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@60
    .line 2873
    const-string v4, "data6"

    #@62
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@65
    move-result-object v8

    #@66
    invoke-virtual {v1, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@69
    .line 2874
    const-string v4, "data7"

    #@6b
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6e
    move-result-object v8

    #@6f
    invoke-virtual {v1, v4, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@72
    .line 2875
    const-string v8, "data8"

    #@74
    move-object v4, v5

    #@75
    check-cast v4, Ljava/lang/String;

    #@77
    invoke-virtual {v1, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7a
    .line 2876
    const-string v8, "data9"

    #@7c
    move-object v4, v5

    #@7d
    check-cast v4, Ljava/lang/String;

    #@7f
    invoke-virtual {v1, v8, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@82
    .line 2877
    const-string v4, "data10"

    #@84
    check-cast v5, Ljava/lang/String;

    #@86
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@89
    .line 2878
    const-string v4, "data11"

    #@8b
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8e
    move-result-object v5

    #@8f
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@92
    .line 2879
    const/4 v2, 0x0

    #@93
    .line 2880
    .local v2, obUri:Landroid/net/Uri;
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@95
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@98
    move-result-object v4

    #@99
    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@9b
    invoke-virtual {v4, v5, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@9e
    move-result-object v2

    #@9f
    .line 2881
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@a2
    .line 2882
    if-nez v2, :cond_ac

    #@a4
    .line 2883
    const-string v4, "[IP][ERROR]ContactData_Insert fail"

    #@a6
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@a9
    move-wide v4, v6

    #@aa
    .line 2884
    goto/16 :goto_9

    #@ac
    .line 2886
    :cond_ac
    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@af
    move-result-object v0

    #@b0
    .line 2887
    .local v0, mDataId:Ljava/lang/String;
    new-instance v3, Ljava/lang/Long;

    #@b2
    invoke-direct {v3, v0}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    #@b5
    .line 2888
    .local v3, oblong:Ljava/lang/Long;
    new-instance v4, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v5, "[IP]ContactData_Insert : Uri Data Id ["

    #@bc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v4

    #@c0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v4

    #@c4
    const-string v5, "], long value ["

    #@c6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v4

    #@ca
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@cd
    move-result-wide v5

    #@ce
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v4

    #@d2
    const-string v5, "]"

    #@d4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v4

    #@d8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v4

    #@dc
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@df
    .line 2889
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@e2
    move-result-wide v4

    #@e3
    goto/16 :goto_9
.end method

.method private ContactData_Update(Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;J)Z
    .registers 12
    .parameter "objRCSContactData"
    .parameter "nUriId"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 3009
    iget-object v6, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4
    if-nez v6, :cond_7

    #@6
    .line 3051
    :cond_6
    :goto_6
    return v4

    #@7
    .line 3013
    :cond_7
    if-eqz p1, :cond_6

    #@9
    .line 3017
    new-instance v1, Landroid/content/ContentValues;

    #@b
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@e
    .line 3018
    .local v1, mValues:Landroid/content/ContentValues;
    const-string v2, "_id=?"

    #@10
    .line 3019
    .local v2, strContactSelection:Ljava/lang/String;
    new-array v0, v5, [Ljava/lang/String;

    #@12
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@15
    move-result-object v6

    #@16
    aput-object v6, v0, v4

    #@18
    .line 3021
    .local v0, astrContactSelectionArg:[Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v7, "[IP]: nUriId ["

    #@1f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    const-string v7, "], nIsRCS["

    #@29
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v6

    #@2d
    iget v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRCS:I

    #@2f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    const-string v7, "], response code ["

    #@35
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    iget v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nResponsCode:I

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, "]"

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4c
    .line 3023
    iget v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRCS:I

    #@4e
    const/4 v7, 0x4

    #@4f
    if-eq v6, v7, :cond_55

    #@51
    iget v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRCS:I

    #@53
    if-nez v6, :cond_b3

    #@55
    .line 3025
    :cond_55
    iget v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nResponsCode:I

    #@57
    if-nez v6, :cond_a0

    #@59
    .line 3026
    const-string v4, "data2"

    #@5b
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e
    move-result-object v6

    #@5f
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@62
    .line 3027
    const-string v4, "data11"

    #@64
    iget v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsMIM:I

    #@66
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6d
    .line 3046
    :goto_6d
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@6f
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@72
    move-result-object v4

    #@73
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@75
    invoke-virtual {v4, v6, v1, v2, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@78
    move-result v3

    #@79
    .line 3047
    .local v3, update:I
    const/4 v4, -0x1

    #@7a
    if-ne v3, v4, :cond_9a

    #@7c
    .line 3048
    new-instance v4, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v6, "[IP][ERROR]ContactData_Update : update is failed ["

    #@83
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v4

    #@87
    iget-object v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strMSISDN:Ljava/lang/String;

    #@89
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v4

    #@8d
    const-string v6, "]"

    #@8f
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v4

    #@93
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v4

    #@97
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@9a
    .line 3050
    :cond_9a
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@9d
    move v4, v5

    #@9e
    .line 3051
    goto/16 :goto_6

    #@a0
    .line 3029
    .end local v3           #update:I
    :cond_a0
    const-string v6, "data2"

    #@a2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a5
    move-result-object v7

    #@a6
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@a9
    .line 3030
    const-string v6, "data11"

    #@ab
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ae
    move-result-object v4

    #@af
    invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@b2
    goto :goto_6d

    #@b3
    .line 3036
    :cond_b3
    iget v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nResponsCode:I

    #@b5
    const/16 v7, 0x194

    #@b7
    if-ne v6, v7, :cond_cc

    #@b9
    .line 3037
    const-string v6, "data2"

    #@bb
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@be
    move-result-object v7

    #@bf
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@c2
    .line 3038
    const-string v6, "data11"

    #@c4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c7
    move-result-object v4

    #@c8
    invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@cb
    goto :goto_6d

    #@cc
    .line 3040
    :cond_cc
    const-string v4, "data2"

    #@ce
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d1
    move-result-object v6

    #@d2
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@d5
    .line 3041
    const-string v4, "data3"

    #@d7
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@da
    move-result-wide v6

    #@db
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@de
    move-result-object v6

    #@df
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@e2
    .line 3042
    const-string v4, "[IP]already add RCS friend. it is same phone number, and insert timestamp because new friend add to contact.."

    #@e4
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@e7
    .line 3043
    const-string v4, "data11"

    #@e9
    iget v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsMIM:I

    #@eb
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ee
    move-result-object v6

    #@ef
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@f2
    goto/16 :goto_6d
.end method

.method private ContactData_Update(Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;Lcom/lge/ims/service/ip/CapaInfo;)Z
    .registers 13
    .parameter "objRCSContactData"
    .parameter "objCapaInfo"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2908
    iget-object v6, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4
    if-nez v6, :cond_7

    #@6
    .line 2951
    :goto_6
    return v4

    #@7
    .line 2911
    :cond_7
    new-instance v1, Landroid/content/ContentValues;

    #@9
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@c
    .line 2912
    .local v1, mValues:Landroid/content/ContentValues;
    const-string v2, "_id=?"

    #@e
    .line 2913
    .local v2, strContactSelection:Ljava/lang/String;
    new-array v0, v5, [Ljava/lang/String;

    #@10
    iget-wide v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nUriId:J

    #@12
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@15
    move-result-object v6

    #@16
    aput-object v6, v0, v4

    #@18
    .line 2915
    .local v0, astrContactSelectionArg:[Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v7, "[IP]: _id["

    #@1f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v6

    #@23
    iget-wide v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nDataId:J

    #@25
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    const-string v7, "], nUriId ["

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    iget-wide v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nUriId:J

    #@31
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    const-string v7, "], nIsRCS["

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    iget v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRCS:I

    #@3d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    const-string v7, "], response code ["

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetResponseCode()I

    #@4a
    move-result v7

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    const-string v7, "]"

    #@51
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v6

    #@55
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v6

    #@59
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5c
    .line 2917
    iget v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRCS:I

    #@5e
    const/4 v7, 0x4

    #@5f
    if-eq v6, v7, :cond_65

    #@61
    iget v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRCS:I

    #@63
    if-nez v6, :cond_f0

    #@65
    .line 2919
    :cond_65
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetResponseCode()I

    #@68
    move-result v6

    #@69
    if-nez v6, :cond_d4

    #@6b
    .line 2920
    const-string v4, "data2"

    #@6d
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@70
    move-result-object v6

    #@71
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@74
    .line 2921
    const-string v4, "data11"

    #@76
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetMIMCapa()I

    #@79
    move-result v6

    #@7a
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7d
    move-result-object v6

    #@7e
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@81
    .line 2922
    iget-wide v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nTimeStamp:J

    #@83
    const-wide/16 v8, 0x0

    #@85
    cmp-long v4, v6, v8

    #@87
    if-nez v4, :cond_ce

    #@89
    .line 2923
    const-string v4, "[IP]objRCSContactData.nTimeStamp is 0. insert timestamp"

    #@8b
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8e
    .line 2924
    const-string v4, "data3"

    #@90
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@93
    move-result-wide v6

    #@94
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@97
    move-result-object v6

    #@98
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@9b
    .line 2946
    :goto_9b
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@9d
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a0
    move-result-object v4

    #@a1
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@a3
    invoke-virtual {v4, v6, v1, v2, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@a6
    move-result v3

    #@a7
    .line 2947
    .local v3, update:I
    const/4 v4, -0x1

    #@a8
    if-ne v3, v4, :cond_c8

    #@aa
    .line 2948
    new-instance v4, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v6, "[IP][ERROR]ContactData_Update : update is failed ["

    #@b1
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v4

    #@b5
    iget-object v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strMSISDN:Ljava/lang/String;

    #@b7
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    const-string v6, "]"

    #@bd
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v4

    #@c1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v4

    #@c5
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@c8
    .line 2950
    :cond_c8
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@cb
    move v4, v5

    #@cc
    .line 2951
    goto/16 :goto_6

    #@ce
    .line 2926
    .end local v3           #update:I
    :cond_ce
    const-string v4, "[IP]objRCSContactData.nTimeStamp is not 0."

    #@d0
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@d3
    goto :goto_9b

    #@d4
    .line 2929
    :cond_d4
    const-string v6, "data2"

    #@d6
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d9
    move-result-object v7

    #@da
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@dd
    .line 2930
    const-string v6, "data7"

    #@df
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e2
    move-result-object v7

    #@e3
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@e6
    .line 2931
    const-string v6, "data11"

    #@e8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@eb
    move-result-object v4

    #@ec
    invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@ef
    goto :goto_9b

    #@f0
    .line 2937
    :cond_f0
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetResponseCode()I

    #@f3
    move-result v6

    #@f4
    const/16 v7, 0x194

    #@f6
    if-ne v6, v7, :cond_114

    #@f8
    .line 2938
    const-string v6, "data2"

    #@fa
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@fd
    move-result-object v7

    #@fe
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@101
    .line 2939
    const-string v6, "data11"

    #@103
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@106
    move-result-object v7

    #@107
    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@10a
    .line 2940
    const-string v6, "data7"

    #@10c
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10f
    move-result-object v4

    #@110
    invoke-virtual {v1, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@113
    goto :goto_9b

    #@114
    .line 2942
    :cond_114
    const-string v4, "data2"

    #@116
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@119
    move-result-object v6

    #@11a
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@11d
    .line 2943
    const-string v4, "data11"

    #@11f
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetMIMCapa()I

    #@122
    move-result v6

    #@123
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@126
    move-result-object v6

    #@127
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@12a
    goto/16 :goto_9b
.end method

.method private ContactData_Update(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;J)Z
    .registers 14
    .parameter "objPresenceContactData"
    .parameter "nUriId"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 3054
    iget-object v6, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4
    if-nez v6, :cond_7

    #@6
    .line 3075
    :goto_6
    return v4

    #@7
    .line 3057
    :cond_7
    new-instance v6, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v7, "[IP]msisdn : ["

    #@e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v6

    #@12
    iget-object v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@14
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v6

    #@18
    const-string v7, "], PresenceId : ["

    #@1a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v6

    #@1e
    iget-wide v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nId:J

    #@20
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    const-string v7, "], Status : ["

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    iget v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nStatus:I

    #@2c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v6

    #@30
    const-string v7, "], FreeText: ["

    #@32
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    iget-object v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strFreeText:Ljava/lang/String;

    #@38
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    const-string v7, "]"

    #@3e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v6

    #@42
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@49
    .line 3058
    new-instance v6, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v7, "[IP]timestamp : ["

    #@50
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    iget-wide v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nTimeStamp:J

    #@56
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@59
    move-result-object v6

    #@5a
    const-string v7, "], birthday : ["

    #@5c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v6

    #@60
    iget-object v7, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strBirthDay:Ljava/lang/String;

    #@62
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    const-string v7, "], nUriId [ "

    #@68
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v6

    #@6c
    invoke-virtual {v6, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v6

    #@70
    const-string v7, "]"

    #@72
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v6

    #@76
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v6

    #@7a
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7d
    .line 3060
    iget-wide v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nTimeStamp:J

    #@7f
    const-wide/16 v8, 0x0

    #@81
    cmp-long v6, v6, v8

    #@83
    if-eqz v6, :cond_ef

    #@85
    .line 3061
    new-instance v1, Landroid/content/ContentValues;

    #@87
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@8a
    .line 3062
    .local v1, mValues:Landroid/content/ContentValues;
    const-string v2, "_id=?"

    #@8c
    .line 3063
    .local v2, strContactSelection:Ljava/lang/String;
    new-array v0, v5, [Ljava/lang/String;

    #@8e
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@91
    move-result-object v6

    #@92
    aput-object v6, v0, v4

    #@94
    .line 3064
    .local v0, astrContactSelectionArg:[Ljava/lang/String;
    const-string v4, "data6"

    #@96
    iget v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nStatus:I

    #@98
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9b
    move-result-object v6

    #@9c
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@9f
    .line 3065
    const-string v4, "data7"

    #@a1
    iget-wide v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nId:J

    #@a3
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@a6
    move-result-object v6

    #@a7
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@aa
    .line 3066
    const-string v4, "data8"

    #@ac
    iget-object v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strFreeText:Ljava/lang/String;

    #@ae
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b1
    .line 3067
    const-string v4, "data9"

    #@b3
    iget-object v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strEMailAddress:Ljava/lang/String;

    #@b5
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b8
    .line 3068
    const-string v4, "data10"

    #@ba
    iget-object v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strBirthDay:Ljava/lang/String;

    #@bc
    invoke-virtual {v1, v4, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@bf
    .line 3069
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@c1
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c4
    move-result-object v4

    #@c5
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@c7
    invoke-virtual {v4, v6, v1, v2, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@ca
    move-result v3

    #@cb
    .line 3070
    .local v3, update:I
    const/4 v4, -0x1

    #@cc
    if-ne v3, v4, :cond_ec

    #@ce
    .line 3071
    new-instance v4, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v6, "[IP][ERROR]ContactData_Update : update is failed ["

    #@d5
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v4

    #@d9
    iget-object v6, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@db
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v4

    #@df
    const-string v6, "]"

    #@e1
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v4

    #@e5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v4

    #@e9
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@ec
    .line 3073
    :cond_ec
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@ef
    .end local v0           #astrContactSelectionArg:[Ljava/lang/String;
    .end local v1           #mValues:Landroid/content/ContentValues;
    .end local v2           #strContactSelection:Ljava/lang/String;
    .end local v3           #update:I
    :cond_ef
    move v4, v5

    #@f0
    .line 3075
    goto/16 :goto_6
.end method

.method private ContactData_Update(Lcom/lge/ims/service/ip/PresenceInfo;J)Z
    .registers 21
    .parameter "objPresenceInfo"
    .parameter "nPresenceId"

    #@0
    .prologue
    .line 2954
    move-object/from16 v0, p0

    #@2
    iget-object v2, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4
    if-nez v2, :cond_8

    #@6
    .line 2955
    const/4 v2, 0x0

    #@7
    .line 3006
    :cond_7
    :goto_7
    return v2

    #@8
    .line 2957
    :cond_8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "[IP]msisdn : ["

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, "], PresenceId : ["

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    move-wide/from16 v0, p2

    #@23
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    const-string v3, "], Status : ["

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatus()I

    #@30
    move-result v3

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    const-string v3, "], FreeText: ["

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetFreeText()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v3, "]"

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v2

    #@4d
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@50
    .line 2958
    new-instance v2, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v3, "[IP]e-mail : ["

    #@57
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetEMailAddress()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v2

    #@63
    const-string v3, "], birthday : ["

    #@65
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetBirthDay()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    const-string v3, "]"

    #@73
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v2

    #@77
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v2

    #@7b
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7e
    .line 2959
    const-string v5, "nor_msisdn=?"

    #@80
    .line 2960
    .local v5, strSelection:Ljava/lang/String;
    const/4 v2, 0x1

    #@81
    new-array v6, v2, [Ljava/lang/String;

    #@83
    const/4 v2, 0x0

    #@84
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@87
    move-result-object v3

    #@88
    aput-object v3, v6, v2

    #@8a
    .line 2961
    .local v6, astrSelectionArgs:[Ljava/lang/String;
    const/4 v12, 0x0

    #@8b
    .line 2963
    .local v12, objCursor:Landroid/database/Cursor;
    :try_start_8b
    move-object/from16 v0, p0

    #@8d
    iget-object v2, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@8f
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@92
    move-result-object v2

    #@93
    sget-object v3, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@95
    sget-object v4, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@97
    const/4 v7, 0x0

    #@98
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@9b
    move-result-object v12

    #@9c
    .line 2968
    if-nez v12, :cond_c7

    #@9e
    .line 2969
    new-instance v2, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v3, "[IP][ERROR]ContactData_Update : Not found ["

    #@a5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v2

    #@a9
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@ac
    move-result-object v3

    #@ad
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v2

    #@b1
    const-string v3, "]"

    #@b3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v2

    #@b7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v2

    #@bb
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_be
    .catchall {:try_start_8b .. :try_end_be} :catchall_1bc
    .catch Ljava/lang/Exception; {:try_start_8b .. :try_end_be} :catch_de

    #@be
    .line 2970
    const/4 v2, 0x0

    #@bf
    .line 3001
    if-eqz v12, :cond_7

    #@c1
    .line 3002
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@c4
    .line 3003
    const/4 v12, 0x0

    #@c5
    goto/16 :goto_7

    #@c7
    .line 2972
    :cond_c7
    :try_start_c7
    new-instance v14, Ljava/util/LinkedList;

    #@c9
    invoke-direct {v14}, Ljava/util/LinkedList;-><init>()V

    #@cc
    .line 2973
    .local v14, objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :cond_cc
    :goto_cc
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    #@cf
    move-result v2

    #@d0
    if-eqz v2, :cond_fe

    #@d2
    .line 2974
    move-object/from16 v0, p0

    #@d4
    invoke-direct {v0, v12}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@d7
    move-result-object v13

    #@d8
    .line 2975
    .local v13, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v13, :cond_cc

    #@da
    .line 2976
    invoke-virtual {v14, v13}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_dd
    .catchall {:try_start_c7 .. :try_end_dd} :catchall_1bc
    .catch Ljava/lang/Exception; {:try_start_c7 .. :try_end_dd} :catch_de

    #@dd
    goto :goto_cc

    #@de
    .line 2998
    .end local v13           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v14           #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :catch_de
    move-exception v9

    #@df
    .line 2999
    .local v9, e:Ljava/lang/Exception;
    :try_start_df
    new-instance v2, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v3, "[IP][ERROR]exception : "

    #@e6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v2

    #@ea
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v2

    #@ee
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v2

    #@f2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_f5
    .catchall {:try_start_df .. :try_end_f5} :catchall_1bc

    #@f5
    .line 3001
    if-eqz v12, :cond_fb

    #@f7
    .line 3002
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@fa
    .line 3003
    const/4 v12, 0x0

    #@fb
    .line 3006
    :cond_fb
    const/4 v2, 0x1

    #@fc
    goto/16 :goto_7

    #@fe
    .line 2979
    .end local v9           #e:Ljava/lang/Exception;
    .restart local v14       #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :cond_fe
    const/4 v10, 0x0

    #@ff
    .local v10, i:I
    :goto_ff
    :try_start_ff
    invoke-virtual {v14}, Ljava/util/LinkedList;->size()I

    #@102
    move-result v2

    #@103
    if-ge v10, v2, :cond_1b3

    #@105
    .line 2980
    invoke-virtual {v14, v10}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@108
    move-result-object v13

    #@109
    check-cast v13, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@10b
    .line 2981
    .restart local v13       #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    new-instance v2, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string v3, "[IP]: _id["

    #@112
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v2

    #@116
    iget-wide v3, v13, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nDataId:J

    #@118
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v2

    #@11c
    const-string v3, "], UriId ["

    #@11e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v2

    #@122
    iget-wide v3, v13, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nUriId:J

    #@124
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@127
    move-result-object v2

    #@128
    const-string v3, "]"

    #@12a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v2

    #@12e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@131
    move-result-object v2

    #@132
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@135
    .line 2982
    new-instance v11, Landroid/content/ContentValues;

    #@137
    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    #@13a
    .line 2983
    .local v11, mValues:Landroid/content/ContentValues;
    const-string v15, "_id=?"

    #@13c
    .line 2984
    .local v15, strContactSelection:Ljava/lang/String;
    const/4 v2, 0x1

    #@13d
    new-array v8, v2, [Ljava/lang/String;

    #@13f
    const/4 v2, 0x0

    #@140
    iget-wide v3, v13, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nUriId:J

    #@142
    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@145
    move-result-object v3

    #@146
    aput-object v3, v8, v2

    #@148
    .line 2985
    .local v8, astrContactSelectionArg:[Ljava/lang/String;
    const-string v2, "data6"

    #@14a
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatus()I

    #@14d
    move-result v3

    #@14e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@151
    move-result-object v3

    #@152
    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@155
    .line 2986
    const-string v2, "data7"

    #@157
    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@15a
    move-result-object v3

    #@15b
    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@15e
    .line 2987
    const-string v2, "data8"

    #@160
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetFreeText()Ljava/lang/String;

    #@163
    move-result-object v3

    #@164
    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@167
    .line 2988
    const-string v2, "data9"

    #@169
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetEMailAddress()Ljava/lang/String;

    #@16c
    move-result-object v3

    #@16d
    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@170
    .line 2989
    const-string v2, "data10"

    #@172
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetBirthDay()Ljava/lang/String;

    #@175
    move-result-object v3

    #@176
    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@179
    .line 2990
    move-object/from16 v0, p0

    #@17b
    iget-object v2, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@17d
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@180
    move-result-object v2

    #@181
    sget-object v3, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@183
    invoke-virtual {v2, v3, v11, v15, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@186
    move-result v16

    #@187
    .line 2991
    .local v16, update:I
    const/4 v2, -0x1

    #@188
    move/from16 v0, v16

    #@18a
    if-ne v0, v2, :cond_1ac

    #@18c
    .line 2992
    new-instance v2, Ljava/lang/StringBuilder;

    #@18e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@191
    const-string v3, "[IP][ERROR]ContactData_Update : update is failed ["

    #@193
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@196
    move-result-object v2

    #@197
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@19a
    move-result-object v3

    #@19b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v2

    #@19f
    const-string v3, "]"

    #@1a1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v2

    #@1a5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a8
    move-result-object v2

    #@1a9
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@1ac
    .line 2994
    :cond_1ac
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V
    :try_end_1af
    .catchall {:try_start_ff .. :try_end_1af} :catchall_1bc
    .catch Ljava/lang/Exception; {:try_start_ff .. :try_end_1af} :catch_de

    #@1af
    .line 2979
    add-int/lit8 v10, v10, 0x1

    #@1b1
    goto/16 :goto_ff

    #@1b3
    .line 2996
    .end local v8           #astrContactSelectionArg:[Ljava/lang/String;
    .end local v11           #mValues:Landroid/content/ContentValues;
    .end local v13           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v15           #strContactSelection:Ljava/lang/String;
    .end local v16           #update:I
    :cond_1b3
    const/4 v2, 0x1

    #@1b4
    .line 3001
    if-eqz v12, :cond_7

    #@1b6
    .line 3002
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@1b9
    .line 3003
    const/4 v12, 0x0

    #@1ba
    goto/16 :goto_7

    #@1bc
    .line 3001
    .end local v10           #i:I
    .end local v14           #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :catchall_1bc
    move-exception v2

    #@1bd
    if-eqz v12, :cond_1c3

    #@1bf
    .line 3002
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@1c2
    .line 3003
    const/4 v12, 0x0

    #@1c3
    :cond_1c3
    throw v2
.end method

.method private ContactImageData_Delete(J)Z
    .registers 10
    .parameter "_UriId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 3351
    iget-object v5, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4
    if-nez v5, :cond_7

    #@6
    .line 3361
    :goto_6
    return v4

    #@7
    .line 3355
    :cond_7
    const-string v2, "_id=? AND mimetype=?"

    #@9
    .line 3356
    .local v2, strContactSelection:Ljava/lang/String;
    const/4 v5, 0x2

    #@a
    new-array v0, v5, [Ljava/lang/String;

    #@c
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@f
    move-result-object v5

    #@10
    aput-object v5, v0, v4

    #@12
    const-string v5, "vnd.android.cursor.item/photo"

    #@14
    aput-object v5, v0, v3

    #@16
    .line 3358
    .local v0, astrContactSelectionArg:[Ljava/lang/String;
    iget-object v5, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@18
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b
    move-result-object v5

    #@1c
    sget-object v6, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@1e
    invoke-virtual {v5, v6, v2, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@21
    move-result v1

    #@22
    .line 3359
    .local v1, delete:I
    new-instance v5, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v6, "[IP]ContactImageData_Delete : _UriId [ "

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    const-string v6, " ], delete result["

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    const-string v6, "]"

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v5

    #@45
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@48
    .line 3361
    const/4 v5, -0x1

    #@49
    if-eq v1, v5, :cond_4d

    #@4b
    :goto_4b
    move v4, v3

    #@4c
    goto :goto_6

    #@4d
    :cond_4d
    move v3, v4

    #@4e
    goto :goto_4b
.end method

.method private ContactImageData_Insert(JLcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;)J
    .registers 16
    .parameter "raw_contact_id"
    .parameter "objPresenceInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3303
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@2
    if-nez v9, :cond_7

    #@4
    .line 3304
    const-wide/16 v9, -0x1

    #@6
    .line 3348
    :goto_6
    return-wide v9

    #@7
    .line 3306
    :cond_7
    new-instance v9, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v10, "[IP]: phone number [ "

    #@e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v9

    #@12
    iget-object v10, p3, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@14
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v9

    #@18
    const-string v10, "], raw_contact_id ["

    #@1a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v9

    #@1e
    invoke-virtual {v9, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@21
    move-result-object v9

    #@22
    const-string v10, "], Image Path [ "

    #@24
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v9

    #@28
    iget-object v10, p3, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIcon:Ljava/lang/String;

    #@2a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v9

    #@2e
    const-string v10, "]"

    #@30
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v9

    #@34
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v9

    #@38
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3b
    .line 3307
    iget-object v9, p3, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIcon:Ljava/lang/String;

    #@3d
    invoke-static {v9}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@40
    move-result-object v7

    #@41
    .line 3308
    .local v7, photo:Landroid/graphics/Bitmap;
    if-nez v7, :cond_46

    #@43
    .line 3309
    const-wide/16 v9, -0x1

    #@45
    goto :goto_6

    #@46
    .line 3312
    :cond_46
    :try_start_46
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    #@49
    move-result v9

    #@4a
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    #@4d
    move-result v10

    #@4e
    mul-int/2addr v9, v10

    #@4f
    mul-int/lit8 v8, v9, 0x4

    #@51
    .line 3313
    .local v8, size:I
    if-gez v8, :cond_56

    #@53
    .line 3314
    const-wide/16 v9, -0x1

    #@55
    goto :goto_6

    #@56
    .line 3316
    :cond_56
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    #@58
    invoke-direct {v6, v8}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@5b
    .line 3317
    .local v6, out:Ljava/io/ByteArrayOutputStream;
    if-nez v6, :cond_60

    #@5d
    .line 3318
    const-wide/16 v9, -0x1

    #@5f
    goto :goto_6

    #@60
    .line 3320
    :cond_60
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@62
    const/16 v10, 0x5f

    #@64
    invoke-virtual {v7, v9, v10, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@67
    move-result v0

    #@68
    .line 3321
    .local v0, compressed:Z
    if-nez v0, :cond_8d

    #@6a
    .line 3322
    new-instance v9, Ljava/io/IOException;

    #@6c
    const-string v10, "Unable to compress image"

    #@6e
    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@71
    throw v9
    :try_end_72
    .catchall {:try_start_46 .. :try_end_72} :catchall_10f
    .catch Ljava/lang/Exception; {:try_start_46 .. :try_end_72} :catch_72

    #@72
    .line 3344
    .end local v0           #compressed:Z
    .end local v6           #out:Ljava/io/ByteArrayOutputStream;
    .end local v8           #size:I
    :catch_72
    move-exception v1

    #@73
    .line 3345
    .local v1, e:Ljava/lang/Exception;
    :try_start_73
    new-instance v9, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v10, "[IP][ERROR]exception : "

    #@7a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v9

    #@7e
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v9

    #@82
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v9

    #@86
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_89
    .catchall {:try_start_73 .. :try_end_89} :catchall_10f

    #@89
    .line 3348
    const-wide/16 v9, -0x1

    #@8b
    goto/16 :goto_6

    #@8d
    .line 3324
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v0       #compressed:Z
    .restart local v6       #out:Ljava/io/ByteArrayOutputStream;
    .restart local v8       #size:I
    :cond_8d
    :try_start_8d
    new-instance v3, Landroid/content/ContentValues;

    #@8f
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@92
    .line 3325
    .local v3, mValues:Landroid/content/ContentValues;
    const-string v9, "raw_contact_id"

    #@94
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@97
    move-result-object v10

    #@98
    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@9b
    .line 3326
    const-string v9, "mimetype"

    #@9d
    const-string v10, "vnd.android.cursor.item/photo"

    #@9f
    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a2
    .line 3327
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->flush()V

    #@a5
    .line 3328
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V

    #@a8
    .line 3329
    const-string v9, "data15"

    #@aa
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@ad
    move-result-object v10

    #@ae
    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@b1
    .line 3330
    const-string v9, "data10"

    #@b3
    const/4 v10, 0x0

    #@b4
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b7
    move-result-object v10

    #@b8
    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@bb
    .line 3332
    const/4 v4, 0x0

    #@bc
    .line 3333
    .local v4, obUri:Landroid/net/Uri;
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@be
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c1
    move-result-object v9

    #@c2
    sget-object v10, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@c4
    invoke-virtual {v9, v10, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@c7
    move-result-object v4

    #@c8
    .line 3334
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    #@cb
    .line 3335
    if-nez v4, :cond_d6

    #@cd
    .line 3336
    const-string v9, "[IP][ERROR]ContactImageData_Insert fail"

    #@cf
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d2
    .line 3337
    const-wide/16 v9, -0x1

    #@d4
    goto/16 :goto_6

    #@d6
    .line 3339
    :cond_d6
    invoke-virtual {v4}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@d9
    move-result-object v2

    #@da
    .line 3340
    .local v2, mDataId:Ljava/lang/String;
    new-instance v5, Ljava/lang/Long;

    #@dc
    invoke-direct {v5, v2}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    #@df
    .line 3341
    .local v5, oblong:Ljava/lang/Long;
    new-instance v9, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v10, "[IP]ContactImageData_Insert : Uri Data Id ["

    #@e6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v9

    #@ea
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v9

    #@ee
    const-string v10, "], long value ["

    #@f0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v9

    #@f4
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    #@f7
    move-result-wide v10

    #@f8
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v9

    #@fc
    const-string v10, "]"

    #@fe
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v9

    #@102
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v9

    #@106
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@109
    .line 3342
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
    :try_end_10c
    .catchall {:try_start_8d .. :try_end_10c} :catchall_10f
    .catch Ljava/lang/Exception; {:try_start_8d .. :try_end_10c} :catch_72

    #@10c
    move-result-wide v9

    #@10d
    goto/16 :goto_6

    #@10f
    .line 3346
    .end local v0           #compressed:Z
    .end local v2           #mDataId:Ljava/lang/String;
    .end local v3           #mValues:Landroid/content/ContentValues;
    .end local v4           #obUri:Landroid/net/Uri;
    .end local v5           #oblong:Ljava/lang/Long;
    .end local v6           #out:Ljava/io/ByteArrayOutputStream;
    .end local v8           #size:I
    :catchall_10f
    move-exception v9

    #@110
    throw v9
.end method

.method private ContactImageData_Insert(JLcom/lge/ims/service/ip/PresenceInfo;)J
    .registers 16
    .parameter "raw_contact_id"
    .parameter "objPresenceInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 3255
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@2
    if-nez v9, :cond_7

    #@4
    .line 3256
    const-wide/16 v9, -0x1

    #@6
    .line 3300
    :goto_6
    return-wide v9

    #@7
    .line 3258
    :cond_7
    new-instance v9, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v10, "[IP]: phone number [ "

    #@e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v9

    #@12
    invoke-virtual {p3}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@15
    move-result-object v10

    #@16
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v9

    #@1a
    const-string v10, "], raw_contact_id ["

    #@1c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v9

    #@20
    invoke-virtual {v9, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@23
    move-result-object v9

    #@24
    const-string v10, "], Image Path [ "

    #@26
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v9

    #@2a
    invoke-virtual {p3}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIcon()Ljava/lang/String;

    #@2d
    move-result-object v10

    #@2e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v9

    #@32
    const-string v10, "]"

    #@34
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v9

    #@38
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v9

    #@3c
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3f
    .line 3259
    invoke-virtual {p3}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIcon()Ljava/lang/String;

    #@42
    move-result-object v9

    #@43
    invoke-static {v9}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@46
    move-result-object v7

    #@47
    .line 3260
    .local v7, photo:Landroid/graphics/Bitmap;
    if-nez v7, :cond_4c

    #@49
    .line 3261
    const-wide/16 v9, -0x1

    #@4b
    goto :goto_6

    #@4c
    .line 3264
    :cond_4c
    :try_start_4c
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getWidth()I

    #@4f
    move-result v9

    #@50
    invoke-virtual {v7}, Landroid/graphics/Bitmap;->getHeight()I

    #@53
    move-result v10

    #@54
    mul-int/2addr v9, v10

    #@55
    mul-int/lit8 v8, v9, 0x4

    #@57
    .line 3265
    .local v8, size:I
    if-gez v8, :cond_5c

    #@59
    .line 3266
    const-wide/16 v9, -0x1

    #@5b
    goto :goto_6

    #@5c
    .line 3268
    :cond_5c
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    #@5e
    invoke-direct {v6, v8}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@61
    .line 3269
    .local v6, out:Ljava/io/ByteArrayOutputStream;
    if-nez v6, :cond_66

    #@63
    .line 3270
    const-wide/16 v9, -0x1

    #@65
    goto :goto_6

    #@66
    .line 3272
    :cond_66
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@68
    const/16 v10, 0x5f

    #@6a
    invoke-virtual {v7, v9, v10, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@6d
    move-result v0

    #@6e
    .line 3273
    .local v0, compressed:Z
    if-nez v0, :cond_93

    #@70
    .line 3274
    new-instance v9, Ljava/io/IOException;

    #@72
    const-string v10, "Unable to compress image"

    #@74
    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@77
    throw v9
    :try_end_78
    .catchall {:try_start_4c .. :try_end_78} :catchall_115
    .catch Ljava/lang/Exception; {:try_start_4c .. :try_end_78} :catch_78

    #@78
    .line 3296
    .end local v0           #compressed:Z
    .end local v6           #out:Ljava/io/ByteArrayOutputStream;
    .end local v8           #size:I
    :catch_78
    move-exception v1

    #@79
    .line 3297
    .local v1, e:Ljava/lang/Exception;
    :try_start_79
    new-instance v9, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v10, "[IP][ERROR]exception : "

    #@80
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v9

    #@84
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v9

    #@88
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v9

    #@8c
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_8f
    .catchall {:try_start_79 .. :try_end_8f} :catchall_115

    #@8f
    .line 3300
    const-wide/16 v9, -0x1

    #@91
    goto/16 :goto_6

    #@93
    .line 3276
    .end local v1           #e:Ljava/lang/Exception;
    .restart local v0       #compressed:Z
    .restart local v6       #out:Ljava/io/ByteArrayOutputStream;
    .restart local v8       #size:I
    :cond_93
    :try_start_93
    new-instance v3, Landroid/content/ContentValues;

    #@95
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@98
    .line 3277
    .local v3, mValues:Landroid/content/ContentValues;
    const-string v9, "raw_contact_id"

    #@9a
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@9d
    move-result-object v10

    #@9e
    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@a1
    .line 3278
    const-string v9, "mimetype"

    #@a3
    const-string v10, "vnd.android.cursor.item/photo"

    #@a5
    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a8
    .line 3279
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->flush()V

    #@ab
    .line 3280
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V

    #@ae
    .line 3281
    const-string v9, "data15"

    #@b0
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@b3
    move-result-object v10

    #@b4
    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@b7
    .line 3282
    const-string v9, "data10"

    #@b9
    const/4 v10, 0x0

    #@ba
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@bd
    move-result-object v10

    #@be
    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@c1
    .line 3284
    const/4 v4, 0x0

    #@c2
    .line 3285
    .local v4, obUri:Landroid/net/Uri;
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@c4
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c7
    move-result-object v9

    #@c8
    sget-object v10, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@ca
    invoke-virtual {v9, v10, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@cd
    move-result-object v4

    #@ce
    .line 3286
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    #@d1
    .line 3287
    if-nez v4, :cond_dc

    #@d3
    .line 3288
    const-string v9, "[IP][ERROR]ContactImageData_Insert fail"

    #@d5
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d8
    .line 3289
    const-wide/16 v9, -0x1

    #@da
    goto/16 :goto_6

    #@dc
    .line 3291
    :cond_dc
    invoke-virtual {v4}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@df
    move-result-object v2

    #@e0
    .line 3292
    .local v2, mDataId:Ljava/lang/String;
    new-instance v5, Ljava/lang/Long;

    #@e2
    invoke-direct {v5, v2}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    #@e5
    .line 3293
    .local v5, oblong:Ljava/lang/Long;
    new-instance v9, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v10, "[IP]ContactImageData_Insert : Uri Data Id ["

    #@ec
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v9

    #@f0
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v9

    #@f4
    const-string v10, "], long value ["

    #@f6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v9

    #@fa
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    #@fd
    move-result-wide v10

    #@fe
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@101
    move-result-object v9

    #@102
    const-string v10, "]"

    #@104
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v9

    #@108
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10b
    move-result-object v9

    #@10c
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@10f
    .line 3294
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J
    :try_end_112
    .catchall {:try_start_93 .. :try_end_112} :catchall_115
    .catch Ljava/lang/Exception; {:try_start_93 .. :try_end_112} :catch_78

    #@112
    move-result-wide v9

    #@113
    goto/16 :goto_6

    #@115
    .line 3298
    .end local v0           #compressed:Z
    .end local v2           #mDataId:Ljava/lang/String;
    .end local v3           #mValues:Landroid/content/ContentValues;
    .end local v4           #obUri:Landroid/net/Uri;
    .end local v5           #oblong:Ljava/lang/Long;
    .end local v6           #out:Ljava/io/ByteArrayOutputStream;
    .end local v8           #size:I
    :catchall_115
    move-exception v9

    #@116
    throw v9
.end method

.method private ContactImageData_ReUpdate(Z)Z
    .registers 15
    .parameter "bIgnorePriority"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v12, 0x1

    #@2
    .line 3450
    const-string v3, "statusicon_update=? "

    #@4
    .line 3451
    .local v3, selection:Ljava/lang/String;
    new-array v4, v12, [Ljava/lang/String;

    #@6
    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    aput-object v0, v4, v11

    #@c
    .line 3453
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v8, 0x0

    #@d
    .line 3455
    .local v8, objCursor:Landroid/database/Cursor;
    :try_start_d
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v0

    #@13
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@15
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_PRESENCE_PROJECTION:[Ljava/lang/String;

    #@17
    const/4 v5, 0x0

    #@18
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1b
    move-result-object v8

    #@1c
    .line 3461
    if-nez v8, :cond_2b

    #@1e
    .line 3462
    const-string v0, "[IP][ERROR] objCursor is null"

    #@20
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_23
    .catchall {:try_start_d .. :try_end_23} :catchall_9d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_23} :catch_40

    #@23
    .line 3487
    if-eqz v8, :cond_29

    #@25
    .line 3488
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@28
    .line 3489
    const/4 v8, 0x0

    #@29
    :cond_29
    move v0, v11

    #@2a
    .line 3492
    :goto_2a
    return v0

    #@2b
    .line 3465
    :cond_2b
    :try_start_2b
    new-instance v10, Ljava/util/LinkedList;

    #@2d
    invoke-direct {v10}, Ljava/util/LinkedList;-><init>()V

    #@30
    .line 3466
    .local v10, objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :cond_30
    :goto_30
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_5f

    #@36
    .line 3467
    invoke-direct {p0, v8}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@39
    move-result-object v9

    #@3a
    .line 3468
    .local v9, objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-eqz v9, :cond_30

    #@3c
    .line 3471
    invoke-virtual {v10, v9}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_3f
    .catchall {:try_start_2b .. :try_end_3f} :catchall_9d
    .catch Ljava/lang/Exception; {:try_start_2b .. :try_end_3f} :catch_40

    #@3f
    goto :goto_30

    #@40
    .line 3484
    .end local v9           #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    .end local v10           #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :catch_40
    move-exception v6

    #@41
    .line 3485
    .local v6, e:Ljava/lang/Exception;
    :try_start_41
    new-instance v0, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v1, "[IP][ERROR]exception : "

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v0

    #@4c
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v0

    #@54
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_57
    .catchall {:try_start_41 .. :try_end_57} :catchall_9d

    #@57
    .line 3487
    if-eqz v8, :cond_5d

    #@59
    .line 3488
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@5c
    .line 3489
    const/4 v8, 0x0

    #@5d
    :cond_5d
    move v0, v12

    #@5e
    .line 3492
    goto :goto_2a

    #@5f
    .line 3475
    .end local v6           #e:Ljava/lang/Exception;
    .restart local v10       #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :cond_5f
    :try_start_5f
    new-instance v0, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v1, "[IP]list size : ["

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v0

    #@6a
    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    #@6d
    move-result v1

    #@6e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    const-string v1, "]"

    #@74
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v0

    #@78
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v0

    #@7c
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7f
    .line 3476
    const/4 v7, 0x0

    #@80
    .local v7, i:I
    :goto_80
    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    #@83
    move-result v0

    #@84
    if-ge v7, v0, :cond_95

    #@86
    .line 3477
    invoke-virtual {v10, v7}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@89
    move-result-object v9

    #@8a
    check-cast v9, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@8c
    .line 3478
    .restart local v9       #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-eqz v9, :cond_92

    #@8e
    .line 3479
    const/4 v0, 0x1

    #@8f
    invoke-direct {p0, v9, p1, v0}, Lcom/lge/ims/service/ip/IPContactHelper;->SyncFullImageToContact(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;ZZ)Z
    :try_end_92
    .catchall {:try_start_5f .. :try_end_92} :catchall_9d
    .catch Ljava/lang/Exception; {:try_start_5f .. :try_end_92} :catch_40

    #@92
    .line 3476
    :cond_92
    add-int/lit8 v7, v7, 0x1

    #@94
    goto :goto_80

    #@95
    .line 3487
    .end local v9           #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    :cond_95
    if-eqz v8, :cond_9b

    #@97
    .line 3488
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@9a
    .line 3489
    const/4 v8, 0x0

    #@9b
    :cond_9b
    move v0, v12

    #@9c
    goto :goto_2a

    #@9d
    .line 3487
    .end local v7           #i:I
    .end local v10           #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :catchall_9d
    move-exception v0

    #@9e
    if-eqz v8, :cond_a4

    #@a0
    .line 3488
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@a3
    .line 3489
    const/4 v8, 0x0

    #@a4
    :cond_a4
    throw v0
.end method

.method private ContactImageData_Update(JLcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;)Z
    .registers 15
    .parameter "_UriId"
    .parameter "objPresenceInfo"

    #@0
    .prologue
    .line 3407
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@2
    if-nez v9, :cond_6

    #@4
    .line 3408
    const/4 v9, 0x0

    #@5
    .line 3447
    :goto_5
    return v9

    #@6
    .line 3410
    :cond_6
    iget-object v9, p3, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIcon:Ljava/lang/String;

    #@8
    invoke-static {v9}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@b
    move-result-object v5

    #@c
    .line 3411
    .local v5, photo:Landroid/graphics/Bitmap;
    if-nez v5, :cond_10

    #@e
    .line 3412
    const/4 v9, 0x0

    #@f
    goto :goto_5

    #@10
    .line 3415
    :cond_10
    :try_start_10
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    #@13
    move-result v9

    #@14
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    #@17
    move-result v10

    #@18
    mul-int/2addr v9, v10

    #@19
    mul-int/lit8 v6, v9, 0x4

    #@1b
    .line 3416
    .local v6, size:I
    if-gez v6, :cond_1f

    #@1d
    .line 3417
    const/4 v9, 0x0

    #@1e
    goto :goto_5

    #@1f
    .line 3419
    :cond_1f
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    #@21
    invoke-direct {v4, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@24
    .line 3420
    .local v4, out:Ljava/io/ByteArrayOutputStream;
    if-nez v4, :cond_28

    #@26
    .line 3421
    const/4 v9, 0x0

    #@27
    goto :goto_5

    #@28
    .line 3423
    :cond_28
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@2a
    const/16 v10, 0x5f

    #@2c
    invoke-virtual {v5, v9, v10, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@2f
    move-result v1

    #@30
    .line 3424
    .local v1, compressed:Z
    if-nez v1, :cond_53

    #@32
    .line 3425
    new-instance v9, Ljava/io/IOException;

    #@34
    const-string v10, "Unable to compress image"

    #@36
    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@39
    throw v9
    :try_end_3a
    .catchall {:try_start_10 .. :try_end_3a} :catchall_bf
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_3a} :catch_3a

    #@3a
    .line 3443
    .end local v1           #compressed:Z
    .end local v4           #out:Ljava/io/ByteArrayOutputStream;
    .end local v6           #size:I
    :catch_3a
    move-exception v2

    #@3b
    .line 3444
    .local v2, e:Ljava/lang/Exception;
    :try_start_3b
    new-instance v9, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v10, "[IP][ERROR]exception : "

    #@42
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v9

    #@46
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v9

    #@4a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v9

    #@4e
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_51
    .catchall {:try_start_3b .. :try_end_51} :catchall_bf

    #@51
    .line 3447
    const/4 v9, 0x1

    #@52
    goto :goto_5

    #@53
    .line 3427
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v1       #compressed:Z
    .restart local v4       #out:Ljava/io/ByteArrayOutputStream;
    .restart local v6       #size:I
    :cond_53
    :try_start_53
    new-instance v3, Landroid/content/ContentValues;

    #@55
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@58
    .line 3429
    .local v3, mValues:Landroid/content/ContentValues;
    const-string v7, "_id=? AND mimetype=?"

    #@5a
    .line 3430
    .local v7, strContactSelection:Ljava/lang/String;
    const/4 v9, 0x2

    #@5b
    new-array v0, v9, [Ljava/lang/String;

    #@5d
    const/4 v9, 0x0

    #@5e
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@61
    move-result-object v10

    #@62
    aput-object v10, v0, v9

    #@64
    const/4 v9, 0x1

    #@65
    const-string v10, "vnd.android.cursor.item/photo"

    #@67
    aput-object v10, v0, v9

    #@69
    .line 3434
    .local v0, astrContactSelectionArg:[Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v10, "[IP]: nUriId ["

    #@70
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v9

    #@74
    invoke-virtual {v9, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@77
    move-result-object v9

    #@78
    const-string v10, "]"

    #@7a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v9

    #@7e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v9

    #@82
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@85
    .line 3435
    const-string v9, "data15"

    #@87
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@8a
    move-result-object v10

    #@8b
    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@8e
    .line 3437
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@90
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@93
    move-result-object v9

    #@94
    sget-object v10, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@96
    invoke-virtual {v9, v10, v3, v7, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@99
    move-result v8

    #@9a
    .line 3438
    .local v8, update:I
    const/4 v9, -0x1

    #@9b
    if-ne v8, v9, :cond_b9

    #@9d
    .line 3439
    new-instance v9, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v10, "[IP][ERROR]ContactImageData_Update : update is failed ["

    #@a4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v9

    #@a8
    invoke-virtual {v9, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v9

    #@ac
    const-string v10, "]"

    #@ae
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v9

    #@b2
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b5
    move-result-object v9

    #@b6
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@b9
    .line 3441
    :cond_b9
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V
    :try_end_bc
    .catchall {:try_start_53 .. :try_end_bc} :catchall_bf
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_bc} :catch_3a

    #@bc
    .line 3442
    const/4 v9, 0x1

    #@bd
    goto/16 :goto_5

    #@bf
    .line 3445
    .end local v0           #astrContactSelectionArg:[Ljava/lang/String;
    .end local v1           #compressed:Z
    .end local v3           #mValues:Landroid/content/ContentValues;
    .end local v4           #out:Ljava/io/ByteArrayOutputStream;
    .end local v6           #size:I
    .end local v7           #strContactSelection:Ljava/lang/String;
    .end local v8           #update:I
    :catchall_bf
    move-exception v9

    #@c0
    throw v9
.end method

.method private ContactImageData_Update(JLcom/lge/ims/service/ip/PresenceInfo;)Z
    .registers 15
    .parameter "_UriId"
    .parameter "objPresenceInfo"

    #@0
    .prologue
    .line 3364
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@2
    if-nez v9, :cond_6

    #@4
    .line 3365
    const/4 v9, 0x0

    #@5
    .line 3404
    :goto_5
    return v9

    #@6
    .line 3367
    :cond_6
    invoke-virtual {p3}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIcon()Ljava/lang/String;

    #@9
    move-result-object v9

    #@a
    invoke-static {v9}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@d
    move-result-object v5

    #@e
    .line 3368
    .local v5, photo:Landroid/graphics/Bitmap;
    if-nez v5, :cond_12

    #@10
    .line 3369
    const/4 v9, 0x0

    #@11
    goto :goto_5

    #@12
    .line 3372
    :cond_12
    :try_start_12
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    #@15
    move-result v9

    #@16
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    #@19
    move-result v10

    #@1a
    mul-int/2addr v9, v10

    #@1b
    mul-int/lit8 v6, v9, 0x4

    #@1d
    .line 3373
    .local v6, size:I
    if-gez v6, :cond_21

    #@1f
    .line 3374
    const/4 v9, 0x0

    #@20
    goto :goto_5

    #@21
    .line 3376
    :cond_21
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    #@23
    invoke-direct {v4, v6}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    #@26
    .line 3377
    .local v4, out:Ljava/io/ByteArrayOutputStream;
    if-nez v4, :cond_2a

    #@28
    .line 3378
    const/4 v9, 0x0

    #@29
    goto :goto_5

    #@2a
    .line 3380
    :cond_2a
    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@2c
    const/16 v10, 0x5f

    #@2e
    invoke-virtual {v5, v9, v10, v4}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@31
    move-result v1

    #@32
    .line 3381
    .local v1, compressed:Z
    if-nez v1, :cond_55

    #@34
    .line 3382
    new-instance v9, Ljava/io/IOException;

    #@36
    const-string v10, "Unable to compress image"

    #@38
    invoke-direct {v9, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@3b
    throw v9
    :try_end_3c
    .catchall {:try_start_12 .. :try_end_3c} :catchall_c1
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_3c} :catch_3c

    #@3c
    .line 3400
    .end local v1           #compressed:Z
    .end local v4           #out:Ljava/io/ByteArrayOutputStream;
    .end local v6           #size:I
    :catch_3c
    move-exception v2

    #@3d
    .line 3401
    .local v2, e:Ljava/lang/Exception;
    :try_start_3d
    new-instance v9, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v10, "[IP][ERROR]exception : "

    #@44
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v9

    #@48
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v9

    #@4c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v9

    #@50
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_53
    .catchall {:try_start_3d .. :try_end_53} :catchall_c1

    #@53
    .line 3404
    const/4 v9, 0x1

    #@54
    goto :goto_5

    #@55
    .line 3384
    .end local v2           #e:Ljava/lang/Exception;
    .restart local v1       #compressed:Z
    .restart local v4       #out:Ljava/io/ByteArrayOutputStream;
    .restart local v6       #size:I
    :cond_55
    :try_start_55
    new-instance v3, Landroid/content/ContentValues;

    #@57
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@5a
    .line 3386
    .local v3, mValues:Landroid/content/ContentValues;
    const-string v7, "_id=? AND mimetype=?"

    #@5c
    .line 3387
    .local v7, strContactSelection:Ljava/lang/String;
    const/4 v9, 0x2

    #@5d
    new-array v0, v9, [Ljava/lang/String;

    #@5f
    const/4 v9, 0x0

    #@60
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@63
    move-result-object v10

    #@64
    aput-object v10, v0, v9

    #@66
    const/4 v9, 0x1

    #@67
    const-string v10, "vnd.android.cursor.item/photo"

    #@69
    aput-object v10, v0, v9

    #@6b
    .line 3391
    .local v0, astrContactSelectionArg:[Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v10, "[IP]: nUriId ["

    #@72
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v9

    #@76
    invoke-virtual {v9, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@79
    move-result-object v9

    #@7a
    const-string v10, "]"

    #@7c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v9

    #@80
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v9

    #@84
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@87
    .line 3392
    const-string v9, "data15"

    #@89
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@8c
    move-result-object v10

    #@8d
    invoke-virtual {v3, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@90
    .line 3394
    iget-object v9, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@92
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@95
    move-result-object v9

    #@96
    sget-object v10, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@98
    invoke-virtual {v9, v10, v3, v7, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@9b
    move-result v8

    #@9c
    .line 3395
    .local v8, update:I
    const/4 v9, -0x1

    #@9d
    if-ne v8, v9, :cond_bb

    #@9f
    .line 3396
    new-instance v9, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v10, "[IP][ERROR]ContactImageData_Update : update is failed ["

    #@a6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v9

    #@aa
    invoke-virtual {v9, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v9

    #@ae
    const-string v10, "]"

    #@b0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v9

    #@b4
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v9

    #@b8
    invoke-static {v9}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@bb
    .line 3398
    :cond_bb
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V
    :try_end_be
    .catchall {:try_start_55 .. :try_end_be} :catchall_c1
    .catch Ljava/lang/Exception; {:try_start_55 .. :try_end_be} :catch_3c

    #@be
    .line 3399
    const/4 v9, 0x1

    #@bf
    goto/16 :goto_5

    #@c1
    .line 3402
    .end local v0           #astrContactSelectionArg:[Ljava/lang/String;
    .end local v1           #compressed:Z
    .end local v3           #mValues:Landroid/content/ContentValues;
    .end local v4           #out:Ljava/io/ByteArrayOutputStream;
    .end local v6           #size:I
    .end local v7           #strContactSelection:Ljava/lang/String;
    .end local v8           #update:I
    :catchall_c1
    move-exception v9

    #@c2
    throw v9
.end method

.method private ContactKeepingImageData_Update(Z)Z
    .registers 19
    .parameter "bIgnorePriority"

    #@0
    .prologue
    .line 3495
    const-string v5, "statusicon_update=? "

    #@2
    .line 3496
    .local v5, selection:Ljava/lang/String;
    const/4 v2, 0x1

    #@3
    new-array v6, v2, [Ljava/lang/String;

    #@5
    const/4 v2, 0x0

    #@6
    const/4 v3, 0x2

    #@7
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@a
    move-result-object v3

    #@b
    aput-object v3, v6, v2

    #@d
    .line 3498
    .local v6, selectionArgs:[Ljava/lang/String;
    const/4 v12, 0x0

    #@e
    .line 3500
    .local v12, objCursor:Landroid/database/Cursor;
    :try_start_e
    move-object/from16 v0, p0

    #@10
    iget-object v2, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@12
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@15
    move-result-object v2

    #@16
    sget-object v3, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@18
    sget-object v4, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_PRESENCE_PROJECTION:[Ljava/lang/String;

    #@1a
    const/4 v7, 0x0

    #@1b
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1e
    move-result-object v12

    #@1f
    .line 3506
    if-nez v12, :cond_2e

    #@21
    .line 3507
    const-string v2, "[IP][ERROR] objCursor is null"

    #@23
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_26
    .catchall {:try_start_e .. :try_end_26} :catchall_fc
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_26} :catch_45

    #@26
    .line 3508
    const/4 v2, 0x0

    #@27
    .line 3542
    if-eqz v12, :cond_2d

    #@29
    .line 3543
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@2c
    .line 3544
    const/4 v12, 0x0

    #@2d
    .line 3547
    :cond_2d
    :goto_2d
    return v2

    #@2e
    .line 3510
    :cond_2e
    :try_start_2e
    new-instance v14, Ljava/util/LinkedList;

    #@30
    invoke-direct {v14}, Ljava/util/LinkedList;-><init>()V

    #@33
    .line 3511
    .local v14, objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :cond_33
    :goto_33
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    #@36
    move-result v2

    #@37
    if-eqz v2, :cond_64

    #@39
    .line 3512
    move-object/from16 v0, p0

    #@3b
    invoke-direct {v0, v12}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@3e
    move-result-object v13

    #@3f
    .line 3513
    .local v13, objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-eqz v13, :cond_33

    #@41
    .line 3516
    invoke-virtual {v14, v13}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_44
    .catchall {:try_start_2e .. :try_end_44} :catchall_fc
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_44} :catch_45

    #@44
    goto :goto_33

    #@45
    .line 3539
    .end local v13           #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    .end local v14           #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :catch_45
    move-exception v9

    #@46
    .line 3540
    .local v9, e:Ljava/lang/Exception;
    :try_start_46
    new-instance v2, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v3, "[IP][ERROR]exception : "

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v2

    #@55
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v2

    #@59
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_5c
    .catchall {:try_start_46 .. :try_end_5c} :catchall_fc

    #@5c
    .line 3542
    if-eqz v12, :cond_62

    #@5e
    .line 3543
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@61
    .line 3544
    const/4 v12, 0x0

    #@62
    .line 3547
    :cond_62
    const/4 v2, 0x1

    #@63
    goto :goto_2d

    #@64
    .line 3520
    .end local v9           #e:Ljava/lang/Exception;
    .restart local v14       #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :cond_64
    :try_start_64
    new-instance v2, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v3, "[IP]list size : ["

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    invoke-virtual {v14}, Ljava/util/LinkedList;->size()I

    #@72
    move-result v3

    #@73
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v2

    #@77
    const-string v3, "]"

    #@79
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v2

    #@7d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v2

    #@81
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@84
    .line 3521
    const/4 v10, 0x0

    #@85
    .local v10, i:I
    :goto_85
    invoke-virtual {v14}, Ljava/util/LinkedList;->size()I

    #@88
    move-result v2

    #@89
    if-ge v10, v2, :cond_f3

    #@8b
    .line 3522
    invoke-virtual {v14, v10}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@8e
    move-result-object v13

    #@8f
    check-cast v13, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@91
    .line 3523
    .restart local v13       #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-eqz v13, :cond_f0

    #@93
    .line 3524
    const/4 v2, 0x0

    #@94
    move-object/from16 v0, p0

    #@96
    move/from16 v1, p1

    #@98
    invoke-direct {v0, v13, v1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->SyncFullImageToContact(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;ZZ)Z

    #@9b
    move-result v2

    #@9c
    const/4 v3, 0x1

    #@9d
    if-ne v2, v3, :cond_f0

    #@9f
    .line 3525
    const-string v15, "_id=?"

    #@a1
    .line 3526
    .local v15, strSelection:Ljava/lang/String;
    const/4 v2, 0x1

    #@a2
    new-array v8, v2, [Ljava/lang/String;

    #@a4
    const/4 v2, 0x0

    #@a5
    iget-wide v3, v13, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nId:J

    #@a7
    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@aa
    move-result-object v3

    #@ab
    aput-object v3, v8, v2

    #@ad
    .line 3527
    .local v8, astrSelectionArg:[Ljava/lang/String;
    new-instance v11, Landroid/content/ContentValues;

    #@af
    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    #@b2
    .line 3528
    .local v11, objContentValues:Landroid/content/ContentValues;
    const-string v2, "statusicon_update"

    #@b4
    const/4 v3, 0x1

    #@b5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b8
    move-result-object v3

    #@b9
    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@bc
    .line 3529
    move-object/from16 v0, p0

    #@be
    iget-object v2, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@c0
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c3
    move-result-object v2

    #@c4
    sget-object v3, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@c6
    invoke-virtual {v2, v3, v11, v15, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@c9
    move-result v16

    #@ca
    .line 3530
    .local v16, update:I
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    #@cd
    .line 3531
    const/4 v2, -0x1

    #@ce
    move/from16 v0, v16

    #@d0
    if-ne v0, v2, :cond_f0

    #@d2
    .line 3532
    new-instance v2, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v3, "[IP][ERROR] status icon update failed [ "

    #@d9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v2

    #@dd
    iget-object v3, v13, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@df
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v2

    #@e3
    const-string v3, " ]"

    #@e5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v2

    #@e9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v2

    #@ed
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_f0
    .catchall {:try_start_64 .. :try_end_f0} :catchall_fc
    .catch Ljava/lang/Exception; {:try_start_64 .. :try_end_f0} :catch_45

    #@f0
    .line 3521
    .end local v8           #astrSelectionArg:[Ljava/lang/String;
    .end local v11           #objContentValues:Landroid/content/ContentValues;
    .end local v15           #strSelection:Ljava/lang/String;
    .end local v16           #update:I
    :cond_f0
    add-int/lit8 v10, v10, 0x1

    #@f2
    goto :goto_85

    #@f3
    .line 3537
    .end local v13           #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    :cond_f3
    const/4 v2, 0x1

    #@f4
    .line 3542
    if-eqz v12, :cond_2d

    #@f6
    .line 3543
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@f9
    .line 3544
    const/4 v12, 0x0

    #@fa
    goto/16 :goto_2d

    #@fc
    .line 3542
    .end local v10           #i:I
    .end local v14           #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :catchall_fc
    move-exception v2

    #@fd
    if-eqz v12, :cond_103

    #@ff
    .line 3543
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@102
    .line 3544
    const/4 v12, 0x0

    #@103
    :cond_103
    throw v2
.end method

.method public static CreateInstance(Landroid/content/Context;)V
    .registers 2
    .parameter "_objContext"

    #@0
    .prologue
    .line 233
    sget-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->objInstance:Lcom/lge/ims/service/ip/IPContactHelper;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 234
    new-instance v0, Lcom/lge/ims/service/ip/IPContactHelper;

    #@6
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPContactHelper;-><init>(Landroid/content/Context;)V

    #@9
    sput-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->objInstance:Lcom/lge/ims/service/ip/IPContactHelper;

    #@b
    .line 236
    :cond_b
    return-void
.end method

.method public static DestroyContactHelper()V
    .registers 1

    #@0
    .prologue
    .line 242
    sget-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->objInstance:Lcom/lge/ims/service/ip/IPContactHelper;

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 243
    const/4 v0, 0x0

    #@5
    sput-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->objInstance:Lcom/lge/ims/service/ip/IPContactHelper;

    #@7
    .line 245
    :cond_7
    return-void
.end method

.method private GetContactImageId(J)J
    .registers 14
    .parameter "raw_contact_id"

    #@0
    .prologue
    .line 2442
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP] raw_contact_id : ["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "]"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1c
    .line 2443
    const-string v3, "raw_contact_id=? AND mimetype=?"

    #@1e
    .line 2444
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x2

    #@1f
    new-array v4, v0, [Ljava/lang/String;

    #@21
    const/4 v0, 0x0

    #@22
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    aput-object v1, v4, v0

    #@28
    const/4 v0, 0x1

    #@29
    const-string v1, "vnd.android.cursor.item/photo"

    #@2b
    aput-object v1, v4, v0

    #@2d
    .line 2446
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@2f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@32
    move-result-object v0

    #@33
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@35
    const/4 v2, 0x4

    #@36
    new-array v2, v2, [Ljava/lang/String;

    #@38
    const/4 v5, 0x0

    #@39
    const-string v10, "_id"

    #@3b
    aput-object v10, v2, v5

    #@3d
    const/4 v5, 0x1

    #@3e
    const-string v10, "raw_contact_id"

    #@40
    aput-object v10, v2, v5

    #@42
    const/4 v5, 0x2

    #@43
    const-string v10, "mimetype"

    #@45
    aput-object v10, v2, v5

    #@47
    const/4 v5, 0x3

    #@48
    const-string v10, "data10"

    #@4a
    aput-object v10, v2, v5

    #@4c
    const/4 v5, 0x0

    #@4d
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@50
    move-result-object v9

    #@51
    .line 2453
    .local v9, objCursor:Landroid/database/Cursor;
    if-eqz v9, :cond_59

    #@53
    :try_start_53
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@56
    move-result v0

    #@57
    if-nez v0, :cond_7e

    #@59
    .line 2454
    :cond_59
    new-instance v0, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v1, "[IP][ERROR]GetContactImageId : Not found ["

    #@60
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@67
    move-result-object v0

    #@68
    const-string v1, "]"

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v0

    #@72
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_75
    .catchall {:try_start_53 .. :try_end_75} :catchall_cb
    .catch Ljava/lang/Exception; {:try_start_53 .. :try_end_75} :catch_ab

    #@75
    .line 2455
    const-wide/16 v6, -0x1

    #@77
    .line 2464
    if-eqz v9, :cond_7d

    #@79
    .line 2465
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@7c
    .line 2466
    const/4 v9, 0x0

    #@7d
    .line 2469
    :cond_7d
    :goto_7d
    return-wide v6

    #@7e
    .line 2457
    :cond_7e
    :try_start_7e
    const-string v0, "_id"

    #@80
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@83
    move-result v0

    #@84
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    #@87
    move-result-wide v6

    #@88
    .line 2458
    .local v6, _id:J
    new-instance v0, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    const-string v1, "[IP]data id [ "

    #@8f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v0

    #@93
    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@96
    move-result-object v0

    #@97
    const-string v1, " ]"

    #@99
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v0

    #@9d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v0

    #@a1
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_a4
    .catchall {:try_start_7e .. :try_end_a4} :catchall_cb
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_a4} :catch_ab

    #@a4
    .line 2464
    if-eqz v9, :cond_7d

    #@a6
    .line 2465
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@a9
    .line 2466
    const/4 v9, 0x0

    #@aa
    goto :goto_7d

    #@ab
    .line 2461
    .end local v6           #_id:J
    :catch_ab
    move-exception v8

    #@ac
    .line 2462
    .local v8, e:Ljava/lang/Exception;
    :try_start_ac
    new-instance v0, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v1, "[IP][ERROR]exception : "

    #@b3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v0

    #@b7
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v0

    #@bb
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v0

    #@bf
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_c2
    .catchall {:try_start_ac .. :try_end_c2} :catchall_cb

    #@c2
    .line 2464
    if-eqz v9, :cond_c8

    #@c4
    .line 2465
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@c7
    .line 2466
    const/4 v9, 0x0

    #@c8
    .line 2469
    :cond_c8
    const-wide/16 v6, -0x1

    #@ca
    goto :goto_7d

    #@cb
    .line 2464
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_cb
    move-exception v0

    #@cc
    if-eqz v9, :cond_d2

    #@ce
    .line 2465
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@d1
    .line 2466
    const/4 v9, 0x0

    #@d2
    :cond_d2
    throw v0
.end method

.method public static GetInstance()Lcom/lge/ims/service/ip/IPContactHelper;
    .registers 1

    #@0
    .prologue
    .line 238
    sget-object v0, Lcom/lge/ims/service/ip/IPContactHelper;->objInstance:Lcom/lge/ims/service/ip/IPContactHelper;

    #@2
    return-object v0
.end method

.method private GetMyStatusData()Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 2412
    const/4 v8, 0x0

    #@2
    .line 2413
    .local v8, objRCSMyStatusData:Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;
    const/4 v7, 0x0

    #@3
    .line 2415
    .local v7, objCursor:Landroid/database/Cursor;
    :try_start_3
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@5
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v0

    #@9
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_CONTENT_URI:Landroid/net/Uri;

    #@b
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_MYSTATUS_PROJECTION:[Ljava/lang/String;

    #@d
    const/4 v3, 0x0

    #@e
    const/4 v4, 0x0

    #@f
    const/4 v5, 0x0

    #@10
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@13
    move-result-object v7

    #@14
    .line 2421
    if-nez v7, :cond_23

    #@16
    .line 2422
    const-string v0, "[IP][ERROR]GetMyStatusData : Can\'t search MyStatus Info"

    #@18
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_61
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_1b} :catch_42

    #@1b
    .line 2434
    if-eqz v7, :cond_21

    #@1d
    .line 2435
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@20
    .line 2436
    const/4 v7, 0x0

    #@21
    :cond_21
    move-object v0, v9

    #@22
    .line 2439
    :goto_22
    return-object v0

    #@23
    .line 2424
    :cond_23
    :try_start_23
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@26
    move-result v0

    #@27
    if-nez v0, :cond_36

    #@29
    .line 2425
    const-string v0, "[IP][ERROR]GetMyStatusData : cursor move to next is false"

    #@2b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_2e
    .catchall {:try_start_23 .. :try_end_2e} :catchall_61
    .catch Ljava/lang/Exception; {:try_start_23 .. :try_end_2e} :catch_42

    #@2e
    .line 2434
    if-eqz v7, :cond_34

    #@30
    .line 2435
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@33
    .line 2436
    const/4 v7, 0x0

    #@34
    :cond_34
    move-object v0, v9

    #@35
    goto :goto_22

    #@36
    .line 2428
    :cond_36
    :try_start_36
    invoke-direct {p0, v7}, Lcom/lge/ims/service/ip/IPContactHelper;->GetMyStatusDataByCursor(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;
    :try_end_39
    .catchall {:try_start_36 .. :try_end_39} :catchall_61
    .catch Ljava/lang/Exception; {:try_start_36 .. :try_end_39} :catch_42

    #@39
    move-result-object v8

    #@3a
    .line 2434
    if-eqz v7, :cond_40

    #@3c
    .line 2435
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@3f
    .line 2436
    const/4 v7, 0x0

    #@40
    :cond_40
    move-object v0, v8

    #@41
    goto :goto_22

    #@42
    .line 2431
    :catch_42
    move-exception v6

    #@43
    .line 2432
    .local v6, e:Ljava/lang/Exception;
    :try_start_43
    new-instance v0, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v1, "[IP][ERROR]exception : "

    #@4a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v0

    #@4e
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v0

    #@52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v0

    #@56
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_59
    .catchall {:try_start_43 .. :try_end_59} :catchall_61

    #@59
    .line 2434
    if-eqz v7, :cond_5f

    #@5b
    .line 2435
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@5e
    .line 2436
    const/4 v7, 0x0

    #@5f
    :cond_5f
    move-object v0, v9

    #@60
    .line 2439
    goto :goto_22

    #@61
    .line 2434
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_61
    move-exception v0

    #@62
    if-eqz v7, :cond_68

    #@64
    .line 2435
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@67
    .line 2436
    const/4 v7, 0x0

    #@68
    :cond_68
    throw v0
.end method

.method private GetMyStatusDataByCursor(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;
    .registers 4
    .parameter "cursor"

    #@0
    .prologue
    .line 2384
    const-string v1, "[IP]"

    #@2
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 2386
    if-nez p1, :cond_e

    #@7
    .line 2387
    const-string v1, "[IP][ERROR]Cursor is null"

    #@9
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@c
    .line 2388
    const/4 v0, 0x0

    #@d
    .line 2409
    :cond_d
    :goto_d
    return-object v0

    #@e
    .line 2391
    :cond_e
    new-instance v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;

    #@10
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;-><init>(Lcom/lge/ims/service/ip/IPContactHelper;)V

    #@13
    .line 2392
    .local v0, obj:Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;
    if-eqz v0, :cond_d

    #@15
    .line 2393
    const/4 v1, 0x0

    #@16
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strHomepage:Ljava/lang/String;

    #@1c
    .line 2394
    const/4 v1, 0x1

    #@1d
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strFreeText:Ljava/lang/String;

    #@23
    .line 2395
    const/4 v1, 0x2

    #@24
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strEMailAddress:Ljava/lang/String;

    #@2a
    .line 2396
    const/4 v1, 0x3

    #@2b
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strBirthDay:Ljava/lang/String;

    #@31
    .line 2397
    const/4 v1, 0x4

    #@32
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@35
    move-result v1

    #@36
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->nStatus:I

    #@38
    .line 2398
    const/4 v1, 0x5

    #@39
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strTwitterAccount:Ljava/lang/String;

    #@3f
    .line 2399
    const/4 v1, 0x6

    #@40
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strFaceBookAccount:Ljava/lang/String;

    #@46
    .line 2400
    const/4 v1, 0x7

    #@47
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strCyworldAccount:Ljava/lang/String;

    #@4d
    .line 2401
    const/16 v1, 0x8

    #@4f
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strWaggleAccount:Ljava/lang/String;

    #@55
    .line 2402
    const/16 v1, 0x9

    #@57
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIcon:Ljava/lang/String;

    #@5d
    .line 2403
    const/16 v1, 0xa

    #@5f
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@62
    move-result-object v1

    #@63
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconLink:Ljava/lang/String;

    #@65
    .line 2404
    const/16 v1, 0xb

    #@67
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    #@6a
    move-result-object v1

    #@6b
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconThumb:[B

    #@6d
    .line 2405
    const/16 v1, 0xc

    #@6f
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconThumbLink:Ljava/lang/String;

    #@75
    .line 2406
    const/16 v1, 0xd

    #@77
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@7a
    move-result-object v1

    #@7b
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconThumbEtag:Ljava/lang/String;

    #@7d
    .line 2407
    const/16 v1, 0xe

    #@7f
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@82
    move-result-object v1

    #@83
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strDisplayName:Ljava/lang/String;

    #@85
    goto :goto_d
.end method

.method private GetPresenceContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    .registers 5
    .parameter "cursor"

    #@0
    .prologue
    .line 2286
    if-nez p1, :cond_9

    #@2
    .line 2287
    const-string v1, "[IP][ERROR]Cursor is null"

    #@4
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@7
    .line 2288
    const/4 v0, 0x0

    #@8
    .line 2312
    :cond_8
    :goto_8
    return-object v0

    #@9
    .line 2291
    :cond_9
    new-instance v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@b
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;-><init>(Lcom/lge/ims/service/ip/IPContactHelper;)V

    #@e
    .line 2292
    .local v0, obj:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-eqz v0, :cond_8

    #@10
    .line 2293
    const/4 v1, 0x0

    #@11
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    #@14
    move-result-wide v1

    #@15
    iput-wide v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nId:J

    #@17
    .line 2294
    const/4 v1, 0x1

    #@18
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@1e
    .line 2295
    const/4 v1, 0x2

    #@1f
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strHomepage:Ljava/lang/String;

    #@25
    .line 2296
    const/4 v1, 0x3

    #@26
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strFreeText:Ljava/lang/String;

    #@2c
    .line 2297
    const/4 v1, 0x4

    #@2d
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strEMailAddress:Ljava/lang/String;

    #@33
    .line 2298
    const/4 v1, 0x5

    #@34
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strBirthDay:Ljava/lang/String;

    #@3a
    .line 2299
    const/4 v1, 0x6

    #@3b
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@3e
    move-result v1

    #@3f
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nStatus:I

    #@41
    .line 2300
    const/4 v1, 0x7

    #@42
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strTwitterAccount:Ljava/lang/String;

    #@48
    .line 2301
    const/16 v1, 0x8

    #@4a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strFaceBookAccount:Ljava/lang/String;

    #@50
    .line 2302
    const/16 v1, 0x9

    #@52
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@55
    move-result-object v1

    #@56
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strCyworldAccount:Ljava/lang/String;

    #@58
    .line 2303
    const/16 v1, 0xa

    #@5a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@5d
    move-result-object v1

    #@5e
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strWaggleAccount:Ljava/lang/String;

    #@60
    .line 2304
    const/16 v1, 0xb

    #@62
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIcon:Ljava/lang/String;

    #@68
    .line 2305
    const/16 v1, 0xc

    #@6a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@6d
    move-result-object v1

    #@6e
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconLink:Ljava/lang/String;

    #@70
    .line 2306
    const/16 v1, 0xd

    #@72
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getBlob(I)[B

    #@75
    move-result-object v1

    #@76
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconThumb:[B

    #@78
    .line 2307
    const/16 v1, 0xe

    #@7a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@7d
    move-result-object v1

    #@7e
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconThumbLink:Ljava/lang/String;

    #@80
    .line 2308
    const/16 v1, 0xf

    #@82
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@85
    move-result-object v1

    #@86
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconThumbEtag:Ljava/lang/String;

    #@88
    .line 2309
    const/16 v1, 0x10

    #@8a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    #@8d
    move-result-wide v1

    #@8e
    iput-wide v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nTimeStamp:J

    #@90
    .line 2310
    const/16 v1, 0x11

    #@92
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@95
    move-result v1

    #@96
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nStatusIconUpdateToContact:I

    #@98
    goto/16 :goto_8
.end method

.method private GetPresenceContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    .registers 13
    .parameter "strMSISDN"
    .parameter "strOrder"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 2316
    if-eqz p1, :cond_9

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 2317
    :cond_9
    const-string v0, "[IP][ERROR]GetPresenceContactDataByMSISDN : MSISDN is null or Empty"

    #@b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e
    move-object v8, v9

    #@f
    .line 2356
    :cond_f
    :goto_f
    return-object v8

    #@10
    .line 2320
    :cond_10
    new-instance v0, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v1, "[IP]GetPresenceContactDataByMSISDN : msisn - "

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@26
    .line 2322
    const-string v3, "nor_msisdn=?"

    #@28
    .line 2323
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@29
    new-array v4, v0, [Ljava/lang/String;

    #@2b
    const/4 v0, 0x0

    #@2c
    aput-object p1, v4, v0

    #@2e
    .line 2324
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    const/4 v8, 0x0

    #@2f
    .line 2325
    .local v8, objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    const/4 v7, 0x0

    #@30
    .line 2328
    .local v7, objCursor:Landroid/database/Cursor;
    :try_start_30
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@32
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@35
    move-result-object v0

    #@36
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@38
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_PRESENCE_PROJECTION:[Ljava/lang/String;

    #@3a
    move-object v5, p2

    #@3b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3e
    move-result-object v7

    #@3f
    .line 2334
    if-nez v7, :cond_4e

    #@41
    .line 2335
    const-string v0, "[IP][ERROR]Can\'t search RCS Contact"

    #@43
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_46
    .catchall {:try_start_30 .. :try_end_46} :catchall_88
    .catch Ljava/lang/Exception; {:try_start_30 .. :try_end_46} :catch_69

    #@46
    .line 2351
    if-eqz v7, :cond_4c

    #@48
    .line 2352
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@4b
    .line 2353
    const/4 v7, 0x0

    #@4c
    :cond_4c
    move-object v8, v9

    #@4d
    goto :goto_f

    #@4e
    .line 2338
    :cond_4e
    :try_start_4e
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@51
    move-result v0

    #@52
    if-eqz v0, :cond_62

    #@54
    .line 2339
    invoke-direct {p0, v7}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@57
    move-result-object v8

    #@58
    .line 2340
    if-eqz v8, :cond_4e

    #@5a
    .line 2341
    iget-object v0, v8, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@5c
    invoke-static {v0, p1}, Lcom/lge/ims/service/ip/IPUtil;->IsEqualsNumber(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_5f
    .catchall {:try_start_4e .. :try_end_5f} :catchall_88
    .catch Ljava/lang/Exception; {:try_start_4e .. :try_end_5f} :catch_69

    #@5f
    move-result v0

    #@60
    if-eqz v0, :cond_4e

    #@62
    .line 2351
    :cond_62
    if-eqz v7, :cond_f

    #@64
    .line 2352
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@67
    .line 2353
    const/4 v7, 0x0

    #@68
    goto :goto_f

    #@69
    .line 2348
    :catch_69
    move-exception v6

    #@6a
    .line 2349
    .local v6, e:Ljava/lang/Exception;
    :try_start_6a
    new-instance v0, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v1, "[IP][ERROR]exception : "

    #@71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v0

    #@75
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v0

    #@79
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v0

    #@7d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_80
    .catchall {:try_start_6a .. :try_end_80} :catchall_88

    #@80
    .line 2351
    if-eqz v7, :cond_86

    #@82
    .line 2352
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@85
    .line 2353
    const/4 v7, 0x0

    #@86
    :cond_86
    move-object v8, v9

    #@87
    .line 2356
    goto :goto_f

    #@88
    .line 2351
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_88
    move-exception v0

    #@89
    if-eqz v7, :cond_8f

    #@8b
    .line 2352
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@8e
    .line 2353
    const/4 v7, 0x0

    #@8f
    :cond_8f
    throw v0
.end method

.method private GetPresenceUserCount()I
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 2359
    const/4 v7, 0x0

    #@2
    .line 2361
    .local v7, objCursor:Landroid/database/Cursor;
    :try_start_2
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@a
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_PRESENCE_PROJECTION:[Ljava/lang/String;

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x0

    #@e
    const/4 v5, 0x0

    #@f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@12
    move-result-object v7

    #@13
    .line 2367
    if-nez v7, :cond_22

    #@15
    .line 2368
    const-string v0, "[IP][ERROR]Can\'t search Presence DB"

    #@17
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_1a
    .catchall {:try_start_2 .. :try_end_1a} :catchall_4c
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_1a} :catch_2d

    #@1a
    .line 2376
    if-eqz v7, :cond_20

    #@1c
    .line 2377
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@1f
    .line 2378
    const/4 v7, 0x0

    #@20
    :cond_20
    move v0, v8

    #@21
    .line 2381
    :cond_21
    :goto_21
    return v0

    #@22
    .line 2371
    :cond_22
    :try_start_22
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I
    :try_end_25
    .catchall {:try_start_22 .. :try_end_25} :catchall_4c
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_25} :catch_2d

    #@25
    move-result v0

    #@26
    .line 2376
    if-eqz v7, :cond_21

    #@28
    .line 2377
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@2b
    .line 2378
    const/4 v7, 0x0

    #@2c
    goto :goto_21

    #@2d
    .line 2373
    :catch_2d
    move-exception v6

    #@2e
    .line 2374
    .local v6, e:Ljava/lang/Exception;
    :try_start_2e
    new-instance v0, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v1, "[IP][ERROR]exception : "

    #@35
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_44
    .catchall {:try_start_2e .. :try_end_44} :catchall_4c

    #@44
    .line 2376
    if-eqz v7, :cond_4a

    #@46
    .line 2377
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@49
    .line 2378
    const/4 v7, 0x0

    #@4a
    :cond_4a
    move v0, v8

    #@4b
    .line 2381
    goto :goto_21

    #@4c
    .line 2376
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_4c
    move-exception v0

    #@4d
    if-eqz v7, :cond_53

    #@4f
    .line 2377
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@52
    .line 2378
    const/4 v7, 0x0

    #@53
    :cond_53
    throw v0
.end method

.method private GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .registers 5
    .parameter "cursor"

    #@0
    .prologue
    .line 2157
    if-nez p1, :cond_9

    #@2
    .line 2158
    const-string v1, "[IP][ERROR]Cursor is null"

    #@4
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@7
    .line 2159
    const/4 v0, 0x0

    #@8
    .line 2188
    :goto_8
    return-object v0

    #@9
    .line 2162
    :cond_9
    new-instance v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@b
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;-><init>(Lcom/lge/ims/service/ip/IPContactHelper;)V

    #@e
    .line 2163
    .local v0, obj:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v0, :cond_b2

    #@10
    .line 2164
    const/4 v1, 0x0

    #@11
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    #@14
    move-result-wide v1

    #@15
    iput-wide v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nId:J

    #@17
    .line 2165
    const/4 v1, 0x1

    #@18
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    #@1b
    move-result-wide v1

    #@1c
    iput-wide v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nDataId:J

    #@1e
    .line 2166
    const/4 v1, 0x2

    #@1f
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strMSISDN:Ljava/lang/String;

    #@25
    .line 2167
    const/4 v1, 0x3

    #@26
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@2c
    .line 2168
    const/4 v1, 0x4

    #@2d
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@30
    move-result v1

    #@31
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRCS:I

    #@33
    .line 2169
    const/4 v1, 0x5

    #@34
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@37
    move-result v1

    #@38
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsIM:I

    #@3a
    .line 2170
    const/4 v1, 0x6

    #@3b
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@3e
    move-result v1

    #@3f
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsFT:I

    #@41
    .line 2171
    const/4 v1, 0x7

    #@42
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    #@45
    move-result-wide v1

    #@46
    iput-wide v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nTimeStamp:J

    #@48
    .line 2172
    const/16 v1, 0x8

    #@4a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@4d
    move-result v1

    #@4e
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nCapaType:I

    #@50
    .line 2173
    const/16 v1, 0x9

    #@52
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@55
    move-result v1

    #@56
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nFirstTimeRcs:I

    #@58
    .line 2174
    const/16 v1, 0xa

    #@5a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@5d
    move-result v1

    #@5e
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsMIM:I

    #@60
    .line 2175
    const/16 v1, 0xb

    #@62
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@65
    move-result v1

    #@66
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsPresence:I

    #@68
    .line 2176
    const/16 v1, 0xc

    #@6a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    #@6d
    move-result-wide v1

    #@6e
    iput-wide v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nUriId:J

    #@70
    .line 2177
    const/16 v1, 0xd

    #@72
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@75
    move-result v1

    #@76
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsHTTP:I

    #@78
    .line 2178
    const/16 v1, 0xe

    #@7a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@7d
    move-result v1

    #@7e
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nResponsCode:I

    #@80
    .line 2179
    const/16 v1, 0xf

    #@82
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@85
    move-result v1

    #@86
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRcsUserList:I

    #@88
    .line 2180
    const/16 v1, 0x10

    #@8a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@8d
    move-result v1

    #@8e
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsNonRcsUserList:I

    #@90
    .line 2181
    const/16 v1, 0x11

    #@92
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    #@95
    move-result-wide v1

    #@96
    iput-wide v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@98
    .line 2182
    const/16 v1, 0x12

    #@9a
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    #@9d
    move-result-wide v1

    #@9e
    iput-wide v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@a0
    .line 2183
    const/16 v1, 0x13

    #@a2
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    #@a5
    move-result v1

    #@a6
    iput v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nPhotoUpdate:I

    #@a8
    .line 2184
    const/16 v1, 0x14

    #@aa
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@ad
    move-result-object v1

    #@ae
    iput-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strDisplayName:Ljava/lang/String;

    #@b0
    goto/16 :goto_8

    #@b2
    .line 2186
    :cond_b2
    const-string v1, "[IP][ERROR]IPCapaContactData is null"

    #@b4
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@b7
    goto/16 :goto_8
.end method

.method private GetRCSContactDataByDataId(JJLjava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .registers 16
    .parameter "_id"
    .parameter "raw_contact_id"
    .parameter "strOrder"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 2231
    const-string v3, "data_id=? AND raw_contact_id=?"

    #@3
    .line 2232
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x2

    #@4
    new-array v4, v0, [Ljava/lang/String;

    #@6
    const/4 v0, 0x0

    #@7
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    aput-object v1, v4, v0

    #@d
    const/4 v0, 0x1

    #@e
    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    aput-object v1, v4, v0

    #@14
    .line 2233
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    const/4 v7, 0x0

    #@15
    .line 2234
    .local v7, objCursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    #@16
    .line 2236
    .local v8, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :try_start_16
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@18
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b
    move-result-object v0

    #@1c
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@1e
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@20
    move-object v5, p5

    #@21
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@24
    move-result-object v7

    #@25
    .line 2242
    if-nez v7, :cond_34

    #@27
    .line 2243
    const-string v0, "[IP][ERROR]GetRCSContactDataByDataId : Can\'t search RCS Contact"

    #@29
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_2c
    .catchall {:try_start_16 .. :try_end_2c} :catchall_6d
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_2c} :catch_4e

    #@2c
    .line 2255
    if-eqz v7, :cond_32

    #@2e
    .line 2256
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@31
    .line 2257
    const/4 v7, 0x0

    #@32
    :cond_32
    move-object v0, v9

    #@33
    .line 2260
    :goto_33
    return-object v0

    #@34
    .line 2246
    :cond_34
    :try_start_34
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_46

    #@3a
    .line 2247
    invoke-direct {p0, v7}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :try_end_3d
    .catchall {:try_start_34 .. :try_end_3d} :catchall_6d
    .catch Ljava/lang/Exception; {:try_start_34 .. :try_end_3d} :catch_4e

    #@3d
    move-result-object v8

    #@3e
    .line 2255
    if-eqz v7, :cond_44

    #@40
    .line 2256
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@43
    .line 2257
    const/4 v7, 0x0

    #@44
    :cond_44
    move-object v0, v8

    #@45
    goto :goto_33

    #@46
    .line 2255
    :cond_46
    if-eqz v7, :cond_4c

    #@48
    .line 2256
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@4b
    .line 2257
    const/4 v7, 0x0

    #@4c
    :cond_4c
    move-object v0, v9

    #@4d
    goto :goto_33

    #@4e
    .line 2252
    :catch_4e
    move-exception v6

    #@4f
    .line 2253
    .local v6, e:Ljava/lang/Exception;
    :try_start_4f
    new-instance v0, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v1, "[IP][ERROR]exception : "

    #@56
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v0

    #@5a
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v0

    #@5e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v0

    #@62
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_65
    .catchall {:try_start_4f .. :try_end_65} :catchall_6d

    #@65
    .line 2255
    if-eqz v7, :cond_6b

    #@67
    .line 2256
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@6a
    .line 2257
    const/4 v7, 0x0

    #@6b
    :cond_6b
    move-object v0, v9

    #@6c
    .line 2260
    goto :goto_33

    #@6d
    .line 2255
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_6d
    move-exception v0

    #@6e
    if-eqz v7, :cond_74

    #@70
    .line 2256
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@73
    .line 2257
    const/4 v7, 0x0

    #@74
    :cond_74
    throw v0
.end method

.method private GetRCSContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .registers 13
    .parameter "strMSISDN"
    .parameter "strOrder"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 2191
    if-eqz p1, :cond_9

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 2192
    :cond_9
    const-string v0, "[IP][ERROR]GetRCSContactDataByMSISDN : MSISDN is null or Empty"

    #@b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e
    move-object v8, v9

    #@f
    .line 2228
    :cond_f
    :goto_f
    return-object v8

    #@10
    .line 2195
    :cond_10
    const-string v3, "nor_msisdn=?"

    #@12
    .line 2196
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@13
    new-array v4, v0, [Ljava/lang/String;

    #@15
    const/4 v0, 0x0

    #@16
    aput-object p1, v4, v0

    #@18
    .line 2197
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    const/4 v7, 0x0

    #@19
    .line 2198
    .local v7, objCursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    #@1a
    .line 2200
    .local v8, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :try_start_1a
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@1c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1f
    move-result-object v0

    #@20
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@22
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@24
    move-object v5, p2

    #@25
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@28
    move-result-object v7

    #@29
    .line 2206
    if-nez v7, :cond_38

    #@2b
    .line 2207
    const-string v0, "[IP][ERROR]GetRCSContactDataByMSISDN : Can\'t search RCS Contact"

    #@2d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_30
    .catchall {:try_start_1a .. :try_end_30} :catchall_72
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_30} :catch_53

    #@30
    .line 2223
    if-eqz v7, :cond_36

    #@32
    .line 2224
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@35
    .line 2225
    const/4 v7, 0x0

    #@36
    :cond_36
    move-object v8, v9

    #@37
    goto :goto_f

    #@38
    .line 2210
    :cond_38
    :try_start_38
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@3b
    move-result v0

    #@3c
    if-eqz v0, :cond_4c

    #@3e
    .line 2211
    invoke-direct {p0, v7}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@41
    move-result-object v8

    #@42
    .line 2212
    if-eqz v8, :cond_38

    #@44
    .line 2213
    iget-object v0, v8, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@46
    invoke-static {v0, p1}, Lcom/lge/ims/service/ip/IPUtil;->IsEqualsNumber(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_49
    .catchall {:try_start_38 .. :try_end_49} :catchall_72
    .catch Ljava/lang/Exception; {:try_start_38 .. :try_end_49} :catch_53

    #@49
    move-result v0

    #@4a
    if-eqz v0, :cond_38

    #@4c
    .line 2223
    :cond_4c
    if-eqz v7, :cond_f

    #@4e
    .line 2224
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@51
    .line 2225
    const/4 v7, 0x0

    #@52
    goto :goto_f

    #@53
    .line 2220
    :catch_53
    move-exception v6

    #@54
    .line 2221
    .local v6, e:Ljava/lang/Exception;
    :try_start_54
    new-instance v0, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v1, "[IP][ERROR]exception : "

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v0

    #@63
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v0

    #@67
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_6a
    .catchall {:try_start_54 .. :try_end_6a} :catchall_72

    #@6a
    .line 2223
    if-eqz v7, :cond_70

    #@6c
    .line 2224
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@6f
    .line 2225
    const/4 v7, 0x0

    #@70
    :cond_70
    move-object v8, v9

    #@71
    .line 2228
    goto :goto_f

    #@72
    .line 2223
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_72
    move-exception v0

    #@73
    if-eqz v7, :cond_79

    #@75
    .line 2224
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@78
    .line 2225
    const/4 v7, 0x0

    #@79
    :cond_79
    throw v0
.end method

.method private GetRCSUserCount(Ljava/lang/String;)I
    .registers 11
    .parameter "number"

    #@0
    .prologue
    .line 2263
    const-string v3, "msisdn=?"

    #@2
    .line 2264
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@3
    new-array v4, v0, [Ljava/lang/String;

    #@5
    const/4 v0, 0x0

    #@6
    aput-object p1, v4, v0

    #@8
    .line 2265
    .local v4, astrSelectionArg:[Ljava/lang/String;
    const/4 v8, 0x0

    #@9
    .line 2266
    .local v8, objCursor:Landroid/database/Cursor;
    const/4 v7, 0x0

    #@a
    .line 2268
    .local v7, nCount:I
    :try_start_a
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@f
    move-result-object v0

    #@10
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@12
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@14
    const/4 v5, 0x0

    #@15
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@18
    move-result-object v8

    #@19
    .line 2269
    if-nez v8, :cond_27

    #@1b
    .line 2270
    const-string v0, "[IP][ERROR]GetRCSUserCount : objCursor is null"

    #@1d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_20
    .catchall {:try_start_a .. :try_end_20} :catchall_64
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_20} :catch_46

    #@20
    .line 2278
    :goto_20
    if-eqz v8, :cond_26

    #@22
    .line 2279
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@25
    .line 2280
    const/4 v8, 0x0

    #@26
    .line 2283
    :cond_26
    :goto_26
    return v7

    #@27
    .line 2272
    :cond_27
    :try_start_27
    new-instance v0, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v1, "[IP]GetRCSUserCount : objCursor count - "

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    #@35
    move-result v1

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v0

    #@3a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v0

    #@3e
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@41
    .line 2273
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_44
    .catchall {:try_start_27 .. :try_end_44} :catchall_64
    .catch Ljava/lang/Exception; {:try_start_27 .. :try_end_44} :catch_46

    #@44
    move-result v7

    #@45
    goto :goto_20

    #@46
    .line 2275
    :catch_46
    move-exception v6

    #@47
    .line 2276
    .local v6, e:Ljava/lang/Exception;
    :try_start_47
    new-instance v0, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v1, "[IP][ERROR]exception : "

    #@4e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v0

    #@52
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v0

    #@56
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_5d
    .catchall {:try_start_47 .. :try_end_5d} :catchall_64

    #@5d
    .line 2278
    if-eqz v8, :cond_26

    #@5f
    .line 2279
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@62
    .line 2280
    const/4 v8, 0x0

    #@63
    goto :goto_26

    #@64
    .line 2278
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_64
    move-exception v0

    #@65
    if-eqz v8, :cond_6b

    #@67
    .line 2279
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@6a
    .line 2280
    const/4 v8, 0x0

    #@6b
    :cond_6b
    throw v0
.end method

.method private HasContactImageData(J)I
    .registers 16
    .parameter "raw_contact_id"

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 3218
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v1, "[IP] raw_contact_id : ["

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    const-string v1, "]"

    #@14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v0

    #@18
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1f
    .line 3219
    const-string v3, "raw_contact_id=? AND mimetype=?"

    #@21
    .line 3220
    .local v3, strSelection:Ljava/lang/String;
    new-array v4, v9, [Ljava/lang/String;

    #@23
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    aput-object v0, v4, v8

    #@29
    const-string v0, "vnd.android.cursor.item/photo"

    #@2b
    aput-object v0, v4, v10

    #@2d
    .line 3222
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@2f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@32
    move-result-object v0

    #@33
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@35
    const/4 v2, 0x4

    #@36
    new-array v2, v2, [Ljava/lang/String;

    #@38
    const-string v5, "_id"

    #@3a
    aput-object v5, v2, v8

    #@3c
    const-string v5, "raw_contact_id"

    #@3e
    aput-object v5, v2, v10

    #@40
    const-string v5, "mimetype"

    #@42
    aput-object v5, v2, v9

    #@44
    const/4 v5, 0x3

    #@45
    const-string v11, "data10"

    #@47
    aput-object v11, v2, v5

    #@49
    const/4 v5, 0x0

    #@4a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@4d
    move-result-object v7

    #@4e
    .line 3229
    .local v7, objCursor:Landroid/database/Cursor;
    if-nez v7, :cond_74

    #@50
    .line 3230
    :try_start_50
    new-instance v0, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v1, "[IP][ERROR]HasContactImageData : Not found ["

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v0

    #@5f
    const-string v1, "]"

    #@61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v0

    #@65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v0

    #@69
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_6c
    .catchall {:try_start_50 .. :try_end_6c} :catchall_d0
    .catch Ljava/lang/Exception; {:try_start_50 .. :try_end_6c} :catch_b1

    #@6c
    .line 3247
    if-eqz v7, :cond_72

    #@6e
    .line 3248
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@71
    .line 3249
    const/4 v7, 0x0

    #@72
    :cond_72
    move v0, v8

    #@73
    .line 3252
    :goto_73
    return v0

    #@74
    .line 3232
    :cond_74
    :try_start_74
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@77
    move-result v0

    #@78
    if-nez v0, :cond_87

    #@7a
    .line 3233
    const-string v0, "[IP][ERROR]objCursor.moveToNext() is null"

    #@7c
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_7f
    .catchall {:try_start_74 .. :try_end_7f} :catchall_d0
    .catch Ljava/lang/Exception; {:try_start_74 .. :try_end_7f} :catch_b1

    #@7f
    .line 3247
    if-eqz v7, :cond_85

    #@81
    .line 3248
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@84
    .line 3249
    const/4 v7, 0x0

    #@85
    :cond_85
    move v0, v8

    #@86
    goto :goto_73

    #@87
    .line 3236
    :cond_87
    :try_start_87
    const-string v0, "data10"

    #@89
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@8c
    move-result v0

    #@8d
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    #@90
    move-result-wide v0

    #@91
    const-wide/16 v11, 0x1

    #@93
    cmp-long v0, v0, v11

    #@95
    if-nez v0, :cond_a4

    #@97
    .line 3237
    const-string v0, "[IP]HasContactImageData is 2"

    #@99
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_9c
    .catchall {:try_start_87 .. :try_end_9c} :catchall_d0
    .catch Ljava/lang/Exception; {:try_start_87 .. :try_end_9c} :catch_b1

    #@9c
    .line 3247
    if-eqz v7, :cond_a2

    #@9e
    .line 3248
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@a1
    .line 3249
    const/4 v7, 0x0

    #@a2
    :cond_a2
    move v0, v9

    #@a3
    goto :goto_73

    #@a4
    .line 3240
    :cond_a4
    :try_start_a4
    const-string v0, "[IP]HasContactImageData is 1"

    #@a6
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_a9
    .catchall {:try_start_a4 .. :try_end_a9} :catchall_d0
    .catch Ljava/lang/Exception; {:try_start_a4 .. :try_end_a9} :catch_b1

    #@a9
    .line 3247
    if-eqz v7, :cond_af

    #@ab
    .line 3248
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@ae
    .line 3249
    const/4 v7, 0x0

    #@af
    :cond_af
    move v0, v10

    #@b0
    goto :goto_73

    #@b1
    .line 3244
    :catch_b1
    move-exception v6

    #@b2
    .line 3245
    .local v6, e:Ljava/lang/Exception;
    :try_start_b2
    new-instance v0, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v1, "[IP][ERROR]exception : "

    #@b9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v0

    #@bd
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v0

    #@c1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v0

    #@c5
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_c8
    .catchall {:try_start_b2 .. :try_end_c8} :catchall_d0

    #@c8
    .line 3247
    if-eqz v7, :cond_ce

    #@ca
    .line 3248
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@cd
    .line 3249
    const/4 v7, 0x0

    #@ce
    :cond_ce
    move v0, v8

    #@cf
    .line 3252
    goto :goto_73

    #@d0
    .line 3247
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_d0
    move-exception v0

    #@d1
    if-eqz v7, :cond_d7

    #@d3
    .line 3248
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@d6
    .line 3249
    const/4 v7, 0x0

    #@d7
    :cond_d7
    throw v0
.end method

.method private IPCapabilityData_Delete(Landroid/database/Cursor;)Landroid/content/ContentProviderOperation;
    .registers 13
    .parameter "rcse"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 1848
    const/4 v5, 0x0

    #@3
    .line 1849
    .local v5, ops:Landroid/content/ContentProviderOperation;
    if-eqz p1, :cond_70

    #@5
    .line 1850
    const/16 v7, 0xc

    #@7
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getLong(I)J

    #@a
    move-result-wide v7

    #@b
    invoke-direct {p0, v7, v8}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactData_Delete(J)Z

    #@e
    .line 1851
    const/4 v7, 0x2

    #@f
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@12
    move-result-object v4

    #@13
    .line 1852
    .local v4, number:Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSUserCount(Ljava/lang/String;)I

    #@16
    move-result v7

    #@17
    if-ne v7, v9, :cond_20

    #@19
    .line 1853
    invoke-static {v4}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v7

    #@1d
    invoke-direct {p0, v7}, Lcom/lge/ims/service/ip/IPContactHelper;->PresenceData_Delete(Ljava/lang/String;)Z

    #@20
    .line 1855
    :cond_20
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getLong(I)J

    #@23
    move-result-wide v2

    #@24
    .line 1856
    .local v2, _id:J
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    #@27
    move-result-wide v0

    #@28
    .line 1857
    .local v0, _data_id:J
    new-array v6, v9, [Ljava/lang/String;

    #@2a
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@2d
    move-result-object v7

    #@2e
    aput-object v7, v6, v10

    #@30
    .line 1858
    .local v6, selectionArgs:[Ljava/lang/String;
    sget-object v7, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@32
    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    #@35
    move-result-object v7

    #@36
    const-string v8, "_id=?"

    #@38
    invoke-virtual {v7, v8, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    #@3b
    move-result-object v7

    #@3c
    invoke-virtual {v7}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    #@3f
    move-result-object v5

    #@40
    .line 1861
    new-instance v7, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v8, "[IP]Deleted : [ "

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    const-string v8, " ], _id : [ "

    #@51
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@58
    move-result-object v7

    #@59
    const-string v8, " ], _data_id : [ "

    #@5b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    const-string v8, "]"

    #@65
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v7

    #@69
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v7

    #@6d
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@70
    .line 1863
    .end local v0           #_data_id:J
    .end local v2           #_id:J
    .end local v4           #number:Ljava/lang/String;
    .end local v6           #selectionArgs:[Ljava/lang/String;
    :cond_70
    return-object v5
.end method

.method private IPCapabilityData_DuplicatonCheck(Landroid/database/Cursor;)Z
    .registers 12
    .parameter "phone"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1704
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1705
    const/4 v7, 0x0

    #@7
    .line 1706
    .local v7, ops:Landroid/content/ContentProviderOperation;
    if-eqz p1, :cond_5d

    #@9
    .line 1707
    const-string v0, "raw_contact_id"

    #@b
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@e
    move-result v0

    #@f
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    #@12
    move-result-wide v3

    #@13
    .line 1708
    .local v3, raw_contact_id:J
    const-string v0, "_id"

    #@15
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@18
    move-result v0

    #@19
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    #@1c
    move-result-wide v1

    #@1d
    .line 1710
    .local v1, _id:J
    const/4 v5, 0x0

    #@1e
    move-object v0, p0

    #@1f
    invoke-direct/range {v0 .. v5}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactDataByDataId(JJLjava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@22
    move-result-object v6

    #@23
    .line 1711
    .local v6, objTempRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v6, :cond_5b

    #@25
    .line 1713
    new-instance v0, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "[IP][ERROR]IPCapabilityData_DuplicatonCheck : already same user is exist : bDescending [ "

    #@2c
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v0

    #@30
    sget-boolean v5, Lcom/lge/ims/service/ip/IPContactHelper;->bDescending:Z

    #@32
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@35
    move-result-object v0

    #@36
    const-string v5, "] contact _id [ "

    #@38
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v0

    #@40
    const-string v5, " ] IMS data_id [ "

    #@42
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v0

    #@46
    iget-wide v8, v6, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nDataId:J

    #@48
    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v0

    #@4c
    const-string v5, " ]"

    #@4e
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v0

    #@52
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v0

    #@56
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@59
    .line 1714
    const/4 v0, 0x1

    #@5a
    .line 1719
    .end local v1           #_id:J
    .end local v3           #raw_contact_id:J
    .end local v6           #objTempRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :goto_5a
    return v0

    #@5b
    .restart local v1       #_id:J
    .restart local v3       #raw_contact_id:J
    .restart local v6       #objTempRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :cond_5b
    move v0, v8

    #@5c
    .line 1716
    goto :goto_5a

    #@5d
    .end local v1           #_id:J
    .end local v3           #raw_contact_id:J
    .end local v6           #objTempRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :cond_5d
    move v0, v8

    #@5e
    .line 1719
    goto :goto_5a
.end method

.method private IPCapabilityData_Insert(Landroid/database/Cursor;)Landroid/content/ContentProviderOperation;
    .registers 27
    .parameter "phone"

    #@0
    .prologue
    .line 1723
    const/16 v17, 0x0

    #@2
    .line 1724
    .local v17, ops:Landroid/content/ContentProviderOperation;
    if-eqz p1, :cond_3e4

    #@4
    .line 1725
    const-string v21, "data1"

    #@6
    move-object/from16 v0, p1

    #@8
    move-object/from16 v1, v21

    #@a
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@d
    move-result v21

    #@e
    move-object/from16 v0, p1

    #@10
    move/from16 v1, v21

    #@12
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@15
    move-result-object v14

    #@16
    .line 1727
    .local v14, number:Ljava/lang/String;
    const-string v21, "+82"

    #@18
    const/16 v22, 0x0

    #@1a
    move-object/from16 v0, v21

    #@1c
    move/from16 v1, v22

    #@1e
    invoke-virtual {v14, v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    #@21
    move-result v21

    #@22
    const/16 v22, 0x1

    #@24
    move/from16 v0, v21

    #@26
    move/from16 v1, v22

    #@28
    if-ne v0, v1, :cond_246

    #@2a
    .line 1728
    new-instance v21, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v22, "0"

    #@31
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v21

    #@35
    const/16 v22, 0x3

    #@37
    move/from16 v0, v22

    #@39
    invoke-virtual {v14, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3c
    move-result-object v22

    #@3d
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v21

    #@41
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v6

    #@45
    .line 1732
    .local v6, convertNumber:Ljava/lang/String;
    :goto_45
    const-string v21, "raw_contact_id"

    #@47
    move-object/from16 v0, p1

    #@49
    move-object/from16 v1, v21

    #@4b
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@4e
    move-result v21

    #@4f
    move-object/from16 v0, p1

    #@51
    move/from16 v1, v21

    #@53
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    #@56
    move-result-wide v18

    #@57
    .line 1733
    .local v18, raw_contact_id:J
    const/16 v20, -0x1

    #@59
    .line 1734
    .local v20, status:I
    const-string v21, "_id"

    #@5b
    move-object/from16 v0, p1

    #@5d
    move-object/from16 v1, v21

    #@5f
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@62
    move-result v21

    #@63
    move-object/from16 v0, p1

    #@65
    move/from16 v1, v21

    #@67
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    #@6a
    move-result-wide v3

    #@6b
    .line 1735
    .local v3, _id:J
    const-string v21, "account_type"

    #@6d
    move-object/from16 v0, p1

    #@6f
    move-object/from16 v1, v21

    #@71
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@74
    move-result v21

    #@75
    move-object/from16 v0, p1

    #@77
    move/from16 v1, v21

    #@79
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    .line 1736
    .local v5, account_type:Ljava/lang/String;
    move-object/from16 v0, p0

    #@7f
    invoke-direct {v0, v5}, Lcom/lge/ims/service/ip/IPContactHelper;->IsPhotoUpdateUser(Ljava/lang/String;)Z

    #@82
    move-result v21

    #@83
    const/16 v22, 0x1

    #@85
    move/from16 v0, v21

    #@87
    move/from16 v1, v22

    #@89
    if-ne v0, v1, :cond_249

    #@8b
    const/4 v11, 0x1

    #@8c
    .line 1737
    .local v11, nPhotoUpdate:I
    :goto_8c
    const-string v21, "display_name"

    #@8e
    move-object/from16 v0, p1

    #@90
    move-object/from16 v1, v21

    #@92
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@95
    move-result v21

    #@96
    move-object/from16 v0, p1

    #@98
    move/from16 v1, v21

    #@9a
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9d
    move-result-object v7

    #@9e
    .line 1738
    .local v7, displayname:Ljava/lang/String;
    invoke-static {v6}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@a1
    move-result-object v21

    #@a2
    const/16 v22, 0x0

    #@a4
    move-object/from16 v0, p0

    #@a6
    move-object/from16 v1, v21

    #@a8
    move-object/from16 v2, v22

    #@aa
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@ad
    move-result-object v16

    #@ae
    .line 1739
    .local v16, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v16, :cond_24c

    #@b0
    .line 1740
    move-object/from16 v0, v16

    #@b2
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRCS:I

    #@b4
    move/from16 v20, v0

    #@b6
    .line 1744
    :goto_b6
    new-instance v21, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v22, "[IP]IPCapabilityData_Insert - account_type [ "

    #@bd
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v21

    #@c1
    move-object/from16 v0, v21

    #@c3
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v21

    #@c7
    const-string v22, " ], number [ "

    #@c9
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v21

    #@cd
    move-object/from16 v0, v21

    #@cf
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v21

    #@d3
    const-string v22, " ], convertNumber ["

    #@d5
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v21

    #@d9
    move-object/from16 v0, v21

    #@db
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v21

    #@df
    const-string v22, "], id ["

    #@e1
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v21

    #@e5
    move-object/from16 v0, v21

    #@e7
    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v21

    #@eb
    const-string v22, "], status ["

    #@ed
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v21

    #@f1
    move-object/from16 v0, v21

    #@f3
    move/from16 v1, v20

    #@f5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v21

    #@f9
    const-string v22, "], raw_contact_id [ "

    #@fb
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v21

    #@ff
    move-object/from16 v0, v21

    #@101
    move-wide/from16 v1, v18

    #@103
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@106
    move-result-object v21

    #@107
    const-string v22, "]"

    #@109
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v21

    #@10d
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v21

    #@111
    invoke-static/range {v21 .. v21}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@114
    .line 1745
    const/16 v21, -0x1

    #@116
    move/from16 v0, v20

    #@118
    move/from16 v1, v21

    #@11a
    if-eq v0, v1, :cond_3e0

    #@11c
    .line 1746
    move-object/from16 v0, p0

    #@11e
    move-wide/from16 v1, v18

    #@120
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactData_Insert(JJ)J

    #@123
    move-result-wide v12

    #@124
    .line 1747
    .local v12, nUriid:J
    const/16 v21, 0x4

    #@126
    move/from16 v0, v20

    #@128
    move/from16 v1, v21

    #@12a
    if-ne v0, v1, :cond_256

    #@12c
    .line 1748
    sget-object v21, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@12e
    invoke-static/range {v21 .. v21}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    #@131
    move-result-object v21

    #@132
    const-string v22, "data_id"

    #@134
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@137
    move-result-object v23

    #@138
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@13b
    move-result-object v21

    #@13c
    const-string v22, "msisdn"

    #@13e
    const/16 v23, -0x1

    #@140
    move/from16 v0, v20

    #@142
    move/from16 v1, v23

    #@144
    if-ne v0, v1, :cond_147

    #@146
    const/4 v14, 0x0

    #@147
    .end local v14           #number:Ljava/lang/String;
    :cond_147
    move-object/from16 v0, v21

    #@149
    move-object/from16 v1, v22

    #@14b
    invoke-virtual {v0, v1, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@14e
    move-result-object v22

    #@14f
    const-string v23, "nor_msisdn"

    #@151
    const/16 v21, -0x1

    #@153
    move/from16 v0, v20

    #@155
    move/from16 v1, v21

    #@157
    if-ne v0, v1, :cond_250

    #@159
    const/16 v21, 0x0

    #@15b
    :goto_15b
    move-object/from16 v0, v22

    #@15d
    move-object/from16 v1, v23

    #@15f
    move-object/from16 v2, v21

    #@161
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@164
    move-result-object v21

    #@165
    const-string v22, "rcs_status"

    #@167
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16a
    move-result-object v23

    #@16b
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@16e
    move-result-object v21

    #@16f
    const-string v22, "im_status"

    #@171
    const/16 v23, 0x0

    #@173
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@176
    move-result-object v23

    #@177
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@17a
    move-result-object v21

    #@17b
    const-string v22, "ft_status"

    #@17d
    const/16 v23, 0x0

    #@17f
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@182
    move-result-object v23

    #@183
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@186
    move-result-object v21

    #@187
    const-string v22, "time_stamp"

    #@189
    const/16 v23, 0x0

    #@18b
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18e
    move-result-object v23

    #@18f
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@192
    move-result-object v21

    #@193
    const-string v22, "capa_type"

    #@195
    const/16 v23, 0x0

    #@197
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19a
    move-result-object v23

    #@19b
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@19e
    move-result-object v21

    #@19f
    const-string v22, "first_time_rcs"

    #@1a1
    const/16 v23, 0x0

    #@1a3
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a6
    move-result-object v23

    #@1a7
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@1aa
    move-result-object v21

    #@1ab
    const-string v22, "mim_status"

    #@1ad
    const/16 v23, 0x0

    #@1af
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b2
    move-result-object v23

    #@1b3
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@1b6
    move-result-object v21

    #@1b7
    const-string v22, "presence_status"

    #@1b9
    const/16 v23, 0x0

    #@1bb
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1be
    move-result-object v23

    #@1bf
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@1c2
    move-result-object v21

    #@1c3
    const-string v22, "uri_id"

    #@1c5
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1c8
    move-result-object v23

    #@1c9
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@1cc
    move-result-object v21

    #@1cd
    const-string v22, "http_status"

    #@1cf
    const/16 v23, 0x0

    #@1d1
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d4
    move-result-object v23

    #@1d5
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@1d8
    move-result-object v21

    #@1d9
    const-string v22, "response_code"

    #@1db
    const/16 v23, 0x0

    #@1dd
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e0
    move-result-object v23

    #@1e1
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@1e4
    move-result-object v21

    #@1e5
    const-string v22, "rcs_user_list"

    #@1e7
    const/16 v23, 0x0

    #@1e9
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ec
    move-result-object v23

    #@1ed
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@1f0
    move-result-object v21

    #@1f1
    const-string v22, "nonrcs_user_list"

    #@1f3
    const/16 v23, 0x0

    #@1f5
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f8
    move-result-object v23

    #@1f9
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@1fc
    move-result-object v21

    #@1fd
    const-string v22, "image_uri_id"

    #@1ff
    const/16 v23, -0x1

    #@201
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@204
    move-result-object v23

    #@205
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@208
    move-result-object v21

    #@209
    const-string v22, "raw_contact_id"

    #@20b
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@20e
    move-result-object v23

    #@20f
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@212
    move-result-object v21

    #@213
    const-string v22, "photo_update"

    #@215
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@218
    move-result-object v23

    #@219
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@21c
    move-result-object v21

    #@21d
    const-string v22, "display_name"

    #@21f
    move-object/from16 v0, v21

    #@221
    move-object/from16 v1, v22

    #@223
    invoke-virtual {v0, v1, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@226
    move-result-object v21

    #@227
    invoke-virtual/range {v21 .. v21}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    #@22a
    move-result-object v17

    #@22b
    .line 1816
    :goto_22b
    new-instance v21, Ljava/lang/StringBuilder;

    #@22d
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@230
    const-string v22, "[IP]Added : "

    #@232
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@235
    move-result-object v21

    #@236
    move-object/from16 v0, v21

    #@238
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23b
    move-result-object v21

    #@23c
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23f
    move-result-object v21

    #@240
    invoke-static/range {v21 .. v21}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@243
    move-object/from16 v21, v17

    #@245
    .line 1824
    .end local v3           #_id:J
    .end local v5           #account_type:Ljava/lang/String;
    .end local v6           #convertNumber:Ljava/lang/String;
    .end local v7           #displayname:Ljava/lang/String;
    .end local v11           #nPhotoUpdate:I
    .end local v12           #nUriid:J
    .end local v16           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v18           #raw_contact_id:J
    .end local v20           #status:I
    :goto_245
    return-object v21

    #@246
    .line 1730
    .restart local v14       #number:Ljava/lang/String;
    :cond_246
    move-object v6, v14

    #@247
    .restart local v6       #convertNumber:Ljava/lang/String;
    goto/16 :goto_45

    #@249
    .line 1736
    .restart local v3       #_id:J
    .restart local v5       #account_type:Ljava/lang/String;
    .restart local v18       #raw_contact_id:J
    .restart local v20       #status:I
    :cond_249
    const/4 v11, 0x0

    #@24a
    goto/16 :goto_8c

    #@24c
    .line 1742
    .restart local v7       #displayname:Ljava/lang/String;
    .restart local v11       #nPhotoUpdate:I
    .restart local v16       #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :cond_24c
    const/16 v20, 0x4

    #@24e
    goto/16 :goto_b6

    #@250
    .line 1748
    .end local v14           #number:Ljava/lang/String;
    .restart local v12       #nUriid:J
    :cond_250
    invoke-static {v6}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@253
    move-result-object v21

    #@254
    goto/16 :goto_15b

    #@256
    .line 1771
    .restart local v14       #number:Ljava/lang/String;
    :cond_256
    if-nez v16, :cond_25b

    #@258
    .line 1772
    const/16 v21, 0x0

    #@25a
    goto :goto_245

    #@25b
    .line 1775
    :cond_25b
    const-wide/16 v9, -0x1

    #@25d
    .line 1776
    .local v9, nImageUriId:J
    move-object/from16 v0, p0

    #@25f
    move-object/from16 v1, v16

    #@261
    invoke-direct {v0, v1, v12, v13}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactData_Update(Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;J)Z

    #@264
    .line 1777
    invoke-static {v6}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@267
    move-result-object v21

    #@268
    const/16 v22, 0x0

    #@26a
    move-object/from16 v0, p0

    #@26c
    move-object/from16 v1, v21

    #@26e
    move-object/from16 v2, v22

    #@270
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@273
    move-result-object v15

    #@274
    .line 1778
    .local v15, objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-eqz v15, :cond_3d3

    #@276
    .line 1779
    move-object/from16 v0, p0

    #@278
    invoke-direct {v0, v15, v12, v13}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactData_Update(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;J)Z

    #@27b
    .line 1780
    move-object/from16 v0, p0

    #@27d
    move-wide/from16 v1, v18

    #@27f
    invoke-direct {v0, v1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->HasContactImageData(J)I

    #@282
    move-result v21

    #@283
    if-nez v21, :cond_28d

    #@285
    .line 1782
    :try_start_285
    move-object/from16 v0, p0

    #@287
    move-wide/from16 v1, v18

    #@289
    invoke-direct {v0, v1, v2, v15}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactImageData_Insert(JLcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;)J
    :try_end_28c
    .catch Ljava/lang/Exception; {:try_start_285 .. :try_end_28c} :catch_3b8

    #@28c
    move-result-wide v9

    #@28d
    .line 1791
    :cond_28d
    :goto_28d
    sget-object v21, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@28f
    invoke-static/range {v21 .. v21}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    #@292
    move-result-object v21

    #@293
    const-string v22, "data_id"

    #@295
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@298
    move-result-object v23

    #@299
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@29c
    move-result-object v21

    #@29d
    const-string v22, "msisdn"

    #@29f
    const/16 v23, -0x1

    #@2a1
    move/from16 v0, v20

    #@2a3
    move/from16 v1, v23

    #@2a5
    if-ne v0, v1, :cond_2a8

    #@2a7
    const/4 v14, 0x0

    #@2a8
    .end local v14           #number:Ljava/lang/String;
    :cond_2a8
    move-object/from16 v0, v21

    #@2aa
    move-object/from16 v1, v22

    #@2ac
    invoke-virtual {v0, v1, v14}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@2af
    move-result-object v22

    #@2b0
    const-string v23, "nor_msisdn"

    #@2b2
    const/16 v21, -0x1

    #@2b4
    move/from16 v0, v20

    #@2b6
    move/from16 v1, v21

    #@2b8
    if-ne v0, v1, :cond_3da

    #@2ba
    const/16 v21, 0x0

    #@2bc
    :goto_2bc
    move-object/from16 v0, v22

    #@2be
    move-object/from16 v1, v23

    #@2c0
    move-object/from16 v2, v21

    #@2c2
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@2c5
    move-result-object v21

    #@2c6
    const-string v22, "rcs_status"

    #@2c8
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2cb
    move-result-object v23

    #@2cc
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@2cf
    move-result-object v21

    #@2d0
    const-string v22, "im_status"

    #@2d2
    move-object/from16 v0, v16

    #@2d4
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsIM:I

    #@2d6
    move/from16 v23, v0

    #@2d8
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2db
    move-result-object v23

    #@2dc
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@2df
    move-result-object v21

    #@2e0
    const-string v22, "ft_status"

    #@2e2
    move-object/from16 v0, v16

    #@2e4
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsFT:I

    #@2e6
    move/from16 v23, v0

    #@2e8
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2eb
    move-result-object v23

    #@2ec
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@2ef
    move-result-object v21

    #@2f0
    const-string v22, "time_stamp"

    #@2f2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@2f5
    move-result-wide v23

    #@2f6
    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2f9
    move-result-object v23

    #@2fa
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@2fd
    move-result-object v21

    #@2fe
    const-string v22, "capa_type"

    #@300
    move-object/from16 v0, v16

    #@302
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nCapaType:I

    #@304
    move/from16 v23, v0

    #@306
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@309
    move-result-object v23

    #@30a
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@30d
    move-result-object v21

    #@30e
    const-string v22, "first_time_rcs"

    #@310
    move-object/from16 v0, v16

    #@312
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nFirstTimeRcs:I

    #@314
    move/from16 v23, v0

    #@316
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@319
    move-result-object v23

    #@31a
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@31d
    move-result-object v21

    #@31e
    const-string v22, "mim_status"

    #@320
    move-object/from16 v0, v16

    #@322
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsMIM:I

    #@324
    move/from16 v23, v0

    #@326
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@329
    move-result-object v23

    #@32a
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@32d
    move-result-object v21

    #@32e
    const-string v22, "presence_status"

    #@330
    move-object/from16 v0, v16

    #@332
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsPresence:I

    #@334
    move/from16 v23, v0

    #@336
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@339
    move-result-object v23

    #@33a
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@33d
    move-result-object v21

    #@33e
    const-string v22, "uri_id"

    #@340
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@343
    move-result-object v23

    #@344
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@347
    move-result-object v21

    #@348
    const-string v22, "http_status"

    #@34a
    move-object/from16 v0, v16

    #@34c
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsHTTP:I

    #@34e
    move/from16 v23, v0

    #@350
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@353
    move-result-object v23

    #@354
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@357
    move-result-object v21

    #@358
    const-string v22, "response_code"

    #@35a
    move-object/from16 v0, v16

    #@35c
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nResponsCode:I

    #@35e
    move/from16 v23, v0

    #@360
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@363
    move-result-object v23

    #@364
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@367
    move-result-object v21

    #@368
    const-string v22, "rcs_user_list"

    #@36a
    move-object/from16 v0, v16

    #@36c
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRcsUserList:I

    #@36e
    move/from16 v23, v0

    #@370
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@373
    move-result-object v23

    #@374
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@377
    move-result-object v21

    #@378
    const-string v22, "nonrcs_user_list"

    #@37a
    move-object/from16 v0, v16

    #@37c
    iget v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsNonRcsUserList:I

    #@37e
    move/from16 v23, v0

    #@380
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@383
    move-result-object v23

    #@384
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@387
    move-result-object v21

    #@388
    const-string v22, "image_uri_id"

    #@38a
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@38d
    move-result-object v23

    #@38e
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@391
    move-result-object v21

    #@392
    const-string v22, "raw_contact_id"

    #@394
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@397
    move-result-object v23

    #@398
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@39b
    move-result-object v21

    #@39c
    const-string v22, "photo_update"

    #@39e
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3a1
    move-result-object v23

    #@3a2
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@3a5
    move-result-object v21

    #@3a6
    const-string v22, "display_name"

    #@3a8
    move-object/from16 v0, v16

    #@3aa
    iget-object v0, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strDisplayName:Ljava/lang/String;

    #@3ac
    move-object/from16 v23, v0

    #@3ae
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    #@3b1
    move-result-object v21

    #@3b2
    invoke-virtual/range {v21 .. v21}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    #@3b5
    move-result-object v17

    #@3b6
    goto/16 :goto_22b

    #@3b8
    .line 1783
    .restart local v14       #number:Ljava/lang/String;
    :catch_3b8
    move-exception v8

    #@3b9
    .line 1784
    .local v8, e:Ljava/lang/Exception;
    new-instance v21, Ljava/lang/StringBuilder;

    #@3bb
    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    #@3be
    const-string v22, "[IP][ERROR]IPCapabilityData_Insert - "

    #@3c0
    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c3
    move-result-object v21

    #@3c4
    move-object/from16 v0, v21

    #@3c6
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c9
    move-result-object v21

    #@3ca
    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3cd
    move-result-object v21

    #@3ce
    invoke-static/range {v21 .. v21}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@3d1
    goto/16 :goto_28d

    #@3d3
    .line 1788
    .end local v8           #e:Ljava/lang/Exception;
    :cond_3d3
    const-string v21, "[IP]IPCapabilityData_Insert - objPresenceContactData is null "

    #@3d5
    invoke-static/range {v21 .. v21}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@3d8
    goto/16 :goto_28d

    #@3da
    .line 1791
    .end local v14           #number:Ljava/lang/String;
    :cond_3da
    invoke-static {v6}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@3dd
    move-result-object v21

    #@3de
    goto/16 :goto_2bc

    #@3e0
    .line 1821
    .end local v9           #nImageUriId:J
    .end local v12           #nUriid:J
    .end local v15           #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    .restart local v14       #number:Ljava/lang/String;
    :cond_3e0
    const/16 v21, 0x0

    #@3e2
    goto/16 :goto_245

    #@3e4
    .line 1824
    .end local v3           #_id:J
    .end local v5           #account_type:Ljava/lang/String;
    .end local v6           #convertNumber:Ljava/lang/String;
    .end local v7           #displayname:Ljava/lang/String;
    .end local v11           #nPhotoUpdate:I
    .end local v14           #number:Ljava/lang/String;
    .end local v16           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v18           #raw_contact_id:J
    .end local v20           #status:I
    :cond_3e4
    const/16 v21, 0x0

    #@3e6
    goto/16 :goto_245
.end method

.method private IsExceedMaxUserSize()Z
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 2106
    const/4 v9, 0x0

    #@2
    .line 2108
    .local v9, objCursor:Landroid/database/Cursor;
    :try_start_2
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_PROVISIONING_CONTENT_URI:Landroid/net/Uri;

    #@a
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_PROVISIONING_PROJECTION:[Ljava/lang/String;

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x0

    #@e
    const/4 v5, 0x0

    #@f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@12
    move-result-object v9

    #@13
    .line 2113
    if-eqz v9, :cond_1b

    #@15
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@18
    move-result v0

    #@19
    if-nez v0, :cond_28

    #@1b
    .line 2114
    :cond_1b
    const-string v0, "[IP][ERROR]IsExceedMaxUserSize - objCursor is null"

    #@1d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_20
    .catchall {:try_start_2 .. :try_end_20} :catchall_8e
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_20} :catch_6f

    #@20
    .line 2134
    if-eqz v9, :cond_26

    #@22
    .line 2135
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@25
    .line 2136
    const/4 v9, 0x0

    #@26
    :cond_26
    move v0, v10

    #@27
    .line 2139
    :cond_27
    :goto_27
    return v0

    #@28
    .line 2117
    :cond_28
    :try_start_28
    const-string v0, "excess_max_user"

    #@2a
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@2d
    move-result v6

    #@2e
    .line 2118
    .local v6, columnExcessMaxUserIndex:I
    if-gez v6, :cond_3d

    #@30
    .line 2119
    const-string v0, "[IP][ERROR]IsExceedMaxUserSize - columnExcessMaxUserIndex is under 0"

    #@32
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_35
    .catchall {:try_start_28 .. :try_end_35} :catchall_8e
    .catch Ljava/lang/Exception; {:try_start_28 .. :try_end_35} :catch_6f

    #@35
    .line 2134
    if-eqz v9, :cond_3b

    #@37
    .line 2135
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@3a
    .line 2136
    const/4 v9, 0x0

    #@3b
    :cond_3b
    move v0, v10

    #@3c
    goto :goto_27

    #@3d
    .line 2122
    :cond_3d
    :try_start_3d
    invoke-interface {v9, v6}, Landroid/database/Cursor;->getInt(I)I

    #@40
    move-result v8

    #@41
    .line 2123
    .local v8, nTemp:I
    new-instance v0, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v1, "[IP]IsExceedMaxUserSize nTemp [ "

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v0

    #@4c
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v0

    #@50
    const-string v1, " ]"

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v0

    #@56
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v0

    #@5a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V
    :try_end_5d
    .catchall {:try_start_3d .. :try_end_5d} :catchall_8e
    .catch Ljava/lang/Exception; {:try_start_3d .. :try_end_5d} :catch_6f

    #@5d
    .line 2124
    if-nez v8, :cond_67

    #@5f
    .line 2134
    if-eqz v9, :cond_65

    #@61
    .line 2135
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@64
    .line 2136
    const/4 v9, 0x0

    #@65
    :cond_65
    move v0, v10

    #@66
    goto :goto_27

    #@67
    .line 2127
    :cond_67
    const/4 v0, 0x1

    #@68
    .line 2134
    if-eqz v9, :cond_27

    #@6a
    .line 2135
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@6d
    .line 2136
    const/4 v9, 0x0

    #@6e
    goto :goto_27

    #@6f
    .line 2131
    .end local v6           #columnExcessMaxUserIndex:I
    .end local v8           #nTemp:I
    :catch_6f
    move-exception v7

    #@70
    .line 2132
    .local v7, e:Ljava/lang/Exception;
    :try_start_70
    new-instance v0, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v1, "[IP][ERROR]exception : "

    #@77
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v0

    #@7b
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v0

    #@7f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v0

    #@83
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_86
    .catchall {:try_start_70 .. :try_end_86} :catchall_8e

    #@86
    .line 2134
    if-eqz v9, :cond_8c

    #@88
    .line 2135
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@8b
    .line 2136
    const/4 v9, 0x0

    #@8c
    :cond_8c
    move v0, v10

    #@8d
    .line 2139
    goto :goto_27

    #@8e
    .line 2134
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_8e
    move-exception v0

    #@8f
    if-eqz v9, :cond_95

    #@91
    .line 2135
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@94
    .line 2136
    const/4 v9, 0x0

    #@95
    :cond_95
    throw v0
.end method

.method private IsPhotoUpdateUser(Ljava/lang/String;)Z
    .registers 5
    .parameter "account_type"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2088
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v2, "[IP]IsPhotoUpdateUser : account type [ "

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    const-string v2, " ]"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1d
    .line 2090
    if-nez p1, :cond_25

    #@1f
    .line 2091
    const-string v1, "[IP]return true"

    #@21
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@24
    .line 2102
    :goto_24
    return v0

    #@25
    .line 2095
    :cond_25
    const-string v1, "com.android.contacts.sim"

    #@27
    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_4d

    #@2d
    const-string v1, "com.facebook.katana"

    #@2f
    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@32
    move-result v1

    #@33
    if-eqz v1, :cond_4d

    #@35
    const-string v1, "com.twitter.android.auth.login"

    #@37
    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@3a
    move-result v1

    #@3b
    if-eqz v1, :cond_4d

    #@3d
    const-string v1, "com.lge.sns.facebook"

    #@3f
    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@42
    move-result v1

    #@43
    if-eqz v1, :cond_4d

    #@45
    const-string v1, "com.lge.sns.twitter"

    #@47
    invoke-virtual {p1, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    #@4a
    move-result v1

    #@4b
    if-nez v1, :cond_54

    #@4d
    .line 2098
    :cond_4d
    const-string v0, "[IP]return false"

    #@4f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@52
    .line 2099
    const/4 v0, 0x0

    #@53
    goto :goto_24

    #@54
    .line 2101
    :cond_54
    const-string v1, "[IP]return true"

    #@56
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@59
    goto :goto_24
.end method

.method private JoinCursorNCreateRCSContact()Z
    .registers 32

    #@0
    .prologue
    .line 1881
    const-string v3, "[IP]"

    #@2
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 1882
    const/4 v15, 0x0

    #@6
    .line 1883
    .local v15, bReSync:Z
    new-instance v10, Ljava/util/ArrayList;

    #@8
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@b
    .line 1885
    .local v10, aobjCPOs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    new-instance v25, Ljava/util/ArrayList;

    #@d
    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    #@10
    .line 1886
    .local v25, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->clear()V

    #@13
    .line 1887
    new-instance v24, Ljava/util/ArrayList;

    #@15
    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    #@18
    .line 1888
    .local v24, objDuplicatingList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->clear()V

    #@1b
    .line 1889
    const/16 v27, 0x0

    #@1d
    .line 1890
    .local v27, objPhonesCursor:Landroid/database/Cursor;
    const/16 v28, 0x0

    #@1f
    .line 1891
    .local v28, objRCSeCursor:Landroid/database/Cursor;
    move-object/from16 v0, p0

    #@21
    iget-object v3, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@23
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@26
    move-result-object v3

    #@27
    sget-object v4, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    #@29
    const-string v5, "phones"

    #@2b
    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@2e
    move-result-object v4

    #@2f
    const/4 v5, 0x5

    #@30
    new-array v5, v5, [Ljava/lang/String;

    #@32
    const/4 v6, 0x0

    #@33
    const-string v7, "_id"

    #@35
    aput-object v7, v5, v6

    #@37
    const/4 v6, 0x1

    #@38
    const-string v7, "data1"

    #@3a
    aput-object v7, v5, v6

    #@3c
    const/4 v6, 0x2

    #@3d
    const-string v7, "raw_contact_id"

    #@3f
    aput-object v7, v5, v6

    #@41
    const/4 v6, 0x3

    #@42
    const-string v7, "account_type"

    #@44
    aput-object v7, v5, v6

    #@46
    const/4 v6, 0x4

    #@47
    const-string v7, "display_name"

    #@49
    aput-object v7, v5, v6

    #@4b
    const/4 v6, 0x0

    #@4c
    const/4 v7, 0x0

    #@4d
    const-string v8, "_id asc"

    #@4f
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@52
    move-result-object v27

    #@53
    .line 1905
    if-nez v27, :cond_5c

    #@55
    .line 1906
    const-string v3, "[IP][ERROR]Phone Cursor is null"

    #@57
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@5a
    .line 1907
    const/4 v3, 0x0

    #@5b
    .line 2085
    :goto_5b
    return v3

    #@5c
    .line 1912
    :cond_5c
    new-instance v3, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v4, "[IP] Phone count : "

    #@63
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v3

    #@67
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->getCount()I

    #@6a
    move-result v4

    #@6b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@76
    .line 1918
    move-object/from16 v0, p0

    #@78
    iget-object v3, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@7a
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7d
    move-result-object v3

    #@7e
    sget-object v4, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@80
    sget-object v5, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@82
    const/4 v6, 0x0

    #@83
    const/4 v7, 0x0

    #@84
    const-string v8, "data_id asc"

    #@86
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@89
    move-result-object v28

    #@8a
    .line 1921
    if-nez v28, :cond_98

    #@8c
    .line 1922
    if-eqz v27, :cond_91

    #@8e
    .line 1923
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    #@91
    .line 1925
    :cond_91
    const-string v3, "[IP][ERROR]RCS-e Cursor is null"

    #@93
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@96
    .line 1926
    const/4 v3, 0x0

    #@97
    goto :goto_5b

    #@98
    .line 1931
    :cond_98
    new-instance v3, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v4, "[IP]RCSe count : "

    #@9f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v3

    #@a3
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->getCount()I

    #@a6
    move-result v4

    #@a7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v3

    #@ab
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v3

    #@af
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@b2
    .line 1934
    const/4 v3, 0x2

    #@b3
    new-array v12, v3, [Ljava/lang/String;

    #@b5
    const/4 v3, 0x0

    #@b6
    const-string v4, "_id"

    #@b8
    aput-object v4, v12, v3

    #@ba
    const/4 v3, 0x1

    #@bb
    const-string v4, "data1"

    #@bd
    aput-object v4, v12, v3

    #@bf
    .line 1935
    .local v12, astrPhoneJoinColumn:[Ljava/lang/String;
    const/4 v3, 0x2

    #@c0
    new-array v13, v3, [Ljava/lang/String;

    #@c2
    const/4 v3, 0x0

    #@c3
    const-string v4, "data_id"

    #@c5
    aput-object v4, v13, v3

    #@c7
    const/4 v3, 0x1

    #@c8
    const-string v4, "msisdn"

    #@ca
    aput-object v4, v13, v3

    #@cc
    .line 1940
    .local v13, astrRCSeJoinColumn:[Ljava/lang/String;
    new-instance v19, Lcom/lge/ims/service/ip/CustomCursorJoiner;

    #@ce
    move-object/from16 v0, v19

    #@d0
    move-object/from16 v1, v27

    #@d2
    move-object/from16 v2, v28

    #@d4
    invoke-direct {v0, v1, v12, v2, v13}, Lcom/lge/ims/service/ip/CustomCursorJoiner;-><init>(Landroid/database/Cursor;[Ljava/lang/String;Landroid/database/Cursor;[Ljava/lang/String;)V

    #@d7
    .line 1942
    .local v19, joiner:Lcom/lge/ims/service/ip/CustomCursorJoiner;
    const/4 v14, 0x0

    #@d8
    .line 1943
    .local v14, bDuplicationChecker:Z
    invoke-virtual/range {v19 .. v19}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->iterator()Ljava/util/Iterator;

    #@db
    move-result-object v17

    #@dc
    .local v17, i$:Ljava/util/Iterator;
    :cond_dc
    :goto_dc
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    #@df
    move-result v3

    #@e0
    if-eqz v3, :cond_f1

    #@e2
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e5
    move-result-object v20

    #@e6
    check-cast v20, Landroid/database/CursorJoiner$Result;

    #@e8
    .line 1944
    .local v20, joinerResult:Landroid/database/CursorJoiner$Result;
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@eb
    move-result v3

    #@ec
    const/16 v4, 0x14

    #@ee
    if-le v3, v4, :cond_1d0

    #@f0
    .line 1945
    const/4 v15, 0x1

    #@f1
    .line 2022
    .end local v20           #joinerResult:Landroid/database/CursorJoiner$Result;
    :cond_f1
    if-eqz v27, :cond_f6

    #@f3
    .line 2023
    invoke-interface/range {v27 .. v27}, Landroid/database/Cursor;->close()V

    #@f6
    .line 2025
    :cond_f6
    if-eqz v28, :cond_fb

    #@f8
    .line 2026
    invoke-interface/range {v28 .. v28}, Landroid/database/Cursor;->close()V

    #@fb
    .line 2029
    :cond_fb
    new-instance v3, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v4, "[IP]Total operation : "

    #@102
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v3

    #@106
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@109
    move-result v4

    #@10a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v3

    #@10e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@111
    move-result-object v3

    #@112
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@115
    .line 2032
    :try_start_115
    move-object/from16 v0, p0

    #@117
    iget-object v3, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@119
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11c
    move-result-object v3

    #@11d
    const-string v4, "com.lge.ims.provider.ip"

    #@11f
    invoke-virtual {v3, v4, v10}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    #@122
    move-result-object v11

    #@123
    .line 2033
    .local v11, aobjResults:[Landroid/content/ContentProviderResult;
    new-instance v3, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v4, "[IP]Create RCS Contact : Success ("

    #@12a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v3

    #@12e
    array-length v4, v11

    #@12f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@132
    move-result-object v3

    #@133
    const-string v4, ")"

    #@135
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v3

    #@139
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13c
    move-result-object v3

    #@13d
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@140
    .line 2035
    new-instance v3, Ljava/lang/StringBuilder;

    #@142
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@145
    const-string v4, "[IP]objDuplicatingList.size["

    #@147
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v3

    #@14b
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    #@14e
    move-result v4

    #@14f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@152
    move-result-object v3

    #@153
    const-string v4, "]"

    #@155
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v3

    #@159
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v3

    #@15d
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@160
    .line 2036
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    #@163
    move-result v3

    #@164
    if-eqz v3, :cond_2ab

    #@166
    .line 2037
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@169
    move-result-object v17

    #@16a
    :cond_16a
    :goto_16a
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    #@16d
    move-result v3

    #@16e
    if-eqz v3, :cond_2ab

    #@170
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@173
    move-result-object v9

    #@174
    check-cast v9, Ljava/lang/String;

    #@176
    .line 2038
    .local v9, MSISDN:Ljava/lang/String;
    move-object/from16 v0, p0

    #@178
    invoke-direct {v0, v9}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSUserCount(Ljava/lang/String;)I

    #@17b
    move-result v3

    #@17c
    if-nez v3, :cond_16a

    #@17e
    .line 2039
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@181
    move-result-object v3

    #@182
    move-object/from16 v0, p0

    #@184
    invoke-direct {v0, v3}, Lcom/lge/ims/service/ip/IPContactHelper;->PresenceData_Delete(Ljava/lang/String;)Z

    #@187
    .line 2040
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@18a
    move-result-object v3

    #@18b
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@18e
    move-result v3

    #@18f
    if-nez v3, :cond_16a

    #@191
    .line 2041
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@194
    move-result-object v3

    #@195
    move-object/from16 v0, v25

    #@197
    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@19a
    move-result v3

    #@19b
    if-nez v3, :cond_16a

    #@19d
    .line 2042
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@1a0
    move-result-object v3

    #@1a1
    move-object/from16 v0, v25

    #@1a3
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1a6
    .line 2043
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1ab
    const-string v4, "[IP]RCSeData_DeleteUserListUp Added [ "

    #@1ad
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v3

    #@1b1
    invoke-static {v9}, Lcom/lge/ims/service/ip/IPUtil;->NomalizeMSISDN(Ljava/lang/String;)Ljava/lang/String;

    #@1b4
    move-result-object v4

    #@1b5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v3

    #@1b9
    const-string v4, " ]"

    #@1bb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1be
    move-result-object v3

    #@1bf
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c2
    move-result-object v3

    #@1c3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V
    :try_end_1c6
    .catch Landroid/os/RemoteException; {:try_start_115 .. :try_end_1c6} :catch_1c7
    .catch Landroid/content/OperationApplicationException; {:try_start_115 .. :try_end_1c6} :catch_2f7

    #@1c6
    goto :goto_16a

    #@1c7
    .line 2069
    .end local v9           #MSISDN:Ljava/lang/String;
    .end local v11           #aobjResults:[Landroid/content/ContentProviderResult;
    :catch_1c7
    move-exception v16

    #@1c8
    .line 2070
    .local v16, e:Landroid/os/RemoteException;
    const-string v3, "[IP][ERROR]Create RCS Contact : Failure (RemoteException)"

    #@1ca
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@1cd
    .line 2071
    const/4 v3, 0x0

    #@1ce
    goto/16 :goto_5b

    #@1d0
    .line 1948
    .end local v16           #e:Landroid/os/RemoteException;
    .restart local v20       #joinerResult:Landroid/database/CursorJoiner$Result;
    :cond_1d0
    const/16 v23, 0x0

    #@1d2
    .line 1949
    .local v23, objCPO:Landroid/content/ContentProviderOperation;
    sget-object v3, Lcom/lge/ims/service/ip/IPContactHelper$1;->$SwitchMap$android$database$CursorJoiner$Result:[I

    #@1d4
    invoke-virtual/range {v20 .. v20}, Landroid/database/CursorJoiner$Result;->ordinal()I

    #@1d7
    move-result v4

    #@1d8
    aget v3, v3, v4

    #@1da
    packed-switch v3, :pswitch_data_364

    #@1dd
    goto/16 :goto_dc

    #@1df
    .line 1959
    :pswitch_1df
    move-object/from16 v0, p0

    #@1e1
    move-object/from16 v1, v27

    #@1e3
    invoke-direct {v0, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->IPCapabilityData_Insert(Landroid/database/Cursor;)Landroid/content/ContentProviderOperation;

    #@1e6
    move-result-object v23

    #@1e7
    .line 1960
    if-eqz v23, :cond_dc

    #@1e9
    .line 1961
    move-object/from16 v0, v23

    #@1eb
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1ee
    goto/16 :goto_dc

    #@1f0
    .line 1967
    :pswitch_1f0
    if-eqz v28, :cond_dc

    #@1f2
    .line 1968
    const/4 v3, 0x2

    #@1f3
    move-object/from16 v0, v28

    #@1f5
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1f8
    move-result-object v3

    #@1f9
    move-object/from16 v0, p0

    #@1fb
    move-object/from16 v1, v25

    #@1fd
    move-object/from16 v2, v24

    #@1ff
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/ims/service/ip/IPContactHelper;->ListUpDeleteRCSUser(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Z

    #@202
    .line 1969
    move-object/from16 v0, p0

    #@204
    move-object/from16 v1, v28

    #@206
    invoke-direct {v0, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->IPCapabilityData_Delete(Landroid/database/Cursor;)Landroid/content/ContentProviderOperation;

    #@209
    move-result-object v23

    #@20a
    .line 1970
    if-eqz v23, :cond_dc

    #@20c
    .line 1971
    move-object/from16 v0, v23

    #@20e
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@211
    goto/16 :goto_dc

    #@213
    .line 1977
    :pswitch_213
    const-string v3, "data1"

    #@215
    move-object/from16 v0, v27

    #@217
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@21a
    move-result v3

    #@21b
    move-object/from16 v0, v27

    #@21d
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@220
    move-result-object v30

    #@221
    .line 1978
    .local v30, phoneNumber:Ljava/lang/String;
    const/4 v3, 0x2

    #@222
    move-object/from16 v0, v28

    #@224
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@227
    move-result-object v18

    #@228
    .line 1979
    .local v18, imsNumber:Ljava/lang/String;
    if-eqz v30, :cond_231

    #@22a
    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->isEmpty()Z

    #@22d
    move-result v3

    #@22e
    const/4 v4, 0x1

    #@22f
    if-ne v3, v4, :cond_244

    #@231
    .line 1980
    :cond_231
    move-object/from16 v0, p0

    #@233
    move-object/from16 v1, v28

    #@235
    invoke-direct {v0, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->IPCapabilityData_Delete(Landroid/database/Cursor;)Landroid/content/ContentProviderOperation;

    #@238
    move-result-object v23

    #@239
    .line 1981
    if-eqz v23, :cond_240

    #@23b
    .line 1982
    move-object/from16 v0, v23

    #@23d
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@240
    .line 1984
    :cond_240
    const/16 v23, 0x0

    #@242
    goto/16 :goto_dc

    #@244
    .line 1986
    :cond_244
    if-eqz v18, :cond_24d

    #@246
    invoke-virtual/range {v18 .. v18}, Ljava/lang/String;->isEmpty()Z

    #@249
    move-result v3

    #@24a
    const/4 v4, 0x1

    #@24b
    if-ne v3, v4, :cond_26f

    #@24d
    .line 1987
    :cond_24d
    move-object/from16 v0, p0

    #@24f
    move-object/from16 v1, v28

    #@251
    invoke-direct {v0, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->IPCapabilityData_Delete(Landroid/database/Cursor;)Landroid/content/ContentProviderOperation;

    #@254
    move-result-object v23

    #@255
    .line 1988
    if-eqz v23, :cond_25c

    #@257
    .line 1989
    move-object/from16 v0, v23

    #@259
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25c
    .line 1991
    :cond_25c
    const/16 v23, 0x0

    #@25e
    .line 1992
    move-object/from16 v0, p0

    #@260
    move-object/from16 v1, v27

    #@262
    invoke-direct {v0, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->IPCapabilityData_Insert(Landroid/database/Cursor;)Landroid/content/ContentProviderOperation;

    #@265
    move-result-object v23

    #@266
    .line 1993
    if-eqz v23, :cond_dc

    #@268
    .line 1994
    move-object/from16 v0, v23

    #@26a
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@26d
    goto/16 :goto_dc

    #@26f
    .line 1997
    :cond_26f
    move-object/from16 v0, v30

    #@271
    move-object/from16 v1, v18

    #@273
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@276
    move-result v3

    #@277
    if-nez v3, :cond_dc

    #@279
    .line 1998
    const/4 v3, 0x2

    #@27a
    move-object/from16 v0, v28

    #@27c
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@27f
    move-result-object v3

    #@280
    move-object/from16 v0, p0

    #@282
    move-object/from16 v1, v25

    #@284
    move-object/from16 v2, v24

    #@286
    invoke-direct {v0, v1, v2, v3}, Lcom/lge/ims/service/ip/IPContactHelper;->ListUpDeleteRCSUser(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Z

    #@289
    .line 1999
    move-object/from16 v0, p0

    #@28b
    move-object/from16 v1, v28

    #@28d
    invoke-direct {v0, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->IPCapabilityData_Delete(Landroid/database/Cursor;)Landroid/content/ContentProviderOperation;

    #@290
    move-result-object v23

    #@291
    .line 2000
    if-eqz v23, :cond_298

    #@293
    .line 2001
    move-object/from16 v0, v23

    #@295
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@298
    .line 2003
    :cond_298
    const/16 v23, 0x0

    #@29a
    .line 2004
    move-object/from16 v0, p0

    #@29c
    move-object/from16 v1, v27

    #@29e
    invoke-direct {v0, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->IPCapabilityData_Insert(Landroid/database/Cursor;)Landroid/content/ContentProviderOperation;

    #@2a1
    move-result-object v23

    #@2a2
    .line 2005
    if-eqz v23, :cond_dc

    #@2a4
    .line 2006
    move-object/from16 v0, v23

    #@2a6
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2a9
    goto/16 :goto_dc

    #@2ab
    .line 2050
    .end local v18           #imsNumber:Ljava/lang/String;
    .end local v20           #joinerResult:Landroid/database/CursorJoiner$Result;
    .end local v23           #objCPO:Landroid/content/ContentProviderOperation;
    .end local v30           #phoneNumber:Ljava/lang/String;
    .restart local v11       #aobjResults:[Landroid/content/ContentProviderResult;
    :cond_2ab
    :try_start_2ab
    new-instance v3, Ljava/lang/StringBuilder;

    #@2ad
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b0
    const-string v4, "[IP]objList.size["

    #@2b2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b5
    move-result-object v3

    #@2b6
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    #@2b9
    move-result v4

    #@2ba
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2bd
    move-result-object v3

    #@2be
    const-string v4, "]"

    #@2c0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c3
    move-result-object v3

    #@2c4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c7
    move-result-object v3

    #@2c8
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2cb
    .line 2051
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    #@2ce
    move-result v3

    #@2cf
    if-eqz v3, :cond_33f

    #@2d1
    .line 2052
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    #@2d4
    move-result v21

    #@2d5
    .line 2053
    .local v21, nTotalSize:I
    new-instance v29, Lcom/lge/ims/service/ip/ResourceListUserInfo;

    #@2d7
    invoke-direct/range {v29 .. v29}, Lcom/lge/ims/service/ip/ResourceListUserInfo;-><init>()V

    #@2da
    .line 2054
    .local v29, objResourceListUserInfo:Lcom/lge/ims/service/ip/ResourceListUserInfo;
    move-object/from16 v0, v29

    #@2dc
    move/from16 v1, v21

    #@2de
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->SetCurrentResourceListUserSize(I)V

    #@2e1
    .line 2055
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2e4
    move-result-object v17

    #@2e5
    :goto_2e5
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    #@2e8
    move-result v3

    #@2e9
    if-eqz v3, :cond_300

    #@2eb
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2ee
    move-result-object v9

    #@2ef
    check-cast v9, Ljava/lang/String;

    #@2f1
    .line 2056
    .restart local v9       #MSISDN:Ljava/lang/String;
    move-object/from16 v0, v29

    #@2f3
    invoke-virtual {v0, v9}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->SetResourceListUser(Ljava/lang/String;)V
    :try_end_2f6
    .catch Landroid/os/RemoteException; {:try_start_2ab .. :try_end_2f6} :catch_1c7
    .catch Landroid/content/OperationApplicationException; {:try_start_2ab .. :try_end_2f6} :catch_2f7

    #@2f6
    goto :goto_2e5

    #@2f7
    .line 2072
    .end local v9           #MSISDN:Ljava/lang/String;
    .end local v11           #aobjResults:[Landroid/content/ContentProviderResult;
    .end local v21           #nTotalSize:I
    .end local v29           #objResourceListUserInfo:Lcom/lge/ims/service/ip/ResourceListUserInfo;
    :catch_2f7
    move-exception v16

    #@2f8
    .line 2073
    .local v16, e:Landroid/content/OperationApplicationException;
    const-string v3, "[IP][ERROR]Create RCS Contact : Failure(OperationApplicationException)"

    #@2fa
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@2fd
    .line 2074
    const/4 v3, 0x0

    #@2fe
    goto/16 :goto_5b

    #@300
    .line 2058
    .end local v16           #e:Landroid/content/OperationApplicationException;
    .restart local v11       #aobjResults:[Landroid/content/ContentProviderResult;
    .restart local v21       #nTotalSize:I
    .restart local v29       #objResourceListUserInfo:Lcom/lge/ims/service/ip/ResourceListUserInfo;
    :cond_300
    :try_start_300
    new-instance v26, Landroid/os/Message;

    #@302
    invoke-direct/range {v26 .. v26}, Landroid/os/Message;-><init>()V

    #@305
    .line 2059
    .local v26, objMessage:Landroid/os/Message;
    new-instance v22, Landroid/os/Bundle;

    #@307
    invoke-direct/range {v22 .. v22}, Landroid/os/Bundle;-><init>()V

    #@30a
    .line 2060
    .local v22, objBundle:Landroid/os/Bundle;
    const/16 v3, 0xb

    #@30c
    move-object/from16 v0, v26

    #@30e
    iput v3, v0, Landroid/os/Message;->what:I

    #@310
    .line 2061
    const-string v3, "delete_rcsuser"

    #@312
    move-object/from16 v0, v22

    #@314
    move-object/from16 v1, v29

    #@316
    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@319
    .line 2062
    move-object/from16 v0, v26

    #@31b
    move-object/from16 v1, v22

    #@31d
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@320
    .line 2063
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@323
    move-result-object v3

    #@324
    if-eqz v3, :cond_33f

    #@326
    .line 2064
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@329
    move-result-object v3

    #@32a
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@32d
    move-result-object v3

    #@32e
    if-eqz v3, :cond_33f

    #@330
    .line 2065
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@333
    move-result-object v3

    #@334
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@337
    move-result-object v3

    #@338
    const-wide/16 v4, 0x64

    #@33a
    move-object/from16 v0, v26

    #@33c
    invoke-virtual {v3, v0, v4, v5}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z
    :try_end_33f
    .catch Landroid/os/RemoteException; {:try_start_300 .. :try_end_33f} :catch_1c7
    .catch Landroid/content/OperationApplicationException; {:try_start_300 .. :try_end_33f} :catch_2f7

    #@33f
    .line 2077
    .end local v21           #nTotalSize:I
    .end local v22           #objBundle:Landroid/os/Bundle;
    .end local v26           #objMessage:Landroid/os/Message;
    .end local v29           #objResourceListUserInfo:Lcom/lge/ims/service/ip/ResourceListUserInfo;
    :cond_33f
    if-eqz v15, :cond_360

    #@341
    .line 2078
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@344
    move-result-object v3

    #@345
    if-eqz v3, :cond_360

    #@347
    .line 2079
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@34a
    move-result-object v3

    #@34b
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@34e
    move-result-object v3

    #@34f
    if-eqz v3, :cond_360

    #@351
    .line 2080
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@354
    move-result-object v3

    #@355
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@358
    move-result-object v3

    #@359
    const/16 v4, 0x13

    #@35b
    const-wide/16 v5, 0x1388

    #@35d
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@360
    .line 2085
    :cond_360
    const/4 v3, 0x1

    #@361
    goto/16 :goto_5b

    #@363
    .line 1949
    nop

    #@364
    :pswitch_data_364
    .packed-switch 0x1
        :pswitch_1df
        :pswitch_1f0
        :pswitch_213
    .end packed-switch
.end method

.method private ListUpDeleteRCSUser(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Z
    .registers 8
    .parameter
    .parameter
    .parameter "number"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    #@0
    .prologue
    .local p1, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .local p2, objDuplicationList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v3, 0x1

    #@1
    .line 1866
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v2, "[IP] : number ["

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    const-string v2, "]"

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1d
    .line 1867
    invoke-direct {p0, p3}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSUserCount(Ljava/lang/String;)I

    #@20
    move-result v0

    #@21
    .line 1868
    .local v0, nUserCount:I
    if-le v0, v3, :cond_4f

    #@23
    .line 1869
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@26
    move-result v1

    #@27
    if-nez v1, :cond_4e

    #@29
    .line 1870
    invoke-interface {p2, p3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@2c
    move-result v1

    #@2d
    if-nez v1, :cond_4e

    #@2f
    .line 1871
    invoke-interface {p2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@32
    .line 1872
    new-instance v1, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v2, "[IP]objDuplicationList Added [ "

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    const-string v2, " ]"

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4e
    .line 1878
    :cond_4e
    :goto_4e
    return v3

    #@4f
    .line 1875
    :cond_4f
    if-ne v0, v3, :cond_4e

    #@51
    .line 1876
    invoke-direct {p0, p3, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->RCSeData_DeleteUserListUp(Ljava/lang/String;Ljava/util/List;)V

    #@54
    goto :goto_4e
.end method

.method private PresenceData_CheckStatusIconEtag(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;Lcom/lge/ims/service/ip/PresenceInfo;)Z
    .registers 14
    .parameter "objPresenceContactData"
    .parameter "objPresenceInfo"

    #@0
    .prologue
    const/16 v10, 0x1c

    #@2
    const/16 v9, 0x1b

    #@4
    const/4 v8, 0x1

    #@5
    const-wide/16 v6, 0x1f4

    #@7
    .line 2786
    if-nez p1, :cond_78

    #@9
    .line 2787
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@c
    move-result-object v4

    #@d
    if-eqz v4, :cond_77

    #@f
    .line 2788
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@12
    move-result-object v4

    #@13
    if-eqz v4, :cond_77

    #@15
    .line 2789
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@1c
    move-result-object v4

    #@1d
    if-eqz v4, :cond_77

    #@1f
    .line 2790
    new-instance v3, Landroid/os/Message;

    #@21
    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    #@24
    .line 2791
    .local v3, objStatusIconThumbMessage:Landroid/os/Message;
    new-instance v2, Landroid/os/Bundle;

    #@26
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@29
    .line 2792
    .local v2, objStatusIconThumbBundle:Landroid/os/Bundle;
    iput v9, v3, Landroid/os/Message;->what:I

    #@2b
    .line 2793
    const-string v4, "statusIconThumbUrl"

    #@2d
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    .line 2794
    const-string v4, "msisdn"

    #@36
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    .line 2795
    invoke-virtual {v3, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@40
    .line 2796
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@43
    move-result-object v4

    #@44
    invoke-virtual {v4}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4, v3, v6, v7}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@4b
    .line 2798
    new-instance v1, Landroid/os/Message;

    #@4d
    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    #@50
    .line 2799
    .local v1, objStatusIconMessage:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    #@52
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@55
    .line 2800
    .local v0, objStatusIconBundle:Landroid/os/Bundle;
    iput v10, v1, Landroid/os/Message;->what:I

    #@57
    .line 2801
    const-string v4, "statusIconUrl"

    #@59
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@60
    .line 2802
    const-string v4, "msisdn"

    #@62
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@69
    .line 2803
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@6c
    .line 2804
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@6f
    move-result-object v4

    #@70
    invoke-virtual {v4}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4, v1, v6, v7}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@77
    .line 2860
    .end local v0           #objStatusIconBundle:Landroid/os/Bundle;
    .end local v1           #objStatusIconMessage:Landroid/os/Message;
    .end local v2           #objStatusIconThumbBundle:Landroid/os/Bundle;
    .end local v3           #objStatusIconThumbMessage:Landroid/os/Message;
    :cond_77
    :goto_77
    return v8

    #@78
    .line 2809
    :cond_78
    new-instance v4, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v5, "[IP]strStatusIconThumbEtag ["

    #@7f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v4

    #@83
    iget-object v5, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconThumbEtag:Ljava/lang/String;

    #@85
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v4

    #@89
    const-string v5, "], GetStatusIconThumbEtag[ "

    #@8b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v4

    #@8f
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@92
    move-result-object v5

    #@93
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v4

    #@97
    const-string v5, "]"

    #@99
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v4

    #@9d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v4

    #@a1
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a4
    .line 2810
    iget-object v4, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconThumbEtag:Ljava/lang/String;

    #@a6
    if-eqz v4, :cond_132

    #@a8
    iget-object v4, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconThumbEtag:Ljava/lang/String;

    #@aa
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    #@ad
    move-result v4

    #@ae
    if-eq v4, v8, :cond_132

    #@b0
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@b3
    move-result-object v4

    #@b4
    if-eqz v4, :cond_132

    #@b6
    .line 2811
    iget-object v4, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconThumbEtag:Ljava/lang/String;

    #@b8
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@bb
    move-result-object v5

    #@bc
    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@bf
    move-result v4

    #@c0
    if-eq v4, v8, :cond_77

    #@c2
    .line 2814
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@c5
    move-result-object v4

    #@c6
    if-eqz v4, :cond_77

    #@c8
    .line 2815
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@cb
    move-result-object v4

    #@cc
    if-eqz v4, :cond_77

    #@ce
    .line 2816
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@d1
    move-result-object v4

    #@d2
    invoke-virtual {v4}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@d5
    move-result-object v4

    #@d6
    if-eqz v4, :cond_77

    #@d8
    .line 2817
    new-instance v3, Landroid/os/Message;

    #@da
    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    #@dd
    .line 2818
    .restart local v3       #objStatusIconThumbMessage:Landroid/os/Message;
    new-instance v2, Landroid/os/Bundle;

    #@df
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@e2
    .line 2819
    .restart local v2       #objStatusIconThumbBundle:Landroid/os/Bundle;
    iput v9, v3, Landroid/os/Message;->what:I

    #@e4
    .line 2820
    const-string v4, "statusIconThumbUrl"

    #@e6
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@e9
    move-result-object v5

    #@ea
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@ed
    .line 2821
    const-string v4, "msisdn"

    #@ef
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@f2
    move-result-object v5

    #@f3
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@f6
    .line 2822
    invoke-virtual {v3, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@f9
    .line 2823
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@fc
    move-result-object v4

    #@fd
    invoke-virtual {v4}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@100
    move-result-object v4

    #@101
    invoke-virtual {v4, v3, v6, v7}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@104
    .line 2825
    new-instance v1, Landroid/os/Message;

    #@106
    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    #@109
    .line 2826
    .restart local v1       #objStatusIconMessage:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    #@10b
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@10e
    .line 2827
    .restart local v0       #objStatusIconBundle:Landroid/os/Bundle;
    iput v10, v1, Landroid/os/Message;->what:I

    #@110
    .line 2828
    const-string v4, "statusIconUrl"

    #@112
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@115
    move-result-object v5

    #@116
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@119
    .line 2829
    const-string v4, "msisdn"

    #@11b
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@11e
    move-result-object v5

    #@11f
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@122
    .line 2830
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@125
    .line 2831
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@128
    move-result-object v4

    #@129
    invoke-virtual {v4}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@12c
    move-result-object v4

    #@12d
    invoke-virtual {v4, v1, v6, v7}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@130
    goto/16 :goto_77

    #@132
    .line 2836
    .end local v0           #objStatusIconBundle:Landroid/os/Bundle;
    .end local v1           #objStatusIconMessage:Landroid/os/Message;
    .end local v2           #objStatusIconThumbBundle:Landroid/os/Bundle;
    .end local v3           #objStatusIconThumbMessage:Landroid/os/Message;
    :cond_132
    iget-object v4, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconThumbEtag:Ljava/lang/String;

    #@134
    if-eqz v4, :cond_13e

    #@136
    iget-object v4, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconThumbEtag:Ljava/lang/String;

    #@138
    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    #@13b
    move-result v4

    #@13c
    if-ne v4, v8, :cond_77

    #@13e
    :cond_13e
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@141
    move-result-object v4

    #@142
    if-eqz v4, :cond_77

    #@144
    .line 2837
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@147
    move-result-object v4

    #@148
    if-eqz v4, :cond_77

    #@14a
    .line 2838
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@14d
    move-result-object v4

    #@14e
    if-eqz v4, :cond_77

    #@150
    .line 2839
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@153
    move-result-object v4

    #@154
    invoke-virtual {v4}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@157
    move-result-object v4

    #@158
    if-eqz v4, :cond_77

    #@15a
    .line 2840
    new-instance v3, Landroid/os/Message;

    #@15c
    invoke-direct {v3}, Landroid/os/Message;-><init>()V

    #@15f
    .line 2841
    .restart local v3       #objStatusIconThumbMessage:Landroid/os/Message;
    new-instance v2, Landroid/os/Bundle;

    #@161
    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    #@164
    .line 2842
    .restart local v2       #objStatusIconThumbBundle:Landroid/os/Bundle;
    iput v9, v3, Landroid/os/Message;->what:I

    #@166
    .line 2843
    const-string v4, "statusIconThumbUrl"

    #@168
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@16b
    move-result-object v5

    #@16c
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@16f
    .line 2844
    const-string v4, "msisdn"

    #@171
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@174
    move-result-object v5

    #@175
    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@178
    .line 2845
    invoke-virtual {v3, v2}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@17b
    .line 2846
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@17e
    move-result-object v4

    #@17f
    invoke-virtual {v4}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@182
    move-result-object v4

    #@183
    invoke-virtual {v4, v3, v6, v7}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@186
    .line 2848
    new-instance v1, Landroid/os/Message;

    #@188
    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    #@18b
    .line 2849
    .restart local v1       #objStatusIconMessage:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    #@18d
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@190
    .line 2850
    .restart local v0       #objStatusIconBundle:Landroid/os/Bundle;
    iput v10, v1, Landroid/os/Message;->what:I

    #@192
    .line 2851
    const-string v4, "statusIconUrl"

    #@194
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@197
    move-result-object v5

    #@198
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@19b
    .line 2852
    const-string v4, "msisdn"

    #@19d
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@1a0
    move-result-object v5

    #@1a1
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1a4
    .line 2853
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@1a7
    .line 2854
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1aa
    move-result-object v4

    #@1ab
    invoke-virtual {v4}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@1ae
    move-result-object v4

    #@1af
    invoke-virtual {v4, v1, v6, v7}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@1b2
    goto/16 :goto_77
.end method

.method private PresenceData_Delete(Ljava/lang/String;)Z
    .registers 12
    .parameter "strMSISDN"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 2700
    if-eqz p1, :cond_a

    #@4
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@7
    move-result v8

    #@8
    if-eqz v8, :cond_11

    #@a
    .line 2701
    :cond_a
    const-string v6, "[IP][ERROR]strMSISDN is null"

    #@c
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@f
    move v6, v7

    #@10
    .line 2728
    :cond_10
    :goto_10
    return v6

    #@11
    .line 2704
    :cond_11
    const/4 v8, 0x0

    #@12
    invoke-direct {p0, p1, v8}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@15
    move-result-object v3

    #@16
    .line 2705
    .local v3, objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-eqz v3, :cond_26

    #@18
    .line 2706
    iget-object v8, v3, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIcon:Ljava/lang/String;

    #@1a
    if-eqz v8, :cond_26

    #@1c
    .line 2707
    new-instance v5, Ljava/io/File;

    #@1e
    iget-object v8, v3, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIcon:Ljava/lang/String;

    #@20
    invoke-direct {v5, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@23
    .line 2708
    .local v5, tempFile:Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    #@26
    .line 2711
    .end local v5           #tempFile:Ljava/io/File;
    :cond_26
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->BuddyImageDB_Delete(Ljava/lang/String;)Z

    #@29
    .line 2712
    const-string v4, "nor_msisdn=?"

    #@2b
    .line 2713
    .local v4, strSelection:Ljava/lang/String;
    new-array v0, v6, [Ljava/lang/String;

    #@2d
    aput-object p1, v0, v7

    #@2f
    .line 2715
    .local v0, astrSelectionArg:[Ljava/lang/String;
    iget-object v8, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@31
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@34
    move-result-object v8

    #@35
    sget-object v9, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@37
    invoke-virtual {v8, v9, v4, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@3a
    move-result v1

    #@3b
    .line 2716
    .local v1, delete:I
    new-instance v8, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v9, "[IP]PresenceData_Delete : MSISDN [ "

    #@42
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v8

    #@46
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v8

    #@4a
    const-string v9, " ], delete result["

    #@4c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v8

    #@50
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v8

    #@54
    const-string v9, "]"

    #@56
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v8

    #@5a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v8

    #@5e
    invoke-static {v8}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@61
    .line 2717
    const/4 v8, -0x1

    #@62
    if-eq v1, v8, :cond_9f

    #@64
    .line 2718
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceUserCount()I

    #@67
    move-result v2

    #@68
    .line 2719
    .local v2, nPresenceCount:I
    new-instance v8, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v9, "[IP]nPresenceCount [ "

    #@6f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v8

    #@73
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@76
    move-result-object v8

    #@77
    const-string v9, " ], nLimitationResourceListUserCount [ "

    #@79
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    sget v9, Lcom/lge/ims/service/ip/IPContactHelper;->nLimitationResourceListUserCount:I

    #@7f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    const-string v9, " ]"

    #@85
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v8

    #@89
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v8

    #@8d
    invoke-static {v8}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@90
    .line 2720
    sget v8, Lcom/lge/ims/service/ip/IPContactHelper;->nLimitationResourceListUserCount:I

    #@92
    if-ge v2, v8, :cond_10

    #@94
    .line 2721
    sget-boolean v8, Lcom/lge/ims/service/ip/IPContactHelper;->isExcessMaxUserSize:Z

    #@96
    if-ne v8, v6, :cond_10

    #@98
    .line 2722
    sput-boolean v7, Lcom/lge/ims/service/ip/IPContactHelper;->isExcessMaxUserSize:Z

    #@9a
    .line 2723
    invoke-direct {p0, v7}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateExcessMaxUserValue(I)Z

    #@9d
    goto/16 :goto_10

    #@9f
    .end local v2           #nPresenceCount:I
    :cond_9f
    move v6, v7

    #@a0
    .line 2728
    goto/16 :goto_10
.end method

.method private PresenceData_Insert(Lcom/lge/ims/service/ip/PresenceInfo;)Z
    .registers 10
    .parameter "objPresenceInfo"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2617
    new-instance v5, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v6, "[IP]: msisdn [ "

    #@8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v5

    #@c
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    const-string v6, " ]"

    #@16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v5

    #@1e
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@21
    .line 2618
    const/4 v2, 0x0

    #@22
    .line 2619
    .local v2, objUri:Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    #@24
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@27
    .line 2620
    .local v1, objContentValues:Landroid/content/ContentValues;
    const-string v5, "nor_msisdn"

    #@29
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@30
    .line 2621
    const-string v5, "homepage"

    #@32
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetHomePage()Ljava/lang/String;

    #@35
    move-result-object v6

    #@36
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 2622
    const-string v5, "free_text"

    #@3b
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetFreeText()Ljava/lang/String;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    .line 2623
    const-string v5, "e_mail"

    #@44
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetEMailAddress()Ljava/lang/String;

    #@47
    move-result-object v6

    #@48
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 2624
    const-string v5, "birthday"

    #@4d
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetBirthDay()Ljava/lang/String;

    #@50
    move-result-object v6

    #@51
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@54
    .line 2625
    const-string v5, "status"

    #@56
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatus()I

    #@59
    move-result v6

    #@5a
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5d
    move-result-object v6

    #@5e
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@61
    .line 2626
    const-string v5, "twitter_account"

    #@63
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetTwitterAccount()Ljava/lang/String;

    #@66
    move-result-object v6

    #@67
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6a
    .line 2627
    const-string v5, "facebook_account"

    #@6c
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetFaceBookAccount()Ljava/lang/String;

    #@6f
    move-result-object v6

    #@70
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 2628
    const-string v5, "cyworld_account"

    #@75
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetCyworldAccount()Ljava/lang/String;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7c
    .line 2629
    const-string v5, "waggle_account"

    #@7e
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetWaggleAccount()Ljava/lang/String;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@85
    .line 2630
    const-string v5, "status_icon"

    #@87
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIcon()Ljava/lang/String;

    #@8a
    move-result-object v6

    #@8b
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@8e
    .line 2631
    const-string v5, "status_icon_link"

    #@90
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@93
    move-result-object v6

    #@94
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@97
    .line 2632
    const-string v5, "status_icon_thumb"

    #@99
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@9c
    move-result-object v6

    #@9d
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a0
    .line 2633
    const-string v5, "status_icon_thumb_link"

    #@a2
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@a5
    move-result-object v6

    #@a6
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a9
    .line 2634
    const-string v5, "status_icon_thumb_etag"

    #@ab
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@ae
    move-result-object v6

    #@af
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b2
    .line 2635
    const-string v5, "time_stamp"

    #@b4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@b7
    move-result-wide v6

    #@b8
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@bb
    move-result-object v6

    #@bc
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@bf
    .line 2636
    const-string v5, "statusicon_update"

    #@c1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c4
    move-result-object v6

    #@c5
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@c8
    .line 2637
    iget-object v5, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@ca
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@cd
    move-result-object v5

    #@ce
    sget-object v6, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@d0
    invoke-virtual {v5, v6, v1}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@d3
    move-result-object v2

    #@d4
    .line 2638
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@d7
    .line 2639
    if-nez v2, :cond_df

    #@d9
    .line 2640
    const-string v5, "[IP][ERROR]PresenceData_Insert : presence DB insert failed"

    #@db
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@de
    .line 2648
    :goto_de
    return v4

    #@df
    .line 2643
    :cond_df
    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@e2
    move-result-object v0

    #@e3
    .line 2644
    .local v0, mDataId:Ljava/lang/String;
    new-instance v3, Ljava/lang/Long;

    #@e5
    invoke-direct {v3, v0}, Ljava/lang/Long;-><init>(Ljava/lang/String;)V

    #@e8
    .line 2645
    .local v3, oblong:Ljava/lang/Long;
    new-instance v4, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v5, "[IP]PresenceData_Insert : Uri Data Id ["

    #@ef
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v4

    #@f3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v4

    #@f7
    const-string v5, "], long value ["

    #@f9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v4

    #@fd
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@100
    move-result-wide v5

    #@101
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@104
    move-result-object v4

    #@105
    const-string v5, "]"

    #@107
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v4

    #@10b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10e
    move-result-object v4

    #@10f
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@112
    .line 2646
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@115
    move-result-wide v4

    #@116
    invoke-direct {p0, p1, v4, v5}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactData_Update(Lcom/lge/ims/service/ip/PresenceInfo;J)Z

    #@119
    .line 2647
    const-string v4, "[IP]PresenceData_Insert : presence DB insert success"

    #@11b
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@11e
    .line 2648
    const/4 v4, 0x1

    #@11f
    goto :goto_de
.end method

.method private PresenceData_StatusIconUpdate(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;Lcom/lge/ims/service/ip/PresenceInfo;)Z
    .registers 16
    .parameter "objPresenceContactData"
    .parameter "objPresenceInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 2733
    const-string v10, "[IP]"

    #@2
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 2734
    const-string v5, "_id=?"

    #@7
    .line 2735
    .local v5, strSelection:Ljava/lang/String;
    const/4 v10, 0x1

    #@8
    new-array v0, v10, [Ljava/lang/String;

    #@a
    const/4 v10, 0x0

    #@b
    iget-wide v11, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nId:J

    #@d
    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@10
    move-result-object v11

    #@11
    aput-object v11, v0, v10

    #@13
    .line 2736
    .local v0, astrSelectionArg:[Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    #@15
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@18
    .line 2738
    .local v3, objContentValues:Landroid/content/ContentValues;
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIcon()Ljava/lang/String;

    #@1b
    move-result-object v10

    #@1c
    if-eqz v10, :cond_3b

    #@1e
    .line 2739
    invoke-direct {p0, p2}, Lcom/lge/ims/service/ip/IPContactHelper;->BuddyImageDB_Update(Lcom/lge/ims/service/ip/PresenceInfo;)Z

    #@21
    .line 2740
    invoke-direct {p0, p2}, Lcom/lge/ims/service/ip/IPContactHelper;->SyncFullImageToContact(Lcom/lge/ims/service/ip/PresenceInfo;)Z

    #@24
    move-result v10

    #@25
    const/4 v11, 0x1

    #@26
    if-ne v10, v11, :cond_96

    #@28
    .line 2741
    const-string v10, "statusicon_update"

    #@2a
    const/4 v11, 0x1

    #@2b
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v11

    #@2f
    invoke-virtual {v3, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@32
    .line 2746
    :goto_32
    const-string v10, "status_icon"

    #@34
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIcon()Ljava/lang/String;

    #@37
    move-result-object v11

    #@38
    invoke-virtual {v3, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 2748
    :cond_3b
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@3e
    move-result-object v10

    #@3f
    if-eqz v10, :cond_4a

    #@41
    .line 2749
    const-string v10, "status_icon_link"

    #@43
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@46
    move-result-object v11

    #@47
    invoke-virtual {v3, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4a
    .line 2751
    :cond_4a
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@4d
    move-result-object v10

    #@4e
    if-eqz v10, :cond_c1

    #@50
    .line 2752
    new-instance v10, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v11, "[IP]thumbnail file path : "

    #@57
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v10

    #@5b
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@5e
    move-result-object v11

    #@5f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v10

    #@63
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v10

    #@67
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6a
    .line 2753
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@6d
    move-result-object v10

    #@6e
    invoke-static {v10}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@71
    move-result-object v1

    #@72
    .line 2754
    .local v1, bit:Landroid/graphics/Bitmap;
    if-eqz v1, :cond_f3

    #@74
    .line 2755
    const/16 v10, 0x60

    #@76
    const/16 v11, 0x60

    #@78
    const/4 v12, 0x1

    #@79
    invoke-static {v1, v10, v11, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    #@7c
    move-result-object v4

    #@7d
    .line 2756
    .local v4, resized:Landroid/graphics/Bitmap;
    if-eqz v4, :cond_b5

    #@7f
    .line 2757
    new-instance v6, Ljava/io/ByteArrayOutputStream;

    #@81
    invoke-direct {v6}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@84
    .line 2758
    .local v6, stream:Ljava/io/ByteArrayOutputStream;
    sget-object v10, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@86
    const/16 v11, 0x5f

    #@88
    invoke-virtual {v4, v10, v11, v6}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@8b
    move-result v2

    #@8c
    .line 2759
    .local v2, compressed:Z
    if-nez v2, :cond_a6

    #@8e
    .line 2760
    new-instance v10, Ljava/io/IOException;

    #@90
    const-string v11, "Unable to compress image"

    #@92
    invoke-direct {v10, v11}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@95
    throw v10

    #@96
    .line 2743
    .end local v1           #bit:Landroid/graphics/Bitmap;
    .end local v2           #compressed:Z
    .end local v4           #resized:Landroid/graphics/Bitmap;
    .end local v6           #stream:Ljava/io/ByteArrayOutputStream;
    :cond_96
    const-string v10, "statusicon_update"

    #@98
    const/4 v11, 0x2

    #@99
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9c
    move-result-object v11

    #@9d
    invoke-virtual {v3, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@a0
    .line 2744
    const-string v10, "[IP]set KEY_PRES_STATUSICON_UPDATE is 2"

    #@a2
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a5
    goto :goto_32

    #@a6
    .line 2762
    .restart local v1       #bit:Landroid/graphics/Bitmap;
    .restart local v2       #compressed:Z
    .restart local v4       #resized:Landroid/graphics/Bitmap;
    .restart local v6       #stream:Ljava/io/ByteArrayOutputStream;
    :cond_a6
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->flush()V

    #@a9
    .line 2763
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V

    #@ac
    .line 2764
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@af
    move-result-object v9

    #@b0
    .line 2765
    .local v9, value:[B
    const-string v10, "status_icon_thumb"

    #@b2
    invoke-virtual {v3, v10, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@b5
    .line 2770
    .end local v2           #compressed:Z
    .end local v4           #resized:Landroid/graphics/Bitmap;
    .end local v6           #stream:Ljava/io/ByteArrayOutputStream;
    .end local v9           #value:[B
    :cond_b5
    :goto_b5
    new-instance v7, Ljava/io/File;

    #@b7
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@ba
    move-result-object v10

    #@bb
    invoke-direct {v7, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@be
    .line 2771
    .local v7, tempFile:Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    #@c1
    .line 2773
    .end local v1           #bit:Landroid/graphics/Bitmap;
    .end local v7           #tempFile:Ljava/io/File;
    :cond_c1
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@c4
    move-result-object v10

    #@c5
    if-eqz v10, :cond_d0

    #@c7
    .line 2774
    const-string v10, "status_icon_thumb_link"

    #@c9
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@cc
    move-result-object v11

    #@cd
    invoke-virtual {v3, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d0
    .line 2776
    :cond_d0
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@d3
    move-result-object v10

    #@d4
    if-eqz v10, :cond_df

    #@d6
    .line 2777
    const-string v10, "status_icon_thumb_etag"

    #@d8
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@db
    move-result-object v11

    #@dc
    invoke-virtual {v3, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@df
    .line 2780
    :cond_df
    iget-object v10, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@e1
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e4
    move-result-object v10

    #@e5
    sget-object v11, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@e7
    invoke-virtual {v10, v11, v3, v5, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@ea
    move-result v8

    #@eb
    .line 2781
    .local v8, update:I
    invoke-virtual {v3}, Landroid/content/ContentValues;->clear()V

    #@ee
    .line 2783
    const/4 v10, -0x1

    #@ef
    if-eq v8, v10, :cond_f9

    #@f1
    const/4 v10, 0x1

    #@f2
    :goto_f2
    return v10

    #@f3
    .line 2768
    .end local v8           #update:I
    .restart local v1       #bit:Landroid/graphics/Bitmap;
    :cond_f3
    const-string v10, "[IP][ERROR]thumbnail decode file is null"

    #@f5
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@f8
    goto :goto_b5

    #@f9
    .line 2783
    .end local v1           #bit:Landroid/graphics/Bitmap;
    .restart local v8       #update:I
    :cond_f9
    const/4 v10, 0x0

    #@fa
    goto :goto_f2
.end method

.method private PresenceData_Update(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;Lcom/lge/ims/service/ip/PresenceInfo;)Z
    .registers 14
    .parameter "objPresenceContactData"
    .parameter "objPresenceInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v6, 0x0

    #@3
    .line 2652
    const-string v5, "[IP]"

    #@5
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8
    .line 2654
    const-string v2, "_id=?"

    #@a
    .line 2655
    .local v2, strSelection:Ljava/lang/String;
    new-array v0, v7, [Ljava/lang/String;

    #@c
    iget-wide v9, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nId:J

    #@e
    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@11
    move-result-object v5

    #@12
    aput-object v5, v0, v8

    #@14
    .line 2656
    .local v0, astrSelectionArg:[Ljava/lang/String;
    new-instance v1, Landroid/content/ContentValues;

    #@16
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@19
    .line 2657
    .local v1, objContentValues:Landroid/content/ContentValues;
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v9, "[IP]"

    #@20
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->toString()Ljava/lang/String;

    #@27
    move-result-object v9

    #@28
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@33
    .line 2659
    const-string v5, "homepage"

    #@35
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetHomePage()Ljava/lang/String;

    #@38
    move-result-object v9

    #@39
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 2660
    const-string v5, "free_text"

    #@3e
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetFreeText()Ljava/lang/String;

    #@41
    move-result-object v9

    #@42
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 2661
    const-string v5, "e_mail"

    #@47
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetEMailAddress()Ljava/lang/String;

    #@4a
    move-result-object v9

    #@4b
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4e
    .line 2662
    const-string v5, "birthday"

    #@50
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetBirthDay()Ljava/lang/String;

    #@53
    move-result-object v9

    #@54
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@57
    .line 2663
    const-string v5, "status"

    #@59
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatus()I

    #@5c
    move-result v9

    #@5d
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@60
    move-result-object v9

    #@61
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@64
    .line 2664
    const-string v5, "twitter_account"

    #@66
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetTwitterAccount()Ljava/lang/String;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 2665
    const-string v5, "facebook_account"

    #@6f
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetFaceBookAccount()Ljava/lang/String;

    #@72
    move-result-object v9

    #@73
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 2666
    const-string v5, "cyworld_account"

    #@78
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetCyworldAccount()Ljava/lang/String;

    #@7b
    move-result-object v9

    #@7c
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7f
    .line 2667
    const-string v5, "waggle_account"

    #@81
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetWaggleAccount()Ljava/lang/String;

    #@84
    move-result-object v9

    #@85
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@88
    .line 2668
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@8b
    move-result-object v5

    #@8c
    if-eqz v5, :cond_d5

    #@8e
    .line 2669
    const-string v5, "status_icon_link"

    #@90
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconLink()Ljava/lang/String;

    #@93
    move-result-object v9

    #@94
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@97
    .line 2681
    :goto_97
    const-string v5, "status_icon_thumb_etag"

    #@99
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@9c
    move-result-object v9

    #@9d
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a0
    .line 2682
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@a3
    move-result-object v5

    #@a4
    if-eqz v5, :cond_12b

    #@a6
    .line 2683
    const-string v5, "status_icon_thumb_link"

    #@a8
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@ab
    move-result-object v6

    #@ac
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@af
    .line 2688
    :goto_af
    const-string v5, "time_stamp"

    #@b1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@b4
    move-result-wide v9

    #@b5
    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b8
    move-result-object v6

    #@b9
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@bc
    .line 2689
    iget-object v5, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@be
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c1
    move-result-object v5

    #@c2
    sget-object v6, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@c4
    invoke-virtual {v5, v6, v1, v2, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@c7
    move-result v4

    #@c8
    .line 2690
    .local v4, update:I
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@cb
    .line 2692
    const/4 v5, -0x1

    #@cc
    if-eq v4, v5, :cond_13c

    #@ce
    .line 2693
    iget-wide v5, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nId:J

    #@d0
    invoke-direct {p0, p2, v5, v6}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactData_Update(Lcom/lge/ims/service/ip/PresenceInfo;J)Z

    #@d3
    move v5, v7

    #@d4
    .line 2696
    :goto_d4
    return v5

    #@d5
    .line 2671
    .end local v4           #update:I
    :cond_d5
    iget-object v5, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconLink:Ljava/lang/String;

    #@d7
    if-eqz v5, :cond_110

    #@d9
    iget-object v5, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconLink:Ljava/lang/String;

    #@db
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    #@de
    move-result v5

    #@df
    if-eqz v5, :cond_110

    #@e1
    .line 2672
    new-instance v5, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v9, "[IP]file delete [ "

    #@e8
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v5

    #@ec
    iget-object v9, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconLink:Ljava/lang/String;

    #@ee
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v5

    #@f2
    const-string v9, " ]"

    #@f4
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v5

    #@f8
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fb
    move-result-object v5

    #@fc
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@ff
    .line 2673
    new-instance v3, Ljava/io/File;

    #@101
    iget-object v5, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconLink:Ljava/lang/String;

    #@103
    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@106
    .line 2674
    .local v3, tempFile:Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    #@109
    .line 2675
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@10c
    move-result-object v5

    #@10d
    invoke-direct {p0, v5}, Lcom/lge/ims/service/ip/IPContactHelper;->BuddyImageDB_Delete(Ljava/lang/String;)Z

    #@110
    .line 2677
    .end local v3           #tempFile:Ljava/io/File;
    :cond_110
    const-string v5, "statusicon_update"

    #@112
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@115
    move-result-object v9

    #@116
    invoke-virtual {v1, v5, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@119
    .line 2678
    const-string v9, "status_icon"

    #@11b
    move-object v5, v6

    #@11c
    check-cast v5, Ljava/lang/String;

    #@11e
    invoke-virtual {v1, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@121
    .line 2679
    const-string v9, "status_icon_link"

    #@123
    move-object v5, v6

    #@124
    check-cast v5, Ljava/lang/String;

    #@126
    invoke-virtual {v1, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@129
    goto/16 :goto_97

    #@12b
    .line 2685
    :cond_12b
    const-string v9, "status_icon_thumb"

    #@12d
    move-object v5, v6

    #@12e
    check-cast v5, Ljava/lang/Byte;

    #@130
    invoke-virtual {v1, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    #@133
    .line 2686
    const-string v5, "status_icon_thumb_link"

    #@135
    check-cast v6, Ljava/lang/String;

    #@137
    invoke-virtual {v1, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@13a
    goto/16 :goto_af

    #@13c
    .restart local v4       #update:I
    :cond_13c
    move v5, v8

    #@13d
    .line 2696
    goto :goto_d4
.end method

.method private RCSeData_DeleteUserListUp(Ljava/lang/String;Ljava/util/List;)V
    .registers 13
    .parameter "number"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p2, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x1

    #@1
    .line 2472
    const-string v3, "msisdn=? AND rcs_user_list=?"

    #@3
    .line 2473
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x2

    #@4
    new-array v4, v0, [Ljava/lang/String;

    #@6
    const/4 v0, 0x0

    #@7
    aput-object p1, v4, v0

    #@9
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@c
    move-result-object v0

    #@d
    aput-object v0, v4, v1

    #@f
    .line 2474
    .local v4, astrSelectionArg:[Ljava/lang/String;
    const/4 v8, 0x0

    #@10
    .line 2476
    .local v8, objCursor:Landroid/database/Cursor;
    :try_start_10
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@12
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@15
    move-result-object v0

    #@16
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@18
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@1a
    const/4 v5, 0x0

    #@1b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1e
    move-result-object v8

    #@1f
    .line 2477
    if-nez v8, :cond_5c

    #@21
    .line 2478
    const-string v0, "[IP][ERROR] RCSeData_DeleteUserListUp : objCursor is null"

    #@23
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_26
    .catchall {:try_start_10 .. :try_end_26} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_26} :catch_6b

    #@26
    .line 2498
    :cond_26
    :goto_26
    if-eqz v8, :cond_2c

    #@28
    .line 2499
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@2b
    .line 2500
    const/4 v8, 0x0

    #@2c
    .line 2503
    :cond_2c
    :goto_2c
    return-void

    #@2d
    .line 2486
    .local v6, columnNormalMsisdnIndex:I
    :cond_2d
    :try_start_2d
    invoke-interface {v8, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@30
    move-result-object v9

    #@31
    .line 2487
    .local v9, strNormalizeMSISDN:Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@34
    move-result v0

    #@35
    if-nez v0, :cond_5c

    #@37
    .line 2488
    invoke-interface {p2, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@3a
    move-result v0

    #@3b
    if-nez v0, :cond_5c

    #@3d
    .line 2489
    invoke-interface {p2, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@40
    .line 2490
    new-instance v0, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v1, "[IP]RCSeData_DeleteUserListUp Added [ "

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    const-string v1, " ]"

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v0

    #@59
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5c
    .line 2480
    .end local v6           #columnNormalMsisdnIndex:I
    .end local v9           #strNormalizeMSISDN:Ljava/lang/String;
    :cond_5c
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@5f
    move-result v0

    #@60
    if-eqz v0, :cond_26

    #@62
    .line 2481
    const-string v0, "nor_msisdn"

    #@64
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_67
    .catchall {:try_start_2d .. :try_end_67} :catchall_89
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_67} :catch_6b

    #@67
    move-result v6

    #@68
    .line 2482
    .restart local v6       #columnNormalMsisdnIndex:I
    if-gez v6, :cond_2d

    #@6a
    goto :goto_26

    #@6b
    .line 2495
    .end local v6           #columnNormalMsisdnIndex:I
    :catch_6b
    move-exception v7

    #@6c
    .line 2496
    .local v7, e:Ljava/lang/Exception;
    :try_start_6c
    new-instance v0, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v1, "[IP][ERROR]exception : "

    #@73
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v0

    #@77
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v0

    #@7b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v0

    #@7f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_82
    .catchall {:try_start_6c .. :try_end_82} :catchall_89

    #@82
    .line 2498
    if-eqz v8, :cond_2c

    #@84
    .line 2499
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@87
    .line 2500
    const/4 v8, 0x0

    #@88
    goto :goto_2c

    #@89
    .line 2498
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_89
    move-exception v0

    #@8a
    if-eqz v8, :cond_90

    #@8c
    .line 2499
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@8f
    .line 2500
    const/4 v8, 0x0

    #@90
    :cond_90
    throw v0
.end method

.method private RCSeData_Update(JJ)I
    .registers 11
    .parameter "nId"
    .parameter "nImageUriId"

    #@0
    .prologue
    .line 2601
    new-instance v4, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v5, "[IP] nId : ["

    #@7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v4

    #@b
    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    const-string v5, "], nImageUriId [ "

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    const-string v5, "]"

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@26
    .line 2602
    const-string v2, "_id=?"

    #@28
    .line 2603
    .local v2, strSelection:Ljava/lang/String;
    const/4 v4, 0x1

    #@29
    new-array v0, v4, [Ljava/lang/String;

    #@2b
    const/4 v4, 0x0

    #@2c
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    aput-object v5, v0, v4

    #@32
    .line 2604
    .local v0, astrSelectionArg:[Ljava/lang/String;
    new-instance v1, Landroid/content/ContentValues;

    #@34
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@37
    .line 2606
    .local v1, objContentValues:Landroid/content/ContentValues;
    const-string v4, "image_uri_id"

    #@39
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3c
    move-result-object v5

    #@3d
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@40
    .line 2608
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@42
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@45
    move-result-object v4

    #@46
    sget-object v5, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@48
    invoke-virtual {v4, v5, v1, v2, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@4b
    move-result v3

    #@4c
    .line 2609
    .local v3, update:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v5, "[IP]update ["

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v4

    #@5b
    const-string v5, "]"

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@68
    .line 2610
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@6b
    .line 2611
    const/4 v4, -0x1

    #@6c
    if-ne v3, v4, :cond_73

    #@6e
    .line 2612
    const-string v4, "[IP][ERROR]RCSeData_Update fail"

    #@70
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@73
    .line 2614
    :cond_73
    return v3
.end method

.method private RCSeData_Update(Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;Lcom/lge/ims/service/ip/CapaInfo;)I
    .registers 16
    .parameter "objRCSContactData"
    .parameter "objCapaInfo"

    #@0
    .prologue
    .line 2505
    const-string v8, "_id=?"

    #@2
    .line 2506
    .local v8, strSelection:Ljava/lang/String;
    const/4 v10, 0x1

    #@3
    new-array v0, v10, [Ljava/lang/String;

    #@5
    const/4 v10, 0x0

    #@6
    iget-wide v11, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nId:J

    #@8
    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@b
    move-result-object v11

    #@c
    aput-object v11, v0, v10

    #@e
    .line 2507
    .local v0, astrSelectionArg:[Ljava/lang/String;
    new-instance v6, Landroid/content/ContentValues;

    #@10
    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    #@13
    .line 2508
    .local v6, objContentValues:Landroid/content/ContentValues;
    const/4 v3, -0x1

    #@14
    .line 2509
    .local v3, nRcsUserType:I
    iget v10, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRCS:I

    #@16
    const/4 v11, 0x4

    #@17
    if-eq v10, v11, :cond_1d

    #@19
    iget v10, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRCS:I

    #@1b
    if-nez v10, :cond_118

    #@1d
    .line 2510
    :cond_1d
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetResponseCode()I

    #@20
    move-result v10

    #@21
    if-nez v10, :cond_101

    #@23
    .line 2511
    const-string v10, "rcs_status"

    #@25
    const/4 v11, 0x1

    #@26
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object v11

    #@2a
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2d
    .line 2512
    const-string v10, "presence_status"

    #@2f
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetPresenceCapa()I

    #@32
    move-result v11

    #@33
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@36
    move-result-object v11

    #@37
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3a
    .line 2513
    const/4 v3, 0x1

    #@3b
    .line 2519
    :goto_3b
    const-string v10, "time_stamp"

    #@3d
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@40
    move-result-wide v11

    #@41
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@44
    move-result-object v11

    #@45
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@48
    .line 2520
    const-string v10, "im_status"

    #@4a
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetIMCapa()I

    #@4d
    move-result v11

    #@4e
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@51
    move-result-object v11

    #@52
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@55
    .line 2521
    const-string v10, "ft_status"

    #@57
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetFTCapa()I

    #@5a
    move-result v11

    #@5b
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e
    move-result-object v11

    #@5f
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@62
    .line 2522
    const-string v10, "mim_status"

    #@64
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetMIMCapa()I

    #@67
    move-result v11

    #@68
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v11

    #@6c
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6f
    .line 2523
    const-string v10, "http_status"

    #@71
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetHTTPCapa()I

    #@74
    move-result v11

    #@75
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@78
    move-result-object v11

    #@79
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@7c
    .line 2524
    const-string v10, "response_code"

    #@7e
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetResponseCode()I

    #@81
    move-result v11

    #@82
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@85
    move-result-object v11

    #@86
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@89
    .line 2545
    :goto_89
    const-string v10, "first_time_rcs"

    #@8b
    const/4 v11, 0x0

    #@8c
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8f
    move-result-object v11

    #@90
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@93
    .line 2547
    iget-object v10, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@95
    invoke-virtual {v10}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@98
    move-result-object v10

    #@99
    sget-object v11, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@9b
    invoke-virtual {v10, v11, v6, v8, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@9e
    move-result v9

    #@9f
    .line 2548
    .local v9, update:I
    new-instance v10, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v11, "[IP]update ["

    #@a6
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v10

    #@aa
    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v10

    #@ae
    const-string v11, "]"

    #@b0
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v10

    #@b4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v10

    #@b8
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@bb
    .line 2549
    invoke-virtual {v6}, Landroid/content/ContentValues;->clear()V

    #@be
    .line 2550
    const/4 v10, -0x1

    #@bf
    if-eq v9, v10, :cond_295

    #@c1
    .line 2551
    if-nez v3, :cond_1b2

    #@c3
    .line 2552
    iget v10, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRcsUserList:I

    #@c5
    const/4 v11, 0x1

    #@c6
    if-ne v10, v11, :cond_ff

    #@c8
    .line 2553
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@cb
    move-result-object v10

    #@cc
    if-eqz v10, :cond_ff

    #@ce
    .line 2554
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@d1
    move-result-object v10

    #@d2
    invoke-virtual {v10}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@d5
    move-result-object v10

    #@d6
    if-eqz v10, :cond_ff

    #@d8
    .line 2555
    new-instance v7, Landroid/os/Message;

    #@da
    invoke-direct {v7}, Landroid/os/Message;-><init>()V

    #@dd
    .line 2556
    .local v7, objMessage:Landroid/os/Message;
    new-instance v5, Landroid/os/Bundle;

    #@df
    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    #@e2
    .line 2557
    .local v5, objBundle:Landroid/os/Bundle;
    const/16 v10, 0x8

    #@e4
    iput v10, v7, Landroid/os/Message;->what:I

    #@e6
    .line 2558
    const-string v10, "msisdn"

    #@e8
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetMSISDN()Ljava/lang/String;

    #@eb
    move-result-object v11

    #@ec
    invoke-virtual {v5, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@ef
    .line 2559
    invoke-virtual {v7, v5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@f2
    .line 2560
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@f5
    move-result-object v10

    #@f6
    invoke-virtual {v10}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@f9
    move-result-object v10

    #@fa
    const-wide/16 v11, 0x3e8

    #@fc
    invoke-virtual {v10, v7, v11, v12}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@ff
    .end local v5           #objBundle:Landroid/os/Bundle;
    .end local v7           #objMessage:Landroid/os/Message;
    :cond_ff
    :goto_ff
    move v4, v3

    #@100
    .line 2598
    .end local v3           #nRcsUserType:I
    .local v4, nRcsUserType:I
    :goto_100
    return v4

    #@101
    .line 2515
    .end local v4           #nRcsUserType:I
    .end local v9           #update:I
    .restart local v3       #nRcsUserType:I
    :cond_101
    const-string v10, "rcs_status"

    #@103
    const/4 v11, 0x0

    #@104
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@107
    move-result-object v11

    #@108
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@10b
    .line 2516
    const-string v10, "presence_status"

    #@10d
    const/4 v11, 0x0

    #@10e
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@111
    move-result-object v11

    #@112
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@115
    .line 2517
    const/4 v3, 0x0

    #@116
    goto/16 :goto_3b

    #@118
    .line 2526
    :cond_118
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetResponseCode()I

    #@11b
    move-result v10

    #@11c
    const/16 v11, 0x194

    #@11e
    if-ne v10, v11, :cond_185

    #@120
    .line 2527
    const-string v10, "rcs_status"

    #@122
    const/4 v11, 0x0

    #@123
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@126
    move-result-object v11

    #@127
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@12a
    .line 2528
    const-string v10, "presence_status"

    #@12c
    const/4 v11, 0x0

    #@12d
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@130
    move-result-object v11

    #@131
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@134
    .line 2529
    const-string v10, "time_stamp"

    #@136
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@139
    move-result-wide v11

    #@13a
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@13d
    move-result-object v11

    #@13e
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@141
    .line 2530
    const-string v10, "im_status"

    #@143
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetIMCapa()I

    #@146
    move-result v11

    #@147
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14a
    move-result-object v11

    #@14b
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@14e
    .line 2531
    const-string v10, "ft_status"

    #@150
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetFTCapa()I

    #@153
    move-result v11

    #@154
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@157
    move-result-object v11

    #@158
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@15b
    .line 2532
    const-string v10, "mim_status"

    #@15d
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetMIMCapa()I

    #@160
    move-result v11

    #@161
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@164
    move-result-object v11

    #@165
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@168
    .line 2533
    const-string v10, "http_status"

    #@16a
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetHTTPCapa()I

    #@16d
    move-result v11

    #@16e
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@171
    move-result-object v11

    #@172
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@175
    .line 2534
    const-string v10, "response_code"

    #@177
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetResponseCode()I

    #@17a
    move-result v11

    #@17b
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17e
    move-result-object v11

    #@17f
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@182
    .line 2535
    const/4 v3, 0x0

    #@183
    goto/16 :goto_89

    #@185
    .line 2537
    :cond_185
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetResponseCode()I

    #@188
    move-result v10

    #@189
    if-nez v10, :cond_198

    #@18b
    .line 2538
    const-string v10, "presence_status"

    #@18d
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetPresenceCapa()I

    #@190
    move-result v11

    #@191
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@194
    move-result-object v11

    #@195
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@198
    .line 2540
    :cond_198
    const-string v10, "rcs_status"

    #@19a
    const/4 v11, 0x1

    #@19b
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@19e
    move-result-object v11

    #@19f
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1a2
    .line 2541
    const-string v10, "time_stamp"

    #@1a4
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1a7
    move-result-wide v11

    #@1a8
    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1ab
    move-result-object v11

    #@1ac
    invoke-virtual {v6, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@1af
    .line 2542
    const/4 v3, 0x1

    #@1b0
    goto/16 :goto_89

    #@1b2
    .line 2565
    .restart local v9       #update:I
    :cond_1b2
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceUserCount()I

    #@1b5
    move-result v2

    #@1b6
    .line 2566
    .local v2, nPresenceCount:I
    new-instance v10, Ljava/lang/StringBuilder;

    #@1b8
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1bb
    const-string v11, "[IP]Presence User Count [ "

    #@1bd
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c0
    move-result-object v10

    #@1c1
    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v10

    #@1c5
    const-string v11, " ], nLimitationResourceListUserCount [ "

    #@1c7
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ca
    move-result-object v10

    #@1cb
    sget v11, Lcom/lge/ims/service/ip/IPContactHelper;->nLimitationResourceListUserCount:I

    #@1cd
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d0
    move-result-object v10

    #@1d1
    const-string v11, " ]"

    #@1d3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d6
    move-result-object v10

    #@1d7
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1da
    move-result-object v10

    #@1db
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1de
    .line 2567
    sget v10, Lcom/lge/ims/service/ip/IPContactHelper;->nLimitationResourceListUserCount:I

    #@1e0
    if-lt v2, v10, :cond_206

    #@1e2
    .line 2568
    const-string v10, "[IP]Presence user count is over the limitation resource-list user. do not add RCS user"

    #@1e4
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1e7
    .line 2569
    sget-boolean v10, Lcom/lge/ims/service/ip/IPContactHelper;->isExcessMaxUserSize:Z

    #@1e9
    if-nez v10, :cond_203

    #@1eb
    .line 2570
    const/4 v10, 0x1

    #@1ec
    sput-boolean v10, Lcom/lge/ims/service/ip/IPContactHelper;->isExcessMaxUserSize:Z

    #@1ee
    .line 2572
    new-instance v1, Landroid/content/Intent;

    #@1f0
    const-string v10, "com.lge.ims.rcs.excess_max_user"

    #@1f2
    invoke-direct {v1, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1f5
    .line 2573
    .local v1, intent:Landroid/content/Intent;
    iget-object v10, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@1f7
    invoke-virtual {v10, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1fa
    .line 2574
    const/4 v10, 0x1

    #@1fb
    invoke-direct {p0, v10}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateExcessMaxUserValue(I)Z

    #@1fe
    .line 2575
    const-string v10, "[IP]Send IP_ACTION_EXCESS_MAX_USER intent!!"

    #@200
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@203
    .end local v1           #intent:Landroid/content/Intent;
    :cond_203
    move v4, v3

    #@204
    .line 2577
    .end local v3           #nRcsUserType:I
    .restart local v4       #nRcsUserType:I
    goto/16 :goto_100

    #@206
    .line 2579
    .end local v4           #nRcsUserType:I
    .restart local v3       #nRcsUserType:I
    :cond_206
    new-instance v10, Ljava/lang/StringBuilder;

    #@208
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@20b
    const-string v11, "[IP]nRcsUserType["

    #@20d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@210
    move-result-object v10

    #@211
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@214
    move-result-object v10

    #@215
    const-string v11, "], Presence status["

    #@217
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21a
    move-result-object v10

    #@21b
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetPresenceCapa()I

    #@21e
    move-result v11

    #@21f
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@222
    move-result-object v10

    #@223
    const-string v11, "], RCS user list["

    #@225
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@228
    move-result-object v10

    #@229
    iget v11, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRcsUserList:I

    #@22b
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v10

    #@22f
    const-string v11, "], Non RCS user list["

    #@231
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@234
    move-result-object v10

    #@235
    iget v11, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsNonRcsUserList:I

    #@237
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v10

    #@23b
    const-string v11, "]"

    #@23d
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@240
    move-result-object v10

    #@241
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@244
    move-result-object v10

    #@245
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@248
    .line 2580
    const/4 v10, 0x1

    #@249
    if-ne v3, v10, :cond_ff

    #@24b
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetPresenceCapa()I

    #@24e
    move-result v10

    #@24f
    const/4 v11, 0x1

    #@250
    if-ne v10, v11, :cond_ff

    #@252
    iget v10, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsRcsUserList:I

    #@254
    if-nez v10, :cond_ff

    #@256
    .line 2581
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@259
    move-result-object v10

    #@25a
    if-eqz v10, :cond_ff

    #@25c
    .line 2582
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@25f
    move-result-object v10

    #@260
    invoke-virtual {v10}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@263
    move-result-object v10

    #@264
    if-eqz v10, :cond_ff

    #@266
    .line 2583
    new-instance v7, Landroid/os/Message;

    #@268
    invoke-direct {v7}, Landroid/os/Message;-><init>()V

    #@26b
    .line 2584
    .restart local v7       #objMessage:Landroid/os/Message;
    new-instance v5, Landroid/os/Bundle;

    #@26d
    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    #@270
    .line 2585
    .restart local v5       #objBundle:Landroid/os/Bundle;
    const/4 v10, 0x7

    #@271
    iput v10, v7, Landroid/os/Message;->what:I

    #@273
    .line 2586
    const-string v10, "msisdn"

    #@275
    invoke-virtual {p2}, Lcom/lge/ims/service/ip/CapaInfo;->GetMSISDN()Ljava/lang/String;

    #@278
    move-result-object v11

    #@279
    invoke-virtual {v5, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@27c
    .line 2587
    const-string v10, "displayname"

    #@27e
    iget-object v11, p1, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strDisplayName:Ljava/lang/String;

    #@280
    invoke-virtual {v5, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@283
    .line 2588
    invoke-virtual {v7, v5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@286
    .line 2589
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@289
    move-result-object v10

    #@28a
    invoke-virtual {v10}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@28d
    move-result-object v10

    #@28e
    const-wide/16 v11, 0x3e8

    #@290
    invoke-virtual {v10, v7, v11, v12}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@293
    goto/16 :goto_ff

    #@295
    .line 2595
    .end local v2           #nPresenceCount:I
    .end local v5           #objBundle:Landroid/os/Bundle;
    .end local v7           #objMessage:Landroid/os/Message;
    :cond_295
    const-string v10, "[IP][ERROR]RCSeData_Update fail"

    #@297
    invoke-static {v10}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@29a
    .line 2596
    const/4 v3, -0x1

    #@29b
    goto/16 :goto_ff
.end method

.method private SyncFullImageToContact(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;ZZ)Z
    .registers 23
    .parameter "objPresenceInfo"
    .parameter "bIgnorePriority"
    .parameter "bOnlyInsert"

    #@0
    .prologue
    .line 3141
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v4, "[IP]msisdn : [ "

    #@7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    move-object/from16 v0, p1

    #@d
    iget-object v4, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, " ], bIgnorePriority [ "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    move/from16 v0, p2

    #@1b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, " ], bOnlyInsert [ "

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    move/from16 v0, p3

    #@27
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    const-string v4, " ]"

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@38
    .line 3142
    const-string v6, "nor_msisdn=?"

    #@3a
    .line 3143
    .local v6, strSelection:Ljava/lang/String;
    const/4 v3, 0x1

    #@3b
    new-array v7, v3, [Ljava/lang/String;

    #@3d
    const/4 v3, 0x0

    #@3e
    move-object/from16 v0, p1

    #@40
    iget-object v4, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@42
    aput-object v4, v7, v3

    #@44
    .line 3144
    .local v7, astrSelectionArgs:[Ljava/lang/String;
    const/4 v14, 0x0

    #@45
    .line 3145
    .local v14, objCursor:Landroid/database/Cursor;
    const/4 v9, 0x1

    #@46
    .line 3147
    .local v9, bReturn:Z
    :try_start_46
    move-object/from16 v0, p0

    #@48
    iget-object v3, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4a
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4d
    move-result-object v3

    #@4e
    sget-object v4, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@50
    sget-object v5, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@52
    const/4 v8, 0x0

    #@53
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@56
    move-result-object v14

    #@57
    .line 3152
    if-nez v14, :cond_81

    #@59
    .line 3153
    new-instance v3, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v4, "[IP][ERROR]SyncFullImageToContact : Not found [ "

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    move-object/from16 v0, p1

    #@66
    iget-object v4, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@68
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    const-string v4, " ]"

    #@6e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v3

    #@76
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_79
    .catchall {:try_start_46 .. :try_end_79} :catchall_142
    .catch Ljava/lang/Exception; {:try_start_46 .. :try_end_79} :catch_b8

    #@79
    .line 3154
    const/4 v3, 0x0

    #@7a
    .line 3210
    if-eqz v14, :cond_80

    #@7c
    .line 3211
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@7f
    .line 3212
    const/4 v14, 0x0

    #@80
    .line 3215
    :cond_80
    :goto_80
    return v3

    #@81
    .line 3156
    :cond_81
    :try_start_81
    new-instance v16, Ljava/util/LinkedList;

    #@83
    invoke-direct/range {v16 .. v16}, Ljava/util/LinkedList;-><init>()V

    #@86
    .line 3157
    .local v16, objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :cond_86
    :goto_86
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    #@89
    move-result v3

    #@8a
    if-eqz v3, :cond_d7

    #@8c
    .line 3158
    move-object/from16 v0, p0

    #@8e
    invoke-direct {v0, v14}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@91
    move-result-object v15

    #@92
    .line 3159
    .local v15, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v15, :cond_86

    #@94
    .line 3160
    new-instance v3, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v4, "[IP]add : ["

    #@9b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v3

    #@9f
    iget-wide v4, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@a1
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    const-string v4, "]"

    #@a7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v3

    #@ab
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ae
    move-result-object v3

    #@af
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@b2
    .line 3161
    move-object/from16 v0, v16

    #@b4
    invoke-virtual {v0, v15}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_b7
    .catchall {:try_start_81 .. :try_end_b7} :catchall_142
    .catch Ljava/lang/Exception; {:try_start_81 .. :try_end_b7} :catch_b8

    #@b7
    goto :goto_86

    #@b8
    .line 3207
    .end local v15           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v16           #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :catch_b8
    move-exception v10

    #@b9
    .line 3208
    .local v10, e:Ljava/lang/Exception;
    :try_start_b9
    new-instance v3, Ljava/lang/StringBuilder;

    #@bb
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@be
    const-string v4, "[IP][ERROR]exception : "

    #@c0
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v3

    #@c4
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v3

    #@c8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v3

    #@cc
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_cf
    .catchall {:try_start_b9 .. :try_end_cf} :catchall_142

    #@cf
    .line 3210
    if-eqz v14, :cond_d5

    #@d1
    .line 3211
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@d4
    .line 3212
    const/4 v14, 0x0

    #@d5
    .end local v10           #e:Ljava/lang/Exception;
    :cond_d5
    :goto_d5
    move v3, v9

    #@d6
    .line 3215
    goto :goto_80

    #@d7
    .line 3164
    .restart local v16       #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :cond_d7
    const/4 v11, 0x0

    #@d8
    .local v11, i:I
    :goto_d8
    :try_start_d8
    invoke-virtual/range {v16 .. v16}, Ljava/util/LinkedList;->size()I

    #@db
    move-result v3

    #@dc
    if-ge v11, v3, :cond_1bc

    #@de
    .line 3165
    move-object/from16 v0, v16

    #@e0
    invoke-virtual {v0, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@e3
    move-result-object v15

    #@e4
    check-cast v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@e6
    .line 3166
    .restart local v15       #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    iget v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nPhotoUpdate:I

    #@e8
    if-nez v3, :cond_123

    #@ea
    .line 3167
    new-instance v3, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v4, "[IP]photo don`t update : msisdn ["

    #@f1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v3

    #@f5
    iget-object v4, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@f7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v3

    #@fb
    const-string v4, " ], raw_contact_id [ "

    #@fd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v3

    #@101
    iget-wide v4, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@103
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@106
    move-result-object v3

    #@107
    const-string v4, " ], data_id [ "

    #@109
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v3

    #@10d
    iget-wide v4, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nDataId:J

    #@10f
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@112
    move-result-object v3

    #@113
    const-string v4, " ]"

    #@115
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v3

    #@119
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v3

    #@11d
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@120
    .line 3164
    :cond_120
    :goto_120
    add-int/lit8 v11, v11, 0x1

    #@122
    goto :goto_d8

    #@123
    .line 3169
    :cond_123
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@125
    move-object/from16 v0, p0

    #@127
    invoke-direct {v0, v3, v4}, Lcom/lge/ims/service/ip/IPContactHelper;->HasContactImageData(J)I

    #@12a
    move-result v3

    #@12b
    packed-switch v3, :pswitch_data_1c4

    #@12e
    .line 3201
    const/4 v9, 0x0

    #@12f
    goto :goto_120

    #@130
    .line 3171
    :pswitch_130
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@132
    move-object/from16 v0, p0

    #@134
    move-object/from16 v1, p1

    #@136
    invoke-direct {v0, v3, v4, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactImageData_Insert(JLcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;)J

    #@139
    move-result-wide v12

    #@13a
    .line 3172
    .local v12, nImageUriId:J
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nId:J

    #@13c
    move-object/from16 v0, p0

    #@13e
    invoke-direct {v0, v3, v4, v12, v13}, Lcom/lge/ims/service/ip/IPContactHelper;->RCSeData_Update(JJ)I
    :try_end_141
    .catchall {:try_start_d8 .. :try_end_141} :catchall_142
    .catch Ljava/lang/Exception; {:try_start_d8 .. :try_end_141} :catch_b8

    #@141
    goto :goto_120

    #@142
    .line 3210
    .end local v11           #i:I
    .end local v12           #nImageUriId:J
    .end local v15           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v16           #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :catchall_142
    move-exception v3

    #@143
    if-eqz v14, :cond_149

    #@145
    .line 3211
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@148
    .line 3212
    const/4 v14, 0x0

    #@149
    :cond_149
    throw v3

    #@14a
    .line 3175
    .restart local v11       #i:I
    .restart local v15       #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .restart local v16       #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :pswitch_14a
    if-nez p3, :cond_120

    #@14c
    .line 3176
    :try_start_14c
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@14e
    const-wide/16 v17, -0x1

    #@150
    cmp-long v3, v3, v17

    #@152
    if-nez v3, :cond_175

    #@154
    .line 3177
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@156
    move-object/from16 v0, p0

    #@158
    invoke-direct {v0, v3, v4}, Lcom/lge/ims/service/ip/IPContactHelper;->GetContactImageId(J)J

    #@15b
    move-result-wide v3

    #@15c
    iput-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@15e
    .line 3178
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@160
    move-object/from16 v0, p0

    #@162
    move-object/from16 v1, p1

    #@164
    invoke-direct {v0, v3, v4, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactImageData_Update(JLcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;)Z

    #@167
    .line 3179
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nId:J

    #@169
    iget-wide v0, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@16b
    move-wide/from16 v17, v0

    #@16d
    move-object/from16 v0, p0

    #@16f
    move-wide/from16 v1, v17

    #@171
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->RCSeData_Update(JJ)I

    #@174
    goto :goto_120

    #@175
    .line 3181
    :cond_175
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@177
    move-object/from16 v0, p0

    #@179
    move-object/from16 v1, p1

    #@17b
    invoke-direct {v0, v3, v4, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactImageData_Update(JLcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;)Z

    #@17e
    goto :goto_120

    #@17f
    .line 3186
    :pswitch_17f
    const/4 v3, 0x1

    #@180
    move/from16 v0, p2

    #@182
    if-ne v0, v3, :cond_1b9

    #@184
    .line 3187
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@186
    const-wide/16 v17, -0x1

    #@188
    cmp-long v3, v3, v17

    #@18a
    if-nez v3, :cond_1ae

    #@18c
    .line 3188
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@18e
    move-object/from16 v0, p0

    #@190
    invoke-direct {v0, v3, v4}, Lcom/lge/ims/service/ip/IPContactHelper;->GetContactImageId(J)J

    #@193
    move-result-wide v3

    #@194
    iput-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@196
    .line 3189
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@198
    move-object/from16 v0, p0

    #@19a
    move-object/from16 v1, p1

    #@19c
    invoke-direct {v0, v3, v4, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactImageData_Update(JLcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;)Z

    #@19f
    .line 3190
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nId:J

    #@1a1
    iget-wide v0, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@1a3
    move-wide/from16 v17, v0

    #@1a5
    move-object/from16 v0, p0

    #@1a7
    move-wide/from16 v1, v17

    #@1a9
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->RCSeData_Update(JJ)I

    #@1ac
    goto/16 :goto_120

    #@1ae
    .line 3192
    :cond_1ae
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@1b0
    move-object/from16 v0, p0

    #@1b2
    move-object/from16 v1, p1

    #@1b4
    invoke-direct {v0, v3, v4, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactImageData_Update(JLcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;)Z
    :try_end_1b7
    .catchall {:try_start_14c .. :try_end_1b7} :catchall_142
    .catch Ljava/lang/Exception; {:try_start_14c .. :try_end_1b7} :catch_b8

    #@1b7
    goto/16 :goto_120

    #@1b9
    .line 3197
    :cond_1b9
    const/4 v9, 0x0

    #@1ba
    .line 3199
    goto/16 :goto_120

    #@1bc
    .line 3210
    .end local v15           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :cond_1bc
    if-eqz v14, :cond_d5

    #@1be
    .line 3211
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@1c1
    .line 3212
    const/4 v14, 0x0

    #@1c2
    goto/16 :goto_d5

    #@1c4
    .line 3169
    :pswitch_data_1c4
    .packed-switch 0x0
        :pswitch_130
        :pswitch_14a
        :pswitch_17f
    .end packed-switch
.end method

.method private SyncFullImageToContact(Lcom/lge/ims/service/ip/PresenceInfo;)Z
    .registers 21
    .parameter "objPresenceInfo"

    #@0
    .prologue
    .line 3078
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v4, "[IP]msisdn : ["

    #@7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, "]"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@20
    .line 3079
    const-string v6, "nor_msisdn=?"

    #@22
    .line 3080
    .local v6, strSelection:Ljava/lang/String;
    const/4 v3, 0x1

    #@23
    new-array v7, v3, [Ljava/lang/String;

    #@25
    const/4 v3, 0x0

    #@26
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@29
    move-result-object v4

    #@2a
    aput-object v4, v7, v3

    #@2c
    .line 3081
    .local v7, astrSelectionArgs:[Ljava/lang/String;
    const/4 v14, 0x0

    #@2d
    .line 3082
    .local v14, objCursor:Landroid/database/Cursor;
    const/4 v9, 0x1

    #@2e
    .line 3084
    .local v9, bReturn:Z
    :try_start_2e
    move-object/from16 v0, p0

    #@30
    iget-object v3, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@32
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@35
    move-result-object v3

    #@36
    sget-object v4, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@38
    sget-object v5, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@3a
    const/4 v8, 0x0

    #@3b
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3e
    move-result-object v14

    #@3f
    .line 3089
    if-nez v14, :cond_69

    #@41
    .line 3090
    new-instance v3, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v4, "[IP][ERROR]SyncFullImageToContact : Not found ["

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@4f
    move-result-object v4

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    const-string v4, "]"

    #@56
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_61
    .catchall {:try_start_2e .. :try_end_61} :catchall_12a
    .catch Ljava/lang/Exception; {:try_start_2e .. :try_end_61} :catch_a0

    #@61
    .line 3091
    const/4 v3, 0x0

    #@62
    .line 3133
    if-eqz v14, :cond_68

    #@64
    .line 3134
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@67
    .line 3135
    const/4 v14, 0x0

    #@68
    .line 3138
    :cond_68
    :goto_68
    return v3

    #@69
    .line 3093
    :cond_69
    :try_start_69
    new-instance v16, Ljava/util/LinkedList;

    #@6b
    invoke-direct/range {v16 .. v16}, Ljava/util/LinkedList;-><init>()V

    #@6e
    .line 3094
    .local v16, objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :cond_6e
    :goto_6e
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    #@71
    move-result v3

    #@72
    if-eqz v3, :cond_bf

    #@74
    .line 3095
    move-object/from16 v0, p0

    #@76
    invoke-direct {v0, v14}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@79
    move-result-object v15

    #@7a
    .line 3096
    .local v15, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v15, :cond_6e

    #@7c
    .line 3097
    new-instance v3, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v4, "[IP]add : ["

    #@83
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    iget-wide v4, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@89
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v3

    #@8d
    const-string v4, "]"

    #@8f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v3

    #@93
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v3

    #@97
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@9a
    .line 3098
    move-object/from16 v0, v16

    #@9c
    invoke-virtual {v0, v15}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_9f
    .catchall {:try_start_69 .. :try_end_9f} :catchall_12a
    .catch Ljava/lang/Exception; {:try_start_69 .. :try_end_9f} :catch_a0

    #@9f
    goto :goto_6e

    #@a0
    .line 3130
    .end local v15           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v16           #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :catch_a0
    move-exception v10

    #@a1
    .line 3131
    .local v10, e:Ljava/lang/Exception;
    :try_start_a1
    new-instance v3, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v4, "[IP][ERROR]exception : "

    #@a8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v3

    #@ac
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v3

    #@b0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v3

    #@b4
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_b7
    .catchall {:try_start_a1 .. :try_end_b7} :catchall_12a

    #@b7
    .line 3133
    if-eqz v14, :cond_bd

    #@b9
    .line 3134
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@bc
    .line 3135
    const/4 v14, 0x0

    #@bd
    .end local v10           #e:Ljava/lang/Exception;
    :cond_bd
    :goto_bd
    move v3, v9

    #@be
    .line 3138
    goto :goto_68

    #@bf
    .line 3101
    .restart local v16       #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :cond_bf
    const/4 v11, 0x0

    #@c0
    .local v11, i:I
    :goto_c0
    :try_start_c0
    invoke-virtual/range {v16 .. v16}, Ljava/util/LinkedList;->size()I

    #@c3
    move-result v3

    #@c4
    if-ge v11, v3, :cond_167

    #@c6
    .line 3102
    move-object/from16 v0, v16

    #@c8
    invoke-virtual {v0, v11}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@cb
    move-result-object v15

    #@cc
    check-cast v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@ce
    .line 3103
    .restart local v15       #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    iget v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nPhotoUpdate:I

    #@d0
    if-nez v3, :cond_10b

    #@d2
    .line 3104
    new-instance v3, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v4, "[IP]photo don`t update : msisdn ["

    #@d9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v3

    #@dd
    iget-object v4, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@df
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v3

    #@e3
    const-string v4, " ], raw_contact_id [ "

    #@e5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v3

    #@e9
    iget-wide v4, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@eb
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v3

    #@ef
    const-string v4, " ], data_id [ "

    #@f1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v3

    #@f5
    iget-wide v4, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nDataId:J

    #@f7
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v3

    #@fb
    const-string v4, " ]"

    #@fd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v3

    #@101
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@104
    move-result-object v3

    #@105
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@108
    .line 3101
    :goto_108
    add-int/lit8 v11, v11, 0x1

    #@10a
    goto :goto_c0

    #@10b
    .line 3106
    :cond_10b
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@10d
    move-object/from16 v0, p0

    #@10f
    invoke-direct {v0, v3, v4}, Lcom/lge/ims/service/ip/IPContactHelper;->HasContactImageData(J)I

    #@112
    move-result v3

    #@113
    packed-switch v3, :pswitch_data_170

    #@116
    .line 3124
    const/4 v9, 0x0

    #@117
    goto :goto_108

    #@118
    .line 3108
    :pswitch_118
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@11a
    move-object/from16 v0, p0

    #@11c
    move-object/from16 v1, p1

    #@11e
    invoke-direct {v0, v3, v4, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactImageData_Insert(JLcom/lge/ims/service/ip/PresenceInfo;)J

    #@121
    move-result-wide v12

    #@122
    .line 3109
    .local v12, nImageUriId:J
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nId:J

    #@124
    move-object/from16 v0, p0

    #@126
    invoke-direct {v0, v3, v4, v12, v13}, Lcom/lge/ims/service/ip/IPContactHelper;->RCSeData_Update(JJ)I
    :try_end_129
    .catchall {:try_start_c0 .. :try_end_129} :catchall_12a
    .catch Ljava/lang/Exception; {:try_start_c0 .. :try_end_129} :catch_a0

    #@129
    goto :goto_108

    #@12a
    .line 3133
    .end local v11           #i:I
    .end local v12           #nImageUriId:J
    .end local v15           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v16           #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :catchall_12a
    move-exception v3

    #@12b
    if-eqz v14, :cond_131

    #@12d
    .line 3134
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@130
    .line 3135
    const/4 v14, 0x0

    #@131
    :cond_131
    throw v3

    #@132
    .line 3112
    .restart local v11       #i:I
    .restart local v15       #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .restart local v16       #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :pswitch_132
    :try_start_132
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@134
    const-wide/16 v17, -0x1

    #@136
    cmp-long v3, v3, v17

    #@138
    if-nez v3, :cond_15b

    #@13a
    .line 3113
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nRawContactId:J

    #@13c
    move-object/from16 v0, p0

    #@13e
    invoke-direct {v0, v3, v4}, Lcom/lge/ims/service/ip/IPContactHelper;->GetContactImageId(J)J

    #@141
    move-result-wide v3

    #@142
    iput-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@144
    .line 3114
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@146
    move-object/from16 v0, p0

    #@148
    move-object/from16 v1, p1

    #@14a
    invoke-direct {v0, v3, v4, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactImageData_Update(JLcom/lge/ims/service/ip/PresenceInfo;)Z

    #@14d
    .line 3115
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nId:J

    #@14f
    iget-wide v0, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@151
    move-wide/from16 v17, v0

    #@153
    move-object/from16 v0, p0

    #@155
    move-wide/from16 v1, v17

    #@157
    invoke-direct {v0, v3, v4, v1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->RCSeData_Update(JJ)I

    #@15a
    goto :goto_108

    #@15b
    .line 3117
    :cond_15b
    iget-wide v3, v15, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nImageUriId:J

    #@15d
    move-object/from16 v0, p0

    #@15f
    move-object/from16 v1, p1

    #@161
    invoke-direct {v0, v3, v4, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactImageData_Update(JLcom/lge/ims/service/ip/PresenceInfo;)Z
    :try_end_164
    .catchall {:try_start_132 .. :try_end_164} :catchall_12a
    .catch Ljava/lang/Exception; {:try_start_132 .. :try_end_164} :catch_a0

    #@164
    goto :goto_108

    #@165
    .line 3121
    :pswitch_165
    const/4 v9, 0x0

    #@166
    .line 3122
    goto :goto_108

    #@167
    .line 3133
    .end local v15           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :cond_167
    if-eqz v14, :cond_bd

    #@169
    .line 3134
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@16c
    .line 3135
    const/4 v14, 0x0

    #@16d
    goto/16 :goto_bd

    #@16f
    .line 3106
    nop

    #@170
    :pswitch_data_170
    .packed-switch 0x0
        :pswitch_118
        :pswitch_132
        :pswitch_165
    .end packed-switch
.end method

.method private UpdateExcessMaxUserValue(I)Z
    .registers 7
    .parameter "nExcessMaxUser"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2142
    new-instance v1, Landroid/content/ContentValues;

    #@3
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@6
    .line 2144
    .local v1, objContentValues:Landroid/content/ContentValues;
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "[IP]nExcessMaxUser ["

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "]"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@22
    .line 2145
    const-string v2, "excess_max_user"

    #@24
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2b
    .line 2147
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@2d
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@30
    move-result-object v2

    #@31
    sget-object v3, Lcom/lge/ims/service/ip/IPConstant;->IP_PROVISIONING_CONTENT_URI:Landroid/net/Uri;

    #@33
    invoke-virtual {v2, v3, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@36
    move-result v0

    #@37
    .line 2148
    .local v0, nCount:I
    if-nez v0, :cond_40

    #@39
    .line 2149
    const-string v2, "[IP][ERROR]UpdateExcessMaxUserValue - nCount is 0"

    #@3b
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@3e
    .line 2150
    const/4 v2, 0x0

    #@3f
    .line 2153
    :goto_3f
    return v2

    #@40
    .line 2152
    :cond_40
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@43
    .line 2153
    const/4 v2, 0x1

    #@44
    goto :goto_3f
.end method


# virtual methods
.method public AddNonRCSUserList(Ljava/lang/String;)Z
    .registers 15
    .parameter "szMsisdn"

    #@0
    .prologue
    .line 1618
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP]msisdn : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 1619
    const-string v3, "nor_msisdn=?"

    #@18
    .line 1620
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@19
    new-array v4, v0, [Ljava/lang/String;

    #@1b
    const/4 v0, 0x0

    #@1c
    aput-object p1, v4, v0

    #@1e
    .line 1621
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    const/4 v9, 0x0

    #@1f
    .line 1623
    .local v9, objCursor:Landroid/database/Cursor;
    :try_start_1f
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@21
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v0

    #@25
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@27
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@29
    const/4 v5, 0x0

    #@2a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2d
    move-result-object v9

    #@2e
    .line 1628
    if-nez v9, :cond_54

    #@30
    .line 1629
    new-instance v0, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v1, "[IP][ERROR]AddNonRCSUserList : Not found ["

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, "]"

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_4c
    .catchall {:try_start_1f .. :try_end_4c} :catchall_e3
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_4c} :catch_c3

    #@4c
    .line 1630
    const/4 v0, 0x0

    #@4d
    .line 1655
    if-eqz v9, :cond_53

    #@4f
    .line 1656
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@52
    .line 1657
    const/4 v9, 0x0

    #@53
    .line 1660
    :cond_53
    :goto_53
    return v0

    #@54
    .line 1632
    :cond_54
    :try_start_54
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@57
    move-result v0

    #@58
    if-eqz v0, :cond_bb

    #@5a
    .line 1633
    invoke-direct {p0, v9}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@5d
    move-result-object v10

    #@5e
    .line 1634
    .local v10, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v10, :cond_54

    #@60
    .line 1635
    const-string v11, "_id=?"

    #@62
    .line 1636
    .local v11, strDeleteSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@63
    new-array v6, v0, [Ljava/lang/String;

    #@65
    const/4 v0, 0x0

    #@66
    iget-wide v1, v10, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nId:J

    #@68
    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@6b
    move-result-object v1

    #@6c
    aput-object v1, v6, v0

    #@6e
    .line 1637
    .local v6, astrDeleteSelectionArg:[Ljava/lang/String;
    new-instance v8, Landroid/content/ContentValues;

    #@70
    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    #@73
    .line 1638
    .local v8, objContentValues:Landroid/content/ContentValues;
    const-string v0, "nonrcs_user_list"

    #@75
    const/4 v1, 0x1

    #@76
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@79
    move-result-object v1

    #@7a
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@7d
    .line 1640
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@7f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@82
    move-result-object v0

    #@83
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@85
    invoke-virtual {v0, v1, v8, v11, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@88
    move-result v12

    #@89
    .line 1641
    .local v12, update:I
    new-instance v0, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v1, "[IP]update ["

    #@90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v0

    #@94
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@97
    move-result-object v0

    #@98
    const-string v1, "]"

    #@9a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v0

    #@9e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v0

    #@a2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a5
    .line 1642
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V
    :try_end_a8
    .catchall {:try_start_54 .. :try_end_a8} :catchall_e3
    .catch Ljava/lang/Exception; {:try_start_54 .. :try_end_a8} :catch_c3

    #@a8
    .line 1643
    const/4 v0, -0x1

    #@a9
    if-eq v12, v0, :cond_b3

    #@ab
    .line 1644
    const/4 v0, 0x1

    #@ac
    .line 1655
    if-eqz v9, :cond_53

    #@ae
    .line 1656
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@b1
    .line 1657
    const/4 v9, 0x0

    #@b2
    goto :goto_53

    #@b3
    .line 1646
    :cond_b3
    const/4 v0, 0x0

    #@b4
    .line 1655
    if-eqz v9, :cond_53

    #@b6
    .line 1656
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@b9
    .line 1657
    const/4 v9, 0x0

    #@ba
    goto :goto_53

    #@bb
    .line 1650
    .end local v6           #astrDeleteSelectionArg:[Ljava/lang/String;
    .end local v8           #objContentValues:Landroid/content/ContentValues;
    .end local v10           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v11           #strDeleteSelection:Ljava/lang/String;
    .end local v12           #update:I
    :cond_bb
    const/4 v0, 0x1

    #@bc
    .line 1655
    if-eqz v9, :cond_53

    #@be
    .line 1656
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@c1
    .line 1657
    const/4 v9, 0x0

    #@c2
    goto :goto_53

    #@c3
    .line 1652
    :catch_c3
    move-exception v7

    #@c4
    .line 1653
    .local v7, e:Ljava/lang/Exception;
    :try_start_c4
    new-instance v0, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v1, "[IP][ERROR]exception : "

    #@cb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v0

    #@cf
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v0

    #@d3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v0

    #@d7
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_da
    .catchall {:try_start_c4 .. :try_end_da} :catchall_e3

    #@da
    .line 1655
    if-eqz v9, :cond_e0

    #@dc
    .line 1656
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@df
    .line 1657
    const/4 v9, 0x0

    #@e0
    .line 1660
    :cond_e0
    const/4 v0, 0x1

    #@e1
    goto/16 :goto_53

    #@e3
    .line 1655
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_e3
    move-exception v0

    #@e4
    if-eqz v9, :cond_ea

    #@e6
    .line 1656
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@e9
    .line 1657
    const/4 v9, 0x0

    #@ea
    :cond_ea
    throw v0
.end method

.method public AddRCSUserList(Ljava/lang/String;)Z
    .registers 18
    .parameter "szMsisdn"

    #@0
    .prologue
    .line 1563
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "[IP]msisdn : "

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    move-object/from16 v0, p1

    #@d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@18
    .line 1564
    const-string v4, "nor_msisdn=?"

    #@1a
    .line 1565
    .local v4, strSelection:Ljava/lang/String;
    const/4 v1, 0x1

    #@1b
    new-array v5, v1, [Ljava/lang/String;

    #@1d
    const/4 v1, 0x0

    #@1e
    aput-object p1, v5, v1

    #@20
    .line 1566
    .local v5, astrSelectionArgs:[Ljava/lang/String;
    const/4 v11, 0x0

    #@21
    .line 1568
    .local v11, objCursor:Landroid/database/Cursor;
    :try_start_21
    move-object/from16 v0, p0

    #@23
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@25
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@28
    move-result-object v1

    #@29
    sget-object v2, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@2b
    sget-object v3, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@2d
    const/4 v6, 0x0

    #@2e
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@31
    move-result-object v11

    #@32
    .line 1573
    if-nez v11, :cond_5a

    #@34
    .line 1574
    new-instance v1, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v2, "[IP][ERROR]AddRCSUserList : Not found ["

    #@3b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    move-object/from16 v0, p1

    #@41
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    const-string v2, "]"

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_52
    .catchall {:try_start_21 .. :try_end_52} :catchall_125
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_52} :catch_105

    #@52
    .line 1575
    const/4 v1, 0x0

    #@53
    .line 1610
    if-eqz v11, :cond_59

    #@55
    .line 1611
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@58
    .line 1612
    const/4 v11, 0x0

    #@59
    .line 1615
    :cond_59
    :goto_59
    return v1

    #@5a
    .line 1577
    :cond_5a
    :try_start_5a
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    #@5d
    move-result v1

    #@5e
    if-eqz v1, :cond_fc

    #@60
    .line 1578
    move-object/from16 v0, p0

    #@62
    invoke-direct {v0, v11}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@65
    move-result-object v13

    #@66
    .line 1579
    .local v13, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v13, :cond_5a

    #@68
    .line 1580
    const-string v14, "_id=?"

    #@6a
    .line 1581
    .local v14, strDeleteSelection:Ljava/lang/String;
    const/4 v1, 0x1

    #@6b
    new-array v7, v1, [Ljava/lang/String;

    #@6d
    const/4 v1, 0x0

    #@6e
    iget-wide v2, v13, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nId:J

    #@70
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@73
    move-result-object v2

    #@74
    aput-object v2, v7, v1

    #@76
    .line 1582
    .local v7, astrDeleteSelectionArg:[Ljava/lang/String;
    new-instance v10, Landroid/content/ContentValues;

    #@78
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    #@7b
    .line 1583
    .local v10, objContentValues:Landroid/content/ContentValues;
    const-string v1, "rcs_user_list"

    #@7d
    const/4 v2, 0x1

    #@7e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@81
    move-result-object v2

    #@82
    invoke-virtual {v10, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@85
    .line 1585
    move-object/from16 v0, p0

    #@87
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@89
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8c
    move-result-object v1

    #@8d
    sget-object v2, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@8f
    invoke-virtual {v1, v2, v10, v14, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@92
    move-result v15

    #@93
    .line 1586
    .local v15, update:I
    new-instance v1, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v2, "[IP]update ["

    #@9a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v1

    #@9e
    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v1

    #@a2
    const-string v2, "]"

    #@a4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v1

    #@a8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ab
    move-result-object v1

    #@ac
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@af
    .line 1587
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    #@b2
    .line 1588
    const/4 v1, -0x1

    #@b3
    if-eq v15, v1, :cond_f3

    #@b5
    .line 1589
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@b8
    move-result-object v1

    #@b9
    if-eqz v1, :cond_ea

    #@bb
    .line 1590
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@be
    move-result-object v1

    #@bf
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@c2
    move-result-object v1

    #@c3
    if-eqz v1, :cond_ea

    #@c5
    .line 1591
    new-instance v12, Landroid/os/Message;

    #@c7
    invoke-direct {v12}, Landroid/os/Message;-><init>()V

    #@ca
    .line 1592
    .local v12, objMessage:Landroid/os/Message;
    new-instance v9, Landroid/os/Bundle;

    #@cc
    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    #@cf
    .line 1593
    .local v9, objBundle:Landroid/os/Bundle;
    const/16 v1, 0xa

    #@d1
    iput v1, v12, Landroid/os/Message;->what:I

    #@d3
    .line 1594
    const-string v1, "msisdn"

    #@d5
    move-object/from16 v0, p1

    #@d7
    invoke-virtual {v9, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@da
    .line 1595
    invoke-virtual {v12, v9}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@dd
    .line 1596
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@e0
    move-result-object v1

    #@e1
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@e4
    move-result-object v1

    #@e5
    const-wide/16 v2, 0x3e8

    #@e7
    invoke-virtual {v1, v12, v2, v3}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z
    :try_end_ea
    .catchall {:try_start_5a .. :try_end_ea} :catchall_125
    .catch Ljava/lang/Exception; {:try_start_5a .. :try_end_ea} :catch_105

    #@ea
    .line 1599
    .end local v9           #objBundle:Landroid/os/Bundle;
    .end local v12           #objMessage:Landroid/os/Message;
    :cond_ea
    const/4 v1, 0x1

    #@eb
    .line 1610
    if-eqz v11, :cond_59

    #@ed
    .line 1611
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@f0
    .line 1612
    const/4 v11, 0x0

    #@f1
    goto/16 :goto_59

    #@f3
    .line 1601
    :cond_f3
    const/4 v1, 0x0

    #@f4
    .line 1610
    if-eqz v11, :cond_59

    #@f6
    .line 1611
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@f9
    .line 1612
    const/4 v11, 0x0

    #@fa
    goto/16 :goto_59

    #@fc
    .line 1605
    .end local v7           #astrDeleteSelectionArg:[Ljava/lang/String;
    .end local v10           #objContentValues:Landroid/content/ContentValues;
    .end local v13           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v14           #strDeleteSelection:Ljava/lang/String;
    .end local v15           #update:I
    :cond_fc
    const/4 v1, 0x1

    #@fd
    .line 1610
    if-eqz v11, :cond_59

    #@ff
    .line 1611
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@102
    .line 1612
    const/4 v11, 0x0

    #@103
    goto/16 :goto_59

    #@105
    .line 1607
    :catch_105
    move-exception v8

    #@106
    .line 1608
    .local v8, e:Ljava/lang/Exception;
    :try_start_106
    new-instance v1, Ljava/lang/StringBuilder;

    #@108
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10b
    const-string v2, "[IP][ERROR]exception : "

    #@10d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v1

    #@111
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v1

    #@115
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@118
    move-result-object v1

    #@119
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_11c
    .catchall {:try_start_106 .. :try_end_11c} :catchall_125

    #@11c
    .line 1610
    if-eqz v11, :cond_122

    #@11e
    .line 1611
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@121
    .line 1612
    const/4 v11, 0x0

    #@122
    .line 1615
    :cond_122
    const/4 v1, 0x1

    #@123
    goto/16 :goto_59

    #@125
    .line 1610
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_125
    move-exception v1

    #@126
    if-eqz v11, :cond_12c

    #@128
    .line 1611
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@12b
    .line 1612
    const/4 v11, 0x0

    #@12c
    :cond_12c
    throw v1
.end method

.method public AddRCSUserToResourceList(Ljava/util/List;)V
    .registers 12
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p1, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x1

    #@1
    .line 1663
    const-string v0, "[IP]"

    #@3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1664
    invoke-interface {p1}, Ljava/util/List;->clear()V

    #@9
    .line 1666
    const-string v3, "rcs_user_list=?"

    #@b
    .line 1667
    .local v3, strSelection:Ljava/lang/String;
    new-array v4, v1, [Ljava/lang/String;

    #@d
    const/4 v0, 0x0

    #@e
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    aput-object v1, v4, v0

    #@14
    .line 1668
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v8, 0x0

    #@15
    .line 1670
    .local v8, objCursor:Landroid/database/Cursor;
    :try_start_15
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@17
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1a
    move-result-object v0

    #@1b
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@1d
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@1f
    const/4 v5, 0x0

    #@20
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@23
    move-result-object v8

    #@24
    .line 1671
    if-nez v8, :cond_77

    #@26
    .line 1672
    const-string v0, "[IP][ERROR] AddRCSUserToResourceList : the result is null"

    #@28
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_2b
    .catchall {:try_start_15 .. :try_end_2b} :catchall_aa
    .catch Ljava/lang/Exception; {:try_start_15 .. :try_end_2b} :catch_8c

    #@2b
    .line 1694
    if-eqz v8, :cond_31

    #@2d
    .line 1695
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@30
    .line 1696
    const/4 v8, 0x0

    #@31
    .line 1699
    :cond_31
    :goto_31
    return-void

    #@32
    .line 1681
    .local v6, columnNormalMsisdnIndex:I
    :cond_32
    :try_start_32
    invoke-interface {v8, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@35
    move-result-object v9

    #@36
    .line 1682
    .local v9, strNormalizeMSISDN:Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v1, "[IP]AddRCSUserToResourceList - strNormalizeMSISDN : "

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4c
    .line 1683
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4f
    move-result v0

    #@50
    if-nez v0, :cond_77

    #@52
    .line 1684
    invoke-interface {p1, v9}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@55
    move-result v0

    #@56
    if-nez v0, :cond_77

    #@58
    .line 1685
    invoke-interface {p1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@5b
    .line 1686
    new-instance v0, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v1, "[IP]AddRCSUserToResourceList :["

    #@62
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v0

    #@66
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v0

    #@6a
    const-string v1, "]added"

    #@6c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v0

    #@70
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v0

    #@74
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@77
    .line 1675
    .end local v6           #columnNormalMsisdnIndex:I
    .end local v9           #strNormalizeMSISDN:Ljava/lang/String;
    :cond_77
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@7a
    move-result v0

    #@7b
    if-eqz v0, :cond_85

    #@7d
    .line 1676
    const-string v0, "nor_msisdn"

    #@7f
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_82
    .catchall {:try_start_32 .. :try_end_82} :catchall_aa
    .catch Ljava/lang/Exception; {:try_start_32 .. :try_end_82} :catch_8c

    #@82
    move-result v6

    #@83
    .line 1677
    .restart local v6       #columnNormalMsisdnIndex:I
    if-gez v6, :cond_32

    #@85
    .line 1694
    .end local v6           #columnNormalMsisdnIndex:I
    :cond_85
    if-eqz v8, :cond_31

    #@87
    .line 1695
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@8a
    .line 1696
    const/4 v8, 0x0

    #@8b
    goto :goto_31

    #@8c
    .line 1691
    :catch_8c
    move-exception v7

    #@8d
    .line 1692
    .local v7, e:Ljava/lang/Exception;
    :try_start_8d
    new-instance v0, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v1, "[IP][ERROR]exception : "

    #@94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v0

    #@98
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v0

    #@9c
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v0

    #@a0
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_a3
    .catchall {:try_start_8d .. :try_end_a3} :catchall_aa

    #@a3
    .line 1694
    if-eqz v8, :cond_31

    #@a5
    .line 1695
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@a8
    .line 1696
    const/4 v8, 0x0

    #@a9
    goto :goto_31

    #@aa
    .line 1694
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_aa
    move-exception v0

    #@ab
    if-eqz v8, :cond_b1

    #@ad
    .line 1695
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@b0
    .line 1696
    const/4 v8, 0x0

    #@b1
    :cond_b1
    throw v0
.end method

.method public DeleteMyStatusIconInfo()Z
    .registers 15

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    const/4 v12, 0x0

    #@2
    .line 891
    const/4 v6, 0x0

    #@3
    .line 892
    .local v6, bIsExitStatusInfo:Z
    const/4 v11, 0x0

    #@4
    .line 893
    .local v11, objCursor:Landroid/database/Cursor;
    new-instance v10, Landroid/content/ContentValues;

    #@6
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    #@9
    .line 894
    .local v10, objContentValues:Landroid/content/ContentValues;
    const-string v1, "status_icon"

    #@b
    move-object v0, v12

    #@c
    check-cast v0, Ljava/lang/String;

    #@e
    invoke-virtual {v10, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 895
    const-string v1, "status_icon_link"

    #@13
    move-object v0, v12

    #@14
    check-cast v0, Ljava/lang/String;

    #@16
    invoke-virtual {v10, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 896
    const-string v1, "status_icon_thumb"

    #@1b
    move-object v0, v12

    #@1c
    check-cast v0, Ljava/lang/Byte;

    #@1e
    invoke-virtual {v10, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    #@21
    .line 897
    const-string v1, "status_icon_thumb_link"

    #@23
    move-object v0, v12

    #@24
    check-cast v0, Ljava/lang/String;

    #@26
    invoke-virtual {v10, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 898
    const-string v1, "status_icon_thumb_etag"

    #@2b
    move-object v0, v12

    #@2c
    check-cast v0, Ljava/lang/String;

    #@2e
    invoke-virtual {v10, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 900
    :try_start_31
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@33
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@36
    move-result-object v0

    #@37
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_CONTENT_URI:Landroid/net/Uri;

    #@39
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_MYSTATUS_PROJECTION:[Ljava/lang/String;

    #@3b
    const/4 v3, 0x0

    #@3c
    const/4 v4, 0x0

    #@3d
    const/4 v5, 0x0

    #@3e
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_41
    .catchall {:try_start_31 .. :try_end_41} :catchall_8d
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_41} :catch_6f

    #@41
    move-result-object v11

    #@42
    .line 906
    if-nez v11, :cond_65

    #@44
    .line 907
    const/4 v6, 0x0

    #@45
    .line 916
    :goto_45
    if-eqz v11, :cond_4b

    #@47
    .line 917
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@4a
    .line 918
    const/4 v11, 0x0

    #@4b
    .line 921
    :cond_4b
    :goto_4b
    if-eqz v6, :cond_95

    #@4d
    .line 922
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@52
    move-result-object v0

    #@53
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_SERVER_CONTENT_URI:Landroid/net/Uri;

    #@55
    invoke-virtual {v0, v1, v10, v12, v12}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@58
    move-result v8

    #@59
    .line 923
    .local v8, nCount:I
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    #@5c
    .line 924
    if-nez v8, :cond_ae

    #@5e
    .line 925
    const-string v0, "[IP][ERROR]MY Status DB update fail"

    #@60
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@63
    move v0, v13

    #@64
    .line 937
    .end local v8           #nCount:I
    :goto_64
    return v0

    #@65
    .line 908
    :cond_65
    :try_start_65
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_68
    .catchall {:try_start_65 .. :try_end_68} :catchall_8d
    .catch Ljava/lang/Exception; {:try_start_65 .. :try_end_68} :catch_6f

    #@68
    move-result v0

    #@69
    if-nez v0, :cond_6d

    #@6b
    .line 909
    const/4 v6, 0x0

    #@6c
    goto :goto_45

    #@6d
    .line 911
    :cond_6d
    const/4 v6, 0x1

    #@6e
    goto :goto_45

    #@6f
    .line 913
    :catch_6f
    move-exception v7

    #@70
    .line 914
    .local v7, e:Ljava/lang/Exception;
    :try_start_70
    new-instance v0, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v1, "[IP][ERROR]exception : "

    #@77
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v0

    #@7b
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v0

    #@7f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v0

    #@83
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_86
    .catchall {:try_start_70 .. :try_end_86} :catchall_8d

    #@86
    .line 916
    if-eqz v11, :cond_4b

    #@88
    .line 917
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@8b
    .line 918
    const/4 v11, 0x0

    #@8c
    goto :goto_4b

    #@8d
    .line 916
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_8d
    move-exception v0

    #@8e
    if-eqz v11, :cond_94

    #@90
    .line 917
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@93
    .line 918
    const/4 v11, 0x0

    #@94
    :cond_94
    throw v0

    #@95
    .line 929
    :cond_95
    const/4 v9, 0x0

    #@96
    .line 930
    .local v9, obUri:Landroid/net/Uri;
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@98
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9b
    move-result-object v0

    #@9c
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_SERVER_CONTENT_URI:Landroid/net/Uri;

    #@9e
    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@a1
    move-result-object v9

    #@a2
    .line 931
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    #@a5
    .line 932
    if-nez v9, :cond_ae

    #@a7
    .line 933
    const-string v0, "[IP][ERROR]MY Status DB insert fail"

    #@a9
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@ac
    move v0, v13

    #@ad
    .line 934
    goto :goto_64

    #@ae
    .line 937
    .end local v9           #obUri:Landroid/net/Uri;
    :cond_ae
    const/4 v0, 0x1

    #@af
    goto :goto_64
.end method

.method public DeleteRCSUserList(Ljava/lang/String;)Z
    .registers 15
    .parameter "szMsisdn"

    #@0
    .prologue
    .line 940
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP]msisdn : "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@16
    .line 941
    const-string v3, "nor_msisdn=?"

    #@18
    .line 942
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@19
    new-array v4, v0, [Ljava/lang/String;

    #@1b
    const/4 v0, 0x0

    #@1c
    aput-object p1, v4, v0

    #@1e
    .line 943
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    const/4 v9, 0x0

    #@1f
    .line 945
    .local v9, objCursor:Landroid/database/Cursor;
    :try_start_1f
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@21
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v0

    #@25
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@27
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@29
    const/4 v5, 0x0

    #@2a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2d
    move-result-object v9

    #@2e
    .line 950
    if-nez v9, :cond_54

    #@30
    .line 951
    new-instance v0, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v1, "[IP][ERROR]DeleteRCSUserList : Not found ["

    #@37
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v0

    #@3b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v0

    #@3f
    const-string v1, "]"

    #@41
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v0

    #@49
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_4c
    .catchall {:try_start_1f .. :try_end_4c} :catchall_e3
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_4c} :catch_c3

    #@4c
    .line 952
    const/4 v0, 0x0

    #@4d
    .line 977
    if-eqz v9, :cond_53

    #@4f
    .line 978
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@52
    .line 979
    const/4 v9, 0x0

    #@53
    .line 982
    :cond_53
    :goto_53
    return v0

    #@54
    .line 954
    :cond_54
    :try_start_54
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@57
    move-result v0

    #@58
    if-eqz v0, :cond_bb

    #@5a
    .line 955
    invoke-direct {p0, v9}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@5d
    move-result-object v10

    #@5e
    .line 956
    .local v10, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v10, :cond_54

    #@60
    .line 957
    const-string v11, "_id=?"

    #@62
    .line 958
    .local v11, strDeleteSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@63
    new-array v6, v0, [Ljava/lang/String;

    #@65
    const/4 v0, 0x0

    #@66
    iget-wide v1, v10, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nId:J

    #@68
    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@6b
    move-result-object v1

    #@6c
    aput-object v1, v6, v0

    #@6e
    .line 959
    .local v6, astrDeleteSelectionArg:[Ljava/lang/String;
    new-instance v8, Landroid/content/ContentValues;

    #@70
    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    #@73
    .line 960
    .local v8, objContentValues:Landroid/content/ContentValues;
    const-string v0, "rcs_user_list"

    #@75
    const/4 v1, 0x0

    #@76
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@79
    move-result-object v1

    #@7a
    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@7d
    .line 962
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@7f
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@82
    move-result-object v0

    #@83
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@85
    invoke-virtual {v0, v1, v8, v11, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@88
    move-result v12

    #@89
    .line 963
    .local v12, update:I
    new-instance v0, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v1, "[IP]update ["

    #@90
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v0

    #@94
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@97
    move-result-object v0

    #@98
    const-string v1, "]"

    #@9a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v0

    #@9e
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v0

    #@a2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@a5
    .line 964
    invoke-virtual {v8}, Landroid/content/ContentValues;->clear()V
    :try_end_a8
    .catchall {:try_start_54 .. :try_end_a8} :catchall_e3
    .catch Ljava/lang/Exception; {:try_start_54 .. :try_end_a8} :catch_c3

    #@a8
    .line 965
    const/4 v0, -0x1

    #@a9
    if-eq v12, v0, :cond_b3

    #@ab
    .line 966
    const/4 v0, 0x1

    #@ac
    .line 977
    if-eqz v9, :cond_53

    #@ae
    .line 978
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@b1
    .line 979
    const/4 v9, 0x0

    #@b2
    goto :goto_53

    #@b3
    .line 968
    :cond_b3
    const/4 v0, 0x0

    #@b4
    .line 977
    if-eqz v9, :cond_53

    #@b6
    .line 978
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@b9
    .line 979
    const/4 v9, 0x0

    #@ba
    goto :goto_53

    #@bb
    .line 972
    .end local v6           #astrDeleteSelectionArg:[Ljava/lang/String;
    .end local v8           #objContentValues:Landroid/content/ContentValues;
    .end local v10           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v11           #strDeleteSelection:Ljava/lang/String;
    .end local v12           #update:I
    :cond_bb
    const/4 v0, 0x1

    #@bc
    .line 977
    if-eqz v9, :cond_53

    #@be
    .line 978
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@c1
    .line 979
    const/4 v9, 0x0

    #@c2
    goto :goto_53

    #@c3
    .line 974
    :catch_c3
    move-exception v7

    #@c4
    .line 975
    .local v7, e:Ljava/lang/Exception;
    :try_start_c4
    new-instance v0, Ljava/lang/StringBuilder;

    #@c6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c9
    const-string v1, "[IP][ERROR]exception : "

    #@cb
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v0

    #@cf
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v0

    #@d3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v0

    #@d7
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_da
    .catchall {:try_start_c4 .. :try_end_da} :catchall_e3

    #@da
    .line 977
    if-eqz v9, :cond_e0

    #@dc
    .line 978
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@df
    .line 979
    const/4 v9, 0x0

    #@e0
    .line 982
    :cond_e0
    const/4 v0, 0x1

    #@e1
    goto/16 :goto_53

    #@e3
    .line 977
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_e3
    move-exception v0

    #@e4
    if-eqz v9, :cond_ea

    #@e6
    .line 978
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@e9
    .line 979
    const/4 v9, 0x0

    #@ea
    :cond_ea
    throw v0
.end method

.method public GetCapaType(Ljava/lang/String;)I
    .registers 14
    .parameter "strMSISDN"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 985
    const/4 v8, -0x1

    #@3
    .line 986
    .local v8, nReturn:I
    if-eqz p1, :cond_b

    #@5
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_12

    #@b
    .line 987
    :cond_b
    const-string v0, "[IP][ERROR]GetCapaType : MSISDN is null or Empty"

    #@d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@10
    move v9, v8

    #@11
    .line 1025
    .end local v8           #nReturn:I
    .local v9, nReturn:I
    :goto_11
    return v9

    #@12
    .line 991
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :cond_12
    new-instance v0, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v1, "[IP]msisdn : "

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@28
    .line 993
    new-array v11, v5, [Ljava/lang/String;

    #@2a
    const-string v0, "capa_type"

    #@2c
    aput-object v0, v11, v2

    #@2e
    .line 994
    .local v11, projection:[Ljava/lang/String;
    const-string v3, "nor_msisdn=?"

    #@30
    .line 995
    .local v3, selection:Ljava/lang/String;
    new-array v4, v5, [Ljava/lang/String;

    #@32
    aput-object p1, v4, v2

    #@34
    .line 996
    .local v4, selectionArgs:[Ljava/lang/String;
    const/4 v10, 0x0

    #@35
    .line 998
    .local v10, objCursor:Landroid/database/Cursor;
    :try_start_35
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@37
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3a
    move-result-object v0

    #@3b
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@3d
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@3f
    const/4 v5, 0x0

    #@40
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@43
    move-result-object v10

    #@44
    .line 999
    if-nez v10, :cond_6a

    #@46
    .line 1000
    new-instance v0, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v1, "[IP][ERROR]GetCapaType : Not found ["

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    const-string v1, "]"

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_62
    .catchall {:try_start_35 .. :try_end_62} :catchall_f4
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_62} :catch_d4

    #@62
    .line 1020
    if-eqz v10, :cond_68

    #@64
    .line 1021
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@67
    .line 1022
    const/4 v10, 0x0

    #@68
    :cond_68
    move v9, v8

    #@69
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto :goto_11

    #@6a
    .line 1002
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :cond_6a
    :try_start_6a
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@6d
    move-result v0

    #@6e
    if-nez v0, :cond_95

    #@70
    .line 1003
    new-instance v0, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v1, "[IP][ERROR]GetCapaType : Not found ["

    #@77
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v0

    #@7b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v0

    #@7f
    const-string v1, "]"

    #@81
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v0

    #@85
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v0

    #@89
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_8c
    .catchall {:try_start_6a .. :try_end_8c} :catchall_f4
    .catch Ljava/lang/Exception; {:try_start_6a .. :try_end_8c} :catch_d4

    #@8c
    .line 1020
    if-eqz v10, :cond_92

    #@8e
    .line 1021
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@91
    .line 1022
    const/4 v10, 0x0

    #@92
    :cond_92
    move v9, v8

    #@93
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto/16 :goto_11

    #@95
    .line 1007
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :cond_95
    :try_start_95
    const-string v0, "capa_type"

    #@97
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@9a
    move-result v6

    #@9b
    .line 1008
    .local v6, columnIndex:I
    if-gez v6, :cond_ab

    #@9d
    .line 1009
    const-string v0, "[IP][ERROR]GetCapaType - columnIndex is under 0"

    #@9f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_a2
    .catchall {:try_start_95 .. :try_end_a2} :catchall_f4
    .catch Ljava/lang/Exception; {:try_start_95 .. :try_end_a2} :catch_d4

    #@a2
    .line 1020
    if-eqz v10, :cond_a8

    #@a4
    .line 1021
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@a7
    .line 1022
    const/4 v10, 0x0

    #@a8
    :cond_a8
    move v9, v8

    #@a9
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto/16 :goto_11

    #@ab
    .line 1012
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :cond_ab
    :try_start_ab
    invoke-interface {v10, v6}, Landroid/database/Cursor;->getInt(I)I

    #@ae
    move-result v8

    #@af
    .line 1013
    new-instance v0, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v1, "[IP]nReturn : ["

    #@b6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v0

    #@ba
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v0

    #@be
    const-string v1, "]"

    #@c0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v0

    #@c4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v0

    #@c8
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V
    :try_end_cb
    .catchall {:try_start_ab .. :try_end_cb} :catchall_f4
    .catch Ljava/lang/Exception; {:try_start_ab .. :try_end_cb} :catch_d4

    #@cb
    .line 1020
    if-eqz v10, :cond_d1

    #@cd
    .line 1021
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@d0
    .line 1022
    const/4 v10, 0x0

    #@d1
    :cond_d1
    move v9, v8

    #@d2
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto/16 :goto_11

    #@d4
    .line 1017
    .end local v6           #columnIndex:I
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :catch_d4
    move-exception v7

    #@d5
    .line 1018
    .local v7, e:Ljava/lang/Exception;
    :try_start_d5
    new-instance v0, Ljava/lang/StringBuilder;

    #@d7
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@da
    const-string v1, "[IP][ERROR]exception : "

    #@dc
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v0

    #@e0
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v0

    #@e4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e7
    move-result-object v0

    #@e8
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_eb
    .catchall {:try_start_d5 .. :try_end_eb} :catchall_f4

    #@eb
    .line 1020
    if-eqz v10, :cond_f1

    #@ed
    .line 1021
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@f0
    .line 1022
    const/4 v10, 0x0

    #@f1
    :cond_f1
    move v9, v8

    #@f2
    .line 1025
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto/16 :goto_11

    #@f4
    .line 1020
    .end local v7           #e:Ljava/lang/Exception;
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :catchall_f4
    move-exception v0

    #@f5
    if-eqz v10, :cond_fb

    #@f7
    .line 1021
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@fa
    .line 1022
    const/4 v10, 0x0

    #@fb
    :cond_fb
    throw v0
.end method

.method public GetDocumentETag(Lcom/lge/ims/service/ip/XDMEtagInfo;)Z
    .registers 15
    .parameter "objXDMEtagInfo"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    .line 1028
    const/4 v8, 0x0

    #@2
    .line 1030
    .local v8, objCursor:Landroid/database/Cursor;
    :try_start_2
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    sget-object v1, Lcom/lge/ims/service/ip/XDMConstant;->XDM_CONTENT_URI:Landroid/net/Uri;

    #@a
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->XDM_FULL_ETAG_PROJECTION:[Ljava/lang/String;

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x0

    #@e
    const/4 v5, 0x0

    #@f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@12
    move-result-object v8

    #@13
    .line 1036
    if-nez v8, :cond_22

    #@15
    .line 1037
    const-string v0, "[IP][ERROR]GetDocumentETag : Can\'t search MyStatus Info"

    #@17
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_1a
    .catchall {:try_start_2 .. :try_end_1a} :catchall_98
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_1a} :catch_79

    #@1a
    .line 1064
    if-eqz v8, :cond_20

    #@1c
    .line 1065
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@1f
    .line 1066
    const/4 v8, 0x0

    #@20
    :cond_20
    move v0, v12

    #@21
    .line 1069
    :cond_21
    :goto_21
    return v0

    #@22
    .line 1039
    :cond_22
    :try_start_22
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_35

    #@28
    .line 1040
    const-string v0, "[IP][ERROR]GetDocumentETag : cursor move to next is false"

    #@2a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_22 .. :try_end_2d} :catchall_98
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_2d} :catch_79

    #@2d
    .line 1064
    if-eqz v8, :cond_33

    #@2f
    .line 1065
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@32
    .line 1066
    const/4 v8, 0x0

    #@33
    :cond_33
    move v0, v12

    #@34
    goto :goto_21

    #@35
    .line 1043
    :cond_35
    :try_start_35
    const-string v0, "list_etag"

    #@37
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3a
    move-result v7

    #@3b
    .line 1044
    .local v7, listcolumnIndex:I
    if-ltz v7, :cond_44

    #@3d
    .line 1045
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {p1, v0}, Lcom/lge/ims/service/ip/XDMEtagInfo;->SetListEtag(Ljava/lang/String;)V

    #@44
    .line 1047
    :cond_44
    const-string v0, "rls_etag"

    #@46
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@49
    move-result v10

    #@4a
    .line 1048
    .local v10, rlscolumnIndex:I
    if-ltz v7, :cond_53

    #@4c
    .line 1049
    invoke-interface {v8, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4f
    move-result-object v0

    #@50
    invoke-virtual {p1, v0}, Lcom/lge/ims/service/ip/XDMEtagInfo;->SetRLSEtag(Ljava/lang/String;)V

    #@53
    .line 1051
    :cond_53
    const-string v0, "rule_etag"

    #@55
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@58
    move-result v11

    #@59
    .line 1052
    .local v11, rulecolumnIndex:I
    if-ltz v7, :cond_62

    #@5b
    .line 1053
    invoke-interface {v8, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    invoke-virtual {p1, v0}, Lcom/lge/ims/service/ip/XDMEtagInfo;->SetRuleEtag(Ljava/lang/String;)V

    #@62
    .line 1055
    :cond_62
    const-string v0, "pidf_etag"

    #@64
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@67
    move-result v9

    #@68
    .line 1056
    .local v9, pidfcolumnIndex:I
    if-ltz v7, :cond_71

    #@6a
    .line 1057
    invoke-interface {v8, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {p1, v0}, Lcom/lge/ims/service/ip/XDMEtagInfo;->SetPidfEtag(Ljava/lang/String;)V
    :try_end_71
    .catchall {:try_start_35 .. :try_end_71} :catchall_98
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_71} :catch_79

    #@71
    .line 1059
    :cond_71
    const/4 v0, 0x1

    #@72
    .line 1064
    if-eqz v8, :cond_21

    #@74
    .line 1065
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@77
    .line 1066
    const/4 v8, 0x0

    #@78
    goto :goto_21

    #@79
    .line 1061
    .end local v7           #listcolumnIndex:I
    .end local v9           #pidfcolumnIndex:I
    .end local v10           #rlscolumnIndex:I
    .end local v11           #rulecolumnIndex:I
    :catch_79
    move-exception v6

    #@7a
    .line 1062
    .local v6, e:Ljava/lang/Exception;
    :try_start_7a
    new-instance v0, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v1, "[IP][ERROR]exception : "

    #@81
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v0

    #@85
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v0

    #@89
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v0

    #@8d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_90
    .catchall {:try_start_7a .. :try_end_90} :catchall_98

    #@90
    .line 1064
    if-eqz v8, :cond_96

    #@92
    .line 1065
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@95
    .line 1066
    const/4 v8, 0x0

    #@96
    :cond_96
    move v0, v12

    #@97
    .line 1069
    goto :goto_21

    #@98
    .line 1064
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_98
    move-exception v0

    #@99
    if-eqz v8, :cond_9f

    #@9b
    .line 1065
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@9e
    .line 1066
    const/4 v8, 0x0

    #@9f
    :cond_9f
    throw v0
.end method

.method public GetFirstPollingLatestTime()J
    .registers 11

    #@0
    .prologue
    .line 1270
    const/4 v9, 0x0

    #@1
    .line 1272
    .local v9, objCursor:Landroid/database/Cursor;
    :try_start_1
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_PROVISIONING_CONTENT_URI:Landroid/net/Uri;

    #@9
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_PROVISIONING_PROJECTION:[Ljava/lang/String;

    #@b
    const/4 v3, 0x0

    #@c
    const/4 v4, 0x0

    #@d
    const/4 v5, 0x0

    #@e
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@11
    move-result-object v9

    #@12
    .line 1277
    if-nez v9, :cond_22

    #@14
    .line 1278
    const-string v0, "[IP][ERROR]GetFirstPollingLatestTime - objCursor is null"

    #@16
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@19
    .line 1279
    sget-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_a2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1b} :catch_64

    #@1b
    .line 1299
    if-eqz v9, :cond_21

    #@1d
    .line 1300
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@20
    .line 1301
    const/4 v9, 0x0

    #@21
    .line 1305
    :cond_21
    :goto_21
    return-wide v0

    #@22
    .line 1280
    :cond_22
    :try_start_22
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_31

    #@28
    .line 1281
    sget-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J
    :try_end_2a
    .catchall {:try_start_22 .. :try_end_2a} :catchall_a2
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_2a} :catch_64

    #@2a
    .line 1299
    if-eqz v9, :cond_21

    #@2c
    .line 1300
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@2f
    .line 1301
    const/4 v9, 0x0

    #@30
    goto :goto_21

    #@31
    .line 1283
    :cond_31
    :try_start_31
    const-string v0, "latest_polling"

    #@33
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@36
    move-result v6

    #@37
    .line 1284
    .local v6, columnPollingIndex:I
    const-string v0, "latest_rlssub_polling"

    #@39
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3c
    move-result v7

    #@3d
    .line 1285
    .local v7, columnRLSSubPollingIndex:I
    if-gez v6, :cond_4d

    #@3f
    .line 1286
    const-string v0, "[IP][ERROR]GetFirstPollingLatestTime - columnPollingIndex is under 0"

    #@41
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@44
    .line 1287
    sget-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J
    :try_end_46
    .catchall {:try_start_31 .. :try_end_46} :catchall_a2
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_46} :catch_64

    #@46
    .line 1299
    if-eqz v9, :cond_21

    #@48
    .line 1300
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@4b
    .line 1301
    const/4 v9, 0x0

    #@4c
    goto :goto_21

    #@4d
    .line 1289
    :cond_4d
    :try_start_4d
    invoke-interface {v9, v6}, Landroid/database/Cursor;->getLong(I)J

    #@50
    move-result-wide v0

    #@51
    sput-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J

    #@53
    .line 1290
    if-ltz v7, :cond_5b

    #@55
    .line 1291
    invoke-interface {v9, v7}, Landroid/database/Cursor;->getLong(I)J

    #@58
    move-result-wide v0

    #@59
    sput-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nSubRLSPollingLatestTime:J

    #@5b
    .line 1293
    :cond_5b
    sget-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J
    :try_end_5d
    .catchall {:try_start_4d .. :try_end_5d} :catchall_a2
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_5d} :catch_64

    #@5d
    .line 1299
    if-eqz v9, :cond_21

    #@5f
    .line 1300
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@62
    .line 1301
    const/4 v9, 0x0

    #@63
    goto :goto_21

    #@64
    .line 1296
    .end local v6           #columnPollingIndex:I
    .end local v7           #columnRLSSubPollingIndex:I
    :catch_64
    move-exception v8

    #@65
    .line 1297
    .local v8, e:Ljava/lang/Exception;
    :try_start_65
    new-instance v0, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v1, "[IP][ERROR]exception : "

    #@6c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v0

    #@70
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v0

    #@74
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v0

    #@78
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_7b
    .catchall {:try_start_65 .. :try_end_7b} :catchall_a2

    #@7b
    .line 1299
    if-eqz v9, :cond_81

    #@7d
    .line 1300
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@80
    .line 1301
    const/4 v9, 0x0

    #@81
    .line 1304
    :cond_81
    new-instance v0, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v1, "[IP]nPollingLatestTime ["

    #@88
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v0

    #@8c
    sget-wide v1, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J

    #@8e
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@91
    move-result-object v0

    #@92
    const-string v1, "]"

    #@94
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v0

    #@98
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v0

    #@9c
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@9f
    .line 1305
    sget-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J

    #@a1
    goto :goto_21

    #@a2
    .line 1299
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_a2
    move-exception v0

    #@a3
    if-eqz v9, :cond_a9

    #@a5
    .line 1300
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@a8
    .line 1301
    const/4 v9, 0x0

    #@a9
    :cond_a9
    throw v0
.end method

.method public GetPollingLatestTime()J
    .registers 4

    #@0
    .prologue
    .line 1309
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP]nPollingLatestTime ["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-wide v1, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J

    #@d
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "]"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1e
    .line 1310
    sget-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J

    #@20
    return-wide v0
.end method

.method public GetRCSContactMSISDNByDataId(JLjava/lang/String;)Ljava/lang/String;
    .registers 14
    .parameter "_id"
    .parameter "strOrder"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 1317
    const-string v3, "data_id=?"

    #@3
    .line 1318
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@4
    new-array v4, v0, [Ljava/lang/String;

    #@6
    const/4 v0, 0x0

    #@7
    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    aput-object v1, v4, v0

    #@d
    .line 1319
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    const/4 v7, 0x0

    #@e
    .line 1320
    .local v7, objCursor:Landroid/database/Cursor;
    const/4 v8, 0x0

    #@f
    .line 1322
    .local v8, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :try_start_f
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@11
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@14
    move-result-object v0

    #@15
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@17
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@19
    move-object v5, p3

    #@1a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1d
    move-result-object v7

    #@1e
    .line 1328
    if-nez v7, :cond_2d

    #@20
    .line 1329
    const-string v0, "[IP][ERROR]GetRCSContactMSISDNByDataId : Can\'t search RCS Contact"

    #@22
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_25
    .catchall {:try_start_f .. :try_end_25} :catchall_67
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_25} :catch_48

    #@25
    .line 1341
    if-eqz v7, :cond_2b

    #@27
    .line 1342
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@2a
    .line 1343
    const/4 v7, 0x0

    #@2b
    :cond_2b
    move-object v0, v9

    #@2c
    .line 1346
    :cond_2c
    :goto_2c
    return-object v0

    #@2d
    .line 1332
    :cond_2d
    :try_start_2d
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_40

    #@33
    .line 1333
    invoke-direct {p0, v7}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@36
    move-result-object v8

    #@37
    .line 1334
    iget-object v0, v8, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strNormalizeMSISDN:Ljava/lang/String;
    :try_end_39
    .catchall {:try_start_2d .. :try_end_39} :catchall_67
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_39} :catch_48

    #@39
    .line 1341
    if-eqz v7, :cond_2c

    #@3b
    .line 1342
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@3e
    .line 1343
    const/4 v7, 0x0

    #@3f
    goto :goto_2c

    #@40
    .line 1341
    :cond_40
    if-eqz v7, :cond_46

    #@42
    .line 1342
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@45
    .line 1343
    const/4 v7, 0x0

    #@46
    :cond_46
    move-object v0, v9

    #@47
    goto :goto_2c

    #@48
    .line 1338
    :catch_48
    move-exception v6

    #@49
    .line 1339
    .local v6, e:Ljava/lang/Exception;
    :try_start_49
    new-instance v0, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v1, "[IP][ERROR]exception : "

    #@50
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v0

    #@58
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v0

    #@5c
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_5f
    .catchall {:try_start_49 .. :try_end_5f} :catchall_67

    #@5f
    .line 1341
    if-eqz v7, :cond_65

    #@61
    .line 1342
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@64
    .line 1343
    const/4 v7, 0x0

    #@65
    :cond_65
    move-object v0, v9

    #@66
    .line 1346
    goto :goto_2c

    #@67
    .line 1341
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_67
    move-exception v0

    #@68
    if-eqz v7, :cond_6e

    #@6a
    .line 1342
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@6d
    .line 1343
    const/4 v7, 0x0

    #@6e
    :cond_6e
    throw v0
.end method

.method public GetRCSEPollingContacts(Ljava/util/List;II)Z
    .registers 27
    .parameter
    .parameter "nLimit"
    .parameter "_nPollingTimer"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;II)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 1103
    .local p1, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x0

    #@1
    .line 1104
    .local v9, bReturn:Z
    if-nez p1, :cond_a

    #@3
    .line 1105
    const-string v2, "[IP][ERROR]IPContactHelper:GetRCSEPollingContacts - objList is null"

    #@5
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@8
    move v10, v9

    #@9
    .line 1200
    .end local v9           #bReturn:Z
    .local v10, bReturn:I
    :goto_9
    return v10

    #@a
    .line 1108
    .end local v10           #bReturn:I
    .restart local v9       #bReturn:Z
    :cond_a
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->clear()V

    #@d
    .line 1109
    const-string v5, "rcs_status=? AND (response_code=? OR response_code=? OR response_code=? OR response_code=?)"

    #@f
    .line 1115
    .local v5, selection:Ljava/lang/String;
    const/4 v2, 0x5

    #@10
    new-array v6, v2, [Ljava/lang/String;

    #@12
    const/4 v2, 0x0

    #@13
    const/4 v3, 0x0

    #@14
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    aput-object v3, v6, v2

    #@1a
    const/4 v2, 0x1

    #@1b
    const/16 v3, 0x198

    #@1d
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    aput-object v3, v6, v2

    #@23
    const/4 v2, 0x2

    #@24
    const/16 v3, 0x1e0

    #@26
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    aput-object v3, v6, v2

    #@2c
    const/4 v2, 0x3

    #@2d
    const/16 v3, 0x1f3

    #@2f
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    aput-object v3, v6, v2

    #@35
    const/4 v2, 0x4

    #@36
    const/16 v3, 0x1f7

    #@38
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    aput-object v3, v6, v2

    #@3e
    .line 1135
    .local v6, selectionArgs:[Ljava/lang/String;
    const/16 v19, 0x0

    #@40
    .line 1137
    .local v19, objCursor:Landroid/database/Cursor;
    :try_start_40
    move-object/from16 v0, p0

    #@42
    iget-object v2, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@44
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@47
    move-result-object v2

    #@48
    sget-object v3, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@4a
    sget-object v4, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@4c
    const/4 v7, 0x0

    #@4d
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@50
    move-result-object v19

    #@51
    .line 1138
    if-nez v19, :cond_61

    #@53
    .line 1139
    const-string v2, "[IP][ERROR] GetRCSEPollingContacts : the result is null"

    #@55
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_58
    .catchall {:try_start_40 .. :try_end_58} :catchall_1be
    .catch Ljava/lang/Exception; {:try_start_40 .. :try_end_58} :catch_f9

    #@58
    .line 1195
    if-eqz v19, :cond_5f

    #@5a
    .line 1196
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@5d
    .line 1197
    const/16 v19, 0x0

    #@5f
    :cond_5f
    move v10, v9

    #@60
    .restart local v10       #bReturn:I
    goto :goto_9

    #@61
    .line 1142
    .end local v10           #bReturn:I
    :cond_61
    :try_start_61
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToNext()Z

    #@64
    move-result v2

    #@65
    if-eqz v2, :cond_71

    #@67
    .line 1143
    const-string v2, "time_stamp"

    #@69
    move-object/from16 v0, v19

    #@6b
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@6e
    move-result v12

    #@6f
    .line 1144
    .local v12, columnTimeStampIndex:I
    if-gez v12, :cond_b0

    #@71
    .line 1184
    .end local v12           #columnTimeStampIndex:I
    :cond_71
    :goto_71
    new-instance v2, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v3, "[IP]GetRCSEPollingContacts ("

    #@78
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v2

    #@7c
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    #@7f
    move-result v3

    #@80
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v2

    #@84
    const-string v3, " / "

    #@86
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v2

    #@8a
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->getCount()I

    #@8d
    move-result v3

    #@8e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@91
    move-result-object v2

    #@92
    const-string v3, ") Added"

    #@94
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v2

    #@98
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v2

    #@9c
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@9f
    .line 1185
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I
    :try_end_a2
    .catchall {:try_start_61 .. :try_end_a2} :catchall_1be
    .catch Ljava/lang/Exception; {:try_start_61 .. :try_end_a2} :catch_f9

    #@a2
    move-result v2

    #@a3
    if-lez v2, :cond_1c7

    #@a5
    .line 1186
    const/4 v9, 0x1

    #@a6
    .line 1195
    :goto_a6
    if-eqz v19, :cond_ad

    #@a8
    .line 1196
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@ab
    .line 1197
    const/16 v19, 0x0

    #@ad
    :cond_ad
    move v10, v9

    #@ae
    .restart local v10       #bReturn:I
    goto/16 :goto_9

    #@b0
    .line 1148
    .end local v10           #bReturn:I
    .restart local v12       #columnTimeStampIndex:I
    :cond_b0
    :try_start_b0
    move-object/from16 v0, v19

    #@b2
    invoke-interface {v0, v12}, Landroid/database/Cursor;->getLong(I)J

    #@b5
    move-result-wide v16

    #@b6
    .line 1149
    .local v16, nTimeStamp:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@b9
    move-result-wide v14

    #@ba
    .line 1150
    .local v14, nCurrentTimeStamp:J
    new-instance v2, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v3, "[IP]GetRCSEPollingContacts - nCurrentTimeStamp : ["

    #@c1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v2

    #@c5
    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v2

    #@c9
    const-string v3, "], user time stamp + polling time : ["

    #@cb
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v2

    #@cf
    move/from16 v0, p3

    #@d1
    mul-int/lit16 v3, v0, 0x3e8

    #@d3
    int-to-long v3, v3

    #@d4
    add-long v3, v3, v16

    #@d6
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v2

    #@da
    const-string v3, "]"

    #@dc
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v2

    #@e0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v2

    #@e4
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@e7
    .line 1151
    move/from16 v0, p3

    #@e9
    mul-int/lit16 v2, v0, 0x3e8

    #@eb
    int-to-long v2, v2

    #@ec
    add-long v2, v2, v16

    #@ee
    cmp-long v2, v14, v2

    #@f0
    if-gez v2, :cond_11a

    #@f2
    .line 1152
    const-string v2, "[IP][ERROR] GetRCSEPollingContacts : it is not polling status"

    #@f4
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_f7
    .catchall {:try_start_b0 .. :try_end_f7} :catchall_1be
    .catch Ljava/lang/Exception; {:try_start_b0 .. :try_end_f7} :catch_f9

    #@f7
    goto/16 :goto_71

    #@f9
    .line 1192
    .end local v12           #columnTimeStampIndex:I
    .end local v14           #nCurrentTimeStamp:J
    .end local v16           #nTimeStamp:J
    :catch_f9
    move-exception v13

    #@fa
    .line 1193
    .local v13, e:Ljava/lang/Exception;
    :try_start_fa
    new-instance v2, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    const-string v3, "[IP][ERROR]exception : "

    #@101
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v2

    #@105
    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v2

    #@109
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v2

    #@10d
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_110
    .catchall {:try_start_fa .. :try_end_110} :catchall_1be

    #@110
    .line 1195
    if-eqz v19, :cond_117

    #@112
    .line 1196
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@115
    .line 1197
    const/16 v19, 0x0

    #@117
    :cond_117
    move v10, v9

    #@118
    .line 1200
    .restart local v10       #bReturn:I
    goto/16 :goto_9

    #@11a
    .line 1155
    .end local v10           #bReturn:I
    .end local v13           #e:Ljava/lang/Exception;
    .restart local v12       #columnTimeStampIndex:I
    .restart local v14       #nCurrentTimeStamp:J
    .restart local v16       #nTimeStamp:J
    :cond_11a
    :try_start_11a
    const-string v2, "nor_msisdn"

    #@11c
    move-object/from16 v0, v19

    #@11e
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@121
    move-result v11

    #@122
    .line 1156
    .local v11, columnNormalMsisdnIndex:I
    if-ltz v11, :cond_71

    #@124
    .line 1160
    move-object/from16 v0, v19

    #@126
    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@129
    move-result-object v20

    #@12a
    .line 1161
    .local v20, strNormalizeMSISDN:Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    #@12c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12f
    const-string v3, "[IP]GetRCSEPollingContacts - strNormalizeMSISDN : "

    #@131
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v2

    #@135
    move-object/from16 v0, v20

    #@137
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v2

    #@13b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13e
    move-result-object v2

    #@13f
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@142
    .line 1162
    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@145
    move-result v2

    #@146
    if-nez v2, :cond_1ae

    #@148
    .line 1163
    move-object/from16 v0, p1

    #@14a
    move-object/from16 v1, v20

    #@14c
    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@14f
    move-result v2

    #@150
    if-nez v2, :cond_1ae

    #@152
    .line 1164
    const-string v21, "nor_msisdn=?"

    #@154
    .line 1165
    .local v21, strSelection:Ljava/lang/String;
    const/4 v2, 0x1

    #@155
    new-array v8, v2, [Ljava/lang/String;

    #@157
    const/4 v2, 0x0

    #@158
    aput-object v20, v8, v2

    #@15a
    .line 1166
    .local v8, astrSelectionArg:[Ljava/lang/String;
    new-instance v18, Landroid/content/ContentValues;

    #@15c
    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    #@15f
    .line 1167
    .local v18, objContentValues:Landroid/content/ContentValues;
    const-string v2, "capa_type"

    #@161
    const/4 v3, 0x1

    #@162
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@165
    move-result-object v3

    #@166
    move-object/from16 v0, v18

    #@168
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@16b
    .line 1168
    move-object/from16 v0, p0

    #@16d
    iget-object v2, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@16f
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@172
    move-result-object v2

    #@173
    new-instance v3, Ljava/lang/StringBuilder;

    #@175
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@178
    sget-object v4, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT:Ljava/lang/String;

    #@17a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17d
    move-result-object v3

    #@17e
    const-string v4, "/nor_msisdn/"

    #@180
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@183
    move-result-object v3

    #@184
    move-object/from16 v0, v20

    #@186
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@189
    move-result-object v3

    #@18a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18d
    move-result-object v3

    #@18e
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@191
    move-result-object v3

    #@192
    move-object/from16 v0, v18

    #@194
    move-object/from16 v1, v21

    #@196
    invoke-virtual {v2, v3, v0, v1, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@199
    move-result v22

    #@19a
    .line 1169
    .local v22, update:I
    invoke-virtual/range {v18 .. v18}, Landroid/content/ContentValues;->clear()V

    #@19d
    .line 1171
    const/4 v2, -0x1

    #@19e
    move/from16 v0, v22

    #@1a0
    if-eq v0, v2, :cond_1b8

    #@1a2
    .line 1172
    const-string v2, "[IP]capa type update success"

    #@1a4
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1a7
    .line 1177
    :goto_1a7
    move-object/from16 v0, p1

    #@1a9
    move-object/from16 v1, v20

    #@1ab
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1ae
    .line 1180
    .end local v8           #astrSelectionArg:[Ljava/lang/String;
    .end local v18           #objContentValues:Landroid/content/ContentValues;
    .end local v21           #strSelection:Ljava/lang/String;
    .end local v22           #update:I
    :cond_1ae
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    #@1b1
    move-result v2

    #@1b2
    move/from16 v0, p2

    #@1b4
    if-ne v2, v0, :cond_61

    #@1b6
    goto/16 :goto_71

    #@1b8
    .line 1174
    .restart local v8       #astrSelectionArg:[Ljava/lang/String;
    .restart local v18       #objContentValues:Landroid/content/ContentValues;
    .restart local v21       #strSelection:Ljava/lang/String;
    .restart local v22       #update:I
    :cond_1b8
    const-string v2, "[IP]capa type update failure"

    #@1ba
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V
    :try_end_1bd
    .catchall {:try_start_11a .. :try_end_1bd} :catchall_1be
    .catch Ljava/lang/Exception; {:try_start_11a .. :try_end_1bd} :catch_f9

    #@1bd
    goto :goto_1a7

    #@1be
    .line 1195
    .end local v8           #astrSelectionArg:[Ljava/lang/String;
    .end local v11           #columnNormalMsisdnIndex:I
    .end local v12           #columnTimeStampIndex:I
    .end local v14           #nCurrentTimeStamp:J
    .end local v16           #nTimeStamp:J
    .end local v18           #objContentValues:Landroid/content/ContentValues;
    .end local v20           #strNormalizeMSISDN:Ljava/lang/String;
    .end local v21           #strSelection:Ljava/lang/String;
    .end local v22           #update:I
    :catchall_1be
    move-exception v2

    #@1bf
    if-eqz v19, :cond_1c6

    #@1c1
    .line 1196
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    #@1c4
    .line 1197
    const/16 v19, 0x0

    #@1c6
    :cond_1c6
    throw v2

    #@1c7
    .line 1188
    :cond_1c7
    const/4 v9, 0x0

    #@1c8
    goto/16 :goto_a6
.end method

.method public GetRCSETempContacts(Ljava/util/List;I)Z
    .registers 20
    .parameter
    .parameter "nLimit"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 1203
    .local p1, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v8, 0x0

    #@1
    .line 1204
    .local v8, bReturn:Z
    if-nez p1, :cond_a

    #@3
    .line 1205
    const-string v1, "[IP][ERROR]IPContactHelper:GetRCSETempContacts - objList is null"

    #@5
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@8
    move v9, v8

    #@9
    .line 1267
    .end local v8           #bReturn:Z
    .local v9, bReturn:I
    :goto_9
    return v9

    #@a
    .line 1208
    .end local v9           #bReturn:I
    .restart local v8       #bReturn:Z
    :cond_a
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->clear()V

    #@d
    .line 1210
    const-string v4, "rcs_status=? OR (first_time_rcs=? AND response_code=?)"

    #@f
    .line 1213
    .local v4, selection:Ljava/lang/String;
    const/4 v1, 0x3

    #@10
    new-array v5, v1, [Ljava/lang/String;

    #@12
    const/4 v1, 0x0

    #@13
    const/4 v2, 0x4

    #@14
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    aput-object v2, v5, v1

    #@1a
    const/4 v1, 0x1

    #@1b
    const/4 v2, 0x1

    #@1c
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    aput-object v2, v5, v1

    #@22
    const/4 v1, 0x2

    #@23
    const/16 v2, 0x194

    #@25
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    aput-object v2, v5, v1

    #@2b
    .line 1218
    .local v5, selectionArgs:[Ljava/lang/String;
    const/4 v13, 0x0

    #@2c
    .line 1220
    .local v13, objCursor:Landroid/database/Cursor;
    :try_start_2c
    move-object/from16 v0, p0

    #@2e
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@30
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@33
    move-result-object v1

    #@34
    sget-object v2, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@36
    sget-object v3, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@38
    const/4 v6, 0x0

    #@39
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3c
    move-result-object v13

    #@3d
    .line 1221
    if-nez v13, :cond_4c

    #@3f
    .line 1222
    const-string v1, "[IP][ERROR] GetRCSETempContacts : the result is null"

    #@41
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_44
    .catchall {:try_start_2c .. :try_end_44} :catchall_13a
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_44} :catch_11a

    #@44
    .line 1262
    if-eqz v13, :cond_4a

    #@46
    .line 1263
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@49
    .line 1264
    const/4 v13, 0x0

    #@4a
    :cond_4a
    move v9, v8

    #@4b
    .restart local v9       #bReturn:I
    goto :goto_9

    #@4c
    .line 1225
    .end local v9           #bReturn:I
    :cond_4c
    :try_start_4c
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    #@4f
    move-result v1

    #@50
    if-eqz v1, :cond_5f

    #@52
    .line 1226
    const-string v1, "nor_msisdn"

    #@54
    invoke-interface {v13, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@57
    move-result v10

    #@58
    .line 1227
    .local v10, columnIndex:I
    if-gez v10, :cond_9d

    #@5a
    .line 1228
    const-string v1, "[IP]GetRCSETempContacts - columnIndex is under 0. break API"

    #@5c
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5f
    .line 1250
    .end local v10           #columnIndex:I
    :cond_5f
    :goto_5f
    new-instance v1, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v2, "[IP]GetRCSETempContacts ("

    #@66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v1

    #@6a
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    #@6d
    move-result v2

    #@6e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@71
    move-result-object v1

    #@72
    const-string v2, " / "

    #@74
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v1

    #@78
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    #@7b
    move-result v2

    #@7c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v1

    #@80
    const-string v2, ") Added"

    #@82
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v1

    #@86
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v1

    #@8a
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@8d
    .line 1252
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I
    :try_end_90
    .catchall {:try_start_4c .. :try_end_90} :catchall_13a
    .catch Ljava/lang/Exception; {:try_start_4c .. :try_end_90} :catch_11a

    #@90
    move-result v1

    #@91
    if-lez v1, :cond_117

    #@93
    .line 1253
    const/4 v8, 0x1

    #@94
    .line 1262
    :goto_94
    if-eqz v13, :cond_9a

    #@96
    .line 1263
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@99
    .line 1264
    const/4 v13, 0x0

    #@9a
    :cond_9a
    move v9, v8

    #@9b
    .restart local v9       #bReturn:I
    goto/16 :goto_9

    #@9d
    .line 1232
    .end local v9           #bReturn:I
    .restart local v10       #columnIndex:I
    :cond_9d
    :try_start_9d
    invoke-interface {v13, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@a0
    move-result-object v14

    #@a1
    .line 1233
    .local v14, strNormalizeMSISDN:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v2, "[IP]GetRCSETempContacts - strMSISDN : "

    #@a8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v1

    #@ac
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v1

    #@b0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v1

    #@b4
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@b7
    .line 1234
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@ba
    move-result v1

    #@bb
    if-nez v1, :cond_10d

    #@bd
    .line 1235
    move-object/from16 v0, p1

    #@bf
    invoke-interface {v0, v14}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@c2
    move-result v1

    #@c3
    if-nez v1, :cond_10d

    #@c5
    .line 1236
    const-string v15, "nor_msisdn=?"

    #@c7
    .line 1237
    .local v15, strSelection:Ljava/lang/String;
    const/4 v1, 0x1

    #@c8
    new-array v7, v1, [Ljava/lang/String;

    #@ca
    const/4 v1, 0x0

    #@cb
    aput-object v14, v7, v1

    #@cd
    .line 1238
    .local v7, astrSelectionArg:[Ljava/lang/String;
    new-instance v12, Landroid/content/ContentValues;

    #@cf
    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    #@d2
    .line 1239
    .local v12, objContentValues:Landroid/content/ContentValues;
    const-string v1, "capa_type"

    #@d4
    const/4 v2, 0x0

    #@d5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d8
    move-result-object v2

    #@d9
    invoke-virtual {v12, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@dc
    .line 1240
    move-object/from16 v0, p0

    #@de
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@e0
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e3
    move-result-object v1

    #@e4
    new-instance v2, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    sget-object v3, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT:Ljava/lang/String;

    #@eb
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v2

    #@ef
    const-string v3, "/nor_msisdn/"

    #@f1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v2

    #@f5
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v2

    #@f9
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v2

    #@fd
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@100
    move-result-object v2

    #@101
    invoke-virtual {v1, v2, v12, v15, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@104
    move-result v16

    #@105
    .line 1241
    .local v16, update:I
    invoke-virtual {v12}, Landroid/content/ContentValues;->clear()V

    #@108
    .line 1243
    move-object/from16 v0, p1

    #@10a
    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@10d
    .line 1246
    .end local v7           #astrSelectionArg:[Ljava/lang/String;
    .end local v12           #objContentValues:Landroid/content/ContentValues;
    .end local v15           #strSelection:Ljava/lang/String;
    .end local v16           #update:I
    :cond_10d
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I
    :try_end_110
    .catchall {:try_start_9d .. :try_end_110} :catchall_13a
    .catch Ljava/lang/Exception; {:try_start_9d .. :try_end_110} :catch_11a

    #@110
    move-result v1

    #@111
    move/from16 v0, p2

    #@113
    if-ne v1, v0, :cond_4c

    #@115
    goto/16 :goto_5f

    #@117
    .line 1255
    .end local v10           #columnIndex:I
    .end local v14           #strNormalizeMSISDN:Ljava/lang/String;
    :cond_117
    const/4 v8, 0x0

    #@118
    goto/16 :goto_94

    #@11a
    .line 1259
    :catch_11a
    move-exception v11

    #@11b
    .line 1260
    .local v11, e:Ljava/lang/Exception;
    :try_start_11b
    new-instance v1, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    const-string v2, "[IP][ERROR]exception : "

    #@122
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v1

    #@126
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v1

    #@12a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v1

    #@12e
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_131
    .catchall {:try_start_11b .. :try_end_131} :catchall_13a

    #@131
    .line 1262
    if-eqz v13, :cond_137

    #@133
    .line 1263
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@136
    .line 1264
    const/4 v13, 0x0

    #@137
    :cond_137
    move v9, v8

    #@138
    .line 1267
    .restart local v9       #bReturn:I
    goto/16 :goto_9

    #@13a
    .line 1262
    .end local v9           #bReturn:I
    .end local v11           #e:Ljava/lang/Exception;
    :catchall_13a
    move-exception v1

    #@13b
    if-eqz v13, :cond_141

    #@13d
    .line 1263
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@140
    .line 1264
    const/4 v13, 0x0

    #@141
    :cond_141
    throw v1
.end method

.method public GetRLSListUri()Ljava/lang/String;
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 1072
    const/4 v7, 0x0

    #@2
    .line 1074
    .local v7, objCursor:Landroid/database/Cursor;
    :try_start_2
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v0

    #@8
    sget-object v1, Lcom/lge/ims/service/ip/XDMConstant;->XDM_CONTENT_URI:Landroid/net/Uri;

    #@a
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->XDM_FULL_ETAG_PROJECTION:[Ljava/lang/String;

    #@c
    const/4 v3, 0x0

    #@d
    const/4 v4, 0x0

    #@e
    const/4 v5, 0x0

    #@f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@12
    move-result-object v7

    #@13
    .line 1080
    if-nez v7, :cond_22

    #@15
    .line 1081
    const-string v0, "[IP][ERROR]GetRLSListUri : Can\'t search XDM Info"

    #@17
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_1a
    .catchall {:try_start_2 .. :try_end_1a} :catchall_6e
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_1a} :catch_50

    #@1a
    .line 1095
    if-eqz v7, :cond_20

    #@1c
    .line 1096
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@1f
    .line 1097
    const/4 v7, 0x0

    #@20
    :cond_20
    move-object v0, v9

    #@21
    .line 1100
    :cond_21
    :goto_21
    return-object v0

    #@22
    .line 1083
    :cond_22
    :try_start_22
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_35

    #@28
    .line 1084
    const-string v0, "[IP][ERROR]GetRLSListUri : cursor move to next is false"

    #@2a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_2d
    .catchall {:try_start_22 .. :try_end_2d} :catchall_6e
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_2d} :catch_50

    #@2d
    .line 1095
    if-eqz v7, :cond_33

    #@2f
    .line 1096
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@32
    .line 1097
    const/4 v7, 0x0

    #@33
    :cond_33
    move-object v0, v9

    #@34
    goto :goto_21

    #@35
    .line 1087
    :cond_35
    :try_start_35
    const-string v0, "rls_uri"

    #@37
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@3a
    move-result v8

    #@3b
    .line 1088
    .local v8, rlsuricolumnIndex:I
    if-ltz v8, :cond_48

    #@3d
    .line 1089
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_40
    .catchall {:try_start_35 .. :try_end_40} :catchall_6e
    .catch Ljava/lang/Exception; {:try_start_35 .. :try_end_40} :catch_50

    #@40
    move-result-object v0

    #@41
    .line 1095
    if-eqz v7, :cond_21

    #@43
    .line 1096
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@46
    .line 1097
    const/4 v7, 0x0

    #@47
    goto :goto_21

    #@48
    .line 1095
    :cond_48
    if-eqz v7, :cond_4e

    #@4a
    .line 1096
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@4d
    .line 1097
    const/4 v7, 0x0

    #@4e
    .end local v8           #rlsuricolumnIndex:I
    :cond_4e
    :goto_4e
    move-object v0, v9

    #@4f
    .line 1100
    goto :goto_21

    #@50
    .line 1092
    :catch_50
    move-exception v6

    #@51
    .line 1093
    .local v6, e:Ljava/lang/Exception;
    :try_start_51
    new-instance v0, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v1, "[IP][ERROR]exception : "

    #@58
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v0

    #@5c
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v0

    #@60
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v0

    #@64
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_67
    .catchall {:try_start_51 .. :try_end_67} :catchall_6e

    #@67
    .line 1095
    if-eqz v7, :cond_4e

    #@69
    .line 1096
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@6c
    .line 1097
    const/4 v7, 0x0

    #@6d
    goto :goto_4e

    #@6e
    .line 1095
    .end local v6           #e:Ljava/lang/Exception;
    :catchall_6e
    move-exception v0

    #@6f
    if-eqz v7, :cond_75

    #@71
    .line 1096
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@74
    .line 1097
    const/4 v7, 0x0

    #@75
    :cond_75
    throw v0
.end method

.method public GetRLSSubPollingLatestTime()J
    .registers 4

    #@0
    .prologue
    .line 1313
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP]nSubRLSPollingLatestTime ["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    sget-wide v1, Lcom/lge/ims/service/ip/IPContactHelper;->nSubRLSPollingLatestTime:J

    #@d
    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "]"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1e
    .line 1314
    sget-wide v0, Lcom/lge/ims/service/ip/IPContactHelper;->nSubRLSPollingLatestTime:J

    #@20
    return-wide v0
.end method

.method public IsRCSContact(Ljava/lang/String;)Z
    .registers 15
    .parameter "strMSISDN"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v12, 0x1

    #@2
    .line 1462
    if-eqz p1, :cond_a

    #@4
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@7
    move-result v1

    #@8
    if-eqz v1, :cond_11

    #@a
    .line 1463
    :cond_a
    const-string v1, "[IP][ERROR]IsRCSContact : MSISDN is null or Empty"

    #@c
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@f
    move v6, v0

    #@10
    .line 1508
    :cond_10
    :goto_10
    return v6

    #@11
    .line 1466
    :cond_11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "[IP]IsRCSContact : strMSISDN - "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@27
    .line 1467
    const/4 v6, 0x0

    #@28
    .line 1469
    .local v6, bReturn:Z
    const-string v3, "nor_msisdn=?"

    #@2a
    .line 1470
    .local v3, strSelection:Ljava/lang/String;
    new-array v4, v12, [Ljava/lang/String;

    #@2c
    aput-object p1, v4, v0

    #@2e
    .line 1471
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    const/4 v11, 0x0

    #@2f
    .line 1473
    .local v11, objCursor:Landroid/database/Cursor;
    :try_start_2f
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@31
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@34
    move-result-object v0

    #@35
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@37
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@39
    const/4 v5, 0x0

    #@3a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3d
    move-result-object v11

    #@3e
    .line 1479
    if-nez v11, :cond_4c

    #@40
    .line 1480
    const-string v0, "[IP][ERROR]IsRCSContact - objCursor is null"

    #@42
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_45
    .catchall {:try_start_2f .. :try_end_45} :catchall_fe
    .catch Ljava/lang/Exception; {:try_start_2f .. :try_end_45} :catch_df

    #@45
    .line 1503
    if-eqz v11, :cond_10

    #@47
    .line 1504
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@4a
    .line 1505
    const/4 v11, 0x0

    #@4b
    goto :goto_10

    #@4c
    .line 1482
    :cond_4c
    :try_start_4c
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    #@4f
    move-result v0

    #@50
    if-nez v0, :cond_5e

    #@52
    .line 1483
    const-string v0, "[IP][ERROR]IsRCSContact - objCursor move to next is false"

    #@54
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_57
    .catchall {:try_start_4c .. :try_end_57} :catchall_fe
    .catch Ljava/lang/Exception; {:try_start_4c .. :try_end_57} :catch_df

    #@57
    .line 1503
    if-eqz v11, :cond_10

    #@59
    .line 1504
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@5c
    .line 1505
    const/4 v11, 0x0

    #@5d
    goto :goto_10

    #@5e
    .line 1486
    :cond_5e
    :try_start_5e
    const-string v0, "rcs_status"

    #@60
    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@63
    move-result v8

    #@64
    .line 1487
    .local v8, columnRcsStatusIndex:I
    const-string v0, "presence_status"

    #@66
    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@69
    move-result v7

    #@6a
    .line 1488
    .local v7, columnPresenceStatusIndex:I
    const-string v0, "rcs_user_list"

    #@6c
    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@6f
    move-result v9

    #@70
    .line 1489
    .local v9, columnRcsUserListIndex:I
    if-ltz v8, :cond_76

    #@72
    if-ltz v7, :cond_76

    #@74
    if-gez v9, :cond_ae

    #@76
    .line 1490
    :cond_76
    new-instance v0, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v1, "[IP][ERROR]IsRCSContact - columnIndex is under 0 columnRcsStatusIndex["

    #@7d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v0

    #@81
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@84
    move-result-object v0

    #@85
    const-string v1, "], columnPresenceStatusIndex["

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v0

    #@8f
    const-string v1, "], columnRcsUserListIndex ["

    #@91
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v0

    #@95
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@98
    move-result-object v0

    #@99
    const-string v1, "]"

    #@9b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v0

    #@9f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v0

    #@a3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_a6
    .catchall {:try_start_5e .. :try_end_a6} :catchall_fe
    .catch Ljava/lang/Exception; {:try_start_5e .. :try_end_a6} :catch_df

    #@a6
    .line 1503
    if-eqz v11, :cond_10

    #@a8
    .line 1504
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@ab
    .line 1505
    const/4 v11, 0x0

    #@ac
    goto/16 :goto_10

    #@ae
    .line 1493
    :cond_ae
    :try_start_ae
    invoke-interface {v11, v8}, Landroid/database/Cursor;->getInt(I)I

    #@b1
    move-result v0

    #@b2
    if-ne v0, v12, :cond_c1

    #@b4
    invoke-interface {v11, v7}, Landroid/database/Cursor;->getInt(I)I

    #@b7
    move-result v0

    #@b8
    if-ne v0, v12, :cond_c1

    #@ba
    invoke-interface {v11, v9}, Landroid/database/Cursor;->getInt(I)I

    #@bd
    move-result v0

    #@be
    if-ne v0, v12, :cond_c1

    #@c0
    .line 1494
    const/4 v6, 0x1

    #@c1
    .line 1497
    :cond_c1
    new-instance v0, Ljava/lang/StringBuilder;

    #@c3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c6
    const-string v1, "[IP]IsRCSContact : bResult - "

    #@c8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v0

    #@cc
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v0

    #@d0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d3
    move-result-object v0

    #@d4
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V
    :try_end_d7
    .catchall {:try_start_ae .. :try_end_d7} :catchall_fe
    .catch Ljava/lang/Exception; {:try_start_ae .. :try_end_d7} :catch_df

    #@d7
    .line 1503
    if-eqz v11, :cond_10

    #@d9
    .line 1504
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@dc
    .line 1505
    const/4 v11, 0x0

    #@dd
    goto/16 :goto_10

    #@df
    .line 1500
    .end local v7           #columnPresenceStatusIndex:I
    .end local v8           #columnRcsStatusIndex:I
    .end local v9           #columnRcsUserListIndex:I
    :catch_df
    move-exception v10

    #@e0
    .line 1501
    .local v10, e:Ljava/lang/Exception;
    :try_start_e0
    new-instance v0, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v1, "[IP][ERROR]exception : "

    #@e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v0

    #@eb
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v0

    #@ef
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v0

    #@f3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_f6
    .catchall {:try_start_e0 .. :try_end_f6} :catchall_fe

    #@f6
    .line 1503
    if-eqz v11, :cond_10

    #@f8
    .line 1504
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@fb
    .line 1505
    const/4 v11, 0x0

    #@fc
    goto/16 :goto_10

    #@fe
    .line 1503
    .end local v10           #e:Ljava/lang/Exception;
    :catchall_fe
    move-exception v0

    #@ff
    if-eqz v11, :cond_105

    #@101
    .line 1504
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@104
    .line 1505
    const/4 v11, 0x0

    #@105
    :cond_105
    throw v0
.end method

.method public IsValidCachingContact(Ljava/lang/String;I)Z
    .registers 9
    .parameter "strMSISDN"
    .parameter "nMSecs"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1511
    if-eqz p1, :cond_b

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_b

    #@9
    if-gez p2, :cond_28

    #@b
    .line 1512
    :cond_b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "[IP][ERROR]IsValidCachingContact : parameter is invalid ("

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, ")"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@27
    .line 1526
    :cond_27
    :goto_27
    return v1

    #@28
    .line 1516
    :cond_28
    const/4 v2, 0x0

    #@29
    invoke-direct {p0, p1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@2c
    move-result-object v0

    #@2d
    .line 1518
    .local v0, objIPCapaContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v0, :cond_27

    #@2f
    .line 1522
    iget-wide v2, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nTimeStamp:J

    #@31
    int-to-long v4, p2

    #@32
    add-long/2addr v2, v4

    #@33
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@36
    move-result-wide v4

    #@37
    cmp-long v2, v2, v4

    #@39
    if-ltz v2, :cond_27

    #@3b
    .line 1525
    const-string v1, "[IP]is not valid caching time. so do not send capability query"

    #@3d
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@40
    .line 1526
    const/4 v1, 0x1

    #@41
    goto :goto_27
.end method

.method public IsValidPresenceCachingContact(Ljava/lang/String;I)Z
    .registers 10
    .parameter "strMSISDN"
    .parameter "nMSecs"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1530
    if-eqz p1, :cond_b

    #@3
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_b

    #@9
    if-gez p2, :cond_28

    #@b
    .line 1531
    :cond_b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "[IP][ERROR]IsValidPresenceCachingContact : parameter is invalid ("

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, ")"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@27
    .line 1545
    :cond_27
    :goto_27
    return v1

    #@28
    .line 1534
    :cond_28
    const/4 v2, 0x0

    #@29
    invoke-direct {p0, p1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@2c
    move-result-object v0

    #@2d
    .line 1536
    .local v0, objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-eqz v0, :cond_27

    #@2f
    .line 1540
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "[IP]My Time["

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    iget-wide v3, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nTimeStamp:J

    #@3c
    int-to-long v5, p2

    #@3d
    add-long/2addr v3, v5

    #@3e
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    const-string v3, "] system time ["

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@4b
    move-result-wide v3

    #@4c
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    const-string v3, "]"

    #@52
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v2

    #@56
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5d
    .line 1541
    iget-wide v2, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->nTimeStamp:J

    #@5f
    int-to-long v4, p2

    #@60
    add-long/2addr v2, v4

    #@61
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@64
    move-result-wide v4

    #@65
    cmp-long v2, v2, v4

    #@67
    if-ltz v2, :cond_27

    #@69
    .line 1544
    const-string v1, "[IP]IsValidPresenceCachingContact : not valid caching time"

    #@6b
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6e
    .line 1545
    const/4 v1, 0x1

    #@6f
    goto :goto_27
.end method

.method public IsValidRLSSubCachingContact(I)Z
    .registers 9
    .parameter "nMSecs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1549
    if-gez p1, :cond_9

    #@3
    .line 1550
    const-string v3, "[IP][ERROR]IsValidRLSSubCachingContact : invalid timer"

    #@5
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@8
    .line 1559
    :cond_8
    :goto_8
    return v2

    #@9
    .line 1553
    :cond_9
    invoke-virtual {p0}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRLSSubPollingLatestTime()J

    #@c
    move-result-wide v0

    #@d
    .line 1554
    .local v0, nRLSSubPollingLatestTime:J
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "[IP]My Time["

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    int-to-long v4, p1

    #@19
    add-long/2addr v4, v0

    #@1a
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    const-string v4, "] system time ["

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@27
    move-result-wide v4

    #@28
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    const-string v4, "]"

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v3

    #@36
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@39
    .line 1555
    int-to-long v3, p1

    #@3a
    add-long/2addr v3, v0

    #@3b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3e
    move-result-wide v5

    #@3f
    cmp-long v3, v3, v5

    #@41
    if-ltz v3, :cond_8

    #@43
    .line 1558
    const-string v2, "[IP]IsValidRLSSubCachingContact : not valid caching time"

    #@45
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@48
    .line 1559
    const/4 v2, 0x1

    #@49
    goto :goto_8
.end method

.method public QueryBuddyStatusIcon(Lcom/lge/ims/service/ip/PresenceInfo;Ljava/lang/String;)Z
    .registers 6
    .parameter "objBuddyPresenceInfo"
    .parameter "szMSISDN"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1448
    if-nez p1, :cond_9

    #@3
    .line 1449
    const-string v2, "[IP][ERROR]QueryBuddyStatusIcon : Parameter is invalid"

    #@5
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@8
    .line 1459
    :goto_8
    return v1

    #@9
    .line 1453
    :cond_9
    const/4 v2, 0x0

    #@a
    invoke-direct {p0, p2, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@d
    move-result-object v0

    #@e
    .line 1454
    .local v0, objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-nez v0, :cond_16

    #@10
    .line 1455
    const-string v2, "[IP][ERROR]QueryBuddyStatusIcon : IPPresenceContactData is null"

    #@12
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@15
    goto :goto_8

    #@16
    .line 1458
    :cond_16
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strStatusIconLink:Ljava/lang/String;

    #@18
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/PresenceInfo;->SetStatusIconLink(Ljava/lang/String;)V

    #@1b
    .line 1459
    const/4 v1, 0x1

    #@1c
    goto :goto_8
.end method

.method public QueryCapaInfoByMSISDN(Ljava/lang/String;Lcom/lge/ims/service/ip/CapaInfo;)Z
    .registers 7
    .parameter "strMSISDN"
    .parameter "objCapaInfo"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1349
    const-string v2, "[IP]"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 1350
    if-eqz p1, :cond_10

    #@8
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@b
    move-result v2

    #@c
    if-nez v2, :cond_10

    #@e
    if-nez p2, :cond_16

    #@10
    .line 1351
    :cond_10
    const-string v2, "[IP][ERROR]Parameter is invalid"

    #@12
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@15
    .line 1370
    :goto_15
    return v1

    #@16
    .line 1355
    :cond_16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "[IP]- "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2c
    .line 1356
    const/4 v2, 0x0

    #@2d
    invoke-direct {p0, p1, v2}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@30
    move-result-object v0

    #@31
    .line 1358
    .local v0, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-nez v0, :cond_50

    #@33
    .line 1359
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "[IP][ERROR] the result is null ("

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    const-string v3, ")"

    #@44
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@4f
    goto :goto_15

    #@50
    .line 1362
    :cond_50
    invoke-virtual {p2, p1}, Lcom/lge/ims/service/ip/CapaInfo;->SetMSISDN(Ljava/lang/String;)V

    #@53
    .line 1363
    iget v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nResponsCode:I

    #@55
    invoke-virtual {p2, v1}, Lcom/lge/ims/service/ip/CapaInfo;->SetResponseCode(I)V

    #@58
    .line 1364
    iget v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsIM:I

    #@5a
    invoke-virtual {p2, v1}, Lcom/lge/ims/service/ip/CapaInfo;->SetIMCapa(I)V

    #@5d
    .line 1365
    iget v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsFT:I

    #@5f
    invoke-virtual {p2, v1}, Lcom/lge/ims/service/ip/CapaInfo;->SetFTCapa(I)V

    #@62
    .line 1366
    iget v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsMIM:I

    #@64
    invoke-virtual {p2, v1}, Lcom/lge/ims/service/ip/CapaInfo;->SetMIMCapa(I)V

    #@67
    .line 1367
    iget v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nCapaType:I

    #@69
    invoke-virtual {p2, v1}, Lcom/lge/ims/service/ip/CapaInfo;->SetCapaType(I)V

    #@6c
    .line 1368
    iget v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsHTTP:I

    #@6e
    invoke-virtual {p2, v1}, Lcom/lge/ims/service/ip/CapaInfo;->SetHTTPCapa(I)V

    #@71
    .line 1369
    iget v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->nIsPresence:I

    #@73
    invoke-virtual {p2, v1}, Lcom/lge/ims/service/ip/CapaInfo;->SetPresenceCapa(I)V

    #@76
    .line 1370
    const/4 v1, 0x1

    #@77
    goto :goto_15
.end method

.method public QueryMyStatusIcon(Lcom/lge/ims/service/ip/MyStatusInfo;)Z
    .registers 11
    .parameter "objMyStatusInfo"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 1401
    if-nez p1, :cond_9

    #@3
    .line 1402
    const-string v7, "[IP][ERROR]QueryMyStatusThumbIconInfo : Parameter is invalid"

    #@5
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@8
    .line 1445
    :cond_8
    :goto_8
    return v6

    #@9
    .line 1406
    :cond_9
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPContactHelper;->GetMyStatusData()Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;

    #@c
    move-result-object v3

    #@d
    .line 1408
    .local v3, objMyStatusData:Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;
    if-nez v3, :cond_15

    #@f
    .line 1409
    const-string v7, "[IP][ERROR]QueryMyStatusThumbIconInfo : IPMyStatusData is null"

    #@11
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@14
    goto :goto_8

    #@15
    .line 1412
    :cond_15
    iget-object v7, v3, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconThumbEtag:Ljava/lang/String;

    #@17
    invoke-virtual {p1, v7}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconThumbEtag(Ljava/lang/String;)V

    #@1a
    .line 1413
    new-instance v1, Ljava/io/File;

    #@1c
    const-string v7, "/data/data/com.lge.ims/image/mythumbnail.jpg"

    #@1e
    invoke-direct {v1, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@21
    .line 1414
    .local v1, copyFile:Ljava/io/File;
    const/4 v4, 0x0

    #@22
    .line 1415
    .local v4, out:Ljava/io/OutputStream;
    iget-object v7, v3, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconThumb:[B

    #@24
    if-eqz v7, :cond_8

    #@26
    .line 1416
    iget-object v7, v3, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconThumb:[B

    #@28
    iget-object v8, v3, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconThumb:[B

    #@2a
    array-length v8, v8

    #@2b
    invoke-static {v7, v6, v8}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    #@2e
    move-result-object v0

    #@2f
    .line 1417
    .local v0, bit:Landroid/graphics/Bitmap;
    if-eqz v0, :cond_8

    #@31
    .line 1419
    :try_start_31
    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    #@34
    .line 1420
    new-instance v5, Ljava/io/FileOutputStream;

    #@36
    invoke-direct {v5, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_39
    .catchall {:try_start_31 .. :try_end_39} :catchall_c4
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_39} :catch_8f

    #@39
    .line 1421
    .end local v4           #out:Ljava/io/OutputStream;
    .local v5, out:Ljava/io/OutputStream;
    :try_start_39
    sget-object v7, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@3b
    const/16 v8, 0x5f

    #@3d
    invoke-virtual {v0, v7, v8, v5}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@40
    move-result v7

    #@41
    if-eqz v7, :cond_58

    #@43
    .line 1422
    const-string v6, "/data/data/com.lge.ims/image/mythumbnail.jpg"

    #@45
    invoke-virtual {p1, v6}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconThumb(Ljava/lang/String;)V

    #@48
    .line 1423
    iget-object v6, v3, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIcon:Ljava/lang/String;

    #@4a
    invoke-virtual {p1, v6}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIcon(Ljava/lang/String;)V

    #@4d
    .line 1424
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_50
    .catchall {:try_start_39 .. :try_end_50} :catchall_e3
    .catch Ljava/lang/Exception; {:try_start_39 .. :try_end_50} :catch_e6

    #@50
    .line 1432
    if-eqz v5, :cond_55

    #@52
    .line 1433
    :try_start_52
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_55
    .catch Ljava/lang/Exception; {:try_start_52 .. :try_end_55} :catch_76

    #@55
    :cond_55
    move-object v4, v5

    #@56
    .line 1445
    .end local v5           #out:Ljava/io/OutputStream;
    .restart local v4       #out:Ljava/io/OutputStream;
    :cond_56
    :goto_56
    const/4 v6, 0x1

    #@57
    goto :goto_8

    #@58
    .line 1432
    .end local v4           #out:Ljava/io/OutputStream;
    .restart local v5       #out:Ljava/io/OutputStream;
    :cond_58
    if-eqz v5, :cond_8

    #@5a
    .line 1433
    :try_start_5a
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_5d
    .catch Ljava/lang/Exception; {:try_start_5a .. :try_end_5d} :catch_5e

    #@5d
    goto :goto_8

    #@5e
    .line 1435
    :catch_5e
    move-exception v2

    #@5f
    .line 1436
    .local v2, e:Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v8, "[IP][ERROR]exception : "

    #@66
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v7

    #@6e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v7

    #@72
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@75
    goto :goto_8

    #@76
    .line 1435
    .end local v2           #e:Ljava/lang/Exception;
    :catch_76
    move-exception v2

    #@77
    .line 1436
    .restart local v2       #e:Ljava/lang/Exception;
    new-instance v6, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v7, "[IP][ERROR]exception : "

    #@7e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v6

    #@82
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v6

    #@86
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v6

    #@8a
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@8d
    move-object v4, v5

    #@8e
    .line 1438
    .end local v5           #out:Ljava/io/OutputStream;
    .restart local v4       #out:Ljava/io/OutputStream;
    goto :goto_56

    #@8f
    .line 1428
    .end local v2           #e:Ljava/lang/Exception;
    :catch_8f
    move-exception v2

    #@90
    .line 1429
    .restart local v2       #e:Ljava/lang/Exception;
    :goto_90
    :try_start_90
    new-instance v6, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v7, "[IP][ERROR]exception : "

    #@97
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v6

    #@9f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v6

    #@a3
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_a6
    .catchall {:try_start_90 .. :try_end_a6} :catchall_c4

    #@a6
    .line 1432
    if-eqz v4, :cond_56

    #@a8
    .line 1433
    :try_start_a8
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_ab
    .catch Ljava/lang/Exception; {:try_start_a8 .. :try_end_ab} :catch_ac

    #@ab
    goto :goto_56

    #@ac
    .line 1435
    :catch_ac
    move-exception v2

    #@ad
    .line 1436
    new-instance v6, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v7, "[IP][ERROR]exception : "

    #@b4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v6

    #@b8
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v6

    #@bc
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v6

    #@c0
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@c3
    goto :goto_56

    #@c4
    .line 1431
    .end local v2           #e:Ljava/lang/Exception;
    :catchall_c4
    move-exception v6

    #@c5
    .line 1432
    :goto_c5
    if-eqz v4, :cond_ca

    #@c7
    .line 1433
    :try_start_c7
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V
    :try_end_ca
    .catch Ljava/lang/Exception; {:try_start_c7 .. :try_end_ca} :catch_cb

    #@ca
    .line 1437
    :cond_ca
    :goto_ca
    throw v6

    #@cb
    .line 1435
    :catch_cb
    move-exception v2

    #@cc
    .line 1436
    .restart local v2       #e:Ljava/lang/Exception;
    new-instance v7, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v8, "[IP][ERROR]exception : "

    #@d3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v7

    #@d7
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v7

    #@db
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@de
    move-result-object v7

    #@df
    invoke-static {v7}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e2
    goto :goto_ca

    #@e3
    .line 1431
    .end local v2           #e:Ljava/lang/Exception;
    .end local v4           #out:Ljava/io/OutputStream;
    .restart local v5       #out:Ljava/io/OutputStream;
    :catchall_e3
    move-exception v6

    #@e4
    move-object v4, v5

    #@e5
    .end local v5           #out:Ljava/io/OutputStream;
    .restart local v4       #out:Ljava/io/OutputStream;
    goto :goto_c5

    #@e6
    .line 1428
    .end local v4           #out:Ljava/io/OutputStream;
    .restart local v5       #out:Ljava/io/OutputStream;
    :catch_e6
    move-exception v2

    #@e7
    move-object v4, v5

    #@e8
    .end local v5           #out:Ljava/io/OutputStream;
    .restart local v4       #out:Ljava/io/OutputStream;
    goto :goto_90
.end method

.method public QueryMyStatusInfo(Lcom/lge/ims/service/ip/MyStatusInfo;)Z
    .registers 5
    .parameter "objMyStatusInfo"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1373
    if-nez p1, :cond_9

    #@3
    .line 1374
    const-string v2, "[IP][ERROR]QueryMyStatusInfo : Parameter is invalid"

    #@5
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@8
    .line 1398
    :goto_8
    return v1

    #@9
    .line 1378
    :cond_9
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPContactHelper;->GetMyStatusData()Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;

    #@c
    move-result-object v0

    #@d
    .line 1380
    .local v0, objMyStatusData:Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;
    if-nez v0, :cond_15

    #@f
    .line 1381
    const-string v2, "[IP][ERROR]QueryMyStatusInfo : IPMyStatusData is null"

    #@11
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@14
    goto :goto_8

    #@15
    .line 1384
    :cond_15
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strHomepage:Ljava/lang/String;

    #@17
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetHomePage(Ljava/lang/String;)V

    #@1a
    .line 1385
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strFreeText:Ljava/lang/String;

    #@1c
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetFreeText(Ljava/lang/String;)V

    #@1f
    .line 1386
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strDisplayName:Ljava/lang/String;

    #@21
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetDisplayName(Ljava/lang/String;)V

    #@24
    .line 1387
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strEMailAddress:Ljava/lang/String;

    #@26
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetEMailAddress(Ljava/lang/String;)V

    #@29
    .line 1388
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strBirthDay:Ljava/lang/String;

    #@2b
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetBirthDay(Ljava/lang/String;)V

    #@2e
    .line 1389
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strTwitterAccount:Ljava/lang/String;

    #@30
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetTwitterAccount(Ljava/lang/String;)V

    #@33
    .line 1390
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strFaceBookAccount:Ljava/lang/String;

    #@35
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetFaceBookAccount(Ljava/lang/String;)V

    #@38
    .line 1391
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strCyworldAccount:Ljava/lang/String;

    #@3a
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetCyworldAccount(Ljava/lang/String;)V

    #@3d
    .line 1392
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strWaggleAccount:Ljava/lang/String;

    #@3f
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetWaggleAccount(Ljava/lang/String;)V

    #@42
    .line 1393
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIcon:Ljava/lang/String;

    #@44
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIcon(Ljava/lang/String;)V

    #@47
    .line 1394
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconLink:Ljava/lang/String;

    #@49
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconLink(Ljava/lang/String;)V

    #@4c
    .line 1395
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconThumbLink:Ljava/lang/String;

    #@4e
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconThumbLink(Ljava/lang/String;)V

    #@51
    .line 1396
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->strStatusIconThumbEtag:Ljava/lang/String;

    #@53
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatusIconThumbEtag(Ljava/lang/String;)V

    #@56
    .line 1397
    iget v1, v0, Lcom/lge/ims/service/ip/IPContactHelper$IPMyStatusData;->nStatus:I

    #@58
    invoke-virtual {p1, v1}, Lcom/lge/ims/service/ip/MyStatusInfo;->SetStatus(I)V

    #@5b
    .line 1398
    const/4 v1, 0x1

    #@5c
    goto :goto_8
.end method

.method public SetFirstTimeRcs()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 349
    const-string v4, "[IP]"

    #@4
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7
    .line 350
    const-string v3, "first_time_rcs=?"

    #@9
    .line 351
    .local v3, strSelection:Ljava/lang/String;
    new-array v2, v6, [Ljava/lang/String;

    #@b
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@e
    move-result-object v4

    #@f
    aput-object v4, v2, v5

    #@11
    .line 352
    .local v2, selectionArgs:[Ljava/lang/String;
    new-instance v1, Landroid/content/ContentValues;

    #@13
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@16
    .line 353
    .local v1, objContentValues:Landroid/content/ContentValues;
    const-string v4, "first_time_rcs"

    #@18
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v1, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1f
    .line 355
    iget-object v4, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@21
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v4

    #@25
    sget-object v5, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@27
    invoke-virtual {v4, v5, v1, v3, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@2a
    move-result v0

    #@2b
    .line 356
    .local v0, ncount:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v5, "[IP]ncount ["

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    const-string v5, "]"

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@47
    .line 357
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@4a
    .line 358
    return-void
.end method

.method public SetLimitationResourceListUserCount(I)Z
    .registers 4
    .parameter "_nLimitationResourceListUserCount"

    #@0
    .prologue
    .line 258
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP]nLimitationResourceListUserCount [ "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, " ]"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1c
    .line 259
    sput p1, Lcom/lge/ims/service/ip/IPContactHelper;->nLimitationResourceListUserCount:I

    #@1e
    .line 260
    const/4 v0, 0x1

    #@1f
    return v0
.end method

.method public Sync()Z
    .registers 10

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 263
    const-string v6, "[IP]"

    #@4
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7
    .line 264
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@a
    move-result-wide v2

    #@b
    .line 266
    .local v2, nPreviousTime:J
    sget-boolean v6, Lcom/lge/ims/service/ip/IPContactHelper;->isSynchronizing:Z

    #@d
    if-eqz v6, :cond_17

    #@f
    .line 267
    const-string v5, "[IP][ERROR]Because of Sync, return"

    #@11
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@14
    .line 268
    sput-boolean v4, Lcom/lge/ims/service/ip/IPContactHelper;->isToSync:Z

    #@16
    .line 289
    :cond_16
    :goto_16
    return v4

    #@17
    .line 272
    :cond_17
    sput-boolean v4, Lcom/lge/ims/service/ip/IPContactHelper;->isSynchronizing:Z

    #@19
    .line 273
    sput-boolean v5, Lcom/lge/ims/service/ip/IPContactHelper;->isToSync:Z

    #@1b
    .line 275
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPContactHelper;->JoinCursorNCreateRCSContact()Z

    #@1e
    move-result v6

    #@1f
    if-nez v6, :cond_28

    #@21
    .line 276
    const-string v4, "[IP][ERROR]Join Cursor Error"

    #@23
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@26
    move v4, v5

    #@27
    .line 277
    goto :goto_16

    #@28
    .line 280
    :cond_28
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@2b
    move-result-wide v0

    #@2c
    .line 281
    .local v0, nAfterTime:J
    new-instance v6, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v7, "[IP]Sync Total Time : "

    #@33
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    sub-long v7, v0, v2

    #@39
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v6

    #@3d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v6

    #@41
    invoke-static {v6}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@44
    .line 283
    sput-boolean v5, Lcom/lge/ims/service/ip/IPContactHelper;->isSynchronizing:Z

    #@46
    .line 285
    sget-boolean v5, Lcom/lge/ims/service/ip/IPContactHelper;->isToSync:Z

    #@48
    if-eqz v5, :cond_16

    #@4a
    .line 286
    const-string v5, "[IP]--- ReSync"

    #@4c
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@4f
    .line 287
    invoke-virtual {p0}, Lcom/lge/ims/service/ip/IPContactHelper;->Sync()Z

    #@52
    goto :goto_16
.end method

.method public SyncPresenceFullImageToContact(Z)Z
    .registers 3
    .parameter "bIgnorePriority"

    #@0
    .prologue
    .line 340
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 341
    const/4 v0, 0x0

    #@5
    .line 346
    :goto_5
    return v0

    #@6
    .line 343
    :cond_6
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactImageData_ReUpdate(Z)Z

    #@9
    .line 344
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactKeepingImageData_Update(Z)Z

    #@c
    .line 346
    const/4 v0, 0x1

    #@d
    goto :goto_5
.end method

.method public SyncRCSUserToResourceList(Ljava/util/List;Z)V
    .registers 14
    .parameter
    .parameter "bAddOperation"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 292
    .local p1, objList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "[IP]: is Add operations ["

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "]"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1c
    .line 293
    invoke-interface {p1}, Ljava/util/List;->clear()V

    #@1f
    .line 294
    const-string v3, "(rcs_status=? OR rcs_status=?) AND rcs_user_list=? AND presence_status=?"

    #@21
    .line 296
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@22
    if-ne p2, v0, :cond_9b

    #@24
    .line 297
    invoke-direct {p0}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceUserCount()I

    #@27
    move-result v8

    #@28
    .line 298
    .local v8, nPresenceCount:I
    new-instance v0, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v1, "[IP]Presence User Count [ "

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v0

    #@37
    const-string v1, " ], limit resource-list user [ "

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    sget v1, Lcom/lge/ims/service/ip/IPContactHelper;->nLimitationResourceListUserCount:I

    #@3f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v0

    #@43
    const-string v1, " ]"

    #@45
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v0

    #@49
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@50
    .line 299
    sget v0, Lcom/lge/ims/service/ip/IPContactHelper;->nLimitationResourceListUserCount:I

    #@52
    if-lt v8, v0, :cond_5a

    #@54
    .line 300
    const-string v0, "[IP][ERROR] SyncRCSUserToResourceList : exceed max user size. Don`t not add sync operation"

    #@56
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@59
    .line 338
    .end local v8           #nPresenceCount:I
    :cond_59
    :goto_59
    return-void

    #@5a
    .line 303
    .restart local v8       #nPresenceCount:I
    :cond_5a
    const/4 v0, 0x4

    #@5b
    new-array v4, v0, [Ljava/lang/String;

    #@5d
    const/4 v0, 0x0

    #@5e
    const/4 v1, 0x1

    #@5f
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@62
    move-result-object v1

    #@63
    aput-object v1, v4, v0

    #@65
    const/4 v0, 0x1

    #@66
    const/4 v1, 0x1

    #@67
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@6a
    move-result-object v1

    #@6b
    aput-object v1, v4, v0

    #@6d
    const/4 v0, 0x2

    #@6e
    const/4 v1, 0x0

    #@6f
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    aput-object v1, v4, v0

    #@75
    const/4 v0, 0x3

    #@76
    const/4 v1, 0x1

    #@77
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@7a
    move-result-object v1

    #@7b
    aput-object v1, v4, v0

    #@7d
    .line 307
    .end local v8           #nPresenceCount:I
    .local v4, selectionArgs:[Ljava/lang/String;
    :goto_7d
    const/4 v9, 0x0

    #@7e
    .line 310
    .local v9, objCursor:Landroid/database/Cursor;
    :try_start_7e
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@80
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@83
    move-result-object v0

    #@84
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@86
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@88
    const/4 v5, 0x0

    #@89
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@8c
    move-result-object v9

    #@8d
    .line 311
    if-nez v9, :cond_ee

    #@8f
    .line 312
    const-string v0, "[IP][ERROR] AddRCSUserToResourceList : the result is null"

    #@91
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_94
    .catchall {:try_start_7e .. :try_end_94} :catchall_123
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_94} :catch_104

    #@94
    .line 333
    if-eqz v9, :cond_59

    #@96
    .line 334
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@99
    .line 335
    const/4 v9, 0x0

    #@9a
    goto :goto_59

    #@9b
    .line 305
    .end local v4           #selectionArgs:[Ljava/lang/String;
    .end local v9           #objCursor:Landroid/database/Cursor;
    :cond_9b
    const/4 v0, 0x4

    #@9c
    new-array v4, v0, [Ljava/lang/String;

    #@9e
    const/4 v0, 0x0

    #@9f
    const/4 v1, 0x0

    #@a0
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@a3
    move-result-object v1

    #@a4
    aput-object v1, v4, v0

    #@a6
    const/4 v0, 0x1

    #@a7
    const/4 v1, 0x4

    #@a8
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@ab
    move-result-object v1

    #@ac
    aput-object v1, v4, v0

    #@ae
    const/4 v0, 0x2

    #@af
    const/4 v1, 0x1

    #@b0
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@b3
    move-result-object v1

    #@b4
    aput-object v1, v4, v0

    #@b6
    const/4 v0, 0x3

    #@b7
    const/4 v1, 0x1

    #@b8
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@bb
    move-result-object v1

    #@bc
    aput-object v1, v4, v0

    #@be
    .restart local v4       #selectionArgs:[Ljava/lang/String;
    goto :goto_7d

    #@bf
    .line 321
    .local v6, columnNormalMsisdnIndex:I
    .restart local v9       #objCursor:Landroid/database/Cursor;
    :cond_bf
    :try_start_bf
    invoke-interface {v9, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@c2
    move-result-object v10

    #@c3
    .line 322
    .local v10, strNormalizeMSISDN:Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c6
    move-result v0

    #@c7
    if-nez v0, :cond_ee

    #@c9
    .line 323
    invoke-interface {p1, v10}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    #@cc
    move-result v0

    #@cd
    if-nez v0, :cond_ee

    #@cf
    .line 324
    invoke-interface {p1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@d2
    .line 325
    new-instance v0, Ljava/lang/StringBuilder;

    #@d4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@d7
    const-string v1, "[IP]:[ "

    #@d9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v0

    #@dd
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v0

    #@e1
    const-string v1, " ]added"

    #@e3
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v0

    #@e7
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ea
    move-result-object v0

    #@eb
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@ee
    .line 315
    .end local v6           #columnNormalMsisdnIndex:I
    .end local v10           #strNormalizeMSISDN:Ljava/lang/String;
    :cond_ee
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@f1
    move-result v0

    #@f2
    if-eqz v0, :cond_fc

    #@f4
    .line 316
    const-string v0, "nor_msisdn"

    #@f6
    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_f9
    .catchall {:try_start_bf .. :try_end_f9} :catchall_123
    .catch Ljava/lang/Exception; {:try_start_bf .. :try_end_f9} :catch_104

    #@f9
    move-result v6

    #@fa
    .line 317
    .restart local v6       #columnNormalMsisdnIndex:I
    if-gez v6, :cond_bf

    #@fc
    .line 333
    .end local v6           #columnNormalMsisdnIndex:I
    :cond_fc
    if-eqz v9, :cond_59

    #@fe
    .line 334
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@101
    .line 335
    const/4 v9, 0x0

    #@102
    goto/16 :goto_59

    #@104
    .line 330
    :catch_104
    move-exception v7

    #@105
    .line 331
    .local v7, e:Ljava/lang/Exception;
    :try_start_105
    new-instance v0, Ljava/lang/StringBuilder;

    #@107
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@10a
    const-string v1, "[IP][ERROR]exception : "

    #@10c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10f
    move-result-object v0

    #@110
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v0

    #@114
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@117
    move-result-object v0

    #@118
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_11b
    .catchall {:try_start_105 .. :try_end_11b} :catchall_123

    #@11b
    .line 333
    if-eqz v9, :cond_59

    #@11d
    .line 334
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@120
    .line 335
    const/4 v9, 0x0

    #@121
    goto/16 :goto_59

    #@123
    .line 333
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_123
    move-exception v0

    #@124
    if-eqz v9, :cond_12a

    #@126
    .line 334
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@129
    .line 335
    const/4 v9, 0x0

    #@12a
    :cond_12a
    throw v0
.end method

.method public UpdateBuddyPresenceInformation(Lcom/lge/ims/service/ip/PresenceInfo;)I
    .registers 15
    .parameter "objPresenceInfo"

    #@0
    .prologue
    .line 422
    const/4 v8, 0x0

    #@1
    .line 423
    .local v8, nReturn:I
    if-eqz p1, :cond_9

    #@3
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->IsInvalid()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 424
    :cond_9
    const-string v0, "[IP][ERROR] MSISDN is null or Empty"

    #@b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e
    move v9, v8

    #@f
    .line 481
    .end local v8           #nReturn:I
    .local v9, nReturn:I
    :goto_f
    return v9

    #@10
    .line 428
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :cond_10
    new-instance v0, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v1, "[IP]msisdn : "

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2a
    .line 430
    const-string v3, "nor_msisdn=?"

    #@2c
    .line 431
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@2d
    new-array v4, v0, [Ljava/lang/String;

    #@2f
    const/4 v0, 0x0

    #@30
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    aput-object v1, v4, v0

    #@36
    .line 432
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    const/4 v10, 0x0

    #@37
    .line 434
    .local v10, objCursor:Landroid/database/Cursor;
    :try_start_37
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@39
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3c
    move-result-object v0

    #@3d
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@3f
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_PRESENCE_PROJECTION:[Ljava/lang/String;

    #@41
    const/4 v5, 0x0

    #@42
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@45
    move-result-object v10

    #@46
    .line 440
    if-nez v10, :cond_6a

    #@48
    .line 441
    new-instance v0, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v1, "[IP][ERROR]Not found : "

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_62
    .catchall {:try_start_37 .. :try_end_62} :catchall_124
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_62} :catch_ba

    #@62
    .line 476
    if-eqz v10, :cond_68

    #@64
    .line 477
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@67
    .line 478
    const/4 v10, 0x0

    #@68
    :cond_68
    move v9, v8

    #@69
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto :goto_f

    #@6a
    .line 444
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :cond_6a
    :try_start_6a
    new-instance v0, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v1, "[IP]: objCursor count - "

    #@71
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v0

    #@75
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    #@78
    move-result v1

    #@79
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v0

    #@7d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v0

    #@81
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@84
    .line 445
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I

    #@87
    move-result v0

    #@88
    if-nez v0, :cond_a5

    #@8a
    .line 446
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@8d
    move-result-object v0

    #@8e
    const/4 v1, 0x0

    #@8f
    invoke-direct {p0, v0, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@92
    move-result-object v0

    #@93
    if-eqz v0, :cond_9c

    #@95
    .line 447
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->PresenceData_Insert(Lcom/lge/ims/service/ip/PresenceInfo;)Z

    #@98
    .line 448
    const/4 v0, 0x0

    #@99
    invoke-direct {p0, v0, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->PresenceData_CheckStatusIconEtag(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;Lcom/lge/ims/service/ip/PresenceInfo;)Z
    :try_end_9c
    .catchall {:try_start_6a .. :try_end_9c} :catchall_124
    .catch Ljava/lang/Exception; {:try_start_6a .. :try_end_9c} :catch_ba

    #@9c
    .line 476
    :cond_9c
    if-eqz v10, :cond_a2

    #@9e
    .line 477
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@a1
    .line 478
    const/4 v10, 0x0

    #@a2
    :cond_a2
    :goto_a2
    move v9, v8

    #@a3
    .line 481
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto/16 :goto_f

    #@a5
    .line 451
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :cond_a5
    :try_start_a5
    new-instance v12, Ljava/util/LinkedList;

    #@a7
    invoke-direct {v12}, Ljava/util/LinkedList;-><init>()V

    #@aa
    .line 452
    .local v12, objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :cond_aa
    :goto_aa
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@ad
    move-result v0

    #@ae
    if-eqz v0, :cond_d8

    #@b0
    .line 453
    invoke-direct {p0, v10}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@b3
    move-result-object v11

    #@b4
    .line 454
    .local v11, objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-eqz v11, :cond_aa

    #@b6
    .line 457
    invoke-virtual {v12, v11}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_b9
    .catchall {:try_start_a5 .. :try_end_b9} :catchall_124
    .catch Ljava/lang/Exception; {:try_start_a5 .. :try_end_b9} :catch_ba

    #@b9
    goto :goto_aa

    #@ba
    .line 473
    .end local v11           #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    .end local v12           #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :catch_ba
    move-exception v6

    #@bb
    .line 474
    .local v6, e:Ljava/lang/Exception;
    :try_start_bb
    new-instance v0, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v1, "[IP][ERROR]exception : "

    #@c2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v0

    #@c6
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v0

    #@ca
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v0

    #@ce
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_d1
    .catchall {:try_start_bb .. :try_end_d1} :catchall_124

    #@d1
    .line 476
    if-eqz v10, :cond_a2

    #@d3
    .line 477
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@d6
    .line 478
    const/4 v10, 0x0

    #@d7
    goto :goto_a2

    #@d8
    .line 461
    .end local v6           #e:Ljava/lang/Exception;
    .restart local v12       #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :cond_d8
    :try_start_d8
    new-instance v0, Ljava/lang/StringBuilder;

    #@da
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@dd
    const-string v1, "[IP]list size : ["

    #@df
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v0

    #@e3
    invoke-virtual {v12}, Ljava/util/LinkedList;->size()I

    #@e6
    move-result v1

    #@e7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v0

    #@eb
    const-string v1, "]"

    #@ed
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v0

    #@f1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v0

    #@f5
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@f8
    .line 462
    const/4 v7, 0x0

    #@f9
    .local v7, i:I
    :goto_f9
    invoke-virtual {v12}, Ljava/util/LinkedList;->size()I

    #@fc
    move-result v0

    #@fd
    if-ge v7, v0, :cond_117

    #@ff
    .line 463
    invoke-virtual {v12, v7}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@102
    move-result-object v11

    #@103
    check-cast v11, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@105
    .line 464
    .restart local v11       #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    iget-object v0, v11, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;->strNormalizeMSISDN:Ljava/lang/String;

    #@107
    const/4 v1, 0x0

    #@108
    invoke-direct {p0, v0, v1}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactDataByMSISDN(Ljava/lang/String;Ljava/lang/String;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@10b
    move-result-object v0

    #@10c
    if-eqz v0, :cond_114

    #@10e
    .line 465
    invoke-direct {p0, v11, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->PresenceData_CheckStatusIconEtag(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;Lcom/lge/ims/service/ip/PresenceInfo;)Z

    #@111
    .line 466
    invoke-direct {p0, v11, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->PresenceData_Update(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;Lcom/lge/ims/service/ip/PresenceInfo;)Z

    #@114
    .line 462
    :cond_114
    add-int/lit8 v7, v7, 0x1

    #@116
    goto :goto_f9

    #@117
    .line 469
    .end local v11           #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    :cond_117
    invoke-virtual {v12}, Ljava/util/LinkedList;->size()I
    :try_end_11a
    .catchall {:try_start_d8 .. :try_end_11a} :catchall_124
    .catch Ljava/lang/Exception; {:try_start_d8 .. :try_end_11a} :catch_ba

    #@11a
    move-result v8

    #@11b
    .line 476
    if-eqz v10, :cond_121

    #@11d
    .line 477
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@120
    .line 478
    const/4 v10, 0x0

    #@121
    :cond_121
    move v9, v8

    #@122
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto/16 :goto_f

    #@124
    .line 476
    .end local v7           #i:I
    .end local v9           #nReturn:I
    .end local v12           #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    .restart local v8       #nReturn:I
    :catchall_124
    move-exception v0

    #@125
    if-eqz v10, :cond_12b

    #@127
    .line 477
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@12a
    .line 478
    const/4 v10, 0x0

    #@12b
    :cond_12b
    throw v0
.end method

.method public UpdateBuddyStatusIconInformation(Lcom/lge/ims/service/ip/PresenceInfo;)I
    .registers 15
    .parameter "objPresenceInfo"

    #@0
    .prologue
    .line 484
    const/4 v8, 0x0

    #@1
    .line 485
    .local v8, nReturn:I
    if-eqz p1, :cond_9

    #@3
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->IsInvalid()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 486
    :cond_9
    const-string v0, "[IP][ERROR] MSISDN is null or Empty"

    #@b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e
    move v9, v8

    #@f
    .line 533
    .end local v8           #nReturn:I
    .local v9, nReturn:I
    :goto_f
    return v9

    #@10
    .line 490
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :cond_10
    new-instance v0, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v1, "[IP]msisdn : "

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2a
    .line 492
    const-string v3, "nor_msisdn=?"

    #@2c
    .line 493
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@2d
    new-array v4, v0, [Ljava/lang/String;

    #@2f
    const/4 v0, 0x0

    #@30
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    aput-object v1, v4, v0

    #@36
    .line 494
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    const/4 v10, 0x0

    #@37
    .line 496
    .local v10, objCursor:Landroid/database/Cursor;
    :try_start_37
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@39
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3c
    move-result-object v0

    #@3d
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_PRESENCE_CONTENT_URI:Landroid/net/Uri;

    #@3f
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_PRESENCE_PROJECTION:[Ljava/lang/String;

    #@41
    const/4 v5, 0x0

    #@42
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@45
    move-result-object v10

    #@46
    .line 502
    if-nez v10, :cond_6a

    #@48
    .line 503
    new-instance v0, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v1, "[IP][ERROR]Not found : "

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/PresenceInfo;->GetMSISDN()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_62
    .catchall {:try_start_37 .. :try_end_62} :catchall_e2
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_62} :catch_7f

    #@62
    .line 528
    if-eqz v10, :cond_68

    #@64
    .line 529
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@67
    .line 530
    const/4 v10, 0x0

    #@68
    :cond_68
    move v9, v8

    #@69
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto :goto_f

    #@6a
    .line 506
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    :cond_6a
    :try_start_6a
    new-instance v12, Ljava/util/LinkedList;

    #@6c
    invoke-direct {v12}, Ljava/util/LinkedList;-><init>()V

    #@6f
    .line 507
    .local v12, objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :cond_6f
    :goto_6f
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@72
    move-result v0

    #@73
    if-eqz v0, :cond_9f

    #@75
    .line 508
    invoke-direct {p0, v10}, Lcom/lge/ims/service/ip/IPContactHelper;->GetPresenceContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@78
    move-result-object v11

    #@79
    .line 509
    .local v11, objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    if-eqz v11, :cond_6f

    #@7b
    .line 512
    invoke-virtual {v12, v11}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_7e
    .catchall {:try_start_6a .. :try_end_7e} :catchall_e2
    .catch Ljava/lang/Exception; {:try_start_6a .. :try_end_7e} :catch_7f

    #@7e
    goto :goto_6f

    #@7f
    .line 525
    .end local v11           #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    .end local v12           #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :catch_7f
    move-exception v6

    #@80
    .line 526
    .local v6, e:Ljava/lang/Exception;
    :try_start_80
    new-instance v0, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v1, "[IP][ERROR]exception : "

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v0

    #@8f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v0

    #@93
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_96
    .catchall {:try_start_80 .. :try_end_96} :catchall_e2

    #@96
    .line 528
    if-eqz v10, :cond_9c

    #@98
    .line 529
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@9b
    .line 530
    const/4 v10, 0x0

    #@9c
    :cond_9c
    move v9, v8

    #@9d
    .line 533
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto/16 :goto_f

    #@9f
    .line 516
    .end local v6           #e:Ljava/lang/Exception;
    .end local v9           #nReturn:I
    .restart local v8       #nReturn:I
    .restart local v12       #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    :cond_9f
    :try_start_9f
    new-instance v0, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v1, "[IP]list size : ["

    #@a6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v0

    #@aa
    invoke-virtual {v12}, Ljava/util/LinkedList;->size()I

    #@ad
    move-result v1

    #@ae
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v0

    #@b2
    const-string v1, "]"

    #@b4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v0

    #@b8
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bb
    move-result-object v0

    #@bc
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@bf
    .line 517
    const/4 v7, 0x0

    #@c0
    .local v7, i:I
    :goto_c0
    invoke-virtual {v12}, Ljava/util/LinkedList;->size()I

    #@c3
    move-result v0

    #@c4
    if-ge v7, v0, :cond_d5

    #@c6
    .line 518
    invoke-virtual {v12, v7}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@c9
    move-result-object v11

    #@ca
    check-cast v11, Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;

    #@cc
    .line 519
    .restart local v11       #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    invoke-direct {p0, v11, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->PresenceData_CheckStatusIconEtag(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;Lcom/lge/ims/service/ip/PresenceInfo;)Z

    #@cf
    .line 520
    invoke-direct {p0, v11, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->PresenceData_StatusIconUpdate(Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;Lcom/lge/ims/service/ip/PresenceInfo;)Z

    #@d2
    .line 517
    add-int/lit8 v7, v7, 0x1

    #@d4
    goto :goto_c0

    #@d5
    .line 522
    .end local v11           #objPresenceContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;
    :cond_d5
    invoke-virtual {v12}, Ljava/util/LinkedList;->size()I
    :try_end_d8
    .catchall {:try_start_9f .. :try_end_d8} :catchall_e2
    .catch Ljava/lang/Exception; {:try_start_9f .. :try_end_d8} :catch_7f

    #@d8
    move-result v8

    #@d9
    .line 528
    if-eqz v10, :cond_df

    #@db
    .line 529
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@de
    .line 530
    const/4 v10, 0x0

    #@df
    :cond_df
    move v9, v8

    #@e0
    .end local v8           #nReturn:I
    .restart local v9       #nReturn:I
    goto/16 :goto_f

    #@e2
    .line 528
    .end local v7           #i:I
    .end local v9           #nReturn:I
    .end local v12           #objPresenceContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPPresenceContactData;>;"
    .restart local v8       #nReturn:I
    :catchall_e2
    move-exception v0

    #@e3
    if-eqz v10, :cond_e9

    #@e5
    .line 529
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@e8
    .line 530
    const/4 v10, 0x0

    #@e9
    :cond_e9
    throw v0
.end method

.method public UpdateCapability(Lcom/lge/ims/service/ip/CapaInfo;)I
    .registers 16
    .parameter "objCapaInfo"

    #@0
    .prologue
    .line 360
    const/4 v9, 0x0

    #@1
    .line 361
    .local v9, nReturn:I
    if-eqz p1, :cond_9

    #@3
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/CapaInfo;->IsInvalid()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 362
    :cond_9
    const-string v0, "[IP][ERROR]UpdateCapability : MSISDN is null or Empty"

    #@b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e
    move v10, v9

    #@f
    .line 418
    .end local v9           #nReturn:I
    .local v10, nReturn:I
    :goto_f
    return v10

    #@10
    .line 365
    .end local v10           #nReturn:I
    .restart local v9       #nReturn:I
    :cond_10
    new-instance v0, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v1, "[IP]msisdn : "

    #@17
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v0

    #@1b
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/CapaInfo;->GetMSISDN()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2a
    .line 366
    const-string v3, "nor_msisdn=?"

    #@2c
    .line 367
    .local v3, strSelection:Ljava/lang/String;
    const/4 v0, 0x1

    #@2d
    new-array v4, v0, [Ljava/lang/String;

    #@2f
    const/4 v0, 0x0

    #@30
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/CapaInfo;->GetMSISDN()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    aput-object v1, v4, v0

    #@36
    .line 368
    .local v4, astrSelectionArgs:[Ljava/lang/String;
    const/4 v11, 0x0

    #@37
    .line 370
    .local v11, objCursor:Landroid/database/Cursor;
    :try_start_37
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@39
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3c
    move-result-object v0

    #@3d
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_CAPA_CONTENT_URI:Landroid/net/Uri;

    #@3f
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_CAPA_PROJECTION:[Ljava/lang/String;

    #@41
    const/4 v5, 0x0

    #@42
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@45
    move-result-object v11

    #@46
    .line 375
    if-nez v11, :cond_71

    #@48
    .line 376
    new-instance v0, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v1, "[IP][ERROR]UpdateCapability : Not found ["

    #@4f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v0

    #@53
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/CapaInfo;->GetMSISDN()Ljava/lang/String;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    const-string v1, "]"

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v0

    #@65
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_68
    .catchall {:try_start_37 .. :try_end_68} :catchall_d4
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_68} :catch_86

    #@68
    .line 377
    const/4 v9, 0x0

    #@69
    .line 413
    if-eqz v11, :cond_6f

    #@6b
    .line 414
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@6e
    .line 415
    const/4 v11, 0x0

    #@6f
    :cond_6f
    move v10, v9

    #@70
    .end local v9           #nReturn:I
    .restart local v10       #nReturn:I
    goto :goto_f

    #@71
    .line 380
    .end local v10           #nReturn:I
    .restart local v9       #nReturn:I
    :cond_71
    :try_start_71
    new-instance v13, Ljava/util/LinkedList;

    #@73
    invoke-direct {v13}, Ljava/util/LinkedList;-><init>()V

    #@76
    .line 381
    .local v13, objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :cond_76
    :goto_76
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    #@79
    move-result v0

    #@7a
    if-eqz v0, :cond_a6

    #@7c
    .line 382
    invoke-direct {p0, v11}, Lcom/lge/ims/service/ip/IPContactHelper;->GetRCSContactData(Landroid/database/Cursor;)Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@7f
    move-result-object v12

    #@80
    .line 383
    .local v12, objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    if-eqz v12, :cond_76

    #@82
    .line 384
    invoke-virtual {v13, v12}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_85
    .catchall {:try_start_71 .. :try_end_85} :catchall_d4
    .catch Ljava/lang/Exception; {:try_start_71 .. :try_end_85} :catch_86

    #@85
    goto :goto_76

    #@86
    .line 410
    .end local v12           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    .end local v13           #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :catch_86
    move-exception v6

    #@87
    .line 411
    .local v6, e:Ljava/lang/Exception;
    :try_start_87
    new-instance v0, Ljava/lang/StringBuilder;

    #@89
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8c
    const-string v1, "[IP][ERROR]exception : "

    #@8e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v0

    #@92
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v0

    #@96
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v0

    #@9a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_9d
    .catchall {:try_start_87 .. :try_end_9d} :catchall_d4

    #@9d
    .line 413
    if-eqz v11, :cond_a3

    #@9f
    .line 414
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@a2
    .line 415
    const/4 v11, 0x0

    #@a3
    :cond_a3
    move v10, v9

    #@a4
    .line 418
    .end local v9           #nReturn:I
    .restart local v10       #nReturn:I
    goto/16 :goto_f

    #@a6
    .line 387
    .end local v6           #e:Ljava/lang/Exception;
    .end local v10           #nReturn:I
    .restart local v9       #nReturn:I
    .restart local v13       #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    :cond_a6
    const/4 v7, 0x0

    #@a7
    .local v7, i:I
    :goto_a7
    :try_start_a7
    invoke-virtual {v13}, Ljava/util/LinkedList;->size()I

    #@aa
    move-result v0

    #@ab
    if-ge v7, v0, :cond_c7

    #@ad
    .line 388
    invoke-virtual {v13, v7}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@b0
    move-result-object v12

    #@b1
    check-cast v12, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;

    #@b3
    .line 390
    .restart local v12       #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    invoke-direct {p0, v12, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->ContactData_Update(Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;Lcom/lge/ims/service/ip/CapaInfo;)Z

    #@b6
    .line 391
    invoke-direct {p0, v12, p1}, Lcom/lge/ims/service/ip/IPContactHelper;->RCSeData_Update(Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;Lcom/lge/ims/service/ip/CapaInfo;)I

    #@b9
    move-result v8

    #@ba
    .line 393
    .local v8, nRCSResult:I
    if-eqz v8, :cond_bf

    #@bc
    const/4 v0, 0x4

    #@bd
    if-ne v8, v0, :cond_c4

    #@bf
    .line 394
    :cond_bf
    iget-object v0, v12, Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;->strMSISDN:Ljava/lang/String;

    #@c1
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ip/IPContactHelper;->PresenceData_Delete(Ljava/lang/String;)Z

    #@c4
    .line 387
    :cond_c4
    add-int/lit8 v7, v7, 0x1

    #@c6
    goto :goto_a7

    #@c7
    .line 407
    .end local v8           #nRCSResult:I
    .end local v12           #objRCSContactData:Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;
    :cond_c7
    invoke-virtual {v13}, Ljava/util/LinkedList;->size()I
    :try_end_ca
    .catchall {:try_start_a7 .. :try_end_ca} :catchall_d4
    .catch Ljava/lang/Exception; {:try_start_a7 .. :try_end_ca} :catch_86

    #@ca
    move-result v9

    #@cb
    .line 413
    if-eqz v11, :cond_d1

    #@cd
    .line 414
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@d0
    .line 415
    const/4 v11, 0x0

    #@d1
    :cond_d1
    move v10, v9

    #@d2
    .end local v9           #nReturn:I
    .restart local v10       #nReturn:I
    goto/16 :goto_f

    #@d4
    .line 413
    .end local v7           #i:I
    .end local v10           #nReturn:I
    .end local v13           #objRCSContactDataList:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/lge/ims/service/ip/IPContactHelper$IPCapaContactData;>;"
    .restart local v9       #nReturn:I
    :catchall_d4
    move-exception v0

    #@d5
    if-eqz v11, :cond_db

    #@d7
    .line 414
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@da
    .line 415
    const/4 v11, 0x0

    #@db
    :cond_db
    throw v0
.end method

.method public UpdateDocumentETag(Lcom/lge/ims/service/ip/XDMEtagInfo;)Z
    .registers 15
    .parameter "objXdmEtagInfo"

    #@0
    .prologue
    .line 759
    const/4 v6, 0x0

    #@1
    .line 760
    .local v6, bHasEtagInfoDB:Z
    const/4 v7, 0x0

    #@2
    .line 761
    .local v7, bHasEtagInfoData:Z
    const/4 v11, 0x0

    #@3
    .line 762
    .local v11, objCursor:Landroid/database/Cursor;
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v1, "[IP]"

    #@a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->toString()Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v0

    #@16
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1d
    .line 763
    new-instance v10, Landroid/content/ContentValues;

    #@1f
    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    #@22
    .line 765
    .local v10, objContentValues:Landroid/content/ContentValues;
    :try_start_22
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@24
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@27
    move-result-object v0

    #@28
    sget-object v1, Lcom/lge/ims/service/ip/XDMConstant;->XDM_CONTENT_URI:Landroid/net/Uri;

    #@2a
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->XDM_FULL_ETAG_PROJECTION:[Ljava/lang/String;

    #@2c
    const/4 v3, 0x0

    #@2d
    const/4 v4, 0x0

    #@2e
    const/4 v5, 0x0

    #@2f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_32
    .catchall {:try_start_22 .. :try_end_32} :catchall_c4
    .catch Ljava/lang/Exception; {:try_start_22 .. :try_end_32} :catch_a5

    #@32
    move-result-object v11

    #@33
    .line 771
    if-nez v11, :cond_9b

    #@35
    .line 772
    const/4 v6, 0x0

    #@36
    .line 781
    :goto_36
    if-eqz v11, :cond_3c

    #@38
    .line 782
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@3b
    .line 783
    const/4 v11, 0x0

    #@3c
    .line 786
    :cond_3c
    :goto_3c
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetListEtag()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    if-eqz v0, :cond_4c

    #@42
    .line 787
    const/4 v7, 0x1

    #@43
    .line 788
    const-string v0, "list_etag"

    #@45
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetListEtag()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4c
    .line 790
    :cond_4c
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRLSEtag()Ljava/lang/String;

    #@4f
    move-result-object v0

    #@50
    if-eqz v0, :cond_5c

    #@52
    .line 791
    const/4 v7, 0x1

    #@53
    .line 792
    const-string v0, "rls_etag"

    #@55
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRLSEtag()Ljava/lang/String;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@5c
    .line 794
    :cond_5c
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRuleEtag()Ljava/lang/String;

    #@5f
    move-result-object v0

    #@60
    if-eqz v0, :cond_6c

    #@62
    .line 795
    const/4 v7, 0x1

    #@63
    .line 796
    const-string v0, "rule_etag"

    #@65
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetRuleEtag()Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6c
    .line 798
    :cond_6c
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetPidfEtag()Ljava/lang/String;

    #@6f
    move-result-object v0

    #@70
    if-eqz v0, :cond_7c

    #@72
    .line 799
    const/4 v7, 0x1

    #@73
    .line 800
    const-string v0, "pidf_etag"

    #@75
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/XDMEtagInfo;->GetPidfEtag()Ljava/lang/String;

    #@78
    move-result-object v1

    #@79
    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7c
    .line 802
    :cond_7c
    if-eqz v6, :cond_cc

    #@7e
    .line 803
    const/4 v0, 0x1

    #@7f
    if-ne v7, v0, :cond_e8

    #@81
    .line 804
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@83
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@86
    move-result-object v0

    #@87
    sget-object v1, Lcom/lge/ims/service/ip/XDMConstant;->XDM_CONTENT_URI:Landroid/net/Uri;

    #@89
    const/4 v2, 0x0

    #@8a
    const/4 v3, 0x0

    #@8b
    invoke-virtual {v0, v1, v10, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@8e
    move-result v9

    #@8f
    .line 805
    .local v9, nCount:I
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    #@92
    .line 806
    if-nez v9, :cond_e8

    #@94
    .line 807
    const-string v0, "[IP][ERROR]XDM etag update fail"

    #@96
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@99
    .line 808
    const/4 v0, 0x0

    #@9a
    .line 822
    .end local v9           #nCount:I
    :goto_9a
    return v0

    #@9b
    .line 773
    :cond_9b
    :try_start_9b
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_9e
    .catchall {:try_start_9b .. :try_end_9e} :catchall_c4
    .catch Ljava/lang/Exception; {:try_start_9b .. :try_end_9e} :catch_a5

    #@9e
    move-result v0

    #@9f
    if-nez v0, :cond_a3

    #@a1
    .line 774
    const/4 v6, 0x0

    #@a2
    goto :goto_36

    #@a3
    .line 776
    :cond_a3
    const/4 v6, 0x1

    #@a4
    goto :goto_36

    #@a5
    .line 778
    :catch_a5
    move-exception v8

    #@a6
    .line 779
    .local v8, e:Ljava/lang/Exception;
    :try_start_a6
    new-instance v0, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v1, "[IP][ERROR]exception : "

    #@ad
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v0

    #@b1
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v0

    #@b5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v0

    #@b9
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_bc
    .catchall {:try_start_a6 .. :try_end_bc} :catchall_c4

    #@bc
    .line 781
    if-eqz v11, :cond_3c

    #@be
    .line 782
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@c1
    .line 783
    const/4 v11, 0x0

    #@c2
    goto/16 :goto_3c

    #@c4
    .line 781
    .end local v8           #e:Ljava/lang/Exception;
    :catchall_c4
    move-exception v0

    #@c5
    if-eqz v11, :cond_cb

    #@c7
    .line 782
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@ca
    .line 783
    const/4 v11, 0x0

    #@cb
    :cond_cb
    throw v0

    #@cc
    .line 812
    :cond_cc
    const/4 v0, 0x1

    #@cd
    if-ne v7, v0, :cond_e8

    #@cf
    .line 813
    const/4 v12, 0x0

    #@d0
    .line 814
    .local v12, obxdmjUri:Landroid/net/Uri;
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@d2
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d5
    move-result-object v0

    #@d6
    sget-object v1, Lcom/lge/ims/service/ip/XDMConstant;->XDM_CONTENT_URI:Landroid/net/Uri;

    #@d8
    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@db
    move-result-object v12

    #@dc
    .line 815
    invoke-virtual {v10}, Landroid/content/ContentValues;->clear()V

    #@df
    .line 816
    if-nez v12, :cond_e8

    #@e1
    .line 817
    const-string v0, "[IP][ERROR]XDM etag insert fail"

    #@e3
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@e6
    .line 818
    const/4 v0, 0x0

    #@e7
    goto :goto_9a

    #@e8
    .line 822
    .end local v12           #obxdmjUri:Landroid/net/Uri;
    :cond_e8
    const/4 v0, 0x1

    #@e9
    goto :goto_9a
.end method

.method public UpdateMyStatusIconInfo(Lcom/lge/ims/service/ip/MyStatusInfo;)Z
    .registers 21
    .parameter "objMyStatusInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 681
    const/4 v7, 0x0

    #@1
    .line 682
    .local v7, bIsExitStatusInfo:Z
    const/4 v14, 0x0

    #@2
    .line 683
    .local v14, objCursor:Landroid/database/Cursor;
    new-instance v13, Landroid/content/ContentValues;

    #@4
    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    #@7
    .line 685
    .local v13, objContentValues:Landroid/content/ContentValues;
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIcon()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    if-eqz v1, :cond_16

    #@d
    .line 686
    const-string v1, "status_icon"

    #@f
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIcon()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 688
    :cond_16
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    if-eqz v1, :cond_45

    #@1c
    .line 689
    const-string v1, "status_icon_link"

    #@1e
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 690
    new-instance v1, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v2, "[IP]GetStatusIconLink["

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    const-string v2, "]"

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@45
    .line 692
    :cond_45
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    if-eqz v1, :cond_b0

    #@4b
    .line 693
    new-instance v1, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v2, "[IP]Thumbnail path : "

    #@52
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v1

    #@56
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v1

    #@62
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@65
    .line 694
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@68
    move-result-object v1

    #@69
    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    #@6c
    move-result-object v8

    #@6d
    .line 695
    .local v8, bit:Landroid/graphics/Bitmap;
    if-eqz v8, :cond_a2

    #@6f
    .line 696
    const/16 v1, 0x60

    #@71
    const/16 v2, 0x60

    #@73
    const/4 v3, 0x1

    #@74
    invoke-static {v8, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    #@77
    move-result-object v15

    #@78
    .line 697
    .local v15, resized:Landroid/graphics/Bitmap;
    new-instance v16, Ljava/io/ByteArrayOutputStream;

    #@7a
    invoke-direct/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@7d
    .line 698
    .local v16, stream:Ljava/io/ByteArrayOutputStream;
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    #@7f
    const/16 v2, 0x5f

    #@81
    move-object/from16 v0, v16

    #@83
    invoke-virtual {v15, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    #@86
    move-result v9

    #@87
    .line 699
    .local v9, compressed:Z
    if-nez v9, :cond_91

    #@89
    .line 700
    new-instance v1, Ljava/io/IOException;

    #@8b
    const-string v2, "Unable to compress image"

    #@8d
    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    #@90
    throw v1

    #@91
    .line 702
    :cond_91
    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->flush()V

    #@94
    .line 703
    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->close()V

    #@97
    .line 704
    invoke-virtual/range {v16 .. v16}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@9a
    move-result-object v18

    #@9b
    .line 705
    .local v18, value:[B
    const-string v1, "status_icon_thumb"

    #@9d
    move-object/from16 v0, v18

    #@9f
    invoke-virtual {v13, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    #@a2
    .line 707
    .end local v9           #compressed:Z
    .end local v15           #resized:Landroid/graphics/Bitmap;
    .end local v16           #stream:Ljava/io/ByteArrayOutputStream;
    .end local v18           #value:[B
    :cond_a2
    new-instance v17, Ljava/io/File;

    #@a4
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumb()Ljava/lang/String;

    #@a7
    move-result-object v1

    #@a8
    move-object/from16 v0, v17

    #@aa
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@ad
    .line 708
    .local v17, tempFile:Ljava/io/File;
    invoke-virtual/range {v17 .. v17}, Ljava/io/File;->delete()Z

    #@b0
    .line 710
    .end local v8           #bit:Landroid/graphics/Bitmap;
    .end local v17           #tempFile:Ljava/io/File;
    :cond_b0
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@b3
    move-result-object v1

    #@b4
    if-eqz v1, :cond_df

    #@b6
    .line 711
    const-string v1, "status_icon_thumb_link"

    #@b8
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@bb
    move-result-object v2

    #@bc
    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@bf
    .line 712
    new-instance v1, Ljava/lang/StringBuilder;

    #@c1
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c4
    const-string v2, "[IP]GetStatusIconThumbLink["

    #@c6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v1

    #@ca
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@cd
    move-result-object v2

    #@ce
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v1

    #@d2
    const-string v2, "]"

    #@d4
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v1

    #@d8
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v1

    #@dc
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@df
    .line 714
    :cond_df
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@e2
    move-result-object v1

    #@e3
    if-eqz v1, :cond_10e

    #@e5
    .line 715
    const-string v1, "status_icon_thumb_etag"

    #@e7
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@ea
    move-result-object v2

    #@eb
    invoke-virtual {v13, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@ee
    .line 716
    new-instance v1, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string v2, "[IP]GetStatusIconThumbEtag["

    #@f5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v1

    #@f9
    invoke-virtual/range {p1 .. p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@fc
    move-result-object v2

    #@fd
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v1

    #@101
    const-string v2, "]"

    #@103
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v1

    #@107
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v1

    #@10b
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@10e
    .line 719
    :cond_10e
    :try_start_10e
    move-object/from16 v0, p0

    #@110
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@112
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@115
    move-result-object v1

    #@116
    sget-object v2, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_CONTENT_URI:Landroid/net/Uri;

    #@118
    sget-object v3, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_MYSTATUS_PROJECTION:[Ljava/lang/String;

    #@11a
    const/4 v4, 0x0

    #@11b
    const/4 v5, 0x0

    #@11c
    const/4 v6, 0x0

    #@11d
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_120
    .catchall {:try_start_10e .. :try_end_120} :catchall_170
    .catch Ljava/lang/Exception; {:try_start_10e .. :try_end_120} :catch_152

    #@120
    move-result-object v14

    #@121
    .line 725
    if-nez v14, :cond_148

    #@123
    .line 726
    const/4 v7, 0x0

    #@124
    .line 735
    :goto_124
    if-eqz v14, :cond_12a

    #@126
    .line 736
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@129
    .line 737
    const/4 v14, 0x0

    #@12a
    .line 740
    :cond_12a
    :goto_12a
    if-eqz v7, :cond_178

    #@12c
    .line 741
    move-object/from16 v0, p0

    #@12e
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@130
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@133
    move-result-object v1

    #@134
    sget-object v2, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_SERVER_CONTENT_URI:Landroid/net/Uri;

    #@136
    const/4 v3, 0x0

    #@137
    const/4 v4, 0x0

    #@138
    invoke-virtual {v1, v2, v13, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@13b
    move-result v11

    #@13c
    .line 742
    .local v11, nCount:I
    invoke-virtual {v13}, Landroid/content/ContentValues;->clear()V

    #@13f
    .line 743
    if-nez v11, :cond_193

    #@141
    .line 744
    const-string v1, "[IP][ERROR]MY Status DB update fail"

    #@143
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@146
    .line 745
    const/4 v1, 0x0

    #@147
    .line 756
    .end local v11           #nCount:I
    :goto_147
    return v1

    #@148
    .line 727
    :cond_148
    :try_start_148
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_14b
    .catchall {:try_start_148 .. :try_end_14b} :catchall_170
    .catch Ljava/lang/Exception; {:try_start_148 .. :try_end_14b} :catch_152

    #@14b
    move-result v1

    #@14c
    if-nez v1, :cond_150

    #@14e
    .line 728
    const/4 v7, 0x0

    #@14f
    goto :goto_124

    #@150
    .line 730
    :cond_150
    const/4 v7, 0x1

    #@151
    goto :goto_124

    #@152
    .line 732
    :catch_152
    move-exception v10

    #@153
    .line 733
    .local v10, e:Ljava/lang/Exception;
    :try_start_153
    new-instance v1, Ljava/lang/StringBuilder;

    #@155
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@158
    const-string v2, "[IP][ERROR]exception : "

    #@15a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v1

    #@15e
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v1

    #@162
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@165
    move-result-object v1

    #@166
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_169
    .catchall {:try_start_153 .. :try_end_169} :catchall_170

    #@169
    .line 735
    if-eqz v14, :cond_12a

    #@16b
    .line 736
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@16e
    .line 737
    const/4 v14, 0x0

    #@16f
    goto :goto_12a

    #@170
    .line 735
    .end local v10           #e:Ljava/lang/Exception;
    :catchall_170
    move-exception v1

    #@171
    if-eqz v14, :cond_177

    #@173
    .line 736
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    #@176
    .line 737
    const/4 v14, 0x0

    #@177
    :cond_177
    throw v1

    #@178
    .line 748
    :cond_178
    const/4 v12, 0x0

    #@179
    .line 749
    .local v12, obUri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@17b
    iget-object v1, v0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@17d
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@180
    move-result-object v1

    #@181
    sget-object v2, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_SERVER_CONTENT_URI:Landroid/net/Uri;

    #@183
    invoke-virtual {v1, v2, v13}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@186
    move-result-object v12

    #@187
    .line 750
    invoke-virtual {v13}, Landroid/content/ContentValues;->clear()V

    #@18a
    .line 751
    if-nez v12, :cond_193

    #@18c
    .line 752
    const-string v1, "[IP][ERROR]MY Status DB insert fail"

    #@18e
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@191
    .line 753
    const/4 v1, 0x0

    #@192
    goto :goto_147

    #@193
    .line 756
    .end local v12           #obUri:Landroid/net/Uri;
    :cond_193
    const/4 v1, 0x1

    #@194
    goto :goto_147
.end method

.method public UpdateMyStatusInfo(Lcom/lge/ims/service/ip/MyStatusInfo;)Z
    .registers 16
    .parameter "objMyStatusInfo"

    #@0
    .prologue
    .line 568
    const/4 v6, 0x0

    #@1
    .line 569
    .local v6, bIsExitStatusInfo:Z
    const/4 v12, 0x0

    #@2
    .line 570
    .local v12, objCursor:Landroid/database/Cursor;
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "[IP]"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->toString()Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1c
    .line 571
    new-instance v11, Landroid/content/ContentValues;

    #@1e
    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    #@21
    .line 572
    .local v11, objContentValues:Landroid/content/ContentValues;
    const-string v0, "homepage"

    #@23
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetHomePage()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    .line 573
    const-string v0, "free_text"

    #@2c
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetFreeText()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 574
    const-string v0, "e_mail"

    #@35
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetEMailAddress()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    .line 575
    const-string v0, "birthday"

    #@3e
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetBirthDay()Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@45
    .line 576
    const-string v0, "status"

    #@47
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatus()I

    #@4a
    move-result v1

    #@4b
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@52
    .line 577
    const-string v0, "twitter_account"

    #@54
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetTwitterAccount()Ljava/lang/String;

    #@57
    move-result-object v1

    #@58
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    .line 578
    const-string v0, "facebook_account"

    #@5d
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetFaceBookAccount()Ljava/lang/String;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@64
    .line 579
    const-string v0, "cyworld_account"

    #@66
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetCyworldAccount()Ljava/lang/String;

    #@69
    move-result-object v1

    #@6a
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 580
    const-string v0, "waggle_account"

    #@6f
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetWaggleAccount()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@76
    .line 581
    const-string v0, "display_name"

    #@78
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetDisplayName()Ljava/lang/String;

    #@7b
    move-result-object v1

    #@7c
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@7f
    .line 582
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@82
    move-result-object v0

    #@83
    if-eqz v0, :cond_dc

    #@85
    .line 583
    const-string v0, "status_icon_link"

    #@87
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@8a
    move-result-object v1

    #@8b
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@8e
    .line 589
    :goto_8e
    const-string v0, "status_icon_thumb_etag"

    #@90
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbEtag()Ljava/lang/String;

    #@93
    move-result-object v1

    #@94
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@97
    .line 590
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@9a
    move-result-object v0

    #@9b
    if-eqz v0, :cond_f2

    #@9d
    .line 591
    const-string v0, "status_icon_thumb_link"

    #@9f
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@a2
    move-result-object v1

    #@a3
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@a6
    .line 599
    :goto_a6
    :try_start_a6
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@a8
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@ab
    move-result-object v0

    #@ac
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_CONTENT_URI:Landroid/net/Uri;

    #@ae
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->IP_FULL_MYSTATUS_PROJECTION:[Ljava/lang/String;

    #@b0
    const/4 v3, 0x0

    #@b1
    const/4 v4, 0x0

    #@b2
    const/4 v5, 0x0

    #@b3
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_b6
    .catchall {:try_start_a6 .. :try_end_b6} :catchall_138
    .catch Ljava/lang/Exception; {:try_start_a6 .. :try_end_b6} :catch_11a

    #@b6
    move-result-object v12

    #@b7
    .line 605
    if-nez v12, :cond_110

    #@b9
    .line 606
    const/4 v6, 0x0

    #@ba
    .line 615
    :goto_ba
    if-eqz v12, :cond_c0

    #@bc
    .line 616
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@bf
    .line 617
    const/4 v12, 0x0

    #@c0
    .line 620
    :cond_c0
    :goto_c0
    if-eqz v6, :cond_1ad

    #@c2
    .line 621
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@c4
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c7
    move-result-object v0

    #@c8
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_SERVER_CONTENT_URI:Landroid/net/Uri;

    #@ca
    const/4 v2, 0x0

    #@cb
    const/4 v3, 0x0

    #@cc
    invoke-virtual {v0, v1, v11, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@cf
    move-result v8

    #@d0
    .line 622
    .local v8, nCount:I
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    #@d3
    .line 623
    if-nez v8, :cond_140

    #@d5
    .line 624
    const-string v0, "[IP][ERROR]MY Status DB update fail"

    #@d7
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@da
    .line 625
    const/4 v0, 0x0

    #@db
    .line 678
    .end local v8           #nCount:I
    :goto_db
    return v0

    #@dc
    .line 585
    :cond_dc
    const-string v0, "[IP]GetStatusIconLink is null. set status icon, link is null"

    #@de
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@e1
    .line 586
    const-string v1, "status_icon"

    #@e3
    const/4 v0, 0x0

    #@e4
    check-cast v0, Ljava/lang/String;

    #@e6
    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@e9
    .line 587
    const-string v1, "status_icon_link"

    #@eb
    const/4 v0, 0x0

    #@ec
    check-cast v0, Ljava/lang/String;

    #@ee
    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@f1
    goto :goto_8e

    #@f2
    .line 593
    :cond_f2
    const-string v0, "[IP]GetStatusIconThumbLink is null. set status thumb, thumb etag, thumb link is null"

    #@f4
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@f7
    .line 594
    const-string v1, "status_icon_thumb"

    #@f9
    const/4 v0, 0x0

    #@fa
    check-cast v0, Ljava/lang/Byte;

    #@fc
    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Byte;)V

    #@ff
    .line 595
    const-string v1, "status_icon_thumb_etag"

    #@101
    const/4 v0, 0x0

    #@102
    check-cast v0, Ljava/lang/String;

    #@104
    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@107
    .line 596
    const-string v1, "status_icon_thumb_link"

    #@109
    const/4 v0, 0x0

    #@10a
    check-cast v0, Ljava/lang/String;

    #@10c
    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@10f
    goto :goto_a6

    #@110
    .line 607
    :cond_110
    :try_start_110
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_113
    .catchall {:try_start_110 .. :try_end_113} :catchall_138
    .catch Ljava/lang/Exception; {:try_start_110 .. :try_end_113} :catch_11a

    #@113
    move-result v0

    #@114
    if-nez v0, :cond_118

    #@116
    .line 608
    const/4 v6, 0x0

    #@117
    goto :goto_ba

    #@118
    .line 610
    :cond_118
    const/4 v6, 0x1

    #@119
    goto :goto_ba

    #@11a
    .line 612
    :catch_11a
    move-exception v7

    #@11b
    .line 613
    .local v7, e:Ljava/lang/Exception;
    :try_start_11b
    new-instance v0, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    const-string v1, "[IP][ERROR]exception : "

    #@122
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v0

    #@126
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v0

    #@12a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v0

    #@12e
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_131
    .catchall {:try_start_11b .. :try_end_131} :catchall_138

    #@131
    .line 615
    if-eqz v12, :cond_c0

    #@133
    .line 616
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@136
    .line 617
    const/4 v12, 0x0

    #@137
    goto :goto_c0

    #@138
    .line 615
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_138
    move-exception v0

    #@139
    if-eqz v12, :cond_13f

    #@13b
    .line 616
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@13e
    .line 617
    const/4 v12, 0x0

    #@13f
    :cond_13f
    throw v0

    #@140
    .line 627
    .restart local v8       #nCount:I
    :cond_140
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@143
    move-result-object v0

    #@144
    if-eqz v0, :cond_1aa

    #@146
    .line 628
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@149
    move-result-object v0

    #@14a
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@14d
    move-result-object v0

    #@14e
    if-eqz v0, :cond_1aa

    #@150
    .line 629
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@153
    move-result-object v0

    #@154
    if-eqz v0, :cond_17d

    #@156
    .line 630
    new-instance v13, Landroid/os/Message;

    #@158
    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    #@15b
    .line 631
    .local v13, objMessage:Landroid/os/Message;
    new-instance v10, Landroid/os/Bundle;

    #@15d
    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    #@160
    .line 632
    .local v10, objBundle:Landroid/os/Bundle;
    const/16 v0, 0x19

    #@162
    iput v0, v13, Landroid/os/Message;->what:I

    #@164
    .line 633
    const-string v0, "statusIconThumbUrl"

    #@166
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@169
    move-result-object v1

    #@16a
    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@16d
    .line 634
    invoke-virtual {v13, v10}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@170
    .line 635
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@173
    move-result-object v0

    #@174
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@177
    move-result-object v0

    #@178
    const-wide/16 v1, 0x3e8

    #@17a
    invoke-virtual {v0, v13, v1, v2}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@17d
    .line 637
    .end local v10           #objBundle:Landroid/os/Bundle;
    .end local v13           #objMessage:Landroid/os/Message;
    :cond_17d
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@180
    move-result-object v0

    #@181
    if-eqz v0, :cond_1aa

    #@183
    .line 638
    new-instance v13, Landroid/os/Message;

    #@185
    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    #@188
    .line 639
    .restart local v13       #objMessage:Landroid/os/Message;
    new-instance v10, Landroid/os/Bundle;

    #@18a
    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    #@18d
    .line 640
    .restart local v10       #objBundle:Landroid/os/Bundle;
    const/16 v0, 0x1a

    #@18f
    iput v0, v13, Landroid/os/Message;->what:I

    #@191
    .line 641
    const-string v0, "statusIconUrl"

    #@193
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@196
    move-result-object v1

    #@197
    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@19a
    .line 642
    invoke-virtual {v13, v10}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@19d
    .line 643
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1a0
    move-result-object v0

    #@1a1
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@1a4
    move-result-object v0

    #@1a5
    const-wide/16 v1, 0x3e8

    #@1a7
    invoke-virtual {v0, v13, v1, v2}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@1aa
    .line 678
    .end local v8           #nCount:I
    .end local v10           #objBundle:Landroid/os/Bundle;
    .end local v13           #objMessage:Landroid/os/Message;
    :cond_1aa
    :goto_1aa
    const/4 v0, 0x1

    #@1ab
    goto/16 :goto_db

    #@1ad
    .line 649
    :cond_1ad
    const/4 v9, 0x0

    #@1ae
    .line 650
    .local v9, obUri:Landroid/net/Uri;
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@1b0
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b3
    move-result-object v0

    #@1b4
    sget-object v1, Lcom/lge/ims/service/ip/IPConstant;->IP_MYSTAUS_SERVER_CONTENT_URI:Landroid/net/Uri;

    #@1b6
    invoke-virtual {v0, v1, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@1b9
    move-result-object v9

    #@1ba
    .line 651
    invoke-virtual {v11}, Landroid/content/ContentValues;->clear()V

    #@1bd
    .line 652
    if-nez v9, :cond_1c7

    #@1bf
    .line 653
    const-string v0, "[IP][ERROR]MY Status DB insert fail"

    #@1c1
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@1c4
    .line 654
    const/4 v0, 0x0

    #@1c5
    goto/16 :goto_db

    #@1c7
    .line 656
    :cond_1c7
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1ca
    move-result-object v0

    #@1cb
    if-eqz v0, :cond_1aa

    #@1cd
    .line 657
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1d0
    move-result-object v0

    #@1d1
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@1d4
    move-result-object v0

    #@1d5
    if-eqz v0, :cond_1aa

    #@1d7
    .line 658
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@1da
    move-result-object v0

    #@1db
    if-eqz v0, :cond_204

    #@1dd
    .line 659
    new-instance v13, Landroid/os/Message;

    #@1df
    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    #@1e2
    .line 660
    .restart local v13       #objMessage:Landroid/os/Message;
    new-instance v10, Landroid/os/Bundle;

    #@1e4
    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    #@1e7
    .line 661
    .restart local v10       #objBundle:Landroid/os/Bundle;
    const/16 v0, 0x19

    #@1e9
    iput v0, v13, Landroid/os/Message;->what:I

    #@1eb
    .line 662
    const-string v0, "statusIconThumbUrl"

    #@1ed
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconThumbLink()Ljava/lang/String;

    #@1f0
    move-result-object v1

    #@1f1
    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1f4
    .line 663
    invoke-virtual {v13, v10}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@1f7
    .line 664
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@1fa
    move-result-object v0

    #@1fb
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@1fe
    move-result-object v0

    #@1ff
    const-wide/16 v1, 0x3e8

    #@201
    invoke-virtual {v0, v13, v1, v2}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@204
    .line 666
    .end local v10           #objBundle:Landroid/os/Bundle;
    .end local v13           #objMessage:Landroid/os/Message;
    :cond_204
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@207
    move-result-object v0

    #@208
    if-eqz v0, :cond_1aa

    #@20a
    .line 667
    new-instance v13, Landroid/os/Message;

    #@20c
    invoke-direct {v13}, Landroid/os/Message;-><init>()V

    #@20f
    .line 668
    .restart local v13       #objMessage:Landroid/os/Message;
    new-instance v10, Landroid/os/Bundle;

    #@211
    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    #@214
    .line 669
    .restart local v10       #objBundle:Landroid/os/Bundle;
    const/16 v0, 0x1a

    #@216
    iput v0, v13, Landroid/os/Message;->what:I

    #@218
    .line 670
    const-string v0, "statusIconUrl"

    #@21a
    invoke-virtual {p1}, Lcom/lge/ims/service/ip/MyStatusInfo;->GetStatusIconLink()Ljava/lang/String;

    #@21d
    move-result-object v1

    #@21e
    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@221
    .line 671
    invoke-virtual {v13, v10}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@224
    .line 672
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@227
    move-result-object v0

    #@228
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@22b
    move-result-object v0

    #@22c
    const-wide/16 v1, 0x3e8

    #@22e
    invoke-virtual {v0, v13, v1, v2}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@231
    goto/16 :goto_1aa
.end method

.method public UpdateOneContactQueryFailed(Ljava/lang/String;I)Z
    .registers 6
    .parameter "szMsisdn"
    .parameter "nResponseCode"

    #@0
    .prologue
    .line 871
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_f

    #@8
    .line 872
    :cond_8
    const-string v1, "[IP][ERROR]UpdateOneContactQueryFailed : MSISDN is null or Empty"

    #@a
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d
    .line 873
    const/4 v1, 0x0

    #@e
    .line 880
    :goto_e
    return v1

    #@f
    .line 875
    :cond_f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "[IP]msisdn : ["

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    const-string v2, "], ResponseCode [ "

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    const-string v2, " ]"

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@35
    .line 876
    new-instance v0, Lcom/lge/ims/service/ip/CapaInfo;

    #@37
    invoke-direct {v0}, Lcom/lge/ims/service/ip/CapaInfo;-><init>()V

    #@3a
    .line 877
    .local v0, objCapaInfo:Lcom/lge/ims/service/ip/CapaInfo;
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/ip/CapaInfo;->SetMSISDN(Ljava/lang/String;)V

    #@3d
    .line 878
    invoke-virtual {v0, p2}, Lcom/lge/ims/service/ip/CapaInfo;->SetResponseCode(I)V

    #@40
    .line 879
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/ip/IPContactHelper;->UpdateCapability(Lcom/lge/ims/service/ip/CapaInfo;)I

    #@43
    .line 880
    const/4 v1, 0x1

    #@44
    goto :goto_e
.end method

.method public UpdateOneContactQuerySuccess(Ljava/lang/String;)Z
    .registers 4
    .parameter "szMsisdn"

    #@0
    .prologue
    .line 883
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 884
    :cond_8
    const-string v0, "[IP][ERROR]UpdateOneContactQuerySuccess : MSISDN is null or Empty"

    #@a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@d
    .line 885
    const/4 v0, 0x0

    #@e
    .line 888
    :goto_e
    return v0

    #@f
    .line 887
    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v1, "[IP]msisdn : ["

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v0

    #@1a
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v0

    #@1e
    const-string v1, "]"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v0

    #@24
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2b
    .line 888
    const/4 v0, 0x1

    #@2c
    goto :goto_e
.end method

.method public UpdatePollingLatestTime()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 536
    const-string v2, "[IP]"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 537
    new-instance v1, Landroid/content/ContentValues;

    #@8
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@b
    .line 538
    .local v1, objContentValues:Landroid/content/ContentValues;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@e
    move-result-wide v2

    #@f
    sput-wide v2, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J

    #@11
    .line 540
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "[IP]nPollingLatestTime ["

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    sget-wide v3, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J

    #@1e
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, "]"

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2f
    .line 541
    const-string v2, "latest_polling"

    #@31
    sget-wide v3, Lcom/lge/ims/service/ip/IPContactHelper;->nPollingLatestTime:J

    #@33
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@3a
    .line 543
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@3c
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3f
    move-result-object v2

    #@40
    sget-object v3, Lcom/lge/ims/service/ip/IPConstant;->IP_PROVISIONING_CONTENT_URI:Landroid/net/Uri;

    #@42
    invoke-virtual {v2, v3, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@45
    move-result v0

    #@46
    .line 544
    .local v0, nCount:I
    if-nez v0, :cond_4f

    #@48
    .line 545
    const-string v2, "[IP][ERROR]UpdatePollingLatestTime - nCount is 0"

    #@4a
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@4d
    .line 546
    const/4 v2, 0x0

    #@4e
    .line 549
    :goto_4e
    return v2

    #@4f
    .line 548
    :cond_4f
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@52
    .line 549
    const/4 v2, 0x1

    #@53
    goto :goto_4e
.end method

.method public UpdateRLSListUri(Ljava/lang/String;)Z
    .registers 16
    .parameter "szRlsListUri"

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v13, 0x0

    #@2
    .line 825
    const/4 v6, 0x0

    #@3
    .line 826
    .local v6, bIsExitEtagInfo:Z
    const/4 v10, 0x0

    #@4
    .line 827
    .local v10, objCursor:Landroid/database/Cursor;
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v1, "[IP]: "

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1a
    .line 828
    new-instance v9, Landroid/content/ContentValues;

    #@1c
    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    #@1f
    .line 830
    .local v9, objContentValues:Landroid/content/ContentValues;
    :try_start_1f
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@21
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v0

    #@25
    sget-object v1, Lcom/lge/ims/service/ip/XDMConstant;->XDM_CONTENT_URI:Landroid/net/Uri;

    #@27
    sget-object v2, Lcom/lge/ims/service/ip/IPContactHelper;->XDM_FULL_ETAG_PROJECTION:[Ljava/lang/String;

    #@29
    const/4 v3, 0x0

    #@2a
    const/4 v4, 0x0

    #@2b
    const/4 v5, 0x0

    #@2c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2f
    .catchall {:try_start_1f .. :try_end_2f} :catchall_80
    .catch Ljava/lang/Exception; {:try_start_1f .. :try_end_2f} :catch_62

    #@2f
    move-result-object v10

    #@30
    .line 836
    if-nez v10, :cond_58

    #@32
    .line 837
    const/4 v6, 0x0

    #@33
    .line 846
    :goto_33
    if-eqz v10, :cond_39

    #@35
    .line 847
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@38
    .line 848
    const/4 v10, 0x0

    #@39
    .line 851
    :cond_39
    :goto_39
    const-string v0, "rls_uri"

    #@3b
    invoke-virtual {v9, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3e
    .line 852
    if-eqz v6, :cond_88

    #@40
    .line 853
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@42
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@45
    move-result-object v0

    #@46
    sget-object v1, Lcom/lge/ims/service/ip/XDMConstant;->XDM_CONTENT_URI:Landroid/net/Uri;

    #@48
    invoke-virtual {v0, v1, v9, v13, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@4b
    move-result v8

    #@4c
    .line 854
    .local v8, nCount:I
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    #@4f
    .line 855
    if-nez v8, :cond_a1

    #@51
    .line 856
    const-string v0, "[IP][ERROR]XDM RLS list uri update fail"

    #@53
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@56
    move v0, v12

    #@57
    .line 868
    .end local v8           #nCount:I
    :goto_57
    return v0

    #@58
    .line 838
    :cond_58
    :try_start_58
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_5b
    .catchall {:try_start_58 .. :try_end_5b} :catchall_80
    .catch Ljava/lang/Exception; {:try_start_58 .. :try_end_5b} :catch_62

    #@5b
    move-result v0

    #@5c
    if-nez v0, :cond_60

    #@5e
    .line 839
    const/4 v6, 0x0

    #@5f
    goto :goto_33

    #@60
    .line 841
    :cond_60
    const/4 v6, 0x1

    #@61
    goto :goto_33

    #@62
    .line 843
    :catch_62
    move-exception v7

    #@63
    .line 844
    .local v7, e:Ljava/lang/Exception;
    :try_start_63
    new-instance v0, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v1, "[IP][ERROR]exception : "

    #@6a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v0

    #@6e
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v0

    #@72
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V
    :try_end_79
    .catchall {:try_start_63 .. :try_end_79} :catchall_80

    #@79
    .line 846
    if-eqz v10, :cond_39

    #@7b
    .line 847
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@7e
    .line 848
    const/4 v10, 0x0

    #@7f
    goto :goto_39

    #@80
    .line 846
    .end local v7           #e:Ljava/lang/Exception;
    :catchall_80
    move-exception v0

    #@81
    if-eqz v10, :cond_87

    #@83
    .line 847
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@86
    .line 848
    const/4 v10, 0x0

    #@87
    :cond_87
    throw v0

    #@88
    .line 860
    :cond_88
    const/4 v11, 0x0

    #@89
    .line 861
    .local v11, obxdmjUri:Landroid/net/Uri;
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@8b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8e
    move-result-object v0

    #@8f
    sget-object v1, Lcom/lge/ims/service/ip/XDMConstant;->XDM_CONTENT_URI:Landroid/net/Uri;

    #@91
    invoke-virtual {v0, v1, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@94
    move-result-object v11

    #@95
    .line 862
    invoke-virtual {v9}, Landroid/content/ContentValues;->clear()V

    #@98
    .line 863
    if-nez v11, :cond_a1

    #@9a
    .line 864
    const-string v0, "[IP][ERROR]XDM RLS list uri insert fail"

    #@9c
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@9f
    move v0, v12

    #@a0
    .line 865
    goto :goto_57

    #@a1
    .line 868
    .end local v11           #obxdmjUri:Landroid/net/Uri;
    :cond_a1
    const/4 v0, 0x1

    #@a2
    goto :goto_57
.end method

.method public UpdateRLSSubPollingLatestTime()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 552
    const-string v2, "[IP]"

    #@3
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 553
    new-instance v1, Landroid/content/ContentValues;

    #@8
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@b
    .line 554
    .local v1, objContentValues:Landroid/content/ContentValues;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@e
    move-result-wide v2

    #@f
    sput-wide v2, Lcom/lge/ims/service/ip/IPContactHelper;->nSubRLSPollingLatestTime:J

    #@11
    .line 556
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "[IP]nSubRLSPollingLatestTime ["

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    sget-wide v3, Lcom/lge/ims/service/ip/IPContactHelper;->nSubRLSPollingLatestTime:J

    #@1e
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, "]"

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@2f
    .line 557
    const-string v2, "latest_rlssub_polling"

    #@31
    sget-wide v3, Lcom/lge/ims/service/ip/IPContactHelper;->nSubRLSPollingLatestTime:J

    #@33
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@3a
    .line 559
    iget-object v2, p0, Lcom/lge/ims/service/ip/IPContactHelper;->objContext:Landroid/content/Context;

    #@3c
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3f
    move-result-object v2

    #@40
    sget-object v3, Lcom/lge/ims/service/ip/IPConstant;->IP_PROVISIONING_CONTENT_URI:Landroid/net/Uri;

    #@42
    invoke-virtual {v2, v3, v1, v5, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@45
    move-result v0

    #@46
    .line 560
    .local v0, nCount:I
    if-nez v0, :cond_4f

    #@48
    .line 561
    const-string v2, "[IP][ERROR]UpdateRLSSubPollingLatestTime - nCount is 0"

    #@4a
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@4d
    .line 562
    const/4 v2, 0x0

    #@4e
    .line 565
    :goto_4e
    return v2

    #@4f
    .line 564
    :cond_4f
    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    #@52
    .line 565
    const/4 v2, 0x1

    #@53
    goto :goto_4e
.end method
