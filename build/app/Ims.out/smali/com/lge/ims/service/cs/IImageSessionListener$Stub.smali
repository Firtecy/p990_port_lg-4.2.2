.class public abstract Lcom/lge/ims/service/cs/IImageSessionListener$Stub;
.super Landroid/os/Binder;
.source "IImageSessionListener.java"

# interfaces
.implements Lcom/lge/ims/service/cs/IImageSessionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cs/IImageSessionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cs/IImageSessionListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.cs.IImageSessionListener"

.field static final TRANSACTION_onProgress:I = 0x4

.field static final TRANSACTION_onReceivedFile:I = 0x3

.field static final TRANSACTION_onStart:I = 0x1

.field static final TRANSACTION_onStop:I = 0x2

.field static final TRANSACTION_onTransferResult:I = 0x5


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.cs.IImageSessionListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/cs/IImageSessionListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IImageSessionListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.cs.IImageSessionListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/cs/IImageSessionListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/cs/IImageSessionListener;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/cs/IImageSessionListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/cs/IImageSessionListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_6c

    #@4
    .line 97
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 42
    :sswitch_9
    const-string v3, "com.lge.ims.service.cs.IImageSessionListener"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v3, "com.lge.ims.service.cs.IImageSessionListener"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 51
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    .line 52
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/cs/IImageSessionListener$Stub;->onStart(II)V

    #@1f
    .line 53
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@22
    goto :goto_8

    #@23
    .line 58
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_23
    const-string v3, "com.lge.ims.service.cs.IImageSessionListener"

    #@25
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28
    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v0

    #@2c
    .line 61
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IImageSessionListener$Stub;->onStop(I)V

    #@2f
    .line 62
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@32
    goto :goto_8

    #@33
    .line 67
    .end local v0           #_arg0:I
    :sswitch_33
    const-string v3, "com.lge.ims.service.cs.IImageSessionListener"

    #@35
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@38
    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    .line 71
    .local v0, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@3f
    move-result v1

    #@40
    .line 72
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/cs/IImageSessionListener$Stub;->onReceivedFile(Ljava/lang/String;I)V

    #@43
    .line 73
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@46
    goto :goto_8

    #@47
    .line 78
    .end local v0           #_arg0:Ljava/lang/String;
    .end local v1           #_arg1:I
    :sswitch_47
    const-string v3, "com.lge.ims.service.cs.IImageSessionListener"

    #@49
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@4c
    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@4f
    move-result v0

    #@50
    .line 82
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@53
    move-result v1

    #@54
    .line 83
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/cs/IImageSessionListener$Stub;->onProgress(II)V

    #@57
    .line 84
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5a
    goto :goto_8

    #@5b
    .line 89
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_5b
    const-string v3, "com.lge.ims.service.cs.IImageSessionListener"

    #@5d
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@60
    .line 91
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@63
    move-result v0

    #@64
    .line 92
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IImageSessionListener$Stub;->onTransferResult(I)V

    #@67
    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6a
    goto :goto_8

    #@6b
    .line 38
    nop

    #@6c
    :sswitch_data_6c
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_23
        0x3 -> :sswitch_33
        0x4 -> :sswitch_47
        0x5 -> :sswitch_5b
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
