.class public abstract Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub;
.super Landroid/os/Binder;
.source "ICapabilityQueryListener.java"

# interfaces
.implements Lcom/lge/ims/service/cd/ICapabilityQueryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cd/ICapabilityQueryListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.cd.ICapabilityQueryListener"

.field static final TRANSACTION_capabilityQueryDelivered:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.lge.ims.service.cd.ICapabilityQueryListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cd/ICapabilityQueryListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.lge.ims.service.cd.ICapabilityQueryListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/cd/ICapabilityQueryListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/lge/ims/service/cd/ICapabilityQueryListener;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_38

    #@4
    .line 69
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v3

    #@8
    :goto_8
    return v3

    #@9
    .line 47
    :sswitch_9
    const-string v4, "com.lge.ims.service.cd.ICapabilityQueryListener"

    #@b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v4, "com.lge.ims.service.cd.ICapabilityQueryListener"

    #@11
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@17
    move-result-object v4

    #@18
    invoke-static {v4}, Lcom/lge/ims/service/cd/ICapabilityQuery$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cd/ICapabilityQuery;

    #@1b
    move-result-object v0

    #@1c
    .line 56
    .local v0, _arg0:Lcom/lge/ims/service/cd/ICapabilityQuery;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_35

    #@22
    .line 57
    sget-object v4, Lcom/lge/ims/service/cd/Capabilities;->CREATOR:Landroid/os/Parcelable$Creator;

    #@24
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@27
    move-result-object v1

    #@28
    check-cast v1, Lcom/lge/ims/service/cd/Capabilities;

    #@2a
    .line 63
    .local v1, _arg1:Lcom/lge/ims/service/cd/Capabilities;
    :goto_2a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d
    move-result v2

    #@2e
    .line 64
    .local v2, _arg2:I
    invoke-virtual {p0, v0, v1, v2}, Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub;->capabilityQueryDelivered(Lcom/lge/ims/service/cd/ICapabilityQuery;Lcom/lge/ims/service/cd/Capabilities;I)V

    #@31
    .line 65
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@34
    goto :goto_8

    #@35
    .line 60
    .end local v1           #_arg1:Lcom/lge/ims/service/cd/Capabilities;
    .end local v2           #_arg2:I
    :cond_35
    const/4 v1, 0x0

    #@36
    .restart local v1       #_arg1:Lcom/lge/ims/service/cd/Capabilities;
    goto :goto_2a

    #@37
    .line 43
    nop

    #@38
    :sswitch_data_38
    .sparse-switch
        0x1 -> :sswitch_f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
