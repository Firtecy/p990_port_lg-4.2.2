.class public Lcom/lge/ims/service/ip/IPMessage;
.super Ljava/lang/Object;
.source "IPMessage.java"


# static fields
.field public static final ADDNONRCSUSER_CMD:I = 0x2a5c

.field public static final ADDNONRCSUSER_IND:I = 0x2a5d

.field public static final ADDRCSUSERLIST_CMD:I = 0x2a5e

.field public static final ADDRCSUSER_CMD:I = 0x2a58

.field public static final ADDRCSUSER_IND:I = 0x2a59

.field public static final CONNECTEDIPSERVICE_IND:I = 0x2a30

.field public static final CONNECTEDXDMSERVICE_IND:I = 0x2a32

.field public static final DELETERCSUSERLIST_CMD:I = 0x2a5f

.field public static final DELETERCSUSER_CMD:I = 0x2a5a

.field public static final DELETERCSUSER_IND:I = 0x2a5b

.field public static final DISCONNECTEDIPSERVICE_IND:I = 0x2a31

.field public static final GETBUDDYSTATUSICONTHUMB_CMD:I = 0x2a60

.field public static final GETBUDDYSTATUSICONTHUMB_IND:I = 0x2a61

.field public static final GETBUDDYSTATUSICON_CMD:I = 0x2a62

.field public static final GETBUDDYSTATUSICON_IND:I = 0x2a63

.field public static final GETMYSTATUSICONTHUMB_CMD:I = 0x2a52

.field public static final GETMYSTATUSICONTHUMB_IND:I = 0x2a53

.field public static final GETMYSTATUSICON_CMD:I = 0x2a54

.field public static final GETMYSTATUSICON_IND:I = 0x2a55

.field public static final GETRLSLISTURI_IND:I = 0x2a6e

.field public static final GETXDMDOCUMENT_CMD:I = 0x2a6c

.field public static final GETXDMDOCUMENT_IND:I = 0x2a6d

.field public static final INVITEFRIEND_CMD:I = 0x2a33

.field public static final IP_MSG_ADDRBOOK_AGENT_CHANGED:I = 0xe

.field public static final IP_MSG_AGENT_LISTENER_GETBUDDYSTATUSICON:I = 0x15

.field public static final IP_MSG_AGENT_LISTENER_NOTIFYQUERYCAPA:I = 0xd

.field public static final IP_MSG_AGENT_LISTENER_NOTIFYQUERYPRESENCE:I = 0x14

.field public static final IP_MSG_BGHELPER_AGENT_OFF:I = 0x10

.field public static final IP_MSG_BGHELPER_AGENT_ON:I = 0xf

.field public static final IP_MSG_CONTACT_ADD_NONRCSFRIEND:I = 0x9

.field public static final IP_MSG_CONTACT_ADD_RCSFRIEND:I = 0x7

.field public static final IP_MSG_CONTACT_CAPA_QUERY:I = 0x6

.field public static final IP_MSG_CONTACT_DELETE_RCSFRIEND:I = 0x8

.field public static final IP_MSG_CONTACT_DELETE_RCSFRIENDLIST:I = 0xb

.field public static final IP_MSG_CONTACT_ONEUSER_QUERY:I = 0xa

.field public static final IP_MSG_IMS_AGENT_CONNECTED:I = 0x1

.field public static final IP_MSG_IMS_AGENT_DISCONNECTED:I = 0x2

.field public static final IP_MSG_QUERYPOLLING_AGENT_EMPTY:I = 0x12

.field public static final IP_MSG_QUERYREPO_AGENT_EMPTY:I = 0x11

.field public static final IP_MSG_SAVE_ADD_NONRCSUSER:I = 0x2f

.field public static final IP_MSG_SAVE_ADD_RCSUSER:I = 0x2e

.field public static final IP_MSG_SAVE_DELETE_RCSUSER:I = 0x2d

.field public static final IP_MSG_SAVE_MY_STATUSICON_INFO:I = 0x2a

.field public static final IP_MSG_SAVE_MY_STATUS_INFO:I = 0x29

.field public static final IP_MSG_SAVE_ONE_CONTACTQUERY_FAILED:I = 0x30

.field public static final IP_MSG_SAVE_ONE_CONTACTQUERY_SUCCESS:I = 0x31

.field public static final IP_MSG_SAVE_RLS_LIST_URI:I = 0x28

.field public static final IP_MSG_SAVE_STATUS_ICON_URI:I = 0x2c

.field public static final IP_MSG_SAVE_XDM_DOC:I = 0x2b

.field public static final IP_MSG_SVC_AGENT_SYNC:I = 0x13

.field public static final IP_MSG_SVC_LISTENER_STOPLISTENER:I = 0xc

.field public static final IP_MSG_TIMER_AGENT_POLLING_EXPIRED:I = 0x4

.field public static final IP_MSG_TIMER_AGENT_SUBRLS_POLLING_EXPIRED:I = 0x5

.field public static final IP_MSG_XDM_AGENT_CONNECTED:I = 0x3

.field public static final IP_MSG_XDM_AGENT_GETRLSLIST_FAILED:I = 0x16

.field public static final IP_MSG_XDM_AGENT_GETRLSLIST_SUCCESS:I = 0x17

.field public static final IP_MSG_XDM_AGENT_GET_BUDDYSTATUSICON:I = 0x1c

.field public static final IP_MSG_XDM_AGENT_GET_BUDDYSTATUSICONTHUMB:I = 0x1b

.field public static final IP_MSG_XDM_AGENT_GET_MYSTATUSICON:I = 0x1a

.field public static final IP_MSG_XDM_AGENT_GET_MYSTATUSICONTHUMB:I = 0x19

.field public static final IP_MSG_XDM_AGENT_RLS_SYNCFAILED:I = 0x18

.field public static final IP_RESULT_FAILURE:I = 0x1

.field public static final IP_RESULT_SUCCESS:I = 0x0

.field public static final QUERYONECONTACTFAILED_IND:I = 0x2a49

.field public static final QUERYONECONTACTSUCCESS_IND:I = 0x2a4a

.field public static final QUERYONECONTACT_CMD:I = 0x2a44

.field public static final QUERYRLSLISTFAILED_IND:I = 0x2a47

.field public static final QUERYRLSLIST_CMD:I = 0x2a46

.field public static final QUERYRLSLIST_IND:I = 0x2a48

.field public static final RCSE_IP_SERVICE:I = 0x2a30

.field public static final RECEIVEDPRESENCEQUERY_IND:I = 0x2a45

.field public static final RLSSYNCFAILED_IND:I = 0x2a6f

.field public static final STOPXDMAGENT_CMD:I = 0x2a70

.field public static final UPDATEMYSTATUSICON_CMD:I = 0x2a4e

.field public static final UPDATEMYSTATUSICON_IND:I = 0x2a4f

.field public static final UPDATEMYSTATUSINFO_CMD:I = 0x2a3a

.field public static final UPDATEMYSTATUSINFO_IND:I = 0x2a3b


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 14
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
