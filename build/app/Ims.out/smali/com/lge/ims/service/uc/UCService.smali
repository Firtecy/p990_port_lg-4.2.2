.class public Lcom/lge/ims/service/uc/UCService;
.super Landroid/app/Service;
.source "UCService.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "UC"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 36
    const-string v0, "UC"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onBind() :: PID = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@10
    move-result v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " , TID = "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {}, Landroid/os/Process;->myTid()I

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    .line 38
    invoke-static {}, Lcom/lge/ims/service/uc/UCServiceManager;->getInstance()Lcom/lge/ims/service/uc/UCServiceManager;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCServiceManager;->getService()Lcom/lge/ims/service/uc/UCServiceImpl;

    #@31
    move-result-object v0

    #@32
    return-object v0
.end method

.method public onCreate()V
    .registers 3

    #@0
    .prologue
    .line 23
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 25
    const-string v0, "UC"

    #@5
    const-string v1, "onCreate()"

    #@7
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 26
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 30
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@3
    .line 32
    const-string v0, "UC"

    #@5
    const-string v1, "onDestroy()"

    #@7
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 33
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 42
    const-string v0, "UC"

    #@2
    const-string v1, "onRebind()"

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 43
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 46
    const-string v2, "UC"

    #@2
    const-string v3, "onUnbind()"

    #@4
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 48
    invoke-static {}, Lcom/lge/ims/service/uc/UCServiceManager;->getInstance()Lcom/lge/ims/service/uc/UCServiceManager;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2}, Lcom/lge/ims/service/uc/UCServiceManager;->getService()Lcom/lge/ims/service/uc/UCServiceImpl;

    #@e
    move-result-object v1

    #@f
    .line 50
    .local v1, ucSvc:Lcom/lge/ims/service/uc/UCServiceImpl;
    if-eqz v1, :cond_15

    #@11
    .line 51
    const/4 v2, 0x0

    #@12
    invoke-virtual {v1, v2}, Lcom/lge/ims/service/uc/UCServiceImpl;->setListener(Lcom/lge/ims/service/uc/IUCServiceListener;)V

    #@15
    .line 54
    :cond_15
    invoke-static {}, Lcom/lge/ims/service/uc/UCCallManager;->getInstance()Lcom/lge/ims/service/uc/UCCallManager;

    #@18
    move-result-object v0

    #@19
    .line 56
    .local v0, ucCall:Lcom/lge/ims/service/uc/UCCallManager;
    if-eqz v0, :cond_1e

    #@1b
    .line 57
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCCallManager;->terminateAllSession()V

    #@1e
    .line 60
    :cond_1e
    const/4 v2, 0x0

    #@1f
    return v2
.end method
