.class public Lcom/lge/ims/service/uc/UCCallManager$CallNode;
.super Ljava/lang/Object;
.source "UCCallManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/UCCallManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CallNode"
.end annotation


# instance fields
.field public mCallState:I

.field public mCallType:I

.field public mIncomingNumber:Ljava/lang/String;

.field public mSessKey:I

.field public mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

.field public m_CallModeChangeable:I

.field public m_EnableConf:I

.field public m_IsConf:I

.field public m_IsE911:I

.field public m_IsVMS:I

.field final synthetic this$0:Lcom/lge/ims/service/uc/UCCallManager;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/uc/UCCallManager;Lcom/lge/ims/service/uc/UCSessionImpl;)V
    .registers 5
    .parameter
    .parameter "session"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 135
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->this$0:Lcom/lge/ims/service/uc/UCCallManager;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 121
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@9
    .line 122
    const/4 v0, -0x1

    #@a
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@c
    .line 123
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSessKey:I

    #@e
    .line 124
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallState:I

    #@10
    .line 125
    const-string v0, ""

    #@12
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mIncomingNumber:Ljava/lang/String;

    #@14
    .line 128
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsConf:I

    #@16
    .line 129
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsVMS:I

    #@18
    .line 130
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsE911:I

    #@1a
    .line 131
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_EnableConf:I

    #@1c
    .line 132
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_CallModeChangeable:I

    #@1e
    .line 136
    iput-object p2, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@20
    .line 137
    return-void
.end method

.method public constructor <init>(Lcom/lge/ims/service/uc/UCCallManager;Lcom/lge/ims/service/uc/UCSessionImpl;II)V
    .registers 7
    .parameter
    .parameter "session"
    .parameter "sessKey"
    .parameter "callType"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 140
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->this$0:Lcom/lge/ims/service/uc/UCCallManager;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 121
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@9
    .line 122
    const/4 v0, -0x1

    #@a
    iput v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@c
    .line 123
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSessKey:I

    #@e
    .line 124
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallState:I

    #@10
    .line 125
    const-string v0, ""

    #@12
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mIncomingNumber:Ljava/lang/String;

    #@14
    .line 128
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsConf:I

    #@16
    .line 129
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsVMS:I

    #@18
    .line 130
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsE911:I

    #@1a
    .line 131
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_EnableConf:I

    #@1c
    .line 132
    iput v1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_CallModeChangeable:I

    #@1e
    .line 141
    iput-object p2, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@20
    .line 142
    iput p3, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSessKey:I

    #@22
    .line 143
    iput p4, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@24
    .line 144
    return-void
.end method


# virtual methods
.method public getProperty(I)I
    .registers 6
    .parameter "item"

    #@0
    .prologue
    .line 193
    const/4 v0, 0x0

    #@1
    .line 195
    .local v0, value:I
    sparse-switch p1, :sswitch_data_4a

    #@4
    .line 217
    const-string v1, "UC"

    #@6
    const-string v2, "NOT Expected"

    #@8
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 221
    :goto_b
    const-string v1, "UC"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "getProperty() ["

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, "]"

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    const-string v3, "["

    #@24
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    const-string v3, "]"

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 222
    return v0

    #@3a
    .line 197
    :sswitch_3a
    iget v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsConf:I

    #@3c
    .line 198
    goto :goto_b

    #@3d
    .line 201
    :sswitch_3d
    iget v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsVMS:I

    #@3f
    .line 202
    goto :goto_b

    #@40
    .line 205
    :sswitch_40
    iget v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsE911:I

    #@42
    .line 206
    goto :goto_b

    #@43
    .line 209
    :sswitch_43
    iget v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_EnableConf:I

    #@45
    .line 210
    goto :goto_b

    #@46
    .line 213
    :sswitch_46
    iget v0, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_CallModeChangeable:I

    #@48
    .line 214
    goto :goto_b

    #@49
    .line 195
    nop

    #@4a
    :sswitch_data_4a
    .sparse-switch
        0x0 -> :sswitch_3a
        0x1 -> :sswitch_3d
        0x2 -> :sswitch_40
        0x32 -> :sswitch_43
        0x33 -> :sswitch_46
    .end sparse-switch
.end method

.method public setCallState(I)V
    .registers 2
    .parameter "callState"

    #@0
    .prologue
    .line 155
    iput p1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallState:I

    #@2
    .line 156
    return-void
.end method

.method public setMTNumber(Ljava/lang/String;)V
    .registers 2
    .parameter "number"

    #@0
    .prologue
    .line 159
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mIncomingNumber:Ljava/lang/String;

    #@2
    .line 160
    return-void
.end method

.method public setProperty(II)V
    .registers 6
    .parameter "item"
    .parameter "value"

    #@0
    .prologue
    .line 163
    const-string v0, "UC"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setProperty() ["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "]"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, "["

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, "]"

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 165
    sparse-switch p1, :sswitch_data_48

    #@31
    .line 187
    const-string v0, "UC"

    #@33
    const-string v1, "NOT Expected"

    #@35
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@38
    .line 190
    :goto_38
    return-void

    #@39
    .line 167
    :sswitch_39
    iput p2, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsConf:I

    #@3b
    goto :goto_38

    #@3c
    .line 171
    :sswitch_3c
    iput p2, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsVMS:I

    #@3e
    goto :goto_38

    #@3f
    .line 175
    :sswitch_3f
    iput p2, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_IsE911:I

    #@41
    goto :goto_38

    #@42
    .line 179
    :sswitch_42
    iput p2, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_EnableConf:I

    #@44
    goto :goto_38

    #@45
    .line 183
    :sswitch_45
    iput p2, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->m_CallModeChangeable:I

    #@47
    goto :goto_38

    #@48
    .line 165
    :sswitch_data_48
    .sparse-switch
        0x0 -> :sswitch_39
        0x1 -> :sswitch_3c
        0x2 -> :sswitch_3f
        0x32 -> :sswitch_42
        0x33 -> :sswitch_45
    .end sparse-switch
.end method

.method public setSession(Lcom/lge/ims/service/uc/UCSessionImpl;)V
    .registers 2
    .parameter "session"

    #@0
    .prologue
    .line 151
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mSession:Lcom/lge/ims/service/uc/UCSessionImpl;

    #@2
    .line 152
    return-void
.end method

.method public updateCallType(I)V
    .registers 2
    .parameter "callType"

    #@0
    .prologue
    .line 147
    iput p1, p0, Lcom/lge/ims/service/uc/UCCallManager$CallNode;->mCallType:I

    #@2
    .line 148
    return-void
.end method
