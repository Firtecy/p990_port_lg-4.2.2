.class Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub$Proxy;
.super Ljava/lang/Object;
.source "ICapabilityQueryListener.java"

# interfaces
.implements Lcom/lge/ims/service/cd/ICapabilityQueryListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 75
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 76
    iput-object p1, p0, Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 77
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public capabilityQueryDelivered(Lcom/lge/ims/service/cd/ICapabilityQuery;Lcom/lge/ims/service/cd/Capabilities;I)V
    .registers 9
    .parameter "cq"
    .parameter "capabilities"
    .parameter "resultCode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 96
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 97
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 99
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cd.ICapabilityQueryListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 100
    if-eqz p1, :cond_34

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/cd/ICapabilityQuery;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 101
    if-eqz p2, :cond_36

    #@18
    .line 102
    const/4 v2, 0x1

    #@19
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1c
    .line 103
    const/4 v2, 0x0

    #@1d
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/cd/Capabilities;->writeToParcel(Landroid/os/Parcel;I)V

    #@20
    .line 108
    :goto_20
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@23
    .line 109
    iget-object v2, p0, Lcom/lge/ims/service/cd/ICapabilityQueryListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@25
    const/4 v3, 0x1

    #@26
    const/4 v4, 0x0

    #@27
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2a
    .line 110
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2d
    .catchall {:try_start_8 .. :try_end_2d} :catchall_3b

    #@2d
    .line 113
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@30
    .line 114
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@33
    .line 116
    return-void

    #@34
    .line 100
    :cond_34
    const/4 v2, 0x0

    #@35
    goto :goto_13

    #@36
    .line 106
    :cond_36
    const/4 v2, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_20

    #@3b
    .line 113
    :catchall_3b
    move-exception v2

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 114
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v2
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 84
    const-string v0, "com.lge.ims.service.cd.ICapabilityQueryListener"

    #@2
    return-object v0
.end method
