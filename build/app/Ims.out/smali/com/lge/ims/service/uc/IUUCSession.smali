.class public Lcom/lge/ims/service/uc/IUUCSession;
.super Ljava/lang/Object;
.source "IUUCSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/IUUCSession$Terminate_Reason;,
        Lcom/lge/ims/service/uc/IUUCSession$Reject_Reason;,
        Lcom/lge/ims/service/uc/IUUCSession$Fail_Reason;
    }
.end annotation


# static fields
.field public static final ACCEPT:I = 0x4b3

.field public static final ACCEPT_UPDATE:I = 0x4be

.field public static final ACTION_REMOTE_MEDIA:Ljava/lang/String; = "com.lge.ims.action.REMOTE_MEDIA"

.field public static final ATTACH:I = 0x512

.field public static final CONFINFO_CREATED:I = 0x1

.field public static final CONFINFO_INVALID:I = 0x0

.field public static final CONFINFO_INVITING:I = 0x2

.field public static final CONFINFO_INVITING_FAILED:I = 0x3

.field public static final CONFINFO_JOINED:I = 0x5

.field public static final CONFINFO_LEFT:I = 0x4

.field public static final DROPED_CONF:I = 0x526

.field public static final DROP_CONF:I = 0x4bc

.field public static final DROP_CONF_FAILED:I = 0x527

.field public static final EVENT_I2U:I = 0x514

.field public static final EVENT_U2I:I = 0x4b0

.field public static final EXPANDED_CONF:I = 0x51f

.field public static final EXPANDED_CONF_BY:I = 0x521

.field public static final EXPAND_CONF_FAILED:I = 0x520

.field public static final EXPAND_TO_CONF:I = 0x4b9

.field public static final HELD:I = 0x518

.field public static final HELD_BY:I = 0x51a

.field public static final HOLD:I = 0x4b5

.field public static final HOLD_FAILED:I = 0x519

.field public static final INCOMING_UPDATE:I = 0x529

.field public static final INFO_TYPE_BALANCE:I = 0x2

.field public static final INFO_TYPE_MINUTES:I = 0x3

.field public static final INFO_TYPE_MYNUMBER:I = 0x1

.field public static final INFO_TYPE_NONE:I = 0x0

.field public static final INFO_TYPE_PREPAID:I = 0x5

.field public static final INFO_TYPE_TEXTUSAGE:I = 0x4

.field public static final JOINED_CONF:I = 0x524

.field public static final JOIN_CONF:I = 0x4bb

.field public static final JOIN_CONF_FAILED:I = 0x525

.field public static final MERGE:I = 0x4ba

.field public static final MERGED:I = 0x522

.field public static final MERGE_FAILED:I = 0x523

.field public static final NOTIFY_CONF_INFO:I = 0x528

.field public static final NOTIFY_INFO:I = 0x52e

.field public static final NOTIFY_REMOTE_MEDIA:I = 0x52d

.field public static final PROGRESSING:I = 0x517

.field public static final PROPERTY_CALL_MODE_CHANGEABLE:I = 0x33

.field public static final PROPERTY_ENABLE_CONF:I = 0x32

.field public static final PROPERTY_IS_CONF:I = 0x0

.field public static final PROPERTY_IS_EMERGENCY:I = 0x2

.field public static final PROPERTY_IS_MMC:I = 0x3

.field public static final PROPERTY_IS_VMS:I = 0x1

.field public static final REJECT:I = 0x4b4

.field public static final REJECT_UPDATE:I = 0x4bf

.field public static final REJECT_USER:I = 0x0

.field public static final RESUME:I = 0x4b6

.field public static final RESUMED:I = 0x51b

.field public static final RESUMED_BY:I = 0x51d

.field public static final RESUME_FAILED:I = 0x51c

.field public static final SEND_DTMF:I = 0x4b7

.field public static final SESSIONTYPE_VIDEOSHARE:I = 0x1

.field public static final SESSIONTYPE_VOIP:I = 0x0

.field public static final SESSIONTYPE_VT:I = 0x2

.field public static final START:I = 0x4b1

.field public static final STARTCONF:I = 0x4b2

.field public static final STARTED:I = 0x515

.field public static final START_FAILED:I = 0x516

.field public static final TERMINATE:I = 0x4b8

.field public static final TERMINATED:I = 0x51e

.field public static final UPDATE:I = 0x4bd

.field public static final UPDATED:I = 0x52a

.field public static final UPDATED_BY:I = 0x52c

.field public static final UPDATE_FAILED:I = 0x52b


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
