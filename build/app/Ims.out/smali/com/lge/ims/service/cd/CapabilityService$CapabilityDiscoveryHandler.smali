.class final Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;
.super Landroid/os/Handler;
.source "CapabilityService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cd/CapabilityService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "CapabilityDiscoveryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/cd/CapabilityService;


# direct methods
.method private constructor <init>(Lcom/lge/ims/service/cd/CapabilityService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 545
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/service/cd/CapabilityService;Lcom/lge/ims/service/cd/CapabilityService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 545
    invoke-direct {p0, p1}, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;-><init>(Lcom/lge/ims/service/cd/CapabilityService;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    .line 549
    if-nez p1, :cond_3

    #@2
    .line 574
    :cond_2
    :goto_2
    return-void

    #@3
    .line 553
    :cond_3
    const-string v2, "CapService"

    #@5
    new-instance v3, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "CapabilityDiscoveryHandler :: handleMessage - "

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    iget v4, p1, Landroid/os/Message;->what:I

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v3

    #@1a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 555
    iget v2, p1, Landroid/os/Message;->what:I

    #@1f
    packed-switch v2, :pswitch_data_5a

    #@22
    .line 571
    const-string v2, "CapService"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "Unhandled Message :: "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    iget v4, p1, Landroid/os/Message;->what:I

    #@31
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3c
    goto :goto_2

    #@3d
    .line 558
    :pswitch_3d
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@3f
    invoke-static {v2}, Lcom/lge/ims/service/cd/CapabilityService;->access$700(Lcom/lge/ims/service/cd/CapabilityService;)Ljava/util/ArrayList;

    #@42
    move-result-object v1

    #@43
    .line 560
    .local v1, trackingNumbers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_2

    #@45
    .line 564
    const/4 v0, 0x0

    #@46
    .local v0, i:I
    :goto_46
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@49
    move-result v2

    #@4a
    if-ge v0, v2, :cond_2

    #@4c
    .line 565
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;->this$0:Lcom/lge/ims/service/cd/CapabilityService;

    #@4e
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@51
    move-result-object v2

    #@52
    check-cast v2, Ljava/lang/String;

    #@54
    invoke-static {v3, v2}, Lcom/lge/ims/service/cd/CapabilityService;->access$800(Lcom/lge/ims/service/cd/CapabilityService;Ljava/lang/String;)Z

    #@57
    .line 564
    add-int/lit8 v0, v0, 0x1

    #@59
    goto :goto_46

    #@5a
    .line 555
    :pswitch_data_5a
    .packed-switch 0xc8
        :pswitch_3d
    .end packed-switch
.end method
