.class public Lcom/lge/ims/service/uc/IUUCService;
.super Ljava/lang/Object;
.source "IUUCService.java"


# static fields
.field public static final ACTION_INCOMING_SESSION:Ljava/lang/String; = "com.lge.ims.service.uc.IncomingUCSession"

.field public static final ATTCH_SESSION:I = 0x3ea

.field public static final CALLTYPE_EMERGENCY:I = 0x1

.field public static final CALLTYPE_NORMAL:I = 0x0

.field public static final CALLTYPE_OFFLINE:I = 0x2

.field public static final CLOSE_SESSION:I = 0x3eb

.field public static final ES_ECM:I = 0x5

.field public static final ES_ES:I = 0x4

.field public static final ES_IDLE:I = 0x0

.field public static final ES_OPENED:I = 0x2

.field public static final ES_OPENFAILED:I = 0x3

.field public static final ES_OPENING:I = 0x1

.field public static final EVENT_I2U:I = 0x44c

.field public static final EVENT_U2I:I = 0x3e8

.field public static final E_SERVICE_CHANGED:I = 0x44e

.field public static final INCOMING_SESSION:I = 0x44f

.field public static final OPEN_SESSION:I = 0x3e9

.field public static final REGISTER_SERVICE:I = 0x3ec

.field public static final SERVICESTATUS_REASON_BYSERVER:I = 0x3

.field public static final SERVICESTATUS_REASON_NETWORKDISABLE:I = 0x1

.field public static final SERVICESTATUS_REASON_SIMSINVALID:I = 0x2

.field public static final SERVICESTATUS_REASON_UNKNWON:I = 0x0

.field public static final SERVICESTATUS_REASON_USERSELECT:I = 0x4

.field public static final SERVICE_CHANGED:I = 0x44d

.field public static final SERVICE_EMERGENCY:I = 0x4

.field public static final SERVICE_NONE:I = 0x0

.field public static final SERVICE_OPENING:I = 0x5

.field public static final SERVICE_UC:I = 0x3

.field public static final SERVICE_VOIP:I = 0x1

.field public static final SERVICE_VT:I = 0x2

.field public static final STATE_REG:I = 0x2

.field public static final STATE_VOIP:I = 0x1

.field public static final STATE_VT:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 4
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
