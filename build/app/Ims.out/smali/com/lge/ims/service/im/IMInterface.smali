.class public Lcom/lge/ims/service/im/IMInterface;
.super Ljava/lang/Object;
.source "IMInterface.java"


# static fields
.field public static final CLOSE_CHAT_CMD:I = 0x296a

.field public static final CLOSE_CONFERENCE_CMD:I = 0x296c

.field public static final CLOSE_FILE_TRANSFER_CMD:I = 0x296e

.field public static final FT_ACCEPT_CMD:I = 0x29a7

.field public static final FT_ESTABLISHED_IND:I = 0x29af

.field public static final FT_HTTP_DOWNLOAD_CMD:I = 0x29a9

.field public static final FT_HTTP_UPLOAD_CMD:I = 0x29aa

.field public static final FT_HTTP_UPLOAD_TERMINATED_IND:I = 0x29b3

.field public static final FT_REJECT_CMD:I = 0x29a8

.field public static final FT_START_CMD:I = 0x29a5

.field public static final FT_TERMINATED_IND:I = 0x29b0

.field public static final FT_TERMINATE_CMD:I = 0x29a6

.field public static final FT_TRANSFERCOMPLETED_IND:I = 0x29b2

.field public static final FT_TRANSFERPROGRESS_IND:I = 0x29b1

.field public static final IM_ACCEPT_CMD:I = 0x298c

.field public static final IM_ADDED_PARTICIPANTS_IND:I = 0x2997

.field public static final IM_ADD_PARTICIPANTS_CMD:I = 0x298e

.field public static final IM_ESTABLISHED_IND:I = 0x2994

.field public static final IM_EXTENDED_CONFERENCE_IND:I = 0x2996

.field public static final IM_EXTEND_TO_CONFERENCE_CMD:I = 0x298f

.field public static final IM_JOINED_PARTICIPANT_IND:I = 0x2998

.field public static final IM_LEAVE_CONFERENCE_CMD:I = 0x298a

.field public static final IM_LEFT_PARTICIPANT_IND:I = 0x2999

.field public static final IM_RECEIVED_CONFERENCE_WITH_FILE_LINK_IND:I = 0x299e

.field public static final IM_RECEIVED_FILE_LINK_IND:I = 0x299d

.field public static final IM_RECEIVED_IS_COMPOSING_IND:I = 0x299c

.field public static final IM_RECEIVED_MESSAGE_IND:I = 0x299b

.field public static final IM_REJECT_CMD:I = 0x298d

.field public static final IM_REJECT_DENIED_INVITATION_CMD:I = 0x2993

.field public static final IM_REJOIN_CONFERENCE_CMD:I = 0x2989

.field public static final IM_SEND_DISPLAY_NOTIFICATION_CMD:I = 0x2992

.field public static final IM_SEND_IS_COMPOSING_CMD:I = 0x2991

.field public static final IM_SEND_MESSAGE_CMD:I = 0x2990

.field public static final IM_SENT_MESSAGE_IND:I = 0x299a

.field public static final IM_START_CHAT_CMD:I = 0x2987

.field public static final IM_START_CONFERENCE_CMD:I = 0x2988

.field public static final IM_TERMINATED_IND:I = 0x2995

.field public static final IM_TERMINATE_CMD:I = 0x298b

.field public static final OCCURRED_EXCEPTION_IND:I = 0x297b

.field public static final OPEN_CHAT_CMD:I = 0x2969

.field public static final OPEN_CONFERENCE_CMD:I = 0x296b

.field public static final OPEN_FILE_TRANSFER_CMD:I = 0x296d

.field public static final RCS_IM:I = 0x2968

.field public static final RECEIVED_CHAT_IND:I = 0x2973

.field public static final RECEIVED_CONFERENCE_EVENT_IND:I = 0x2979

.field public static final RECEIVED_CONFERENCE_IND:I = 0x2974

.field public static final RECEIVED_CONFERENCE_WITH_FILE_LINK_IND:I = 0x2978

.field public static final RECEIVED_FILE_LINK_IND:I = 0x2977

.field public static final RECEIVED_FILE_TRANSFER_IND:I = 0x2975

.field public static final RECEIVED_IMDN_IND:I = 0x2976

.field public static final SEND_IMDN_CMD:I = 0x2970

.field public static final SEND_INVITATION_MESSAGE_CMD:I = 0x2972

.field public static final SERVICE_STATUS_IND:I = 0x297a

.field public static final UPDATE_CONFIGURATION_CMD:I = 0x296f


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
