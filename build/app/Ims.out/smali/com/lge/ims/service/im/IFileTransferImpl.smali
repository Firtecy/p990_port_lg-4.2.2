.class public abstract Lcom/lge/ims/service/im/IFileTransferImpl;
.super Ljava/lang/Object;
.source "IFileTransferImpl.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;
.implements Ljava/io/Serializable;


# instance fields
.field listener:Lcom/lge/ims/service/im/IFileTransferListener;

.field protected nativeObj:I

.field remoteUsers:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 8
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 9
    const/4 v0, 0x0

    #@5
    iput v0, p0, Lcom/lge/ims/service/im/IFileTransferImpl;->nativeObj:I

    #@7
    .line 10
    iput-object v1, p0, Lcom/lge/ims/service/im/IFileTransferImpl;->listener:Lcom/lge/ims/service/im/IFileTransferListener;

    #@9
    .line 11
    iput-object v1, p0, Lcom/lge/ims/service/im/IFileTransferImpl;->remoteUsers:[Ljava/lang/String;

    #@b
    return-void
.end method


# virtual methods
.method public abstract accept(Ljava/lang/String;)V
.end method

.method public abstract cancel()V
.end method

.method public abstract destroy()V
.end method

.method public abstract download(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract onMessage(Landroid/os/Parcel;)V
.end method

.method public abstract reject()V
.end method

.method public abstract sendFile(Lcom/lge/ims/service/im/IMFileInfo;)V
.end method

.method protected abstract sendMessageToJNI(Landroid/os/Parcel;)V
.end method

.method public abstract setListener(Lcom/lge/ims/service/im/IFileTransferListener;)V
.end method
