.class public Lcom/lge/ims/service/ac/content/ACServiceAvailability;
.super Ljava/lang/Object;
.source "ACServiceAvailability.java"


# instance fields
.field public mPSVT:Ljava/lang/String;

.field public mRCSOEM:Ljava/lang/String;

.field public mVOLTE:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 13
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 7
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@6
    .line 8
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@8
    .line 9
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@a
    .line 14
    return-void
.end method


# virtual methods
.method public clear()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 17
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@3
    .line 18
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@5
    .line 19
    iput-object v0, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@7
    .line 20
    return-void
.end method

.method public containsContent(Lcom/lge/ims/service/ac/content/ACServiceAvailability;)Z
    .registers 6
    .parameter "cache"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 23
    if-nez p1, :cond_5

    #@4
    .line 45
    :cond_4
    :goto_4
    return v0

    #@5
    .line 27
    :cond_5
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@7
    if-eqz v2, :cond_19

    #@9
    iget-object v2, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@b
    if-eqz v2, :cond_19

    #@d
    .line 28
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@f
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_19

    #@17
    move v0, v1

    #@18
    .line 29
    goto :goto_4

    #@19
    .line 33
    :cond_19
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@1b
    if-eqz v2, :cond_2d

    #@1d
    iget-object v2, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@1f
    if-eqz v2, :cond_2d

    #@21
    .line 34
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@23
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@28
    move-result v2

    #@29
    if-nez v2, :cond_2d

    #@2b
    move v0, v1

    #@2c
    .line 35
    goto :goto_4

    #@2d
    .line 39
    :cond_2d
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@2f
    if-eqz v2, :cond_4

    #@31
    iget-object v2, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@33
    if-eqz v2, :cond_4

    #@35
    .line 40
    iget-object v2, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@37
    iget-object v3, p1, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3c
    move-result v2

    #@3d
    if-nez v2, :cond_4

    #@3f
    move v0, v1

    #@40
    .line 41
    goto :goto_4
.end method

.method public isContentPresent()Z
    .registers 2

    #@0
    .prologue
    .line 49
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@2
    if-nez v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@6
    if-nez v0, :cond_c

    #@8
    iget-object v0, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public toString()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "RCS_OEM="

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mRCSOEM:Ljava/lang/String;

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, ", VOLTE="

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mVOLTE:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    const-string v1, ", VT="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    iget-object v1, p0, Lcom/lge/ims/service/ac/content/ACServiceAvailability;->mPSVT:Ljava/lang/String;

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    return-object v0
.end method
