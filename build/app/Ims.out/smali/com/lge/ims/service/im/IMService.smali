.class public Lcom/lge/ims/service/im/IMService;
.super Landroid/app/Service;
.source "IMService.java"


# instance fields
.field private DELETE_CHAT:I

.field private DELETE_FILE:I

.field handler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 15
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/lge/ims/service/im/IMService;->DELETE_CHAT:I

    #@6
    .line 16
    const/4 v0, 0x1

    #@7
    iput v0, p0, Lcom/lge/ims/service/im/IMService;->DELETE_FILE:I

    #@9
    .line 18
    new-instance v0, Lcom/lge/ims/service/im/IMService$1;

    #@b
    invoke-direct {v0, p0}, Lcom/lge/ims/service/im/IMService$1;-><init>(Lcom/lge/ims/service/im/IMService;)V

    #@e
    iput-object v0, p0, Lcom/lge/ims/service/im/IMService;->handler:Landroid/os/Handler;

    #@10
    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/service/im/IMService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 13
    iget v0, p0, Lcom/lge/ims/service/im/IMService;->DELETE_CHAT:I

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/lge/ims/service/im/IMService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 13
    iget v0, p0, Lcom/lge/ims/service/im/IMService;->DELETE_FILE:I

    #@2
    return v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "arg0"

    #@0
    .prologue
    .line 137
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5
    .line 139
    invoke-static {}, Lcom/lge/ims/service/im/IMServiceManager;->getInstance()Lcom/lge/ims/service/im/IMServiceManager;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {v0}, Lcom/lge/ims/service/im/IMServiceManager;->getService()Lcom/lge/ims/service/im/IMServiceImpl;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public onCreate()V
    .registers 2

    #@0
    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 63
    const-string v0, ""

    #@5
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@8
    .line 64
    return-void
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 68
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@3
    .line 70
    const-string v0, ""

    #@5
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@8
    .line 71
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .registers 13
    .parameter "intent"
    .parameter "startId"

    #@0
    .prologue
    const-wide/16 v8, 0x3

    #@2
    const/4 v7, 0x1

    #@3
    const/4 v6, 0x0

    #@4
    .line 75
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    #@7
    .line 77
    const-string v5, ""

    #@9
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@c
    .line 79
    const-string v5, "deleteFileTransfer"

    #@e
    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@11
    move-result v5

    #@12
    if-ne v5, v7, :cond_52

    #@14
    .line 80
    const-string v5, "deleteFileTransfer"

    #@16
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@19
    .line 82
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@1c
    move-result-object v0

    #@1d
    .line 83
    .local v0, bundle:Landroid/os/Bundle;
    if-nez v0, :cond_25

    #@1f
    .line 84
    const-string v5, "bundle is null"

    #@21
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@24
    .line 133
    .end local v0           #bundle:Landroid/os/Bundle;
    :goto_24
    return-void

    #@25
    .line 88
    .restart local v0       #bundle:Landroid/os/Bundle;
    :cond_25
    const-string v5, "FileTransferImplFacade"

    #@27
    invoke-virtual {v0, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@2a
    move-result-object v4

    #@2b
    .line 89
    .local v4, obj:Ljava/lang/Object;
    if-nez v4, :cond_33

    #@2d
    .line 90
    const-string v5, "obj is null"

    #@2f
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@32
    goto :goto_24

    #@33
    :cond_33
    move-object v5, v4

    #@34
    .line 94
    check-cast v5, Landroid/os/IBinder;

    #@36
    invoke-static {v5}, Lcom/lge/ims/service/im/IFileTransfer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IFileTransfer;

    #@39
    move-result-object v2

    #@3a
    .line 95
    .local v2, iFileTransfer:Lcom/lge/ims/service/im/IFileTransfer;
    if-nez v2, :cond_42

    #@3c
    .line 96
    const-string v5, "iFileTransfer is null"

    #@3e
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@41
    goto :goto_24

    #@42
    .line 100
    :cond_42
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@45
    move-result-object v3

    #@46
    .line 101
    .local v3, msg:Landroid/os/Message;
    iget v5, p0, Lcom/lge/ims/service/im/IMService;->DELETE_FILE:I

    #@48
    iput v5, v3, Landroid/os/Message;->what:I

    #@4a
    .line 102
    iput-object v2, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4c
    .line 103
    iget-object v5, p0, Lcom/lge/ims/service/im/IMService;->handler:Landroid/os/Handler;

    #@4e
    invoke-virtual {v5, v3, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@51
    goto :goto_24

    #@52
    .line 104
    .end local v0           #bundle:Landroid/os/Bundle;
    .end local v2           #iFileTransfer:Lcom/lge/ims/service/im/IFileTransfer;
    .end local v3           #msg:Landroid/os/Message;
    .end local v4           #obj:Ljava/lang/Object;
    :cond_52
    const-string v5, "deleteChat"

    #@54
    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@57
    move-result v5

    #@58
    if-ne v5, v7, :cond_98

    #@5a
    .line 105
    const-string v5, "deleteChat"

    #@5c
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@5f
    .line 107
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@62
    move-result-object v0

    #@63
    .line 108
    .restart local v0       #bundle:Landroid/os/Bundle;
    if-nez v0, :cond_6b

    #@65
    .line 109
    const-string v5, "bundle is null"

    #@67
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@6a
    goto :goto_24

    #@6b
    .line 113
    :cond_6b
    const-string v5, "ChatImpl"

    #@6d
    invoke-virtual {v0, v5}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@70
    move-result-object v4

    #@71
    .line 114
    .restart local v4       #obj:Ljava/lang/Object;
    if-nez v4, :cond_79

    #@73
    .line 115
    const-string v5, "obj is null"

    #@75
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@78
    goto :goto_24

    #@79
    :cond_79
    move-object v5, v4

    #@7a
    .line 119
    check-cast v5, Landroid/os/IBinder;

    #@7c
    invoke-static {v5}, Lcom/lge/ims/service/im/IChat$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IChat;

    #@7f
    move-result-object v1

    #@80
    .line 120
    .local v1, iChat:Lcom/lge/ims/service/im/IChat;
    if-nez v1, :cond_88

    #@82
    .line 121
    const-string v5, "iChat is null"

    #@84
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@87
    goto :goto_24

    #@88
    .line 125
    :cond_88
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@8b
    move-result-object v3

    #@8c
    .line 126
    .restart local v3       #msg:Landroid/os/Message;
    iget v5, p0, Lcom/lge/ims/service/im/IMService;->DELETE_CHAT:I

    #@8e
    iput v5, v3, Landroid/os/Message;->what:I

    #@90
    .line 127
    iput-object v1, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@92
    .line 128
    iget-object v5, p0, Lcom/lge/ims/service/im/IMService;->handler:Landroid/os/Handler;

    #@94
    invoke-virtual {v5, v3, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@97
    goto :goto_24

    #@98
    .line 130
    .end local v0           #bundle:Landroid/os/Bundle;
    .end local v1           #iChat:Lcom/lge/ims/service/im/IChat;
    .end local v3           #msg:Landroid/os/Message;
    .end local v4           #obj:Ljava/lang/Object;
    :cond_98
    const-string v5, ""

    #@9a
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@9d
    goto :goto_24
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    #@3
    .line 146
    const-string v0, ""

    #@5
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@8
    .line 148
    const/4 v0, 0x0

    #@9
    return v0
.end method
