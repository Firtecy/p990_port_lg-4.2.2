.class Lcom/lge/ims/service/ip/IPAgent$2;
.super Landroid/database/ContentObserver;
.source "IPAgent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ip/IPAgent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/ip/IPAgent;


# direct methods
.method constructor <init>(Lcom/lge/ims/service/ip/IPAgent;Landroid/os/Handler;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 1876
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPAgent$2;->this$0:Lcom/lge/ims/service/ip/IPAgent;

    #@2
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@5
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 6
    .parameter "selfChange"

    #@0
    .prologue
    const/16 v3, 0xe

    #@2
    .line 1879
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    #@5
    .line 1880
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@8
    move-result-object v0

    #@9
    if-nez v0, :cond_11

    #@b
    .line 1881
    const-string v0, "[IP][ERROR] IPAgent.GetIPAgent() is NULL"

    #@d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@10
    .line 1891
    :goto_10
    return-void

    #@11
    .line 1883
    :cond_11
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@18
    move-result-object v0

    #@19
    if-nez v0, :cond_21

    #@1b
    .line 1884
    const-string v0, "[IP][ERROR] IPAgent.GetIPAgent().GetHandler() is NULL"

    #@1d
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@20
    goto :goto_10

    #@21
    .line 1887
    :cond_21
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@28
    move-result-object v0

    #@29
    invoke-virtual {v0, v3}, Landroid/os/Handler;->hasMessages(I)Z

    #@2c
    move-result v0

    #@2d
    const/4 v1, 0x1

    #@2e
    if-ne v0, v1, :cond_3b

    #@30
    .line 1888
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@33
    move-result-object v0

    #@34
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@3b
    .line 1890
    :cond_3b
    invoke-static {}, Lcom/lge/ims/service/ip/IPAgent;->GetIPAgent()Lcom/lge/ims/service/ip/IPAgent;

    #@3e
    move-result-object v0

    #@3f
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/IPAgent;->GetHandler()Landroid/os/Handler;

    #@42
    move-result-object v0

    #@43
    const-wide/16 v1, 0xbb8

    #@45
    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@48
    goto :goto_10
.end method
