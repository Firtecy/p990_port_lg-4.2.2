.class public abstract Lcom/lge/ims/service/cs/IVideoSessionListener$Stub;
.super Landroid/os/Binder;
.source "IVideoSessionListener.java"

# interfaces
.implements Lcom/lge/ims/service/cs/IVideoSessionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cs/IVideoSessionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cs/IVideoSessionListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.cs.IVideoSessionListener"

.field static final TRANSACTION_onPrepared:I = 0x2

.field static final TRANSACTION_onStart:I = 0x1

.field static final TRANSACTION_onStartMediaRecord:I = 0x4

.field static final TRANSACTION_onStop:I = 0x3

.field static final TRANSACTION_onStopMediaRecord:I = 0x5


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.cs.IVideoSessionListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/cs/IVideoSessionListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/cs/IVideoSessionListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.cs.IVideoSessionListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/cs/IVideoSessionListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/cs/IVideoSessionListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 38
    sparse-switch p1, :sswitch_data_60

    #@4
    .line 91
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 42
    :sswitch_9
    const-string v3, "com.lge.ims.service.cs.IVideoSessionListener"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 47
    :sswitch_f
    const-string v3, "com.lge.ims.service.cs.IVideoSessionListener"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 51
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    .line 52
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/cs/IVideoSessionListener$Stub;->onStart(II)V

    #@1f
    .line 53
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@22
    goto :goto_8

    #@23
    .line 58
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_23
    const-string v3, "com.lge.ims.service.cs.IVideoSessionListener"

    #@25
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28
    .line 59
    invoke-virtual {p0}, Lcom/lge/ims/service/cs/IVideoSessionListener$Stub;->onPrepared()V

    #@2b
    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2e
    goto :goto_8

    #@2f
    .line 65
    :sswitch_2f
    const-string v3, "com.lge.ims.service.cs.IVideoSessionListener"

    #@31
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@34
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@37
    move-result v0

    #@38
    .line 68
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IVideoSessionListener$Stub;->onStop(I)V

    #@3b
    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@3e
    goto :goto_8

    #@3f
    .line 74
    .end local v0           #_arg0:I
    :sswitch_3f
    const-string v3, "com.lge.ims.service.cs.IVideoSessionListener"

    #@41
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@44
    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v0

    #@48
    .line 77
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IVideoSessionListener$Stub;->onStartMediaRecord(I)V

    #@4b
    .line 78
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e
    goto :goto_8

    #@4f
    .line 83
    .end local v0           #_arg0:I
    :sswitch_4f
    const-string v3, "com.lge.ims.service.cs.IVideoSessionListener"

    #@51
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@54
    .line 85
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@57
    move-result v0

    #@58
    .line 86
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/cs/IVideoSessionListener$Stub;->onStopMediaRecord(I)V

    #@5b
    .line 87
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    goto :goto_8

    #@5f
    .line 38
    nop

    #@60
    :sswitch_data_60
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_23
        0x3 -> :sswitch_2f
        0x4 -> :sswitch_3f
        0x5 -> :sswitch_4f
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
