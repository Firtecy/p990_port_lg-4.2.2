.class Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;
.super Ljava/lang/Object;
.source "IPListenerThread.java"

# interfaces
.implements Lcom/lge/ims/service/cd/CapabilitiesListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/ip/IPListenerThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "IPCapaListener"
.end annotation


# instance fields
.field private objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

.field final synthetic this$0:Lcom/lge/ims/service/ip/IPListenerThread;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/ip/IPListenerThread;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 155
    iput-object p1, p0, Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;->this$0:Lcom/lge/ims/service/ip/IPListenerThread;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 153
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@8
    .line 156
    const-string v0, ""

    #@a
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@d
    .line 157
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@f
    if-nez v0, :cond_17

    #@11
    .line 158
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityService;->getInstance()Lcom/lge/ims/service/cd/CapabilityService;

    #@14
    move-result-object v0

    #@15
    iput-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@17
    .line 161
    :cond_17
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@19
    if-eqz v0, :cond_20

    #@1b
    .line 162
    iget-object v0, p0, Lcom/lge/ims/service/ip/IPListenerThread$IPCapaListener;->objCapabilityService:Lcom/lge/ims/service/cd/CapabilityService;

    #@1d
    invoke-virtual {v0, p0}, Lcom/lge/ims/service/cd/CapabilityService;->addBroadcastListener(Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@20
    .line 164
    :cond_20
    return-void
.end method


# virtual methods
.method public capabilitiesUpdated(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V
    .registers 9
    .parameter "sc"
    .parameter "resultCode"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x1

    #@2
    .line 176
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@5
    move-result-object v3

    #@6
    if-eqz v3, :cond_96

    #@8
    .line 177
    new-instance v2, Landroid/os/Message;

    #@a
    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    #@d
    .line 178
    .local v2, objMessage:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    #@f
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@12
    .line 179
    .local v0, objBundle:Landroid/os/Bundle;
    new-instance v1, Lcom/lge/ims/service/ip/CapaInfo;

    #@14
    invoke-direct {v1}, Lcom/lge/ims/service/ip/CapaInfo;-><init>()V

    #@17
    .line 180
    .local v1, objCapaInfo:Lcom/lge/ims/service/ip/CapaInfo;
    invoke-virtual {p1}, Lcom/lge/ims/service/cd/ServiceCapabilities;->getPhoneNumber()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v1, v3}, Lcom/lge/ims/service/ip/CapaInfo;->SetMSISDN(Ljava/lang/String;)V

    #@1e
    .line 181
    invoke-virtual {v1, p2}, Lcom/lge/ims/service/ip/CapaInfo;->SetResponseCode(I)V

    #@21
    .line 182
    invoke-virtual {p1, v4}, Lcom/lge/ims/service/cd/ServiceCapabilities;->isServiceSupported(I)Z

    #@24
    move-result v3

    #@25
    if-eqz v3, :cond_97

    #@27
    .line 183
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/ip/CapaInfo;->SetFTCapa(I)V

    #@2a
    .line 188
    :goto_2a
    const/4 v3, 0x2

    #@2b
    invoke-virtual {p1, v3}, Lcom/lge/ims/service/cd/ServiceCapabilities;->isServiceSupported(I)Z

    #@2e
    move-result v3

    #@2f
    if-eqz v3, :cond_9b

    #@31
    .line 189
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/ip/CapaInfo;->SetIMCapa(I)V

    #@34
    .line 190
    invoke-virtual {v1, p2}, Lcom/lge/ims/service/ip/CapaInfo;->SetResponseCode(I)V

    #@37
    .line 198
    :cond_37
    :goto_37
    const/16 v3, 0x100

    #@39
    invoke-virtual {p1, v3}, Lcom/lge/ims/service/cd/ServiceCapabilities;->isServiceSupported(I)Z

    #@3c
    move-result v3

    #@3d
    if-eqz v3, :cond_a6

    #@3f
    .line 199
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/ip/CapaInfo;->SetPresenceCapa(I)V

    #@42
    .line 204
    :goto_42
    const/16 v3, 0x200

    #@44
    invoke-virtual {p1, v3}, Lcom/lge/ims/service/cd/ServiceCapabilities;->isServiceSupported(I)Z

    #@47
    move-result v3

    #@48
    if-eqz v3, :cond_aa

    #@4a
    .line 205
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/ip/CapaInfo;->SetDiscoveryPresenceCapa(I)V

    #@4d
    .line 210
    :goto_4d
    const/16 v3, 0x2000

    #@4f
    invoke-virtual {p1, v3}, Lcom/lge/ims/service/cd/ServiceCapabilities;->isServiceSupported(I)Z

    #@52
    move-result v3

    #@53
    if-eqz v3, :cond_ae

    #@55
    .line 211
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/ip/CapaInfo;->SetMIMCapa(I)V

    #@58
    .line 216
    :goto_58
    const/16 v3, 0x1000

    #@5a
    invoke-virtual {p1, v3}, Lcom/lge/ims/service/cd/ServiceCapabilities;->isServiceSupported(I)Z

    #@5d
    move-result v3

    #@5e
    if-eqz v3, :cond_b2

    #@60
    .line 217
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/ip/CapaInfo;->SetHTTPCapa(I)V

    #@63
    .line 222
    :goto_63
    new-instance v3, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v4, "[IP]capabilitiesUpdated : "

    #@6a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v1}, Lcom/lge/ims/service/ip/CapaInfo;->toString()Ljava/lang/String;

    #@71
    move-result-object v4

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v3

    #@7a
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@7d
    .line 223
    const/16 v3, 0xd

    #@7f
    iput v3, v2, Landroid/os/Message;->what:I

    #@81
    .line 224
    const-string v3, "capa"

    #@83
    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@86
    .line 225
    invoke-virtual {v2, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@89
    .line 226
    invoke-static {}, Lcom/lge/ims/service/ip/IPListenerThread;->GetIPListenerThread()Lcom/lge/ims/service/ip/IPListenerThread;

    #@8c
    move-result-object v3

    #@8d
    invoke-virtual {v3}, Lcom/lge/ims/service/ip/IPListenerThread;->GetHandler()Landroid/os/Handler;

    #@90
    move-result-object v3

    #@91
    const-wide/16 v4, 0x3e8

    #@93
    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Handler;->sendMessageAtTime(Landroid/os/Message;J)Z

    #@96
    .line 228
    .end local v0           #objBundle:Landroid/os/Bundle;
    .end local v1           #objCapaInfo:Lcom/lge/ims/service/ip/CapaInfo;
    .end local v2           #objMessage:Landroid/os/Message;
    :cond_96
    return-void

    #@97
    .line 185
    .restart local v0       #objBundle:Landroid/os/Bundle;
    .restart local v1       #objCapaInfo:Lcom/lge/ims/service/ip/CapaInfo;
    .restart local v2       #objMessage:Landroid/os/Message;
    :cond_97
    invoke-virtual {v1, v5}, Lcom/lge/ims/service/ip/CapaInfo;->SetFTCapa(I)V

    #@9a
    goto :goto_2a

    #@9b
    .line 192
    :cond_9b
    invoke-virtual {v1, v5}, Lcom/lge/ims/service/ip/CapaInfo;->SetIMCapa(I)V

    #@9e
    .line 193
    if-nez p2, :cond_37

    #@a0
    .line 194
    const/16 v3, 0x194

    #@a2
    invoke-virtual {v1, v3}, Lcom/lge/ims/service/ip/CapaInfo;->SetResponseCode(I)V

    #@a5
    goto :goto_37

    #@a6
    .line 201
    :cond_a6
    invoke-virtual {v1, v5}, Lcom/lge/ims/service/ip/CapaInfo;->SetPresenceCapa(I)V

    #@a9
    goto :goto_42

    #@aa
    .line 207
    :cond_aa
    invoke-virtual {v1, v5}, Lcom/lge/ims/service/ip/CapaInfo;->SetDiscoveryPresenceCapa(I)V

    #@ad
    goto :goto_4d

    #@ae
    .line 213
    :cond_ae
    invoke-virtual {v1, v5}, Lcom/lge/ims/service/ip/CapaInfo;->SetMIMCapa(I)V

    #@b1
    goto :goto_58

    #@b2
    .line 219
    :cond_b2
    invoke-virtual {v1, v5}, Lcom/lge/ims/service/ip/CapaInfo;->SetHTTPCapa(I)V

    #@b5
    goto :goto_63
.end method
