.class public Lcom/lge/ims/service/uc/UCServiceImpl;
.super Lcom/lge/ims/service/uc/IUCService$Stub;
.source "UCServiceImpl.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "UC"


# instance fields
.field private mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

.field private mContext:Landroid/content/Context;

.field private mListener:Lcom/lge/ims/service/uc/IUCServiceListener;

.field private mNativeService:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 30
    invoke-direct {p0}, Lcom/lge/ims/service/uc/IUCService$Stub;-><init>()V

    #@4
    .line 22
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mContext:Landroid/content/Context;

    #@6
    .line 23
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@8
    .line 24
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mListener:Lcom/lge/ims/service/uc/IUCServiceListener;

    #@a
    .line 26
    const/4 v0, 0x0

    #@b
    iput v0, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@d
    .line 31
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mContext:Landroid/content/Context;

    #@f
    .line 32
    invoke-static {}, Lcom/lge/ims/service/uc/UCCallManager;->getInstance()Lcom/lge/ims/service/uc/UCCallManager;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@15
    .line 33
    iget-object v0, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@17
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mContext:Landroid/content/Context;

    #@19
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/uc/UCCallManager;->setContext(Landroid/content/Context;)V

    #@1c
    .line 34
    return-void
.end method


# virtual methods
.method public attachSession(I)Lcom/lge/ims/service/uc/IUCSession;
    .registers 7
    .parameter "sessionKey"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 105
    const-string v2, "UC"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "attachSession() :: sessionKey="

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 107
    if-nez p1, :cond_24

    #@1b
    .line 108
    const-string v2, "UC"

    #@1d
    const-string v3, "sessionKey is null"

    #@1f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    move-object v0, v1

    #@23
    .line 128
    :goto_23
    return-object v0

    #@24
    .line 112
    :cond_24
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@26
    invoke-virtual {v2, p1}, Lcom/lge/ims/service/uc/UCCallManager;->getSession(I)Lcom/lge/ims/service/uc/UCSessionImpl;

    #@29
    move-result-object v0

    #@2a
    .line 114
    .local v0, session:Lcom/lge/ims/service/uc/UCSessionImpl;
    if-nez v0, :cond_35

    #@2c
    .line 115
    const-string v2, "UC"

    #@2e
    const-string v3, "session is null"

    #@30
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    move-object v0, v1

    #@34
    .line 116
    goto :goto_23

    #@35
    .line 119
    :cond_35
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCSessionImpl;->isTerminated()Z

    #@38
    move-result v2

    #@39
    const/4 v3, 0x1

    #@3a
    if-ne v2, v3, :cond_4d

    #@3c
    .line 120
    const-string v2, "UC"

    #@3e
    const-string v3, "session is terminated"

    #@40
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 121
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCSessionImpl;->close()V

    #@46
    .line 122
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@48
    invoke-virtual {v2, v0}, Lcom/lge/ims/service/uc/UCCallManager;->closeSession(Lcom/lge/ims/service/uc/UCSessionImpl;)V

    #@4b
    move-object v0, v1

    #@4c
    .line 123
    goto :goto_23

    #@4d
    .line 126
    :cond_4d
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCSessionImpl;->attach()V

    #@50
    goto :goto_23
.end method

.method public closeSession(Lcom/lge/ims/service/uc/IUCSession;)V
    .registers 6
    .parameter "session"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 132
    const-string v1, "UC"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "closeSession() :: IUCSession="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 134
    if-nez p1, :cond_22

    #@1a
    .line 135
    const-string v1, "UC"

    #@1c
    const-string v2, "Session is null"

    #@1e
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@21
    .line 144
    :goto_21
    return-void

    #@22
    :cond_22
    move-object v0, p1

    #@23
    .line 139
    check-cast v0, Lcom/lge/ims/service/uc/UCSessionImpl;

    #@25
    .line 140
    .local v0, ucSession:Lcom/lge/ims/service/uc/UCSessionImpl;
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCSessionImpl;->close()V

    #@28
    .line 142
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@2a
    invoke-virtual {v1, v0}, Lcom/lge/ims/service/uc/UCCallManager;->closeSession(Lcom/lge/ims/service/uc/UCSessionImpl;)V

    #@2d
    goto :goto_21
.end method

.method public create()Z
    .registers 6

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 37
    const-string v3, "UC"

    #@4
    const-string v4, "create()"

    #@6
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 39
    iget v3, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@b
    if-eqz v3, :cond_2e

    #@d
    .line 40
    const-string v2, "UC"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "native is exist ("

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    iget v4, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, ")"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@2d
    .line 58
    :goto_2d
    return v1

    #@2e
    .line 44
    :cond_2e
    invoke-static {v2}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@31
    move-result v3

    #@32
    iput v3, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@34
    .line 46
    iget v3, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@36
    if-nez v3, :cond_41

    #@38
    .line 47
    const-string v1, "UC"

    #@3a
    const-string v3, "native is null"

    #@3c
    invoke-static {v1, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3f
    move v1, v2

    #@40
    .line 48
    goto :goto_2d

    #@41
    .line 51
    :cond_41
    iget v2, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@43
    invoke-static {v2, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@46
    .line 53
    new-instance v0, Landroid/content/Intent;

    #@48
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@4b
    .line 54
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "com.lge.ims.service.uc.UCSERVICE_STARTED"

    #@4d
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@50
    .line 55
    const/high16 v2, 0x1000

    #@52
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@55
    .line 56
    iget-object v2, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mContext:Landroid/content/Context;

    #@57
    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@5a
    goto :goto_2d
.end method

.method public destroy()V
    .registers 4

    #@0
    .prologue
    .line 62
    const-string v1, "UC"

    #@2
    const-string v2, "destroy()"

    #@4
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 64
    iget v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@9
    if-nez v1, :cond_c

    #@b
    .line 76
    :goto_b
    return-void

    #@c
    .line 68
    :cond_c
    iget v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@e
    invoke-static {v1, p0}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@11
    .line 69
    iget v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@13
    invoke-static {v1}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@16
    .line 71
    const/4 v1, 0x0

    #@17
    iput v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@19
    .line 73
    new-instance v0, Landroid/content/Intent;

    #@1b
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@1e
    .line 74
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.lge.ims.service.uc.UCSERVICE_STOPED"

    #@20
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@23
    .line 75
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@28
    goto :goto_b
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 13
    .parameter "parcel"

    #@0
    .prologue
    .line 153
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v2

    #@4
    .line 155
    .local v2, msg:I
    const-string v7, "UC"

    #@6
    new-instance v8, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v9, "onMessage() :: msg = "

    #@d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v8

    #@15
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v8

    #@19
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 157
    packed-switch v2, :pswitch_data_174

    #@1f
    .line 220
    const-string v7, "UC"

    #@21
    new-instance v8, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v9, "not handled msg = "

    #@28
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v8

    #@2c
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v8

    #@34
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 225
    :cond_37
    :goto_37
    return-void

    #@38
    .line 159
    :pswitch_38
    new-instance v4, Lcom/lge/ims/service/uc/IncomingUCSession;

    #@3a
    invoke-direct {v4, p1}, Lcom/lge/ims/service/uc/IncomingUCSession;-><init>(Landroid/os/Parcel;)V

    #@3d
    .line 161
    .local v4, session:Lcom/lge/ims/service/uc/IncomingUCSession;
    new-instance v5, Lcom/lge/ims/service/uc/UCSessionImpl;

    #@3f
    iget-object v7, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mContext:Landroid/content/Context;

    #@41
    iget v8, v4, Lcom/lge/ims/service/uc/IncomingUCSession;->sessionKey:I

    #@43
    invoke-direct {v5, v7, v8}, Lcom/lge/ims/service/uc/UCSessionImpl;-><init>(Landroid/content/Context;I)V

    #@46
    .line 163
    .local v5, sessionImpl:Lcom/lge/ims/service/uc/UCSessionImpl;
    iget v7, v4, Lcom/lge/ims/service/uc/IncomingUCSession;->OIPType:I

    #@48
    const/4 v8, 0x1

    #@49
    if-ne v7, v8, :cond_71

    #@4b
    .line 164
    iget-object v7, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@4d
    iget v8, v4, Lcom/lge/ims/service/uc/IncomingUCSession;->sessionKey:I

    #@4f
    iget-object v9, v4, Lcom/lge/ims/service/uc/IncomingUCSession;->sessInfo:Lcom/lge/ims/service/uc/SessInfo;

    #@51
    iget v9, v9, Lcom/lge/ims/service/uc/SessInfo;->SessionType:I

    #@53
    iget-object v10, v4, Lcom/lge/ims/service/uc/IncomingUCSession;->callerPartyNum:Ljava/lang/String;

    #@55
    invoke-virtual {v7, v5, v8, v9, v10}, Lcom/lge/ims/service/uc/UCCallManager;->incomingSession(Lcom/lge/ims/service/uc/UCSessionImpl;IILjava/lang/String;)V

    #@58
    .line 169
    :goto_58
    new-instance v1, Landroid/content/Intent;

    #@5a
    const-string v7, "com.lge.ims.service.uc.IncomingUCSession"

    #@5c
    invoke-direct {v1, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5f
    .line 170
    .local v1, intent:Landroid/content/Intent;
    const-string v7, "incomingUCSession"

    #@61
    invoke-virtual {v1, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@64
    .line 171
    iget-object v7, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mContext:Landroid/content/Context;

    #@66
    invoke-virtual {v7, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@69
    .line 173
    const-string v7, "UC"

    #@6b
    const-string v8, "send INCOMING_SESSION Intent"

    #@6d
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@70
    goto :goto_37

    #@71
    .line 166
    .end local v1           #intent:Landroid/content/Intent;
    :cond_71
    iget-object v7, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@73
    iget v8, v4, Lcom/lge/ims/service/uc/IncomingUCSession;->sessionKey:I

    #@75
    iget-object v9, v4, Lcom/lge/ims/service/uc/IncomingUCSession;->sessInfo:Lcom/lge/ims/service/uc/SessInfo;

    #@77
    iget v9, v9, Lcom/lge/ims/service/uc/SessInfo;->SessionType:I

    #@79
    const-string v10, ""

    #@7b
    invoke-virtual {v7, v5, v8, v9, v10}, Lcom/lge/ims/service/uc/UCCallManager;->incomingSession(Lcom/lge/ims/service/uc/UCSessionImpl;IILjava/lang/String;)V

    #@7e
    goto :goto_58

    #@7f
    .line 178
    .end local v4           #session:Lcom/lge/ims/service/uc/IncomingUCSession;
    .end local v5           #sessionImpl:Lcom/lge/ims/service/uc/UCSessionImpl;
    :pswitch_7f
    const-string v7, "UC"

    #@81
    const-string v8, "IUUCService.SERVICE_CHANGE"

    #@83
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@86
    .line 180
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@89
    move-result v6

    #@8a
    .line 181
    .local v6, status:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@8d
    move-result v3

    #@8e
    .line 183
    .local v3, reason:I
    :try_start_8e
    iget-object v7, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mListener:Lcom/lge/ims/service/uc/IUCServiceListener;

    #@90
    if-eqz v7, :cond_b9

    #@92
    .line 184
    const-string v7, "UC"

    #@94
    new-instance v8, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v9, "status = "

    #@9b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v8

    #@9f
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v8

    #@a3
    const-string v9, " , reason = "

    #@a5
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v8

    #@a9
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v8

    #@ad
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v8

    #@b1
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@b4
    .line 185
    iget-object v7, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mListener:Lcom/lge/ims/service/uc/IUCServiceListener;

    #@b6
    invoke-interface {v7, v6, v3}, Lcom/lge/ims/service/uc/IUCServiceListener;->changedStatus(II)V
    :try_end_b9
    .catch Landroid/os/RemoteException; {:try_start_8e .. :try_end_b9} :catch_c2
    .catch Ljava/lang/Exception; {:try_start_8e .. :try_end_b9} :catch_df

    #@b9
    .line 195
    :cond_b9
    :goto_b9
    invoke-static {}, Lcom/lge/ims/service/uc/UCCallManager;->getInstance()Lcom/lge/ims/service/uc/UCCallManager;

    #@bc
    move-result-object v7

    #@bd
    invoke-virtual {v7, v6}, Lcom/lge/ims/service/uc/UCCallManager;->updateUcReg(I)V

    #@c0
    goto/16 :goto_37

    #@c2
    .line 187
    :catch_c2
    move-exception v0

    #@c3
    .line 188
    .local v0, e:Landroid/os/RemoteException;
    const-string v7, "UC"

    #@c5
    new-instance v8, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v9, "RemoteException ::"

    #@cc
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v8

    #@d0
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v8

    #@d4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v8

    #@d8
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@db
    .line 189
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@de
    goto :goto_b9

    #@df
    .line 190
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_df
    move-exception v0

    #@e0
    .line 191
    .local v0, e:Ljava/lang/Exception;
    const-string v7, "UC"

    #@e2
    new-instance v8, Ljava/lang/StringBuilder;

    #@e4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@e7
    const-string v9, "Exception ::"

    #@e9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ec
    move-result-object v8

    #@ed
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v8

    #@f1
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v8

    #@f5
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@f8
    .line 192
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@fb
    goto :goto_b9

    #@fc
    .line 200
    .end local v0           #e:Ljava/lang/Exception;
    .end local v3           #reason:I
    .end local v6           #status:I
    :pswitch_fc
    const-string v7, "UC"

    #@fe
    const-string v8, "IUUCService.E_SERVICE_CHANGED"

    #@100
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@103
    .line 202
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@106
    move-result v6

    #@107
    .line 203
    .restart local v6       #status:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@10a
    move-result v3

    #@10b
    .line 205
    .restart local v3       #reason:I
    :try_start_10b
    iget-object v7, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mListener:Lcom/lge/ims/service/uc/IUCServiceListener;

    #@10d
    if-eqz v7, :cond_37

    #@10f
    .line 206
    const-string v7, "UC"

    #@111
    new-instance v8, Ljava/lang/StringBuilder;

    #@113
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@116
    const-string v9, "status = "

    #@118
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v8

    #@11c
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v8

    #@120
    const-string v9, " , reason = "

    #@122
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v8

    #@126
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@129
    move-result-object v8

    #@12a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12d
    move-result-object v8

    #@12e
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@131
    .line 207
    iget-object v7, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mListener:Lcom/lge/ims/service/uc/IUCServiceListener;

    #@133
    invoke-interface {v7, v6, v3}, Lcom/lge/ims/service/uc/IUCServiceListener;->changedEStatus(II)V
    :try_end_136
    .catch Landroid/os/RemoteException; {:try_start_10b .. :try_end_136} :catch_138
    .catch Ljava/lang/Exception; {:try_start_10b .. :try_end_136} :catch_156

    #@136
    goto/16 :goto_37

    #@138
    .line 209
    :catch_138
    move-exception v0

    #@139
    .line 210
    .local v0, e:Landroid/os/RemoteException;
    const-string v7, "UC"

    #@13b
    new-instance v8, Ljava/lang/StringBuilder;

    #@13d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@140
    const-string v9, "RemoteException ::"

    #@142
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v8

    #@146
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v8

    #@14a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14d
    move-result-object v8

    #@14e
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@151
    .line 211
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    #@154
    goto/16 :goto_37

    #@156
    .line 212
    .end local v0           #e:Landroid/os/RemoteException;
    :catch_156
    move-exception v0

    #@157
    .line 213
    .local v0, e:Ljava/lang/Exception;
    const-string v7, "UC"

    #@159
    new-instance v8, Ljava/lang/StringBuilder;

    #@15b
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@15e
    const-string v9, "Exception ::"

    #@160
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v8

    #@164
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v8

    #@168
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v8

    #@16c
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@16f
    .line 214
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@172
    goto/16 :goto_37

    #@174
    .line 157
    :pswitch_data_174
    .packed-switch 0x44d
        :pswitch_7f
        :pswitch_fc
        :pswitch_38
    .end packed-switch
.end method

.method public openSession(I)Lcom/lge/ims/service/uc/IUCSession;
    .registers 6
    .parameter "callType"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 79
    const-string v1, "UC"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "openSession() :: callType="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 81
    iget v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@1b
    if-nez v1, :cond_37

    #@1d
    .line 82
    const-string v1, "UC"

    #@1f
    const-string v2, "native is null"

    #@21
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 83
    const/4 v1, 0x0

    #@25
    invoke-static {v1}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@28
    move-result v1

    #@29
    iput v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@2b
    .line 85
    iget v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mNativeService:I

    #@2d
    if-nez v1, :cond_37

    #@2f
    .line 86
    const-string v1, "UC"

    #@31
    const-string v2, "native is null"

    #@33
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    .line 101
    :goto_36
    return-object v0

    #@37
    .line 91
    :cond_37
    if-eqz p1, :cond_58

    #@39
    .line 92
    const-string v1, "UC"

    #@3b
    new-instance v2, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v3, "callType ("

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    const-string v3, ") is not supported"

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@57
    goto :goto_36

    #@58
    .line 96
    :cond_58
    new-instance v0, Lcom/lge/ims/service/uc/UCSessionImpl;

    #@5a
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mContext:Landroid/content/Context;

    #@5c
    invoke-direct {v0, v1}, Lcom/lge/ims/service/uc/UCSessionImpl;-><init>(Landroid/content/Context;)V

    #@5f
    .line 98
    .local v0, session:Lcom/lge/ims/service/uc/UCSessionImpl;
    iget-object v1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mCallMngr:Lcom/lge/ims/service/uc/UCCallManager;

    #@61
    invoke-virtual {v1, v0}, Lcom/lge/ims/service/uc/UCCallManager;->openSession(Lcom/lge/ims/service/uc/UCSessionImpl;)V

    #@64
    .line 99
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/UCSessionImpl;->attach()V

    #@67
    goto :goto_36
.end method

.method public setListener(Lcom/lge/ims/service/uc/IUCServiceListener;)V
    .registers 5
    .parameter "listener"

    #@0
    .prologue
    .line 148
    const-string v0, "UC"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setListener() :: listener = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 149
    iput-object p1, p0, Lcom/lge/ims/service/uc/UCServiceImpl;->mListener:Lcom/lge/ims/service/uc/IUCServiceListener;

    #@1a
    .line 150
    return-void
.end method
