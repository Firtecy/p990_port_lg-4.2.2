.class public abstract Lcom/lge/ims/service/im/IChat$Stub;
.super Landroid/os/Binder;
.source "IChat.java"

# interfaces
.implements Lcom/lge/ims/service/im/IChat;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/im/IChat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/im/IChat$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.im.IChat"

.field static final TRANSACTION_accept:I = 0x3

.field static final TRANSACTION_addParticipants:I = 0x7

.field static final TRANSACTION_close:I = 0x6

.field static final TRANSACTION_extendToConference:I = 0x8

.field static final TRANSACTION_leaveConference:I = 0xa

.field static final TRANSACTION_reject:I = 0x4

.field static final TRANSACTION_rejectDeniedInvitation:I = 0x5

.field static final TRANSACTION_rejoinConference:I = 0x9

.field static final TRANSACTION_sendChatInvitation:I = 0x1

.field static final TRANSACTION_sendComposingIndicator:I = 0xc

.field static final TRANSACTION_sendConferenceInvitation:I = 0x2

.field static final TRANSACTION_sendDisplayNotification:I = 0xd

.field static final TRANSACTION_sendMessage:I = 0xb

.field static final TRANSACTION_setListener:I = 0xe


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 15
    const-string v0, "com.lge.ims.service.im.IChat"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/im/IChat$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IChat;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 23
    if-nez p0, :cond_4

    #@2
    .line 24
    const/4 v0, 0x0

    #@3
    .line 30
    :goto_3
    return-object v0

    #@4
    .line 26
    :cond_4
    const-string v1, "com.lge.ims.service.im.IChat"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 27
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/im/IChat;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 28
    check-cast v0, Lcom/lge/ims/service/im/IChat;

    #@12
    goto :goto_3

    #@13
    .line 30
    :cond_13
    new-instance v0, Lcom/lge/ims/service/im/IChat$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/im/IChat$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 13
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 38
    sparse-switch p1, :sswitch_data_16e

    #@5
    .line 216
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@8
    move-result v6

    #@9
    :goto_9
    return v6

    #@a
    .line 42
    :sswitch_a
    const-string v0, "com.lge.ims.service.im.IChat"

    #@c
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@f
    goto :goto_9

    #@10
    .line 47
    :sswitch_10
    const-string v0, "com.lge.ims.service.im.IChat"

    #@12
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15
    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    .line 51
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1c
    move-result v0

    #@1d
    if-eqz v0, :cond_32

    #@1f
    .line 52
    sget-object v0, Lcom/lge/ims/service/im/IMMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    #@21
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@24
    move-result-object v2

    #@25
    check-cast v2, Lcom/lge/ims/service/im/IMMessage;

    #@27
    .line 58
    .local v2, _arg1:Lcom/lge/ims/service/im/IMMessage;
    :goto_27
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    .line 59
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/lge/ims/service/im/IChat$Stub;->sendChatInvitation(Ljava/lang/String;Lcom/lge/ims/service/im/IMMessage;Ljava/lang/String;)V

    #@2e
    .line 60
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@31
    goto :goto_9

    #@32
    .line 55
    .end local v2           #_arg1:Lcom/lge/ims/service/im/IMMessage;
    .end local v3           #_arg2:Ljava/lang/String;
    :cond_32
    const/4 v2, 0x0

    #@33
    .restart local v2       #_arg1:Lcom/lge/ims/service/im/IMMessage;
    goto :goto_27

    #@34
    .line 65
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Lcom/lge/ims/service/im/IMMessage;
    :sswitch_34
    const-string v0, "com.lge.ims.service.im.IChat"

    #@36
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@39
    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@3c
    move-result-object v1

    #@3d
    .line 69
    .restart local v1       #_arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    .line 71
    .local v2, _arg1:[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@44
    move-result v0

    #@45
    if-eqz v0, :cond_5f

    #@47
    .line 72
    sget-object v0, Lcom/lge/ims/service/im/IMMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    #@49
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@4c
    move-result-object v3

    #@4d
    check-cast v3, Lcom/lge/ims/service/im/IMMessage;

    #@4f
    .line 78
    .local v3, _arg2:Lcom/lge/ims/service/im/IMMessage;
    :goto_4f
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    .line 80
    .local v4, _arg3:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    .local v5, _arg4:Ljava/lang/String;
    move-object v0, p0

    #@58
    .line 81
    invoke-virtual/range {v0 .. v5}, Lcom/lge/ims/service/im/IChat$Stub;->sendConferenceInvitation(Ljava/lang/String;[Ljava/lang/String;Lcom/lge/ims/service/im/IMMessage;Ljava/lang/String;Ljava/lang/String;)V

    #@5b
    .line 82
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@5e
    goto :goto_9

    #@5f
    .line 75
    .end local v3           #_arg2:Lcom/lge/ims/service/im/IMMessage;
    .end local v4           #_arg3:Ljava/lang/String;
    .end local v5           #_arg4:Ljava/lang/String;
    :cond_5f
    const/4 v3, 0x0

    #@60
    .restart local v3       #_arg2:Lcom/lge/ims/service/im/IMMessage;
    goto :goto_4f

    #@61
    .line 87
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:[Ljava/lang/String;
    .end local v3           #_arg2:Lcom/lge/ims/service/im/IMMessage;
    :sswitch_61
    const-string v0, "com.lge.ims.service.im.IChat"

    #@63
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@66
    .line 88
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IChat$Stub;->accept()V

    #@69
    .line 89
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@6c
    goto :goto_9

    #@6d
    .line 94
    :sswitch_6d
    const-string v0, "com.lge.ims.service.im.IChat"

    #@6f
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@72
    .line 95
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IChat$Stub;->reject()V

    #@75
    .line 96
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@78
    goto :goto_9

    #@79
    .line 101
    :sswitch_79
    const-string v0, "com.lge.ims.service.im.IChat"

    #@7b
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@7e
    .line 102
    invoke-virtual {p0}, Lcom/lge/ims/service/im/IChat$Stub;->rejectDeniedInvitation()V

    #@81
    .line 103
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@84
    goto :goto_9

    #@85
    .line 108
    :sswitch_85
    const-string v7, "com.lge.ims.service.im.IChat"

    #@87
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@8a
    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@8d
    move-result v7

    #@8e
    if-eqz v7, :cond_99

    #@90
    move v1, v6

    #@91
    .line 111
    .local v1, _arg0:Z
    :goto_91
    invoke-virtual {p0, v1}, Lcom/lge/ims/service/im/IChat$Stub;->close(Z)V

    #@94
    .line 112
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@97
    goto/16 :goto_9

    #@99
    .end local v1           #_arg0:Z
    :cond_99
    move v1, v0

    #@9a
    .line 110
    goto :goto_91

    #@9b
    .line 117
    :sswitch_9b
    const-string v0, "com.lge.ims.service.im.IChat"

    #@9d
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@a0
    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@a3
    move-result-object v1

    #@a4
    .line 120
    .local v1, _arg0:[Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/lge/ims/service/im/IChat$Stub;->addParticipants([Ljava/lang/String;)V

    #@a7
    .line 121
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@aa
    goto/16 :goto_9

    #@ac
    .line 126
    .end local v1           #_arg0:[Ljava/lang/String;
    :sswitch_ac
    const-string v0, "com.lge.ims.service.im.IChat"

    #@ae
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@b1
    .line 128
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@b4
    move-result-object v1

    #@b5
    .line 130
    .restart local v1       #_arg0:[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b8
    move-result v0

    #@b9
    if-eqz v0, :cond_d3

    #@bb
    .line 131
    sget-object v0, Lcom/lge/ims/service/im/IMMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    #@bd
    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c0
    move-result-object v2

    #@c1
    check-cast v2, Lcom/lge/ims/service/im/IMMessage;

    #@c3
    .line 137
    .local v2, _arg1:Lcom/lge/ims/service/im/IMMessage;
    :goto_c3
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@c6
    move-result-object v3

    #@c7
    .line 139
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ca
    move-result-object v4

    #@cb
    .line 140
    .restart local v4       #_arg3:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/lge/ims/service/im/IChat$Stub;->extendToConference([Ljava/lang/String;Lcom/lge/ims/service/im/IMMessage;Ljava/lang/String;Ljava/lang/String;)V

    #@ce
    .line 141
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d1
    goto/16 :goto_9

    #@d3
    .line 134
    .end local v2           #_arg1:Lcom/lge/ims/service/im/IMMessage;
    .end local v3           #_arg2:Ljava/lang/String;
    .end local v4           #_arg3:Ljava/lang/String;
    :cond_d3
    const/4 v2, 0x0

    #@d4
    .restart local v2       #_arg1:Lcom/lge/ims/service/im/IMMessage;
    goto :goto_c3

    #@d5
    .line 146
    .end local v1           #_arg0:[Ljava/lang/String;
    .end local v2           #_arg1:Lcom/lge/ims/service/im/IMMessage;
    :sswitch_d5
    const-string v0, "com.lge.ims.service.im.IChat"

    #@d7
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@da
    .line 148
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@dd
    move-result-object v1

    #@de
    .line 150
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@e1
    move-result-object v2

    #@e2
    .line 151
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/lge/ims/service/im/IChat$Stub;->rejoinConference(Ljava/lang/String;Ljava/lang/String;)V

    #@e5
    .line 152
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@e8
    goto/16 :goto_9

    #@ea
    .line 157
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_ea
    const-string v0, "com.lge.ims.service.im.IChat"

    #@ec
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ef
    .line 159
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@f2
    move-result-object v1

    #@f3
    .line 161
    .local v1, _arg0:[Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@f6
    move-result-object v2

    #@f7
    .line 162
    .restart local v2       #_arg1:Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/lge/ims/service/im/IChat$Stub;->leaveConference([Ljava/lang/String;Ljava/lang/String;)V

    #@fa
    .line 163
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fd
    goto/16 :goto_9

    #@ff
    .line 168
    .end local v1           #_arg0:[Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    :sswitch_ff
    const-string v7, "com.lge.ims.service.im.IChat"

    #@101
    invoke-virtual {p2, v7}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@104
    .line 170
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@107
    move-result v7

    #@108
    if-eqz v7, :cond_128

    #@10a
    .line 171
    sget-object v7, Lcom/lge/ims/service/im/IMMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    #@10c
    invoke-interface {v7, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@10f
    move-result-object v1

    #@110
    check-cast v1, Lcom/lge/ims/service/im/IMMessage;

    #@112
    .line 177
    .local v1, _arg0:Lcom/lge/ims/service/im/IMMessage;
    :goto_112
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@115
    move-result v7

    #@116
    if-eqz v7, :cond_12a

    #@118
    move v2, v6

    #@119
    .line 179
    .local v2, _arg1:Z
    :goto_119
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11c
    move-result v7

    #@11d
    if-eqz v7, :cond_12c

    #@11f
    move v3, v6

    #@120
    .line 180
    .local v3, _arg2:Z
    :goto_120
    invoke-virtual {p0, v1, v2, v3}, Lcom/lge/ims/service/im/IChat$Stub;->sendMessage(Lcom/lge/ims/service/im/IMMessage;ZZ)V

    #@123
    .line 181
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@126
    goto/16 :goto_9

    #@128
    .line 174
    .end local v1           #_arg0:Lcom/lge/ims/service/im/IMMessage;
    .end local v2           #_arg1:Z
    .end local v3           #_arg2:Z
    :cond_128
    const/4 v1, 0x0

    #@129
    .restart local v1       #_arg0:Lcom/lge/ims/service/im/IMMessage;
    goto :goto_112

    #@12a
    :cond_12a
    move v2, v0

    #@12b
    .line 177
    goto :goto_119

    #@12c
    .restart local v2       #_arg1:Z
    :cond_12c
    move v3, v0

    #@12d
    .line 179
    goto :goto_120

    #@12e
    .line 186
    .end local v1           #_arg0:Lcom/lge/ims/service/im/IMMessage;
    .end local v2           #_arg1:Z
    :sswitch_12e
    const-string v0, "com.lge.ims.service.im.IChat"

    #@130
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@133
    .line 188
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@136
    move-result v1

    #@137
    .line 189
    .local v1, _arg0:I
    invoke-virtual {p0, v1}, Lcom/lge/ims/service/im/IChat$Stub;->sendComposingIndicator(I)V

    #@13a
    .line 190
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13d
    goto/16 :goto_9

    #@13f
    .line 195
    .end local v1           #_arg0:I
    :sswitch_13f
    const-string v0, "com.lge.ims.service.im.IChat"

    #@141
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@144
    .line 197
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@147
    move-result-object v1

    #@148
    .line 199
    .local v1, _arg0:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14b
    move-result-object v2

    #@14c
    .line 201
    .local v2, _arg1:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@14f
    move-result-object v3

    #@150
    .line 202
    .local v3, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v1, v2, v3}, Lcom/lge/ims/service/im/IChat$Stub;->sendDisplayNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@153
    .line 203
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@156
    goto/16 :goto_9

    #@158
    .line 208
    .end local v1           #_arg0:Ljava/lang/String;
    .end local v2           #_arg1:Ljava/lang/String;
    .end local v3           #_arg2:Ljava/lang/String;
    :sswitch_158
    const-string v0, "com.lge.ims.service.im.IChat"

    #@15a
    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@15d
    .line 210
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    #@160
    move-result-object v0

    #@161
    invoke-static {v0}, Lcom/lge/ims/service/im/IChatListener$Stub;->asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/im/IChatListener;

    #@164
    move-result-object v1

    #@165
    .line 211
    .local v1, _arg0:Lcom/lge/ims/service/im/IChatListener;
    invoke-virtual {p0, v1}, Lcom/lge/ims/service/im/IChat$Stub;->setListener(Lcom/lge/ims/service/im/IChatListener;)V

    #@168
    .line 212
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@16b
    goto/16 :goto_9

    #@16d
    .line 38
    nop

    #@16e
    :sswitch_data_16e
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_34
        0x3 -> :sswitch_61
        0x4 -> :sswitch_6d
        0x5 -> :sswitch_79
        0x6 -> :sswitch_85
        0x7 -> :sswitch_9b
        0x8 -> :sswitch_ac
        0x9 -> :sswitch_d5
        0xa -> :sswitch_ea
        0xb -> :sswitch_ff
        0xc -> :sswitch_12e
        0xd -> :sswitch_13f
        0xe -> :sswitch_158
        0x5f4e5446 -> :sswitch_a
    .end sparse-switch
.end method
