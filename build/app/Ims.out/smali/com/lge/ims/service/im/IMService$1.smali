.class Lcom/lge/ims/service/im/IMService$1;
.super Landroid/os/Handler;
.source "IMService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/im/IMService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/im/IMService;


# direct methods
.method constructor <init>(Lcom/lge/ims/service/im/IMService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 18
    iput-object p1, p0, Lcom/lge/ims/service/im/IMService$1;->this$0:Lcom/lge/ims/service/im/IMService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    .line 20
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v6, ""

    #@7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v5

    #@b
    iget v6, p1, Landroid/os/Message;->what:I

    #@d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v5

    #@15
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->d(Ljava/lang/String;)V

    #@18
    .line 22
    iget v5, p1, Landroid/os/Message;->what:I

    #@1a
    iget-object v6, p0, Lcom/lge/ims/service/im/IMService$1;->this$0:Lcom/lge/ims/service/im/IMService;

    #@1c
    invoke-static {v6}, Lcom/lge/ims/service/im/IMService;->access$000(Lcom/lge/ims/service/im/IMService;)I

    #@1f
    move-result v6

    #@20
    if-ne v5, v6, :cond_41

    #@22
    .line 24
    :try_start_22
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24
    check-cast v5, Lcom/lge/ims/service/im/IChat;

    #@26
    move-object v0, v5

    #@27
    check-cast v0, Lcom/lge/ims/service/im/IChat;

    #@29
    move-object v2, v0

    #@2a
    .line 25
    .local v2, iChat:Lcom/lge/ims/service/im/IChat;
    const/4 v5, 0x0

    #@2b
    invoke-interface {v2, v5}, Lcom/lge/ims/service/im/IChat;->close(Z)V

    #@2e
    .line 27
    invoke-static {}, Lcom/lge/ims/service/im/IMServiceManager;->getInstance()Lcom/lge/ims/service/im/IMServiceManager;

    #@31
    move-result-object v5

    #@32
    invoke-virtual {v5}, Lcom/lge/ims/service/im/IMServiceManager;->getService()Lcom/lge/ims/service/im/IMServiceImpl;

    #@35
    move-result-object v4

    #@36
    .line 29
    .local v4, imService:Lcom/lge/ims/service/im/IMServiceImpl;
    if-eqz v4, :cond_3b

    #@38
    .line 30
    invoke-virtual {v4, v2}, Lcom/lge/ims/service/im/IMServiceImpl;->closeChat(Lcom/lge/ims/service/im/IChat;)Z
    :try_end_3b
    .catch Landroid/os/RemoteException; {:try_start_22 .. :try_end_3b} :catch_3c

    #@3b
    .line 53
    .end local v2           #iChat:Lcom/lge/ims/service/im/IChat;
    .end local v4           #imService:Lcom/lge/ims/service/im/IMServiceImpl;
    :cond_3b
    :goto_3b
    return-void

    #@3c
    .line 32
    :catch_3c
    move-exception v1

    #@3d
    .line 33
    .local v1, e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@40
    goto :goto_3b

    #@41
    .line 36
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_41
    iget v5, p1, Landroid/os/Message;->what:I

    #@43
    iget-object v6, p0, Lcom/lge/ims/service/im/IMService$1;->this$0:Lcom/lge/ims/service/im/IMService;

    #@45
    invoke-static {v6}, Lcom/lge/ims/service/im/IMService;->access$100(Lcom/lge/ims/service/im/IMService;)I

    #@48
    move-result v6

    #@49
    if-ne v5, v6, :cond_69

    #@4b
    .line 38
    :try_start_4b
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4d
    check-cast v5, Lcom/lge/ims/service/im/IFileTransfer;

    #@4f
    move-object v0, v5

    #@50
    check-cast v0, Lcom/lge/ims/service/im/IFileTransfer;

    #@52
    move-object v3, v0

    #@53
    .line 39
    .local v3, iFileTransfer:Lcom/lge/ims/service/im/IFileTransfer;
    invoke-interface {v3}, Lcom/lge/ims/service/im/IFileTransfer;->cancel()V

    #@56
    .line 41
    invoke-static {}, Lcom/lge/ims/service/im/IMServiceManager;->getInstance()Lcom/lge/ims/service/im/IMServiceManager;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5}, Lcom/lge/ims/service/im/IMServiceManager;->getService()Lcom/lge/ims/service/im/IMServiceImpl;

    #@5d
    move-result-object v4

    #@5e
    .line 43
    .restart local v4       #imService:Lcom/lge/ims/service/im/IMServiceImpl;
    if-eqz v4, :cond_3b

    #@60
    .line 44
    invoke-virtual {v4, v3}, Lcom/lge/ims/service/im/IMServiceImpl;->closeFileTransfer(Lcom/lge/ims/service/im/IFileTransfer;)Z
    :try_end_63
    .catch Landroid/os/RemoteException; {:try_start_4b .. :try_end_63} :catch_64

    #@63
    goto :goto_3b

    #@64
    .line 46
    .end local v3           #iFileTransfer:Lcom/lge/ims/service/im/IFileTransfer;
    .end local v4           #imService:Lcom/lge/ims/service/im/IMServiceImpl;
    :catch_64
    move-exception v1

    #@65
    .line 47
    .restart local v1       #e:Landroid/os/RemoteException;
    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    #@68
    goto :goto_3b

    #@69
    .line 51
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_69
    const-string v5, "invalid msg"

    #@6b
    invoke-static {v5}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@6e
    goto :goto_3b
.end method
