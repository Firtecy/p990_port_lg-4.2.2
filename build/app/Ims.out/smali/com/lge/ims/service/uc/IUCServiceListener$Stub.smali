.class public abstract Lcom/lge/ims/service/uc/IUCServiceListener$Stub;
.super Landroid/os/Binder;
.source "IUCServiceListener.java"

# interfaces
.implements Lcom/lge/ims/service/uc/IUCServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUCServiceListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/IUCServiceListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.uc.IUCServiceListener"

.field static final TRANSACTION_changedEStatus:I = 0x2

.field static final TRANSACTION_changedStatus:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.lge.ims.service.uc.IUCServiceListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/uc/IUCServiceListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCServiceListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.lge.ims.service.uc.IUCServiceListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/uc/IUCServiceListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/lge/ims/service/uc/IUCServiceListener;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/lge/ims/service/uc/IUCServiceListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/IUCServiceListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 9
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_38

    #@4
    .line 73
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v2

    #@8
    :goto_8
    return v2

    #@9
    .line 47
    :sswitch_9
    const-string v3, "com.lge.ims.service.uc.IUCServiceListener"

    #@b
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v3, "com.lge.ims.service.uc.IUCServiceListener"

    #@11
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v0

    #@18
    .line 56
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1b
    move-result v1

    #@1c
    .line 57
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCServiceListener$Stub;->changedStatus(II)V

    #@1f
    .line 58
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@22
    goto :goto_8

    #@23
    .line 63
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_23
    const-string v3, "com.lge.ims.service.uc.IUCServiceListener"

    #@25
    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28
    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2b
    move-result v0

    #@2c
    .line 67
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2f
    move-result v1

    #@30
    .line 68
    .restart local v1       #_arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCServiceListener$Stub;->changedEStatus(II)V

    #@33
    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@36
    goto :goto_8

    #@37
    .line 43
    nop

    #@38
    :sswitch_data_38
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_23
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
