.class public Lcom/lge/ims/service/ac/ACService;
.super Ljava/lang/Object;
.source "ACService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/ac/ACService$1;,
        Lcom/lge/ims/service/ac/ACService$ACServiceThread;,
        Lcom/lge/ims/service/ac/ACService$ACServiceHandler;
    }
.end annotation


# static fields
.field private static final ACSI_REQUEST_AIRPLANE_MODE_OFF:I = 0x2

.field private static final ACSI_REQUEST_NORMAL:I = 0x0

.field private static final ACSI_REQUEST_VALIDITY_PERIOD_EXPIRED:I = 0x1

.field private static final EVENT_AIRPLANE_MODE_CHANGED:I = 0x6a

.field private static final EVENT_CONFIGURATION_RETRIEVAL_BY_AIRPLANE_MODE_OFF:I = 0x6b

.field private static final EVENT_DATA_STATE_CHANGED:I = 0x65

.field private static final EVENT_JOIN_SERVICE:I = 0x7a

.field private static final EVENT_JOIN_SERVICES:I = 0x79

.field private static final EVENT_RECOVERY_TIMER_EXPIRED:I = 0x67

.field private static final EVENT_REQUEST_CONFIGURATION_RETRIEVAL:I = 0xc9

.field private static final EVENT_RETRY_CONFIGURATION_RETRIEVAL:I = 0x66

.field private static final EVENT_SIM_STATE_CHANGED:I = 0x69

.field private static final EVENT_VALIDITY_TIMER_EXPIRED:I = 0x68

.field private static final MAX_RETRY_COUNT_FOR_JOIN:I = 0x5

.field private static final SERVICE_PSVT:I = 0x2

.field private static final SERVICE_VOLTE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ACService"

.field private static mACService:Lcom/lge/ims/service/ac/ACService;


# instance fields
.field private mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

.field private mACSThread:Lcom/lge/ims/service/ac/ACService$ACServiceThread;

.field private mAKASupportedForAuthType:Z

.field private mConfigurationAccessAllowed:Z

.field private mConfigurationAccessAllowedRegistrants:Landroid/os/RegistrantList;

.field private mConfigurationRetrievalRequiredByAirplaneModeOff:Z

.field private mConfigurationUpdateCompletedRegistrants:Landroid/os/RegistrantList;

.field private mContext:Landroid/content/Context;

.field private mRecoveryTimerId:I

.field private mRecoveryTimerInterval:I

.field private mResultCode:I

.field private mResultCodeForJoin:I

.field private mRetryCount:I

.field private mRetryCountForJoin:I

.field private mRetryIntervals:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mServerInterworkingDone:Z

.field private mServerInterworkingRequest:I

.field private mServerInterworkingResult:I

.field private mServerInterworkingResultRegistrants:Landroid/os/RegistrantList;

.field private mTimestampForServerInterworkingResult:Ljava/lang/String;

.field private mValidityTimerId:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 58
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/ac/ACService;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v2, -0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 91
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 59
    iput-object v3, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@8
    .line 60
    new-instance v0, Landroid/os/RegistrantList;

    #@a
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@d
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationUpdateCompletedRegistrants:Landroid/os/RegistrantList;

    #@f
    .line 61
    new-instance v0, Landroid/os/RegistrantList;

    #@11
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@14
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationAccessAllowedRegistrants:Landroid/os/RegistrantList;

    #@16
    .line 63
    new-instance v0, Landroid/os/RegistrantList;

    #@18
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1b
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResultRegistrants:Landroid/os/RegistrantList;

    #@1d
    .line 64
    iput-object v3, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@1f
    .line 65
    iput-object v3, p0, Lcom/lge/ims/service/ac/ACService;->mACSThread:Lcom/lge/ims/service/ac/ACService$ACServiceThread;

    #@21
    .line 67
    new-instance v0, Ljava/util/ArrayList;

    #@23
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@26
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@28
    .line 68
    iput v1, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@2a
    .line 70
    iput v2, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerId:I

    #@2c
    .line 72
    iput v2, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerInterval:I

    #@2e
    .line 73
    iput v2, p0, Lcom/lge/ims/service/ac/ACService;->mValidityTimerId:I

    #@30
    .line 74
    iput v1, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@32
    .line 76
    iput v2, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResult:I

    #@34
    .line 77
    iput-object v3, p0, Lcom/lge/ims/service/ac/ACService;->mTimestampForServerInterworkingResult:Ljava/lang/String;

    #@36
    .line 78
    iput-boolean v1, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingDone:Z

    #@38
    .line 79
    iput-boolean v1, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationAccessAllowed:Z

    #@3a
    .line 80
    iput-boolean v1, p0, Lcom/lge/ims/service/ac/ACService;->mAKASupportedForAuthType:Z

    #@3c
    .line 81
    iput-boolean v1, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationRetrievalRequiredByAirplaneModeOff:Z

    #@3e
    .line 83
    const/4 v0, 0x5

    #@3f
    iput v0, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCountForJoin:I

    #@41
    .line 85
    iput v1, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@43
    .line 87
    iput v1, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@45
    .line 92
    new-instance v0, Lcom/lge/ims/service/ac/ACService$ACServiceThread;

    #@47
    invoke-direct {v0, p0}, Lcom/lge/ims/service/ac/ACService$ACServiceThread;-><init>(Lcom/lge/ims/service/ac/ACService;)V

    #@4a
    iput-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mACSThread:Lcom/lge/ims/service/ac/ACService$ACServiceThread;

    #@4c
    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/service/ac/ACService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->retrieveConfigurationOnDataConnected()V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/lge/ims/service/ac/ACService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->retrieveConfiguration()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/lge/ims/service/ac/ACService;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$1102(Lcom/lge/ims/service/ac/ACService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationAccessAllowed:Z

    #@2
    return p1
.end method

.method static synthetic access$1200(Lcom/lge/ims/service/ac/ACService;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationAccessAllowedRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$1302(Lcom/lge/ims/service/ac/ACService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationRetrievalRequiredByAirplaneModeOff:Z

    #@2
    return p1
.end method

.method static synthetic access$1400(Lcom/lge/ims/service/ac/ACService;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ac/ACService;->joinService(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1500(Lcom/lge/ims/service/ac/ACService;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$1502(Lcom/lge/ims/service/ac/ACService;Lcom/lge/ims/service/ac/ACService$ACServiceHandler;)Lcom/lge/ims/service/ac/ACService$ACServiceHandler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput-object p1, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@2
    return-object p1
.end method

.method static synthetic access$202(Lcom/lge/ims/service/ac/ACService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput p1, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/lge/ims/service/ac/ACService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingDone:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Lcom/lge/ims/service/ac/ACService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingDone:Z

    #@2
    return p1
.end method

.method static synthetic access$402(Lcom/lge/ims/service/ac/ACService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput p1, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/lge/ims/service/ac/ACService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/lge/ims/service/ac/ACService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ac/ACService;->stopRecoveryTimer(Z)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/lge/ims/service/ac/ACService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget v0, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@2
    return v0
.end method

.method static synthetic access$800(Lcom/lge/ims/service/ac/ACService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget v0, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCountForJoin:I

    #@2
    return v0
.end method

.method static synthetic access$802(Lcom/lge/ims/service/ac/ACService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    iput p1, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCountForJoin:I

    #@2
    return p1
.end method

.method static synthetic access$810(Lcom/lge/ims/service/ac/ACService;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget v0, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCountForJoin:I

    #@2
    add-int/lit8 v1, v0, -0x1

    #@4
    iput v1, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCountForJoin:I

    #@6
    return v0
.end method

.method static synthetic access$900(Lcom/lge/ims/service/ac/ACService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ac/ACService;->stopValidityTimer(Z)V

    #@3
    return-void
.end method

.method private canRetryConfigurationRetrieval()Z
    .registers 2

    #@0
    .prologue
    .line 263
    iget v0, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@2
    if-ltz v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private checkValidityAndStartTimer()V
    .registers 13

    #@0
    .prologue
    const-wide/16 v10, 0x3e8

    #@2
    const-wide/16 v8, 0x0

    #@4
    .line 267
    iget-object v4, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@6
    invoke-static {v4}, Lcom/lge/ims/service/ac/ACContentHelper;->getValidityPeriodFromVersion(Landroid/content/Context;)Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    .line 269
    .local v2, validityPeriod:Ljava/lang/String;
    if-nez v2, :cond_14

    #@c
    .line 270
    const-string v4, "ACService"

    #@e
    const-string v5, "ValidityPeriod is null"

    #@10
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    .line 326
    :goto_13
    return-void

    #@14
    .line 274
    :cond_14
    const-string v4, "ACService"

    #@16
    new-instance v5, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v6, "checkValidityAndStartTimer :: ValidityPeriod="

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    .line 277
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@2f
    move-result-wide v4

    #@30
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@33
    move-result-object v3

    #@34
    .line 279
    .local v3, validityTime:Ljava/lang/Long;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@37
    move-result-wide v4

    #@38
    cmp-long v4, v4, v8

    #@3a
    if-gtz v4, :cond_44

    #@3c
    .line 280
    const-string v4, "ACService"

    #@3e
    const-string v5, "ValidityPeriod is invalid"

    #@40
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    goto :goto_13

    #@44
    .line 284
    :cond_44
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@47
    move-result-wide v4

    #@48
    div-long/2addr v4, v10

    #@49
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@4c
    move-result-object v0

    #@4d
    .line 286
    .local v0, currentTime:Ljava/lang/Long;
    const-string v4, "ACService"

    #@4f
    new-instance v5, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v6, "validity="

    #@56
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@5d
    move-result-wide v6

    #@5e
    invoke-static {v6, v7}, Lcom/lge/ims/service/ac/ACService;->getDateFormat(J)Ljava/lang/String;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    const-string v6, ", current="

    #@68
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@6f
    move-result-wide v6

    #@70
    invoke-static {v6, v7}, Lcom/lge/ims/service/ac/ACService;->getDateFormat(J)Ljava/lang/String;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v5

    #@7c
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@7f
    .line 289
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@82
    move-result-wide v4

    #@83
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@86
    move-result-wide v6

    #@87
    cmp-long v4, v4, v6

    #@89
    if-gtz v4, :cond_129

    #@8b
    .line 290
    const-string v4, "ACService"

    #@8d
    const-string v5, "Configuration validity is expired"

    #@8f
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@92
    .line 293
    iget-object v4, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@94
    invoke-static {v4}, Lcom/lge/ims/service/ac/ACContentHelper;->updateValidityPeriod(Landroid/content/Context;)V

    #@97
    .line 295
    iget-object v4, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@99
    invoke-static {v4}, Lcom/lge/ims/service/ac/ACContentHelper;->getValidityPeriodFromVersion(Landroid/content/Context;)Ljava/lang/String;

    #@9c
    move-result-object v2

    #@9d
    .line 297
    if-nez v2, :cond_a8

    #@9f
    .line 298
    const-string v4, "ACService"

    #@a1
    const-string v5, "ValidityPeriod is null"

    #@a3
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@a6
    goto/16 :goto_13

    #@a8
    .line 302
    :cond_a8
    const-string v4, "ACService"

    #@aa
    new-instance v5, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v6, "checkValidityAndStartTimer :: ValidityPeriod="

    #@b1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v5

    #@b5
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v5

    #@b9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v5

    #@bd
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@c0
    .line 305
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@c3
    move-result-wide v4

    #@c4
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@c7
    move-result-object v3

    #@c8
    .line 307
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@cb
    move-result-wide v4

    #@cc
    cmp-long v4, v4, v8

    #@ce
    if-gtz v4, :cond_d9

    #@d0
    .line 308
    const-string v4, "ACService"

    #@d2
    const-string v5, "ValidityPeriod is invalid"

    #@d4
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d7
    goto/16 :goto_13

    #@d9
    .line 312
    :cond_d9
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@dc
    move-result-wide v4

    #@dd
    div-long/2addr v4, v10

    #@de
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@e1
    move-result-object v0

    #@e2
    .line 314
    const-string v4, "ACService"

    #@e4
    new-instance v5, Ljava/lang/StringBuilder;

    #@e6
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e9
    const-string v6, "validity="

    #@eb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v5

    #@ef
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@f2
    move-result-wide v6

    #@f3
    invoke-static {v6, v7}, Lcom/lge/ims/service/ac/ACService;->getDateFormat(J)Ljava/lang/String;

    #@f6
    move-result-object v6

    #@f7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v5

    #@fb
    const-string v6, ", current="

    #@fd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v5

    #@101
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@104
    move-result-wide v6

    #@105
    invoke-static {v6, v7}, Lcom/lge/ims/service/ac/ACService;->getDateFormat(J)Ljava/lang/String;

    #@108
    move-result-object v6

    #@109
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v5

    #@10d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v5

    #@111
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@114
    .line 316
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@117
    move-result-wide v4

    #@118
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@11b
    move-result-wide v6

    #@11c
    cmp-long v4, v4, v6

    #@11e
    if-gtz v4, :cond_129

    #@120
    .line 317
    const-string v4, "ACService"

    #@122
    const-string v5, "Configuration validity is expired; don\'t retrieve the configuration until the power cycle"

    #@124
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@127
    goto/16 :goto_13

    #@129
    .line 322
    :cond_129
    invoke-virtual {v3}, Ljava/lang/Long;->intValue()I

    #@12c
    move-result v4

    #@12d
    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    #@130
    move-result v5

    #@131
    sub-int v1, v4, v5

    #@133
    .line 324
    .local v1, duration:I
    const/4 v4, 0x1

    #@134
    invoke-direct {p0, v4}, Lcom/lge/ims/service/ac/ACService;->stopValidityTimer(Z)V

    #@137
    .line 325
    mul-int/lit16 v4, v1, 0x3e8

    #@139
    invoke-direct {p0, v4}, Lcom/lge/ims/service/ac/ACService;->startValidityTimer(I)V

    #@13c
    goto/16 :goto_13
.end method

.method private static getDateFormat(J)Ljava/lang/String;
    .registers 6
    .parameter "currentTimeSeconds"

    #@0
    .prologue
    .line 911
    new-instance v0, Ljava/text/SimpleDateFormat;

    #@2
    const-string v1, "yyyy-MM-dd HH:mm:ss"

    #@4
    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@7
    .line 912
    .local v0, sdf:Ljava/text/SimpleDateFormat;
    new-instance v1, Ljava/util/Date;

    #@9
    const-wide/16 v2, 0x3e8

    #@b
    mul-long/2addr v2, p0

    #@c
    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    #@f
    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    return-object v1
.end method

.method public static getInstance()Lcom/lge/ims/service/ac/ACService;
    .registers 2

    #@0
    .prologue
    .line 96
    sget-object v0, Lcom/lge/ims/service/ac/ACService;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@2
    if-nez v0, :cond_18

    #@4
    .line 97
    new-instance v0, Lcom/lge/ims/service/ac/ACService;

    #@6
    invoke-direct {v0}, Lcom/lge/ims/service/ac/ACService;-><init>()V

    #@9
    sput-object v0, Lcom/lge/ims/service/ac/ACService;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@b
    .line 99
    sget-object v0, Lcom/lge/ims/service/ac/ACService;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@d
    if-nez v0, :cond_18

    #@f
    .line 100
    const-string v0, "ACService"

    #@11
    const-string v1, "Instantiating an AC Service failed"

    #@13
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 101
    const/4 v0, 0x0

    #@17
    .line 105
    :goto_17
    return-object v0

    #@18
    :cond_18
    sget-object v0, Lcom/lge/ims/service/ac/ACService;->mACService:Lcom/lge/ims/service/ac/ACService;

    #@1a
    goto :goto_17
.end method

.method private isConfigurationRetrievalAllowed()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 364
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v2}, Lcom/lge/ims/service/ac/ACContentHelper;->getValidityFromVersion(Landroid/content/Context;)Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 366
    .local v0, validity:Ljava/lang/String;
    if-nez v0, :cond_a

    #@9
    .line 378
    :cond_9
    :goto_9
    return v1

    #@a
    .line 371
    :cond_a
    const-string v2, "-1"

    #@c
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_1b

    #@12
    .line 372
    const-string v1, "ACService"

    #@14
    const-string v2, "Service is unavailable & configuration retrieval is not allowed"

    #@16
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    .line 373
    const/4 v1, 0x0

    #@1a
    goto :goto_9

    #@1b
    .line 374
    :cond_1b
    const-string v2, "0"

    #@1d
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@20
    move-result v2

    #@21
    if-eqz v2, :cond_9

    #@23
    .line 375
    const-string v2, "ACService"

    #@25
    const-string v3, "Service is unavailable, but configuration retrieval is allowed"

    #@27
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    goto :goto_9
.end method

.method private isConfigurationRetrievalBlocked()Z
    .registers 3

    #@0
    .prologue
    .line 329
    iget v0, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@2
    const/16 v1, 0x1f7

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method private isConfigurationRetrievalRequired()Z
    .registers 9

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 333
    iget-object v4, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@3
    invoke-static {v4}, Lcom/lge/ims/service/ac/ACContentHelper;->getValidityPeriodFromVersion(Landroid/content/Context;)Ljava/lang/String;

    #@6
    move-result-object v1

    #@7
    .line 335
    .local v1, validityPeriod:Ljava/lang/String;
    if-nez v1, :cond_a

    #@9
    .line 360
    :goto_9
    return v3

    #@a
    .line 340
    :cond_a
    const-string v4, "ACService"

    #@c
    new-instance v5, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "ValidityPeriod="

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v5

    #@1f
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 343
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@25
    move-result-wide v4

    #@26
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@29
    move-result-object v2

    #@2a
    .line 345
    .local v2, validityTime:Ljava/lang/Long;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@2d
    move-result-wide v4

    #@2e
    const-wide/16 v6, 0x0

    #@30
    cmp-long v4, v4, v6

    #@32
    if-gtz v4, :cond_3c

    #@34
    .line 346
    const-string v4, "ACService"

    #@36
    const-string v5, "ValidityPeriod is invalid"

    #@38
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    goto :goto_9

    #@3c
    .line 350
    :cond_3c
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3f
    move-result-wide v4

    #@40
    const-wide/16 v6, 0x3e8

    #@42
    div-long/2addr v4, v6

    #@43
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@46
    move-result-object v0

    #@47
    .line 352
    .local v0, currentTime:Ljava/lang/Long;
    const-string v4, "ACService"

    #@49
    new-instance v5, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v6, "validity="

    #@50
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v5

    #@54
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@57
    move-result-wide v6

    #@58
    invoke-static {v6, v7}, Lcom/lge/ims/service/ac/ACService;->getDateFormat(J)Ljava/lang/String;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    const-string v6, ", current="

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@69
    move-result-wide v6

    #@6a
    invoke-static {v6, v7}, Lcom/lge/ims/service/ac/ACService;->getDateFormat(J)Ljava/lang/String;

    #@6d
    move-result-object v6

    #@6e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v5

    #@72
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v5

    #@76
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@79
    .line 355
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@7c
    move-result-wide v4

    #@7d
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@80
    move-result-wide v6

    #@81
    cmp-long v4, v4, v6

    #@83
    if-gtz v4, :cond_8e

    #@85
    .line 356
    const-string v4, "ACService"

    #@87
    const-string v5, "Configuration validity is expired"

    #@89
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@8c
    goto/16 :goto_9

    #@8e
    .line 360
    :cond_8e
    const/4 v3, 0x0

    #@8f
    goto/16 :goto_9
.end method

.method private isServerInterworkingDone()Z
    .registers 2

    #@0
    .prologue
    .line 382
    iget-boolean v0, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingDone:Z

    #@2
    return v0
.end method

.method private joinService(I)Z
    .registers 11
    .parameter "serviceType"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v8, 0x2

    #@3
    const/4 v4, 0x0

    #@4
    .line 386
    iget v5, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCountForJoin:I

    #@6
    if-gez v5, :cond_9

    #@8
    .line 447
    :goto_8
    return v4

    #@9
    .line 390
    :cond_9
    if-eq p1, v3, :cond_15

    #@b
    if-eq p1, v8, :cond_15

    #@d
    .line 391
    const-string v3, "ACService"

    #@f
    const-string v5, "Service type is not valid"

    #@11
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    goto :goto_8

    #@15
    .line 395
    :cond_15
    invoke-static {v3}, Lcom/lge/ims/service/ac/ACUrl;->getPath(I)Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    .line 397
    .local v2, urlPath:Ljava/lang/String;
    if-nez v2, :cond_23

    #@1b
    .line 398
    const-string v3, "ACService"

    #@1d
    const-string v5, "URL path is null"

    #@1f
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    goto :goto_8

    #@23
    .line 402
    :cond_23
    invoke-static {v2, v7}, Lcom/lge/ims/service/ac/ACUrl;->getDefaultUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    .line 404
    .local v1, url:Ljava/lang/String;
    if-nez v1, :cond_31

    #@29
    .line 405
    const-string v3, "ACService"

    #@2b
    const-string v5, "URL is null"

    #@2d
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@30
    goto :goto_8

    #@31
    .line 409
    :cond_31
    new-instance v0, Lcom/lge/ims/service/ac/ACHttpConnection;

    #@33
    invoke-direct {v0, v1}, Lcom/lge/ims/service/ac/ACHttpConnection;-><init>(Ljava/lang/String;)V

    #@36
    .line 411
    .local v0, httpConnection:Lcom/lge/ims/service/ac/ACHttpConnection;
    iput v4, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@38
    .line 413
    if-ne p1, v8, :cond_85

    #@3a
    .line 414
    const-string v5, "PSVT"

    #@3c
    invoke-virtual {v0, v5}, Lcom/lge/ims/service/ac/ACHttpConnection;->join(Ljava/lang/String;)I

    #@3f
    move-result v5

    #@40
    iput v5, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@42
    .line 419
    :goto_42
    iget v5, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@44
    const/16 v6, 0x191

    #@46
    if-ne v5, v6, :cond_55

    #@48
    .line 420
    invoke-direct {p0, v0}, Lcom/lge/ims/service/ac/ACService;->setCredentialOnChallenge(Lcom/lge/ims/service/ac/ACHttpConnection;)V

    #@4b
    .line 422
    if-ne p1, v8, :cond_8c

    #@4d
    .line 423
    const-string v5, "PSVT"

    #@4f
    invoke-virtual {v0, v5}, Lcom/lge/ims/service/ac/ACHttpConnection;->join(Ljava/lang/String;)I

    #@52
    move-result v5

    #@53
    iput v5, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@55
    .line 429
    :cond_55
    :goto_55
    const-string v5, "ACService"

    #@57
    new-instance v6, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v7, "joinService :: resultCode="

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    iget v7, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@64
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@67
    move-result-object v6

    #@68
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v6

    #@6c
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@6f
    .line 431
    iget v5, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@71
    if-nez v5, :cond_9b

    #@73
    .line 432
    if-ne p1, v8, :cond_93

    #@75
    .line 433
    const-string v5, "ACService"

    #@77
    const-string v6, "joinService :: PSVT service is OK"

    #@79
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@7c
    .line 445
    :goto_7c
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/ACHttpConnection;->close()V

    #@7f
    .line 447
    iget v5, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@81
    if-nez v5, :cond_ad

    #@83
    :goto_83
    move v4, v3

    #@84
    goto :goto_8

    #@85
    .line 416
    :cond_85
    invoke-virtual {v0, v7}, Lcom/lge/ims/service/ac/ACHttpConnection;->join(Ljava/lang/String;)I

    #@88
    move-result v5

    #@89
    iput v5, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@8b
    goto :goto_42

    #@8c
    .line 425
    :cond_8c
    invoke-virtual {v0, v7}, Lcom/lge/ims/service/ac/ACHttpConnection;->join(Ljava/lang/String;)I

    #@8f
    move-result v5

    #@90
    iput v5, p0, Lcom/lge/ims/service/ac/ACService;->mResultCodeForJoin:I

    #@92
    goto :goto_55

    #@93
    .line 435
    :cond_93
    const-string v5, "ACService"

    #@95
    const-string v6, "joinService :: VoLTE service is OK"

    #@97
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@9a
    goto :goto_7c

    #@9b
    .line 438
    :cond_9b
    if-ne p1, v8, :cond_a5

    #@9d
    .line 439
    const-string v5, "ACService"

    #@9f
    const-string v6, "joinService :: PSVT service is not OK"

    #@a1
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@a4
    goto :goto_7c

    #@a5
    .line 441
    :cond_a5
    const-string v5, "ACService"

    #@a7
    const-string v6, "joinService :: VoLTE service is not OK"

    #@a9
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@ac
    goto :goto_7c

    #@ad
    :cond_ad
    move v3, v4

    #@ae
    .line 447
    goto :goto_83
.end method

.method private onConfigurationRetrievalFailed(Lcom/lge/ims/service/ac/ACHttpConnection;)V
    .registers 15
    .parameter "httpConnection"

    #@0
    .prologue
    const/16 v12, 0x66

    #@2
    const/4 v7, 0x1

    #@3
    const/4 v8, 0x0

    #@4
    .line 536
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@7
    move-result-object v0

    #@8
    .line 538
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->isConnected()Z

    #@b
    move-result v6

    #@c
    if-eqz v6, :cond_4a

    #@e
    .line 539
    iget v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@10
    if-lez v6, :cond_71

    #@12
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACHttpConnection;->getRetryAfter()I

    #@15
    move-result v6

    #@16
    if-lez v6, :cond_71

    #@18
    .line 540
    const-string v6, "ACService"

    #@1a
    new-instance v9, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v10, "Configuration retrieval will be requested after "

    #@21
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v9

    #@25
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACHttpConnection;->getRetryAfter()I

    #@28
    move-result v10

    #@29
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v9

    #@2d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v9

    #@31
    invoke-static {v6, v9}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    .line 542
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@36
    if-eqz v6, :cond_44

    #@38
    .line 543
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@3a
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACHttpConnection;->getRetryAfter()I

    #@3d
    move-result v9

    #@3e
    mul-int/lit16 v9, v9, 0x3e8

    #@40
    int-to-long v9, v9

    #@41
    invoke-virtual {v6, v12, v9, v10}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@44
    .line 555
    :cond_44
    :goto_44
    iget v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@46
    add-int/lit8 v6, v6, -0x1

    #@48
    iput v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@4a
    .line 558
    :cond_4a
    iget v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@4c
    if-gez v6, :cond_cf

    #@4e
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@50
    invoke-static {v6}, Lcom/lge/ims/service/ac/ACContentHelper;->hasCachedConfiguration(Landroid/content/Context;)Z

    #@53
    move-result v6

    #@54
    if-eqz v6, :cond_cf

    #@56
    .line 559
    iget v6, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@58
    if-ne v6, v7, :cond_9a

    #@5a
    .line 560
    const-string v6, "ACService"

    #@5c
    const-string v9, "Configuration is not changed; ACS interworking is requested by validity period expired"

    #@5e
    invoke-static {v6, v9}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@61
    .line 576
    :goto_61
    iput v8, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@63
    .line 577
    iput-boolean v7, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingDone:Z

    #@65
    .line 578
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@67
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@6a
    move-result v6

    #@6b
    iput v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@6d
    .line 581
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->checkValidityAndStartTimer()V

    #@70
    .line 601
    :cond_70
    :goto_70
    return-void

    #@71
    .line 547
    :cond_71
    iget v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@73
    if-lez v6, :cond_44

    #@75
    iget v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@77
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@79
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@7c
    move-result v9

    #@7d
    if-gt v6, v9, :cond_44

    #@7f
    .line 548
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@81
    if-eqz v6, :cond_44

    #@83
    .line 549
    iget-object v9, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@85
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@87
    iget v10, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@89
    add-int/lit8 v10, v10, -0x1

    #@8b
    invoke-virtual {v6, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@8e
    move-result-object v6

    #@8f
    check-cast v6, Ljava/lang/Integer;

    #@91
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@94
    move-result v6

    #@95
    int-to-long v10, v6

    #@96
    invoke-virtual {v9, v12, v10, v11}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@99
    goto :goto_44

    #@9a
    .line 561
    :cond_9a
    iget v6, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@9c
    const/4 v9, 0x2

    #@9d
    if-ne v6, v9, :cond_a7

    #@9f
    .line 562
    const-string v6, "ACService"

    #@a1
    const-string v9, "Configuration is not changed; ACS interworking is requested by airplane mode off"

    #@a3
    invoke-static {v6, v9}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@a6
    goto :goto_61

    #@a7
    .line 564
    :cond_a7
    invoke-virtual {p0}, Lcom/lge/ims/service/ac/ACService;->getProvisionedServices()I

    #@aa
    move-result v3

    #@ab
    .line 565
    .local v3, provisionedServices:I
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@ad
    invoke-static {v6}, Lcom/lge/ims/service/ac/ACContentHelper;->updateContentForIMS(Landroid/content/Context;)I

    #@b0
    move-result v5

    #@b1
    .line 568
    .local v5, updatedTables:I
    and-int/lit8 v6, v3, 0x1

    #@b3
    if-eqz v6, :cond_cb

    #@b5
    move v2, v7

    #@b6
    .line 569
    .local v2, isVoLTEOn:Z
    :goto_b6
    and-int/lit16 v6, v3, 0x100

    #@b8
    if-eqz v6, :cond_cd

    #@ba
    move v1, v7

    #@bb
    .line 571
    .local v1, isVTOn:Z
    :goto_bb
    invoke-static {v2, v1}, Lcom/lge/ims/service/ac/ACContentHelper;->updateServiceAvailabilityForIMS(ZZ)I

    #@be
    move-result v6

    #@bf
    or-int/2addr v5, v6

    #@c0
    .line 573
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationUpdateCompletedRegistrants:Landroid/os/RegistrantList;

    #@c2
    new-instance v9, Lcom/lge/ims/service/ac/ACUpdateState;

    #@c4
    invoke-direct {v9, v3, v5}, Lcom/lge/ims/service/ac/ACUpdateState;-><init>(II)V

    #@c7
    invoke-virtual {v6, v9}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@ca
    goto :goto_61

    #@cb
    .end local v1           #isVTOn:Z
    .end local v2           #isVoLTEOn:Z
    :cond_cb
    move v2, v8

    #@cc
    .line 568
    goto :goto_b6

    #@cd
    .restart local v2       #isVoLTEOn:Z
    :cond_cd
    move v1, v8

    #@ce
    .line 569
    goto :goto_bb

    #@cf
    .line 582
    .end local v2           #isVoLTEOn:Z
    .end local v3           #provisionedServices:I
    .end local v5           #updatedTables:I
    :cond_cf
    iget v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@d1
    if-gez v6, :cond_70

    #@d3
    .line 583
    const-string v6, "ACService"

    #@d5
    const-string v9, "All the retries are consumed; do not retrieve the configuration until power cycle"

    #@d7
    invoke-static {v6, v9}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@da
    .line 585
    iput v8, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@dc
    .line 586
    iput-boolean v7, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingDone:Z

    #@de
    .line 587
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@e0
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@e3
    move-result v6

    #@e4
    iput v6, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@e6
    .line 590
    iget v6, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerInterval:I

    #@e8
    if-lez v6, :cond_f1

    #@ea
    .line 591
    iget v6, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerInterval:I

    #@ec
    mul-int/lit16 v6, v6, 0x3e8

    #@ee
    invoke-direct {p0, v6}, Lcom/lge/ims/service/ac/ACService;->startRecoveryTimer(I)V

    #@f1
    .line 594
    :cond_f1
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@f3
    const v7, 0x7f0603a7

    #@f6
    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@f9
    move-result-object v4

    #@fa
    .line 596
    .local v4, text:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v6

    #@103
    const-string v7, " "

    #@105
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v6

    #@109
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v4

    #@10d
    .line 597
    new-instance v6, Ljava/lang/StringBuilder;

    #@10f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@112
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v6

    #@116
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@118
    const v9, 0x7f0603a8

    #@11b
    invoke-virtual {v7, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@11e
    move-result-object v7

    #@11f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v6

    #@123
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@126
    move-result-object v4

    #@127
    .line 599
    iget-object v6, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@129
    invoke-static {v6, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@12c
    move-result-object v6

    #@12d
    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    #@130
    goto/16 :goto_70
.end method

.method private onConfigurationRetrieved(Lcom/lge/ims/service/ac/ACHttpConnection;)V
    .registers 15
    .parameter "httpConnection"

    #@0
    .prologue
    const/4 v12, 0x2

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    .line 452
    invoke-static {}, Lcom/lge/ims/service/ac/ACContentParser;->getInstance()Lcom/lge/ims/service/ac/ACContentParser;

    #@6
    move-result-object v1

    #@7
    .line 453
    .local v1, contentParser:Lcom/lge/ims/service/ac/ACContentParser;
    invoke-virtual {v1}, Lcom/lge/ims/service/ac/ACContentParser;->getContentCache()Lcom/lge/ims/service/ac/ACContentCache;

    #@a
    move-result-object v0

    #@b
    .line 455
    .local v0, contentCache:Lcom/lge/ims/service/ac/ACContentCache;
    invoke-virtual {v0}, Lcom/lge/ims/service/ac/ACContentCache;->displayContent()V

    #@e
    .line 457
    invoke-virtual {v1}, Lcom/lge/ims/service/ac/ACContentParser;->hasValidContent()Z

    #@11
    move-result v10

    #@12
    if-eqz v10, :cond_ab

    #@14
    .line 458
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@16
    invoke-static {v10, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->isSameACVersion(Landroid/content/Context;Lcom/lge/ims/service/ac/ACContentCache;)Z

    #@19
    move-result v10

    #@1a
    if-nez v10, :cond_5b

    #@1c
    .line 459
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@1e
    invoke-static {v10, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->updateContentForAC(Landroid/content/Context;Lcom/lge/ims/service/ac/ACContentCache;)V

    #@21
    .line 460
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@23
    invoke-static {v10, v0}, Lcom/lge/ims/service/ac/ACContentHelper;->updateContentForIMS(Landroid/content/Context;Lcom/lge/ims/service/ac/ACContentCache;)I

    #@26
    move-result v7

    #@27
    .line 461
    .local v7, updatedTables:I
    invoke-virtual {p0}, Lcom/lge/ims/service/ac/ACService;->getProvisionedServices()I

    #@2a
    move-result v6

    #@2b
    .line 464
    .local v6, provisionedServices:I
    and-int/lit8 v10, v6, 0x1

    #@2d
    if-eqz v10, :cond_57

    #@2f
    move v4, v8

    #@30
    .line 465
    .local v4, isVoLTEOn:Z
    :goto_30
    and-int/lit16 v10, v6, 0x100

    #@32
    if-eqz v10, :cond_59

    #@34
    move v3, v8

    #@35
    .line 467
    .local v3, isVTOn:Z
    :goto_35
    invoke-static {v4, v3}, Lcom/lge/ims/service/ac/ACContentHelper;->updateServiceAvailabilityForIMS(ZZ)I

    #@38
    move-result v10

    #@39
    or-int/2addr v7, v10

    #@3a
    .line 469
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationUpdateCompletedRegistrants:Landroid/os/RegistrantList;

    #@3c
    new-instance v11, Lcom/lge/ims/service/ac/ACUpdateState;

    #@3e
    invoke-direct {v11, v6, v7}, Lcom/lge/ims/service/ac/ACUpdateState;-><init>(II)V

    #@41
    invoke-virtual {v10, v11}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@44
    .line 502
    .end local v3           #isVTOn:Z
    .end local v4           #isVoLTEOn:Z
    .end local v6           #provisionedServices:I
    .end local v7           #updatedTables:I
    :cond_44
    :goto_44
    invoke-virtual {v1}, Lcom/lge/ims/service/ac/ACContentParser;->invalidate()V

    #@47
    .line 505
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->checkValidityAndStartTimer()V

    #@4a
    .line 530
    :cond_4a
    :goto_4a
    iput v9, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@4c
    .line 531
    iput-boolean v8, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingDone:Z

    #@4e
    .line 532
    iget-object v8, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@50
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@53
    move-result v8

    #@54
    iput v8, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@56
    .line 533
    return-void

    #@57
    .restart local v6       #provisionedServices:I
    .restart local v7       #updatedTables:I
    :cond_57
    move v4, v9

    #@58
    .line 464
    goto :goto_30

    #@59
    .restart local v4       #isVoLTEOn:Z
    :cond_59
    move v3, v9

    #@5a
    .line 465
    goto :goto_35

    #@5b
    .line 471
    .end local v4           #isVoLTEOn:Z
    .end local v6           #provisionedServices:I
    .end local v7           #updatedTables:I
    :cond_5b
    const-string v10, "ACService"

    #@5d
    const-string v11, "AC version is same"

    #@5f
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@62
    .line 473
    const/4 v5, 0x1

    #@63
    .line 474
    .local v5, notifyConfigurationRetrievalResult:Z
    const/4 v2, 0x0

    #@64
    .line 476
    .local v2, isConfigurationUpdated:Z
    iget v10, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@66
    if-ne v10, v8, :cond_98

    #@68
    .line 477
    if-nez v2, :cond_72

    #@6a
    .line 478
    const-string v10, "ACService"

    #@6c
    const-string v11, "Configuration is not changed; ACS interworking is requested by validity period expired"

    #@6e
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@71
    .line 479
    const/4 v5, 0x0

    #@72
    .line 488
    :cond_72
    :goto_72
    if-eqz v5, :cond_44

    #@74
    .line 489
    invoke-virtual {p0}, Lcom/lge/ims/service/ac/ACService;->getProvisionedServices()I

    #@77
    move-result v6

    #@78
    .line 490
    .restart local v6       #provisionedServices:I
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@7a
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACContentHelper;->updateContentForIMS(Landroid/content/Context;)I

    #@7d
    move-result v7

    #@7e
    .line 493
    .restart local v7       #updatedTables:I
    and-int/lit8 v10, v6, 0x1

    #@80
    if-eqz v10, :cond_a7

    #@82
    move v4, v8

    #@83
    .line 494
    .restart local v4       #isVoLTEOn:Z
    :goto_83
    and-int/lit16 v10, v6, 0x100

    #@85
    if-eqz v10, :cond_a9

    #@87
    move v3, v8

    #@88
    .line 496
    .restart local v3       #isVTOn:Z
    :goto_88
    invoke-static {v4, v3}, Lcom/lge/ims/service/ac/ACContentHelper;->updateServiceAvailabilityForIMS(ZZ)I

    #@8b
    move-result v10

    #@8c
    or-int/2addr v7, v10

    #@8d
    .line 498
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationUpdateCompletedRegistrants:Landroid/os/RegistrantList;

    #@8f
    new-instance v11, Lcom/lge/ims/service/ac/ACUpdateState;

    #@91
    invoke-direct {v11, v6, v7}, Lcom/lge/ims/service/ac/ACUpdateState;-><init>(II)V

    #@94
    invoke-virtual {v10, v11}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@97
    goto :goto_44

    #@98
    .line 481
    .end local v3           #isVTOn:Z
    .end local v4           #isVoLTEOn:Z
    .end local v6           #provisionedServices:I
    .end local v7           #updatedTables:I
    :cond_98
    iget v10, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@9a
    if-ne v10, v12, :cond_72

    #@9c
    .line 482
    if-nez v2, :cond_72

    #@9e
    .line 483
    const-string v10, "ACService"

    #@a0
    const-string v11, "Configuration is not changed; ACS interworking is requested by airplane mode off"

    #@a2
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@a5
    .line 484
    const/4 v5, 0x0

    #@a6
    goto :goto_72

    #@a7
    .restart local v6       #provisionedServices:I
    .restart local v7       #updatedTables:I
    :cond_a7
    move v4, v9

    #@a8
    .line 493
    goto :goto_83

    #@a9
    .restart local v4       #isVoLTEOn:Z
    :cond_a9
    move v3, v9

    #@aa
    .line 494
    goto :goto_88

    #@ab
    .line 507
    .end local v2           #isConfigurationUpdated:Z
    .end local v4           #isVoLTEOn:Z
    .end local v5           #notifyConfigurationRetrievalResult:Z
    .end local v6           #provisionedServices:I
    .end local v7           #updatedTables:I
    :cond_ab
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@ad
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACContentHelper;->hasCachedConfiguration(Landroid/content/Context;)Z

    #@b0
    move-result v10

    #@b1
    if-eqz v10, :cond_4a

    #@b3
    .line 508
    iget v10, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@b5
    if-ne v10, v8, :cond_c2

    #@b7
    .line 509
    const-string v10, "ACService"

    #@b9
    const-string v11, "Configuration is not changed; ACS interworking is requested by validity period expired"

    #@bb
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@be
    .line 526
    :goto_be
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->checkValidityAndStartTimer()V

    #@c1
    goto :goto_4a

    #@c2
    .line 510
    :cond_c2
    iget v10, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@c4
    if-ne v10, v12, :cond_ce

    #@c6
    .line 511
    const-string v10, "ACService"

    #@c8
    const-string v11, "Configuration is not changed; ACS interworking is requested by airplane mode off"

    #@ca
    invoke-static {v10, v11}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@cd
    goto :goto_be

    #@ce
    .line 513
    :cond_ce
    invoke-virtual {p0}, Lcom/lge/ims/service/ac/ACService;->getProvisionedServices()I

    #@d1
    move-result v6

    #@d2
    .line 514
    .restart local v6       #provisionedServices:I
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@d4
    invoke-static {v10}, Lcom/lge/ims/service/ac/ACContentHelper;->updateContentForIMS(Landroid/content/Context;)I

    #@d7
    move-result v7

    #@d8
    .line 517
    .restart local v7       #updatedTables:I
    and-int/lit8 v10, v6, 0x1

    #@da
    if-eqz v10, :cond_f2

    #@dc
    move v4, v8

    #@dd
    .line 518
    .restart local v4       #isVoLTEOn:Z
    :goto_dd
    and-int/lit16 v10, v6, 0x100

    #@df
    if-eqz v10, :cond_f4

    #@e1
    move v3, v8

    #@e2
    .line 520
    .restart local v3       #isVTOn:Z
    :goto_e2
    invoke-static {v4, v3}, Lcom/lge/ims/service/ac/ACContentHelper;->updateServiceAvailabilityForIMS(ZZ)I

    #@e5
    move-result v10

    #@e6
    or-int/2addr v7, v10

    #@e7
    .line 522
    iget-object v10, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationUpdateCompletedRegistrants:Landroid/os/RegistrantList;

    #@e9
    new-instance v11, Lcom/lge/ims/service/ac/ACUpdateState;

    #@eb
    invoke-direct {v11, v6, v7}, Lcom/lge/ims/service/ac/ACUpdateState;-><init>(II)V

    #@ee
    invoke-virtual {v10, v11}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@f1
    goto :goto_be

    #@f2
    .end local v3           #isVTOn:Z
    .end local v4           #isVoLTEOn:Z
    :cond_f2
    move v4, v9

    #@f3
    .line 517
    goto :goto_dd

    #@f4
    .restart local v4       #isVoLTEOn:Z
    :cond_f4
    move v3, v9

    #@f5
    .line 518
    goto :goto_e2
.end method

.method private retrieveConfiguration()V
    .registers 12

    #@0
    .prologue
    .line 604
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@3
    move-result v7

    #@4
    if-nez v7, :cond_e

    #@6
    .line 605
    const-string v7, "ACService"

    #@8
    const-string v8, "AC is off, configuration retrieval is not allowed"

    #@a
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 737
    :cond_d
    :goto_d
    return-void

    #@e
    .line 609
    :cond_e
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->isConfigurationRetrievalBlocked()Z

    #@11
    move-result v7

    #@12
    if-eqz v7, :cond_1c

    #@14
    .line 610
    const-string v7, "ACService"

    #@16
    const-string v8, "ACS interworking is blocked until power cycle"

    #@18
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    goto :goto_d

    #@1c
    .line 614
    :cond_1c
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->isServerInterworkingDone()Z

    #@1f
    move-result v7

    #@20
    if-eqz v7, :cond_2a

    #@22
    .line 615
    const-string v7, "ACService"

    #@24
    const-string v8, "ACS interworking is already done on boot-up"

    #@26
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    goto :goto_d

    #@2a
    .line 619
    :cond_2a
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->canRetryConfigurationRetrieval()Z

    #@2d
    move-result v7

    #@2e
    if-nez v7, :cond_38

    #@30
    .line 620
    const-string v7, "ACService"

    #@32
    const-string v8, "All the retries are consumed; do not retrieve the configuration until power cycle"

    #@34
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    goto :goto_d

    #@38
    .line 624
    :cond_38
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@3b
    move-result-object v0

    #@3c
    .line 626
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->isConnected()Z

    #@3f
    move-result v7

    #@40
    if-nez v7, :cond_4a

    #@42
    .line 627
    const-string v7, "ACService"

    #@44
    const-string v8, "No data connection; it will be retryed when the data connection is available"

    #@46
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    goto :goto_d

    #@4a
    .line 631
    :cond_4a
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->isConfigurationRetrievalAllowed()Z

    #@4d
    move-result v7

    #@4e
    if-eqz v7, :cond_d

    #@50
    .line 635
    const/4 v7, 0x0

    #@51
    invoke-static {v7}, Lcom/lge/ims/service/ac/ACUrl;->getPath(I)Ljava/lang/String;

    #@54
    move-result-object v6

    #@55
    .line 637
    .local v6, urlPath:Ljava/lang/String;
    if-nez v6, :cond_5f

    #@57
    .line 638
    const-string v7, "ACService"

    #@59
    const-string v8, "URL path is null"

    #@5b
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@5e
    goto :goto_d

    #@5f
    .line 642
    :cond_5f
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@61
    invoke-static {v7}, Lcom/lge/ims/service/ac/ACUrl;->getQuery(Landroid/content/Context;)Ljava/lang/String;

    #@64
    move-result-object v7

    #@65
    invoke-static {v6, v7}, Lcom/lge/ims/service/ac/ACUrl;->getDefaultUrl(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@68
    move-result-object v5

    #@69
    .line 644
    .local v5, url:Ljava/lang/String;
    if-nez v5, :cond_73

    #@6b
    .line 645
    const-string v7, "ACService"

    #@6d
    const-string v8, "URL is null"

    #@6f
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@72
    goto :goto_d

    #@73
    .line 649
    :cond_73
    new-instance v1, Lcom/lge/ims/service/ac/ACHttpConnection;

    #@75
    invoke-direct {v1, v5}, Lcom/lge/ims/service/ac/ACHttpConnection;-><init>(Ljava/lang/String;)V

    #@78
    .line 652
    .local v1, httpConnection:Lcom/lge/ims/service/ac/ACHttpConnection;
    iget-boolean v7, p0, Lcom/lge/ims/service/ac/ACService;->mAKASupportedForAuthType:Z

    #@7a
    invoke-virtual {v1, v7}, Lcom/lge/ims/service/ac/ACHttpConnection;->setAuthType(Z)V

    #@7d
    .line 654
    invoke-virtual {v1}, Lcom/lge/ims/service/ac/ACHttpConnection;->send()I

    #@80
    move-result v7

    #@81
    iput v7, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@83
    .line 656
    iget v7, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@85
    const/16 v8, 0x191

    #@87
    if-ne v7, v8, :cond_a8

    #@89
    .line 658
    const/16 v7, 0x191

    #@8b
    iput v7, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResult:I

    #@8d
    .line 659
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@90
    move-result-wide v7

    #@91
    const-wide/16 v9, 0x3e8

    #@93
    div-long/2addr v7, v9

    #@94
    invoke-static {v7, v8}, Lcom/lge/ims/service/ac/ACService;->getDateFormat(J)Ljava/lang/String;

    #@97
    move-result-object v7

    #@98
    iput-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mTimestampForServerInterworkingResult:Ljava/lang/String;

    #@9a
    .line 660
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResultRegistrants:Landroid/os/RegistrantList;

    #@9c
    invoke-virtual {v7}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@9f
    .line 662
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ac/ACService;->setCredentialOnChallenge(Lcom/lge/ims/service/ac/ACHttpConnection;)V

    #@a2
    .line 663
    invoke-virtual {v1}, Lcom/lge/ims/service/ac/ACHttpConnection;->send()I

    #@a5
    move-result v7

    #@a6
    iput v7, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@a8
    .line 666
    :cond_a8
    const-string v7, "ACService"

    #@aa
    new-instance v8, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v9, "retrieveConfiguration :: resultCode="

    #@b1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v8

    #@b5
    iget v9, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@b7
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v8

    #@bb
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v8

    #@bf
    invoke-static {v7, v8}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@c2
    .line 668
    iget v7, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@c4
    if-nez v7, :cond_137

    #@c6
    .line 670
    const/16 v7, 0xc8

    #@c8
    iput v7, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResult:I

    #@ca
    .line 671
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@cd
    move-result-wide v7

    #@ce
    const-wide/16 v9, 0x3e8

    #@d0
    div-long/2addr v7, v9

    #@d1
    invoke-static {v7, v8}, Lcom/lge/ims/service/ac/ACService;->getDateFormat(J)Ljava/lang/String;

    #@d4
    move-result-object v7

    #@d5
    iput-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mTimestampForServerInterworkingResult:Ljava/lang/String;

    #@d7
    .line 672
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ac/ACService;->onConfigurationRetrieved(Lcom/lge/ims/service/ac/ACHttpConnection;)V

    #@da
    .line 674
    sget-object v7, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@dc
    const-string v8, "KT"

    #@de
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e1
    move-result v7

    #@e2
    if-eqz v7, :cond_105

    #@e4
    .line 675
    sget-object v7, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@e6
    const-string v8, "F180K"

    #@e8
    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@eb
    move-result v7

    #@ec
    if-eqz v7, :cond_105

    #@ee
    .line 676
    invoke-virtual {p0}, Lcom/lge/ims/service/ac/ACService;->getProvisionedServices()I

    #@f1
    move-result v3

    #@f2
    .line 677
    .local v3, provisionedServices:I
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@f4
    if-eqz v7, :cond_105

    #@f6
    .line 678
    and-int/lit8 v7, v3, 0x1

    #@f8
    if-nez v7, :cond_10f

    #@fa
    and-int/lit16 v7, v3, 0x100

    #@fc
    if-nez v7, :cond_10f

    #@fe
    .line 679
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@100
    const/16 v8, 0x79

    #@102
    invoke-virtual {v7, v8}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessage(I)Z

    #@105
    .line 733
    .end local v3           #provisionedServices:I
    :cond_105
    :goto_105
    invoke-virtual {v1}, Lcom/lge/ims/service/ac/ACHttpConnection;->close()V

    #@108
    .line 736
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResultRegistrants:Landroid/os/RegistrantList;

    #@10a
    invoke-virtual {v7}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@10d
    goto/16 :goto_d

    #@10f
    .line 680
    .restart local v3       #provisionedServices:I
    :cond_10f
    and-int/lit8 v7, v3, 0x1

    #@111
    if-nez v7, :cond_123

    #@113
    .line 681
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@115
    const/16 v8, 0x7a

    #@117
    const/4 v9, 0x1

    #@118
    const/4 v10, 0x0

    #@119
    invoke-static {v7, v8, v9, v10}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@11c
    move-result-object v2

    #@11d
    .line 682
    .local v2, msg:Landroid/os/Message;
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@11f
    invoke-virtual {v7, v2}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@122
    goto :goto_105

    #@123
    .line 683
    .end local v2           #msg:Landroid/os/Message;
    :cond_123
    and-int/lit16 v7, v3, 0x100

    #@125
    if-nez v7, :cond_105

    #@127
    .line 684
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@129
    const/16 v8, 0x7a

    #@12b
    const/4 v9, 0x2

    #@12c
    const/4 v10, 0x0

    #@12d
    invoke-static {v7, v8, v9, v10}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    #@130
    move-result-object v2

    #@131
    .line 685
    .restart local v2       #msg:Landroid/os/Message;
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@133
    invoke-virtual {v7, v2}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@136
    goto :goto_105

    #@137
    .line 692
    .end local v2           #msg:Landroid/os/Message;
    .end local v3           #provisionedServices:I
    :cond_137
    iget v7, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@139
    if-gez v7, :cond_171

    #@13b
    .line 694
    const/4 v7, 0x0

    #@13c
    iput v7, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResult:I

    #@13e
    .line 699
    :goto_13e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@141
    move-result-wide v7

    #@142
    const-wide/16 v9, 0x3e8

    #@144
    div-long/2addr v7, v9

    #@145
    invoke-static {v7, v8}, Lcom/lge/ims/service/ac/ACService;->getDateFormat(J)Ljava/lang/String;

    #@148
    move-result-object v7

    #@149
    iput-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mTimestampForServerInterworkingResult:Ljava/lang/String;

    #@14b
    .line 701
    sget-object v7, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@14d
    const-string v8, "KT"

    #@14f
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@152
    move-result v7

    #@153
    if-eqz v7, :cond_1c1

    #@155
    .line 702
    iget v7, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@157
    const/16 v8, 0x194

    #@159
    if-ne v7, v8, :cond_17a

    #@15b
    .line 704
    sget-object v7, Lcom/lge/ims/Configuration;->MODEL:Ljava/lang/String;

    #@15d
    const-string v8, "F180K"

    #@15f
    invoke-virtual {v7, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@162
    move-result v7

    #@163
    if-eqz v7, :cond_176

    #@165
    .line 705
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@167
    if-eqz v7, :cond_105

    #@169
    .line 706
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@16b
    const/16 v8, 0x79

    #@16d
    invoke-virtual {v7, v8}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessage(I)Z

    #@170
    goto :goto_105

    #@171
    .line 696
    :cond_171
    iget v7, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@173
    iput v7, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResult:I

    #@175
    goto :goto_13e

    #@176
    .line 709
    :cond_176
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ac/ACService;->onConfigurationRetrievalFailed(Lcom/lge/ims/service/ac/ACHttpConnection;)V

    #@179
    goto :goto_105

    #@17a
    .line 711
    :cond_17a
    iget v7, p0, Lcom/lge/ims/service/ac/ACService;->mResultCode:I

    #@17c
    const/16 v8, 0x1f7

    #@17e
    if-ne v7, v8, :cond_1bc

    #@180
    .line 712
    const/4 v7, 0x0

    #@181
    iput v7, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingRequest:I

    #@183
    .line 713
    const/4 v7, 0x1

    #@184
    iput-boolean v7, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingDone:Z

    #@186
    .line 714
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@188
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@18b
    move-result v7

    #@18c
    iput v7, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@18e
    .line 716
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@190
    invoke-static {v7}, Lcom/lge/ims/service/ac/ACContentHelper;->hasCachedConfiguration(Landroid/content/Context;)Z

    #@193
    move-result v7

    #@194
    if-eqz v7, :cond_1a7

    #@196
    .line 718
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@198
    invoke-static {v7}, Lcom/lge/ims/service/ac/ACContentHelper;->deleteContentForAC(Landroid/content/Context;)V

    #@19b
    .line 720
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationUpdateCompletedRegistrants:Landroid/os/RegistrantList;

    #@19d
    new-instance v8, Lcom/lge/ims/service/ac/ACUpdateState;

    #@19f
    const/4 v9, 0x0

    #@1a0
    const/4 v10, 0x0

    #@1a1
    invoke-direct {v8, v9, v10}, Lcom/lge/ims/service/ac/ACUpdateState;-><init>(II)V

    #@1a4
    invoke-virtual {v7, v8}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@1a7
    .line 723
    :cond_1a7
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@1a9
    const v8, 0x7f0603a9

    #@1ac
    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1af
    move-result-object v4

    #@1b0
    .line 724
    .local v4, text:Ljava/lang/String;
    iget-object v7, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@1b2
    const/4 v8, 0x0

    #@1b3
    invoke-static {v7, v4, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@1b6
    move-result-object v7

    #@1b7
    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    #@1ba
    goto/16 :goto_105

    #@1bc
    .line 726
    .end local v4           #text:Ljava/lang/String;
    :cond_1bc
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ac/ACService;->onConfigurationRetrievalFailed(Lcom/lge/ims/service/ac/ACHttpConnection;)V

    #@1bf
    goto/16 :goto_105

    #@1c1
    .line 729
    :cond_1c1
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ac/ACService;->onConfigurationRetrievalFailed(Lcom/lge/ims/service/ac/ACHttpConnection;)V

    #@1c4
    goto/16 :goto_105
.end method

.method private retrieveConfigurationOnDataConnected()V
    .registers 6

    #@0
    .prologue
    const/16 v2, 0xc9

    #@2
    const/4 v4, 0x0

    #@3
    .line 740
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->isConfigurationRetrievalBlocked()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_13

    #@9
    .line 741
    const-string v0, "ACService"

    #@b
    const-string v1, "ACS interworking is blocked until power cycle"

    #@d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 742
    iput-boolean v4, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationRetrievalRequiredByAirplaneModeOff:Z

    #@12
    .line 777
    :goto_12
    return-void

    #@13
    .line 746
    :cond_13
    iget-boolean v0, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationRetrievalRequiredByAirplaneModeOff:Z

    #@15
    if-nez v0, :cond_1b

    #@17
    .line 747
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->retrieveConfiguration()V

    #@1a
    goto :goto_12

    #@1b
    .line 749
    :cond_1b
    const-string v0, "ACService"

    #@1d
    const-string v1, "ACS interworking is re-started by the airplane mode"

    #@1f
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 751
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->isServerInterworkingDone()Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_38

    #@28
    .line 752
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@2a
    if-eqz v0, :cond_34

    #@2c
    .line 753
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@2e
    invoke-virtual {v0, v2}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessage(I)Z

    #@31
    .line 775
    :goto_31
    iput-boolean v4, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationRetrievalRequiredByAirplaneModeOff:Z

    #@33
    goto :goto_12

    #@34
    .line 755
    :cond_34
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->retrieveConfiguration()V

    #@37
    goto :goto_31

    #@38
    .line 758
    :cond_38
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@3a
    invoke-static {v0}, Lcom/lge/ims/service/ac/ACContentHelper;->hasCachedConfiguration(Landroid/content/Context;)Z

    #@3d
    move-result v0

    #@3e
    if-eqz v0, :cond_59

    #@40
    .line 759
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@42
    if-eqz v0, :cond_55

    #@44
    .line 761
    const-string v0, "ACService"

    #@46
    const-string v1, "ACS interworking :: after 8s"

    #@48
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 762
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@4d
    const/16 v1, 0x6b

    #@4f
    const-wide/16 v2, 0x1f40

    #@51
    invoke-virtual {v0, v1, v2, v3}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@54
    goto :goto_31

    #@55
    .line 764
    :cond_55
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->retrieveConfiguration()V

    #@58
    goto :goto_31

    #@59
    .line 767
    :cond_59
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@5b
    if-eqz v0, :cond_63

    #@5d
    .line 768
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@5f
    invoke-virtual {v0, v2}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessage(I)Z

    #@62
    goto :goto_31

    #@63
    .line 770
    :cond_63
    invoke-direct {p0}, Lcom/lge/ims/service/ac/ACService;->retrieveConfiguration()V

    #@66
    goto :goto_31
.end method

.method private setCredentialOnChallenge(Lcom/lge/ims/service/ac/ACHttpConnection;)V
    .registers 9
    .parameter "httpConnection"

    #@0
    .prologue
    .line 780
    const-string v4, "ACService"

    #@2
    const-string v5, "setCredentialOnChallenge"

    #@4
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 782
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getSubscriberId()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 784
    .local v1, imsi:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->isUsimOn()Z

    #@e
    move-result v4

    #@f
    if-nez v4, :cond_15

    #@11
    .line 785
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getIMSI()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    .line 788
    :cond_15
    invoke-virtual {p1}, Lcom/lge/ims/service/ac/ACHttpConnection;->getNonce()Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    invoke-static {v4, v1}, Lcom/lge/ims/service/ac/ACAuthHelper;->calculateResponse(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    .line 789
    .local v3, response:Ljava/lang/String;
    invoke-static {}, Lcom/lge/ims/PhoneInfo;->getPhoneNumber()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    .line 792
    .local v2, msisdn:Ljava/lang/String;
    :try_start_21
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->isUsimOn()Z

    #@24
    move-result v4

    #@25
    if-nez v4, :cond_45

    #@27
    .line 793
    invoke-static {}, Lcom/lge/ims/service/ac/ACConfiguration;->getPhoneNumber()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    .line 795
    if-eqz v2, :cond_45

    #@2d
    .line 796
    const/4 v4, 0x1

    #@2e
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    .line 797
    new-instance v4, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v5, "82"

    #@39
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v4

    #@41
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v2

    #@45
    .line 801
    :cond_45
    if-eqz v2, :cond_54

    #@47
    .line 802
    const-string v4, "+"

    #@49
    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@4c
    move-result v4

    #@4d
    if-eqz v4, :cond_54

    #@4f
    .line 803
    const/4 v4, 0x1

    #@50
    invoke-virtual {v2, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_53
    .catch Ljava/lang/Exception; {:try_start_21 .. :try_end_53} :catch_58

    #@53
    move-result-object v2

    #@54
    .line 811
    :cond_54
    :goto_54
    invoke-virtual {p1, v2, v3}, Lcom/lge/ims/service/ac/ACHttpConnection;->setCredential(Ljava/lang/String;Ljava/lang/String;)V

    #@57
    .line 812
    return-void

    #@58
    .line 806
    :catch_58
    move-exception v0

    #@59
    .line 807
    .local v0, e:Ljava/lang/Exception;
    const-string v4, "ACService"

    #@5b
    new-instance v5, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v6, "Exception :: "

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v5

    #@72
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@75
    .line 808
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@78
    goto :goto_54
.end method

.method private startRecoveryTimer(I)V
    .registers 7
    .parameter "duration"

    #@0
    .prologue
    .line 816
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getInstance()Lcom/lge/ims/AlarmTimerManager;

    #@3
    move-result-object v0

    #@4
    .line 818
    .local v0, atm:Lcom/lge/ims/AlarmTimerManager;
    if-nez v0, :cond_e

    #@6
    .line 819
    const-string v1, "ACService"

    #@8
    const-string v2, "AlamTimerManager is null"

    #@a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 839
    :goto_d
    return-void

    #@e
    .line 823
    :cond_e
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getTimerId()I

    #@11
    move-result v1

    #@12
    iput v1, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerId:I

    #@14
    .line 825
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerId:I

    #@16
    if-gtz v1, :cond_20

    #@18
    .line 826
    const-string v1, "ACService"

    #@1a
    const-string v2, "Recovery timer id is invalid"

    #@1c
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    goto :goto_d

    #@20
    .line 830
    :cond_20
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerId:I

    #@22
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@24
    const/16 v3, 0x67

    #@26
    const/4 v4, 0x0

    #@27
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/lge/ims/AlarmTimerManager;->registerForTimerExpired(ILandroid/os/Handler;ILjava/lang/Object;)V

    #@2a
    .line 832
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerId:I

    #@2c
    invoke-virtual {v0, v1, p1}, Lcom/lge/ims/AlarmTimerManager;->startTimer(II)Z

    #@2f
    move-result v1

    #@30
    if-nez v1, :cond_3e

    #@32
    .line 833
    const/4 v1, 0x0

    #@33
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ac/ACService;->stopRecoveryTimer(Z)V

    #@36
    .line 834
    const-string v1, "ACService"

    #@38
    const-string v2, "Starting a recovery timer failed"

    #@3a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    goto :goto_d

    #@3e
    .line 838
    :cond_3e
    const-string v1, "ACService"

    #@40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "Recovery timer is started :: tid="

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    iget v3, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerId:I

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    const-string v3, ", duration="

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@62
    goto :goto_d
.end method

.method private startValidityTimer(I)V
    .registers 7
    .parameter "duration"

    #@0
    .prologue
    .line 864
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getInstance()Lcom/lge/ims/AlarmTimerManager;

    #@3
    move-result-object v0

    #@4
    .line 866
    .local v0, atm:Lcom/lge/ims/AlarmTimerManager;
    if-nez v0, :cond_e

    #@6
    .line 867
    const-string v1, "ACService"

    #@8
    const-string v2, "AlamTimerManager is null"

    #@a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 887
    :goto_d
    return-void

    #@e
    .line 871
    :cond_e
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getTimerId()I

    #@11
    move-result v1

    #@12
    iput v1, p0, Lcom/lge/ims/service/ac/ACService;->mValidityTimerId:I

    #@14
    .line 873
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mValidityTimerId:I

    #@16
    if-gtz v1, :cond_20

    #@18
    .line 874
    const-string v1, "ACService"

    #@1a
    const-string v2, "Validity timer id is invalid"

    #@1c
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    goto :goto_d

    #@20
    .line 878
    :cond_20
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mValidityTimerId:I

    #@22
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@24
    const/16 v3, 0x68

    #@26
    const/4 v4, 0x0

    #@27
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/lge/ims/AlarmTimerManager;->registerForTimerExpired(ILandroid/os/Handler;ILjava/lang/Object;)V

    #@2a
    .line 880
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mValidityTimerId:I

    #@2c
    invoke-virtual {v0, v1, p1}, Lcom/lge/ims/AlarmTimerManager;->startTimer(II)Z

    #@2f
    move-result v1

    #@30
    if-nez v1, :cond_3e

    #@32
    .line 881
    const/4 v1, 0x0

    #@33
    invoke-direct {p0, v1}, Lcom/lge/ims/service/ac/ACService;->stopValidityTimer(Z)V

    #@36
    .line 882
    const-string v1, "ACService"

    #@38
    const-string v2, "Starting a validity timer failed"

    #@3a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    goto :goto_d

    #@3e
    .line 886
    :cond_3e
    const-string v1, "ACService"

    #@40
    new-instance v2, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v3, "Validity timer is started :: tid="

    #@47
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    iget v3, p0, Lcom/lge/ims/service/ac/ACService;->mValidityTimerId:I

    #@4d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    const-string v3, ", duration="

    #@53
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v2

    #@5b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@62
    goto :goto_d
.end method

.method private stopRecoveryTimer(Z)V
    .registers 5
    .parameter "stopRequired"

    #@0
    .prologue
    .line 842
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerId:I

    #@2
    if-gtz v1, :cond_5

    #@4
    .line 860
    :goto_4
    return-void

    #@5
    .line 846
    :cond_5
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getInstance()Lcom/lge/ims/AlarmTimerManager;

    #@8
    move-result-object v0

    #@9
    .line 848
    .local v0, atm:Lcom/lge/ims/AlarmTimerManager;
    if-nez v0, :cond_13

    #@b
    .line 849
    const-string v1, "ACService"

    #@d
    const-string v2, "AlamTimerManager is null"

    #@f
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    goto :goto_4

    #@13
    .line 853
    :cond_13
    if-eqz p1, :cond_1a

    #@15
    .line 854
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerId:I

    #@17
    invoke-virtual {v0, v1}, Lcom/lge/ims/AlarmTimerManager;->stopTimer(I)V

    #@1a
    .line 857
    :cond_1a
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerId:I

    #@1c
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@1e
    invoke-virtual {v0, v1, v2}, Lcom/lge/ims/AlarmTimerManager;->unregisterForTimerExpired(ILandroid/os/Handler;)V

    #@21
    .line 859
    const/4 v1, -0x1

    #@22
    iput v1, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerId:I

    #@24
    goto :goto_4
.end method

.method private stopValidityTimer(Z)V
    .registers 5
    .parameter "stopRequired"

    #@0
    .prologue
    .line 890
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mValidityTimerId:I

    #@2
    if-gtz v1, :cond_5

    #@4
    .line 908
    :goto_4
    return-void

    #@5
    .line 894
    :cond_5
    invoke-static {}, Lcom/lge/ims/AlarmTimerManager;->getInstance()Lcom/lge/ims/AlarmTimerManager;

    #@8
    move-result-object v0

    #@9
    .line 896
    .local v0, atm:Lcom/lge/ims/AlarmTimerManager;
    if-nez v0, :cond_13

    #@b
    .line 897
    const-string v1, "ACService"

    #@d
    const-string v2, "AlamTimerManager is null"

    #@f
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    goto :goto_4

    #@13
    .line 901
    :cond_13
    if-eqz p1, :cond_1a

    #@15
    .line 902
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mValidityTimerId:I

    #@17
    invoke-virtual {v0, v1}, Lcom/lge/ims/AlarmTimerManager;->stopTimer(I)V

    #@1a
    .line 905
    :cond_1a
    iget v1, p0, Lcom/lge/ims/service/ac/ACService;->mValidityTimerId:I

    #@1c
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@1e
    invoke-virtual {v0, v1, v2}, Lcom/lge/ims/AlarmTimerManager;->unregisterForTimerExpired(ILandroid/os/Handler;)V

    #@21
    .line 907
    const/4 v1, -0x1

    #@22
    iput v1, p0, Lcom/lge/ims/service/ac/ACService;->mValidityTimerId:I

    #@24
    goto :goto_4
.end method


# virtual methods
.method public destroy()V
    .registers 4

    #@0
    .prologue
    .line 159
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@2
    if-eqz v2, :cond_1f

    #@4
    .line 160
    invoke-static {}, Lcom/lge/ims/DataConnectionManager;->getInstance()Lcom/lge/ims/DataConnectionManager;

    #@7
    move-result-object v0

    #@8
    .line 162
    .local v0, dcm:Lcom/lge/ims/DataConnectionManager;
    if-eqz v0, :cond_f

    #@a
    .line 163
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@c
    invoke-virtual {v0, v2}, Lcom/lge/ims/DataConnectionManager;->unregisterForDataStateChanged(Landroid/os/Handler;)V

    #@f
    .line 166
    :cond_f
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@12
    move-result-object v1

    #@13
    .line 168
    .local v1, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v1, :cond_1f

    #@15
    .line 169
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@17
    invoke-virtual {v1, v2}, Lcom/lge/ims/PhoneStateTracker;->unregisterForAirplaneModeChanged(Landroid/os/Handler;)V

    #@1a
    .line 170
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@1c
    invoke-virtual {v1, v2}, Lcom/lge/ims/PhoneStateTracker;->unregisterForSimStateChanged(Landroid/os/Handler;)V

    #@1f
    .line 173
    .end local v0           #dcm:Lcom/lge/ims/DataConnectionManager;
    .end local v1           #pst:Lcom/lge/ims/PhoneStateTracker;
    :cond_1f
    return-void
.end method

.method public getProvisionedServices()I
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v0, 0x0

    #@2
    const/4 v4, 0x1

    #@3
    .line 186
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@6
    move-result v2

    #@7
    if-nez v2, :cond_15

    #@9
    .line 187
    const/16 v0, 0x101

    #@b
    .line 188
    .local v0, provisionedServices:I
    const-string v2, "ACService"

    #@d
    const-string v3, "AC is off, VoLTE & VT service is provisioned"

    #@f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 189
    const/16 v0, 0x101

    #@14
    .line 216
    .end local v0           #provisionedServices:I
    :cond_14
    :goto_14
    return v0

    #@15
    .line 192
    :cond_15
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@17
    invoke-static {v2}, Lcom/lge/ims/service/ac/ACContentHelper;->getServiceAvailability(Landroid/content/Context;)[Ljava/lang/Integer;

    #@1a
    move-result-object v1

    #@1b
    .line 194
    .local v1, serviceAvailability:[Ljava/lang/Integer;
    if-eqz v1, :cond_14

    #@1d
    .line 198
    array-length v2, v1

    #@1e
    const/4 v3, 0x3

    #@1f
    if-lt v2, v3, :cond_14

    #@21
    .line 202
    const/4 v0, 0x0

    #@22
    .line 205
    .restart local v0       #provisionedServices:I
    aget-object v2, v1, v4

    #@24
    if-eqz v2, :cond_37

    #@26
    aget-object v2, v1, v4

    #@28
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@2b
    move-result v2

    #@2c
    if-ne v2, v4, :cond_37

    #@2e
    .line 206
    or-int/lit8 v0, v0, 0x1

    #@30
    .line 207
    const-string v2, "ACService"

    #@32
    const-string v3, "VoLTE service is provisioned"

    #@34
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@37
    .line 211
    :cond_37
    aget-object v2, v1, v5

    #@39
    if-eqz v2, :cond_14

    #@3b
    aget-object v2, v1, v5

    #@3d
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@40
    move-result v2

    #@41
    if-ne v2, v4, :cond_14

    #@43
    .line 212
    or-int/lit16 v0, v0, 0x100

    #@45
    .line 213
    const-string v2, "ACService"

    #@47
    const-string v3, "VT service is provisioned"

    #@49
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@4c
    goto :goto_14
.end method

.method public getServerInterworkingResult()I
    .registers 2

    #@0
    .prologue
    .line 177
    iget v0, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResult:I

    #@2
    return v0
.end method

.method public getTimestampForServerInterworkingResult()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 181
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mTimestampForServerInterworkingResult:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public isConfigurationAccessAllowed()Z
    .registers 3

    #@0
    .prologue
    .line 220
    invoke-static {}, Lcom/lge/ims/Configuration;->isACOn()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_f

    #@6
    .line 221
    const-string v0, "ACService"

    #@8
    const-string v1, "AC is off, configuration access is allowed"

    #@a
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 222
    const/4 v0, 0x1

    #@e
    .line 225
    :goto_e
    return v0

    #@f
    :cond_f
    iget-boolean v0, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationAccessAllowed:Z

    #@11
    goto :goto_e
.end method

.method public registerForConfigurationAccessAllowed(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationAccessAllowedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 238
    return-void
.end method

.method public registerForConfigurationUpdateCompleted(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 246
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationUpdateCompletedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 247
    return-void
.end method

.method public registerForServerInterworkingResult(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 255
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResultRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 256
    return-void
.end method

.method public requestConfigurationRetrieval()V
    .registers 5

    #@0
    .prologue
    .line 229
    iget-object v1, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@2
    if-eqz v1, :cond_24

    #@4
    .line 230
    iget-object v1, p0, Lcom/lge/ims/service/ac/ACService;->mACSHandler:Lcom/lge/ims/service/ac/ACService$ACServiceHandler;

    #@6
    const/16 v2, 0xc9

    #@8
    invoke-virtual {v1, v2}, Lcom/lge/ims/service/ac/ACService$ACServiceHandler;->sendEmptyMessage(I)Z

    #@b
    move-result v0

    #@c
    .line 231
    .local v0, result:Z
    const-string v1, "ACService"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "Configuration retrieval is explicitly requested - result="

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@24
    .line 233
    .end local v0           #result:Z
    :cond_24
    return-void
.end method

.method public start(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const/16 v5, 0x7d0

    #@2
    const/16 v4, 0xfa0

    #@4
    .line 109
    iput-object p1, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@6
    .line 111
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@8
    invoke-static {v2}, Lcom/lge/ims/service/ac/ACConfiguration;->init(Landroid/content/Context;)V

    #@b
    .line 113
    sget-object v2, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@d
    const-string v3, "KT"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_b1

    #@15
    .line 115
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@17
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1e
    .line 116
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@20
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@27
    .line 117
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@29
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@30
    .line 118
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@32
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@39
    .line 120
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@3b
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@3e
    move-result v2

    #@3f
    iput v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@41
    .line 123
    const/16 v2, 0x1c20

    #@43
    iput v2, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerInterval:I

    #@45
    .line 125
    const-string v2, "ACService"

    #@47
    new-instance v3, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v4, "RecoveryTimeInterval="

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    iget v4, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerInterval:I

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    const-string v4, ", RetryCount="

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v3

    #@5e
    iget v4, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v3

    #@68
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@6b
    .line 140
    :cond_6b
    :goto_6b
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mACSThread:Lcom/lge/ims/service/ac/ACService$ACServiceThread;

    #@6d
    if-eqz v2, :cond_74

    #@6f
    .line 141
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mACSThread:Lcom/lge/ims/service/ac/ACService$ACServiceThread;

    #@71
    invoke-virtual {v2}, Lcom/lge/ims/service/ac/ACService$ACServiceThread;->start()V

    #@74
    .line 144
    :cond_74
    const-string v2, "ACService"

    #@76
    new-instance v3, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v4, "cachedConfiguration :: flag="

    #@7d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    iget-object v4, p0, Lcom/lge/ims/service/ac/ACService;->mContext:Landroid/content/Context;

    #@83
    invoke-static {v4}, Lcom/lge/ims/service/ac/ACContentHelper;->hasCachedConfiguration(Landroid/content/Context;)Z

    #@86
    move-result v4

    #@87
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v3

    #@8b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v3

    #@8f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@92
    .line 146
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@95
    move-result-object v1

    #@96
    .line 148
    .local v1, pst:Lcom/lge/ims/PhoneStateTracker;
    if-eqz v1, :cond_b0

    #@98
    .line 149
    invoke-virtual {v1}, Lcom/lge/ims/PhoneStateTracker;->getIccState()Ljava/lang/String;

    #@9b
    move-result-object v0

    #@9c
    .line 151
    .local v0, iccState:Ljava/lang/String;
    if-eqz v0, :cond_b0

    #@9e
    const-string v2, "LOADED"

    #@a0
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a3
    move-result v2

    #@a4
    if-eqz v2, :cond_b0

    #@a6
    .line 152
    const/4 v2, 0x1

    #@a7
    iput-boolean v2, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationAccessAllowed:Z

    #@a9
    .line 153
    const-string v2, "ACService"

    #@ab
    const-string v3, "AC configuration access is allowed"

    #@ad
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@b0
    .line 156
    .end local v0           #iccState:Ljava/lang/String;
    :cond_b0
    return-void

    #@b1
    .line 126
    .end local v1           #pst:Lcom/lge/ims/PhoneStateTracker;
    :cond_b1
    sget-object v2, Lcom/lge/ims/Configuration;->OPERATOR:Ljava/lang/String;

    #@b3
    const-string v3, "SKT"

    #@b5
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b8
    move-result v2

    #@b9
    if-eqz v2, :cond_6b

    #@bb
    .line 128
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@bd
    const/16 v3, 0x1f40

    #@bf
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c2
    move-result-object v3

    #@c3
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@c6
    .line 129
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@c8
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cb
    move-result-object v3

    #@cc
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@cf
    .line 130
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@d1
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d4
    move-result-object v3

    #@d5
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d8
    .line 132
    iget-object v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryIntervals:Ljava/util/ArrayList;

    #@da
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@dd
    move-result v2

    #@de
    iput v2, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@e0
    .line 135
    const/4 v2, -0x1

    #@e1
    iput v2, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerInterval:I

    #@e3
    .line 137
    const-string v2, "ACService"

    #@e5
    new-instance v3, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v4, "RecoveryTimeInterval="

    #@ec
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v3

    #@f0
    iget v4, p0, Lcom/lge/ims/service/ac/ACService;->mRecoveryTimerInterval:I

    #@f2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v3

    #@f6
    const-string v4, ", RetryCount="

    #@f8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v3

    #@fc
    iget v4, p0, Lcom/lge/ims/service/ac/ACService;->mRetryCount:I

    #@fe
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@101
    move-result-object v3

    #@102
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v3

    #@106
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@109
    goto/16 :goto_6b
.end method

.method public unregisterForConfigurationAccessAllowed(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 241
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationAccessAllowedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 242
    return-void
.end method

.method public unregisterForConfigurationUpdateCompleted(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 250
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mConfigurationUpdateCompletedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 251
    return-void
.end method

.method public unregisterForServerInterworkingResult(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 259
    iget-object v0, p0, Lcom/lge/ims/service/ac/ACService;->mServerInterworkingResultRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 260
    return-void
.end method
