.class public Lcom/lge/ims/service/uc/IncomingUCSession;
.super Ljava/lang/Object;
.source "IncomingUCSession.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CDIVCAUSE_BUSY:I = 0x2

.field public static final CDIVCAUSE_NOANSWER:I = 0x4

.field public static final CDIVCAUSE_NONE:I = 0x0

.field public static final CDIVCAUSE_REJECT:I = 0x3

.field public static final CDIVCAUSE_UNCONDITION:I = 0x1

.field public static final CREATOR:Landroid/os/Parcelable$Creator; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/uc/IncomingUCSession;",
            ">;"
        }
    .end annotation
.end field

.field public static final OIPTYPE_IDENTITY:I = 0x1

.field public static final OIPTYPE_NONE:I = 0x0

.field public static final OIPTYPE_RESTICTED:I = 0x2

.field public static final PARAM_INCOMING_SESSION:Ljava/lang/String; = "incomingUCSession"

.field private static final TAG:Ljava/lang/String; = "LGIMS"


# instance fields
.field public OIPType:I

.field public calleePartyNum:Ljava/lang/String;

.field public callerPartyNum:Ljava/lang/String;

.field public mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;

.field public sessInfo:Lcom/lge/ims/service/uc/SessInfo;

.field public sessionKey:I

.field public suppInfo:Lcom/lge/ims/service/uc/SuppInfo;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 96
    new-instance v0, Lcom/lge/ims/service/uc/IncomingUCSession$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/uc/IncomingUCSession$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/uc/IncomingUCSession;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .registers 4
    .parameter "source"

    #@0
    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 48
    const-string v0, "LGIMS"

    #@5
    const-string v1, "IncomingUCSession()"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 49
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/uc/IncomingUCSession;->readFromParcel(Landroid/os/Parcel;)V

    #@d
    .line 50
    return-void
.end method


# virtual methods
.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 93
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 5
    .parameter "source"

    #@0
    .prologue
    .line 54
    const-string v0, "LGIMS"

    #@2
    const-string v1, "readFromParcel"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@a
    move-result v0

    #@b
    iput v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->sessionKey:I

    #@d
    .line 58
    new-instance v0, Lcom/lge/ims/service/uc/SessInfo;

    #@f
    invoke-direct {v0, p1}, Lcom/lge/ims/service/uc/SessInfo;-><init>(Landroid/os/Parcel;)V

    #@12
    iput-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->sessInfo:Lcom/lge/ims/service/uc/SessInfo;

    #@14
    .line 59
    new-instance v0, Lcom/lge/ims/service/uc/MediaInfo;

    #@16
    invoke-direct {v0, p1}, Lcom/lge/ims/service/uc/MediaInfo;-><init>(Landroid/os/Parcel;)V

    #@19
    iput-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;

    #@1b
    .line 61
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@1e
    move-result v0

    #@1f
    iput v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->OIPType:I

    #@21
    .line 62
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    iput-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->calleePartyNum:Ljava/lang/String;

    #@27
    .line 63
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->callerPartyNum:Ljava/lang/String;

    #@2d
    .line 65
    new-instance v0, Lcom/lge/ims/service/uc/SuppInfo;

    #@2f
    invoke-direct {v0, p1}, Lcom/lge/ims/service/uc/SuppInfo;-><init>(Landroid/os/Parcel;)V

    #@32
    iput-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->suppInfo:Lcom/lge/ims/service/uc/SuppInfo;

    #@34
    .line 67
    const-string v0, "LGIMS"

    #@36
    new-instance v1, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v2, "sessionKey : "

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    iget v2, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->sessionKey:I

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 68
    iget-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->sessInfo:Lcom/lge/ims/service/uc/SessInfo;

    #@50
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/SessInfo;->logIn()V

    #@53
    .line 69
    iget-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;

    #@55
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/MediaInfo;->logIn()V

    #@58
    .line 70
    const-string v0, "LGIMS"

    #@5a
    new-instance v1, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v2, " OIPType : "

    #@61
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v1

    #@65
    iget v2, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->OIPType:I

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    const-string v2, " calleePartyNum : "

    #@6d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v1

    #@71
    iget-object v2, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->calleePartyNum:Ljava/lang/String;

    #@73
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v1

    #@77
    const-string v2, " callerPartyNum : "

    #@79
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v1

    #@7d
    iget-object v2, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->callerPartyNum:Ljava/lang/String;

    #@7f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v1

    #@83
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v1

    #@87
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 73
    iget-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->suppInfo:Lcom/lge/ims/service/uc/SuppInfo;

    #@8c
    invoke-virtual {v0}, Lcom/lge/ims/service/uc/SuppInfo;->logIn()V

    #@8f
    .line 75
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 5
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 79
    iget v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->sessionKey:I

    #@3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@6
    .line 81
    iget-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->sessInfo:Lcom/lge/ims/service/uc/SessInfo;

    #@8
    invoke-virtual {v0, p1, v1}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@b
    .line 82
    iget-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->mediaInfo:Lcom/lge/ims/service/uc/MediaInfo;

    #@d
    invoke-virtual {v0, p1, v1}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@10
    .line 84
    iget v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->OIPType:I

    #@12
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    #@15
    .line 85
    iget-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->calleePartyNum:Ljava/lang/String;

    #@17
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 86
    iget-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->callerPartyNum:Ljava/lang/String;

    #@1c
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1f
    .line 88
    iget-object v0, p0, Lcom/lge/ims/service/uc/IncomingUCSession;->suppInfo:Lcom/lge/ims/service/uc/SuppInfo;

    #@21
    invoke-virtual {v0, p1, v1}, Lcom/lge/ims/service/uc/SuppInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@24
    .line 90
    return-void
.end method
