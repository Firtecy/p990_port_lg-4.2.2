.class public Lcom/lge/ims/service/cs/ImageSessionImpl;
.super Lcom/lge/ims/service/cs/IImageSession$Stub;
.source "ImageSessionImpl.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;


# instance fields
.field private listener:Lcom/lge/ims/service/cs/IImageSessionListener;

.field private nSessionType:I

.field private nativeObj:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 18
    invoke-direct {p0}, Lcom/lge/ims/service/cs/IImageSession$Stub;-><init>()V

    #@4
    .line 14
    iput v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nSessionType:I

    #@6
    .line 15
    iput v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@8
    .line 16
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->listener:Lcom/lge/ims/service/cs/IImageSessionListener;

    #@b
    .line 19
    const/16 v0, 0xd

    #@d
    invoke-static {v0}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@10
    move-result v0

    #@11
    iput v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@13
    .line 20
    iget v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@15
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@18
    .line 21
    const/4 v0, 0x1

    #@19
    iput v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nSessionType:I

    #@1b
    .line 22
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "nativeObj"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 24
    invoke-direct {p0}, Lcom/lge/ims/service/cs/IImageSession$Stub;-><init>()V

    #@4
    .line 14
    iput v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nSessionType:I

    #@6
    .line 15
    iput v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@8
    .line 16
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->listener:Lcom/lge/ims/service/cs/IImageSessionListener;

    #@b
    .line 25
    iput p1, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@d
    .line 26
    invoke-static {p1, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@10
    .line 27
    const/4 v0, 0x2

    #@11
    iput v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nSessionType:I

    #@13
    .line 28
    return-void
.end method


# virtual methods
.method public accept(Ljava/lang/String;)I
    .registers 5
    .parameter "filePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 123
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 124
    iget v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 125
    const/4 v2, 0x0

    #@a
    .line 137
    :goto_a
    return v2

    #@b
    .line 128
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 129
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2906

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 130
    const/4 v2, 0x1

    #@15
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 132
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1b
    .line 133
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1e
    move-result-object v0

    #@1f
    .line 134
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@22
    .line 135
    const/4 v1, 0x0

    #@23
    .line 137
    iget v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@25
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@28
    move-result v2

    #@29
    goto :goto_a
.end method

.method public getSessionType()I
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 83
    iget v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nSessionType:I

    #@2
    return v0
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 7
    .parameter "parcel"

    #@0
    .prologue
    .line 41
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 43
    iget-object v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->listener:Lcom/lge/ims/service/cs/IImageSessionListener;

    #@7
    if-nez v2, :cond_f

    #@9
    .line 44
    const-string v2, "called but listener is null"

    #@b
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@e
    .line 74
    :goto_e
    return-void

    #@f
    .line 48
    :cond_f
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@12
    move-result v1

    #@13
    .line 51
    .local v1, msg:I
    packed-switch v1, :pswitch_data_78

    #@16
    .line 68
    :pswitch_16
    :try_start_16
    new-instance v2, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v3, "unknown message received from native msg : "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V
    :try_end_2c
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_2c} :catch_2d

    #@2c
    goto :goto_e

    #@2d
    .line 71
    :catch_2d
    move-exception v0

    #@2e
    .line 72
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@39
    goto :goto_e

    #@3a
    .line 53
    .end local v0           #e:Landroid/os/RemoteException;
    :pswitch_3a
    :try_start_3a
    iget-object v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->listener:Lcom/lge/ims/service/cs/IImageSessionListener;

    #@3c
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3f
    move-result v3

    #@40
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v4

    #@44
    invoke-interface {v2, v3, v4}, Lcom/lge/ims/service/cs/IImageSessionListener;->onStart(II)V

    #@47
    goto :goto_e

    #@48
    .line 56
    :pswitch_48
    iget-object v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->listener:Lcom/lge/ims/service/cs/IImageSessionListener;

    #@4a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@4d
    move-result v3

    #@4e
    invoke-interface {v2, v3}, Lcom/lge/ims/service/cs/IImageSessionListener;->onStop(I)V

    #@51
    goto :goto_e

    #@52
    .line 59
    :pswitch_52
    iget-object v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->listener:Lcom/lge/ims/service/cs/IImageSessionListener;

    #@54
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5b
    move-result v4

    #@5c
    invoke-interface {v2, v3, v4}, Lcom/lge/ims/service/cs/IImageSessionListener;->onReceivedFile(Ljava/lang/String;I)V

    #@5f
    goto :goto_e

    #@60
    .line 62
    :pswitch_60
    iget-object v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->listener:Lcom/lge/ims/service/cs/IImageSessionListener;

    #@62
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@65
    move-result v3

    #@66
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@69
    move-result v4

    #@6a
    invoke-interface {v2, v3, v4}, Lcom/lge/ims/service/cs/IImageSessionListener;->onProgress(II)V

    #@6d
    goto :goto_e

    #@6e
    .line 65
    :pswitch_6e
    iget-object v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->listener:Lcom/lge/ims/service/cs/IImageSessionListener;

    #@70
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v3

    #@74
    invoke-interface {v2, v3}, Lcom/lge/ims/service/cs/IImageSessionListener;->onTransferResult(I)V
    :try_end_77
    .catch Landroid/os/RemoteException; {:try_start_3a .. :try_end_77} :catch_2d

    #@77
    goto :goto_e

    #@78
    .line 51
    :pswitch_data_78
    .packed-switch 0x2907
        :pswitch_3a
        :pswitch_16
        :pswitch_48
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_52
        :pswitch_60
        :pswitch_6e
    .end packed-switch
.end method

.method public reject()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 141
    const-string v3, ""

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 142
    iget v3, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@8
    if-nez v3, :cond_b

    #@a
    .line 153
    :goto_a
    return v2

    #@b
    .line 146
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 147
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v3, 0x2906

    #@11
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 148
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 149
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1a
    move-result-object v0

    #@1b
    .line 150
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 151
    const/4 v1, 0x0

    #@1f
    .line 153
    iget v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@21
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@24
    move-result v2

    #@25
    goto :goto_a
.end method

.method public setListener(Lcom/lge/ims/service/cs/IImageSessionListener;)Z
    .registers 3
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 77
    iput-object p1, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->listener:Lcom/lge/ims/service/cs/IImageSessionListener;

    #@2
    .line 78
    const/4 v0, 0x1

    #@3
    return v0
.end method

.method public start(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)I
    .registers 8
    .parameter "peerPhoneNumber"
    .parameter "filePath"
    .parameter "fileName"
    .parameter "fileSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 88
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 89
    iget v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 90
    const/4 v2, 0x0

    #@a
    .line 104
    :goto_a
    return v2

    #@b
    .line 93
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 94
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2905

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 95
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 97
    invoke-virtual {v1, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1a
    .line 98
    invoke-virtual {v1, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@1d
    .line 99
    invoke-virtual {v1, p4}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 100
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@23
    move-result-object v0

    #@24
    .line 101
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 102
    const/4 v1, 0x0

    #@28
    .line 104
    iget v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@2a
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@2d
    move-result v2

    #@2e
    goto :goto_a
.end method

.method public stop()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 108
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 109
    iget v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 110
    const/4 v2, 0x0

    #@a
    .line 119
    :goto_a
    return v2

    #@b
    .line 113
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 114
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x2908

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 115
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@17
    move-result-object v0

    #@18
    .line 116
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 117
    const/4 v1, 0x0

    #@1c
    .line 119
    iget v2, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@1e
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@21
    move-result v2

    #@22
    goto :goto_a
.end method

.method public terminate()V
    .registers 2

    #@0
    .prologue
    .line 31
    iget v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@2
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@5
    .line 32
    iget v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@7
    invoke-static {v0}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@a
    .line 35
    const/4 v0, 0x0

    #@b
    iput v0, p0, Lcom/lge/ims/service/cs/ImageSessionImpl;->nativeObj:I

    #@d
    .line 38
    return-void
.end method
