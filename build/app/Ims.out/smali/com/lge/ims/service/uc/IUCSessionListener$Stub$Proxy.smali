.class Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;
.super Ljava/lang/Object;
.source "IUCSessionListener.java"

# interfaces
.implements Lcom/lge/ims/service/uc/IUCSessionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUCSessionListener$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 429
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 430
    iput-object p1, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 431
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 434
    iget-object v0, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public dropConfFailed(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 824
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 825
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 827
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 828
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 829
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x13

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 830
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 833
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 834
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 836
    return-void

    #@22
    .line 833
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 834
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public dropedConf()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 810
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 811
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 813
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 814
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x12

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 815
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 818
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 819
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 821
    return-void

    #@1f
    .line 818
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 819
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method

.method public expandConfFailed(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 693
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 694
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 696
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 697
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 698
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0xc

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 699
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 702
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 703
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 705
    return-void

    #@22
    .line 702
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 703
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public expandedConf(ILcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 9
    .parameter "sessionKey"
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 664
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 665
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 667
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 668
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 669
    if-eqz p2, :cond_36

    #@12
    .line 670
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 671
    const/4 v2, 0x0

    #@17
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 676
    :goto_1a
    if-eqz p3, :cond_43

    #@1c
    .line 677
    const/4 v2, 0x1

    #@1d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 678
    const/4 v2, 0x0

    #@21
    invoke-virtual {p3, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@24
    .line 683
    :goto_24
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@26
    const/16 v3, 0xb

    #@28
    const/4 v4, 0x0

    #@29
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2c
    .line 684
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2f
    .catchall {:try_start_8 .. :try_end_2f} :catchall_3b

    #@2f
    .line 687
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 688
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 690
    return-void

    #@36
    .line 674
    :cond_36
    const/4 v2, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_1a

    #@3b
    .line 687
    :catchall_3b
    move-exception v2

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 688
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v2

    #@43
    .line 681
    :cond_43
    const/4 v2, 0x0

    #@44
    :try_start_44
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_47
    .catchall {:try_start_44 .. :try_end_47} :catchall_3b

    #@47
    goto :goto_24
.end method

.method public expandedConfBy(ILcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 9
    .parameter "sessionKey"
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 708
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 709
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 711
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 712
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 713
    if-eqz p2, :cond_36

    #@12
    .line 714
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 715
    const/4 v2, 0x0

    #@17
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 720
    :goto_1a
    if-eqz p3, :cond_43

    #@1c
    .line 721
    const/4 v2, 0x1

    #@1d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 722
    const/4 v2, 0x0

    #@21
    invoke-virtual {p3, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@24
    .line 727
    :goto_24
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@26
    const/16 v3, 0xd

    #@28
    const/4 v4, 0x0

    #@29
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2c
    .line 728
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2f
    .catchall {:try_start_8 .. :try_end_2f} :catchall_3b

    #@2f
    .line 731
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 732
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 734
    return-void

    #@36
    .line 718
    :cond_36
    const/4 v2, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_1a

    #@3b
    .line 731
    :catchall_3b
    move-exception v2

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 732
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v2

    #@43
    .line 725
    :cond_43
    const/4 v2, 0x0

    #@44
    :try_start_44
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_47
    .catchall {:try_start_44 .. :try_end_47} :catchall_3b

    #@47
    goto :goto_24
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 438
    const-string v0, "com.lge.ims.service.uc.IUCSessionListener"

    #@2
    return-object v0
.end method

.method public held(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 507
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 508
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 510
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 511
    if-eqz p1, :cond_32

    #@f
    .line 512
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 513
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 518
    :goto_17
    if-eqz p2, :cond_3f

    #@19
    .line 519
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 520
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 525
    :goto_21
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/4 v3, 0x4

    #@24
    const/4 v4, 0x0

    #@25
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@28
    .line 526
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2b
    .catchall {:try_start_8 .. :try_end_2b} :catchall_37

    #@2b
    .line 529
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 530
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 532
    return-void

    #@32
    .line 516
    :cond_32
    const/4 v2, 0x0

    #@33
    :try_start_33
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_37

    #@36
    goto :goto_17

    #@37
    .line 529
    :catchall_37
    move-exception v2

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 530
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v2

    #@3f
    .line 523
    :cond_3f
    const/4 v2, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_37

    #@43
    goto :goto_21
.end method

.method public heldBy(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 550
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 551
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 553
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 554
    if-eqz p1, :cond_32

    #@f
    .line 555
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 556
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 561
    :goto_17
    if-eqz p2, :cond_3f

    #@19
    .line 562
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 563
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 568
    :goto_21
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/4 v3, 0x6

    #@24
    const/4 v4, 0x0

    #@25
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@28
    .line 569
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2b
    .catchall {:try_start_8 .. :try_end_2b} :catchall_37

    #@2b
    .line 572
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 573
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 575
    return-void

    #@32
    .line 559
    :cond_32
    const/4 v2, 0x0

    #@33
    :try_start_33
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_37

    #@36
    goto :goto_17

    #@37
    .line 572
    :catchall_37
    move-exception v2

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 573
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v2

    #@3f
    .line 566
    :cond_3f
    const/4 v2, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_37

    #@43
    goto :goto_21
.end method

.method public holdFailed(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 535
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 536
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 538
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 539
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 540
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/4 v3, 0x5

    #@13
    const/4 v4, 0x0

    #@14
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@17
    .line 541
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1a
    .catchall {:try_start_8 .. :try_end_1a} :catchall_21

    #@1a
    .line 544
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1d
    .line 545
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 547
    return-void

    #@21
    .line 544
    :catchall_21
    move-exception v2

    #@22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 545
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    throw v2
.end method

.method public incomingUpdate(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 855
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 856
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 858
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 859
    if-eqz p1, :cond_33

    #@f
    .line 860
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 861
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 866
    :goto_17
    if-eqz p2, :cond_40

    #@19
    .line 867
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 868
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 873
    :goto_21
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/16 v3, 0x15

    #@25
    const/4 v4, 0x0

    #@26
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 874
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2c
    .catchall {:try_start_8 .. :try_end_2c} :catchall_38

    #@2c
    .line 877
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 878
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 880
    return-void

    #@33
    .line 864
    :cond_33
    const/4 v2, 0x0

    #@34
    :try_start_34
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_38

    #@37
    goto :goto_17

    #@38
    .line 877
    :catchall_38
    move-exception v2

    #@39
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 878
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    throw v2

    #@40
    .line 871
    :cond_40
    const/4 v2, 0x0

    #@41
    :try_start_41
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_44
    .catchall {:try_start_41 .. :try_end_44} :catchall_38

    #@44
    goto :goto_21
.end method

.method public joinConfFailed(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 795
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 796
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 798
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 799
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 800
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x11

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 801
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 804
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 805
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 807
    return-void

    #@22
    .line 804
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 805
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public joinedConf()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 781
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 782
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 784
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 785
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@f
    const/16 v3, 0x10

    #@11
    const/4 v4, 0x0

    #@12
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@15
    .line 786
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_18
    .catchall {:try_start_8 .. :try_end_18} :catchall_1f

    #@18
    .line 789
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 790
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 792
    return-void

    #@1f
    .line 789
    :catchall_1f
    move-exception v2

    #@20
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 790
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    throw v2
.end method

.method public mergeFailed(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 766
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 767
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 769
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 770
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 771
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0xf

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 772
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 775
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 776
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 778
    return-void

    #@22
    .line 775
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 776
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public merged(ILcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 9
    .parameter "sessionKey"
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 737
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 738
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 740
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 741
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 742
    if-eqz p2, :cond_36

    #@12
    .line 743
    const/4 v2, 0x1

    #@13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@16
    .line 744
    const/4 v2, 0x0

    #@17
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@1a
    .line 749
    :goto_1a
    if-eqz p3, :cond_43

    #@1c
    .line 750
    const/4 v2, 0x1

    #@1d
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@20
    .line 751
    const/4 v2, 0x0

    #@21
    invoke-virtual {p3, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@24
    .line 756
    :goto_24
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@26
    const/16 v3, 0xe

    #@28
    const/4 v4, 0x0

    #@29
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@2c
    .line 757
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2f
    .catchall {:try_start_8 .. :try_end_2f} :catchall_3b

    #@2f
    .line 760
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 761
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@35
    .line 763
    return-void

    #@36
    .line 747
    :cond_36
    const/4 v2, 0x0

    #@37
    :try_start_37
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_3a
    .catchall {:try_start_37 .. :try_end_3a} :catchall_3b

    #@3a
    goto :goto_1a

    #@3b
    .line 760
    :catchall_3b
    move-exception v2

    #@3c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 761
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@42
    throw v2

    #@43
    .line 754
    :cond_43
    const/4 v2, 0x0

    #@44
    :try_start_44
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_47
    .catchall {:try_start_44 .. :try_end_47} :catchall_3b

    #@47
    goto :goto_24
.end method

.method public notifyConfInfo([I[Ljava/lang/String;)V
    .registers 8
    .parameter "infoType"
    .parameter "recipients"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 839
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 840
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 842
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 843
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeIntArray([I)V

    #@10
    .line 844
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeStringArray([Ljava/lang/String;)V

    #@13
    .line 845
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/16 v3, 0x14

    #@17
    const/4 v4, 0x0

    #@18
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1b
    .line 846
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1e
    .catchall {:try_start_8 .. :try_end_1e} :catchall_25

    #@1e
    .line 849
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 850
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 852
    return-void

    #@25
    .line 849
    :catchall_25
    move-exception v2

    #@26
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@29
    .line 850
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2c
    throw v2
.end method

.method public notifyInfo(IILjava/lang/String;)V
    .registers 9
    .parameter "eType"
    .parameter "nValue"
    .parameter "strValue"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 954
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 955
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 957
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 958
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 959
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 960
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@16
    .line 961
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/16 v3, 0x19

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 962
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_28

    #@21
    .line 965
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 966
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 968
    return-void

    #@28
    .line 965
    :catchall_28
    move-exception v2

    #@29
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2c
    .line 966
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2f
    throw v2
.end method

.method public progressing(Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 7
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 486
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 487
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 489
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 490
    if-eqz p1, :cond_28

    #@f
    .line 491
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 492
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 497
    :goto_17
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@19
    const/4 v3, 0x3

    #@1a
    const/4 v4, 0x0

    #@1b
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1e
    .line 498
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_21
    .catchall {:try_start_8 .. :try_end_21} :catchall_2d

    #@21
    .line 501
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@24
    .line 502
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@27
    .line 504
    return-void

    #@28
    .line 495
    :cond_28
    const/4 v2, 0x0

    #@29
    :try_start_29
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_2d

    #@2c
    goto :goto_17

    #@2d
    .line 501
    :catchall_2d
    move-exception v2

    #@2e
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 502
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@34
    throw v2
.end method

.method public resumeFailed(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 606
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 607
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 609
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 610
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 611
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x8

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 612
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 615
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 616
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 618
    return-void

    #@22
    .line 615
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 616
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public resumed(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 578
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 579
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 581
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 582
    if-eqz p1, :cond_32

    #@f
    .line 583
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 584
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 589
    :goto_17
    if-eqz p2, :cond_3f

    #@19
    .line 590
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 591
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 596
    :goto_21
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/4 v3, 0x7

    #@24
    const/4 v4, 0x0

    #@25
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@28
    .line 597
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2b
    .catchall {:try_start_8 .. :try_end_2b} :catchall_37

    #@2b
    .line 600
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 601
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 603
    return-void

    #@32
    .line 587
    :cond_32
    const/4 v2, 0x0

    #@33
    :try_start_33
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_37

    #@36
    goto :goto_17

    #@37
    .line 600
    :catchall_37
    move-exception v2

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 601
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v2

    #@3f
    .line 594
    :cond_3f
    const/4 v2, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_37

    #@43
    goto :goto_21
.end method

.method public resumedBy(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 621
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 622
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 624
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 625
    if-eqz p1, :cond_33

    #@f
    .line 626
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 627
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 632
    :goto_17
    if-eqz p2, :cond_40

    #@19
    .line 633
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 634
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 639
    :goto_21
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/16 v3, 0x9

    #@25
    const/4 v4, 0x0

    #@26
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 640
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2c
    .catchall {:try_start_8 .. :try_end_2c} :catchall_38

    #@2c
    .line 643
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 644
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 646
    return-void

    #@33
    .line 630
    :cond_33
    const/4 v2, 0x0

    #@34
    :try_start_34
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_38

    #@37
    goto :goto_17

    #@38
    .line 643
    :catchall_38
    move-exception v2

    #@39
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 644
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    throw v2

    #@40
    .line 637
    :cond_40
    const/4 v2, 0x0

    #@41
    :try_start_41
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_44
    .catchall {:try_start_41 .. :try_end_44} :catchall_38

    #@44
    goto :goto_21
.end method

.method public startFailed(II)V
    .registers 8
    .parameter "reason"
    .parameter "code"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 470
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 471
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 473
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 474
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 475
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 476
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v3, 0x2

    #@16
    const/4 v4, 0x0

    #@17
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 477
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_24

    #@1d
    .line 480
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 481
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 483
    return-void

    #@24
    .line 480
    :catchall_24
    move-exception v2

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 481
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v2
.end method

.method public started(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 442
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 443
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 445
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 446
    if-eqz p1, :cond_32

    #@f
    .line 447
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 448
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 453
    :goto_17
    if-eqz p2, :cond_3f

    #@19
    .line 454
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 455
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 460
    :goto_21
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/4 v3, 0x1

    #@24
    const/4 v4, 0x0

    #@25
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@28
    .line 461
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2b
    .catchall {:try_start_8 .. :try_end_2b} :catchall_37

    #@2b
    .line 464
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2e
    .line 465
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@31
    .line 467
    return-void

    #@32
    .line 451
    :cond_32
    const/4 v2, 0x0

    #@33
    :try_start_33
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_36
    .catchall {:try_start_33 .. :try_end_36} :catchall_37

    #@36
    goto :goto_17

    #@37
    .line 464
    :catchall_37
    move-exception v2

    #@38
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3b
    .line 465
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3e
    throw v2

    #@3f
    .line 458
    :cond_3f
    const/4 v2, 0x0

    #@40
    :try_start_40
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_37

    #@43
    goto :goto_21
.end method

.method public terminated(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 649
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 650
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 652
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 653
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 654
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0xa

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 655
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 658
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 659
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 661
    return-void

    #@22
    .line 658
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 659
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public updateFailed(I)V
    .registers 7
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 911
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 912
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 914
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 915
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 916
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@12
    const/16 v3, 0x17

    #@14
    const/4 v4, 0x0

    #@15
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@18
    .line 917
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1b
    .catchall {:try_start_8 .. :try_end_1b} :catchall_22

    #@1b
    .line 920
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 921
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 923
    return-void

    #@22
    .line 920
    :catchall_22
    move-exception v2

    #@23
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 921
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@29
    throw v2
.end method

.method public updated(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 883
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 884
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 886
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 887
    if-eqz p1, :cond_33

    #@f
    .line 888
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 889
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 894
    :goto_17
    if-eqz p2, :cond_40

    #@19
    .line 895
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 896
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 901
    :goto_21
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/16 v3, 0x16

    #@25
    const/4 v4, 0x0

    #@26
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 902
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2c
    .catchall {:try_start_8 .. :try_end_2c} :catchall_38

    #@2c
    .line 905
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 906
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 908
    return-void

    #@33
    .line 892
    :cond_33
    const/4 v2, 0x0

    #@34
    :try_start_34
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_38

    #@37
    goto :goto_17

    #@38
    .line 905
    :catchall_38
    move-exception v2

    #@39
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 906
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    throw v2

    #@40
    .line 899
    :cond_40
    const/4 v2, 0x0

    #@41
    :try_start_41
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_44
    .catchall {:try_start_41 .. :try_end_44} :catchall_38

    #@44
    goto :goto_21
.end method

.method public updatedBy(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V
    .registers 8
    .parameter "sessInfo"
    .parameter "mediaInfo"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 926
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 927
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 929
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCSessionListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 930
    if-eqz p1, :cond_33

    #@f
    .line 931
    const/4 v2, 0x1

    #@10
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 932
    const/4 v2, 0x0

    #@14
    invoke-virtual {p1, v0, v2}, Lcom/lge/ims/service/uc/SessInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@17
    .line 937
    :goto_17
    if-eqz p2, :cond_40

    #@19
    .line 938
    const/4 v2, 0x1

    #@1a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@1d
    .line 939
    const/4 v2, 0x0

    #@1e
    invoke-virtual {p2, v0, v2}, Lcom/lge/ims/service/uc/MediaInfo;->writeToParcel(Landroid/os/Parcel;I)V

    #@21
    .line 944
    :goto_21
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@23
    const/16 v3, 0x18

    #@25
    const/4 v4, 0x0

    #@26
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@29
    .line 945
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_2c
    .catchall {:try_start_8 .. :try_end_2c} :catchall_38

    #@2c
    .line 948
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 949
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 951
    return-void

    #@33
    .line 935
    :cond_33
    const/4 v2, 0x0

    #@34
    :try_start_34
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_37
    .catchall {:try_start_34 .. :try_end_37} :catchall_38

    #@37
    goto :goto_17

    #@38
    .line 948
    :catchall_38
    move-exception v2

    #@39
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3c
    .line 949
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@3f
    throw v2

    #@40
    .line 942
    :cond_40
    const/4 v2, 0x0

    #@41
    :try_start_41
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInt(I)V
    :try_end_44
    .catchall {:try_start_41 .. :try_end_44} :catchall_38

    #@44
    goto :goto_21
.end method
