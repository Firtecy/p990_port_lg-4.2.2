.class public Lcom/lge/ims/service/cd/CapabilityServiceThread;
.super Ljava/lang/Thread;
.source "CapabilityServiceThread.java"


# static fields
.field private static final EXIT:I = 0x1

.field public static final MSG_SERVICE:I = 0x64

.field public static final MSG_SERVICE_METHOD:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "CapServiceThread"

.field private static final TEST_MODE:Z = false

.field private static final USER_MESSAGE:I = 0xa

.field private static mCapServiceThread:Lcom/lge/ims/service/cd/CapabilityServiceThread;


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mListeners:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/lge/ims/JNICoreListener;",
            ">;"
        }
    .end annotation
.end field

.field private mMessageQ:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private mServiceListener:Lcom/lge/ims/JNICoreListener;

.field private tid:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 42
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mCapServiceThread:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 45
    const-string v0, "CapabilityServiceThread"

    #@3
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@6
    .line 28
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mMessageQ:Ljava/util/List;

    #@d
    .line 30
    iput-object v1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mHandler:Landroid/os/Handler;

    #@f
    .line 39
    iput-object v1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mServiceListener:Lcom/lge/ims/JNICoreListener;

    #@11
    .line 40
    iput-object v1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mListeners:Ljava/util/HashMap;

    #@13
    .line 47
    new-instance v0, Ljava/util/HashMap;

    #@15
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@18
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mListeners:Ljava/util/HashMap;

    #@1a
    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/service/cd/CapabilityServiceThread;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->tid:I

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/lge/ims/service/cd/CapabilityServiceThread;Landroid/os/Handler;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mHandler:Landroid/os/Handler;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/lge/ims/service/cd/CapabilityServiceThread;)Lcom/lge/ims/JNICoreListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mServiceListener:Lcom/lge/ims/JNICoreListener;

    #@2
    return-object v0
.end method

.method public static getInstance()Lcom/lge/ims/service/cd/CapabilityServiceThread;
    .registers 1

    #@0
    .prologue
    .line 51
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mCapServiceThread:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@2
    if-nez v0, :cond_14

    #@4
    .line 52
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@6
    invoke-direct {v0}, Lcom/lge/ims/service/cd/CapabilityServiceThread;-><init>()V

    #@9
    sput-object v0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mCapServiceThread:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@b
    .line 54
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mCapServiceThread:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@d
    if-eqz v0, :cond_14

    #@f
    .line 55
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mCapServiceThread:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@11
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityServiceThread;->start()V

    #@14
    .line 59
    :cond_14
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mCapServiceThread:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@16
    return-object v0
.end method

.method public static isTestModeEnabled()Z
    .registers 1

    #@0
    .prologue
    .line 64
    const/4 v0, 0x0

    #@1
    return v0
.end method


# virtual methods
.method public addListener(ILcom/lge/ims/JNICoreListener;)V
    .registers 6
    .parameter "object"
    .parameter "listener"

    #@0
    .prologue
    .line 80
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mListeners:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 81
    :try_start_3
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mListeners:Ljava/util/HashMap;

    #@5
    new-instance v2, Ljava/lang/Integer;

    #@7
    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    #@a
    invoke-virtual {v0, v2, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 82
    monitor-exit v1

    #@e
    .line 83
    return-void

    #@f
    .line 82
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public exit()V
    .registers 3

    #@0
    .prologue
    .line 68
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x1

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@6
    .line 77
    return-void
.end method

.method public getListener(I)Lcom/lge/ims/JNICoreListener;
    .registers 7
    .parameter "object"

    #@0
    .prologue
    .line 86
    const/4 v1, 0x0

    #@1
    .line 88
    .local v1, listener:Lcom/lge/ims/JNICoreListener;
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mListeners:Ljava/util/HashMap;

    #@3
    monitor-enter v3

    #@4
    .line 89
    :try_start_4
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mListeners:Ljava/util/HashMap;

    #@6
    new-instance v4, Ljava/lang/Integer;

    #@8
    invoke-direct {v4, p1}, Ljava/lang/Integer;-><init>(I)V

    #@b
    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    move-result-object v2

    #@f
    move-object v0, v2

    #@10
    check-cast v0, Lcom/lge/ims/JNICoreListener;

    #@12
    move-object v1, v0

    #@13
    .line 90
    monitor-exit v3

    #@14
    .line 92
    return-object v1

    #@15
    .line 90
    :catchall_15
    move-exception v2

    #@16
    monitor-exit v3
    :try_end_17
    .catchall {:try_start_4 .. :try_end_17} :catchall_15

    #@17
    throw v2
.end method

.method public removeAllListeners()V
    .registers 3

    #@0
    .prologue
    .line 102
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mListeners:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 103
    :try_start_3
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mListeners:Ljava/util/HashMap;

    #@5
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@8
    .line 104
    monitor-exit v1

    #@9
    .line 105
    return-void

    #@a
    .line 104
    :catchall_a
    move-exception v0

    #@b
    monitor-exit v1
    :try_end_c
    .catchall {:try_start_3 .. :try_end_c} :catchall_a

    #@c
    throw v0
.end method

.method public removeListener(I)V
    .registers 5
    .parameter "object"

    #@0
    .prologue
    .line 96
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mListeners:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 97
    :try_start_3
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mListeners:Ljava/util/HashMap;

    #@5
    new-instance v2, Ljava/lang/Integer;

    #@7
    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(I)V

    #@a
    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 98
    monitor-exit v1

    #@e
    .line 99
    return-void

    #@f
    .line 98
    :catchall_f
    move-exception v0

    #@10
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_3 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method public run()V
    .registers 2

    #@0
    .prologue
    .line 135
    invoke-static {}, Landroid/os/Looper;->prepare()V

    #@3
    .line 137
    invoke-static {}, Landroid/os/Process;->myTid()I

    #@6
    move-result v0

    #@7
    iput v0, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->tid:I

    #@9
    .line 142
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityServiceThread$1;

    #@b
    invoke-direct {v0, p0}, Lcom/lge/ims/service/cd/CapabilityServiceThread$1;-><init>(Lcom/lge/ims/service/cd/CapabilityServiceThread;)V

    #@e
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mHandler:Landroid/os/Handler;

    #@10
    .line 224
    invoke-static {}, Landroid/os/Looper;->loop()V

    #@13
    .line 225
    return-void
.end method

.method public sendMessage(IILandroid/os/Parcel;)V
    .registers 8
    .parameter "msgType"
    .parameter "object"
    .parameter "parcel"

    #@0
    .prologue
    .line 112
    const-string v1, "CapServiceThread"

    #@2
    const-string v2, "sendMessage"

    #@4
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 114
    if-nez p3, :cond_11

    #@9
    .line 115
    const-string v1, "CapServiceThread"

    #@b
    const-string v2, "parcel is null"

    #@d
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 132
    :goto_10
    return-void

    #@11
    .line 119
    :cond_11
    const/4 v1, 0x0

    #@12
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->setDataPosition(I)V

    #@15
    .line 121
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@18
    move-result-object v0

    #@19
    .line 123
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    #@1b
    .line 124
    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1d
    .line 125
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@1f
    .line 127
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mHandler:Landroid/os/Handler;

    #@21
    if-eqz v1, :cond_2b

    #@23
    .line 128
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mHandler:Landroid/os/Handler;

    #@25
    const-wide/16 v2, 0xbb8

    #@27
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2a
    goto :goto_10

    #@2b
    .line 130
    :cond_2b
    const-string v1, "CapServiceThread"

    #@2d
    const-string v2, "Handler is null"

    #@2f
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@32
    goto :goto_10
.end method

.method public setServiceListener(Lcom/lge/ims/JNICoreListener;)V
    .registers 2
    .parameter "listener"

    #@0
    .prologue
    .line 108
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread;->mServiceListener:Lcom/lge/ims/JNICoreListener;

    #@2
    .line 109
    return-void
.end method
