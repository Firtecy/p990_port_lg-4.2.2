.class public Lcom/lge/ims/service/IUIMS;
.super Ljava/lang/Object;
.source "IUIMS.java"


# static fields
.field public static final APP_BASE:I = -0x1

.field public static final APP_IM:I = 0x2

.field public static final APP_IS:I = 0x1

.field public static final APP_MAX:I = 0xa

.field public static final APP_PRESENCE:I = 0x3

.field public static final APP_RCS:I = 0x4

.field public static final APP_SMS:I = 0x5

.field public static final APP_UC:I = 0x0

.field public static final APP_VOIP:I = 0x6

.field public static final APP_VS:I = 0x7

.field public static final APP_VT:I = 0x8

.field public static final APP_XDM:I = 0x9

.field public static final M_APP_IM:I = 0x4

.field public static final M_APP_IS:I = 0x2

.field public static final M_APP_PRESENCE:I = 0x8

.field public static final M_APP_RCS:I = 0x10

.field public static final M_APP_SMS:I = 0x20

.field public static final M_APP_UC:I = 0x1

.field public static final M_APP_VOIP:I = 0x40

.field public static final M_APP_VS:I = 0x80

.field public static final M_APP_VT:I = 0x100

.field public static final M_APP_XDM:I = 0x200

.field public static final M_RCS_ALL:I = 0xff0000

.field public static final M_RCS_CD:I = 0x10000

.field public static final M_RCS_CS:I = 0x20000

.field public static final M_RCS_CS_IMAGE:I = 0x40000

.field public static final M_RCS_CS_VIDEO:I = 0x80000

.field public static final M_RCS_IM:I = 0x100000

.field public static final M_RCS_IP:I = 0x200000

.field public static final M_RCS_IP_XDM:I = 0x400000

.field public static final M_SERVICE_ALL:I = -0x1

.field public static final RCS_CD:I = 0x1f

.field public static final RCS_CD_CAPABILITY_QUERY:I = 0x20

.field public static final RCS_CS:I = 0xb

.field public static final RCS_CS_IMAGE:I = 0xd

.field public static final RCS_CS_VIDEO:I = 0xc

.field public static final RCS_IM:I = 0x15

.field public static final RCS_IM_CHAT:I = 0x16

.field public static final RCS_IM_CONF:I = 0x17

.field public static final RCS_IM_FT:I = 0x18

.field public static final RCS_IM_FT_HTTP:I = 0x19

.field public static final RCS_IP:I = 0x29

.field public static final RCS_IP_XDM:I = 0x2e

.field public static final UC_E_SESSION:I = 0xca

.field public static final UC_N_OFFLINE_SESSION:I = 0xcb

.field public static final UC_N_SESSION:I = 0xc9


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 14
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
