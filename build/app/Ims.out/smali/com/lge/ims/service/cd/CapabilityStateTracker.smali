.class public Lcom/lge/ims/service/cd/CapabilityStateTracker;
.super Ljava/lang/Object;
.source "CapabilityStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "CapService"

.field private static final TEST_MODE:Z

.field private static mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;


# instance fields
.field private mCallStateChangedRegistrants:Landroid/os/RegistrantList;

.field private mContext:Landroid/content/Context;

.field private mExternalStorageAvailabilityRequired:Z

.field private mExternalStorageAvailable:Z

.field private mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

.field private mExternalStorageStateChangedRegistrants:Landroid/os/RegistrantList;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 24
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@3
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 35
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 26
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mContext:Landroid/content/Context;

    #@7
    .line 27
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    #@9
    .line 28
    new-instance v0, Landroid/os/RegistrantList;

    #@b
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@e
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mCallStateChangedRegistrants:Landroid/os/RegistrantList;

    #@10
    .line 29
    new-instance v0, Landroid/os/RegistrantList;

    #@12
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@15
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageStateChangedRegistrants:Landroid/os/RegistrantList;

    #@17
    .line 30
    iput-boolean v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageAvailable:Z

    #@19
    .line 31
    iput-boolean v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageAvailabilityRequired:Z

    #@1b
    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/service/cd/CapabilityStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->updateExternalStorageState()V

    #@3
    return-void
.end method

.method public static getInstance()Lcom/lge/ims/service/cd/CapabilityStateTracker;
    .registers 1

    #@0
    .prologue
    .line 39
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 40
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@6
    invoke-direct {v0}, Lcom/lge/ims/service/cd/CapabilityStateTracker;-><init>()V

    #@9
    sput-object v0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@b
    .line 43
    :cond_b
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@d
    return-object v0
.end method

.method private updateExternalStorageState()V
    .registers 4

    #@0
    .prologue
    .line 138
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 140
    .local v0, state:Ljava/lang/String;
    const-string v1, "mounted"

    #@6
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_1c

    #@c
    .line 141
    const-string v1, "CapService"

    #@e
    const-string v2, "External storage is available"

    #@10
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    .line 143
    const/4 v1, 0x1

    #@14
    iput-boolean v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageAvailable:Z

    #@16
    .line 150
    :goto_16
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageStateChangedRegistrants:Landroid/os/RegistrantList;

    #@18
    invoke-virtual {v1}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@1b
    .line 151
    return-void

    #@1c
    .line 145
    :cond_1c
    const-string v1, "CapService"

    #@1e
    const-string v2, "External storage is unavailable"

    #@20
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    .line 147
    const/4 v1, 0x0

    #@24
    iput-boolean v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageAvailable:Z

    #@26
    goto :goto_16
.end method


# virtual methods
.method public isExternalStorageAvailable()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 47
    iget-boolean v2, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageAvailabilityRequired:Z

    #@3
    if-nez v2, :cond_6

    #@5
    .line 61
    :goto_5
    return v1

    #@6
    .line 51
    :cond_6
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    #@8
    if-eqz v2, :cond_d

    #@a
    .line 52
    iget-boolean v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageAvailable:Z

    #@c
    goto :goto_5

    #@d
    .line 54
    :cond_d
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    .line 56
    .local v0, state:Ljava/lang/String;
    const-string v2, "mounted"

    #@13
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_21

    #@19
    .line 57
    const-string v2, "CapService"

    #@1b
    const-string v3, "External storage is available"

    #@1d
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    goto :goto_5

    #@21
    .line 60
    :cond_21
    const-string v1, "CapService"

    #@23
    const-string v2, "External storage is unavailable"

    #@25
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    .line 61
    const/4 v1, 0x0

    #@29
    goto :goto_5
.end method

.method public registerForCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mCallStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 68
    return-void
.end method

.method public registerForExternalStorageStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 75
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 76
    return-void
.end method

.method public requireExternalStorageAvailability(Z)V
    .registers 2
    .parameter "isRequired"

    #@0
    .prologue
    .line 83
    iput-boolean p1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageAvailabilityRequired:Z

    #@2
    .line 84
    return-void
.end method

.method public setActiveCallNumber(ILjava/lang/String;)V
    .registers 5
    .parameter "callType"
    .parameter "number"

    #@0
    .prologue
    .line 91
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mCallStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;

    #@4
    invoke-direct {v1, p0, p1, p2}, Lcom/lge/ims/service/cd/CapabilityStateTracker$CallInfo;-><init>(Lcom/lge/ims/service/cd/CapabilityStateTracker;ILjava/lang/String;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->notifyResult(Ljava/lang/Object;)V

    #@a
    .line 92
    return-void
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 2
    .parameter "context"

    #@0
    .prologue
    .line 87
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mContext:Landroid/content/Context;

    #@2
    .line 88
    return-void
.end method

.method public startWatchingExternalStorage()V
    .registers 4

    #@0
    .prologue
    .line 95
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mContext:Landroid/content/Context;

    #@2
    if-nez v1, :cond_8

    #@4
    .line 96
    invoke-direct {p0}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->updateExternalStorageState()V

    #@7
    .line 123
    :goto_7
    return-void

    #@8
    .line 100
    :cond_8
    const-string v1, "CapService"

    #@a
    const-string v2, "Start watching an external storage ..."

    #@c
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 102
    new-instance v1, Lcom/lge/ims/service/cd/CapabilityStateTracker$1;

    #@11
    invoke-direct {v1, p0}, Lcom/lge/ims/service/cd/CapabilityStateTracker$1;-><init>(Lcom/lge/ims/service/cd/CapabilityStateTracker;)V

    #@14
    iput-object v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    #@16
    .line 113
    new-instance v0, Landroid/content/IntentFilter;

    #@18
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@1b
    .line 115
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    #@1d
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@20
    .line 116
    const-string v1, "android.intent.action.MEDIA_REMOVED"

    #@22
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@25
    .line 117
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    #@27
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2a
    .line 118
    const-string v1, "file"

    #@2c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@2f
    .line 120
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mContext:Landroid/content/Context;

    #@31
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    #@33
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@36
    .line 122
    invoke-direct {p0}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->updateExternalStorageState()V

    #@39
    goto :goto_7
.end method

.method public stopWatchingExternalStorage()V
    .registers 3

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mContext:Landroid/content/Context;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 135
    :cond_4
    :goto_4
    return-void

    #@5
    .line 130
    :cond_5
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    #@7
    if-eqz v0, :cond_4

    #@9
    .line 134
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mContext:Landroid/content/Context;

    #@b
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageReceiver:Landroid/content/BroadcastReceiver;

    #@d
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@10
    goto :goto_4
.end method

.method public unregisterForCallStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mCallStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 72
    return-void
.end method

.method public unregisterForExternalStorageStateChanged(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 79
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityStateTracker;->mExternalStorageStateChangedRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 80
    return-void
.end method
