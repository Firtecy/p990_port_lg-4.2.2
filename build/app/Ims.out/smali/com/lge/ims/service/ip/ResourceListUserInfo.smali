.class public Lcom/lge/ims/service/ip/ResourceListUserInfo;
.super Ljava/lang/Object;
.source "ResourceListUserInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/lge/ims/service/ip/ResourceListUserInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private nCurrentIndex:I

.field private nCurrentSize:I

.field private nRemainUser:I

.field private nTotalSize:I

.field private szResourceListUser:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 27
    new-instance v0, Lcom/lge/ims/service/ip/ResourceListUserInfo$1;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/service/ip/ResourceListUserInfo$1;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 42
    iput v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentIndex:I

    #@6
    .line 43
    iput v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nTotalSize:I

    #@8
    .line 44
    iput v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentSize:I

    #@a
    .line 45
    iput v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nRemainUser:I

    #@c
    .line 54
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 56
    invoke-virtual {p0, p1}, Lcom/lge/ims/service/ip/ResourceListUserInfo;->readFromParcel(Landroid/os/Parcel;)V

    #@6
    .line 57
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/lge/ims/service/ip/ResourceListUserInfo$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/lge/ims/service/ip/ResourceListUserInfo;-><init>(Landroid/os/Parcel;)V

    #@3
    return-void
.end method


# virtual methods
.method public GetCurrentResourceListUserSize()I
    .registers 2

    #@0
    .prologue
    .line 84
    iget v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentSize:I

    #@2
    return v0
.end method

.method public GetRemainResourceListUser()I
    .registers 2

    #@0
    .prologue
    .line 96
    iget v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nRemainUser:I

    #@2
    return v0
.end method

.method public GetResourceListUser(I)Ljava/lang/String;
    .registers 3
    .parameter "nIndex"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->szResourceListUser:[Ljava/lang/String;

    #@2
    aget-object v0, v0, p1

    #@4
    return-object v0
.end method

.method public GetResourceListUser()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->szResourceListUser:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public GetResourceListUserIndex()I
    .registers 2

    #@0
    .prologue
    .line 103
    iget v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentIndex:I

    #@2
    return v0
.end method

.method public GetTotalResourceListUserSize()I
    .registers 2

    #@0
    .prologue
    .line 77
    iget v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nTotalSize:I

    #@2
    return v0
.end method

.method public SetCurrentResourceListUserSize(I)V
    .registers 5
    .parameter "_nCurrentSize"

    #@0
    .prologue
    .line 87
    iput p1, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentSize:I

    #@2
    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[IP]nCurrentSize["

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, "]"

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v1}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1e
    .line 89
    iget v1, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentSize:I

    #@20
    new-array v1, v1, [Ljava/lang/String;

    #@22
    iput-object v1, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->szResourceListUser:[Ljava/lang/String;

    #@24
    .line 90
    const/4 v0, 0x0

    #@25
    .local v0, i:I
    :goto_25
    iget v1, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentSize:I

    #@27
    if-ge v0, v1, :cond_31

    #@29
    .line 91
    iget-object v1, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->szResourceListUser:[Ljava/lang/String;

    #@2b
    const/4 v2, 0x0

    #@2c
    aput-object v2, v1, v0

    #@2e
    .line 90
    add-int/lit8 v0, v0, 0x1

    #@30
    goto :goto_25

    #@31
    .line 93
    :cond_31
    const/4 v1, 0x0

    #@32
    iput v1, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentIndex:I

    #@34
    .line 94
    return-void
.end method

.method public SetRemainResourceListUser(I)V
    .registers 4
    .parameter "_nRemainUser"

    #@0
    .prologue
    .line 99
    iput p1, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nRemainUser:I

    #@2
    .line 100
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "[IP]nRemainUser["

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "]"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1e
    .line 101
    return-void
.end method

.method public SetResourceListUser(Ljava/lang/String;)V
    .registers 4
    .parameter "_szResourceListUser"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->szResourceListUser:[Ljava/lang/String;

    #@2
    iget v1, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentIndex:I

    #@4
    aput-object p1, v0, v1

    #@6
    .line 74
    iget v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentIndex:I

    #@8
    add-int/lit8 v0, v0, 0x1

    #@a
    iput v0, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentIndex:I

    #@c
    .line 75
    return-void
.end method

.method public SetTotalResourceListUserSize(I)V
    .registers 4
    .parameter "_nTotalSize"

    #@0
    .prologue
    .line 80
    iput p1, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nTotalSize:I

    #@2
    .line 81
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "[IP]nTotalSize["

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    const-string v1, "]"

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@1e
    .line 82
    return-void
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 59
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public readFromParcel(Landroid/os/Parcel;)V
    .registers 2
    .parameter "in"

    #@0
    .prologue
    .line 64
    return-void
.end method

.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 106
    new-instance v3, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v4, "ResourceListUserInfo : nTotalSize[ "

    #@7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v3

    #@b
    iget v4, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nTotalSize:I

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, "], nRemainUser["

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    iget v4, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nRemainUser:I

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, "] , nCurrentIndex[ "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    iget v4, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentIndex:I

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, "]"

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    .line 108
    .local v1, szPrint:Ljava/lang/String;
    const/4 v0, 0x0

    #@34
    .local v0, i:I
    :goto_34
    iget v3, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->nCurrentIndex:I

    #@36
    if-ge v0, v3, :cond_73

    #@38
    .line 109
    new-instance v3, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v4, "Index["

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    const-string v4, "], User["

    #@49
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    iget-object v4, p0, Lcom/lge/ims/service/ip/ResourceListUserInfo;->szResourceListUser:[Ljava/lang/String;

    #@4f
    aget-object v4, v4, v0

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    const-string v4, "]"

    #@57
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    .line 110
    .local v2, szTemp:Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v1

    #@70
    .line 108
    add-int/lit8 v0, v0, 0x1

    #@72
    goto :goto_34

    #@73
    .line 112
    .end local v2           #szTemp:Ljava/lang/String;
    :cond_73
    return-object v1
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 3
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 62
    return-void
.end method
