.class public abstract Lcom/lge/ims/service/uc/IUCSessionListener$Stub;
.super Landroid/os/Binder;
.source "IUCSessionListener.java"

# interfaces
.implements Lcom/lge/ims/service/uc/IUCSessionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUCSessionListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.lge.ims.service.uc.IUCSessionListener"

.field static final TRANSACTION_dropConfFailed:I = 0x13

.field static final TRANSACTION_dropedConf:I = 0x12

.field static final TRANSACTION_expandConfFailed:I = 0xc

.field static final TRANSACTION_expandedConf:I = 0xb

.field static final TRANSACTION_expandedConfBy:I = 0xd

.field static final TRANSACTION_held:I = 0x4

.field static final TRANSACTION_heldBy:I = 0x6

.field static final TRANSACTION_holdFailed:I = 0x5

.field static final TRANSACTION_incomingUpdate:I = 0x15

.field static final TRANSACTION_joinConfFailed:I = 0x11

.field static final TRANSACTION_joinedConf:I = 0x10

.field static final TRANSACTION_mergeFailed:I = 0xf

.field static final TRANSACTION_merged:I = 0xe

.field static final TRANSACTION_notifyConfInfo:I = 0x14

.field static final TRANSACTION_notifyInfo:I = 0x19

.field static final TRANSACTION_progressing:I = 0x3

.field static final TRANSACTION_resumeFailed:I = 0x8

.field static final TRANSACTION_resumed:I = 0x7

.field static final TRANSACTION_resumedBy:I = 0x9

.field static final TRANSACTION_startFailed:I = 0x2

.field static final TRANSACTION_started:I = 0x1

.field static final TRANSACTION_terminated:I = 0xa

.field static final TRANSACTION_updateFailed:I = 0x17

.field static final TRANSACTION_updated:I = 0x16

.field static final TRANSACTION_updatedBy:I = 0x18


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    #@3
    .line 20
    const-string v0, "com.lge.ims.service.uc.IUCSessionListener"

    #@5
    invoke-virtual {p0, p0, v0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    #@8
    .line 21
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/lge/ims/service/uc/IUCSessionListener;
    .registers 3
    .parameter "obj"

    #@0
    .prologue
    .line 28
    if-nez p0, :cond_4

    #@2
    .line 29
    const/4 v0, 0x0

    #@3
    .line 35
    :goto_3
    return-object v0

    #@4
    .line 31
    :cond_4
    const-string v1, "com.lge.ims.service.uc.IUCSessionListener"

    #@6
    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    #@9
    move-result-object v0

    #@a
    .line 32
    .local v0, iin:Landroid/os/IInterface;
    if-eqz v0, :cond_13

    #@c
    instance-of v1, v0, Lcom/lge/ims/service/uc/IUCSessionListener;

    #@e
    if-eqz v1, :cond_13

    #@10
    .line 33
    check-cast v0, Lcom/lge/ims/service/uc/IUCSessionListener;

    #@12
    goto :goto_3

    #@13
    .line 35
    :cond_13
    new-instance v0, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;

    #@15
    .end local v0           #iin:Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    #@18
    goto :goto_3
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 1

    #@0
    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .registers 10
    .parameter "code"
    .parameter "data"
    .parameter "reply"
    .parameter "flags"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 43
    sparse-switch p1, :sswitch_data_30a

    #@4
    .line 423
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@7
    move-result v3

    #@8
    :goto_8
    return v3

    #@9
    .line 47
    :sswitch_9
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@e
    goto :goto_8

    #@f
    .line 52
    :sswitch_f
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@11
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@14
    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_37

    #@1a
    .line 55
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1c
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1f
    move-result-object v0

    #@20
    check-cast v0, Lcom/lge/ims/service/uc/SessInfo;

    #@22
    .line 61
    .local v0, _arg0:Lcom/lge/ims/service/uc/SessInfo;
    :goto_22
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_39

    #@28
    .line 62
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2a
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2d
    move-result-object v1

    #@2e
    check-cast v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@30
    .line 67
    .local v1, _arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_30
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->started(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@33
    .line 68
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@36
    goto :goto_8

    #@37
    .line 58
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_37
    const/4 v0, 0x0

    #@38
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_22

    #@39
    .line 65
    :cond_39
    const/4 v1, 0x0

    #@3a
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_30

    #@3b
    .line 73
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_3b
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@3d
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@40
    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@43
    move-result v0

    #@44
    .line 77
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@47
    move-result v1

    #@48
    .line 78
    .local v1, _arg1:I
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->startFailed(II)V

    #@4b
    .line 79
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@4e
    goto :goto_8

    #@4f
    .line 84
    .end local v0           #_arg0:I
    .end local v1           #_arg1:I
    :sswitch_4f
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@51
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@54
    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@57
    move-result v4

    #@58
    if-eqz v4, :cond_69

    #@5a
    .line 87
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@5c
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@5f
    move-result-object v0

    #@60
    check-cast v0, Lcom/lge/ims/service/uc/MediaInfo;

    #@62
    .line 92
    .local v0, _arg0:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_62
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->progressing(Lcom/lge/ims/service/uc/MediaInfo;)V

    #@65
    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@68
    goto :goto_8

    #@69
    .line 90
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_69
    const/4 v0, 0x0

    #@6a
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_62

    #@6b
    .line 98
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_6b
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@6d
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@70
    .line 100
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@73
    move-result v4

    #@74
    if-eqz v4, :cond_94

    #@76
    .line 101
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@78
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@7b
    move-result-object v0

    #@7c
    check-cast v0, Lcom/lge/ims/service/uc/SessInfo;

    #@7e
    .line 107
    .local v0, _arg0:Lcom/lge/ims/service/uc/SessInfo;
    :goto_7e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@81
    move-result v4

    #@82
    if-eqz v4, :cond_96

    #@84
    .line 108
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@86
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@89
    move-result-object v1

    #@8a
    check-cast v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@8c
    .line 113
    .local v1, _arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_8c
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->held(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@8f
    .line 114
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@92
    goto/16 :goto_8

    #@94
    .line 104
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_94
    const/4 v0, 0x0

    #@95
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_7e

    #@96
    .line 111
    :cond_96
    const/4 v1, 0x0

    #@97
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_8c

    #@98
    .line 119
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_98
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@9a
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@9d
    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@a0
    move-result v0

    #@a1
    .line 122
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->holdFailed(I)V

    #@a4
    .line 123
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@a7
    goto/16 :goto_8

    #@a9
    .line 128
    .end local v0           #_arg0:I
    :sswitch_a9
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@ab
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@ae
    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@b1
    move-result v4

    #@b2
    if-eqz v4, :cond_d2

    #@b4
    .line 131
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@b6
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@b9
    move-result-object v0

    #@ba
    check-cast v0, Lcom/lge/ims/service/uc/SessInfo;

    #@bc
    .line 137
    .local v0, _arg0:Lcom/lge/ims/service/uc/SessInfo;
    :goto_bc
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@bf
    move-result v4

    #@c0
    if-eqz v4, :cond_d4

    #@c2
    .line 138
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@c4
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@c7
    move-result-object v1

    #@c8
    check-cast v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@ca
    .line 143
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_ca
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->heldBy(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@cd
    .line 144
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@d0
    goto/16 :goto_8

    #@d2
    .line 134
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_d2
    const/4 v0, 0x0

    #@d3
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_bc

    #@d4
    .line 141
    :cond_d4
    const/4 v1, 0x0

    #@d5
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_ca

    #@d6
    .line 149
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_d6
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@d8
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@db
    .line 151
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@de
    move-result v4

    #@df
    if-eqz v4, :cond_ff

    #@e1
    .line 152
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@e3
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@e6
    move-result-object v0

    #@e7
    check-cast v0, Lcom/lge/ims/service/uc/SessInfo;

    #@e9
    .line 158
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    :goto_e9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@ec
    move-result v4

    #@ed
    if-eqz v4, :cond_101

    #@ef
    .line 159
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@f1
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@f4
    move-result-object v1

    #@f5
    check-cast v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@f7
    .line 164
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_f7
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->resumed(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@fa
    .line 165
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@fd
    goto/16 :goto_8

    #@ff
    .line 155
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_ff
    const/4 v0, 0x0

    #@100
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_e9

    #@101
    .line 162
    :cond_101
    const/4 v1, 0x0

    #@102
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_f7

    #@103
    .line 170
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_103
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@105
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@108
    .line 172
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@10b
    move-result v0

    #@10c
    .line 173
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->resumeFailed(I)V

    #@10f
    .line 174
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@112
    goto/16 :goto_8

    #@114
    .line 179
    .end local v0           #_arg0:I
    :sswitch_114
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@116
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@119
    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@11c
    move-result v4

    #@11d
    if-eqz v4, :cond_13d

    #@11f
    .line 182
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@121
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@124
    move-result-object v0

    #@125
    check-cast v0, Lcom/lge/ims/service/uc/SessInfo;

    #@127
    .line 188
    .local v0, _arg0:Lcom/lge/ims/service/uc/SessInfo;
    :goto_127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@12a
    move-result v4

    #@12b
    if-eqz v4, :cond_13f

    #@12d
    .line 189
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@12f
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@132
    move-result-object v1

    #@133
    check-cast v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@135
    .line 194
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_135
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->resumedBy(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@138
    .line 195
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@13b
    goto/16 :goto_8

    #@13d
    .line 185
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_13d
    const/4 v0, 0x0

    #@13e
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_127

    #@13f
    .line 192
    :cond_13f
    const/4 v1, 0x0

    #@140
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_135

    #@141
    .line 200
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_141
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@143
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@146
    .line 202
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@149
    move-result v0

    #@14a
    .line 203
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->terminated(I)V

    #@14d
    .line 204
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@150
    goto/16 :goto_8

    #@152
    .line 209
    .end local v0           #_arg0:I
    :sswitch_152
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@154
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@157
    .line 211
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@15a
    move-result v0

    #@15b
    .line 213
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@15e
    move-result v4

    #@15f
    if-eqz v4, :cond_17f

    #@161
    .line 214
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@163
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@166
    move-result-object v1

    #@167
    check-cast v1, Lcom/lge/ims/service/uc/SessInfo;

    #@169
    .line 220
    .local v1, _arg1:Lcom/lge/ims/service/uc/SessInfo;
    :goto_169
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@16c
    move-result v4

    #@16d
    if-eqz v4, :cond_181

    #@16f
    .line 221
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@171
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@174
    move-result-object v2

    #@175
    check-cast v2, Lcom/lge/ims/service/uc/MediaInfo;

    #@177
    .line 226
    .local v2, _arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_177
    invoke-virtual {p0, v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->expandedConf(ILcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@17a
    .line 227
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@17d
    goto/16 :goto_8

    #@17f
    .line 217
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    .end local v2           #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_17f
    const/4 v1, 0x0

    #@180
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_169

    #@181
    .line 224
    :cond_181
    const/4 v2, 0x0

    #@182
    .restart local v2       #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_177

    #@183
    .line 232
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    .end local v2           #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_183
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@185
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@188
    .line 234
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@18b
    move-result v0

    #@18c
    .line 235
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->expandConfFailed(I)V

    #@18f
    .line 236
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@192
    goto/16 :goto_8

    #@194
    .line 241
    .end local v0           #_arg0:I
    :sswitch_194
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@196
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@199
    .line 243
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@19c
    move-result v0

    #@19d
    .line 245
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1a0
    move-result v4

    #@1a1
    if-eqz v4, :cond_1c1

    #@1a3
    .line 246
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1a5
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1a8
    move-result-object v1

    #@1a9
    check-cast v1, Lcom/lge/ims/service/uc/SessInfo;

    #@1ab
    .line 252
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    :goto_1ab
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1ae
    move-result v4

    #@1af
    if-eqz v4, :cond_1c3

    #@1b1
    .line 253
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1b3
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1b6
    move-result-object v2

    #@1b7
    check-cast v2, Lcom/lge/ims/service/uc/MediaInfo;

    #@1b9
    .line 258
    .restart local v2       #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_1b9
    invoke-virtual {p0, v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->expandedConfBy(ILcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@1bc
    .line 259
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1bf
    goto/16 :goto_8

    #@1c1
    .line 249
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    .end local v2           #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_1c1
    const/4 v1, 0x0

    #@1c2
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_1ab

    #@1c3
    .line 256
    :cond_1c3
    const/4 v2, 0x0

    #@1c4
    .restart local v2       #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_1b9

    #@1c5
    .line 264
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    .end local v2           #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_1c5
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@1c7
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1ca
    .line 266
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1cd
    move-result v0

    #@1ce
    .line 268
    .restart local v0       #_arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1d1
    move-result v4

    #@1d2
    if-eqz v4, :cond_1f2

    #@1d4
    .line 269
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1d6
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1d9
    move-result-object v1

    #@1da
    check-cast v1, Lcom/lge/ims/service/uc/SessInfo;

    #@1dc
    .line 275
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    :goto_1dc
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1df
    move-result v4

    #@1e0
    if-eqz v4, :cond_1f4

    #@1e2
    .line 276
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@1e4
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@1e7
    move-result-object v2

    #@1e8
    check-cast v2, Lcom/lge/ims/service/uc/MediaInfo;

    #@1ea
    .line 281
    .restart local v2       #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_1ea
    invoke-virtual {p0, v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->merged(ILcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@1ed
    .line 282
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@1f0
    goto/16 :goto_8

    #@1f2
    .line 272
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    .end local v2           #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_1f2
    const/4 v1, 0x0

    #@1f3
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_1dc

    #@1f4
    .line 279
    :cond_1f4
    const/4 v2, 0x0

    #@1f5
    .restart local v2       #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_1ea

    #@1f6
    .line 287
    .end local v0           #_arg0:I
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/SessInfo;
    .end local v2           #_arg2:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_1f6
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@1f8
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@1fb
    .line 289
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@1fe
    move-result v0

    #@1ff
    .line 290
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->mergeFailed(I)V

    #@202
    .line 291
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@205
    goto/16 :goto_8

    #@207
    .line 296
    .end local v0           #_arg0:I
    :sswitch_207
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@209
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@20c
    .line 297
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->joinedConf()V

    #@20f
    .line 298
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@212
    goto/16 :goto_8

    #@214
    .line 303
    :sswitch_214
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@216
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@219
    .line 305
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@21c
    move-result v0

    #@21d
    .line 306
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->joinConfFailed(I)V

    #@220
    .line 307
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@223
    goto/16 :goto_8

    #@225
    .line 312
    .end local v0           #_arg0:I
    :sswitch_225
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@227
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@22a
    .line 313
    invoke-virtual {p0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->dropedConf()V

    #@22d
    .line 314
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@230
    goto/16 :goto_8

    #@232
    .line 319
    :sswitch_232
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@234
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@237
    .line 321
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@23a
    move-result v0

    #@23b
    .line 322
    .restart local v0       #_arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->dropConfFailed(I)V

    #@23e
    .line 323
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@241
    goto/16 :goto_8

    #@243
    .line 328
    .end local v0           #_arg0:I
    :sswitch_243
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@245
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@248
    .line 330
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    #@24b
    move-result-object v0

    #@24c
    .line 332
    .local v0, _arg0:[I
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    #@24f
    move-result-object v1

    #@250
    .line 333
    .local v1, _arg1:[Ljava/lang/String;
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->notifyConfInfo([I[Ljava/lang/String;)V

    #@253
    .line 334
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@256
    goto/16 :goto_8

    #@258
    .line 339
    .end local v0           #_arg0:[I
    .end local v1           #_arg1:[Ljava/lang/String;
    :sswitch_258
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@25a
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@25d
    .line 341
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@260
    move-result v4

    #@261
    if-eqz v4, :cond_281

    #@263
    .line 342
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@265
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@268
    move-result-object v0

    #@269
    check-cast v0, Lcom/lge/ims/service/uc/SessInfo;

    #@26b
    .line 348
    .local v0, _arg0:Lcom/lge/ims/service/uc/SessInfo;
    :goto_26b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@26e
    move-result v4

    #@26f
    if-eqz v4, :cond_283

    #@271
    .line 349
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@273
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@276
    move-result-object v1

    #@277
    check-cast v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@279
    .line 354
    .local v1, _arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_279
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->incomingUpdate(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@27c
    .line 355
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@27f
    goto/16 :goto_8

    #@281
    .line 345
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_281
    const/4 v0, 0x0

    #@282
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_26b

    #@283
    .line 352
    :cond_283
    const/4 v1, 0x0

    #@284
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_279

    #@285
    .line 360
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_285
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@287
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@28a
    .line 362
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@28d
    move-result v4

    #@28e
    if-eqz v4, :cond_2ae

    #@290
    .line 363
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@292
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@295
    move-result-object v0

    #@296
    check-cast v0, Lcom/lge/ims/service/uc/SessInfo;

    #@298
    .line 369
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    :goto_298
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@29b
    move-result v4

    #@29c
    if-eqz v4, :cond_2b0

    #@29e
    .line 370
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2a0
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2a3
    move-result-object v1

    #@2a4
    check-cast v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@2a6
    .line 375
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_2a6
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->updated(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@2a9
    .line 376
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ac
    goto/16 :goto_8

    #@2ae
    .line 366
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_2ae
    const/4 v0, 0x0

    #@2af
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_298

    #@2b0
    .line 373
    :cond_2b0
    const/4 v1, 0x0

    #@2b1
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_2a6

    #@2b2
    .line 381
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_2b2
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@2b4
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2b7
    .line 383
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2ba
    move-result v0

    #@2bb
    .line 384
    .local v0, _arg0:I
    invoke-virtual {p0, v0}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->updateFailed(I)V

    #@2be
    .line 385
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2c1
    goto/16 :goto_8

    #@2c3
    .line 390
    .end local v0           #_arg0:I
    :sswitch_2c3
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@2c5
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2c8
    .line 392
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2cb
    move-result v4

    #@2cc
    if-eqz v4, :cond_2ec

    #@2ce
    .line 393
    sget-object v4, Lcom/lge/ims/service/uc/SessInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2d0
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2d3
    move-result-object v0

    #@2d4
    check-cast v0, Lcom/lge/ims/service/uc/SessInfo;

    #@2d6
    .line 399
    .local v0, _arg0:Lcom/lge/ims/service/uc/SessInfo;
    :goto_2d6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2d9
    move-result v4

    #@2da
    if-eqz v4, :cond_2ee

    #@2dc
    .line 400
    sget-object v4, Lcom/lge/ims/service/uc/MediaInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    #@2de
    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    #@2e1
    move-result-object v1

    #@2e2
    check-cast v1, Lcom/lge/ims/service/uc/MediaInfo;

    #@2e4
    .line 405
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :goto_2e4
    invoke-virtual {p0, v0, v1}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->updatedBy(Lcom/lge/ims/service/uc/SessInfo;Lcom/lge/ims/service/uc/MediaInfo;)V

    #@2e7
    .line 406
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@2ea
    goto/16 :goto_8

    #@2ec
    .line 396
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :cond_2ec
    const/4 v0, 0x0

    #@2ed
    .restart local v0       #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    goto :goto_2d6

    #@2ee
    .line 403
    :cond_2ee
    const/4 v1, 0x0

    #@2ef
    .restart local v1       #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    goto :goto_2e4

    #@2f0
    .line 411
    .end local v0           #_arg0:Lcom/lge/ims/service/uc/SessInfo;
    .end local v1           #_arg1:Lcom/lge/ims/service/uc/MediaInfo;
    :sswitch_2f0
    const-string v4, "com.lge.ims.service.uc.IUCSessionListener"

    #@2f2
    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    #@2f5
    .line 413
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2f8
    move-result v0

    #@2f9
    .line 415
    .local v0, _arg0:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    #@2fc
    move-result v1

    #@2fd
    .line 417
    .local v1, _arg1:I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@300
    move-result-object v2

    #@301
    .line 418
    .local v2, _arg2:Ljava/lang/String;
    invoke-virtual {p0, v0, v1, v2}, Lcom/lge/ims/service/uc/IUCSessionListener$Stub;->notifyInfo(IILjava/lang/String;)V

    #@304
    .line 419
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    #@307
    goto/16 :goto_8

    #@309
    .line 43
    nop

    #@30a
    :sswitch_data_30a
    .sparse-switch
        0x1 -> :sswitch_f
        0x2 -> :sswitch_3b
        0x3 -> :sswitch_4f
        0x4 -> :sswitch_6b
        0x5 -> :sswitch_98
        0x6 -> :sswitch_a9
        0x7 -> :sswitch_d6
        0x8 -> :sswitch_103
        0x9 -> :sswitch_114
        0xa -> :sswitch_141
        0xb -> :sswitch_152
        0xc -> :sswitch_183
        0xd -> :sswitch_194
        0xe -> :sswitch_1c5
        0xf -> :sswitch_1f6
        0x10 -> :sswitch_207
        0x11 -> :sswitch_214
        0x12 -> :sswitch_225
        0x13 -> :sswitch_232
        0x14 -> :sswitch_243
        0x15 -> :sswitch_258
        0x16 -> :sswitch_285
        0x17 -> :sswitch_2b2
        0x18 -> :sswitch_2c3
        0x19 -> :sswitch_2f0
        0x5f4e5446 -> :sswitch_9
    .end sparse-switch
.end method
