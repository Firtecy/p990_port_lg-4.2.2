.class Lcom/lge/ims/service/uc/IUCServiceListener$Stub$Proxy;
.super Ljava/lang/Object;
.source "IUCServiceListener.java"

# interfaces
.implements Lcom/lge/ims/service/uc/IUCServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/uc/IUCServiceListener$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 79
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 80
    iput-object p1, p0, Lcom/lge/ims/service/uc/IUCServiceListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 81
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 84
    iget-object v0, p0, Lcom/lge/ims/service/uc/IUCServiceListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public changedEStatus(II)V
    .registers 8
    .parameter "status"
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 108
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 109
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 111
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCServiceListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 112
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 113
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 114
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCServiceListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v3, 0x2

    #@16
    const/4 v4, 0x0

    #@17
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 115
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_24

    #@1d
    .line 118
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 119
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 121
    return-void

    #@24
    .line 118
    :catchall_24
    move-exception v2

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 119
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v2
.end method

.method public changedStatus(II)V
    .registers 8
    .parameter "status"
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 92
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 93
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 95
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.uc.IUCServiceListener"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 96
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 97
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 98
    iget-object v2, p0, Lcom/lge/ims/service/uc/IUCServiceListener$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@15
    const/4 v3, 0x1

    #@16
    const/4 v4, 0x0

    #@17
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1a
    .line 99
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_1d
    .catchall {:try_start_8 .. :try_end_1d} :catchall_24

    #@1d
    .line 102
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 103
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 105
    return-void

    #@24
    .line 102
    :catchall_24
    move-exception v2

    #@25
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 103
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@2b
    throw v2
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 88
    const-string v0, "com.lge.ims.service.uc.IUCServiceListener"

    #@2
    return-object v0
.end method
