.class public Lcom/lge/ims/service/cd/CapabilityService;
.super Ljava/lang/Object;
.source "CapabilityService.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/cd/CapabilityService$1;,
        Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;,
        Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;,
        Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;,
        Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;
    }
.end annotation


# static fields
.field private static final EVENT_CALL_STATE_CHANGED:I = 0x64

.field private static final EVENT_CAPABILITY_QUERY_COMPLETED:I = 0x32

.field private static final EVENT_EXTERNAL_STORAGE_STATE_CHANGED:I = 0x66

.field private static final EVENT_EXTERNAL_STORAGE_STATE_INIT:I = 0x65

.field private static final EVENT_UPDATE_CAPABILITIES:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "CapService"

.field private static final TEST_MODE:Z

.field private static mCapService:Lcom/lge/ims/service/cd/CapabilityService;


# instance fields
.field private mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

.field private mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

.field private mCapTrackingNumbers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;",
            ">;"
        }
    .end annotation
.end field

.field private mCheckExternalStorageAvailability:Z

.field private mContext:Landroid/content/Context;

.field private mDiscoveryHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;

.field private mDiscoveryThread:Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;

.field private mIsExternalStorageAvailable:Z

.field private mIsFirstExternalStorageStateChangeNotified:Z

.field private mNativeObject:I

.field private mServiceHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 34
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/ims/service/cd/CapabilityService;->mCapService:Lcom/lge/ims/service/cd/CapabilityService;

    #@3
    return-void
.end method

.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x0

    #@2
    .line 51
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 36
    iput-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mContext:Landroid/content/Context;

    #@7
    .line 37
    iput v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@9
    .line 38
    new-instance v0, Ljava/util/ArrayList;

    #@b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@e
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@10
    .line 40
    iput-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@12
    .line 41
    iput-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@14
    .line 42
    iput-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mServiceHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;

    #@16
    .line 43
    iput-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mDiscoveryHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;

    #@18
    .line 44
    iput-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mDiscoveryThread:Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;

    #@1a
    .line 45
    iput-boolean v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCheckExternalStorageAvailability:Z

    #@1c
    .line 46
    iput-boolean v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mIsExternalStorageAvailable:Z

    #@1e
    .line 47
    iput-boolean v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mIsFirstExternalStorageStateChangeNotified:Z

    #@20
    .line 52
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@22
    invoke-direct {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier;-><init>()V

    #@25
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@27
    .line 53
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;

    #@29
    invoke-direct {v0, p0, v1}, Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;-><init>(Lcom/lge/ims/service/cd/CapabilityService;Lcom/lge/ims/service/cd/CapabilityService$1;)V

    #@2c
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mServiceHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;

    #@2e
    .line 54
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;

    #@30
    invoke-direct {v0, p0}, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;-><init>(Lcom/lge/ims/service/cd/CapabilityService;)V

    #@33
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mDiscoveryThread:Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;

    #@35
    .line 56
    invoke-static {}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->getInstance()Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@38
    move-result-object v0

    #@39
    iput-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@3b
    .line 58
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@3d
    if-eqz v0, :cond_46

    #@3f
    .line 59
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@41
    iget-boolean v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCheckExternalStorageAvailability:Z

    #@43
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->requireExternalStorageAvailability(Z)V

    #@46
    .line 61
    :cond_46
    return-void
.end method

.method static synthetic access$100(Lcom/lge/ims/service/cd/CapabilityService;ILjava/lang/String;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/lge/ims/service/cd/CapabilityService;->updateCallState(ILjava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/lge/ims/service/cd/CapabilityService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mIsExternalStorageAvailable:Z

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/lge/ims/service/cd/CapabilityService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mIsExternalStorageAvailable:Z

    #@2
    return p1
.end method

.method static synthetic access$300(Lcom/lge/ims/service/cd/CapabilityService;III)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Lcom/lge/ims/service/cd/CapabilityService;->updateServiceState(III)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Lcom/lge/ims/service/cd/CapabilityService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-boolean v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mIsFirstExternalStorageStateChangeNotified:Z

    #@2
    return v0
.end method

.method static synthetic access$402(Lcom/lge/ims/service/cd/CapabilityService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput-boolean p1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mIsFirstExternalStorageStateChangeNotified:Z

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/lge/ims/service/cd/CapabilityService;)Lcom/lge/ims/service/cd/CapabilityStateTracker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/lge/ims/service/cd/CapabilityService;)Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mDiscoveryHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Lcom/lge/ims/service/cd/CapabilityService;Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;)Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mDiscoveryHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryHandler;

    #@2
    return-object p1
.end method

.method static synthetic access$700(Lcom/lge/ims/service/cd/CapabilityService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 24
    invoke-direct {p0}, Lcom/lge/ims/service/cd/CapabilityService;->cloneCapabilityTrackingNumbers()Ljava/util/ArrayList;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$800(Lcom/lge/ims/service/cd/CapabilityService;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/lge/ims/service/cd/CapabilityService;->updateCapabilities(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private cloneCapabilityTrackingNumbers()Ljava/util/ArrayList;
    .registers 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 333
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 335
    .local v2, trackingNumbers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-nez v2, :cond_10

    #@7
    .line 336
    const-string v3, "CapService"

    #@9
    const-string v4, "Instantiating ArrayList<String> failed"

    #@b
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@e
    .line 337
    const/4 v2, 0x0

    #@f
    .line 352
    .end local v2           #trackingNumbers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_f
    return-object v2

    #@10
    .line 340
    .restart local v2       #trackingNumbers:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_10
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@12
    monitor-enter v4

    #@13
    .line 341
    const/4 v0, 0x0

    #@14
    .local v0, i:I
    :goto_14
    :try_start_14
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@16
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@19
    move-result v3

    #@1a
    if-ge v0, v3, :cond_39

    #@1c
    .line 342
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@21
    move-result-object v1

    #@22
    check-cast v1, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;

    #@24
    .line 344
    .local v1, phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    if-nez v1, :cond_29

    #@26
    .line 341
    :goto_26
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_14

    #@29
    .line 348
    :cond_29
    new-instance v3, Ljava/lang/String;

    #@2b
    invoke-virtual {v1}, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;->getNumber()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-direct {v3, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@32
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@35
    goto :goto_26

    #@36
    .line 350
    .end local v1           #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    :catchall_36
    move-exception v3

    #@37
    monitor-exit v4
    :try_end_38
    .catchall {:try_start_14 .. :try_end_38} :catchall_36

    #@38
    throw v3

    #@39
    :cond_39
    :try_start_39
    monitor-exit v4
    :try_end_3a
    .catchall {:try_start_39 .. :try_end_3a} :catchall_36

    #@3a
    goto :goto_f
.end method

.method public static getInstance()Lcom/lge/ims/service/cd/CapabilityService;
    .registers 2

    #@0
    .prologue
    .line 64
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityService;->mCapService:Lcom/lge/ims/service/cd/CapabilityService;

    #@2
    if-nez v0, :cond_18

    #@4
    .line 65
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityService;

    #@6
    invoke-direct {v0}, Lcom/lge/ims/service/cd/CapabilityService;-><init>()V

    #@9
    sput-object v0, Lcom/lge/ims/service/cd/CapabilityService;->mCapService:Lcom/lge/ims/service/cd/CapabilityService;

    #@b
    .line 67
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityService;->mCapService:Lcom/lge/ims/service/cd/CapabilityService;

    #@d
    if-nez v0, :cond_18

    #@f
    .line 68
    const-string v0, "CapService"

    #@11
    const-string v1, "Instantiating a Capability Service failed"

    #@13
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 69
    const/4 v0, 0x0

    #@17
    .line 77
    :goto_17
    return-object v0

    #@18
    .line 73
    :cond_18
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityService;->mCapService:Lcom/lge/ims/service/cd/CapabilityService;

    #@1a
    invoke-direct {v0}, Lcom/lge/ims/service/cd/CapabilityService;->isConnected()Z

    #@1d
    move-result v0

    #@1e
    if-nez v0, :cond_25

    #@20
    .line 74
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityService;->mCapService:Lcom/lge/ims/service/cd/CapabilityService;

    #@22
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityService;->create()Z

    #@25
    .line 77
    :cond_25
    sget-object v0, Lcom/lge/ims/service/cd/CapabilityService;->mCapService:Lcom/lge/ims/service/cd/CapabilityService;

    #@27
    goto :goto_17
.end method

.method private getNativeService(I)I
    .registers 8
    .parameter "interval"

    #@0
    .prologue
    .line 356
    const/4 v0, 0x0

    #@1
    .line 357
    .local v0, nativeService:I
    const/4 v1, 0x0

    #@2
    .line 358
    .local v1, retryCount:I
    if-eqz p1, :cond_e

    #@4
    move v2, p1

    #@5
    .line 361
    .local v2, sleepInterval:I
    :cond_5
    :goto_5
    const/16 v3, 0x1f

    #@7
    invoke-static {v3}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@a
    move-result v0

    #@b
    .line 363
    if-eqz v0, :cond_11

    #@d
    .line 374
    :goto_d
    return v0

    #@e
    .line 358
    .end local v2           #sleepInterval:I
    :cond_e
    const/16 v2, 0x5dc

    #@10
    goto :goto_5

    #@11
    .line 367
    .restart local v2       #sleepInterval:I
    :cond_11
    add-int/lit8 v1, v1, 0x1

    #@13
    .line 368
    const-string v3, "CapService"

    #@15
    new-instance v4, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v5, "Waiting a native Capability Service ... ( "

    #@1c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    const-string v5, " )"

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v4

    #@2e
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 371
    int-to-long v3, v2

    #@32
    invoke-static {v3, v4}, Landroid/os/SystemClock;->sleep(J)V

    #@35
    .line 372
    const/4 v3, 0x5

    #@36
    if-lt v1, v3, :cond_5

    #@38
    goto :goto_d
.end method

.method private isConnected()Z
    .registers 2

    #@0
    .prologue
    .line 378
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@2
    if-lez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private updateCallState(ILjava/lang/String;)Z
    .registers 7
    .parameter "callType"
    .parameter "number"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 382
    const/16 v3, 0x27da

    #@4
    invoke-static {v3}, Lcom/lge/ims/JNICore;->obtainParcel(I)Landroid/os/Parcel;

    #@7
    move-result-object v0

    #@8
    .line 384
    .local v0, parcel:Landroid/os/Parcel;
    if-nez v0, :cond_b

    #@a
    .line 406
    :goto_a
    return v1

    #@b
    .line 388
    :cond_b
    if-ne p1, v2, :cond_24

    #@d
    .line 390
    const/4 v3, 0x4

    #@e
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@11
    .line 399
    :goto_11
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@14
    .line 401
    iget v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@16
    invoke-static {v3, v0}, Lcom/lge/ims/JNICore;->sendEvent(ILandroid/os/Parcel;)I

    #@19
    move-result v3

    #@1a
    if-gtz v3, :cond_31

    #@1c
    .line 402
    const-string v2, "CapService"

    #@1e
    const-string v3, "Sending a UPDATE_CALL_STATE event failed"

    #@20
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    goto :goto_a

    #@24
    .line 391
    :cond_24
    const/4 v3, 0x2

    #@25
    if-ne p1, v3, :cond_2d

    #@27
    .line 393
    const/16 v3, 0x8

    #@29
    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    goto :goto_11

    #@2d
    .line 395
    :cond_2d
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    goto :goto_a

    #@31
    :cond_31
    move v1, v2

    #@32
    .line 406
    goto :goto_a
.end method

.method private updateCapabilities(Ljava/lang/String;)Z
    .registers 7
    .parameter "target"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 410
    invoke-virtual {p0}, Lcom/lge/ims/service/cd/CapabilityService;->createCapabilityQuery()Lcom/lge/ims/service/cd/CapabilityQuery;

    #@4
    move-result-object v0

    #@5
    .line 412
    .local v0, capQuery:Lcom/lge/ims/service/cd/CapabilityQuery;
    if-nez v0, :cond_20

    #@7
    .line 413
    const-string v2, "CapService"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "Creating a CapabilityQuery failed :: target= "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 422
    :goto_1f
    return v1

    #@20
    .line 417
    :cond_20
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/cd/CapabilityQuery;->send(Ljava/lang/String;)Z

    #@23
    move-result v2

    #@24
    if-nez v2, :cond_3f

    #@26
    .line 418
    const-string v2, "CapService"

    #@28
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v4, "Sending an OPTIONS failed :: target= "

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v3

    #@3b
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3e
    goto :goto_1f

    #@3f
    .line 422
    :cond_3f
    const/4 v1, 0x1

    #@40
    goto :goto_1f
.end method

.method private updateServiceState(III)Z
    .registers 8
    .parameter "capabilityInfo"
    .parameter "blockedServices"
    .parameter "unblockedServices"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 426
    const/16 v2, 0x27db

    #@3
    invoke-static {v2}, Lcom/lge/ims/JNICore;->obtainParcel(I)Landroid/os/Parcel;

    #@6
    move-result-object v0

    #@7
    .line 428
    .local v0, parcel:Landroid/os/Parcel;
    if-nez v0, :cond_a

    #@9
    .line 444
    :goto_9
    return v1

    #@a
    .line 433
    :cond_a
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@d
    .line 435
    invoke-virtual {v0, p2}, Landroid/os/Parcel;->writeInt(I)V

    #@10
    .line 437
    invoke-virtual {v0, p3}, Landroid/os/Parcel;->writeInt(I)V

    #@13
    .line 439
    iget v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@15
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendEvent(ILandroid/os/Parcel;)I

    #@18
    move-result v2

    #@19
    if-gtz v2, :cond_23

    #@1b
    .line 440
    const-string v2, "CapService"

    #@1d
    const-string v3, "Sending a UPDATE_SERVICE_STATE event failed"

    #@1f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    goto :goto_9

    #@23
    .line 444
    :cond_23
    const/4 v1, 0x1

    #@24
    goto :goto_9
.end method


# virtual methods
.method public addBroadcastListener(Lcom/lge/ims/service/cd/CapabilitiesListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 86
    :goto_4
    return-void

    #@5
    .line 85
    :cond_5
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@7
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/cd/CapabilityNotifier;->addBroadcastListener(Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@a
    goto :goto_4
.end method

.method public addDedicatedListener(Ljava/lang/String;Lcom/lge/ims/service/cd/CapabilitiesListener;)V
    .registers 10
    .parameter "number"
    .parameter "listener"

    #@0
    .prologue
    .line 89
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@2
    if-nez v3, :cond_5

    #@4
    .line 123
    :goto_4
    return-void

    #@5
    .line 93
    :cond_5
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@7
    invoke-virtual {v3, p1, p2}, Lcom/lge/ims/service/cd/CapabilityNotifier;->addDedicatedListener(Ljava/lang/String;Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@a
    .line 95
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@c
    monitor-enter v4

    #@d
    .line 96
    const/4 v1, 0x0

    #@e
    .line 98
    .local v1, phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    const/4 v0, 0x0

    #@f
    .local v0, i:I
    move-object v2, v1

    #@10
    .end local v1           #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    .local v2, phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    :goto_10
    :try_start_10
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@15
    move-result v3

    #@16
    if-ge v0, v3, :cond_6e

    #@18
    .line 99
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1d
    move-result-object v1

    #@1e
    check-cast v1, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    :try_end_20
    .catchall {:try_start_10 .. :try_end_20} :catchall_dc

    #@20
    .line 101
    .end local v2           #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    .restart local v1       #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    if-nez v1, :cond_26

    #@22
    .line 98
    :cond_22
    add-int/lit8 v0, v0, 0x1

    #@24
    move-object v2, v1

    #@25
    .end local v1           #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    .restart local v2       #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    goto :goto_10

    #@26
    .line 105
    .end local v2           #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    .restart local v1       #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    :cond_26
    :try_start_26
    invoke-virtual {v1}, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;->getNumber()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_22

    #@30
    .line 106
    invoke-virtual {v1}, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;->addReference()I

    #@33
    .line 107
    const-string v3, "CapService"

    #@35
    new-instance v5, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v6, "Phone number :: add="

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    const-string v6, ":"

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v1}, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;->getRefCount()I

    #@4d
    move-result v6

    #@4e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    const-string v6, ":"

    #@54
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v5

    #@58
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@5a
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@5d
    move-result v6

    #@5e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@61
    move-result-object v5

    #@62
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v5

    #@66
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@69
    .line 109
    monitor-exit v4

    #@6a
    goto :goto_4

    #@6b
    .line 122
    :catchall_6b
    move-exception v3

    #@6c
    :goto_6c
    monitor-exit v4
    :try_end_6d
    .catchall {:try_start_26 .. :try_end_6d} :catchall_6b

    #@6d
    throw v3

    #@6e
    .line 113
    .end local v1           #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    .restart local v2       #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    :cond_6e
    :try_start_6e
    new-instance v1, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;

    #@70
    invoke-direct {v1, p0, p1}, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;-><init>(Lcom/lge/ims/service/cd/CapabilityService;Ljava/lang/String;)V
    :try_end_73
    .catchall {:try_start_6e .. :try_end_73} :catchall_dc

    #@73
    .line 115
    .end local v2           #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    .restart local v1       #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    if-eqz v1, :cond_b3

    #@75
    .line 116
    :try_start_75
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@77
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7a
    .line 117
    const-string v3, "CapService"

    #@7c
    new-instance v5, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v6, "Phone number :: add="

    #@83
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v5

    #@87
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v5

    #@8b
    const-string v6, ":"

    #@8d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v1}, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;->getRefCount()I

    #@94
    move-result v6

    #@95
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@98
    move-result-object v5

    #@99
    const-string v6, ":"

    #@9b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v5

    #@9f
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@a1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@a4
    move-result v6

    #@a5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v5

    #@a9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ac
    move-result-object v5

    #@ad
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@b0
    .line 122
    :goto_b0
    monitor-exit v4

    #@b1
    goto/16 :goto_4

    #@b3
    .line 120
    :cond_b3
    const-string v3, "CapService"

    #@b5
    new-instance v5, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v6, "Phone number :: add="

    #@bc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v5

    #@c0
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v5

    #@c4
    const-string v6, ":null:"

    #@c6
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v5

    #@ca
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@cc
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@cf
    move-result v6

    #@d0
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v5

    #@d4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v5

    #@d8
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_db
    .catchall {:try_start_75 .. :try_end_db} :catchall_6b

    #@db
    goto :goto_b0

    #@dc
    .line 122
    .end local v1           #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    .restart local v2       #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    :catchall_dc
    move-exception v3

    #@dd
    move-object v1, v2

    #@de
    .end local v2           #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    .restart local v1       #phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    goto :goto_6c
.end method

.method public close()V
    .registers 3

    #@0
    .prologue
    .line 162
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@2
    if-nez v0, :cond_5

    #@4
    .line 182
    :goto_4
    return-void

    #@5
    .line 166
    :cond_5
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 167
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@b
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityNotifier;->clearAllListeners()V

    #@e
    .line 170
    :cond_e
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@10
    if-eqz v0, :cond_25

    #@12
    .line 171
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@14
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mServiceHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;

    #@16
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->unregisterForCallStateChanged(Landroid/os/Handler;)V

    #@19
    .line 172
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@1b
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mServiceHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;

    #@1d
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->unregisterForExternalStorageStateChanged(Landroid/os/Handler;)V

    #@20
    .line 173
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@22
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->stopWatchingExternalStorage()V

    #@25
    .line 177
    :cond_25
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@27
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@2a
    .line 178
    iget v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@2c
    invoke-static {v0}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@2f
    .line 181
    const/4 v0, 0x0

    #@30
    iput v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@32
    goto :goto_4
.end method

.method public create()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    const/4 v1, 0x0

    #@3
    .line 189
    iget v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@5
    if-eqz v2, :cond_28

    #@7
    .line 190
    const-string v1, "CapService"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "Native service is already created ("

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, ")"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@27
    .line 213
    :cond_27
    :goto_27
    return v0

    #@28
    .line 194
    :cond_28
    const/16 v2, 0x3e8

    #@2a
    invoke-direct {p0, v2}, Lcom/lge/ims/service/cd/CapabilityService;->getNativeService(I)I

    #@2d
    move-result v2

    #@2e
    iput v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@30
    .line 196
    iget v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@32
    if-gtz v2, :cond_3f

    #@34
    .line 197
    iput v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@36
    .line 199
    const-string v0, "CapService"

    #@38
    const-string v2, "Getting an interface for Capability Service failed"

    #@3a
    invoke-static {v0, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    move v0, v1

    #@3e
    .line 200
    goto :goto_27

    #@3f
    .line 203
    :cond_3f
    iget v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@41
    invoke-static {v1, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@44
    .line 205
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@46
    if-eqz v1, :cond_27

    #@48
    .line 206
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@4a
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mServiceHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;

    #@4c
    const/16 v3, 0x64

    #@4e
    invoke-virtual {v1, v2, v3, v4}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->registerForCallStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@51
    .line 209
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@53
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mServiceHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;

    #@55
    const/16 v3, 0x66

    #@57
    invoke-virtual {v1, v2, v3, v4}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->registerForExternalStorageStateChanged(Landroid/os/Handler;ILjava/lang/Object;)V

    #@5a
    goto :goto_27
.end method

.method public createCapabilityQuery()Lcom/lge/ims/service/cd/CapabilityQuery;
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 235
    iget v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mNativeObject:I

    #@3
    if-nez v3, :cond_e

    #@5
    .line 236
    const-string v3, "CapService"

    #@7
    const-string v4, "Creating a CapabilityQuery failed"

    #@9
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    move-object v0, v2

    #@d
    .line 270
    :goto_d
    return-object v0

    #@e
    .line 254
    :cond_e
    const/16 v3, 0x20

    #@10
    invoke-static {v3}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@13
    move-result v1

    #@14
    .line 256
    .local v1, nativeObject:I
    if-gtz v1, :cond_1f

    #@16
    .line 257
    const-string v3, "CapService"

    #@18
    const-string v4, "Creating a native object for CapabilityQuery failed"

    #@1a
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    move-object v0, v2

    #@1e
    .line 258
    goto :goto_d

    #@1f
    .line 261
    :cond_1f
    new-instance v0, Lcom/lge/ims/service/cd/CapabilityQuery;

    #@21
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@23
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@25
    invoke-direct {v0, v1, v3, v4}, Lcom/lge/ims/service/cd/CapabilityQuery;-><init>(ILcom/lge/ims/service/cd/CapabilityNotifier;Lcom/lge/ims/service/cd/CapabilityStateTracker;)V

    #@28
    .line 263
    .local v0, capQuery:Lcom/lge/ims/service/cd/CapabilityQuery;
    if-nez v0, :cond_33

    #@2a
    .line 264
    const-string v3, "CapService"

    #@2c
    const-string v4, "Instantiating a CapabilityQuery failed"

    #@2e
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    move-object v0, v2

    #@32
    .line 265
    goto :goto_d

    #@33
    .line 268
    :cond_33
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mServiceHandler:Lcom/lge/ims/service/cd/CapabilityService$CapabilityServiceHandler;

    #@35
    const/16 v4, 0x32

    #@37
    invoke-virtual {v0, v3, v4, v2}, Lcom/lge/ims/service/cd/CapabilityQuery;->registerForCapabilityQueryCompleted(Landroid/os/Handler;ILjava/lang/Object;)V

    #@3a
    goto :goto_d
.end method

.method public createCapabilityQueryImpl()Lcom/lge/ims/service/cd/CapabilityQueryImpl;
    .registers 5

    #@0
    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/lge/ims/service/cd/CapabilityService;->createCapabilityQuery()Lcom/lge/ims/service/cd/CapabilityQuery;

    #@3
    move-result-object v0

    #@4
    .line 277
    .local v0, capQuery:Lcom/lge/ims/service/cd/CapabilityQuery;
    if-nez v0, :cond_f

    #@6
    .line 278
    const-string v2, "CapService"

    #@8
    const-string v3, "Creating a CapabilityQuery failed"

    #@a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 279
    const/4 v1, 0x0

    #@e
    .line 288
    :cond_e
    :goto_e
    return-object v1

    #@f
    .line 282
    :cond_f
    new-instance v1, Lcom/lge/ims/service/cd/CapabilityQueryImpl;

    #@11
    invoke-direct {v1, v0}, Lcom/lge/ims/service/cd/CapabilityQueryImpl;-><init>(Lcom/lge/ims/service/cd/CapabilityQuery;)V

    #@14
    .line 284
    .local v1, capQueryImpl:Lcom/lge/ims/service/cd/CapabilityQueryImpl;
    if-nez v1, :cond_e

    #@16
    .line 285
    const-string v2, "CapService"

    #@18
    const-string v3, "Instantiating a CapabilityQueryImpl failed"

    #@1a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    goto :goto_e
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 7
    .parameter "parcel"

    #@0
    .prologue
    .line 293
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3
    move-result v0

    #@4
    .line 295
    .local v0, event:I
    packed-switch v0, :pswitch_data_a2

    #@7
    .line 327
    const-string v2, "CapService"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "Not handled event ("

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    const-string v4, ")"

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@25
    .line 330
    :cond_25
    :goto_25
    return-void

    #@26
    .line 298
    :pswitch_26
    new-instance v1, Lcom/lge/ims/service/cd/ServiceCapabilities;

    #@28
    invoke-direct {v1, p1}, Lcom/lge/ims/service/cd/ServiceCapabilities;-><init>(Landroid/os/Parcel;)V

    #@2b
    .line 300
    .local v1, sc:Lcom/lge/ims/service/cd/ServiceCapabilities;
    if-nez v1, :cond_35

    #@2d
    .line 301
    const-string v2, "CapService"

    #@2f
    const-string v3, "Instantiating a ServiceCapabilities failed"

    #@31
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@34
    goto :goto_25

    #@35
    .line 306
    :cond_35
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@37
    if-eqz v2, :cond_76

    #@39
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@3b
    invoke-virtual {v2}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->isExternalStorageAvailable()Z

    #@3e
    move-result v2

    #@3f
    if-nez v2, :cond_76

    #@41
    .line 308
    const-string v2, "CapService"

    #@43
    new-instance v3, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v4, "Capabilities (disable features) :: IS = "

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    const/4 v4, 0x4

    #@4f
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/cd/ServiceCapabilities;->isServiceSupported(I)Z

    #@52
    move-result v4

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    const-string v4, ", FT = "

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    const/4 v4, 0x1

    #@5e
    invoke-virtual {v1, v4}, Lcom/lge/ims/service/cd/ServiceCapabilities;->isServiceSupported(I)Z

    #@61
    move-result v4

    #@62
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v3

    #@6a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@6d
    .line 312
    const/4 v2, 0x5

    #@6e
    invoke-virtual {v1, v2}, Lcom/lge/ims/service/cd/ServiceCapabilities;->clearServiceCapabilities(I)V

    #@71
    .line 313
    const/high16 v2, 0xff

    #@73
    invoke-virtual {v1, v2}, Lcom/lge/ims/service/cd/ServiceCapabilities;->clearMediaCapabilities(I)V

    #@76
    .line 316
    :cond_76
    const-string v2, "CapService"

    #@78
    new-instance v3, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v4, "Capability query received :: "

    #@7f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v3

    #@83
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v3

    #@8b
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@8e
    .line 318
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@90
    if-eqz v2, :cond_25

    #@92
    .line 319
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@94
    invoke-virtual {v2}, Lcom/lge/ims/service/cd/CapabilityNotifier;->hasListener()Z

    #@97
    move-result v2

    #@98
    if-eqz v2, :cond_25

    #@9a
    .line 320
    iget-object v2, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@9c
    const/4 v3, 0x0

    #@9d
    invoke-virtual {v2, v1, v3}, Lcom/lge/ims/service/cd/CapabilityNotifier;->updateCapabilities(Lcom/lge/ims/service/cd/ServiceCapabilities;I)V

    #@a0
    goto :goto_25

    #@a1
    .line 295
    nop

    #@a2
    :pswitch_data_a2
    .packed-switch 0x280b
        :pswitch_26
    .end packed-switch
.end method

.method public removeBroadcastListener(Lcom/lge/ims/service/cd/CapabilitiesListener;)V
    .registers 3
    .parameter "listener"

    #@0
    .prologue
    .line 126
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 131
    :goto_4
    return-void

    #@5
    .line 130
    :cond_5
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@7
    invoke-virtual {v0, p1}, Lcom/lge/ims/service/cd/CapabilityNotifier;->removeBroadcastListener(Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@a
    goto :goto_4
.end method

.method public removeDedicatedListener(Ljava/lang/String;Lcom/lge/ims/service/cd/CapabilitiesListener;)V
    .registers 10
    .parameter "number"
    .parameter "listener"

    #@0
    .prologue
    .line 134
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@2
    if-nez v3, :cond_5

    #@4
    .line 159
    :goto_4
    return-void

    #@5
    .line 138
    :cond_5
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapNotifier:Lcom/lge/ims/service/cd/CapabilityNotifier;

    #@7
    invoke-virtual {v3, p1, p2}, Lcom/lge/ims/service/cd/CapabilityNotifier;->removeDedicatedListener(Ljava/lang/String;Lcom/lge/ims/service/cd/CapabilitiesListener;)V

    #@a
    .line 140
    iget-object v4, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@c
    monitor-enter v4

    #@d
    .line 141
    const/4 v2, 0x0

    #@e
    .line 143
    .local v2, phoneNumber:Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;
    const/4 v1, 0x0

    #@f
    .local v1, i:I
    :goto_f
    :try_start_f
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@11
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@14
    move-result v3

    #@15
    if-ge v1, v3, :cond_68

    #@17
    .line 144
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@19
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@1c
    move-result-object v3

    #@1d
    move-object v0, v3

    #@1e
    check-cast v0, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;

    #@20
    move-object v2, v0

    #@21
    .line 146
    if-nez v2, :cond_26

    #@23
    .line 143
    :cond_23
    add-int/lit8 v1, v1, 0x1

    #@25
    goto :goto_f

    #@26
    .line 150
    :cond_26
    invoke-virtual {v2}, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;->getNumber()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_23

    #@30
    .line 151
    invoke-virtual {v2}, Lcom/lge/ims/service/cd/CapabilityService$PhoneNumber;->removeReference()I

    #@33
    move-result v3

    #@34
    if-gtz v3, :cond_63

    #@36
    .line 152
    iget-object v3, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@38
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@3b
    .line 153
    const-string v3, "CapService"

    #@3d
    new-instance v5, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v6, "Phone number :: remove="

    #@44
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v5

    #@4c
    const-string v6, ":"

    #@4e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    iget-object v6, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapTrackingNumbers:Ljava/util/ArrayList;

    #@54
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@57
    move-result v6

    #@58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    invoke-static {v3, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@63
    .line 155
    :cond_63
    monitor-exit v4

    #@64
    goto :goto_4

    #@65
    .line 158
    :catchall_65
    move-exception v3

    #@66
    monitor-exit v4
    :try_end_67
    .catchall {:try_start_f .. :try_end_67} :catchall_65

    #@67
    throw v3

    #@68
    :cond_68
    :try_start_68
    monitor-exit v4
    :try_end_69
    .catchall {:try_start_68 .. :try_end_69} :catchall_65

    #@69
    goto :goto_4
.end method

.method public start(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 217
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mContext:Landroid/content/Context;

    #@2
    .line 221
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@4
    if-eqz v0, :cond_1b

    #@6
    .line 222
    iget-boolean v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCheckExternalStorageAvailability:Z

    #@8
    if-eqz v0, :cond_1b

    #@a
    .line 223
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@c
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->stopWatchingExternalStorage()V

    #@f
    .line 224
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@11
    iget-object v1, p0, Lcom/lge/ims/service/cd/CapabilityService;->mContext:Landroid/content/Context;

    #@13
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->setContext(Landroid/content/Context;)V

    #@16
    .line 225
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mCapStateTracker:Lcom/lge/ims/service/cd/CapabilityStateTracker;

    #@18
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityStateTracker;->startWatchingExternalStorage()V

    #@1b
    .line 229
    :cond_1b
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mDiscoveryThread:Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;

    #@1d
    if-eqz v0, :cond_24

    #@1f
    .line 230
    iget-object v0, p0, Lcom/lge/ims/service/cd/CapabilityService;->mDiscoveryThread:Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;

    #@21
    invoke-virtual {v0}, Lcom/lge/ims/service/cd/CapabilityService$CapabilityDiscoveryThread;->start()V

    #@24
    .line 232
    :cond_24
    return-void
.end method
