.class Lcom/lge/ims/service/cd/ICapabilityQuery$Stub$Proxy;
.super Ljava/lang/Object;
.source "ICapabilityQuery.java"

# interfaces
.implements Lcom/lge/ims/service/cd/ICapabilityQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/service/cd/ICapabilityQuery$Stub;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Proxy"
.end annotation


# instance fields
.field private mRemote:Landroid/os/IBinder;


# direct methods
.method constructor <init>(Landroid/os/IBinder;)V
    .registers 2
    .parameter "remote"

    #@0
    .prologue
    .line 76
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 77
    iput-object p1, p0, Lcom/lge/ims/service/cd/ICapabilityQuery$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@5
    .line 78
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .registers 2

    #@0
    .prologue
    .line 81
    iget-object v0, p0, Lcom/lge/ims/service/cd/ICapabilityQuery$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@2
    return-object v0
.end method

.method public getInterfaceDescriptor()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 85
    const-string v0, "com.lge.ims.service.cd.ICapabilityQuery"

    #@2
    return-object v0
.end method

.method public send(Ljava/lang/String;)Z
    .registers 9
    .parameter "target"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 91
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@5
    move-result-object v0

    #@6
    .line 92
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@9
    move-result-object v1

    #@a
    .line 95
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_a
    const-string v4, "com.lge.ims.service.cd.ICapabilityQuery"

    #@c
    invoke-virtual {v0, v4}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@f
    .line 96
    invoke-virtual {v0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@12
    .line 97
    iget-object v4, p0, Lcom/lge/ims/service/cd/ICapabilityQuery$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@14
    const/4 v5, 0x1

    #@15
    const/4 v6, 0x0

    #@16
    invoke-interface {v4, v5, v0, v1, v6}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@19
    .line 98
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V

    #@1c
    .line 99
    invoke-virtual {v1}, Landroid/os/Parcel;->readInt()I
    :try_end_1f
    .catchall {:try_start_a .. :try_end_1f} :catchall_2b

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_29

    #@22
    .line 102
    .local v2, _result:Z
    :goto_22
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@25
    .line 103
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@28
    .line 105
    return v2

    #@29
    .end local v2           #_result:Z
    :cond_29
    move v2, v3

    #@2a
    .line 99
    goto :goto_22

    #@2b
    .line 102
    :catchall_2b
    move-exception v3

    #@2c
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2f
    .line 103
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@32
    throw v3
.end method

.method public setListener(Lcom/lge/ims/service/cd/ICapabilityQueryListener;)V
    .registers 7
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 111
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 112
    .local v0, _data:Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@7
    move-result-object v1

    #@8
    .line 114
    .local v1, _reply:Landroid/os/Parcel;
    :try_start_8
    const-string v2, "com.lge.ims.service.cd.ICapabilityQuery"

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    #@d
    .line 115
    if-eqz p1, :cond_27

    #@f
    invoke-interface {p1}, Lcom/lge/ims/service/cd/ICapabilityQueryListener;->asBinder()Landroid/os/IBinder;

    #@12
    move-result-object v2

    #@13
    :goto_13
    invoke-virtual {v0, v2}, Landroid/os/Parcel;->writeStrongBinder(Landroid/os/IBinder;)V

    #@16
    .line 116
    iget-object v2, p0, Lcom/lge/ims/service/cd/ICapabilityQuery$Stub$Proxy;->mRemote:Landroid/os/IBinder;

    #@18
    const/4 v3, 0x2

    #@19
    const/4 v4, 0x0

    #@1a
    invoke-interface {v2, v3, v0, v1, v4}, Landroid/os/IBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    #@1d
    .line 117
    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_20
    .catchall {:try_start_8 .. :try_end_20} :catchall_29

    #@20
    .line 120
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@23
    .line 121
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@26
    .line 123
    return-void

    #@27
    .line 115
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_13

    #@29
    .line 120
    :catchall_29
    move-exception v2

    #@2a
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@2d
    .line 121
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    #@30
    throw v2
.end method
