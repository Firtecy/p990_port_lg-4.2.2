.class public Lcom/lge/ims/service/cs/VideoSessionImpl;
.super Lcom/lge/ims/service/cs/IVideoSession$Stub;
.source "VideoSessionImpl.java"

# interfaces
.implements Lcom/lge/ims/JNICoreListener;


# instance fields
.field private listener:Lcom/lge/ims/service/cs/IVideoSessionListener;

.field private nMediaQuality:I

.field private nSessionType:I

.field private nativeObj:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 19
    invoke-direct {p0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;-><init>()V

    #@4
    .line 14
    iput v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nSessionType:I

    #@6
    .line 15
    iput v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nMediaQuality:I

    #@8
    .line 16
    iput v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@a
    .line 17
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->listener:Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@d
    .line 20
    const-string v0, "create MO session"

    #@f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@12
    .line 21
    const/4 v0, 0x1

    #@13
    iput v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nSessionType:I

    #@15
    .line 22
    const/16 v0, 0xc

    #@17
    invoke-static {v0}, Lcom/lge/ims/JNICore;->getInterface(I)I

    #@1a
    move-result v0

    #@1b
    iput v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@1d
    .line 23
    iget v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@1f
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@22
    .line 24
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "nativeObj"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 26
    invoke-direct {p0}, Lcom/lge/ims/service/cs/IVideoSession$Stub;-><init>()V

    #@4
    .line 14
    iput v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nSessionType:I

    #@6
    .line 15
    iput v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nMediaQuality:I

    #@8
    .line 16
    iput v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@a
    .line 17
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->listener:Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@d
    .line 27
    const-string v0, "create MT session"

    #@f
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@12
    .line 28
    const/4 v0, 0x2

    #@13
    iput v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nSessionType:I

    #@15
    .line 29
    iput p1, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@17
    .line 30
    invoke-static {p1, p0}, Lcom/lge/ims/JNICore;->setListener(ILcom/lge/ims/JNICoreListener;)I

    #@1a
    .line 31
    return-void
.end method


# virtual methods
.method public accept()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 131
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 132
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 133
    const/4 v2, 0x0

    #@a
    .line 143
    :goto_a
    return v2

    #@b
    .line 136
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 137
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x28d5

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 138
    const/4 v2, 0x1

    #@15
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@18
    .line 139
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1b
    move-result-object v0

    #@1c
    .line 140
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1f
    .line 141
    const/4 v1, 0x0

    #@20
    .line 143
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@22
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@25
    move-result v2

    #@26
    goto :goto_a
.end method

.method public changeViewSize(I)I
    .registers 7
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 231
    const-string v3, ""

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 232
    iget v3, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@8
    if-nez v3, :cond_b

    #@a
    .line 246
    :goto_a
    return v2

    #@b
    .line 236
    :cond_b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v4, "VideoSessionImpl changeViewSize Called: nSessionType "

    #@12
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    iget v4, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nSessionType:I

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@23
    .line 237
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@26
    move-result-object v1

    #@27
    .line 238
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v3, 0x28ea

    #@29
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 239
    iget v3, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nSessionType:I

    #@2e
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 240
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 241
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@37
    .line 242
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@3a
    move-result-object v0

    #@3b
    .line 243
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3e
    .line 244
    const/4 v1, 0x0

    #@3f
    .line 246
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@41
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@44
    move-result v2

    #@45
    goto :goto_a
.end method

.method public changeViewSizeEx(I)I
    .registers 6
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 250
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 251
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 252
    const/4 v2, 0x0

    #@a
    .line 265
    :goto_a
    return v2

    #@b
    .line 255
    :cond_b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "VideoSessionImpl changeViewSizeEx Called: nSessionType "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    iget v3, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nSessionType:I

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@23
    .line 256
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@26
    move-result-object v1

    #@27
    .line 257
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x28ea

    #@29
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@2c
    .line 258
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nSessionType:I

    #@2e
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@31
    .line 259
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@34
    .line 260
    const/4 v2, 0x1

    #@35
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@38
    .line 261
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@3b
    move-result-object v0

    #@3c
    .line 262
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@3f
    .line 263
    const/4 v1, 0x0

    #@40
    .line 265
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@42
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@45
    move-result v2

    #@46
    goto :goto_a
.end method

.method public getMediaQuality()I
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 226
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 227
    iget v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nMediaQuality:I

    #@7
    return v0
.end method

.method public getNativeObj()I
    .registers 2

    #@0
    .prologue
    .line 34
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 35
    iget v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@7
    return v0
.end method

.method public getSessionType()I
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 95
    iget v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nSessionType:I

    #@2
    return v0
.end method

.method public onMessage(Landroid/os/Parcel;)V
    .registers 8
    .parameter "parcel"

    #@0
    .prologue
    .line 49
    iget-object v4, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->listener:Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@2
    if-nez v4, :cond_a

    #@4
    .line 50
    const-string v4, "called but listener is null"

    #@6
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@9
    .line 84
    :goto_9
    return-void

    #@a
    .line 54
    :cond_a
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@d
    move-result v2

    #@e
    .line 57
    .local v2, msg:I
    sparse-switch v2, :sswitch_data_6e

    #@11
    .line 78
    :try_start_11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "unknown message received from native msg : "

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_11 .. :try_end_27} :catch_28

    #@27
    goto :goto_9

    #@28
    .line 81
    :catch_28
    move-exception v0

    #@29
    .line 82
    .local v0, e:Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v4}, Lcom/lge/ims/util/IMSLog;->e(Ljava/lang/String;)V

    #@34
    goto :goto_9

    #@35
    .line 59
    .end local v0           #e:Landroid/os/RemoteException;
    :sswitch_35
    :try_start_35
    iget-object v4, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->listener:Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@37
    invoke-interface {v4}, Lcom/lge/ims/service/cs/IVideoSessionListener;->onPrepared()V

    #@3a
    goto :goto_9

    #@3b
    .line 63
    :sswitch_3b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@3e
    move-result v3

    #@3f
    .line 64
    .local v3, result:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@42
    move-result v1

    #@43
    .line 65
    .local v1, errorReason:I
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@46
    move-result v4

    #@47
    iput v4, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nMediaQuality:I

    #@49
    .line 66
    iget-object v4, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->listener:Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@4b
    invoke-interface {v4, v3, v1}, Lcom/lge/ims/service/cs/IVideoSessionListener;->onStart(II)V

    #@4e
    goto :goto_9

    #@4f
    .line 69
    .end local v1           #errorReason:I
    .end local v3           #result:I
    :sswitch_4f
    iget-object v4, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->listener:Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@51
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@54
    move-result v5

    #@55
    invoke-interface {v4, v5}, Lcom/lge/ims/service/cs/IVideoSessionListener;->onStop(I)V

    #@58
    goto :goto_9

    #@59
    .line 72
    :sswitch_59
    iget-object v4, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->listener:Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@5b
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@5e
    move-result v5

    #@5f
    invoke-interface {v4, v5}, Lcom/lge/ims/service/cs/IVideoSessionListener;->onStartMediaRecord(I)V

    #@62
    goto :goto_9

    #@63
    .line 75
    :sswitch_63
    iget-object v4, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->listener:Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@65
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    #@68
    move-result v5

    #@69
    invoke-interface {v4, v5}, Lcom/lge/ims/service/cs/IVideoSessionListener;->onStopMediaRecord(I)V
    :try_end_6c
    .catch Landroid/os/RemoteException; {:try_start_35 .. :try_end_6c} :catch_28

    #@6c
    goto :goto_9

    #@6d
    .line 57
    nop

    #@6e
    :sswitch_data_6e
    .sparse-switch
        0x28d4 -> :sswitch_35
        0x28d6 -> :sswitch_3b
        0x28d8 -> :sswitch_4f
        0x28de -> :sswitch_59
        0x28e0 -> :sswitch_63
    .end sparse-switch
.end method

.method public reject()I
    .registers 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 147
    const-string v3, ""

    #@3
    invoke-static {v3}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@6
    .line 148
    iget v3, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@8
    if-nez v3, :cond_b

    #@a
    .line 159
    :goto_a
    return v2

    #@b
    .line 152
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 153
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v3, 0x28d5

    #@11
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 154
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 155
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1a
    move-result-object v0

    #@1b
    .line 156
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 157
    const/4 v1, 0x0

    #@1f
    .line 159
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@21
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@24
    move-result v2

    #@25
    goto :goto_a
.end method

.method public setListener(Lcom/lge/ims/service/cs/IVideoSessionListener;)Z
    .registers 3
    .parameter "listener"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 88
    iput-object p1, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->listener:Lcom/lge/ims/service/cs/IVideoSessionListener;

    #@2
    .line 90
    const/4 v0, 0x1

    #@3
    return v0
.end method

.method public start(Ljava/lang/String;)I
    .registers 5
    .parameter "peerPhoneNumber"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 99
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 100
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 101
    const/4 v2, 0x0

    #@a
    .line 112
    :goto_a
    return v2

    #@b
    .line 104
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 105
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x28d3

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 106
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 108
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1a
    move-result-object v0

    #@1b
    .line 109
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 110
    const/4 v1, 0x0

    #@1f
    .line 112
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@21
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@24
    move-result v2

    #@25
    goto :goto_a
.end method

.method public startMediaRecord(Ljava/lang/String;)I
    .registers 5
    .parameter "fileName"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 163
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 164
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 165
    const/4 v2, 0x0

    #@a
    .line 175
    :goto_a
    return v2

    #@b
    .line 168
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 169
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x28dd

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 170
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@17
    .line 171
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1a
    move-result-object v0

    #@1b
    .line 172
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 173
    const/4 v1, 0x0

    #@1f
    .line 175
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@21
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@24
    move-result v2

    #@25
    goto :goto_a
.end method

.method public stop()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 116
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 117
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 118
    const/4 v2, 0x0

    #@a
    .line 127
    :goto_a
    return v2

    #@b
    .line 121
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 122
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x28d7

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 123
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@17
    move-result-object v0

    #@18
    .line 124
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 125
    const/4 v1, 0x0

    #@1c
    .line 127
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@1e
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@21
    move-result v2

    #@22
    goto :goto_a
.end method

.method public stopMediaRecord()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 179
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 180
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 181
    const/4 v2, 0x0

    #@a
    .line 190
    :goto_a
    return v2

    #@b
    .line 184
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 185
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x28df

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 186
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@17
    move-result-object v0

    #@18
    .line 187
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1b
    .line 188
    const/4 v1, 0x0

    #@1c
    .line 190
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@1e
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@21
    move-result v2

    #@22
    goto :goto_a
.end method

.method public swapCamera(I)I
    .registers 5
    .parameter "cameraID"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 194
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 195
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 196
    const/4 v2, 0x0

    #@a
    .line 206
    :goto_a
    return v2

    #@b
    .line 199
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 200
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x28e7

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 201
    invoke-virtual {v1, p1}, Landroid/os/Parcel;->writeInt(I)V

    #@17
    .line 202
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1a
    move-result-object v0

    #@1b
    .line 203
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@1e
    .line 204
    const/4 v1, 0x0

    #@1f
    .line 206
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@21
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@24
    move-result v2

    #@25
    goto :goto_a
.end method

.method public terminate()V
    .registers 2

    #@0
    .prologue
    .line 39
    const-string v0, ""

    #@2
    invoke-static {v0}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 40
    iget v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@7
    invoke-static {v0, p0}, Lcom/lge/ims/JNICore;->removeListener(ILcom/lge/ims/JNICoreListener;)I

    #@a
    .line 41
    iget v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@c
    invoke-static {v0}, Lcom/lge/ims/JNICore;->releaseInterface(I)I

    #@f
    .line 43
    const/4 v0, 0x0

    #@10
    iput v0, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@12
    .line 46
    return-void
.end method

.method public updateDiaplay()I
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 210
    const-string v2, ""

    #@2
    invoke-static {v2}, Lcom/lge/ims/util/IMSLog;->i(Ljava/lang/String;)V

    #@5
    .line 211
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@7
    if-nez v2, :cond_b

    #@9
    .line 212
    const/4 v2, 0x0

    #@a
    .line 222
    :goto_a
    return v2

    #@b
    .line 215
    :cond_b
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@e
    move-result-object v1

    #@f
    .line 216
    .local v1, parcel:Landroid/os/Parcel;
    const/16 v2, 0x28e8

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@14
    .line 217
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nSessionType:I

    #@16
    invoke-virtual {v1, v2}, Landroid/os/Parcel;->writeInt(I)V

    #@19
    .line 218
    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    #@1c
    move-result-object v0

    #@1d
    .line 219
    .local v0, baData:[B
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@20
    .line 220
    const/4 v1, 0x0

    #@21
    .line 222
    iget v2, p0, Lcom/lge/ims/service/cs/VideoSessionImpl;->nativeObj:I

    #@23
    invoke-static {v2, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@26
    move-result v2

    #@27
    goto :goto_a
.end method
