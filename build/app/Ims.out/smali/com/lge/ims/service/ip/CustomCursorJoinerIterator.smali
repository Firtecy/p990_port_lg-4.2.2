.class public Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;
.super Ljava/lang/Object;
.source "CustomCursorJoinerIterator.java"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/service/ip/CustomCursorJoinerIterator$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Landroid/database/CursorJoiner$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private iTotalLength:I

.field private index:I

.field private leftCur:Landroid/database/Cursor;

.field private objCursorJoiner:Lcom/lge/ims/service/ip/CustomCursorJoiner;

.field private rightCur:Landroid/database/Cursor;

.field private tempResult:Landroid/database/CursorJoiner$Result;


# direct methods
.method public constructor <init>(Lcom/lge/ims/service/ip/CustomCursorJoiner;)V
    .registers 4
    .parameter "objJoiner"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 44
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 37
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->objCursorJoiner:Lcom/lge/ims/service/ip/CustomCursorJoiner;

    #@7
    .line 39
    iput v1, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->iTotalLength:I

    #@9
    .line 40
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->tempResult:Landroid/database/CursorJoiner$Result;

    #@b
    .line 41
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->leftCur:Landroid/database/Cursor;

    #@d
    .line 42
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->rightCur:Landroid/database/Cursor;

    #@f
    .line 45
    iput-object p1, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->objCursorJoiner:Lcom/lge/ims/service/ip/CustomCursorJoiner;

    #@11
    .line 46
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->objCursorJoiner:Lcom/lge/ims/service/ip/CustomCursorJoiner;

    #@13
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->getLeftCursor()Landroid/database/Cursor;

    #@16
    move-result-object v0

    #@17
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->leftCur:Landroid/database/Cursor;

    #@19
    .line 47
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->objCursorJoiner:Lcom/lge/ims/service/ip/CustomCursorJoiner;

    #@1b
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->getRightCursor()Landroid/database/Cursor;

    #@1e
    move-result-object v0

    #@1f
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->rightCur:Landroid/database/Cursor;

    #@21
    .line 48
    iput v1, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->index:I

    #@23
    .line 49
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->objCursorJoiner:Lcom/lge/ims/service/ip/CustomCursorJoiner;

    #@25
    invoke-virtual {v0}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->getLength()I

    #@28
    move-result v0

    #@29
    iput v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->iTotalLength:I

    #@2b
    .line 51
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->leftCur:Landroid/database/Cursor;

    #@2d
    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    #@30
    .line 52
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->rightCur:Landroid/database/Cursor;

    #@32
    invoke-interface {v0}, Landroid/database/Cursor;->moveToPrevious()Z

    #@35
    .line 53
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .registers 3

    #@0
    .prologue
    .line 57
    iget v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->index:I

    #@2
    iget v1, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->iTotalLength:I

    #@4
    if-ge v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public next()Landroid/database/CursorJoiner$Result;
    .registers 3

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->objCursorJoiner:Lcom/lge/ims/service/ip/CustomCursorJoiner;

    #@2
    iget v1, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->index:I

    #@4
    invoke-virtual {v0, v1}, Lcom/lge/ims/service/ip/CustomCursorJoiner;->getResultAt(I)Landroid/database/CursorJoiner$Result;

    #@7
    move-result-object v0

    #@8
    iput-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->tempResult:Landroid/database/CursorJoiner$Result;

    #@a
    .line 64
    sget-object v0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator$1;->$SwitchMap$android$database$CursorJoiner$Result:[I

    #@c
    iget-object v1, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->tempResult:Landroid/database/CursorJoiner$Result;

    #@e
    invoke-virtual {v1}, Landroid/database/CursorJoiner$Result;->ordinal()I

    #@11
    move-result v1

    #@12
    aget v0, v0, v1

    #@14
    packed-switch v0, :pswitch_data_38

    #@17
    .line 79
    :goto_17
    iget v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->index:I

    #@19
    add-int/lit8 v0, v0, 0x1

    #@1b
    iput v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->index:I

    #@1d
    .line 81
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->tempResult:Landroid/database/CursorJoiner$Result;

    #@1f
    return-object v0

    #@20
    .line 66
    :pswitch_20
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->leftCur:Landroid/database/Cursor;

    #@22
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    #@25
    goto :goto_17

    #@26
    .line 69
    :pswitch_26
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->rightCur:Landroid/database/Cursor;

    #@28
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    #@2b
    goto :goto_17

    #@2c
    .line 72
    :pswitch_2c
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->leftCur:Landroid/database/Cursor;

    #@2e
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    #@31
    .line 73
    iget-object v0, p0, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->rightCur:Landroid/database/Cursor;

    #@33
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    #@36
    goto :goto_17

    #@37
    .line 64
    nop

    #@38
    :pswitch_data_38
    .packed-switch 0x1
        :pswitch_20
        :pswitch_26
        :pswitch_2c
    .end packed-switch
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .registers 2

    #@0
    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/lge/ims/service/ip/CustomCursorJoinerIterator;->next()Landroid/database/CursorJoiner$Result;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public remove()V
    .registers 1

    #@0
    .prologue
    .line 87
    return-void
.end method
