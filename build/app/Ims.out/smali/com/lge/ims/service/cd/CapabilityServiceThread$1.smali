.class Lcom/lge/ims/service/cd/CapabilityServiceThread$1;
.super Landroid/os/Handler;
.source "CapabilityServiceThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/ims/service/cd/CapabilityServiceThread;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/service/cd/CapabilityServiceThread;


# direct methods
.method constructor <init>(Lcom/lge/ims/service/cd/CapabilityServiceThread;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 142
    iput-object p1, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread$1;->this$0:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 12
    .parameter "msg"

    #@0
    .prologue
    const v9, 0xffffff

    #@3
    const/4 v8, 0x0

    #@4
    .line 145
    if-nez p1, :cond_7

    #@6
    .line 221
    :goto_6
    return-void

    #@7
    .line 149
    :cond_7
    const-string v5, "CapServiceThread"

    #@9
    new-instance v6, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v7, "handleMessage - "

    #@10
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    iget v7, p1, Landroid/os/Message;->what:I

    #@16
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    const-string v7, " tid="

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    iget-object v7, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread$1;->this$0:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@22
    invoke-static {v7}, Lcom/lge/ims/service/cd/CapabilityServiceThread;->access$000(Lcom/lge/ims/service/cd/CapabilityServiceThread;)I

    #@25
    move-result v7

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v6

    #@2e
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@31
    .line 151
    iget v5, p1, Landroid/os/Message;->what:I

    #@33
    const/4 v6, 0x1

    #@34
    if-ne v5, v6, :cond_46

    #@36
    .line 152
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@39
    move-result-object v2

    #@3a
    .line 154
    .local v2, looper:Landroid/os/Looper;
    if-eqz v2, :cond_3f

    #@3c
    .line 155
    invoke-virtual {v2}, Landroid/os/Looper;->quit()V

    #@3f
    .line 157
    :cond_3f
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread$1;->this$0:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@41
    const/4 v6, 0x0

    #@42
    invoke-static {v5, v6}, Lcom/lge/ims/service/cd/CapabilityServiceThread;->access$102(Lcom/lge/ims/service/cd/CapabilityServiceThread;Landroid/os/Handler;)Landroid/os/Handler;

    #@45
    goto :goto_6

    #@46
    .line 161
    .end local v2           #looper:Landroid/os/Looper;
    :cond_46
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@48
    check-cast v3, Landroid/os/Parcel;

    #@4a
    .line 163
    .local v3, parcel:Landroid/os/Parcel;
    if-nez v3, :cond_54

    #@4c
    .line 164
    const-string v5, "CapServiceThread"

    #@4e
    const-string v6, "parcel is null"

    #@50
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@53
    goto :goto_6

    #@54
    .line 168
    :cond_54
    iget v5, p1, Landroid/os/Message;->what:I

    #@56
    sparse-switch v5, :sswitch_data_ea

    #@59
    .line 215
    const-string v5, "CapServiceThread"

    #@5b
    new-instance v6, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v7, "Unhandled Message :: "

    #@62
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    iget v7, p1, Landroid/os/Message;->what:I

    #@68
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v6

    #@6c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v6

    #@70
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@73
    .line 219
    :cond_73
    :goto_73
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    #@76
    .line 220
    const/4 v3, 0x0

    #@77
    .line 221
    goto :goto_6

    #@78
    .line 171
    :sswitch_78
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread$1;->this$0:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@7a
    invoke-static {v5}, Lcom/lge/ims/service/cd/CapabilityServiceThread;->access$200(Lcom/lge/ims/service/cd/CapabilityServiceThread;)Lcom/lge/ims/JNICoreListener;

    #@7d
    move-result-object v5

    #@7e
    if-eqz v5, :cond_73

    #@80
    .line 172
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread$1;->this$0:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@82
    invoke-static {v5}, Lcom/lge/ims/service/cd/CapabilityServiceThread;->access$200(Lcom/lge/ims/service/cd/CapabilityServiceThread;)Lcom/lge/ims/JNICoreListener;

    #@85
    move-result-object v5

    #@86
    invoke-interface {v5, v3}, Lcom/lge/ims/JNICoreListener;->onMessage(Landroid/os/Parcel;)V

    #@89
    goto :goto_73

    #@8a
    .line 179
    :sswitch_8a
    invoke-virtual {v3}, Landroid/os/Parcel;->readInt()I

    #@8d
    move-result v0

    #@8e
    .line 181
    .local v0, event:I
    const-string v5, "CapServiceThread"

    #@90
    new-instance v6, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    const-string v7, "Event = "

    #@97
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v6

    #@9f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v6

    #@a3
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@a6
    .line 183
    const/16 v5, 0x283d

    #@a8
    if-ne v0, v5, :cond_73

    #@aa
    .line 184
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@ad
    move-result-object v4

    #@ae
    .line 186
    .local v4, parcel4CQ:Landroid/os/Parcel;
    iget-object v5, p0, Lcom/lge/ims/service/cd/CapabilityServiceThread$1;->this$0:Lcom/lge/ims/service/cd/CapabilityServiceThread;

    #@b0
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@b2
    invoke-virtual {v5, v6}, Lcom/lge/ims/service/cd/CapabilityServiceThread;->getListener(I)Lcom/lge/ims/JNICoreListener;

    #@b5
    move-result-object v1

    #@b6
    .line 188
    .local v1, listener:Lcom/lge/ims/JNICoreListener;
    if-nez v1, :cond_c3

    #@b8
    .line 189
    const-string v5, "CapServiceThread"

    #@ba
    const-string v6, "Listener is null"

    #@bc
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@bf
    .line 190
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@c2
    goto :goto_73

    #@c3
    .line 194
    :cond_c3
    const/16 v5, 0x286f

    #@c5
    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    #@c8
    .line 196
    invoke-virtual {v4, v8}, Landroid/os/Parcel;->writeInt(I)V

    #@cb
    .line 198
    invoke-virtual {v3}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    #@ce
    move-result-object v5

    #@cf
    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    #@d2
    .line 200
    invoke-virtual {v4, v9}, Landroid/os/Parcel;->writeInt(I)V

    #@d5
    .line 202
    invoke-virtual {v4, v9}, Landroid/os/Parcel;->writeInt(I)V

    #@d8
    .line 204
    const-string v5, "CapServiceThread"

    #@da
    const-string v6, "handleMessage - invoke onMessage()"

    #@dc
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@df
    .line 206
    invoke-virtual {v4, v8}, Landroid/os/Parcel;->setDataPosition(I)V

    #@e2
    .line 208
    invoke-interface {v1, v4}, Lcom/lge/ims/JNICoreListener;->onMessage(Landroid/os/Parcel;)V

    #@e5
    .line 209
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    #@e8
    goto :goto_73

    #@e9
    .line 168
    nop

    #@ea
    :sswitch_data_ea
    .sparse-switch
        0x64 -> :sswitch_78
        0xc8 -> :sswitch_8a
    .end sparse-switch
.end method
