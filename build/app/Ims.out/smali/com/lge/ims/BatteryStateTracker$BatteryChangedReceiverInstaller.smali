.class final Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverInstaller;
.super Ljava/lang/Object;
.source "BatteryStateTracker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/BatteryStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BatteryChangedReceiverInstaller"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/BatteryStateTracker;


# direct methods
.method public constructor <init>(Lcom/lge/ims/BatteryStateTracker;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 411
    iput-object p1, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverInstaller;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 412
    return-void
.end method


# virtual methods
.method public run()V
    .registers 4

    #@0
    .prologue
    .line 416
    const-string v0, "BatteryStateTracker"

    #@2
    const-string v1, "BatteryChangedReceiverInstaller is run..."

    #@4
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 418
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverInstaller;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@9
    invoke-static {v0}, Lcom/lge/ims/BatteryStateTracker;->access$1000(Lcom/lge/ims/BatteryStateTracker;)Landroid/content/Context;

    #@c
    move-result-object v0

    #@d
    if-nez v0, :cond_17

    #@f
    .line 419
    const-string v0, "BatteryStateTracker"

    #@11
    const-string v1, "Context is null"

    #@13
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 426
    :cond_16
    :goto_16
    return-void

    #@17
    .line 423
    :cond_17
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverInstaller;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@19
    invoke-static {v0}, Lcom/lge/ims/BatteryStateTracker;->access$1100(Lcom/lge/ims/BatteryStateTracker;)Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@1c
    move-result-object v0

    #@1d
    if-eqz v0, :cond_16

    #@1f
    .line 424
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverInstaller;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@21
    invoke-static {v0}, Lcom/lge/ims/BatteryStateTracker;->access$1000(Lcom/lge/ims/BatteryStateTracker;)Landroid/content/Context;

    #@24
    move-result-object v0

    #@25
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverInstaller;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@27
    invoke-static {v1}, Lcom/lge/ims/BatteryStateTracker;->access$1100(Lcom/lge/ims/BatteryStateTracker;)Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@2a
    move-result-object v1

    #@2b
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverInstaller;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@2d
    invoke-static {v2}, Lcom/lge/ims/BatteryStateTracker;->access$1100(Lcom/lge/ims/BatteryStateTracker;)Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->getFilter()Landroid/content/IntentFilter;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@38
    goto :goto_16
.end method
