.class public Lcom/lge/ims/util/RCSConstant;
.super Ljava/lang/Object;
.source "RCSConstant.java"


# static fields
.field public static final AOS_ONSTART:I = 0x2714

.field public static final AOS_ONSTOP:I = 0x2716

.field public static final AOS_START:I = 0x2713

.field public static final AOS_STOP:I = 0x2715

.field public static final INTERFACETYPE_CAPADISCOVERY:I = 0x1f

.field public static final INTERFACETYPE_CHAT:I = 0x16

.field public static final INTERFACETYPE_CONFERENCE:I = 0x17

.field public static final INTERFACETYPE_CONTENTSHARE:I = 0xb

.field public static final INTERFACETYPE_FILETRANSFER:I = 0x18

.field public static final INTERFACETYPE_IMAGESESSION:I = 0xd

.field public static final INTERFACETYPE_IMSERVICE:I = 0x15

.field public static final INTERFACETYPE_IMSPEOPLE:I = 0x20

.field public static final INTERFACETYPE_SERVICE:I = 0x0

.field public static final INTERFACETYPE_SYSTEM:I = 0x1

.field public static final INTERFACETYPE_VIDEOSESSION:I = 0xc

.field public static final SYSTEM_CMD_CHANGED_STATE_AIRPLANE:I = 0x5

.field public static final SYSTEM_CMD_CHANGED_STATE_CALL:I = 0x3

.field public static final SYSTEM_CMD_CHANGED_STATE_DATA:I = 0x2

.field public static final SYSTEM_CMD_CHANGED_STATE_POWER:I = 0x4

.field public static final SYSTEM_CMD_CHANGED_STATE_SERVICE:I = 0x1

.field public static final SYSTEM_CMD_CHANGED_STATE_WIFI:I = 0x6

.field public static final SYSTEM_CMD_GET_DATASTATE:I = 0x66

.field public static final SYSTEM_CMD_GET_MCC:I = 0x6e

.field public static final SYSTEM_CMD_GET_MNC:I = 0x6f

.field public static final SYSTEM_CMD_GET_NETWORKTYPE:I = 0x67

.field public static final SYSTEM_CMD_GET_PHONENUMBER:I = 0x6b

.field public static final SYSTEM_CMD_GET_POWERLEVEL:I = 0x6a

.field public static final SYSTEM_CMD_GET_SERVICESTATE:I = 0x68

.field public static final SYSTEM_CMD_GET_SUBSCRIBERID:I = 0x6c

.field public static final SYSTEM_CMD_GET_WIFIADDRESS:I = 0x6d

.field public static final SYSTEM_CMD_GET_WIFISTATE:I = 0x69

.field public static final SYSTEM_CMD_KILL_ALARM:I = 0x73

.field public static final SYSTEM_CMD_NOTIFY_ALARM:I = 0x8

.field public static final SYSTEM_CMD_RESET_DATACONNECTIVITY:I = 0x71

.field public static final SYSTEM_CMD_SET_ALARM:I = 0x72

.field public static final SYSTEM_CMD_SET_DATACONNECTIVITY:I = 0x65

.field public static final SYSTEM_CMD_SUCCESS_DATA_CONNECTION:I = 0x7

.field public static final SYTEM_CMD_GET_MOBILEADDRESS:I = 0x70


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
