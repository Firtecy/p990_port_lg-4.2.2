.class public Lcom/lge/ims/JNICore;
.super Ljava/lang/Object;
.source "JNICore.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "JNICore"

.field private static mListeners:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/lge/ims/JNICoreListener;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 18
    new-instance v0, Ljava/util/Hashtable;

    #@2
    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/JNICore;->mListeners:Ljava/util/Hashtable;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 15
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static native construct()V
.end method

.method public static native destruct()V
.end method

.method public static native getInterface(I)I
.end method

.method public static getListener(I)Lcom/lge/ims/JNICoreListener;
    .registers 7
    .parameter "nativeObject"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 87
    if-nez p0, :cond_1c

    #@3
    .line 88
    const-string v3, "JNICore"

    #@5
    new-instance v4, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v5, "Illegal argument :: nativeObject="

    #@c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v4

    #@10
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    .line 105
    :goto_1b
    return-object v2

    #@1c
    .line 92
    :cond_1c
    new-instance v1, Ljava/lang/Integer;

    #@1e
    invoke-direct {v1, p0}, Ljava/lang/Integer;-><init>(I)V

    #@21
    .line 94
    .local v1, key:Ljava/lang/Integer;
    if-nez v1, :cond_42

    #@23
    .line 95
    const-string v3, "JNICore"

    #@25
    new-instance v4, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v5, "Instantiating key ("

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    const-string v5, ") failed"

    #@36
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v4

    #@3a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    goto :goto_1b

    #@42
    .line 99
    :cond_42
    const/4 v2, 0x0

    #@43
    .line 101
    .local v2, listener:Lcom/lge/ims/JNICoreListener;
    sget-object v4, Lcom/lge/ims/JNICore;->mListeners:Ljava/util/Hashtable;

    #@45
    monitor-enter v4

    #@46
    .line 102
    :try_start_46
    sget-object v3, Lcom/lge/ims/JNICore;->mListeners:Ljava/util/Hashtable;

    #@48
    invoke-virtual {v3, v1}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4b
    move-result-object v3

    #@4c
    move-object v0, v3

    #@4d
    check-cast v0, Lcom/lge/ims/JNICoreListener;

    #@4f
    move-object v2, v0

    #@50
    .line 103
    monitor-exit v4

    #@51
    goto :goto_1b

    #@52
    :catchall_52
    move-exception v3

    #@53
    monitor-exit v4
    :try_end_54
    .catchall {:try_start_46 .. :try_end_54} :catchall_52

    #@54
    throw v3
.end method

.method public static obtainParcel(I)Landroid/os/Parcel;
    .registers 4
    .parameter "eventName"

    #@0
    .prologue
    .line 27
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@3
    move-result-object v0

    #@4
    .line 29
    .local v0, parcel:Landroid/os/Parcel;
    if-nez v0, :cond_f

    #@6
    .line 30
    const-string v1, "JNICore"

    #@8
    const-string v2, "Parcel is null"

    #@a
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 31
    const/4 v0, 0x0

    #@e
    .line 37
    .end local v0           #parcel:Landroid/os/Parcel;
    :goto_e
    return-object v0

    #@f
    .line 35
    .restart local v0       #parcel:Landroid/os/Parcel;
    :cond_f
    invoke-virtual {v0, p0}, Landroid/os/Parcel;->writeInt(I)V

    #@12
    goto :goto_e
.end method

.method public static native releaseInterface(I)I
.end method

.method public static removeListener(ILcom/lge/ims/JNICoreListener;)I
    .registers 7
    .parameter "nativeObject"
    .parameter "listener"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 136
    const-string v2, "JNICore"

    #@3
    const-string v3, ""

    #@5
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 138
    if-nez p0, :cond_23

    #@a
    .line 139
    const-string v2, "JNICore"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Illegal argument :: nativeObject="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 154
    :goto_22
    return v1

    #@23
    .line 143
    :cond_23
    new-instance v0, Ljava/lang/Integer;

    #@25
    invoke-direct {v0, p0}, Ljava/lang/Integer;-><init>(I)V

    #@28
    .line 145
    .local v0, key:Ljava/lang/Integer;
    if-nez v0, :cond_49

    #@2a
    .line 146
    const-string v2, "JNICore"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "Instantiating key ("

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    const-string v4, ") failed"

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@48
    goto :goto_22

    #@49
    .line 150
    :cond_49
    sget-object v2, Lcom/lge/ims/JNICore;->mListeners:Ljava/util/Hashtable;

    #@4b
    monitor-enter v2

    #@4c
    .line 151
    :try_start_4c
    sget-object v1, Lcom/lge/ims/JNICore;->mListeners:Ljava/util/Hashtable;

    #@4e
    invoke-virtual {v1, v0}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@51
    .line 152
    monitor-exit v2

    #@52
    .line 154
    const/4 v1, 0x1

    #@53
    goto :goto_22

    #@54
    .line 152
    :catchall_54
    move-exception v1

    #@55
    monitor-exit v2
    :try_end_56
    .catchall {:try_start_4c .. :try_end_56} :catchall_54

    #@56
    throw v1
.end method

.method public static native sendData(I[B)I
.end method

.method public static sendData2Java(I[B)I
    .registers 7
    .parameter "nativeObject"
    .parameter "baData"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 66
    invoke-static {p0}, Lcom/lge/ims/JNICore;->getListener(I)Lcom/lge/ims/JNICoreListener;

    #@4
    move-result-object v0

    #@5
    .line 68
    .local v0, listener:Lcom/lge/ims/JNICoreListener;
    if-nez v0, :cond_21

    #@7
    .line 69
    const-string v2, "JNICore"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "No listener :: nativeObject="

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 70
    const/4 v2, -0x1

    #@20
    .line 83
    :goto_20
    return v2

    #@21
    .line 74
    :cond_21
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    #@24
    move-result-object v1

    #@25
    .line 75
    .local v1, parcel:Landroid/os/Parcel;
    array-length v2, p1

    #@26
    invoke-virtual {v1, p1, v3, v2}, Landroid/os/Parcel;->unmarshall([BII)V

    #@29
    .line 76
    invoke-virtual {v1, v3}, Landroid/os/Parcel;->setDataPosition(I)V

    #@2c
    .line 78
    invoke-interface {v0, v1}, Lcom/lge/ims/JNICoreListener;->onMessage(Landroid/os/Parcel;)V

    #@2f
    .line 81
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    #@32
    .line 83
    const/4 v2, 0x1

    #@33
    goto :goto_20
.end method

.method public static sendEvent(ILandroid/os/Parcel;)I
    .registers 6
    .parameter "nativeObject"
    .parameter "parcel"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 41
    if-nez p0, :cond_10

    #@3
    .line 42
    const-string v2, "JNICore"

    #@5
    const-string v3, "Native object is null"

    #@7
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 44
    if-eqz p1, :cond_f

    #@c
    .line 45
    invoke-virtual {p1}, Landroid/os/Parcel;->recycle()V

    #@f
    .line 62
    :cond_f
    :goto_f
    return v1

    #@10
    .line 51
    :cond_10
    if-nez p1, :cond_1a

    #@12
    .line 52
    const-string v2, "JNICore"

    #@14
    const-string v3, "Parcel is null"

    #@16
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@19
    goto :goto_f

    #@1a
    .line 56
    :cond_1a
    invoke-virtual {p1}, Landroid/os/Parcel;->marshall()[B

    #@1d
    move-result-object v0

    #@1e
    .line 57
    .local v0, buffer:[B
    invoke-virtual {p1}, Landroid/os/Parcel;->recycle()V

    #@21
    .line 58
    const/4 p1, 0x0

    #@22
    .line 60
    const-string v1, "JNICore"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "sendEvent - native object ("

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    const-string v3, "), length ("

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    array-length v3, v0

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    const-string v3, ")"

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    invoke-static {v1, v2}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 62
    invoke-static {p0, v0}, Lcom/lge/ims/JNICore;->sendData(I[B)I

    #@4e
    move-result v1

    #@4f
    goto :goto_f
.end method

.method public static setListener(ILcom/lge/ims/JNICoreListener;)I
    .registers 7
    .parameter "nativeObject"
    .parameter "listener"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 109
    const-string v2, "JNICore"

    #@3
    const-string v3, ""

    #@5
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 111
    if-nez p0, :cond_23

    #@a
    .line 112
    const-string v2, "JNICore"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Illegal argument :: nativeObject="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 132
    :goto_22
    return v1

    #@23
    .line 116
    :cond_23
    if-nez p1, :cond_2d

    #@25
    .line 117
    const-string v2, "JNICore"

    #@27
    const-string v3, "Illegal argument :: listener is null"

    #@29
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@2c
    goto :goto_22

    #@2d
    .line 121
    :cond_2d
    new-instance v0, Ljava/lang/Integer;

    #@2f
    invoke-direct {v0, p0}, Ljava/lang/Integer;-><init>(I)V

    #@32
    .line 123
    .local v0, key:Ljava/lang/Integer;
    if-nez v0, :cond_53

    #@34
    .line 124
    const-string v2, "JNICore"

    #@36
    new-instance v3, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v4, "Instantiating key ("

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    const-string v4, ") failed"

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@52
    goto :goto_22

    #@53
    .line 128
    :cond_53
    sget-object v2, Lcom/lge/ims/JNICore;->mListeners:Ljava/util/Hashtable;

    #@55
    monitor-enter v2

    #@56
    .line 129
    :try_start_56
    sget-object v1, Lcom/lge/ims/JNICore;->mListeners:Ljava/util/Hashtable;

    #@58
    invoke-virtual {v1, v0, p1}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    .line 130
    monitor-exit v2

    #@5c
    .line 132
    const/4 v1, 0x1

    #@5d
    goto :goto_22

    #@5e
    .line 130
    :catchall_5e
    move-exception v1

    #@5f
    monitor-exit v2
    :try_end_60
    .catchall {:try_start_56 .. :try_end_60} :catchall_5e

    #@60
    throw v1
.end method
