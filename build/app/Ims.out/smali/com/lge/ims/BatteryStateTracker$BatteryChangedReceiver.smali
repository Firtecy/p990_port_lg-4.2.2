.class final Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BatteryStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/BatteryStateTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "BatteryChangedReceiver"
.end annotation


# instance fields
.field private mIntentFilter:Landroid/content/IntentFilter;

.field final synthetic this$0:Lcom/lge/ims/BatteryStateTracker;


# direct methods
.method public constructor <init>(Lcom/lge/ims/BatteryStateTracker;)V
    .registers 4
    .parameter

    #@0
    .prologue
    .line 301
    iput-object p1, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    .line 299
    new-instance v0, Landroid/content/IntentFilter;

    #@7
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@a
    iput-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@c
    .line 302
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@e
    if-eqz v0, :cond_17

    #@10
    .line 303
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@12
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    #@14
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 305
    :cond_17
    return-void
.end method


# virtual methods
.method public getFilter()Landroid/content/IntentFilter;
    .registers 2

    #@0
    .prologue
    .line 308
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->mIntentFilter:Landroid/content/IntentFilter;

    #@2
    return-object v0
.end method

.method public declared-synchronized onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 313
    monitor-enter p0

    #@1
    :try_start_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 315
    .local v0, action:Ljava/lang/String;
    const-string v5, "BatteryStateTracker"

    #@7
    new-instance v6, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v7, "BatteryChangedReceiver - "

    #@e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v6

    #@12
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 317
    const-string v5, "android.intent.action.BATTERY_CHANGED"

    #@1f
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v5

    #@23
    if-eqz v5, :cond_b1

    #@25
    .line 318
    const-string v5, "level"

    #@27
    iget-object v6, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@29
    invoke-static {v6}, Lcom/lge/ims/BatteryStateTracker;->access$000(Lcom/lge/ims/BatteryStateTracker;)I

    #@2c
    move-result v6

    #@2d
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@30
    move-result v2

    #@31
    .line 320
    .local v2, level:I
    const-string v5, "BatteryStateTracker"

    #@33
    new-instance v6, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v7, "Battery level="

    #@3a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v6

    #@42
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v6

    #@46
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@49
    .line 322
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@4b
    invoke-static {v5}, Lcom/lge/ims/BatteryStateTracker;->access$000(Lcom/lge/ims/BatteryStateTracker;)I

    #@4e
    move-result v5

    #@4f
    if-gt v2, v5, :cond_b3

    #@51
    .line 323
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@53
    invoke-static {v5}, Lcom/lge/ims/BatteryStateTracker;->access$100(Lcom/lge/ims/BatteryStateTracker;)Z

    #@56
    move-result v5

    #@57
    if-nez v5, :cond_68

    #@59
    .line 324
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@5b
    invoke-static {v5}, Lcom/lge/ims/BatteryStateTracker;->access$200(Lcom/lge/ims/BatteryStateTracker;)Landroid/os/RegistrantList;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual {v5}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@62
    .line 325
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@64
    const/4 v6, 0x1

    #@65
    invoke-static {v5, v6}, Lcom/lge/ims/BatteryStateTracker;->access$102(Lcom/lge/ims/BatteryStateTracker;Z)Z

    #@68
    .line 334
    :cond_68
    :goto_68
    const/16 v5, 0x10

    #@6a
    if-le v2, v5, :cond_b1

    #@6c
    .line 335
    const-string v5, "status"

    #@6e
    const/4 v6, 0x0

    #@6f
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@72
    move-result v4

    #@73
    .line 336
    .local v4, status:I
    const-string v5, "plugged"

    #@75
    const/4 v6, 0x0

    #@76
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@79
    move-result v3

    #@7a
    .line 338
    .local v3, plugged:I
    const-string v5, "BatteryStateTracker"

    #@7c
    new-instance v6, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v7, "Battery status="

    #@83
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v6

    #@87
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v6

    #@8b
    const-string v7, ", plugged="

    #@8d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v6

    #@91
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v6

    #@95
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v6

    #@99
    invoke-static {v5, v6}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@9c
    .line 340
    if-nez v3, :cond_a1

    #@9e
    const/4 v5, 0x2

    #@9f
    if-ne v4, v5, :cond_ce

    #@a1
    .line 341
    :cond_a1
    invoke-static {v2}, Lcom/lge/ims/BatteryStateTracker;->access$400(I)I

    #@a4
    move-result v1

    #@a5
    .line 343
    .local v1, interval:I
    if-eqz v1, :cond_b1

    #@a7
    .line 344
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@a9
    invoke-static {v5}, Lcom/lge/ims/BatteryStateTracker;->access$500(Lcom/lge/ims/BatteryStateTracker;)V

    #@ac
    .line 345
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@ae
    invoke-static {v5, v1}, Lcom/lge/ims/BatteryStateTracker;->access$600(Lcom/lge/ims/BatteryStateTracker;I)V
    :try_end_b1
    .catchall {:try_start_1 .. :try_end_b1} :catchall_cb

    #@b1
    .line 352
    .end local v1           #interval:I
    .end local v2           #level:I
    .end local v3           #plugged:I
    .end local v4           #status:I
    :cond_b1
    :goto_b1
    monitor-exit p0

    #@b2
    return-void

    #@b3
    .line 328
    .restart local v2       #level:I
    :cond_b3
    :try_start_b3
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@b5
    invoke-static {v5}, Lcom/lge/ims/BatteryStateTracker;->access$100(Lcom/lge/ims/BatteryStateTracker;)Z

    #@b8
    move-result v5

    #@b9
    if-eqz v5, :cond_68

    #@bb
    .line 329
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@bd
    invoke-static {v5}, Lcom/lge/ims/BatteryStateTracker;->access$300(Lcom/lge/ims/BatteryStateTracker;)Landroid/os/RegistrantList;

    #@c0
    move-result-object v5

    #@c1
    invoke-virtual {v5}, Landroid/os/RegistrantList;->notifyRegistrants()V

    #@c4
    .line 330
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@c6
    const/4 v6, 0x0

    #@c7
    invoke-static {v5, v6}, Lcom/lge/ims/BatteryStateTracker;->access$102(Lcom/lge/ims/BatteryStateTracker;Z)Z
    :try_end_ca
    .catchall {:try_start_b3 .. :try_end_ca} :catchall_cb

    #@ca
    goto :goto_68

    #@cb
    .line 313
    .end local v0           #action:Ljava/lang/String;
    .end local v2           #level:I
    :catchall_cb
    move-exception v5

    #@cc
    monitor-exit p0

    #@cd
    throw v5

    #@ce
    .line 348
    .restart local v0       #action:Ljava/lang/String;
    .restart local v2       #level:I
    .restart local v3       #plugged:I
    .restart local v4       #status:I
    :cond_ce
    :try_start_ce
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->this$0:Lcom/lge/ims/BatteryStateTracker;

    #@d0
    invoke-static {v5}, Lcom/lge/ims/BatteryStateTracker;->access$500(Lcom/lge/ims/BatteryStateTracker;)V
    :try_end_d3
    .catchall {:try_start_ce .. :try_end_d3} :catchall_cb

    #@d3
    goto :goto_b1
.end method
