.class final Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;
.super Landroid/os/Handler;
.source "DataConnectionManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/DataConnectionManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DataConnectionHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/ims/DataConnectionManager;


# direct methods
.method private constructor <init>(Lcom/lge/ims/DataConnectionManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1496
    iput-object p1, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/lge/ims/DataConnectionManager;Lcom/lge/ims/DataConnectionManager$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1496
    invoke-direct {p0, p1}, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;-><init>(Lcom/lge/ims/DataConnectionManager;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 7
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 1499
    const-string v0, "DataConnectionManager"

    #@4
    new-instance v1, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v2, "DataConnectionHandler - what="

    #@b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    iget v2, p1, Landroid/os/Message;->what:I

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 1501
    iget v0, p1, Landroid/os/Message;->what:I

    #@1e
    sparse-switch v0, :sswitch_data_da

    #@21
    .line 1572
    :cond_21
    :goto_21
    return-void

    #@22
    .line 1503
    :sswitch_22
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@24
    invoke-static {v0, v4}, Lcom/lge/ims/DataConnectionManager;->access$502(Lcom/lge/ims/DataConnectionManager;I)I

    #@27
    .line 1505
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@29
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@2c
    move-result v0

    #@2d
    if-eqz v0, :cond_21

    #@2f
    .line 1506
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@31
    invoke-virtual {v0, v3}, Lcom/lge/ims/DataConnectionManager;->connect(I)Z

    #@34
    goto :goto_21

    #@35
    .line 1511
    :sswitch_35
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@37
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@3a
    move-result v0

    #@3b
    if-eqz v0, :cond_21

    #@3d
    .line 1512
    invoke-static {}, Lcom/lge/ims/PhoneStateTracker;->getInstance()Lcom/lge/ims/PhoneStateTracker;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Lcom/lge/ims/PhoneStateTracker;->getDataServiceState()I

    #@44
    move-result v0

    #@45
    if-nez v0, :cond_21

    #@47
    .line 1513
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@49
    invoke-virtual {v0, v3}, Lcom/lge/ims/DataConnectionManager;->connect(I)Z

    #@4c
    goto :goto_21

    #@4d
    .line 1519
    :sswitch_4d
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@4f
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@52
    move-result v0

    #@53
    if-eqz v0, :cond_21

    #@55
    .line 1520
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@57
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->checkRadioTechnology()Z

    #@5a
    move-result v0

    #@5b
    if-eqz v0, :cond_21

    #@5d
    .line 1521
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@5f
    invoke-virtual {v0, v3}, Lcom/lge/ims/DataConnectionManager;->connect(I)Z

    #@62
    goto :goto_21

    #@63
    .line 1529
    :sswitch_63
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@65
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@68
    move-result v0

    #@69
    if-eqz v0, :cond_21

    #@6b
    .line 1530
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@6d
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->checkRoaming()Z

    #@70
    move-result v0

    #@71
    if-nez v0, :cond_7a

    #@73
    .line 1531
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@75
    const/4 v1, -0x1

    #@76
    invoke-virtual {v0, v3, v1}, Lcom/lge/ims/DataConnectionManager;->disconnect(II)V

    #@79
    goto :goto_21

    #@7a
    .line 1533
    :cond_7a
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@7c
    invoke-virtual {v0, v3}, Lcom/lge/ims/DataConnectionManager;->connect(I)Z

    #@7f
    goto :goto_21

    #@80
    .line 1539
    :sswitch_80
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@82
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->isImsServiceStarted()Z

    #@85
    move-result v0

    #@86
    if-nez v0, :cond_21

    #@88
    .line 1540
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@8a
    invoke-static {v0, v3}, Lcom/lge/ims/DataConnectionManager;->access$1202(Lcom/lge/ims/DataConnectionManager;Z)Z

    #@8d
    .line 1542
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@8f
    invoke-virtual {v0}, Lcom/lge/ims/DataConnectionManager;->isMultipleApnSupported()Z

    #@92
    move-result v0

    #@93
    if-eqz v0, :cond_21

    #@95
    .line 1543
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@97
    invoke-virtual {v0, v3}, Lcom/lge/ims/DataConnectionManager;->connect(I)Z

    #@9a
    goto :goto_21

    #@9b
    .line 1549
    :sswitch_9b
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@9d
    invoke-static {v0}, Lcom/lge/ims/DataConnectionManager;->access$500(Lcom/lge/ims/DataConnectionManager;)I

    #@a0
    move-result v0

    #@a1
    if-ne v0, v3, :cond_21

    #@a3
    .line 1550
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@a5
    invoke-static {v0, v4}, Lcom/lge/ims/DataConnectionManager;->access$502(Lcom/lge/ims/DataConnectionManager;I)I

    #@a8
    .line 1551
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@aa
    invoke-virtual {v0, v3}, Lcom/lge/ims/DataConnectionManager;->connect(I)Z

    #@ad
    goto/16 :goto_21

    #@af
    .line 1556
    :sswitch_af
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@b1
    invoke-static {v0}, Lcom/lge/ims/DataConnectionManager;->access$800(Lcom/lge/ims/DataConnectionManager;)I

    #@b4
    move-result v0

    #@b5
    if-ne v0, v3, :cond_21

    #@b7
    .line 1557
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@b9
    invoke-static {v0, v4}, Lcom/lge/ims/DataConnectionManager;->access$802(Lcom/lge/ims/DataConnectionManager;I)I

    #@bc
    .line 1558
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@be
    const/16 v1, 0x9

    #@c0
    invoke-virtual {v0, v1}, Lcom/lge/ims/DataConnectionManager;->connect(I)Z

    #@c3
    goto/16 :goto_21

    #@c5
    .line 1563
    :sswitch_c5
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@c7
    invoke-static {v0}, Lcom/lge/ims/DataConnectionManager;->access$500(Lcom/lge/ims/DataConnectionManager;)I

    #@ca
    move-result v0

    #@cb
    const/4 v1, 0x3

    #@cc
    if-ne v0, v1, :cond_21

    #@ce
    .line 1564
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@d0
    invoke-static {v0, v4}, Lcom/lge/ims/DataConnectionManager;->access$502(Lcom/lge/ims/DataConnectionManager;I)I

    #@d3
    .line 1565
    iget-object v0, p0, Lcom/lge/ims/DataConnectionManager$DataConnectionHandler;->this$0:Lcom/lge/ims/DataConnectionManager;

    #@d5
    invoke-virtual {v0, v3}, Lcom/lge/ims/DataConnectionManager;->connect(I)Z

    #@d8
    goto/16 :goto_21

    #@da
    .line 1501
    :sswitch_data_da
    .sparse-switch
        0x3e9 -> :sswitch_22
        0x3ea -> :sswitch_35
        0x3eb -> :sswitch_4d
        0x3ec -> :sswitch_63
        0x7d1 -> :sswitch_80
        0x7d2 -> :sswitch_9b
        0x7d3 -> :sswitch_af
        0x7d5 -> :sswitch_c5
    .end sparse-switch
.end method
