.class synthetic Lcom/lge/ims/BootupController$2;
.super Ljava/lang/Object;
.source "BootupController.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/ims/BootupController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$lge$ims$BootupController$STATE:[I

.field static final synthetic $SwitchMap$com$lge$ims$BootupController$STATE_CONFIG:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 1092
    invoke-static {}, Lcom/lge/ims/BootupController$STATE_CONFIG;->values()[Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE_CONFIG:[I

    #@9
    :try_start_9
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE_CONFIG:[I

    #@b
    sget-object v1, Lcom/lge/ims/BootupController$STATE_CONFIG;->INVALID:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@d
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE_CONFIG;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_93

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE_CONFIG:[I

    #@16
    sget-object v1, Lcom/lge/ims/BootupController$STATE_CONFIG;->DIRTY:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@18
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE_CONFIG;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_91

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE_CONFIG:[I

    #@21
    sget-object v1, Lcom/lge/ims/BootupController$STATE_CONFIG;->VALID:Lcom/lge/ims/BootupController$STATE_CONFIG;

    #@23
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE_CONFIG;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_8f

    #@2a
    .line 307
    :goto_2a
    invoke-static {}, Lcom/lge/ims/BootupController$STATE;->values()[Lcom/lge/ims/BootupController$STATE;

    #@2d
    move-result-object v0

    #@2e
    array-length v0, v0

    #@2f
    new-array v0, v0, [I

    #@31
    sput-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@33
    :try_start_33
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@35
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->NONE:Lcom/lge/ims/BootupController$STATE;

    #@37
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@3a
    move-result v1

    #@3b
    const/4 v2, 0x1

    #@3c
    aput v2, v0, v1
    :try_end_3e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_33 .. :try_end_3e} :catch_8d

    #@3e
    :goto_3e
    :try_start_3e
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@40
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WAITING_NETWORK_CONNECTED:Lcom/lge/ims/BootupController$STATE;

    #@42
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@45
    move-result v1

    #@46
    const/4 v2, 0x2

    #@47
    aput v2, v0, v1
    :try_end_49
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3e .. :try_end_49} :catch_8b

    #@49
    :goto_49
    :try_start_49
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@4b
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WAITING_BOOT_COMPLETE:Lcom/lge/ims/BootupController$STATE;

    #@4d
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@50
    move-result v1

    #@51
    const/4 v2, 0x3

    #@52
    aput v2, v0, v1
    :try_end_54
    .catch Ljava/lang/NoSuchFieldError; {:try_start_49 .. :try_end_54} :catch_89

    #@54
    :goto_54
    :try_start_54
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@56
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->UPDATING_AUTOCONFIG:Lcom/lge/ims/BootupController$STATE;

    #@58
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@5b
    move-result v1

    #@5c
    const/4 v2, 0x4

    #@5d
    aput v2, v0, v1
    :try_end_5f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_54 .. :try_end_5f} :catch_87

    #@5f
    :goto_5f
    :try_start_5f
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@61
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WORKING_RCSE:Lcom/lge/ims/BootupController$STATE;

    #@63
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@66
    move-result v1

    #@67
    const/4 v2, 0x5

    #@68
    aput v2, v0, v1
    :try_end_6a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5f .. :try_end_6a} :catch_85

    #@6a
    :goto_6a
    :try_start_6a
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@6c
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WAITING_USER_ACTION:Lcom/lge/ims/BootupController$STATE;

    #@6e
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@71
    move-result v1

    #@72
    const/4 v2, 0x6

    #@73
    aput v2, v0, v1
    :try_end_75
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6a .. :try_end_75} :catch_83

    #@75
    :goto_75
    :try_start_75
    sget-object v0, Lcom/lge/ims/BootupController$2;->$SwitchMap$com$lge$ims$BootupController$STATE:[I

    #@77
    sget-object v1, Lcom/lge/ims/BootupController$STATE;->WAITING_CLEANUP:Lcom/lge/ims/BootupController$STATE;

    #@79
    invoke-virtual {v1}, Lcom/lge/ims/BootupController$STATE;->ordinal()I

    #@7c
    move-result v1

    #@7d
    const/4 v2, 0x7

    #@7e
    aput v2, v0, v1
    :try_end_80
    .catch Ljava/lang/NoSuchFieldError; {:try_start_75 .. :try_end_80} :catch_81

    #@80
    :goto_80
    return-void

    #@81
    :catch_81
    move-exception v0

    #@82
    goto :goto_80

    #@83
    :catch_83
    move-exception v0

    #@84
    goto :goto_75

    #@85
    :catch_85
    move-exception v0

    #@86
    goto :goto_6a

    #@87
    :catch_87
    move-exception v0

    #@88
    goto :goto_5f

    #@89
    :catch_89
    move-exception v0

    #@8a
    goto :goto_54

    #@8b
    :catch_8b
    move-exception v0

    #@8c
    goto :goto_49

    #@8d
    :catch_8d
    move-exception v0

    #@8e
    goto :goto_3e

    #@8f
    .line 1092
    :catch_8f
    move-exception v0

    #@90
    goto :goto_2a

    #@91
    :catch_91
    move-exception v0

    #@92
    goto :goto_1f

    #@93
    :catch_93
    move-exception v0

    #@94
    goto :goto_14
.end method
