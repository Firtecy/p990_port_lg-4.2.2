.class public Lcom/lge/ims/IMSConstants;
.super Ljava/lang/Object;
.source "IMSConstants.java"


# static fields
.field public static final ACTION_CONFIG_UPDATE:Ljava/lang/String; = "com.lge.ims.action.CONFIG_UPDATE"

.field public static final ACTION_IMS_BOOT_COMPLETED:Ljava/lang/String; = "com.lge.ims.action.IMS_BOOT_COMPLETED"

.field public static final ACTION_IMS_STARTED:Ljava/lang/String; = "com.lge.ims.action.IMS_STARTED"

.field public static final ACTION_SVC_STATUS_IND:Ljava/lang/String; = "com.lge.ims.action.SVC_STATUS_IND"

.field public static final ACTION_VT_CALL_ENDED:Ljava/lang/String; = "com.lge.ims.action.VT_ENDED"

.field public static final ACTION_VT_CALL_STARTED:Ljava/lang/String; = "com.lge.ims.action.VT_STARTED"

.field public static final ACTION_VT_DEBUG:Ljava/lang/String; = "com.lge.ims.action.VT_DEBUG"

.field public static final ACTION_VT_INVITATION:Ljava/lang/String; = "com.lge.ims.action.VT_INVITATION"

.field public static final DEBUG_LTE_DATA_NOT_AVAILABLE:I = 0xca

.field public static final DEBUG_LTE_REG_FAILURE:I = 0xc8

.field public static final DEBUG_LTE_REG_FORBIDDEN:I = 0xcb

.field public static final DEBUG_LTE_REG_TXN_TIMEOUT:I = 0xc9

.field public static final DEBUG_LTE_REREG_FAILURE:I = 0xcc

.field public static final DEBUG_LTE_REREG_TXN_TIMEOUT:I = 0xcd

.field public static final DEBUG_WIFI_DATA_NOT_AVAILABLE:I = 0xd4

.field public static final DEBUG_WIFI_REG_FAILURE:I = 0xd2

.field public static final DEBUG_WIFI_REG_FORBIDDEN:I = 0xd5

.field public static final DEBUG_WIFI_REG_TXN_TIMEOUT:I = 0xd3

.field public static final DEBUG_WIFI_REREG_FAILURE:I = 0xd6

.field public static final DEBUG_WIFI_REREG_TXN_TIMEOUT:I = 0xd7

.field public static final EXIT_THREAD:I = -0x1

.field public static final IMS_AIRPLANE_MODE_OFF:I = 0x0

.field public static final IMS_AIRPLANE_MODE_ON:I = 0x1

.field public static final IMS_CONFIG_CAT_0:I = 0x0

.field public static final IMS_CONFIG_CAT_1:I = 0x1

.field public static final IMS_CONFIG_CAT_100:I = 0x64

.field public static final IMS_CONFIG_CAT_10002:I = 0x2712

.field public static final IMS_CONFIG_CAT_102:I = 0x66

.field public static final IMS_CONFIG_CAT_2:I = 0x2

.field public static final IMS_CSCALL_STATE_ACTIVE:I = 0x2

.field public static final IMS_CSCALL_STATE_IDLE:I = 0x0

.field public static final IMS_CSCALL_STATE_INCOMING:I = 0x1

.field public static final IMS_CS_CALL_PROTECTION_OFF:I = 0x0

.field public static final IMS_CS_CALL_PROTECTION_ON:I = 0x1

.field public static final IMS_EVENT_AC_CONFIGURATION_RETRIEVAL:I = 0x70

.field public static final IMS_EVENT_AIRPLANE_MODE:I = 0x1

.field public static final IMS_EVENT_AOS_START:I = 0x10000

.field public static final IMS_EVENT_CONFIG_UPDATE:I = 0x40

.field public static final IMS_EVENT_CONFIG_UPDATE_COMPLETED:I = 0x80000

.field public static final IMS_EVENT_CSCALL_STATE:I = 0x20

.field public static final IMS_EVENT_CS_CALL_PROTECTION:I = 0x100

.field public static final IMS_EVENT_CS_CALL_PROTECTION_SETTING:I = 0x200000

.field public static final IMS_EVENT_DEBUG:I = 0x800

.field public static final IMS_EVENT_DEBUG_STRING:I = 0x3

.field public static final IMS_EVENT_EHPRD_REG_SENDING:I = 0x9

.field public static final IMS_EVENT_LOCK_ORDER_STATE:I = 0x4000

.field public static final IMS_EVENT_LTE_VT_STATE:I = 0x8

.field public static final IMS_EVENT_MEM_DEBUG:I = 0x10000000

.field public static final IMS_EVENT_MOBILE_NETWORK_DISABLE:I = 0x2

.field public static final IMS_EVENT_NATIVE_BOOT_COMPLETED:I = 0x12

.field public static final IMS_EVENT_NETWORK_INFO:I = 0x20000000

.field public static final IMS_EVENT_PHONE_NUMBER_AVAILABLE:I = 0x10

.field public static final IMS_EVENT_POWER_LOW_BATTERY:I = 0x8

.field public static final IMS_EVENT_POWER_OFF:I = 0x4

.field public static final IMS_EVENT_QOS_ACTIVATED:I = 0x0

.field public static final IMS_EVENT_QOS_DEACTIVATED:I = 0x2

.field public static final IMS_EVENT_QOS_MODIFIED:I = 0x1

.field public static final IMS_EVENT_QOS_STATUS:I = 0x100

.field public static final IMS_EVENT_REG_STATE:I = 0x2

.field public static final IMS_EVENT_ROAMING_STATE:I = 0x200

.field public static final IMS_EVENT_SERVICE_SETTING:I = 0x8000

.field public static final IMS_EVENT_SHOW_MESSAGE:I = 0x80

.field public static final IMS_EVENT_VOLTE_CALL_SETTING:I = 0x400000

.field public static final IMS_EVENT_VOLTE_INDICATOR:I = 0x6

.field public static final IMS_EVENT_VT_CALL:I = 0x4

.field public static final IMS_EVENT_VT_INDICATOR:I = 0x5

.field public static final IMS_EVENT_WAKE_LOCK:I = 0x13

.field public static final IMS_EVENT_WIFI_LOCK:I = 0x1

.field public static final IMS_EVENT_WIFI_OFF:I = 0x2000

.field public static final IMS_EVENT_WIFI_OFF_REPLY:I = 0x7

.field public static final IMS_EVENT_WIFI_RSSI_VALUE:I = 0x800

.field public static final IMS_EVENT_WIFI_SERVICE:I = 0x10000000

.field public static final IMS_LOCK_ORDER_STATE_OFF:I = 0x0

.field public static final IMS_LOCK_ORDER_STATE_ON:I = 0x1

.field public static final IMS_LTE_VT_STATE_ACTIVE:I = 0x1

.field public static final IMS_LTE_VT_STATE_INACTIVE:I = 0x0

.field public static final IMS_MEM_DEBUG_ENABLE_FILE_WRITE:I = 0x0

.field public static final IMS_MEM_DEBUG_ENABLE_FILE_WRITE_OFF:I = 0x0

.field public static final IMS_MEM_DEBUG_ENABLE_FILE_WRITE_ON:I = 0x1

.field public static final IMS_MEM_DEBUG_PRINT_HEAP_LEAKAGE:I = 0x1

.field public static final IMS_MESSAGE_REGISTRAION_FAILED:I = 0x1

.field public static final IMS_MESSAGE_SERVICE_NOT_PROVISIONED:I = 0x2

.field public static final IMS_NET_LTE:I = 0x1

.field public static final IMS_NET_WIFI:I = 0x2

.field public static final IMS_POWER_LOW_BATTERY:I = 0x0

.field public static final IMS_POWER_LOW_CHANGED:I = 0x1

.field public static final IMS_REG_OFF:I = 0x0

.field public static final IMS_REG_ON:I = 0x1

.field public static final IMS_ROAMING_STATE_OFF:I = 0x0

.field public static final IMS_ROAMING_STATE_ON:I = 0x1

.field public static final IMS_SERVICE_OFF:I = 0x0

.field public static final IMS_SERVICE_ON:I = 0x1

.field public static final IMS_SERVICE_PRESENTITY:I = 0x2

.field public static final IMS_VOLTE_SETTING_BLOCKOFF:I = 0x1

.field public static final IMS_VOLTE_SETTING_BLOCKON:I = 0x0

.field public static final IMS_VT_CALL_ENDED:I = 0x1

.field public static final IMS_VT_CALL_STARTED:I = 0x0

.field public static final IMS_VT_INDICATOR_OFF:I = 0x0

.field public static final IMS_VT_INDICATOR_ON:I = 0x1

.field public static final IMS_VT_LTE:I = 0x1

.field public static final IMS_VT_OTHER:I = 0x0

.field public static final IMS_WIFI_LOCK_ACQUIRE:I = 0x1

.field public static final IMS_WIFI_LOCK_RELEASE:I = 0x2

.field public static final IMS_WIFI_LOCK_TYPE_FMC:I = 0x2

.field public static final IMS_WIFI_LOCK_TYPE_VT:I = 0x1

.field public static final IMS_WIFI_OFF:I = 0x0

.field public static final IMS_WIFI_ON:I = 0x1


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 15
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
