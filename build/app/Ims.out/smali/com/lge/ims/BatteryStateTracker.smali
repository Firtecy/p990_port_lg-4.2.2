.class public Lcom/lge/ims/BatteryStateTracker;
.super Ljava/lang/Object;
.source "BatteryStateTracker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverUninstaller;,
        Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverInstaller;,
        Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;,
        Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_BATTERY_POLLING_TIMER:Ljava/lang/String; = "com.lge.ims.action.BATTERY_POLLING_TIMER"

.field private static final DEFAULT_BATTERY_LOW:I = 0x2

.field private static final DEFAULT_GUARD_LEVEL:I = 0x3

.field private static final HANDLER_POST_DELAY:I = 0x64

.field public static final LOW_BATTERY_THRESHOLD:I = 0xf

.field private static final TAG:Ljava/lang/String; = "BatteryStateTracker"

.field private static mBSTracker:Lcom/lge/ims/BatteryStateTracker;


# instance fields
.field private mBatteryChangedReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

.field private mBatteryChangedReceiverInstalled:Z

.field private mBatteryLowRegistrants:Landroid/os/RegistrantList;

.field private mBatteryLowThreshold:I

.field private mBatteryOkayRegistrants:Landroid/os/RegistrantList;

.field private mBatteryStateReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mLowBatteryNotified:Z

.field private mPollingTimerStarted:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 30
    new-instance v0, Lcom/lge/ims/BatteryStateTracker;

    #@2
    invoke-direct {v0}, Lcom/lge/ims/BatteryStateTracker;-><init>()V

    #@5
    sput-object v0, Lcom/lge/ims/BatteryStateTracker;->mBSTracker:Lcom/lge/ims/BatteryStateTracker;

    #@7
    return-void
.end method

.method private constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 46
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 32
    const/4 v0, 0x0

    #@5
    iput-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@7
    .line 33
    new-instance v0, Landroid/os/Handler;

    #@9
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@c
    iput-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mHandler:Landroid/os/Handler;

    #@e
    .line 34
    new-instance v0, Landroid/os/RegistrantList;

    #@10
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@13
    iput-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryOkayRegistrants:Landroid/os/RegistrantList;

    #@15
    .line 35
    new-instance v0, Landroid/os/RegistrantList;

    #@17
    invoke-direct {v0}, Landroid/os/RegistrantList;-><init>()V

    #@1a
    iput-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryLowRegistrants:Landroid/os/RegistrantList;

    #@1c
    .line 36
    new-instance v0, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;

    #@1e
    invoke-direct {v0, p0}, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;-><init>(Lcom/lge/ims/BatteryStateTracker;)V

    #@21
    iput-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryStateReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;

    #@23
    .line 37
    new-instance v0, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@25
    invoke-direct {v0, p0}, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;-><init>(Lcom/lge/ims/BatteryStateTracker;)V

    #@28
    iput-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@2a
    .line 39
    iput-boolean v1, p0, Lcom/lge/ims/BatteryStateTracker;->mLowBatteryNotified:Z

    #@2c
    .line 40
    iput-boolean v1, p0, Lcom/lge/ims/BatteryStateTracker;->mPollingTimerStarted:Z

    #@2e
    .line 41
    iput-boolean v1, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiverInstalled:Z

    #@30
    .line 42
    const/4 v0, 0x2

    #@31
    iput v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryLowThreshold:I

    #@33
    .line 47
    const-string v0, "BatteryStateTracker"

    #@35
    const-string v1, "BatteryStateTracker is created"

    #@37
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@3a
    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/lge/ims/BatteryStateTracker;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryLowThreshold:I

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/lge/ims/BatteryStateTracker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/lge/ims/BatteryStateTracker;->mLowBatteryNotified:Z

    #@2
    return v0
.end method

.method static synthetic access$1000(Lcom/lge/ims/BatteryStateTracker;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/lge/ims/BatteryStateTracker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/lge/ims/BatteryStateTracker;->mLowBatteryNotified:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/lge/ims/BatteryStateTracker;)Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/lge/ims/BatteryStateTracker;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryLowRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/lge/ims/BatteryStateTracker;)Landroid/os/RegistrantList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryOkayRegistrants:Landroid/os/RegistrantList;

    #@2
    return-object v0
.end method

.method static synthetic access$400(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 17
    invoke-static {p0}, Lcom/lge/ims/BatteryStateTracker;->getPollingInterval(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Lcom/lge/ims/BatteryStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Lcom/lge/ims/BatteryStateTracker;->uninstallBatteryChangedReceiver()V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/lge/ims/BatteryStateTracker;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/lge/ims/BatteryStateTracker;->startPollingTimer(I)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/lge/ims/BatteryStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Lcom/lge/ims/BatteryStateTracker;->installBatteryChangedReceiver()V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/lge/ims/BatteryStateTracker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 17
    invoke-direct {p0}, Lcom/lge/ims/BatteryStateTracker;->stopPollingTimer()V

    #@3
    return-void
.end method

.method static synthetic access$902(Lcom/lge/ims/BatteryStateTracker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/lge/ims/BatteryStateTracker;->mPollingTimerStarted:Z

    #@2
    return p1
.end method

.method public static getInstance()Lcom/lge/ims/BatteryStateTracker;
    .registers 1

    #@0
    .prologue
    .line 51
    sget-object v0, Lcom/lge/ims/BatteryStateTracker;->mBSTracker:Lcom/lge/ims/BatteryStateTracker;

    #@2
    return-object v0
.end method

.method private static getPollingInterval(I)I
    .registers 3
    .parameter "level"

    #@0
    .prologue
    const v0, 0x36ee80

    #@3
    .line 277
    const/16 v1, 0x10

    #@5
    if-gt p0, v1, :cond_9

    #@7
    .line 279
    const/4 v0, 0x0

    #@8
    .line 294
    :cond_8
    :goto_8
    return v0

    #@9
    .line 280
    :cond_9
    const/16 v1, 0x14

    #@b
    if-ge p0, v1, :cond_11

    #@d
    .line 282
    const v0, 0x927c0

    #@10
    goto :goto_8

    #@11
    .line 283
    :cond_11
    const/16 v1, 0x1e

    #@13
    if-ge p0, v1, :cond_19

    #@15
    .line 285
    const v0, 0x124f80

    #@18
    goto :goto_8

    #@19
    .line 286
    :cond_19
    const/16 v1, 0x28

    #@1b
    if-ge p0, v1, :cond_21

    #@1d
    .line 288
    const v0, 0x1b7740

    #@20
    goto :goto_8

    #@21
    .line 289
    :cond_21
    const/16 v1, 0x32

    #@23
    if-ge p0, v1, :cond_8

    #@25
    goto :goto_8
.end method

.method private installBatteryChangedReceiver()V
    .registers 5

    #@0
    .prologue
    .line 192
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mHandler:Landroid/os/Handler;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 204
    :goto_4
    return-void

    #@5
    .line 196
    :cond_5
    iget-boolean v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiverInstalled:Z

    #@7
    if-nez v0, :cond_1c

    #@9
    .line 197
    const/4 v0, 0x1

    #@a
    iput-boolean v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiverInstalled:Z

    #@c
    .line 198
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mHandler:Landroid/os/Handler;

    #@e
    new-instance v1, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverInstaller;

    #@10
    invoke-direct {v1, p0}, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverInstaller;-><init>(Lcom/lge/ims/BatteryStateTracker;)V

    #@13
    const-wide/16 v2, 0x64

    #@15
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@18
    .line 200
    invoke-direct {p0}, Lcom/lge/ims/BatteryStateTracker;->stopPollingTimer()V

    #@1b
    goto :goto_4

    #@1c
    .line 202
    :cond_1c
    const-string v0, "BatteryStateTracker"

    #@1e
    const-string v1, "BatteryChangedReceiver is already installed"

    #@20
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@23
    goto :goto_4
.end method

.method private startPollingTimer(I)V
    .registers 10
    .parameter "interval"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 220
    iget-boolean v3, p0, Lcom/lge/ims/BatteryStateTracker;->mPollingTimerStarted:Z

    #@3
    if-eqz v3, :cond_d

    #@5
    .line 221
    const-string v3, "BatteryStateTracker"

    #@7
    const-string v4, "Polling timer is already started"

    #@9
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 245
    :goto_c
    return-void

    #@d
    .line 225
    :cond_d
    const/4 v3, 0x1

    #@e
    iput-boolean v3, p0, Lcom/lge/ims/BatteryStateTracker;->mPollingTimerStarted:Z

    #@10
    .line 227
    iget-object v3, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@12
    if-nez v3, :cond_1c

    #@14
    .line 228
    const-string v3, "BatteryStateTracker"

    #@16
    const-string v4, "Context is null"

    #@18
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1b
    goto :goto_c

    #@1c
    .line 232
    :cond_1c
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getAlarmManager()Landroid/app/AlarmManager;

    #@1f
    move-result-object v0

    #@20
    .line 234
    .local v0, am:Landroid/app/AlarmManager;
    if-nez v0, :cond_2a

    #@22
    .line 235
    const-string v3, "BatteryStateTracker"

    #@24
    const-string v4, "AlarmManager is null"

    #@26
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    goto :goto_c

    #@2a
    .line 239
    :cond_2a
    new-instance v1, Landroid/content/Intent;

    #@2c
    const-string v3, "com.lge.ims.action.BATTERY_POLLING_TIMER"

    #@2e
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@31
    .line 240
    .local v1, intent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@33
    const/16 v4, 0x64

    #@35
    invoke-static {v3, v4, v1, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@38
    move-result-object v2

    #@39
    .line 242
    .local v2, sender:Landroid/app/PendingIntent;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@3c
    move-result-wide v3

    #@3d
    int-to-long v5, p1

    #@3e
    add-long/2addr v3, v5

    #@3f
    invoke-virtual {v0, v7, v3, v4, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    #@42
    .line 244
    const-string v3, "BatteryStateTracker"

    #@44
    new-instance v4, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v5, "Polling timer is started - interval="

    #@4b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v4

    #@57
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@5a
    goto :goto_c
.end method

.method private stopPollingTimer()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 248
    iget-boolean v3, p0, Lcom/lge/ims/BatteryStateTracker;->mPollingTimerStarted:Z

    #@3
    if-nez v3, :cond_d

    #@5
    .line 249
    const-string v3, "BatteryStateTracker"

    #@7
    const-string v4, "Polling timer is already stopped"

    #@9
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@c
    .line 273
    :goto_c
    return-void

    #@d
    .line 253
    :cond_d
    iput-boolean v5, p0, Lcom/lge/ims/BatteryStateTracker;->mPollingTimerStarted:Z

    #@f
    .line 255
    iget-object v3, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@11
    if-nez v3, :cond_1b

    #@13
    .line 256
    const-string v3, "BatteryStateTracker"

    #@15
    const-string v4, "Context is null"

    #@17
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1a
    goto :goto_c

    #@1b
    .line 260
    :cond_1b
    invoke-static {}, Lcom/lge/ims/SystemServiceManager;->getAlarmManager()Landroid/app/AlarmManager;

    #@1e
    move-result-object v0

    #@1f
    .line 262
    .local v0, am:Landroid/app/AlarmManager;
    if-nez v0, :cond_29

    #@21
    .line 263
    const-string v3, "BatteryStateTracker"

    #@23
    const-string v4, "AlarmManager is null"

    #@25
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    goto :goto_c

    #@29
    .line 267
    :cond_29
    new-instance v1, Landroid/content/Intent;

    #@2b
    const-string v3, "com.lge.ims.action.BATTERY_POLLING_TIMER"

    #@2d
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@30
    .line 268
    .local v1, intent:Landroid/content/Intent;
    iget-object v3, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@32
    const/16 v4, 0x64

    #@34
    invoke-static {v3, v4, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@37
    move-result-object v2

    #@38
    .line 270
    .local v2, sender:Landroid/app/PendingIntent;
    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    #@3b
    .line 272
    const-string v3, "BatteryStateTracker"

    #@3d
    const-string v4, "Polling timer is stopped"

    #@3f
    invoke-static {v3, v4}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@42
    goto :goto_c
.end method

.method private uninstallBatteryChangedReceiver()V
    .registers 5

    #@0
    .prologue
    .line 207
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mHandler:Landroid/os/Handler;

    #@2
    if-nez v0, :cond_5

    #@4
    .line 217
    :goto_4
    return-void

    #@5
    .line 211
    :cond_5
    iget-boolean v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiverInstalled:Z

    #@7
    if-eqz v0, :cond_19

    #@9
    .line 212
    const/4 v0, 0x0

    #@a
    iput-boolean v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiverInstalled:Z

    #@c
    .line 213
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mHandler:Landroid/os/Handler;

    #@e
    new-instance v1, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverUninstaller;

    #@10
    invoke-direct {v1, p0}, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiverUninstaller;-><init>(Lcom/lge/ims/BatteryStateTracker;)V

    #@13
    const-wide/16 v2, 0x64

    #@15
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@18
    goto :goto_4

    #@19
    .line 215
    :cond_19
    const-string v0, "BatteryStateTracker"

    #@1b
    const-string v1, "BatteryChangedReceiver is already uninstalled"

    #@1d
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    goto :goto_4
.end method


# virtual methods
.method public destroy()V
    .registers 3

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@2
    if-eqz v0, :cond_1c

    #@4
    .line 56
    iget-boolean v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiverInstalled:Z

    #@6
    if-eqz v0, :cond_12

    #@8
    .line 57
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@a
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 58
    const/4 v0, 0x0

    #@10
    iput-boolean v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiverInstalled:Z

    #@12
    .line 61
    :cond_12
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@14
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryStateReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;

    #@16
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@19
    .line 63
    invoke-direct {p0}, Lcom/lge/ims/BatteryStateTracker;->stopPollingTimer()V

    #@1c
    .line 65
    :cond_1c
    return-void
.end method

.method public getBatteryLevel()I
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v0, -0x1

    #@2
    .line 68
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@4
    if-nez v2, :cond_e

    #@6
    .line 69
    const-string v2, "BatteryStateTracker"

    #@8
    const-string v3, "Context is null"

    #@a
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 90
    :goto_d
    return v0

    #@e
    .line 73
    :cond_e
    const/4 v1, 0x0

    #@f
    .line 75
    .local v1, stickyIntent:Landroid/content/Intent;
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@11
    if-eqz v2, :cond_29

    #@13
    .line 76
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@15
    iget-object v3, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@17
    invoke-virtual {v3}, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->getFilter()Landroid/content/IntentFilter;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1e
    move-result-object v1

    #@1f
    .line 81
    :goto_1f
    if-nez v1, :cond_37

    #@21
    .line 82
    const-string v2, "BatteryStateTracker"

    #@23
    const-string v3, "Sticky intent is null"

    #@25
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    goto :goto_d

    #@29
    .line 78
    :cond_29
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@2b
    new-instance v3, Landroid/content/IntentFilter;

    #@2d
    const-string v4, "android.intent.action.BATTERY_CHANGED"

    #@2f
    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@32
    invoke-virtual {v2, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@35
    move-result-object v1

    #@36
    goto :goto_1f

    #@37
    .line 86
    :cond_37
    const-string v2, "level"

    #@39
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@3c
    move-result v0

    #@3d
    .line 88
    .local v0, level:I
    const-string v2, "BatteryStateTracker"

    #@3f
    new-instance v3, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v4, "Battery level="

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v3

    #@52
    invoke-static {v2, v3}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    goto :goto_d
.end method

.method public getBatteryLowThreshold()I
    .registers 2

    #@0
    .prologue
    .line 94
    iget v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryLowThreshold:I

    #@2
    return v0
.end method

.method public isPowerPlugged(Z)Z
    .registers 10
    .parameter "log"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 98
    iget-object v4, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@4
    if-nez v4, :cond_e

    #@6
    .line 99
    const-string v4, "BatteryStateTracker"

    #@8
    const-string v5, "Context is null"

    #@a
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@d
    .line 123
    :cond_d
    :goto_d
    return v3

    #@e
    .line 103
    :cond_e
    const/4 v2, 0x0

    #@f
    .line 105
    .local v2, stickyIntent:Landroid/content/Intent;
    iget-object v4, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@11
    if-eqz v4, :cond_29

    #@13
    .line 106
    iget-object v4, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@15
    iget-object v5, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@17
    invoke-virtual {v5}, Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;->getFilter()Landroid/content/IntentFilter;

    #@1a
    move-result-object v5

    #@1b
    invoke-virtual {v4, v7, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1e
    move-result-object v2

    #@1f
    .line 111
    :goto_1f
    if-nez v2, :cond_37

    #@21
    .line 112
    const-string v4, "BatteryStateTracker"

    #@23
    const-string v5, "Sticky intent is null"

    #@25
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@28
    goto :goto_d

    #@29
    .line 108
    :cond_29
    iget-object v4, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@2b
    new-instance v5, Landroid/content/IntentFilter;

    #@2d
    const-string v6, "android.intent.action.BATTERY_CHANGED"

    #@2f
    invoke-direct {v5, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@32
    invoke-virtual {v4, v7, v5}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@35
    move-result-object v2

    #@36
    goto :goto_1f

    #@37
    .line 116
    :cond_37
    const-string v4, "status"

    #@39
    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@3c
    move-result v1

    #@3d
    .line 117
    .local v1, status:I
    const-string v4, "plugged"

    #@3f
    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@42
    move-result v0

    #@43
    .line 119
    .local v0, plugged:I
    if-eqz p1, :cond_67

    #@45
    .line 120
    const-string v4, "BatteryStateTracker"

    #@47
    new-instance v5, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v6, "Battery status="

    #@4e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    const-string v6, ", plugged="

    #@58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v5

    #@64
    invoke-static {v4, v5}, Lcom/lge/ims/ImsLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@67
    .line 123
    :cond_67
    if-nez v0, :cond_6c

    #@69
    const/4 v4, 0x2

    #@6a
    if-ne v1, v4, :cond_d

    #@6c
    :cond_6c
    const/4 v3, 0x1

    #@6d
    goto :goto_d
.end method

.method public registerForBatteryLow(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 136
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryLowRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 137
    return-void
.end method

.method public registerForBatteryOkay(Landroid/os/Handler;ILjava/lang/Object;)V
    .registers 6
    .parameter "h"
    .parameter "what"
    .parameter "obj"

    #@0
    .prologue
    .line 144
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryOkayRegistrants:Landroid/os/RegistrantList;

    #@2
    new-instance v1, Landroid/os/Registrant;

    #@4
    invoke-direct {v1, p1, p2, p3}, Landroid/os/Registrant;-><init>(Landroid/os/Handler;ILjava/lang/Object;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/RegistrantList;->add(Landroid/os/Registrant;)V

    #@a
    .line 145
    return-void
.end method

.method public setBatteryLowThreshold(I)V
    .registers 5
    .parameter "threshold"

    #@0
    .prologue
    .line 127
    const/16 v0, 0xf

    #@2
    if-le p1, v0, :cond_1d

    #@4
    .line 128
    const-string v0, "BatteryStateTracker"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Low battery threshold MUST be less than a default value (15) - Threshold="

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Lcom/lge/ims/ImsLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    #@1c
    .line 133
    :goto_1c
    return-void

    #@1d
    .line 132
    :cond_1d
    iput p1, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryLowThreshold:I

    #@1f
    goto :goto_1c
.end method

.method public setContext(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 152
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@2
    if-eq v1, p1, :cond_26

    #@4
    .line 154
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@6
    if-eqz v1, :cond_20

    #@8
    .line 155
    iget-boolean v1, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiverInstalled:Z

    #@a
    if-eqz v1, :cond_16

    #@c
    .line 156
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@e
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryChangedReceiver;

    #@10
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@13
    .line 157
    const/4 v1, 0x0

    #@14
    iput-boolean v1, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryChangedReceiverInstalled:Z

    #@16
    .line 160
    :cond_16
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@18
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryStateReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;

    #@1a
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@1d
    .line 162
    invoke-direct {p0}, Lcom/lge/ims/BatteryStateTracker;->stopPollingTimer()V

    #@20
    .line 165
    :cond_20
    iput-object p1, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@22
    .line 167
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@24
    if-nez v1, :cond_27

    #@26
    .line 189
    :cond_26
    :goto_26
    return-void

    #@27
    .line 171
    :cond_27
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryStateReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;

    #@29
    if-eqz v1, :cond_38

    #@2b
    .line 172
    iget-object v1, p0, Lcom/lge/ims/BatteryStateTracker;->mContext:Landroid/content/Context;

    #@2d
    iget-object v2, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryStateReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;

    #@2f
    iget-object v3, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryStateReceiver:Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;

    #@31
    invoke-virtual {v3}, Lcom/lge/ims/BatteryStateTracker$BatteryStateReceiver;->getFilter()Landroid/content/IntentFilter;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@38
    .line 175
    :cond_38
    const/4 v1, 0x1

    #@39
    invoke-virtual {p0, v1}, Lcom/lge/ims/BatteryStateTracker;->isPowerPlugged(Z)Z

    #@3c
    move-result v1

    #@3d
    if-eqz v1, :cond_51

    #@3f
    .line 176
    invoke-virtual {p0}, Lcom/lge/ims/BatteryStateTracker;->getBatteryLevel()I

    #@42
    move-result v1

    #@43
    invoke-static {v1}, Lcom/lge/ims/BatteryStateTracker;->getPollingInterval(I)I

    #@46
    move-result v0

    #@47
    .line 178
    .local v0, interval:I
    if-nez v0, :cond_4d

    #@49
    .line 179
    invoke-direct {p0}, Lcom/lge/ims/BatteryStateTracker;->installBatteryChangedReceiver()V

    #@4c
    goto :goto_26

    #@4d
    .line 181
    :cond_4d
    invoke-direct {p0, v0}, Lcom/lge/ims/BatteryStateTracker;->startPollingTimer(I)V

    #@50
    goto :goto_26

    #@51
    .line 184
    .end local v0           #interval:I
    :cond_51
    invoke-virtual {p0}, Lcom/lge/ims/BatteryStateTracker;->getBatteryLevel()I

    #@54
    move-result v1

    #@55
    const/16 v2, 0x10

    #@57
    if-gt v1, v2, :cond_26

    #@59
    .line 185
    invoke-direct {p0}, Lcom/lge/ims/BatteryStateTracker;->installBatteryChangedReceiver()V

    #@5c
    goto :goto_26
.end method

.method public unregisterForBatteryLow(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 140
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryLowRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 141
    return-void
.end method

.method public unregisterForBatteryOkay(Landroid/os/Handler;)V
    .registers 3
    .parameter "h"

    #@0
    .prologue
    .line 148
    iget-object v0, p0, Lcom/lge/ims/BatteryStateTracker;->mBatteryOkayRegistrants:Landroid/os/RegistrantList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RegistrantList;->remove(Landroid/os/Handler;)V

    #@5
    .line 149
    return-void
.end method
