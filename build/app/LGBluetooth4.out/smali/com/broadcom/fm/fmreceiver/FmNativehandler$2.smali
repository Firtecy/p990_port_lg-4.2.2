.class Lcom/broadcom/fm/fmreceiver/FmNativehandler$2;
.super Landroid/content/BroadcastReceiver;
.source "FmNativehandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmNativehandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;


# direct methods
.method constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 893
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$2;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 896
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 897
    .local v0, action:Ljava/lang/String;
    const-string v3, "FmNativehandler"

    #@6
    invoke-static {v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 905
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@b
    if-eqz v3, :cond_23

    #@d
    const-string v3, "android.intent.action.PACKAGE_REMOVED"

    #@f
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v3

    #@13
    if-nez v3, :cond_1d

    #@15
    const-string v3, "android.intent.action.PACKAGE_RESTARTED"

    #@17
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_23

    #@1d
    .line 910
    :cond_1d
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@20
    move-result-object v2

    #@21
    .line 911
    .local v2, uri:Landroid/net/Uri;
    if-nez v2, :cond_24

    #@23
    .line 925
    .end local v2           #uri:Landroid/net/Uri;
    :cond_23
    :goto_23
    return-void

    #@24
    .line 914
    .restart local v2       #uri:Landroid/net/Uri;
    :cond_24
    invoke-virtual {v2}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    .line 915
    .local v1, pkgName:Ljava/lang/String;
    if-eqz v1, :cond_23

    #@2a
    .line 918
    const-string v3, "FmNativehandler"

    #@2c
    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 921
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$2;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@31
    invoke-static {v3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$2600(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v3

    #@39
    if-eqz v3, :cond_23

    #@3b
    .line 922
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$2;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@3d
    invoke-virtual {v3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->turnOffRadio()I

    #@40
    goto :goto_23
.end method
