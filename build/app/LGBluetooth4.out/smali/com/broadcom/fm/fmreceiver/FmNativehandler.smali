.class public final Lcom/broadcom/fm/fmreceiver/FmNativehandler;
.super Ljava/lang/Object;
.source "FmNativehandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;,
        Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;,
        Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;,
        Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;,
        Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FmNativehandler"

.field private static final V:Z = true

.field private static instance:I


# instance fields
.field private FMQueue:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;",
            ">;"
        }
    .end annotation
.end field

.field private current_CMD:I

.field private final mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mClientName:Ljava/lang/String;

.field protected mContext:Landroid/content/Context;

.field mFunctionalityMask:I

.field private mIntentRadioState:Landroid/content/BroadcastReceiver;

.field private mIntentReceiver:Landroid/content/BroadcastReceiver;

.field protected mIsFinish:Z

.field protected mIsStarted:Z

.field protected operationHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 529
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->classInitNative()V

    #@3
    .line 829
    const/4 v0, 0x0

    #@4
    sput v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->instance:I

    #@6
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "context"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 86
    new-instance v0, Landroid/os/RemoteCallbackList;

    #@6
    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    #@9
    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@b
    .line 253
    new-instance v0, Ljava/util/LinkedList;

    #@d
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@10
    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@12
    .line 254
    const/4 v0, -0x1

    #@13
    iput v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->current_CMD:I

    #@15
    .line 532
    iput-boolean v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIsStarted:Z

    #@17
    .line 533
    iput-boolean v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIsFinish:Z

    #@19
    .line 697
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;

    #@1b
    invoke-direct {v0, p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)V

    #@1e
    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@20
    .line 893
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$2;

    #@22
    invoke-direct {v0, p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$2;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)V

    #@25
    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@27
    .line 929
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$3;

    #@29
    invoke-direct {v0, p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$3;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)V

    #@2c
    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIntentRadioState:Landroid/content/BroadcastReceiver;

    #@2e
    .line 537
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mContext:Landroid/content/Context;

    #@30
    .line 538
    return-void
.end method

.method static synthetic access$000(Lcom/broadcom/fm/fmreceiver/FmNativehandler;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->logTimeOut(I)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 80
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->initializeStateMachine()V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/broadcom/fm/fmreceiver/FmNativehandler;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendRdsModeEventCallback(II)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IILjava/lang/String;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1, p2, p3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendRdsDataEventCallback(IILjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/broadcom/fm/fmreceiver/FmNativehandler;Z)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->disableFmNative(Z)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2000(Lcom/broadcom/fm/fmreceiver/FmNativehandler;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendAudioModeEventCallback(I)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/broadcom/fm/fmreceiver/FmNativehandler;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendAudioPathEventCallback(I)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Lcom/broadcom/fm/fmreceiver/FmNativehandler;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendWorldRegionEventCallback(I)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Lcom/broadcom/fm/fmreceiver/FmNativehandler;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendEstimateNflEventCallback(I)V

    #@3
    return-void
.end method

.method static synthetic access$2400(Lcom/broadcom/fm/fmreceiver/FmNativehandler;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendLiveAudioQualityEventCallback(II)V

    #@3
    return-void
.end method

.method static synthetic access$2500(Lcom/broadcom/fm/fmreceiver/FmNativehandler;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendVolumeEventCallback(II)V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 80
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mClientName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$2700(Lcom/broadcom/fm/fmreceiver/FmNativehandler;Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/broadcom/fm/fmreceiver/FmNativehandler;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendStatusEventCallbackFromLocalStore(Z)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IIIZI)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 80
    invoke-direct/range {p0 .. p5}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendSeekCompleteEventCallback(IIIZI)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .registers 11
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"
    .parameter "x8"
    .parameter "x9"
    .parameter "x10"

    #@0
    .prologue
    .line 80
    invoke-direct/range {p0 .. p10}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendStatusEventCallback(IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    #@3
    return-void
.end method

.method private static classInitNative()V
	.registers 0
	return-void
.end method

.method private cleanQueue(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    .registers 11
    .parameter "job"

    #@0
    .prologue
    const-wide/16 v7, 0x0

    #@2
    .line 258
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5
    move-result-wide v2

    #@6
    .line 259
    .local v2, now:J
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@8
    invoke-virtual {v4}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@b
    move-result-object v1

    #@c
    .line 260
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;>;"
    if-eqz p1, :cond_8d

    #@e
    .line 261
    :cond_e
    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v4

    #@12
    if-eqz v4, :cond_8d

    #@14
    .line 262
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@1a
    .line 265
    .local v0, existingJob:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;
    iget-object v4, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@1c
    sget-object v5, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@1e
    if-ne v4, v5, :cond_43

    #@20
    .line 266
    iget-wide v4, v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@22
    cmp-long v4, v4, v7

    #@24
    if-nez v4, :cond_66

    #@26
    .line 267
    const-string v4, "FmNativehandler"

    #@28
    new-instance v5, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v6, "Removed because of a FM off request. "

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 268
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@41
    .line 269
    const/4 v0, 0x0

    #@42
    .line 270
    goto :goto_e

    #@43
    .line 273
    :cond_43
    iget-object v4, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@45
    sget-object v5, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION_ABORT:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@47
    if-ne v4, v5, :cond_66

    #@49
    .line 274
    const-string v4, "FmNativehandler"

    #@4b
    new-instance v5, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v6, "Removed because of an abort request. "

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v5

    #@5e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 275
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@64
    .line 276
    const/4 v0, 0x0

    #@65
    .line 277
    goto :goto_e

    #@66
    .line 280
    :cond_66
    iget-wide v4, v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@68
    cmp-long v4, v4, v7

    #@6a
    if-eqz v4, :cond_e

    #@6c
    .line 281
    const-string v4, "FmNativehandler"

    #@6e
    new-instance v5, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v6, "****** ***** Sent. So remove Job:"

    #@75
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v5

    #@79
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->toString()Ljava/lang/String;

    #@7c
    move-result-object v6

    #@7d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v5

    #@81
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v5

    #@85
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 282
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@8b
    .line 283
    const/4 v0, 0x0

    #@8c
    .line 284
    goto :goto_e

    #@8d
    .line 289
    .end local v0           #existingJob:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;
    :cond_8d
    return-void
.end method

.method private cleanupNative()V
	.registers 1
	return-void
.end method

.method private comboSearchNative(IIIIIZII)Z
	.registers 10
	const/4 v0, 0x0
	return v0
.end method

.method private configureDeemphasisNative(I)Z
	.registers 3
	const/4 v0, 0x0
	return v0
.end method

.method private configureSignalNotificationNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private disableFmNative(Z)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private enableFmNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private estimateNoiseFloorNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private fetchNextJob()V
    .registers 6

    #@0
    .prologue
    .line 450
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@2
    monitor-enter v2

    #@3
    .line 451
    :try_start_3
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@5
    invoke-virtual {v1}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@b
    .line 452
    .local v0, job:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;
    if-nez v0, :cond_f

    #@d
    .line 453
    monitor-exit v2

    #@e
    .line 461
    :goto_e
    return-void

    #@f
    .line 455
    :cond_f
    const-string v1, "FmNativehandler"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "******* ******* Processing job:"

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 456
    const/4 v1, 0x0

    #@2c
    invoke-direct {p0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->cleanQueue(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V

    #@2f
    .line 457
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@31
    invoke-virtual {v1}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    #@34
    .line 459
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->processCommands()V

    #@37
    .line 460
    monitor-exit v2

    #@38
    goto :goto_e

    #@39
    .end local v0           #job:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;
    :catchall_39
    move-exception v1

    #@3a
    monitor-exit v2
    :try_end_3b
    .catchall {:try_start_3 .. :try_end_3b} :catchall_39

    #@3b
    throw v1
.end method

.method private getAudioQualityNative(Z)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private initializeNative()V
	.registers 1 
	return-void
.end method

.method private initializeStateMachine()V
    .registers 4

    #@0
    .prologue
    const/16 v0, 0x7f

    #@2
    const/4 v2, 0x1

    #@3
    const/4 v1, 0x0

    #@4
    .line 630
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@6
    .line 631
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFreq:I

    #@8
    .line 632
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@a
    .line 633
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSnr:I

    #@c
    .line 634
    sput-boolean v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRadioIsOn:Z

    #@e
    .line 635
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramType:I

    #@10
    .line 637
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramIdentification:I

    #@12
    .line 639
    const-string v0, ""

    #@14
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramService:Ljava/lang/String;

    #@16
    .line 640
    const-string v0, ""

    #@18
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsRadioText:Ljava/lang/String;

    #@1a
    .line 641
    const-string v0, ""

    #@1c
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramTypeName:Ljava/lang/String;

    #@1e
    .line 642
    sput-boolean v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mIsMute:Z

    #@20
    .line 643
    sput-boolean v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSeekSuccess:Z

    #@22
    .line 644
    sput-boolean v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsOn:Z

    #@24
    .line 645
    sput-boolean v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAfOn:Z

    #@26
    .line 646
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsType:I

    #@28
    .line 647
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAlternateFreqHopThreshold:I

    #@2a
    .line 648
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAudioMode:I

    #@2c
    .line 649
    sput v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAudioPath:I

    #@2e
    .line 650
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mWorldRegion:I

    #@30
    .line 651
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mStepSize:I

    #@32
    .line 652
    sput-boolean v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mLiveAudioQuality:Z

    #@34
    .line 653
    sput v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mEstimatedNoiseFloorLevel:I

    #@36
    .line 654
    const/16 v0, 0x64

    #@38
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSignalPollInterval:I

    #@3a
    .line 655
    const/16 v0, 0x40

    #@3c
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mDeemphasisTime:I

    #@3e
    .line 656
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_op_state:I

    #@40
    .line 657
    return-void
.end method

.method private logTimeOut(I)V
    .registers 5
    .parameter "what"

    #@0
    .prologue
    .line 660
    packed-switch p1, :pswitch_data_92

    #@3
    .line 690
    const-string v0, "FmNativehandler"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "BTAPP TIMEOUT (1, "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v2, ")"

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v1

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 692
    :goto_21
    return-void

    #@22
    .line 662
    :pswitch_22
    const-string v0, "FmNativehandler"

    #@24
    const-string v1, "BTAPP TIMEOUT on OPERATION_ENABLE"

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_21

    #@2a
    .line 664
    :pswitch_2a
    const-string v0, "FmNativehandler"

    #@2c
    const-string v1, "BTAPP TIMEOUT on OPERATION_DISABLE"

    #@2e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    goto :goto_21

    #@32
    .line 666
    :pswitch_32
    const-string v0, "FmNativehandler"

    #@34
    const-string v1, "BTAPP TIMEOUT on OPERATION_SEARCH"

    #@36
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_21

    #@3a
    .line 668
    :pswitch_3a
    const-string v0, "FmNativehandler"

    #@3c
    const-string v1, "BTAPP TIMEOUT on OPERATION_TUNE"

    #@3e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    goto :goto_21

    #@42
    .line 670
    :pswitch_42
    const-string v0, "FmNativehandler"

    #@44
    const-string v1, "BTAPP TIMEOUT on OPERATION_MUTE"

    #@46
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_21

    #@4a
    .line 672
    :pswitch_4a
    const-string v0, "FmNativehandler"

    #@4c
    const-string v1, "BTAPP TIMEOUT on OPERATION_SEEK_ABORT"

    #@4e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    goto :goto_21

    #@52
    .line 674
    :pswitch_52
    const-string v0, "FmNativehandler"

    #@54
    const-string v1, "BTAPP TIMEOUT on OPERATION_SET_RDS_MODE"

    #@56
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    goto :goto_21

    #@5a
    .line 676
    :pswitch_5a
    const-string v0, "FmNativehandler"

    #@5c
    const-string v1, "BTAPP TIMEOUT on OPERATION_SET_AUDIO_MODE"

    #@5e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    goto :goto_21

    #@62
    .line 678
    :pswitch_62
    const-string v0, "FmNativehandler"

    #@64
    const-string v1, "BTAPP TIMEOUT on OPERATION_SET_AUDIO_PATH"

    #@66
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    goto :goto_21

    #@6a
    .line 680
    :pswitch_6a
    const-string v0, "FmNativehandler"

    #@6c
    const-string v1, "BTAPP TIMEOUT on OPERATION_SET_STEP_SIZE"

    #@6e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    goto :goto_21

    #@72
    .line 682
    :pswitch_72
    const-string v0, "FmNativehandler"

    #@74
    const-string v1, "BTAPP TIMEOUT on OPERATION_SET_FM_VOLUME"

    #@76
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    goto :goto_21

    #@7a
    .line 684
    :pswitch_7a
    const-string v0, "FmNativehandler"

    #@7c
    const-string v1, "BTAPP TIMEOUT on OPERATION_SET_WORLD_REGION"

    #@7e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    goto :goto_21

    #@82
    .line 686
    :pswitch_82
    const-string v0, "FmNativehandler"

    #@84
    const-string v1, "BTAPP TIMEOUT on OPERATION_NFL"

    #@86
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    goto :goto_21

    #@8a
    .line 688
    :pswitch_8a
    const-string v0, "FmNativehandler"

    #@8c
    const-string v1, "BTAPP TIMEOUT on OPERATION_GENERIC"

    #@8e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    goto :goto_21

    #@92
    .line 660
    :pswitch_data_92
    .packed-switch 0x2
        :pswitch_22
        :pswitch_2a
        :pswitch_32
        :pswitch_3a
        :pswitch_42
        :pswitch_4a
        :pswitch_52
        :pswitch_5a
        :pswitch_62
        :pswitch_6a
        :pswitch_72
        :pswitch_7a
        :pswitch_82
        :pswitch_8a
    .end packed-switch
.end method

.method private muteNative(Z)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private processCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)Z
    .registers 13
    .parameter "job"

    #@0
    .prologue
    const/4 v4, 0x4

    #@1
    .line 313
    const/4 v10, 0x0

    #@2
    .line 315
    .local v10, successful:I
    const/4 v0, -0x1

    #@3
    iput v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->current_CMD:I

    #@5
    .line 317
    iget-wide v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@7
    const-wide/16 v2, 0x0

    #@9
    cmp-long v0, v0, v2

    #@b
    if-nez v0, :cond_3c

    #@d
    .line 318
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@10
    move-result-wide v0

    #@11
    iput-wide v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@13
    .line 319
    const-string v0, "FmNativehandler"

    #@15
    new-instance v1, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v2, "***** ***** processCommand:"

    #@1c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v1

    #@28
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v1

    #@2c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 320
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@31
    iget-object v1, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@33
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@36
    move-result v1

    #@37
    aget v0, v0, v1

    #@39
    packed-switch v0, :pswitch_data_ec

    #@3c
    .line 395
    :cond_3c
    :goto_3c
    if-nez v10, :cond_e9

    #@3e
    const/4 v0, 0x1

    #@3f
    :goto_3f
    return v0

    #@40
    .line 322
    :pswitch_40
    invoke-virtual {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_enableChip()I

    #@43
    move-result v10

    #@44
    .line 323
    goto :goto_3c

    #@45
    .line 325
    :pswitch_45
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@47
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_turnOnRadio(I)I

    #@4a
    move-result v10

    #@4b
    .line 326
    goto :goto_3c

    #@4c
    .line 328
    :pswitch_4c
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_turnOffRadio()I

    #@4f
    move-result v10

    #@50
    .line 329
    goto :goto_3c

    #@51
    .line 331
    :pswitch_51
    invoke-virtual {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_disableChip()I

    #@54
    move-result v10

    #@55
    .line 332
    goto :goto_3c

    #@56
    .line 334
    :pswitch_56
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@58
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_tuneRadio(I)I

    #@5b
    move-result v10

    #@5c
    .line 335
    goto :goto_3c

    #@5d
    .line 337
    :pswitch_5d
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_getStatus()I

    #@60
    move-result v10

    #@61
    .line 338
    goto :goto_3c

    #@62
    .line 340
    :pswitch_62
    iget-boolean v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->b_arg1:Z

    #@64
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_muteAudio(Z)I

    #@67
    move-result v10

    #@68
    .line 341
    goto :goto_3c

    #@69
    .line 343
    :pswitch_69
    iput v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->current_CMD:I

    #@6b
    .line 344
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@6d
    iget v1, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@6f
    invoke-direct {p0, v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_seekStation(II)I

    #@72
    move-result v10

    #@73
    .line 345
    goto :goto_3c

    #@74
    .line 347
    :pswitch_74
    iput v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->current_CMD:I

    #@76
    .line 348
    iget-boolean v1, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->b_arg1:Z

    #@78
    iget v2, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@7a
    iget v3, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@7c
    iget v4, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg3:I

    #@7e
    iget v5, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg4:I

    #@80
    iget v6, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg5:I

    #@82
    iget v7, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg6:I

    #@84
    iget v8, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg7:I

    #@86
    move-object v0, p0

    #@87
    invoke-direct/range {v0 .. v8}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_seekStationCombo(ZIIIIIII)I

    #@8a
    move-result v10

    #@8b
    .line 350
    goto :goto_3c

    #@8c
    .line 352
    :pswitch_8c
    iput v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->current_CMD:I

    #@8e
    .line 353
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@90
    iget v1, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@92
    iget v2, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg3:I

    #@94
    iget v3, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg4:I

    #@96
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_seekRdsStation(IIII)I

    #@99
    move-result v10

    #@9a
    .line 354
    goto :goto_3c

    #@9b
    .line 360
    :pswitch_9b
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@9d
    iget v1, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@9f
    iget v2, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg3:I

    #@a1
    iget v3, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg4:I

    #@a3
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_setRdsMode(IIII)I

    #@a6
    move-result v10

    #@a7
    .line 361
    goto :goto_3c

    #@a8
    .line 363
    :pswitch_a8
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@aa
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_setAudioMode(I)I

    #@ad
    move-result v10

    #@ae
    .line 364
    goto :goto_3c

    #@af
    .line 366
    :pswitch_af
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@b1
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_setAudioPath(I)I

    #@b4
    move-result v10

    #@b5
    .line 367
    goto :goto_3c

    #@b6
    .line 369
    :pswitch_b6
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@b8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_setStepSize(I)I

    #@bb
    move-result v10

    #@bc
    .line 370
    goto :goto_3c

    #@bd
    .line 372
    :pswitch_bd
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@bf
    iget v1, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@c1
    invoke-direct {p0, v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_setWorldRegion(II)I

    #@c4
    move-result v10

    #@c5
    .line 373
    goto/16 :goto_3c

    #@c7
    .line 376
    :pswitch_c7
    :try_start_c7
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@c9
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_estimateNoiseFloorLevel(I)I
    :try_end_cc
    .catch Landroid/os/RemoteException; {:try_start_c7 .. :try_end_cc} :catch_cf

    #@cc
    move-result v10

    #@cd
    goto/16 :goto_3c

    #@cf
    .line 377
    :catch_cf
    move-exception v9

    #@d0
    .line 378
    .local v9, e:Landroid/os/RemoteException;
    const/4 v10, 0x2

    #@d1
    .line 380
    goto/16 :goto_3c

    #@d3
    .line 383
    .end local v9           #e:Landroid/os/RemoteException;
    :pswitch_d3
    :try_start_d3
    iget-boolean v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->b_arg1:Z

    #@d5
    iget v1, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@d7
    invoke-direct {p0, v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_setLiveAudioPolling(ZI)I
    :try_end_da
    .catch Landroid/os/RemoteException; {:try_start_d3 .. :try_end_da} :catch_dd

    #@da
    move-result v10

    #@db
    goto/16 :goto_3c

    #@dd
    .line 384
    :catch_dd
    move-exception v9

    #@de
    .line 385
    .restart local v9       #e:Landroid/os/RemoteException;
    const/4 v10, 0x2

    #@df
    .line 387
    goto/16 :goto_3c

    #@e1
    .line 389
    .end local v9           #e:Landroid/os/RemoteException;
    :pswitch_e1
    iget v0, p1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@e3
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->process_setFMVolume(I)I

    #@e6
    move-result v10

    #@e7
    .line 390
    goto/16 :goto_3c

    #@e9
    .line 395
    :cond_e9
    const/4 v0, 0x0

    #@ea
    goto/16 :goto_3f

    #@ec
    .line 320
    :pswitch_data_ec
    .packed-switch 0x1
        :pswitch_40
        :pswitch_45
        :pswitch_4c
        :pswitch_51
        :pswitch_56
        :pswitch_5d
        :pswitch_62
        :pswitch_69
        :pswitch_74
        :pswitch_8c
        :pswitch_9b
        :pswitch_a8
        :pswitch_af
        :pswitch_b6
        :pswitch_bd
        :pswitch_c7
        :pswitch_d3
        :pswitch_e1
    .end packed-switch
.end method

.method private processCommands()V
    .registers 7

    #@0
    .prologue
    .line 403
    const-string v3, "FmNativehandler"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "***** ***** processCommands:"

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    iget-object v5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@f
    invoke-virtual {v5}, Ljava/util/LinkedList;->toString()Ljava/lang/String;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v4

    #@1b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 406
    new-instance v2, Ljava/util/LinkedList;

    #@20
    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    #@23
    .line 408
    .local v2, to_remove:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;>;"
    const/4 v0, 0x0

    #@24
    .local v0, i:I
    :goto_24
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@26
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    #@29
    move-result v3

    #@2a
    if-ge v0, v3, :cond_3a

    #@2c
    .line 410
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@2e
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@31
    move-result-object v1

    #@32
    check-cast v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@34
    .line 411
    .local v1, job:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;
    invoke-direct {p0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->processCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)Z

    #@37
    move-result v3

    #@38
    if-eqz v3, :cond_90

    #@3a
    .line 424
    .end local v1           #job:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;
    :cond_3a
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    #@3d
    move-result v3

    #@3e
    if-lez v3, :cond_8f

    #@40
    .line 426
    const-string v3, "FmNativehandler"

    #@42
    new-instance v4, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v5, "***** ***** FMQueue size: "

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    iget-object v5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@4f
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    #@52
    move-result v5

    #@53
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v4

    #@57
    const-string v5, " - remove size "

    #@59
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    #@60
    move-result v5

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 427
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@6e
    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->removeAll(Ljava/util/Collection;)Z

    #@71
    .line 428
    const-string v3, "FmNativehandler"

    #@73
    new-instance v4, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v5, "***** ***** FMQueue size:"

    #@7a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v4

    #@7e
    iget-object v5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@80
    invoke-virtual {v5}, Ljava/util/LinkedList;->size()I

    #@83
    move-result v5

    #@84
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@87
    move-result-object v4

    #@88
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v4

    #@8c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 447
    :cond_8f
    return-void

    #@90
    .line 421
    .restart local v1       #job:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;
    :cond_90
    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@93
    .line 408
    add-int/lit8 v0, v0, 0x1

    #@95
    goto :goto_24
.end method

.method private process_estimateNoiseFloorLevel(I)I
    .registers 9
    .parameter "nflLevel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    .line 1980
    const-string v3, "FmNativehandler"

    #@4
    const-string v4, "estimateNoiseFloorLevel()"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 1982
    const/4 v2, 0x0

    #@a
    .line 1985
    .local v2, returnStatus:I
    if-eq p1, v5, :cond_12

    #@c
    if-eq p1, v6, :cond_12

    #@e
    if-eqz p1, :cond_12

    #@10
    .line 1988
    const/4 v2, 0x4

    #@11
    .line 2017
    :cond_11
    :goto_11
    return v2

    #@12
    .line 1989
    :cond_12
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@14
    if-eq v5, v3, :cond_36

    #@16
    .line 1991
    const-string v3, "FmNativehandler"

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "STATE = "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@25
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 1992
    const/4 v2, 0x3

    #@35
    goto :goto_11

    #@36
    .line 1994
    :cond_36
    const/4 v3, 0x4

    #@37
    sput v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@39
    .line 1996
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3c
    move-result-object v1

    #@3d
    .line 1997
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@3f
    .line 1998
    const/16 v3, 0xe

    #@41
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@43
    .line 1999
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@45
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@48
    .line 2000
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@4a
    const-wide/16 v4, 0x4e20

    #@4c
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@4f
    .line 2003
    :try_start_4f
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->estimateNoiseFloorNative(I)Z
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_4f .. :try_end_52} :catch_60

    #@52
    move-result v3

    #@53
    if-eqz v3, :cond_5e

    #@55
    .line 2004
    const/4 v2, 0x0

    #@56
    .line 2012
    :goto_56
    if-eqz v2, :cond_11

    #@58
    .line 2013
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@5a
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@5d
    goto :goto_11

    #@5e
    .line 2006
    :cond_5e
    const/4 v2, 0x2

    #@5f
    goto :goto_56

    #@60
    .line 2008
    :catch_60
    move-exception v0

    #@61
    .line 2009
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@62
    .line 2010
    const-string v3, "FmNativehandler"

    #@64
    const-string v4, "estimateNoiseFloorNative failed"

    #@66
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@69
    goto :goto_56
.end method

.method private process_getStatus()I
    .registers 5

    #@0
    .prologue
    .line 1210
    const-string v1, "FmNativehandler"

    #@2
    const-string v2, "getStatus()"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1212
    const/4 v0, 0x0

    #@8
    .line 1214
    .local v0, returnStatus:I
    const/4 v1, 0x2

    #@9
    sget v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@b
    if-eq v1, v2, :cond_2d

    #@d
    .line 1215
    const-string v1, "FmNativehandler"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "STATE = "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@1c
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 1216
    const/4 v0, 0x3

    #@2c
    .line 1222
    :goto_2c
    return v0

    #@2d
    .line 1219
    :cond_2d
    const/4 v1, 0x0

    #@2e
    invoke-direct {p0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendStatusEventCallbackFromLocalStore(Z)V

    #@31
    goto :goto_2c
.end method

.method private process_muteAudio(Z)I
    .registers 9
    .parameter "mute"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1241
    const-string v3, "FmNativehandler"

    #@3
    const-string v4, "muteAudio()"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1243
    const/4 v2, 0x0

    #@9
    .line 1245
    .local v2, returnStatus:I
    const/4 v3, 0x2

    #@a
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@c
    if-eq v3, v4, :cond_2e

    #@e
    .line 1246
    const-string v3, "FmNativehandler"

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "STATE = "

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@1d
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1247
    const/4 v2, 0x3

    #@2d
    .line 1272
    :cond_2d
    :goto_2d
    return v2

    #@2e
    .line 1249
    :cond_2e
    const/4 v3, 0x4

    #@2f
    sput v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@31
    .line 1251
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@34
    move-result-object v1

    #@35
    .line 1252
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@37
    .line 1253
    const/4 v3, 0x6

    #@38
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@3a
    .line 1254
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@3c
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@3f
    .line 1255
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@41
    const-wide/16 v4, 0x1388

    #@43
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@46
    .line 1258
    :try_start_46
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->muteNative(Z)Z
    :try_end_49
    .catch Ljava/lang/Exception; {:try_start_46 .. :try_end_49} :catch_57

    #@49
    move-result v3

    #@4a
    if-eqz v3, :cond_55

    #@4c
    .line 1259
    const/4 v2, 0x0

    #@4d
    .line 1267
    :goto_4d
    if-eqz v2, :cond_2d

    #@4f
    .line 1268
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@51
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@54
    goto :goto_2d

    #@55
    .line 1261
    :cond_55
    const/4 v2, 0x2

    #@56
    goto :goto_4d

    #@57
    .line 1263
    :catch_57
    move-exception v0

    #@58
    .line 1264
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@59
    .line 1265
    const-string v3, "FmNativehandler"

    #@5b
    const-string v4, "muteAudio failed"

    #@5d
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@60
    goto :goto_4d
.end method

.method private process_seekRdsStation(IIII)I
    .registers 14
    .parameter "scanMode"
    .parameter "minSignalStrength"
    .parameter "rdsCondition"
    .parameter "rdsValue"

    #@0
    .prologue
    const/16 v8, 0xff

    #@2
    const/4 v7, 0x4

    #@3
    const/4 v5, 0x2

    #@4
    const/4 v6, 0x1

    #@5
    .line 1469
    const-string v3, "FmNativehandler"

    #@7
    const-string v4, "seekRdsStation():1"

    #@9
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 1471
    const/4 v2, 0x0

    #@d
    .line 1474
    .local v2, returnStatus:I
    if-ltz p2, :cond_11

    #@f
    if-le p2, v8, :cond_13

    #@11
    .line 1476
    :cond_11
    const/4 v2, 0x4

    #@12
    .line 1515
    :cond_12
    :goto_12
    return v2

    #@13
    .line 1477
    :cond_13
    if-ltz p4, :cond_17

    #@15
    if-le p4, v8, :cond_19

    #@17
    .line 1479
    :cond_17
    const/4 v2, 0x4

    #@18
    goto :goto_12

    #@19
    .line 1480
    :cond_19
    if-eqz p3, :cond_21

    #@1b
    if-eq p3, v6, :cond_21

    #@1d
    if-eq p3, v5, :cond_21

    #@1f
    .line 1483
    const/4 v2, 0x4

    #@20
    goto :goto_12

    #@21
    .line 1484
    :cond_21
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@23
    if-eq v5, v3, :cond_45

    #@25
    .line 1486
    const-string v3, "FmNativehandler"

    #@27
    new-instance v4, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v5, "STATE = "

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@34
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v4

    #@40
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 1487
    const/4 v2, 0x3

    #@44
    goto :goto_12

    #@45
    .line 1489
    :cond_45
    sput v7, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@47
    .line 1491
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@4a
    move-result-object v1

    #@4b
    .line 1492
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@4d
    .line 1493
    iput v7, v1, Landroid/os/Message;->arg1:I

    #@4f
    .line 1494
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@51
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@54
    .line 1495
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@56
    const-wide/16 v4, 0x4e20

    #@58
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@5b
    .line 1498
    and-int/lit16 p1, p1, 0x83

    #@5d
    .line 1501
    :try_start_5d
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->searchNative(IIII)Z
    :try_end_60
    .catch Ljava/lang/Exception; {:try_start_5d .. :try_end_60} :catch_6e

    #@60
    move-result v3

    #@61
    if-eqz v3, :cond_6c

    #@63
    .line 1502
    const/4 v2, 0x0

    #@64
    .line 1510
    :goto_64
    if-eqz v2, :cond_12

    #@66
    .line 1511
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@68
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@6b
    goto :goto_12

    #@6c
    .line 1504
    :cond_6c
    const/4 v2, 0x2

    #@6d
    goto :goto_64

    #@6e
    .line 1506
    :catch_6e
    move-exception v0

    #@6f
    .line 1507
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@70
    .line 1508
    const-string v3, "FmNativehandler"

    #@72
    const-string v4, "searchNative failed"

    #@74
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@77
    goto :goto_64
.end method

.method private process_seekStation(II)I
    .registers 10
    .parameter "scanMode"
    .parameter "minSignalStrength"

    #@0
    .prologue
    const/4 v5, 0x4

    #@1
    const/4 v6, 0x1

    #@2
    .line 1299
    const-string v3, "FmNativehandler"

    #@4
    const-string v4, "seekStation():1"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 1301
    const/4 v2, 0x0

    #@a
    .line 1304
    .local v2, returnStatus:I
    if-ltz p2, :cond_10

    #@c
    const/16 v3, 0xff

    #@e
    if-le p2, v3, :cond_12

    #@10
    .line 1306
    :cond_10
    const/4 v2, 0x4

    #@11
    .line 1345
    :cond_11
    :goto_11
    return v2

    #@12
    .line 1307
    :cond_12
    if-eqz p1, :cond_42

    #@14
    const/16 v3, 0x80

    #@16
    if-eq p1, v3, :cond_42

    #@18
    if-eq p1, v6, :cond_42

    #@1a
    const/16 v3, 0x82

    #@1c
    if-eq p1, v3, :cond_42

    #@1e
    .line 1310
    const-string v3, "FmNativehandler"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "seekStation failed, scanMode too high (0x"

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, ")"

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 1312
    const/4 v2, 0x3

    #@41
    goto :goto_11

    #@42
    .line 1313
    :cond_42
    const/4 v3, 0x2

    #@43
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@45
    if-eq v3, v4, :cond_67

    #@47
    .line 1315
    const-string v3, "FmNativehandler"

    #@49
    new-instance v4, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v5, "STATE = "

    #@50
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v4

    #@54
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@56
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v4

    #@62
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 1316
    const/4 v2, 0x3

    #@66
    goto :goto_11

    #@67
    .line 1318
    :cond_67
    sput v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@69
    .line 1320
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@6c
    move-result-object v1

    #@6d
    .line 1321
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@6f
    .line 1322
    iput v5, v1, Landroid/os/Message;->arg1:I

    #@71
    .line 1323
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@73
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@76
    .line 1324
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@78
    const-wide/16 v4, 0x4e20

    #@7a
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@7d
    .line 1328
    and-int/lit16 p1, p1, 0x83

    #@7f
    .line 1331
    const/4 v3, 0x0

    #@80
    const/4 v4, 0x0

    #@81
    :try_start_81
    invoke-direct {p0, p1, p2, v3, v4}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->searchNative(IIII)Z
    :try_end_84
    .catch Ljava/lang/Exception; {:try_start_81 .. :try_end_84} :catch_92

    #@84
    move-result v3

    #@85
    if-eqz v3, :cond_90

    #@87
    .line 1332
    const/4 v2, 0x0

    #@88
    .line 1340
    :goto_88
    if-eqz v2, :cond_11

    #@8a
    .line 1341
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@8c
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@8f
    goto :goto_11

    #@90
    .line 1334
    :cond_90
    const/4 v2, 0x2

    #@91
    goto :goto_88

    #@92
    .line 1336
    :catch_92
    move-exception v0

    #@93
    .line 1337
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@94
    .line 1338
    const-string v3, "FmNativehandler"

    #@96
    const-string v4, "searchNative failed"

    #@98
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9b
    goto :goto_88
.end method

.method private process_seekStationCombo(ZIIIIIII)I
    .registers 22
    .parameter "multiChannel"
    .parameter "startFreq"
    .parameter "endFreq"
    .parameter "minSignalStrength"
    .parameter "direction"
    .parameter "scanMethod"
    .parameter "rdsType"
    .parameter "rdsTypeValue"

    #@0
    .prologue
    .line 1386
    const-string v1, "FmNativehandler"

    #@2
    const-string v2, "process_seekStationCombo()"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1388
    const/4 v12, 0x0

    #@8
    .line 1391
    .local v12, returnStatus:I
    if-ltz p4, :cond_10

    #@a
    const/16 v1, 0xff

    #@c
    move/from16 v0, p4

    #@e
    if-le v0, v1, :cond_12

    #@10
    .line 1393
    :cond_10
    const/4 v12, 0x4

    #@11
    .line 1432
    :cond_11
    :goto_11
    return v12

    #@12
    .line 1394
    :cond_12
    const/4 v1, 0x2

    #@13
    sget v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@15
    if-eq v1, v2, :cond_37

    #@17
    .line 1396
    const-string v1, "FmNativehandler"

    #@19
    new-instance v2, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v3, "STATE = "

    #@20
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v2

    #@24
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@26
    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 1397
    const/4 v12, 0x3

    #@36
    goto :goto_11

    #@37
    .line 1399
    :cond_37
    const/4 v1, 0x4

    #@38
    sput v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@3a
    .line 1401
    const-string v1, "FmNativehandler"

    #@3c
    const-string v2, "process_seekStationCombo(), OPERATION_TIMEOUT_SEARCH: 20000"

    #@3e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 1403
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@44
    move-result-object v11

    #@45
    .line 1404
    .local v11, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@46
    iput v1, v11, Landroid/os/Message;->what:I

    #@48
    .line 1405
    const/4 v1, 0x4

    #@49
    iput v1, v11, Landroid/os/Message;->arg1:I

    #@4b
    .line 1406
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@4d
    const/4 v2, 0x1

    #@4e
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@51
    .line 1407
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@53
    const-wide/16 v2, 0x4e20

    #@55
    invoke-virtual {v1, v11, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@58
    .line 1410
    const-string v1, "FmNativehandler"

    #@5a
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v3, "seekStationCombo: startFreq = "

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@68
    move-result-object v2

    #@69
    const-string v3, ", endFeq = "

    #@6b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v2

    #@6f
    move/from16 v0, p3

    #@71
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    const-string v3, ", minSignalStrength = "

    #@77
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v2

    #@7b
    move/from16 v0, p4

    #@7d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v2

    #@81
    const-string v3, ", direction = "

    #@83
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v2

    #@87
    move/from16 v0, p5

    #@89
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v2

    #@8d
    const-string v3, ", scanMethod = "

    #@8f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v2

    #@93
    move/from16 v0, p6

    #@95
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@98
    move-result-object v2

    #@99
    const-string v3, ", multiChannel = "

    #@9b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v2

    #@9f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v2

    #@a3
    const-string v3, ", rdsType = "

    #@a5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v2

    #@a9
    move/from16 v0, p7

    #@ab
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v2

    #@af
    const-string v3, ", rdsTypeValue = "

    #@b1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v2

    #@b5
    move/from16 v0, p8

    #@b7
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v2

    #@bb
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v2

    #@bf
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    move-object v1, p0

    #@c3
    move v2, p2

    #@c4
    move/from16 v3, p3

    #@c6
    move/from16 v4, p4

    #@c8
    move/from16 v5, p5

    #@ca
    move/from16 v6, p6

    #@cc
    move v7, p1

    #@cd
    move/from16 v8, p7

    #@cf
    move/from16 v9, p8

    #@d1
    .line 1417
    :try_start_d1
    invoke-direct/range {v1 .. v9}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->comboSearchNative(IIIIIZII)Z
    :try_end_d4
    .catch Ljava/lang/Exception; {:try_start_d1 .. :try_end_d4} :catch_e4

    #@d4
    move-result v1

    #@d5
    if-eqz v1, :cond_e2

    #@d7
    .line 1419
    const/4 v12, 0x0

    #@d8
    .line 1427
    :goto_d8
    if-eqz v12, :cond_11

    #@da
    .line 1428
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@dc
    const/4 v2, 0x1

    #@dd
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@e0
    goto/16 :goto_11

    #@e2
    .line 1421
    :cond_e2
    const/4 v12, 0x2

    #@e3
    goto :goto_d8

    #@e4
    .line 1423
    :catch_e4
    move-exception v10

    #@e5
    .line 1424
    .local v10, e:Ljava/lang/Exception;
    const/4 v12, 0x2

    #@e6
    .line 1425
    const-string v1, "FmNativehandler"

    #@e8
    const-string v2, "comboSearchNative failed"

    #@ea
    invoke-static {v1, v2, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ed
    goto :goto_d8
.end method

.method private process_setAudioMode(I)I
    .registers 9
    .parameter "audioMode"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1652
    const/4 v2, 0x0

    #@2
    .line 1654
    .local v2, returnStatus:I
    const-string v3, "FmNativehandler"

    #@4
    const-string v4, "setAudioMode()"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 1656
    const/4 v3, 0x2

    #@a
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@c
    if-eq v3, v4, :cond_2e

    #@e
    .line 1658
    const-string v3, "FmNativehandler"

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "STATE = "

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@1d
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1659
    const/4 v2, 0x3

    #@2d
    .line 1689
    :cond_2d
    :goto_2d
    return v2

    #@2e
    .line 1661
    :cond_2e
    const/4 v3, 0x4

    #@2f
    sput v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@31
    .line 1663
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@34
    move-result-object v1

    #@35
    .line 1664
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@37
    .line 1665
    const/16 v3, 0x9

    #@39
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@3b
    .line 1666
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@3d
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@40
    .line 1667
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@42
    const-wide/16 v4, 0x1388

    #@44
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@47
    .line 1671
    and-int/lit8 p1, p1, 0x3

    #@49
    .line 1674
    :try_start_49
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setAudioModeNative(I)Z
    :try_end_4c
    .catch Ljava/lang/Exception; {:try_start_49 .. :try_end_4c} :catch_5a

    #@4c
    move-result v3

    #@4d
    if-eqz v3, :cond_58

    #@4f
    .line 1675
    const/4 v2, 0x0

    #@50
    .line 1683
    :goto_50
    if-eqz v2, :cond_2d

    #@52
    .line 1684
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@54
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@57
    goto :goto_2d

    #@58
    .line 1677
    :cond_58
    const/4 v2, 0x2

    #@59
    goto :goto_50

    #@5a
    .line 1679
    :catch_5a
    move-exception v0

    #@5b
    .line 1680
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@5c
    .line 1681
    const-string v3, "FmNativehandler"

    #@5e
    const-string v4, "setAudioModeNative failed"

    #@60
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@63
    goto :goto_50
.end method

.method private process_setAudioPath(I)I
    .registers 10
    .parameter "audioPath"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    .line 1716
    const/4 v2, 0x0

    #@3
    .line 1718
    .local v2, returnStatus:I
    const-string v3, "FmNativehandler"

    #@5
    new-instance v4, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v5, "setAudioPath("

    #@c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v4

    #@10
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    const-string v5, ")"

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 1720
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@27
    if-eq v7, v3, :cond_57

    #@29
    .line 1722
    const-string v3, "FmNativehandler"

    #@2b
    new-instance v4, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v5, "STATE = "

    #@32
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v4

    #@36
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@38
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v4

    #@40
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v4

    #@44
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 1723
    const/4 v2, 0x3

    #@48
    .line 1748
    :goto_48
    if-eqz v2, :cond_4f

    #@4a
    .line 1749
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@4c
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@4f
    .line 1754
    :cond_4f
    if-nez p1, :cond_86

    #@51
    .line 1755
    const-string v3, "fm_route=disabled"

    #@53
    invoke-static {v3}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@56
    .line 1763
    :cond_56
    :goto_56
    return v2

    #@57
    .line 1725
    :cond_57
    const/4 v3, 0x4

    #@58
    sput v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@5a
    .line 1727
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@5d
    move-result-object v1

    #@5e
    .line 1728
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@60
    .line 1729
    const/16 v3, 0xa

    #@62
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@64
    .line 1730
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@66
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@69
    .line 1731
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@6b
    const-wide/16 v4, 0x1388

    #@6d
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@70
    .line 1735
    and-int/lit8 p1, p1, 0x3

    #@72
    .line 1738
    :try_start_72
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setAudioPathNative(I)Z
    :try_end_75
    .catch Ljava/lang/Exception; {:try_start_72 .. :try_end_75} :catch_7c

    #@75
    move-result v3

    #@76
    if-eqz v3, :cond_7a

    #@78
    .line 1739
    const/4 v2, 0x0

    #@79
    goto :goto_48

    #@7a
    .line 1741
    :cond_7a
    const/4 v2, 0x2

    #@7b
    goto :goto_48

    #@7c
    .line 1743
    :catch_7c
    move-exception v0

    #@7d
    .line 1744
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@7e
    .line 1745
    const-string v3, "FmNativehandler"

    #@80
    const-string v4, "setAudioPathNative failed"

    #@82
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@85
    goto :goto_48

    #@86
    .line 1756
    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #msg:Landroid/os/Message;
    :cond_86
    if-ne p1, v6, :cond_8e

    #@88
    .line 1757
    const-string v3, "fm_route=fm_speaker"

    #@8a
    invoke-static {v3}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@8d
    goto :goto_56

    #@8e
    .line 1758
    :cond_8e
    if-ne p1, v7, :cond_56

    #@90
    .line 1759
    const-string v3, "fm_route=fm_headset"

    #@92
    invoke-static {v3}, Landroid/media/AudioSystem;->setParameters(Ljava/lang/String;)I

    #@95
    goto :goto_56
.end method

.method private process_setFMVolume(I)I
    .registers 9
    .parameter "volume"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1846
    const-string v3, "FmNativehandler"

    #@3
    const-string v4, "setFMVolume()"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1848
    const/4 v2, 0x0

    #@9
    .line 1851
    .local v2, returnStatus:I
    if-ltz p1, :cond_f

    #@b
    const/16 v3, 0x100

    #@d
    if-le p1, v3, :cond_29

    #@f
    .line 1853
    :cond_f
    const-string v3, "FmNativehandler"

    #@11
    new-instance v4, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v5, "volume is illegal ="

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v4

    #@24
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 1854
    const/4 v2, 0x4

    #@28
    .line 1884
    :cond_28
    :goto_28
    return v2

    #@29
    .line 1855
    :cond_29
    const/4 v3, 0x2

    #@2a
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@2c
    if-eq v3, v4, :cond_4e

    #@2e
    .line 1857
    const-string v3, "FmNativehandler"

    #@30
    new-instance v4, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v5, "STATE = "

    #@37
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@3d
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@40
    move-result-object v5

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 1858
    const/4 v2, 0x3

    #@4d
    goto :goto_28

    #@4e
    .line 1860
    :cond_4e
    const/4 v3, 0x4

    #@4f
    sput v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@51
    .line 1862
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@54
    move-result-object v1

    #@55
    .line 1863
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@57
    .line 1864
    const/16 v3, 0xc

    #@59
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@5b
    .line 1865
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@5d
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@60
    .line 1866
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@62
    const-wide/16 v4, 0x1388

    #@64
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@67
    .line 1868
    const-string v3, "FmNativehandler"

    #@69
    new-instance v4, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    const-string v5, "setFMVolume ="

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 1870
    :try_start_7f
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setFMVolumeNative(I)Z
    :try_end_82
    .catch Ljava/lang/Exception; {:try_start_7f .. :try_end_82} :catch_90

    #@82
    move-result v3

    #@83
    if-eqz v3, :cond_8e

    #@85
    .line 1871
    const/4 v2, 0x0

    #@86
    .line 1879
    :goto_86
    if-eqz v2, :cond_28

    #@88
    .line 1880
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@8a
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@8d
    goto :goto_28

    #@8e
    .line 1873
    :cond_8e
    const/4 v2, 0x2

    #@8f
    goto :goto_86

    #@90
    .line 1875
    :catch_90
    move-exception v0

    #@91
    .line 1876
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@92
    .line 1877
    const-string v3, "FmNativehandler"

    #@94
    const-string v4, "setFMVolumeNative failed"

    #@96
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@99
    goto :goto_86
.end method

.method private process_setLiveAudioPolling(ZI)I
    .registers 8
    .parameter "liveAudioPolling"
    .parameter "signalPollInterval"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    .line 2044
    const/4 v1, 0x0

    #@2
    .line 2046
    .local v1, returnStatus:I
    const-string v2, "FmNativehandler"

    #@4
    const-string v3, "setLiveAudioPolling()"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 2049
    if-eqz p1, :cond_37

    #@b
    const/16 v2, 0xa

    #@d
    if-lt p2, v2, :cond_14

    #@f
    const v2, 0x186a0

    #@12
    if-le p2, v2, :cond_37

    #@14
    .line 2052
    :cond_14
    const/4 v1, 0x4

    #@15
    .line 2077
    :goto_15
    const-string v2, "FmNativehandler"

    #@17
    new-instance v3, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v4, "STATE = "

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@24
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 2078
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@36
    .line 2079
    return v1

    #@37
    .line 2053
    :cond_37
    sget v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@39
    if-eq v4, v2, :cond_5b

    #@3b
    .line 2055
    const-string v2, "FmNativehandler"

    #@3d
    new-instance v3, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v4, "STATE = "

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@4a
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v3

    #@56
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 2056
    const/4 v1, 0x3

    #@5a
    goto :goto_15

    #@5b
    .line 2058
    :cond_5b
    sput v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@5d
    .line 2062
    :try_start_5d
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->getAudioQualityNative(Z)Z

    #@60
    move-result v2

    #@61
    if-eqz v2, :cond_6d

    #@63
    invoke-direct {p0, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->configureSignalNotificationNative(I)Z
    :try_end_66
    .catch Ljava/lang/Exception; {:try_start_5d .. :try_end_66} :catch_6f

    #@66
    move-result v2

    #@67
    if-eqz v2, :cond_6d

    #@69
    .line 2064
    const/4 v1, 0x0

    #@6a
    .line 2073
    :goto_6a
    sput v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@6c
    goto :goto_15

    #@6d
    .line 2066
    :cond_6d
    const/4 v1, 0x2

    #@6e
    goto :goto_6a

    #@6f
    .line 2068
    :catch_6f
    move-exception v0

    #@70
    .line 2069
    .local v0, e:Ljava/lang/Exception;
    const/4 v1, 0x2

    #@71
    .line 2070
    const-string v2, "FmNativehandler"

    #@73
    const-string v3, "setLiveAudioPolling failed"

    #@75
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@78
    goto :goto_6a
.end method

.method private process_setRdsMode(IIII)I
    .registers 15
    .parameter "rdsMode"
    .parameter "rdsFeatures"
    .parameter "afMode"
    .parameter "afThreshold"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 1578
    const/4 v5, 0x0

    #@3
    .line 1580
    .local v5, returnStatus:I
    const-string v7, "FmNativehandler"

    #@5
    const-string v8, "setRdsMode()"

    #@7
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 1583
    if-ltz p4, :cond_10

    #@c
    const/16 v7, 0xff

    #@e
    if-le p4, v7, :cond_12

    #@10
    .line 1585
    :cond_10
    const/4 v5, 0x4

    #@11
    .line 1627
    :cond_11
    :goto_11
    return v5

    #@12
    .line 1586
    :cond_12
    const/4 v7, 0x2

    #@13
    sget v8, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@15
    if-eq v7, v8, :cond_37

    #@17
    .line 1588
    const-string v6, "FmNativehandler"

    #@19
    new-instance v7, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v8, "STATE = "

    #@20
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v7

    #@24
    sget v8, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@26
    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v7

    #@2e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v7

    #@32
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 1589
    const/4 v5, 0x3

    #@36
    goto :goto_11

    #@37
    .line 1591
    :cond_37
    const/4 v7, 0x4

    #@38
    sput v7, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@3a
    .line 1592
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_op_state:I

    #@3c
    .line 1594
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3f
    move-result-object v2

    #@40
    .line 1595
    .local v2, msg:Landroid/os/Message;
    iput v6, v2, Landroid/os/Message;->what:I

    #@42
    .line 1596
    const/16 v7, 0x8

    #@44
    iput v7, v2, Landroid/os/Message;->arg1:I

    #@46
    .line 1597
    iget-object v7, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@48
    invoke-virtual {v7, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@4b
    .line 1598
    iget-object v7, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@4d
    const-wide/16 v8, 0x1388

    #@4f
    invoke-virtual {v7, v2, v8, v9}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@52
    .line 1602
    and-int/lit8 p1, p1, 0x3

    #@54
    .line 1603
    and-int/lit8 p3, p3, 0x1

    #@56
    .line 1604
    and-int/lit8 p2, p2, 0x7c

    #@58
    .line 1605
    if-eqz p1, :cond_6f

    #@5a
    move v4, v6

    #@5b
    .line 1606
    .local v4, rdsOnNative:Z
    :goto_5b
    if-eqz p3, :cond_5e

    #@5d
    move v0, v6

    #@5e
    .line 1609
    .local v0, afOnNative:Z
    :cond_5e
    and-int/lit8 v3, p1, 0x1

    #@60
    .line 1613
    .local v3, rdsModeNative:I
    :try_start_60
    invoke-direct {p0, v4, v0, v3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setRdsModeNative(ZZI)Z
    :try_end_63
    .catch Ljava/lang/Exception; {:try_start_60 .. :try_end_63} :catch_73

    #@63
    move-result v7

    #@64
    if-eqz v7, :cond_71

    #@66
    .line 1614
    const/4 v5, 0x0

    #@67
    .line 1622
    :goto_67
    if-eqz v5, :cond_11

    #@69
    .line 1623
    iget-object v7, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@6b
    invoke-virtual {v7, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@6e
    goto :goto_11

    #@6f
    .end local v0           #afOnNative:Z
    .end local v3           #rdsModeNative:I
    .end local v4           #rdsOnNative:Z
    :cond_6f
    move v4, v0

    #@70
    .line 1605
    goto :goto_5b

    #@71
    .line 1616
    .restart local v0       #afOnNative:Z
    .restart local v3       #rdsModeNative:I
    .restart local v4       #rdsOnNative:Z
    :cond_71
    const/4 v5, 0x2

    #@72
    goto :goto_67

    #@73
    .line 1618
    :catch_73
    move-exception v1

    #@74
    .line 1619
    .local v1, e:Ljava/lang/Exception;
    const-string v7, "FmNativehandler"

    #@76
    const-string v8, "setRdsNative failed"

    #@78
    invoke-static {v7, v8, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@7b
    .line 1620
    const/4 v5, 0x2

    #@7c
    goto :goto_67
.end method

.method private process_setStepSize(I)I
    .registers 9
    .parameter "stepSize"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1789
    const-string v3, "FmNativehandler"

    #@3
    const-string v4, "setStepSize()"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1791
    const/4 v2, 0x0

    #@9
    .line 1794
    .local v2, returnStatus:I
    const/16 v3, 0x10

    #@b
    if-eq p1, v3, :cond_11

    #@d
    if-eqz p1, :cond_11

    #@f
    .line 1796
    const/4 v2, 0x4

    #@10
    .line 1826
    :cond_10
    :goto_10
    return v2

    #@11
    .line 1797
    :cond_11
    const/4 v3, 0x2

    #@12
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@14
    if-eq v3, v4, :cond_36

    #@16
    .line 1799
    const-string v3, "FmNativehandler"

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "STATE = "

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@25
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v4

    #@31
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 1800
    const/4 v2, 0x3

    #@35
    goto :goto_10

    #@36
    .line 1802
    :cond_36
    const/4 v3, 0x4

    #@37
    sput v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@39
    .line 1804
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3c
    move-result-object v1

    #@3d
    .line 1805
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@3f
    .line 1806
    const/16 v3, 0xb

    #@41
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@43
    .line 1807
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@45
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@48
    .line 1808
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@4a
    const-wide/16 v4, 0x1388

    #@4c
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@4f
    .line 1812
    :try_start_4f
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setScanStepNative(I)Z
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_4f .. :try_end_52} :catch_60

    #@52
    move-result v3

    #@53
    if-eqz v3, :cond_5e

    #@55
    .line 1813
    const/4 v2, 0x0

    #@56
    .line 1821
    :goto_56
    if-eqz v2, :cond_10

    #@58
    .line 1822
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@5a
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@5d
    goto :goto_10

    #@5e
    .line 1815
    :cond_5e
    const/4 v2, 0x2

    #@5f
    goto :goto_56

    #@60
    .line 1817
    :catch_60
    move-exception v0

    #@61
    .line 1818
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@62
    .line 1819
    const-string v3, "FmNativehandler"

    #@64
    const-string v4, "setScanStepNative failed"

    #@66
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@69
    goto :goto_56
.end method

.method private process_setWorldRegion(II)I
    .registers 10
    .parameter "worldRegion"
    .parameter "deemphasisTime"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v6, 0x1

    #@2
    .line 1913
    const-string v3, "FmNativehandler"

    #@4
    const-string v4, "setWorldRegion()"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 1915
    const/4 v2, 0x0

    #@a
    .line 1918
    .local v2, returnStatus:I
    if-eqz p1, :cond_12

    #@c
    if-eq p1, v6, :cond_12

    #@e
    if-eq p1, v5, :cond_12

    #@10
    .line 1921
    const/4 v2, 0x4

    #@11
    .line 1954
    :cond_11
    :goto_11
    return v2

    #@12
    .line 1922
    :cond_12
    if-eqz p2, :cond_1a

    #@14
    const/16 v3, 0x40

    #@16
    if-eq p2, v3, :cond_1a

    #@18
    .line 1924
    const/4 v2, 0x4

    #@19
    goto :goto_11

    #@1a
    .line 1925
    :cond_1a
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@1c
    if-eq v5, v3, :cond_3e

    #@1e
    .line 1927
    const-string v3, "FmNativehandler"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "STATE = "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@2d
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v4

    #@39
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 1928
    const/4 v2, 0x3

    #@3d
    goto :goto_11

    #@3e
    .line 1930
    :cond_3e
    const/4 v3, 0x4

    #@3f
    sput v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@41
    .line 1932
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@44
    move-result-object v1

    #@45
    .line 1933
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@47
    .line 1934
    const/16 v3, 0xd

    #@49
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@4b
    .line 1935
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@4d
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@50
    .line 1936
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@52
    const-wide/16 v4, 0x1388

    #@54
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@57
    .line 1940
    :try_start_57
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setRegionNative(I)Z

    #@5a
    move-result v3

    #@5b
    if-eqz v3, :cond_6c

    #@5d
    invoke-direct {p0, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->configureDeemphasisNative(I)Z
    :try_end_60
    .catch Ljava/lang/Exception; {:try_start_57 .. :try_end_60} :catch_6e

    #@60
    move-result v3

    #@61
    if-eqz v3, :cond_6c

    #@63
    .line 1941
    const/4 v2, 0x0

    #@64
    .line 1949
    :goto_64
    if-eqz v2, :cond_11

    #@66
    .line 1950
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@68
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@6b
    goto :goto_11

    #@6c
    .line 1943
    :cond_6c
    const/4 v2, 0x2

    #@6d
    goto :goto_64

    #@6e
    .line 1945
    :catch_6e
    move-exception v0

    #@6f
    .line 1946
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@70
    .line 1947
    const-string v3, "FmNativehandler"

    #@72
    const-string v4, "setRdsNative failed"

    #@74
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@77
    goto :goto_64
.end method

.method private process_tuneRadio(I)I
    .registers 9
    .parameter "freq"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 1158
    const-string v3, "FmNativehandler"

    #@3
    const-string v4, "tuneRadio()"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1160
    const/4 v2, 0x0

    #@9
    .line 1163
    .local v2, returnStatus:I
    if-lt p1, v6, :cond_10

    #@b
    const v3, 0x1869f

    #@e
    if-le p1, v3, :cond_12

    #@10
    .line 1165
    :cond_10
    const/4 v2, 0x4

    #@11
    .line 1193
    :cond_11
    :goto_11
    return v2

    #@12
    .line 1166
    :cond_12
    const/4 v3, 0x2

    #@13
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@15
    if-eq v3, v4, :cond_37

    #@17
    .line 1167
    const-string v3, "FmNativehandler"

    #@19
    new-instance v4, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v5, "STATE = "

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@26
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 1168
    const/4 v2, 0x3

    #@36
    goto :goto_11

    #@37
    .line 1170
    :cond_37
    const/4 v3, 0x4

    #@38
    sput v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@3a
    .line 1172
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3d
    move-result-object v1

    #@3e
    .line 1173
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@40
    .line 1174
    const/4 v3, 0x5

    #@41
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@43
    .line 1175
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@45
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@48
    .line 1176
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@4a
    const-wide/16 v4, 0x4e20

    #@4c
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@4f
    .line 1179
    :try_start_4f
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->tuneNative(I)Z
    :try_end_52
    .catch Ljava/lang/Exception; {:try_start_4f .. :try_end_52} :catch_60

    #@52
    move-result v3

    #@53
    if-eqz v3, :cond_5e

    #@55
    .line 1180
    const/4 v2, 0x0

    #@56
    .line 1188
    :goto_56
    if-eqz v2, :cond_11

    #@58
    .line 1189
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@5a
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@5d
    goto :goto_11

    #@5e
    .line 1182
    :cond_5e
    const/4 v2, 0x2

    #@5f
    goto :goto_56

    #@60
    .line 1184
    :catch_60
    move-exception v0

    #@61
    .line 1185
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@62
    .line 1186
    const-string v3, "FmNativehandler"

    #@64
    const-string v4, "tuneNative failed"

    #@66
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@69
    goto :goto_56
.end method

.method private process_turnOffRadio()I
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x3

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v6, 0x1

    #@3
    .line 1087
    const-string v3, "FmNativehandler"

    #@5
    const-string v4, "turnOffRadio()"

    #@7
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 1090
    const/4 v2, 0x0

    #@b
    .line 1092
    .local v2, returnStatus:I
    const/4 v3, 0x2

    #@c
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@e
    if-eq v3, v4, :cond_15

    #@10
    .line 1094
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@12
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@15
    .line 1096
    :cond_15
    sput v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@17
    .line 1098
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@1a
    move-result-object v1

    #@1b
    .line 1099
    .local v1, msg:Landroid/os/Message;
    iput v6, v1, Landroid/os/Message;->what:I

    #@1d
    .line 1100
    iput v5, v1, Landroid/os/Message;->arg1:I

    #@1f
    .line 1101
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@21
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@24
    .line 1102
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@26
    const-wide/16 v4, 0x2710

    #@28
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2b
    .line 1106
    const/4 v3, 0x0

    #@2c
    :try_start_2c
    invoke-direct {p0, v3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->disableFmNative(Z)Z
    :try_end_2f
    .catch Ljava/lang/Exception; {:try_start_2c .. :try_end_2f} :catch_42

    #@2f
    move-result v3

    #@30
    if-eqz v3, :cond_40

    #@32
    .line 1107
    const/4 v2, 0x0

    #@33
    .line 1116
    :goto_33
    if-eqz v2, :cond_3a

    #@35
    .line 1118
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@37
    invoke-virtual {v3, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@3a
    .line 1121
    :cond_3a
    invoke-virtual {p0, v7}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setAudioPath(I)I

    #@3d
    .line 1126
    sput v7, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@3f
    .line 1128
    return v2

    #@40
    .line 1109
    :cond_40
    const/4 v2, 0x2

    #@41
    goto :goto_33

    #@42
    .line 1111
    :catch_42
    move-exception v0

    #@43
    .line 1112
    .local v0, e:Ljava/lang/Exception;
    const/4 v2, 0x2

    #@44
    .line 1113
    const-string v3, "FmNativehandler"

    #@46
    const-string v4, "turnOnRadioNative failed"

    #@48
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4b
    goto :goto_33
.end method

.method private process_turnOnRadio(I)I
    .registers 11
    .parameter "functionalityMask"

    #@0
    .prologue
    const/4 v7, 0x2

    #@1
    const/4 v8, 0x1

    #@2
    .line 964
    const-string v5, "FmNativehandler"

    #@4
    const-string v6, "turnOnRadio........()"

    #@6
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 966
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->initializeNative()V

    #@c
    .line 967
    const/4 v4, 0x0

    #@d
    .line 968
    .local v4, returnStatus:I
    and-int/lit8 v3, p1, 0x3

    #@f
    .line 969
    .local v3, requestedRegion:I
    and-int/lit8 v2, p1, 0x70

    #@11
    .line 972
    .local v2, requestedRdsFeatures:I
    if-eq v3, v8, :cond_23

    #@13
    if-eq v3, v7, :cond_23

    #@15
    const/4 v5, 0x3

    #@16
    if-eq v3, v5, :cond_23

    #@18
    if-eqz v3, :cond_23

    #@1a
    .line 977
    const-string v5, "FmNativehandler"

    #@1c
    const-string v6, "Illegal parameter"

    #@1e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 979
    const/4 v4, 0x4

    #@22
    .line 1023
    :cond_22
    :goto_22
    return v4

    #@23
    .line 980
    :cond_23
    and-int/lit8 v5, v2, 0x10

    #@25
    if-eqz v5, :cond_34

    #@27
    and-int/lit8 v5, v2, 0x20

    #@29
    if-eqz v5, :cond_34

    #@2b
    .line 983
    const-string v5, "FmNativehandler"

    #@2d
    const-string v6, "Illegal parameter (2)"

    #@2f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 985
    const/4 v4, 0x4

    #@33
    goto :goto_22

    #@34
    .line 986
    :cond_34
    sget-boolean v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRadioIsOn:Z

    #@36
    if-eqz v5, :cond_3c

    #@38
    .line 988
    invoke-direct {p0, v8}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendStatusEventCallbackFromLocalStore(Z)V

    #@3b
    goto :goto_22

    #@3c
    .line 989
    :cond_3c
    const/4 v5, 0x5

    #@3d
    sget v6, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@3f
    if-eq v5, v6, :cond_61

    #@41
    .line 991
    const-string v5, "FmNativehandler"

    #@43
    new-instance v6, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v7, "STATE = "

    #@4a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v6

    #@4e
    sget v7, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@50
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v6

    #@5c
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 993
    const/4 v4, 0x3

    #@60
    goto :goto_22

    #@61
    .line 995
    :cond_61
    sput p1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFuncMask:I

    #@63
    .line 996
    sput v8, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@65
    .line 998
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@68
    move-result-object v1

    #@69
    .line 999
    .local v1, msg:Landroid/os/Message;
    iput v8, v1, Landroid/os/Message;->what:I

    #@6b
    .line 1000
    iput v7, v1, Landroid/os/Message;->arg1:I

    #@6d
    .line 1001
    iget-object v5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@6f
    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    #@72
    .line 1002
    iget-object v5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@74
    const-wide/16 v6, 0x2710

    #@76
    invoke-virtual {v5, v1, v6, v7}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@79
    .line 1006
    and-int/lit8 p1, p1, 0x73

    #@7b
    .line 1009
    :try_start_7b
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->enableFmNative(I)Z
    :try_end_7e
    .catch Ljava/lang/Exception; {:try_start_7b .. :try_end_7e} :catch_8c

    #@7e
    move-result v5

    #@7f
    if-eqz v5, :cond_8a

    #@81
    .line 1010
    const/4 v4, 0x0

    #@82
    .line 1018
    :goto_82
    if-eqz v4, :cond_22

    #@84
    .line 1019
    iget-object v5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@86
    invoke-virtual {v5, v8}, Landroid/os/Handler;->removeMessages(I)V

    #@89
    goto :goto_22

    #@8a
    .line 1012
    :cond_8a
    const/4 v4, 0x2

    #@8b
    goto :goto_82

    #@8c
    .line 1014
    :catch_8c
    move-exception v0

    #@8d
    .line 1015
    .local v0, e:Ljava/lang/Exception;
    const/4 v4, 0x2

    #@8e
    .line 1016
    const-string v5, "FmNativehandler"

    #@90
    const-string v6, "turnOnRadioNative failed"

    #@92
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@95
    goto :goto_82
.end method

.method private queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    .registers 6
    .parameter "job"

    #@0
    .prologue
    .line 296
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@2
    monitor-enter v1

    #@3
    .line 298
    :try_start_3
    const-string v0, "FmNativehandler"

    #@5
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@7
    invoke-virtual {v2}, Ljava/util/LinkedList;->toString()Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 301
    const-string v0, "FmNativehandler"

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "****** ****** Adding FM Job: "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 302
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->cleanQueue(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V

    #@2d
    .line 303
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@2f
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@32
    .line 306
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@34
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    #@37
    move-result v0

    #@38
    const/4 v2, 0x1

    #@39
    if-ne v0, v2, :cond_3e

    #@3b
    .line 307
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->processCommands()V

    #@3e
    .line 309
    :cond_3e
    monitor-exit v1

    #@3f
    .line 310
    return-void

    #@40
    .line 309
    :catchall_40
    move-exception v0

    #@41
    monitor-exit v1
    :try_end_42
    .catchall {:try_start_3 .. :try_end_42} :catchall_40

    #@42
    throw v0
.end method

.method private registerIntent()V
    .registers 5

    #@0
    .prologue
    .line 855
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 856
    .local v0, intentFilter:Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    #@7
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 857
    const-string v2, "android.intent.action.PACKAGE_RESTARTED"

    #@c
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 858
    const-string v2, "android.intent.action.QUERY_PACKAGE_RESTART"

    #@11
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@14
    .line 859
    const-string v2, "package"

    #@16
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@19
    .line 860
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mContext:Landroid/content/Context;

    #@1b
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@1d
    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@20
    .line 863
    new-instance v1, Landroid/content/IntentFilter;

    #@22
    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    #@25
    .line 864
    .local v1, intentFilter1:Landroid/content/IntentFilter;
    const-string v2, "android.bluetooth.adapter.action.RADIO_STATE_CHANGED"

    #@27
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2a
    .line 865
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mContext:Landroid/content/Context;

    #@2c
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIntentRadioState:Landroid/content/BroadcastReceiver;

    #@2e
    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@31
    .line 866
    return-void
.end method

.method private searchAbortNative()Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private searchNative(IIII)Z
	.registers 6
	const/4 v0, 0x1
	return v0
.end method

.method private sendAudioModeEventCallback(I)V
    .registers 8
    .parameter "audioMode"

    #@0
    .prologue
    .line 2372
    const-string v4, "FmNativehandler"

    #@2
    const-string v5, "sendAudioModeEventCallback"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2375
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@9
    const/4 v5, 0x3

    #@a
    if-eq v4, v5, :cond_13

    #@c
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@e
    if-eqz v4, :cond_13

    #@10
    .line 2377
    const/4 v4, 0x2

    #@11
    sput v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@13
    .line 2386
    :cond_13
    :try_start_13
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@15
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_18
    .catch Ljava/lang/IllegalStateException; {:try_start_13 .. :try_end_18} :catch_33

    #@18
    move-result v0

    #@19
    .line 2387
    .local v0, callbacks:I
    const/4 v2, 0x0

    #@1a
    .local v2, i:I
    :goto_1a
    if-ge v2, v0, :cond_3b

    #@1c
    .line 2390
    :try_start_1c
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@1e
    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@21
    move-result-object v4

    #@22
    check-cast v4, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    #@24
    invoke-interface {v4, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;->onAudioModeEvent(I)V
    :try_end_27
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_27} :catch_2a
    .catch Ljava/lang/IllegalStateException; {:try_start_1c .. :try_end_27} :catch_33

    #@27
    .line 2387
    :goto_27
    add-int/lit8 v2, v2, 0x1

    #@29
    goto :goto_1a

    #@2a
    .line 2391
    :catch_2a
    move-exception v3

    #@2b
    .line 2392
    .local v3, t:Ljava/lang/Throwable;
    :try_start_2b
    const-string v4, "FmNativehandler"

    #@2d
    const-string v5, "sendAudioModeEventCallback"

    #@2f
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_32
    .catch Ljava/lang/IllegalStateException; {:try_start_2b .. :try_end_32} :catch_33

    #@32
    goto :goto_27

    #@33
    .line 2396
    .end local v0           #callbacks:I
    .end local v2           #i:I
    .end local v3           #t:Ljava/lang/Throwable;
    :catch_33
    move-exception v1

    #@34
    .line 2397
    .local v1, e_i:Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@37
    .line 2407
    .end local v1           #e_i:Ljava/lang/IllegalStateException;
    :goto_37
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@3a
    .line 2408
    return-void

    #@3b
    .line 2395
    .restart local v0       #callbacks:I
    .restart local v2       #i:I
    :cond_3b
    :try_start_3b
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@3d
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_40
    .catch Ljava/lang/IllegalStateException; {:try_start_3b .. :try_end_40} :catch_33

    #@40
    goto :goto_37
.end method

.method private sendAudioModeEventCallbackFromLocalStore()V
    .registers 3

    #@0
    .prologue
    .line 2359
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 2360
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x14

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 2361
    sget v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAudioMode:I

    #@a
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@c
    .line 2362
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 2363
    return-void
.end method

.method private sendAudioPathEventCallback(I)V
    .registers 8
    .parameter "audioPath"

    #@0
    .prologue
    .line 2427
    const-string v4, "FmNativehandler"

    #@2
    const-string v5, "sendAudioPathEventCallback"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2430
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@9
    const/4 v5, 0x3

    #@a
    if-eq v4, v5, :cond_13

    #@c
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@e
    if-eqz v4, :cond_13

    #@10
    .line 2432
    const/4 v4, 0x2

    #@11
    sput v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@13
    .line 2442
    :cond_13
    :try_start_13
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@15
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_18
    .catch Ljava/lang/IllegalStateException; {:try_start_13 .. :try_end_18} :catch_33

    #@18
    move-result v0

    #@19
    .line 2443
    .local v0, callbacks:I
    const/4 v2, 0x0

    #@1a
    .local v2, i:I
    :goto_1a
    if-ge v2, v0, :cond_3b

    #@1c
    .line 2446
    :try_start_1c
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@1e
    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@21
    move-result-object v4

    #@22
    check-cast v4, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    #@24
    invoke-interface {v4, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;->onAudioPathEvent(I)V
    :try_end_27
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_27} :catch_2a
    .catch Ljava/lang/IllegalStateException; {:try_start_1c .. :try_end_27} :catch_33

    #@27
    .line 2443
    :goto_27
    add-int/lit8 v2, v2, 0x1

    #@29
    goto :goto_1a

    #@2a
    .line 2447
    :catch_2a
    move-exception v3

    #@2b
    .line 2448
    .local v3, t:Ljava/lang/Throwable;
    :try_start_2b
    const-string v4, "FmNativehandler"

    #@2d
    const-string v5, "sendAudioPathEventCallback"

    #@2f
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_32
    .catch Ljava/lang/IllegalStateException; {:try_start_2b .. :try_end_32} :catch_33

    #@32
    goto :goto_27

    #@33
    .line 2452
    .end local v0           #callbacks:I
    .end local v2           #i:I
    .end local v3           #t:Ljava/lang/Throwable;
    :catch_33
    move-exception v1

    #@34
    .line 2453
    .local v1, e_i:Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@37
    .line 2463
    .end local v1           #e_i:Ljava/lang/IllegalStateException;
    :goto_37
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@3a
    .line 2465
    return-void

    #@3b
    .line 2451
    .restart local v0       #callbacks:I
    .restart local v2       #i:I
    :cond_3b
    :try_start_3b
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@3d
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_40
    .catch Ljava/lang/IllegalStateException; {:try_start_3b .. :try_end_40} :catch_33

    #@40
    goto :goto_37
.end method

.method private sendAudioPathEventCallbackFromLocalStore()V
    .registers 3

    #@0
    .prologue
    .line 2414
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 2415
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x15

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 2416
    sget v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAudioPath:I

    #@a
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@c
    .line 2417
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 2418
    return-void
.end method

.method private sendEstimateNflEventCallback(I)V
    .registers 8
    .parameter "nfl"

    #@0
    .prologue
    .line 2534
    const-string v4, "FmNativehandler"

    #@2
    const-string v5, "sendEstimateNflEventCallback"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2537
    const/4 v4, 0x2

    #@8
    sput v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@a
    .line 2546
    :try_start_a
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@c
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_f
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_f} :catch_2a

    #@f
    move-result v0

    #@10
    .line 2547
    .local v0, callbacks:I
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    if-ge v2, v0, :cond_32

    #@13
    .line 2550
    :try_start_13
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@15
    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@18
    move-result-object v4

    #@19
    check-cast v4, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    #@1b
    invoke-interface {v4, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;->onEstimateNflEvent(I)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_1e} :catch_21
    .catch Ljava/lang/IllegalStateException; {:try_start_13 .. :try_end_1e} :catch_2a

    #@1e
    .line 2547
    :goto_1e
    add-int/lit8 v2, v2, 0x1

    #@20
    goto :goto_11

    #@21
    .line 2551
    :catch_21
    move-exception v3

    #@22
    .line 2552
    .local v3, t:Ljava/lang/Throwable;
    :try_start_22
    const-string v4, "FmNativehandler"

    #@24
    const-string v5, "sendEstimateNflEventCallback"

    #@26
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_29
    .catch Ljava/lang/IllegalStateException; {:try_start_22 .. :try_end_29} :catch_2a

    #@29
    goto :goto_1e

    #@2a
    .line 2556
    .end local v0           #callbacks:I
    .end local v2           #i:I
    .end local v3           #t:Ljava/lang/Throwable;
    :catch_2a
    move-exception v1

    #@2b
    .line 2557
    .local v1, e_i:Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@2e
    .line 2565
    .end local v1           #e_i:Ljava/lang/IllegalStateException;
    :goto_2e
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@31
    .line 2567
    return-void

    #@32
    .line 2555
    .restart local v0       #callbacks:I
    .restart local v2       #i:I
    :cond_32
    :try_start_32
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@34
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_37
    .catch Ljava/lang/IllegalStateException; {:try_start_32 .. :try_end_37} :catch_2a

    #@37
    goto :goto_2e
.end method

.method private sendEstimateNflEventCallbackFromLocalStore()V
    .registers 3

    #@0
    .prologue
    .line 2521
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 2522
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x17

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 2523
    sget v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mEstimatedNoiseFloorLevel:I

    #@a
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@c
    .line 2524
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 2525
    return-void
.end method

.method private sendLiveAudioQualityEventCallback(II)V
    .registers 9
    .parameter "rssi"
    .parameter "snr"

    #@0
    .prologue
    .line 2587
    const-string v4, "FmNativehandler"

    #@2
    const-string v5, "sendLiveAudioQualityEventCallback"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2597
    :try_start_7
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@9
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_c
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_c} :catch_27

    #@c
    move-result v0

    #@d
    .line 2598
    .local v0, callbacks:I
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, v0, :cond_2c

    #@10
    .line 2601
    :try_start_10
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@12
    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@15
    move-result-object v4

    #@16
    check-cast v4, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    #@18
    invoke-interface {v4, p1, p2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;->onLiveAudioQualityEvent(II)V
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_1b} :catch_1e
    .catch Ljava/lang/IllegalStateException; {:try_start_10 .. :try_end_1b} :catch_27

    #@1b
    .line 2598
    :goto_1b
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_e

    #@1e
    .line 2602
    :catch_1e
    move-exception v3

    #@1f
    .line 2603
    .local v3, t:Ljava/lang/Throwable;
    :try_start_1f
    const-string v4, "FmNativehandler"

    #@21
    const-string v5, "sendLiveAudioQualityEventCallback"

    #@23
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_26
    .catch Ljava/lang/IllegalStateException; {:try_start_1f .. :try_end_26} :catch_27

    #@26
    goto :goto_1b

    #@27
    .line 2607
    .end local v0           #callbacks:I
    .end local v2           #i:I
    .end local v3           #t:Ljava/lang/Throwable;
    :catch_27
    move-exception v1

    #@28
    .line 2608
    .local v1, e_i:Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@2b
    .line 2611
    .end local v1           #e_i:Ljava/lang/IllegalStateException;
    :goto_2b
    return-void

    #@2c
    .line 2606
    .restart local v0       #callbacks:I
    .restart local v2       #i:I
    :cond_2c
    :try_start_2c
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@2e
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_31
    .catch Ljava/lang/IllegalStateException; {:try_start_2c .. :try_end_31} :catch_27

    #@31
    goto :goto_2b
.end method

.method private sendLiveAudioQualityEventCallbackFromLocalStore(II)V
    .registers 5
    .parameter "rssi"
    .parameter "snr"

    #@0
    .prologue
    .line 2573
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 2574
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x18

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 2575
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@a
    .line 2576
    iput p2, v0, Landroid/os/Message;->arg2:I

    #@c
    .line 2577
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 2578
    return-void
.end method

.method private sendRdsDataEventCallback(IILjava/lang/String;)V
    .registers 10
    .parameter "rdsDataType"
    .parameter "rdsIndex"
    .parameter "rdsText"

    #@0
    .prologue
    .line 2329
    const-string v4, "FmNativehandler"

    #@2
    const-string v5, "sendRdsDataEventCallback"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2339
    :try_start_7
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@9
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_c
    .catch Ljava/lang/IllegalStateException; {:try_start_7 .. :try_end_c} :catch_27

    #@c
    move-result v0

    #@d
    .line 2340
    .local v0, callbacks:I
    const/4 v2, 0x0

    #@e
    .local v2, i:I
    :goto_e
    if-ge v2, v0, :cond_2c

    #@10
    .line 2343
    :try_start_10
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@12
    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@15
    move-result-object v4

    #@16
    check-cast v4, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    #@18
    invoke-interface {v4, p1, p2, p3}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;->onRdsDataEvent(IILjava/lang/String;)V
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_1b} :catch_1e
    .catch Ljava/lang/IllegalStateException; {:try_start_10 .. :try_end_1b} :catch_27

    #@1b
    .line 2340
    :goto_1b
    add-int/lit8 v2, v2, 0x1

    #@1d
    goto :goto_e

    #@1e
    .line 2344
    :catch_1e
    move-exception v3

    #@1f
    .line 2345
    .local v3, t:Ljava/lang/Throwable;
    :try_start_1f
    const-string v4, "FmNativehandler"

    #@21
    const-string v5, "sendRdsModeEventCallback"

    #@23
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_26
    .catch Ljava/lang/IllegalStateException; {:try_start_1f .. :try_end_26} :catch_27

    #@26
    goto :goto_1b

    #@27
    .line 2349
    .end local v0           #callbacks:I
    .end local v2           #i:I
    .end local v3           #t:Ljava/lang/Throwable;
    :catch_27
    move-exception v1

    #@28
    .line 2350
    .local v1, e_i:Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@2b
    .line 2353
    .end local v1           #e_i:Ljava/lang/IllegalStateException;
    :goto_2b
    return-void

    #@2c
    .line 2348
    .restart local v0       #callbacks:I
    .restart local v2       #i:I
    :cond_2c
    :try_start_2c
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@2e
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_31
    .catch Ljava/lang/IllegalStateException; {:try_start_2c .. :try_end_31} :catch_27

    #@31
    goto :goto_2b
.end method

.method private sendRdsDataEventCallbackFromLocalStore(IILjava/lang/String;)V
    .registers 6
    .parameter "rdsDataType"
    .parameter "rdsIndex"
    .parameter "rdsText"

    #@0
    .prologue
    .line 2312
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 2313
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x13

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 2314
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@a
    .line 2315
    iput p2, v0, Landroid/os/Message;->arg2:I

    #@c
    .line 2316
    new-instance v1, Ljava/lang/String;

    #@e
    invoke-direct {v1, p3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@11
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@13
    .line 2317
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@15
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@18
    .line 2318
    return-void
.end method

.method private sendRdsModeEventCallback(II)V
    .registers 9
    .parameter "rdsMode"
    .parameter "alternateFreqMode"

    #@0
    .prologue
    .line 2272
    const-string v4, "FmNativehandler"

    #@2
    const-string v5, "sendRdsModeEventCallback"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2275
    const/4 v4, 0x2

    #@8
    sput v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@a
    .line 2285
    :try_start_a
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@c
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_f
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_f} :catch_2a

    #@f
    move-result v0

    #@10
    .line 2286
    .local v0, callbacks:I
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    if-ge v2, v0, :cond_32

    #@13
    .line 2289
    :try_start_13
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@15
    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@18
    move-result-object v4

    #@19
    check-cast v4, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    #@1b
    invoke-interface {v4, p1, p2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;->onRdsModeEvent(II)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_1e} :catch_21
    .catch Ljava/lang/IllegalStateException; {:try_start_13 .. :try_end_1e} :catch_2a

    #@1e
    .line 2286
    :goto_1e
    add-int/lit8 v2, v2, 0x1

    #@20
    goto :goto_11

    #@21
    .line 2290
    :catch_21
    move-exception v3

    #@22
    .line 2291
    .local v3, t:Ljava/lang/Throwable;
    :try_start_22
    const-string v4, "FmNativehandler"

    #@24
    const-string v5, "sendRdsModeEventCallback"

    #@26
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_29
    .catch Ljava/lang/IllegalStateException; {:try_start_22 .. :try_end_29} :catch_2a

    #@29
    goto :goto_1e

    #@2a
    .line 2295
    .end local v0           #callbacks:I
    .end local v2           #i:I
    .end local v3           #t:Ljava/lang/Throwable;
    :catch_2a
    move-exception v1

    #@2b
    .line 2296
    .local v1, e_i:Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@2e
    .line 2303
    .end local v1           #e_i:Ljava/lang/IllegalStateException;
    :goto_2e
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@31
    .line 2305
    return-void

    #@32
    .line 2294
    .restart local v0       #callbacks:I
    .restart local v2       #i:I
    :cond_32
    :try_start_32
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@34
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_37
    .catch Ljava/lang/IllegalStateException; {:try_start_32 .. :try_end_37} :catch_2a

    #@37
    goto :goto_2e
.end method

.method private sendRdsModeEventCallbackFromLocalStore()V
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 2247
    const-string v4, "FmNativehandler"

    #@3
    const-string v5, "sendRdsModeEventCallbackFromLocalStore"

    #@5
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2248
    const/4 v2, 0x0

    #@9
    .line 2249
    .local v2, rds:I
    sget-boolean v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAfOn:Z

    #@b
    if-eqz v4, :cond_29

    #@d
    move v0, v3

    #@e
    .line 2251
    .local v0, af:I
    :goto_e
    sget-boolean v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsOn:Z

    #@10
    if-eqz v4, :cond_17

    #@12
    .line 2252
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsType:I

    #@14
    if-nez v4, :cond_2b

    #@16
    move v2, v3

    #@17
    .line 2254
    :cond_17
    :goto_17
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@1a
    move-result-object v1

    #@1b
    .line 2255
    .local v1, msg:Landroid/os/Message;
    const/16 v3, 0x12

    #@1d
    iput v3, v1, Landroid/os/Message;->what:I

    #@1f
    .line 2256
    iput v2, v1, Landroid/os/Message;->arg1:I

    #@21
    .line 2257
    iput v0, v1, Landroid/os/Message;->arg2:I

    #@23
    .line 2258
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@25
    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@28
    .line 2261
    return-void

    #@29
    .line 2249
    .end local v0           #af:I
    .end local v1           #msg:Landroid/os/Message;
    :cond_29
    const/4 v0, 0x0

    #@2a
    goto :goto_e

    #@2b
    .line 2252
    .restart local v0       #af:I
    :cond_2b
    const/4 v2, 0x2

    #@2c
    goto :goto_17
.end method

.method private sendSeekCompleteEventCallback(IIIZI)V
    .registers 12
    .parameter "freq"
    .parameter "rssi"
    .parameter "snr"
    .parameter "seekSuccess"
    .parameter "iSendNextJob"

    #@0
    .prologue
    .line 2201
    const-string v4, "FmNativehandler"

    #@2
    const-string v5, "sendSeekCompleteEventCallback"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2204
    const/4 v4, 0x2

    #@8
    sput v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@a
    .line 2217
    :try_start_a
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@c
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_f
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_f} :catch_2a

    #@f
    move-result v0

    #@10
    .line 2218
    .local v0, callbacks:I
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    if-ge v2, v0, :cond_34

    #@13
    .line 2221
    :try_start_13
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@15
    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@18
    move-result-object v4

    #@19
    check-cast v4, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    #@1b
    invoke-interface {v4, p1, p2, p3, p4}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;->onSeekCompleteEvent(IIIZ)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_1e} :catch_21
    .catch Ljava/lang/IllegalStateException; {:try_start_13 .. :try_end_1e} :catch_2a

    #@1e
    .line 2218
    :goto_1e
    add-int/lit8 v2, v2, 0x1

    #@20
    goto :goto_11

    #@21
    .line 2223
    :catch_21
    move-exception v3

    #@22
    .line 2224
    .local v3, t:Ljava/lang/Throwable;
    :try_start_22
    const-string v4, "FmNativehandler"

    #@24
    const-string v5, "sendSeekCompleteEventCallback"

    #@26
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_29
    .catch Ljava/lang/IllegalStateException; {:try_start_22 .. :try_end_29} :catch_2a

    #@29
    goto :goto_1e

    #@2a
    .line 2228
    .end local v0           #callbacks:I
    .end local v2           #i:I
    .end local v3           #t:Ljava/lang/Throwable;
    :catch_2a
    move-exception v1

    #@2b
    .line 2229
    .local v1, e_i:Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@2e
    .line 2238
    .end local v1           #e_i:Ljava/lang/IllegalStateException;
    :goto_2e
    if-lez p5, :cond_33

    #@30
    .line 2239
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@33
    .line 2241
    :cond_33
    return-void

    #@34
    .line 2227
    .restart local v0       #callbacks:I
    .restart local v2       #i:I
    :cond_34
    :try_start_34
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@36
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_39
    .catch Ljava/lang/IllegalStateException; {:try_start_34 .. :try_end_39} :catch_2a

    #@39
    goto :goto_2e
.end method

.method private sendSeekCompleteEventCallbackFromLocalStore(Z)V
    .registers 10
    .parameter "SendNextJob"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 2172
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@4
    move-result-object v6

    #@5
    .line 2173
    .local v6, msg:Landroid/os/Message;
    const/16 v1, 0x11

    #@7
    iput v1, v6, Landroid/os/Message;->what:I

    #@9
    .line 2174
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;

    #@b
    sget v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFreq:I

    #@d
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@f
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSnr:I

    #@11
    sget-boolean v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSeekSuccess:Z

    #@13
    move-object v1, p0

    #@14
    invoke-direct/range {v0 .. v5}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IIIZ)V

    #@17
    .line 2179
    .local v0, search_st:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;
    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19
    .line 2180
    if-ne p1, v7, :cond_24

    #@1b
    move v1, v7

    #@1c
    :goto_1c
    iput v1, v6, Landroid/os/Message;->arg1:I

    #@1e
    .line 2181
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@20
    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@23
    .line 2183
    return-void

    #@24
    .line 2180
    :cond_24
    const/4 v1, 0x0

    #@25
    goto :goto_1c
.end method

.method private sendStatusEventCallback(IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
    .registers 25
    .parameter "freq"
    .parameter "rssi"
    .parameter "snr"
    .parameter "radioIsOn"
    .parameter "rdsProgramType"
    .parameter "rdsProgramService"
    .parameter "rdsRadioText"
    .parameter "rdsProgramTypeName"
    .parameter "isMute"
    .parameter "iSendNextJob"

    #@0
    .prologue
    .line 2132
    sget v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@2
    const/4 v1, 0x3

    #@3
    if-eq v0, v1, :cond_c

    #@5
    sget v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 2134
    const/4 v0, 0x2

    #@a
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@c
    .line 2139
    :cond_c
    :try_start_c
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@e
    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_11
    .catch Ljava/lang/IllegalStateException; {:try_start_c .. :try_end_11} :catch_39

    #@11
    move-result v10

    #@12
    .line 2140
    .local v10, callbacks:I
    const/4 v13, 0x0

    #@13
    .local v13, i:I
    :goto_13
    if-ge v13, v10, :cond_43

    #@15
    .line 2143
    :try_start_15
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@17
    invoke-virtual {v0, v13}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@1a
    move-result-object v0

    #@1b
    check-cast v0, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    #@1d
    move v1, p1

    #@1e
    move/from16 v2, p2

    #@20
    move/from16 v3, p3

    #@22
    move/from16 v4, p4

    #@24
    move/from16 v5, p5

    #@26
    move-object/from16 v6, p6

    #@28
    move-object/from16 v7, p7

    #@2a
    move-object/from16 v8, p8

    #@2c
    move/from16 v9, p9

    #@2e
    invoke-interface/range {v0 .. v9}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;->onStatusEvent(IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_31
    .catch Landroid/os/RemoteException; {:try_start_15 .. :try_end_31} :catch_34
    .catch Ljava/lang/IllegalStateException; {:try_start_15 .. :try_end_31} :catch_39

    #@31
    .line 2140
    :goto_31
    add-int/lit8 v13, v13, 0x1

    #@33
    goto :goto_13

    #@34
    .line 2147
    :catch_34
    move-exception v11

    #@35
    .line 2148
    .local v11, e:Landroid/os/RemoteException;
    :try_start_35
    invoke-virtual {v11}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_38
    .catch Ljava/lang/IllegalStateException; {:try_start_35 .. :try_end_38} :catch_39

    #@38
    goto :goto_31

    #@39
    .line 2152
    .end local v10           #callbacks:I
    .end local v11           #e:Landroid/os/RemoteException;
    .end local v13           #i:I
    :catch_39
    move-exception v12

    #@3a
    .line 2153
    .local v12, e_i:Ljava/lang/IllegalStateException;
    invoke-virtual {v12}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@3d
    .line 2162
    .end local v12           #e_i:Ljava/lang/IllegalStateException;
    :goto_3d
    if-lez p10, :cond_42

    #@3f
    .line 2163
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@42
    .line 2165
    :cond_42
    return-void

    #@43
    .line 2151
    .restart local v10       #callbacks:I
    .restart local v13       #i:I
    :cond_43
    :try_start_43
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@45
    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_48
    .catch Ljava/lang/IllegalStateException; {:try_start_43 .. :try_end_48} :catch_39

    #@48
    goto :goto_3d
.end method

.method private sendStatusEventCallbackFromLocalStore(Z)V
    .registers 15
    .parameter "SendNextJob"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    .line 2096
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@4
    move-result-object v11

    #@5
    .line 2097
    .local v11, msg:Landroid/os/Message;
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;

    #@7
    sget v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFreq:I

    #@9
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@b
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSnr:I

    #@d
    sget-boolean v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRadioIsOn:Z

    #@f
    sget v6, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramType:I

    #@11
    sget-object v7, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramService:Ljava/lang/String;

    #@13
    sget-object v8, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsRadioText:Ljava/lang/String;

    #@15
    sget-object v9, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramTypeName:Ljava/lang/String;

    #@17
    sget-boolean v10, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mIsMute:Z

    #@19
    move-object v1, p0

    #@1a
    invoke-direct/range {v0 .. v10}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    #@1d
    .line 2106
    .local v0, status:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;
    const/16 v1, 0x10

    #@1f
    iput v1, v11, Landroid/os/Message;->what:I

    #@21
    .line 2107
    if-ne p1, v12, :cond_2e

    #@23
    move v1, v12

    #@24
    :goto_24
    iput v1, v11, Landroid/os/Message;->arg1:I

    #@26
    .line 2108
    iput-object v0, v11, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@28
    .line 2109
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@2a
    invoke-virtual {v1, v11}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@2d
    .line 2110
    return-void

    #@2e
    .line 2107
    :cond_2e
    const/4 v1, 0x0

    #@2f
    goto :goto_24
.end method

.method private sendVolumeEventCallback(II)V
    .registers 9
    .parameter "status"
    .parameter "volume"

    #@0
    .prologue
    .line 2635
    const-string v4, "FmNativehandler"

    #@2
    const-string v5, "sendVolumeEventCallback"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2638
    const/4 v4, 0x2

    #@8
    sput v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@a
    .line 2648
    :try_start_a
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@c
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_f
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_f} :catch_2a

    #@f
    move-result v0

    #@10
    .line 2649
    .local v0, callbacks:I
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    if-ge v2, v0, :cond_32

    #@13
    .line 2652
    :try_start_13
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@15
    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@18
    move-result-object v4

    #@19
    check-cast v4, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    #@1b
    invoke-interface {v4, p1, p2}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;->onVolumeEvent(II)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_1e} :catch_21
    .catch Ljava/lang/IllegalStateException; {:try_start_13 .. :try_end_1e} :catch_2a

    #@1e
    .line 2649
    :goto_1e
    add-int/lit8 v2, v2, 0x1

    #@20
    goto :goto_11

    #@21
    .line 2653
    :catch_21
    move-exception v3

    #@22
    .line 2654
    .local v3, t:Ljava/lang/Throwable;
    :try_start_22
    const-string v4, "FmNativehandler"

    #@24
    const-string v5, "sendVolumeEventCallback"

    #@26
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_29
    .catch Ljava/lang/IllegalStateException; {:try_start_22 .. :try_end_29} :catch_2a

    #@29
    goto :goto_1e

    #@2a
    .line 2658
    .end local v0           #callbacks:I
    .end local v2           #i:I
    .end local v3           #t:Ljava/lang/Throwable;
    :catch_2a
    move-exception v1

    #@2b
    .line 2659
    .local v1, e_i:Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@2e
    .line 2666
    .end local v1           #e_i:Ljava/lang/IllegalStateException;
    :goto_2e
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@31
    .line 2668
    return-void

    #@32
    .line 2657
    .restart local v0       #callbacks:I
    .restart local v2       #i:I
    :cond_32
    :try_start_32
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@34
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_37
    .catch Ljava/lang/IllegalStateException; {:try_start_32 .. :try_end_37} :catch_2a

    #@37
    goto :goto_2e
.end method

.method private sendVolumeEventCallbackFromLocalStore(II)V
    .registers 5
    .parameter "status"
    .parameter "volume"

    #@0
    .prologue
    .line 2618
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 2619
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x19

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 2620
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@a
    .line 2621
    iput p2, v0, Landroid/os/Message;->arg2:I

    #@c
    .line 2622
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 2623
    return-void
.end method

.method private sendWorldRegionEventCallback(I)V
    .registers 8
    .parameter "worldRegion"

    #@0
    .prologue
    .line 2484
    const-string v4, "FmNativehandler"

    #@2
    const-string v5, "sendWorldRegionEventCallback"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2487
    const/4 v4, 0x2

    #@8
    sput v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@a
    .line 2496
    :try_start_a
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@c
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_f
    .catch Ljava/lang/IllegalStateException; {:try_start_a .. :try_end_f} :catch_2a

    #@f
    move-result v0

    #@10
    .line 2497
    .local v0, callbacks:I
    const/4 v2, 0x0

    #@11
    .local v2, i:I
    :goto_11
    if-ge v2, v0, :cond_32

    #@13
    .line 2500
    :try_start_13
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@15
    invoke-virtual {v4, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@18
    move-result-object v4

    #@19
    check-cast v4, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;

    #@1b
    invoke-interface {v4, p1}, Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;->onWorldRegionEvent(I)V
    :try_end_1e
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_1e} :catch_21
    .catch Ljava/lang/IllegalStateException; {:try_start_13 .. :try_end_1e} :catch_2a

    #@1e
    .line 2497
    :goto_1e
    add-int/lit8 v2, v2, 0x1

    #@20
    goto :goto_11

    #@21
    .line 2501
    :catch_21
    move-exception v3

    #@22
    .line 2502
    .local v3, t:Ljava/lang/Throwable;
    :try_start_22
    const-string v4, "FmNativehandler"

    #@24
    const-string v5, "sendWorldRegionEventCallback"

    #@26
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_29
    .catch Ljava/lang/IllegalStateException; {:try_start_22 .. :try_end_29} :catch_2a

    #@29
    goto :goto_1e

    #@2a
    .line 2506
    .end local v0           #callbacks:I
    .end local v2           #i:I
    .end local v3           #t:Ljava/lang/Throwable;
    :catch_2a
    move-exception v1

    #@2b
    .line 2507
    .local v1, e_i:Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    #@2e
    .line 2514
    .end local v1           #e_i:Ljava/lang/IllegalStateException;
    :goto_2e
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@31
    .line 2515
    return-void

    #@32
    .line 2505
    .restart local v0       #callbacks:I
    .restart local v2       #i:I
    :cond_32
    :try_start_32
    iget-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@34
    invoke-virtual {v4}, Landroid/os/RemoteCallbackList;->finishBroadcast()V
    :try_end_37
    .catch Ljava/lang/IllegalStateException; {:try_start_32 .. :try_end_37} :catch_2a

    #@37
    goto :goto_2e
.end method

.method private sendWorldRegionEventCallbackFromLocalStore()V
    .registers 3

    #@0
    .prologue
    .line 2471
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    .line 2472
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x16

    #@6
    iput v1, v0, Landroid/os/Message;->what:I

    #@8
    .line 2473
    sget v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mWorldRegion:I

    #@a
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@c
    .line 2474
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 2475
    return-void
.end method

.method private setAudioModeNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private setAudioPathNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private setFMVolumeNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private setRdsModeNative(ZZI)Z
	.registers 5
	const/4 v0, 0x1
	return v0
.end method

.method private setRegionNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private setScanStepNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private setSnrThresholdNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private tuneNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private unRegisterIntent()V
    .registers 4

    #@0
    .prologue
    .line 871
    :try_start_0
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mContext:Landroid/content/Context;

    #@2
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIntentReceiver:Landroid/content/BroadcastReceiver;

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_7} :catch_12

    #@7
    .line 879
    :goto_7
    const/4 v1, 0x0

    #@8
    iput-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mClientName:Ljava/lang/String;

    #@a
    .line 882
    :try_start_a
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mContext:Landroid/content/Context;

    #@c
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIntentRadioState:Landroid/content/BroadcastReceiver;

    #@e
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_11} :catch_1b

    #@11
    .line 890
    :goto_11
    return-void

    #@12
    .line 872
    :catch_12
    move-exception v0

    #@13
    .line 873
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "FmNativehandler"

    #@15
    const-string v2, "unRegisterIntent failed"

    #@17
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1a
    goto :goto_7

    #@1b
    .line 883
    .end local v0           #e:Ljava/lang/Exception;
    :catch_1b
    move-exception v0

    #@1c
    .line 884
    .restart local v0       #e:Ljava/lang/Exception;
    const-string v1, "FmNativehandler"

    #@1e
    const-string v2, "unRegisterIntent failed"

    #@20
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23
    goto :goto_11
.end method


# virtual methods
.method public checkForPendingResponses()V
    .registers 3

    #@0
    .prologue
    .line 2675
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "checkForPendingResponses"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2677
    sget v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@9
    sget v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSnr:I

    #@b
    invoke-direct {p0, v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendLiveAudioQualityEventCallbackFromLocalStore(II)V

    #@e
    .line 2679
    return-void
.end method

.method public declared-synchronized cleanupFmService()I
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1139
    monitor-enter p0

    #@2
    const/4 v0, 0x0

    #@3
    :try_start_3
    invoke-virtual {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->onRadioOffEvent(I)V
    :try_end_6
    .catchall {:try_start_3 .. :try_end_6} :catchall_8

    #@6
    .line 1140
    monitor-exit p0

    #@7
    return v1

    #@8
    .line 1139
    :catchall_8
    move-exception v0

    #@9
    monitor-exit p0

    #@a
    throw v0
.end method

.method public clearAllQueue()V
    .registers 5

    #@0
    .prologue
    .line 468
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@2
    monitor-enter v1

    #@3
    .line 469
    :try_start_3
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@5
    if-eqz v0, :cond_2d

    #@7
    .line 470
    const-string v0, "FmNativehandler"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "******* Clearing the queue. Present size is "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@16
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    #@19
    move-result v3

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 471
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@27
    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    #@2a
    .line 472
    const/4 v0, 0x0

    #@2b
    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->FMQueue:Ljava/util/LinkedList;

    #@2d
    .line 474
    :cond_2d
    monitor-exit v1

    #@2e
    .line 475
    return-void

    #@2f
    .line 474
    :catchall_2f
    move-exception v0

    #@30
    monitor-exit v1
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    #@31
    throw v0
.end method

.method public declared-synchronized estimateNoiseFloorLevel(I)I
    .registers 4
    .parameter "nflLevel"

    #@0
    .prologue
    .line 1974
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_ESTIMATE_NOISE_FLOOR_LEVEL:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;I)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1975
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1974
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method protected finalize()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    #@0
    .prologue
    .line 541
    return-void
.end method

.method public finish()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/16 v3, 0x4e20

    #@3
    const/16 v2, 0x2710

    #@5
    .line 586
    const-string v0, "FmNativehandler"

    #@7
    const-string v1, "finish - cleanup Service here"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 588
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@e
    if-eqz v0, :cond_da

    #@10
    .line 590
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@12
    const/4 v1, 0x1

    #@13
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@16
    .line 591
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@18
    const/4 v1, 0x2

    #@19
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@1c
    .line 592
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@1e
    const/4 v1, 0x3

    #@1f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@22
    .line 593
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@24
    const/4 v1, 0x4

    #@25
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@28
    .line 594
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@2a
    const/4 v1, 0x5

    #@2b
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@2e
    .line 595
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@30
    const/4 v1, 0x6

    #@31
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@34
    .line 596
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@36
    const/4 v1, 0x7

    #@37
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@3a
    .line 597
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@3c
    const/16 v1, 0x8

    #@3e
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@41
    .line 598
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@43
    const/16 v1, 0x9

    #@45
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@48
    .line 599
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@4a
    const/16 v1, 0xa

    #@4c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@4f
    .line 600
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@51
    const/16 v1, 0xb

    #@53
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@56
    .line 601
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@58
    const/16 v1, 0xc

    #@5a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@5d
    .line 602
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@5f
    const/16 v1, 0xd

    #@61
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@64
    .line 603
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@66
    const/16 v1, 0xe

    #@68
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@6b
    .line 604
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@6d
    const/16 v1, 0xf

    #@6f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@72
    .line 606
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@74
    const/16 v1, 0x10

    #@76
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@79
    .line 607
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@7b
    const/16 v1, 0x11

    #@7d
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@80
    .line 608
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@82
    const/16 v1, 0x12

    #@84
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@87
    .line 609
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@89
    const/16 v1, 0x13

    #@8b
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@8e
    .line 610
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@90
    const/16 v1, 0x14

    #@92
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@95
    .line 611
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@97
    const/16 v1, 0x15

    #@99
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@9c
    .line 612
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@9e
    const/16 v1, 0x16

    #@a0
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@a3
    .line 613
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@a5
    const/16 v1, 0x17

    #@a7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@aa
    .line 614
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@ac
    const/16 v1, 0x18

    #@ae
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@b1
    .line 615
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@b3
    const/16 v1, 0x19

    #@b5
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@b8
    .line 617
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@ba
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@bd
    .line 618
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@bf
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@c2
    .line 619
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@c4
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@c7
    .line 620
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@c9
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@cc
    .line 621
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@ce
    const/16 v1, 0x1388

    #@d0
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d3
    .line 622
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@d5
    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@d8
    .line 623
    iput-object v4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@da
    .line 625
    :cond_da
    invoke-virtual {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->clearAllQueue()V

    #@dd
    .line 626
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@df
    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    #@e2
    .line 627
    return-void
.end method

.method public declared-synchronized getIsMute()Z
    .registers 2

    #@0
    .prologue
    .line 513
    monitor-enter p0

    #@1
    :try_start_1
    sget-boolean v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mIsMute:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized getMonoStereoMode()I
    .registers 2

    #@0
    .prologue
    .line 495
    monitor-enter p0

    #@1
    :try_start_1
    sget v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAudioMode:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public getRadioIsOn()Z
    .registers 2

    #@0
    .prologue
    .line 483
    sget-boolean v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRadioIsOn:Z

    #@2
    return v0
.end method

.method public declared-synchronized getStatus()I
    .registers 3

    #@0
    .prologue
    .line 1205
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_GET_STATUS:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1206
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1205
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized getTunedFrequency()I
    .registers 2

    #@0
    .prologue
    .line 504
    monitor-enter p0

    #@1
    :try_start_1
    sget v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFreq:I
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public declared-synchronized muteAudio(Z)I
    .registers 4
    .parameter "mute"

    #@0
    .prologue
    .line 1235
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_MUTE_AUDIO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;Z)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1236
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1235
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public onRadioAfJumpEvent(III)V
    .registers 8
    .parameter "status"
    .parameter "rssi"
    .parameter "freq"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 2843
    const-string v0, "FmNativehandler"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "onRadioAfJumpEvent: status = "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ", rssi = "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, ", freq = "

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, ")"

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 2846
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@35
    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    #@38
    .line 2847
    sput p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@3a
    .line 2848
    sput p3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFreq:I

    #@3c
    .line 2849
    sput-boolean v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSeekSuccess:Z

    #@3e
    .line 2850
    invoke-direct {p0, v3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendSeekCompleteEventCallbackFromLocalStore(Z)V

    #@41
    .line 2853
    return-void
.end method

.method public onRadioAudioDataEvent(IIII)V
    .registers 7
    .parameter "status"
    .parameter "rssi"
    .parameter "snr"
    .parameter "mode"

    #@0
    .prologue
    .line 2890
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "onRadioAudioDataEvent()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2893
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 2894
    const/4 v0, 0x2

    #@e
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@10
    .line 2897
    if-nez p1, :cond_18

    #@12
    .line 2898
    sput p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@14
    .line 2899
    sput p3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSnr:I

    #@16
    .line 2900
    sput p4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAudioMode:I

    #@18
    .line 2902
    :cond_18
    invoke-direct {p0, p2, p3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendLiveAudioQualityEventCallbackFromLocalStore(II)V

    #@1b
    .line 2903
    return-void
.end method

.method public onRadioAudioModeEvent(II)V
    .registers 5
    .parameter "status"
    .parameter "mode"

    #@0
    .prologue
    .line 2857
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "onRadioAudioModeEvent()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2861
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 2865
    if-nez p1, :cond_11

    #@f
    .line 2866
    sput p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAudioMode:I

    #@11
    .line 2868
    :cond_11
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendAudioModeEventCallbackFromLocalStore()V

    #@14
    .line 2870
    return-void
.end method

.method public onRadioAudioPathEvent(II)V
    .registers 5
    .parameter "status"
    .parameter "path"

    #@0
    .prologue
    .line 2874
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "onRadioAudioPathEvent()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2878
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 2882
    if-nez p1, :cond_11

    #@f
    .line 2883
    sput p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAudioPath:I

    #@11
    .line 2885
    :cond_11
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendAudioPathEventCallbackFromLocalStore()V

    #@14
    .line 2887
    return-void
.end method

.method public onRadioDeemphEvent(II)V
    .registers 5
    .parameter "status"
    .parameter "time"

    #@0
    .prologue
    .line 2993
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "onRadioDeemphEvent()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2997
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 3000
    const/4 v0, 0x2

    #@e
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@10
    .line 3001
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@13
    .line 3002
    return-void
.end method

.method public onRadioMuteEvent(IZ)V
    .registers 6
    .parameter "status"
    .parameter "muted"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2768
    const-string v0, "FmNativehandler"

    #@3
    const-string v1, "onRadioMuteEvent()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2772
    if-nez p1, :cond_c

    #@a
    .line 2773
    sput-boolean p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mIsMute:Z

    #@c
    .line 2777
    :cond_c
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@11
    .line 2781
    invoke-direct {p0, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendStatusEventCallbackFromLocalStore(Z)V

    #@14
    .line 2783
    return-void
.end method

.method public onRadioNflEstimationEvent(I)V
    .registers 4
    .parameter "level"

    #@0
    .prologue
    .line 3032
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "onRadioNflEstimationEvent()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3036
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 3039
    const/4 v0, 0x2

    #@e
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@10
    .line 3042
    sput p1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mEstimatedNoiseFloorLevel:I

    #@12
    .line 3043
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendEstimateNflEventCallbackFromLocalStore()V

    #@15
    .line 3045
    return-void
.end method

.method public onRadioOffEvent(I)V
    .registers 4
    .parameter "status"

    #@0
    .prologue
    .line 2760
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "onRadioOffEvent()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2763
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@9
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_CHIP_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@b
    invoke-direct {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;)V

    #@e
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V

    #@11
    .line 2764
    return-void
.end method

.method public onRadioOnEvent(I)V
    .registers 5
    .parameter "status"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2739
    const-string v0, "FmNativehandler"

    #@3
    const-string v1, "onRadioOnEvent()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2742
    if-nez p1, :cond_c

    #@a
    .line 2743
    sput-boolean v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRadioIsOn:Z

    #@c
    .line 2747
    :cond_c
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@11
    .line 2750
    sget-boolean v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRadioIsOn:Z

    #@13
    if-nez v0, :cond_18

    #@15
    .line 2751
    const/4 v0, 0x0

    #@16
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@18
    .line 2755
    :cond_18
    invoke-direct {p0, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendStatusEventCallbackFromLocalStore(Z)V

    #@1b
    .line 2756
    return-void
.end method

.method public onRadioRdsModeEvent(IZZI)V
    .registers 8
    .parameter "status"
    .parameter "rdsOn"
    .parameter "afOn"
    .parameter "rdsType"

    #@0
    .prologue
    .line 2907
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "onRadioRdsModeEvent()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2911
    if-nez p1, :cond_4f

    #@9
    .line 2912
    sput-boolean p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsOn:Z

    #@b
    .line 2913
    sput-boolean p3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mAfOn:Z

    #@d
    .line 2914
    sput p4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsType:I

    #@f
    .line 2916
    const-string v0, "FmNativehandler"

    #@11
    new-instance v1, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v2, "onRadioRdsModeEvent( rdsOn "

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    const-string v2, ", afOn"

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-static {p3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    const-string v2, ","

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    sget v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsType:I

    #@3a
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    const-string v2, ")"

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v1

    #@4c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 2923
    :cond_4f
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@51
    const/4 v1, 0x1

    #@52
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@55
    .line 2925
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendRdsModeEventCallbackFromLocalStore()V

    #@58
    .line 2926
    const/4 v0, 0x0

    #@59
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_op_state:I

    #@5b
    .line 2930
    return-void
.end method

.method public onRadioRdsTypeEvent(II)V
    .registers 6
    .parameter "status"
    .parameter "rdsType"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2935
    const-string v0, "FmNativehandler"

    #@3
    const-string v1, "onRadioRdsTypeEvent()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2939
    if-nez p1, :cond_c

    #@a
    .line 2940
    sput p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsType:I

    #@c
    .line 2943
    :cond_c
    sget v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_op_state:I

    #@e
    if-ne v2, v0, :cond_1c

    #@10
    .line 2946
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@12
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@15
    .line 2948
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendRdsModeEventCallbackFromLocalStore()V

    #@18
    .line 2949
    const/4 v0, 0x0

    #@19
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_op_state:I

    #@1b
    .line 2954
    :goto_1b
    return-void

    #@1c
    .line 2952
    :cond_1c
    const/4 v0, 0x2

    #@1d
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_op_state:I

    #@1f
    goto :goto_1b
.end method

.method public onRadioRdsUpdateEvent(IIILjava/lang/String;)V
    .registers 8
    .parameter "status"
    .parameter "data"
    .parameter "index"
    .parameter "text"

    #@0
    .prologue
    .line 2958
    const-string v0, "FmNativehandler"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onRadioRdsUpdateEvent("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ","

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, ","

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string v2, ","

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    const-string v2, ")"

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v1

    #@45
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 2962
    if-nez p1, :cond_50

    #@4a
    .line 2964
    packed-switch p2, :pswitch_data_64

    #@4d
    .line 2987
    :goto_4d
    :pswitch_4d
    invoke-direct {p0, p2, p3, p4}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendRdsDataEventCallbackFromLocalStore(IILjava/lang/String;)V

    #@50
    .line 2989
    :cond_50
    return-void

    #@51
    .line 2966
    :pswitch_51
    sput p3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramType:I

    #@53
    goto :goto_4d

    #@54
    .line 2969
    :pswitch_54
    sput-object p4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramTypeName:Ljava/lang/String;

    #@56
    goto :goto_4d

    #@57
    .line 2972
    :pswitch_57
    sput-object p4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsRadioText:Ljava/lang/String;

    #@59
    goto :goto_4d

    #@5a
    .line 2975
    :pswitch_5a
    sput-object p4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramService:Ljava/lang/String;

    #@5c
    goto :goto_4d

    #@5d
    .line 2979
    :pswitch_5d
    invoke-static {p4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@60
    move-result v0

    #@61
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramIdentification:I

    #@63
    goto :goto_4d

    #@64
    .line 2964
    :pswitch_data_64
    .packed-switch 0x1
        :pswitch_5d
        :pswitch_51
        :pswitch_4d
        :pswitch_4d
        :pswitch_4d
        :pswitch_4d
        :pswitch_5a
        :pswitch_54
        :pswitch_57
    .end packed-switch
.end method

.method public onRadioRegionEvent(II)V
    .registers 5
    .parameter "status"
    .parameter "region"

    #@0
    .prologue
    .line 3019
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "onRadioRegionEvent()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3023
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 3025
    sput p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mWorldRegion:I

    #@f
    .line 3026
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendWorldRegionEventCallbackFromLocalStore()V

    #@12
    .line 3028
    return-void
.end method

.method public onRadioScanStepEvent(I)V
    .registers 4
    .parameter "stepSize"

    #@0
    .prologue
    .line 3006
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "onRadioScanStepEvent()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3010
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 3013
    const/4 v0, 0x2

    #@e
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@10
    .line 3014
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->fetchNextJob()V

    #@13
    .line 3015
    return-void
.end method

.method public onRadioSearchCompleteEvent(IIII)V
    .registers 8
    .parameter "status"
    .parameter "rssi"
    .parameter "snr"
    .parameter "freq"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2822
    const-string v0, "FmNativehandler"

    #@3
    const-string v1, "onRadioSearchCompleteEvent()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2826
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 2829
    sput p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@f
    .line 2830
    sput p3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSnr:I

    #@11
    .line 2831
    sput p4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFreq:I

    #@13
    .line 2833
    const/4 v0, 0x0

    #@14
    sput-boolean v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSeekSuccess:Z

    #@16
    .line 2838
    invoke-direct {p0, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendSeekCompleteEventCallbackFromLocalStore(Z)V

    #@19
    .line 2840
    return-void
.end method

.method public onRadioSearchEvent(III)V
    .registers 7
    .parameter "rssi"
    .parameter "snr"
    .parameter "freq"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2806
    const-string v0, "FmNativehandler"

    #@3
    const-string v1, "onRadioSearchEvent()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2810
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 2813
    sput p1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@f
    .line 2814
    sput p3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFreq:I

    #@11
    .line 2815
    sput p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSnr:I

    #@13
    .line 2816
    sput-boolean v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSeekSuccess:Z

    #@15
    .line 2817
    const/4 v0, 0x0

    #@16
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendSeekCompleteEventCallbackFromLocalStore(Z)V

    #@19
    .line 2818
    return-void
.end method

.method public onRadioTuneEvent(IIII)V
    .registers 8
    .parameter "status"
    .parameter "rssi"
    .parameter "snr"
    .parameter "freq"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2787
    const-string v0, "FmNativehandler"

    #@3
    const-string v1, "onRadioTuneEvent()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 2791
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 2795
    if-nez p1, :cond_15

    #@f
    .line 2796
    sput p2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@11
    .line 2797
    sput p3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSnr:I

    #@13
    .line 2798
    sput p4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFreq:I

    #@15
    .line 2800
    :cond_15
    invoke-direct {p0, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendStatusEventCallbackFromLocalStore(Z)V

    #@18
    .line 2802
    return-void
.end method

.method public onRadioVolumeEvent(II)V
    .registers 5
    .parameter "status"
    .parameter "volume"

    #@0
    .prologue
    .line 3049
    const-string v0, "FmNativehandler"

    #@2
    const-string v1, "onRadioVolumeEvent()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3053
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    #@d
    .line 3055
    invoke-direct {p0, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->sendVolumeEventCallbackFromLocalStore(II)V

    #@10
    .line 3056
    return-void
.end method

.method public declared-synchronized process_disableChip()I
    .registers 4

    #@0
    .prologue
    .line 1078
    monitor-enter p0

    #@1
    :try_start_1
    const-string v1, "FmNativehandler"

    #@3
    const-string v2, "processDisableChip()"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1080
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@b
    move-result-object v0

    #@c
    .line 1081
    .local v0, btAdap:Landroid/bluetooth/BluetoothAdapter;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disableRadio()Z
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    #@f
    .line 1082
    const/4 v1, 0x0

    #@10
    monitor-exit p0

    #@11
    return v1

    #@12
    .line 1078
    .end local v0           #btAdap:Landroid/bluetooth/BluetoothAdapter;
    :catchall_12
    move-exception v1

    #@13
    monitor-exit p0

    #@14
    throw v1
.end method

.method public declared-synchronized process_enableChip()I
    .registers 7

    #@0
    .prologue
    .line 1045
    monitor-enter p0

    #@1
    const/4 v2, 0x0

    #@2
    .line 1047
    .local v2, returnStatus:I
    :try_start_2
    const-string v3, "FmNativehandler"

    #@4
    const-string v4, "processEnableChip()"

    #@6
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 1050
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@b
    if-eqz v3, :cond_2e

    #@d
    .line 1052
    const-string v3, "FmNativehandler"

    #@f
    new-instance v4, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v5, "STATE = "

    #@16
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v4

    #@1a
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@1c
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@1f
    move-result-object v5

    #@20
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v4

    #@24
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2b
    .catchall {:try_start_2 .. :try_end_2b} :catchall_58

    #@2b
    .line 1054
    const/4 v2, 0x3

    #@2c
    .line 1072
    :cond_2c
    :goto_2c
    monitor-exit p0

    #@2d
    return v2

    #@2e
    .line 1056
    :cond_2e
    const/4 v3, 0x5

    #@2f
    :try_start_2f
    sput v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@31
    .line 1058
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@34
    move-result-object v1

    #@35
    .line 1059
    .local v1, msg:Landroid/os/Message;
    const/4 v3, 0x1

    #@36
    iput v3, v1, Landroid/os/Message;->what:I

    #@38
    .line 1060
    const/4 v3, 0x2

    #@39
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@3b
    .line 1061
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@3d
    const/4 v4, 0x1

    #@3e
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    #@41
    .line 1062
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@43
    const-wide/16 v4, 0x2710

    #@45
    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@48
    .line 1065
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@4b
    move-result-object v0

    #@4c
    .line 1066
    .local v0, btAdap:Landroid/bluetooth/BluetoothAdapter;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enableRadio()Z

    #@4f
    .line 1067
    if-eqz v2, :cond_2c

    #@51
    .line 1068
    iget-object v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@53
    const/4 v4, 0x1

    #@54
    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V
    :try_end_57
    .catchall {:try_start_2f .. :try_end_57} :catchall_58

    #@57
    goto :goto_2c

    #@58
    .line 1045
    .end local v0           #btAdap:Landroid/bluetooth/BluetoothAdapter;
    .end local v1           #msg:Landroid/os/Message;
    :catchall_58
    move-exception v3

    #@59
    monitor-exit p0

    #@5a
    throw v3
.end method

.method public registerCallback(Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;)V
    .registers 3
    .parameter "cb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 517
    if-eqz p1, :cond_7

    #@2
    .line 518
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@4
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@7
    .line 520
    :cond_7
    return-void
.end method

.method public declared-synchronized seekRdsStation(IIII)I
    .registers 11
    .parameter "scanMode"
    .parameter "minSignalStrength"
    .parameter "rdsCondition"
    .parameter "rdsValue"

    #@0
    .prologue
    .line 1460
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_RDS_STATION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    move v2, p1

    #@6
    move v3, p2

    #@7
    move v4, p3

    #@8
    move v5, p4

    #@9
    invoke-direct/range {v0 .. v5}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;IIII)V

    #@c
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    #@f
    .line 1462
    const/4 v0, 0x0

    #@10
    monitor-exit p0

    #@11
    return v0

    #@12
    .line 1460
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public declared-synchronized seekStation(II)I
    .registers 5
    .parameter "scanMode"
    .parameter "minSignalStrength"

    #@0
    .prologue
    .line 1293
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;II)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1294
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1293
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized seekStationAbort()I
    .registers 5

    #@0
    .prologue
    .line 1529
    monitor-enter p0

    #@1
    :try_start_1
    const-string v2, "FmNativehandler"

    #@3
    const-string v3, "seekStationAbort()"

    #@5
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1531
    const/4 v1, 0x3

    #@9
    .line 1533
    .local v1, returnStatus:I
    iget v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->current_CMD:I

    #@b
    const/4 v3, -0x1

    #@c
    if-eq v2, v3, :cond_1a

    #@e
    iget v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->current_CMD:I
    :try_end_10
    .catchall {:try_start_1 .. :try_end_10} :catchall_28

    #@10
    const/4 v3, 0x4

    #@11
    if-ne v2, v3, :cond_1a

    #@13
    .line 1535
    :try_start_13
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->searchAbortNative()Z
    :try_end_16
    .catchall {:try_start_13 .. :try_end_16} :catchall_28
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_16} :catch_1e

    #@16
    move-result v2

    #@17
    if-eqz v2, :cond_1c

    #@19
    .line 1536
    const/4 v1, 0x0

    #@1a
    .line 1545
    :cond_1a
    :goto_1a
    monitor-exit p0

    #@1b
    return v1

    #@1c
    .line 1538
    :cond_1c
    const/4 v1, 0x2

    #@1d
    goto :goto_1a

    #@1e
    .line 1540
    :catch_1e
    move-exception v0

    #@1f
    .line 1541
    .local v0, e:Ljava/lang/Exception;
    const/4 v1, 0x2

    #@20
    .line 1542
    :try_start_20
    const-string v2, "FmNativehandler"

    #@22
    const-string v3, "searchAbortNative failed"

    #@24
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_27
    .catchall {:try_start_20 .. :try_end_27} :catchall_28

    #@27
    goto :goto_1a

    #@28
    .line 1529
    .end local v0           #e:Ljava/lang/Exception;
    .end local v1           #returnStatus:I
    :catchall_28
    move-exception v2

    #@29
    monitor-exit p0

    #@2a
    throw v2
.end method

.method public declared-synchronized seekStationCombo(IIIIIZII)I
    .registers 19
    .parameter "startFreq"
    .parameter "endFreq"
    .parameter "minSignalStrength"
    .parameter "direction"
    .parameter "scanMethod"
    .parameter "multiChannel"
    .parameter "rdsType"
    .parameter "rdsTypeValue"

    #@0
    .prologue
    .line 1378
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION_COMBO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    move/from16 v2, p6

    #@7
    move v3, p1

    #@8
    move v4, p2

    #@9
    move v5, p3

    #@a
    move v6, p4

    #@b
    move v7, p5

    #@c
    move/from16 v8, p7

    #@e
    move/from16 v9, p8

    #@10
    invoke-direct/range {v0 .. v9}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;ZIIIIIII)V

    #@13
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_19

    #@16
    .line 1381
    const/4 v0, 0x0

    #@17
    monitor-exit p0

    #@18
    return v0

    #@19
    .line 1378
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit p0

    #@1b
    throw v0
.end method

.method public declared-synchronized setAudioMode(I)I
    .registers 4
    .parameter "audioMode"

    #@0
    .prologue
    .line 1646
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_AUDIO_MODE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;I)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1647
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1646
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized setAudioPath(I)I
    .registers 4
    .parameter "audioPath"

    #@0
    .prologue
    .line 1710
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_AUDIO_PATH:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;I)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1711
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1710
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized setFMVolume(I)I
    .registers 4
    .parameter "volume"

    #@0
    .prologue
    .line 1841
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_VOLUME:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;I)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1842
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1841
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized setLiveAudioPolling(ZI)I
    .registers 5
    .parameter "liveAudioPolling"
    .parameter "signalPollInterval"

    #@0
    .prologue
    .line 2036
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_LIVE_AUDIO_POLLING:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;ZI)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 2038
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 2036
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized setRdsMode(IIII)I
    .registers 11
    .parameter "rdsMode"
    .parameter "rdsFeatures"
    .parameter "afMode"
    .parameter "afThreshold"

    #@0
    .prologue
    .line 1570
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_RDS_MODE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    move v2, p1

    #@6
    move v3, p2

    #@7
    move v4, p3

    #@8
    move v5, p4

    #@9
    invoke-direct/range {v0 .. v5}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;IIII)V

    #@c
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_12

    #@f
    .line 1572
    const/4 v0, 0x0

    #@10
    monitor-exit p0

    #@11
    return v0

    #@12
    .line 1570
    :catchall_12
    move-exception v0

    #@13
    monitor-exit p0

    #@14
    throw v0
.end method

.method public declared-synchronized setSnrThreshold(I)I
    .registers 8
    .parameter "snrThreshold"

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    .line 2693
    monitor-enter p0

    #@2
    const/4 v1, 0x0

    #@3
    .line 2696
    .local v1, returnStatus:I
    :try_start_3
    const-string v2, "FmNativehandler"

    #@5
    new-instance v3, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "setSnrThreshold() - "

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 2700
    if-ltz p1, :cond_21

    #@1d
    const/16 v2, 0x1f

    #@1f
    if-le p1, v2, :cond_42

    #@21
    .line 2702
    :cond_21
    const/4 v1, 0x4

    #@22
    .line 2729
    :goto_22
    const-string v2, "FmNativehandler"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "STATE = "

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@31
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v3

    #@3d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_40
    .catchall {:try_start_3 .. :try_end_40} :catchall_74

    #@40
    .line 2731
    monitor-exit p0

    #@41
    return v1

    #@42
    .line 2703
    :cond_42
    :try_start_42
    sget v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@44
    if-eq v5, v2, :cond_66

    #@46
    .line 2706
    const-string v2, "FmNativehandler"

    #@48
    new-instance v3, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v4, "STATE = "

    #@4f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    sget v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@55
    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 2708
    const/4 v1, 0x3

    #@65
    goto :goto_22

    #@66
    .line 2710
    :cond_66
    const/4 v2, 0x2

    #@67
    sput v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I
    :try_end_69
    .catchall {:try_start_42 .. :try_end_69} :catchall_74

    #@69
    .line 2714
    :try_start_69
    invoke-direct {p0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setSnrThresholdNative(I)Z
    :try_end_6c
    .catchall {:try_start_69 .. :try_end_6c} :catchall_74
    .catch Ljava/lang/Exception; {:try_start_69 .. :try_end_6c} :catch_79

    #@6c
    move-result v2

    #@6d
    if-eqz v2, :cond_77

    #@6f
    .line 2715
    const/4 v1, 0x0

    #@70
    .line 2724
    :goto_70
    const/4 v2, 0x2

    #@71
    :try_start_71
    sput v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I
    :try_end_73
    .catchall {:try_start_71 .. :try_end_73} :catchall_74

    #@73
    goto :goto_22

    #@74
    .line 2693
    :catchall_74
    move-exception v2

    #@75
    monitor-exit p0

    #@76
    throw v2

    #@77
    .line 2717
    :cond_77
    const/4 v1, 0x2

    #@78
    goto :goto_70

    #@79
    .line 2719
    :catch_79
    move-exception v0

    #@7a
    .line 2720
    .local v0, e:Ljava/lang/Exception;
    const/4 v1, 0x2

    #@7b
    .line 2721
    :try_start_7b
    const-string v2, "FmNativehandler"

    #@7d
    const-string v3, "setSnrThreshold failed"

    #@7f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_82
    .catchall {:try_start_7b .. :try_end_82} :catchall_74

    #@82
    goto :goto_70
.end method

.method public declared-synchronized setStepSize(I)I
    .registers 4
    .parameter "stepSize"

    #@0
    .prologue
    .line 1782
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_STEP_SIZE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;I)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1783
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1782
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized setWorldRegion(II)I
    .registers 5
    .parameter "worldRegion"
    .parameter "deemphasisTime"

    #@0
    .prologue
    .line 1907
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_WORLD_REGION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;II)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1908
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1907
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized start()V
    .registers 3

    #@0
    .prologue
    .line 545
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "FmNativehandler"

    #@3
    const-string v1, "start"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 547
    iget-boolean v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIsStarted:Z

    #@a
    if-eqz v0, :cond_15

    #@c
    .line 548
    const-string v0, "FmNativehandler"

    #@e
    const-string v1, "Service already started...Skipping"

    #@10
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_19

    #@13
    .line 553
    :goto_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 551
    :cond_15
    const/4 v0, 0x1

    #@16
    :try_start_16
    iput-boolean v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIsStarted:Z
    :try_end_18
    .catchall {:try_start_16 .. :try_end_18} :catchall_19

    #@18
    goto :goto_13

    #@19
    .line 545
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit p0

    #@1b
    throw v0
.end method

.method public declared-synchronized stop()V
    .registers 4

    #@0
    .prologue
    .line 557
    monitor-enter p0

    #@1
    :try_start_1
    const-string v1, "FmNativehandler"

    #@3
    const-string v2, "stop"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 559
    iget-boolean v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIsStarted:Z

    #@a
    if-nez v1, :cond_15

    #@c
    .line 560
    const-string v1, "FmNativehandler"

    #@e
    const-string v2, "Service already stopped...Skipping"

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_13
    .catchall {:try_start_1 .. :try_end_13} :catchall_3e

    #@13
    .line 577
    :goto_13
    monitor-exit p0

    #@14
    return-void

    #@15
    .line 564
    :cond_15
    :try_start_15
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->unRegisterIntent()V

    #@18
    .line 565
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@1b
    move-result-object v0

    #@1c
    .line 567
    .local v0, btAdap:Landroid/bluetooth/BluetoothAdapter;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isRadioEnabled()Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_37

    #@22
    .line 568
    const-string v1, "FmNativehandler"

    #@24
    const-string v2, "Disable radio if app failed to disable radio"

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 569
    const/4 v1, 0x0

    #@2a
    invoke-direct {p0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setAudioPathNative(I)Z

    #@2d
    .line 570
    const/4 v1, 0x0

    #@2e
    invoke-direct {p0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->disableFmNative(Z)Z

    #@31
    .line 571
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disableRadio()Z

    #@34
    .line 572
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->initializeStateMachine()V

    #@37
    .line 574
    :cond_37
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->cleanupNative()V

    #@3a
    .line 575
    const/4 v1, 0x0

    #@3b
    iput-boolean v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mIsStarted:Z
    :try_end_3d
    .catchall {:try_start_15 .. :try_end_3d} :catchall_3e

    #@3d
    goto :goto_13

    #@3e
    .line 557
    .end local v0           #btAdap:Landroid/bluetooth/BluetoothAdapter;
    :catchall_3e
    move-exception v1

    #@3f
    monitor-exit p0

    #@40
    throw v1
.end method

.method public declared-synchronized tuneRadio(I)I
    .registers 4
    .parameter "freq"

    #@0
    .prologue
    .line 1153
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_TUNE_RADIO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;I)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1154
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1153
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized turnOffRadio()I
    .registers 3

    #@0
    .prologue
    .line 1038
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    invoke-direct {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;)V

    #@8
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_b
    .catchall {:try_start_1 .. :try_end_b} :catchall_e

    #@b
    .line 1040
    const/4 v0, 0x0

    #@c
    monitor-exit p0

    #@d
    return v0

    #@e
    .line 1038
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method public declared-synchronized turnOnRadio(I[C)I
    .registers 6
    .parameter "functionalityMask"
    .parameter "clientPackagename"

    #@0
    .prologue
    .line 832
    monitor-enter p0

    #@1
    :try_start_1
    invoke-static {p2}, Ljava/lang/String;->copyValueOf([C)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mClientName:Ljava/lang/String;

    #@7
    .line 834
    const-string v0, "FmNativehandler"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "turnOnRadio: functionalityMask = "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    const-string v2, ", clientPackagename = "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 838
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->registerIntent()V

    #@2c
    .line 839
    iput p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mFunctionalityMask:I

    #@2e
    .line 841
    sget v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->instance:I

    #@30
    add-int/lit8 v0, v0, 0x1

    #@32
    sput v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->instance:I

    #@34
    .line 842
    const-string v0, "FmNativehandler"

    #@36
    new-instance v1, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v2, "instance"

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    sget v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->instance:I

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 844
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@50
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_CHIP_ON:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@52
    invoke-direct {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;)V

    #@55
    invoke-direct {p0, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->queueFMCommand(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V
    :try_end_58
    .catchall {:try_start_1 .. :try_end_58} :catchall_5b

    #@58
    .line 847
    const/4 v0, 0x0

    #@59
    monitor-exit p0

    #@5a
    return v0

    #@5b
    .line 832
    :catchall_5b
    move-exception v0

    #@5c
    monitor-exit p0

    #@5d
    throw v0
.end method

.method public declared-synchronized unregisterCallback(Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;)V
    .registers 3
    .parameter "cb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 523
    monitor-enter p0

    #@1
    if-eqz p1, :cond_8

    #@3
    .line 524
    :try_start_3
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@5
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z
    :try_end_8
    .catchall {:try_start_3 .. :try_end_8} :catchall_a

    #@8
    .line 526
    :cond_8
    monitor-exit p0

    #@9
    return-void

    #@a
    .line 523
    :catchall_a
    move-exception v0

    #@b
    monitor-exit p0

    #@c
    throw v0
.end method
