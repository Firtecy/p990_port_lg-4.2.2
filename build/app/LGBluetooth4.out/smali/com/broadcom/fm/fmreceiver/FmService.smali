.class public Lcom/broadcom/fm/fmreceiver/FmService;
.super Landroid/app/Service;
.source "FmService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;
    }
.end annotation


# static fields
.field private static final D:Z = true

.field public static final TAG:Ljava/lang/String; = "FmService"

.field private static final V:Z = true


# instance fields
.field private mBinder:Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;

.field public mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 70
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@5
    invoke-direct {v0, p0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;-><init>(Landroid/content/Context;)V

    #@8
    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@a
    .line 76
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 107
    const-string v0, "FmService"

    #@2
    const-string v1, "Binding service..."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 109
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService;->mBinder:Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;

    #@9
    return-object v0
.end method

.method public onCreate()V
    .registers 3

    #@0
    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 81
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;

    #@5
    invoke-direct {v0, p0}, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;-><init>(Lcom/broadcom/fm/fmreceiver/FmService;)V

    #@8
    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService;->mBinder:Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;

    #@a
    .line 83
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService;->mBinder:Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;

    #@c
    #getter for: Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->access$000(Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;)Lcom/broadcom/fm/fmreceiver/FmService;

    #@f
    move-result-object v0

    #@10
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@12
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->start()V

    #@15
    .line 85
    const-string v0, "FmService"

    #@17
    const-string v1, "FM Service  onCreate"

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 87
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 97
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@3
    .line 98
    const-string v0, "FmService"

    #@5
    const-string v1, "onDestroy"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 99
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService;->mBinder:Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;

    #@c
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->cleanUp()V

    #@f
    .line 100
    const/4 v0, 0x0

    #@10
    iput-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService;->mBinder:Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;

    #@12
    .line 102
    const-string v0, "FmService"

    #@14
    const-string v1, "onDestroy done"

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 103
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 92
    const/4 v0, 0x2

    #@1
    return v0
.end method
