.class Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;
.super Landroid/os/Handler;
.source "FmNativehandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmNativehandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;


# direct methods
.method constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 697
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 14
    .parameter "msg"

    #@0
    .prologue
    .line 700
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_16a

    #@5
    .line 796
    :pswitch_5
    const-string v0, "FmNativehandler"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "Handling UNKNOWN CALLBACK: "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget v2, p1, Landroid/os/Message;->what:I

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 799
    :goto_1f
    return-void

    #@20
    .line 703
    :pswitch_20
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@22
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@24
    invoke-static {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$000(Lcom/broadcom/fm/fmreceiver/FmNativehandler;I)V

    #@27
    .line 705
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@29
    const/4 v1, 0x2

    #@2a
    if-ne v0, v1, :cond_35

    #@2c
    .line 707
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@2e
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$100(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)V

    #@31
    .line 708
    const/4 v0, 0x0

    #@32
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@34
    goto :goto_1f

    #@35
    .line 709
    :cond_35
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@37
    const/4 v1, 0x3

    #@38
    if-ne v0, v1, :cond_49

    #@3a
    .line 711
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@3c
    invoke-static {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$100(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)V

    #@3f
    .line 713
    :try_start_3f
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@41
    const/4 v1, 0x1

    #@42
    invoke-static {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$200(Lcom/broadcom/fm/fmreceiver/FmNativehandler;Z)Z
    :try_end_45
    .catch Ljava/lang/Exception; {:try_start_3f .. :try_end_45} :catch_167

    #@45
    .line 715
    :goto_45
    const/4 v0, 0x0

    #@46
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@48
    goto :goto_1f

    #@49
    .line 716
    :cond_49
    iget v0, p1, Landroid/os/Message;->arg1:I

    #@4b
    const/4 v1, 0x4

    #@4c
    if-ne v0, v1, :cond_63

    #@4e
    .line 718
    const/4 v0, 0x2

    #@4f
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@51
    .line 719
    const/4 v0, 0x1

    #@52
    sput-boolean v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSeekSuccess:Z

    #@54
    .line 720
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@56
    sget v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFreq:I

    #@58
    sget v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@5a
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSnr:I

    #@5c
    sget-boolean v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSeekSuccess:Z

    #@5e
    const/4 v5, 0x1

    #@5f
    invoke-static/range {v0 .. v5}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$300(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IIIZI)V

    #@62
    goto :goto_1f

    #@63
    .line 723
    :cond_63
    const/4 v0, 0x2

    #@64
    sput v0, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@66
    .line 724
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@68
    sget v1, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mFreq:I

    #@6a
    sget v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRssi:I

    #@6c
    sget v3, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mSnr:I

    #@6e
    sget-boolean v4, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRadioIsOn:Z

    #@70
    sget v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramType:I

    #@72
    sget-object v6, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramService:Ljava/lang/String;

    #@74
    sget-object v7, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsRadioText:Ljava/lang/String;

    #@76
    sget-object v8, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRdsProgramTypeName:Ljava/lang/String;

    #@78
    sget-boolean v9, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mIsMute:Z

    #@7a
    const/4 v10, 0x1

    #@7b
    invoke-static/range {v0 .. v10}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$400(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    #@7e
    goto :goto_1f

    #@7f
    .line 738
    :pswitch_7f
    const-string v0, "FmNativehandler"

    #@81
    const-string v1, "Handling OPERATION_STATUS_EVENT_CALLBACK: calls sendStatusEventCallback"

    #@83
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 739
    iget-object v11, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@88
    check-cast v11, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;

    #@8a
    .line 740
    .local v11, st:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@8c
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->access$500(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)I

    #@8f
    move-result v1

    #@90
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->access$600(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)I

    #@93
    move-result v2

    #@94
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->access$700(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)I

    #@97
    move-result v3

    #@98
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->access$800(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)Z

    #@9b
    move-result v4

    #@9c
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->access$900(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)I

    #@9f
    move-result v5

    #@a0
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->access$1000(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)Ljava/lang/String;

    #@a3
    move-result-object v6

    #@a4
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->access$1100(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)Ljava/lang/String;

    #@a7
    move-result-object v7

    #@a8
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->access$1200(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)Ljava/lang/String;

    #@ab
    move-result-object v8

    #@ac
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->access$1300(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)Z

    #@af
    move-result v9

    #@b0
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@b2
    invoke-static/range {v0 .. v10}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$400(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V

    #@b5
    goto/16 :goto_1f

    #@b7
    .line 749
    .end local v11           #st:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;
    :pswitch_b7
    const-string v0, "FmNativehandler"

    #@b9
    const-string v1, "Handling OPERATION_SEARCH_EVENT_CALLBACK: calls sendSeekCompleteEventCallback"

    #@bb
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@be
    .line 750
    iget-object v11, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c0
    check-cast v11, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;

    #@c2
    .line 751
    .local v11, st:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c4
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->access$1400(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;)I

    #@c7
    move-result v1

    #@c8
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->access$1500(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;)I

    #@cb
    move-result v2

    #@cc
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->access$1600(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;)I

    #@cf
    move-result v3

    #@d0
    invoke-static {v11}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->access$1700(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;)Z

    #@d3
    move-result v4

    #@d4
    iget v5, p1, Landroid/os/Message;->arg1:I

    #@d6
    invoke-static/range {v0 .. v5}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$300(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IIIZI)V

    #@d9
    goto/16 :goto_1f

    #@db
    .line 755
    .end local v11           #st:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;
    :pswitch_db
    const-string v0, "FmNativehandler"

    #@dd
    const-string v1, "Handling OPERATION_RDS_EVENT_CALLBACK: calls sendRdsModeEventCallback"

    #@df
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e2
    .line 756
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@e4
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@e6
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@e8
    invoke-static {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$1800(Lcom/broadcom/fm/fmreceiver/FmNativehandler;II)V

    #@eb
    goto/16 :goto_1f

    #@ed
    .line 760
    :pswitch_ed
    const-string v0, "FmNativehandler"

    #@ef
    const-string v1, "Handling OPERATION_RDS_DATA_EVENT_CALLBACK: calls sendRdsDataEventCallback"

    #@f1
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 761
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@f6
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@f8
    iget v3, p1, Landroid/os/Message;->arg2:I

    #@fa
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@fc
    check-cast v0, Ljava/lang/String;

    #@fe
    invoke-static {v1, v2, v3, v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$1900(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IILjava/lang/String;)V

    #@101
    goto/16 :goto_1f

    #@103
    .line 765
    :pswitch_103
    const-string v0, "FmNativehandler"

    #@105
    const-string v1, "Handling OPERATION_AUDIO_MODE_EVENT_CALLBACK: calls sendAudioModeEventCallback"

    #@107
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    .line 766
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@10c
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@10e
    invoke-static {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$2000(Lcom/broadcom/fm/fmreceiver/FmNativehandler;I)V

    #@111
    goto/16 :goto_1f

    #@113
    .line 770
    :pswitch_113
    const-string v0, "FmNativehandler"

    #@115
    const-string v1, "Handling OPERATION_AUDIO_PATH_EVENT_CALLBACK: calls sendAudioPathEventCallback"

    #@117
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@11a
    .line 771
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@11c
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@11e
    invoke-static {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$2100(Lcom/broadcom/fm/fmreceiver/FmNativehandler;I)V

    #@121
    goto/16 :goto_1f

    #@123
    .line 775
    :pswitch_123
    const-string v0, "FmNativehandler"

    #@125
    const-string v1, "Handling OPERATION_REGION_EVENT_CALLBACK: calls sendWorldRegionEventCallback"

    #@127
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12a
    .line 776
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@12c
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@12e
    invoke-static {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$2200(Lcom/broadcom/fm/fmreceiver/FmNativehandler;I)V

    #@131
    goto/16 :goto_1f

    #@133
    .line 780
    :pswitch_133
    const-string v0, "FmNativehandler"

    #@135
    const-string v1, "Handling OPERATION_NFE_EVENT_CALLBACK: calls sendEstimateNflEventCallback"

    #@137
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13a
    .line 781
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@13c
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@13e
    invoke-static {v0, v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$2300(Lcom/broadcom/fm/fmreceiver/FmNativehandler;I)V

    #@141
    goto/16 :goto_1f

    #@143
    .line 785
    :pswitch_143
    const-string v0, "FmNativehandler"

    #@145
    const-string v1, "Handling OPERATION_LIVE_AUDIO_EVENT_CALLBACK: calls sendLiveAudioQualityEventCallback"

    #@147
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14a
    .line 786
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@14c
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@14e
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@150
    invoke-static {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$2400(Lcom/broadcom/fm/fmreceiver/FmNativehandler;II)V

    #@153
    goto/16 :goto_1f

    #@155
    .line 790
    :pswitch_155
    const-string v0, "FmNativehandler"

    #@157
    const-string v1, "Handling OPERATION_VOLUME_EVENT_CALLBACK: calls sendVolumeEventCallback"

    #@159
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15c
    .line 791
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$1;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@15e
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@160
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@162
    invoke-static {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$2500(Lcom/broadcom/fm/fmreceiver/FmNativehandler;II)V

    #@165
    goto/16 :goto_1f

    #@167
    .line 714
    :catch_167
    move-exception v0

    #@168
    goto/16 :goto_45

    #@16a
    .line 700
    :pswitch_data_16a
    .packed-switch 0x1
        :pswitch_20
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_7f
        :pswitch_b7
        :pswitch_db
        :pswitch_ed
        :pswitch_103
        :pswitch_113
        :pswitch_123
        :pswitch_133
        :pswitch_143
        :pswitch_155
    .end packed-switch
.end method
