.class Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;
.super Ljava/lang/Object;
.source "FmNativehandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmNativehandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FM_Search_Params"
.end annotation


# instance fields
.field private mStFreq:I

.field private mStRssi:I

.field private mStSeekSuccess:Z

.field private mStSnr:I

.field final synthetic this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;


# direct methods
.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IIIZ)V
    .registers 6
    .parameter
    .parameter "freq"
    .parameter "rssi"
    .parameter "snr"
    .parameter "seekSuccess"

    #@0
    .prologue
    .line 149
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 150
    iput p2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->mStFreq:I

    #@7
    .line 151
    iput p3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->mStRssi:I

    #@9
    .line 152
    iput p4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->mStSnr:I

    #@b
    .line 153
    iput-boolean p5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->mStSeekSuccess:Z

    #@d
    .line 154
    return-void
.end method

.method static synthetic access$1400(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 143
    iget v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->mStFreq:I

    #@2
    return v0
.end method

.method static synthetic access$1500(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 143
    iget v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->mStRssi:I

    #@2
    return v0
.end method

.method static synthetic access$1600(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 143
    iget v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->mStSnr:I

    #@2
    return v0
.end method

.method static synthetic access$1700(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Search_Params;->mStSeekSuccess:Z

    #@2
    return v0
.end method
