.class Lcom/broadcom/fm/fmreceiver/FmNativehandler$3;
.super Landroid/content/BroadcastReceiver;
.source "FmNativehandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmNativehandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;


# direct methods
.method constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 929
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$3;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 932
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 933
    .local v0, action:Ljava/lang/String;
    const-string v2, "FmNativehandler"

    #@8
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 934
    const-string v2, "android.bluetooth.adapter.action.RADIO_STATE_CHANGED"

    #@d
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_47

    #@13
    .line 935
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    #@15
    const/high16 v3, -0x8000

    #@17
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1a
    move-result v1

    #@1b
    .line 938
    .local v1, state:I
    const-string v2, "FmNativehandler"

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v4, "***********state = "

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 940
    const/16 v2, 0xe

    #@35
    if-ne v1, v2, :cond_48

    #@37
    .line 941
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$3;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@39
    new-instance v3, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;

    #@3b
    sget-object v4, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_ON:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@3d
    iget-object v5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$3;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@3f
    iget v5, v5, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->mFunctionalityMask:I

    #@41
    invoke-direct {v3, v4, v5}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;-><init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;I)V

    #@44
    invoke-static {v2, v3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$2700(Lcom/broadcom/fm/fmreceiver/FmNativehandler;Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;)V

    #@47
    .line 960
    .end local v1           #state:I
    :cond_47
    :goto_47
    return-void

    #@48
    .line 942
    .restart local v1       #state:I
    :cond_48
    const/16 v2, 0xf

    #@4a
    if-ne v1, v2, :cond_47

    #@4c
    .line 945
    sput-boolean v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRadioIsOn:Z

    #@4e
    .line 948
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$3;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@50
    iget-object v2, v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->operationHandler:Landroid/os/Handler;

    #@52
    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    #@55
    .line 951
    sget-boolean v2, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->mRadioIsOn:Z

    #@57
    if-nez v2, :cond_5b

    #@59
    .line 952
    sput v5, Lcom/broadcom/fm/fmreceiver/FmReceiverServiceState;->radio_state:I

    #@5b
    .line 956
    :cond_5b
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$3;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@5d
    invoke-static {v2, v6}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->access$2800(Lcom/broadcom/fm/fmreceiver/FmNativehandler;Z)V

    #@60
    goto :goto_47
.end method
