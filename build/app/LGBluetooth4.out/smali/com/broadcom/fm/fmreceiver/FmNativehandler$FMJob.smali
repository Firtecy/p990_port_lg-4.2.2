.class Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;
.super Ljava/lang/Object;
.source "FmNativehandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmNativehandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FMJob"
.end annotation


# instance fields
.field arg1:I

.field arg2:I

.field arg3:I

.field arg4:I

.field arg5:I

.field arg6:I

.field arg7:I

.field b_arg1:Z

.field final command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field timeSent:J


# direct methods
.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;)V
    .registers 4
    .parameter "command"

    #@0
    .prologue
    .line 171
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 172
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    .line 173
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@9
    .line 174
    return-void
.end method

.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;I)V
    .registers 5
    .parameter "command"
    .parameter "arg1"

    #@0
    .prologue
    .line 177
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 178
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    .line 179
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@9
    .line 180
    iput p2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@b
    .line 181
    return-void
.end method

.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;II)V
    .registers 6
    .parameter "command"
    .parameter "arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 184
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 185
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    .line 186
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@9
    .line 187
    iput p2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@b
    .line 188
    iput p3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@d
    .line 189
    return-void
.end method

.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;III)V
    .registers 7
    .parameter "command"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "arg3"

    #@0
    .prologue
    .line 192
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 193
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    .line 194
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@9
    .line 195
    iput p2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@b
    .line 196
    iput p3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@d
    .line 197
    iput p4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg3:I

    #@f
    .line 198
    return-void
.end method

.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;IIII)V
    .registers 8
    .parameter "command"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "arg3"
    .parameter "arg4"

    #@0
    .prologue
    .line 201
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 202
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    .line 203
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@9
    .line 204
    iput p2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@b
    .line 205
    iput p3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@d
    .line 206
    iput p4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg3:I

    #@f
    .line 207
    iput p5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg4:I

    #@11
    .line 208
    return-void
.end method

.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;Z)V
    .registers 5
    .parameter "command"
    .parameter "b_arg1"

    #@0
    .prologue
    .line 226
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 227
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    .line 228
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@9
    .line 229
    iput-boolean p2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->b_arg1:Z

    #@b
    .line 230
    return-void
.end method

.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;ZI)V
    .registers 6
    .parameter "command"
    .parameter "b_arg1"
    .parameter "arg2"

    #@0
    .prologue
    .line 233
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 234
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    .line 235
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@9
    .line 236
    iput-boolean p2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->b_arg1:Z

    #@b
    .line 237
    iput p3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@d
    .line 238
    return-void
.end method

.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;ZIIIIIII)V
    .registers 12
    .parameter "command"
    .parameter "b_arg1"
    .parameter "arg1"
    .parameter "arg2"
    .parameter "arg3"
    .parameter "arg4"
    .parameter "arg5"
    .parameter "arg6"
    .parameter "arg7"

    #@0
    .prologue
    .line 212
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 213
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5
    .line 214
    const-wide/16 v0, 0x0

    #@7
    iput-wide v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@9
    .line 215
    iput-boolean p2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->b_arg1:Z

    #@b
    .line 216
    iput p3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg1:I

    #@d
    .line 217
    iput p4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg2:I

    #@f
    .line 218
    iput p5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg3:I

    #@11
    .line 219
    iput p6, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg4:I

    #@13
    .line 220
    iput p7, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg5:I

    #@15
    .line 221
    iput p8, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg6:I

    #@17
    .line 222
    iput p9, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->arg7:I

    #@19
    .line 223
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .registers 6

    #@0
    .prologue
    .line 241
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 242
    .local v0, sb:Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->command:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@7
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->name()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 243
    const-string v1, " TimeSent:"

    #@10
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    .line 244
    iget-wide v1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@15
    const-wide/16 v3, 0x0

    #@17
    cmp-long v1, v1, v3

    #@19
    if-nez v1, :cond_25

    #@1b
    .line 245
    const-string v1, "not yet"

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    .line 249
    :goto_20
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    return-object v1

    #@25
    .line 247
    :cond_25
    invoke-static {}, Ljava/text/DateFormat;->getTimeInstance()Ljava/text/DateFormat;

    #@28
    move-result-object v1

    #@29
    new-instance v2, Ljava/util/Date;

    #@2b
    iget-wide v3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMJob;->timeSent:J

    #@2d
    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    #@30
    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    goto :goto_20
.end method
