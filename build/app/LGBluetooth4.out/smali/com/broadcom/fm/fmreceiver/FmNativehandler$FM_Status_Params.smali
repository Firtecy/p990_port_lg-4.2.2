.class Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;
.super Ljava/lang/Object;
.source "FmNativehandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmNativehandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FM_Status_Params"
.end annotation


# instance fields
.field private mStFreq:I

.field private mStIsMute:Z

.field private mStRadioIsOn:Z

.field private mStRdsProgramService:Ljava/lang/String;

.field private mStRdsProgramType:I

.field private mStRdsProgramTypeName:Ljava/lang/String;

.field private mStRdsRadioText:Ljava/lang/String;

.field private mStRssi:I

.field private mStSnr:I

.field final synthetic this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;


# direct methods
.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmNativehandler;IIIZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 11
    .parameter
    .parameter "freq"
    .parameter "rssi"
    .parameter "snr"
    .parameter "radioIsOn"
    .parameter "rdsProgramType"
    .parameter "rdsProgramService"
    .parameter "rdsRadioText"
    .parameter "rdsProgramTypeName"
    .parameter "isMute"

    #@0
    .prologue
    .line 130
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->this$0:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 131
    iput p2, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStFreq:I

    #@7
    .line 132
    iput p3, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRssi:I

    #@9
    .line 133
    iput p4, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStSnr:I

    #@b
    .line 134
    iput-boolean p5, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRadioIsOn:Z

    #@d
    .line 135
    iput p6, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRdsProgramType:I

    #@f
    .line 136
    iput-object p7, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRdsProgramService:Ljava/lang/String;

    #@11
    .line 137
    iput-object p8, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRdsRadioText:Ljava/lang/String;

    #@13
    .line 138
    iput-object p9, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRdsProgramTypeName:Ljava/lang/String;

    #@15
    .line 139
    iput-boolean p10, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStIsMute:Z

    #@17
    .line 140
    return-void
.end method

.method static synthetic access$1000(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRdsProgramService:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRdsRadioText:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRdsProgramTypeName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStIsMute:Z

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStFreq:I

    #@2
    return v0
.end method

.method static synthetic access$600(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRssi:I

    #@2
    return v0
.end method

.method static synthetic access$700(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStSnr:I

    #@2
    return v0
.end method

.method static synthetic access$800(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget-boolean v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRadioIsOn:Z

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FM_Status_Params;->mStRdsProgramType:I

    #@2
    return v0
.end method
