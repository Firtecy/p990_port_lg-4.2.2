.class final Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;
.super Lcom/broadcom/fm/fmreceiver/IFmReceiverService$Stub;
.source "FmService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FmReceiverServiceStub"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "FmService"


# instance fields
.field private mSvc:Lcom/broadcom/fm/fmreceiver/FmService;


# direct methods
.method public constructor <init>(Lcom/broadcom/fm/fmreceiver/FmService;)V
    .registers 5
    .parameter "service"

    #@0
    .prologue
    .line 126
    invoke-direct {p0}, Lcom/broadcom/fm/fmreceiver/IFmReceiverService$Stub;-><init>()V

    #@3
    .line 128
    iput-object p1, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@5
    .line 130
    const-string v0, "FmService"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "FmReceiverServiceStub created"

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, "service"

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v1

    #@24
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 132
    return-void
.end method

.method static synthetic access$000(Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;)Lcom/broadcom/fm/fmreceiver/FmService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@2
    return-object v0
.end method


# virtual methods
.method public cleanUp()V
    .registers 3

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@2
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@4
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->stop()V

    #@7
    .line 136
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@9
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@b
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->finish()V

    #@e
    .line 137
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@10
    const/4 v1, 0x0

    #@11
    iput-object v1, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@13
    .line 138
    return-void
.end method

.method public declared-synchronized cleanupFmService()I
    .registers 2

    #@0
    .prologue
    .line 227
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 228
    const/4 v0, 0x2

    #@6
    .line 230
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->cleanupFmService()I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 227
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized estimateNoiseFloorLevel(I)I
    .registers 3
    .parameter "nflLevel"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 546
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 547
    const/4 v0, 0x2

    #@6
    .line 549
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->estimateNoiseFloorLevel(I)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 546
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized getIsMute()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 606
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 607
    const/4 v0, 0x0

    #@6
    .line 609
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->getIsMute()Z
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 606
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized getMonoStereoMode()I
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 582
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 583
    const/4 v0, 0x2

    #@6
    .line 585
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->getMonoStereoMode()I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 582
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public getRadioIsOn()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 616
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 617
    const/4 v0, 0x0

    #@5
    .line 619
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@8
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@a
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->getRadioIsOn()Z

    #@d
    move-result v0

    #@e
    goto :goto_5
.end method

.method public declared-synchronized getStatus()I
    .registers 2

    #@0
    .prologue
    .line 256
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 257
    const/4 v0, 0x3

    #@6
    .line 259
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->getStatus()I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 256
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized getTunedFrequency()I
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 594
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 595
    const/4 v0, 0x2

    #@6
    .line 597
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->getTunedFrequency()I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 594
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public init()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 614
    return-void
.end method

.method public declared-synchronized muteAudio(Z)I
    .registers 3
    .parameter "mute"

    #@0
    .prologue
    .line 272
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 273
    const/4 v0, 0x3

    #@6
    .line 275
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->muteAudio(Z)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 272
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized registerCallback(Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;)V
    .registers 3
    .parameter "cb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 150
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_f

    #@3
    if-nez v0, :cond_7

    #@5
    .line 154
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 153
    :cond_7
    :try_start_7
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@9
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@b
    invoke-virtual {v0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->registerCallback(Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;)V
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_f

    #@e
    goto :goto_5

    #@f
    .line 150
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0
.end method

.method public declared-synchronized seekRdsStation(IIII)I
    .registers 6
    .parameter "scanMode"
    .parameter "minSignalStrength"
    .parameter "rdsCondition"
    .parameter "rdsValue"

    #@0
    .prologue
    .line 323
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 324
    const/4 v0, 0x2

    #@6
    .line 326
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->seekRdsStation(IIII)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 323
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized seekStation(II)I
    .registers 4
    .parameter "scanMode"
    .parameter "minSignalStrength"

    #@0
    .prologue
    .line 294
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 295
    const/4 v0, 0x2

    #@6
    .line 297
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->seekStation(II)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 294
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized seekStationAbort()I
    .registers 2

    #@0
    .prologue
    .line 339
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 340
    const/4 v0, 0x2

    #@6
    .line 342
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->seekStationAbort()I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 339
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized seekStationCombo(IIIIIZII)I
    .registers 18
    .parameter "startFrequency"
    .parameter "endFrequency"
    .parameter "minSignalStrength"
    .parameter "scanDirection"
    .parameter "scanMethod"
    .parameter "multi_channel"
    .parameter "rdsType"
    .parameter "rdsTypeValue"

    #@0
    .prologue
    .line 378
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_1b

    #@3
    if-nez v0, :cond_8

    #@5
    .line 379
    const/4 v0, 0x2

    #@6
    .line 381
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    move v1, p1

    #@d
    move v2, p2

    #@e
    move v3, p3

    #@f
    move v4, p4

    #@10
    move v5, p5

    #@11
    move v6, p6

    #@12
    move/from16 v7, p7

    #@14
    move/from16 v8, p8

    #@16
    invoke-virtual/range {v0 .. v8}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->seekStationCombo(IIIIIZII)I
    :try_end_19
    .catchall {:try_start_8 .. :try_end_19} :catchall_1b

    #@19
    move-result v0

    #@1a
    goto :goto_6

    #@1b
    .line 378
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0

    #@1d
    throw v0
.end method

.method public declared-synchronized setAudioMode(I)I
    .registers 3
    .parameter "audioMode"

    #@0
    .prologue
    .line 429
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 430
    const/4 v0, 0x2

    #@6
    .line 432
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setAudioMode(I)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 429
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized setAudioPath(I)I
    .registers 3
    .parameter "audioPath"

    #@0
    .prologue
    .line 452
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 453
    const/4 v0, 0x2

    #@6
    .line 455
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setAudioPath(I)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 452
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized setFMVolume(I)I
    .registers 3
    .parameter "volume"

    #@0
    .prologue
    .line 488
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 489
    const/4 v0, 0x2

    #@6
    .line 491
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setFMVolume(I)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 488
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized setLiveAudioPolling(ZI)I
    .registers 4
    .parameter "liveAudioPolling"
    .parameter "signalPollInterval"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 567
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 568
    const/4 v0, 0x2

    #@6
    .line 570
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setLiveAudioPolling(ZI)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 567
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized setRdsMode(IIII)I
    .registers 6
    .parameter "rdsMode"
    .parameter "rdsFeatures"
    .parameter "afMode"
    .parameter "afThreshold"

    #@0
    .prologue
    .line 409
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 410
    const/4 v0, 0x2

    #@6
    .line 412
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setRdsMode(IIII)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 409
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized setSnrThreshold(I)I
    .registers 3
    .parameter "snrThreshold"

    #@0
    .prologue
    .line 502
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 503
    const/4 v0, 0x2

    #@6
    .line 505
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setSnrThreshold(I)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 502
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized setStepSize(I)I
    .registers 3
    .parameter "stepSize"

    #@0
    .prologue
    .line 472
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 473
    const/4 v0, 0x2

    #@6
    .line 475
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setStepSize(I)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 472
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized setWorldRegion(II)I
    .registers 4
    .parameter "worldRegion"
    .parameter "deemphasisTime"

    #@0
    .prologue
    .line 526
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 527
    const/4 v0, 0x2

    #@6
    .line 529
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->setWorldRegion(II)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 526
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized tuneRadio(I)I
    .registers 3
    .parameter "freq"

    #@0
    .prologue
    .line 243
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 244
    const/4 v0, 0x2

    #@6
    .line 246
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->tuneRadio(I)I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 243
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized turnOffRadio()I
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 214
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_11

    #@3
    if-nez v0, :cond_8

    #@5
    .line 215
    const/4 v0, 0x2

    #@6
    .line 217
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    :cond_8
    :try_start_8
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@a
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@c
    invoke-virtual {v0}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->turnOffRadio()I
    :try_end_f
    .catchall {:try_start_8 .. :try_end_f} :catchall_11

    #@f
    move-result v0

    #@10
    goto :goto_6

    #@11
    .line 214
    :catchall_11
    move-exception v0

    #@12
    monitor-exit p0

    #@13
    throw v0
.end method

.method public declared-synchronized turnOnRadio(I[C)I
    .registers 6
    .parameter "functionalityMask"
    .parameter "clientPackagename"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 197
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_2b

    #@3
    if-nez v0, :cond_8

    #@5
    .line 198
    const/4 v0, 0x2

    #@6
    .line 201
    :goto_6
    monitor-exit p0

    #@7
    return v0

    #@8
    .line 200
    :cond_8
    :try_start_8
    const-string v0, "FmService"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "turnOnRadio() mSvc:"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget-object v2, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 201
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@24
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@26
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->turnOnRadio(I[C)I
    :try_end_29
    .catchall {:try_start_8 .. :try_end_29} :catchall_2b

    #@29
    move-result v0

    #@2a
    goto :goto_6

    #@2b
    .line 197
    :catchall_2b
    move-exception v0

    #@2c
    monitor-exit p0

    #@2d
    throw v0
.end method

.method public declared-synchronized unregisterCallback(Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;)V
    .registers 3
    .parameter "cb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 165
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_f

    #@3
    if-nez v0, :cond_7

    #@5
    .line 169
    :goto_5
    monitor-exit p0

    #@6
    return-void

    #@7
    .line 168
    :cond_7
    :try_start_7
    iget-object v0, p0, Lcom/broadcom/fm/fmreceiver/FmService$FmReceiverServiceStub;->mSvc:Lcom/broadcom/fm/fmreceiver/FmService;

    #@9
    iget-object v0, v0, Lcom/broadcom/fm/fmreceiver/FmService;->mSvcHandler:Lcom/broadcom/fm/fmreceiver/FmNativehandler;

    #@b
    invoke-virtual {v0, p1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler;->unregisterCallback(Lcom/broadcom/fm/fmreceiver/IFmReceiverCallback;)V
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_f

    #@e
    goto :goto_5

    #@f
    .line 165
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0
.end method
