.class final enum Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;
.super Ljava/lang/Enum;
.source "FmNativehandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmNativehandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FMCommand"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_CHIP_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_CHIP_ON:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_ESTIMATE_NOISE_FLOOR_LEVEL:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_GET_STATUS:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_MUTE_AUDIO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_ON:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SEEK_RDS_STATION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SEEK_STATION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SEEK_STATION_ABORT:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SEEK_STATION_COMBO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SET_AUDIO_MODE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SET_AUDIO_PATH:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SET_LIVE_AUDIO_POLLING:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SET_RDS_MODE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SET_STEP_SIZE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SET_VOLUME:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_SET_WORLD_REGION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

.field public static final enum FM_TUNE_RADIO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 95
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@7
    const-string v1, "FM_CHIP_ON"

    #@9
    invoke-direct {v0, v1, v3}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@c
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_CHIP_ON:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@e
    .line 96
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@10
    const-string v1, "FM_CHIP_OFF"

    #@12
    invoke-direct {v0, v1, v4}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@15
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_CHIP_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@17
    .line 97
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@19
    const-string v1, "FM_ON"

    #@1b
    invoke-direct {v0, v1, v5}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@1e
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_ON:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@20
    .line 98
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@22
    const-string v1, "FM_OFF"

    #@24
    invoke-direct {v0, v1, v6}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@27
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@29
    .line 99
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@2b
    const-string v1, "FM_TUNE_RADIO"

    #@2d
    invoke-direct {v0, v1, v7}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@30
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_TUNE_RADIO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@32
    .line 100
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@34
    const-string v1, "FM_GET_STATUS"

    #@36
    const/4 v2, 0x5

    #@37
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@3a
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_GET_STATUS:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@3c
    .line 101
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@3e
    const-string v1, "FM_MUTE_AUDIO"

    #@40
    const/4 v2, 0x6

    #@41
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@44
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_MUTE_AUDIO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@46
    .line 102
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@48
    const-string v1, "FM_SEEK_STATION"

    #@4a
    const/4 v2, 0x7

    #@4b
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@4e
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@50
    .line 103
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@52
    const-string v1, "FM_SEEK_STATION_COMBO"

    #@54
    const/16 v2, 0x8

    #@56
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@59
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION_COMBO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5b
    .line 104
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5d
    const-string v1, "FM_SEEK_RDS_STATION"

    #@5f
    const/16 v2, 0x9

    #@61
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@64
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_RDS_STATION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@66
    .line 105
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@68
    const-string v1, "FM_SEEK_STATION_ABORT"

    #@6a
    const/16 v2, 0xa

    #@6c
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@6f
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION_ABORT:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@71
    .line 106
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@73
    const-string v1, "FM_SET_RDS_MODE"

    #@75
    const/16 v2, 0xb

    #@77
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@7a
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_RDS_MODE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@7c
    .line 107
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@7e
    const-string v1, "FM_SET_AUDIO_MODE"

    #@80
    const/16 v2, 0xc

    #@82
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@85
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_AUDIO_MODE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@87
    .line 108
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@89
    const-string v1, "FM_SET_AUDIO_PATH"

    #@8b
    const/16 v2, 0xd

    #@8d
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@90
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_AUDIO_PATH:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@92
    .line 109
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@94
    const-string v1, "FM_SET_STEP_SIZE"

    #@96
    const/16 v2, 0xe

    #@98
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@9b
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_STEP_SIZE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@9d
    .line 110
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@9f
    const-string v1, "FM_SET_WORLD_REGION"

    #@a1
    const/16 v2, 0xf

    #@a3
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@a6
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_WORLD_REGION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@a8
    .line 111
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@aa
    const-string v1, "FM_ESTIMATE_NOISE_FLOOR_LEVEL"

    #@ac
    const/16 v2, 0x10

    #@ae
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@b1
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_ESTIMATE_NOISE_FLOOR_LEVEL:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@b3
    .line 112
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@b5
    const-string v1, "FM_SET_LIVE_AUDIO_POLLING"

    #@b7
    const/16 v2, 0x11

    #@b9
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@bc
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_LIVE_AUDIO_POLLING:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@be
    .line 113
    new-instance v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@c0
    const-string v1, "FM_SET_VOLUME"

    #@c2
    const/16 v2, 0x12

    #@c4
    invoke-direct {v0, v1, v2}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;-><init>(Ljava/lang/String;I)V

    #@c7
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_VOLUME:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@c9
    .line 94
    const/16 v0, 0x13

    #@cb
    new-array v0, v0, [Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@cd
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_CHIP_ON:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@cf
    aput-object v1, v0, v3

    #@d1
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_CHIP_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@d3
    aput-object v1, v0, v4

    #@d5
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_ON:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@d7
    aput-object v1, v0, v5

    #@d9
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@db
    aput-object v1, v0, v6

    #@dd
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_TUNE_RADIO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@df
    aput-object v1, v0, v7

    #@e1
    const/4 v1, 0x5

    #@e2
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_GET_STATUS:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@e4
    aput-object v2, v0, v1

    #@e6
    const/4 v1, 0x6

    #@e7
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_MUTE_AUDIO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@e9
    aput-object v2, v0, v1

    #@eb
    const/4 v1, 0x7

    #@ec
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@ee
    aput-object v2, v0, v1

    #@f0
    const/16 v1, 0x8

    #@f2
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION_COMBO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@f4
    aput-object v2, v0, v1

    #@f6
    const/16 v1, 0x9

    #@f8
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_RDS_STATION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@fa
    aput-object v2, v0, v1

    #@fc
    const/16 v1, 0xa

    #@fe
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION_ABORT:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@100
    aput-object v2, v0, v1

    #@102
    const/16 v1, 0xb

    #@104
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_RDS_MODE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@106
    aput-object v2, v0, v1

    #@108
    const/16 v1, 0xc

    #@10a
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_AUDIO_MODE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@10c
    aput-object v2, v0, v1

    #@10e
    const/16 v1, 0xd

    #@110
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_AUDIO_PATH:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@112
    aput-object v2, v0, v1

    #@114
    const/16 v1, 0xe

    #@116
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_STEP_SIZE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@118
    aput-object v2, v0, v1

    #@11a
    const/16 v1, 0xf

    #@11c
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_WORLD_REGION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@11e
    aput-object v2, v0, v1

    #@120
    const/16 v1, 0x10

    #@122
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_ESTIMATE_NOISE_FLOOR_LEVEL:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@124
    aput-object v2, v0, v1

    #@126
    const/16 v1, 0x11

    #@128
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_LIVE_AUDIO_POLLING:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@12a
    aput-object v2, v0, v1

    #@12c
    const/16 v1, 0x12

    #@12e
    sget-object v2, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_VOLUME:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@130
    aput-object v2, v0, v1

    #@132
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->$VALUES:[Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@134
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .registers 3
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    #@0
    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;
    .registers 2
    .parameter "name"

    #@0
    .prologue
    .line 94
    const-class v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@2
    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@8
    return-object v0
.end method

.method public static values()[Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;
    .registers 1

    #@0
    .prologue
    .line 94
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->$VALUES:[Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@2
    invoke-virtual {v0}, [Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, [Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@8
    return-object v0
.end method
