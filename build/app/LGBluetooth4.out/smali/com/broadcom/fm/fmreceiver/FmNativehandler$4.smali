.class synthetic Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;
.super Ljava/lang/Object;
.source "FmNativehandler.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/fm/fmreceiver/FmNativehandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 320
    invoke-static {}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->values()[Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@3
    move-result-object v0

    #@4
    array-length v0, v0

    #@5
    new-array v0, v0, [I

    #@7
    sput-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@9
    :try_start_9
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@b
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_CHIP_ON:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@d
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@10
    move-result v1

    #@11
    const/4 v2, 0x1

    #@12
    aput v2, v0, v1
    :try_end_14
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_14} :catch_104

    #@14
    :goto_14
    :try_start_14
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@16
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_ON:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@18
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@1b
    move-result v1

    #@1c
    const/4 v2, 0x2

    #@1d
    aput v2, v0, v1
    :try_end_1f
    .catch Ljava/lang/NoSuchFieldError; {:try_start_14 .. :try_end_1f} :catch_101

    #@1f
    :goto_1f
    :try_start_1f
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@21
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@23
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@26
    move-result v1

    #@27
    const/4 v2, 0x3

    #@28
    aput v2, v0, v1
    :try_end_2a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1f .. :try_end_2a} :catch_fe

    #@2a
    :goto_2a
    :try_start_2a
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@2c
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_CHIP_OFF:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@2e
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@31
    move-result v1

    #@32
    const/4 v2, 0x4

    #@33
    aput v2, v0, v1
    :try_end_35
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2a .. :try_end_35} :catch_fb

    #@35
    :goto_35
    :try_start_35
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@37
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_TUNE_RADIO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@39
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@3c
    move-result v1

    #@3d
    const/4 v2, 0x5

    #@3e
    aput v2, v0, v1
    :try_end_40
    .catch Ljava/lang/NoSuchFieldError; {:try_start_35 .. :try_end_40} :catch_f8

    #@40
    :goto_40
    :try_start_40
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@42
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_GET_STATUS:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@44
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@47
    move-result v1

    #@48
    const/4 v2, 0x6

    #@49
    aput v2, v0, v1
    :try_end_4b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_40 .. :try_end_4b} :catch_f5

    #@4b
    :goto_4b
    :try_start_4b
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@4d
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_MUTE_AUDIO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@4f
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@52
    move-result v1

    #@53
    const/4 v2, 0x7

    #@54
    aput v2, v0, v1
    :try_end_56
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4b .. :try_end_56} :catch_f2

    #@56
    :goto_56
    :try_start_56
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@58
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@5a
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@5d
    move-result v1

    #@5e
    const/16 v2, 0x8

    #@60
    aput v2, v0, v1
    :try_end_62
    .catch Ljava/lang/NoSuchFieldError; {:try_start_56 .. :try_end_62} :catch_ef

    #@62
    :goto_62
    :try_start_62
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@64
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_STATION_COMBO:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@66
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@69
    move-result v1

    #@6a
    const/16 v2, 0x9

    #@6c
    aput v2, v0, v1
    :try_end_6e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_62 .. :try_end_6e} :catch_ed

    #@6e
    :goto_6e
    :try_start_6e
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@70
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SEEK_RDS_STATION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@72
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@75
    move-result v1

    #@76
    const/16 v2, 0xa

    #@78
    aput v2, v0, v1
    :try_end_7a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6e .. :try_end_7a} :catch_eb

    #@7a
    :goto_7a
    :try_start_7a
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@7c
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_RDS_MODE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@7e
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@81
    move-result v1

    #@82
    const/16 v2, 0xb

    #@84
    aput v2, v0, v1
    :try_end_86
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7a .. :try_end_86} :catch_e9

    #@86
    :goto_86
    :try_start_86
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@88
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_AUDIO_MODE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@8a
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@8d
    move-result v1

    #@8e
    const/16 v2, 0xc

    #@90
    aput v2, v0, v1
    :try_end_92
    .catch Ljava/lang/NoSuchFieldError; {:try_start_86 .. :try_end_92} :catch_e7

    #@92
    :goto_92
    :try_start_92
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@94
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_AUDIO_PATH:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@96
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@99
    move-result v1

    #@9a
    const/16 v2, 0xd

    #@9c
    aput v2, v0, v1
    :try_end_9e
    .catch Ljava/lang/NoSuchFieldError; {:try_start_92 .. :try_end_9e} :catch_e5

    #@9e
    :goto_9e
    :try_start_9e
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@a0
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_STEP_SIZE:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@a2
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@a5
    move-result v1

    #@a6
    const/16 v2, 0xe

    #@a8
    aput v2, v0, v1
    :try_end_aa
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9e .. :try_end_aa} :catch_e3

    #@aa
    :goto_aa
    :try_start_aa
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@ac
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_WORLD_REGION:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@ae
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@b1
    move-result v1

    #@b2
    const/16 v2, 0xf

    #@b4
    aput v2, v0, v1
    :try_end_b6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_aa .. :try_end_b6} :catch_e1

    #@b6
    :goto_b6
    :try_start_b6
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@b8
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_ESTIMATE_NOISE_FLOOR_LEVEL:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@ba
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@bd
    move-result v1

    #@be
    const/16 v2, 0x10

    #@c0
    aput v2, v0, v1
    :try_end_c2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b6 .. :try_end_c2} :catch_df

    #@c2
    :goto_c2
    :try_start_c2
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@c4
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_LIVE_AUDIO_POLLING:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@c6
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@c9
    move-result v1

    #@ca
    const/16 v2, 0x11

    #@cc
    aput v2, v0, v1
    :try_end_ce
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c2 .. :try_end_ce} :catch_dd

    #@ce
    :goto_ce
    :try_start_ce
    sget-object v0, Lcom/broadcom/fm/fmreceiver/FmNativehandler$4;->$SwitchMap$com$broadcom$fm$fmreceiver$FmNativehandler$FMCommand:[I

    #@d0
    sget-object v1, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->FM_SET_VOLUME:Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;

    #@d2
    invoke-virtual {v1}, Lcom/broadcom/fm/fmreceiver/FmNativehandler$FMCommand;->ordinal()I

    #@d5
    move-result v1

    #@d6
    const/16 v2, 0x12

    #@d8
    aput v2, v0, v1
    :try_end_da
    .catch Ljava/lang/NoSuchFieldError; {:try_start_ce .. :try_end_da} :catch_db

    #@da
    :goto_da
    return-void

    #@db
    :catch_db
    move-exception v0

    #@dc
    goto :goto_da

    #@dd
    :catch_dd
    move-exception v0

    #@de
    goto :goto_ce

    #@df
    :catch_df
    move-exception v0

    #@e0
    goto :goto_c2

    #@e1
    :catch_e1
    move-exception v0

    #@e2
    goto :goto_b6

    #@e3
    :catch_e3
    move-exception v0

    #@e4
    goto :goto_aa

    #@e5
    :catch_e5
    move-exception v0

    #@e6
    goto :goto_9e

    #@e7
    :catch_e7
    move-exception v0

    #@e8
    goto :goto_92

    #@e9
    :catch_e9
    move-exception v0

    #@ea
    goto :goto_86

    #@eb
    :catch_eb
    move-exception v0

    #@ec
    goto :goto_7a

    #@ed
    :catch_ed
    move-exception v0

    #@ee
    goto :goto_6e

    #@ef
    :catch_ef
    move-exception v0

    #@f0
    goto/16 :goto_62

    #@f2
    :catch_f2
    move-exception v0

    #@f3
    goto/16 :goto_56

    #@f5
    :catch_f5
    move-exception v0

    #@f6
    goto/16 :goto_4b

    #@f8
    :catch_f8
    move-exception v0

    #@f9
    goto/16 :goto_40

    #@fb
    :catch_fb
    move-exception v0

    #@fc
    goto/16 :goto_35

    #@fe
    :catch_fe
    move-exception v0

    #@ff
    goto/16 :goto_2a

    #@101
    :catch_101
    move-exception v0

    #@102
    goto/16 :goto_1f

    #@104
    :catch_104
    move-exception v0

    #@105
    goto/16 :goto_14
.end method
