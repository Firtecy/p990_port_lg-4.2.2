.class public abstract Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;
.super Ljava/lang/Object;
.source "SmsMmsDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "MessageListIterator"
.end annotation


# instance fields
.field protected mContactFilter:Ljava/lang/String;

.field protected mCount:I

.field protected mCursor:Landroid/database/Cursor;

.field protected mCursorHasMoved:Z

.field protected mFolderPath:Ljava/lang/String;

.field protected mHasNewMessages:Z

.field protected mIncludeMessageSize:Z

.field protected mMaxSubjectLength:I

.field protected mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

.field protected mOwnerPhoneNumber:Ljava/lang/String;

.field protected mParams:Lcom/broadcom/bt/map/MessageParameterFilter;

.field protected mPhoneType:I


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private moveToNext(Z)Z
    .registers 7
    .parameter "updateCount"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1019
    const/4 v0, 0x0

    #@3
    .line 1020
    .local v0, hasNext:Z
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCursor:Landroid/database/Cursor;

    #@5
    if-nez v3, :cond_8

    #@7
    .line 1069
    :goto_7
    return v1

    #@8
    .line 1023
    :cond_8
    iget-boolean v3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCursorHasMoved:Z

    #@a
    if-nez v3, :cond_27

    #@c
    .line 1024
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCursor:Landroid/database/Cursor;

    #@e
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    #@11
    move-result v0

    #@12
    .line 1025
    iput-boolean v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCursorHasMoved:Z

    #@14
    .line 1029
    :goto_14
    if-nez v0, :cond_2e

    #@16
    .line 1030
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@19
    move-result v2

    #@1a
    if-eqz v2, :cond_23

    #@1c
    .line 1031
    const-string v2, "BtMap.SmsMmsDataSource"

    #@1e
    const-string v3, "moveToNext(): no entry found. Returning false"

    #@20
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 1033
    :cond_23
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->close()V

    #@26
    goto :goto_7

    #@27
    .line 1027
    :cond_27
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCursor:Landroid/database/Cursor;

    #@29
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    #@2c
    move-result v0

    #@2d
    goto :goto_14

    #@2e
    .line 1038
    :cond_2e
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mContactFilter:Ljava/lang/String;

    #@30
    if-nez v3, :cond_59

    #@32
    .line 1039
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@35
    move-result v3

    #@36
    if-eqz v3, :cond_3f

    #@38
    .line 1040
    const-string v3, "BtMap.SmsMmsDataSource"

    #@3a
    const-string v4, "moveToNext(): filter not set, returning true"

    #@3c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 1042
    :cond_3f
    if-eqz p1, :cond_59

    #@41
    .line 1043
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->updateCountAndNewMessageStatus()V

    #@44
    move v1, v2

    #@45
    .line 1044
    goto :goto_7

    #@46
    .line 1059
    :cond_46
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@49
    move-result v3

    #@4a
    if-eqz v3, :cond_53

    #@4c
    .line 1060
    const-string v3, "BtMap.SmsMmsDataSource"

    #@4e
    const-string v4, "moveToNext(): filter does not match: fetching next entry"

    #@50
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 1062
    :cond_53
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCursor:Landroid/database/Cursor;

    #@55
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    #@58
    move-result v0

    #@59
    .line 1049
    :cond_59
    if-eqz v0, :cond_75

    #@5b
    .line 1050
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->contactMatchesFilter()Z

    #@5e
    move-result v3

    #@5f
    if-eqz v3, :cond_46

    #@61
    .line 1051
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@64
    move-result v3

    #@65
    if-eqz v3, :cond_6e

    #@67
    .line 1052
    const-string v3, "BtMap.SmsMmsDataSource"

    #@69
    const-string v4, "moveToNext(): filter matches, returning true"

    #@6b
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 1054
    :cond_6e
    if-eqz p1, :cond_59

    #@70
    .line 1055
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->updateCountAndNewMessageStatus()V

    #@73
    move v1, v2

    #@74
    .line 1056
    goto :goto_7

    #@75
    .line 1065
    :cond_75
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@78
    move-result v2

    #@79
    if-eqz v2, :cond_82

    #@7b
    .line 1066
    const-string v2, "BtMap.SmsMmsDataSource"

    #@7d
    const-string v3, "moveToNext(): no matching entry found.."

    #@7f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 1068
    :cond_82
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->close()V

    #@85
    goto :goto_7
.end method


# virtual methods
.method public close()V
    .registers 2

    #@0
    .prologue
    .line 1102
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCursor:Landroid/database/Cursor;

    #@2
    invoke-static {v0}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@5
    .line 1103
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCursor:Landroid/database/Cursor;

    #@8
    .line 1104
    return-void
.end method

.method protected abstract contactMatchesFilter()Z
.end method

.method public countAll(I)V
    .registers 3
    .parameter "maxCount"

    #@0
    .prologue
    .line 1086
    if-gtz p1, :cond_9

    #@2
    .line 1087
    :cond_2
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->moveToNext()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_2

    #@8
    .line 1095
    :cond_8
    return-void

    #@9
    .line 1091
    :cond_9
    :goto_9
    iget v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCount:I

    #@b
    if-ge v0, p1, :cond_8

    #@d
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->moveToNext()Z

    #@10
    move-result v0

    #@11
    if-eqz v0, :cond_8

    #@13
    goto :goto_9
.end method

.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 1098
    iget v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCount:I

    #@2
    return v0
.end method

.method protected abstract getMessageDateTime()J
.end method

.method protected abstract getMessageInfo()Lcom/broadcom/bt/map/MessageInfo;
.end method

.method protected abstract getReadStatus()Z
.end method

.method public hasNewMessages()Z
    .registers 2

    #@0
    .prologue
    .line 1073
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mHasNewMessages:Z

    #@2
    return v0
.end method

.method protected abstract initCountIterator(Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Ljava/lang/String;Lcom/broadcom/bt/map/MessageParameterFilter;)V
.end method

.method protected abstract initMessageInfoIterator(Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Ljava/lang/String;Lcom/broadcom/bt/map/MessageParameterFilter;ZILjava/lang/String;I)V
.end method

.method public moveToNext()Z
    .registers 2

    #@0
    .prologue
    .line 1015
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->moveToNext(Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public skip(I)Z
    .registers 5
    .parameter "offset"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1077
    const/4 v0, 0x0

    #@2
    .local v0, i:I
    :goto_2
    if-ge v0, p1, :cond_e

    #@4
    .line 1078
    invoke-direct {p0, v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->moveToNext(Z)Z

    #@7
    move-result v2

    #@8
    if-nez v2, :cond_b

    #@a
    .line 1082
    :goto_a
    return v1

    #@b
    .line 1077
    :cond_b
    add-int/lit8 v0, v0, 0x1

    #@d
    goto :goto_2

    #@e
    .line 1082
    :cond_e
    const/4 v1, 0x1

    #@f
    goto :goto_a
.end method

.method protected updateCountAndNewMessageStatus()V
    .registers 4

    #@0
    .prologue
    .line 993
    iget v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCount:I

    #@2
    add-int/lit8 v1, v1, 0x1

    #@4
    iput v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mCount:I

    #@6
    .line 994
    iget-boolean v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mHasNewMessages:Z

    #@8
    if-nez v1, :cond_20

    #@a
    .line 995
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->getReadStatus()Z

    #@d
    move-result v0

    #@e
    .line 996
    .local v0, isRead:Z
    if-nez v0, :cond_20

    #@10
    .line 997
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_1d

    #@16
    .line 998
    const-string v1, "BtMap.SmsMmsDataSource"

    #@18
    const-string v2, "Found new message!"

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 1000
    :cond_1d
    const/4 v1, 0x1

    #@1e
    iput-boolean v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->mHasNewMessages:Z

    #@20
    .line 1003
    .end local v0           #isRead:Z
    :cond_20
    return-void
.end method
