.class Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
.super Ljava/lang/Object;
.source "HandleMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/gatt/HandleMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Entry"
.end annotation


# instance fields
.field charHandle:I

.field handle:I

.field instance:I

.field serverIf:B

.field serviceHandle:I

.field serviceType:I

.field started:Z

.field final synthetic this$0:Lcom/broadcom/bt/service/gatt/HandleMap;

.field type:B

.field uuid:Ljava/util/UUID;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/gatt/HandleMap;BBILjava/util/UUID;I)V
    .registers 9
    .parameter
    .parameter "serverIf"
    .parameter "type"
    .parameter "handle"
    .parameter "uuid"
    .parameter "serviceHandle"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 86
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->this$0:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 67
    iput-byte v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@8
    .line 68
    iput-byte v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@a
    .line 69
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@c
    .line 70
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@f
    .line 71
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@11
    .line 72
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceType:I

    #@13
    .line 73
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@15
    .line 74
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->charHandle:I

    #@17
    .line 75
    iput-boolean v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->started:Z

    #@19
    .line 87
    iput-byte p2, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@1b
    .line 88
    iput-byte p3, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@1d
    .line 89
    iput p4, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@1f
    .line 90
    iput-object p5, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@21
    .line 96
    iput p6, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@23
    .line 97
    return-void
.end method

.method constructor <init>(Lcom/broadcom/bt/service/gatt/HandleMap;BBILjava/util/UUID;II)V
    .registers 10
    .parameter
    .parameter "serverIf"
    .parameter "type"
    .parameter "handle"
    .parameter "uuid"
    .parameter "serviceHandle"
    .parameter "charHandle"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 99
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->this$0:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 67
    iput-byte v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@8
    .line 68
    iput-byte v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@a
    .line 69
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@c
    .line 70
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@f
    .line 71
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@11
    .line 72
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceType:I

    #@13
    .line 73
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@15
    .line 74
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->charHandle:I

    #@17
    .line 75
    iput-boolean v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->started:Z

    #@19
    .line 100
    iput-byte p2, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@1b
    .line 101
    iput-byte p3, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@1d
    .line 102
    iput p4, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@1f
    .line 103
    iput-object p5, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@21
    .line 109
    iput p6, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@23
    .line 110
    iput p7, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->charHandle:I

    #@25
    .line 111
    return-void
.end method

.method constructor <init>(Lcom/broadcom/bt/service/gatt/HandleMap;BILjava/util/UUID;II)V
    .registers 9
    .parameter
    .parameter "serverIf"
    .parameter "handle"
    .parameter "uuid"
    .parameter "serviceType"
    .parameter "instance"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 77
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->this$0:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 67
    iput-byte v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@8
    .line 68
    iput-byte v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@a
    .line 69
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@c
    .line 70
    const/4 v0, 0x0

    #@d
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@f
    .line 71
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@11
    .line 72
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceType:I

    #@13
    .line 73
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@15
    .line 74
    iput v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->charHandle:I

    #@17
    .line 75
    iput-boolean v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->started:Z

    #@19
    .line 78
    iput-byte p2, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@1b
    .line 79
    const/4 v0, 0x1

    #@1c
    iput-byte v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@1e
    .line 80
    iput p3, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@20
    .line 81
    iput-object p4, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@22
    .line 82
    iput p6, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@24
    .line 83
    iput p5, p0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceType:I

    #@26
    .line 84
    return-void
.end method
