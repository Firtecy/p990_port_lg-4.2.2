.class public final Lcom/broadcom/bt/service/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BluetoothBRCMUtils"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getProfileUUIDFromServiceId(I)Landroid/os/ParcelUuid;
    .registers 2
    .parameter "serviceId"

    #@0
    .prologue
    .line 38
    packed-switch p0, :pswitch_data_30

    #@3
    .line 54
    :pswitch_3
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 39
    :pswitch_5
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->AudioSource:Landroid/os/ParcelUuid;

    #@7
    goto :goto_4

    #@8
    .line 40
    :pswitch_8
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->AvrcpTarget:Landroid/os/ParcelUuid;

    #@a
    goto :goto_4

    #@b
    .line 41
    :pswitch_b
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Handsfree:Landroid/os/ParcelUuid;

    #@d
    goto :goto_4

    #@e
    .line 42
    :pswitch_e
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->HSP:Landroid/os/ParcelUuid;

    #@10
    goto :goto_4

    #@11
    .line 43
    :pswitch_11
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->PANU:Landroid/os/ParcelUuid;

    #@13
    goto :goto_4

    #@14
    .line 44
    :pswitch_14
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->NAP:Landroid/os/ParcelUuid;

    #@16
    goto :goto_4

    #@17
    .line 45
    :pswitch_17
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->PBAP_PSE:Landroid/os/ParcelUuid;

    #@19
    goto :goto_4

    #@1a
    .line 46
    :pswitch_1a
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->Hid:Landroid/os/ParcelUuid;

    #@1c
    goto :goto_4

    #@1d
    .line 47
    :pswitch_1d
    sget-object v0, Landroid/bluetooth/BluetoothUuid;->ObexObjectPush:Landroid/os/ParcelUuid;

    #@1f
    goto :goto_4

    #@20
    .line 48
    :pswitch_20
    sget-object v0, Lcom/lge/bluetooth/LGBluetoothDevice;->SIM_ACC:Landroid/os/ParcelUuid;

    #@22
    goto :goto_4

    #@23
    .line 49
    :pswitch_23
    sget-object v0, Lcom/lge/bluetooth/LGBluetoothDevice;->SPP:Landroid/os/ParcelUuid;

    #@25
    goto :goto_4

    #@26
    .line 50
    :pswitch_26
    sget-object v0, Lcom/lge/bluetooth/LGBluetoothDevice;->FILE_TRANSFER:Landroid/os/ParcelUuid;

    #@28
    goto :goto_4

    #@29
    .line 51
    :pswitch_29
    sget-object v0, Lcom/lge/bluetooth/LGBluetoothDevice;->MNS:Landroid/os/ParcelUuid;

    #@2b
    goto :goto_4

    #@2c
    .line 52
    :pswitch_2c
    sget-object v0, Lcom/lge/bluetooth/LGBluetoothDevice;->MAP:Landroid/os/ParcelUuid;

    #@2e
    goto :goto_4

    #@2f
    .line 38
    nop

    #@30
    :pswitch_data_30
    .packed-switch 0x1
        :pswitch_23
        :pswitch_3
        :pswitch_5
        :pswitch_3
        :pswitch_e
        :pswitch_b
        :pswitch_1d
        :pswitch_26
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_11
        :pswitch_14
        :pswitch_3
        :pswitch_20
        :pswitch_3
        :pswitch_8
        :pswitch_1a
        :pswitch_3
        :pswitch_17
        :pswitch_3
        :pswitch_3
        :pswitch_2c
        :pswitch_29
    .end packed-switch
.end method

.method public static getServiceIdFromProfileUUID(Landroid/os/ParcelUuid;)I
    .registers 3
    .parameter "uuid"

    #@0
    .prologue
    const/16 v0, 0x19

    #@2
    .line 60
    invoke-static {p0}, Landroid/bluetooth/BluetoothUuid;->isAudioSource(Landroid/os/ParcelUuid;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_a

    #@8
    .line 61
    const/4 v0, 0x3

    #@9
    .line 103
    :cond_9
    :goto_9
    return v0

    #@a
    .line 63
    :cond_a
    invoke-static {p0}, Landroid/bluetooth/BluetoothUuid;->isHandsfree(Landroid/os/ParcelUuid;)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    .line 64
    const/4 v0, 0x6

    #@11
    goto :goto_9

    #@12
    .line 66
    :cond_12
    invoke-static {p0}, Landroid/bluetooth/BluetoothUuid;->isHeadset(Landroid/os/ParcelUuid;)Z

    #@15
    move-result v1

    #@16
    if-eqz v1, :cond_1a

    #@18
    .line 67
    const/4 v0, 0x5

    #@19
    goto :goto_9

    #@1a
    .line 69
    :cond_1a
    invoke-static {p0}, Landroid/bluetooth/BluetoothUuid;->isPanu(Landroid/os/ParcelUuid;)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_23

    #@20
    .line 70
    const/16 v0, 0xe

    #@22
    goto :goto_9

    #@23
    .line 72
    :cond_23
    invoke-static {p0}, Landroid/bluetooth/BluetoothUuid;->isNap(Landroid/os/ParcelUuid;)Z

    #@26
    move-result v1

    #@27
    if-eqz v1, :cond_2c

    #@29
    .line 73
    const/16 v0, 0xf

    #@2b
    goto :goto_9

    #@2c
    .line 75
    :cond_2c
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothDevice;->isPbap(Landroid/os/ParcelUuid;)Z

    #@2f
    move-result v1

    #@30
    if-eqz v1, :cond_35

    #@32
    .line 76
    const/16 v0, 0x16

    #@34
    goto :goto_9

    #@35
    .line 78
    :cond_35
    invoke-static {p0}, Landroid/bluetooth/BluetoothUuid;->isInputDevice(Landroid/os/ParcelUuid;)Z

    #@38
    move-result v1

    #@39
    if-eqz v1, :cond_3e

    #@3b
    .line 79
    const/16 v0, 0x14

    #@3d
    goto :goto_9

    #@3e
    .line 81
    :cond_3e
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothDevice;->isMap(Landroid/os/ParcelUuid;)Z

    #@41
    move-result v1

    #@42
    if-nez v1, :cond_9

    #@44
    .line 84
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothDevice;->isMse(Landroid/os/ParcelUuid;)Z

    #@47
    move-result v1

    #@48
    if-nez v1, :cond_9

    #@4a
    .line 87
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothDevice;->isMns(Landroid/os/ParcelUuid;)Z

    #@4d
    move-result v0

    #@4e
    if-eqz v0, :cond_53

    #@50
    .line 88
    const/16 v0, 0x1a

    #@52
    goto :goto_9

    #@53
    .line 90
    :cond_53
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothDevice;->isOpp(Landroid/os/ParcelUuid;)Z

    #@56
    move-result v0

    #@57
    if-eqz v0, :cond_5b

    #@59
    .line 91
    const/4 v0, 0x7

    #@5a
    goto :goto_9

    #@5b
    .line 93
    :cond_5b
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothDevice;->isFtp(Landroid/os/ParcelUuid;)Z

    #@5e
    move-result v0

    #@5f
    if-eqz v0, :cond_64

    #@61
    .line 94
    const/16 v0, 0x8

    #@63
    goto :goto_9

    #@64
    .line 96
    :cond_64
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothDevice;->isSerialPort(Landroid/os/ParcelUuid;)Z

    #@67
    move-result v0

    #@68
    if-eqz v0, :cond_6c

    #@6a
    .line 97
    const/4 v0, 0x1

    #@6b
    goto :goto_9

    #@6c
    .line 99
    :cond_6c
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothDevice;->isSap(Landroid/os/ParcelUuid;)Z

    #@6f
    move-result v0

    #@70
    if-eqz v0, :cond_75

    #@72
    .line 100
    const/16 v0, 0x11

    #@74
    goto :goto_9

    #@75
    .line 103
    :cond_75
    const/4 v0, 0x0

    #@76
    goto :goto_9
.end method
