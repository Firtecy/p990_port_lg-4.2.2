.class public Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;
.super Lcom/broadcom/bt/map/BaseDataSource;
.source "SmsMmsDataSource.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;,
        Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;,
        Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;
    }
.end annotation


# static fields
.field private static DBG:Z = false

.field private static final FOLDER_MAPPINGS:[Ljava/lang/String; = null

.field private static final FOLDER_MAPPINGS_NO_DELETE:[Ljava/lang/String; = null

.field private static final PROVIDER_DISPLAY_NAME_ID:I = 0x7f070090

.field private static final PROVIDER_ID:Ljava/lang/String; = "com.broadcom.bt.SMSMMS"

.field private static final SMS_MMS_ROOT_FOLDERS:[Ljava/lang/String; = null

.field private static final SMS_MMS_ROOT_FOLDERS_NO_DELETE:[Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "BtMap.SmsMmsDataSource"


# instance fields
.field private mDefaultOwnerNumber:Ljava/lang/String;

.field private mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

.field private mPhoneType:I

.field private mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x3

    #@2
    const/4 v4, 0x2

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, 0x1

    #@5
    .line 72
    sput-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@7
    .line 77
    const/4 v0, 0x5

    #@8
    new-array v0, v0, [Ljava/lang/String;

    #@a
    const-string v1, "inbox=inbox"

    #@c
    aput-object v1, v0, v3

    #@e
    const-string v1, "outbox=outbox"

    #@10
    aput-object v1, v0, v2

    #@12
    const-string v1, "sent=sent"

    #@14
    aput-object v1, v0, v4

    #@16
    const-string v1, "deleted=deleted"

    #@18
    aput-object v1, v0, v5

    #@1a
    const-string v1, "draft=draft"

    #@1c
    aput-object v1, v0, v6

    #@1e
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->FOLDER_MAPPINGS:[Ljava/lang/String;

    #@20
    .line 83
    const/4 v0, 0x5

    #@21
    new-array v0, v0, [Ljava/lang/String;

    #@23
    const-string v1, "inbox"

    #@25
    aput-object v1, v0, v3

    #@27
    const-string v1, "outbox"

    #@29
    aput-object v1, v0, v2

    #@2b
    const-string v1, "sent"

    #@2d
    aput-object v1, v0, v4

    #@2f
    const-string v1, "deleted"

    #@31
    aput-object v1, v0, v5

    #@33
    const-string v1, "draft"

    #@35
    aput-object v1, v0, v6

    #@37
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->SMS_MMS_ROOT_FOLDERS:[Ljava/lang/String;

    #@39
    .line 86
    new-array v0, v6, [Ljava/lang/String;

    #@3b
    const-string v1, "inbox=inbox"

    #@3d
    aput-object v1, v0, v3

    #@3f
    const-string v1, "outbox=outbox"

    #@41
    aput-object v1, v0, v2

    #@43
    const-string v1, "sent=sent"

    #@45
    aput-object v1, v0, v4

    #@47
    const-string v1, "draft=draft"

    #@49
    aput-object v1, v0, v5

    #@4b
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->FOLDER_MAPPINGS_NO_DELETE:[Ljava/lang/String;

    #@4d
    .line 92
    new-array v0, v6, [Ljava/lang/String;

    #@4f
    const-string v1, "inbox"

    #@51
    aput-object v1, v0, v3

    #@53
    const-string v1, "outbox"

    #@55
    aput-object v1, v0, v2

    #@57
    const-string v1, "sent"

    #@59
    aput-object v1, v0, v4

    #@5b
    const-string v1, "draft"

    #@5d
    aput-object v1, v0, v5

    #@5f
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->SMS_MMS_ROOT_FOLDERS_NO_DELETE:[Ljava/lang/String;

    #@61
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 70
    invoke-direct {p0}, Lcom/broadcom/bt/map/BaseDataSource;-><init>()V

    #@3
    .line 122
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->getSmsHelper()Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@9
    .line 123
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->getMmsHelper()Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@f
    .line 976
    return-void
.end method

.method static synthetic access$100(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;ZLcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/String;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getContactFilter(ZLcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@2
    return-object v0
.end method

.method static synthetic access$300()Z
    .registers 1

    #@0
    .prologue
    .line 70
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@2
    return v0
.end method

.method static synthetic access$400(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    iget v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 70
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getOwnerNumber()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Ljava/lang/String;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->isValidMsgFolder(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;ZLcom/broadcom/bt/map/MessageListFilter;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->matchesOwnerFilter(ZLcom/broadcom/bt/map/MessageListFilter;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$800(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Lcom/broadcom/bt/map/MessageListFilter;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->checkGetSmsMessages(Lcom/broadcom/bt/map/MessageListFilter;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$900(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Lcom/broadcom/bt/map/MessageListFilter;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->checkGetMmsMessages(Lcom/broadcom/bt/map/MessageListFilter;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private checkGetMmsMessages(Lcom/broadcom/bt/map/MessageListFilter;)Z
    .registers 5
    .parameter "messageListFilter"

    #@0
    .prologue
    .line 526
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    iget-byte v0, p1, Lcom/broadcom/bt/map/MessageListFilter;->mMsgMask:B

    #@6
    and-int/lit8 v0, v0, 0x8

    #@8
    if-lez v0, :cond_2a

    #@a
    .line 528
    :cond_a
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@c
    if-eqz v0, :cond_28

    #@e
    .line 529
    const-string v0, "BtMap.SmsMmsDataSource"

    #@10
    new-instance v1, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v2, "getMsgListing(): Filtering out MMS ... providerType = "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    iget v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 531
    :cond_28
    const/4 v0, 0x0

    #@29
    .line 533
    :goto_29
    return v0

    #@2a
    :cond_2a
    const/4 v0, 0x1

    #@2b
    goto :goto_29
.end method

.method private checkGetSmsMessages(Lcom/broadcom/bt/map/MessageListFilter;)Z
    .registers 5
    .parameter "messageListFilter"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v0, 0x0

    #@2
    .line 506
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@4
    if-eqz v1, :cond_10

    #@6
    iget-byte v1, p1, Lcom/broadcom/bt/map/MessageListFilter;->mMsgMask:B

    #@8
    and-int/lit8 v1, v1, 0x1

    #@a
    if-lez v1, :cond_1c

    #@c
    iget v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@e
    if-eq v1, v2, :cond_1c

    #@10
    .line 509
    :cond_10
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@12
    if-eqz v1, :cond_1b

    #@14
    .line 510
    const-string v1, "BtMap.SmsMmsDataSource"

    #@16
    const-string v2, "getMsgListing(): Filtering out SMS_GSM so we won\'t do SMS..."

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 521
    :cond_1b
    :goto_1b
    return v0

    #@1c
    .line 513
    :cond_1c
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@1e
    if-eqz v1, :cond_2a

    #@20
    iget-byte v1, p1, Lcom/broadcom/bt/map/MessageListFilter;->mMsgMask:B

    #@22
    and-int/lit8 v1, v1, 0x2

    #@24
    if-lez v1, :cond_36

    #@26
    iget v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@28
    if-ne v1, v2, :cond_36

    #@2a
    .line 516
    :cond_2a
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@2c
    if-eqz v1, :cond_1b

    #@2e
    .line 517
    const-string v1, "BtMap.SmsMmsDataSource"

    #@30
    const-string v2, "getMsgListing(): Filtering out SMS_CDMA so we won\'t do SMS..."

    #@32
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_1b

    #@36
    .line 521
    :cond_36
    const/4 v0, 0x1

    #@37
    goto :goto_1b
.end method

.method private getContactFilter(ZLcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/String;
    .registers 5
    .parameter "isInboundMessage"
    .parameter "messageListFilter"

    #@0
    .prologue
    .line 483
    const/4 v0, 0x0

    #@1
    .line 484
    .local v0, contactFilter:Ljava/lang/String;
    if-eqz p2, :cond_d

    #@3
    .line 485
    if-eqz p1, :cond_e

    #@5
    .line 486
    invoke-virtual {p2}, Lcom/broadcom/bt/map/MessageListFilter;->originatorFilterSet()Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_d

    #@b
    .line 487
    iget-object v0, p2, Lcom/broadcom/bt/map/MessageListFilter;->mOriginator:Ljava/lang/String;

    #@d
    .line 496
    :cond_d
    :goto_d
    return-object v0

    #@e
    .line 491
    :cond_e
    invoke-virtual {p2}, Lcom/broadcom/bt/map/MessageListFilter;->recipientFilterSet()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_d

    #@14
    .line 492
    iget-object v0, p2, Lcom/broadcom/bt/map/MessageListFilter;->mRecipient:Ljava/lang/String;

    #@16
    goto :goto_d
.end method

.method private getOwnerNumber()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 128
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const/4 v2, 0x2

    #@7
    invoke-static {v1, v2}, Lcom/broadcom/bt/map/ContactsUtil;->getOwnerNumber(Landroid/content/ContentResolver;I)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 130
    .local v0, ownerNumber:Ljava/lang/String;
    if-nez v0, :cond_f

    #@d
    .line 134
    const-string v0, ""

    #@f
    .line 137
    :cond_f
    return-object v0
.end method

.method private isValidMsgFolder(Ljava/lang/String;)Z
    .registers 3
    .parameter "folderPath"

    #@0
    .prologue
    .line 443
    invoke-static {p1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->isRootFolder(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_e

    #@6
    invoke-static {p1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->isValidFolderPath(Ljava/lang/String;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private matchesOwnerFilter(ZLcom/broadcom/bt/map/MessageListFilter;)Z
    .registers 6
    .parameter "isInboundMessage"
    .parameter "messageListFilter"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 456
    if-eqz p2, :cond_3d

    #@3
    .line 457
    if-eqz p1, :cond_21

    #@5
    .line 458
    invoke-virtual {p2}, Lcom/broadcom/bt/map/MessageListFilter;->recipientFilterSet()Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_3d

    #@b
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContentResolver:Landroid/content/ContentResolver;

    #@d
    iget-object v2, p2, Lcom/broadcom/bt/map/MessageListFilter;->mRecipient:Ljava/lang/String;

    #@f
    invoke-static {v1, v2}, Lcom/broadcom/bt/map/ContactsUtil;->ownerMatchesFilter(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_3d

    #@15
    .line 461
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@17
    if-eqz v1, :cond_20

    #@19
    .line 462
    const-string v1, "BtMap.SmsMmsDataSource"

    #@1b
    const-string v2, "Owner name does not match recipient filter. Return no messages"

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 479
    :cond_20
    :goto_20
    return v0

    #@21
    .line 467
    :cond_21
    invoke-virtual {p2}, Lcom/broadcom/bt/map/MessageListFilter;->originatorFilterSet()Z

    #@24
    move-result v1

    #@25
    if-eqz v1, :cond_3d

    #@27
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContentResolver:Landroid/content/ContentResolver;

    #@29
    iget-object v2, p2, Lcom/broadcom/bt/map/MessageListFilter;->mOriginator:Ljava/lang/String;

    #@2b
    invoke-static {v1, v2}, Lcom/broadcom/bt/map/ContactsUtil;->ownerMatchesFilter(Landroid/content/ContentResolver;Ljava/lang/String;)Z

    #@2e
    move-result v1

    #@2f
    if-nez v1, :cond_3d

    #@31
    .line 470
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@33
    if-eqz v1, :cond_20

    #@35
    .line 471
    const-string v1, "BtMap.SmsMmsDataSource"

    #@37
    const-string v2, "Owner name does not match originator filter. Return no messages"

    #@39
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_20

    #@3d
    .line 479
    :cond_3d
    const/4 v0, 0x1

    #@3e
    goto :goto_20
.end method


# virtual methods
.method protected getDatasourceDisplayName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 194
    :try_start_0
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@2
    if-eqz v1, :cond_12

    #@4
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@6
    if-eqz v1, :cond_12

    #@8
    .line 195
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContext:Landroid/content/Context;

    #@a
    const v2, 0x7f070093

    #@d
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    .line 209
    :goto_11
    return-object v1

    #@12
    .line 198
    :cond_12
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@14
    if-eqz v1, :cond_20

    #@16
    .line 199
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContext:Landroid/content/Context;

    #@18
    const v2, 0x7f070092

    #@1b
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    goto :goto_11

    #@20
    .line 201
    :cond_20
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@22
    if-eqz v1, :cond_2e

    #@24
    .line 202
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContext:Landroid/content/Context;

    #@26
    const v2, 0x7f070091

    #@29
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    goto :goto_11

    #@2e
    .line 205
    :cond_2e
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContext:Landroid/content/Context;

    #@30
    const v2, 0x7f070091

    #@33
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_36
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_36} :catch_38

    #@36
    move-result-object v1

    #@37
    goto :goto_11

    #@38
    .line 206
    :catch_38
    move-exception v0

    #@39
    .line 207
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "BtMap.SmsMmsDataSource"

    #@3b
    const-string v2, "Unable to get datasource display name"

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 209
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getDatasourceId()Ljava/lang/String;

    #@43
    move-result-object v1

    #@44
    goto :goto_11
.end method

.method protected getDatasourceId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 178
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@2
    if-eqz v0, :cond_b

    #@4
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@6
    if-eqz v0, :cond_b

    #@8
    .line 179
    const-string v0, "SMS-MMS"

    #@a
    .line 188
    :goto_a
    return-object v0

    #@b
    .line 181
    :cond_b
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@d
    if-eqz v0, :cond_12

    #@f
    .line 182
    const-string v0, "MMS"

    #@11
    goto :goto_a

    #@12
    .line 184
    :cond_12
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@14
    if-eqz v0, :cond_19

    #@16
    .line 185
    const-string v0, "SMS"

    #@18
    goto :goto_a

    #@19
    .line 188
    :cond_19
    const-string v0, "SMS-MMS"

    #@1b
    goto :goto_a
.end method

.method protected getDatasourceType()B
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    .line 165
    const/4 v0, 0x0

    #@2
    .line 166
    .local v0, dsType:B
    sget-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@4
    if-eqz v2, :cond_b

    #@6
    .line 167
    iget v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@8
    if-ne v2, v1, :cond_13

    #@a
    const/4 v0, 0x4

    #@b
    .line 170
    :cond_b
    :goto_b
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@d
    if-eqz v1, :cond_12

    #@f
    .line 171
    or-int/lit8 v1, v0, 0x8

    #@11
    int-to-byte v0, v1

    #@12
    .line 173
    :cond_12
    return v0

    #@13
    :cond_13
    move v0, v1

    #@14
    .line 167
    goto :goto_b
.end method

.method protected getFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/FolderListFilter;)V
    .registers 7
    .parameter "requestId"
    .parameter "folderPath"
    .parameter "folderListFilter"

    #@0
    .prologue
    .line 414
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@2
    if-eqz v0, :cond_34

    #@4
    .line 415
    const-string v0, "BtMap.SmsMmsDataSource"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "getFolderListing(): sessionId="

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    iget v2, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ", eventId="

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget v2, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, ", folderPath="

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 419
    :cond_34
    invoke-static {p2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->isRootFolder(Ljava/lang/String;)Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_40

    #@3a
    .line 423
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->SMS_MMS_ROOT_FOLDERS_NO_DELETE:[Ljava/lang/String;

    #@3c
    invoke-virtual {p0, p1, p2, v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setFolderListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;[Ljava/lang/String;)V

    #@3f
    .line 431
    :goto_3f
    return-void

    #@40
    .line 426
    :cond_40
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@42
    if-eqz v0, :cond_4b

    #@44
    .line 427
    const-string v0, "BtMap.SmsMmsDataSource"

    #@46
    const-string v1, "SMS-MMS datasource only has root folders"

    #@48
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 429
    :cond_4b
    const/4 v0, 0x0

    #@4c
    const/4 v1, 0x0

    #@4d
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setFolderListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLjava/util/List;)V

    #@50
    goto :goto_3f
.end method

.method protected getFolderMappings()[Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 217
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->FOLDER_MAPPINGS_NO_DELETE:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public getHandler()Landroid/os/Handler;
    .registers 2

    #@0
    .prologue
    .line 145
    invoke-super {p0}, Lcom/broadcom/bt/map/BaseDataSource;->getHandler()Landroid/os/Handler;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method protected getMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZB)V
    .registers 16
    .parameter "requestId"
    .parameter "folderPath"
    .parameter "virtualPath"
    .parameter "messageId"
    .parameter "includeAttachments"
    .parameter "charsetid"

    #@0
    .prologue
    .line 698
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@2
    if-eqz v0, :cond_4a

    #@4
    .line 699
    const-string v0, "BtMap.SmsMmsDataSource"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "getMessage(): sessionId="

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    iget v2, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ", eventId="

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    iget v2, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, ", folderPath="

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    const-string v2, " messageId="

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    const-string v2, ", providerType = "

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    iget v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 704
    :cond_4a
    invoke-direct {p0, p2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->isValidMsgFolder(Ljava/lang/String;)Z

    #@4d
    move-result v0

    #@4e
    if-nez v0, :cond_6e

    #@50
    .line 705
    const-string v0, "BtMap.SmsMmsDataSource"

    #@52
    new-instance v1, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v2, "getMessage(): Invalid folder path:"

    #@59
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v1

    #@5d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v1

    #@61
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v1

    #@65
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 706
    const/4 v0, 0x0

    #@69
    const/4 v1, 0x0

    #@6a
    invoke-virtual {p0, p1, p4, v0, v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setGetMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLcom/broadcom/bt/util/bmsg/BMessage;)V

    #@6d
    .line 729
    :goto_6d
    return-void

    #@6e
    .line 711
    :cond_6e
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@70
    if-eqz v0, :cond_9f

    #@72
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@74
    invoke-interface {v0, p4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->isEncodedMessageId(Ljava/lang/String;)Z

    #@77
    move-result v0

    #@78
    if-eqz v0, :cond_9f

    #@7a
    .line 712
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@7c
    if-eqz v0, :cond_85

    #@7e
    .line 713
    const-string v0, "BtMap.SmsMmsDataSource"

    #@80
    const-string v1, "getMessage(): processing SMS message"

    #@82
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    .line 715
    :cond_85
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@87
    iget v4, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@89
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getOwnerNumber()Ljava/lang/String;

    #@8c
    move-result-object v5

    #@8d
    move-object v1, p2

    #@8e
    move-object v2, p3

    #@8f
    move-object v3, p4

    #@90
    move v6, p5

    #@91
    move v7, p6

    #@92
    invoke-interface/range {v0 .. v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->getMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZB)Lcom/broadcom/bt/util/bmsg/BMessage;

    #@95
    move-result-object v8

    #@96
    .line 717
    .local v8, msg:Lcom/broadcom/bt/util/bmsg/BMessage;
    if-nez v8, :cond_9d

    #@98
    const/4 v0, 0x0

    #@99
    :goto_99
    invoke-virtual {p0, p1, p4, v0, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setGetMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLcom/broadcom/bt/util/bmsg/BMessage;)V

    #@9c
    goto :goto_6d

    #@9d
    :cond_9d
    const/4 v0, 0x1

    #@9e
    goto :goto_99

    #@9f
    .line 718
    .end local v8           #msg:Lcom/broadcom/bt/util/bmsg/BMessage;
    :cond_9f
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@a1
    if-eqz v0, :cond_d0

    #@a3
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@a5
    invoke-interface {v0, p4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->isEncodedMessageId(Ljava/lang/String;)Z

    #@a8
    move-result v0

    #@a9
    if-eqz v0, :cond_d0

    #@ab
    .line 719
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@ad
    if-eqz v0, :cond_b6

    #@af
    .line 720
    const-string v0, "BtMap.SmsMmsDataSource"

    #@b1
    const-string v1, "getMessage(): processing MMS message"

    #@b3
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 722
    :cond_b6
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@b8
    iget v4, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@ba
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getOwnerNumber()Ljava/lang/String;

    #@bd
    move-result-object v5

    #@be
    move-object v1, p2

    #@bf
    move-object v2, p3

    #@c0
    move-object v3, p4

    #@c1
    move v6, p5

    #@c2
    move v7, p6

    #@c3
    invoke-interface/range {v0 .. v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->getMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZB)Lcom/broadcom/bt/util/bmsg/BMessage;

    #@c6
    move-result-object v8

    #@c7
    .line 724
    .restart local v8       #msg:Lcom/broadcom/bt/util/bmsg/BMessage;
    if-nez v8, :cond_ce

    #@c9
    const/4 v0, 0x0

    #@ca
    :goto_ca
    invoke-virtual {p0, p1, p4, v0, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setGetMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLcom/broadcom/bt/util/bmsg/BMessage;)V

    #@cd
    goto :goto_6d

    #@ce
    :cond_ce
    const/4 v0, 0x1

    #@cf
    goto :goto_ca

    #@d0
    .line 726
    .end local v8           #msg:Lcom/broadcom/bt/util/bmsg/BMessage;
    :cond_d0
    const-string v0, "BtMap.SmsMmsDataSource"

    #@d2
    const-string v1, "Unknown message type..Ignoring request"

    #@d4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d7
    .line 727
    const/4 v0, 0x0

    #@d8
    const/4 v1, 0x0

    #@d9
    invoke-virtual {p0, p1, p4, v0, v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setGetMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLcom/broadcom/bt/util/bmsg/BMessage;)V

    #@dc
    goto :goto_6d
.end method

.method protected getMsgListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V
    .registers 6
    .parameter "requestId"
    .parameter "folderPath"
    .parameter "messageListFilter"
    .parameter "paramFilter"

    #@0
    .prologue
    .line 628
    new-instance v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;

    #@2
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;-><init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)V

    #@5
    .line 690
    .local v0, p:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->init(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V

    #@8
    .line 691
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->run()V

    #@b
    .line 693
    return-void
.end method

.method protected getMsgListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V
    .registers 6
    .parameter "requestId"
    .parameter "folderPath"
    .parameter "messageListFilter"
    .parameter "paramFilter"

    #@0
    .prologue
    .line 540
    new-instance v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;

    #@2
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;-><init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)V

    #@5
    .line 617
    .local v0, p:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->init(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V

    #@8
    .line 618
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->run()V

    #@b
    .line 619
    return-void
.end method

.method public getPhoneType()I
    .registers 2

    #@0
    .prologue
    .line 141
    iget v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@2
    return v0
.end method

.method protected getProviderDisplayName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 156
    :try_start_0
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContext:Landroid/content/Context;

    #@2
    const v2, 0x7f070090

    #@5
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_8} :catch_a

    #@8
    move-result-object v1

    #@9
    .line 160
    :goto_9
    return-object v1

    #@a
    .line 157
    :catch_a
    move-exception v0

    #@b
    .line 158
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "BtMap.SmsMmsDataSource"

    #@d
    const-string v2, "Unable to get provider display name"

    #@f
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 160
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getProviderId()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    goto :goto_9
.end method

.method protected getProviderId()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 150
    const-string v0, "com.broadcom.bt.SMSMMS"

    #@2
    return-object v0
.end method

.method protected declared-synchronized onStarted()V
    .registers 3

    #@0
    .prologue
    .line 251
    monitor-enter p0

    #@1
    :try_start_1
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 252
    const-string v0, "BtMap.SmsMmsDataSource"

    #@7
    const-string v1, "onStarted()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catchall {:try_start_1 .. :try_end_c} :catchall_e

    #@c
    .line 254
    :cond_c
    monitor-exit p0

    #@d
    return-void

    #@e
    .line 251
    :catchall_e
    move-exception v0

    #@f
    monitor-exit p0

    #@10
    throw v0
.end method

.method protected declared-synchronized onStopped()V
    .registers 3

    #@0
    .prologue
    .line 258
    monitor-enter p0

    #@1
    :try_start_1
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@3
    if-eqz v0, :cond_c

    #@5
    .line 259
    const-string v0, "BtMap.SmsMmsDataSource"

    #@7
    const-string v1, "onStopped()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 261
    :cond_c
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 262
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@12
    invoke-interface {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->finish()V

    #@15
    .line 264
    :cond_15
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@17
    if-eqz v0, :cond_1e

    #@19
    .line 265
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@1b
    invoke-interface {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->finish()V
    :try_end_1e
    .catchall {:try_start_1 .. :try_end_1e} :catchall_20

    #@1e
    .line 267
    :cond_1e
    monitor-exit p0

    #@1f
    return-void

    #@20
    .line 258
    :catchall_20
    move-exception v0

    #@21
    monitor-exit p0

    #@22
    throw v0
.end method

.method protected declared-synchronized preStart()V
    .registers 4

    #@0
    .prologue
    .line 233
    monitor-enter p0

    #@1
    :try_start_1
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@3
    if-eqz v1, :cond_c

    #@5
    .line 234
    const-string v1, "BtMap.SmsMmsDataSource"

    #@7
    const-string v2, "preStart()"

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 236
    :cond_c
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContext:Landroid/content/Context;

    #@e
    const-string v2, "phone"

    #@10
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@16
    .line 238
    .local v0, mgr:Landroid/telephony/TelephonyManager;
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    #@19
    move-result v1

    #@1a
    iput v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@1c
    .line 239
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    iput-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mDefaultOwnerNumber:Ljava/lang/String;

    #@22
    .line 240
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContext:Landroid/content/Context;

    #@24
    invoke-static {v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->init(Landroid/content/Context;)V

    #@27
    .line 241
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@29
    if-eqz v1, :cond_32

    #@2b
    .line 242
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@2d
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContext:Landroid/content/Context;

    #@2f
    invoke-interface {v1, p0, v2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->init(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Landroid/content/Context;)V

    #@32
    .line 244
    :cond_32
    sget-boolean v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@34
    if-eqz v1, :cond_3d

    #@36
    .line 245
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@38
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mContext:Landroid/content/Context;

    #@3a
    invoke-interface {v1, p0, v2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->init(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Landroid/content/Context;)V
    :try_end_3d
    .catchall {:try_start_1 .. :try_end_3d} :catchall_3f

    #@3d
    .line 247
    :cond_3d
    monitor-exit p0

    #@3e
    return-void

    #@3f
    .line 233
    .end local v0           #mgr:Landroid/telephony/TelephonyManager;
    :catchall_3f
    move-exception v1

    #@40
    monitor-exit p0

    #@41
    throw v1
.end method

.method protected pushMessage(Lcom/broadcom/bt/map/RequestId;Lcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;Ljava/lang/String;ZZI)V
    .registers 32
    .parameter "requestId"
    .parameter "bMessage"
    .parameter "folderPath"
    .parameter "virtualFolderPath"
    .parameter "isTransparentMsg"
    .parameter "isRetry"
    .parameter "charset"

    #@0
    .prologue
    .line 272
    sget-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@2
    if-eqz v2, :cond_6a

    #@4
    .line 273
    const-string v2, "BtMap.SmsMmsDataSource"

    #@6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v4, "pushMessage(): sessionId="

    #@d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    move-object/from16 v0, p1

    #@13
    iget v4, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, ", eventId="

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    move-object/from16 v0, p1

    #@21
    iget v4, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, ", folderPath="

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    move-object/from16 v0, p3

    #@2f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, ",virtualFolderPath="

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    move-object/from16 v0, p4

    #@3b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    const-string v4, ", isTransparentMsg="

    #@41
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    move/from16 v0, p5

    #@47
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    const-string v4, ",isRetry="

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    move/from16 v0, p6

    #@53
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    const-string v4, ", charset="

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    move/from16 v0, p7

    #@5f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v3

    #@67
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 278
    :cond_6a
    move-object/from16 v0, p0

    #@6c
    move-object/from16 v1, p3

    #@6e
    invoke-direct {v0, v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->isValidMsgFolder(Ljava/lang/String;)Z

    #@71
    move-result v2

    #@72
    if-nez v2, :cond_8a

    #@74
    .line 279
    const-string v2, "BtMap.SmsMmsDataSource"

    #@76
    const-string v3, "pushMessage(): invalid destination folder"

    #@78
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 280
    const/4 v6, 0x0

    #@7c
    const-string v7, ""

    #@7e
    move-object/from16 v2, p0

    #@80
    move-object/from16 v3, p1

    #@82
    move-object/from16 v4, p2

    #@84
    move-object/from16 v5, p3

    #@86
    invoke-virtual/range {v2 .. v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Lcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;ZLjava/lang/String;)V

    #@89
    .line 327
    :cond_89
    :goto_89
    return-void

    #@8a
    .line 283
    :cond_8a
    invoke-virtual/range {p2 .. p2}, Lcom/broadcom/bt/util/bmsg/BMessage;->getMessageType()B

    #@8d
    move-result v23

    #@8e
    .line 284
    .local v23, msgType:B
    sget-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@90
    if-eqz v2, :cond_ac

    #@92
    .line 285
    const-string v2, "BtMap.SmsMmsDataSource"

    #@94
    new-instance v3, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v4, "pushMessage(): processing message type "

    #@9b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v3

    #@9f
    move/from16 v0, v23

    #@a1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v3

    #@a9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 287
    :cond_ac
    const/4 v7, 0x0

    #@ad
    .line 288
    .local v7, messageId:Ljava/lang/String;
    sget-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@af
    if-eqz v2, :cond_fd

    #@b1
    const/4 v2, 0x4

    #@b2
    move/from16 v0, v23

    #@b4
    if-eq v0, v2, :cond_bb

    #@b6
    const/4 v2, 0x2

    #@b7
    move/from16 v0, v23

    #@b9
    if-ne v0, v2, :cond_fd

    #@bb
    .line 290
    :cond_bb
    sget-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@bd
    if-eqz v2, :cond_c6

    #@bf
    .line 291
    const-string v2, "BtMap.SmsMmsDataSource"

    #@c1
    const-string v3, "pushMessage(): processing SMS"

    #@c3
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c6
    .line 293
    :cond_c6
    const/4 v2, 0x1

    #@c7
    new-array v11, v2, [Ljava/lang/String;

    #@c9
    .line 294
    .local v11, newFolderPath:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@cb
    iget-object v2, v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@cd
    move-object/from16 v0, p0

    #@cf
    iget v4, v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@d1
    move-object/from16 v3, p1

    #@d3
    move-object/from16 v5, p2

    #@d5
    move-object/from16 v6, p3

    #@d7
    move-object/from16 v7, p4

    #@d9
    move/from16 v8, p5

    #@db
    move/from16 v9, p6

    #@dd
    move/from16 v10, p7

    #@df
    invoke-interface/range {v2 .. v11}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->pushMessage(Lcom/broadcom/bt/map/RequestId;ILcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;Ljava/lang/String;ZZI[Ljava/lang/String;)Ljava/lang/String;

    #@e2
    .end local v7           #messageId:Ljava/lang/String;
    move-result-object v7

    #@e3
    .line 296
    .restart local v7       #messageId:Ljava/lang/String;
    const/4 v2, 0x0

    #@e4
    aget-object v2, v11, v2

    #@e6
    if-eqz v2, :cond_f8

    #@e8
    const/4 v2, 0x0

    #@e9
    aget-object v5, v11, v2

    #@eb
    :goto_eb
    if-nez v7, :cond_fb

    #@ed
    const/4 v6, 0x0

    #@ee
    :goto_ee
    move-object/from16 v2, p0

    #@f0
    move-object/from16 v3, p1

    #@f2
    move-object/from16 v4, p2

    #@f4
    invoke-virtual/range {v2 .. v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Lcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;ZLjava/lang/String;)V

    #@f7
    goto :goto_89

    #@f8
    :cond_f8
    move-object/from16 v5, p3

    #@fa
    goto :goto_eb

    #@fb
    :cond_fb
    const/4 v6, 0x1

    #@fc
    goto :goto_ee

    #@fd
    .line 298
    .end local v11           #newFolderPath:[Ljava/lang/String;
    :cond_fd
    sget-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@ff
    if-eqz v2, :cond_182

    #@101
    const/16 v2, 0x8

    #@103
    move/from16 v0, v23

    #@105
    if-ne v0, v2, :cond_182

    #@107
    .line 299
    sget-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@109
    if-eqz v2, :cond_112

    #@10b
    .line 300
    const-string v2, "BtMap.SmsMmsDataSource"

    #@10d
    const-string v3, "pushMessage(): processing MMS"

    #@10f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@112
    .line 302
    :cond_112
    const/4 v2, 0x1

    #@113
    new-array v11, v2, [Ljava/lang/String;

    #@115
    .line 303
    .restart local v11       #newFolderPath:[Ljava/lang/String;
    move-object/from16 v0, p0

    #@117
    iget-object v12, v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@119
    move-object/from16 v0, p0

    #@11b
    iget v14, v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I

    #@11d
    move-object/from16 v13, p1

    #@11f
    move-object/from16 v15, p2

    #@121
    move-object/from16 v16, p3

    #@123
    move-object/from16 v17, p4

    #@125
    move/from16 v18, p5

    #@127
    move/from16 v19, p6

    #@129
    move/from16 v20, p7

    #@12b
    move-object/from16 v21, v11

    #@12d
    invoke-interface/range {v12 .. v21}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->pushMessage(Lcom/broadcom/bt/map/RequestId;ILcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;Ljava/lang/String;ZZI[Ljava/lang/String;)Ljava/lang/String;

    #@130
    move-result-object v7

    #@131
    .line 305
    if-eqz v7, :cond_174

    #@133
    const/4 v6, 0x1

    #@134
    :goto_134
    move-object/from16 v2, p0

    #@136
    move-object/from16 v3, p1

    #@138
    move-object/from16 v4, p2

    #@13a
    move-object/from16 v5, p3

    #@13c
    invoke-virtual/range {v2 .. v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Lcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;ZLjava/lang/String;)V

    #@13f
    .line 309
    move-object/from16 v0, p0

    #@141
    iget-object v2, v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@143
    move-object/from16 v0, p3

    #@145
    invoke-interface {v2, v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->getMessageType(Ljava/lang/String;)I

    #@148
    move-result v22

    #@149
    .line 311
    .local v22, mBox:I
    const/4 v2, 0x4

    #@14a
    move/from16 v0, v22

    #@14c
    if-ne v0, v2, :cond_89

    #@14e
    invoke-virtual/range {p0 .. p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->isNotificationEnabled()Z

    #@151
    move-result v2

    #@152
    if-eqz v2, :cond_89

    #@154
    .line 312
    sget-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@156
    if-eqz v2, :cond_15f

    #@158
    .line 313
    const-string v2, "BtMap.SmsMmsDataSource"

    #@15a
    const-string v3, "Sending send notification.."

    #@15c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15f
    .line 315
    :cond_15f
    if-eqz v7, :cond_176

    #@161
    .line 316
    const/16 v2, 0x8

    #@163
    move-object/from16 v0, p0

    #@165
    iget-object v3, v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@167
    const/4 v4, 0x2

    #@168
    invoke-interface {v3, v4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->getFolderPath(I)Ljava/lang/String;

    #@16b
    move-result-object v3

    #@16c
    const/4 v4, 0x1

    #@16d
    move-object/from16 v0, p0

    #@16f
    invoke-virtual {v0, v7, v2, v3, v4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifySendStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@172
    goto/16 :goto_89

    #@174
    .line 305
    .end local v22           #mBox:I
    :cond_174
    const/4 v6, 0x0

    #@175
    goto :goto_134

    #@176
    .line 319
    .restart local v22       #mBox:I
    :cond_176
    const/16 v2, 0x8

    #@178
    const/4 v3, 0x0

    #@179
    move-object/from16 v0, p0

    #@17b
    move-object/from16 v1, p3

    #@17d
    invoke-virtual {v0, v7, v2, v1, v3}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifySendStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@180
    goto/16 :goto_89

    #@182
    .line 324
    .end local v11           #newFolderPath:[Ljava/lang/String;
    .end local v22           #mBox:I
    :cond_182
    const-string v2, "BtMap.SmsMmsDataSource"

    #@184
    new-instance v3, Ljava/lang/StringBuilder;

    #@186
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@189
    const-string v4, "Unable to process unsupported BMessage type "

    #@18b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v3

    #@18f
    move/from16 v0, v23

    #@191
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@194
    move-result-object v3

    #@195
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v3

    #@199
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19c
    .line 325
    const/16 v16, 0x0

    #@19e
    const/16 v17, 0x0

    #@1a0
    move-object/from16 v12, p0

    #@1a2
    move-object/from16 v13, p1

    #@1a4
    move-object/from16 v14, p2

    #@1a6
    move-object/from16 v15, p3

    #@1a8
    invoke-virtual/range {v12 .. v17}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Lcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;ZLjava/lang/String;)V

    #@1ab
    goto/16 :goto_89
.end method

.method protected setMessageDeleted(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z)V
    .registers 16
    .parameter "requestId"
    .parameter "messageId"
    .parameter "setDeleted"

    #@0
    .prologue
    .line 336
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@2
    if-eqz v0, :cond_1c

    #@4
    .line 337
    const-string v0, "BtMap.SmsMmsDataSource"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "setMessageDeleted(): messageId = "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 339
    :cond_1c
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@1e
    if-eqz v0, :cond_84

    #@20
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@22
    invoke-interface {v0, p2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->isEncodedMessageId(Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_84

    #@28
    .line 340
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@2a
    if-eqz v0, :cond_33

    #@2c
    .line 341
    const-string v0, "BtMap.SmsMmsDataSource"

    #@2e
    const-string v1, "setMessageDeleted(): Setting SMS message delete status ..."

    #@30
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 343
    :cond_33
    const/4 v0, 0x1

    #@34
    new-array v4, v0, [Ljava/lang/String;

    #@36
    .line 344
    .local v4, folderPath:[Ljava/lang/String;
    const/4 v0, 0x1

    #@37
    new-array v5, v0, [Ljava/lang/String;

    #@39
    .line 345
    .local v5, newMessageId:[Ljava/lang/String;
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@3b
    move-object v1, p1

    #@3c
    move-object v2, p2

    #@3d
    move v3, p3

    #@3e
    invoke-interface/range {v0 .. v5}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->setMessageDeleted(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z[Ljava/lang/String;[Ljava/lang/String;)Z

    #@41
    move-result v10

    #@42
    .line 347
    .local v10, success:Z
    if-nez v10, :cond_53

    #@44
    .line 348
    if-eqz v10, :cond_51

    #@46
    const/4 v0, 0x0

    #@47
    aget-object v11, v4, v0

    #@49
    :goto_49
    move-object v6, p0

    #@4a
    move-object v7, p1

    #@4b
    move-object v8, p2

    #@4c
    move v9, p3

    #@4d
    invoke-virtual/range {v6 .. v11}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V

    #@50
    .line 387
    .end local v4           #folderPath:[Ljava/lang/String;
    .end local v5           #newMessageId:[Ljava/lang/String;
    .end local v10           #success:Z
    :goto_50
    return-void

    #@51
    .line 348
    .restart local v4       #folderPath:[Ljava/lang/String;
    .restart local v5       #newMessageId:[Ljava/lang/String;
    .restart local v10       #success:Z
    :cond_51
    const/4 v11, 0x0

    #@52
    goto :goto_49

    #@53
    .line 351
    :cond_53
    const/4 v0, 0x0

    #@54
    aget-object v0, v5, v0

    #@56
    if-eqz v0, :cond_6f

    #@58
    .line 352
    const/4 v0, 0x0

    #@59
    aget-object v0, v5, v0

    #@5b
    invoke-virtual {p0, p2, v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->updateMessageId(Ljava/lang/String;Ljava/lang/String;)V

    #@5e
    .line 353
    const/4 v0, 0x0

    #@5f
    aget-object v8, v5, v0

    #@61
    if-eqz v10, :cond_6d

    #@63
    const/4 v0, 0x0

    #@64
    aget-object v11, v4, v0

    #@66
    :goto_66
    move-object v6, p0

    #@67
    move-object v7, p1

    #@68
    move v9, p3

    #@69
    invoke-virtual/range {v6 .. v11}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V

    #@6c
    goto :goto_50

    #@6d
    :cond_6d
    const/4 v11, 0x0

    #@6e
    goto :goto_66

    #@6f
    .line 356
    :cond_6f
    if-eqz v10, :cond_82

    #@71
    const/4 v0, 0x0

    #@72
    aget-object v11, v4, v0

    #@74
    :goto_74
    move-object v6, p0

    #@75
    move-object v7, p1

    #@76
    move-object v8, p2

    #@77
    move v9, p3

    #@78
    invoke-virtual/range {v6 .. v11}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V

    #@7b
    .line 358
    const/4 v0, 0x0

    #@7c
    aget-object v0, v5, v0

    #@7e
    invoke-virtual {p0, p2, v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->updateMessageId(Ljava/lang/String;Ljava/lang/String;)V

    #@81
    goto :goto_50

    #@82
    .line 356
    :cond_82
    const/4 v11, 0x0

    #@83
    goto :goto_74

    #@84
    .line 361
    .end local v4           #folderPath:[Ljava/lang/String;
    .end local v5           #newMessageId:[Ljava/lang/String;
    .end local v10           #success:Z
    :cond_84
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@86
    if-eqz v0, :cond_ee

    #@88
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@8a
    invoke-interface {v0, p2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->isEncodedMessageId(Ljava/lang/String;)Z

    #@8d
    move-result v0

    #@8e
    if-eqz v0, :cond_ee

    #@90
    .line 362
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@92
    if-eqz v0, :cond_9b

    #@94
    .line 363
    const-string v0, "BtMap.SmsMmsDataSource"

    #@96
    const-string v1, "setMessageDeleted(): Setting MMS message deleted status..."

    #@98
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9b
    .line 365
    :cond_9b
    const/4 v0, 0x1

    #@9c
    new-array v4, v0, [Ljava/lang/String;

    #@9e
    .line 366
    .restart local v4       #folderPath:[Ljava/lang/String;
    const/4 v0, 0x1

    #@9f
    new-array v5, v0, [Ljava/lang/String;

    #@a1
    .line 367
    .restart local v5       #newMessageId:[Ljava/lang/String;
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@a3
    move-object v1, p1

    #@a4
    move-object v2, p2

    #@a5
    move v3, p3

    #@a6
    invoke-interface/range {v0 .. v5}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->setMessageDeleted(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z[Ljava/lang/String;[Ljava/lang/String;)Z

    #@a9
    move-result v10

    #@aa
    .line 369
    .restart local v10       #success:Z
    if-nez v10, :cond_bb

    #@ac
    .line 370
    if-eqz v10, :cond_b9

    #@ae
    const/4 v0, 0x0

    #@af
    aget-object v11, v4, v0

    #@b1
    :goto_b1
    move-object v6, p0

    #@b2
    move-object v7, p1

    #@b3
    move-object v8, p2

    #@b4
    move v9, p3

    #@b5
    invoke-virtual/range {v6 .. v11}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V

    #@b8
    goto :goto_50

    #@b9
    :cond_b9
    const/4 v11, 0x0

    #@ba
    goto :goto_b1

    #@bb
    .line 373
    :cond_bb
    const/4 v0, 0x0

    #@bc
    aget-object v0, v5, v0

    #@be
    if-eqz v0, :cond_d8

    #@c0
    .line 374
    const/4 v0, 0x0

    #@c1
    aget-object v0, v5, v0

    #@c3
    invoke-virtual {p0, p2, v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->updateMessageId(Ljava/lang/String;Ljava/lang/String;)V

    #@c6
    .line 375
    const/4 v0, 0x0

    #@c7
    aget-object v8, v5, v0

    #@c9
    if-eqz v10, :cond_d6

    #@cb
    const/4 v0, 0x0

    #@cc
    aget-object v11, v4, v0

    #@ce
    :goto_ce
    move-object v6, p0

    #@cf
    move-object v7, p1

    #@d0
    move v9, p3

    #@d1
    invoke-virtual/range {v6 .. v11}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V

    #@d4
    goto/16 :goto_50

    #@d6
    :cond_d6
    const/4 v11, 0x0

    #@d7
    goto :goto_ce

    #@d8
    .line 378
    :cond_d8
    if-eqz v10, :cond_ec

    #@da
    const/4 v0, 0x0

    #@db
    aget-object v11, v4, v0

    #@dd
    :goto_dd
    move-object v6, p0

    #@de
    move-object v7, p1

    #@df
    move-object v8, p2

    #@e0
    move v9, p3

    #@e1
    invoke-virtual/range {v6 .. v11}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V

    #@e4
    .line 380
    const/4 v0, 0x0

    #@e5
    aget-object v0, v5, v0

    #@e7
    invoke-virtual {p0, p2, v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->updateMessageId(Ljava/lang/String;Ljava/lang/String;)V

    #@ea
    goto/16 :goto_50

    #@ec
    .line 378
    :cond_ec
    const/4 v11, 0x0

    #@ed
    goto :goto_dd

    #@ee
    .line 385
    .end local v4           #folderPath:[Ljava/lang/String;
    .end local v5           #newMessageId:[Ljava/lang/String;
    .end local v10           #success:Z
    :cond_ee
    const-string v0, "BtMap.SmsMmsDataSource"

    #@f0
    const-string v1, "setMessageDeleted(): Received invalid message type..."

    #@f2
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f5
    goto/16 :goto_50
.end method

.method protected setMessageRead(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z)V
    .registers 7
    .parameter "requestId"
    .parameter "messageId"
    .parameter "isRead"

    #@0
    .prologue
    .line 390
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@2
    if-eqz v0, :cond_1c

    #@4
    .line 391
    const-string v0, "BtMap.SmsMmsDataSource"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "setMessageRead(): messageId = "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 393
    :cond_1c
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@1e
    if-eqz v0, :cond_39

    #@20
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@22
    invoke-interface {v0, p2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->isEncodedMessageId(Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_39

    #@28
    .line 394
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@2a
    if-eqz v0, :cond_33

    #@2c
    .line 395
    const-string v0, "BtMap.SmsMmsDataSource"

    #@2e
    const-string v1, "setMessageRead(): Setting SMS message read status ..."

    #@30
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 397
    :cond_33
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@35
    invoke-interface {v0, p1, p2, p3}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->setMessageRead(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z)V

    #@38
    .line 406
    :goto_38
    return-void

    #@39
    .line 398
    :cond_39
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@3b
    if-eqz v0, :cond_56

    #@3d
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@3f
    invoke-interface {v0, p2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->isEncodedMessageId(Ljava/lang/String;)Z

    #@42
    move-result v0

    #@43
    if-eqz v0, :cond_56

    #@45
    .line 399
    sget-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->DBG:Z

    #@47
    if-eqz v0, :cond_50

    #@49
    .line 400
    const-string v0, "BtMap.SmsMmsDataSource"

    #@4b
    const-string v1, "setMessageRead(): Setting MMS message read status ..."

    #@4d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 402
    :cond_50
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@52
    invoke-interface {v0, p1, p2, p3}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->setMessageRead(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z)V

    #@55
    goto :goto_38

    #@56
    .line 404
    :cond_56
    const-string v0, "BtMap.SmsMmsDataSource"

    #@58
    const-string v1, "setMessageRead(): Received invalid message type..."

    #@5a
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_38
.end method

.method protected supportsMessageFiltering()Z
    .registers 2

    #@0
    .prologue
    .line 223
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected supportsMessageOffsetBrowsing()Z
    .registers 2

    #@0
    .prologue
    .line 228
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected updateInbox()V
    .registers 3

    #@0
    .prologue
    .line 331
    const-string v0, "BtMap.SmsMmsDataSource"

    #@2
    const-string v1, "SMS/MMS Datasource does not support updateInbox() MSE call"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 332
    return-void
.end method
