.class Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;
.super Lcom/broadcom/bt/service/ftp/IBluetoothFTP$Stub;
.source "FTPService.java"

# interfaces
.implements Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/ftp/FTPService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BluetoothFtpBinder"
.end annotation


# instance fields
.field private mService:Lcom/broadcom/bt/service/ftp/FTPService;

.field final synthetic this$0:Lcom/broadcom/bt/service/ftp/FTPService;


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/service/ftp/FTPService;Lcom/broadcom/bt/service/ftp/FTPService;)V
    .registers 5
    .parameter
    .parameter "svc"

    #@0
    .prologue
    .line 413
    iput-object p1, p0, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@2
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/IBluetoothFTP$Stub;-><init>()V

    #@5
    .line 414
    const-string v0, "BluetoothFTPService"

    #@7
    const-string v1, "BluetoothFtpBinder()"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 415
    iput-object p2, p0, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->mService:Lcom/broadcom/bt/service/ftp/FTPService;

    #@e
    .line 416
    return-void
.end method

.method private getService()Lcom/broadcom/bt/service/ftp/FTPService;
    .registers 2

    #@0
    .prologue
    .line 425
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->mService:Lcom/broadcom/bt/service/ftp/FTPService;

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->mService:Lcom/broadcom/bt/service/ftp/FTPService;

    #@6
    invoke-virtual {v0}, Lcom/broadcom/bt/service/ftp/FTPService;->isAvailable()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_f

    #@c
    .line 426
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->mService:Lcom/broadcom/bt/service/ftp/FTPService;

    #@e
    .line 428
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method


# virtual methods
.method public cleanup()Z
    .registers 3

    #@0
    .prologue
    .line 419
    const-string v0, "BluetoothFTPService"

    #@2
    const-string v1, "BluetoothFtpBinder.cleanup()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 420
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->mService:Lcom/broadcom/bt/service/ftp/FTPService;

    #@a
    .line 421
    const/4 v0, 0x1

    #@b
    return v0
.end method

.method public ftpServerAccessRsp(BZLjava/lang/String;)V
    .registers 5
    .parameter "op_code"
    .parameter "access"
    .parameter "filename"

    #@0
    .prologue
    .line 462
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->getService()Lcom/broadcom/bt/service/ftp/FTPService;

    #@3
    move-result-object v0

    #@4
    .line 463
    .local v0, service:Lcom/broadcom/bt/service/ftp/FTPService;
    if-nez v0, :cond_7

    #@6
    .line 467
    :goto_6
    return-void

    #@7
    .line 466
    :cond_7
    invoke-virtual {v0, p1, p2, p3}, Lcom/broadcom/bt/service/ftp/FTPService;->ftpServerAccessRsp(BZLjava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method public ftpServerAuthenRsp(Ljava/lang/String;Ljava/lang/String;)V
    .registers 4
    .parameter "password"
    .parameter "user_id"

    #@0
    .prologue
    .line 453
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->getService()Lcom/broadcom/bt/service/ftp/FTPService;

    #@3
    move-result-object v0

    #@4
    .line 454
    .local v0, service:Lcom/broadcom/bt/service/ftp/FTPService;
    if-nez v0, :cond_7

    #@6
    .line 458
    :goto_6
    return-void

    #@7
    .line 457
    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/ftp/FTPService;->ftpServerAuthenRsp(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 439
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [I

    #@3
    const/4 v1, 0x0

    #@4
    const/4 v2, 0x2

    #@5
    aput v2, v0, v1

    #@7
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 431
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->getService()Lcom/broadcom/bt/service/ftp/FTPService;

    #@3
    move-result-object v0

    #@4
    .line 432
    .local v0, service:Lcom/broadcom/bt/service/ftp/FTPService;
    if-nez v0, :cond_8

    #@6
    .line 433
    const/4 v1, 0x0

    #@7
    .line 435
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 5
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 445
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->getService()Lcom/broadcom/bt/service/ftp/FTPService;

    #@3
    move-result-object v0

    #@4
    .line 446
    .local v0, service:Lcom/broadcom/bt/service/ftp/FTPService;
    if-nez v0, :cond_d

    #@6
    .line 447
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 449
    :goto_c
    return-object v1

    #@d
    :cond_d
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method

.method public registerCallback(Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 470
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->getService()Lcom/broadcom/bt/service/ftp/FTPService;

    #@3
    move-result-object v0

    #@4
    .line 471
    .local v0, service:Lcom/broadcom/bt/service/ftp/FTPService;
    if-nez v0, :cond_7

    #@6
    .line 475
    :goto_6
    return-void

    #@7
    .line 474
    :cond_7
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->registerCallback(Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;)V

    #@a
    goto :goto_6
.end method

.method public unregisterCallback(Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 478
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;->getService()Lcom/broadcom/bt/service/ftp/FTPService;

    #@3
    move-result-object v0

    #@4
    .line 479
    .local v0, service:Lcom/broadcom/bt/service/ftp/FTPService;
    if-nez v0, :cond_7

    #@6
    .line 483
    :goto_6
    return-void

    #@7
    .line 482
    :cond_7
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->unregisterCallback(Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;)V

    #@a
    goto :goto_6
.end method
