.class public final Lcom/broadcom/bt/service/BrcmAbstractionLayer;
.super Ljava/lang/Object;
.source "BrcmAbstractionLayer.java"


# static fields
.field public static final BT_A2DP_SERVICE_ID:I = 0x12

.field public static final BT_A2DP_SRC_SERVICE_ID:I = 0x3

.field public static final BT_AVRCP_SERVICE_ID:I = 0x13

.field public static final BT_BIP_SERVICE_ID:I = 0xd

.field public static final BT_BPP_SERVICE_ID:I = 0xc

.field public static final BT_CTP_SERVICE_ID:I = 0x9

.field public static final BT_DUN_SERVICE_ID:I = 0x2

.field public static final BT_FTP_SERVICE_ID:I = 0x8

.field public static final BT_GN_SERVICE_ID:I = 0x10

.field public static final BT_HDP_SERVICE_ID:I = 0x1b

.field public static final BT_HFP_HS_SERVICE_ID:I = 0x18

.field public static final BT_HFP_SERVICE_ID:I = 0x6

.field public static final BT_HID_SERVICE_ID:I = 0x14

.field public static final BT_HSP_HS_SERVICE_ID:I = 0x17

.field public static final BT_HSP_SERVICE_ID:I = 0x5

.field public static final BT_ICP_SERVICE_ID:I = 0xa

.field public static final BT_LAP_SERVICE_ID:I = 0x4

.field public static final BT_MAP_SERVICE_ID:I = 0x19

.field public static final BT_MN_SERVICE_ID:I = 0x1a

.field public static final BT_NAP_SERVICE_ID:I = 0xf

.field public static final BT_OPP_SERVICE_ID:I = 0x7

.field public static final BT_PANU_SERVICE_ID:I = 0xe

.field public static final BT_PBAP_SERVICE_ID:I = 0x16

.field public static final BT_PCE_SERVICE_ID:I = 0x1c

.field public static final BT_PROPERTY_REMOTE_GLOBAL_TRUST:I = 0xc

.field public static final BT_PROPERTY_REMOTE_JUST_WORKS:I = 0xd

.field public static final BT_RADIO_OFF:I = 0x2

.field public static final BT_RADIO_ON:I = 0x3

.field public static final BT_RES_SERVICE_ID:I = 0x0

.field public static final BT_SAP_SERVICE_ID:I = 0x11

.field public static final BT_SPP_SERVICE_ID:I = 0x1

.field public static final BT_STATUS_AUTH_REJECTED:I = 0xb

.field public static final BT_STATUS_AUTH_TIMEOUT:I = 0xc

.field public static final BT_SYNC_SERVICE_ID:I = 0xb

.field public static final BT_VDP_SERVICE_ID:I = 0x15


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 24
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
