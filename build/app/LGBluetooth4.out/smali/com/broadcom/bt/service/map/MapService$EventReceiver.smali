.class Lcom/broadcom/bt/service/map/MapService$EventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MapService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/MapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/map/MapService;


# direct methods
.method private constructor <init>(Lcom/broadcom/bt/service/map/MapService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 915
    iput-object p1, p0, Lcom/broadcom/bt/service/map/MapService$EventReceiver;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/service/map/MapService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 915
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/MapService$EventReceiver;-><init>(Lcom/broadcom/bt/service/map/MapService;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/broadcom/bt/service/map/MapService$EventReceiver;Landroid/content/Context;Landroid/os/Handler;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 915
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/MapService$EventReceiver;->init(Landroid/content/Context;Landroid/os/Handler;)V

    #@3
    return-void
.end method

.method private init(Landroid/content/Context;Landroid/os/Handler;)V
    .registers 6
    .parameter "ctx"
    .parameter "handler"

    #@0
    .prologue
    .line 917
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 918
    .local v0, ifilter:Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 919
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService$EventReceiver;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@c
    const/4 v2, 0x0

    #@d
    invoke-virtual {v1, p0, v0, v2, p2}, Lcom/broadcom/bt/service/map/MapService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;)Landroid/content/Intent;

    #@10
    .line 920
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/16 v4, 0xc

    #@2
    .line 923
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 925
    .local v0, action:Ljava/lang/String;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@8
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_1f

    #@e
    .line 926
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    #@10
    const/high16 v3, -0x8000

    #@12
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@15
    move-result v1

    #@16
    .line 928
    .local v1, state:I
    if-ne v4, v1, :cond_20

    #@18
    .line 929
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$EventReceiver;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@1a
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@1c
    invoke-virtual {v2, v4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessageProfileStateChanged(I)V

    #@1f
    .line 934
    .end local v1           #state:I
    :cond_1f
    :goto_1f
    return-void

    #@20
    .line 930
    .restart local v1       #state:I
    :cond_20
    const/16 v2, 0xd

    #@22
    if-ne v2, v1, :cond_1f

    #@24
    .line 931
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$EventReceiver;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@26
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@28
    const/16 v3, 0xa

    #@2a
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessageProfileStateChanged(I)V

    #@2d
    goto :goto_1f
.end method
