.class public Lcom/broadcom/bt/service/ftp/FTPService;
.super Lcom/android/bluetooth/btservice/ProfileService;
.source "FTPService.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;,
        Lcom/broadcom/bt/service/ftp/FTPService$UnmountBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final CONN_STATE_CLOSE:I = 0x0

.field private static final CONN_STATE_OPEN:I = 0x1

.field private static final MESSAGE_ON_CONNECT_STATE_CHANGED:I = 0x3

.field private static final MESSAGE_ON_DEL_COMPLETED:I = 0x8

.field private static final MESSAGE_ON_FTPS_ACCESS_REQUESTED:I = 0x5

.field private static final MESSAGE_ON_FTPS_AUTH_REQUESTED:I = 0x4

.field private static final MESSAGE_ON_GET_COMPLETED:I = 0x6

.field private static final MESSAGE_ON_PUT_COMPLETED:I = 0x7

.field private static final MESSAGE_SERVER_ACCESS_RESP:I = 0x1

.field private static final MESSAGE_SERVER_AUTHEN_RSP:I = 0x2

.field private static final MESSAGE_TRANSFER_IN_PROGRESS:I = 0x9

.field private static final TAG:Ljava/lang/String; = "BluetoothFTPService"

.field private static final V:Z = true

.field private static ftpRootPath:Ljava/lang/String;

.field private static isStateRunning:Z

.field private static mFtpServerStopped:Z


# instance fields
.field private fileNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field private filePathForMediaScan:Ljava/lang/String;

.field private is_del_dir:Z

.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mFtpClientDevices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Landroid/os/Handler;

.field mMediaScanner:Landroid/media/MediaScannerConnection;

.field private mNativeAvailable:Z

.field private mUnmountReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 99
    sput-boolean v1, Lcom/broadcom/bt/service/ftp/FTPService;->isStateRunning:Z

    #@3
    .line 100
    const-string v0, "/storage/"

    #@5
    sput-object v0, Lcom/broadcom/bt/service/ftp/FTPService;->ftpRootPath:Ljava/lang/String;

    #@7
    .line 127
    sput-boolean v1, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpServerStopped:Z

    #@9
    .line 218
    invoke-static {}, Lcom/broadcom/bt/service/ftp/FTPService;->classInitNative()V

    #@c
    .line 219
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 95
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/ProfileService;-><init>()V

    #@3
    .line 101
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->fileNames:Ljava/util/ArrayList;

    #@a
    .line 102
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->is_del_dir:Z

    #@d
    .line 130
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->filePathForMediaScan:Ljava/lang/String;

    #@10
    .line 131
    new-instance v0, Lcom/broadcom/bt/service/ftp/FTPService$1;

    #@12
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/ftp/FTPService$1;-><init>(Lcom/broadcom/bt/service/ftp/FTPService;)V

    #@15
    iput-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@17
    .line 410
    return-void
.end method

.method private static SaveAllFiles(Ljava/util/ArrayList;Ljava/io/File;)V
    .registers 8
    .parameter "fileNames"
    .parameter "dir"

    #@0
    .prologue
    .line 1030
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    #@3
    move-result v5

    #@4
    if-eqz v5, :cond_19

    #@6
    .line 1031
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    #@9
    move-result-object v1

    #@a
    .line 1033
    .local v1, children:[Ljava/io/File;
    if-eqz v1, :cond_1c

    #@c
    .line 1034
    move-object v0, v1

    #@d
    .local v0, arr$:[Ljava/io/File;
    array-length v4, v0

    #@e
    .local v4, len$:I
    const/4 v3, 0x0

    #@f
    .local v3, i$:I
    :goto_f
    if-ge v3, v4, :cond_1c

    #@11
    aget-object v2, v0, v3

    #@13
    .line 1035
    .local v2, f:Ljava/io/File;
    invoke-static {p0, v2}, Lcom/broadcom/bt/service/ftp/FTPService;->SaveAllFiles(Ljava/util/ArrayList;Ljava/io/File;)V

    #@16
    .line 1034
    add-int/lit8 v3, v3, 0x1

    #@18
    goto :goto_f

    #@19
    .line 1045
    .end local v0           #arr$:[Ljava/io/File;
    .end local v1           #children:[Ljava/io/File;
    .end local v2           #f:Ljava/io/File;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_19
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@1c
    .line 1047
    :cond_1c
    return-void
.end method

.method static synthetic access$000(Lcom/broadcom/bt/service/ftp/FTPService;)Ljava/util/Map;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 95
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpClientDevices:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/broadcom/bt/service/ftp/FTPService;Landroid/bluetooth/BluetoothDevice;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/broadcom/bt/service/ftp/FTPService;->broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;II)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/broadcom/bt/service/ftp/FTPService;Ljava/lang/String;BZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/broadcom/bt/service/ftp/FTPService;->processFtpServerAuthen(Ljava/lang/String;BZ)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/broadcom/bt/service/ftp/FTPService;Ljava/lang/String;ILjava/lang/String;BLjava/lang/String;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 95
    invoke-direct/range {p0 .. p5}, Lcom/broadcom/bt/service/ftp/FTPService;->processFtpServerAccessRequested(Ljava/lang/String;ILjava/lang/String;BLjava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/broadcom/bt/service/ftp/FTPService;Ljava/lang/String;B)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/ftp/FTPService;->processFtpServerGetCompleted(Ljava/lang/String;B)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/broadcom/bt/service/ftp/FTPService;Ljava/lang/String;B)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/ftp/FTPService;->processFtpServerPutCompleted(Ljava/lang/String;B)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/broadcom/bt/service/ftp/FTPService;Ljava/lang/String;B)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/ftp/FTPService;->processFtpServerDelCompleted(Ljava/lang/String;B)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/broadcom/bt/service/ftp/FTPService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/ftp/FTPService;->processFtpServerFileTransferInProgress(II)V

    #@3
    return-void
.end method

.method static synthetic access$800()Z
    .registers 1

    #@0
    .prologue
    .line 95
    sget-boolean v0, Lcom/broadcom/bt/service/ftp/FTPService;->isStateRunning:Z

    #@2
    return v0
.end method

.method private broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;II)V
    .registers 9
    .parameter "device"
    .parameter "newState"
    .parameter "prevState"

    #@0
    .prologue
    .line 676
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpClientDevices:Ljava/util/Map;

    #@2
    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    #@5
    move-result v2

    #@6
    if-eqz v2, :cond_23

    #@8
    if-ne p3, p2, :cond_23

    #@a
    .line 677
    const-string v2, "BluetoothFTPService"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "no state change: "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 707
    :cond_22
    :goto_22
    return-void

    #@23
    .line 680
    :cond_23
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpClientDevices:Ljava/util/Map;

    #@25
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@28
    move-result-object v3

    #@29
    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    .line 682
    new-instance v1, Landroid/content/Intent;

    #@2e
    const-string v2, "android.broadcom.ftpserver.CONNECTION_STATE_CHANGED"

    #@30
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@33
    .line 683
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    #@35
    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@38
    .line 684
    const-string v2, "android.bluetooth.profile.extra.STATE"

    #@3a
    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@3d
    .line 685
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    #@3f
    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@42
    .line 686
    const/high16 v2, 0x800

    #@44
    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@47
    .line 687
    const-string v2, "android.permission.BLUETOOTH"

    #@49
    invoke-virtual {p0, v1, v2}, Lcom/broadcom/bt/service/ftp/FTPService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@4c
    .line 689
    const-string v2, "BluetoothFTPService"

    #@4e
    new-instance v3, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v4, "Connection state "

    #@55
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    const-string v4, ": "

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v3

    #@67
    const-string v4, "->"

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 692
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@7b
    move-result-object v0

    #@7c
    .line 693
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_82

    #@7e
    .line 694
    const/4 v2, 0x7

    #@7f
    invoke-virtual {v0, p1, v2, p2, p3}, Lcom/android/bluetooth/btservice/AdapterService;->onProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V

    #@82
    .line 698
    :cond_82
    const/4 v2, 0x2

    #@83
    if-ne v2, p2, :cond_22

    #@85
    .line 699
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@88
    move-result-object v2

    #@89
    if-eqz v2, :cond_22

    #@8b
    .line 700
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@8e
    move-result-object v2

    #@8f
    const/4 v3, 0x0

    #@90
    invoke-interface {v2, v3}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@93
    move-result v2

    #@94
    if-eqz v2, :cond_22

    #@96
    .line 701
    const-string v2, "BluetoothFTPService"

    #@98
    const-string v3, "broadcastConnectionState is blocked by LG MDM Server Policy"

    #@9a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    .line 702
    invoke-virtual {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->cleanup()Z

    #@a0
    goto :goto_22
.end method

.method private static native classInitNative()V
.end method

.method private native cleanupFtpNativeDataNative()V
.end method

.method private native closeFtpServerNative()V
.end method

.method private native closeFtpServerWithoutCleanupNative()V
.end method

.method private deleteMedia(Ljava/lang/String;)V
    .registers 10
    .parameter "filePath"

    #@0
    .prologue
    .line 644
    if-nez p1, :cond_a

    #@2
    .line 645
    const-string v5, "BluetoothFTPService"

    #@4
    const-string v6, "deleteMedia(): invalid media file path"

    #@6
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 671
    :goto_9
    return-void

    #@a
    .line 649
    :cond_a
    sget-object v5, Lcom/broadcom/bt/service/ftp/FTPService;->ftpRootPath:Ljava/lang/String;

    #@c
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@f
    move-result-object v6

    #@10
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@13
    move-result-object v6

    #@14
    invoke-virtual {p1, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    .line 651
    .local v4, tmpPath:Ljava/lang/String;
    const-string v5, "BluetoothFTPService"

    #@1a
    new-instance v6, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v7, "FTP "

    #@21
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    const-string v7, " --> "

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v6

    #@37
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 652
    invoke-virtual {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->getApplicationContext()Landroid/content/Context;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@41
    move-result-object v3

    #@42
    .line 653
    .local v3, r:Landroid/content/ContentResolver;
    const-string v5, "external"

    #@44
    invoke-static {v5}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    #@47
    move-result-object v2

    #@48
    .line 654
    .local v2, externalMediaUri:Landroid/net/Uri;
    if-eqz v3, :cond_4c

    #@4a
    if-nez v2, :cond_54

    #@4c
    .line 655
    :cond_4c
    const-string v5, "BluetoothFTPService"

    #@4e
    const-string v6, "deleteMedia(): invalid content resolver or media uri "

    #@50
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_9

    #@54
    .line 659
    :cond_54
    const/4 v0, 0x0

    #@55
    .line 661
    .local v0, deleteCount:I
    :try_start_55
    const-string v5, "_data =? OR _data =? "

    #@57
    const/4 v6, 0x2

    #@58
    new-array v6, v6, [Ljava/lang/String;

    #@5a
    const/4 v7, 0x0

    #@5b
    aput-object v4, v6, v7

    #@5d
    const/4 v7, 0x1

    #@5e
    aput-object p1, v6, v7

    #@60
    invoke-virtual {v3, v2, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_63
    .catch Ljava/lang/Exception; {:try_start_55 .. :try_end_63} :catch_8e

    #@63
    move-result v0

    #@64
    .line 668
    :goto_64
    const-string v5, "BluetoothFTPService"

    #@66
    new-instance v6, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v7, "Deleted "

    #@6d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v6

    #@71
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@74
    move-result-object v6

    #@75
    const-string v7, "entries for media "

    #@77
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v6

    #@7b
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v6

    #@7f
    const-string v7, " from media store"

    #@81
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v6

    #@85
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v6

    #@89
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    goto/16 :goto_9

    #@8e
    .line 664
    :catch_8e
    move-exception v1

    #@8f
    .line 665
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "BluetoothFTPService"

    #@91
    new-instance v6, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v7, "Error while deleting "

    #@98
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v6

    #@9c
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v6

    #@a0
    const-string v7, " from media store"

    #@a2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v6

    #@a6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a9
    move-result-object v6

    #@aa
    invoke-static {v5, v6, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ad
    goto :goto_64
.end method

.method private ftpMediaScanFilePath(Ljava/lang/String;)V
    .registers 5
    .parameter "filePath"

    #@0
    .prologue
    .line 1006
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@2
    if-eqz v1, :cond_12

    #@4
    .line 1007
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@6
    invoke-virtual {v1}, Landroid/media/MediaScannerConnection;->isConnected()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_1c

    #@c
    .line 1009
    :try_start_c
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@e
    const/4 v2, 0x0

    #@f
    invoke-virtual {v1, p1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_c .. :try_end_12} :catch_13

    #@12
    .line 1027
    :cond_12
    :goto_12
    return-void

    #@13
    .line 1010
    :catch_13
    move-exception v0

    #@14
    .line 1011
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "BluetoothFTPService"

    #@16
    const-string v2, "Unable to invoke MediaScanner.scanFile()"

    #@18
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1b
    goto :goto_12

    #@1c
    .line 1019
    .end local v0           #t:Ljava/lang/Throwable;
    :cond_1c
    :try_start_1c
    const-string v1, "BluetoothFTPService"

    #@1e
    const-string v2, "calling MediaScanner.connect()"

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 1020
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@25
    invoke-virtual {v1}, Landroid/media/MediaScannerConnection;->connect()V

    #@28
    .line 1021
    iput-object p1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->filePathForMediaScan:Ljava/lang/String;
    :try_end_2a
    .catch Ljava/lang/Throwable; {:try_start_1c .. :try_end_2a} :catch_2b

    #@2a
    goto :goto_12

    #@2b
    .line 1022
    :catch_2b
    move-exception v0

    #@2c
    .line 1023
    .restart local v0       #t:Ljava/lang/Throwable;
    const-string v1, "BluetoothFTPService"

    #@2e
    const-string v2, "Unable to connect to media scanner"

    #@30
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    goto :goto_12
.end method

.method private native ftpServerAccessRspNative(BZLjava/lang/String;)V
.end method

.method private native ftpServerAuthenRspNative(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private getDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;
    .registers 3
    .parameter "address"

    #@0
    .prologue
    .line 494
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method private native initFtpNativeDataNative()V
.end method

.method private isActionCommand(B)Z
    .registers 3
    .parameter "opCode"

    #@0
    .prologue
    .line 628
    const/4 v0, 0x7

    #@1
    if-lt p1, v0, :cond_9

    #@3
    const/16 v0, 0x9

    #@5
    if-gt p1, v0, :cond_9

    #@7
    const/4 v0, 0x1

    #@8
    :goto_8
    return v0

    #@9
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_8
.end method

.method private isActionCommandSupported(B)Z
    .registers 4
    .parameter "opCode"

    #@0
    .prologue
    .line 635
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    sget-object v1, Lcom/broadcom/bt/service/ftp/FTPServiceConfig;->SUPPORTED_ACTION_COMMANDS:[B

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_11

    #@6
    .line 636
    sget-object v1, Lcom/broadcom/bt/service/ftp/FTPServiceConfig;->SUPPORTED_ACTION_COMMANDS:[B

    #@8
    aget-byte v1, v1, v0

    #@a
    if-ne v1, p1, :cond_e

    #@c
    .line 637
    const/4 v1, 0x1

    #@d
    .line 640
    :goto_d
    return v1

    #@e
    .line 635
    :cond_e
    add-int/lit8 v0, v0, 0x1

    #@10
    goto :goto_1

    #@11
    .line 640
    :cond_11
    const/4 v1, 0x0

    #@12
    goto :goto_d
.end method

.method private static needAccessRequest()Z
    .registers 4

    #@0
    .prologue
    .line 249
    :try_start_0
    const-string v1, "true"

    #@2
    const-string v2, "service.brcm.bt.secure_mode"

    #@4
    const-string v3, ""

    #@6
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_d} :catch_f

    #@d
    move-result v1

    #@e
    .line 253
    .local v0, t:Ljava/lang/Throwable;
    :goto_e
    return v1

    #@f
    .line 251
    .end local v0           #t:Ljava/lang/Throwable;
    :catch_f
    move-exception v0

    #@10
    .line 252
    .restart local v0       #t:Ljava/lang/Throwable;
    const-string v1, "BluetoothFTPService"

    #@12
    const-string v2, "needAccessRequest()"

    #@14
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17
    .line 253
    const/4 v1, 0x0

    #@18
    goto :goto_e
.end method

.method private onConnectStateChanged([BI)V
    .registers 8
    .parameter "address"
    .parameter "state"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 757
    const-string v1, "BluetoothFTPService"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "onConnectStateChanged() address:"

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    const-string v3, ", state:"

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 758
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@25
    const/4 v2, 0x3

    #@26
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@29
    move-result-object v0

    #@2a
    .line 759
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@2d
    move-result-object v1

    #@2e
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@30
    .line 760
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@32
    .line 761
    if-ne p2, v4, :cond_36

    #@34
    .line 763
    sput-boolean v4, Lcom/broadcom/bt/service/ftp/FTPService;->isStateRunning:Z

    #@36
    .line 765
    :cond_36
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@38
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@3b
    .line 766
    return-void
.end method

.method private onFtpServerAccessRequested(Ljava/lang/String;ILjava/lang/String;BLjava/lang/String;)V
    .registers 11
    .parameter "fileName"
    .parameter "fileSize"
    .parameter "remoteDeviceName"
    .parameter "opCode"
    .parameter "remoteAddress"

    #@0
    .prologue
    .line 789
    const-string v2, "BluetoothFTPService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "onFtpServerAccessRequested() remoteAddress:"

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 791
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@1a
    const/4 v3, 0x5

    #@1b
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1e
    move-result-object v1

    #@1f
    .line 792
    .local v1, msg:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    #@21
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@24
    .line 793
    .local v0, data:Landroid/os/Bundle;
    const-string v2, "filename"

    #@26
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 794
    const-string v2, "size"

    #@2b
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2e
    .line 795
    const-string v2, "remotename"

    #@30
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@33
    .line 796
    const-string v2, "opcode"

    #@35
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@38
    .line 797
    const-string v2, "remoteaddr"

    #@3a
    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@3d
    .line 798
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@40
    .line 799
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@42
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@45
    .line 800
    return-void
.end method

.method private onFtpServerAuthen(Ljava/lang/String;BZ)V
    .registers 8
    .parameter "user_id"
    .parameter "userid_length"
    .parameter "userid_required"

    #@0
    .prologue
    .line 770
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v3, 0x4

    #@3
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v1

    #@7
    .line 771
    .local v1, msg:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    #@9
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@c
    .line 772
    .local v0, data:Landroid/os/Bundle;
    const-string v2, "userid_length"

    #@e
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@11
    .line 773
    const-string v2, "userid"

    #@13
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@16
    .line 774
    const-string v2, "required"

    #@18
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@1b
    .line 775
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@1e
    .line 776
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@20
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@23
    .line 777
    return-void
.end method

.method private onFtpServerDelCompleted(Ljava/lang/String;B)V
    .registers 8
    .parameter "filePath"
    .parameter "status"

    #@0
    .prologue
    .line 961
    const-string v2, "BluetoothFTPService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "onFtpServerDelCompleted() filePath:"

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 963
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@1a
    const/16 v3, 0x8

    #@1c
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1f
    move-result-object v1

    #@20
    .line 964
    .local v1, msg:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    #@22
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@25
    .line 965
    .local v0, data:Landroid/os/Bundle;
    const-string v2, "filepath"

    #@27
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@2a
    .line 966
    const-string v2, "status"

    #@2c
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@2f
    .line 967
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@32
    .line 968
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@34
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@37
    .line 969
    return-void
.end method

.method private onFtpServerDisabled()V
    .registers 6

    #@0
    .prologue
    .line 729
    const-string v3, "BluetoothFTPService"

    #@2
    const-string v4, "onFtpServerDisabled() called."

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 731
    const/4 v3, 0x1

    #@8
    sput-boolean v3, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpServerStopped:Z

    #@a
    .line 733
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@c
    if-eqz v3, :cond_1b

    #@e
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@10
    invoke-virtual {v3}, Landroid/media/MediaScannerConnection;->isConnected()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_1b

    #@16
    .line 735
    :try_start_16
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@18
    invoke-virtual {v3}, Landroid/media/MediaScannerConnection;->disconnect()V
    :try_end_1b
    .catch Ljava/lang/Throwable; {:try_start_16 .. :try_end_1b} :catch_36

    #@1b
    .line 742
    :cond_1b
    :goto_1b
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@1d
    if-eqz v3, :cond_4d

    #@1f
    .line 743
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@21
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@24
    move-result v0

    #@25
    .line 744
    .local v0, N:I
    const/4 v1, 0x0

    #@26
    .local v1, i:I
    :goto_26
    if-ge v1, v0, :cond_48

    #@28
    .line 746
    :try_start_28
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@2a
    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@2d
    move-result-object v3

    #@2e
    check-cast v3, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

    #@30
    invoke-interface {v3}, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;->onFtpServerClosed()V
    :try_end_33
    .catch Ljava/lang/Throwable; {:try_start_28 .. :try_end_33} :catch_3f

    #@33
    .line 744
    :goto_33
    add-int/lit8 v1, v1, 0x1

    #@35
    goto :goto_26

    #@36
    .line 736
    .end local v0           #N:I
    .end local v1           #i:I
    :catch_36
    move-exception v2

    #@37
    .line 737
    .local v2, t:Ljava/lang/Throwable;
    const-string v3, "BluetoothFTPService"

    #@39
    const-string v4, "Unable to disconnect from media scanner"

    #@3b
    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3e
    goto :goto_1b

    #@3f
    .line 747
    .end local v2           #t:Ljava/lang/Throwable;
    .restart local v0       #N:I
    .restart local v1       #i:I
    :catch_3f
    move-exception v2

    #@40
    .line 748
    .restart local v2       #t:Ljava/lang/Throwable;
    const-string v3, "BluetoothFTPService"

    #@42
    const-string v4, "Error: onFtpServerClosed()"

    #@44
    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@47
    goto :goto_33

    #@48
    .line 751
    .end local v2           #t:Ljava/lang/Throwable;
    :cond_48
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@4a
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@4d
    .line 754
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_4d
    return-void
.end method

.method private onFtpServerEnabled(Ljava/lang/String;)V
    .registers 8
    .parameter "rootPath"

    #@0
    .prologue
    .line 710
    const-string v3, "BluetoothFTPService"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "onFtpServerEnabled: "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 711
    const/4 v3, 0x1

    #@19
    sput-boolean v3, Lcom/broadcom/bt/service/ftp/FTPService;->isStateRunning:Z

    #@1b
    .line 712
    sput-object p1, Lcom/broadcom/bt/service/ftp/FTPService;->ftpRootPath:Ljava/lang/String;

    #@1d
    .line 715
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@1f
    if-eqz v3, :cond_46

    #@21
    .line 716
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@23
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@26
    move-result v0

    #@27
    .line 717
    .local v0, N:I
    const/4 v1, 0x0

    #@28
    .local v1, i:I
    :goto_28
    if-ge v1, v0, :cond_41

    #@2a
    .line 719
    :try_start_2a
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@2c
    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@2f
    move-result-object v3

    #@30
    check-cast v3, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

    #@32
    invoke-interface {v3}, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;->onFtpServerEnabled()V
    :try_end_35
    .catch Ljava/lang/Throwable; {:try_start_2a .. :try_end_35} :catch_38

    #@35
    .line 717
    :goto_35
    add-int/lit8 v1, v1, 0x1

    #@37
    goto :goto_28

    #@38
    .line 720
    :catch_38
    move-exception v2

    #@39
    .line 721
    .local v2, t:Ljava/lang/Throwable;
    const-string v3, "BluetoothFTPService"

    #@3b
    const-string v4, "Error: onFtpServerOpened()"

    #@3d
    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@40
    goto :goto_35

    #@41
    .line 724
    .end local v2           #t:Ljava/lang/Throwable;
    :cond_41
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@43
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@46
    .line 726
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_46
    return-void
.end method

.method private onFtpServerFileTransferInProgress(II)V
    .registers 6
    .parameter "fileSize"
    .parameter "bytesTransferred"

    #@0
    .prologue
    .line 868
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0x9

    #@4
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 869
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@a
    .line 870
    iput p2, v0, Landroid/os/Message;->arg2:I

    #@c
    .line 871
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 872
    return-void
.end method

.method private onFtpServerGetCompleted(Ljava/lang/String;B)V
    .registers 8
    .parameter "filePath"
    .parameter "status"

    #@0
    .prologue
    .line 930
    const-string v2, "BluetoothFTPService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "onFtpServerGetCompleted() filePath:"

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 932
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@1a
    const/4 v3, 0x6

    #@1b
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1e
    move-result-object v1

    #@1f
    .line 933
    .local v1, msg:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    #@21
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@24
    .line 934
    .local v0, data:Landroid/os/Bundle;
    const-string v2, "filepath"

    #@26
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 935
    const-string v2, "status"

    #@2b
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@2e
    .line 936
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@31
    .line 937
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@33
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@36
    .line 938
    return-void
.end method

.method private onFtpServerPutCompleted(Ljava/lang/String;B)V
    .registers 8
    .parameter "filePath"
    .parameter "status"

    #@0
    .prologue
    .line 895
    const-string v2, "BluetoothFTPService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "onFtpServerPutCompleted() filePath:"

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 897
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@1a
    const/4 v3, 0x7

    #@1b
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1e
    move-result-object v1

    #@1f
    .line 898
    .local v1, msg:Landroid/os/Message;
    new-instance v0, Landroid/os/Bundle;

    #@21
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@24
    .line 899
    .local v0, data:Landroid/os/Bundle;
    const-string v2, "filepath"

    #@26
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 900
    const-string v2, "status"

    #@2b
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@2e
    .line 901
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@31
    .line 902
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@33
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@36
    .line 903
    return-void
.end method

.method private processFtpServerAccessRequested(Ljava/lang/String;ILjava/lang/String;BLjava/lang/String;)V
    .registers 16
    .parameter "fileName"
    .parameter "fileSize"
    .parameter "remoteDeviceName"
    .parameter "opCode"
    .parameter "remoteAddress"

    #@0
    .prologue
    .line 805
    const-string v6, "BluetoothFTPService"

    #@2
    const-string v7, "processFtpServerAccessRequested()"

    #@4
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 807
    const/4 v6, 0x1

    #@8
    if-ne p4, v6, :cond_5f

    #@a
    .line 809
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@d
    move-result-object v6

    #@e
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@11
    move-result-object v4

    #@12
    .line 812
    .local v4, root:Ljava/lang/String;
    :try_start_12
    new-instance v5, Landroid/os/StatFs;

    #@14
    new-instance v6, Ljava/io/File;

    #@16
    invoke-direct {v6, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@19
    invoke-virtual {v6}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@1c
    move-result-object v6

    #@1d
    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    #@20
    .line 813
    .local v5, stat:Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    #@23
    move-result v6

    #@24
    int-to-long v6, v6

    #@25
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    #@28
    move-result v8

    #@29
    int-to-long v8, v8

    #@2a
    mul-long v2, v6, v8

    #@2c
    .line 815
    .local v2, freeSpace:J
    int-to-long v6, p2

    #@2d
    cmp-long v6, v6, v2

    #@2f
    if-lez v6, :cond_76

    #@31
    .line 816
    const-string v6, "BluetoothFTPService"

    #@33
    const-string v7, "onFtpServerAccessRequested - Not enough free space"

    #@35
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 818
    const/4 v6, 0x0

    #@39
    invoke-virtual {p0, p4, v6, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->ftpServerAccessRsp(BZLjava/lang/String;)V
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_3c} :catch_3d

    #@3c
    .line 865
    .end local v2           #freeSpace:J
    .end local v4           #root:Ljava/lang/String;
    .end local v5           #stat:Landroid/os/StatFs;
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 821
    .restart local v4       #root:Ljava/lang/String;
    :catch_3d
    move-exception v1

    #@3e
    .line 822
    .local v1, eee:Ljava/lang/Exception;
    const-string v6, "BluetoothFTPService"

    #@40
    new-instance v7, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v8, "Error calling statfs() : "

    #@47
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@4e
    move-result-object v8

    #@4f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 823
    const/4 v6, 0x0

    #@5b
    invoke-virtual {p0, p4, v6, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->ftpServerAccessRsp(BZLjava/lang/String;)V

    #@5e
    goto :goto_3c

    #@5f
    .line 826
    .end local v1           #eee:Ljava/lang/Exception;
    .end local v4           #root:Ljava/lang/String;
    :cond_5f
    const/4 v6, 0x4

    #@60
    if-ne p4, v6, :cond_a7

    #@62
    .line 829
    :try_start_62
    new-instance v0, Ljava/io/File;

    #@64
    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@67
    .line 830
    .local v0, dir:Ljava/io/File;
    if-eqz v0, :cond_81

    #@69
    .line 831
    iget-object v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->fileNames:Ljava/util/ArrayList;

    #@6b
    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    #@6e
    .line 832
    iget-object v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->fileNames:Ljava/util/ArrayList;

    #@70
    invoke-static {v6, v0}, Lcom/broadcom/bt/service/ftp/FTPService;->SaveAllFiles(Ljava/util/ArrayList;Ljava/io/File;)V
    :try_end_73
    .catch Ljava/lang/Exception; {:try_start_62 .. :try_end_73} :catch_89

    #@73
    .line 839
    .end local v0           #dir:Ljava/io/File;
    :goto_73
    const/4 v6, 0x1

    #@74
    iput-boolean v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->is_del_dir:Z

    #@76
    .line 856
    :cond_76
    invoke-static {}, Lcom/broadcom/bt/service/ftp/FTPService;->needAccessRequest()Z

    #@79
    move-result v6

    #@7a
    if-nez v6, :cond_3c

    #@7c
    .line 863
    const/4 v6, 0x1

    #@7d
    invoke-virtual {p0, p4, v6, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->ftpServerAccessRsp(BZLjava/lang/String;)V

    #@80
    goto :goto_3c

    #@81
    .line 834
    .restart local v0       #dir:Ljava/io/File;
    :cond_81
    :try_start_81
    const-string v6, "BluetoothFTPService"

    #@83
    const-string v7, "Error () : dir is null"

    #@85
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_88
    .catch Ljava/lang/Exception; {:try_start_81 .. :try_end_88} :catch_89

    #@88
    goto :goto_73

    #@89
    .line 836
    .end local v0           #dir:Ljava/io/File;
    :catch_89
    move-exception v1

    #@8a
    .line 837
    .restart local v1       #eee:Ljava/lang/Exception;
    const-string v6, "BluetoothFTPService"

    #@8c
    new-instance v7, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v8, "Error () : "

    #@93
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v7

    #@97
    invoke-virtual {v1}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@9a
    move-result-object v8

    #@9b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v7

    #@9f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v7

    #@a3
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    goto :goto_73

    #@a7
    .line 847
    .end local v1           #eee:Ljava/lang/Exception;
    :cond_a7
    invoke-direct {p0, p4}, Lcom/broadcom/bt/service/ftp/FTPService;->isActionCommand(B)Z

    #@aa
    move-result v6

    #@ab
    if-eqz v6, :cond_76

    #@ad
    .line 848
    const-string v6, "BluetoothFTPService"

    #@af
    new-instance v7, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v8, "Checking if action command "

    #@b6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v7

    #@ba
    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v7

    #@be
    const-string v8, " is supported."

    #@c0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v7

    #@c4
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v7

    #@c8
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cb
    .line 849
    invoke-direct {p0, p4}, Lcom/broadcom/bt/service/ftp/FTPService;->isActionCommandSupported(B)Z

    #@ce
    move-result v6

    #@cf
    if-nez v6, :cond_76

    #@d1
    .line 850
    const-string v6, "BluetoothFTPService"

    #@d3
    const-string v7, "Action command not supported. Denying request..."

    #@d5
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    .line 851
    const/4 v6, 0x0

    #@d9
    invoke-virtual {p0, p4, v6, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->ftpServerAccessRsp(BZLjava/lang/String;)V

    #@dc
    goto/16 :goto_3c
.end method

.method private processFtpServerAuthen(Ljava/lang/String;BZ)V
    .registers 4
    .parameter "user_id"
    .parameter "userid_length"
    .parameter "userid_required"

    #@0
    .prologue
    .line 784
    return-void
.end method

.method private processFtpServerDelCompleted(Ljava/lang/String;B)V
    .registers 11
    .parameter "filePath"
    .parameter "status"

    #@0
    .prologue
    .line 973
    const-string v5, "BluetoothFTPService"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "onFtpServerDelCompleted(): "

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    const-string v7, " : state = "

    #@13
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v6

    #@1b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v6

    #@1f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 975
    const-string v5, "BluetoothFTPService"

    #@24
    new-instance v6, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v7, "ftpRootPath:"

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    sget-object v7, Lcom/broadcom/bt/service/ftp/FTPService;->ftpRootPath:Ljava/lang/String;

    #@31
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    const-string v7, ", SystemPath:"

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@3e
    move-result-object v7

    #@3f
    invoke-virtual {v7}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@42
    move-result-object v7

    #@43
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v6

    #@47
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 978
    if-nez p2, :cond_79

    #@50
    .line 979
    iget-boolean v5, p0, Lcom/broadcom/bt/service/ftp/FTPService;->is_del_dir:Z

    #@52
    if-eqz v5, :cond_76

    #@54
    .line 981
    iget-object v5, p0, Lcom/broadcom/bt/service/ftp/FTPService;->fileNames:Ljava/util/ArrayList;

    #@56
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@59
    move-result-object v3

    #@5a
    .local v3, i$:Ljava/util/Iterator;
    :goto_5a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@5d
    move-result v5

    #@5e
    if-eqz v5, :cond_6e

    #@60
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@63
    move-result-object v1

    #@64
    check-cast v1, Ljava/io/File;

    #@66
    .line 982
    .local v1, f:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    invoke-direct {p0, v5}, Lcom/broadcom/bt/service/ftp/FTPService;->deleteMedia(Ljava/lang/String;)V

    #@6d
    goto :goto_5a

    #@6e
    .line 984
    .end local v1           #f:Ljava/io/File;
    :cond_6e
    iget-object v5, p0, Lcom/broadcom/bt/service/ftp/FTPService;->fileNames:Ljava/util/ArrayList;

    #@70
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    #@73
    .line 985
    const/4 v5, 0x0

    #@74
    iput-boolean v5, p0, Lcom/broadcom/bt/service/ftp/FTPService;->is_del_dir:Z

    #@76
    .line 987
    .end local v3           #i$:Ljava/util/Iterator;
    :cond_76
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->deleteMedia(Ljava/lang/String;)V

    #@79
    .line 991
    :cond_79
    iget-object v5, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@7b
    if-eqz v5, :cond_a2

    #@7d
    .line 992
    iget-object v5, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@7f
    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@82
    move-result v0

    #@83
    .line 993
    .local v0, N:I
    const/4 v2, 0x0

    #@84
    .local v2, i:I
    :goto_84
    if-ge v2, v0, :cond_9d

    #@86
    .line 995
    :try_start_86
    iget-object v5, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@88
    invoke-virtual {v5, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@8b
    move-result-object v5

    #@8c
    check-cast v5, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

    #@8e
    invoke-interface {v5, p1, p2}, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;->onFtpServerDelCompleted(Ljava/lang/String;B)V
    :try_end_91
    .catch Ljava/lang/Throwable; {:try_start_86 .. :try_end_91} :catch_94

    #@91
    .line 993
    :goto_91
    add-int/lit8 v2, v2, 0x1

    #@93
    goto :goto_84

    #@94
    .line 996
    :catch_94
    move-exception v4

    #@95
    .line 997
    .local v4, t:Ljava/lang/Throwable;
    const-string v5, "BluetoothFTPService"

    #@97
    const-string v6, "Error: onFtpServerDelCompleted()"

    #@99
    invoke-static {v5, v6, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9c
    goto :goto_91

    #@9d
    .line 1000
    .end local v4           #t:Ljava/lang/Throwable;
    :cond_9d
    iget-object v5, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@9f
    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@a2
    .line 1002
    .end local v0           #N:I
    .end local v2           #i:I
    :cond_a2
    return-void
.end method

.method private processFtpServerFileTransferInProgress(II)V
    .registers 9
    .parameter "fileSize"
    .parameter "bytesTransferred"

    #@0
    .prologue
    .line 876
    const-string v3, "BluetoothFTPService"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "Transferring file via FTP "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, " bytes of "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 879
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@24
    if-eqz v3, :cond_4b

    #@26
    .line 880
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@28
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@2b
    move-result v0

    #@2c
    .line 881
    .local v0, N:I
    const/4 v1, 0x0

    #@2d
    .local v1, i:I
    :goto_2d
    if-ge v1, v0, :cond_46

    #@2f
    .line 883
    :try_start_2f
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@31
    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@34
    move-result-object v3

    #@35
    check-cast v3, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

    #@37
    invoke-interface {v3, p1, p2}, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;->onFtpServerFileTransferInProgress(II)V
    :try_end_3a
    .catch Ljava/lang/Throwable; {:try_start_2f .. :try_end_3a} :catch_3d

    #@3a
    .line 881
    :goto_3a
    add-int/lit8 v1, v1, 0x1

    #@3c
    goto :goto_2d

    #@3d
    .line 885
    :catch_3d
    move-exception v2

    #@3e
    .line 886
    .local v2, t:Ljava/lang/Throwable;
    const-string v3, "BluetoothFTPService"

    #@40
    const-string v4, "Error: onFtpServerFileTransferInProgress()"

    #@42
    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@45
    goto :goto_3a

    #@46
    .line 889
    .end local v2           #t:Ljava/lang/Throwable;
    :cond_46
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@48
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@4b
    .line 891
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_4b
    return-void
.end method

.method private processFtpServerGetCompleted(Ljava/lang/String;B)V
    .registers 9
    .parameter "filePath"
    .parameter "status"

    #@0
    .prologue
    .line 942
    const-string v3, "BluetoothFTPService"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "onFtpServerGetCompleted(): "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, " : state = "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 946
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@24
    if-eqz v3, :cond_4b

    #@26
    .line 947
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@28
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@2b
    move-result v0

    #@2c
    .line 948
    .local v0, N:I
    const/4 v1, 0x0

    #@2d
    .local v1, i:I
    :goto_2d
    if-ge v1, v0, :cond_46

    #@2f
    .line 950
    :try_start_2f
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@31
    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@34
    move-result-object v3

    #@35
    check-cast v3, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

    #@37
    invoke-interface {v3, p1, p2}, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;->onFtpServerGetCompleted(Ljava/lang/String;B)V
    :try_end_3a
    .catch Ljava/lang/Throwable; {:try_start_2f .. :try_end_3a} :catch_3d

    #@3a
    .line 948
    :goto_3a
    add-int/lit8 v1, v1, 0x1

    #@3c
    goto :goto_2d

    #@3d
    .line 951
    :catch_3d
    move-exception v2

    #@3e
    .line 952
    .local v2, t:Ljava/lang/Throwable;
    const-string v3, "BluetoothFTPService"

    #@40
    const-string v4, "Error: onFtpServerGetCompleted()"

    #@42
    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@45
    goto :goto_3a

    #@46
    .line 955
    .end local v2           #t:Ljava/lang/Throwable;
    :cond_46
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@48
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@4b
    .line 957
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_4b
    return-void
.end method

.method private processFtpServerPutCompleted(Ljava/lang/String;B)V
    .registers 9
    .parameter "filePath"
    .parameter "status"

    #@0
    .prologue
    .line 907
    const-string v3, "BluetoothFTPService"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "onFtpServerPutCompleted(): "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, " : status = "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 910
    if-nez p2, :cond_27

    #@24
    .line 911
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/ftp/FTPService;->ftpMediaScanFilePath(Ljava/lang/String;)V

    #@27
    .line 915
    :cond_27
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@29
    if-eqz v3, :cond_50

    #@2b
    .line 916
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@2d
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@30
    move-result v0

    #@31
    .line 917
    .local v0, N:I
    const/4 v1, 0x0

    #@32
    .local v1, i:I
    :goto_32
    if-ge v1, v0, :cond_4b

    #@34
    .line 919
    :try_start_34
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@36
    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@39
    move-result-object v3

    #@3a
    check-cast v3, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;

    #@3c
    invoke-interface {v3, p1, p2}, Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;->onFtpServerPutCompleted(Ljava/lang/String;B)V
    :try_end_3f
    .catch Ljava/lang/Throwable; {:try_start_34 .. :try_end_3f} :catch_42

    #@3f
    .line 917
    :goto_3f
    add-int/lit8 v1, v1, 0x1

    #@41
    goto :goto_32

    #@42
    .line 920
    :catch_42
    move-exception v2

    #@43
    .line 921
    .local v2, t:Ljava/lang/Throwable;
    const-string v3, "BluetoothFTPService"

    #@45
    const-string v4, "Error: onFtpServerPutCompleted()"

    #@47
    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4a
    goto :goto_3f

    #@4b
    .line 924
    .end local v2           #t:Ljava/lang/Throwable;
    :cond_4b
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@4d
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@50
    .line 926
    .end local v0           #N:I
    .end local v1           #i:I
    :cond_50
    return-void
.end method


# virtual methods
.method protected cleanup()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 345
    const-string v1, "BluetoothFTPService"

    #@3
    const-string v2, "cleanup FTP Service"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 347
    sget-boolean v1, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpServerStopped:Z

    #@a
    if-nez v1, :cond_f

    #@c
    .line 348
    invoke-virtual {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->closeFtpServer()V

    #@f
    .line 350
    :cond_f
    iget-boolean v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mNativeAvailable:Z

    #@11
    if-eqz v1, :cond_19

    #@13
    .line 352
    :try_start_13
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->cleanupFtpNativeDataNative()V
    :try_end_16
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_16} :catch_3c

    #@16
    .line 356
    :goto_16
    const/4 v1, 0x0

    #@17
    iput-boolean v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mNativeAvailable:Z

    #@19
    .line 358
    :cond_19
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mBinder:Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

    #@1b
    if-eqz v1, :cond_22

    #@1d
    .line 359
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mBinder:Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

    #@1f
    invoke-interface {v1}, Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;->cleanup()Z

    #@22
    .line 361
    :cond_22
    iput-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mBinder:Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

    #@24
    .line 363
    iget-object v1, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    #@26
    if-eqz v1, :cond_33

    #@28
    .line 365
    :try_start_28
    invoke-virtual {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->getApplicationContext()Landroid/content/Context;

    #@2b
    move-result-object v1

    #@2c
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    #@2e
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_31
    .catchall {:try_start_28 .. :try_end_31} :catchall_4e
    .catch Ljava/lang/Throwable; {:try_start_28 .. :try_end_31} :catch_45

    #@31
    .line 369
    :goto_31
    iput-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    #@33
    .line 373
    :cond_33
    const-string v1, "BluetoothFTPService"

    #@35
    const-string v2, "cleanup done"

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 374
    const/4 v1, 0x1

    #@3b
    return v1

    #@3c
    .line 353
    :catch_3c
    move-exception v0

    #@3d
    .line 354
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "BluetoothFTPService"

    #@3f
    const-string v2, "Unable to cleanup ftp service"

    #@41
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@44
    goto :goto_16

    #@45
    .line 366
    .end local v0           #t:Ljava/lang/Throwable;
    :catch_45
    move-exception v0

    #@46
    .line 367
    .restart local v0       #t:Ljava/lang/Throwable;
    :try_start_46
    const-string v1, "BluetoothFTPService"

    #@48
    const-string v2, "Unable to unregister unmount broadcast receiver"

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4d
    .catchall {:try_start_46 .. :try_end_4d} :catchall_4e

    #@4d
    goto :goto_31

    #@4e
    .line 369
    .end local v0           #t:Ljava/lang/Throwable;
    :catchall_4e
    move-exception v1

    #@4f
    iput-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    #@51
    throw v1
.end method

.method public declared-synchronized closeFtpServer()V
    .registers 5

    #@0
    .prologue
    .line 518
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->closeFtpServerNative()V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_3d
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_4} :catch_34

    #@4
    .line 526
    :goto_4
    :try_start_4
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@6
    if-eqz v2, :cond_15

    #@8
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@a
    invoke-virtual {v2}, Landroid/media/MediaScannerConnection;->isConnected()Z
    :try_end_d
    .catchall {:try_start_4 .. :try_end_d} :catchall_3d

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_15

    #@10
    .line 529
    :try_start_10
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@12
    invoke-virtual {v2}, Landroid/media/MediaScannerConnection;->disconnect()V
    :try_end_15
    .catchall {:try_start_10 .. :try_end_15} :catchall_3d
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_15} :catch_40

    #@15
    .line 534
    :cond_15
    :goto_15
    :try_start_15
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;
    :try_end_17
    .catchall {:try_start_15 .. :try_end_17} :catchall_3d

    #@17
    if-eqz v2, :cond_25

    #@19
    .line 536
    :try_start_19
    invoke-virtual {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->getApplicationContext()Landroid/content/Context;

    #@1c
    move-result-object v2

    #@1d
    iget-object v3, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    #@1f
    invoke-virtual {v2, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@22
    .line 537
    const/4 v2, 0x0

    #@23
    iput-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;
    :try_end_25
    .catchall {:try_start_19 .. :try_end_25} :catchall_3d
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_25} :catch_49

    #@25
    .line 542
    :cond_25
    :goto_25
    :try_start_25
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@27
    if-eqz v2, :cond_2f

    #@29
    .line 543
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mHandler:Landroid/os/Handler;

    #@2b
    const/4 v3, 0x0

    #@2c
    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@2f
    .line 545
    :cond_2f
    const/4 v2, 0x0

    #@30
    sput-boolean v2, Lcom/broadcom/bt/service/ftp/FTPService;->isStateRunning:Z
    :try_end_32
    .catchall {:try_start_25 .. :try_end_32} :catchall_3d

    #@32
    .line 546
    monitor-exit p0

    #@33
    return-void

    #@34
    .line 519
    :catch_34
    move-exception v0

    #@35
    .line 520
    .local v0, e:Ljava/lang/Exception;
    :try_start_35
    const-string v2, "BluetoothFTPService"

    #@37
    const-string v3, "closeFtpServerNative failed"

    #@39
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3c
    .catchall {:try_start_35 .. :try_end_3c} :catchall_3d

    #@3c
    goto :goto_4

    #@3d
    .line 518
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_3d
    move-exception v2

    #@3e
    monitor-exit p0

    #@3f
    throw v2

    #@40
    .line 530
    :catch_40
    move-exception v1

    #@41
    .line 531
    .local v1, t:Ljava/lang/Throwable;
    :try_start_41
    const-string v2, "BluetoothFTPService"

    #@43
    const-string v3, "Unable to disconnect from media scanner"

    #@45
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    goto :goto_15

    #@49
    .line 538
    .end local v1           #t:Ljava/lang/Throwable;
    :catch_49
    move-exception v1

    #@4a
    .line 539
    .restart local v1       #t:Ljava/lang/Throwable;
    const-string v2, "BluetoothFTPService"

    #@4c
    const-string v3, "Unable to unregister unmount broadcast receiver"

    #@4e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_51
    .catchall {:try_start_41 .. :try_end_51} :catchall_3d

    #@51
    goto :goto_25
.end method

.method public declared-synchronized closeFtpServerWithoutCleanup()V
    .registers 5

    #@0
    .prologue
    .line 553
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->closeFtpServerWithoutCleanupNative()V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_23
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_4} :catch_1a

    #@4
    .line 559
    :goto_4
    :try_start_4
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@6
    if-eqz v2, :cond_15

    #@8
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@a
    invoke-virtual {v2}, Landroid/media/MediaScannerConnection;->isConnected()Z
    :try_end_d
    .catchall {:try_start_4 .. :try_end_d} :catchall_23

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_15

    #@10
    .line 561
    :try_start_10
    iget-object v2, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@12
    invoke-virtual {v2}, Landroid/media/MediaScannerConnection;->disconnect()V
    :try_end_15
    .catchall {:try_start_10 .. :try_end_15} :catchall_23
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_15} :catch_26

    #@15
    .line 566
    :cond_15
    :goto_15
    const/4 v2, 0x0

    #@16
    :try_start_16
    sput-boolean v2, Lcom/broadcom/bt/service/ftp/FTPService;->isStateRunning:Z
    :try_end_18
    .catchall {:try_start_16 .. :try_end_18} :catchall_23

    #@18
    .line 567
    monitor-exit p0

    #@19
    return-void

    #@1a
    .line 554
    :catch_1a
    move-exception v0

    #@1b
    .line 555
    .local v0, e:Ljava/lang/Exception;
    :try_start_1b
    const-string v2, "BluetoothFTPService"

    #@1d
    const-string v3, "closeFtpServerNative failed"

    #@1f
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_22
    .catchall {:try_start_1b .. :try_end_22} :catchall_23

    #@22
    goto :goto_4

    #@23
    .line 553
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_23
    move-exception v2

    #@24
    monitor-exit p0

    #@25
    throw v2

    #@26
    .line 562
    :catch_26
    move-exception v1

    #@27
    .line 563
    .local v1, t:Ljava/lang/Throwable;
    :try_start_27
    const-string v2, "BluetoothFTPService"

    #@29
    const-string v3, "Unable to disconnect from media scanner"

    #@2b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2e
    .catchall {:try_start_27 .. :try_end_2e} :catchall_23

    #@2e
    goto :goto_15
.end method

.method protected finalize()V
    .registers 5

    #@0
    .prologue
    .line 384
    const-class v1, Lcom/broadcom/bt/service/ftp/FTPService;

    #@2
    monitor-enter v1

    #@3
    .line 385
    :try_start_3
    const-string v0, "BluetoothFTPService"

    #@5
    new-instance v2, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v3, "FINALIZED. Class= "

    #@c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v2

    #@10
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 386
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_20

    #@1c
    .line 388
    invoke-super {p0}, Lcom/android/bluetooth/btservice/ProfileService;->finalize()V

    #@1f
    .line 389
    return-void

    #@20
    .line 386
    :catchall_20
    move-exception v0

    #@21
    :try_start_21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_21 .. :try_end_22} :catchall_20

    #@22
    throw v0
.end method

.method public declared-synchronized ftpServerAccessRsp(BZLjava/lang/String;)V
    .registers 7
    .parameter "op_code"
    .parameter "access"
    .parameter "filename"

    #@0
    .prologue
    .line 600
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1, p2, p3}, Lcom/broadcom/bt/service/ftp/FTPService;->ftpServerAccessRspNative(BZLjava/lang/String;)V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_f
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_4} :catch_6

    #@4
    .line 604
    :goto_4
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 601
    :catch_6
    move-exception v0

    #@7
    .line 602
    .local v0, e:Ljava/lang/Exception;
    :try_start_7
    const-string v1, "BluetoothFTPService"

    #@9
    const-string v2, "ftpServerAccessRspNative failed"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_f

    #@e
    goto :goto_4

    #@f
    .line 600
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_f
    move-exception v1

    #@10
    monitor-exit p0

    #@11
    throw v1
.end method

.method public declared-synchronized ftpServerAuthenRsp(Ljava/lang/String;Ljava/lang/String;)V
    .registers 6
    .parameter "password"
    .parameter "userId"

    #@0
    .prologue
    .line 578
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/ftp/FTPService;->ftpServerAuthenRspNative(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_1 .. :try_end_4} :catchall_f
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_4} :catch_6

    #@4
    .line 582
    :goto_4
    monitor-exit p0

    #@5
    return-void

    #@6
    .line 579
    :catch_6
    move-exception v0

    #@7
    .line 580
    .local v0, e:Ljava/lang/Exception;
    :try_start_7
    const-string v1, "BluetoothFTPService"

    #@9
    const-string v2, "ftpServerAuthRspNative failed"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catchall {:try_start_7 .. :try_end_e} :catchall_f

    #@e
    goto :goto_4

    #@f
    .line 578
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_f
    move-exception v1

    #@10
    monitor-exit p0

    #@11
    throw v1
.end method

.method getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 487
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpClientDevices:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    if-nez v0, :cond_a

    #@8
    .line 488
    const/4 v0, 0x0

    #@9
    .line 490
    :goto_9
    return v0

    #@a
    :cond_a
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpClientDevices:Ljava/util/Map;

    #@c
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Ljava/lang/Integer;

    #@12
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@15
    move-result v0

    #@16
    goto :goto_9
.end method

.method getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 12
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 498
    const-string v8, "android.permission.BLUETOOTH"

    #@2
    const-string v9, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v8, v9}, Lcom/broadcom/bt/service/ftp/FTPService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 499
    new-instance v2, Ljava/util/ArrayList;

    #@9
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@c
    .line 501
    .local v2, ftpClientDevices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    iget-object v8, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpClientDevices:Ljava/util/Map;

    #@e
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@11
    move-result-object v8

    #@12
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v3

    #@16
    :cond_16
    :goto_16
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v8

    #@1a
    if-eqz v8, :cond_36

    #@1c
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@22
    .line 502
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p0, v1}, Lcom/broadcom/bt/service/ftp/FTPService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@25
    move-result v5

    #@26
    .line 503
    .local v5, inputDeviceState:I
    move-object v0, p1

    #@27
    .local v0, arr$:[I
    array-length v6, v0

    #@28
    .local v6, len$:I
    const/4 v4, 0x0

    #@29
    .local v4, i$:I
    :goto_29
    if-ge v4, v6, :cond_16

    #@2b
    aget v7, v0, v4

    #@2d
    .line 504
    .local v7, state:I
    if-ne v7, v5, :cond_33

    #@2f
    .line 505
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@32
    goto :goto_16

    #@33
    .line 503
    :cond_33
    add-int/lit8 v4, v4, 0x1

    #@35
    goto :goto_29

    #@36
    .line 510
    .end local v0           #arr$:[I
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #i$:I
    .end local v5           #inputDeviceState:I
    .end local v6           #len$:I
    .end local v7           #state:I
    :cond_36
    return-object v2
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 261
    const-string v0, "**BluetoothFTPService"

    #@2
    return-object v0
.end method

.method protected initBinder()Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;
    .registers 2

    #@0
    .prologue
    .line 265
    new-instance v0, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;

    #@2
    invoke-direct {v0, p0, p0}, Lcom/broadcom/bt/service/ftp/FTPService$BluetoothFtpBinder;-><init>(Lcom/broadcom/bt/service/ftp/FTPService;Lcom/broadcom/bt/service/ftp/FTPService;)V

    #@5
    return-object v0
.end method

.method public isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 378
    iget-boolean v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mNativeAvailable:Z

    #@2
    if-eqz v0, :cond_10

    #@4
    sget-boolean v0, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpServerStopped:Z

    #@6
    if-nez v0, :cond_10

    #@8
    invoke-super {p0}, Lcom/android/bluetooth/btservice/ProfileService;->isAvailable()Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_10

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    return v0

    #@10
    :cond_10
    const/4 v0, 0x0

    #@11
    goto :goto_f
.end method

.method public onMediaScannerConnected()V
    .registers 3

    #@0
    .prologue
    .line 393
    const-string v0, "BluetoothFTPService"

    #@2
    const-string v1, "MediaScannerConnection onMediaScannerConnected"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 396
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->filePathForMediaScan:Ljava/lang/String;

    #@9
    if-eqz v0, :cond_10

    #@b
    .line 398
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->filePathForMediaScan:Ljava/lang/String;

    #@d
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/ftp/FTPService;->ftpMediaScanFilePath(Ljava/lang/String;)V

    #@10
    .line 400
    :cond_10
    return-void
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .registers 6
    .parameter "path"
    .parameter "uri"

    #@0
    .prologue
    .line 404
    const-string v0, "BluetoothFTPService"

    #@2
    const-string v1, "MediaScannerConnection onScanCompleted"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 405
    const-string v0, "BluetoothFTPService"

    #@9
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "MediaScannerConnection path is "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 406
    const-string v0, "BluetoothFTPService"

    #@21
    new-instance v1, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v2, "MediaScannerConnection Uri is "

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 408
    return-void
.end method

.method public registerCallback(Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 607
    if-eqz p1, :cond_b

    #@2
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 608
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@b
    .line 610
    :cond_b
    return-void
.end method

.method protected declared-synchronized start()Z
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 270
    monitor-enter p0

    #@3
    :try_start_3
    const-string v6, "BluetoothFTPService"

    #@5
    const-string v7, "FTP-Server Service start"

    #@7
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 272
    const/4 v6, 0x0

    #@b
    sput-boolean v6, Lcom/broadcom/bt/service/ftp/FTPService;->isStateRunning:Z

    #@d
    .line 273
    const/4 v6, 0x0

    #@e
    sput-boolean v6, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpServerStopped:Z

    #@10
    .line 280
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@13
    move-result-object v6

    #@14
    iput-object v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    :try_end_16
    .catchall {:try_start_3 .. :try_end_16} :catchall_99

    #@16
    .line 282
    :try_start_16
    new-instance v6, Ljava/util/HashMap;

    #@18
    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    #@1b
    iput-object v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpClientDevices:Ljava/util/Map;

    #@1d
    .line 284
    new-instance v2, Landroid/content/IntentFilter;

    #@1f
    const-string v6, "android.intent.action.MEDIA_EJECT"

    #@21
    invoke-direct {v2, v6}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@24
    .line 285
    .local v2, iFilter:Landroid/content/IntentFilter;
    const-string v6, "android.intent.action.MEDIA_UNMOUNTED"

    #@26
    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@29
    .line 286
    const-string v6, "android.intent.action.MEDIA_BAD_REMOVAL"

    #@2b
    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2e
    .line 287
    const-string v6, "android.intent.action.MEDIA_REMOVED"

    #@30
    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@33
    .line 288
    const-string v6, "broadcom.android.bluetooth.intent.action.MEDIA_EJECT"

    #@35
    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@38
    .line 289
    const-string v6, "file"

    #@3a
    invoke-virtual {v2, v6}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    #@3d
    .line 290
    const-string v6, "*"

    #@3f
    const/4 v7, 0x0

    #@40
    invoke-virtual {v2, v6, v7}, Landroid/content/IntentFilter;->addDataAuthority(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 291
    iget-object v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;
    :try_end_45
    .catchall {:try_start_16 .. :try_end_45} :catchall_99
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_45} :catch_8d

    #@45
    if-eqz v6, :cond_50

    #@47
    .line 293
    :try_start_47
    invoke-virtual {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->getBaseContext()Landroid/content/Context;

    #@4a
    move-result-object v6

    #@4b
    iget-object v7, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    #@4d
    invoke-virtual {v6, v7}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_50
    .catchall {:try_start_47 .. :try_end_50} :catchall_99
    .catch Ljava/lang/Throwable; {:try_start_47 .. :try_end_50} :catch_84
    .catch Ljava/lang/Exception; {:try_start_47 .. :try_end_50} :catch_8d

    #@50
    .line 299
    :cond_50
    :goto_50
    const/4 v1, 0x0

    #@51
    .local v1, i:I
    :goto_51
    const/16 v6, 0xa

    #@53
    if-ge v1, v6, :cond_67

    #@55
    .line 301
    :try_start_55
    new-instance v6, Landroid/media/MediaScannerConnection;

    #@57
    invoke-direct {v6, p0, p0}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    #@5a
    iput-object v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@5c
    .line 302
    iget-object v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@5e
    if-eqz v6, :cond_9c

    #@60
    .line 304
    const-string v6, "BluetoothFTPService"

    #@62
    const-string v7, "[BTUI] Connet to the MediaScanner"

    #@64
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 316
    :cond_67
    new-instance v6, Lcom/broadcom/bt/service/ftp/FTPService$UnmountBroadcastReceiver;

    #@69
    const/4 v7, 0x0

    #@6a
    invoke-direct {v6, p0, v7}, Lcom/broadcom/bt/service/ftp/FTPService$UnmountBroadcastReceiver;-><init>(Lcom/broadcom/bt/service/ftp/FTPService;Lcom/broadcom/bt/service/ftp/FTPService$1;)V

    #@6d
    iput-object v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    #@6f
    .line 317
    invoke-virtual {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->getApplicationContext()Landroid/content/Context;

    #@72
    move-result-object v6

    #@73
    iget-object v7, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mUnmountReceiver:Landroid/content/BroadcastReceiver;

    #@75
    invoke-virtual {v6, v7, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@78
    .line 319
    invoke-direct {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->initFtpNativeDataNative()V

    #@7b
    .line 320
    const/4 v6, 0x1

    #@7c
    iput-boolean v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mNativeAvailable:Z

    #@7e
    .line 321
    const/4 v6, 0x0

    #@7f
    sput-boolean v6, Lcom/broadcom/bt/service/ftp/FTPService;->mFtpServerStopped:Z
    :try_end_81
    .catchall {:try_start_55 .. :try_end_81} :catchall_99
    .catch Ljava/lang/Exception; {:try_start_55 .. :try_end_81} :catch_8d

    #@81
    move v4, v5

    #@82
    .line 327
    .end local v1           #i:I
    .end local v2           #iFilter:Landroid/content/IntentFilter;
    :goto_82
    monitor-exit p0

    #@83
    return v4

    #@84
    .line 294
    .restart local v2       #iFilter:Landroid/content/IntentFilter;
    :catch_84
    move-exception v3

    #@85
    .line 295
    .local v3, t:Ljava/lang/Throwable;
    :try_start_85
    const-string v6, "BluetoothFTPService"

    #@87
    const-string v7, "Unable to unregister unmount broadcast receiver"

    #@89
    invoke-static {v6, v7, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8c
    .catchall {:try_start_85 .. :try_end_8c} :catchall_99
    .catch Ljava/lang/Exception; {:try_start_85 .. :try_end_8c} :catch_8d

    #@8c
    goto :goto_50

    #@8d
    .line 322
    .end local v2           #iFilter:Landroid/content/IntentFilter;
    .end local v3           #t:Ljava/lang/Throwable;
    :catch_8d
    move-exception v0

    #@8e
    .line 323
    .local v0, e:Ljava/lang/Exception;
    :try_start_8e
    const-string v5, "BluetoothFTPService"

    #@90
    const-string v6, "onCreate failed"

    #@92
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@95
    .line 324
    const/4 v5, 0x0

    #@96
    iput-boolean v5, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mNativeAvailable:Z
    :try_end_98
    .catchall {:try_start_8e .. :try_end_98} :catchall_99

    #@98
    goto :goto_82

    #@99
    .line 270
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_99
    move-exception v4

    #@9a
    monitor-exit p0

    #@9b
    throw v4

    #@9c
    .line 307
    .restart local v1       #i:I
    .restart local v2       #iFilter:Landroid/content/IntentFilter;
    :cond_9c
    const/16 v6, 0x9

    #@9e
    if-ne v1, v6, :cond_af

    #@a0
    :try_start_a0
    iget-object v6, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mMediaScanner:Landroid/media/MediaScannerConnection;

    #@a2
    if-nez v6, :cond_af

    #@a4
    .line 309
    const-string v5, "BluetoothFTPService"

    #@a6
    const-string v6, "[BTUI] Can\'t connect to the MediaScanner and FTPService start failed!!"

    #@a8
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    .line 310
    const/4 v5, 0x0

    #@ac
    iput-boolean v5, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mNativeAvailable:Z
    :try_end_ae
    .catchall {:try_start_a0 .. :try_end_ae} :catchall_99
    .catch Ljava/lang/Exception; {:try_start_a0 .. :try_end_ae} :catch_8d

    #@ae
    goto :goto_82

    #@af
    .line 299
    :cond_af
    add-int/lit8 v1, v1, 0x1

    #@b1
    goto :goto_51
.end method

.method protected stop()Z
    .registers 2

    #@0
    .prologue
    .line 335
    const-string v0, "Stopping Bluetooth FTP Service"

    #@2
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/ftp/FTPService;->log(Ljava/lang/String;)V

    #@5
    .line 337
    invoke-virtual {p0}, Lcom/broadcom/bt/service/ftp/FTPService;->closeFtpServer()V

    #@8
    .line 338
    const/4 v0, 0x1

    #@9
    return v0
.end method

.method public unregisterCallback(Lcom/broadcom/bt/service/ftp/IBluetoothFTPCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 613
    if-eqz p1, :cond_b

    #@2
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 614
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@8
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@b
    .line 616
    :cond_b
    return-void
.end method
