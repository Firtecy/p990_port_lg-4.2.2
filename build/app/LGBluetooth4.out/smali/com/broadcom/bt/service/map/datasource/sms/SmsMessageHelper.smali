.class public Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;
.super Ljava/lang/Object;
.source "SmsMessageHelper.java"

# interfaces
.implements Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;
    }
.end annotation


# static fields
.field private static final COL_ADDRESS:I = 0x1

.field private static final COL_BODY:I = 0x9

.field private static final COL_DATE:I = 0x2

.field private static final COL_ID:I = 0x0

.field private static final COL_LOCKED:I = 0x3

.field private static final COL_PERSON:I = 0x4

.field private static final COL_READ:I = 0x5

.field private static final COL_THREAD_ID:I = 0x8

.field private static final COL_TYPE_ID:I = 0x7

.field public static final COUNT_COL_ADDRESS:I = 0x4

.field public static final COUNT_COL_DATE:I = 0x2

.field public static final COUNT_COL_ID:I = 0x0

.field public static final COUNT_COL_PERSON:I = 0x3

.field public static final COUNT_COL_READ:I = 0x1

.field public static final COUNT_COL_THREAD_ID:I = 0x5

.field private static final DBG:Z = true

.field private static final FOLDER_PATH_DELETED:Ljava/lang/String; = "deleted"

.field private static final FOLDER_PATH_DRAFT:Ljava/lang/String; = "draft"

.field private static final FOLDER_PATH_SENT:Ljava/lang/String; = "sent"

.field private static final SMS_MESSAGE_TYPE_DELETED:I = 0x378

.field private static final SMS_MSG_ID_PREFIX:Ljava/lang/String; = "SMS_"

#the value of this static final field might be set in the static constructor
.field private static final SMS_MSG_ID_PREFIX_LENGTH:I = 0x0

.field private static final SMS_PROVIDER_COUNT_PROJ:[Ljava/lang/String; = null

.field private static final SMS_PROVIDER_PROJ:[Ljava/lang/String; = null

.field private static final SMS_PROVIDER_PROJ_NO_BODY:[Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "BtMap.SmsMessageHelper"

.field public static final TP_PID_SHORT_MESSAGE_TYPE_1:I = 0x0

.field public static final TP_STATUS_NONE:I = 0xff

.field public static final TP_STATUS_NOT_SET:I = -0x1

.field public static final TP_STATUS_PERM_DELETED_BY_ORIG_SME:I = 0x47

.field public static final TP_STATUS_PERM_DELETED_BY_SC_ADMIN:I = 0x48

.field public static final TP_STATUS_PERM_INCOMPATIBLE_DEST:I = 0x41

.field public static final TP_STATUS_PERM_NOT_OBTAINABLE:I = 0x43

.field public static final TP_STATUS_PERM_NO_INTERWORKING:I = 0x45

.field public static final TP_STATUS_PERM_QOS_NOT_AVAILABLE:I = 0x44

.field public static final TP_STATUS_PERM_REJECTED_BY_SME:I = 0x42

.field public static final TP_STATUS_PERM_REMOTE_PROCEDURE_ERROR:I = 0x40

.field public static final TP_STATUS_PERM_SM_NO_EXIST:I = 0x49

.field public static final TP_STATUS_PERM_VALID_PER_EXPIRED:I = 0x46

.field public static final TP_STATUS_RECEIVED_OK:I = 0x0

.field public static final TP_STATUS_REPLACED:I = 0x2

.field public static final TP_STATUS_TMP_CONGESTION:I = 0x60

.field public static final TP_STATUS_TMP_NO_RESPONSE_FROM_SME:I = 0x62

.field public static final TP_STATUS_TMP_QOS_NOT_AVAILABLE:I = 0x64

.field public static final TP_STATUS_TMP_SERVICE_REJECTED:I = 0x63

.field public static final TP_STATUS_TMP_SME_BUSY:I = 0x61

.field public static final TP_STATUS_TMP_SME_ERROR:I = 0x65

.field public static final TP_STATUS_TRY_CONGESTION:I = 0x20

.field public static final TP_STATUS_TRY_NO_RESPONSE_FROM_SME:I = 0x22

.field public static final TP_STATUS_TRY_QOS_NOT_AVAILABLE:I = 0x24

.field public static final TP_STATUS_TRY_SERVICE_REJECTED:I = 0x23

.field public static final TP_STATUS_TRY_SME_BUSY:I = 0x21

.field public static final TP_STATUS_TRY_SME_ERROR:I = 0x25

.field public static final TP_STATUS_UNABLE_TO_CONFIRM_DELIVERY:I = 0x1


# instance fields
.field private mContext:Landroid/content/Context;

.field private mReceiverRegistered:Z

.field private mSender:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;

.field private mSmsEventManager:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;

.field private mSmsReceiver:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 195
    const-string v0, "SMS_"

    #@7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@a
    move-result v0

    #@b
    sput v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->SMS_MSG_ID_PREFIX_LENGTH:I

    #@d
    .line 197
    const/16 v0, 0x9

    #@f
    new-array v0, v0, [Ljava/lang/String;

    #@11
    const-string v1, "_id"

    #@13
    aput-object v1, v0, v3

    #@15
    const-string v1, "address"

    #@17
    aput-object v1, v0, v4

    #@19
    const-string v1, "date"

    #@1b
    aput-object v1, v0, v5

    #@1d
    const-string v1, "locked"

    #@1f
    aput-object v1, v0, v6

    #@21
    const-string v1, "person"

    #@23
    aput-object v1, v0, v7

    #@25
    const/4 v1, 0x5

    #@26
    const-string v2, "read"

    #@28
    aput-object v2, v0, v1

    #@2a
    const/4 v1, 0x6

    #@2b
    const-string v2, "subject"

    #@2d
    aput-object v2, v0, v1

    #@2f
    const/4 v1, 0x7

    #@30
    const-string v2, "type"

    #@32
    aput-object v2, v0, v1

    #@34
    const/16 v1, 0x8

    #@36
    const-string v2, "thread_id"

    #@38
    aput-object v2, v0, v1

    #@3a
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->SMS_PROVIDER_PROJ_NO_BODY:[Ljava/lang/String;

    #@3c
    .line 201
    const/16 v0, 0xa

    #@3e
    new-array v0, v0, [Ljava/lang/String;

    #@40
    const-string v1, "_id"

    #@42
    aput-object v1, v0, v3

    #@44
    const-string v1, "address"

    #@46
    aput-object v1, v0, v4

    #@48
    const-string v1, "date"

    #@4a
    aput-object v1, v0, v5

    #@4c
    const-string v1, "locked"

    #@4e
    aput-object v1, v0, v6

    #@50
    const-string v1, "person"

    #@52
    aput-object v1, v0, v7

    #@54
    const/4 v1, 0x5

    #@55
    const-string v2, "read"

    #@57
    aput-object v2, v0, v1

    #@59
    const/4 v1, 0x6

    #@5a
    const-string v2, "subject"

    #@5c
    aput-object v2, v0, v1

    #@5e
    const/4 v1, 0x7

    #@5f
    const-string v2, "type"

    #@61
    aput-object v2, v0, v1

    #@63
    const/16 v1, 0x8

    #@65
    const-string v2, "thread_id"

    #@67
    aput-object v2, v0, v1

    #@69
    const/16 v1, 0x9

    #@6b
    const-string v2, "body"

    #@6d
    aput-object v2, v0, v1

    #@6f
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->SMS_PROVIDER_PROJ:[Ljava/lang/String;

    #@71
    .line 215
    const/4 v0, 0x6

    #@72
    new-array v0, v0, [Ljava/lang/String;

    #@74
    const-string v1, "_id"

    #@76
    aput-object v1, v0, v3

    #@78
    const-string v1, "read"

    #@7a
    aput-object v1, v0, v4

    #@7c
    const-string v1, "date"

    #@7e
    aput-object v1, v0, v5

    #@80
    const-string v1, "person"

    #@82
    aput-object v1, v0, v6

    #@84
    const-string v1, "address"

    #@86
    aput-object v1, v0, v7

    #@88
    const/4 v1, 0x5

    #@89
    const-string v2, "thread_id"

    #@8b
    aput-object v2, v0, v1

    #@8d
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->SMS_PROVIDER_COUNT_PROJ:[Ljava/lang/String;

    #@8f
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1015
    return-void
.end method

.method static synthetic access$000(Ljava/lang/StringBuilder;Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 96
    invoke-static {p0, p1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->createMessageListSelector(Ljava/lang/StringBuilder;Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 96
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->SMS_PROVIDER_COUNT_PROJ:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 96
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$300()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 96
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->SMS_PROVIDER_PROJ:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$400()[Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 96
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->SMS_PROVIDER_PROJ_NO_BODY:[Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Landroid/content/ContentResolver;I)Ljava/util/List;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 96
    invoke-static {p0, p1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getThreadRecipientAddresses(Landroid/content/ContentResolver;I)Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$600(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 96
    invoke-static {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getPersonInfo(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static createFolderUri(Ljava/lang/String;)Landroid/net/Uri;
    .registers 5
    .parameter "folderPath"

    #@0
    .prologue
    const/16 v3, 0x2f

    #@2
    const/4 v2, 0x1

    #@3
    .line 293
    if-eqz p0, :cond_11

    #@5
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v0

    #@9
    if-lt v0, v2, :cond_11

    #@b
    invoke-virtual {p0, v3}, Ljava/lang/String;->lastIndexOf(I)I

    #@e
    move-result v0

    #@f
    if-lez v0, :cond_2b

    #@11
    .line 294
    :cond_11
    const-string v0, "BtMap.SmsMessageHelper"

    #@13
    new-instance v1, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v2, "createFolderUri(): invalid folderPath "

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 295
    const/4 v0, 0x0

    #@2a
    .line 298
    .end local p0
    :goto_2a
    return-object v0

    #@2b
    .restart local p0
    :cond_2b
    sget-object v0, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@2d
    const/4 v1, 0x0

    #@2e
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    #@31
    move-result v1

    #@32
    if-ne v1, v3, :cond_38

    #@34
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@37
    move-result-object p0

    #@38
    .end local p0
    :cond_38
    invoke-static {v0, p0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@3b
    move-result-object v0

    #@3c
    goto :goto_2a
.end method

.method private static createMessageListSelector(Ljava/lang/StringBuilder;Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;
    .registers 11
    .parameter "selection"
    .parameter "messageListFilter"

    #@0
    .prologue
    .line 346
    if-nez p0, :cond_7

    #@2
    .line 347
    new-instance p0, Ljava/lang/StringBuilder;

    #@4
    .end local p0
    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    .line 350
    .restart local p0
    :cond_7
    invoke-virtual {p1}, Lcom/broadcom/bt/map/MessageListFilter;->readStatusFilterSet()Z

    #@a
    move-result v6

    #@b
    if-eqz v6, :cond_1c

    #@d
    .line 351
    const-string v7, "read"

    #@f
    const-string v8, "="

    #@11
    invoke-virtual {p1}, Lcom/broadcom/bt/map/MessageListFilter;->filterRead()Z

    #@14
    move-result v6

    #@15
    if-eqz v6, :cond_8d

    #@17
    const-string v6, "1"

    #@19
    :goto_19
    invoke-static {p0, v7, v8, v6}, Lcom/broadcom/bt/util/DBUtil;->appendSelection(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 356
    :cond_1c
    invoke-virtual {p1}, Lcom/broadcom/bt/map/MessageListFilter;->periodBeginFilterSet()Z

    #@1f
    move-result v6

    #@20
    if-eqz v6, :cond_54

    #@22
    .line 358
    :try_start_22
    new-instance v5, Landroid/text/format/Time;

    #@24
    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    #@27
    .line 359
    .local v5, time:Landroid/text/format/Time;
    iget-object v6, p1, Lcom/broadcom/bt/map/MessageListFilter;->mPeriodBegin:Ljava/lang/String;

    #@29
    invoke-virtual {v5, v6}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    #@2c
    .line 360
    const/4 v6, 0x0

    #@2d
    invoke-virtual {v5, v6}, Landroid/text/format/Time;->normalize(Z)J

    #@30
    move-result-wide v0

    #@31
    .line 361
    .local v0, periodBeginMs:J
    const-string v6, "BtMap.SmsMessageHelper"

    #@33
    new-instance v7, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v8, "periodBegin: "

    #@3a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v7

    #@3e
    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@41
    move-result-object v7

    #@42
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v7

    #@46
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 362
    const-string v6, "date"

    #@4b
    const-string v7, ">="

    #@4d
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@50
    move-result-object v8

    #@51
    invoke-static {p0, v6, v7, v8}, Lcom/broadcom/bt/util/DBUtil;->appendSelection(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_54
    .catch Ljava/lang/Throwable; {:try_start_22 .. :try_end_54} :catch_90

    #@54
    .line 369
    .end local v0           #periodBeginMs:J
    .end local v5           #time:Landroid/text/format/Time;
    :cond_54
    :goto_54
    invoke-virtual {p1}, Lcom/broadcom/bt/map/MessageListFilter;->periodEndFilterSet()Z

    #@57
    move-result v6

    #@58
    if-eqz v6, :cond_8c

    #@5a
    .line 371
    :try_start_5a
    new-instance v5, Landroid/text/format/Time;

    #@5c
    invoke-direct {v5}, Landroid/text/format/Time;-><init>()V

    #@5f
    .line 372
    .restart local v5       #time:Landroid/text/format/Time;
    iget-object v6, p1, Lcom/broadcom/bt/map/MessageListFilter;->mPeriodEnd:Ljava/lang/String;

    #@61
    invoke-virtual {v5, v6}, Landroid/text/format/Time;->parse(Ljava/lang/String;)Z

    #@64
    .line 373
    const/4 v6, 0x0

    #@65
    invoke-virtual {v5, v6}, Landroid/text/format/Time;->normalize(Z)J

    #@68
    move-result-wide v2

    #@69
    .line 374
    .local v2, periodEndMs:J
    const-string v6, "BtMap.SmsMessageHelper"

    #@6b
    new-instance v7, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v8, "periodEnd: "

    #@72
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v7

    #@76
    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@79
    move-result-object v7

    #@7a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v7

    #@7e
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 375
    const-string v6, "date"

    #@83
    const-string v7, "<="

    #@85
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@88
    move-result-object v8

    #@89
    invoke-static {p0, v6, v7, v8}, Lcom/broadcom/bt/util/DBUtil;->appendSelection(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_8c
    .catch Ljava/lang/Throwable; {:try_start_5a .. :try_end_8c} :catch_99

    #@8c
    .line 380
    .end local v2           #periodEndMs:J
    .end local v5           #time:Landroid/text/format/Time;
    :cond_8c
    :goto_8c
    return-object p0

    #@8d
    .line 351
    :cond_8d
    const-string v6, "0"

    #@8f
    goto :goto_19

    #@90
    .line 363
    :catch_90
    move-exception v4

    #@91
    .line 364
    .local v4, t:Ljava/lang/Throwable;
    const-string v6, "BtMap.SmsMessageHelper"

    #@93
    const-string v7, "Error parsing periodBegin filter...."

    #@95
    invoke-static {v6, v7, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@98
    goto :goto_54

    #@99
    .line 376
    .end local v4           #t:Ljava/lang/Throwable;
    :catch_99
    move-exception v4

    #@9a
    .line 377
    .restart local v4       #t:Ljava/lang/Throwable;
    const-string v6, "BtMap.SmsMessageHelper"

    #@9c
    const-string v7, "Error parsing periodEnd filter...."

    #@9e
    invoke-static {v6, v7, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a1
    goto :goto_8c
.end method

.method public static final decodeMessageId(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "encodedMessagedId"

    #@0
    .prologue
    .line 276
    if-eqz p0, :cond_10

    #@2
    const-string v0, "SMS_"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_10

    #@a
    .line 277
    sget v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->SMS_MSG_ID_PREFIX_LENGTH:I

    #@c
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@f
    move-result-object p0

    #@10
    .line 279
    .end local p0
    :cond_10
    return-object p0
.end method

.method public static final encodeMessageId(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "messageId"

    #@0
    .prologue
    .line 271
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "SMS_"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    return-object v0
.end method

.method private static getPersonInfo(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;
    .registers 4
    .parameter "resolver"
    .parameter "personId"
    .parameter "address"

    #@0
    .prologue
    .line 230
    const/4 v0, 0x0

    #@1
    .line 231
    .local v0, pInfo:Lcom/broadcom/bt/map/PersonInfo;
    if-eqz p1, :cond_7

    #@3
    .line 232
    invoke-static {p0, p1}, Lcom/broadcom/bt/map/ContactsUtil;->getPersonInfoByContactId(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;

    #@6
    move-result-object v0

    #@7
    .line 234
    :cond_7
    if-nez v0, :cond_d

    #@9
    .line 235
    invoke-static {p0, p2}, Lcom/broadcom/bt/map/ContactsUtil;->getPersonInfoByPhoneNo(Landroid/content/ContentResolver;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;

    #@c
    move-result-object v0

    #@d
    .line 237
    :cond_d
    return-object v0
.end method

.method public static getSMSMessageType(I)B
    .registers 2
    .parameter "phoneType"

    #@0
    .prologue
    const/4 v0, 0x2

    #@1
    .line 288
    if-ne p0, v0, :cond_4

    #@3
    const/4 v0, 0x4

    #@4
    :cond_4
    return v0
.end method

.method public static getSmsMsgFolderType(Landroid/content/Context;Landroid/net/Uri;)I
    .registers 11
    .parameter "context"
    .parameter "messageUri"

    #@0
    .prologue
    .line 248
    const/4 v7, -0x1

    #@1
    .line 249
    .local v7, smsMsgFolderType:I
    const/4 v6, 0x0

    #@2
    .line 251
    .local v6, c:Landroid/database/Cursor;
    :try_start_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    const/4 v1, 0x1

    #@7
    new-array v2, v1, [Ljava/lang/String;

    #@9
    const/4 v1, 0x0

    #@a
    const-string v3, "type"

    #@c
    aput-object v3, v2, v1

    #@e
    const/4 v3, 0x0

    #@f
    const/4 v4, 0x0

    #@10
    const/4 v5, 0x0

    #@11
    move-object v1, p1

    #@12
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@15
    move-result-object v6

    #@16
    .line 254
    if-nez v6, :cond_33

    #@18
    .line 256
    const-string v0, "BtMap.SmsMessageHelper"

    #@1a
    const-string v1, "c is null!"

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 257
    new-instance v0, Ljava/lang/Exception;

    #@21
    const-string v1, "c is nul!"

    #@23
    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@26
    throw v0
    :try_end_27
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_27} :catch_27

    #@27
    .line 263
    :catch_27
    move-exception v8

    #@28
    .line 264
    .local v8, t:Ljava/lang/Throwable;
    const-string v0, "BtMap.SmsMessageHelper"

    #@2a
    const-string v1, "Error querying database"

    #@2c
    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2f
    .line 266
    .end local v8           #t:Ljava/lang/Throwable;
    :cond_2f
    :goto_2f
    invoke-static {v6}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@32
    .line 267
    return v7

    #@33
    .line 260
    :cond_33
    :try_start_33
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_2f

    #@39
    .line 261
    const/4 v0, 0x0

    #@3a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_3d
    .catch Ljava/lang/Throwable; {:try_start_33 .. :try_end_3d} :catch_27

    #@3d
    move-result v7

    #@3e
    goto :goto_2f
.end method

.method private static getThreadRecipientAddresses(Landroid/content/ContentResolver;I)Ljava/util/List;
    .registers 14
    .parameter "resolver"
    .parameter "threadId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/ContentResolver;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 303
    const/4 v6, 0x0

    #@1
    .line 304
    .local v6, addresses:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    const/4 v9, 0x0

    #@2
    .line 305
    .local v9, recipientIds:Ljava/lang/String;
    const/4 v7, 0x0

    #@3
    .line 307
    .local v7, c:Landroid/database/Cursor;
    :try_start_3
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_CONVERSATIONS_URI:Landroid/net/Uri;

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, "/recipients"

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@1f
    move-result-object v1

    #@20
    const/4 v0, 0x1

    #@21
    new-array v2, v0, [Ljava/lang/String;

    #@23
    const/4 v0, 0x0

    #@24
    const-string v3, "recipient_ids"

    #@26
    aput-object v3, v2, v0

    #@28
    const/4 v3, 0x0

    #@29
    const/4 v4, 0x0

    #@2a
    const/4 v5, 0x0

    #@2b
    move-object v0, p0

    #@2c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2f
    move-result-object v7

    #@30
    .line 311
    invoke-static {v7}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_3b

    #@36
    .line 312
    const/4 v0, 0x0

    #@37
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_3a
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3a} :catch_b8

    #@3a
    move-result-object v9

    #@3b
    .line 317
    :cond_3b
    :goto_3b
    invoke-static {v7}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@3e
    .line 318
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@41
    move-result v0

    #@42
    if-nez v0, :cond_b7

    #@44
    .line 319
    new-instance v10, Ljava/util/StringTokenizer;

    #@46
    const-string v0, " "

    #@48
    invoke-direct {v10, v9, v0}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    .line 320
    .local v10, t:Ljava/util/StringTokenizer;
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@4e
    move-result v0

    #@4f
    if-eqz v0, :cond_b7

    #@51
    .line 322
    :cond_51
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    #@54
    move-result-object v8

    #@55
    .line 323
    .local v8, recipientId:Ljava/lang/String;
    new-instance v6, Ljava/util/LinkedList;

    #@57
    .end local v6           #addresses:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    #@5a
    .line 325
    .restart local v6       #addresses:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    :try_start_5a
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@5c
    new-instance v1, Ljava/lang/StringBuilder;

    #@5e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@61
    const-string v2, "canonical-address/"

    #@63
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v1

    #@67
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v1

    #@6f
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@72
    move-result-object v1

    #@73
    const/4 v0, 0x1

    #@74
    new-array v2, v0, [Ljava/lang/String;

    #@76
    const/4 v0, 0x0

    #@77
    const-string v3, "address"

    #@79
    aput-object v3, v2, v0

    #@7b
    const/4 v3, 0x0

    #@7c
    const/4 v4, 0x0

    #@7d
    const/4 v5, 0x0

    #@7e
    move-object v0, p0

    #@7f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@82
    move-result-object v7

    #@83
    .line 330
    invoke-static {v7}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@86
    move-result v0

    #@87
    if-eqz v0, :cond_ae

    #@89
    .line 331
    const-string v0, "BtMap.SmsMessageHelper"

    #@8b
    new-instance v1, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v2, "Address = "

    #@92
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v1

    #@96
    const/4 v2, 0x0

    #@97
    invoke-interface {v7, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9a
    move-result-object v2

    #@9b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v1

    #@9f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v1

    #@a3
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 332
    const/4 v0, 0x0

    #@a7
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@aa
    move-result-object v0

    #@ab
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_ae
    .catch Ljava/lang/Throwable; {:try_start_5a .. :try_end_ae} :catch_d3

    #@ae
    .line 337
    :cond_ae
    :goto_ae
    invoke-static {v7}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@b1
    .line 338
    invoke-virtual {v10}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    #@b4
    move-result v0

    #@b5
    if-nez v0, :cond_51

    #@b7
    .line 341
    .end local v8           #recipientId:Ljava/lang/String;
    .end local v10           #t:Ljava/util/StringTokenizer;
    :cond_b7
    return-object v6

    #@b8
    .line 314
    :catch_b8
    move-exception v10

    #@b9
    .line 315
    .local v10, t:Ljava/lang/Throwable;
    const-string v0, "BtMap.SmsMessageHelper"

    #@bb
    new-instance v1, Ljava/lang/StringBuilder;

    #@bd
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c0
    const-string v2, "Error getting conversation:"

    #@c2
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v1

    #@c6
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v1

    #@ca
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v1

    #@ce
    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d1
    goto/16 :goto_3b

    #@d3
    .line 334
    .restart local v8       #recipientId:Ljava/lang/String;
    .local v10, t:Ljava/util/StringTokenizer;
    :catch_d3
    move-exception v11

    #@d4
    .line 335
    .local v11, tt:Ljava/lang/Throwable;
    const-string v0, "BtMap.SmsMessageHelper"

    #@d6
    const-string v1, "Unable to get canonical address"

    #@d8
    invoke-static {v0, v1, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@db
    goto :goto_ae
.end method

.method public static final toFolderPath(I)Ljava/lang/String;
    .registers 2
    .parameter "mBox"

    #@0
    .prologue
    .line 124
    sparse-switch p0, :sswitch_data_1a

    #@3
    .line 140
    const/4 v0, 0x0

    #@4
    :goto_4
    return-object v0

    #@5
    .line 126
    :sswitch_5
    const-string v0, "draft"

    #@7
    goto :goto_4

    #@8
    .line 128
    :sswitch_8
    const-string v0, "failed"

    #@a
    goto :goto_4

    #@b
    .line 130
    :sswitch_b
    const-string v0, "inbox"

    #@d
    goto :goto_4

    #@e
    .line 132
    :sswitch_e
    const-string v0, "outbox"

    #@10
    goto :goto_4

    #@11
    .line 134
    :sswitch_11
    const-string v0, "queued"

    #@13
    goto :goto_4

    #@14
    .line 136
    :sswitch_14
    const-string v0, "sent"

    #@16
    goto :goto_4

    #@17
    .line 138
    :sswitch_17
    const-string v0, "deleted"

    #@19
    goto :goto_4

    #@1a
    .line 124
    :sswitch_data_1a
    .sparse-switch
        0x1 -> :sswitch_b
        0x2 -> :sswitch_14
        0x3 -> :sswitch_5
        0x4 -> :sswitch_e
        0x5 -> :sswitch_8
        0x6 -> :sswitch_11
        0x378 -> :sswitch_17
    .end sparse-switch
.end method


# virtual methods
.method public bridge synthetic createMessageListIterator()Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;
    .registers 2

    #@0
    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->createMessageListIterator()Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public createMessageListIterator()Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;
    .registers 2

    #@0
    .prologue
    .line 1012
    new-instance v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;

    #@2
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;-><init>(Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;)V

    #@5
    return-object v0
.end method

.method public declared-synchronized finish()V
    .registers 3

    #@0
    .prologue
    .line 418
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtMap.SmsMessageHelper"

    #@3
    const-string v1, "finish()"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 419
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mReceiverRegistered:Z

    #@a
    if-eqz v0, :cond_1d

    #@c
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsReceiver:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;
    :try_end_e
    .catchall {:try_start_1 .. :try_end_e} :catchall_24

    #@e
    if-eqz v0, :cond_1d

    #@10
    .line 421
    :try_start_10
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@12
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsReceiver:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;

    #@14
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@17
    .line 422
    const/4 v0, 0x0

    #@18
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsReceiver:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;

    #@1a
    .line 423
    const/4 v0, 0x0

    #@1b
    iput-boolean v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mReceiverRegistered:Z
    :try_end_1d
    .catchall {:try_start_10 .. :try_end_1d} :catchall_24
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_1d} :catch_27

    #@1d
    .line 428
    :cond_1d
    :goto_1d
    :try_start_1d
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsEventManager:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;

    #@1f
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->finish()V
    :try_end_22
    .catchall {:try_start_1d .. :try_end_22} :catchall_24

    #@22
    .line 430
    monitor-exit p0

    #@23
    return-void

    #@24
    .line 418
    :catchall_24
    move-exception v0

    #@25
    monitor-exit p0

    #@26
    throw v0

    #@27
    .line 424
    :catch_27
    move-exception v0

    #@28
    goto :goto_1d
.end method

.method public getFolderPath(I)Ljava/lang/String;
    .registers 3
    .parameter "mBox"

    #@0
    .prologue
    .line 145
    invoke-static {p1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method public getMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZB)Lcom/broadcom/bt/util/bmsg/BMessage;
    .registers 24
    .parameter "folderPath"
    .parameter "virtualPath"
    .parameter "encodedMessageId"
    .parameter "phoneType"
    .parameter "ownerPhoneNumber"
    .parameter "includeAttachments"
    .parameter "charset"

    #@0
    .prologue
    .line 626
    invoke-static/range {p3 .. p3}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->decodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v14

    #@4
    .line 627
    .local v14, messageId:Ljava/lang/String;
    const-string v2, "BtMap.SmsMessageHelper"

    #@6
    new-instance v3, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v5, "getMessage(): encodedMessageId = "

    #@d
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    move-object/from16 v0, p3

    #@13
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    const-string v5, ", decodedMessageId="

    #@19
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 631
    if-eqz p6, :cond_31

    #@2a
    .line 632
    const-string v2, "BtMap.SmsMessageHelper"

    #@2c
    const-string v3, "getMessage(): SMS currently doesn\'t support returning attachments"

    #@2e
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 635
    :cond_31
    move-object/from16 v0, p0

    #@33
    iget-object v2, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@35
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@38
    move-result-object v1

    #@39
    .line 638
    .local v1, resolver:Landroid/content/ContentResolver;
    move/from16 v10, p7

    #@3b
    .line 639
    .local v10, bCharset:B
    const/4 v13, 0x0

    #@3c
    .line 640
    .local v13, mInfo:Lcom/broadcom/bt/map/MessageInfo;
    const/4 v12, 0x0

    #@3d
    .line 641
    .local v12, content:Ljava/lang/String;
    const/4 v11, 0x0

    #@3e
    .line 643
    .local v11, c:Landroid/database/Cursor;
    :try_start_3e
    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@40
    invoke-static {v2, v14}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@43
    move-result-object v2

    #@44
    sget-object v3, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->SMS_PROVIDER_PROJ:[Ljava/lang/String;

    #@46
    const/4 v4, 0x0

    #@47
    const/4 v5, 0x0

    #@48
    const/4 v6, 0x0

    #@49
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4c
    .catch Ljava/lang/Throwable; {:try_start_3e .. :try_end_4c} :catch_93

    #@4c
    move-result-object v4

    #@4d
    .line 646
    .end local v11           #c:Landroid/database/Cursor;
    .local v4, c:Landroid/database/Cursor;
    :try_start_4d
    invoke-static {v4}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@50
    move-result v2

    #@51
    if-eqz v2, :cond_74

    #@53
    .line 647
    const/4 v7, 0x0

    #@54
    const/4 v8, 0x0

    #@55
    const/4 v9, 0x0

    #@56
    move-object/from16 v2, p0

    #@58
    move-object/from16 v3, p1

    #@5a
    move/from16 v5, p4

    #@5c
    move-object/from16 v6, p5

    #@5e
    invoke-virtual/range {v2 .. v9}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toMessageInfo(Ljava/lang/String;Landroid/database/Cursor;ILjava/lang/String;ZILcom/broadcom/bt/map/MessageParameterFilter;)Lcom/broadcom/bt/map/MessageInfo;

    #@61
    move-result-object v13

    #@62
    .line 648
    const/16 v2, 0x9

    #@64
    invoke-interface {v4, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@67
    move-result-object v12

    #@68
    .line 649
    if-eqz v13, :cond_74

    #@6a
    if-eqz v12, :cond_74

    #@6c
    .line 650
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@6f
    move-result v2

    #@70
    add-int/lit8 v2, v2, 0x16

    #@72
    iput v2, v13, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I
    :try_end_74
    .catch Ljava/lang/Throwable; {:try_start_4d .. :try_end_74} :catch_b1

    #@74
    .line 656
    :cond_74
    :goto_74
    invoke-static {v4}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@77
    .line 658
    if-nez v13, :cond_9d

    #@79
    .line 659
    const-string v2, "BtMap.SmsMessageHelper"

    #@7b
    new-instance v3, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v5, "getMessage(): Unable to find message: "

    #@82
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v3

    #@8a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v3

    #@8e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 660
    const/4 v2, 0x0

    #@92
    .line 668
    :goto_92
    return-object v2

    #@93
    .line 653
    .end local v4           #c:Landroid/database/Cursor;
    .restart local v11       #c:Landroid/database/Cursor;
    :catch_93
    move-exception v15

    #@94
    move-object v4, v11

    #@95
    .line 654
    .end local v11           #c:Landroid/database/Cursor;
    .restart local v4       #c:Landroid/database/Cursor;
    .local v15, t:Ljava/lang/Throwable;
    :goto_95
    const-string v2, "BtMap.SmsMessageHelper"

    #@97
    const-string v3, "getMessage(): Unable to query for SMS messages"

    #@99
    invoke-static {v2, v3, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9c
    goto :goto_74

    #@9d
    .line 663
    .end local v15           #t:Ljava/lang/Throwable;
    :cond_9d
    iget-boolean v2, v13, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    #@9f
    if-nez v2, :cond_aa

    #@a1
    .line 664
    const-string v2, "BtMap.SmsMessageHelper"

    #@a3
    const-string v3, "getMessage(): Binary SMS not currentlly supported"

    #@a5
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a8
    .line 665
    const/4 v2, 0x0

    #@a9
    goto :goto_92

    #@aa
    .line 668
    :cond_aa
    move-object/from16 v0, p2

    #@ac
    invoke-static {v13, v0, v10, v12}, Lcom/broadcom/bt/map/BMessageUtil;->toBMessage(Lcom/broadcom/bt/map/MessageInfo;Ljava/lang/String;BLjava/lang/String;)Lcom/broadcom/bt/util/bmsg/BMessage;

    #@af
    move-result-object v2

    #@b0
    goto :goto_92

    #@b1
    .line 653
    :catch_b1
    move-exception v15

    #@b2
    goto :goto_95
.end method

.method public getMessageType(Ljava/lang/String;)I
    .registers 3
    .parameter "folderPath"

    #@0
    .prologue
    .line 149
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toMessageType(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public declared-synchronized init(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Landroid/content/Context;)V
    .registers 7
    .parameter "ds"
    .parameter "ctx"

    #@0
    .prologue
    .line 390
    monitor-enter p0

    #@1
    :try_start_1
    const-string v1, "BtMap.SmsMessageHelper"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "init(): context="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v2

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 391
    iput-object p2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@1d
    .line 392
    new-instance v1, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;

    #@1f
    invoke-direct {v1, p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;-><init>(Landroid/content/Context;)V

    #@22
    iput-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSender:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;

    #@24
    .line 394
    iget-boolean v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mReceiverRegistered:Z

    #@26
    if-eqz v1, :cond_40

    #@28
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsReceiver:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;
    :try_end_2a
    .catchall {:try_start_1 .. :try_end_2a} :catchall_7b

    #@2a
    if-eqz v1, :cond_40

    #@2c
    .line 396
    :try_start_2c
    const-string v1, "BtMap.SmsMessageHelper"

    #@2e
    const-string v2, "init(): already registered...Unregister..."

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 397
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@35
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsReceiver:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;

    #@37
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_3a
    .catchall {:try_start_2c .. :try_end_3a} :catchall_7b
    .catch Ljava/lang/Throwable; {:try_start_2c .. :try_end_3a} :catch_72

    #@3a
    .line 402
    :goto_3a
    const/4 v1, 0x0

    #@3b
    :try_start_3b
    iput-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsReceiver:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;

    #@3d
    .line 403
    const/4 v1, 0x0

    #@3e
    iput-boolean v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mReceiverRegistered:Z
    :try_end_40
    .catchall {:try_start_3b .. :try_end_40} :catchall_7b

    #@40
    .line 406
    :cond_40
    :try_start_40
    const-string v1, "BtMap.SmsMessageHelper"

    #@42
    const-string v2, "init(): registering....."

    #@44
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 407
    new-instance v1, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;

    #@49
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@4b
    invoke-virtual {p1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getHandler()Landroid/os/Handler;

    #@4e
    move-result-object v3

    #@4f
    invoke-direct {v1, p1, v2, v3}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;-><init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Landroid/content/Context;Landroid/os/Handler;)V

    #@52
    iput-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsEventManager:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;

    #@54
    .line 408
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsEventManager:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;

    #@56
    invoke-virtual {v1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->init()V

    #@59
    .line 409
    new-instance v1, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;

    #@5b
    invoke-direct {v1, p1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;-><init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)V

    #@5e
    iput-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsReceiver:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;

    #@60
    .line 410
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@62
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsReceiver:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;

    #@64
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsReceiver:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;

    #@66
    invoke-virtual {v3}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->createFilter()Landroid/content/IntentFilter;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@6d
    .line 411
    const/4 v1, 0x1

    #@6e
    iput-boolean v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mReceiverRegistered:Z
    :try_end_70
    .catchall {:try_start_40 .. :try_end_70} :catchall_7b
    .catch Ljava/lang/Throwable; {:try_start_40 .. :try_end_70} :catch_7e

    #@70
    .line 415
    :goto_70
    monitor-exit p0

    #@71
    return-void

    #@72
    .line 398
    :catch_72
    move-exception v0

    #@73
    .line 399
    .local v0, t:Ljava/lang/Throwable;
    :try_start_73
    const-string v1, "BtMap.SmsMessageHelper"

    #@75
    const-string v2, "init(): error unregistering broadcast receiver"

    #@77
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7a
    .catchall {:try_start_73 .. :try_end_7a} :catchall_7b

    #@7a
    goto :goto_3a

    #@7b
    .line 390
    .end local v0           #t:Ljava/lang/Throwable;
    :catchall_7b
    move-exception v1

    #@7c
    monitor-exit p0

    #@7d
    throw v1

    #@7e
    .line 412
    :catch_7e
    move-exception v0

    #@7f
    .line 413
    .restart local v0       #t:Ljava/lang/Throwable;
    :try_start_7f
    const-string v1, "BtMap.SmsMessageHelper"

    #@81
    const-string v2, "init(): error registering broadcast receiver"

    #@83
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_86
    .catchall {:try_start_7f .. :try_end_86} :catchall_7b

    #@86
    goto :goto_70
.end method

.method public final isEncodedMessageId(Ljava/lang/String;)Z
    .registers 3
    .parameter "encodedMessageId"

    #@0
    .prologue
    .line 284
    if-eqz p1, :cond_c

    #@2
    const-string v0, "SMS_"

    #@4
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public pushMessage(Lcom/broadcom/bt/map/RequestId;ILcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;Ljava/lang/String;ZZI[Ljava/lang/String;)Ljava/lang/String;
    .registers 37
    .parameter "requestId"
    .parameter "phoneType"
    .parameter "bMessage"
    .parameter "folderPath"
    .parameter "virtualFolderPath"
    .parameter "isTransparentMsg"
    .parameter "isRetry"
    .parameter "charset"
    .parameter "newFolderPath"

    #@0
    .prologue
    .line 674
    move-object/from16 v0, p0

    #@2
    iget-object v7, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    .line 675
    .local v2, resolver:Landroid/content/ContentResolver;
    const/4 v5, 0x0

    #@9
    .line 676
    .local v5, content:Ljava/lang/String;
    const/4 v4, 0x0

    #@a
    .line 677
    .local v4, destAddress:Ljava/lang/String;
    const/4 v8, 0x0

    #@b
    .line 678
    .local v8, isRead:Z
    const/4 v6, 0x0

    #@c
    .line 679
    .local v6, subject:Ljava/lang/String;
    move-object/from16 v0, p0

    #@e
    move-object/from16 v1, p4

    #@10
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toMessageType(Ljava/lang/String;)I

    #@13
    move-result v22

    #@14
    .line 686
    .local v22, smsMessageType:I
    const/4 v7, 0x4

    #@15
    move/from16 v0, v22

    #@17
    if-ne v0, v7, :cond_58

    #@19
    const/16 v20, 0x1

    #@1b
    .line 687
    .local v20, recipientRequired:Z
    :goto_1b
    const-string v7, "BtMap.SmsMessageHelper"

    #@1d
    new-instance v9, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v24, "pushMessage(): recipientRequired = "

    #@24
    move-object/from16 v0, v24

    #@26
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v9

    #@2a
    move/from16 v0, v20

    #@2c
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v9

    #@30
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v9

    #@34
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 694
    :try_start_37
    invoke-virtual/range {p3 .. p3}, Lcom/broadcom/bt/util/bmsg/BMessage;->isRead()Z

    #@3a
    move-result v8

    #@3b
    .line 697
    invoke-virtual/range {p3 .. p3}, Lcom/broadcom/bt/util/bmsg/BMessage;->getEnvelope()Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;

    #@3e
    move-result-object v14

    #@3f
    .line 698
    .local v14, bEnv:Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;
    if-nez v14, :cond_5b

    #@41
    .line 699
    const-string v7, "BtMap.SmsMessageHelper"

    #@43
    const-string v9, "pushMessage(): Unable to process BMessage. Envelope is null"

    #@45
    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_48
    .catch Ljava/lang/Exception; {:try_start_37 .. :try_end_48} :catch_6c

    #@48
    .line 743
    .end local v14           #bEnv:Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;
    :cond_48
    :goto_48
    if-eqz v5, :cond_4e

    #@4a
    if-nez v4, :cond_b7

    #@4c
    if-eqz v20, :cond_b7

    #@4e
    .line 744
    :cond_4e
    const-string v7, "BtMap.SmsMessageHelper"

    #@50
    const-string v9, "pushMessage(): Unable to push SMS text message. Invalid destination address or content"

    #@52
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 746
    const/16 v17, 0x0

    #@57
    .line 846
    :goto_57
    return-object v17

    #@58
    .line 686
    .end local v20           #recipientRequired:Z
    :cond_58
    const/16 v20, 0x0

    #@5a
    goto :goto_1b

    #@5b
    .line 707
    .restart local v14       #bEnv:Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;
    .restart local v20       #recipientRequired:Z
    :cond_5b
    const/4 v7, 0x2

    #@5c
    :try_start_5c
    invoke-static {v14, v7}, Lcom/broadcom/bt/map/BMessageUtil;->findRecipientProperty(Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;B)Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;

    #@5f
    move-result-object v15

    #@60
    .line 709
    .local v15, bProp:Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;
    if-nez v15, :cond_77

    #@62
    .line 710
    if-eqz v20, :cond_7b

    #@64
    .line 711
    const-string v7, "BtMap.SmsMessageHelper"

    #@66
    const-string v9, "pushMessage(): Unable to process BMessage. Recipient is required, but TEL property is null"

    #@68
    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6b
    .catch Ljava/lang/Exception; {:try_start_5c .. :try_end_6b} :catch_6c

    #@6b
    goto :goto_48

    #@6c
    .line 738
    .end local v14           #bEnv:Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;
    .end local v15           #bProp:Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;
    :catch_6c
    move-exception v16

    #@6d
    .line 739
    .local v16, e:Ljava/lang/Exception;
    const-string v7, "BtMap.SmsMessageHelper"

    #@6f
    const-string v9, "pushMessage(): Error parsing BMessage"

    #@71
    move-object/from16 v0, v16

    #@73
    invoke-static {v7, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@76
    goto :goto_48

    #@77
    .line 716
    .end local v16           #e:Ljava/lang/Exception;
    .restart local v14       #bEnv:Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;
    .restart local v15       #bProp:Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;
    :cond_77
    :try_start_77
    invoke-virtual {v15}, Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;->getValue()Ljava/lang/String;

    #@7a
    move-result-object v4

    #@7b
    .line 720
    :cond_7b
    invoke-static {v14}, Lcom/broadcom/bt/map/BMessageUtil;->findMessageBody(Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;)Lcom/broadcom/bt/util/bmsg/BMessageBody;

    #@7e
    move-result-object v12

    #@7f
    .line 721
    .local v12, bBody:Lcom/broadcom/bt/util/bmsg/BMessageBody;
    if-nez v12, :cond_89

    #@81
    .line 722
    const-string v7, "BtMap.SmsMessageHelper"

    #@83
    const-string v9, "pushMessage(): Unable to process BMessage. Body is null"

    #@85
    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    goto :goto_48

    #@89
    .line 727
    :cond_89
    invoke-virtual {v12}, Lcom/broadcom/bt/util/bmsg/BMessageBody;->getContent()Lcom/broadcom/bt/util/bmsg/BMessageBodyContent;

    #@8c
    move-result-object v13

    #@8d
    .line 728
    .local v13, bContent:Lcom/broadcom/bt/util/bmsg/BMessageBodyContent;
    if-nez v13, :cond_97

    #@8f
    .line 729
    const-string v7, "BtMap.SmsMessageHelper"

    #@91
    const-string v9, "pushMessage(): Unable to process BMessage. Cintent is null"

    #@93
    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    goto :goto_48

    #@97
    .line 732
    :cond_97
    invoke-virtual {v13}, Lcom/broadcom/bt/util/bmsg/BMessageBodyContent;->getFirstMessageContent()Ljava/lang/String;

    #@9a
    move-result-object v5

    #@9b
    .line 733
    const/16 v19, 0x0

    #@9d
    .line 734
    .local v19, next:Ljava/lang/String;
    :goto_9d
    invoke-virtual {v13}, Lcom/broadcom/bt/util/bmsg/BMessageBodyContent;->getNextMessageContent()Ljava/lang/String;

    #@a0
    move-result-object v19

    #@a1
    if-eqz v19, :cond_48

    #@a3
    .line 735
    new-instance v7, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v7

    #@ac
    move-object/from16 v0, v19

    #@ae
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v7

    #@b2
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_b5
    .catch Ljava/lang/Exception; {:try_start_77 .. :try_end_b5} :catch_6c

    #@b5
    move-result-object v5

    #@b6
    goto :goto_9d

    #@b7
    .line 748
    .end local v12           #bBody:Lcom/broadcom/bt/util/bmsg/BMessageBody;
    .end local v13           #bContent:Lcom/broadcom/bt/util/bmsg/BMessageBodyContent;
    .end local v14           #bEnv:Lcom/broadcom/bt/util/bmsg/BMessageEnvelope;
    .end local v15           #bProp:Lcom/broadcom/bt/util/bmsg/BMessageVCardProperty;
    .end local v19           #next:Ljava/lang/String;
    :cond_b7
    if-nez p8, :cond_ef

    #@b9
    invoke-static/range {p2 .. p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getSMSMessageType(I)B

    #@bc
    move-result v7

    #@bd
    const/4 v9, 0x2

    #@be
    if-ne v7, v9, :cond_ef

    #@c0
    .line 751
    const-string v7, "BtMap.SmsMessageHelper"

    #@c2
    new-instance v9, Ljava/lang/StringBuilder;

    #@c4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@c7
    const-string v24, "Native charset used to push message - got message "

    #@c9
    move-object/from16 v0, v24

    #@cb
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v9

    #@cf
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v9

    #@d3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v9

    #@d7
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@da
    .line 753
    move-object/from16 v0, p3

    #@dc
    invoke-virtual {v0, v5}, Lcom/broadcom/bt/util/bmsg/BMessage;->decodeSMSSubmitPDU(Ljava/lang/String;)Ljava/lang/String;

    #@df
    move-result-object v21

    #@e0
    .line 755
    .local v21, sUTF8Content:Ljava/lang/String;
    if-eqz v21, :cond_e8

    #@e2
    invoke-virtual/range {v21 .. v21}, Ljava/lang/String;->length()I

    #@e5
    move-result v7

    #@e6
    if-nez v7, :cond_148

    #@e8
    .line 756
    :cond_e8
    const-string v7, "BtMap.SmsMessageHelper"

    #@ea
    const-string v9, "Decoded message body - failed"

    #@ec
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    .line 764
    .end local v21           #sUTF8Content:Ljava/lang/String;
    :cond_ef
    :goto_ef
    invoke-virtual/range {p3 .. p3}, Lcom/broadcom/bt/util/bmsg/BMessage;->finish()V

    #@f2
    .line 765
    const/16 p3, 0x0

    #@f4
    .line 767
    const/16 v18, 0x0

    #@f6
    .line 769
    .local v18, messageUri:Landroid/net/Uri;
    :try_start_f6
    move-object/from16 v0, p0

    #@f8
    iget-object v7, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@fa
    invoke-static {v7, v4}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/lang/String;)J

    #@fd
    move-result-wide v10

    #@fe
    .line 770
    .local v10, threadId:J
    const/4 v7, 0x4

    #@ff
    move/from16 v0, v22

    #@101
    if-ne v0, v7, :cond_167

    #@103
    .line 772
    const-string v7, "BtMap.SmsMessageHelper"

    #@105
    const-string v9, "Sending message..."

    #@107
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    .line 776
    move-object/from16 v0, p0

    #@10c
    iget-object v7, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSender:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;

    #@10e
    invoke-virtual {v7, v4, v5, v10, v11}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->sendMessage(Ljava/lang/String;Ljava/lang/String;J)Landroid/net/Uri;
    :try_end_111
    .catch Ljava/lang/Exception; {:try_start_f6 .. :try_end_111} :catch_175

    #@111
    move-result-object v18

    #@112
    .line 829
    .end local v10           #threadId:J
    :cond_112
    :goto_112
    if-eqz v18, :cond_23a

    #@114
    .line 831
    const-string v7, "BtMap.SmsMessageHelper"

    #@116
    new-instance v9, Ljava/lang/StringBuilder;

    #@118
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@11b
    const-string v24, "Created message to send. Uri: "

    #@11d
    move-object/from16 v0, v24

    #@11f
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v9

    #@123
    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@126
    move-result-object v24

    #@127
    move-object/from16 v0, v24

    #@129
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v9

    #@12d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@130
    move-result-object v9

    #@131
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@134
    .line 838
    const/4 v7, 0x4

    #@135
    move/from16 v0, v22

    #@137
    if-ne v0, v7, :cond_13e

    #@139
    .line 839
    const-string p4, "sent"

    #@13b
    .line 840
    const/4 v7, 0x0

    #@13c
    aput-object p4, p9, v7

    #@13e
    .line 842
    :cond_13e
    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@141
    move-result-object v7

    #@142
    invoke-static {v7}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->encodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@145
    move-result-object v17

    #@146
    .line 843
    .local v17, messageId:Ljava/lang/String;
    goto/16 :goto_57

    #@148
    .line 759
    .end local v17           #messageId:Ljava/lang/String;
    .end local v18           #messageUri:Landroid/net/Uri;
    .restart local v21       #sUTF8Content:Ljava/lang/String;
    :cond_148
    const-string v7, "BtMap.SmsMessageHelper"

    #@14a
    new-instance v9, Ljava/lang/StringBuilder;

    #@14c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@14f
    const-string v24, "Encoded content = "

    #@151
    move-object/from16 v0, v24

    #@153
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v9

    #@157
    move-object/from16 v0, v21

    #@159
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v9

    #@15d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v9

    #@161
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@164
    .line 760
    move-object/from16 v5, v21

    #@166
    goto :goto_ef

    #@167
    .line 777
    .end local v21           #sUTF8Content:Ljava/lang/String;
    .restart local v10       #threadId:J
    .restart local v18       #messageUri:Landroid/net/Uri;
    :cond_167
    const/16 v7, 0x378

    #@169
    move/from16 v0, v22

    #@16b
    if-ne v0, v7, :cond_180

    #@16d
    .line 779
    :try_start_16d
    const-string v7, "BtMap.SmsMessageHelper"

    #@16f
    const-string v9, "Push to deleted mailbox not supported"

    #@171
    invoke-static {v7, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_174
    .catch Ljava/lang/Exception; {:try_start_16d .. :try_end_174} :catch_175

    #@174
    goto :goto_112

    #@175
    .line 825
    .end local v10           #threadId:J
    :catch_175
    move-exception v16

    #@176
    .line 826
    .restart local v16       #e:Ljava/lang/Exception;
    const-string v7, "BtMap.SmsMessageHelper"

    #@178
    const-string v9, "Unable to push message"

    #@17a
    move-object/from16 v0, v16

    #@17c
    invoke-static {v7, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@17f
    goto :goto_112

    #@180
    .line 782
    .end local v16           #e:Ljava/lang/Exception;
    .restart local v10       #threadId:J
    :cond_180
    :try_start_180
    const-string v7, "BtMap.SmsMessageHelper"

    #@182
    new-instance v9, Ljava/lang/StringBuilder;

    #@184
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@187
    const-string v24, "Pushing message to "

    #@189
    move-object/from16 v0, v24

    #@18b
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18e
    move-result-object v9

    #@18f
    move-object/from16 v0, p4

    #@191
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v9

    #@195
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v9

    #@199
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19c
    .line 784
    invoke-static/range {p4 .. p4}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->createFolderUri(Ljava/lang/String;)Landroid/net/Uri;

    #@19f
    move-result-object v3

    #@1a0
    .line 786
    .local v3, addrUri:Landroid/net/Uri;
    if-nez v3, :cond_1b1

    #@1a2
    .line 788
    const-string v7, "BtMap.SmsMessageHelper"

    #@1a4
    const-string v9, "addrUri is null!"

    #@1a6
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a9
    .line 789
    new-instance v7, Ljava/lang/Exception;

    #@1ab
    const-string v9, "addrUri is nul!"

    #@1ad
    invoke-direct {v7, v9}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@1b0
    throw v7

    #@1b1
    .line 797
    :cond_1b1
    move-object/from16 v0, p0

    #@1b3
    iget-object v0, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsEventManager:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;

    #@1b5
    move-object/from16 v24, v0

    #@1b7
    monitor-enter v24
    :try_end_1b8
    .catch Ljava/lang/Exception; {:try_start_180 .. :try_end_1b8} :catch_175

    #@1b8
    .line 798
    :try_start_1b8
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1bb
    move-result-wide v25

    #@1bc
    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1bf
    move-result-object v7

    #@1c0
    const/4 v9, 0x1

    #@1c1
    invoke-static/range {v2 .. v11}, Landroid/provider/Telephony$Sms;->addMessageToUri(Landroid/content/ContentResolver;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZZJ)Landroid/net/Uri;

    #@1c4
    move-result-object v18

    #@1c5
    .line 801
    if-eqz v18, :cond_1f1

    #@1c7
    .line 802
    const-string v7, "BtMap.SmsMessageHelper"

    #@1c9
    new-instance v9, Ljava/lang/StringBuilder;

    #@1cb
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1ce
    const-string v25, "FRED: new message uri = "

    #@1d0
    move-object/from16 v0, v25

    #@1d2
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d5
    move-result-object v9

    #@1d6
    move-object/from16 v0, v18

    #@1d8
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1db
    move-result-object v9

    #@1dc
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1df
    move-result-object v9

    #@1e0
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e3
    .line 803
    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@1e6
    move-result-object v17

    #@1e7
    .line 804
    .restart local v17       #messageId:Ljava/lang/String;
    move-object/from16 v0, p0

    #@1e9
    iget-object v7, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsEventManager:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;

    #@1eb
    const/4 v9, 0x1

    #@1ec
    move-object/from16 v0, v17

    #@1ee
    invoke-virtual {v7, v0, v9}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->setMessageCreatedByMce(Ljava/lang/String;Z)Z

    #@1f1
    .line 806
    .end local v17           #messageId:Ljava/lang/String;
    :cond_1f1
    monitor-exit v24
    :try_end_1f2
    .catchall {:try_start_1b8 .. :try_end_1f2} :catchall_237

    #@1f2
    .line 808
    if-eqz v18, :cond_112

    #@1f4
    .line 809
    const/4 v7, 0x1

    #@1f5
    move/from16 v0, v22

    #@1f7
    if-ne v0, v7, :cond_112

    #@1f9
    .line 815
    :try_start_1f9
    const-string v7, "BtMap.SmsMessageHelper"

    #@1fb
    const-string v9, "Updating properties for inbox message..."

    #@1fd
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@200
    .line 817
    new-instance v23, Landroid/content/ContentValues;

    #@202
    const/4 v7, 0x3

    #@203
    move-object/from16 v0, v23

    #@205
    invoke-direct {v0, v7}, Landroid/content/ContentValues;-><init>(I)V

    #@208
    .line 818
    .local v23, v:Landroid/content/ContentValues;
    const-string v7, "protocol"

    #@20a
    const/4 v9, 0x0

    #@20b
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@20e
    move-result-object v9

    #@20f
    move-object/from16 v0, v23

    #@211
    invoke-virtual {v0, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@214
    .line 819
    const-string v7, "reply_path_present"

    #@216
    const/4 v9, 0x0

    #@217
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21a
    move-result-object v9

    #@21b
    move-object/from16 v0, v23

    #@21d
    invoke-virtual {v0, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@220
    .line 820
    const-string v7, "status"

    #@222
    const/4 v9, -0x1

    #@223
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@226
    move-result-object v9

    #@227
    move-object/from16 v0, v23

    #@229
    invoke-virtual {v0, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@22c
    .line 821
    const/4 v7, 0x0

    #@22d
    const/4 v9, 0x0

    #@22e
    move-object/from16 v0, v18

    #@230
    move-object/from16 v1, v23

    #@232
    invoke-virtual {v2, v0, v1, v7, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_235
    .catch Ljava/lang/Exception; {:try_start_1f9 .. :try_end_235} :catch_175

    #@235
    goto/16 :goto_112

    #@237
    .line 806
    .end local v23           #v:Landroid/content/ContentValues;
    :catchall_237
    move-exception v7

    #@238
    :try_start_238
    monitor-exit v24
    :try_end_239
    .catchall {:try_start_238 .. :try_end_239} :catchall_237

    #@239
    :try_start_239
    throw v7
    :try_end_23a
    .catch Ljava/lang/Exception; {:try_start_239 .. :try_end_23a} :catch_175

    #@23a
    .line 845
    .end local v3           #addrUri:Landroid/net/Uri;
    .end local v10           #threadId:J
    :cond_23a
    const-string v7, "BtMap.SmsMessageHelper"

    #@23c
    const-string v9, "Error creating message in SMS datasource"

    #@23e
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@241
    .line 846
    const/16 v17, 0x0

    #@243
    goto/16 :goto_57
.end method

.method public setMessageDeleted(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z[Ljava/lang/String;[Ljava/lang/String;)Z
    .registers 14
    .parameter "requestId"
    .parameter "encodedMessageId"
    .parameter "deleteRequest"
    .parameter "newFolderPath"
    .parameter "newMessageId"

    #@0
    .prologue
    .line 853
    const-string v5, "BtMap.SmsMessageHelper"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "setMessageDeleted(): sessionId="

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    iget v7, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v6

    #@13
    const-string v7, ", eventId="

    #@15
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v6

    #@19
    iget v7, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v6

    #@1f
    const-string v7, ", encodedMessageId ="

    #@21
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v6

    #@25
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    const-string v7, ", deleteRequest="

    #@2b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v6

    #@37
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 858
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@3c
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3f
    move-result-object v3

    #@40
    .line 860
    .local v3, resolver:Landroid/content/ContentResolver;
    invoke-static {p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->decodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    .line 862
    .local v2, messageId:Ljava/lang/String;
    const/4 v4, 0x0

    #@45
    .line 863
    .local v4, success:Z
    if-eqz p3, :cond_a9

    #@47
    .line 868
    const-string v5, "BtMap.SmsMessageHelper"

    #@49
    const-string v6, "Permanently deleting message..."

    #@4b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 870
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mSmsEventManager:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;

    #@50
    const/4 v6, 0x0

    #@51
    invoke-virtual {v5, v2, v6}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->setMessageDeletedByMce(Ljava/lang/String;Z)Z

    #@54
    .line 873
    const/4 v5, 0x1

    #@55
    invoke-static {v5, v3, v2}, Lcom/broadcom/bt/util/DBUtil;->smsMmsDBReadUnreadQuary(ILandroid/content/ContentResolver;Ljava/lang/String;)Z

    #@58
    move-result v1

    #@59
    .line 875
    .local v1, isRead:Z
    sget-object v5, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@5b
    invoke-static {v5, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@5e
    move-result-object v5

    #@5f
    const/4 v6, 0x0

    #@60
    const/4 v7, 0x0

    #@61
    invoke-virtual {v3, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@64
    move-result v0

    #@65
    .line 877
    .local v0, deleted:I
    if-lez v0, :cond_9d

    #@67
    const/4 v4, 0x1

    #@68
    .line 878
    :goto_68
    if-eqz v4, :cond_6e

    #@6a
    .line 879
    const/4 v5, 0x0

    #@6b
    const/4 v6, 0x0

    #@6c
    aput-object v6, p5, v5

    #@6e
    .line 884
    :cond_6e
    if-eqz v4, :cond_9f

    #@70
    if-nez v1, :cond_9f

    #@72
    .line 885
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@74
    invoke-static {v5}, Lcom/broadcom/bt/util/DBUtil;->sendMsgAppClosePosterIntent(Landroid/content/Context;)V

    #@77
    .line 886
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@79
    invoke-static {v5}, Lcom/broadcom/bt/util/DBUtil;->sendMsgAppAllSeenIntent(Landroid/content/Context;)V

    #@7c
    .line 935
    :cond_7c
    :goto_7c
    const-string v5, "BtMap.SmsMessageHelper"

    #@7e
    new-instance v6, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v7, "Message deleted = "

    #@85
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v6

    #@89
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v6

    #@8d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v6

    #@91
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 936
    if-eqz v4, :cond_9b

    #@96
    .line 937
    const/4 v5, 0x0

    #@97
    const-string v6, "deleted"

    #@99
    aput-object v6, p4, v5

    #@9b
    :cond_9b
    move v5, v4

    #@9c
    .line 944
    .end local v0           #deleted:I
    .end local v1           #isRead:Z
    :goto_9c
    return v5

    #@9d
    .line 877
    .restart local v0       #deleted:I
    .restart local v1       #isRead:Z
    :cond_9d
    const/4 v4, 0x0

    #@9e
    goto :goto_68

    #@9f
    .line 888
    :cond_9f
    if-eqz v4, :cond_7c

    #@a1
    if-eqz v1, :cond_7c

    #@a3
    .line 889
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@a5
    invoke-static {v5}, Lcom/broadcom/bt/util/DBUtil;->sendMsgAppAllSeenIntent(Landroid/content/Context;)V

    #@a8
    goto :goto_7c

    #@a9
    .line 943
    .end local v0           #deleted:I
    .end local v1           #isRead:Z
    :cond_a9
    const-string v5, "BtMap.SmsMessageHelper"

    #@ab
    const-string v6, "Undelete not supported..."

    #@ad
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    .line 944
    const/4 v5, 0x0

    #@b1
    goto :goto_9c
.end method

.method public setMessageRead(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z)V
    .registers 13
    .parameter "requestId"
    .parameter "encodedMessageId"
    .parameter "isRead"

    #@0
    .prologue
    const/4 v8, 0x1

    #@1
    .line 975
    const-string v5, "BtMap.SmsMessageHelper"

    #@3
    new-instance v6, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v7, "setMessageRead(): sessionId="

    #@a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v6

    #@e
    iget v7, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@10
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v6

    #@14
    const-string v7, ", eventId="

    #@16
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    iget v7, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    const-string v7, ", encodedMessageId ="

    #@22
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v6

    #@26
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    const-string v7, ", isRead="

    #@2c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v6

    #@30
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@33
    move-result-object v6

    #@34
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v6

    #@38
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 980
    invoke-static {p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->decodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    .line 982
    .local v1, messageId:Ljava/lang/String;
    new-instance v4, Landroid/content/ContentValues;

    #@41
    invoke-direct {v4, v8}, Landroid/content/ContentValues;-><init>(I)V

    #@44
    .line 983
    .local v4, values:Landroid/content/ContentValues;
    const-string v6, "read"

    #@46
    if-eqz p3, :cond_9d

    #@48
    const-string v5, "1"

    #@4a
    :goto_4a
    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4d
    .line 985
    const-string v6, "seen"

    #@4f
    if-eqz p3, :cond_a0

    #@51
    const-string v5, "1"

    #@53
    :goto_53
    invoke-virtual {v4, v6, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@56
    .line 986
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@58
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5b
    move-result-object v5

    #@5c
    invoke-static {v8, v5, v1}, Lcom/broadcom/bt/util/DBUtil;->smsMmsDBReadUnreadQuary(ILandroid/content/ContentResolver;Ljava/lang/String;)Z

    #@5f
    move-result v0

    #@60
    .line 990
    .local v0, isAlreadyRead:Z
    :try_start_60
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@62
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@65
    move-result-object v5

    #@66
    sget-object v6, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@68
    invoke-static {v6, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@6b
    move-result-object v6

    #@6c
    const/4 v7, 0x0

    #@6d
    const/4 v8, 0x0

    #@6e
    invoke-virtual {v5, v6, v4, v7, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@71
    move-result v2

    #@72
    .line 992
    .local v2, result:I
    if-gtz v2, :cond_8c

    #@74
    .line 993
    const-string v5, "BtMap.SmsMessageHelper"

    #@76
    new-instance v6, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v7, "setMessageRead(): Unable to update read status. Message not found: "

    #@7d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v6

    #@81
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v6

    #@85
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v6

    #@89
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8c
    .line 997
    :cond_8c
    if-lez v2, :cond_a3

    #@8e
    if-nez v0, :cond_a3

    #@90
    if-eqz p3, :cond_a3

    #@92
    .line 998
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@94
    invoke-static {v5}, Lcom/broadcom/bt/util/DBUtil;->sendMsgAppAllSeenIntent(Landroid/content/Context;)V

    #@97
    .line 999
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@99
    invoke-static {v5}, Lcom/broadcom/bt/util/DBUtil;->sendMsgAppClosePosterIntent(Landroid/content/Context;)V
    :try_end_9c
    .catch Ljava/lang/Throwable; {:try_start_60 .. :try_end_9c} :catch_ad

    #@9c
    .line 1009
    .end local v2           #result:I
    :cond_9c
    :goto_9c
    return-void

    #@9d
    .line 983
    .end local v0           #isAlreadyRead:Z
    :cond_9d
    const-string v5, "0"

    #@9f
    goto :goto_4a

    #@a0
    .line 985
    :cond_a0
    const-string v5, "0"

    #@a2
    goto :goto_53

    #@a3
    .line 1001
    .restart local v0       #isAlreadyRead:Z
    .restart local v2       #result:I
    :cond_a3
    if-lez v2, :cond_9c

    #@a5
    if-eqz p3, :cond_9c

    #@a7
    .line 1002
    :try_start_a7
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@a9
    invoke-static {v5}, Lcom/broadcom/bt/util/DBUtil;->sendMsgAppAllSeenIntent(Landroid/content/Context;)V
    :try_end_ac
    .catch Ljava/lang/Throwable; {:try_start_a7 .. :try_end_ac} :catch_ad

    #@ac
    goto :goto_9c

    #@ad
    .line 1005
    .end local v2           #result:I
    :catch_ad
    move-exception v3

    #@ae
    .line 1006
    .local v3, t:Ljava/lang/Throwable;
    const-string v5, "BtMap.SmsMessageHelper"

    #@b0
    new-instance v6, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v7, "setMessageRead(): error updating read status for message: "

    #@b7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v6

    #@bb
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v6

    #@bf
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v6

    #@c3
    invoke-static {v5, v6, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c6
    goto :goto_9c
.end method

.method public toMessageInfo(Ljava/lang/String;Landroid/database/Cursor;ILjava/lang/String;ZILcom/broadcom/bt/map/MessageParameterFilter;)Lcom/broadcom/bt/map/MessageInfo;
    .registers 35
    .parameter "folderPath"
    .parameter "c"
    .parameter "phoneType"
    .parameter "ownerPhoneNumber"
    .parameter "includeMessageSize"
    .parameter "maxSubjectLength"
    .parameter "params"

    #@0
    .prologue
    .line 446
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;

    #@4
    move-object/from16 v23, v0

    #@6
    invoke-virtual/range {v23 .. v23}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v19

    #@a
    .line 448
    .local v19, resolver:Landroid/content/ContentResolver;
    if-nez p7, :cond_1ba

    #@c
    const-wide/16 v16, 0x0

    #@e
    .line 450
    .local v16, parameterMask:J
    :goto_e
    new-instance v11, Lcom/broadcom/bt/map/MessageInfo;

    #@10
    invoke-direct {v11}, Lcom/broadcom/bt/map/MessageInfo;-><init>()V

    #@13
    .line 451
    .local v11, mInfo:Lcom/broadcom/bt/map/MessageInfo;
    invoke-static/range {v19 .. v19}, Lcom/broadcom/bt/map/ContactsUtil;->getOwnerInfo(Landroid/content/ContentResolver;)Lcom/broadcom/bt/map/PersonInfo;

    #@16
    move-result-object v14

    #@17
    .line 453
    .local v14, ownerInfo:Lcom/broadcom/bt/map/PersonInfo;
    const/16 v23, 0x0

    #@19
    move-object/from16 v0, p2

    #@1b
    move/from16 v1, v23

    #@1d
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@20
    move-result v13

    #@21
    .line 454
    .local v13, msgHandle:I
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@24
    move-result-object v23

    #@25
    invoke-static/range {v23 .. v23}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->encodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v23

    #@29
    move-object/from16 v0, v23

    #@2b
    iput-object v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    #@2d
    .line 456
    const/16 v23, 0x7

    #@2f
    const/16 v24, -0x1

    #@31
    move-object/from16 v0, p2

    #@33
    move/from16 v1, v23

    #@35
    move/from16 v2, v24

    #@37
    invoke-static {v0, v1, v2}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@3a
    move-result v20

    #@3b
    .line 457
    .local v20, smsFolderType:I
    const-wide/16 v23, 0x0

    #@3d
    cmp-long v23, v16, v23

    #@3f
    if-nez v23, :cond_1c2

    #@41
    .line 458
    const-wide/16 v23, 0x10ff

    #@43
    move-wide/from16 v0, v23

    #@45
    iput-wide v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    #@47
    .line 463
    :goto_47
    invoke-static/range {v20 .. v20}, Landroid/provider/Telephony$Sms;->isOutgoingFolder(I)Z

    #@4a
    move-result v23

    #@4b
    if-nez v23, :cond_55

    #@4d
    const/16 v23, 0x3

    #@4f
    move/from16 v0, v20

    #@51
    move/from16 v1, v23

    #@53
    if-ne v0, v1, :cond_1c8

    #@55
    :cond_55
    const/16 v23, 0x1

    #@57
    :goto_57
    move/from16 v0, v23

    #@59
    iput-boolean v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mIsOutbound:Z

    #@5b
    .line 465
    invoke-static/range {p3 .. p3}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getSMSMessageType(I)B

    #@5e
    move-result v23

    #@5f
    move/from16 v0, v23

    #@61
    iput-byte v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    #@63
    .line 466
    const/16 v23, 0x1

    #@65
    move/from16 v0, v23

    #@67
    iput-boolean v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    #@69
    .line 468
    const/16 v23, 0x2

    #@6b
    const-wide/16 v24, 0x0

    #@6d
    move-object/from16 v0, p2

    #@6f
    move/from16 v1, v23

    #@71
    move-wide/from16 v2, v24

    #@73
    invoke-static {v0, v1, v2, v3}, Lcom/broadcom/bt/util/DBUtil;->getLong(Landroid/database/Cursor;IJ)J

    #@76
    move-result-wide v9

    #@77
    .line 469
    .local v9, lDateTime:J
    new-instance v6, Ljava/util/Date;

    #@79
    invoke-direct {v6, v9, v10}, Ljava/util/Date;-><init>(J)V

    #@7c
    .line 470
    .local v6, d:Ljava/util/Date;
    new-instance v7, Ljava/text/SimpleDateFormat;

    #@7e
    const-string v23, "yyyyMMddHHmmss"

    #@80
    move-object/from16 v0, v23

    #@82
    invoke-direct {v7, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@85
    .line 472
    .local v7, formatter:Ljava/text/SimpleDateFormat;
    invoke-virtual {v7, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@88
    move-result-object v23

    #@89
    move-object/from16 v0, v23

    #@8b
    iput-object v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    #@8d
    .line 473
    const-string v23, "BtMap.SmsMessageHelper"

    #@8f
    new-instance v24, Ljava/lang/StringBuilder;

    #@91
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@94
    const-string v25, "Pre-split date :"

    #@96
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v24

    #@9a
    iget-object v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    #@9c
    move-object/from16 v25, v0

    #@9e
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v24

    #@a2
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v24

    #@a6
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 474
    new-instance v23, Ljava/lang/StringBuilder;

    #@ab
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@ae
    iget-object v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    #@b0
    move-object/from16 v24, v0

    #@b2
    const/16 v25, 0x0

    #@b4
    const/16 v26, 0x8

    #@b6
    invoke-virtual/range {v24 .. v26}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@b9
    move-result-object v24

    #@ba
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v23

    #@be
    const-string v24, "T"

    #@c0
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v23

    #@c4
    iget-object v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    #@c6
    move-object/from16 v24, v0

    #@c8
    const/16 v25, 0x8

    #@ca
    const/16 v26, 0xe

    #@cc
    invoke-virtual/range {v24 .. v26}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@cf
    move-result-object v24

    #@d0
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v23

    #@d4
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v23

    #@d8
    move-object/from16 v0, v23

    #@da
    iput-object v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    #@dc
    .line 475
    const-string v23, "BtMap.SmsMessageHelper"

    #@de
    new-instance v24, Ljava/lang/StringBuilder;

    #@e0
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@e3
    const-string v25, "Post-split date :"

    #@e5
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v24

    #@e9
    iget-object v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    #@eb
    move-object/from16 v25, v0

    #@ed
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v24

    #@f1
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v24

    #@f5
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f8
    .line 476
    const/16 v23, 0x0

    #@fa
    move/from16 v0, v23

    #@fc
    iput-byte v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mReceptionStatus:B

    #@fe
    .line 478
    const/16 v23, 0x1

    #@100
    const/16 v24, 0x5

    #@102
    const/16 v25, 0x0

    #@104
    move-object/from16 v0, p2

    #@106
    move/from16 v1, v24

    #@108
    move/from16 v2, v25

    #@10a
    invoke-static {v0, v1, v2}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@10d
    move-result v24

    #@10e
    move/from16 v0, v23

    #@110
    move/from16 v1, v24

    #@112
    if-ne v0, v1, :cond_1cc

    #@114
    const/16 v23, 0x1

    #@116
    :goto_116
    move/from16 v0, v23

    #@118
    iput-boolean v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    #@11a
    .line 480
    const/16 v23, 0x2

    #@11c
    move/from16 v0, v20

    #@11e
    move/from16 v1, v23

    #@120
    if-eq v0, v1, :cond_12a

    #@122
    const/16 v23, 0x5

    #@124
    move/from16 v0, v20

    #@126
    move/from16 v1, v23

    #@128
    if-ne v0, v1, :cond_1d0

    #@12a
    :cond_12a
    const/16 v23, 0x1

    #@12c
    :goto_12c
    move/from16 v0, v23

    #@12e
    iput-boolean v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    #@130
    .line 481
    const/16 v23, 0x0

    #@132
    move/from16 v0, v23

    #@134
    iput-boolean v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    #@136
    .line 482
    const/16 v23, 0x3

    #@138
    const/16 v24, 0x0

    #@13a
    move-object/from16 v0, p2

    #@13c
    move/from16 v1, v23

    #@13e
    move/from16 v2, v24

    #@140
    invoke-static {v0, v1, v2}, Lcom/broadcom/bt/util/DBUtil;->getBooleanFromInt(Landroid/database/Cursor;IZ)Z

    #@143
    move-result v23

    #@144
    move/from16 v0, v23

    #@146
    iput-boolean v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    #@148
    .line 483
    const/16 v23, 0x0

    #@14a
    move/from16 v0, v23

    #@14c
    iput v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mAttachmentSize:I

    #@14e
    .line 488
    iget-boolean v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mIsOutbound:Z

    #@150
    move/from16 v23, v0

    #@152
    if-eqz v23, :cond_263

    #@154
    .line 489
    const-string v23, "BtMap.SmsMessageHelper"

    #@156
    const-string v24, "outbound message"

    #@158
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15b
    .line 493
    const/16 v23, 0x3

    #@15d
    move/from16 v0, v20

    #@15f
    move/from16 v1, v23

    #@161
    if-ne v0, v1, :cond_1d4

    #@163
    .line 494
    const/16 v23, 0x8

    #@165
    const/16 v24, -0x1

    #@167
    move-object/from16 v0, p2

    #@169
    move/from16 v1, v23

    #@16b
    move/from16 v2, v24

    #@16d
    invoke-static {v0, v1, v2}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@170
    move-result v22

    #@171
    .line 495
    .local v22, threadId:I
    if-lez v22, :cond_20b

    #@173
    .line 496
    move-object/from16 v0, v19

    #@175
    move/from16 v1, v22

    #@177
    invoke-static {v0, v1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getThreadRecipientAddresses(Landroid/content/ContentResolver;I)Ljava/util/List;

    #@17a
    move-result-object v5

    #@17b
    .line 497
    .local v5, addresses:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v5, :cond_20b

    #@17d
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@180
    move-result v23

    #@181
    if-lez v23, :cond_20b

    #@183
    .line 498
    const/4 v8, 0x0

    #@184
    .local v8, i:I
    :goto_184
    invoke-interface {v5}, Ljava/util/List;->size()I

    #@187
    move-result v23

    #@188
    move/from16 v0, v23

    #@18a
    if-ge v8, v0, :cond_20b

    #@18c
    .line 499
    invoke-interface {v5, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@18f
    move-result-object v4

    #@190
    check-cast v4, Ljava/lang/String;

    #@192
    .line 501
    .local v4, address:Ljava/lang/String;
    const/16 v21, 0x0

    #@194
    .line 503
    .local v21, tempaddr:Ljava/lang/String;
    if-eqz v4, :cond_1b7

    #@196
    .line 504
    const-string v23, "+"

    #@198
    move-object/from16 v0, v23

    #@19a
    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@19d
    move-result v23

    #@19e
    if-eqz v23, :cond_1a8

    #@1a0
    .line 509
    const/16 v23, 0x1

    #@1a2
    move/from16 v0, v23

    #@1a4
    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1a7
    move-result-object v21

    #@1a8
    .line 516
    :cond_1a8
    const/16 v23, 0x0

    #@1aa
    move-object/from16 v0, v19

    #@1ac
    move-object/from16 v1, v23

    #@1ae
    move-object/from16 v2, v21

    #@1b0
    invoke-static {v0, v1, v2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getPersonInfo(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;

    #@1b3
    move-result-object v15

    #@1b4
    .line 518
    .local v15, pInfo:Lcom/broadcom/bt/map/PersonInfo;
    invoke-virtual {v11, v4, v15}, Lcom/broadcom/bt/map/MessageInfo;->addRecipient(Ljava/lang/String;Lcom/broadcom/bt/map/PersonInfo;)V

    #@1b7
    .line 498
    .end local v15           #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    :cond_1b7
    add-int/lit8 v8, v8, 0x1

    #@1b9
    goto :goto_184

    #@1ba
    .line 448
    .end local v4           #address:Ljava/lang/String;
    .end local v5           #addresses:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v6           #d:Ljava/util/Date;
    .end local v7           #formatter:Ljava/text/SimpleDateFormat;
    .end local v8           #i:I
    .end local v9           #lDateTime:J
    .end local v11           #mInfo:Lcom/broadcom/bt/map/MessageInfo;
    .end local v13           #msgHandle:I
    .end local v14           #ownerInfo:Lcom/broadcom/bt/map/PersonInfo;
    .end local v16           #parameterMask:J
    .end local v20           #smsFolderType:I
    .end local v21           #tempaddr:Ljava/lang/String;
    .end local v22           #threadId:I
    :cond_1ba
    move-object/from16 v0, p7

    #@1bc
    iget-wide v0, v0, Lcom/broadcom/bt/map/MessageParameterFilter;->mParameterMask:J

    #@1be
    move-wide/from16 v16, v0

    #@1c0
    goto/16 :goto_e

    #@1c2
    .line 460
    .restart local v11       #mInfo:Lcom/broadcom/bt/map/MessageInfo;
    .restart local v13       #msgHandle:I
    .restart local v14       #ownerInfo:Lcom/broadcom/bt/map/PersonInfo;
    .restart local v16       #parameterMask:J
    .restart local v20       #smsFolderType:I
    :cond_1c2
    move-wide/from16 v0, v16

    #@1c4
    iput-wide v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    #@1c6
    goto/16 :goto_47

    #@1c8
    .line 463
    :cond_1c8
    const/16 v23, 0x0

    #@1ca
    goto/16 :goto_57

    #@1cc
    .line 478
    .restart local v6       #d:Ljava/util/Date;
    .restart local v7       #formatter:Ljava/text/SimpleDateFormat;
    .restart local v9       #lDateTime:J
    :cond_1cc
    const/16 v23, 0x0

    #@1ce
    goto/16 :goto_116

    #@1d0
    .line 480
    :cond_1d0
    const/16 v23, 0x0

    #@1d2
    goto/16 :goto_12c

    #@1d4
    .line 524
    :cond_1d4
    const/16 v23, 0x1

    #@1d6
    move-object/from16 v0, p2

    #@1d8
    move/from16 v1, v23

    #@1da
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1dd
    move-result-object v4

    #@1de
    .line 525
    .restart local v4       #address:Ljava/lang/String;
    const/16 v23, 0x4

    #@1e0
    move-object/from16 v0, p2

    #@1e2
    move/from16 v1, v23

    #@1e4
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1e7
    move-result-object v18

    #@1e8
    .line 527
    .local v18, personId:Ljava/lang/String;
    const/16 v21, 0x0

    #@1ea
    .line 529
    .restart local v21       #tempaddr:Ljava/lang/String;
    if-eqz v4, :cond_20b

    #@1ec
    .line 530
    const-string v23, "+"

    #@1ee
    move-object/from16 v0, v23

    #@1f0
    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1f3
    move-result v23

    #@1f4
    if-eqz v23, :cond_1fe

    #@1f6
    .line 535
    const/16 v23, 0x1

    #@1f8
    move/from16 v0, v23

    #@1fa
    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1fd
    move-result-object v21

    #@1fe
    .line 542
    :cond_1fe
    move-object/from16 v0, v19

    #@200
    move-object/from16 v1, v18

    #@202
    move-object/from16 v2, v21

    #@204
    invoke-static {v0, v1, v2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getPersonInfo(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;

    #@207
    move-result-object v15

    #@208
    .line 544
    .restart local v15       #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    invoke-virtual {v11, v4, v15}, Lcom/broadcom/bt/map/MessageInfo;->addRecipient(Ljava/lang/String;Lcom/broadcom/bt/map/PersonInfo;)V

    #@20b
    .line 548
    .end local v4           #address:Ljava/lang/String;
    .end local v15           #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    .end local v18           #personId:Ljava/lang/String;
    .end local v21           #tempaddr:Ljava/lang/String;
    :cond_20b
    if-eqz p4, :cond_219

    #@20d
    const-string v23, "+"

    #@20f
    move-object/from16 v0, p4

    #@211
    move-object/from16 v1, v23

    #@213
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@216
    move-result v23

    #@217
    if-eqz v23, :cond_219

    #@219
    .line 555
    :cond_219
    move-object/from16 v0, p4

    #@21b
    invoke-virtual {v11, v0, v14}, Lcom/broadcom/bt/map/MessageInfo;->setSender(Ljava/lang/String;Lcom/broadcom/bt/map/PersonInfo;)V

    #@21e
    .line 556
    move-object/from16 v0, p4

    #@220
    invoke-virtual {v11, v0, v14}, Lcom/broadcom/bt/map/MessageInfo;->setReplyTo(Ljava/lang/String;Lcom/broadcom/bt/map/PersonInfo;)V

    #@223
    .line 607
    :goto_223
    const/16 v23, 0x9

    #@225
    move-object/from16 v0, p2

    #@227
    move/from16 v1, v23

    #@229
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@22c
    move-result-object v12

    #@22d
    .line 608
    .local v12, msg:Ljava/lang/String;
    if-nez v12, :cond_231

    #@22f
    .line 609
    const-string v12, ""

    #@231
    .line 612
    :cond_231
    const-string v23, "BtMap.SmsMessageHelper"

    #@233
    new-instance v24, Ljava/lang/StringBuilder;

    #@235
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@238
    const-string v25, "MaxSubjectLength = "

    #@23a
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23d
    move-result-object v24

    #@23e
    move-object/from16 v0, v24

    #@240
    move/from16 v1, p6

    #@242
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@245
    move-result-object v24

    #@246
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@249
    move-result-object v24

    #@24a
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24d
    .line 614
    move/from16 v0, p6

    #@24f
    invoke-virtual {v11, v12, v0}, Lcom/broadcom/bt/map/MessageInfo;->setSubject(Ljava/lang/String;I)V

    #@252
    .line 615
    if-eqz p5, :cond_261

    #@254
    .line 617
    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    #@257
    move-result-object v23

    #@258
    move-object/from16 v0, v23

    #@25a
    array-length v0, v0

    #@25b
    move/from16 v23, v0

    #@25d
    move/from16 v0, v23

    #@25f
    iput v0, v11, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I

    #@261
    .line 619
    :cond_261
    const/4 v12, 0x0

    #@262
    .line 621
    return-object v11

    #@263
    .line 561
    .end local v12           #msg:Ljava/lang/String;
    :cond_263
    const-string v23, "BtMap.SmsMessageHelper"

    #@265
    const-string v24, "inbound message"

    #@267
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26a
    .line 562
    const/16 v23, 0x1

    #@26c
    move-object/from16 v0, p2

    #@26e
    move/from16 v1, v23

    #@270
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@273
    move-result-object v4

    #@274
    .line 563
    .restart local v4       #address:Ljava/lang/String;
    const/16 v23, 0x4

    #@276
    move-object/from16 v0, p2

    #@278
    move/from16 v1, v23

    #@27a
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@27d
    move-result-object v18

    #@27e
    .line 565
    .restart local v18       #personId:Ljava/lang/String;
    const/16 v21, 0x0

    #@280
    .line 568
    .restart local v21       #tempaddr:Ljava/lang/String;
    const-string v23, "BtMap.SmsMessageHelper"

    #@282
    new-instance v24, Ljava/lang/StringBuilder;

    #@284
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@287
    const-string v25, "address: "

    #@289
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28c
    move-result-object v24

    #@28d
    move-object/from16 v0, v24

    #@28f
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@292
    move-result-object v24

    #@293
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@296
    move-result-object v24

    #@297
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29a
    .line 569
    const-string v23, "BtMap.SmsMessageHelper"

    #@29c
    new-instance v24, Ljava/lang/StringBuilder;

    #@29e
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@2a1
    const-string v25, "Person ID = "

    #@2a3
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a6
    move-result-object v24

    #@2a7
    move-object/from16 v0, v24

    #@2a9
    move-object/from16 v1, v18

    #@2ab
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ae
    move-result-object v24

    #@2af
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b2
    move-result-object v24

    #@2b3
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b6
    .line 571
    if-eqz v4, :cond_2da

    #@2b8
    .line 572
    const-string v23, "+"

    #@2ba
    move-object/from16 v0, v23

    #@2bc
    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2bf
    move-result v23

    #@2c0
    if-eqz v23, :cond_2ca

    #@2c2
    .line 577
    const/16 v23, 0x1

    #@2c4
    move/from16 v0, v23

    #@2c6
    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2c9
    move-result-object v21

    #@2ca
    .line 584
    :cond_2ca
    move-object/from16 v0, v19

    #@2cc
    move-object/from16 v1, v18

    #@2ce
    move-object/from16 v2, v21

    #@2d0
    invoke-static {v0, v1, v2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getPersonInfo(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;

    #@2d3
    move-result-object v15

    #@2d4
    .line 586
    .restart local v15       #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    invoke-virtual {v11, v4, v15}, Lcom/broadcom/bt/map/MessageInfo;->setReplyTo(Ljava/lang/String;Lcom/broadcom/bt/map/PersonInfo;)V

    #@2d7
    .line 587
    invoke-virtual {v11, v4, v15}, Lcom/broadcom/bt/map/MessageInfo;->setSender(Ljava/lang/String;Lcom/broadcom/bt/map/PersonInfo;)V

    #@2da
    .line 590
    .end local v15           #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    :cond_2da
    if-eqz p4, :cond_2e8

    #@2dc
    const-string v23, "+"

    #@2de
    move-object/from16 v0, p4

    #@2e0
    move-object/from16 v1, v23

    #@2e2
    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2e5
    move-result v23

    #@2e6
    if-eqz v23, :cond_2e8

    #@2e8
    .line 597
    :cond_2e8
    move-object/from16 v0, p4

    #@2ea
    invoke-virtual {v11, v0, v14}, Lcom/broadcom/bt/map/MessageInfo;->addRecipient(Ljava/lang/String;Lcom/broadcom/bt/map/PersonInfo;)V

    #@2ed
    .line 599
    const-string v23, "BtMap.SmsMessageHelper"

    #@2ef
    new-instance v24, Ljava/lang/StringBuilder;

    #@2f1
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@2f4
    const-string v25, "Sender Name = "

    #@2f6
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f9
    move-result-object v24

    #@2fa
    const/16 v25, 0x0

    #@2fc
    move/from16 v0, v25

    #@2fe
    invoke-virtual {v11, v0}, Lcom/broadcom/bt/map/MessageInfo;->getSenderDisplayName(Z)Ljava/lang/String;

    #@301
    move-result-object v25

    #@302
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@305
    move-result-object v24

    #@306
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@309
    move-result-object v24

    #@30a
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30d
    .line 600
    const-string v23, "BtMap.SmsMessageHelper"

    #@30f
    new-instance v24, Ljava/lang/StringBuilder;

    #@311
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@314
    const-string v25, "Sender Addressing = "

    #@316
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@319
    move-result-object v24

    #@31a
    const/16 v25, 0x0

    #@31c
    move/from16 v0, v25

    #@31e
    invoke-virtual {v11, v0}, Lcom/broadcom/bt/map/MessageInfo;->getSenderAddress(Z)Ljava/lang/String;

    #@321
    move-result-object v25

    #@322
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@325
    move-result-object v24

    #@326
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@329
    move-result-object v24

    #@32a
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32d
    .line 601
    const-string v23, "BtMap.SmsMessageHelper"

    #@32f
    new-instance v24, Ljava/lang/StringBuilder;

    #@331
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@334
    const-string v25, "ReplyTo Addressing = "

    #@336
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@339
    move-result-object v24

    #@33a
    const/16 v25, 0x0

    #@33c
    move/from16 v0, v25

    #@33e
    invoke-virtual {v11, v0}, Lcom/broadcom/bt/map/MessageInfo;->getReplyToAddress(Z)Ljava/lang/String;

    #@341
    move-result-object v25

    #@342
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@345
    move-result-object v24

    #@346
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@349
    move-result-object v24

    #@34a
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34d
    .line 602
    const-string v23, "BtMap.SmsMessageHelper"

    #@34f
    new-instance v24, Ljava/lang/StringBuilder;

    #@351
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@354
    const-string v25, "Recipient Addressing = "

    #@356
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@359
    move-result-object v24

    #@35a
    const/16 v25, 0x0

    #@35c
    move/from16 v0, v25

    #@35e
    invoke-virtual {v11, v0}, Lcom/broadcom/bt/map/MessageInfo;->getRecipientAddress(Z)Ljava/lang/String;

    #@361
    move-result-object v25

    #@362
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@365
    move-result-object v24

    #@366
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@369
    move-result-object v24

    #@36a
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36d
    .line 603
    const-string v23, "BtMap.SmsMessageHelper"

    #@36f
    new-instance v24, Ljava/lang/StringBuilder;

    #@371
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@374
    const-string v25, "Recipient Name = "

    #@376
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@379
    move-result-object v24

    #@37a
    const/16 v25, 0x0

    #@37c
    move/from16 v0, v25

    #@37e
    invoke-virtual {v11, v0}, Lcom/broadcom/bt/map/MessageInfo;->getRecipientDisplayName(Z)Ljava/lang/String;

    #@381
    move-result-object v25

    #@382
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@385
    move-result-object v24

    #@386
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@389
    move-result-object v24

    #@38a
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38d
    goto/16 :goto_223
.end method

.method public toMessageType(Ljava/lang/String;)I
    .registers 3
    .parameter "folderPath"

    #@0
    .prologue
    .line 103
    if-eqz p1, :cond_49

    #@2
    .line 104
    const-string v0, "draft"

    #@4
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    .line 105
    const/4 v0, 0x3

    #@b
    .line 120
    :goto_b
    return v0

    #@c
    .line 106
    :cond_c
    const-string v0, "failed"

    #@e
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_16

    #@14
    .line 107
    const/4 v0, 0x5

    #@15
    goto :goto_b

    #@16
    .line 108
    :cond_16
    const-string v0, "inbox"

    #@18
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_20

    #@1e
    .line 109
    const/4 v0, 0x1

    #@1f
    goto :goto_b

    #@20
    .line 110
    :cond_20
    const-string v0, "outbox"

    #@22
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_2a

    #@28
    .line 111
    const/4 v0, 0x4

    #@29
    goto :goto_b

    #@2a
    .line 112
    :cond_2a
    const-string v0, "queued"

    #@2c
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2f
    move-result v0

    #@30
    if-eqz v0, :cond_34

    #@32
    .line 113
    const/4 v0, 0x6

    #@33
    goto :goto_b

    #@34
    .line 114
    :cond_34
    const-string v0, "sent"

    #@36
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@39
    move-result v0

    #@3a
    if-eqz v0, :cond_3e

    #@3c
    .line 115
    const/4 v0, 0x2

    #@3d
    goto :goto_b

    #@3e
    .line 116
    :cond_3e
    const-string v0, "deleted"

    #@40
    invoke-virtual {v0, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@43
    move-result v0

    #@44
    if-eqz v0, :cond_49

    #@46
    .line 117
    const/16 v0, 0x378

    #@48
    goto :goto_b

    #@49
    .line 120
    :cond_49
    const/4 v0, -0x1

    #@4a
    goto :goto_b
.end method
