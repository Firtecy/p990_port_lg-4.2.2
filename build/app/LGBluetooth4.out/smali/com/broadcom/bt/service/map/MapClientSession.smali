.class public Lcom/broadcom/bt/service/map/MapClientSession;
.super Ljava/lang/Object;
.source "MapClientSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field public static final INVALID_MESSAGE_ID:J = -0x1L

.field public static final MSG_FOLDER:Ljava/lang/String; = "msg"

.field public static final ROOT_FOLDER:Ljava/lang/String; = "root"

.field public static final ROOT_TELECOM_FOLDER:Ljava/lang/String; = "root/telecom"

.field public static final ROOT_TELECOM_MSG_DELETED_FOLDER:Ljava/lang/String; = "root/telecom/msg/deleted"

.field public static final ROOT_TELECOM_MSG_FOLDER:Ljava/lang/String; = "root/telecom/msg"

.field private static final TAG:Ljava/lang/String; = "BtMap.MapClientSession"

.field public static final TELECOM_FOLDER:Ljava/lang/String; = "telecom"

.field public static final TELECOM_MSG_FOLDER:Ljava/lang/String; = "telecom/msg"


# instance fields
.field mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

.field mCurrentPath:Ljava/lang/String;

.field mDebugFolderEntryMap:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/FolderInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field mDebugMessageInfoEntryMap:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field mDevName:Ljava/lang/String;

.field mFolderEntryMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/FolderInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public mMessageIdCounter:J

.field mMessageIdToFolder:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field mMessageInfoEntryMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;>;"
        }
    .end annotation
.end field

.field private mNotificationEnabled:Z

.field mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

.field mSessionId:I


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/map/MapDatasourceContext;ILandroid/bluetooth/BluetoothDevice;)V
    .registers 8
    .parameter "ctx"
    .parameter "sessionId"
    .parameter "device"

    #@0
    .prologue
    const/16 v3, 0xa

    #@2
    const/16 v2, 0x7530

    #@4
    .line 142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@7
    .line 102
    const-wide/16 v0, 0x1

    #@9
    iput-wide v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdCounter:J

    #@b
    .line 121
    new-instance v0, Ljava/util/HashMap;

    #@d
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@10
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mFolderEntryMap:Ljava/util/HashMap;

    #@12
    .line 124
    new-instance v0, Ljava/util/HashMap;

    #@14
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@17
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageInfoEntryMap:Ljava/util/HashMap;

    #@19
    .line 128
    new-instance v0, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@1b
    invoke-direct {v0, v2}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;-><init>(I)V

    #@1e
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@20
    .line 130
    new-instance v0, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@22
    invoke-direct {v0, v2}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;-><init>(I)V

    #@25
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@27
    .line 132
    new-instance v0, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@29
    invoke-direct {v0, v2}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;-><init>(I)V

    #@2c
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToFolder:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@2e
    .line 143
    iput-object p1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@30
    .line 144
    iput p2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mSessionId:I

    #@32
    .line 145
    iput-object p3, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@34
    .line 146
    const-string v0, "root"

    #@36
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/map/MapClientSession;->setCurrentPath(Ljava/lang/String;)V

    #@39
    .line 148
    new-instance v0, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@3b
    invoke-direct {v0, v3}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;-><init>(I)V

    #@3e
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mDebugFolderEntryMap:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@40
    .line 150
    new-instance v0, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@42
    invoke-direct {v0, v3}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;-><init>(I)V

    #@45
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mDebugMessageInfoEntryMap:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@47
    .line 153
    return-void
.end method

.method private addMessageIdMap(JLjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "messageHandle"
    .parameter "dsMsgHandle"
    .parameter "dsFolderPath"

    #@0
    .prologue
    .line 511
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@2
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1, p3}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 512
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@b
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, p3, v1}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@12
    .line 513
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToFolder:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@14
    invoke-virtual {v0, p3, p4}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17
    .line 514
    return-void
.end method

.method private getMappedPath(Ljava/lang/String;)Ljava/lang/String;
    .registers 13
    .parameter "virtualPath"

    #@0
    .prologue
    .line 371
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v8

    #@6
    if-nez v8, :cond_11

    #@8
    .line 372
    :cond_8
    const-string v8, "BtMap.MapClientSession"

    #@a
    const-string v9, "getMappedPath(): Invalid virtual path "

    #@c
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    move-object v5, p1

    #@10
    .line 395
    :cond_10
    :goto_10
    return-object v5

    #@11
    .line 375
    :cond_11
    const/4 v2, 0x0

    #@12
    .line 377
    .local v2, folderMappings:[Ljava/lang/String;
    :try_start_12
    iget-object v8, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@14
    iget-object v2, v8, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mFolderMappings:[Ljava/lang/String;

    #@16
    .line 378
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@19
    move-result v7

    #@1a
    .line 379
    .local v7, vPathLength:I
    move-object v0, v2

    #@1b
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@1c
    .local v4, len$:I
    const/4 v3, 0x0

    #@1d
    .local v3, i$:I
    :goto_1d
    if-ge v3, v4, :cond_6a

    #@1f
    aget-object v1, v0, v3

    #@21
    .line 380
    .local v1, folderMap:Ljava/lang/String;
    invoke-virtual {v1, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@24
    move-result v8

    #@25
    if-eqz v8, :cond_5f

    #@27
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@2a
    move-result v8

    #@2b
    add-int/lit8 v9, v7, 0x1

    #@2d
    if-le v8, v9, :cond_5f

    #@2f
    invoke-virtual {v1, v7}, Ljava/lang/String;->charAt(I)C

    #@32
    move-result v8

    #@33
    const/16 v9, 0x3d

    #@35
    if-ne v8, v9, :cond_5f

    #@37
    .line 382
    const-string v8, "BtMap.MapClientSession"

    #@39
    new-instance v9, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v10, "Folder mapping found : "

    #@40
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v9

    #@44
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v9

    #@48
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v9

    #@4c
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 383
    add-int/lit8 v8, v7, 0x1

    #@51
    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@54
    move-result-object v5

    #@55
    .line 384
    .local v5, mappedMap:Ljava/lang/String;
    const/4 v8, 0x0

    #@56
    invoke-virtual {v5, v8}, Ljava/lang/String;->charAt(I)C
    :try_end_59
    .catch Ljava/lang/Throwable; {:try_start_12 .. :try_end_59} :catch_62

    #@59
    move-result v8

    #@5a
    const/16 v9, 0x2f

    #@5c
    if-ne v8, v9, :cond_10

    #@5e
    goto :goto_10

    #@5f
    .line 379
    .end local v5           #mappedMap:Ljava/lang/String;
    :cond_5f
    add-int/lit8 v3, v3, 0x1

    #@61
    goto :goto_1d

    #@62
    .line 391
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #folderMap:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v7           #vPathLength:I
    :catch_62
    move-exception v6

    #@63
    .line 392
    .local v6, t:Ljava/lang/Throwable;
    const-string v8, "BtMap.MapClientSession"

    #@65
    const-string v9, "Error getting datasource path"

    #@67
    invoke-static {v8, v9, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6a
    .line 394
    .end local v6           #t:Ljava/lang/Throwable;
    :cond_6a
    const-string v8, "BtMap.MapClientSession"

    #@6c
    new-instance v9, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v10, "Unable to get datasource path for "

    #@73
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v9

    #@77
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v9

    #@7b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v9

    #@7f
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    move-object v5, p1

    #@83
    .line 395
    goto :goto_10
.end method

.method private getReverseMappedPath(Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "dsPath"

    #@0
    .prologue
    .line 340
    if-eqz p1, :cond_8

    #@2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@5
    move-result v6

    #@6
    if-nez v6, :cond_10

    #@8
    .line 341
    :cond_8
    const-string v6, "BtMap.MapClientSession"

    #@a
    const-string v7, "getReverseMappedPath(): invalid datasource path "

    #@c
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 360
    .end local p1
    :goto_f
    return-object p1

    #@10
    .line 344
    .restart local p1
    :cond_10
    const/4 v2, 0x0

    #@11
    .line 346
    .local v2, folderMappings:[Ljava/lang/String;
    :try_start_11
    iget-object v6, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@13
    iget-object v2, v6, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mFolderMappings:[Ljava/lang/String;

    #@15
    .line 348
    move-object v0, v2

    #@16
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@17
    .local v4, len$:I
    const/4 v3, 0x0

    #@18
    .local v3, i$:I
    :goto_18
    if-ge v3, v4, :cond_64

    #@1a
    aget-object v1, v0, v3

    #@1c
    .line 349
    .local v1, folderMap:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v7, "="

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v6

    #@2f
    invoke-virtual {v1, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@32
    move-result v6

    #@33
    if-eqz v6, :cond_59

    #@35
    .line 351
    const-string v6, "BtMap.MapClientSession"

    #@37
    new-instance v7, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v8, "getReverseMappedPath(): folder mapping found : "

    #@3e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v7

    #@42
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v7

    #@46
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v7

    #@4a
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 353
    const/4 v6, 0x0

    #@4e
    const-string v7, "="

    #@50
    invoke-virtual {v1, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@53
    move-result v7

    #@54
    invoke-virtual {v1, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_57
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_57} :catch_5c

    #@57
    move-result-object p1

    #@58
    goto :goto_f

    #@59
    .line 348
    :cond_59
    add-int/lit8 v3, v3, 0x1

    #@5b
    goto :goto_18

    #@5c
    .line 356
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #folderMap:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :catch_5c
    move-exception v5

    #@5d
    .line 357
    .local v5, t:Ljava/lang/Throwable;
    const-string v6, "BtMap.MapClientSession"

    #@5f
    const-string v7, "Error getting datasource path"

    #@61
    invoke-static {v6, v7, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@64
    .line 359
    .end local v5           #t:Ljava/lang/Throwable;
    :cond_64
    const-string v6, "BtMap.MapClientSession"

    #@66
    new-instance v7, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v8, "Unable to get datasource path for "

    #@6d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v7

    #@71
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v7

    #@75
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v7

    #@79
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    goto :goto_f
.end method

.method static isVirtualDeletedFolder(Ljava/lang/String;)Z
    .registers 2
    .parameter "virtualPath"

    #@0
    .prologue
    .line 98
    if-eqz p0, :cond_c

    #@2
    const-string v0, "root/telecom/msg/deleted"

    #@4
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method private static normalizePath(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "path"

    #@0
    .prologue
    .line 189
    if-nez p0, :cond_c

    #@2
    .line 190
    const-string v1, "BtMap.MapClientSession"

    #@4
    const-string v2, "normalizePath(): path is null..."

    #@6
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 191
    const-string v0, ""

    #@b
    .line 207
    :cond_b
    :goto_b
    return-object v0

    #@c
    .line 193
    :cond_c
    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@f
    move-result-object p0

    #@10
    .line 194
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_26

    #@16
    const-string v1, "."

    #@18
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b
    move-result v1

    #@1c
    if-nez v1, :cond_26

    #@1e
    const-string v1, "./"

    #@20
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_30

    #@26
    .line 195
    :cond_26
    const-string v1, "BtMap.MapClientSession"

    #@28
    const-string v2, "normalizePath(): path is empty or current path..."

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 196
    const-string v0, ""

    #@2f
    goto :goto_b

    #@30
    .line 199
    :cond_30
    invoke-static {p0}, Lcom/broadcom/bt/service/map/MapClientSession;->replaceDotInPath(Ljava/lang/String;)Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    .line 201
    .local v0, path2:Ljava/lang/String;
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v1

    #@38
    if-nez v1, :cond_b

    #@3a
    .line 202
    const-string v1, "BtMap.MapClientSession"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "normalizePath(): Requested path has relative path expression...New path is "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    goto :goto_b
.end method

.method private static replaceDotInPath(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "path"

    #@0
    .prologue
    .line 167
    const-string v0, "."

    #@2
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    const-string v0, "./"

    #@a
    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_13

    #@10
    .line 168
    :cond_10
    const-string p0, ""

    #@12
    .line 178
    :cond_12
    :goto_12
    return-object p0

    #@13
    .line 171
    :cond_13
    const-string v0, "./"

    #@15
    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_20

    #@1b
    .line 172
    const/4 v0, 0x2

    #@1c
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1f
    move-result-object p0

    #@20
    .line 174
    :cond_20
    const-string v0, "./"

    #@22
    const-string v1, "/"

    #@24
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@27
    move-result-object p0

    #@28
    .line 175
    const-string v0, "/"

    #@2a
    invoke-virtual {p0, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_12

    #@30
    .line 176
    const/4 v0, 0x0

    #@31
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@34
    move-result v1

    #@35
    add-int/lit8 v1, v1, -0x1

    #@37
    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3a
    move-result-object p0

    #@3b
    goto :goto_12
.end method


# virtual methods
.method addNewMessageHandleToMap(Ljava/lang/String;Ljava/lang/String;)J
    .registers 8
    .parameter "messageId"
    .parameter "dsFolderPath"

    #@0
    .prologue
    .line 517
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageHandleFromId(Ljava/lang/String;)J

    #@3
    move-result-wide v0

    #@4
    .line 518
    .local v0, mHandle:J
    const-wide/16 v2, -0x1

    #@6
    cmp-long v2, v0, v2

    #@8
    if-eqz v2, :cond_2d

    #@a
    .line 519
    const-string v2, "BtMap.MapClientSession"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "addNewMessageHandleToMap(): Message ID "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    const-string v4, " already assigned to handle "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 524
    :goto_2c
    return-wide v0

    #@2d
    .line 521
    :cond_2d
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapClientSession;->getNewMessageHandle()J

    #@30
    move-result-wide v0

    #@31
    .line 522
    invoke-direct {p0, v0, v1, p1, p2}, Lcom/broadcom/bt/service/map/MapClientSession;->addMessageIdMap(JLjava/lang/String;Ljava/lang/String;)V

    #@34
    goto :goto_2c
.end method

.method clearMessageIdMap()V
    .registers 2

    #@0
    .prologue
    .line 490
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@2
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->clear()V

    #@5
    .line 491
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@7
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->clear()V

    #@a
    .line 492
    return-void
.end method

.method public dumpState(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .registers 19
    .parameter "builder"
    .parameter "linePrefix"

    #@0
    .prologue
    .line 629
    invoke-virtual/range {p1 .. p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3
    .line 630
    const-string v13, "Session ID            : "

    #@5
    move-object/from16 v0, p1

    #@7
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 631
    move-object/from16 v0, p0

    #@c
    iget v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mSessionId:I

    #@e
    invoke-static {v13}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@11
    move-result-object v13

    #@12
    move-object/from16 v0, p1

    #@14
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    .line 632
    const-string v13, "\n"

    #@19
    move-object/from16 v0, p1

    #@1b
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 633
    invoke-virtual/range {p1 .. p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    .line 634
    const-string v13, "Remote device address : "

    #@23
    move-object/from16 v0, p1

    #@25
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    .line 635
    move-object/from16 v0, p0

    #@2a
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@2c
    if-eqz v13, :cond_3f

    #@2e
    .line 636
    move-object/from16 v0, p0

    #@30
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@32
    invoke-virtual {v13}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@35
    move-result-object v13

    #@36
    invoke-static {v13}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@39
    move-result-object v13

    #@3a
    move-object/from16 v0, p1

    #@3c
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    .line 638
    :cond_3f
    const-string v13, "\n"

    #@41
    move-object/from16 v0, p1

    #@43
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    .line 639
    invoke-virtual/range {p1 .. p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    .line 640
    const-string v13, "Remote device name    : "

    #@4b
    move-object/from16 v0, p1

    #@4d
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    .line 641
    move-object/from16 v0, p0

    #@52
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mDevName:Ljava/lang/String;

    #@54
    if-eqz v13, :cond_127

    #@56
    move-object/from16 v0, p0

    #@58
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mDevName:Ljava/lang/String;

    #@5a
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    #@5d
    move-result v13

    #@5e
    if-lez v13, :cond_127

    #@60
    .line 642
    move-object/from16 v0, p0

    #@62
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mDevName:Ljava/lang/String;

    #@64
    move-object/from16 v0, p1

    #@66
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    .line 647
    :cond_69
    :goto_69
    const-string v13, "\n"

    #@6b
    move-object/from16 v0, p1

    #@6d
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    .line 648
    invoke-virtual/range {p1 .. p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    .line 649
    const-string v13, "Current Path          : "

    #@75
    move-object/from16 v0, p1

    #@77
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    .line 650
    move-object/from16 v0, p0

    #@7c
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mCurrentPath:Ljava/lang/String;

    #@7e
    invoke-static {v13}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@81
    move-result-object v13

    #@82
    move-object/from16 v0, p1

    #@84
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    .line 652
    const-string v13, "\n"

    #@89
    move-object/from16 v0, p1

    #@8b
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    .line 653
    const-string v13, "Notification Enabled? : "

    #@90
    move-object/from16 v0, p1

    #@92
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    .line 654
    move-object/from16 v0, p0

    #@97
    iget-boolean v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mNotificationEnabled:Z

    #@99
    move-object/from16 v0, p1

    #@9b
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9e
    .line 656
    const-string v13, "\n"

    #@a0
    move-object/from16 v0, p1

    #@a2
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    .line 657
    invoke-virtual/range {p1 .. p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    .line 658
    const-string v13, "Last Folder Entries   :\n"

    #@aa
    move-object/from16 v0, p1

    #@ac
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    .line 659
    move-object/from16 v0, p0

    #@b1
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mDebugFolderEntryMap:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@b3
    if-eqz v13, :cond_140

    #@b5
    .line 660
    move-object/from16 v0, p0

    #@b7
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mDebugFolderEntryMap:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@b9
    invoke-virtual {v13}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->clone()Ljava/lang/Object;

    #@bc
    move-result-object v9

    #@bd
    check-cast v9, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@bf
    .line 662
    .local v9, mFolderEntryMapClone:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;,"Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap<Ljava/lang/Integer;Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;>;"
    invoke-virtual {v9}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->entrySet()Ljava/util/Set;

    #@c2
    move-result-object v13

    #@c3
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@c6
    move-result-object v6

    #@c7
    .line 664
    .local v6, fIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;>;>;"
    :cond_c7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@ca
    move-result v13

    #@cb
    if-eqz v13, :cond_140

    #@cd
    .line 665
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@d0
    move-result-object v2

    #@d1
    check-cast v2, Ljava/util/Map$Entry;

    #@d3
    .line 666
    .local v2, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;>;"
    invoke-virtual/range {p1 .. p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    .line 667
    const-string v13, "    EventId :"

    #@d8
    move-object/from16 v0, p1

    #@da
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    .line 669
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@e0
    move-result-object v13

    #@e1
    move-object/from16 v0, p1

    #@e3
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e6
    .line 670
    const-string v13, "\n"

    #@e8
    move-object/from16 v0, p1

    #@ea
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ed
    .line 671
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@f0
    move-result-object v13

    #@f1
    check-cast v13, Ljava/util/List;

    #@f3
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@f6
    move-result-object v5

    #@f7
    .line 672
    .local v5, fEntryIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/map/FolderInfo;>;"
    if-eqz v5, :cond_c7

    #@f9
    .line 673
    :goto_f9
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@fc
    move-result v13

    #@fd
    if-eqz v13, :cond_c7

    #@ff
    .line 674
    const-string v13, "\n"

    #@101
    move-object/from16 v0, p1

    #@103
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    .line 675
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@109
    move-result-object v13

    #@10a
    check-cast v13, Lcom/broadcom/bt/map/FolderInfo;

    #@10c
    new-instance v14, Ljava/lang/StringBuilder;

    #@10e
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@111
    move-object/from16 v0, p2

    #@113
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v14

    #@117
    const-string v15, "        "

    #@119
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v14

    #@11d
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@120
    move-result-object v14

    #@121
    move-object/from16 v0, p1

    #@123
    invoke-virtual {v13, v0, v14}, Lcom/broadcom/bt/map/FolderInfo;->dumpState(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@126
    goto :goto_f9

    #@127
    .line 643
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;>;"
    .end local v5           #fEntryIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/map/FolderInfo;>;"
    .end local v6           #fIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;>;>;"
    .end local v9           #mFolderEntryMapClone:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;,"Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap<Ljava/lang/Integer;Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;>;"
    :cond_127
    move-object/from16 v0, p0

    #@129
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@12b
    if-eqz v13, :cond_69

    #@12d
    .line 644
    move-object/from16 v0, p0

    #@12f
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@131
    invoke-virtual {v13}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    #@134
    move-result-object v13

    #@135
    invoke-static {v13}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@138
    move-result-object v13

    #@139
    move-object/from16 v0, p1

    #@13b
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    goto/16 :goto_69

    #@140
    .line 681
    :cond_140
    const-string v13, "\n"

    #@142
    move-object/from16 v0, p1

    #@144
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    .line 682
    invoke-virtual/range {p1 .. p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    .line 683
    const-string v13, "Msg Info Entries      : "

    #@14c
    move-object/from16 v0, p1

    #@14e
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    .line 685
    const-string v13, "\n"

    #@153
    move-object/from16 v0, p1

    #@155
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    .line 686
    move-object/from16 v0, p0

    #@15a
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mDebugMessageInfoEntryMap:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@15c
    if-eqz v13, :cond_1d2

    #@15e
    .line 687
    move-object/from16 v0, p0

    #@160
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mDebugMessageInfoEntryMap:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@162
    invoke-virtual {v13}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->clone()Ljava/lang/Object;

    #@165
    move-result-object v12

    #@166
    check-cast v12, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@168
    .line 689
    .local v12, mMessageInfoEntryMapClone:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;,"Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap<Ljava/lang/String;Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;>;"
    invoke-virtual {v12}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->entrySet()Ljava/util/Set;

    #@16b
    move-result-object v13

    #@16c
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@16f
    move-result-object v10

    #@170
    .line 691
    .local v10, mIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;>;>;"
    :cond_170
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@173
    move-result v13

    #@174
    if-eqz v13, :cond_1d2

    #@176
    .line 692
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@179
    move-result-object v4

    #@17a
    check-cast v4, Ljava/util/Map$Entry;

    #@17c
    .line 693
    .local v4, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;>;"
    invoke-virtual/range {p1 .. p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17f
    .line 694
    const-string v13, "    EventId, Path : "

    #@181
    move-object/from16 v0, p1

    #@183
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@186
    .line 695
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@189
    move-result-object v13

    #@18a
    check-cast v13, Ljava/lang/String;

    #@18c
    move-object/from16 v0, p1

    #@18e
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@191
    .line 696
    const-string v13, "\n"

    #@193
    move-object/from16 v0, p1

    #@195
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    .line 697
    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@19b
    move-result-object v13

    #@19c
    check-cast v13, Ljava/util/List;

    #@19e
    invoke-interface {v13}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@1a1
    move-result-object v8

    #@1a2
    .line 698
    .local v8, mEntryIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/map/MessageInfo;>;"
    if-eqz v8, :cond_170

    #@1a4
    .line 699
    :goto_1a4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    #@1a7
    move-result v13

    #@1a8
    if-eqz v13, :cond_170

    #@1aa
    .line 700
    const-string v13, "\n"

    #@1ac
    move-object/from16 v0, p1

    #@1ae
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b1
    .line 701
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1b4
    move-result-object v13

    #@1b5
    check-cast v13, Lcom/broadcom/bt/map/MessageInfo;

    #@1b7
    new-instance v14, Ljava/lang/StringBuilder;

    #@1b9
    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    #@1bc
    move-object/from16 v0, p2

    #@1be
    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c1
    move-result-object v14

    #@1c2
    const-string v15, "        "

    #@1c4
    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c7
    move-result-object v14

    #@1c8
    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1cb
    move-result-object v14

    #@1cc
    move-object/from16 v0, p1

    #@1ce
    invoke-virtual {v13, v0, v14}, Lcom/broadcom/bt/map/MessageInfo;->dumpState(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@1d1
    goto :goto_1a4

    #@1d2
    .line 706
    .end local v4           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;>;"
    .end local v8           #mEntryIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/map/MessageInfo;>;"
    .end local v10           #mIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;>;>;"
    .end local v12           #mMessageInfoEntryMapClone:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;,"Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap<Ljava/lang/String;Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;>;"
    :cond_1d2
    const-string v13, "\n"

    #@1d4
    move-object/from16 v0, p1

    #@1d6
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d9
    .line 707
    invoke-virtual/range {p1 .. p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1dc
    .line 708
    const-string v13, "Msg HANDLE => ID/Folder Map  : "

    #@1de
    move-object/from16 v0, p1

    #@1e0
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e3
    .line 709
    const-string v13, "\n"

    #@1e5
    move-object/from16 v0, p1

    #@1e7
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ea
    .line 710
    move-object/from16 v0, p0

    #@1ec
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@1ee
    invoke-virtual {v13}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->clone()Ljava/lang/Object;

    #@1f1
    move-result-object v11

    #@1f2
    check-cast v11, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@1f4
    .line 712
    .local v11, mMessageIdToHandleClone:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;,"Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap<Ljava/lang/Long;Ljava/lang/String;>;"
    invoke-virtual {v11}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->entrySet()Ljava/util/Set;

    #@1f7
    move-result-object v13

    #@1f8
    invoke-interface {v13}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@1fb
    move-result-object v7

    #@1fc
    .line 713
    .local v7, hIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/String;>;>;"
    :goto_1fc
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@1ff
    move-result v13

    #@200
    if-eqz v13, :cond_255

    #@202
    .line 714
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@205
    move-result-object v3

    #@206
    check-cast v3, Ljava/util/Map$Entry;

    #@208
    .line 715
    .local v3, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20b
    .line 716
    const-string v13, "    Message Handle : 0x"

    #@20d
    move-object/from16 v0, p1

    #@20f
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@212
    .line 717
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@215
    move-result-object v13

    #@216
    check-cast v13, Ljava/lang/Long;

    #@218
    invoke-virtual {v13}, Ljava/lang/Long;->longValue()J

    #@21b
    move-result-wide v13

    #@21c
    invoke-static {v13, v14}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@21f
    move-result-object v13

    #@220
    move-object/from16 v0, p1

    #@222
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@225
    .line 718
    const-string v13, "  =>  "

    #@227
    move-object/from16 v0, p1

    #@229
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22c
    .line 719
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@22f
    move-result-object v1

    #@230
    check-cast v1, Ljava/lang/String;

    #@232
    .line 720
    .local v1, dsMessageHandle:Ljava/lang/String;
    move-object/from16 v0, p1

    #@234
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@237
    .line 721
    const-string v13, "  :   "

    #@239
    move-object/from16 v0, p1

    #@23b
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23e
    .line 722
    move-object/from16 v0, p0

    #@240
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageFolderPath(Ljava/lang/String;)Ljava/lang/String;

    #@243
    move-result-object v13

    #@244
    invoke-static {v13}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@247
    move-result-object v13

    #@248
    move-object/from16 v0, p1

    #@24a
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24d
    .line 723
    const-string v13, "\n"

    #@24f
    move-object/from16 v0, p1

    #@251
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@254
    goto :goto_1fc

    #@255
    .line 725
    .end local v1           #dsMessageHandle:Ljava/lang/String;
    .end local v3           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/lang/String;>;"
    :cond_255
    return-void
.end method

.method public getAppendedPath(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "subPath"

    #@0
    .prologue
    .line 252
    const-string v2, "BtMap.MapClientSession"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "getAppendedPath(): subPath="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 254
    invoke-static {p1}, Lcom/broadcom/bt/service/map/MapClientSession;->normalizePath(Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 256
    .local v1, normalizedSubPath:Ljava/lang/String;
    const-string v2, "BtMap.MapClientSession"

    #@1e
    new-instance v3, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v4, "getPath(): normalized subPath="

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 258
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@37
    move-result v2

    #@38
    if-nez v2, :cond_57

    #@3a
    .line 260
    const-string v2, "BtMap.MapClientSession"

    #@3c
    new-instance v3, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v4, "getAppendedPath(): returning current path : "

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    iget-object v4, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mCurrentPath:Ljava/lang/String;

    #@49
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v3

    #@51
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 262
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mCurrentPath:Ljava/lang/String;

    #@56
    .line 269
    :goto_56
    return-object v0

    #@57
    .line 265
    :cond_57
    new-instance v2, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mCurrentPath:Ljava/lang/String;

    #@5e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v2

    #@62
    const-string v3, "/"

    #@64
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v2

    #@68
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v0

    #@70
    .line 267
    .local v0, appendedPath:Ljava/lang/String;
    const-string v2, "BtMap.MapClientSession"

    #@72
    new-instance v3, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v4, "getAppendedPath(): return path : "

    #@79
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v3

    #@7d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v3

    #@85
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    goto :goto_56
.end method

.method public getDatasourcePath(Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "virtualPath"

    #@0
    .prologue
    const/16 v4, 0x2f

    #@2
    .line 287
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    .line 290
    .local v1, pathLC:Ljava/lang/String;
    const-string v2, "telecom"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_21

    #@e
    .line 291
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "root/"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    .line 294
    :cond_21
    const-string v2, "root/telecom/msg"

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@26
    move-result v2

    #@27
    if-eqz v2, :cond_33

    #@29
    .line 296
    const-string v2, "BtMap.MapClientSession"

    #@2b
    const-string v3, "getDatasourcePath(): path is root/telecom. Setting normalized path to \'/\'"

    #@2d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 299
    const-string p1, "/"

    #@32
    .line 336
    .end local p1
    :cond_32
    :goto_32
    return-object p1

    #@33
    .line 303
    .restart local p1
    :cond_33
    const-string v2, "root/telecom/msg"

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_7e

    #@3b
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@3e
    move-result v2

    #@3f
    const-string v3, "root/telecom/msg"

    #@41
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@44
    move-result v3

    #@45
    add-int/lit8 v3, v3, 0x1

    #@47
    if-le v2, v3, :cond_7e

    #@49
    const-string v2, "root/telecom/msg"

    #@4b
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@4e
    move-result v2

    #@4f
    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    #@52
    move-result v2

    #@53
    if-ne v2, v4, :cond_7e

    #@55
    .line 306
    const-string v2, "root/telecom/msg"

    #@57
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@5a
    move-result v2

    #@5b
    add-int/lit8 v2, v2, 0x1

    #@5d
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@60
    move-result-object v0

    #@61
    .line 308
    .local v0, p:Ljava/lang/String;
    const-string v2, "BtMap.MapClientSession"

    #@63
    new-instance v3, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v4, "getDatasourcePath(): removing root/telecom from path...Normalized path = "

    #@6a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v3

    #@6e
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v3

    #@76
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    .line 311
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/MapClientSession;->getMappedPath(Ljava/lang/String;)Ljava/lang/String;

    #@7c
    move-result-object p1

    #@7d
    goto :goto_32

    #@7e
    .line 315
    .end local v0           #p:Ljava/lang/String;
    :cond_7e
    const-string v2, "root/telecom"

    #@80
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@83
    move-result v2

    #@84
    if-eqz v2, :cond_ca

    #@86
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@89
    move-result v2

    #@8a
    const-string v3, "root/telecom"

    #@8c
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@8f
    move-result v3

    #@90
    add-int/lit8 v3, v3, 0x1

    #@92
    if-le v2, v3, :cond_ca

    #@94
    const-string v2, "root/telecom"

    #@96
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@99
    move-result v2

    #@9a
    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    #@9d
    move-result v2

    #@9e
    if-ne v2, v4, :cond_ca

    #@a0
    .line 318
    const-string v2, "root/telecom"

    #@a2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@a5
    move-result v2

    #@a6
    add-int/lit8 v2, v2, 0x1

    #@a8
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@ab
    move-result-object v0

    #@ac
    .line 320
    .restart local v0       #p:Ljava/lang/String;
    const-string v2, "BtMap.MapClientSession"

    #@ae
    new-instance v3, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v4, "getDatasourcePath(): removing root/telecom from path...Normalized path = "

    #@b5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v3

    #@b9
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v3

    #@bd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v3

    #@c1
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c4
    .line 323
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/MapClientSession;->getMappedPath(Ljava/lang/String;)Ljava/lang/String;

    #@c7
    move-result-object p1

    #@c8
    goto/16 :goto_32

    #@ca
    .line 327
    .end local v0           #p:Ljava/lang/String;
    :cond_ca
    const-string v2, "root"

    #@cc
    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@cf
    move-result v2

    #@d0
    if-eqz v2, :cond_32

    #@d2
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@d5
    move-result v2

    #@d6
    const-string v3, "root"

    #@d8
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@db
    move-result v3

    #@dc
    add-int/lit8 v3, v3, 0x1

    #@de
    if-le v2, v3, :cond_32

    #@e0
    const-string v2, "root"

    #@e2
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@e5
    move-result v2

    #@e6
    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    #@e9
    move-result v2

    #@ea
    if-ne v2, v4, :cond_32

    #@ec
    .line 329
    const-string v2, "root"

    #@ee
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@f1
    move-result v2

    #@f2
    add-int/lit8 v2, v2, 0x1

    #@f4
    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@f7
    move-result-object v0

    #@f8
    .line 331
    .restart local v0       #p:Ljava/lang/String;
    const-string v2, "BtMap.MapClientSession"

    #@fa
    new-instance v3, Ljava/lang/StringBuilder;

    #@fc
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ff
    const-string v4, "getDatasourcePath(): removing root from path...Normalized path = "

    #@101
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v3

    #@105
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v3

    #@109
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v3

    #@10d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    .line 334
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/MapClientSession;->getMappedPath(Ljava/lang/String;)Ljava/lang/String;

    #@113
    move-result-object p1

    #@114
    goto/16 :goto_32
.end method

.method public getFolderEntry(ILjava/lang/String;J)Lcom/broadcom/bt/map/FolderInfo;
    .registers 10
    .parameter "eventId"
    .parameter "path"
    .parameter "entryId"

    #@0
    .prologue
    .line 442
    const-string v2, "BtMap.MapClientSession"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "getFolderEntry(): eventId="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, ", entryId="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 447
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mFolderEntryMap:Ljava/util/HashMap;

    #@24
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    check-cast v0, Ljava/util/List;

    #@2e
    .line 448
    .local v0, folderInfoList:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;"
    if-nez v0, :cond_62

    #@30
    const/4 v1, 0x0

    #@31
    .line 449
    .local v1, folderInfoListSize:I
    :goto_31
    if-eqz v1, :cond_3e

    #@33
    const-wide/16 v2, 0x0

    #@35
    cmp-long v2, p3, v2

    #@37
    if-ltz v2, :cond_3e

    #@39
    int-to-long v2, v1

    #@3a
    cmp-long v2, p3, v2

    #@3c
    if-ltz v2, :cond_67

    #@3e
    .line 451
    :cond_3e
    const-string v2, "BtMap.MapClientSession"

    #@40
    new-instance v3, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v4, "getFolderEntry(): no more entries...entryId="

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    const-string v4, ", folderInfoListSize="

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v3

    #@5d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 454
    const/4 v2, 0x0

    #@61
    .line 456
    :goto_61
    return-object v2

    #@62
    .line 448
    .end local v1           #folderInfoListSize:I
    :cond_62
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@65
    move-result v1

    #@66
    goto :goto_31

    #@67
    .line 456
    .restart local v1       #folderInfoListSize:I
    :cond_67
    long-to-int v2, p3

    #@68
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@6b
    move-result-object v2

    #@6c
    check-cast v2, Lcom/broadcom/bt/map/FolderInfo;

    #@6e
    goto :goto_61
.end method

.method public getMessageFolderPath(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "messageId"

    #@0
    .prologue
    .line 616
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToFolder:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@2
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/String;

    #@8
    return-object v0
.end method

.method public getMessageHandleFromId(Ljava/lang/String;)J
    .registers 5
    .parameter "messageId"

    #@0
    .prologue
    .line 604
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@2
    invoke-virtual {v1, p1}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/Long;

    #@8
    .line 605
    .local v0, mId:Ljava/lang/Long;
    if-nez v0, :cond_d

    #@a
    .line 606
    const-wide/16 v1, -0x1

    #@c
    .line 608
    :goto_c
    return-wide v1

    #@d
    :cond_d
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@10
    move-result-wide v1

    #@11
    goto :goto_c
.end method

.method public getMessageIdFromHandle(J)Ljava/lang/String;
    .registers 5
    .parameter "messageHandle"

    #@0
    .prologue
    .line 612
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@2
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/String;

    #@c
    return-object v0
.end method

.method public getMessageInfo(ILjava/lang/String;J)Lcom/broadcom/bt/map/MessageInfo;
    .registers 10
    .parameter "eventId"
    .parameter "path"
    .parameter "entryId"

    #@0
    .prologue
    .line 469
    const-string v2, "BtMap.MapClientSession"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "getMessageInfo(): eventId="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, ", entryId="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 475
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageInfoEntryMap:Ljava/util/HashMap;

    #@24
    invoke-virtual {v2, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    check-cast v0, Ljava/util/List;

    #@2a
    .line 476
    .local v0, msgList:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;"
    if-nez v0, :cond_5e

    #@2c
    const/4 v1, 0x0

    #@2d
    .line 477
    .local v1, msgListSize:I
    :goto_2d
    if-eqz v1, :cond_3a

    #@2f
    const-wide/16 v2, 0x0

    #@31
    cmp-long v2, p3, v2

    #@33
    if-ltz v2, :cond_3a

    #@35
    int-to-long v2, v1

    #@36
    cmp-long v2, p3, v2

    #@38
    if-ltz v2, :cond_63

    #@3a
    .line 479
    :cond_3a
    const-string v2, "BtMap.MapClientSession"

    #@3c
    new-instance v3, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v4, "getMessageInfo(): no more entries...entryId="

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    const-string v4, ", msgListSize="

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 482
    const/4 v2, 0x0

    #@5d
    .line 484
    :goto_5d
    return-object v2

    #@5e
    .line 476
    .end local v1           #msgListSize:I
    :cond_5e
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@61
    move-result v1

    #@62
    goto :goto_2d

    #@63
    .line 484
    .restart local v1       #msgListSize:I
    :cond_63
    long-to-int v2, p3

    #@64
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@67
    move-result-object v2

    #@68
    check-cast v2, Lcom/broadcom/bt/map/MessageInfo;

    #@6a
    goto :goto_5d
.end method

.method public declared-synchronized getNewMessageHandle()J
    .registers 7

    #@0
    .prologue
    .line 105
    monitor-enter p0

    #@1
    :try_start_1
    iget v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mSessionId:I

    #@3
    int-to-long v2, v2

    #@4
    const/16 v4, 0x38

    #@6
    shl-long v0, v2, v4

    #@8
    .line 106
    .local v0, newId:J
    iget-wide v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdCounter:J

    #@a
    const-wide/16 v4, 0x1

    #@c
    add-long/2addr v4, v2

    #@d
    iput-wide v4, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdCounter:J

    #@f
    add-long/2addr v0, v2

    #@10
    .line 108
    const-string v2, "BtMap.MapClientSession"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "getNewMessageHandle(): new m (hex) =0x"

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2c
    .catchall {:try_start_1 .. :try_end_2c} :catchall_2e

    #@2c
    .line 110
    monitor-exit p0

    #@2d
    return-wide v0

    #@2e
    .line 105
    .end local v0           #newId:J
    :catchall_2e
    move-exception v2

    #@2f
    monitor-exit p0

    #@30
    throw v2
.end method

.method public getPathOrReturnCurrent(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "path"

    #@0
    .prologue
    .line 230
    invoke-static {p1}, Lcom/broadcom/bt/service/map/MapClientSession;->normalizePath(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 232
    .local v0, normalizedPath:Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_27

    #@a
    .line 233
    const-string v1, "BtMap.MapClientSession"

    #@c
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "getPathOrReturnCurrent: path is null. Returning current path: "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mCurrentPath:Ljava/lang/String;

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 235
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mCurrentPath:Ljava/lang/String;

    #@26
    .line 240
    .end local v0           #normalizedPath:Ljava/lang/String;
    :goto_26
    return-object v0

    #@27
    .line 238
    .restart local v0       #normalizedPath:Ljava/lang/String;
    :cond_27
    const-string v1, "BtMap.MapClientSession"

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "getPathOrReturnCurrent(): normalized path="

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_26
.end method

.method public getVirtualPath(Ljava/lang/String;Z)Ljava/lang/String;
    .registers 5
    .parameter "datasourcePath"
    .parameter "includeRootDirectory"

    #@0
    .prologue
    .line 273
    if-eqz p2, :cond_1a

    #@2
    .line 274
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "root/telecom/msg/"

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/MapClientSession;->getReverseMappedPath(Ljava/lang/String;)Ljava/lang/String;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    .line 276
    :goto_19
    return-object v0

    #@1a
    :cond_1a
    new-instance v0, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v1, "telecom/msg/"

    #@21
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/MapClientSession;->getReverseMappedPath(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v0

    #@31
    goto :goto_19
.end method

.method public isNotificationEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 624
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mNotificationEnabled:Z

    #@2
    return v0
.end method

.method public loadFolderEntries(ILjava/lang/String;Ljava/util/List;)V
    .registers 6
    .parameter "eventId"
    .parameter "folderPath"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/FolderInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 405
    .local p3, folderInfoList:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;"
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mFolderEntryMap:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 406
    return-void
.end method

.method public loadMessageEntries(ILjava/lang/String;Ljava/util/List;)V
    .registers 7
    .parameter "eventId"
    .parameter "dsFolderPath"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 570
    .local p3, messageInfoList:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;"
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageInfoEntryMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 575
    if-eqz p3, :cond_23

    #@7
    invoke-interface {p3}, Ljava/util/List;->size()I

    #@a
    move-result v2

    #@b
    if-lez v2, :cond_23

    #@d
    .line 576
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v0

    #@11
    .line 577
    .local v0, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/map/MessageInfo;>;"
    :goto_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v2

    #@15
    if-eqz v2, :cond_23

    #@17
    .line 578
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v1

    #@1b
    check-cast v1, Lcom/broadcom/bt/map/MessageInfo;

    #@1d
    .line 579
    .local v1, m:Lcom/broadcom/bt/map/MessageInfo;
    iget-object v2, v1, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    #@1f
    invoke-virtual {p0, v2, p2}, Lcom/broadcom/bt/service/map/MapClientSession;->addNewMessageHandleToMap(Ljava/lang/String;Ljava/lang/String;)J

    #@22
    goto :goto_11

    #@23
    .line 582
    .end local v0           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/map/MessageInfo;>;"
    .end local v1           #m:Lcom/broadcom/bt/map/MessageInfo;
    :cond_23
    return-void
.end method

.method removeMessageIdMap(J)V
    .registers 6
    .parameter "messageHandle"

    #@0
    .prologue
    .line 495
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@2
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/String;

    #@c
    .line 496
    .local v0, messageId:Ljava/lang/String;
    if-eqz v0, :cond_13

    #@e
    .line 497
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@10
    invoke-virtual {v1, v0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    .line 499
    :cond_13
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToFolder:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@15
    invoke-virtual {v1, v0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    .line 500
    return-void
.end method

.method removeMessageIdMap(Ljava/lang/String;)V
    .registers 4
    .parameter "messageId"

    #@0
    .prologue
    .line 503
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@2
    invoke-virtual {v1, p1}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/lang/Long;

    #@8
    .line 504
    .local v0, messageHandle:Ljava/lang/Long;
    if-eqz v0, :cond_f

    #@a
    .line 505
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@c
    invoke-virtual {v1, v0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    .line 507
    :cond_f
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToFolder:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@11
    invoke-virtual {v1, p1}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    .line 508
    return-void
.end method

.method public setCurrentPath(Ljava/lang/String;)V
    .registers 5
    .parameter "path"

    #@0
    .prologue
    .line 216
    invoke-static {p1}, Lcom/broadcom/bt/service/map/MapClientSession;->normalizePath(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mCurrentPath:Ljava/lang/String;

    #@6
    .line 218
    const-string v0, "BtMap.MapClientSession"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "setCurrentPath(): current path set to: "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mCurrentPath:Ljava/lang/String;

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 220
    return-void
.end method

.method public setNotificationState(Z)V
    .registers 2
    .parameter "isEnabled"

    #@0
    .prologue
    .line 620
    iput-boolean p1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mNotificationEnabled:Z

    #@2
    .line 621
    return-void
.end method

.method public unloadFolderEntries(ILjava/lang/String;)V
    .registers 7
    .parameter "eventId"
    .parameter "folderPath"

    #@0
    .prologue
    .line 416
    const-string v1, "BtMap.MapClientSession"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "unloadFolderEntries(): requestId = "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 418
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mFolderEntryMap:Ljava/util/HashMap;

    #@1a
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    check-cast v0, Ljava/util/List;

    #@24
    .line 420
    .local v0, entries:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;"
    if-nez v0, :cond_3f

    #@26
    .line 421
    const-string v1, "BtMap.MapClientSession"

    #@28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "unloadFolderEntries(): no entries to remove for eventId = "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 431
    :goto_3e
    return-void

    #@3f
    .line 425
    :cond_3f
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mDebugFolderEntryMap:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@41
    new-instance v2, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    const-string v3, " : "

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v2

    #@58
    invoke-virtual {v1, v2, v0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5b
    goto :goto_3e
.end method

.method public unloadMessageEntries(ILjava/lang/String;)V
    .registers 7
    .parameter "eventId"
    .parameter "folderPath"

    #@0
    .prologue
    .line 587
    const-string v1, "BtMap.MapClientSession"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "unloadMessageEntries(): folderPath = "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 589
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageInfoEntryMap:Ljava/util/HashMap;

    #@1a
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Ljava/util/List;

    #@20
    .line 590
    .local v0, entries:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;"
    if-nez v0, :cond_3b

    #@22
    .line 591
    const-string v1, "BtMap.MapClientSession"

    #@24
    new-instance v2, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v3, "unloadMessageEntries(): no entries to remove for folderPath = "

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 601
    :goto_3a
    return-void

    #@3b
    .line 596
    :cond_3b
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mDebugMessageInfoEntryMap:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@3d
    new-instance v2, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    const-string v3, " : "

    #@48
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v2

    #@4c
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-virtual {v1, v2, v0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    goto :goto_3a
.end method

.method updateMessageHandleToMap(JLjava/lang/String;Ljava/lang/String;)V
    .registers 9
    .parameter "messageHandle"
    .parameter "messageId"
    .parameter "dsFolderPath"

    #@0
    .prologue
    .line 528
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@2
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v1, v2}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/String;

    #@c
    .line 529
    .local v0, oldMessageId:Ljava/lang/String;
    if-nez v0, :cond_27

    #@e
    .line 530
    const-string v1, "BtMap.MapClientSession"

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "updateMessageHandleToMap(): no messageId found for messageHandle "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 539
    :goto_26
    return-void

    #@27
    .line 534
    :cond_27
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@29
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v1, v2, p3}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    .line 535
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@32
    invoke-virtual {v1, v0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@35
    .line 536
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@37
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v1, p3, v2}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3e
    .line 537
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToFolder:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@40
    invoke-virtual {v1, v0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@43
    .line 538
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToFolder:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@45
    invoke-virtual {v1, p3, p4}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@48
    goto :goto_26
.end method

.method declared-synchronized updateMessageId(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "oldMessageId"
    .parameter "newMessageId"

    #@0
    .prologue
    .line 542
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@3
    invoke-virtual {v2, p1}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v1

    #@7
    check-cast v1, Ljava/lang/Long;

    #@9
    .line 543
    .local v1, mHandle:Ljava/lang/Long;
    if-nez v1, :cond_2f

    #@b
    .line 544
    const-string v2, "BtMap.MapClientSession"

    #@d
    new-instance v3, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v4, "Message ID "

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-static {p1}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, " not found"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2d
    .catchall {:try_start_1 .. :try_end_2d} :catchall_7c

    #@2d
    .line 559
    :goto_2d
    monitor-exit p0

    #@2e
    return-void

    #@2f
    .line 547
    :cond_2f
    :try_start_2f
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToFolder:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@31
    invoke-virtual {v2, p1}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@34
    move-result-object v0

    #@35
    check-cast v0, Ljava/lang/String;

    #@37
    .line 549
    .local v0, folderPath:Ljava/lang/String;
    const-string v2, "BtMap.MapClientSession"

    #@39
    new-instance v3, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v4, "[BTUI] ### updateMessageId : oMsgId = "

    #@40
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    const-string v4, " nMsgId = "

    #@4a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    const-string v4, " folderPath = "

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v3

    #@5c
    const-string v4, " mHandle = "

    #@5e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v3

    #@66
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v3

    #@6a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 551
    if-nez p2, :cond_7f

    #@6f
    .line 552
    const-string v2, "BtMap.MapClientSession"

    #@71
    const-string v3, "newMessageId is null...Removing messageId map..."

    #@73
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 553
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@78
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7b
    .catchall {:try_start_2f .. :try_end_7b} :catchall_7c

    #@7b
    goto :goto_2d

    #@7c
    .line 542
    .end local v0           #folderPath:Ljava/lang/String;
    .end local v1           #mHandle:Ljava/lang/Long;
    :catchall_7c
    move-exception v2

    #@7d
    monitor-exit p0

    #@7e
    throw v2

    #@7f
    .line 555
    .restart local v0       #folderPath:Ljava/lang/String;
    .restart local v1       #mHandle:Ljava/lang/Long;
    :cond_7f
    :try_start_7f
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToHandle:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@81
    invoke-virtual {v2, p2, v1}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@84
    .line 556
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageIdToFolder:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@86
    invoke-virtual {v2, p2, v0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@89
    .line 557
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapClientSession;->mMessageHandleToId:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;

    #@8b
    invoke-virtual {v2, v1, p2}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_8e
    .catchall {:try_start_7f .. :try_end_8e} :catchall_7c

    #@8e
    goto :goto_2d
.end method
