.class public Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
.super Ljava/lang/Object;
.source "BluetoothAVRCP.java"


# static fields
.field public static final ACTION_ON_AVRCP_CONNECTED:Ljava/lang/String; = "com.broadcom.bt.app.avrcp.action.ON_AVRCP_CONNECTED"

.field public static final ACTION_ON_AVRCP_DISCONNECTED:Ljava/lang/String; = "com.broadcom.bt.app.avrcp.action.ON_AVRCP_DISCONNECTED"

.field static final ACTION_PREFIX:Ljava/lang/String; = "com.broadcom.bt.app.avrcp.action."

#the value of this static final field might be set in the static constructor
.field static final ACTION_PREFIX_LENGTH:I = 0x0

.field public static final APP_SETTINGS_TIMER_EVENT:I = 0x2

.field public static final AVRCP_ACTION_APP_SETTING_REQUEST:[Ljava/lang/String; = null

.field public static final AVRCP_ACTION_META_CHANGED:[Ljava/lang/String; = null

.field public static final AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String; = null

.field public static final AVRCP_ACTION_PLAYSTATUS_REQUEST:[Ljava/lang/String; = null

.field public static final AVRCP_ACTION_QUEUE_CHANGED:[Ljava/lang/String; = null

.field public static final AVRCP_ACTION_REPEAT_CHANGED:[Ljava/lang/String; = null

.field public static final AVRC_SUPPORTED_APP_SETTINGS:[B = null

.field public static final BLUETOOTH_ADMIN_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH_ADMIN"

.field public static final BLUETOOTH_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH"

.field private static final DBG:Z = true

.field public static final DEFAULT_METADATA_NUM:Ljava/lang/String; = "0"

.field public static final DEFAULT_METADATA_STRING:Ljava/lang/String; = "Unknown"

.field public static final DEFAULT_TRACK_NUM:J = -0x1L

.field public static final GET_PLAYPOS_REQ_EVENT:I = 0x3

.field public static final IDX_AMAZON_MEDIA_PLAYER:I = 0x2

.field public static final IDX_ANDROID_MEDIA_PLAYER:I = 0x0

.field public static final IDX_DOUBLETWIST_MEDIA_PLAYER:I = 0x4

.field public static final IDX_GOOGLE_MEDIA_PLAYER:I = 0x1

.field public static final IDX_HTC_MEDIA_PLAYER:I = 0x3

.field public static final PLAYER_ATTR_EQUALIZER:B = 0x1t

.field public static final PLAYER_ATTR_REPEAT:B = 0x2t

.field public static final PLAYER_ATTR_SCAN:B = 0x4t

.field public static final PLAYER_ATTR_SHUFFLE:B = 0x3t

.field public static final PLAYPOS_TIMER_EVENT:I = 0x0

.field public static final PLAYSTATE_FWD_SEEK:I = 0x3

.field public static final PLAYSTATE_INVALID:I = 0xff

.field public static final PLAYSTATE_PAUSED:I = 0x2

.field public static final PLAYSTATE_PLAYING:I = 0x1

.field public static final PLAYSTATE_REV_SEEK:I = 0x4

.field public static final PLAYSTATE_STOPPED:I = 0x0

.field public static final PLAYSTATUS_REQUEST:Ljava/lang/String; = "com.android.music.playstatusrequest"

.field public static final PLAYSTATUS_RESPONSE:Ljava/lang/String; = "com.android.music.playstatusresponse"

.field public static final PLAYSTATUS_TIMER_EVENT:I = 0x1

.field public static final REPEAT_ALL:I = 0x3

.field public static final REPEAT_GROUP:I = 0x4

.field public static final REPEAT_OFF:I = 0x1

.field public static final REPEAT_SINGLE:I = 0x2

.field public static final SHUFFLE_GROUP:I = 0x3

.field public static final SHUFFLE_OFF:I = 0x1

.field public static final SHUFFLE_ON:I = 0x2

.field public static final SUPPORTED_REPEAT_ATTR_TXT:[Ljava/lang/String; = null

.field public static final SUPPORTED_REPEAT_ATTR_VAL:[I = null

.field public static final SUPPORTED_SHUFFLE_ATTR_TXT:[Ljava/lang/String; = null

.field public static final SUPPORTED_SHUFFLE_ATTR_VAL:[I = null

.field private static final TAG:Ljava/lang/String; = "BluetoothAVRCP"

.field private static mPlayerIndex:I


# instance fields
.field private intentExtras:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mAlbumName:Ljava/lang/String;

.field private mArtistName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mDuration:J

.field private mPlayStatus:I

.field private mPosition:J

.field private mRepeat:I

.field private mShuffle:I

.field private mTitle:Ljava/lang/String;

.field private mTotalTracks:J

.field private mTrackID:J

.field private mTrackNum:J


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v4, 0x0

    #@4
    const/4 v3, 0x2

    #@5
    .line 71
    const-string v0, "com.broadcom.bt.app.avrcp.action."

    #@7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@a
    move-result v0

    #@b
    sput v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->ACTION_PREFIX_LENGTH:I

    #@d
    .line 94
    sput v5, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@f
    .line 95
    const/4 v0, 0x5

    #@10
    new-array v0, v0, [Ljava/lang/String;

    #@12
    const-string v1, "com.android.music.playstatechanged"

    #@14
    aput-object v1, v0, v5

    #@16
    const-string v1, "com.android.music.playstatechanged"

    #@18
    aput-object v1, v0, v7

    #@1a
    const-string v1, "com.amazon.mp3.playstatechanged"

    #@1c
    aput-object v1, v0, v3

    #@1e
    const-string v1, "com.htc.music.playstatechanged"

    #@20
    aput-object v1, v0, v6

    #@22
    const/4 v1, 0x4

    #@23
    const-string v2, "com.doubleTwist.androidPlayer.playstatechanged"

    #@25
    aput-object v2, v0, v1

    #@27
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@29
    .line 103
    const/4 v0, 0x5

    #@2a
    new-array v0, v0, [Ljava/lang/String;

    #@2c
    const-string v1, "com.android.music.metachanged"

    #@2e
    aput-object v1, v0, v5

    #@30
    const-string v1, "com.android.music.metachanged"

    #@32
    aput-object v1, v0, v7

    #@34
    const-string v1, "com.amazon.mp3.metachanged"

    #@36
    aput-object v1, v0, v3

    #@38
    const-string v1, "com.htc.music.metachanged"

    #@3a
    aput-object v1, v0, v6

    #@3c
    const/4 v1, 0x4

    #@3d
    const-string v2, "com.doubleTwist.androidPlayer.metachanged"

    #@3f
    aput-object v2, v0, v1

    #@41
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@43
    .line 116
    const/4 v0, 0x5

    #@44
    new-array v0, v0, [Ljava/lang/String;

    #@46
    const-string v1, "com.android.music.settingchanged"

    #@48
    aput-object v1, v0, v5

    #@4a
    const-string v1, "com.android.music.settingchanged"

    #@4c
    aput-object v1, v0, v7

    #@4e
    const-string v1, "com.amazon.mp3.repeatstatechanged"

    #@50
    aput-object v1, v0, v3

    #@52
    aput-object v4, v0, v6

    #@54
    const/4 v1, 0x4

    #@55
    aput-object v4, v0, v1

    #@57
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_REPEAT_CHANGED:[Ljava/lang/String;

    #@59
    .line 123
    const/4 v0, 0x5

    #@5a
    new-array v0, v0, [Ljava/lang/String;

    #@5c
    const-string v1, "com.android.music.queuechanged"

    #@5e
    aput-object v1, v0, v5

    #@60
    const-string v1, "com.android.music.queuechanged"

    #@62
    aput-object v1, v0, v7

    #@64
    aput-object v4, v0, v3

    #@66
    aput-object v4, v0, v6

    #@68
    const/4 v1, 0x4

    #@69
    const-string v2, "com.doubleTwist.androidPlayer.queuechanged"

    #@6b
    aput-object v2, v0, v1

    #@6d
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_QUEUE_CHANGED:[Ljava/lang/String;

    #@6f
    .line 131
    const/4 v0, 0x5

    #@70
    new-array v0, v0, [Ljava/lang/String;

    #@72
    const-string v1, "com.android.music.playstaterequest"

    #@74
    aput-object v1, v0, v5

    #@76
    const-string v1, "com.android.music.playstaterequest"

    #@78
    aput-object v1, v0, v7

    #@7a
    aput-object v4, v0, v3

    #@7c
    aput-object v4, v0, v6

    #@7e
    const/4 v1, 0x4

    #@7f
    aput-object v4, v0, v1

    #@81
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATUS_REQUEST:[Ljava/lang/String;

    #@83
    .line 138
    const/4 v0, 0x5

    #@84
    new-array v0, v0, [Ljava/lang/String;

    #@86
    const-string v1, "com.android.music.settingrequest"

    #@88
    aput-object v1, v0, v5

    #@8a
    const-string v1, "com.android.music.settingrequest"

    #@8c
    aput-object v1, v0, v7

    #@8e
    aput-object v4, v0, v3

    #@90
    aput-object v4, v0, v6

    #@92
    const/4 v1, 0x4

    #@93
    aput-object v4, v0, v1

    #@95
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_APP_SETTING_REQUEST:[Ljava/lang/String;

    #@97
    .line 180
    new-array v0, v3, [B

    #@99
    fill-array-data v0, :array_d2

    #@9c
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRC_SUPPORTED_APP_SETTINGS:[B

    #@9e
    .line 185
    const/4 v0, 0x4

    #@9f
    new-array v0, v0, [Ljava/lang/String;

    #@a1
    const-string v1, "Repeat"

    #@a3
    aput-object v1, v0, v5

    #@a5
    const-string v1, "Off"

    #@a7
    aput-object v1, v0, v7

    #@a9
    const-string v1, "Single track"

    #@ab
    aput-object v1, v0, v3

    #@ad
    const-string v1, "All tracks"

    #@af
    aput-object v1, v0, v6

    #@b1
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->SUPPORTED_REPEAT_ATTR_TXT:[Ljava/lang/String;

    #@b3
    .line 192
    new-array v0, v6, [I

    #@b5
    fill-array-data v0, :array_d8

    #@b8
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->SUPPORTED_REPEAT_ATTR_VAL:[I

    #@ba
    .line 198
    new-array v0, v6, [Ljava/lang/String;

    #@bc
    const-string v1, "Shuffle"

    #@be
    aput-object v1, v0, v5

    #@c0
    const-string v1, "Off"

    #@c2
    aput-object v1, v0, v7

    #@c4
    const-string v1, "All tracks"

    #@c6
    aput-object v1, v0, v3

    #@c8
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->SUPPORTED_SHUFFLE_ATTR_TXT:[Ljava/lang/String;

    #@ca
    .line 204
    new-array v0, v3, [I

    #@cc
    fill-array-data v0, :array_e2

    #@cf
    sput-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->SUPPORTED_SHUFFLE_ATTR_VAL:[I

    #@d1
    return-void

    #@d2
    .line 180
    :array_d2
    .array-data 0x1
        0x2t
        0x3t
    .end array-data

    #@d7
    .line 192
    nop

    #@d8
    :array_d8
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@e2
    .line 204
    :array_e2
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 8
    .parameter "context"

    #@0
    .prologue
    const-wide/16 v4, -0x1

    #@2
    const/4 v3, 0x1

    #@3
    const-wide/16 v1, 0x0

    #@5
    .line 228
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 209
    const-string v0, "Unknown"

    #@a
    iput-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTitle:Ljava/lang/String;

    #@c
    .line 210
    const-string v0, "Unknown"

    #@e
    iput-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mArtistName:Ljava/lang/String;

    #@10
    .line 211
    const-string v0, "Unknown"

    #@12
    iput-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mAlbumName:Ljava/lang/String;

    #@14
    .line 212
    iput-wide v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackNum:J

    #@16
    .line 213
    iput-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTotalTracks:J

    #@18
    .line 214
    iput-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@1a
    .line 215
    const/4 v0, 0x0

    #@1b
    iput v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@1d
    .line 216
    iput-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPosition:J

    #@1f
    .line 218
    iput-wide v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackID:J

    #@21
    .line 221
    iput v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mRepeat:I

    #@23
    .line 222
    iput v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mShuffle:I

    #@25
    .line 225
    new-instance v0, Ljava/util/HashMap;

    #@27
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@2a
    iput-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@2c
    .line 229
    const-string v0, "BluetoothAVRCP"

    #@2e
    const-string v1, "BluetoothAVRCP()"

    #@30
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 230
    iput-object p1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mContext:Landroid/content/Context;

    #@35
    .line 231
    return-void
.end method

.method private findAction([Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "arr"
    .parameter "str"

    #@0
    .prologue
    .line 268
    const-string v3, "BluetoothAVRCP"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "findAction() - "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 269
    const/4 v1, 0x0

    #@19
    .line 270
    .local v1, result:Z
    const/4 v0, 0x0

    #@1a
    .local v0, i:I
    :goto_1a
    array-length v3, p1

    #@1b
    if-ge v0, v3, :cond_31

    #@1d
    .line 271
    aget-object v3, p1, v0

    #@1f
    if-eqz v3, :cond_2e

    #@21
    aget-object v3, p1, v0

    #@23
    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@26
    move-result v3

    #@27
    if-eqz v3, :cond_2e

    #@29
    .line 272
    sput v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@2b
    .line 273
    const/4 v1, 0x1

    #@2c
    move v2, v1

    #@2d
    .line 277
    .end local v1           #result:Z
    .local v2, result:I
    :goto_2d
    return v2

    #@2e
    .line 270
    .end local v2           #result:I
    .restart local v1       #result:Z
    :cond_2e
    add-int/lit8 v0, v0, 0x1

    #@30
    goto :goto_1a

    #@31
    :cond_31
    move v2, v1

    #@32
    .line 277
    .restart local v2       #result:I
    goto :goto_2d
.end method


# virtual methods
.method public checkPlayStateInIntent(Landroid/content/Intent;)Z
    .registers 7
    .parameter "intent"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 961
    const/4 v0, -0x1

    #@2
    .line 962
    .local v0, tmpPlayStatus:I
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayStateValue(Landroid/content/Intent;)I

    #@5
    move-result v0

    #@6
    .line 963
    const/16 v2, 0xff

    #@8
    if-ne v0, v2, :cond_12

    #@a
    .line 964
    const-string v2, "BluetoothAVRCP"

    #@c
    const-string v3, "Failed to extract PLAYSTATE from intent"

    #@e
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 972
    :goto_11
    return v1

    #@12
    .line 967
    :cond_12
    iget v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@14
    if-ne v0, v2, :cond_31

    #@16
    .line 968
    const-string v2, "BluetoothAVRCP"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "Playstate hasnt changed yet : "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    iget v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_11

    #@31
    .line 971
    :cond_31
    const-string v1, "BluetoothAVRCP"

    #@33
    new-instance v2, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v3, "Play State changed from "

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    iget v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    const-string v3, " to "

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 972
    const/4 v1, 0x1

    #@56
    goto :goto_11
.end method

.method public cleanup()V
    .registers 3

    #@0
    .prologue
    .line 234
    const-string v0, "BluetoothAVRCP"

    #@2
    const-string v1, "cleanup()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 235
    const/4 v0, 0x0

    #@8
    iput-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mContext:Landroid/content/Context;

    #@a
    .line 236
    iget-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@c
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@f
    .line 237
    return-void
.end method

.method convertToAvrcpPlayState(II)I
    .registers 7
    .parameter "index"
    .parameter "playstate"

    #@0
    .prologue
    const/4 v1, 0x2

    #@1
    const/4 v0, 0x0

    #@2
    .line 941
    if-eq p1, v1, :cond_1d

    #@4
    .line 942
    const-string v1, "BluetoothAVRCP"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Unsupported operation for player index :"

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 955
    :goto_1c
    :pswitch_1c
    return v0

    #@1d
    .line 946
    :cond_1d
    packed-switch p2, :pswitch_data_3e

    #@20
    .line 954
    const-string v1, "BluetoothAVRCP"

    #@22
    new-instance v2, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v3, "Unknown playstate :"

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_1c

    #@39
    :pswitch_39
    move v0, v1

    #@3a
    .line 948
    goto :goto_1c

    #@3b
    .line 952
    :pswitch_3b
    const/4 v0, 0x1

    #@3c
    goto :goto_1c

    #@3d
    .line 946
    nop

    #@3e
    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_39
        :pswitch_1c
        :pswitch_3b
    .end packed-switch
.end method

.method dumpExtras(Landroid/content/Intent;)V
    .registers 8
    .parameter "intent"

    #@0
    .prologue
    .line 281
    const-string v3, "BluetoothAVRCP"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "In dumpExtras - Received intent - "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v4

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 282
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@1f
    move-result-object v0

    #@20
    .line 283
    .local v0, extras:Landroid/os/Bundle;
    if-eqz v0, :cond_54

    #@22
    .line 285
    const-string v3, "BluetoothAVRCP"

    #@24
    new-instance v4, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v5, "dumpExtras() :"

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 287
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    #@3d
    move-result-object v2

    #@3e
    .line 288
    .local v2, ks:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@41
    move-result-object v1

    #@42
    .line 289
    .local v1, iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_42
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@45
    move-result v3

    #@46
    if-eqz v3, :cond_54

    #@48
    .line 290
    const-string v4, "BluetoothAVRCP - KEY"

    #@4a
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@4d
    move-result-object v3

    #@4e
    check-cast v3, Ljava/lang/String;

    #@50
    invoke-static {v4, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    goto :goto_42

    #@54
    .line 293
    .end local v1           #iterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    .end local v2           #ks:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    :cond_54
    return-void
.end method

.method public fillIntentExtras(Landroid/content/Intent;)V
    .registers 11
    .parameter "intent"

    #@0
    .prologue
    const/4 v8, 0x4

    #@1
    const/4 v7, 0x3

    #@2
    const/4 v6, 0x2

    #@3
    const/4 v5, 0x1

    #@4
    const/4 v4, 0x0

    #@5
    .line 570
    const-string v1, "BluetoothAVRCP"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "fillIntentExtras() "

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 572
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    .line 573
    .local v0, action:Ljava/lang/String;
    const-string v1, "BluetoothAVRCP"

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "fillIntentExtras - received action - "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 574
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@3b
    aget-object v1, v1, v4

    #@3d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v1

    #@41
    if-nez v1, :cond_61

    #@43
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@45
    aget-object v1, v1, v5

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v1

    #@4b
    if-nez v1, :cond_61

    #@4d
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@4f
    aget-object v1, v1, v4

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v1

    #@55
    if-nez v1, :cond_61

    #@57
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@59
    aget-object v1, v1, v5

    #@5b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5e
    move-result v1

    #@5f
    if-eqz v1, :cond_db

    #@61
    .line 579
    :cond_61
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@63
    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    #@66
    .line 581
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@68
    const-string v2, "Title"

    #@6a
    const-string v3, "track"

    #@6c
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6f
    .line 582
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@71
    const-string v2, "Artist"

    #@73
    const-string v3, "artist"

    #@75
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@78
    .line 583
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@7a
    const-string v2, "Album"

    #@7c
    const-string v3, "album"

    #@7e
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@81
    .line 584
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@83
    const-string v2, "TrackNum"

    #@85
    const-string v3, "tracknum"

    #@87
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@8a
    .line 585
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@8c
    const-string v2, "TotalTracks"

    #@8e
    const-string v3, "numberoftracks"

    #@90
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@93
    .line 586
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@95
    const-string v2, "Duration"

    #@97
    const-string v3, "duration"

    #@99
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@9c
    .line 587
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@9e
    const-string v2, "Position"

    #@a0
    const-string v3, "position"

    #@a2
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@a5
    .line 589
    const-string v1, "playing"

    #@a7
    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@aa
    move-result v1

    #@ab
    if-eqz v1, :cond_c9

    #@ad
    .line 590
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@af
    const-string v2, "PlayState"

    #@b1
    const-string v3, "playing"

    #@b3
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@b6
    .line 595
    :cond_b6
    :goto_b6
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@b8
    const-string v2, "Repeat"

    #@ba
    const-string v3, "repeat"

    #@bc
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@bf
    .line 596
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@c1
    const-string v2, "Shuffle"

    #@c3
    const-string v3, "shuffle"

    #@c5
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c8
    .line 649
    :cond_c8
    :goto_c8
    return-void

    #@c9
    .line 592
    :cond_c9
    const-string v1, "playstate"

    #@cb
    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@ce
    move-result v1

    #@cf
    if-eqz v1, :cond_b6

    #@d1
    .line 593
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@d3
    const-string v2, "PlayState"

    #@d5
    const-string v3, "playstate"

    #@d7
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@da
    goto :goto_b6

    #@db
    .line 598
    :cond_db
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@dd
    aget-object v1, v1, v6

    #@df
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e2
    move-result v1

    #@e3
    if-nez v1, :cond_ef

    #@e5
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@e7
    aget-object v1, v1, v6

    #@e9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ec
    move-result v1

    #@ed
    if-eqz v1, :cond_150

    #@ef
    .line 601
    :cond_ef
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@f1
    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    #@f4
    .line 603
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@f6
    const-string v2, "Title"

    #@f8
    const-string v3, "com.amazon.mp3.track"

    #@fa
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@fd
    .line 604
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@ff
    const-string v2, "Artist"

    #@101
    const-string v3, "com.amazon.mp3.artist"

    #@103
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@106
    .line 605
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@108
    const-string v2, "Album"

    #@10a
    const-string v3, "com.amazon.mp3.album"

    #@10c
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@10f
    .line 606
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@111
    const-string v2, "TrackNum"

    #@113
    const-string v3, "com.amazon.mp3.id"

    #@115
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@118
    .line 607
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@11a
    const-string v2, "TotalTracks"

    #@11c
    const-string v3, "track_count_int_key"

    #@11e
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@121
    .line 608
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@123
    const-string v2, "Duration"

    #@125
    const-string v3, "duration"

    #@127
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@12a
    .line 609
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@12c
    const-string v2, "Position"

    #@12e
    const-string v3, "track_position_int_key"

    #@130
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@133
    .line 610
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@135
    const-string v2, "PlayState"

    #@137
    const-string v3, "com.amazon.mp3.playstate"

    #@139
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@13c
    .line 611
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@13e
    const-string v2, "Repeat"

    #@140
    const-string v3, "repeat_state_key"

    #@142
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@145
    .line 612
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@147
    const-string v2, "Shuffle"

    #@149
    const-string v3, "shuffle_enabled_key"

    #@14b
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@14e
    goto/16 :goto_c8

    #@150
    .line 614
    :cond_150
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@152
    aget-object v1, v1, v7

    #@154
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@157
    move-result v1

    #@158
    if-nez v1, :cond_164

    #@15a
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@15c
    aget-object v1, v1, v7

    #@15e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@161
    move-result v1

    #@162
    if-eqz v1, :cond_1c5

    #@164
    .line 617
    :cond_164
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@166
    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    #@169
    .line 619
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@16b
    const-string v2, "Title"

    #@16d
    const-string v3, "track"

    #@16f
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@172
    .line 620
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@174
    const-string v2, "Artist"

    #@176
    const-string v3, "artist"

    #@178
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@17b
    .line 621
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@17d
    const-string v2, "Album"

    #@17f
    const-string v3, "album"

    #@181
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@184
    .line 622
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@186
    const-string v2, "TrackNum"

    #@188
    const-string v3, "id"

    #@18a
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18d
    .line 623
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@18f
    const-string v2, "TotalTracks"

    #@191
    const-string v3, "totaltracks"

    #@193
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@196
    .line 624
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@198
    const-string v2, "Duration"

    #@19a
    const-string v3, "duration"

    #@19c
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@19f
    .line 625
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1a1
    const-string v2, "Position"

    #@1a3
    const-string v3, "position"

    #@1a5
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a8
    .line 626
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1aa
    const-string v2, "PlayState"

    #@1ac
    const-string v3, "isplaying"

    #@1ae
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1b1
    .line 627
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1b3
    const-string v2, "Repeat"

    #@1b5
    const-string v3, "repeat"

    #@1b7
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1ba
    .line 628
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1bc
    const-string v2, "Shuffle"

    #@1be
    const-string v3, "shuffle"

    #@1c0
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1c3
    goto/16 :goto_c8

    #@1c5
    .line 630
    :cond_1c5
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@1c7
    aget-object v1, v1, v8

    #@1c9
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1cc
    move-result v1

    #@1cd
    if-nez v1, :cond_1d9

    #@1cf
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@1d1
    aget-object v1, v1, v8

    #@1d3
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d6
    move-result v1

    #@1d7
    if-eqz v1, :cond_c8

    #@1d9
    .line 633
    :cond_1d9
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1db
    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    #@1de
    .line 635
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1e0
    const-string v2, "Title"

    #@1e2
    const-string v3, "track"

    #@1e4
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1e7
    .line 636
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1e9
    const-string v2, "Artist"

    #@1eb
    const-string v3, "artist"

    #@1ed
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f0
    .line 637
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1f2
    const-string v2, "Album"

    #@1f4
    const-string v3, "album"

    #@1f6
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f9
    .line 638
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1fb
    const-string v2, "TrackNum"

    #@1fd
    const-string v3, "song_id"

    #@1ff
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@202
    .line 640
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@204
    const-string v2, "TotalTracks"

    #@206
    const-string v3, "totaltracks"

    #@208
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20b
    .line 642
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@20d
    const-string v2, "Duration"

    #@20f
    const-string v3, "duration"

    #@211
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@214
    .line 644
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@216
    const-string v2, "Position"

    #@218
    const-string v3, "position"

    #@21a
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@21d
    .line 645
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@21f
    const-string v2, "PlayState"

    #@221
    const-string v3, "playing"

    #@223
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@226
    .line 646
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@228
    const-string v2, "Repeat"

    #@22a
    const-string v3, "repeat"

    #@22c
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@22f
    .line 647
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@231
    const-string v2, "Shuffle"

    #@233
    const-string v3, "shuffle"

    #@235
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@238
    goto/16 :goto_c8
.end method

.method public fillIntentFilter()Landroid/content/IntentFilter;
    .registers 5

    #@0
    .prologue
    .line 296
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 298
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v2, "BluetoothAVRCP"

    #@7
    const-string v3, "fillIntentFilter()"

    #@9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 301
    invoke-virtual {p0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->resetMetadata()V

    #@f
    .line 305
    const/4 v1, 0x0

    #@10
    .local v1, i:I
    :goto_10
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@12
    array-length v2, v2

    #@13
    if-ge v1, v2, :cond_2f

    #@15
    .line 306
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@17
    aget-object v2, v2, v1

    #@19
    if-eqz v2, :cond_2c

    #@1b
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@1d
    aget-object v2, v2, v1

    #@1f
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->hasAction(Ljava/lang/String;)Z

    #@22
    move-result v2

    #@23
    if-nez v2, :cond_2c

    #@25
    .line 308
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@27
    aget-object v2, v2, v1

    #@29
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@2c
    .line 305
    :cond_2c
    add-int/lit8 v1, v1, 0x1

    #@2e
    goto :goto_10

    #@2f
    .line 312
    :cond_2f
    const/4 v1, 0x0

    #@30
    :goto_30
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@32
    array-length v2, v2

    #@33
    if-ge v1, v2, :cond_4f

    #@35
    .line 313
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@37
    aget-object v2, v2, v1

    #@39
    if-eqz v2, :cond_4c

    #@3b
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@3d
    aget-object v2, v2, v1

    #@3f
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->hasAction(Ljava/lang/String;)Z

    #@42
    move-result v2

    #@43
    if-nez v2, :cond_4c

    #@45
    .line 315
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@47
    aget-object v2, v2, v1

    #@49
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@4c
    .line 312
    :cond_4c
    add-int/lit8 v1, v1, 0x1

    #@4e
    goto :goto_30

    #@4f
    .line 319
    :cond_4f
    const/4 v1, 0x0

    #@50
    :goto_50
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_REPEAT_CHANGED:[Ljava/lang/String;

    #@52
    array-length v2, v2

    #@53
    if-ge v1, v2, :cond_6f

    #@55
    .line 320
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_REPEAT_CHANGED:[Ljava/lang/String;

    #@57
    aget-object v2, v2, v1

    #@59
    if-eqz v2, :cond_6c

    #@5b
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_REPEAT_CHANGED:[Ljava/lang/String;

    #@5d
    aget-object v2, v2, v1

    #@5f
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->hasAction(Ljava/lang/String;)Z

    #@62
    move-result v2

    #@63
    if-nez v2, :cond_6c

    #@65
    .line 322
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_REPEAT_CHANGED:[Ljava/lang/String;

    #@67
    aget-object v2, v2, v1

    #@69
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@6c
    .line 319
    :cond_6c
    add-int/lit8 v1, v1, 0x1

    #@6e
    goto :goto_50

    #@6f
    .line 326
    :cond_6f
    const/4 v1, 0x0

    #@70
    :goto_70
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_QUEUE_CHANGED:[Ljava/lang/String;

    #@72
    array-length v2, v2

    #@73
    if-ge v1, v2, :cond_8f

    #@75
    .line 327
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_QUEUE_CHANGED:[Ljava/lang/String;

    #@77
    aget-object v2, v2, v1

    #@79
    if-eqz v2, :cond_8c

    #@7b
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_QUEUE_CHANGED:[Ljava/lang/String;

    #@7d
    aget-object v2, v2, v1

    #@7f
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->hasAction(Ljava/lang/String;)Z

    #@82
    move-result v2

    #@83
    if-nez v2, :cond_8c

    #@85
    .line 329
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_QUEUE_CHANGED:[Ljava/lang/String;

    #@87
    aget-object v2, v2, v1

    #@89
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@8c
    .line 326
    :cond_8c
    add-int/lit8 v1, v1, 0x1

    #@8e
    goto :goto_70

    #@8f
    .line 333
    :cond_8f
    const-string v2, "com.android.music.playstatusresponse"

    #@91
    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@94
    .line 335
    return-object v0
.end method

.method public findMetaChangedAction(Ljava/lang/String;)Z
    .registers 5
    .parameter "str"

    #@0
    .prologue
    .line 241
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "findMetaChangedAction()-"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 243
    sget-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_META_CHANGED:[Ljava/lang/String;

    #@1a
    invoke-direct {p0, v0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->findAction([Ljava/lang/String;Ljava/lang/String;)Z

    #@1d
    move-result v0

    #@1e
    return v0
.end method

.method public findPlayStateChangedAction(Ljava/lang/String;)Z
    .registers 5
    .parameter "str"

    #@0
    .prologue
    .line 248
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "findPlayStateChangedAction()-"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 250
    sget-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@1a
    invoke-direct {p0, v0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->findAction([Ljava/lang/String;Ljava/lang/String;)Z

    #@1d
    move-result v0

    #@1e
    return v0
.end method

.method public findQueueChangedAction(Ljava/lang/String;)Z
    .registers 5
    .parameter "str"

    #@0
    .prologue
    .line 262
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "findQueueChangedAction()-"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 264
    sget-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_QUEUE_CHANGED:[Ljava/lang/String;

    #@1a
    invoke-direct {p0, v0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->findAction([Ljava/lang/String;Ljava/lang/String;)Z

    #@1d
    move-result v0

    #@1e
    return v0
.end method

.method public findRepeatOrShuffleChangedAction(Ljava/lang/String;)Z
    .registers 5
    .parameter "str"

    #@0
    .prologue
    .line 255
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "findRepeatChangedAction()-"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 257
    sget-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_REPEAT_CHANGED:[Ljava/lang/String;

    #@1a
    invoke-direct {p0, v0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->findAction([Ljava/lang/String;Ljava/lang/String;)Z

    #@1d
    move-result v0

    #@1e
    return v0
.end method

.method public getAlbumName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 354
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getAlbumName()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mAlbumName:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 356
    iget-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mAlbumName:Ljava/lang/String;

    #@1c
    return-object v0
.end method

.method public getArtistName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 347
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getArtistName()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mArtistName:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 349
    iget-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mArtistName:Ljava/lang/String;

    #@1c
    return-object v0
.end method

.method public getDuration()J
    .registers 5

    #@0
    .prologue
    .line 382
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getDuration()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-wide v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 384
    iget-wide v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@1c
    return-wide v0
.end method

.method public getDurationText()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 375
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getDurationText()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-wide v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 377
    iget-wide v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@1c
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    return-object v0
.end method

.method public getPlayPosition()J
    .registers 5

    #@0
    .prologue
    .line 411
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getPlayPosition()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-wide v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPosition:J

    #@f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 413
    iget-wide v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPosition:J

    #@1c
    return-wide v0
.end method

.method public getPlayStateValue(Landroid/content/Intent;)I
    .registers 16
    .parameter "intent"

    #@0
    .prologue
    const-wide/16 v12, 0x0

    #@2
    const/16 v9, 0xff

    #@4
    .line 657
    const-string v3, ""

    #@6
    .line 658
    .local v3, strPlayStatus:Ljava/lang/String;
    const/4 v4, -0x1

    #@7
    .line 659
    .local v4, tmpPlayStatus:I
    const/4 v7, 0x0

    #@8
    .line 660
    .local v7, tmp_bPlayStatus:Z
    const/4 v1, 0x0

    #@9
    .line 663
    .local v1, is_playstatus_string:Z
    const-string v8, "BluetoothAVRCP"

    #@b
    new-instance v10, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v11, "getPlayStateValue() : "

    #@12
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v10

    #@16
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v10

    #@1a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v10

    #@1e
    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 665
    iget-object v8, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@23
    const-string v10, "PlayState"

    #@25
    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@28
    move-result-object v8

    #@29
    check-cast v8, Ljava/lang/String;

    #@2b
    invoke-virtual {p1, v8}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@2e
    move-result v8

    #@2f
    if-eqz v8, :cond_154

    #@31
    .line 667
    :try_start_31
    iget-object v8, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@33
    const-string v10, "PlayState"

    #@35
    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@38
    move-result-object v8

    #@39
    check-cast v8, Ljava/lang/String;

    #@3b
    invoke-virtual {p1, v8}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@3e
    move-result-object v2

    #@3f
    .line 669
    .local v2, obj_playstatus:Ljava/lang/Object;
    if-nez v2, :cond_4a

    #@41
    .line 670
    const-string v8, "BluetoothAVRCP"

    #@43
    const-string v10, "Playstate is null in intent"

    #@45
    invoke-static {v8, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    move v8, v9

    #@49
    .line 755
    .end local v2           #obj_playstatus:Ljava/lang/Object;
    :goto_49
    return v8

    #@4a
    .line 672
    .restart local v2       #obj_playstatus:Ljava/lang/Object;
    :cond_4a
    instance-of v8, v2, Ljava/lang/String;

    #@4c
    if-eqz v8, :cond_7f

    #@4e
    .line 674
    const-string v8, "BluetoothAVRCP"

    #@50
    const-string v10, "Play Status is String"

    #@52
    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_55
    .catch Ljava/lang/Exception; {:try_start_31 .. :try_end_55} :catch_81

    #@55
    .line 676
    const/4 v1, 0x1

    #@56
    .line 684
    :goto_56
    const/4 v8, 0x1

    #@57
    if-ne v1, v8, :cond_e8

    #@59
    move-object v3, v2

    #@5a
    .line 685
    check-cast v3, Ljava/lang/String;

    #@5c
    .line 687
    const-string v8, "BluetoothAVRCP"

    #@5e
    new-instance v10, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v11, " *** state:"

    #@65
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v10

    #@69
    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v10

    #@6d
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v10

    #@71
    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 689
    if-nez v3, :cond_8b

    #@76
    .line 690
    const-string v8, "BluetoothAVRCP"

    #@78
    const-string v10, "playstate is null in checkPlayStateInIntent()"

    #@7a
    invoke-static {v8, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    move v8, v9

    #@7e
    .line 691
    goto :goto_49

    #@7f
    .line 678
    :cond_7f
    const/4 v1, 0x0

    #@80
    goto :goto_56

    #@81
    .line 680
    .end local v2           #obj_playstatus:Ljava/lang/Object;
    :catch_81
    move-exception v0

    #@82
    .line 681
    .local v0, ee:Ljava/lang/Exception;
    const-string v8, "BluetoothAVRCP"

    #@84
    const-string v10, "Exception while parsing playstate"

    #@86
    invoke-static {v8, v10, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@89
    move v8, v9

    #@8a
    .line 682
    goto :goto_49

    #@8b
    .line 693
    .end local v0           #ee:Ljava/lang/Exception;
    .restart local v2       #obj_playstatus:Ljava/lang/Object;
    :cond_8b
    const-string v8, "playing"

    #@8d
    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@90
    move-result v8

    #@91
    if-eqz v8, :cond_96

    #@93
    .line 695
    const/4 v4, 0x1

    #@94
    .end local v2           #obj_playstatus:Ljava/lang/Object;
    :goto_94
    move v8, v4

    #@95
    .line 755
    goto :goto_49

    #@96
    .line 697
    .restart local v2       #obj_playstatus:Ljava/lang/Object;
    :cond_96
    const-string v8, "stopped"

    #@98
    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9b
    move-result v8

    #@9c
    if-eqz v8, :cond_a0

    #@9e
    .line 699
    const/4 v4, 0x0

    #@9f
    goto :goto_94

    #@a0
    .line 701
    :cond_a0
    const-string v8, "paused"

    #@a2
    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a5
    move-result v8

    #@a6
    if-eqz v8, :cond_cc

    #@a8
    .line 703
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->updatePositionFromIntent(Landroid/content/Intent;)J

    #@ab
    move-result-wide v5

    #@ac
    .line 705
    .local v5, tmpPosition:J
    const-string v8, "BluetoothAVRCP"

    #@ae
    new-instance v9, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v10, "*** tmpPosition:"

    #@b5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v9

    #@b9
    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v9

    #@bd
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v9

    #@c1
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c4
    .line 707
    cmp-long v8, v5, v12

    #@c6
    if-nez v8, :cond_ca

    #@c8
    .line 708
    const/4 v4, 0x0

    #@c9
    goto :goto_94

    #@ca
    .line 711
    :cond_ca
    const/4 v4, 0x2

    #@cb
    goto :goto_94

    #@cc
    .line 714
    .end local v5           #tmpPosition:J
    :cond_cc
    const-string v8, "forwarding"

    #@ce
    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d1
    move-result v8

    #@d2
    if-eqz v8, :cond_d6

    #@d4
    .line 716
    const/4 v4, 0x3

    #@d5
    goto :goto_94

    #@d6
    .line 718
    :cond_d6
    const-string v8, "rewinding"

    #@d8
    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@db
    move-result v8

    #@dc
    if-eqz v8, :cond_e0

    #@de
    .line 720
    const/4 v4, 0x4

    #@df
    goto :goto_94

    #@e0
    .line 724
    :cond_e0
    const-string v8, "BluetoothAVRCP"

    #@e2
    const-string v9, "play status not defined ??!!"

    #@e4
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e7
    goto :goto_94

    #@e8
    .line 727
    :cond_e8
    instance-of v8, v2, Ljava/lang/Boolean;

    #@ea
    if-eqz v8, :cond_137

    #@ec
    .line 728
    check-cast v2, Ljava/lang/Boolean;

    #@ee
    .end local v2           #obj_playstatus:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    #@f1
    move-result v7

    #@f2
    .line 730
    const-string v8, "BluetoothAVRCP"

    #@f4
    new-instance v9, Ljava/lang/StringBuilder;

    #@f6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@f9
    const-string v10, " @@@@ state:"

    #@fb
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v9

    #@ff
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@102
    move-result-object v9

    #@103
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v9

    #@107
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    .line 732
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->updatePositionFromIntent(Landroid/content/Intent;)J

    #@10d
    move-result-wide v5

    #@10e
    .line 734
    .restart local v5       #tmpPosition:J
    const-string v8, "BluetoothAVRCP"

    #@110
    new-instance v9, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v10, "@@@@ tmpPosition:"

    #@117
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v9

    #@11b
    invoke-virtual {v9, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v9

    #@11f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@122
    move-result-object v9

    #@123
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@126
    .line 736
    if-nez v7, :cond_12f

    #@128
    cmp-long v8, v5, v12

    #@12a
    if-nez v8, :cond_12f

    #@12c
    .line 737
    const/4 v4, 0x0

    #@12d
    goto/16 :goto_94

    #@12f
    .line 738
    :cond_12f
    if-nez v7, :cond_134

    #@131
    .line 739
    const/4 v4, 0x2

    #@132
    goto/16 :goto_94

    #@134
    .line 741
    :cond_134
    const/4 v4, 0x1

    #@135
    goto/16 :goto_94

    #@137
    .line 743
    .end local v5           #tmpPosition:J
    .restart local v2       #obj_playstatus:Ljava/lang/Object;
    :cond_137
    instance-of v8, v2, Ljava/lang/Integer;

    #@139
    if-eqz v8, :cond_149

    #@13b
    .line 744
    check-cast v2, Ljava/lang/Integer;

    #@13d
    .end local v2           #obj_playstatus:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@140
    move-result v4

    #@141
    .line 745
    sget v8, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@143
    invoke-virtual {p0, v8, v4}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->convertToAvrcpPlayState(II)I

    #@146
    move-result v4

    #@147
    goto/16 :goto_94

    #@149
    .line 747
    .restart local v2       #obj_playstatus:Ljava/lang/Object;
    :cond_149
    const-string v8, "BluetoothAVRCP"

    #@14b
    const-string v9, "PlayState present in unsupported datatype"

    #@14d
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@150
    .line 748
    const/16 v4, 0xff

    #@152
    goto/16 :goto_94

    #@154
    .line 752
    .end local v2           #obj_playstatus:Ljava/lang/Object;
    :cond_154
    const-string v8, "BluetoothAVRCP"

    #@156
    const-string v10, "No playstate TAG present in intent"

    #@158
    invoke-static {v8, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15b
    move v8, v9

    #@15c
    .line 753
    goto/16 :goto_49
.end method

.method public getPlayStatus()I
    .registers 4

    #@0
    .prologue
    .line 389
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getPlayStatus()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 391
    iget v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@1c
    return v0
.end method

.method public getPlayerIndex()I
    .registers 4

    #@0
    .prologue
    .line 480
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getPlayerIndex()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 482
    sget v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@1c
    return v0
.end method

.method public getRepeatMode()I
    .registers 4

    #@0
    .prologue
    .line 424
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getRepeatMode()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mRepeat:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 426
    iget v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mRepeat:I

    #@1c
    return v0
.end method

.method public getShuffleMode()I
    .registers 4

    #@0
    .prologue
    .line 452
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getShuffleMode()-"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mShuffle:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 454
    iget v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mShuffle:I

    #@1c
    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 340
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getTitle()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTitle:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 342
    iget-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTitle:Ljava/lang/String;

    #@1c
    return-object v0
.end method

.method public getTotalTracks()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 368
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getTotalTracks()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-wide v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTotalTracks:J

    #@f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 370
    iget-wide v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTotalTracks:J

    #@1c
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    return-object v0
.end method

.method public getTrackNum()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 361
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getTrackNum()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-wide v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackNum:J

    #@f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 363
    iget-wide v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackNum:J

    #@1c
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    return-object v0
.end method

.method public getTrackNumLong()J
    .registers 5

    #@0
    .prologue
    .line 418
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getTrackNumLong()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-wide v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackNum:J

    #@f
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 420
    iget-wide v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackNum:J

    #@1c
    invoke-static {v0, v1}, Ljava/lang/Long;->reverseBytes(J)J

    #@1f
    move-result-wide v0

    #@20
    return-wide v0
.end method

.method public hasTrackInfoChanged(Landroid/content/Intent;)Z
    .registers 12
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 486
    const-string v6, "BluetoothAVRCP"

    #@3
    const-string v8, "hasTrackInfoChanged()"

    #@5
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 490
    :try_start_8
    iget-object v6, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@a
    const-string v8, "Title"

    #@c
    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v6

    #@10
    check-cast v6, Ljava/lang/String;

    #@12
    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@15
    move-result v6

    #@16
    if-eqz v6, :cond_31

    #@18
    .line 491
    iget-object v6, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1a
    const-string v8, "Title"

    #@1c
    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v6

    #@20
    check-cast v6, Ljava/lang/String;

    #@22
    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    .line 492
    .local v2, title:Ljava/lang/String;
    if-nez v2, :cond_3a

    #@28
    .line 493
    const-string v6, "BluetoothAVRCP"

    #@2a
    const-string v8, "No Title present in intent"

    #@2c
    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    move v6, v7

    #@30
    .line 547
    .end local v2           #title:Ljava/lang/String;
    :goto_30
    return v6

    #@31
    .line 497
    :cond_31
    const-string v6, "BluetoothAVRCP"

    #@33
    const-string v8, "No Title present in intent"

    #@35
    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    move v6, v7

    #@39
    .line 498
    goto :goto_30

    #@3a
    .line 501
    .restart local v2       #title:Ljava/lang/String;
    :cond_3a
    iget-object v6, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@3c
    const-string v8, "TrackNum"

    #@3e
    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@41
    move-result-object v6

    #@42
    check-cast v6, Ljava/lang/String;

    #@44
    invoke-virtual {p1, v6}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@47
    move-result v6

    #@48
    if-nez v6, :cond_53

    #@4a
    .line 502
    const-string v6, "BluetoothAVRCP"

    #@4c
    const-string v8, "No Track ID present in intent"

    #@4e
    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    move v6, v7

    #@52
    .line 503
    goto :goto_30

    #@53
    .line 505
    :cond_53
    iget-object v6, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@55
    const-string v8, "TrackNum"

    #@57
    invoke-virtual {v6, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5a
    move-result-object v6

    #@5b
    check-cast v6, Ljava/lang/String;

    #@5d
    const/4 v8, 0x0

    #@5e
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@61
    move-result-object v8

    #@62
    invoke-virtual {p1, v6, v8}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    #@65
    move-result-object v3

    #@66
    .line 506
    .local v3, track:Ljava/lang/Object;
    if-nez v3, :cond_71

    #@68
    .line 507
    const-string v6, "BluetoothAVRCP"

    #@6a
    const-string v8, "TrackNum returned null"

    #@6c
    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    move v6, v7

    #@70
    .line 508
    goto :goto_30

    #@71
    .line 510
    :cond_71
    instance-of v6, v3, Ljava/lang/Long;

    #@73
    if-eqz v6, :cond_8a

    #@75
    .line 511
    check-cast v3, Ljava/lang/Long;

    #@77
    .end local v3           #track:Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@7a
    move-result-wide v4

    #@7b
    .line 523
    .local v4, trackNum:J
    :goto_7b
    const-wide/16 v8, 0x0

    #@7d
    cmp-long v6, v4, v8

    #@7f
    if-nez v6, :cond_a5

    #@81
    .line 524
    const-string v6, "BluetoothAVRCP"

    #@83
    const-string v8, "trackNum is 0. "

    #@85
    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    move v6, v7

    #@89
    .line 525
    goto :goto_30

    #@8a
    .line 513
    .end local v4           #trackNum:J
    .restart local v3       #track:Ljava/lang/Object;
    :cond_8a
    instance-of v6, v3, Ljava/lang/Integer;

    #@8c
    if-eqz v6, :cond_96

    #@8e
    .line 514
    check-cast v3, Ljava/lang/Integer;

    #@90
    .end local v3           #track:Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@93
    move-result v6

    #@94
    int-to-long v4, v6

    #@95
    .restart local v4       #trackNum:J
    goto :goto_7b

    #@96
    .line 516
    .end local v4           #trackNum:J
    .restart local v3       #track:Ljava/lang/Object;
    :cond_96
    instance-of v6, v3, Ljava/lang/Short;

    #@98
    if-eqz v6, :cond_a2

    #@9a
    .line 517
    check-cast v3, Ljava/lang/Short;

    #@9c
    .end local v3           #track:Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Short;->shortValue()S

    #@9f
    move-result v6

    #@a0
    int-to-long v4, v6

    #@a1
    .restart local v4       #trackNum:J
    goto :goto_7b

    #@a2
    .line 520
    .end local v4           #trackNum:J
    .restart local v3       #track:Ljava/lang/Object;
    :cond_a2
    const-wide/16 v4, 0x0

    #@a4
    .restart local v4       #trackNum:J
    goto :goto_7b

    #@a5
    .line 529
    .end local v3           #track:Ljava/lang/Object;
    :cond_a5
    iget-wide v8, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackNum:J

    #@a7
    cmp-long v6, v4, v8

    #@a9
    if-nez v6, :cond_e9

    #@ab
    .line 530
    const-string v6, "BluetoothAVRCP"

    #@ad
    new-instance v8, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v9, "Track ID has not changed : "

    #@b4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v8

    #@b8
    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v8

    #@bc
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v8

    #@c0
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    .line 531
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->checkPlayStateInIntent(Landroid/content/Intent;)Z
    :try_end_c6
    .catch Ljava/lang/ClassCastException; {:try_start_8 .. :try_end_c6} :catch_cc
    .catch Landroid/os/BadParcelableException; {:try_start_8 .. :try_end_c6} :catch_d7

    #@c6
    move-result v6

    #@c7
    if-nez v6, :cond_e9

    #@c9
    move v6, v7

    #@ca
    .line 532
    goto/16 :goto_30

    #@cc
    .line 536
    .end local v2           #title:Ljava/lang/String;
    .end local v4           #trackNum:J
    :catch_cc
    move-exception v1

    #@cd
    .line 537
    .local v1, ex:Ljava/lang/ClassCastException;
    const-string v6, "BluetoothAVRCP"

    #@cf
    const-string v8, "Expected parameter in different format"

    #@d1
    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    move v6, v7

    #@d5
    .line 538
    goto/16 :goto_30

    #@d7
    .line 540
    .end local v1           #ex:Ljava/lang/ClassCastException;
    :catch_d7
    move-exception v0

    #@d8
    .line 541
    .local v0, e:Landroid/os/BadParcelableException;
    const-string v6, "BluetoothAVRCP"

    #@da
    const-string v8, "BadParcelableException caught in hasTrackInfoChanged()"

    #@dc
    invoke-static {v6, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@df
    .line 542
    const-string v6, "BluetoothAVRCP"

    #@e1
    const-string v8, "Player passed serializable object in intent extras. Hence the current version of the player not supported"

    #@e3
    invoke-static {v6, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e6
    move v6, v7

    #@e7
    .line 544
    goto/16 :goto_30

    #@e9
    .line 546
    .end local v0           #e:Landroid/os/BadParcelableException;
    .restart local v2       #title:Ljava/lang/String;
    .restart local v4       #trackNum:J
    :cond_e9
    const-string v6, "BluetoothAVRCP"

    #@eb
    const-string v7, "Info has changed in intent!"

    #@ed
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f0
    .line 547
    const/4 v6, 0x1

    #@f1
    goto/16 :goto_30
.end method

.method public resetAvrcpPlaybackData()V
    .registers 4

    #@0
    .prologue
    const-wide/16 v1, 0x0

    #@2
    .line 395
    const/4 v0, 0x0

    #@3
    iput v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@5
    .line 396
    iput-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPosition:J

    #@7
    .line 397
    iput-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@9
    .line 398
    return-void
.end method

.method public resetMetadata()V
    .registers 3

    #@0
    .prologue
    .line 401
    const-string v0, ""

    #@2
    iput-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTitle:Ljava/lang/String;

    #@4
    .line 402
    const-string v0, ""

    #@6
    iput-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mArtistName:Ljava/lang/String;

    #@8
    .line 403
    const-string v0, ""

    #@a
    iput-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mAlbumName:Ljava/lang/String;

    #@c
    .line 404
    const-wide/16 v0, -0x1

    #@e
    iput-wide v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackNum:J

    #@10
    .line 405
    const-wide/16 v0, 0x0

    #@12
    iput-wide v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTotalTracks:J

    #@14
    .line 406
    invoke-virtual {p0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->resetAvrcpPlaybackData()V

    #@17
    .line 407
    return-void
.end method

.method public sendPlayStateRequest()V
    .registers 5

    #@0
    .prologue
    .line 551
    const-string v0, "BluetoothAVRCP"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "sendPlayStateRequest() for index:"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 553
    sget v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@1c
    if-gez v0, :cond_26

    #@1e
    .line 554
    const-string v0, "BluetoothAVRCP"

    #@20
    const-string v1, "Player not yet selected. Hence returning"

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 563
    :goto_25
    return-void

    #@26
    .line 557
    :cond_26
    sget-object v0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATUS_REQUEST:[Ljava/lang/String;

    #@28
    sget v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@2a
    aget-object v0, v0, v1

    #@2c
    if-eqz v0, :cond_5d

    #@2e
    .line 558
    const-string v0, "BluetoothAVRCP"

    #@30
    new-instance v1, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v2, "Sending broadcast for :"

    #@37
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATUS_REQUEST:[Ljava/lang/String;

    #@3d
    sget v3, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@3f
    aget-object v2, v2, v3

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 559
    iget-object v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mContext:Landroid/content/Context;

    #@4e
    new-instance v1, Landroid/content/Intent;

    #@50
    sget-object v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATUS_REQUEST:[Ljava/lang/String;

    #@52
    sget v3, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@54
    aget-object v2, v2, v3

    #@56
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@59
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@5c
    goto :goto_25

    #@5d
    .line 561
    :cond_5d
    const-string v0, "BluetoothAVRCP"

    #@5f
    new-instance v1, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v2, "No request string supported for player index :"

    #@66
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v1

    #@6a
    sget v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@6c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v1

    #@70
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v1

    #@74
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    goto :goto_25
.end method

.method public setRepeatMode(I)V
    .registers 7
    .parameter "mode"

    #@0
    .prologue
    .line 431
    const-string v1, "BluetoothAVRCP"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "setRepeatMode()-"

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 434
    const-string v1, "BluetoothAVRCP"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "setRepeatMode() for index:"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    sget v3, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 436
    sget v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@34
    if-gez v1, :cond_3e

    #@36
    .line 437
    const-string v1, "BluetoothAVRCP"

    #@38
    const-string v2, "Player not yet selected. Hence returning"

    #@3a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 448
    :goto_3d
    return-void

    #@3e
    .line 440
    :cond_3e
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_APP_SETTING_REQUEST:[Ljava/lang/String;

    #@40
    sget v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@42
    aget-object v1, v1, v2

    #@44
    if-eqz v1, :cond_82

    #@46
    .line 441
    const-string v1, "BluetoothAVRCP"

    #@48
    new-instance v2, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v3, "Sending broadcast for :"

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    sget-object v3, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_APP_SETTING_REQUEST:[Ljava/lang/String;

    #@55
    sget v4, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@57
    aget-object v3, v3, v4

    #@59
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 442
    new-instance v0, Landroid/content/Intent;

    #@66
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_APP_SETTING_REQUEST:[Ljava/lang/String;

    #@68
    sget v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@6a
    aget-object v1, v1, v2

    #@6c
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6f
    .line 443
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@71
    const-string v2, "Repeat"

    #@73
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@76
    move-result-object v1

    #@77
    check-cast v1, Ljava/lang/String;

    #@79
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@7c
    .line 444
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mContext:Landroid/content/Context;

    #@7e
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@81
    goto :goto_3d

    #@82
    .line 446
    .end local v0           #intent:Landroid/content/Intent;
    :cond_82
    const-string v1, "BluetoothAVRCP"

    #@84
    new-instance v2, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v3, "No Repeat Intent supported for player index :"

    #@8b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v2

    #@8f
    sget v3, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v2

    #@95
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v2

    #@99
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    goto :goto_3d
.end method

.method public setShuffleMode(I)V
    .registers 7
    .parameter "shuffle"

    #@0
    .prologue
    .line 459
    const-string v1, "BluetoothAVRCP"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "setShuffleMode()-"

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 462
    const-string v1, "BluetoothAVRCP"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, "setShuffleMode() for index:"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    sget v3, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 464
    sget v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@34
    if-gez v1, :cond_3e

    #@36
    .line 465
    const-string v1, "BluetoothAVRCP"

    #@38
    const-string v2, "Player not yet selected. Hence returning"

    #@3a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3d
    .line 476
    :goto_3d
    return-void

    #@3e
    .line 468
    :cond_3e
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_APP_SETTING_REQUEST:[Ljava/lang/String;

    #@40
    sget v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@42
    aget-object v1, v1, v2

    #@44
    if-eqz v1, :cond_82

    #@46
    .line 469
    const-string v1, "BluetoothAVRCP"

    #@48
    new-instance v2, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v3, "Sending broadcast for :"

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    sget-object v3, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_APP_SETTING_REQUEST:[Ljava/lang/String;

    #@55
    sget v4, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@57
    aget-object v3, v3, v4

    #@59
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 470
    new-instance v0, Landroid/content/Intent;

    #@66
    sget-object v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_APP_SETTING_REQUEST:[Ljava/lang/String;

    #@68
    sget v2, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@6a
    aget-object v1, v1, v2

    #@6c
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6f
    .line 471
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@71
    const-string v2, "Shuffle"

    #@73
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@76
    move-result-object v1

    #@77
    check-cast v1, Ljava/lang/String;

    #@79
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@7c
    .line 472
    iget-object v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mContext:Landroid/content/Context;

    #@7e
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@81
    goto :goto_3d

    #@82
    .line 474
    .end local v0           #intent:Landroid/content/Intent;
    :cond_82
    const-string v1, "BluetoothAVRCP"

    #@84
    new-instance v2, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v3, "No Shuffle Intent supported for player index :"

    #@8b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v2

    #@8f
    sget v3, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayerIndex:I

    #@91
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@94
    move-result-object v2

    #@95
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v2

    #@99
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    goto :goto_3d
.end method

.method public updateApplicationSettings(Landroid/content/Intent;)Z
    .registers 12
    .parameter "applicationsettingsIntent"

    #@0
    .prologue
    const/4 v9, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v6, 0x0

    #@3
    .line 1070
    const-string v4, "BluetoothAVRCP"

    #@5
    new-instance v7, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v8, "updateApplicationSettings() : "

    #@c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v7

    #@10
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v7

    #@14
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v7

    #@18
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 1074
    :try_start_1b
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->dumpExtras(Landroid/content/Intent;)V

    #@1e
    .line 1076
    const/4 v2, 0x1

    #@1f
    .line 1077
    .local v2, repeat:I
    const/4 v3, 0x1

    #@20
    .line 1079
    .local v3, shuffle:I
    const-string v4, "shuffle_enabled_key"

    #@22
    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@25
    move-result v4

    #@26
    if-eqz v4, :cond_c7

    #@28
    .line 1081
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@2a
    const-string v7, "Shuffle"

    #@2c
    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v4

    #@30
    check-cast v4, Ljava/lang/String;

    #@32
    const/4 v7, 0x0

    #@33
    invoke-virtual {p1, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@36
    move-result v4

    #@37
    if-eqz v4, :cond_c4

    #@39
    .line 1082
    const/4 v3, 0x2

    #@3a
    .line 1107
    :goto_3a
    iget v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mShuffle:I

    #@3c
    if-eq v3, v4, :cond_40

    #@3e
    .line 1108
    iput v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mShuffle:I

    #@40
    .line 1111
    :cond_40
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@42
    const-string v7, "Repeat"

    #@44
    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@47
    move-result-object v4

    #@48
    check-cast v4, Ljava/lang/String;

    #@4a
    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@4d
    move-result v4

    #@4e
    if-eqz v4, :cond_117

    #@50
    .line 1112
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@52
    const-string v7, "Repeat"

    #@54
    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@57
    move-result-object v4

    #@58
    check-cast v4, Ljava/lang/String;

    #@5a
    const/4 v7, 0x0

    #@5b
    invoke-virtual {p1, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@5e
    move-result v2

    #@5f
    .line 1114
    const-string v4, "BluetoothAVRCP"

    #@61
    new-instance v7, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v8, "updateApplicationSettings() repeat:"

    #@68
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v7

    #@6c
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v7

    #@70
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v7

    #@74
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 1117
    if-ne v2, v5, :cond_10f

    #@79
    .line 1118
    const/4 v2, 0x2

    #@7a
    .line 1131
    :goto_7a
    iget v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mRepeat:I

    #@7c
    if-eq v2, v4, :cond_94

    #@7e
    .line 1132
    iput v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mRepeat:I

    #@80
    .line 1133
    if-ne v2, v9, :cond_94

    #@82
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@85
    move-result-object v4

    #@86
    sget-object v7, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->AVRCP_ACTION_PLAYSTATE_CHANGED:[Ljava/lang/String;

    #@88
    const/4 v8, 0x4

    #@89
    aget-object v7, v7, v8

    #@8b
    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8e
    move-result v4

    #@8f
    if-eqz v4, :cond_94

    #@91
    .line 1135
    const/4 v4, 0x1

    #@92
    iput v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mShuffle:I

    #@94
    .line 1139
    :cond_94
    const-string v4, "BluetoothAVRCP"

    #@96
    new-instance v7, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v8, "updateApplicationSettings: Repeat: "

    #@9d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v7

    #@a1
    iget v8, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mRepeat:I

    #@a3
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@a6
    move-result-object v8

    #@a7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v7

    #@ab
    const-string v8, " Shuffle: "

    #@ad
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v7

    #@b1
    iget v8, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mShuffle:I

    #@b3
    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@b6
    move-result-object v8

    #@b7
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v7

    #@bb
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v7

    #@bf
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    .end local v2           #repeat:I
    .end local v3           #shuffle:I
    :goto_c2
    move v4, v5

    #@c3
    .line 1151
    :goto_c3
    return v4

    #@c4
    .line 1085
    .restart local v2       #repeat:I
    .restart local v3       #shuffle:I
    :cond_c4
    const/4 v3, 0x1

    #@c5
    goto/16 :goto_3a

    #@c7
    .line 1088
    :cond_c7
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@c9
    const-string v7, "Shuffle"

    #@cb
    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@ce
    move-result-object v4

    #@cf
    check-cast v4, Ljava/lang/String;

    #@d1
    invoke-virtual {p1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@d4
    move-result v4

    #@d5
    if-eqz v4, :cond_10b

    #@d7
    .line 1089
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@d9
    const-string v7, "Shuffle"

    #@db
    invoke-virtual {v4, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@de
    move-result-object v4

    #@df
    check-cast v4, Ljava/lang/String;

    #@e1
    const/4 v7, 0x0

    #@e2
    invoke-virtual {p1, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@e5
    move-result v3

    #@e6
    .line 1091
    const-string v4, "BluetoothAVRCP"

    #@e8
    new-instance v7, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v8, "updateApplicationSettings() shuffle:"

    #@ef
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v7

    #@f3
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v7

    #@f7
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fa
    move-result-object v7

    #@fb
    invoke-static {v4, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fe
    .line 1093
    if-ne v3, v9, :cond_103

    #@100
    .line 1094
    const/4 v3, 0x2

    #@101
    goto/16 :goto_3a

    #@103
    .line 1096
    :cond_103
    if-ne v3, v5, :cond_108

    #@105
    .line 1097
    const/4 v3, 0x3

    #@106
    goto/16 :goto_3a

    #@108
    .line 1100
    :cond_108
    const/4 v3, 0x1

    #@109
    goto/16 :goto_3a

    #@10b
    .line 1104
    :cond_10b
    iget v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mShuffle:I

    #@10d
    goto/16 :goto_3a

    #@10f
    .line 1120
    :cond_10f
    if-ne v2, v9, :cond_114

    #@111
    .line 1121
    const/4 v2, 0x3

    #@112
    goto/16 :goto_7a

    #@114
    .line 1124
    :cond_114
    const/4 v2, 0x1

    #@115
    goto/16 :goto_7a

    #@117
    .line 1128
    :cond_117
    iget v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mRepeat:I
    :try_end_119
    .catch Ljava/lang/ClassCastException; {:try_start_1b .. :try_end_119} :catch_11b
    .catch Landroid/os/BadParcelableException; {:try_start_1b .. :try_end_119} :catch_124

    #@119
    goto/16 :goto_7a

    #@11b
    .line 1142
    .end local v2           #repeat:I
    .end local v3           #shuffle:I
    :catch_11b
    move-exception v1

    #@11c
    .line 1143
    .local v1, ex:Ljava/lang/ClassCastException;
    const-string v4, "BluetoothAVRCP"

    #@11e
    const-string v6, "Expected parameter in different format"

    #@120
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@123
    goto :goto_c2

    #@124
    .line 1145
    .end local v1           #ex:Ljava/lang/ClassCastException;
    :catch_124
    move-exception v0

    #@125
    .line 1146
    .local v0, e:Landroid/os/BadParcelableException;
    const-string v4, "BluetoothAVRCP"

    #@127
    const-string v5, "BadParcelableException caught in updateApplicationSettings()"

    #@129
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@12c
    .line 1147
    const-string v4, "BluetoothAVRCP"

    #@12e
    const-string v5, "Player passed serializable object in intent extras. Hence the current version of the player not supported"

    #@130
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    move v4, v6

    #@134
    .line 1149
    goto :goto_c3
.end method

.method public updateCurrentTrackFromIntent(Landroid/content/Intent;)V
    .registers 10
    .parameter "intent"

    #@0
    .prologue
    .line 851
    const-string v5, "BluetoothAVRCP"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "updateCurrentTrackFromIntent() : "

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 854
    const-wide/16 v3, 0x0

    #@1a
    .line 856
    .local v3, trackNum:J
    :try_start_1a
    iget-object v5, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1c
    const-string v6, "TrackNum"

    #@1e
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v5

    #@22
    check-cast v5, Ljava/lang/String;

    #@24
    const/4 v6, 0x0

    #@25
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    #@2c
    move-result-object v2

    #@2d
    .line 857
    .local v2, track:Ljava/lang/Object;
    if-nez v2, :cond_50

    #@2f
    .line 858
    const-string v5, "BluetoothAVRCP"

    #@31
    const-string v6, "TrackNum returned null"

    #@33
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 859
    const-wide/16 v3, 0x0

    #@38
    .line 874
    .end local v2           #track:Ljava/lang/Object;
    :goto_38
    const-wide/16 v5, 0x0

    #@3a
    cmp-long v5, v3, v5

    #@3c
    if-gez v5, :cond_40

    #@3e
    .line 875
    const-wide/16 v3, -0x1

    #@40
    .line 878
    :cond_40
    iget-wide v5, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackNum:J

    #@42
    cmp-long v5, v3, v5

    #@44
    if-eqz v5, :cond_4d

    #@46
    .line 879
    const-string v5, "BluetoothAVRCP"

    #@48
    const-string v6, "Current Track has changed"

    #@4a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 881
    :cond_4d
    iput-wide v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackNum:J

    #@4f
    .line 891
    :goto_4f
    return-void

    #@50
    .line 861
    .restart local v2       #track:Ljava/lang/Object;
    :cond_50
    instance-of v5, v2, Ljava/lang/Long;

    #@52
    if-eqz v5, :cond_5b

    #@54
    .line 862
    check-cast v2, Ljava/lang/Long;

    #@56
    .end local v2           #track:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    #@59
    move-result-wide v3

    #@5a
    goto :goto_38

    #@5b
    .line 864
    .restart local v2       #track:Ljava/lang/Object;
    :cond_5b
    instance-of v5, v2, Ljava/lang/Integer;

    #@5d
    if-eqz v5, :cond_67

    #@5f
    .line 865
    check-cast v2, Ljava/lang/Integer;

    #@61
    .end local v2           #track:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@64
    move-result v5

    #@65
    int-to-long v3, v5

    #@66
    goto :goto_38

    #@67
    .line 867
    .restart local v2       #track:Ljava/lang/Object;
    :cond_67
    instance-of v5, v2, Ljava/lang/Short;

    #@69
    if-eqz v5, :cond_73

    #@6b
    .line 868
    check-cast v2, Ljava/lang/Short;

    #@6d
    .end local v2           #track:Ljava/lang/Object;
    invoke-virtual {v2}, Ljava/lang/Short;->shortValue()S
    :try_end_70
    .catch Ljava/lang/ClassCastException; {:try_start_1a .. :try_end_70} :catch_76
    .catch Landroid/os/BadParcelableException; {:try_start_1a .. :try_end_70} :catch_7f

    #@70
    move-result v5

    #@71
    int-to-long v3, v5

    #@72
    goto :goto_38

    #@73
    .line 871
    .restart local v2       #track:Ljava/lang/Object;
    :cond_73
    const-wide/16 v3, 0x0

    #@75
    goto :goto_38

    #@76
    .line 882
    .end local v2           #track:Ljava/lang/Object;
    :catch_76
    move-exception v1

    #@77
    .line 883
    .local v1, ex:Ljava/lang/ClassCastException;
    const-string v5, "BluetoothAVRCP"

    #@79
    const-string v6, "Expected parameter in different format"

    #@7b
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    goto :goto_4f

    #@7f
    .line 885
    .end local v1           #ex:Ljava/lang/ClassCastException;
    :catch_7f
    move-exception v0

    #@80
    .line 886
    .local v0, e:Landroid/os/BadParcelableException;
    const-string v5, "BluetoothAVRCP"

    #@82
    const-string v6, "BadParcelableException caught in updateCurrentTrackFromIntent()"

    #@84
    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@87
    .line 887
    const-string v5, "BluetoothAVRCP"

    #@89
    const-string v6, "Player passed serializable object in intent extras. Hence the current version of the player not supported"

    #@8b
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    goto :goto_4f
.end method

.method updateDurationFromIntent(Landroid/content/Intent;)J
    .registers 8
    .parameter "intent"

    #@0
    .prologue
    .line 783
    const-string v3, "BluetoothAVRCP"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "updateDurationFromIntent() : "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 785
    iget-object v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1a
    const-string v4, "Duration"

    #@1c
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    check-cast v3, Ljava/lang/String;

    #@22
    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_71

    #@28
    .line 786
    iget-object v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@2a
    const-string v4, "Duration"

    #@2c
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v3

    #@30
    check-cast v3, Ljava/lang/String;

    #@32
    const/4 v4, 0x0

    #@33
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    move-result-object v0

    #@37
    .line 787
    .local v0, tmp:Ljava/lang/Object;
    if-nez v0, :cond_43

    #@39
    .line 789
    const-string v3, "BluetoothAVRCP"

    #@3b
    const-string v4, "updateDurationFromIntent() Duration not present in intent"

    #@3d
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 791
    iget-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@42
    .line 811
    .end local v0           #tmp:Ljava/lang/Object;
    :cond_42
    :goto_42
    return-wide v1

    #@43
    .line 793
    .restart local v0       #tmp:Ljava/lang/Object;
    :cond_43
    instance-of v3, v0, Ljava/lang/Long;

    #@45
    if-eqz v3, :cond_56

    #@47
    .line 794
    check-cast v0, Ljava/lang/Long;

    #@49
    .end local v0           #tmp:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@4c
    move-result-wide v1

    #@4d
    .line 806
    .local v1, tmpDuration:J
    :goto_4d
    const-wide/16 v3, 0x0

    #@4f
    cmp-long v3, v1, v3

    #@51
    if-gez v3, :cond_42

    #@53
    .line 807
    const-wide/16 v1, 0x0

    #@55
    goto :goto_42

    #@56
    .line 796
    .end local v1           #tmpDuration:J
    .restart local v0       #tmp:Ljava/lang/Object;
    :cond_56
    instance-of v3, v0, Ljava/lang/Integer;

    #@58
    if-eqz v3, :cond_62

    #@5a
    .line 797
    check-cast v0, Ljava/lang/Integer;

    #@5c
    .end local v0           #tmp:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@5f
    move-result v3

    #@60
    int-to-long v1, v3

    #@61
    .restart local v1       #tmpDuration:J
    goto :goto_4d

    #@62
    .line 799
    .end local v1           #tmpDuration:J
    .restart local v0       #tmp:Ljava/lang/Object;
    :cond_62
    instance-of v3, v0, Ljava/lang/Short;

    #@64
    if-eqz v3, :cond_6e

    #@66
    .line 800
    check-cast v0, Ljava/lang/Short;

    #@68
    .end local v0           #tmp:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    #@6b
    move-result v3

    #@6c
    int-to-long v1, v3

    #@6d
    .restart local v1       #tmpDuration:J
    goto :goto_4d

    #@6e
    .line 803
    .end local v1           #tmpDuration:J
    .restart local v0       #tmp:Ljava/lang/Object;
    :cond_6e
    const-wide/16 v1, 0x0

    #@70
    .restart local v1       #tmpDuration:J
    goto :goto_4d

    #@71
    .line 811
    .end local v0           #tmp:Ljava/lang/Object;
    .end local v1           #tmpDuration:J
    :cond_71
    iget-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@73
    goto :goto_42
.end method

.method public updateMetaData(Landroid/content/Intent;)Z
    .registers 11
    .parameter "metadataIntent"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 976
    const/4 v2, 0x0

    #@2
    .line 977
    .local v2, rv:Z
    const-string v4, "BluetoothAVRCP"

    #@4
    const-string v6, "updateMetaData"

    #@6
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 980
    :try_start_9
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->dumpExtras(Landroid/content/Intent;)V

    #@c
    .line 981
    const/4 v3, 0x0

    #@d
    .line 986
    .local v3, title:Ljava/lang/String;
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@f
    const-string v6, "Title"

    #@11
    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@14
    move-result-object v4

    #@15
    check-cast v4, Ljava/lang/String;

    #@17
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    .line 988
    if-nez v3, :cond_26

    #@1d
    .line 989
    const-string v4, "BluetoothAVRCP"

    #@1f
    const-string v6, "updateMetaData: title is null"

    #@21
    invoke-static {v4, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    move v4, v5

    #@25
    .line 1033
    .end local v3           #title:Ljava/lang/String;
    :goto_25
    return v4

    #@26
    .line 992
    .restart local v3       #title:Ljava/lang/String;
    :cond_26
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTitle:Ljava/lang/String;

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v4

    #@2c
    if-nez v4, :cond_2f

    #@2e
    .line 993
    const/4 v2, 0x1

    #@2f
    .line 995
    :cond_2f
    iput-object v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTitle:Ljava/lang/String;

    #@31
    .line 996
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTitle:Ljava/lang/String;

    #@33
    if-nez v4, :cond_39

    #@35
    .line 997
    const-string v4, "Unknown"

    #@37
    iput-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTitle:Ljava/lang/String;

    #@39
    .line 1000
    :cond_39
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@3b
    const-string v6, "Artist"

    #@3d
    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@40
    move-result-object v4

    #@41
    check-cast v4, Ljava/lang/String;

    #@43
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    iput-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mArtistName:Ljava/lang/String;

    #@49
    .line 1002
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mArtistName:Ljava/lang/String;

    #@4b
    if-nez v4, :cond_51

    #@4d
    .line 1003
    const-string v4, "Unknown"

    #@4f
    iput-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mArtistName:Ljava/lang/String;

    #@51
    .line 1006
    :cond_51
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@53
    const-string v6, "Album"

    #@55
    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@58
    move-result-object v4

    #@59
    check-cast v4, Ljava/lang/String;

    #@5b
    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@5e
    move-result-object v4

    #@5f
    iput-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mAlbumName:Ljava/lang/String;

    #@61
    .line 1008
    iget-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mAlbumName:Ljava/lang/String;

    #@63
    if-nez v4, :cond_69

    #@65
    .line 1009
    const-string v4, "Unknown"

    #@67
    iput-object v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mAlbumName:Ljava/lang/String;

    #@69
    .line 1012
    :cond_69
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->updateCurrentTrackFromIntent(Landroid/content/Intent;)V

    #@6c
    .line 1013
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->updateTotalTracksFromIntent(Landroid/content/Intent;)V

    #@6f
    .line 1015
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->updateDurationFromIntent(Landroid/content/Intent;)J

    #@72
    move-result-wide v6

    #@73
    iput-wide v6, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@75
    .line 1016
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->updatePositionFromIntent(Landroid/content/Intent;)J

    #@78
    move-result-wide v6

    #@79
    iput-wide v6, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPosition:J

    #@7b
    .line 1018
    const-string v4, "BluetoothAVRCP"

    #@7d
    new-instance v6, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v7, "updateMetaData: Title: "

    #@84
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v6

    #@88
    iget-object v7, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTitle:Ljava/lang/String;

    #@8a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v6

    #@8e
    const-string v7, " Artist: "

    #@90
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v6

    #@94
    iget-object v7, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mArtistName:Ljava/lang/String;

    #@96
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v6

    #@9a
    const-string v7, " Album: "

    #@9c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v6

    #@a0
    iget-object v7, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mAlbumName:Ljava/lang/String;

    #@a2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v6

    #@a6
    const-string v7, " TrackNum "

    #@a8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v6

    #@ac
    iget-wide v7, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTrackNum:J

    #@ae
    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@b1
    move-result-object v7

    #@b2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v6

    #@b6
    const-string v7, " Duration: "

    #@b8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v6

    #@bc
    iget-wide v7, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@be
    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@c1
    move-result-object v7

    #@c2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v6

    #@c6
    const-string v7, " TotalTracks: "

    #@c8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v6

    #@cc
    iget-wide v7, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTotalTracks:J

    #@ce
    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@d1
    move-result-object v7

    #@d2
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v6

    #@d6
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v6

    #@da
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_dd
    .catch Ljava/lang/ClassCastException; {:try_start_9 .. :try_end_dd} :catch_e0
    .catch Landroid/os/BadParcelableException; {:try_start_9 .. :try_end_dd} :catch_e9

    #@dd
    .end local v3           #title:Ljava/lang/String;
    :goto_dd
    move v4, v2

    #@de
    .line 1033
    goto/16 :goto_25

    #@e0
    .line 1024
    :catch_e0
    move-exception v1

    #@e1
    .line 1025
    .local v1, ex:Ljava/lang/ClassCastException;
    const-string v4, "BluetoothAVRCP"

    #@e3
    const-string v5, "Expected parameter in different format"

    #@e5
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    goto :goto_dd

    #@e9
    .line 1027
    .end local v1           #ex:Ljava/lang/ClassCastException;
    :catch_e9
    move-exception v0

    #@ea
    .line 1028
    .local v0, e:Landroid/os/BadParcelableException;
    const-string v4, "BluetoothAVRCP"

    #@ec
    const-string v6, "BadParcelableException caught in updateMetaData()"

    #@ee
    invoke-static {v4, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f1
    .line 1029
    const-string v4, "BluetoothAVRCP"

    #@f3
    const-string v6, "Player passed serializable object in intent extras. Hence the current version of the player not supported"

    #@f5
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f8
    move v4, v5

    #@f9
    .line 1031
    goto/16 :goto_25
.end method

.method public updatePlayStateValue(Landroid/content/Intent;)V
    .registers 6
    .parameter "intent"

    #@0
    .prologue
    .line 759
    const/4 v0, -0x1

    #@1
    .line 761
    .local v0, tmpStatus:I
    const-string v1, "BluetoothAVRCP"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "updatePlayStateValue() : "

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 764
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayStateValue(Landroid/content/Intent;)I

    #@1c
    move-result v0

    #@1d
    .line 765
    const/16 v1, 0xff

    #@1f
    if-ne v0, v1, :cond_29

    #@21
    .line 766
    const-string v1, "BluetoothAVRCP"

    #@23
    const-string v2, "Failed to extract PLAY STATE from intent"

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 778
    :cond_28
    :goto_28
    return-void

    #@29
    .line 769
    :cond_29
    iget v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@2b
    if-eq v0, v1, :cond_28

    #@2d
    .line 770
    const-string v1, "BluetoothAVRCP"

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "updatePlayStateValue() Play state changing from :"

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    iget v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    const-string v3, " --> "

    #@42
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 772
    iput v0, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@53
    .line 773
    iget v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@55
    if-nez v1, :cond_28

    #@57
    .line 774
    const-string v1, "BluetoothAVRCP"

    #@59
    const-string v2, "Resetting mPosition and mDuration to 0 for PLAYSTATE_STOPPED"

    #@5b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 775
    const-wide/16 v1, 0x0

    #@60
    iput-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@62
    iput-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPosition:J

    #@64
    goto :goto_28
.end method

.method public updatePlayStatus(Landroid/content/Intent;)Z
    .registers 8
    .parameter "playstatusIntent"

    #@0
    .prologue
    .line 1038
    const-string v2, "BluetoothAVRCP"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "updatePlayStatus() : "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1046
    :try_start_18
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->dumpExtras(Landroid/content/Intent;)V

    #@1b
    .line 1048
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->updateDurationFromIntent(Landroid/content/Intent;)J

    #@1e
    move-result-wide v2

    #@1f
    iput-wide v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@21
    .line 1049
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->updatePositionFromIntent(Landroid/content/Intent;)J

    #@24
    move-result-wide v2

    #@25
    iput-wide v2, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPosition:J

    #@27
    .line 1050
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->updatePlayStateValue(Landroid/content/Intent;)V

    #@2a
    .line 1052
    const-string v2, "BluetoothAVRCP"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "updatePlayStatus: PlayStatus: "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    iget v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPlayStatus:I

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, " Duration: "

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    iget-wide v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mDuration:J

    #@45
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    const-string v4, " Position: "

    #@4f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    iget-wide v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPosition:J

    #@55
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_64
    .catch Ljava/lang/ClassCastException; {:try_start_18 .. :try_end_64} :catch_66
    .catch Landroid/os/BadParcelableException; {:try_start_18 .. :try_end_64} :catch_6f

    #@64
    .line 1064
    :goto_64
    const/4 v2, 0x1

    #@65
    :goto_65
    return v2

    #@66
    .line 1055
    :catch_66
    move-exception v1

    #@67
    .line 1056
    .local v1, ex:Ljava/lang/ClassCastException;
    const-string v2, "BluetoothAVRCP"

    #@69
    const-string v3, "Expected parameter in different format"

    #@6b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    goto :goto_64

    #@6f
    .line 1058
    .end local v1           #ex:Ljava/lang/ClassCastException;
    :catch_6f
    move-exception v0

    #@70
    .line 1059
    .local v0, e:Landroid/os/BadParcelableException;
    const-string v2, "BluetoothAVRCP"

    #@72
    const-string v3, "BadParcelableException caught in updatePlayStatus()"

    #@74
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@77
    .line 1060
    const-string v2, "BluetoothAVRCP"

    #@79
    const-string v3, "Player passed serializable object in intent extras. Hence the current version of the player not supported"

    #@7b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 1062
    const/4 v2, 0x0

    #@7f
    goto :goto_65
.end method

.method updatePositionFromIntent(Landroid/content/Intent;)J
    .registers 8
    .parameter "intent"

    #@0
    .prologue
    .line 817
    const-string v3, "BluetoothAVRCP"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "updatePositionFromIntent() : "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v4

    #@15
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 819
    iget-object v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@1a
    const-string v4, "Position"

    #@1c
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    move-result-object v3

    #@20
    check-cast v3, Ljava/lang/String;

    #@22
    invoke-virtual {p1, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    #@25
    move-result v3

    #@26
    if-eqz v3, :cond_71

    #@28
    .line 820
    iget-object v3, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@2a
    const-string v4, "Position"

    #@2c
    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v3

    #@30
    check-cast v3, Ljava/lang/String;

    #@32
    const/4 v4, 0x0

    #@33
    invoke-virtual {p1, v3, v4}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    move-result-object v0

    #@37
    .line 821
    .local v0, tmp:Ljava/lang/Object;
    if-nez v0, :cond_43

    #@39
    .line 823
    const-string v3, "BluetoothAVRCP"

    #@3b
    const-string v4, "updatePositionFromIntent() Position not present in intent"

    #@3d
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 825
    iget-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPosition:J

    #@42
    .line 845
    .end local v0           #tmp:Ljava/lang/Object;
    :cond_42
    :goto_42
    return-wide v1

    #@43
    .line 827
    .restart local v0       #tmp:Ljava/lang/Object;
    :cond_43
    instance-of v3, v0, Ljava/lang/Long;

    #@45
    if-eqz v3, :cond_56

    #@47
    .line 828
    check-cast v0, Ljava/lang/Long;

    #@49
    .end local v0           #tmp:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    #@4c
    move-result-wide v1

    #@4d
    .line 840
    .local v1, tmpPosition:J
    :goto_4d
    const-wide/16 v3, 0x0

    #@4f
    cmp-long v3, v1, v3

    #@51
    if-gez v3, :cond_42

    #@53
    .line 841
    const-wide/16 v1, 0x0

    #@55
    goto :goto_42

    #@56
    .line 830
    .end local v1           #tmpPosition:J
    .restart local v0       #tmp:Ljava/lang/Object;
    :cond_56
    instance-of v3, v0, Ljava/lang/Integer;

    #@58
    if-eqz v3, :cond_62

    #@5a
    .line 831
    check-cast v0, Ljava/lang/Integer;

    #@5c
    .end local v0           #tmp:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@5f
    move-result v3

    #@60
    int-to-long v1, v3

    #@61
    .restart local v1       #tmpPosition:J
    goto :goto_4d

    #@62
    .line 833
    .end local v1           #tmpPosition:J
    .restart local v0       #tmp:Ljava/lang/Object;
    :cond_62
    instance-of v3, v0, Ljava/lang/Short;

    #@64
    if-eqz v3, :cond_6e

    #@66
    .line 834
    check-cast v0, Ljava/lang/Short;

    #@68
    .end local v0           #tmp:Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    #@6b
    move-result v3

    #@6c
    int-to-long v1, v3

    #@6d
    .restart local v1       #tmpPosition:J
    goto :goto_4d

    #@6e
    .line 837
    .end local v1           #tmpPosition:J
    .restart local v0       #tmp:Ljava/lang/Object;
    :cond_6e
    const-wide/16 v1, 0x0

    #@70
    .restart local v1       #tmpPosition:J
    goto :goto_4d

    #@71
    .line 845
    .end local v0           #tmp:Ljava/lang/Object;
    .end local v1           #tmpPosition:J
    :cond_71
    iget-wide v1, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mPosition:J

    #@73
    goto :goto_42
.end method

.method public updateTotalTracksFromIntent(Landroid/content/Intent;)V
    .registers 10
    .parameter "intent"

    #@0
    .prologue
    .line 895
    const/4 v2, 0x0

    #@1
    .line 896
    .local v2, rv:Z
    const-wide/16 v4, 0x0

    #@3
    .line 898
    .local v4, tracks:J
    :try_start_3
    iget-object v6, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->intentExtras:Ljava/util/HashMap;

    #@5
    const-string v7, "TotalTracks"

    #@7
    invoke-virtual {v6, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v6

    #@b
    check-cast v6, Ljava/lang/String;

    #@d
    const/4 v7, 0x0

    #@e
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v7

    #@12
    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getExtra(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    move-result-object v3

    #@16
    .line 899
    .local v3, totalTracks:Ljava/lang/Object;
    if-nez v3, :cond_39

    #@18
    .line 900
    const-string v6, "BluetoothAVRCP"

    #@1a
    const-string v7, "TotalTracks returned null"

    #@1c
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 901
    const-wide/16 v4, 0x0

    #@21
    .line 916
    .end local v3           #totalTracks:Ljava/lang/Object;
    :goto_21
    const-wide/16 v6, 0x0

    #@23
    cmp-long v6, v4, v6

    #@25
    if-gez v6, :cond_29

    #@27
    .line 917
    const-wide/16 v4, 0x0

    #@29
    .line 920
    :cond_29
    iget-wide v6, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTotalTracks:J

    #@2b
    cmp-long v6, v4, v6

    #@2d
    if-eqz v6, :cond_36

    #@2f
    .line 921
    const-string v6, "BluetoothAVRCP"

    #@31
    const-string v7, "Total Tracks count has changed"

    #@33
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 923
    :cond_36
    iput-wide v4, p0, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->mTotalTracks:J

    #@38
    .line 934
    :goto_38
    return-void

    #@39
    .line 903
    .restart local v3       #totalTracks:Ljava/lang/Object;
    :cond_39
    instance-of v6, v3, Ljava/lang/Long;

    #@3b
    if-eqz v6, :cond_44

    #@3d
    .line 904
    check-cast v3, Ljava/lang/Long;

    #@3f
    .end local v3           #totalTracks:Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    #@42
    move-result-wide v4

    #@43
    goto :goto_21

    #@44
    .line 906
    .restart local v3       #totalTracks:Ljava/lang/Object;
    :cond_44
    instance-of v6, v3, Ljava/lang/Integer;

    #@46
    if-eqz v6, :cond_50

    #@48
    .line 907
    check-cast v3, Ljava/lang/Integer;

    #@4a
    .end local v3           #totalTracks:Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@4d
    move-result v6

    #@4e
    int-to-long v4, v6

    #@4f
    goto :goto_21

    #@50
    .line 909
    .restart local v3       #totalTracks:Ljava/lang/Object;
    :cond_50
    instance-of v6, v3, Ljava/lang/Short;

    #@52
    if-eqz v6, :cond_5c

    #@54
    .line 910
    check-cast v3, Ljava/lang/Short;

    #@56
    .end local v3           #totalTracks:Ljava/lang/Object;
    invoke-virtual {v3}, Ljava/lang/Short;->shortValue()S
    :try_end_59
    .catch Ljava/lang/ClassCastException; {:try_start_3 .. :try_end_59} :catch_5f
    .catch Landroid/os/BadParcelableException; {:try_start_3 .. :try_end_59} :catch_68

    #@59
    move-result v6

    #@5a
    int-to-long v4, v6

    #@5b
    goto :goto_21

    #@5c
    .line 913
    .restart local v3       #totalTracks:Ljava/lang/Object;
    :cond_5c
    const-wide/16 v4, 0x0

    #@5e
    goto :goto_21

    #@5f
    .line 925
    .end local v3           #totalTracks:Ljava/lang/Object;
    :catch_5f
    move-exception v1

    #@60
    .line 926
    .local v1, ex:Ljava/lang/ClassCastException;
    const-string v6, "BluetoothAVRCP"

    #@62
    const-string v7, "Expected parameter in different format"

    #@64
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    goto :goto_38

    #@68
    .line 928
    .end local v1           #ex:Ljava/lang/ClassCastException;
    :catch_68
    move-exception v0

    #@69
    .line 929
    .local v0, e:Landroid/os/BadParcelableException;
    const-string v6, "BluetoothAVRCP"

    #@6b
    const-string v7, "BadParcelableException caught in updateTotalTracksFromIntent()"

    #@6d
    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@70
    .line 930
    const-string v6, "BluetoothAVRCP"

    #@72
    const-string v7, "Player passed serializable object in intent extras. Hence the current version of the player not supported"

    #@74
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    goto :goto_38
.end method
