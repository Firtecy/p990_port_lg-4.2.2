.class Lcom/broadcom/bt/service/gatt/ContextMap$App;
.super Ljava/lang/Object;
.source "ContextMap.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/gatt/ContextMap;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "App"
.end annotation


# instance fields
.field callback:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field id:B

.field private mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

.field final synthetic this$0:Lcom/broadcom/bt/service/gatt/ContextMap;

.field uuid:Ljava/util/UUID;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/gatt/ContextMap;Ljava/util/UUID;Ljava/lang/Object;)V
    .registers 4
    .parameter
    .parameter "uuid"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            "TT;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 104
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    .local p3, callback:Ljava/lang/Object;,"TT;"
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->this$0:Lcom/broadcom/bt/service/gatt/ContextMap;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 105
    iput-object p2, p0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->uuid:Ljava/util/UUID;

    #@7
    .line 106
    iput-object p3, p0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@9
    .line 107
    return-void
.end method


# virtual methods
.method linkToDeath(Landroid/os/IBinder$DeathRecipient;)V
    .registers 7
    .parameter "deathRecipient"

    #@0
    .prologue
    .line 114
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    :try_start_0
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@2
    check-cast v2, Landroid/os/IInterface;

    #@4
    invoke-interface {v2}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    #@7
    move-result-object v0

    #@8
    .line 115
    .local v0, binder:Landroid/os/IBinder;
    const/4 v2, 0x0

    #@9
    invoke-interface {v0, p1, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    #@c
    .line 116
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_e} :catch_f

    #@e
    .line 120
    .end local v0           #binder:Landroid/os/IBinder;
    :goto_e
    return-void

    #@f
    .line 117
    :catch_f
    move-exception v1

    #@10
    .line 118
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "BtGatt.ContextMap"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "Unable to link deathRecipient for app id "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    iget-byte v4, p0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_e
.end method

.method unlinkToDeath()V
    .registers 6

    #@0
    .prologue
    .line 126
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    #@2
    if-eqz v2, :cond_12

    #@4
    .line 128
    :try_start_4
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@6
    check-cast v2, Landroid/os/IInterface;

    #@8
    invoke-interface {v2}, Landroid/os/IInterface;->asBinder()Landroid/os/IBinder;

    #@b
    move-result-object v0

    #@c
    .line 129
    .local v0, binder:Landroid/os/IBinder;
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->mDeathRecipient:Landroid/os/IBinder$DeathRecipient;

    #@e
    const/4 v3, 0x0

    #@f
    invoke-interface {v0, v2, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_12
    .catch Ljava/util/NoSuchElementException; {:try_start_4 .. :try_end_12} :catch_13

    #@12
    .line 134
    .end local v0           #binder:Landroid/os/IBinder;
    :cond_12
    :goto_12
    return-void

    #@13
    .line 130
    :catch_13
    move-exception v1

    #@14
    .line 131
    .local v1, e:Ljava/util/NoSuchElementException;
    const-string v2, "BtGatt.ContextMap"

    #@16
    new-instance v3, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v4, "Unable to unlink deathRecipient for app id "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    iget-byte v4, p0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@23
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    goto :goto_12
.end method
