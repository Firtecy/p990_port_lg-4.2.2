.class public Lcom/broadcom/bt/service/map/datasource/mms/MmsEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MmsEventReceiver.java"


# static fields
.field private static final D:Z = true

.field static final EVENT_DELIVERED:Ljava/lang/String; = "com.broadcom.bt.mms.DELIVERED"

.field static final EXTRA_FOLDER_ID:Ljava/lang/String; = "folder"

.field static final EXTRA_MIMETYPE:Ljava/lang/String; = "mimetype"

.field static final EXTRA_MMS_ID:Ljava/lang/String; = "id"

.field static final EXTRA_NEW_FOLDER_ID:Ljava/lang/String; = "new_folder"

.field static final EXTRA_REQUEST_ID:Ljava/lang/String; = "req_id"

.field static final EXTRA_STATUS:Ljava/lang/String; = "status"

.field static final STATUS_ERROR:Ljava/lang/String; = "2"

.field static final STATUS_SUCCESS:Ljava/lang/String; = "1"

.field private static final TAG:Ljava/lang/String; = "MAP.MmsEventReceiver"


# instance fields
.field final intentExtraName:Ljava/lang/String;

.field private mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)V
    .registers 3
    .parameter "ds"

    #@0
    .prologue
    .line 76
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    .line 74
    const-string v0, "id"

    #@5
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventReceiver;->intentExtraName:Ljava/lang/String;

    #@7
    .line 77
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventReceiver;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@9
    .line 78
    return-void
.end method

.method private processMessageDeliveredAction(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 10
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 99
    const-string v4, "MAP.MmsEventReceiver"

    #@2
    const-string v5, "processMessageDeliveredAction()"

    #@4
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 102
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventReceiver;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@9
    invoke-virtual {v4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->isNotificationEnabled()Z

    #@c
    move-result v4

    #@d
    if-nez v4, :cond_17

    #@f
    .line 104
    const-string v4, "MAP.MmsEventReceiver"

    #@11
    const-string v5, "MAP notification not enabled...Skipping event"

    #@13
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 124
    :goto_16
    return-void

    #@17
    .line 110
    :cond_17
    const-string v4, "id"

    #@19
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@1c
    move-result-object v3

    #@1d
    check-cast v3, Landroid/net/Uri;

    #@1f
    .line 111
    .local v3, messageUri:Landroid/net/Uri;
    if-nez v3, :cond_29

    #@21
    .line 112
    const-string v4, "MAP.MmsEventReceiver"

    #@23
    const-string v5, "Unable to process message status. Invalid messageUri"

    #@25
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_16

    #@29
    .line 115
    :cond_29
    const-string v4, "MAP.MmsEventReceiver"

    #@2b
    new-instance v5, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v6, "Got message URI = "

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 116
    const/4 v1, 0x1

    #@46
    .line 117
    .local v1, folderType:I
    invoke-static {v1}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    .line 118
    .local v0, folderPath:Ljava/lang/String;
    if-nez v0, :cond_65

    #@4c
    .line 119
    const-string v4, "MAP.MmsEventReceiver"

    #@4e
    new-instance v5, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v6, "Invalid folder path for folder type:"

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v5

    #@5d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v5

    #@61
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    goto :goto_16

    #@65
    .line 122
    :cond_65
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    invoke-static {v4}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->encodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@6c
    move-result-object v2

    #@6d
    .line 123
    .local v2, messageId:Ljava/lang/String;
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventReceiver;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@6f
    const/16 v5, 0x8

    #@71
    const/4 v6, 0x1

    #@72
    invoke-virtual {v4, v2, v5, v0, v6}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyDeliveryStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@75
    goto :goto_16
.end method


# virtual methods
.method public createFilter()Landroid/content/IntentFilter;
    .registers 3

    #@0
    .prologue
    .line 92
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 93
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "com.broadcom.bt.mms.DELIVERED"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 94
    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 81
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 82
    .local v0, action:Ljava/lang/String;
    const-string v1, "MAP.MmsEventReceiver"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Got intent "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 84
    const-string v1, "com.broadcom.bt.mms.DELIVERED"

    #@1e
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_28

    #@24
    .line 85
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventReceiver;->processMessageDeliveredAction(Landroid/content/Context;Landroid/content/Intent;)V

    #@27
    .line 89
    :goto_27
    return-void

    #@28
    .line 87
    :cond_28
    const-string v1, "MAP.MmsEventReceiver"

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "Unable to process action: "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    goto :goto_27
.end method
