.class public Lcom/broadcom/bt/service/gatt/GattService;
.super Lcom/android/bluetooth/btservice/ProfileService;
.source "GattService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;,
        Lcom/broadcom/bt/service/gatt/GattService$ServerDeathRecipient;,
        Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;,
        Lcom/broadcom/bt/service/gatt/GattService$ServerMap;,
        Lcom/broadcom/bt/service/gatt/GattService$ClientMap;
    }
.end annotation


# static fields
.field public static final BLE_ONEKEY_CHANGED:Ljava/lang/String; = "com.lge.action.BLE_ONEKEY"

.field private static final DBG:Z = true

.field private static final GATT_CLIENT_ROLE:I = 0x1

.field private static final GATT_SERVER_ROLE:I = 0x2

.field static final LE_FUNCTION_COUNT:I = 0x4

.field private static final ONEKEY_CONTROL_DISABLE_STRING:Ljava/lang/String; = "false"

.field private static final ONEKEY_CONTROL_ENABLE_STRING:Ljava/lang/String; = "true"

.field private static final ONEKEY_DOUBLE_STRING:Ljava/lang/String; = "DoubleKey"

.field private static final ONEKEY_FIVEFOLD_STRING:Ljava/lang/String; = "FivefoldKey"

.field private static final ONEKEY_LONG_STRING:Ljava/lang/String; = "LongKey"

.field private static final ONEKEY_SHORT_STRING:Ljava/lang/String; = "ShortKey"

.field private static final ONEKEY_TRIPLE_STRING:Ljava/lang/String; = "TripleKey"

.field private static final TAG:Ljava/lang/String; = "BtGatt.GattService"

.field private static mGattService:Lcom/broadcom/bt/service/gatt/GattService;

.field private static mbAddingService:Z


# instance fields
.field mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

.field mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mReliableQueue:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mScanQueue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/service/gatt/ScanClient;",
            ">;"
        }
    .end annotation
.end field

.field mSearchQueue:Lcom/broadcom/bt/service/gatt/SearchQueue;

.field mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

.field private mServiceDeclarations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/service/gatt/ServiceDeclaration;",
            ">;"
        }
    .end annotation
.end field

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field oneKeyuuid:Ljava/util/UUID;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 112
    const/4 v0, 0x0

    #@1
    sput-boolean v0, Lcom/broadcom/bt/service/gatt/GattService;->mbAddingService:Z

    #@3
    .line 212
    invoke-static {}, Lcom/broadcom/bt/service/gatt/GattService;->classInitNative()V

    #@6
    .line 213
    return-void
.end method

.method public constructor <init>()V
    .registers 6

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 88
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/ProfileService;-><init>()V

    #@4
    .line 106
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mPowerManager:Landroid/os/PowerManager;

    #@6
    .line 107
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@8
    .line 115
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@e
    .line 120
    new-instance v0, Lcom/broadcom/bt/service/gatt/SearchQueue;

    #@10
    invoke-direct {v0}, Lcom/broadcom/bt/service/gatt/SearchQueue;-><init>()V

    #@13
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mSearchQueue:Lcom/broadcom/bt/service/gatt/SearchQueue;

    #@15
    .line 122
    new-instance v0, Ljava/util/UUID;

    #@17
    const-wide v1, 0x185000001000L

    #@1c
    const-wide/16 v3, 0x0

    #@1e
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@21
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->oneKeyuuid:Ljava/util/UUID;

    #@23
    .line 129
    new-instance v0, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@25
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;-><init>(Lcom/broadcom/bt/service/gatt/GattService;)V

    #@28
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@2a
    .line 135
    new-instance v0, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@2c
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;-><init>(Lcom/broadcom/bt/service/gatt/GattService;)V

    #@2f
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@31
    .line 140
    new-instance v0, Lcom/broadcom/bt/service/gatt/HandleMap;

    #@33
    invoke-direct {v0}, Lcom/broadcom/bt/service/gatt/HandleMap;-><init>()V

    #@36
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@38
    .line 145
    new-instance v0, Ljava/util/ArrayList;

    #@3a
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@3d
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@3f
    .line 185
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    #@41
    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    #@44
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@46
    .line 209
    new-instance v0, Ljava/util/HashSet;

    #@48
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@4b
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mReliableQueue:Ljava/util/Set;

    #@4d
    .line 88
    return-void
.end method

.method static synthetic access$000(Lcom/broadcom/bt/service/gatt/GattService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/broadcom/bt/service/gatt/GattService;->isAvailable()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100()Z
    .registers 1

    #@0
    .prologue
    .line 88
    sget-boolean v0, Lcom/broadcom/bt/service/gatt/GattService;->mbAddingService:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Z)Z
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 88
    sput-boolean p0, Lcom/broadcom/bt/service/gatt/GattService;->mbAddingService:Z

    #@2
    return p0
.end method

.method static synthetic access$200(Lcom/broadcom/bt/service/gatt/GattService;Landroid/bluetooth/BluetoothDevice;)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/gatt/GattService;->getDeviceType(Landroid/bluetooth/BluetoothDevice;)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private acquireScreenWakeLock(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 2312
    const/16 v0, 0x2710

    #@2
    .line 2314
    .local v0, BACKLIGHT_ON_DURATION:I
    const-string v1, "power"

    #@4
    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Landroid/os/PowerManager;

    #@a
    iput-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mPowerManager:Landroid/os/PowerManager;

    #@c
    .line 2315
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mPowerManager:Landroid/os/PowerManager;

    #@e
    invoke-virtual {v1}, Landroid/os/PowerManager;->isScreenOn()Z

    #@11
    move-result v1

    #@12
    if-nez v1, :cond_37

    #@14
    .line 2316
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@16
    if-nez v1, :cond_25

    #@18
    .line 2317
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mPowerManager:Landroid/os/PowerManager;

    #@1a
    const v2, 0x3000001a

    #@1d
    const-string v3, "BtGatt.GattService"

    #@1f
    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@22
    move-result-object v1

    #@23
    iput-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@25
    .line 2324
    :cond_25
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@27
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@2a
    move-result v1

    #@2b
    if-nez v1, :cond_37

    #@2d
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2f
    if-eqz v1, :cond_37

    #@31
    .line 2325
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@33
    int-to-long v2, v0

    #@34
    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@37
    .line 2328
    :cond_37
    return-void
.end method

.method private addDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    .registers 4

    #@0
    .prologue
    .line 148
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@2
    monitor-enter v1

    #@3
    .line 149
    :try_start_3
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@5
    new-instance v2, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@7
    invoke-direct {v2}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;-><init>()V

    #@a
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@d
    .line 150
    monitor-exit v1
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_13

    #@e
    .line 151
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService;->getActiveDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@11
    move-result-object v0

    #@12
    return-object v0

    #@13
    .line 150
    :catchall_13
    move-exception v0

    #@14
    :try_start_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method private static native classInitNative()V
.end method

.method private native cleanupNative()V
.end method

.method private static declared-synchronized clearGattService()V
    .registers 2

    #@0
    .prologue
    .line 2368
    const-class v0, Lcom/broadcom/bt/service/gatt/GattService;

    #@2
    monitor-enter v0

    #@3
    const/4 v1, 0x0

    #@4
    :try_start_4
    sput-object v1, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;
    :try_end_6
    .catchall {:try_start_4 .. :try_end_6} :catchall_8

    #@6
    .line 2369
    monitor-exit v0

    #@7
    return-void

    #@8
    .line 2368
    :catchall_8
    move-exception v1

    #@9
    monitor-exit v0

    #@a
    throw v1
.end method

.method private continueSearch(II)V
    .registers 23
    .parameter "connId"
    .parameter "status"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1988
    if-nez p2, :cond_67

    #@2
    move-object/from16 v0, p0

    #@4
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/GattService;->mSearchQueue:Lcom/broadcom/bt/service/gatt/SearchQueue;

    #@6
    invoke-virtual {v1}, Lcom/broadcom/bt/service/gatt/SearchQueue;->isEmpty()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_67

    #@c
    .line 1989
    move-object/from16 v0, p0

    #@e
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/GattService;->mSearchQueue:Lcom/broadcom/bt/service/gatt/SearchQueue;

    #@10
    invoke-virtual {v1}, Lcom/broadcom/bt/service/gatt/SearchQueue;->pop()Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;

    #@13
    move-result-object v19

    #@14
    .line 1991
    .local v19, svc:Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    move-object/from16 v0, v19

    #@16
    iget-wide v1, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->charUuidLsb:J

    #@18
    const-wide/16 v3, 0x0

    #@1a
    cmp-long v1, v1, v3

    #@1c
    if-nez v1, :cond_3d

    #@1e
    .line 1993
    move-object/from16 v0, v19

    #@20
    iget v2, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->connId:I

    #@22
    move-object/from16 v0, v19

    #@24
    iget v3, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcType:I

    #@26
    move-object/from16 v0, v19

    #@28
    iget v4, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcInstId:I

    #@2a
    move-object/from16 v0, v19

    #@2c
    iget-wide v5, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcUuidLsb:J

    #@2e
    move-object/from16 v0, v19

    #@30
    iget-wide v7, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcUuidMsb:J

    #@32
    const/4 v9, 0x0

    #@33
    const-wide/16 v10, 0x0

    #@35
    const-wide/16 v12, 0x0

    #@37
    move-object/from16 v1, p0

    #@39
    invoke-direct/range {v1 .. v13}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientGetCharacteristicNative(IIIJJIJJ)V

    #@3c
    .line 2007
    .end local v19           #svc:Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 1997
    .restart local v19       #svc:Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    :cond_3d
    move-object/from16 v0, v19

    #@3f
    iget v2, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->connId:I

    #@41
    move-object/from16 v0, v19

    #@43
    iget v3, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcType:I

    #@45
    move-object/from16 v0, v19

    #@47
    iget v4, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcInstId:I

    #@49
    move-object/from16 v0, v19

    #@4b
    iget-wide v5, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcUuidLsb:J

    #@4d
    move-object/from16 v0, v19

    #@4f
    iget-wide v7, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcUuidMsb:J

    #@51
    move-object/from16 v0, v19

    #@53
    iget v9, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->charInstId:I

    #@55
    move-object/from16 v0, v19

    #@57
    iget-wide v10, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->charUuidLsb:J

    #@59
    move-object/from16 v0, v19

    #@5b
    iget-wide v12, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->charUuidMsb:J

    #@5d
    const-wide/16 v14, 0x0

    #@5f
    const-wide/16 v16, 0x0

    #@61
    move-object/from16 v1, p0

    #@63
    invoke-direct/range {v1 .. v17}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientGetDescriptorNative(IIIJJIJJJJ)V

    #@66
    goto :goto_3c

    #@67
    .line 2002
    .end local v19           #svc:Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    :cond_67
    move-object/from16 v0, p0

    #@69
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@6b
    move/from16 v0, p1

    #@6d
    invoke-virtual {v1, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@70
    move-result-object v18

    #@71
    .line 2003
    .local v18, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v18, :cond_3c

    #@73
    .line 2004
    move-object/from16 v0, v18

    #@75
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@77
    check-cast v1, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@79
    move-object/from16 v0, p0

    #@7b
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@7d
    move/from16 v0, p1

    #@7f
    invoke-virtual {v2, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    move/from16 v0, p2

    #@85
    invoke-interface {v1, v2, v0}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onSearchComplete(Ljava/lang/String;I)V

    #@88
    goto :goto_3c
.end method

.method private continueServiceDeclaration(BBI)V
    .registers 22
    .parameter "serverIf"
    .parameter "status"
    .parameter "srvcHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2010
    move-object/from16 v0, p0

    #@2
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@4
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@7
    move-result v3

    #@8
    if-nez v3, :cond_b

    #@a
    .line 2113
    :cond_a
    :goto_a
    return-void

    #@b
    .line 2014
    :cond_b
    const-string v3, "BtGatt.GattService"

    #@d
    new-instance v4, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v5, "continueServiceDeclaration() - srvcHandle="

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    move/from16 v0, p3

    #@1a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 2017
    const/4 v14, 0x0

    #@26
    .line 2019
    .local v14, finished:Z
    const/4 v13, 0x0

    #@27
    .line 2021
    .local v13, entry:Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;
    if-nez p2, :cond_33

    #@29
    .line 2022
    invoke-direct/range {p0 .. p0}, Lcom/broadcom/bt/service/gatt/GattService;->getPendingDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@2c
    move-result-object v16

    #@2d
    .line 2023
    .local v16, serviceDeclaration:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    if-eqz v16, :cond_a6

    #@2f
    .line 2024
    invoke-virtual/range {v16 .. v16}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->getNext()Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;

    #@32
    move-result-object v13

    #@33
    .line 2036
    .end local v16           #serviceDeclaration:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    :cond_33
    :goto_33
    if-eqz v13, :cond_120

    #@35
    .line 2038
    const-string v3, "BtGatt.GattService"

    #@37
    new-instance v4, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v5, "continueServiceDeclaration() - next entry type="

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    iget-byte v5, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->type:B

    #@44
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v4

    #@48
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v4

    #@4c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 2040
    iget-byte v3, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->type:B

    #@51
    packed-switch v3, :pswitch_data_13e

    #@54
    .line 2088
    :goto_54
    if-eqz v14, :cond_a

    #@56
    .line 2090
    const-string v3, "BtGatt.GattService"

    #@58
    const-string v4, "continueServiceDeclaration() - completed."

    #@5a
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 2092
    move-object/from16 v0, p0

    #@5f
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@61
    move/from16 v0, p1

    #@63
    invoke-virtual {v3, v0}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@66
    move-result-object v12

    #@67
    .line 2093
    .local v12, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    if-eqz v12, :cond_8f

    #@69
    .line 2094
    move-object/from16 v0, p0

    #@6b
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@6d
    move/from16 v0, p3

    #@6f
    invoke-virtual {v3, v0}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@72
    move-result-object v17

    #@73
    .line 2095
    .local v17, serviceEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    if-eqz v17, :cond_12f

    #@75
    .line 2096
    iget-object v3, v12, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@77
    check-cast v3, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@79
    move-object/from16 v0, v17

    #@7b
    iget v4, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceType:I

    #@7d
    move-object/from16 v0, v17

    #@7f
    iget v5, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@81
    new-instance v6, Landroid/os/ParcelUuid;

    #@83
    move-object/from16 v0, v17

    #@85
    iget-object v7, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@87
    invoke-direct {v6, v7}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@8a
    move/from16 v0, p2

    #@8c
    invoke-interface {v3, v0, v4, v5, v6}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onServiceAdded(BIILandroid/os/ParcelUuid;)V

    #@8f
    .line 2102
    .end local v17           #serviceEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :cond_8f
    :goto_8f
    invoke-direct/range {p0 .. p0}, Lcom/broadcom/bt/service/gatt/GattService;->removePendingDeclaration()V

    #@92
    .line 2104
    invoke-direct/range {p0 .. p0}, Lcom/broadcom/bt/service/gatt/GattService;->getPendingDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@95
    move-result-object v3

    #@96
    if-eqz v3, :cond_a1

    #@98
    .line 2105
    const/4 v3, 0x0

    #@99
    const/4 v4, 0x0

    #@9a
    move-object/from16 v0, p0

    #@9c
    move/from16 v1, p1

    #@9e
    invoke-direct {v0, v1, v3, v4}, Lcom/broadcom/bt/service/gatt/GattService;->continueServiceDeclaration(BBI)V

    #@a1
    .line 2109
    :cond_a1
    const/4 v3, 0x0

    #@a2
    sput-boolean v3, Lcom/broadcom/bt/service/gatt/GattService;->mbAddingService:Z

    #@a4
    goto/16 :goto_a

    #@a6
    .line 2027
    .end local v12           #app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    .restart local v16       #serviceDeclaration:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    :cond_a6
    const-string v3, "BtGatt.GattService"

    #@a8
    const-string v4, "continueServiceDeclaration(): serviceDeclaration is null"

    #@aa
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ad
    goto :goto_33

    #@ae
    .line 2042
    .end local v16           #serviceDeclaration:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    :pswitch_ae
    iget v5, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->serviceType:I

    #@b0
    iget v6, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->instance:I

    #@b2
    iget-object v3, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@b4
    invoke-virtual {v3}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@b7
    move-result-wide v7

    #@b8
    iget-object v3, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@ba
    invoke-virtual {v3}, Ljava/util/UUID;->getMostSignificantBits()J

    #@bd
    move-result-wide v9

    #@be
    invoke-direct/range {p0 .. p0}, Lcom/broadcom/bt/service/gatt/GattService;->getPendingDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@c1
    move-result-object v3

    #@c2
    invoke-virtual {v3}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->getNumHandles()I

    #@c5
    move-result v11

    #@c6
    move-object/from16 v3, p0

    #@c8
    move/from16 v4, p1

    #@ca
    invoke-direct/range {v3 .. v11}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerAddServiceNative(BIIJJI)V

    #@cd
    goto :goto_54

    #@ce
    .line 2050
    :pswitch_ce
    iget-object v3, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@d0
    invoke-virtual {v3}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@d3
    move-result-wide v6

    #@d4
    iget-object v3, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@d6
    invoke-virtual {v3}, Ljava/util/UUID;->getMostSignificantBits()J

    #@d9
    move-result-wide v8

    #@da
    iget v10, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->properties:I

    #@dc
    iget v11, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->permissions:I

    #@de
    move-object/from16 v3, p0

    #@e0
    move/from16 v4, p1

    #@e2
    move/from16 v5, p3

    #@e4
    invoke-direct/range {v3 .. v11}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerAddCharacteristicNative(BIJJII)V

    #@e7
    goto/16 :goto_54

    #@e9
    .line 2057
    :pswitch_e9
    iget-object v3, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@eb
    invoke-virtual {v3}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@ee
    move-result-wide v6

    #@ef
    iget-object v3, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@f1
    invoke-virtual {v3}, Ljava/util/UUID;->getMostSignificantBits()J

    #@f4
    move-result-wide v8

    #@f5
    iget v10, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->permissions:I

    #@f7
    move-object/from16 v3, p0

    #@f9
    move/from16 v4, p1

    #@fb
    move/from16 v5, p3

    #@fd
    invoke-direct/range {v3 .. v10}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerAddDescriptorNative(BIJJI)V

    #@100
    goto/16 :goto_54

    #@102
    .line 2065
    :pswitch_102
    move-object/from16 v0, p0

    #@104
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@106
    iget-object v4, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@108
    iget v5, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->serviceType:I

    #@10a
    iget v6, v13, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->instance:I

    #@10c
    invoke-virtual {v3, v4, v5, v6}, Lcom/broadcom/bt/service/gatt/HandleMap;->getServiceHandle(Ljava/util/UUID;II)I

    #@10f
    move-result v15

    #@110
    .line 2067
    .local v15, inclSrvc:I
    if-eqz v15, :cond_11d

    #@112
    .line 2068
    move-object/from16 v0, p0

    #@114
    move/from16 v1, p1

    #@116
    move/from16 v2, p3

    #@118
    invoke-direct {v0, v1, v2, v15}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerAddIncludedServiceNative(BII)V

    #@11b
    goto/16 :goto_54

    #@11d
    .line 2071
    :cond_11d
    const/4 v14, 0x1

    #@11e
    .line 2073
    goto/16 :goto_54

    #@120
    .line 2080
    .end local v15           #inclSrvc:I
    :cond_120
    if-nez p2, :cond_12c

    #@122
    .line 2082
    const/4 v3, 0x2

    #@123
    move-object/from16 v0, p0

    #@125
    move/from16 v1, p1

    #@127
    move/from16 v2, p3

    #@129
    invoke-direct {v0, v1, v2, v3}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerStartServiceNative(BIB)V

    #@12c
    .line 2085
    :cond_12c
    const/4 v14, 0x1

    #@12d
    goto/16 :goto_54

    #@12f
    .line 2099
    .restart local v12       #app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    .restart local v17       #serviceEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :cond_12f
    iget-object v3, v12, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@131
    check-cast v3, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@133
    const/4 v4, 0x0

    #@134
    const/4 v5, 0x0

    #@135
    const/4 v6, 0x0

    #@136
    move/from16 v0, p2

    #@138
    invoke-interface {v3, v0, v4, v5, v6}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onServiceAdded(BIILandroid/os/ParcelUuid;)V

    #@13b
    goto/16 :goto_8f

    #@13d
    .line 2040
    nop

    #@13e
    :pswitch_data_13e
    .packed-switch 0x1
        :pswitch_ae
        :pswitch_ce
        :pswitch_e9
        :pswitch_102
    .end packed-switch
.end method

.method private deleteServices(B)V
    .registers 10
    .parameter "serverIf"

    #@0
    .prologue
    .line 2137
    const-string v5, "BtGatt.GattService"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "deleteServices() - serverIf="

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 2144
    new-instance v3, Ljava/util/ArrayList;

    #@1a
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@1d
    .line 2145
    .local v3, handleList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@1f
    invoke-virtual {v5}, Lcom/broadcom/bt/service/gatt/HandleMap;->getEntries()Ljava/util/List;

    #@22
    move-result-object v0

    #@23
    .line 2146
    .local v0, entries:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/service/gatt/HandleMap$Entry;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@26
    move-result-object v4

    #@27
    .local v4, i$:Ljava/util/Iterator;
    :cond_27
    :goto_27
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@2a
    move-result v5

    #@2b
    if-eqz v5, :cond_46

    #@2d
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@30
    move-result-object v1

    #@31
    check-cast v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@33
    .line 2147
    .local v1, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    iget-byte v5, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@35
    const/4 v6, 0x1

    #@36
    if-ne v5, v6, :cond_27

    #@38
    iget-byte v5, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@3a
    if-ne v5, p1, :cond_27

    #@3c
    .line 2151
    iget v5, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@3e
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v5

    #@42
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@45
    goto :goto_27

    #@46
    .line 2155
    .end local v1           #entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :cond_46
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@49
    move-result-object v4

    #@4a
    :goto_4a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@4d
    move-result v5

    #@4e
    if-eqz v5, :cond_5e

    #@50
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@53
    move-result-object v2

    #@54
    check-cast v2, Ljava/lang/Integer;

    #@56
    .line 2156
    .local v2, handle:Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@59
    move-result v5

    #@5a
    invoke-direct {p0, p1, v5}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerDeleteServiceNative(BI)V

    #@5d
    goto :goto_4a

    #@5e
    .line 2158
    .end local v2           #handle:Ljava/lang/Integer;
    :cond_5e
    return-void
.end method

.method private native gattClientCloseNative(BLjava/lang/String;I)V
.end method

.method private native gattClientExecuteWriteNative(IZ)V
.end method

.method private native gattClientGetCharacteristicNative(IIIJJIJJ)V
.end method

.method private native gattClientGetDescriptorNative(IIIJJIJJJJ)V
.end method

.method private native gattClientGetDeviceTypeNative(Ljava/lang/String;)I
.end method

.method private native gattClientGetIncludedServiceNative(IIIJJIIJJ)V
.end method

.method private native gattClientOpenNative(BLjava/lang/String;Z)V
.end method

.method private native gattClientReadCharacteristicNative(IIIJJIJJB)V
.end method

.method private native gattClientReadDescriptorNative(IIIJJIJJJJB)V
.end method

.method private native gattClientReadRemoteRssiNative(BLjava/lang/String;)V
.end method

.method private native gattClientRefreshNative(BLjava/lang/String;)V
.end method

.method private native gattClientRegisterAppNative(JJ)V
.end method

.method private native gattClientRegisterForNotificationsNative(BLjava/lang/String;IIJJIJJZ)V
.end method

.method private native gattClientScanNative(BZ)V
.end method

.method private native gattClientSearchServiceNative(IZJJ)V
.end method

.method private native gattClientUnregisterAppNative(B)V
.end method

.method private native gattClientWriteCharacteristicNative(IIIJJIJJIB[B)V
.end method

.method private native gattClientWriteDescriptorNative(IIIJJIJJJJIB[B)V
.end method

.method private native gattServerAddCharacteristicNative(BIJJII)V
.end method

.method private native gattServerAddDescriptorNative(BIJJI)V
.end method

.method private native gattServerAddIncludedServiceNative(BII)V
.end method

.method private native gattServerAddServiceNative(BIIJJI)V
.end method

.method private native gattServerCloseNative(BLjava/lang/String;I)V
.end method

.method private native gattServerDeleteServiceNative(BI)V
.end method

.method private native gattServerOpenNative(BLjava/lang/String;Z)V
.end method

.method private native gattServerReadRemoteRssiNative(BLjava/lang/String;)V
.end method

.method private native gattServerRegisterAppNative(JJ)V
.end method

.method private native gattServerSendIndicationNative(BII[B)V
.end method

.method private native gattServerSendNotificationNative(BII[B)V
.end method

.method private native gattServerSendResponseNative(BIIBII[BB)V
.end method

.method private native gattServerStartServiceNative(BIB)V
.end method

.method private native gattServerStopServiceNative(BI)V
.end method

.method private native gattServerUnregisterAppNative(B)V
.end method

.method private native gattTestNative(IJJLjava/lang/String;IIIII)V
.end method

.method private getActiveDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    .registers 4

    #@0
    .prologue
    .line 155
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@2
    monitor-enter v1

    #@3
    .line 156
    :try_start_3
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@5
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@8
    move-result v0

    #@9
    if-lez v0, :cond_1d

    #@b
    .line 157
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@d
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@f
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@12
    move-result v2

    #@13
    add-int/lit8 v2, v2, -0x1

    #@15
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@1b
    monitor-exit v1

    #@1c
    .line 160
    :goto_1c
    return-object v0

    #@1d
    .line 159
    :cond_1d
    monitor-exit v1

    #@1e
    .line 160
    const/4 v0, 0x0

    #@1f
    goto :goto_1c

    #@20
    .line 159
    :catchall_20
    move-exception v0

    #@21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_20

    #@22
    throw v0
.end method

.method private getDeviceType(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 1980
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    invoke-direct {p0, v1}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientGetDeviceTypeNative(Ljava/lang/String;)I

    #@7
    move-result v0

    #@8
    .line 1984
    .local v0, type:I
    return v0
.end method

.method public static declared-synchronized getGattService()Lcom/broadcom/bt/service/gatt/GattService;
    .registers 4

    #@0
    .prologue
    .line 2332
    const-class v1, Lcom/broadcom/bt/service/gatt/GattService;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;

    #@5
    if-eqz v0, :cond_2d

    #@7
    sget-object v0, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;

    #@9
    invoke-virtual {v0}, Lcom/broadcom/bt/service/gatt/GattService;->isAvailable()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_2d

    #@f
    .line 2334
    const-string v0, "BtGatt.GattService"

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "getGattService(): returning "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    sget-object v3, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 2336
    sget-object v0, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;
    :try_end_2b
    .catchall {:try_start_3 .. :try_end_2b} :catchall_4a

    #@2b
    .line 2347
    :goto_2b
    monitor-exit v1

    #@2c
    return-object v0

    #@2d
    .line 2340
    :cond_2d
    :try_start_2d
    sget-object v0, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;

    #@2f
    if-nez v0, :cond_3a

    #@31
    .line 2341
    const-string v0, "BtGatt.GattService"

    #@33
    const-string v2, "getGattService(): service is NULL"

    #@35
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 2347
    :cond_38
    :goto_38
    const/4 v0, 0x0

    #@39
    goto :goto_2b

    #@3a
    .line 2342
    :cond_3a
    sget-object v0, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;

    #@3c
    invoke-virtual {v0}, Lcom/broadcom/bt/service/gatt/GattService;->isAvailable()Z

    #@3f
    move-result v0

    #@40
    if-nez v0, :cond_38

    #@42
    .line 2343
    const-string v0, "BtGatt.GattService"

    #@44
    const-string v2, "getGattService(): service is not available"

    #@46
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_49
    .catchall {:try_start_2d .. :try_end_49} :catchall_4a

    #@49
    goto :goto_38

    #@4a
    .line 2332
    :catchall_4a
    move-exception v0

    #@4b
    monitor-exit v1

    #@4c
    throw v0
.end method

.method private getPendingDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    .registers 4

    #@0
    .prologue
    .line 164
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@2
    monitor-enter v1

    #@3
    .line 165
    :try_start_3
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@5
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@8
    move-result v0

    #@9
    if-lez v0, :cond_16

    #@b
    .line 166
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@d
    const/4 v2, 0x0

    #@e
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@14
    monitor-exit v1

    #@15
    .line 169
    :goto_15
    return-object v0

    #@16
    .line 168
    :cond_16
    monitor-exit v1

    #@17
    .line 169
    const/4 v0, 0x0

    #@18
    goto :goto_15

    #@19
    .line 168
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1
    :try_end_1b
    .catchall {:try_start_3 .. :try_end_1b} :catchall_19

    #@1b
    throw v0
.end method

.method private getScanClient(BZ)Lcom/broadcom/bt/service/gatt/ScanClient;
    .registers 6
    .parameter "appIf"
    .parameter "isServer"

    #@0
    .prologue
    .line 189
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1b

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/ScanClient;

    #@12
    .line 190
    .local v0, client:Lcom/broadcom/bt/service/gatt/ScanClient;
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/ScanClient;->appIf:B

    #@14
    if-ne v2, p1, :cond_6

    #@16
    iget-boolean v2, v0, Lcom/broadcom/bt/service/gatt/ScanClient;->isServer:Z

    #@18
    if-ne v2, p2, :cond_6

    #@1a
    .line 194
    .end local v0           #client:Lcom/broadcom/bt/service/gatt/ScanClient;
    :goto_1a
    return-object v0

    #@1b
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_1a
.end method

.method private native initializeNative()V
.end method

.method private parseUuids([B)Ljava/util/List;
    .registers 13
    .parameter "adv_data"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List",
            "<",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 2161
    new-instance v5, Ljava/util/ArrayList;

    #@3
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@6
    .line 2163
    .local v5, uuids:Ljava/util/List;,"Ljava/util/List<Ljava/util/UUID;>;"
    const/4 v1, 0x0

    #@7
    .line 2164
    .local v1, offset:I
    :goto_7
    array-length v6, p1

    #@8
    add-int/lit8 v6, v6, -0x2

    #@a
    if-ge v1, v6, :cond_13

    #@c
    .line 2165
    add-int/lit8 v2, v1, 0x1

    #@e
    .end local v1           #offset:I
    .local v2, offset:I
    aget-byte v0, p1, v1

    #@10
    .line 2166
    .local v0, len:I
    if-nez v0, :cond_14

    #@12
    move v1, v2

    #@13
    .line 2189
    .end local v0           #len:I
    .end local v2           #offset:I
    .restart local v1       #offset:I
    :cond_13
    return-object v5

    #@14
    .line 2170
    .end local v1           #offset:I
    .restart local v0       #len:I
    .restart local v2       #offset:I
    :cond_14
    add-int/lit8 v1, v2, 0x1

    #@16
    .end local v2           #offset:I
    .restart local v1       #offset:I
    aget-byte v3, p1, v2

    #@18
    .line 2171
    .local v3, type:I
    packed-switch v3, :pswitch_data_4a

    #@1b
    .line 2184
    add-int/lit8 v6, v0, -0x1

    #@1d
    add-int/2addr v1, v6

    #@1e
    goto :goto_7

    #@1f
    .line 2174
    .end local v1           #offset:I
    .restart local v2       #offset:I
    :goto_1f
    if-le v0, v10, :cond_45

    #@21
    .line 2175
    add-int/lit8 v1, v2, 0x1

    #@23
    .end local v2           #offset:I
    .restart local v1       #offset:I
    aget-byte v4, p1, v2

    #@25
    .line 2176
    .local v4, uuid16:I
    add-int/lit8 v2, v1, 0x1

    #@27
    .end local v1           #offset:I
    .restart local v2       #offset:I
    aget-byte v6, p1, v1

    #@29
    shl-int/lit8 v6, v6, 0x8

    #@2b
    add-int/2addr v4, v6

    #@2c
    .line 2177
    add-int/lit8 v0, v0, -0x2

    #@2e
    .line 2178
    const-string v6, "%08x-0000-1000-8000-00805f9b34fb"

    #@30
    new-array v7, v10, [Ljava/lang/Object;

    #@32
    const/4 v8, 0x0

    #@33
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@36
    move-result-object v9

    #@37
    aput-object v9, v7, v8

    #@39
    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3c
    move-result-object v6

    #@3d
    invoke-static {v6}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@40
    move-result-object v6

    #@41
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@44
    goto :goto_1f

    #@45
    .end local v4           #uuid16:I
    :cond_45
    move v1, v2

    #@46
    .end local v2           #offset:I
    .restart local v1       #offset:I
    goto :goto_7

    #@47
    :pswitch_47
    move v2, v1

    #@48
    .end local v1           #offset:I
    .restart local v2       #offset:I
    goto :goto_1f

    #@49
    .line 2171
    nop

    #@4a
    :pswitch_data_4a
    .packed-switch 0x2
        :pswitch_47
        :pswitch_47
    .end packed-switch
.end method

.method private removePendingDeclaration()V
    .registers 4

    #@0
    .prologue
    .line 173
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@2
    monitor-enter v1

    #@3
    .line 174
    :try_start_3
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@5
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@8
    move-result v0

    #@9
    if-lez v0, :cond_11

    #@b
    .line 175
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@d
    const/4 v2, 0x0

    #@e
    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@11
    .line 177
    :cond_11
    monitor-exit v1

    #@12
    .line 178
    return-void

    #@13
    .line 177
    :catchall_13
    move-exception v0

    #@14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_13

    #@15
    throw v0
.end method

.method private removeScanClient(BZ)V
    .registers 6
    .parameter "appIf"
    .parameter "isServer"

    #@0
    .prologue
    .line 198
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1f

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/ScanClient;

    #@12
    .line 199
    .local v0, client:Lcom/broadcom/bt/service/gatt/ScanClient;
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/ScanClient;->appIf:B

    #@14
    if-ne v2, p1, :cond_6

    #@16
    iget-boolean v2, v0, Lcom/broadcom/bt/service/gatt/ScanClient;->isServer:Z

    #@18
    if-ne v2, p2, :cond_6

    #@1a
    .line 200
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@1c
    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@1f
    .line 204
    .end local v0           #client:Lcom/broadcom/bt/service/gatt/ScanClient;
    :cond_1f
    return-void
.end method

.method private static declared-synchronized setGattService(Lcom/broadcom/bt/service/gatt/GattService;)V
    .registers 5
    .parameter "instance"

    #@0
    .prologue
    .line 2351
    const-class v1, Lcom/broadcom/bt/service/gatt/GattService;

    #@2
    monitor-enter v1

    #@3
    if-eqz p0, :cond_29

    #@5
    :try_start_5
    invoke-virtual {p0}, Lcom/broadcom/bt/service/gatt/GattService;->isAvailable()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_29

    #@b
    .line 2353
    const-string v0, "BtGatt.GattService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "setGattService(): set to: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    sget-object v3, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 2355
    sput-object p0, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;
    :try_end_27
    .catchall {:try_start_5 .. :try_end_27} :catchall_35

    #@27
    .line 2365
    :cond_27
    :goto_27
    monitor-exit v1

    #@28
    return-void

    #@29
    .line 2358
    :cond_29
    :try_start_29
    sget-object v0, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;

    #@2b
    if-nez v0, :cond_38

    #@2d
    .line 2359
    const-string v0, "BtGatt.GattService"

    #@2f
    const-string v2, "setGattService(): service not available"

    #@31
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_34
    .catchall {:try_start_29 .. :try_end_34} :catchall_35

    #@34
    goto :goto_27

    #@35
    .line 2351
    :catchall_35
    move-exception v0

    #@36
    monitor-exit v1

    #@37
    throw v0

    #@38
    .line 2360
    :cond_38
    :try_start_38
    sget-object v0, Lcom/broadcom/bt/service/gatt/GattService;->mGattService:Lcom/broadcom/bt/service/gatt/GattService;

    #@3a
    invoke-virtual {v0}, Lcom/broadcom/bt/service/gatt/GattService;->isAvailable()Z

    #@3d
    move-result v0

    #@3e
    if-nez v0, :cond_27

    #@40
    .line 2361
    const-string v0, "BtGatt.GattService"

    #@42
    const-string v2, "setGattService(): service is cleaning up"

    #@44
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_47
    .catchall {:try_start_38 .. :try_end_47} :catchall_35

    #@47
    goto :goto_27
.end method

.method private stopNextService(BB)V
    .registers 9
    .parameter "serverIf"
    .parameter "status"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 2117
    const-string v3, "BtGatt.GattService"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "stopNextService() - serverIf="

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v4

    #@11
    const-string v5, ", status="

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 2120
    if-nez p2, :cond_4c

    #@24
    .line 2121
    iget-object v3, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@26
    invoke-virtual {v3}, Lcom/broadcom/bt/service/gatt/HandleMap;->getEntries()Ljava/util/List;

    #@29
    move-result-object v0

    #@2a
    .line 2122
    .local v0, entries:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/service/gatt/HandleMap$Entry;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2d
    move-result-object v2

    #@2e
    .local v2, i$:Ljava/util/Iterator;
    :cond_2e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@31
    move-result v3

    #@32
    if-eqz v3, :cond_4c

    #@34
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@37
    move-result-object v1

    #@38
    check-cast v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@3a
    .line 2123
    .local v1, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    iget-byte v3, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@3c
    const/4 v4, 0x1

    #@3d
    if-ne v3, v4, :cond_2e

    #@3f
    iget-byte v3, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@41
    if-ne v3, p1, :cond_2e

    #@43
    iget-boolean v3, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->started:Z

    #@45
    if-eqz v3, :cond_2e

    #@47
    .line 2129
    iget v3, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@49
    invoke-direct {p0, p1, v3}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerStopServiceNative(BI)V

    #@4c
    .line 2133
    .end local v0           #entries:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/service/gatt/HandleMap$Entry;>;"
    .end local v1           #entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    .end local v2           #i$:Ljava/util/Iterator;
    :cond_4c
    return-void
.end method


# virtual methods
.method OneKeyExceptionHandle([B)V
    .registers 15
    .parameter "data"

    #@0
    .prologue
    const/4 v12, 0x4

    #@1
    const/4 v11, 0x0

    #@2
    .line 2194
    const/4 v4, 0x0

    #@3
    .line 2195
    .local v4, isWakeOn:Z
    const/4 v5, 0x0

    #@4
    .line 2196
    .local v5, key:Ljava/lang/String;
    const/4 v7, 0x0

    #@5
    .line 2197
    .local v7, value:Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    #@7
    const-string v8, "com.lge.action.BLE_ONEKEY"

    #@9
    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c
    .line 2198
    .local v3, intent:Landroid/content/Intent;
    const-string v8, "BtGatt.GattService"

    #@e
    new-instance v9, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v10, "onCharacteristicChanged data[0]="

    #@15
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v9

    #@19
    aget-byte v10, p1, v11

    #@1b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v9

    #@1f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v9

    #@23
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 2200
    if-eqz p1, :cond_bd

    #@28
    .line 2202
    aget-byte v8, p1, v11

    #@2a
    const/16 v9, 0x50

    #@2c
    if-ne v8, v9, :cond_97

    #@2e
    .line 2203
    const-string v5, "ShortKey"

    #@30
    .line 2218
    :cond_30
    :goto_30
    const-string v8, "_service"

    #@32
    const-string v9, "lge_onekey_service"

    #@34
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@37
    .line 2219
    const-string v8, "_key"

    #@39
    invoke-virtual {v3, v8, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3c
    .line 2221
    const-string v8, "BtGatt.GattService"

    #@3e
    new-instance v9, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v10, "sendBroadcast to Application key Value : "

    #@45
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v9

    #@49
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v9

    #@4d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v9

    #@51
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 2231
    const-string v8, "persist.service.btui.onekey"

    #@56
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@59
    move-result-object v8

    #@5a
    if-nez v8, :cond_cc

    #@5c
    .line 2232
    const-string v8, "BtGatt.GattService"

    #@5e
    const-string v9, "---------- All Function Value : null -> Set Default(All enable)"

    #@60
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 2233
    const-string v8, "_onekeyMusic"

    #@65
    const-string v9, "true"

    #@67
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@6a
    .line 2234
    const-string v8, "_onekeyCamera"

    #@6c
    const-string v9, "true"

    #@6e
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@71
    .line 2235
    const-string v8, "_onekeyVoiceCall"

    #@73
    const-string v9, "true"

    #@75
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@78
    .line 2236
    const-string v8, "_onekeyFindme"

    #@7a
    const-string v9, "true"

    #@7c
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@7f
    .line 2294
    :cond_7f
    if-eqz v4, :cond_89

    #@81
    .line 2296
    invoke-static {}, Lcom/broadcom/bt/service/gatt/GattService;->getGattService()Lcom/broadcom/bt/service/gatt/GattService;

    #@84
    move-result-object v8

    #@85
    invoke-direct {p0, v8}, Lcom/broadcom/bt/service/gatt/GattService;->acquireScreenWakeLock(Landroid/content/Context;)V

    #@88
    .line 2297
    const/4 v4, 0x0

    #@89
    .line 2301
    :cond_89
    invoke-static {}, Lcom/broadcom/bt/service/gatt/GattService;->getGattService()Lcom/broadcom/bt/service/gatt/GattService;

    #@8c
    move-result-object v2

    #@8d
    .line 2302
    .local v2, gattService:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v2, :cond_18b

    #@8f
    .line 2303
    const-string v8, "BtGatt.GattService"

    #@91
    const-string v9, "gattService is null"

    #@93
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    .line 2309
    .end local v2           #gattService:Lcom/broadcom/bt/service/gatt/GattService;
    :goto_96
    return-void

    #@97
    .line 2205
    :cond_97
    aget-byte v8, p1, v11

    #@99
    const/16 v9, 0x51

    #@9b
    if-ne v8, v9, :cond_a0

    #@9d
    .line 2206
    const-string v5, "DoubleKey"

    #@9f
    goto :goto_30

    #@a0
    .line 2208
    :cond_a0
    aget-byte v8, p1, v11

    #@a2
    const/16 v9, 0x52

    #@a4
    if-ne v8, v9, :cond_a9

    #@a6
    .line 2209
    const-string v5, "TripleKey"

    #@a8
    goto :goto_30

    #@a9
    .line 2211
    :cond_a9
    aget-byte v8, p1, v11

    #@ab
    const/16 v9, 0x53

    #@ad
    if-ne v8, v9, :cond_b3

    #@af
    .line 2212
    const-string v5, "LongKey"

    #@b1
    goto/16 :goto_30

    #@b3
    .line 2214
    :cond_b3
    aget-byte v8, p1, v11

    #@b5
    const/16 v9, 0x60

    #@b7
    if-ne v8, v9, :cond_30

    #@b9
    .line 2215
    const-string v5, "FivefoldKey"

    #@bb
    goto/16 :goto_30

    #@bd
    .line 2224
    :cond_bd
    const-string v8, "BtGatt.GattService"

    #@bf
    const-string v9, "sendBroadcast to Application key Value is null"

    #@c1
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c4
    .line 2225
    const-string v8, "BtGatt.GattService"

    #@c6
    const-string v9, "onCharacteristicChanged. - There is no key value. Onekey cannot service."

    #@c8
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cb
    goto :goto_96

    #@cc
    .line 2238
    :cond_cc
    new-instance v6, Ljava/lang/String;

    #@ce
    const-string v8, "persist.service.btui.onekey"

    #@d0
    invoke-static {v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@d3
    move-result-object v8

    #@d4
    invoke-direct {v6, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@d7
    .line 2239
    .local v6, onekeyFunctionValue:Ljava/lang/String;
    new-array v1, v12, [C

    #@d9
    .line 2241
    .local v1, functionValue:[C
    const/4 v0, 0x0

    #@da
    .local v0, control_funcion:I
    :goto_da
    if-ge v0, v12, :cond_7f

    #@dc
    .line 2242
    invoke-virtual {v6, v0}, Ljava/lang/String;->charAt(I)C

    #@df
    move-result v8

    #@e0
    aput-char v8, v1, v0

    #@e2
    .line 2243
    const-string v8, "1"

    #@e4
    aget-char v9, v1, v0

    #@e6
    invoke-static {v9}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@e9
    move-result-object v9

    #@ea
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ed
    move-result v8

    #@ee
    if-eqz v8, :cond_13b

    #@f0
    .line 2244
    packed-switch v0, :pswitch_data_192

    #@f3
    .line 2241
    :cond_f3
    :goto_f3
    add-int/lit8 v0, v0, 0x1

    #@f5
    goto :goto_da

    #@f6
    .line 2246
    :pswitch_f6
    const-string v8, "BtGatt.GattService"

    #@f8
    const-string v9, "_onekeyMusic Function Value : true"

    #@fa
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fd
    .line 2247
    const-string v8, "_onekeyMusic"

    #@ff
    const-string v9, "true"

    #@101
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@104
    goto :goto_f3

    #@105
    .line 2250
    :pswitch_105
    const-string v8, "BtGatt.GattService"

    #@107
    const-string v9, "_onekeyCamera Function Value : true"

    #@109
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10c
    .line 2251
    const-string v8, "_onekeyCamera"

    #@10e
    const-string v9, "true"

    #@110
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@113
    .line 2253
    const-string v8, "LongKey"

    #@115
    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@118
    move-result v8

    #@119
    if-eqz v8, :cond_f3

    #@11b
    .line 2254
    const/4 v4, 0x1

    #@11c
    goto :goto_f3

    #@11d
    .line 2258
    :pswitch_11d
    const-string v8, "BtGatt.GattService"

    #@11f
    const-string v9, "_onekeyVoiceCall Function Value : true"

    #@121
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@124
    .line 2259
    const-string v8, "_onekeyVoiceCall"

    #@126
    const-string v9, "true"

    #@128
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@12b
    goto :goto_f3

    #@12c
    .line 2262
    :pswitch_12c
    const-string v8, "BtGatt.GattService"

    #@12e
    const-string v9, "_onekeyFindme Function Value : true"

    #@130
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@133
    .line 2263
    const-string v8, "_onekeyFindme"

    #@135
    const-string v9, "true"

    #@137
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@13a
    goto :goto_f3

    #@13b
    .line 2268
    :cond_13b
    const-string v8, "0"

    #@13d
    aget-char v9, v1, v0

    #@13f
    invoke-static {v9}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    #@142
    move-result-object v9

    #@143
    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@146
    move-result v8

    #@147
    if-eqz v8, :cond_f3

    #@149
    .line 2269
    packed-switch v0, :pswitch_data_19e

    #@14c
    goto :goto_f3

    #@14d
    .line 2271
    :pswitch_14d
    const-string v8, "BtGatt.GattService"

    #@14f
    const-string v9, "_onekeyMusic Function Value : false"

    #@151
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@154
    .line 2272
    const-string v8, "_onekeyMusic"

    #@156
    const-string v9, "false"

    #@158
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@15b
    goto :goto_f3

    #@15c
    .line 2275
    :pswitch_15c
    const-string v8, "BtGatt.GattService"

    #@15e
    const-string v9, "_onekeyCamera Function Value : false"

    #@160
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@163
    .line 2276
    const-string v8, "_onekeyCamera"

    #@165
    const-string v9, "false"

    #@167
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@16a
    goto :goto_f3

    #@16b
    .line 2279
    :pswitch_16b
    const-string v8, "BtGatt.GattService"

    #@16d
    const-string v9, "_onekeyVoiceCall Function Value : false"

    #@16f
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    .line 2280
    const-string v8, "_onekeyVoiceCall"

    #@174
    const-string v9, "false"

    #@176
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@179
    goto/16 :goto_f3

    #@17b
    .line 2283
    :pswitch_17b
    const-string v8, "BtGatt.GattService"

    #@17d
    const-string v9, "_onekeyFindme Function Value : false"

    #@17f
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@182
    .line 2284
    const-string v8, "_onekeyFindme"

    #@184
    const-string v9, "false"

    #@186
    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@189
    goto/16 :goto_f3

    #@18b
    .line 2307
    .end local v0           #control_funcion:I
    .end local v1           #functionValue:[C
    .end local v6           #onekeyFunctionValue:Ljava/lang/String;
    .restart local v2       #gattService:Lcom/broadcom/bt/service/gatt/GattService;
    :cond_18b
    const/4 v8, 0x0

    #@18c
    invoke-virtual {v2, v3, v8}, Lcom/broadcom/bt/service/gatt/GattService;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@18f
    goto/16 :goto_96

    #@191
    .line 2244
    nop

    #@192
    :pswitch_data_192
    .packed-switch 0x0
        :pswitch_f6
        :pswitch_105
        :pswitch_11d
        :pswitch_12c
    .end packed-switch

    #@19e
    .line 2269
    :pswitch_data_19e
    .packed-switch 0x0
        :pswitch_14d
        :pswitch_15c
        :pswitch_16b
        :pswitch_17b
    .end packed-switch
.end method

.method addCharacteristic(BLjava/util/UUID;II)V
    .registers 9
    .parameter "serverIf"
    .parameter "charUuid"
    .parameter "properties"
    .parameter "permissions"

    #@0
    .prologue
    .line 1840
    const-string v1, "BtGatt.GattService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "addCharacteristic() - uuid="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1842
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService;->getActiveDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@1b
    move-result-object v0

    #@1c
    .line 1843
    .local v0, serviceDeclaration:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    if-eqz v0, :cond_22

    #@1e
    .line 1844
    invoke-virtual {v0, p2, p3, p4}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->addCharacteristic(Ljava/util/UUID;II)V

    #@21
    .line 1854
    :goto_21
    return-void

    #@22
    .line 1847
    :cond_22
    const-string v1, "BtGatt.GattService"

    #@24
    const-string v2, "addCharacteristic(): serviceDeclaration is null"

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_21
.end method

.method addDescriptor(BLjava/util/UUID;I)V
    .registers 8
    .parameter "serverIf"
    .parameter "descUuid"
    .parameter "permissions"

    #@0
    .prologue
    .line 1858
    const-string v1, "BtGatt.GattService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "addDescriptor() - uuid="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1861
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService;->getActiveDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@1b
    move-result-object v0

    #@1c
    .line 1862
    .local v0, serviceDeclaration:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    if-eqz v0, :cond_22

    #@1e
    .line 1863
    invoke-virtual {v0, p2, p3}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->addDescriptor(Ljava/util/UUID;I)V

    #@21
    .line 1873
    :goto_21
    return-void

    #@22
    .line 1866
    :cond_22
    const-string v1, "BtGatt.GattService"

    #@24
    const-string v2, "addDescriptor(): serviceDeclaration is null"

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_21
.end method

.method addIncludedService(BIILjava/util/UUID;)V
    .registers 9
    .parameter "serverIf"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcUuid"

    #@0
    .prologue
    .line 1821
    const-string v1, "BtGatt.GattService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "addIncludedService() - uuid="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1824
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService;->getActiveDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@1b
    move-result-object v0

    #@1c
    .line 1825
    .local v0, serviceDeclaration:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    if-eqz v0, :cond_22

    #@1e
    .line 1826
    invoke-virtual {v0, p4, p2, p3}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->addIncludedService(Ljava/util/UUID;II)V

    #@21
    .line 1836
    :goto_21
    return-void

    #@22
    .line 1829
    :cond_22
    const-string v1, "BtGatt.GattService"

    #@24
    const-string v2, "addIncludedService(): serviceDeclaration is null"

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_21
.end method

.method beginReliableWrite(BLjava/lang/String;)V
    .registers 6
    .parameter "clientIf"
    .parameter "address"

    #@0
    .prologue
    .line 1367
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "beginReliableWrite() - address="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1369
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mReliableQueue:Ljava/util/Set;

    #@1a
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@1d
    .line 1370
    return-void
.end method

.method beginServiceDeclaration(BIIILjava/util/UUID;)V
    .registers 10
    .parameter "serverIf"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "minHandles"
    .parameter "srvcUuid"

    #@0
    .prologue
    .line 1800
    const-string v1, "BtGatt.GattService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "beginServiceDeclaration() - uuid="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1803
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService;->addDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@1b
    move-result-object v0

    #@1c
    .line 1805
    .local v0, serviceDeclaration:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
    if-eqz v0, :cond_22

    #@1e
    .line 1806
    invoke-virtual {v0, p5, p2, p3, p4}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->addService(Ljava/util/UUID;III)V

    #@21
    .line 1816
    :goto_21
    return-void

    #@22
    .line 1809
    :cond_22
    const-string v1, "BtGatt.GattService"

    #@24
    const-string v2, "beginServiceDeclaration(): serviceDeclaration is null"

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_21
.end method

.method protected cleanup()Z
    .registers 3

    #@0
    .prologue
    .line 250
    const-string v0, "BtGatt.GattService"

    #@2
    const-string v1, "cleanup()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 252
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService;->cleanupNative()V

    #@a
    .line 254
    invoke-static {}, Lcom/broadcom/bt/service/gatt/GattService;->clearGattService()V

    #@d
    .line 256
    const/4 v0, 0x1

    #@e
    return v0
.end method

.method clearServices(B)V
    .registers 4
    .parameter "serverIf"

    #@0
    .prologue
    .line 1904
    const-string v0, "BtGatt.GattService"

    #@2
    const-string v1, "clearServices()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1906
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/gatt/GattService;->deleteServices(B)V

    #@a
    .line 1907
    return-void
.end method

.method clientConnect(BLjava/lang/String;Z)V
    .registers 7
    .parameter "clientIf"
    .parameter "address"
    .parameter "isDirect"

    #@0
    .prologue
    .line 1237
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "clientConnect() - address="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", isDirect="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1239
    invoke-direct {p0, p1, p2, p3}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientOpenNative(BLjava/lang/String;Z)V

    #@25
    .line 1240
    return-void
.end method

.method clientDisconnect(BLjava/lang/String;)V
    .registers 7
    .parameter "clientIf"
    .parameter "address"

    #@0
    .prologue
    .line 1243
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@2
    invoke-virtual {v1, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@5
    move-result-object v0

    #@6
    .line 1245
    .local v0, connId:Ljava/lang/Integer;
    const-string v1, "BtGatt.GattService"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "clientDisconnect() - address="

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, ", connId="

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1248
    if-eqz v0, :cond_32

    #@2a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@2d
    move-result v1

    #@2e
    :goto_2e
    invoke-direct {p0, p1, p2, v1}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientCloseNative(BLjava/lang/String;I)V

    #@31
    .line 1249
    return-void

    #@32
    .line 1248
    :cond_32
    const/4 v1, 0x0

    #@33
    goto :goto_2e
.end method

.method discoverServices(BLjava/lang/String;)V
    .registers 11
    .parameter "clientIf"
    .parameter "address"

    #@0
    .prologue
    const-wide/16 v3, 0x0

    #@2
    .line 1267
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@4
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@7
    move-result-object v7

    #@8
    .line 1269
    .local v7, connId:Ljava/lang/Integer;
    const-string v0, "BtGatt.GattService"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "discoverServices() - address="

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    const-string v2, ", connId="

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 1272
    if-eqz v7, :cond_37

    #@2c
    .line 1273
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@2f
    move-result v1

    #@30
    const/4 v2, 0x1

    #@31
    move-object v0, p0

    #@32
    move-wide v5, v3

    #@33
    invoke-direct/range {v0 .. v6}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientSearchServiceNative(IZJJ)V

    #@36
    .line 1277
    :goto_36
    return-void

    #@37
    .line 1275
    :cond_37
    const-string v0, "BtGatt.GattService"

    #@39
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v2, "discoverServices() - No connection for "

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    const-string v2, "..."

    #@4a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v1

    #@52
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    goto :goto_36
.end method

.method endReliableWrite(BLjava/lang/String;Z)V
    .registers 8
    .parameter "clientIf"
    .parameter "address"
    .parameter "execute"

    #@0
    .prologue
    .line 1374
    const-string v1, "BtGatt.GattService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "endReliableWrite() - address="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, " execute: "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1377
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mReliableQueue:Ljava/util/Set;

    #@24
    invoke-interface {v1, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    #@27
    .line 1379
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@29
    invoke-virtual {v1, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@2c
    move-result-object v0

    #@2d
    .line 1380
    .local v0, connId:Ljava/lang/Integer;
    if-eqz v0, :cond_36

    #@2f
    .line 1381
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@32
    move-result v1

    #@33
    invoke-direct {p0, v1, p3}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientExecuteWriteNative(IZ)V

    #@36
    .line 1383
    :cond_36
    return-void
.end method

.method endServiceDeclaration(B)V
    .registers 6
    .parameter "serverIf"

    #@0
    .prologue
    .line 1877
    const-string v1, "BtGatt.GattService"

    #@2
    const-string v2, "endServiceDeclaration()"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1880
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService;->getActiveDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@a
    move-result-object v1

    #@b
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService;->getPendingDeclaration()Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@e
    move-result-object v2

    #@f
    if-ne v1, v2, :cond_16

    #@11
    .line 1882
    const/4 v1, 0x0

    #@12
    const/4 v2, 0x0

    #@13
    :try_start_13
    invoke-direct {p0, p1, v1, v2}, Lcom/broadcom/bt/service/gatt/GattService;->continueServiceDeclaration(BBI)V
    :try_end_16
    .catch Landroid/os/RemoteException; {:try_start_13 .. :try_end_16} :catch_17

    #@16
    .line 1887
    :cond_16
    :goto_16
    return-void

    #@17
    .line 1883
    :catch_17
    move-exception v0

    #@18
    .line 1884
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "BtGatt.GattService"

    #@1a
    new-instance v2, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v3, ""

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    goto :goto_16
.end method

.method gattTestCommand(ILjava/util/UUID;Ljava/lang/String;IIIII)V
    .registers 21
    .parameter "command"
    .parameter "uuid1"
    .parameter "bda1"
    .parameter "p1"
    .parameter "p2"
    .parameter "p3"
    .parameter "p4"
    .parameter "p5"

    #@0
    .prologue
    .line 2377
    if-nez p3, :cond_4

    #@2
    .line 2378
    const-string p3, "00:00:00:00:00:00"

    #@4
    .line 2380
    :cond_4
    if-eqz p2, :cond_1f

    #@6
    .line 2381
    invoke-virtual {p2}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@9
    move-result-wide v2

    #@a
    invoke-virtual {p2}, Ljava/util/UUID;->getMostSignificantBits()J

    #@d
    move-result-wide v4

    #@e
    move-object v0, p0

    #@f
    move v1, p1

    #@10
    move-object v6, p3

    #@11
    move/from16 v7, p4

    #@13
    move/from16 v8, p5

    #@15
    move/from16 v9, p6

    #@17
    move/from16 v10, p7

    #@19
    move/from16 v11, p8

    #@1b
    invoke-direct/range {v0 .. v11}, Lcom/broadcom/bt/service/gatt/GattService;->gattTestNative(IJJLjava/lang/String;IIIII)V

    #@1e
    .line 2386
    :goto_1e
    return-void

    #@1f
    .line 2384
    :cond_1f
    const-wide/16 v2, 0x0

    #@21
    const-wide/16 v4, 0x0

    #@23
    move-object v0, p0

    #@24
    move v1, p1

    #@25
    move-object v6, p3

    #@26
    move/from16 v7, p4

    #@28
    move/from16 v8, p5

    #@2a
    move/from16 v9, p6

    #@2c
    move/from16 v10, p7

    #@2e
    move/from16 v11, p8

    #@30
    invoke-direct/range {v0 .. v11}, Lcom/broadcom/bt/service/gatt/GattService;->gattTestNative(IJJLjava/lang/String;IIIII)V

    #@33
    goto :goto_1e
.end method

.method getConnectedDevices()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1252
    new-instance v0, Ljava/util/HashSet;

    #@2
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@5
    .line 1253
    .local v0, connectedDevAddress:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@7
    invoke-virtual {v2}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getConnectedDevices()Ljava/util/Set;

    #@a
    move-result-object v2

    #@b
    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    #@e
    .line 1254
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@10
    invoke-virtual {v2}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->getConnectedDevices()Ljava/util/Set;

    #@13
    move-result-object v2

    #@14
    invoke-interface {v0, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    #@17
    .line 1255
    new-instance v1, Ljava/util/ArrayList;

    #@19
    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    #@1c
    .line 1256
    .local v1, connectedDeviceList:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    return-object v1
.end method

.method getDevicesMatchingConnectionStates([II)Ljava/util/List;
    .registers 20
    .parameter "states"
    .parameter "role"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1091
    const/4 v1, 0x1

    #@1
    .line 1093
    .local v1, DEVICE_TYPE_BREDR:I
    new-instance v8, Ljava/util/HashMap;

    #@3
    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    #@6
    .line 1098
    .local v8, deviceStates:Ljava/util/Map;,"Ljava/util/Map<Landroid/bluetooth/BluetoothDevice;Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    #@8
    iget-object v14, v0, Lcom/broadcom/bt/service/gatt/GattService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@a
    invoke-virtual {v14}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    #@d
    move-result-object v4

    #@e
    .line 1100
    .local v4, bondedDevices:Ljava/util/Set;,"Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v4, :cond_54

    #@10
    .line 1101
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@13
    move-result-object v10

    #@14
    .local v10, i$:Ljava/util/Iterator;
    :cond_14
    :goto_14
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@17
    move-result v14

    #@18
    if-eqz v14, :cond_5b

    #@1a
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1d
    move-result-object v6

    #@1e
    check-cast v6, Landroid/bluetooth/BluetoothDevice;

    #@20
    .line 1102
    .local v6, device:Landroid/bluetooth/BluetoothDevice;
    move-object/from16 v0, p0

    #@22
    invoke-direct {v0, v6}, Lcom/broadcom/bt/service/gatt/GattService;->getDeviceType(Landroid/bluetooth/BluetoothDevice;)I

    #@25
    move-result v14

    #@26
    const/4 v15, 0x1

    #@27
    if-eq v14, v15, :cond_14

    #@29
    .line 1103
    const-string v14, "BtGatt.GattService"

    #@2b
    new-instance v15, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v16, " "

    #@32
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v15

    #@36
    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    #@39
    move-result-object v16

    #@3a
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v15

    #@3e
    const-string v16, "BluetoothProfile.STATE_DISCONNECTED"

    #@40
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v15

    #@44
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v15

    #@48
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 1104
    const/4 v14, 0x0

    #@4c
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4f
    move-result-object v14

    #@50
    invoke-interface {v8, v6, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@53
    goto :goto_14

    #@54
    .line 1109
    .end local v6           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v10           #i$:Ljava/util/Iterator;
    :cond_54
    const-string v14, "BtGatt.GattService"

    #@56
    const-string v15, "getDevicesMatchingConnectionStates(): bondedDevices is null."

    #@58
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 1123
    :cond_5b
    new-instance v5, Ljava/util/HashSet;

    #@5d
    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    #@60
    .line 1124
    .local v5, connectedDevices:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    const-string v14, "BtGatt.GattService"

    #@62
    new-instance v15, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v16, "getDevicesMatchingConnectionStates(): ROLE="

    #@69
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v15

    #@6d
    move/from16 v0, p2

    #@6f
    invoke-virtual {v15, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v15

    #@73
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v15

    #@77
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 1126
    and-int/lit8 v14, p2, 0x1

    #@7c
    const/4 v15, 0x1

    #@7d
    if-ne v14, v15, :cond_8a

    #@7f
    .line 1128
    move-object/from16 v0, p0

    #@81
    iget-object v14, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@83
    invoke-virtual {v14}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getConnectedDevices()Ljava/util/Set;

    #@86
    move-result-object v14

    #@87
    invoke-interface {v5, v14}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    #@8a
    .line 1131
    :cond_8a
    and-int/lit8 v14, p2, 0x2

    #@8c
    const/4 v15, 0x2

    #@8d
    if-ne v14, v15, :cond_9a

    #@8f
    .line 1133
    move-object/from16 v0, p0

    #@91
    iget-object v14, v0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@93
    invoke-virtual {v14}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->getConnectedDevices()Ljava/util/Set;

    #@96
    move-result-object v14

    #@97
    invoke-interface {v5, v14}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    #@9a
    .line 1137
    :cond_9a
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9d
    move-result-object v10

    #@9e
    .restart local v10       #i$:Ljava/util/Iterator;
    :cond_9e
    :goto_9e
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@a1
    move-result v14

    #@a2
    if-eqz v14, :cond_df

    #@a4
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@a7
    move-result-object v2

    #@a8
    check-cast v2, Ljava/lang/String;

    #@aa
    .line 1138
    .local v2, address:Ljava/lang/String;
    move-object/from16 v0, p0

    #@ac
    iget-object v14, v0, Lcom/broadcom/bt/service/gatt/GattService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@ae
    invoke-virtual {v14, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@b1
    move-result-object v6

    #@b2
    .line 1139
    .restart local v6       #device:Landroid/bluetooth/BluetoothDevice;
    if-eqz v6, :cond_9e

    #@b4
    .line 1140
    const-string v14, "BtGatt.GattService"

    #@b6
    new-instance v15, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v16, " "

    #@bd
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v15

    #@c1
    invoke-virtual {v6}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    #@c4
    move-result-object v16

    #@c5
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v15

    #@c9
    const-string v16, "BluetoothProfile.STATE_CONNECTED"

    #@cb
    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v15

    #@cf
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d2
    move-result-object v15

    #@d3
    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d6
    .line 1141
    const/4 v14, 0x2

    #@d7
    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@da
    move-result-object v14

    #@db
    invoke-interface {v8, v6, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@de
    goto :goto_9e

    #@df
    .line 1147
    .end local v2           #address:Ljava/lang/String;
    .end local v6           #device:Landroid/bluetooth/BluetoothDevice;
    :cond_df
    new-instance v7, Ljava/util/ArrayList;

    #@e1
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@e4
    .line 1149
    .local v7, deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v8}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@e7
    move-result-object v14

    #@e8
    invoke-interface {v14}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@eb
    move-result-object v10

    #@ec
    .end local v10           #i$:Ljava/util/Iterator;
    :cond_ec
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@ef
    move-result v14

    #@f0
    if-eqz v14, :cond_116

    #@f2
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f5
    move-result-object v9

    #@f6
    check-cast v9, Ljava/util/Map$Entry;

    #@f8
    .line 1150
    .local v9, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/bluetooth/BluetoothDevice;Ljava/lang/Integer;>;"
    move-object/from16 v3, p1

    #@fa
    .local v3, arr$:[I
    array-length v12, v3

    #@fb
    .local v12, len$:I
    const/4 v11, 0x0

    #@fc
    .local v11, i$:I
    :goto_fc
    if-ge v11, v12, :cond_ec

    #@fe
    aget v13, v3, v11

    #@100
    .line 1151
    .local v13, state:I
    invoke-interface {v9}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@103
    move-result-object v14

    #@104
    check-cast v14, Ljava/lang/Integer;

    #@106
    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    #@109
    move-result v14

    #@10a
    if-ne v14, v13, :cond_113

    #@10c
    .line 1152
    invoke-interface {v9}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@10f
    move-result-object v14

    #@110
    invoke-interface {v7, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@113
    .line 1150
    :cond_113
    add-int/lit8 v11, v11, 0x1

    #@115
    goto :goto_fc

    #@116
    .line 1157
    .end local v3           #arr$:[I
    .end local v9           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/bluetooth/BluetoothDevice;Ljava/lang/Integer;>;"
    .end local v11           #i$:I
    .end local v12           #len$:I
    .end local v13           #state:I
    :cond_116
    return-object v7
.end method

.method protected getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 216
    const-string v0, "BtGatt.GattService"

    #@2
    return-object v0
.end method

.method protected initBinder()Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;
    .registers 2

    #@0
    .prologue
    .line 220
    new-instance v0, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;

    #@2
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;-><init>(Lcom/broadcom/bt/service/gatt/GattService;)V

    #@5
    return-object v0
.end method

.method onAttributeRead(Ljava/lang/String;IIIIZ)V
    .registers 24
    .parameter "address"
    .parameter "connId"
    .parameter "transId"
    .parameter "attrHandle"
    .parameter "offset"
    .parameter "isLong"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1548
    const-string v2, "BtGatt.GattService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "onAttributeRead() connId="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    move/from16 v0, p2

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, ", address="

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    move-object/from16 v0, p1

    #@1b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, ", handle="

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    move/from16 v0, p4

    #@27
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    const-string v4, ", requestId="

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    move/from16 v0, p3

    #@33
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    const-string v4, ", offset="

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    move/from16 v0, p5

    #@3f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 1553
    move-object/from16 v0, p0

    #@4c
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@4e
    move/from16 v0, p4

    #@50
    invoke-virtual {v2, v0}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@53
    move-result-object v15

    #@54
    .line 1554
    .local v15, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    if-nez v15, :cond_57

    #@56
    .line 1626
    :cond_56
    :goto_56
    return-void

    #@57
    .line 1559
    :cond_57
    const-string v2, "BtGatt.GattService"

    #@59
    new-instance v3, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v4, "onAttributeRead() UUID="

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    iget-object v4, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    const-string v4, ", serverIf="

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    iget-byte v4, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@72
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v3

    #@76
    const-string v4, ", type="

    #@78
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v3

    #@7c
    iget-byte v4, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@7e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v3

    #@82
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v3

    #@86
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 1563
    move-object/from16 v0, p0

    #@8b
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@8d
    move/from16 v0, p3

    #@8f
    move/from16 v1, p4

    #@91
    invoke-virtual {v2, v0, v1}, Lcom/broadcom/bt/service/gatt/HandleMap;->addRequest(II)V

    #@94
    .line 1565
    move-object/from16 v0, p0

    #@96
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@98
    iget-byte v3, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@9a
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@9d
    move-result-object v13

    #@9e
    .line 1566
    .local v13, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    if-eqz v13, :cond_56

    #@a0
    .line 1570
    iget-byte v2, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@a2
    packed-switch v2, :pswitch_data_140

    #@a5
    .line 1623
    const-string v2, "BtGatt.GattService"

    #@a7
    const-string v3, "onAttributeRead() - Requested unknown attribute type."

    #@a9
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    goto :goto_56

    #@ad
    .line 1574
    :pswitch_ad
    move-object/from16 v0, p0

    #@af
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@b1
    iget v3, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@b3
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@b6
    move-result-object v16

    #@b7
    .line 1575
    .local v16, serviceEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    if-eqz v16, :cond_e4

    #@b9
    .line 1576
    iget-object v2, v13, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@bb
    check-cast v2, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@bd
    move-object/from16 v0, v16

    #@bf
    iget v7, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceType:I

    #@c1
    move-object/from16 v0, v16

    #@c3
    iget v8, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@c5
    new-instance v9, Landroid/os/ParcelUuid;

    #@c7
    move-object/from16 v0, v16

    #@c9
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@cb
    invoke-direct {v9, v3}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@ce
    iget v10, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@d0
    new-instance v11, Landroid/os/ParcelUuid;

    #@d2
    iget-object v3, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@d4
    invoke-direct {v11, v3}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@d7
    move-object/from16 v3, p1

    #@d9
    move/from16 v4, p3

    #@db
    move/from16 v5, p5

    #@dd
    move/from16 v6, p6

    #@df
    invoke-interface/range {v2 .. v11}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onCharacteristicReadRequest(Ljava/lang/String;IIZIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;)V

    #@e2
    goto/16 :goto_56

    #@e4
    .line 1581
    :cond_e4
    const-string v2, "BtGatt.GattService"

    #@e6
    const-string v3, "onAttributeRead() TYPE_CHARACTERISTIC - serviceEntry is null."

    #@e8
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    goto/16 :goto_56

    #@ed
    .line 1597
    .end local v16           #serviceEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :pswitch_ed
    move-object/from16 v0, p0

    #@ef
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@f1
    iget v3, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@f3
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@f6
    move-result-object v16

    #@f7
    .line 1598
    .restart local v16       #serviceEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    move-object/from16 v0, p0

    #@f9
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@fb
    iget v3, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->charHandle:I

    #@fd
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@100
    move-result-object v14

    #@101
    .line 1599
    .local v14, charEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    if-eqz v16, :cond_137

    #@103
    if-eqz v14, :cond_137

    #@105
    .line 1600
    iget-object v2, v13, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@107
    check-cast v2, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@109
    move-object/from16 v0, v16

    #@10b
    iget v7, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceType:I

    #@10d
    move-object/from16 v0, v16

    #@10f
    iget v8, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@111
    new-instance v9, Landroid/os/ParcelUuid;

    #@113
    move-object/from16 v0, v16

    #@115
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@117
    invoke-direct {v9, v3}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@11a
    iget v10, v14, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@11c
    new-instance v11, Landroid/os/ParcelUuid;

    #@11e
    iget-object v3, v14, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@120
    invoke-direct {v11, v3}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@123
    new-instance v12, Landroid/os/ParcelUuid;

    #@125
    iget-object v3, v15, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@127
    invoke-direct {v12, v3}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@12a
    move-object/from16 v3, p1

    #@12c
    move/from16 v4, p3

    #@12e
    move/from16 v5, p5

    #@130
    move/from16 v6, p6

    #@132
    invoke-interface/range {v2 .. v12}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onDescriptorReadRequest(Ljava/lang/String;IIZIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;)V

    #@135
    goto/16 :goto_56

    #@137
    .line 1606
    :cond_137
    const-string v2, "BtGatt.GattService"

    #@139
    const-string v3, "onAttributeRead() TYPE_DESCRIPTOR - serviceEntry or charEntry is null."

    #@13b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13e
    goto/16 :goto_56

    #@140
    .line 1570
    :pswitch_data_140
    .packed-switch 0x2
        :pswitch_ad
        :pswitch_ed
    .end packed-switch
.end method

.method onAttributeWrite(Ljava/lang/String;IIIIIZZ[B)V
    .registers 30
    .parameter "address"
    .parameter "connId"
    .parameter "transId"
    .parameter "attrHandle"
    .parameter "offset"
    .parameter "length"
    .parameter "needRsp"
    .parameter "isPrep"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1634
    const-string v2, "BtGatt.GattService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "onAttributeWrite() connId="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    move/from16 v0, p2

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, ", address="

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    move-object/from16 v0, p1

    #@1b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, ", handle="

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    move/from16 v0, p4

    #@27
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    const-string v4, ", requestId="

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    move/from16 v0, p3

    #@33
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    const-string v4, ", isPrep="

    #@39
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    move/from16 v0, p8

    #@3f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    const-string v4, ", offset="

    #@45
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    move/from16 v0, p5

    #@4b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v3

    #@53
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 1640
    move-object/from16 v0, p0

    #@58
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@5a
    move/from16 v0, p4

    #@5c
    invoke-virtual {v2, v0}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@5f
    move-result-object v18

    #@60
    .line 1641
    .local v18, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    if-nez v18, :cond_63

    #@62
    .line 1717
    :cond_62
    :goto_62
    return-void

    #@63
    .line 1646
    :cond_63
    const-string v2, "BtGatt.GattService"

    #@65
    new-instance v3, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    const-string v4, "onAttributeWrite() UUID="

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    move-object/from16 v0, v18

    #@72
    iget-object v4, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@74
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v3

    #@78
    const-string v4, ", serverIf="

    #@7a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v3

    #@7e
    move-object/from16 v0, v18

    #@80
    iget-byte v4, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@82
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    const-string v4, ", type="

    #@88
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v3

    #@8c
    move-object/from16 v0, v18

    #@8e
    iget-byte v4, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@90
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@93
    move-result-object v3

    #@94
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@97
    move-result-object v3

    #@98
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9b
    .line 1650
    move-object/from16 v0, p0

    #@9d
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@9f
    move/from16 v0, p3

    #@a1
    move/from16 v1, p4

    #@a3
    invoke-virtual {v2, v0, v1}, Lcom/broadcom/bt/service/gatt/HandleMap;->addRequest(II)V

    #@a6
    .line 1652
    move-object/from16 v0, p0

    #@a8
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@aa
    move-object/from16 v0, v18

    #@ac
    iget-byte v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@ae
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@b1
    move-result-object v16

    #@b2
    .line 1653
    .local v16, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    if-eqz v16, :cond_62

    #@b4
    .line 1657
    move-object/from16 v0, v18

    #@b6
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@b8
    packed-switch v2, :pswitch_data_176

    #@bb
    .line 1714
    const-string v2, "BtGatt.GattService"

    #@bd
    const-string v3, "onAttributeWrite() - Requested unknown attribute type."

    #@bf
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c2
    goto :goto_62

    #@c3
    .line 1661
    :pswitch_c3
    move-object/from16 v0, p0

    #@c5
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@c7
    move-object/from16 v0, v18

    #@c9
    iget v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@cb
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@ce
    move-result-object v19

    #@cf
    .line 1662
    .local v19, serviceEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    if-eqz v19, :cond_108

    #@d1
    .line 1663
    move-object/from16 v0, v16

    #@d3
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@d5
    check-cast v2, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@d7
    move-object/from16 v0, v19

    #@d9
    iget v9, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceType:I

    #@db
    move-object/from16 v0, v19

    #@dd
    iget v10, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@df
    new-instance v11, Landroid/os/ParcelUuid;

    #@e1
    move-object/from16 v0, v19

    #@e3
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@e5
    invoke-direct {v11, v3}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@e8
    move-object/from16 v0, v18

    #@ea
    iget v12, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@ec
    new-instance v13, Landroid/os/ParcelUuid;

    #@ee
    move-object/from16 v0, v18

    #@f0
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@f2
    invoke-direct {v13, v3}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@f5
    move-object/from16 v3, p1

    #@f7
    move/from16 v4, p3

    #@f9
    move/from16 v5, p5

    #@fb
    move/from16 v6, p6

    #@fd
    move/from16 v7, p8

    #@ff
    move/from16 v8, p7

    #@101
    move-object/from16 v14, p9

    #@103
    invoke-interface/range {v2 .. v14}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onCharacteristicWriteRequest(Ljava/lang/String;IIIZZIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;[B)V

    #@106
    goto/16 :goto_62

    #@108
    .line 1669
    :cond_108
    const-string v2, "BtGatt.GattService"

    #@10a
    const-string v3, "onAttributeRead() TYPE_CHARACTERISTIC - serviceEntry or charEntry is null."

    #@10c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10f
    goto/16 :goto_62

    #@111
    .line 1687
    .end local v19           #serviceEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :pswitch_111
    move-object/from16 v0, p0

    #@113
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@115
    move-object/from16 v0, v18

    #@117
    iget v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@119
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@11c
    move-result-object v19

    #@11d
    .line 1688
    .restart local v19       #serviceEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    move-object/from16 v0, p0

    #@11f
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@121
    move-object/from16 v0, v18

    #@123
    iget v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->charHandle:I

    #@125
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@128
    move-result-object v17

    #@129
    .line 1689
    .local v17, charEntry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    if-eqz v19, :cond_16d

    #@12b
    if-eqz v17, :cond_16d

    #@12d
    .line 1690
    move-object/from16 v0, v16

    #@12f
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@131
    check-cast v2, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@133
    move-object/from16 v0, v19

    #@135
    iget v9, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceType:I

    #@137
    move-object/from16 v0, v19

    #@139
    iget v10, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@13b
    new-instance v11, Landroid/os/ParcelUuid;

    #@13d
    move-object/from16 v0, v19

    #@13f
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@141
    invoke-direct {v11, v3}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@144
    move-object/from16 v0, v17

    #@146
    iget v12, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@148
    new-instance v13, Landroid/os/ParcelUuid;

    #@14a
    move-object/from16 v0, v17

    #@14c
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@14e
    invoke-direct {v13, v3}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@151
    new-instance v14, Landroid/os/ParcelUuid;

    #@153
    move-object/from16 v0, v18

    #@155
    iget-object v3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@157
    invoke-direct {v14, v3}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@15a
    move-object/from16 v3, p1

    #@15c
    move/from16 v4, p3

    #@15e
    move/from16 v5, p5

    #@160
    move/from16 v6, p6

    #@162
    move/from16 v7, p8

    #@164
    move/from16 v8, p7

    #@166
    move-object/from16 v15, p9

    #@168
    invoke-interface/range {v2 .. v15}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onDescriptorWriteRequest(Ljava/lang/String;IIIZZIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;[B)V

    #@16b
    goto/16 :goto_62

    #@16d
    .line 1697
    :cond_16d
    const-string v2, "BtGatt.GattService"

    #@16f
    const-string v3, "onAttributeRead() TYPE_CHARACTERISTIC - serviceEntry or charEntry is null."

    #@171
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@174
    goto/16 :goto_62

    #@176
    .line 1657
    :pswitch_data_176
    .packed-switch 0x2
        :pswitch_c3
        :pswitch_111
    .end packed-switch
.end method

.method onCharacteristicAdded(BBJJII)V
    .registers 13
    .parameter "status"
    .parameter "serverIf"
    .parameter "charUuidLsb"
    .parameter "charUuidMsb"
    .parameter "srvcHandle"
    .parameter "charHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1467
    new-instance v0, Ljava/util/UUID;

    #@2
    invoke-direct {v0, p5, p6, p3, p4}, Ljava/util/UUID;-><init>(JJ)V

    #@5
    .line 1469
    .local v0, uuid:Ljava/util/UUID;
    const-string v1, "BtGatt.GattService"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "onCharacteristicAdded() UUID="

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, ", status="

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, ", srvcHandle="

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    const-string v3, ", charHandle="

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1472
    if-nez p1, :cond_42

    #@3d
    .line 1473
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@3f
    invoke-virtual {v1, p2, p8, v0, p7}, Lcom/broadcom/bt/service/gatt/HandleMap;->addCharacteristic(BILjava/util/UUID;I)V

    #@42
    .line 1475
    :cond_42
    invoke-direct {p0, p2, p1, p7}, Lcom/broadcom/bt/service/gatt/GattService;->continueServiceDeclaration(BBI)V

    #@45
    .line 1476
    return-void
.end method

.method onClientConnected(Ljava/lang/String;ZII)V
    .registers 13
    .parameter "address"
    .parameter "connected"
    .parameter "connId"
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1528
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onConnected() connId="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", address="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", connected="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, ", reason="

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 1532
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@38
    iget-object v0, v0, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->mApps:Ljava/util/List;

    #@3a
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@3d
    move-result-object v7

    #@3e
    .line 1533
    .local v7, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;>;"
    :goto_3e
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@41
    move-result v0

    #@42
    if-eqz v0, :cond_69

    #@44
    .line 1534
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@47
    move-result-object v6

    #@48
    check-cast v6, Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@4a
    .line 1535
    .local v6, entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    if-eqz p2, :cond_61

    #@4c
    .line 1536
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@4e
    iget-byte v1, v6, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@50
    invoke-virtual {v0, v1, p3, p1}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->addConnection(BILjava/lang/String;)V

    #@53
    .line 1540
    :goto_53
    iget-object v0, v6, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@55
    check-cast v0, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@57
    const/4 v1, 0x0

    #@58
    iget-byte v2, v6, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@5a
    move v3, p2

    #@5b
    move-object v4, p1

    #@5c
    move v5, p4

    #@5d
    invoke-interface/range {v0 .. v5}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onServerConnectionState(BBZLjava/lang/String;I)V

    #@60
    goto :goto_3e

    #@61
    .line 1538
    :cond_61
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@63
    iget-byte v1, v6, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@65
    invoke-virtual {v0, v1, p3}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->removeConnection(BI)V

    #@68
    goto :goto_53

    #@69
    .line 1542
    .end local v6           #entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    :cond_69
    return-void
.end method

.method onClientRegistered(BBJJ)V
    .registers 15
    .parameter "status"
    .parameter "clientIf"
    .parameter "uuidLsb"
    .parameter "uuidMsb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 735
    new-instance v4, Ljava/util/UUID;

    #@2
    invoke-direct {v4, p5, p6, p3, p4}, Ljava/util/UUID;-><init>(JJ)V

    #@5
    .line 737
    .local v4, uuid:Ljava/util/UUID;
    const-string v5, "BtGatt.GattService"

    #@7
    new-instance v6, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v7, "onClientRegistered() - UUID="

    #@e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v6

    #@12
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v6

    #@16
    const-string v7, ", clientIf="

    #@18
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v6

    #@1c
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v6

    #@24
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 739
    iget-object v5, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@29
    invoke-virtual {v5, v4}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByUuid(Ljava/util/UUID;)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@2c
    move-result-object v0

    #@2d
    .line 740
    .local v0, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v0, :cond_7f

    #@2f
    .line 741
    iput-byte p2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@31
    .line 742
    new-instance v5, Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;

    #@33
    invoke-direct {v5, p0, p2}, Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;-><init>(Lcom/broadcom/bt/service/gatt/GattService;B)V

    #@36
    invoke-virtual {v0, v5}, Lcom/broadcom/bt/service/gatt/ContextMap$App;->linkToDeath(Landroid/os/IBinder$DeathRecipient;)V

    #@39
    .line 743
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@3b
    check-cast v5, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@3d
    invoke-interface {v5, p1, p2}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onClientRegistered(BB)V

    #@40
    .line 745
    iget-object v5, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@42
    invoke-virtual {v5, p2}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getConnectionByApp(I)Ljava/util/List;

    #@45
    move-result-object v2

    #@46
    .line 747
    .local v2, connections:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.Connection;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@49
    move-result v5

    #@4a
    if-lez v5, :cond_7f

    #@4c
    .line 748
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@4f
    move-result-object v3

    #@50
    .line 749
    .local v3, ii:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.Connection;>;"
    :goto_50
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@53
    move-result v5

    #@54
    if-eqz v5, :cond_7f

    #@56
    .line 750
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@59
    move-result-object v1

    #@5a
    check-cast v1, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;

    #@5c
    .line 751
    .local v1, connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.Connection;"
    const-string v5, "BtGatt.GattService"

    #@5e
    new-instance v6, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v7, "Notify connect to setting() addr= "

    #@65
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    iget-object v7, v1, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->address:Ljava/lang/String;

    #@6b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v6

    #@6f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v6

    #@73
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 752
    iget v5, v1, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->connId:I

    #@78
    const/4 v6, 0x0

    #@79
    iget-object v7, v1, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->address:Ljava/lang/String;

    #@7b
    invoke-virtual {p0, p2, v5, v6, v7}, Lcom/broadcom/bt/service/gatt/GattService;->onConnected(BIBLjava/lang/String;)V

    #@7e
    goto :goto_50

    #@7f
    .line 756
    .end local v1           #connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.Connection;"
    .end local v2           #connections:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.Connection;>;"
    .end local v3           #ii:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.Connection;>;"
    :cond_7f
    return-void
.end method

.method onConnected(BIBLjava/lang/String;)V
    .registers 9
    .parameter "clientIf"
    .parameter "connId"
    .parameter "status"
    .parameter "address"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 761
    const-string v1, "BtGatt.GattService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "onConnected() - clientIf="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, ", connId="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ", address="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 764
    if-nez p3, :cond_33

    #@2e
    .line 765
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@30
    invoke-virtual {v1, p1, p2, p4}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addConnection(BILjava/lang/String;)V

    #@33
    .line 767
    :cond_33
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@35
    invoke-virtual {v1, p1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@38
    move-result-object v0

    #@39
    .line 768
    .local v0, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v0, :cond_43

    #@3b
    .line 769
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@3d
    check-cast v1, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@3f
    const/4 v2, 0x1

    #@40
    invoke-interface {v1, p3, p1, v2, p4}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onClientConnectionState(BBZLjava/lang/String;)V

    #@43
    .line 771
    :cond_43
    return-void
.end method

.method onDescriptorAdded(BBJJII)V
    .registers 13
    .parameter "status"
    .parameter "serverIf"
    .parameter "descrUuidLsb"
    .parameter "descrUuidMsb"
    .parameter "srvcHandle"
    .parameter "descrHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1482
    new-instance v0, Ljava/util/UUID;

    #@2
    invoke-direct {v0, p5, p6, p3, p4}, Ljava/util/UUID;-><init>(JJ)V

    #@5
    .line 1484
    .local v0, uuid:Ljava/util/UUID;
    const-string v1, "BtGatt.GattService"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "onDescriptorAdded() UUID="

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    const-string v3, ", status="

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, ", srvcHandle="

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    const-string v3, ", descrHandle="

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, p8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 1487
    if-nez p1, :cond_42

    #@3d
    .line 1488
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@3f
    invoke-virtual {v1, p2, p8, v0, p7}, Lcom/broadcom/bt/service/gatt/HandleMap;->addDescriptor(BILjava/util/UUID;I)V

    #@42
    .line 1490
    :cond_42
    invoke-direct {p0, p2, p1, p7}, Lcom/broadcom/bt/service/gatt/GattService;->continueServiceDeclaration(BBI)V

    #@45
    .line 1491
    return-void
.end method

.method onDisconnected(BIBLjava/lang/String;I)V
    .registers 13
    .parameter "clientIf"
    .parameter "connId"
    .parameter "status"
    .parameter "address"
    .parameter "reason"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 776
    const-string v0, "BtGatt.GattService"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "onDisconnected() - clientIf="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ", connId="

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, ", address="

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    const-string v2, ", reason="

    #@28
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v1

    #@34
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 779
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@39
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->removeConnection(BI)V

    #@3c
    .line 780
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mSearchQueue:Lcom/broadcom/bt/service/gatt/SearchQueue;

    #@3e
    invoke-virtual {v0, p2}, Lcom/broadcom/bt/service/gatt/SearchQueue;->removeConnId(I)V

    #@41
    .line 781
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@43
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@46
    move-result-object v6

    #@47
    .line 782
    .local v6, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v6, :cond_5b

    #@49
    .line 783
    iget-object v0, v6, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@4b
    check-cast v0, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@4d
    invoke-interface {v0, p3, p1, v3, p4}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onClientConnectionState(BBZLjava/lang/String;)V

    #@50
    .line 784
    iget-object v0, v6, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@52
    check-cast v0, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@54
    move v1, p3

    #@55
    move v2, p1

    #@56
    move-object v4, p4

    #@57
    move v5, p5

    #@58
    invoke-interface/range {v0 .. v5}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onClientConnectionStatewithReason(BBZLjava/lang/String;I)V

    #@5b
    .line 786
    :cond_5b
    return-void
.end method

.method onExecuteCompleted(II)V
    .registers 8
    .parameter "connId"
    .parameter "status"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1012
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@2
    invoke-virtual {v2, p1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 1014
    .local v0, address:Ljava/lang/String;
    const-string v2, "BtGatt.GattService"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "onExecuteCompleted() - address="

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    const-string v4, ", status="

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1017
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@2a
    invoke-virtual {v2, p1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@2d
    move-result-object v1

    #@2e
    .line 1018
    .local v1, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v1, :cond_37

    #@30
    .line 1019
    iget-object v2, v1, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@32
    check-cast v2, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@34
    invoke-interface {v2, v0, p2}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onExecuteWrite(Ljava/lang/String;I)V

    #@37
    .line 1021
    :cond_37
    return-void
.end method

.method onExecuteWrite(Ljava/lang/String;III)V
    .registers 10
    .parameter "address"
    .parameter "connId"
    .parameter "transId"
    .parameter "execWrite"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1722
    const-string v1, "BtGatt.GattService"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "onExecuteWrite() connId="

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    const-string v4, ", address="

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    const-string v4, ", transId="

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 1725
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@2f
    invoke-virtual {v1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@32
    move-result-object v0

    #@33
    .line 1726
    .local v0, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    if-nez v0, :cond_36

    #@35
    .line 1731
    :goto_35
    return-void

    #@36
    .line 1730
    :cond_36
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@38
    check-cast v1, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@3a
    if-ne p4, v2, :cond_40

    #@3c
    :goto_3c
    invoke-interface {v1, p1, p3, v2}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onExecuteWrite(Ljava/lang/String;IZ)V

    #@3f
    goto :goto_35

    #@40
    :cond_40
    const/4 v2, 0x0

    #@41
    goto :goto_3c
.end method

.method onGetCharacteristic(IIIIJJIJJI)V
    .registers 38
    .parameter "connId"
    .parameter "status"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "charInstId"
    .parameter "charUuidLsb"
    .parameter "charUuidMsb"
    .parameter "charProp"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 820
    new-instance v22, Ljava/util/UUID;

    #@2
    move-object/from16 v0, v22

    #@4
    move-wide/from16 v1, p7

    #@6
    move-wide/from16 v3, p5

    #@8
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@b
    .line 821
    .local v22, srvcUuid:Ljava/util/UUID;
    new-instance v21, Ljava/util/UUID;

    #@d
    move-object/from16 v0, v21

    #@f
    move-wide/from16 v1, p12

    #@11
    move-wide/from16 v3, p10

    #@13
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@16
    .line 822
    .local v21, charUuid:Ljava/util/UUID;
    move-object/from16 v0, p0

    #@18
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@1a
    move/from16 v0, p1

    #@1c
    invoke-virtual {v5, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@1f
    move-result-object v19

    #@20
    .line 825
    .local v19, address:Ljava/lang/String;
    const-string v5, "BtGatt.GattService"

    #@22
    new-instance v6, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v7, "onGetCharacteristic() - address="

    #@29
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v6

    #@2d
    move-object/from16 v0, v19

    #@2f
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    const-string v7, ", status="

    #@35
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    move/from16 v0, p2

    #@3b
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, ", charUuid="

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    move-object/from16 v0, v21

    #@47
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    const-string v7, ", prop="

    #@4d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v6

    #@51
    move/from16 v0, p14

    #@53
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v6

    #@57
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v6

    #@5b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 828
    if-nez p2, :cond_ba

    #@60
    .line 829
    move-object/from16 v0, p0

    #@62
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mSearchQueue:Lcom/broadcom/bt/service/gatt/SearchQueue;

    #@64
    move/from16 v6, p1

    #@66
    move/from16 v7, p3

    #@68
    move/from16 v8, p4

    #@6a
    move-wide/from16 v9, p5

    #@6c
    move-wide/from16 v11, p7

    #@6e
    move/from16 v13, p9

    #@70
    move-wide/from16 v14, p10

    #@72
    move-wide/from16 v16, p12

    #@74
    invoke-virtual/range {v5 .. v17}, Lcom/broadcom/bt/service/gatt/SearchQueue;->add(IIIJJIJJ)V

    #@77
    .line 833
    move-object/from16 v0, p0

    #@79
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@7b
    move/from16 v0, p1

    #@7d
    invoke-virtual {v5, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@80
    move-result-object v20

    #@81
    .line 834
    .local v20, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v20, :cond_a4

    #@83
    .line 835
    move-object/from16 v0, v20

    #@85
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@87
    check-cast v5, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@89
    new-instance v9, Landroid/os/ParcelUuid;

    #@8b
    move-object/from16 v0, v22

    #@8d
    invoke-direct {v9, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@90
    new-instance v11, Landroid/os/ParcelUuid;

    #@92
    move-object/from16 v0, v21

    #@94
    invoke-direct {v11, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@97
    move-object/from16 v6, v19

    #@99
    move/from16 v7, p3

    #@9b
    move/from16 v8, p4

    #@9d
    move/from16 v10, p9

    #@9f
    move/from16 v12, p14

    #@a1
    invoke-interface/range {v5 .. v12}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onGetCharacteristic(Ljava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;I)V

    #@a4
    :cond_a4
    move-object/from16 v5, p0

    #@a6
    move/from16 v6, p1

    #@a8
    move/from16 v7, p3

    #@aa
    move/from16 v8, p4

    #@ac
    move-wide/from16 v9, p5

    #@ae
    move-wide/from16 v11, p7

    #@b0
    move/from16 v13, p9

    #@b2
    move-wide/from16 v14, p10

    #@b4
    move-wide/from16 v16, p12

    #@b6
    .line 841
    invoke-direct/range {v5 .. v17}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientGetCharacteristicNative(IIIJJIJJ)V

    #@b9
    .line 850
    .end local v20           #app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    :goto_b9
    return-void

    #@ba
    .line 846
    :cond_ba
    const/4 v13, 0x0

    #@bb
    const/4 v14, 0x0

    #@bc
    const-wide/16 v15, 0x0

    #@be
    const-wide/16 v17, 0x0

    #@c0
    move-object/from16 v5, p0

    #@c2
    move/from16 v6, p1

    #@c4
    move/from16 v7, p3

    #@c6
    move/from16 v8, p4

    #@c8
    move-wide/from16 v9, p5

    #@ca
    move-wide/from16 v11, p7

    #@cc
    invoke-direct/range {v5 .. v18}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientGetIncludedServiceNative(IIIJJIIJJ)V

    #@cf
    goto :goto_b9
.end method

.method onGetDescriptor(IIIIJJIJJJJ)V
    .registers 46
    .parameter "connId"
    .parameter "status"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "charInstId"
    .parameter "charUuidLsb"
    .parameter "charUuidMsb"
    .parameter "descrUuidLsb"
    .parameter "descrUuidMsb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 857
    new-instance v27, Ljava/util/UUID;

    #@2
    move-object/from16 v0, v27

    #@4
    move-wide/from16 v1, p7

    #@6
    move-wide/from16 v3, p5

    #@8
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@b
    .line 858
    .local v27, srvcUuid:Ljava/util/UUID;
    new-instance v25, Ljava/util/UUID;

    #@d
    move-object/from16 v0, v25

    #@f
    move-wide/from16 v1, p12

    #@11
    move-wide/from16 v3, p10

    #@13
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@16
    .line 859
    .local v25, charUuid:Ljava/util/UUID;
    new-instance v26, Ljava/util/UUID;

    #@18
    move-object/from16 v0, v26

    #@1a
    move-wide/from16 v1, p16

    #@1c
    move-wide/from16 v3, p14

    #@1e
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@21
    .line 860
    .local v26, descUuid:Ljava/util/UUID;
    move-object/from16 v0, p0

    #@23
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@25
    move/from16 v0, p1

    #@27
    invoke-virtual {v5, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@2a
    move-result-object v6

    #@2b
    .line 863
    .local v6, address:Ljava/lang/String;
    const-string v5, "BtGatt.GattService"

    #@2d
    new-instance v7, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v8, "onGetDescriptor() - address="

    #@34
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v7

    #@38
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v7

    #@3c
    const-string v8, ", status="

    #@3e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v7

    #@42
    move/from16 v0, p2

    #@44
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v7

    #@48
    const-string v8, ", descUuid="

    #@4a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v7

    #@4e
    move-object/from16 v0, v26

    #@50
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v7

    #@58
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 866
    if-nez p2, :cond_a7

    #@5d
    .line 867
    move-object/from16 v0, p0

    #@5f
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@61
    move/from16 v0, p1

    #@63
    invoke-virtual {v5, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@66
    move-result-object v24

    #@67
    .line 868
    .local v24, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v24, :cond_8d

    #@69
    .line 869
    move-object/from16 v0, v24

    #@6b
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@6d
    check-cast v5, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@6f
    new-instance v9, Landroid/os/ParcelUuid;

    #@71
    move-object/from16 v0, v27

    #@73
    invoke-direct {v9, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@76
    new-instance v11, Landroid/os/ParcelUuid;

    #@78
    move-object/from16 v0, v25

    #@7a
    invoke-direct {v11, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@7d
    new-instance v12, Landroid/os/ParcelUuid;

    #@7f
    move-object/from16 v0, v26

    #@81
    invoke-direct {v12, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@84
    move/from16 v7, p3

    #@86
    move/from16 v8, p4

    #@88
    move/from16 v10, p9

    #@8a
    invoke-interface/range {v5 .. v12}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onGetDescriptor(Ljava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;)V

    #@8d
    :cond_8d
    move-object/from16 v7, p0

    #@8f
    move/from16 v8, p1

    #@91
    move/from16 v9, p3

    #@93
    move/from16 v10, p4

    #@95
    move-wide/from16 v11, p5

    #@97
    move-wide/from16 v13, p7

    #@99
    move/from16 v15, p9

    #@9b
    move-wide/from16 v16, p10

    #@9d
    move-wide/from16 v18, p12

    #@9f
    move-wide/from16 v20, p14

    #@a1
    move-wide/from16 v22, p16

    #@a3
    .line 876
    invoke-direct/range {v7 .. v23}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientGetDescriptorNative(IIIJJIJJJJ)V

    #@a6
    .line 884
    .end local v24           #app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    :goto_a6
    return-void

    #@a7
    .line 882
    :cond_a7
    const/4 v5, 0x0

    #@a8
    move-object/from16 v0, p0

    #@aa
    move/from16 v1, p1

    #@ac
    invoke-direct {v0, v1, v5}, Lcom/broadcom/bt/service/gatt/GattService;->continueSearch(II)V

    #@af
    goto :goto_a6
.end method

.method onGetIncludedService(IIIIJJIIJJ)V
    .registers 39
    .parameter "connId"
    .parameter "status"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "inclSrvcType"
    .parameter "inclSrvcInstId"
    .parameter "inclSrvcUuidLsb"
    .parameter "inclSrvcUuidMsb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 890
    new-instance v23, Ljava/util/UUID;

    #@2
    move-object/from16 v0, v23

    #@4
    move-wide/from16 v1, p7

    #@6
    move-wide/from16 v3, p5

    #@8
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@b
    .line 891
    .local v23, srvcUuid:Ljava/util/UUID;
    new-instance v22, Ljava/util/UUID;

    #@d
    move-object/from16 v0, v22

    #@f
    move-wide/from16 v1, p13

    #@11
    move-wide/from16 v3, p11

    #@13
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@16
    .line 892
    .local v22, inclSrvcUuid:Ljava/util/UUID;
    move-object/from16 v0, p0

    #@18
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@1a
    move/from16 v0, p1

    #@1c
    invoke-virtual {v5, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@1f
    move-result-object v6

    #@20
    .line 895
    .local v6, address:Ljava/lang/String;
    const-string v5, "BtGatt.GattService"

    #@22
    new-instance v7, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v8, "onGetIncludedService() - address="

    #@29
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v7

    #@2d
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v7

    #@31
    const-string v8, ", status="

    #@33
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v7

    #@37
    move/from16 v0, p2

    #@39
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v7

    #@3d
    const-string v8, ", uuid="

    #@3f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    move-object/from16 v0, v23

    #@45
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v7

    #@49
    const-string v8, ", inclUuid="

    #@4b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v7

    #@4f
    move-object/from16 v0, v22

    #@51
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v7

    #@59
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 901
    if-nez p2, :cond_a1

    #@5e
    .line 902
    move-object/from16 v0, p0

    #@60
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@62
    move/from16 v0, p1

    #@64
    invoke-virtual {v5, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@67
    move-result-object v21

    #@68
    .line 903
    .local v21, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v21, :cond_89

    #@6a
    .line 904
    move-object/from16 v0, v21

    #@6c
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@6e
    check-cast v5, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@70
    new-instance v9, Landroid/os/ParcelUuid;

    #@72
    move-object/from16 v0, v23

    #@74
    invoke-direct {v9, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@77
    new-instance v12, Landroid/os/ParcelUuid;

    #@79
    move-object/from16 v0, v22

    #@7b
    invoke-direct {v12, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@7e
    move/from16 v7, p3

    #@80
    move/from16 v8, p4

    #@82
    move/from16 v10, p9

    #@84
    move/from16 v11, p10

    #@86
    invoke-interface/range {v5 .. v12}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onGetIncludedService(Ljava/lang/String;IILandroid/os/ParcelUuid;IILandroid/os/ParcelUuid;)V

    #@89
    :cond_89
    move-object/from16 v7, p0

    #@8b
    move/from16 v8, p1

    #@8d
    move/from16 v9, p3

    #@8f
    move/from16 v10, p4

    #@91
    move-wide/from16 v11, p5

    #@93
    move-wide/from16 v13, p7

    #@95
    move/from16 v15, p9

    #@97
    move/from16 v16, p10

    #@99
    move-wide/from16 v17, p11

    #@9b
    move-wide/from16 v19, p13

    #@9d
    .line 910
    invoke-direct/range {v7 .. v20}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientGetIncludedServiceNative(IIIJJIIJJ)V

    #@a0
    .line 917
    .end local v21           #app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    :goto_a0
    return-void

    #@a1
    .line 915
    :cond_a1
    const/4 v5, 0x0

    #@a2
    move-object/from16 v0, p0

    #@a4
    move/from16 v1, p1

    #@a6
    invoke-direct {v0, v1, v5}, Lcom/broadcom/bt/service/gatt/GattService;->continueSearch(II)V

    #@a9
    goto :goto_a0
.end method

.method onIncludedServiceAdded(BBII)V
    .registers 8
    .parameter "status"
    .parameter "serverIf"
    .parameter "srvcHandle"
    .parameter "includedSrvcHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1457
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onIncludedServiceAdded() status="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", service="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", included="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 1460
    invoke-direct {p0, p2, p1, p3}, Lcom/broadcom/bt/service/gatt/GattService;->continueServiceDeclaration(BBI)V

    #@2f
    .line 1461
    return-void
.end method

.method onMtuExchange(Ljava/lang/String;III)V
    .registers 8
    .parameter "address"
    .parameter "connId"
    .parameter "transId"
    .parameter "mtuSize"

    #@0
    .prologue
    .line 1741
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onMtuExchange() - address="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", mtuSize="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1743
    return-void
.end method

.method onNotify(ILjava/lang/String;IIJJIJJZ[B)V
    .registers 33
    .parameter "connId"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "charInstId"
    .parameter "charUuidLsb"
    .parameter "charUuidMsb"
    .parameter "isNotify"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 937
    new-instance v15, Ljava/util/UUID;

    #@2
    move-wide/from16 v0, p7

    #@4
    move-wide/from16 v2, p5

    #@6
    invoke-direct {v15, v0, v1, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    #@9
    .line 938
    .local v15, srvcUuid:Ljava/util/UUID;
    new-instance v13, Ljava/util/UUID;

    #@b
    move-wide/from16 v0, p12

    #@d
    move-wide/from16 v2, p10

    #@f
    invoke-direct {v13, v0, v1, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    #@12
    .line 939
    .local v13, charUuid:Ljava/util/UUID;
    new-instance v16, Ljava/util/UUID;

    #@14
    const-wide/16 v4, 0x0

    #@16
    move-object/from16 v0, v16

    #@18
    move-wide/from16 v1, p7

    #@1a
    invoke-direct {v0, v1, v2, v4, v5}, Ljava/util/UUID;-><init>(JJ)V

    #@1d
    .line 942
    .local v16, srvcUuidforOneKey:Ljava/util/UUID;
    const-string v4, "BtGatt.GattService"

    #@1f
    new-instance v5, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v6, "onNotify() - address="

    #@26
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v5

    #@2a
    move-object/from16 v0, p2

    #@2c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    const-string v6, ", charUuid="

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    const-string v6, ", length="

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    move-object/from16 v0, p15

    #@42
    array-length v6, v0

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v5

    #@4b
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 946
    move-object/from16 v0, p0

    #@50
    iget-object v4, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@52
    move/from16 v0, p1

    #@54
    invoke-virtual {v4, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@57
    move-result-object v12

    #@58
    .line 947
    .local v12, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v12, :cond_75

    #@5a
    .line 949
    :try_start_5a
    iget-object v4, v12, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@5c
    check-cast v4, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@5e
    new-instance v8, Landroid/os/ParcelUuid;

    #@60
    invoke-direct {v8, v15}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@63
    new-instance v10, Landroid/os/ParcelUuid;

    #@65
    invoke-direct {v10, v13}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@68
    move-object/from16 v5, p2

    #@6a
    move/from16 v6, p3

    #@6c
    move/from16 v7, p4

    #@6e
    move/from16 v9, p9

    #@70
    move-object/from16 v11, p15

    #@72
    invoke-interface/range {v4 .. v11}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onNotify(Ljava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;[B)V
    :try_end_75
    .catch Landroid/os/RemoteException; {:try_start_5a .. :try_end_75} :catch_76

    #@75
    .line 966
    :cond_75
    :goto_75
    return-void

    #@76
    .line 953
    :catch_76
    move-exception v14

    #@77
    .line 954
    .local v14, e:Landroid/os/RemoteException;
    const-string v4, "BtGatt.GattService"

    #@79
    new-instance v5, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v6, "Error calling onNotify "

    #@80
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v5

    #@84
    invoke-virtual {v14}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@87
    move-result-object v6

    #@88
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v5

    #@8c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v5

    #@90
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 955
    invoke-virtual {v14}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    #@96
    move-result-object v4

    #@97
    const-string v5, "android.os.DeadObjectException"

    #@99
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v4

    #@9d
    if-eqz v4, :cond_75

    #@9f
    .line 956
    const-string v4, "BtGatt.GattService"

    #@a1
    new-instance v5, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v6, "srvcUuidforOneKey="

    #@a8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v5

    #@ac
    move-object/from16 v0, v16

    #@ae
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v5

    #@b2
    const-string v6, " oneKeyUuid="

    #@b4
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v5

    #@b8
    move-object/from16 v0, p0

    #@ba
    iget-object v6, v0, Lcom/broadcom/bt/service/gatt/GattService;->oneKeyuuid:Ljava/util/UUID;

    #@bc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v5

    #@c0
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v5

    #@c4
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c7
    .line 957
    move-object/from16 v0, p0

    #@c9
    iget-object v4, v0, Lcom/broadcom/bt/service/gatt/GattService;->oneKeyuuid:Ljava/util/UUID;

    #@cb
    move-object/from16 v0, v16

    #@cd
    invoke-virtual {v4, v0}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    #@d0
    move-result v4

    #@d1
    if-eqz v4, :cond_75

    #@d3
    .line 959
    const-string v4, "BtGatt.GattService"

    #@d5
    const-string v5, "onNitify: It seems Settings is dead. Do One key action here"

    #@d7
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@da
    .line 961
    move-object/from16 v0, p0

    #@dc
    move-object/from16 v1, p15

    #@de
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/gatt/GattService;->OneKeyExceptionHandle([B)V

    #@e1
    goto :goto_75
.end method

.method onReadCharacteristic(IIIIJJIJJI[B)V
    .registers 32
    .parameter "connId"
    .parameter "status"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "charInstId"
    .parameter "charUuidLsb"
    .parameter "charUuidMsb"
    .parameter "charType"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 973
    new-instance v15, Ljava/util/UUID;

    #@2
    move-wide/from16 v0, p7

    #@4
    move-wide/from16 v2, p5

    #@6
    invoke-direct {v15, v0, v1, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    #@9
    .line 974
    .local v15, srvcUuid:Ljava/util/UUID;
    new-instance v14, Ljava/util/UUID;

    #@b
    move-wide/from16 v0, p12

    #@d
    move-wide/from16 v2, p10

    #@f
    invoke-direct {v14, v0, v1, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    #@12
    .line 975
    .local v14, charUuid:Ljava/util/UUID;
    move-object/from16 v0, p0

    #@14
    iget-object v4, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@16
    move/from16 v0, p1

    #@18
    invoke-virtual {v4, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@1b
    move-result-object v5

    #@1c
    .line 978
    .local v5, address:Ljava/lang/String;
    const-string v4, "BtGatt.GattService"

    #@1e
    new-instance v6, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v7, "onReadCharacteristic() - address="

    #@25
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v6

    #@29
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v6

    #@2d
    const-string v7, ", status="

    #@2f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    move/from16 v0, p2

    #@35
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    const-string v7, ", length="

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    move-object/from16 v0, p15

    #@41
    array-length v7, v0

    #@42
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v6

    #@4a
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 982
    move-object/from16 v0, p0

    #@4f
    iget-object v4, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@51
    move/from16 v0, p1

    #@53
    invoke-virtual {v4, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@56
    move-result-object v13

    #@57
    .line 983
    .local v13, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v13, :cond_74

    #@59
    .line 984
    iget-object v4, v13, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@5b
    check-cast v4, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@5d
    new-instance v9, Landroid/os/ParcelUuid;

    #@5f
    invoke-direct {v9, v15}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@62
    new-instance v11, Landroid/os/ParcelUuid;

    #@64
    invoke-direct {v11, v14}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@67
    move/from16 v6, p2

    #@69
    move/from16 v7, p3

    #@6b
    move/from16 v8, p4

    #@6d
    move/from16 v10, p9

    #@6f
    move-object/from16 v12, p15

    #@71
    invoke-interface/range {v4 .. v12}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onCharacteristicRead(Ljava/lang/String;IIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;[B)V

    #@74
    .line 988
    :cond_74
    return-void
.end method

.method onReadDescriptor(IIIIJJIJJJJI[B)V
    .registers 39
    .parameter "connId"
    .parameter "status"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "charInstId"
    .parameter "charUuidLsb"
    .parameter "charUuidMsb"
    .parameter "descrUuidLsb"
    .parameter "descrUuidMsb"
    .parameter "charType"
    .parameter "data"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1029
    new-instance v18, Ljava/util/UUID;

    #@2
    move-object/from16 v0, v18

    #@4
    move-wide/from16 v1, p7

    #@6
    move-wide/from16 v3, p5

    #@8
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@b
    .line 1030
    .local v18, srvcUuid:Ljava/util/UUID;
    new-instance v16, Ljava/util/UUID;

    #@d
    move-object/from16 v0, v16

    #@f
    move-wide/from16 v1, p12

    #@11
    move-wide/from16 v3, p10

    #@13
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@16
    .line 1031
    .local v16, charUuid:Ljava/util/UUID;
    new-instance v17, Ljava/util/UUID;

    #@18
    move-object/from16 v0, v17

    #@1a
    move-wide/from16 v1, p16

    #@1c
    move-wide/from16 v3, p14

    #@1e
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@21
    .line 1032
    .local v17, descrUuid:Ljava/util/UUID;
    move-object/from16 v0, p0

    #@23
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@25
    move/from16 v0, p1

    #@27
    invoke-virtual {v5, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@2a
    move-result-object v6

    #@2b
    .line 1035
    .local v6, address:Ljava/lang/String;
    const-string v5, "BtGatt.GattService"

    #@2d
    new-instance v7, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v8, "onReadDescriptor() - address="

    #@34
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v7

    #@38
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v7

    #@3c
    const-string v8, ", status="

    #@3e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v7

    #@42
    move/from16 v0, p2

    #@44
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v7

    #@48
    const-string v8, ", length="

    #@4a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v7

    #@4e
    move-object/from16 v0, p19

    #@50
    array-length v8, v0

    #@51
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v7

    #@55
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v7

    #@59
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 1039
    move-object/from16 v0, p0

    #@5e
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@60
    move/from16 v0, p1

    #@62
    invoke-virtual {v5, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@65
    move-result-object v15

    #@66
    .line 1040
    .local v15, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v15, :cond_8e

    #@68
    .line 1041
    iget-object v5, v15, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@6a
    check-cast v5, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@6c
    new-instance v10, Landroid/os/ParcelUuid;

    #@6e
    move-object/from16 v0, v18

    #@70
    invoke-direct {v10, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@73
    new-instance v12, Landroid/os/ParcelUuid;

    #@75
    move-object/from16 v0, v16

    #@77
    invoke-direct {v12, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@7a
    new-instance v13, Landroid/os/ParcelUuid;

    #@7c
    move-object/from16 v0, v17

    #@7e
    invoke-direct {v13, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@81
    move/from16 v7, p2

    #@83
    move/from16 v8, p3

    #@85
    move/from16 v9, p4

    #@87
    move/from16 v11, p9

    #@89
    move-object/from16 v14, p19

    #@8b
    invoke-interface/range {v5 .. v14}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onDescriptorRead(Ljava/lang/String;IIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;[B)V

    #@8e
    .line 1046
    :cond_8e
    return-void
.end method

.method onReadRemoteRssi(BLjava/lang/String;BI)V
    .registers 9
    .parameter "clientIf"
    .parameter "address"
    .parameter "rssi"
    .parameter "status"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1074
    const-string v1, "BtGatt.GattService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "onReadRemoteRssi() - clientIf="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, " address="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ", rssi="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, ", status="

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 1077
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@38
    invoke-virtual {v1, p1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@3b
    move-result-object v0

    #@3c
    .line 1078
    .local v0, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v0, :cond_45

    #@3e
    .line 1079
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@40
    check-cast v1, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@42
    invoke-interface {v1, p2, p3, p4}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onReadRemoteRssi(Ljava/lang/String;II)V

    #@45
    .line 1081
    :cond_45
    return-void
.end method

.method onRegisterForNotifications(IIIIIJJIJJ)V
    .registers 25
    .parameter "connId"
    .parameter "status"
    .parameter "registered"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "charInstId"
    .parameter "charUuidLsb"
    .parameter "charUuidMsb"

    #@0
    .prologue
    .line 922
    new-instance v6, Ljava/util/UUID;

    #@2
    move-wide/from16 v0, p8

    #@4
    move-wide/from16 v2, p6

    #@6
    invoke-direct {v6, v0, v1, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    #@9
    .line 923
    .local v6, srvcUuid:Ljava/util/UUID;
    new-instance v5, Ljava/util/UUID;

    #@b
    move-wide/from16 v0, p13

    #@d
    move-wide/from16 v2, p11

    #@f
    invoke-direct {v5, v0, v1, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    #@12
    .line 924
    .local v5, charUuid:Ljava/util/UUID;
    iget-object v7, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@14
    invoke-virtual {v7, p1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    .line 927
    .local v4, address:Ljava/lang/String;
    const-string v7, "BtGatt.GattService"

    #@1a
    new-instance v8, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v9, "onRegisterForNotifications() - address="

    #@21
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v8

    #@25
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v8

    #@29
    const-string v9, ", status="

    #@2b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v8

    #@2f
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v8

    #@33
    const-string v9, ", registered="

    #@35
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    const-string v9, ", charUuid="

    #@3f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v8

    #@43
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v8

    #@47
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v8

    #@4b
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 931
    return-void
.end method

.method onResponseSendCompleted(BI)V
    .registers 6
    .parameter "status"
    .parameter "attrHandle"

    #@0
    .prologue
    .line 1735
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onResponseSendCompleted() handle="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1737
    return-void
.end method

.method onScanResult(Ljava/lang/String;B[B)V
    .registers 24
    .parameter "address"
    .parameter "rssi"
    .parameter "adv_data"

    #@0
    .prologue
    .line 681
    const-string v17, "BtGatt.GattService"

    #@2
    new-instance v18, Ljava/lang/StringBuilder;

    #@4
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v19, "onScanResult() - address="

    #@9
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v18

    #@d
    move-object/from16 v0, v18

    #@f
    move-object/from16 v1, p1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v18

    #@15
    const-string v19, ", rssi="

    #@17
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v18

    #@1b
    move-object/from16 v0, v18

    #@1d
    move/from16 v1, p2

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v18

    #@23
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v18

    #@27
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 684
    move-object/from16 v0, p0

    #@2c
    move-object/from16 v1, p3

    #@2e
    invoke-direct {v0, v1}, Lcom/broadcom/bt/service/gatt/GattService;->parseUuids([B)Ljava/util/List;

    #@31
    move-result-object v15

    #@32
    .line 687
    .local v15, remoteUuids:Ljava/util/List;,"Ljava/util/List<Ljava/util/UUID;>;"
    :try_start_32
    move-object/from16 v0, p0

    #@34
    iget-object v0, v0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@36
    move-object/from16 v17, v0

    #@38
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@3b
    move-result-object v9

    #@3c
    :cond_3c
    :goto_3c
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    #@3f
    move-result v17

    #@40
    if-eqz v17, :cond_100

    #@42
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@45
    move-result-object v7

    #@46
    check-cast v7, Lcom/broadcom/bt/service/gatt/ScanClient;

    #@48
    .line 688
    .local v7, client:Lcom/broadcom/bt/service/gatt/ScanClient;
    iget-object v0, v7, Lcom/broadcom/bt/service/gatt/ScanClient;->uuids:[Ljava/util/UUID;

    #@4a
    move-object/from16 v17, v0

    #@4c
    move-object/from16 v0, v17

    #@4e
    array-length v0, v0

    #@4f
    move/from16 v17, v0

    #@51
    if-lez v17, :cond_88

    #@53
    .line 689
    const/4 v13, 0x0

    #@54
    .line 690
    .local v13, matches:I
    iget-object v6, v7, Lcom/broadcom/bt/service/gatt/ScanClient;->uuids:[Ljava/util/UUID;

    #@56
    .local v6, arr$:[Ljava/util/UUID;
    array-length v12, v6

    #@57
    .local v12, len$:I
    const/4 v10, 0x0

    #@58
    .local v10, i$:I
    move v11, v10

    #@59
    .end local v10           #i$:I
    .local v11, i$:I
    :goto_59
    if-ge v11, v12, :cond_7b

    #@5b
    aget-object v16, v6, v11

    #@5d
    .line 691
    .local v16, search:Ljava/util/UUID;
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@60
    move-result-object v10

    #@61
    .end local v11           #i$:I
    .local v10, i$:Ljava/util/Iterator;
    :cond_61
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    #@64
    move-result v17

    #@65
    if-eqz v17, :cond_77

    #@67
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6a
    move-result-object v14

    #@6b
    check-cast v14, Ljava/util/UUID;

    #@6d
    .line 692
    .local v14, remote:Ljava/util/UUID;
    move-object/from16 v0, v16

    #@6f
    invoke-virtual {v14, v0}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    #@72
    move-result v17

    #@73
    if-eqz v17, :cond_61

    #@75
    .line 693
    add-int/lit8 v13, v13, 0x1

    #@77
    .line 690
    .end local v14           #remote:Ljava/util/UUID;
    :cond_77
    add-int/lit8 v10, v11, 0x1

    #@79
    .local v10, i$:I
    move v11, v10

    #@7a
    .end local v10           #i$:I
    .restart local v11       #i$:I
    goto :goto_59

    #@7b
    .line 699
    .end local v16           #search:Ljava/util/UUID;
    :cond_7b
    iget-object v0, v7, Lcom/broadcom/bt/service/gatt/ScanClient;->uuids:[Ljava/util/UUID;

    #@7d
    move-object/from16 v17, v0

    #@7f
    move-object/from16 v0, v17

    #@81
    array-length v0, v0

    #@82
    move/from16 v17, v0

    #@84
    move/from16 v0, v17

    #@86
    if-lt v13, v0, :cond_3c

    #@88
    .line 704
    .end local v6           #arr$:[Ljava/util/UUID;
    .end local v11           #i$:I
    .end local v12           #len$:I
    .end local v13           #matches:I
    :cond_88
    iget-boolean v0, v7, Lcom/broadcom/bt/service/gatt/ScanClient;->isServer:Z

    #@8a
    move/from16 v17, v0

    #@8c
    if-nez v17, :cond_101

    #@8e
    .line 705
    move-object/from16 v0, p0

    #@90
    iget-object v0, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@92
    move-object/from16 v17, v0

    #@94
    iget-byte v0, v7, Lcom/broadcom/bt/service/gatt/ScanClient;->appIf:B

    #@96
    move/from16 v18, v0

    #@98
    invoke-virtual/range {v17 .. v18}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;
    :try_end_9b
    .catch Ljava/util/ConcurrentModificationException; {:try_start_32 .. :try_end_9b} :catch_e5

    #@9b
    move-result-object v4

    #@9c
    .line 706
    .local v4, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v4, :cond_3c

    #@9e
    .line 708
    :try_start_9e
    iget-object v0, v4, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@a0
    move-object/from16 v17, v0

    #@a2
    check-cast v17, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@a4
    move-object/from16 v0, v17

    #@a6
    move-object/from16 v1, p1

    #@a8
    move/from16 v2, p2

    #@aa
    move-object/from16 v3, p3

    #@ac
    invoke-interface {v0, v1, v2, v3}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onScanResult(Ljava/lang/String;I[B)V
    :try_end_af
    .catch Landroid/os/RemoteException; {:try_start_9e .. :try_end_af} :catch_b0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_9e .. :try_end_af} :catch_e5

    #@af
    goto :goto_3c

    #@b0
    .line 709
    :catch_b0
    move-exception v8

    #@b1
    .line 710
    .local v8, e:Landroid/os/RemoteException;
    :try_start_b1
    const-string v17, "BtGatt.GattService"

    #@b3
    new-instance v18, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v19, "Exception: "

    #@ba
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v18

    #@be
    move-object/from16 v0, v18

    #@c0
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v18

    #@c4
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v18

    #@c8
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@cb
    .line 711
    move-object/from16 v0, p0

    #@cd
    iget-object v0, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@cf
    move-object/from16 v17, v0

    #@d1
    iget-byte v0, v7, Lcom/broadcom/bt/service/gatt/ScanClient;->appIf:B

    #@d3
    move/from16 v18, v0

    #@d5
    invoke-virtual/range {v17 .. v18}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->remove(B)V

    #@d8
    .line 712
    move-object/from16 v0, p0

    #@da
    iget-object v0, v0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@dc
    move-object/from16 v17, v0

    #@de
    move-object/from16 v0, v17

    #@e0
    invoke-interface {v0, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_e3
    .catch Ljava/util/ConcurrentModificationException; {:try_start_b1 .. :try_end_e3} :catch_e5

    #@e3
    goto/16 :goto_3c

    #@e5
    .line 728
    .end local v4           #app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    .end local v7           #client:Lcom/broadcom/bt/service/gatt/ScanClient;
    .end local v8           #e:Landroid/os/RemoteException;
    :catch_e5
    move-exception v8

    #@e6
    .line 729
    .local v8, e:Ljava/util/ConcurrentModificationException;
    const-string v17, "BtGatt.GattService"

    #@e8
    new-instance v18, Ljava/lang/StringBuilder;

    #@ea
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@ed
    const-string v19, "ConcurrentModificationException: "

    #@ef
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v18

    #@f3
    move-object/from16 v0, v18

    #@f5
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v18

    #@f9
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fc
    move-result-object v18

    #@fd
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@100
    .line 731
    .end local v8           #e:Ljava/util/ConcurrentModificationException;
    :cond_100
    return-void

    #@101
    .line 716
    .restart local v7       #client:Lcom/broadcom/bt/service/gatt/ScanClient;
    :cond_101
    :try_start_101
    move-object/from16 v0, p0

    #@103
    iget-object v0, v0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@105
    move-object/from16 v17, v0

    #@107
    iget-byte v0, v7, Lcom/broadcom/bt/service/gatt/ScanClient;->appIf:B

    #@109
    move/from16 v18, v0

    #@10b
    invoke-virtual/range {v17 .. v18}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;
    :try_end_10e
    .catch Ljava/util/ConcurrentModificationException; {:try_start_101 .. :try_end_10e} :catch_e5

    #@10e
    move-result-object v5

    #@10f
    .line 717
    .local v5, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    if-eqz v5, :cond_3c

    #@111
    .line 719
    :try_start_111
    iget-object v0, v5, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@113
    move-object/from16 v17, v0

    #@115
    check-cast v17, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@117
    move-object/from16 v0, v17

    #@119
    move-object/from16 v1, p1

    #@11b
    move/from16 v2, p2

    #@11d
    move-object/from16 v3, p3

    #@11f
    invoke-interface {v0, v1, v2, v3}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onScanResult(Ljava/lang/String;I[B)V
    :try_end_122
    .catch Landroid/os/RemoteException; {:try_start_111 .. :try_end_122} :catch_124
    .catch Ljava/util/ConcurrentModificationException; {:try_start_111 .. :try_end_122} :catch_e5

    #@122
    goto/16 :goto_3c

    #@124
    .line 720
    :catch_124
    move-exception v8

    #@125
    .line 721
    .local v8, e:Landroid/os/RemoteException;
    :try_start_125
    const-string v17, "BtGatt.GattService"

    #@127
    new-instance v18, Ljava/lang/StringBuilder;

    #@129
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@12c
    const-string v19, "Exception: "

    #@12e
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v18

    #@132
    move-object/from16 v0, v18

    #@134
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@137
    move-result-object v18

    #@138
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13b
    move-result-object v18

    #@13c
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@13f
    .line 722
    move-object/from16 v0, p0

    #@141
    iget-object v0, v0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@143
    move-object/from16 v17, v0

    #@145
    iget-byte v0, v7, Lcom/broadcom/bt/service/gatt/ScanClient;->appIf:B

    #@147
    move/from16 v18, v0

    #@149
    invoke-virtual/range {v17 .. v18}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->remove(B)V

    #@14c
    .line 723
    move-object/from16 v0, p0

    #@14e
    iget-object v0, v0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@150
    move-object/from16 v17, v0

    #@152
    move-object/from16 v0, v17

    #@154
    invoke-interface {v0, v7}, Ljava/util/List;->remove(Ljava/lang/Object;)Z
    :try_end_157
    .catch Ljava/util/ConcurrentModificationException; {:try_start_125 .. :try_end_157} :catch_e5

    #@157
    goto/16 :goto_3c
.end method

.method onSearchCompleted(II)V
    .registers 6
    .parameter "connId"
    .parameter "status"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 790
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onSearchCompleted() - connId="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", status="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 793
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->continueSearch(II)V

    #@25
    .line 794
    return-void
.end method

.method onSearchResult(IIIJJ)V
    .registers 23
    .parameter "connId"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 799
    new-instance v14, Ljava/util/UUID;

    #@2
    move-wide/from16 v0, p6

    #@4
    move-wide/from16 v2, p4

    #@6
    invoke-direct {v14, v0, v1, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    #@9
    .line 800
    .local v14, uuid:Ljava/util/UUID;
    iget-object v4, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@b
    move/from16 v0, p1

    #@d
    invoke-virtual {v4, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@10
    move-result-object v12

    #@11
    .line 803
    .local v12, address:Ljava/lang/String;
    const-string v4, "BtGatt.GattService"

    #@13
    new-instance v5, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v6, "onSearchResult() - address="

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v5

    #@22
    const-string v6, ", uuid="

    #@24
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v5

    #@30
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 806
    iget-object v4, p0, Lcom/broadcom/bt/service/gatt/GattService;->mSearchQueue:Lcom/broadcom/bt/service/gatt/SearchQueue;

    #@35
    move/from16 v5, p1

    #@37
    move/from16 v6, p2

    #@39
    move/from16 v7, p3

    #@3b
    move-wide/from16 v8, p4

    #@3d
    move-wide/from16 v10, p6

    #@3f
    invoke-virtual/range {v4 .. v11}, Lcom/broadcom/bt/service/gatt/SearchQueue;->add(IIIJJ)V

    #@42
    .line 808
    iget-object v4, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@44
    move/from16 v0, p1

    #@46
    invoke-virtual {v4, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@49
    move-result-object v13

    #@4a
    .line 809
    .local v13, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v13, :cond_5c

    #@4c
    .line 810
    iget-object v4, v13, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@4e
    check-cast v4, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@50
    new-instance v5, Landroid/os/ParcelUuid;

    #@52
    invoke-direct {v5, v14}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@55
    move/from16 v0, p2

    #@57
    move/from16 v1, p3

    #@59
    invoke-interface {v4, v12, v0, v1, v5}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onGetService(Ljava/lang/String;IILandroid/os/ParcelUuid;)V

    #@5c
    .line 813
    :cond_5c
    return-void
.end method

.method onServerReadRemoteRssi(BLjava/lang/String;BI)V
    .registers 9
    .parameter "serverIf"
    .parameter "address"
    .parameter "rssi"
    .parameter "status"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1747
    const-string v1, "BtGatt.GattService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "onServerReadRemoteRssi() - serverIf="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, " address="

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ", rssi="

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, ", status="

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 1750
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@38
    invoke-virtual {v1, p1}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@3b
    move-result-object v0

    #@3c
    .line 1751
    .local v0, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    if-eqz v0, :cond_46

    #@3e
    .line 1752
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@40
    check-cast v1, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@42
    invoke-interface {v1, p2, p3, p4}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onServerReadRemoteRssi(Ljava/lang/String;II)V

    #@45
    .line 1756
    :goto_45
    return-void

    #@46
    .line 1754
    :cond_46
    const-string v1, "BtGatt.GattService"

    #@48
    const-string v2, "Server app is null"

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    goto :goto_45
.end method

.method onServerRegistered(BBJJ)V
    .registers 12
    .parameter "status"
    .parameter "serverIf"
    .parameter "uuidLsb"
    .parameter "uuidMsb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1428
    new-instance v1, Ljava/util/UUID;

    #@2
    invoke-direct {v1, p5, p6, p3, p4}, Ljava/util/UUID;-><init>(JJ)V

    #@5
    .line 1430
    .local v1, uuid:Ljava/util/UUID;
    const-string v2, "BtGatt.GattService"

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v4, "onServerRegistered() - UUID="

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, ", serverIf="

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 1432
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@29
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->getByUuid(Ljava/util/UUID;)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@2c
    move-result-object v0

    #@2d
    .line 1433
    .local v0, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;>.App;"
    if-eqz v0, :cond_40

    #@2f
    .line 1434
    iput-byte p2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@31
    .line 1435
    new-instance v2, Lcom/broadcom/bt/service/gatt/GattService$ServerDeathRecipient;

    #@33
    invoke-direct {v2, p0, p2}, Lcom/broadcom/bt/service/gatt/GattService$ServerDeathRecipient;-><init>(Lcom/broadcom/bt/service/gatt/GattService;B)V

    #@36
    invoke-virtual {v0, v2}, Lcom/broadcom/bt/service/gatt/ContextMap$App;->linkToDeath(Landroid/os/IBinder$DeathRecipient;)V

    #@39
    .line 1436
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@3b
    check-cast v2, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;

    #@3d
    invoke-interface {v2, p1, p2}, Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;->onServerRegistered(BB)V

    #@40
    .line 1438
    :cond_40
    return-void
.end method

.method onServiceAdded(BBIIJJI)V
    .registers 16
    .parameter "status"
    .parameter "serverIf"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "srvcHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1443
    new-instance v3, Ljava/util/UUID;

    #@2
    invoke-direct {v3, p7, p8, p5, p6}, Ljava/util/UUID;-><init>(JJ)V

    #@5
    .line 1445
    .local v3, uuid:Ljava/util/UUID;
    const-string v0, "BtGatt.GattService"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "onServiceAdded() UUID="

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, ", status="

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v2, ", handle="

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, p9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 1448
    if-nez p1, :cond_3c

    #@33
    .line 1449
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@35
    move v1, p2

    #@36
    move v2, p9

    #@37
    move v4, p3

    #@38
    move v5, p4

    #@39
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/gatt/HandleMap;->addService(BILjava/util/UUID;II)V

    #@3c
    .line 1451
    :cond_3c
    invoke-direct {p0, p2, p1, p9}, Lcom/broadcom/bt/service/gatt/GattService;->continueServiceDeclaration(BBI)V

    #@3f
    .line 1452
    return-void
.end method

.method onServiceDeleted(BBI)V
    .registers 7
    .parameter "status"
    .parameter "serverIf"
    .parameter "srvcHandle"

    #@0
    .prologue
    .line 1518
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onServiceDeleted() srvcHandle="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", status="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1521
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@24
    invoke-virtual {v0, p2, p3}, Lcom/broadcom/bt/service/gatt/HandleMap;->deleteService(BI)V

    #@27
    .line 1522
    return-void
.end method

.method onServiceStarted(BBI)V
    .registers 7
    .parameter "status"
    .parameter "serverIf"
    .parameter "srvcHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1496
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onServiceStarted() srvcHandle="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", status="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1499
    if-nez p1, :cond_2a

    #@24
    .line 1500
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@26
    const/4 v1, 0x1

    #@27
    invoke-virtual {v0, p2, p3, v1}, Lcom/broadcom/bt/service/gatt/HandleMap;->setStarted(BIZ)V

    #@2a
    .line 1502
    :cond_2a
    return-void
.end method

.method onServiceStopped(BBI)V
    .registers 7
    .parameter "status"
    .parameter "serverIf"
    .parameter "srvcHandle"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1507
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onServiceStopped() srvcHandle="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", status="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1510
    if-nez p1, :cond_2a

    #@24
    .line 1511
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@26
    const/4 v1, 0x0

    #@27
    invoke-virtual {v0, p2, p3, v1}, Lcom/broadcom/bt/service/gatt/HandleMap;->setStarted(BIZ)V

    #@2a
    .line 1513
    :cond_2a
    invoke-direct {p0, p2, p1}, Lcom/broadcom/bt/service/gatt/GattService;->stopNextService(BB)V

    #@2d
    .line 1514
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 261
    invoke-static {p0, p1}, Lcom/broadcom/bt/service/gatt/GattDebugUtils;->handleDebugAction(Lcom/broadcom/bt/service/gatt/GattService;Landroid/content/Intent;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 262
    const/4 v0, 0x2

    #@7
    .line 264
    :goto_7
    return v0

    #@8
    :cond_8
    invoke-super {p0, p1, p2, p3}, Lcom/android/bluetooth/btservice/ProfileService;->onStartCommand(Landroid/content/Intent;II)I

    #@b
    move-result v0

    #@c
    goto :goto_7
.end method

.method onWriteCharacteristic(IIIIJJIJJ)V
    .registers 29
    .parameter "connId"
    .parameter "status"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "charInstId"
    .parameter "charUuidLsb"
    .parameter "charUuidMsb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 995
    new-instance v14, Ljava/util/UUID;

    #@2
    move-wide/from16 v0, p7

    #@4
    move-wide/from16 v2, p5

    #@6
    invoke-direct {v14, v0, v1, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    #@9
    .line 996
    .local v14, srvcUuid:Ljava/util/UUID;
    new-instance v13, Ljava/util/UUID;

    #@b
    move-wide/from16 v0, p12

    #@d
    move-wide/from16 v2, p10

    #@f
    invoke-direct {v13, v0, v1, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    #@12
    .line 997
    .local v13, charUuid:Ljava/util/UUID;
    iget-object v4, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@14
    move/from16 v0, p1

    #@16
    invoke-virtual {v4, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@19
    move-result-object v5

    #@1a
    .line 1000
    .local v5, address:Ljava/lang/String;
    const-string v4, "BtGatt.GattService"

    #@1c
    new-instance v6, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v7, "onWriteCharacteristic() - address="

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    const-string v7, ", status="

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    move/from16 v0, p2

    #@33
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 1003
    iget-object v4, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@40
    move/from16 v0, p1

    #@42
    invoke-virtual {v4, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@45
    move-result-object v12

    #@46
    .line 1004
    .local v12, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v12, :cond_61

    #@48
    .line 1005
    iget-object v4, v12, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@4a
    check-cast v4, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@4c
    new-instance v9, Landroid/os/ParcelUuid;

    #@4e
    invoke-direct {v9, v14}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@51
    new-instance v11, Landroid/os/ParcelUuid;

    #@53
    invoke-direct {v11, v13}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@56
    move/from16 v6, p2

    #@58
    move/from16 v7, p3

    #@5a
    move/from16 v8, p4

    #@5c
    move/from16 v10, p9

    #@5e
    invoke-interface/range {v4 .. v11}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onCharacteristicWrite(Ljava/lang/String;IIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;)V

    #@61
    .line 1009
    :cond_61
    return-void
.end method

.method onWriteDescriptor(IIIIJJIJJJJ)V
    .registers 36
    .parameter "connId"
    .parameter "status"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "charInstId"
    .parameter "charUuidLsb"
    .parameter "charUuidMsb"
    .parameter "descrUuidLsb"
    .parameter "descrUuidMsb"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 1053
    new-instance v17, Ljava/util/UUID;

    #@2
    move-object/from16 v0, v17

    #@4
    move-wide/from16 v1, p7

    #@6
    move-wide/from16 v3, p5

    #@8
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@b
    .line 1054
    .local v17, srvcUuid:Ljava/util/UUID;
    new-instance v15, Ljava/util/UUID;

    #@d
    move-wide/from16 v0, p12

    #@f
    move-wide/from16 v2, p10

    #@11
    invoke-direct {v15, v0, v1, v2, v3}, Ljava/util/UUID;-><init>(JJ)V

    #@14
    .line 1055
    .local v15, charUuid:Ljava/util/UUID;
    new-instance v16, Ljava/util/UUID;

    #@16
    move-object/from16 v0, v16

    #@18
    move-wide/from16 v1, p16

    #@1a
    move-wide/from16 v3, p14

    #@1c
    invoke-direct {v0, v1, v2, v3, v4}, Ljava/util/UUID;-><init>(JJ)V

    #@1f
    .line 1056
    .local v16, descrUuid:Ljava/util/UUID;
    move-object/from16 v0, p0

    #@21
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@23
    move/from16 v0, p1

    #@25
    invoke-virtual {v5, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->addressByConnId(I)Ljava/lang/String;

    #@28
    move-result-object v6

    #@29
    .line 1059
    .local v6, address:Ljava/lang/String;
    const-string v5, "BtGatt.GattService"

    #@2b
    new-instance v7, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v8, "onWriteDescriptor() - address="

    #@32
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v7

    #@36
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v7

    #@3a
    const-string v8, ", status="

    #@3c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v7

    #@40
    move/from16 v0, p2

    #@42
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v7

    #@46
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v7

    #@4a
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 1062
    move-object/from16 v0, p0

    #@4f
    iget-object v5, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@51
    move/from16 v0, p1

    #@53
    invoke-virtual {v5, v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@56
    move-result-object v14

    #@57
    .line 1063
    .local v14, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v14, :cond_7b

    #@59
    .line 1064
    iget-object v5, v14, Lcom/broadcom/bt/service/gatt/ContextMap$App;->callback:Ljava/lang/Object;

    #@5b
    check-cast v5, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;

    #@5d
    new-instance v10, Landroid/os/ParcelUuid;

    #@5f
    move-object/from16 v0, v17

    #@61
    invoke-direct {v10, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@64
    new-instance v12, Landroid/os/ParcelUuid;

    #@66
    invoke-direct {v12, v15}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@69
    new-instance v13, Landroid/os/ParcelUuid;

    #@6b
    move-object/from16 v0, v16

    #@6d
    invoke-direct {v13, v0}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@70
    move/from16 v7, p2

    #@72
    move/from16 v8, p3

    #@74
    move/from16 v9, p4

    #@76
    move/from16 v11, p9

    #@78
    invoke-interface/range {v5 .. v13}, Lcom/broadcom/bt/gatt/IBluetoothGattCallback;->onDescriptorWrite(Ljava/lang/String;IIILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;)V

    #@7b
    .line 1069
    :cond_7b
    return-void
.end method

.method readCharacteristic(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;B)V
    .registers 26
    .parameter "clientIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcUuid"
    .parameter "charInstanceId"
    .parameter "charUuid"
    .parameter "authReq"

    #@0
    .prologue
    .line 1283
    const-string v2, "BtGatt.GattService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "readCharacteristic() - address="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    move-object/from16 v0, p2

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1286
    move-object/from16 v0, p0

    #@1c
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@1e
    move/from16 v0, p1

    #@20
    move-object/from16 v1, p2

    #@22
    invoke-virtual {v2, v0, v1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@25
    move-result-object v16

    #@26
    .line 1287
    .local v16, connId:Ljava/lang/Integer;
    if-eqz v16, :cond_4a

    #@28
    .line 1288
    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    #@2b
    move-result v3

    #@2c
    invoke-virtual/range {p5 .. p5}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@2f
    move-result-wide v6

    #@30
    invoke-virtual/range {p5 .. p5}, Ljava/util/UUID;->getMostSignificantBits()J

    #@33
    move-result-wide v8

    #@34
    invoke-virtual/range {p7 .. p7}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@37
    move-result-wide v11

    #@38
    invoke-virtual/range {p7 .. p7}, Ljava/util/UUID;->getMostSignificantBits()J

    #@3b
    move-result-wide v13

    #@3c
    move-object/from16 v2, p0

    #@3e
    move/from16 v4, p3

    #@40
    move/from16 v5, p4

    #@42
    move/from16 v10, p6

    #@44
    move/from16 v15, p8

    #@46
    invoke-direct/range {v2 .. v15}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientReadCharacteristicNative(IIIJJIJJB)V

    #@49
    .line 1296
    :goto_49
    return-void

    #@4a
    .line 1294
    :cond_4a
    const-string v2, "BtGatt.GattService"

    #@4c
    new-instance v3, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v4, "readCharacteristic() - No connection for "

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    move-object/from16 v0, p2

    #@59
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    const-string v4, "..."

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@66
    move-result-object v3

    #@67
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    goto :goto_49
.end method

.method readDescriptor(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;Ljava/util/UUID;B)V
    .registers 31
    .parameter "clientIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcUuid"
    .parameter "charInstanceId"
    .parameter "charUuid"
    .parameter "descrUuid"
    .parameter "authReq"

    #@0
    .prologue
    .line 1327
    const-string v2, "BtGatt.GattService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "readDescriptor() - address="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    move-object/from16 v0, p2

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1330
    move-object/from16 v0, p0

    #@1c
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@1e
    move/from16 v0, p1

    #@20
    move-object/from16 v1, p2

    #@22
    invoke-virtual {v2, v0, v1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@25
    move-result-object v20

    #@26
    .line 1331
    .local v20, connId:Ljava/lang/Integer;
    if-eqz v20, :cond_52

    #@28
    .line 1332
    invoke-virtual/range {v20 .. v20}, Ljava/lang/Integer;->intValue()I

    #@2b
    move-result v3

    #@2c
    invoke-virtual/range {p5 .. p5}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@2f
    move-result-wide v6

    #@30
    invoke-virtual/range {p5 .. p5}, Ljava/util/UUID;->getMostSignificantBits()J

    #@33
    move-result-wide v8

    #@34
    invoke-virtual/range {p7 .. p7}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@37
    move-result-wide v11

    #@38
    invoke-virtual/range {p7 .. p7}, Ljava/util/UUID;->getMostSignificantBits()J

    #@3b
    move-result-wide v13

    #@3c
    invoke-virtual/range {p8 .. p8}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@3f
    move-result-wide v15

    #@40
    invoke-virtual/range {p8 .. p8}, Ljava/util/UUID;->getMostSignificantBits()J

    #@43
    move-result-wide v17

    #@44
    move-object/from16 v2, p0

    #@46
    move/from16 v4, p3

    #@48
    move/from16 v5, p4

    #@4a
    move/from16 v10, p6

    #@4c
    move/from16 v19, p9

    #@4e
    invoke-direct/range {v2 .. v19}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientReadDescriptorNative(IIIJJIJJJJB)V

    #@51
    .line 1341
    :goto_51
    return-void

    #@52
    .line 1339
    :cond_52
    const-string v2, "BtGatt.GattService"

    #@54
    new-instance v3, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v4, "readDescriptor() - No connection for "

    #@5b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v3

    #@5f
    move-object/from16 v0, p2

    #@61
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    const-string v4, "..."

    #@67
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v3

    #@6f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    goto :goto_51
.end method

.method readRemoteRssi(BLjava/lang/String;)V
    .registers 6
    .parameter "clientIf"
    .parameter "address"

    #@0
    .prologue
    .line 1407
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "readRemoteRssi() - address="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1408
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientReadRemoteRssiNative(BLjava/lang/String;)V

    #@1b
    .line 1410
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientReadRemoteRssiNative(BLjava/lang/String;)V

    #@1e
    .line 1411
    return-void
.end method

.method refreshDevice(BLjava/lang/String;)V
    .registers 6
    .parameter "clientIf"
    .parameter "address"

    #@0
    .prologue
    .line 1261
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "refreshDevice() - address="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1263
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientRefreshNative(BLjava/lang/String;)V

    #@1b
    .line 1264
    return-void
.end method

.method registerClient(Ljava/util/UUID;Lcom/broadcom/bt/gatt/IBluetoothGattCallback;)V
    .registers 7
    .parameter "uuid"
    .parameter "callback"

    #@0
    .prologue
    .line 1211
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "registerClient() - UUID="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1214
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@1a
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByUuid(Ljava/util/UUID;)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@1d
    move-result-object v0

    #@1e
    if-nez v0, :cond_31

    #@20
    .line 1215
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@22
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->add(Ljava/util/UUID;Ljava/lang/Object;)V

    #@25
    .line 1222
    :goto_25
    invoke-virtual {p1}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@28
    move-result-wide v0

    #@29
    invoke-virtual {p1}, Ljava/util/UUID;->getMostSignificantBits()J

    #@2c
    move-result-wide v2

    #@2d
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientRegisterAppNative(JJ)V

    #@30
    .line 1224
    return-void

    #@31
    .line 1217
    :cond_31
    const-string v0, "BtGatt.GattService"

    #@33
    new-instance v1, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v2, "Already registered remove it and register again id="

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@40
    invoke-virtual {v2, p1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByUuid(Ljava/util/UUID;)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v1

    #@4c
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 1218
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@51
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@53
    invoke-virtual {v1, p1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getByUuid(Ljava/util/UUID;)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@56
    move-result-object v1

    #@57
    iget-byte v1, v1, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@59
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->remove(B)V

    #@5c
    .line 1219
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@5e
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->add(Ljava/util/UUID;Ljava/lang/Object;)V

    #@61
    goto :goto_25
.end method

.method registerForNotification(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;Z)V
    .registers 27
    .parameter "clientIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcUuid"
    .parameter "charInstanceId"
    .parameter "charUuid"
    .parameter "enable"

    #@0
    .prologue
    .line 1390
    const-string v2, "BtGatt.GattService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "registerForNotification() - address="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    move-object/from16 v0, p2

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, " enable: "

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    move/from16 v0, p8

    #@1b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 1393
    move-object/from16 v0, p0

    #@28
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@2a
    move/from16 v0, p1

    #@2c
    move-object/from16 v1, p2

    #@2e
    invoke-virtual {v2, v0, v1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@31
    move-result-object v17

    #@32
    .line 1394
    .local v17, connId:Ljava/lang/Integer;
    if-eqz v17, :cond_56

    #@34
    .line 1395
    invoke-virtual/range {p5 .. p5}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@37
    move-result-wide v7

    #@38
    invoke-virtual/range {p5 .. p5}, Ljava/util/UUID;->getMostSignificantBits()J

    #@3b
    move-result-wide v9

    #@3c
    invoke-virtual/range {p7 .. p7}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@3f
    move-result-wide v12

    #@40
    invoke-virtual/range {p7 .. p7}, Ljava/util/UUID;->getMostSignificantBits()J

    #@43
    move-result-wide v14

    #@44
    move-object/from16 v2, p0

    #@46
    move/from16 v3, p1

    #@48
    move-object/from16 v4, p2

    #@4a
    move/from16 v5, p3

    #@4c
    move/from16 v6, p4

    #@4e
    move/from16 v11, p6

    #@50
    move/from16 v16, p8

    #@52
    invoke-direct/range {v2 .. v16}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientRegisterForNotificationsNative(BLjava/lang/String;IIJJIJJZ)V

    #@55
    .line 1403
    :goto_55
    return-void

    #@56
    .line 1401
    :cond_56
    const-string v2, "BtGatt.GattService"

    #@58
    new-instance v3, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v4, "registerForNotification() - No connection for "

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    move-object/from16 v0, p2

    #@65
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    const-string v4, "..."

    #@6b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    goto :goto_55
.end method

.method registerServer(Ljava/util/UUID;Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;)V
    .registers 7
    .parameter "uuid"
    .parameter "callback"

    #@0
    .prologue
    .line 1764
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "registerServer() - UUID="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1766
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@1a
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->add(Ljava/util/UUID;Ljava/lang/Object;)V

    #@1d
    .line 1767
    invoke-virtual {p1}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@20
    move-result-wide v0

    #@21
    invoke-virtual {p1}, Ljava/util/UUID;->getMostSignificantBits()J

    #@24
    move-result-wide v2

    #@25
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerRegisterAppNative(JJ)V

    #@28
    .line 1768
    return-void
.end method

.method removeService(BIILjava/util/UUID;)V
    .registers 9
    .parameter "serverIf"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcUuid"

    #@0
    .prologue
    .line 1892
    const-string v1, "BtGatt.GattService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "removeService() - uuid="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1895
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@1a
    invoke-virtual {v1, p4, p2, p3}, Lcom/broadcom/bt/service/gatt/HandleMap;->getServiceHandle(Ljava/util/UUID;II)I

    #@1d
    move-result v0

    #@1e
    .line 1896
    .local v0, srvcHandle:I
    if-nez v0, :cond_21

    #@20
    .line 1900
    :goto_20
    return-void

    #@21
    .line 1899
    :cond_21
    invoke-direct {p0, p1, v0}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerDeleteServiceNative(BI)V

    #@24
    goto :goto_20
.end method

.method sendNotification(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;Z[B)V
    .registers 18
    .parameter "serverIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcUuid"
    .parameter "charInstanceId"
    .parameter "charUuid"
    .parameter "confirm"
    .parameter "value"

    #@0
    .prologue
    .line 1941
    const-string v5, "BtGatt.GattService"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "sendNotification() - address="

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1944
    iget-object v5, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@1a
    invoke-virtual {v5, p5, p3, p4}, Lcom/broadcom/bt/service/gatt/HandleMap;->getServiceHandle(Ljava/util/UUID;II)I

    #@1d
    move-result v4

    #@1e
    .line 1945
    .local v4, srvcHandle:I
    if-nez v4, :cond_21

    #@20
    .line 1973
    :cond_20
    :goto_20
    return-void

    #@21
    .line 1949
    :cond_21
    iget-object v5, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@23
    invoke-virtual {v5, v4, p7, p6}, Lcom/broadcom/bt/service/gatt/HandleMap;->getCharacteristicHandle(ILjava/util/UUID;I)I

    #@26
    move-result v1

    #@27
    .line 1950
    .local v1, charHandle:I
    if-eqz v1, :cond_20

    #@29
    .line 1955
    const/4 v2, 0x0

    #@2a
    .line 1956
    .local v2, connId:I
    iget-object v5, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@2c
    invoke-virtual {v5, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@2f
    move-result-object v3

    #@30
    .line 1957
    .local v3, connIdTemp:Ljava/lang/Integer;
    if-eqz v3, :cond_36

    #@32
    .line 1958
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@35
    move-result v2

    #@36
    .line 1964
    :cond_36
    if-eqz v2, :cond_20

    #@38
    .line 1968
    if-eqz p8, :cond_40

    #@3a
    .line 1969
    move-object/from16 v0, p9

    #@3c
    invoke-direct {p0, p1, v1, v2, v0}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerSendIndicationNative(BII[B)V

    #@3f
    goto :goto_20

    #@40
    .line 1971
    :cond_40
    move-object/from16 v0, p9

    #@42
    invoke-direct {p0, p1, v1, v2, v0}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerSendNotificationNative(BII[B)V

    #@45
    goto :goto_20
.end method

.method sendResponse(BLjava/lang/String;III[B)V
    .registers 18
    .parameter "serverIf"
    .parameter "address"
    .parameter "requestId"
    .parameter "status"
    .parameter "offset"
    .parameter "value"

    #@0
    .prologue
    .line 1912
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "sendResponse() - address="

    #@9
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1915
    const/4 v5, 0x0

    #@19
    .line 1916
    .local v5, handle:I
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@1b
    invoke-virtual {v0, p3}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByRequestId(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@1e
    move-result-object v10

    #@1f
    .line 1917
    .local v10, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    if-eqz v10, :cond_23

    #@21
    .line 1918
    iget v5, v10, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@23
    .line 1922
    :cond_23
    const/4 v2, 0x0

    #@24
    .line 1923
    .local v2, connId:I
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@26
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@29
    move-result-object v9

    #@2a
    .line 1924
    .local v9, connIdTemp:Ljava/lang/Integer;
    if-eqz v9, :cond_30

    #@2c
    .line 1925
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    #@2f
    move-result v2

    #@30
    .line 1931
    :cond_30
    int-to-byte v4, p4

    #@31
    const/4 v8, 0x0

    #@32
    move-object v0, p0

    #@33
    move v1, p1

    #@34
    move v3, p3

    #@35
    move/from16 v6, p5

    #@37
    move-object/from16 v7, p6

    #@39
    invoke-direct/range {v0 .. v8}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerSendResponseNative(BIIBII[BB)V

    #@3c
    .line 1933
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@3e
    invoke-virtual {v0, p3}, Lcom/broadcom/bt/service/gatt/HandleMap;->deleteRequest(I)V

    #@41
    .line 1934
    return-void
.end method

.method serverConnect(BLjava/lang/String;Z)V
    .registers 7
    .parameter "serverIf"
    .parameter "address"
    .parameter "isDirect"

    #@0
    .prologue
    .line 1783
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "serverConnect() - address="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1785
    invoke-direct {p0, p1, p2, p3}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerOpenNative(BLjava/lang/String;Z)V

    #@1b
    .line 1786
    return-void
.end method

.method serverDisconnect(BLjava/lang/String;)V
    .registers 7
    .parameter "serverIf"
    .parameter "address"

    #@0
    .prologue
    .line 1789
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@2
    invoke-virtual {v1, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@5
    move-result-object v0

    #@6
    .line 1791
    .local v0, connId:Ljava/lang/Integer;
    const-string v1, "BtGatt.GattService"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "serverDisconnect() - address="

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    const-string v3, ", connId="

    #@19
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1794
    if-eqz v0, :cond_32

    #@2a
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@2d
    move-result v1

    #@2e
    :goto_2e
    invoke-direct {p0, p1, p2, v1}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerCloseNative(BLjava/lang/String;I)V

    #@31
    .line 1795
    return-void

    #@32
    .line 1794
    :cond_32
    const/4 v1, 0x0

    #@33
    goto :goto_2e
.end method

.method serverReadRemoteRssi(BLjava/lang/String;)V
    .registers 6
    .parameter "serverIf"
    .parameter "address"

    #@0
    .prologue
    .line 1415
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "serverReadRemoteRssi() - address="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1417
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerReadRemoteRssiNative(BLjava/lang/String;)V

    #@1b
    .line 1418
    return-void
.end method

.method protected start()Z
    .registers 3

    #@0
    .prologue
    .line 225
    const-string v0, "BtGatt.GattService"

    #@2
    const-string v1, "start()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 227
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService;->initializeNative()V

    #@a
    .line 229
    invoke-static {p0}, Lcom/broadcom/bt/service/gatt/GattService;->setGattService(Lcom/broadcom/bt/service/gatt/GattService;)V

    #@d
    .line 231
    const/4 v0, 0x1

    #@e
    return v0
.end method

.method startScan(BZ)V
    .registers 6
    .parameter "appIf"
    .parameter "isServer"

    #@0
    .prologue
    .line 1162
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "startScan() - queue="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@f
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@12
    move-result v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1165
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->getScanClient(BZ)Lcom/broadcom/bt/service/gatt/ScanClient;

    #@21
    move-result-object v0

    #@22
    if-nez v0, :cond_46

    #@24
    .line 1167
    const-string v0, "BtGatt.GattService"

    #@26
    new-instance v1, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v2, "startScan() - adding client="

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 1169
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@3e
    new-instance v1, Lcom/broadcom/bt/service/gatt/ScanClient;

    #@40
    invoke-direct {v1, p1, p2}, Lcom/broadcom/bt/service/gatt/ScanClient;-><init>(BZ)V

    #@43
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@46
    .line 1172
    :cond_46
    const/4 v0, 0x1

    #@47
    invoke-direct {p0, p1, v0}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientScanNative(BZ)V

    #@4a
    .line 1173
    return-void
.end method

.method startScanWithUuids(BZ[Ljava/util/UUID;)V
    .registers 7
    .parameter "appIf"
    .parameter "isServer"
    .parameter "uuids"

    #@0
    .prologue
    .line 1177
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "startScanWithUuids() - queue="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@f
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@12
    move-result v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1180
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->getScanClient(BZ)Lcom/broadcom/bt/service/gatt/ScanClient;

    #@21
    move-result-object v0

    #@22
    if-nez v0, :cond_46

    #@24
    .line 1182
    const-string v0, "BtGatt.GattService"

    #@26
    new-instance v1, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v2, "startScanWithUuids() - adding client="

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 1184
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@3e
    new-instance v1, Lcom/broadcom/bt/service/gatt/ScanClient;

    #@40
    invoke-direct {v1, p1, p2, p3}, Lcom/broadcom/bt/service/gatt/ScanClient;-><init>(BZ[Ljava/util/UUID;)V

    #@43
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@46
    .line 1187
    :cond_46
    const/4 v0, 0x1

    #@47
    invoke-direct {p0, p1, v0}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientScanNative(BZ)V

    #@4a
    .line 1188
    return-void
.end method

.method protected stop()Z
    .registers 3

    #@0
    .prologue
    .line 236
    const-string v0, "BtGatt.GattService"

    #@2
    const-string v1, "stop()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 238
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@9
    invoke-virtual {v0}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->clear()V

    #@c
    .line 239
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@e
    invoke-virtual {v0}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->clear()V

    #@11
    .line 240
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mSearchQueue:Lcom/broadcom/bt/service/gatt/SearchQueue;

    #@13
    invoke-virtual {v0}, Lcom/broadcom/bt/service/gatt/SearchQueue;->clear()V

    #@16
    .line 241
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@18
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@1b
    .line 242
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@1d
    invoke-virtual {v0}, Lcom/broadcom/bt/service/gatt/HandleMap;->clear()V

    #@20
    .line 243
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServiceDeclarations:Ljava/util/List;

    #@22
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@25
    .line 244
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mReliableQueue:Ljava/util/Set;

    #@27
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    #@2a
    .line 245
    const/4 v0, 0x1

    #@2b
    return v0
.end method

.method stopScan(BZ)V
    .registers 6
    .parameter "appIf"
    .parameter "isServer"

    #@0
    .prologue
    .line 1192
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "stopScan() - queue="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@f
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@12
    move-result v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1195
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->removeScanClient(BZ)V

    #@21
    .line 1197
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mScanQueue:Ljava/util/List;

    #@23
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@26
    move-result v0

    #@27
    if-eqz v0, :cond_34

    #@29
    .line 1199
    const-string v0, "BtGatt.GattService"

    #@2b
    const-string v1, "stopScan() - queue empty; stopping scan"

    #@2d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 1201
    const/4 v0, 0x0

    #@31
    invoke-direct {p0, p1, v0}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientScanNative(BZ)V

    #@34
    .line 1203
    :cond_34
    return-void
.end method

.method unregisterClient(B)V
    .registers 5
    .parameter "clientIf"

    #@0
    .prologue
    .line 1228
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "unregisterClient() - clientIf="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1230
    const/4 v0, 0x0

    #@19
    invoke-direct {p0, p1, v0}, Lcom/broadcom/bt/service/gatt/GattService;->removeScanClient(BZ)V

    #@1c
    .line 1231
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@1e
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->remove(B)V

    #@21
    .line 1232
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientUnregisterAppNative(B)V

    #@24
    .line 1233
    return-void
.end method

.method unregisterServer(B)V
    .registers 5
    .parameter "serverIf"

    #@0
    .prologue
    .line 1772
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "unregisterServer() - serverIf="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1775
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/gatt/GattService;->deleteServices(B)V

    #@1b
    .line 1777
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@1d
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->remove(B)V

    #@20
    .line 1778
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/gatt/GattService;->gattServerUnregisterAppNative(B)V

    #@23
    .line 1779
    return-void
.end method

.method writeCharacteristic(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;IB[B)V
    .registers 30
    .parameter "clientIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcUuid"
    .parameter "charInstanceId"
    .parameter "charUuid"
    .parameter "writeType"
    .parameter "authReq"
    .parameter "value"

    #@0
    .prologue
    .line 1303
    const-string v2, "BtGatt.GattService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "writeCharacteristic() - address="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    move-object/from16 v0, p2

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1306
    move-object/from16 v0, p0

    #@1c
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mReliableQueue:Ljava/util/Set;

    #@1e
    move-object/from16 v0, p2

    #@20
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    #@23
    move-result v2

    #@24
    if-eqz v2, :cond_28

    #@26
    .line 1307
    const/16 p8, 0x3

    #@28
    .line 1310
    :cond_28
    move-object/from16 v0, p0

    #@2a
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@2c
    move/from16 v0, p1

    #@2e
    move-object/from16 v1, p2

    #@30
    invoke-virtual {v2, v0, v1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@33
    move-result-object v18

    #@34
    .line 1311
    .local v18, connId:Ljava/lang/Integer;
    if-eqz v18, :cond_5c

    #@36
    .line 1312
    invoke-virtual/range {v18 .. v18}, Ljava/lang/Integer;->intValue()I

    #@39
    move-result v3

    #@3a
    invoke-virtual/range {p5 .. p5}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@3d
    move-result-wide v6

    #@3e
    invoke-virtual/range {p5 .. p5}, Ljava/util/UUID;->getMostSignificantBits()J

    #@41
    move-result-wide v8

    #@42
    invoke-virtual/range {p7 .. p7}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@45
    move-result-wide v11

    #@46
    invoke-virtual/range {p7 .. p7}, Ljava/util/UUID;->getMostSignificantBits()J

    #@49
    move-result-wide v13

    #@4a
    move-object/from16 v2, p0

    #@4c
    move/from16 v4, p3

    #@4e
    move/from16 v5, p4

    #@50
    move/from16 v10, p6

    #@52
    move/from16 v15, p8

    #@54
    move/from16 v16, p9

    #@56
    move-object/from16 v17, p10

    #@58
    invoke-direct/range {v2 .. v17}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientWriteCharacteristicNative(IIIJJIJJIB[B)V

    #@5b
    .line 1320
    :goto_5b
    return-void

    #@5c
    .line 1318
    :cond_5c
    const-string v2, "BtGatt.GattService"

    #@5e
    new-instance v3, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v4, "writeCharacteristic() - No connection for "

    #@65
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    move-object/from16 v0, p2

    #@6b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    const-string v4, "..."

    #@71
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@78
    move-result-object v3

    #@79
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    goto :goto_5b
.end method

.method writeDescriptor(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;Ljava/util/UUID;IB[B)V
    .registers 35
    .parameter "clientIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcUuid"
    .parameter "charInstanceId"
    .parameter "charUuid"
    .parameter "descrUuid"
    .parameter "writeType"
    .parameter "authReq"
    .parameter "value"

    #@0
    .prologue
    .line 1349
    const-string v2, "BtGatt.GattService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "writeDescriptor() - address="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    move-object/from16 v0, p2

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 1352
    move-object/from16 v0, p0

    #@1c
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@1e
    move/from16 v0, p1

    #@20
    move-object/from16 v1, p2

    #@22
    invoke-virtual {v2, v0, v1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;

    #@25
    move-result-object v22

    #@26
    .line 1353
    .local v22, connId:Ljava/lang/Integer;
    if-eqz v22, :cond_56

    #@28
    .line 1354
    invoke-virtual/range {v22 .. v22}, Ljava/lang/Integer;->intValue()I

    #@2b
    move-result v3

    #@2c
    invoke-virtual/range {p5 .. p5}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@2f
    move-result-wide v6

    #@30
    invoke-virtual/range {p5 .. p5}, Ljava/util/UUID;->getMostSignificantBits()J

    #@33
    move-result-wide v8

    #@34
    invoke-virtual/range {p7 .. p7}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@37
    move-result-wide v11

    #@38
    invoke-virtual/range {p7 .. p7}, Ljava/util/UUID;->getMostSignificantBits()J

    #@3b
    move-result-wide v13

    #@3c
    invoke-virtual/range {p8 .. p8}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@3f
    move-result-wide v15

    #@40
    invoke-virtual/range {p8 .. p8}, Ljava/util/UUID;->getMostSignificantBits()J

    #@43
    move-result-wide v17

    #@44
    move-object/from16 v2, p0

    #@46
    move/from16 v4, p3

    #@48
    move/from16 v5, p4

    #@4a
    move/from16 v10, p6

    #@4c
    move/from16 v19, p9

    #@4e
    move/from16 v20, p10

    #@50
    move-object/from16 v21, p11

    #@52
    invoke-direct/range {v2 .. v21}, Lcom/broadcom/bt/service/gatt/GattService;->gattClientWriteDescriptorNative(IIIJJIJJJJIB[B)V

    #@55
    .line 1363
    :goto_55
    return-void

    #@56
    .line 1361
    :cond_56
    const-string v2, "BtGatt.GattService"

    #@58
    new-instance v3, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v4, "writeDescriptor() - No connection for "

    #@5f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v3

    #@63
    move-object/from16 v0, p2

    #@65
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    const-string v4, "..."

    #@6b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    goto :goto_55
.end method
