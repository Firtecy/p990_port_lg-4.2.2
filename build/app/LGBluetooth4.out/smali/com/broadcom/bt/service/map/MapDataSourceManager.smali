.class public Lcom/broadcom/bt/service/map/MapDataSourceManager;
.super Ljava/lang/Object;
.source "MapDataSourceManager.java"


# static fields
.field private static final DBG:Z = true

.field public static final PREFERENCE_KEY_PREFIX_DS:Ljava/lang/String; = "ds/"

.field public static final PREFERENCE_KEY_PREFIX_DS_NAME:Ljava/lang/String; = "dsname/"

.field public static final PREFERENCE_KEY_PREFIX_PROVIDER_NAME:Ljava/lang/String; = "pname/"

.field public static final SHARED_PREFERENCE_NAME:Ljava/lang/String; = "MapSharedPreferences"

.field private static final TAG:Ljava/lang/String; = "BtMap.DataSourceManager"

.field private static sDsSmsMms:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

.field private static sInstance:Lcom/broadcom/bt/service/map/MapDataSourceManager;

.field private static sIsInited:Z


# instance fields
.field private mDatasources:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/broadcom/bt/service/map/MapDatasourceContext;",
            ">;"
        }
    .end annotation
.end field

.field private mSessionToDatasourceMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/broadcom/bt/service/map/MapDatasourceContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 71
    const/4 v0, 0x0

    #@2
    sput-boolean v0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sIsInited:Z

    #@4
    .line 72
    sput-object v1, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sInstance:Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@6
    .line 73
    sput-object v1, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sDsSmsMms:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 163
    new-instance v0, Ljava/util/LinkedList;

    #@5
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@a
    .line 165
    new-instance v0, Ljava/util/HashMap;

    #@c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@f
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mSessionToDatasourceMap:Ljava/util/HashMap;

    #@11
    return-void
.end method

.method public static cleanup()V
    .registers 1

    #@0
    .prologue
    .line 158
    sget-object v0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sInstance:Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 159
    sget-object v0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sInstance:Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@6
    invoke-direct {v0}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->clear()V

    #@9
    .line 161
    :cond_9
    return-void
.end method

.method private clear()V
    .registers 2

    #@0
    .prologue
    .line 187
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@2
    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    #@5
    .line 188
    return-void
.end method

.method private createStoredDataSourceState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 12
    .parameter "ctx"
    .parameter "providerId"
    .parameter "dsId"
    .parameter "providerName"
    .parameter "dsName"
    .parameter "setEnabled"

    #@0
    .prologue
    .line 291
    const-string v3, "MapSharedPreferences"

    #@2
    const/4 v4, 0x0

    #@3
    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@6
    move-result-object v2

    #@7
    .line 293
    .local v2, sp:Landroid/content/SharedPreferences;
    if-nez v2, :cond_12

    #@9
    .line 294
    const-string v3, "BtMap.DataSourceManager"

    #@b
    const-string v4, "isEnableStored(): SharedPreferences not available"

    #@d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 295
    const/4 v3, 0x1

    #@11
    .line 302
    :goto_11
    return v3

    #@12
    .line 297
    :cond_12
    invoke-static {p2, p3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 298
    .local v1, key:Ljava/lang/String;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@19
    move-result-object v0

    #@1a
    .line 299
    .local v0, ed:Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, v1, p6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@1d
    .line 300
    invoke-static {v1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceProviderNameKey(Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-interface {v0, v3, p4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@24
    .line 301
    invoke-static {v1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceDsNameKey(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-interface {v0, v3, p5}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@2b
    .line 302
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@2e
    move-result v3

    #@2f
    goto :goto_11
.end method

.method public static getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;
    .registers 1

    #@0
    .prologue
    .line 148
    sget-object v0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sInstance:Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@2
    return-object v0
.end method

.method static declared-synchronized init(Landroid/content/Context;)V
    .registers 6
    .parameter "ctx"

    #@0
    .prologue
    .line 130
    const-class v2, Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@2
    monitor-enter v2

    #@3
    :try_start_3
    const-string v1, "BtMap.DataSourceManager"

    #@5
    new-instance v3, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v4, "init(): application context = "

    #@c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@13
    move-result-object v4

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 131
    sget-boolean v1, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sIsInited:Z

    #@21
    if-nez v1, :cond_59

    #@23
    .line 132
    const-string v1, "BtMap.DataSourceManager"

    #@25
    const-string v3, "init(): done initializing"

    #@27
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2a
    .catchall {:try_start_3 .. :try_end_2a} :catchall_56

    #@2a
    .line 134
    :try_start_2a
    new-instance v1, Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@2c
    invoke-direct {v1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;-><init>()V

    #@2f
    sput-object v1, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sInstance:Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@31
    .line 135
    new-instance v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@33
    invoke-direct {v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;-><init>()V

    #@36
    sput-object v1, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sDsSmsMms:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@38
    .line 136
    sget-object v1, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sDsSmsMms:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@3a
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v1, v3}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->init(Landroid/content/Context;)V

    #@41
    .line 137
    const/4 v1, 0x1

    #@42
    sput-boolean v1, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sIsInited:Z

    #@44
    .line 138
    const-string v1, "BtMap.DataSourceManager"

    #@46
    const-string v3, "init(): done initializing"

    #@48
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4b
    .catchall {:try_start_2a .. :try_end_4b} :catchall_56
    .catch Ljava/lang/Throwable; {:try_start_2a .. :try_end_4b} :catch_4d

    #@4b
    .line 145
    :goto_4b
    monitor-exit v2

    #@4c
    return-void

    #@4d
    .line 139
    :catch_4d
    move-exception v0

    #@4e
    .line 140
    .local v0, t:Ljava/lang/Throwable;
    :try_start_4e
    const-string v1, "BtMap.DataSourceManager"

    #@50
    const-string v3, "Error initializing datasources...."

    #@52
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_55
    .catchall {:try_start_4e .. :try_end_55} :catchall_56

    #@55
    goto :goto_4b

    #@56
    .line 130
    .end local v0           #t:Ljava/lang/Throwable;
    :catchall_56
    move-exception v1

    #@57
    monitor-exit v2

    #@58
    throw v1

    #@59
    .line 143
    :cond_59
    :try_start_59
    const-string v1, "BtMap.DataSourceManager"

    #@5b
    const-string v3, "init(): already initialized"

    #@5d
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_60
    .catchall {:try_start_59 .. :try_end_60} :catchall_56

    #@60
    goto :goto_4b
.end method

.method private isDataSourceStateStored(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "ctx"
    .parameter "providerId"
    .parameter "dsId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 279
    const-string v3, "MapSharedPreferences"

    #@3
    invoke-virtual {p1, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@6
    move-result-object v1

    #@7
    .line 281
    .local v1, sp:Landroid/content/SharedPreferences;
    if-nez v1, :cond_11

    #@9
    .line 282
    const-string v3, "BtMap.DataSourceManager"

    #@b
    const-string v4, "isEnableStored(): SharedPreferences not available"

    #@d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 286
    :goto_10
    return v2

    #@11
    .line 285
    :cond_11
    invoke-static {p2, p3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 286
    .local v0, key:Ljava/lang/String;
    invoke-interface {v1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    #@18
    move-result v2

    #@19
    goto :goto_10
.end method

.method private isEnableStored(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "ctx"
    .parameter "providerId"
    .parameter "dsId"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 335
    const-string v3, "MapSharedPreferences"

    #@3
    const/4 v4, 0x0

    #@4
    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@7
    move-result-object v1

    #@8
    .line 337
    .local v1, sp:Landroid/content/SharedPreferences;
    if-nez v1, :cond_12

    #@a
    .line 338
    const-string v3, "BtMap.DataSourceManager"

    #@c
    const-string v4, "isEnableStored(): SharedPreferences not available"

    #@e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 342
    :goto_11
    return v2

    #@12
    .line 341
    :cond_12
    invoke-static {p2, p3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 342
    .local v0, key:Ljava/lang/String;
    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@19
    move-result v2

    #@1a
    goto :goto_11
.end method

.method private removeStoredDataSourceState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "ctx"
    .parameter "providerId"
    .parameter "dsId"

    #@0
    .prologue
    .line 306
    const-string v3, "MapSharedPreferences"

    #@2
    const/4 v4, 0x0

    #@3
    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@6
    move-result-object v2

    #@7
    .line 308
    .local v2, sp:Landroid/content/SharedPreferences;
    if-nez v2, :cond_12

    #@9
    .line 309
    const-string v3, "BtMap.DataSourceManager"

    #@b
    const-string v4, "isEnableStored(): SharedPreferences not available"

    #@d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 310
    const/4 v3, 0x1

    #@11
    .line 317
    :goto_11
    return v3

    #@12
    .line 312
    :cond_12
    invoke-static {p2, p3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    .line 313
    .local v1, key:Ljava/lang/String;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@19
    move-result-object v0

    #@1a
    .line 314
    .local v0, ed:Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@1d
    .line 315
    invoke-static {v1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceProviderNameKey(Ljava/lang/String;)Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@24
    .line 316
    invoke-static {v1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceDsNameKey(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@2b
    .line 317
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@2e
    move-result v3

    #@2f
    goto :goto_11
.end method

.method private setStoredDataSourceState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 11
    .parameter "ctx"
    .parameter "providerId"
    .parameter "dsId"
    .parameter "setEnabled"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 322
    const-string v4, "MapSharedPreferences"

    #@3
    invoke-virtual {p1, v4, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@6
    move-result-object v2

    #@7
    .line 324
    .local v2, sp:Landroid/content/SharedPreferences;
    if-nez v2, :cond_11

    #@9
    .line 325
    const-string v4, "BtMap.DataSourceManager"

    #@b
    const-string v5, "isEnableStored(): SharedPreferences not available"

    #@d
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 331
    :goto_10
    return v3

    #@11
    .line 328
    :cond_11
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@14
    move-result-object v0

    #@15
    .line 329
    .local v0, ed:Landroid/content/SharedPreferences$Editor;
    invoke-static {p2, p3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    .line 330
    .local v1, key:Ljava/lang/String;
    invoke-interface {v0, v1, p4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@1c
    .line 331
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    #@1f
    move-result v3

    #@20
    goto :goto_10
.end method

.method public static stopAll(Lcom/broadcom/bt/service/map/MapService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 152
    sget-object v0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sInstance:Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 153
    sget-object v0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sInstance:Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@6
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->stopAllDatasources(Lcom/broadcom/bt/service/map/MapService;)V

    #@9
    .line 155
    :cond_9
    return-void
.end method

.method private stopAllDatasources(Lcom/broadcom/bt/service/map/MapService;)V
    .registers 11
    .parameter "svc"

    #@0
    .prologue
    .line 169
    const/4 v3, 0x0

    #@1
    .line 170
    .local v3, dsCopy:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/broadcom/bt/service/map/MapDatasourceContext;>;"
    iget-object v7, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@3
    monitor-enter v7

    #@4
    .line 171
    :try_start_4
    iget-object v6, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@6
    invoke-virtual {v6}, Ljava/util/LinkedList;->clone()Ljava/lang/Object;

    #@9
    move-result-object v6

    #@a
    move-object v0, v6

    #@b
    check-cast v0, Ljava/util/LinkedList;

    #@d
    move-object v3, v0

    #@e
    .line 172
    monitor-exit v7
    :try_end_f
    .catchall {:try_start_4 .. :try_end_f} :catchall_29

    #@f
    .line 173
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    #@12
    move-result v1

    #@13
    .line 174
    .local v1, count:I
    if-lez v1, :cond_46

    #@15
    .line 175
    const/4 v4, 0x0

    #@16
    .local v4, i:I
    :goto_16
    if-ge v4, v1, :cond_46

    #@18
    .line 176
    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@1e
    .line 178
    .local v2, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :try_start_1e
    iget-object v6, v2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@20
    iget-object v7, v2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@22
    const/4 v8, 0x0

    #@23
    invoke-virtual {p1, v6, v7, v8}, Lcom/broadcom/bt/service/map/MapService;->unregisterDatasource(Ljava/lang/String;Ljava/lang/String;Z)Z
    :try_end_26
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_26} :catch_2c

    #@26
    .line 175
    :goto_26
    add-int/lit8 v4, v4, 0x1

    #@28
    goto :goto_16

    #@29
    .line 172
    .end local v1           #count:I
    .end local v2           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    .end local v4           #i:I
    :catchall_29
    move-exception v6

    #@2a
    :try_start_2a
    monitor-exit v7
    :try_end_2b
    .catchall {:try_start_2a .. :try_end_2b} :catchall_29

    #@2b
    throw v6

    #@2c
    .line 179
    .restart local v1       #count:I
    .restart local v2       #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    .restart local v4       #i:I
    :catch_2c
    move-exception v5

    #@2d
    .line 180
    .local v5, t:Ljava/lang/Throwable;
    const-string v6, "BtMap.DataSourceManager"

    #@2f
    new-instance v7, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v8, "Unable to finish datasource #"

    #@36
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v7

    #@3a
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v7

    #@3e
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v7

    #@42
    invoke-static {v6, v7, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@45
    goto :goto_26

    #@46
    .line 184
    .end local v2           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    .end local v4           #i:I
    .end local v5           #t:Ljava/lang/Throwable;
    :cond_46
    return-void
.end method

.method public static toDsId(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "preferenceKey"

    #@0
    .prologue
    .line 96
    if-eqz p0, :cond_8

    #@2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v2

    #@6
    if-gtz v2, :cond_a

    #@8
    .line 97
    :cond_8
    const/4 v2, 0x0

    #@9
    .line 107
    :goto_9
    return-object v2

    #@a
    .line 100
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 101
    .local v0, providerId:Ljava/lang/String;
    const-string v2, "ds/"

    #@d
    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_2a

    #@13
    .line 102
    const-string v2, "ds/"

    #@15
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@18
    move-result v2

    #@19
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 106
    :goto_1d
    const/16 v2, 0x2f

    #@1f
    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    #@22
    move-result v1

    #@23
    .line 107
    .local v1, splitIndex:I
    add-int/lit8 v2, v1, 0x1

    #@25
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    goto :goto_9

    #@2a
    .line 104
    .end local v1           #splitIndex:I
    :cond_2a
    move-object v0, p0

    #@2b
    goto :goto_1d
.end method

.method public static toPreferenceDsNameKey(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "prefKey"

    #@0
    .prologue
    .line 120
    if-nez p0, :cond_4

    #@2
    .line 121
    const/4 v0, 0x0

    #@3
    .line 123
    .end local p0
    :goto_3
    return-object v0

    #@4
    .restart local p0
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v1, "dsname/"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "ds/"

    #@11
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_21

    #@17
    const-string v1, "ds/"

    #@19
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@1c
    move-result v1

    #@1d
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@20
    move-result-object p0

    #@21
    .end local p0
    :cond_21
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    goto :goto_3
.end method

.method public static toPreferenceKey(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "providerId"
    .parameter "dsId"

    #@0
    .prologue
    .line 76
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "ds/"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-static {p0}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, "/"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-static {p1}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v0

    #@25
    return-object v0
.end method

.method public static toPreferenceProviderNameKey(Ljava/lang/String;)Ljava/lang/String;
    .registers 3
    .parameter "prefKey"

    #@0
    .prologue
    .line 111
    if-nez p0, :cond_4

    #@2
    .line 112
    const/4 v0, 0x0

    #@3
    .line 114
    .end local p0
    :goto_3
    return-object v0

    #@4
    .restart local p0
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v1, "pname/"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, "ds/"

    #@11
    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_21

    #@17
    const-string v1, "ds/"

    #@19
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@1c
    move-result v1

    #@1d
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@20
    move-result-object p0

    #@21
    .end local p0
    :cond_21
    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v0

    #@25
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    goto :goto_3
.end method

.method public static toProviderId(Ljava/lang/String;)Ljava/lang/String;
    .registers 4
    .parameter "preferenceKey"

    #@0
    .prologue
    .line 81
    if-eqz p0, :cond_8

    #@2
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@5
    move-result v2

    #@6
    if-gtz v2, :cond_a

    #@8
    .line 82
    :cond_8
    const/4 v2, 0x0

    #@9
    .line 92
    :goto_9
    return-object v2

    #@a
    .line 85
    :cond_a
    const/4 v0, 0x0

    #@b
    .line 86
    .local v0, providerId:Ljava/lang/String;
    const-string v2, "ds/"

    #@d
    invoke-virtual {p0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_29

    #@13
    .line 87
    const-string v2, "ds/"

    #@15
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@18
    move-result v2

    #@19
    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    .line 91
    :goto_1d
    const/16 v2, 0x2f

    #@1f
    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    #@22
    move-result v1

    #@23
    .line 92
    .local v1, splitIndex:I
    const/4 v2, 0x0

    #@24
    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    goto :goto_9

    #@29
    .line 89
    .end local v1           #splitIndex:I
    :cond_29
    move-object v0, p0

    #@2a
    goto :goto_1d
.end method


# virtual methods
.method public addSession(IILandroid/bluetooth/BluetoothDevice;)Lcom/broadcom/bt/service/map/MapClientSession;
    .registers 9
    .parameter "mseInstanceId"
    .parameter "sessionId"
    .parameter "remoteDevice"

    #@0
    .prologue
    .line 433
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getByMseInstanceId(I)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@3
    move-result-object v1

    #@4
    .line 434
    .local v1, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-nez v1, :cond_20

    #@6
    .line 435
    const-string v2, "BtMap.DataSourceManager"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "addSession() Datasource not found with mseInstanceId="

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 436
    const/4 v0, 0x0

    #@1f
    .line 440
    :goto_1f
    return-object v0

    #@20
    .line 438
    :cond_20
    invoke-virtual {v1, p2, p3}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->addSession(ILandroid/bluetooth/BluetoothDevice;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@23
    move-result-object v0

    #@24
    .line 439
    .local v0, c:Lcom/broadcom/bt/service/map/MapClientSession;
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mSessionToDatasourceMap:Ljava/util/HashMap;

    #@26
    invoke-static {p2, p3}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->toSessionKey(ILandroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    goto :goto_1f
.end method

.method debugDumpState()V
    .registers 9

    #@0
    .prologue
    .line 192
    :try_start_0
    new-instance v3, Ljava/util/LinkedList;

    #@2
    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    #@5
    .line 193
    .local v3, dsCopy:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/broadcom/bt/service/map/MapDatasourceContext;>;"
    iget-object v7, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@7
    monitor-enter v7
    :try_end_8
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_8} :catch_4e

    #@8
    .line 194
    :try_start_8
    iget-object v6, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@a
    invoke-virtual {v3, v6}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    #@d
    .line 195
    monitor-exit v7
    :try_end_e
    .catchall {:try_start_8 .. :try_end_e} :catchall_4b

    #@e
    .line 196
    :try_start_e
    new-instance v0, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    .line 197
    .local v0, buffer:Ljava/lang/StringBuilder;
    const-string v6, "******************Map Datasource States*******************\n"

    #@15
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    .line 198
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    #@1b
    move-result v1

    #@1c
    .line 199
    .local v1, count:I
    if-lez v1, :cond_57

    #@1e
    .line 200
    const/4 v4, 0x0

    #@1f
    .local v4, i:I
    :goto_1f
    if-ge v4, v1, :cond_57

    #@21
    .line 201
    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@24
    move-result-object v2

    #@25
    check-cast v2, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@27
    .line 202
    .local v2, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    new-instance v6, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v7, "DS # "

    #@2e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35
    move-result-object v6

    #@36
    const-string v7, ": \n"

    #@38
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v6

    #@3c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v6

    #@40
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    .line 203
    const-string v6, ""

    #@45
    invoke-virtual {v2, v0, v6}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->debugDumpState(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    :try_end_48
    .catch Ljava/lang/Throwable; {:try_start_e .. :try_end_48} :catch_4e

    #@48
    .line 200
    add-int/lit8 v4, v4, 0x1

    #@4a
    goto :goto_1f

    #@4b
    .line 195
    .end local v0           #buffer:Ljava/lang/StringBuilder;
    .end local v1           #count:I
    .end local v2           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    .end local v4           #i:I
    :catchall_4b
    move-exception v6

    #@4c
    :try_start_4c
    monitor-exit v7
    :try_end_4d
    .catchall {:try_start_4c .. :try_end_4d} :catchall_4b

    #@4d
    :try_start_4d
    throw v6
    :try_end_4e
    .catch Ljava/lang/Throwable; {:try_start_4d .. :try_end_4e} :catch_4e

    #@4e
    .line 209
    .end local v3           #dsCopy:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/broadcom/bt/service/map/MapDatasourceContext;>;"
    :catch_4e
    move-exception v5

    #@4f
    .line 210
    .local v5, t:Ljava/lang/Throwable;
    const-string v6, "BtMap.DataSourceManager"

    #@51
    const-string v7, "DEBUG: error dumping datasource state"

    #@53
    invoke-static {v6, v7, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@56
    .line 212
    .end local v5           #t:Ljava/lang/Throwable;
    :goto_56
    return-void

    #@57
    .line 206
    .restart local v0       #buffer:Ljava/lang/StringBuilder;
    .restart local v1       #count:I
    .restart local v3       #dsCopy:Ljava/util/LinkedList;,"Ljava/util/LinkedList<Lcom/broadcom/bt/service/map/MapDatasourceContext;>;"
    :cond_57
    :try_start_57
    const-string v6, "**********************************************************\n"

    #@59
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    .line 207
    const-string v6, "BtMap.DataSourceManager"

    #@5e
    const-string v7, "DEBUG"

    #@60
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 208
    const-string v6, "BtMap.DataSourceManager"

    #@65
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v7

    #@69
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6c
    .catch Ljava/lang/Throwable; {:try_start_57 .. :try_end_6c} :catch_4e

    #@6c
    goto :goto_56
.end method

.method public get(Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/service/map/MapDatasourceContext;
    .registers 7
    .parameter "providerId"
    .parameter "dsId"

    #@0
    .prologue
    .line 373
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@2
    monitor-enter v3

    #@3
    .line 374
    :try_start_3
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@5
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .local v1, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1d

    #@f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@15
    .line 375
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_9

    #@1b
    .line 376
    monitor-exit v3

    #@1c
    .line 379
    .end local v0           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :goto_1c
    return-object v0

    #@1d
    :cond_1d
    const/4 v0, 0x0

    #@1e
    monitor-exit v3

    #@1f
    goto :goto_1c

    #@20
    .line 380
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_20
    move-exception v2

    #@21
    monitor-exit v3
    :try_end_22
    .catchall {:try_start_3 .. :try_end_22} :catchall_20

    #@22
    throw v2
.end method

.method public getByDsId(BLjava/lang/String;)Lcom/broadcom/bt/service/map/MapDatasourceContext;
    .registers 7
    .parameter "dsType"
    .parameter "dsId"

    #@0
    .prologue
    .line 401
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@2
    monitor-enter v3

    #@3
    .line 402
    :try_start_3
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@5
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .local v1, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_23

    #@f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@15
    .line 403
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    iget-byte v2, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsType:B

    #@17
    if-ne p1, v2, :cond_9

    #@19
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@1b
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_9

    #@21
    .line 404
    monitor-exit v3

    #@22
    .line 407
    .end local v0           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :goto_22
    return-object v0

    #@23
    :cond_23
    const/4 v0, 0x0

    #@24
    monitor-exit v3

    #@25
    goto :goto_22

    #@26
    .line 408
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_26
    move-exception v2

    #@27
    monitor-exit v3
    :try_end_28
    .catchall {:try_start_3 .. :try_end_28} :catchall_26

    #@28
    throw v2
.end method

.method public getByMseInstanceId(I)Lcom/broadcom/bt/service/map/MapDatasourceContext;
    .registers 6
    .parameter "mseInstanceId"

    #@0
    .prologue
    .line 412
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@2
    monitor-enter v3

    #@3
    .line 413
    :try_start_3
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@5
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .local v1, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1b

    #@f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@15
    .line 414
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    iget v2, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@17
    if-ne p1, v2, :cond_9

    #@19
    .line 415
    monitor-exit v3

    #@1a
    .line 418
    .end local v0           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :goto_1a
    return-object v0

    #@1b
    :cond_1b
    const/4 v0, 0x0

    #@1c
    monitor-exit v3

    #@1d
    goto :goto_1a

    #@1e
    .line 419
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_1e
    move-exception v2

    #@1f
    monitor-exit v3
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v2
.end method

.method public getCallback(Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;
    .registers 6
    .parameter "providerId"
    .parameter "dsId"

    #@0
    .prologue
    .line 384
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@2
    monitor-enter v2

    #@3
    .line 385
    :try_start_3
    invoke-virtual {p0, p1, p2}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@6
    move-result-object v0

    #@7
    .line 386
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-eqz v0, :cond_d

    #@9
    .line 387
    iget-object v1, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@b
    monitor-exit v2

    #@c
    .line 390
    :goto_c
    return-object v1

    #@d
    .line 389
    :cond_d
    monitor-exit v2

    #@e
    .line 390
    const/4 v1, 0x0

    #@f
    goto :goto_c

    #@10
    .line 389
    .end local v0           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :catchall_10
    move-exception v1

    #@11
    monitor-exit v2
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v1
.end method

.method public getSession(I)Lcom/broadcom/bt/service/map/MapClientSession;
    .registers 6
    .parameter "sessionId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 423
    invoke-static {p1, v2}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->toSessionKey(ILandroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@4
    move-result-object v1

    #@5
    .line 424
    .local v1, sessionKey:Ljava/lang/String;
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mSessionToDatasourceMap:Ljava/util/HashMap;

    #@7
    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@d
    .line 425
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-nez v0, :cond_10

    #@f
    .line 428
    :goto_f
    return-object v2

    #@10
    :cond_10
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->getSession(I)Lcom/broadcom/bt/service/map/MapClientSession;

    #@13
    move-result-object v2

    #@14
    goto :goto_f
.end method

.method public isRegistered(Ljava/lang/String;BLjava/lang/String;)Z
    .registers 8
    .parameter "providerId"
    .parameter "dsType"
    .parameter "dsId"

    #@0
    .prologue
    .line 354
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@2
    monitor-enter v3

    #@3
    .line 355
    :try_start_3
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@5
    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .local v1, i$:Ljava/util/Iterator;
    :cond_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1e

    #@f
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@15
    .line 356
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    invoke-virtual {v0, p1, p2, p3}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->isEqual(Ljava/lang/String;BLjava/lang/String;)Z

    #@18
    move-result v2

    #@19
    if-eqz v2, :cond_9

    #@1b
    .line 357
    const/4 v2, 0x1

    #@1c
    monitor-exit v3

    #@1d
    .line 360
    .end local v0           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :goto_1d
    return v2

    #@1e
    :cond_1e
    const/4 v2, 0x0

    #@1f
    monitor-exit v3

    #@20
    goto :goto_1d

    #@21
    .line 361
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_21
    move-exception v2

    #@22
    monitor-exit v3
    :try_end_23
    .catchall {:try_start_3 .. :try_end_23} :catchall_21

    #@23
    throw v2
.end method

.method public register(Landroid/content/Context;Lcom/broadcom/bt/service/map/MapDatasourceContext;)Z
    .registers 15
    .parameter "ctx"
    .parameter "datasource"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 215
    iget-object v11, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@3
    monitor-enter v11

    #@4
    .line 216
    :try_start_4
    iget-object v0, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@6
    iget-object v1, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@8
    invoke-direct {p0, p1, v0, v1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->isDataSourceStateStored(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_1c

    #@e
    .line 217
    iget-object v2, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@10
    iget-object v3, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@12
    iget-object v4, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderDisplayName:Ljava/lang/String;

    #@14
    iget-object v5, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsDisplayName:Ljava/lang/String;

    #@16
    const/4 v6, 0x1

    #@17
    move-object v0, p0

    #@18
    move-object v1, p1

    #@19
    invoke-direct/range {v0 .. v6}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->createStoredDataSourceState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Z

    #@1c
    .line 220
    :cond_1c
    iget-object v0, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@1e
    iget-object v1, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@20
    invoke-direct {p0, p1, v0, v1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->isEnableStored(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    #@23
    move-result v9

    #@24
    .line 222
    .local v9, setEnabled:Z
    const/4 v8, 0x0

    #@25
    .local v8, i:I
    :goto_25
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@27
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    #@2a
    move-result v0

    #@2b
    if-ge v8, v0, :cond_4e

    #@2d
    .line 223
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@2f
    invoke-virtual {v0, v8}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@32
    move-result-object v7

    #@33
    check-cast v7, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@35
    .line 224
    .local v7, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    iget-object v0, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@37
    iget-byte v1, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsType:B

    #@39
    iget-object v2, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@3b
    invoke-virtual {v7, v0, v1, v2}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->isEqual(Ljava/lang/String;BLjava/lang/String;)Z

    #@3e
    move-result v0

    #@3f
    if-eqz v0, :cond_4b

    #@41
    .line 225
    iput-boolean v9, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mIsEnabled:Z

    #@43
    .line 226
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@45
    invoke-virtual {v0, v8, p2}, Ljava/util/LinkedList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    #@48
    .line 227
    monitor-exit v11

    #@49
    move v0, v10

    #@4a
    .line 240
    .end local v7           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :goto_4a
    return v0

    #@4b
    .line 222
    .restart local v7       #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :cond_4b
    add-int/lit8 v8, v8, 0x1

    #@4d
    goto :goto_25

    #@4e
    .line 232
    .end local v7           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :cond_4e
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@50
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    #@53
    move-result v0

    #@54
    const/4 v1, 0x5

    #@55
    if-lt v0, v1, :cond_64

    #@57
    .line 233
    const-string v0, "BtMap.DataSourceManager"

    #@59
    const-string v1, "Maximum of 5datasources allowed."

    #@5b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 235
    const/4 v0, 0x0

    #@5f
    monitor-exit v11

    #@60
    goto :goto_4a

    #@61
    .line 241
    .end local v8           #i:I
    .end local v9           #setEnabled:Z
    :catchall_61
    move-exception v0

    #@62
    monitor-exit v11
    :try_end_63
    .catchall {:try_start_4 .. :try_end_63} :catchall_61

    #@63
    throw v0

    #@64
    .line 237
    .restart local v8       #i:I
    .restart local v9       #setEnabled:Z
    :cond_64
    :try_start_64
    iput-boolean v9, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mIsEnabled:Z

    #@66
    .line 238
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@68
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    #@6b
    move-result v0

    #@6c
    iput v0, p2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@6e
    .line 239
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@70
    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@73
    .line 240
    monitor-exit v11
    :try_end_74
    .catchall {:try_start_64 .. :try_end_74} :catchall_61

    #@74
    move v0, v10

    #@75
    goto :goto_4a
.end method

.method public removeSession(IILandroid/bluetooth/BluetoothDevice;)Lcom/broadcom/bt/service/map/MapClientSession;
    .registers 9
    .parameter "mseInstanceId"
    .parameter "sessionId"
    .parameter "remoteDevice"

    #@0
    .prologue
    .line 445
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getByMseInstanceId(I)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@3
    move-result-object v1

    #@4
    .line 446
    .local v1, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-nez v1, :cond_20

    #@6
    .line 447
    const-string v2, "BtMap.DataSourceManager"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "removeSession() Datasource not found with mseInstanceId="

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 448
    const/4 v0, 0x0

    #@1f
    .line 452
    :goto_1f
    return-object v0

    #@20
    .line 450
    :cond_20
    invoke-virtual {v1, p2, p3}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->removeSession(ILandroid/bluetooth/BluetoothDevice;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@23
    move-result-object v0

    #@24
    .line 451
    .local v0, c:Lcom/broadcom/bt/service/map/MapClientSession;
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mSessionToDatasourceMap:Ljava/util/HashMap;

    #@26
    invoke-static {p2, p3}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->toSessionKey(ILandroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    goto :goto_1f
.end method

.method public sendNotification(Lcom/broadcom/bt/service/map/MapService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V
    .registers 24
    .parameter "svc"
    .parameter "providerId"
    .parameter "datasourceId"
    .parameter "messageId"
    .parameter "messageType"
    .parameter "notificationType"
    .parameter "dsFolderPath"
    .parameter "dsOldFolderPath"

    #@0
    .prologue
    .line 462
    move-object/from16 v0, p2

    #@2
    move-object/from16 v1, p3

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@7
    move-result-object v10

    #@8
    .line 463
    .local v10, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-nez v10, :cond_25

    #@a
    .line 464
    const-string v2, "BtMap.DataSourceManager"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "sendNotification(): Data source not found for with ID: "

    #@13
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    move-object/from16 v0, p3

    #@19
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 539
    :cond_24
    :goto_24
    return-void

    #@25
    .line 469
    :cond_25
    invoke-virtual {v10}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->getSessionMapCopy()Ljava/util/HashMap;

    #@28
    move-result-object v11

    #@29
    .line 470
    .local v11, mClonedSessions:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/broadcom/bt/service/map/MapClientSession;>;"
    if-eqz v11, :cond_31

    #@2b
    invoke-virtual {v11}, Ljava/util/HashMap;->size()I

    #@2e
    move-result v2

    #@2f
    if-nez v2, :cond_4c

    #@31
    .line 471
    :cond_31
    const-string v2, "BtMap.DataSourceManager"

    #@33
    new-instance v3, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v6, "sendNotification(): no sessions active for MSE "

    #@3a
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    move-object/from16 v0, p3

    #@40
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    goto :goto_24

    #@4c
    .line 476
    :cond_4c
    invoke-virtual {v11}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@4f
    move-result-object v2

    #@50
    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@53
    move-result-object v12

    #@54
    .line 477
    .local v12, sIterator:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/map/MapClientSession;>;"
    if-eqz v12, :cond_5c

    #@56
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@59
    move-result v2

    #@5a
    if-nez v2, :cond_77

    #@5c
    .line 478
    :cond_5c
    const-string v2, "BtMap.DataSourceManager"

    #@5e
    new-instance v3, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v6, "sendNotification(): no sessions active for MSE "

    #@65
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    move-object/from16 v0, p3

    #@6b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    goto :goto_24

    #@77
    .line 484
    :cond_77
    if-nez p6, :cond_e9

    #@79
    .line 485
    :goto_79
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@7c
    move-result v2

    #@7d
    if-eqz v2, :cond_24

    #@7f
    .line 487
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@82
    move-result-object v13

    #@83
    check-cast v13, Lcom/broadcom/bt/service/map/MapClientSession;

    #@85
    .line 488
    .local v13, session:Lcom/broadcom/bt/service/map/MapClientSession;
    const/4 v2, 0x0

    #@86
    move-object/from16 v0, p7

    #@88
    invoke-virtual {v13, v0, v2}, Lcom/broadcom/bt/service/map/MapClientSession;->getVirtualPath(Ljava/lang/String;Z)Ljava/lang/String;

    #@8b
    move-result-object v8

    #@8c
    .line 489
    .local v8, vFolderPath:Ljava/lang/String;
    move-object/from16 v0, p4

    #@8e
    move-object/from16 v1, p7

    #@90
    invoke-virtual {v13, v0, v1}, Lcom/broadcom/bt/service/map/MapClientSession;->addNewMessageHandleToMap(Ljava/lang/String;Ljava/lang/String;)J

    #@93
    move-result-wide v4

    #@94
    .line 491
    .local v4, messageHandle:J
    const-string v2, "BtMap.DataSourceManager"

    #@96
    new-instance v3, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v6, "sendNotification(): added new message to session "

    #@9d
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v3

    #@a1
    iget v6, v13, Lcom/broadcom/bt/service/map/MapClientSession;->mSessionId:I

    #@a3
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v3

    #@a7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v3

    #@ab
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 493
    const-string v2, "BtMap.DataSourceManager"

    #@b0
    new-instance v3, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v6, "sendNotification(): New Message: messageId<=>messageHandle : "

    #@b7
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v3

    #@bb
    move-object/from16 v0, p4

    #@bd
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v3

    #@c1
    const-string v6, "<==>"

    #@c3
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v3

    #@c7
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v3

    #@cb
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v3

    #@cf
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    .line 497
    :try_start_d2
    iget v3, v10, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@d4
    const-string v9, ""

    #@d6
    move-object/from16 v2, p1

    #@d8
    move/from16 v6, p5

    #@da
    move/from16 v7, p6

    #@dc
    invoke-virtual/range {v2 .. v9}, Lcom/broadcom/bt/service/map/MapService;->sendNotification(IJBBLjava/lang/String;Ljava/lang/String;)V
    :try_end_df
    .catch Ljava/lang/Throwable; {:try_start_d2 .. :try_end_df} :catch_e0

    #@df
    goto :goto_79

    #@e0
    .line 499
    :catch_e0
    move-exception v14

    #@e1
    .line 500
    .local v14, t:Ljava/lang/Throwable;
    const-string v2, "BtMap.DataSourceManager"

    #@e3
    const-string v3, "sendNotification(): Error sending notification"

    #@e5
    invoke-static {v2, v3, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e8
    goto :goto_79

    #@e9
    .line 506
    .end local v4           #messageHandle:J
    .end local v8           #vFolderPath:Ljava/lang/String;
    .end local v13           #session:Lcom/broadcom/bt/service/map/MapClientSession;
    .end local v14           #t:Ljava/lang/Throwable;
    :cond_e9
    :goto_e9
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    #@ec
    move-result v2

    #@ed
    if-eqz v2, :cond_24

    #@ef
    .line 507
    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f2
    move-result-object v13

    #@f3
    check-cast v13, Lcom/broadcom/bt/service/map/MapClientSession;

    #@f5
    .line 509
    .restart local v13       #session:Lcom/broadcom/bt/service/map/MapClientSession;
    move-object/from16 v0, p4

    #@f7
    invoke-virtual {v13, v0}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageHandleFromId(Ljava/lang/String;)J

    #@fa
    move-result-wide v4

    #@fb
    .line 510
    .restart local v4       #messageHandle:J
    const-wide/16 v2, -0x1

    #@fd
    cmp-long v2, v4, v2

    #@ff
    if-nez v2, :cond_109

    #@101
    .line 513
    move-object/from16 v0, p4

    #@103
    move-object/from16 v1, p7

    #@105
    invoke-virtual {v13, v0, v1}, Lcom/broadcom/bt/service/map/MapClientSession;->addNewMessageHandleToMap(Ljava/lang/String;Ljava/lang/String;)J

    #@108
    move-result-wide v4

    #@109
    .line 515
    :cond_109
    const-wide/16 v2, -0x1

    #@10b
    cmp-long v2, v4, v2

    #@10d
    if-eqz v2, :cond_18e

    #@10f
    .line 519
    const/4 v2, 0x0

    #@110
    move-object/from16 v0, p7

    #@112
    invoke-virtual {v13, v0, v2}, Lcom/broadcom/bt/service/map/MapClientSession;->getVirtualPath(Ljava/lang/String;Z)Ljava/lang/String;

    #@115
    move-result-object v8

    #@116
    .line 520
    .restart local v8       #vFolderPath:Ljava/lang/String;
    if-nez p8, :cond_186

    #@118
    const/4 v9, 0x0

    #@119
    .line 522
    .local v9, vOldFolderPath:Ljava/lang/String;
    :goto_119
    const-string v2, "BtMap.DataSourceManager"

    #@11b
    new-instance v3, Ljava/lang/StringBuilder;

    #@11d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@120
    const-string v6, "Sending notification on MSE Instance: "

    #@122
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@125
    move-result-object v3

    #@126
    iget v6, v10, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@128
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v3

    #@12c
    const-string v6, ", sessionId="

    #@12e
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v3

    #@132
    iget v6, v13, Lcom/broadcom/bt/service/map/MapClientSession;->mSessionId:I

    #@134
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@137
    move-result-object v3

    #@138
    const-string v6, ", messageHandle="

    #@13a
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v3

    #@13e
    invoke-static/range {p4 .. p4}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@141
    move-result-object v6

    #@142
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v3

    #@146
    const-string v6, ", messageId="

    #@148
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v3

    #@14c
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v3

    #@150
    const-string v6, ", vFolderPath="

    #@152
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v3

    #@156
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@159
    move-result-object v3

    #@15a
    const-string v6, ", vOldFolderPath="

    #@15c
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15f
    move-result-object v3

    #@160
    invoke-static {v9}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@163
    move-result-object v6

    #@164
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    move-result-object v3

    #@168
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v3

    #@16c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16f
    .line 529
    :try_start_16f
    iget v3, v10, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@171
    move-object/from16 v2, p1

    #@173
    move/from16 v6, p5

    #@175
    move/from16 v7, p6

    #@177
    invoke-virtual/range {v2 .. v9}, Lcom/broadcom/bt/service/map/MapService;->sendNotification(IJBBLjava/lang/String;Ljava/lang/String;)V
    :try_end_17a
    .catch Ljava/lang/Throwable; {:try_start_16f .. :try_end_17a} :catch_17c

    #@17a
    goto/16 :goto_e9

    #@17c
    .line 531
    :catch_17c
    move-exception v14

    #@17d
    .line 532
    .restart local v14       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.DataSourceManager"

    #@17f
    const-string v3, "sendNotification(): Error sending notification"

    #@181
    invoke-static {v2, v3, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@184
    goto/16 :goto_e9

    #@186
    .line 520
    .end local v9           #vOldFolderPath:Ljava/lang/String;
    .end local v14           #t:Ljava/lang/Throwable;
    :cond_186
    const/4 v2, 0x0

    #@187
    move-object/from16 v0, p8

    #@189
    invoke-virtual {v13, v0, v2}, Lcom/broadcom/bt/service/map/MapClientSession;->getVirtualPath(Ljava/lang/String;Z)Ljava/lang/String;

    #@18c
    move-result-object v9

    #@18d
    goto :goto_119

    #@18e
    .line 535
    .end local v8           #vFolderPath:Ljava/lang/String;
    :cond_18e
    const-string v2, "BtMap.DataSourceManager"

    #@190
    new-instance v3, Ljava/lang/StringBuilder;

    #@192
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@195
    const-string v6, "sendNotification(): unable to find message handle for messageId "

    #@197
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v3

    #@19b
    move-object/from16 v0, p4

    #@19d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a0
    move-result-object v3

    #@1a1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a4
    move-result-object v3

    #@1a5
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a8
    goto/16 :goto_e9
.end method

.method public setEnabled(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 10
    .parameter "ctx"
    .parameter "providerId"
    .parameter "dsId"
    .parameter "enabled"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 261
    if-nez p1, :cond_b

    #@3
    .line 262
    const-string v2, "BtMap.DataSourceManager"

    #@5
    const-string v3, "setEnabled(): application context is not available"

    #@7
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 275
    :goto_a
    return v1

    #@b
    .line 266
    :cond_b
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@d
    monitor-enter v2

    #@e
    .line 267
    :try_start_e
    invoke-virtual {p0, p2, p3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@11
    move-result-object v0

    #@12
    .line 268
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-nez v0, :cond_20

    #@14
    .line 269
    const-string v3, "BtMap.DataSourceManager"

    #@16
    const-string v4, "setEnabled(): datasource not found"

    #@18
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 270
    monitor-exit v2

    #@1c
    goto :goto_a

    #@1d
    .line 274
    .end local v0           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :catchall_1d
    move-exception v1

    #@1e
    monitor-exit v2
    :try_end_1f
    .catchall {:try_start_e .. :try_end_1f} :catchall_1d

    #@1f
    throw v1

    #@20
    .line 272
    .restart local v0       #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :cond_20
    :try_start_20
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->setStoredDataSourceState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    #@23
    .line 273
    iput-boolean p4, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mIsEnabled:Z

    #@25
    .line 274
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_20 .. :try_end_26} :catchall_1d

    #@26
    .line 275
    const/4 v1, 0x1

    #@27
    goto :goto_a
.end method

.method public unregister(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 9
    .parameter "ctx"
    .parameter "providerId"
    .parameter "dsId"
    .parameter "removePermanently"

    #@0
    .prologue
    .line 245
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@2
    monitor-enter v3

    #@3
    .line 246
    const/4 v1, 0x0

    #@4
    .local v1, i:I
    :goto_4
    :try_start_4
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@6
    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    #@9
    move-result v2

    #@a
    if-ge v1, v2, :cond_25

    #@c
    .line 247
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@e
    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    #@11
    move-result-object v0

    #@12
    check-cast v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@14
    .line 248
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    invoke-virtual {v0, p2, p3}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->isEqual(Ljava/lang/String;Ljava/lang/String;)Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_22

    #@1a
    .line 249
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDataSourceManager;->mDatasources:Ljava/util/LinkedList;

    #@1c
    invoke-virtual {v2, v1}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    #@1f
    .line 250
    const/4 v2, 0x1

    #@20
    monitor-exit v3

    #@21
    .line 257
    .end local v0           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :goto_21
    return v2

    #@22
    .line 246
    .restart local v0       #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :cond_22
    add-int/lit8 v1, v1, 0x1

    #@24
    goto :goto_4

    #@25
    .line 253
    .end local v0           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :cond_25
    if-eqz p4, :cond_2a

    #@27
    .line 254
    invoke-direct {p0, p1, p2, p3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->removeStoredDataSourceState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    #@2a
    .line 256
    :cond_2a
    monitor-exit v3

    #@2b
    .line 257
    const/4 v2, 0x0

    #@2c
    goto :goto_21

    #@2d
    .line 256
    :catchall_2d
    move-exception v2

    #@2e
    monitor-exit v3
    :try_end_2f
    .catchall {:try_start_4 .. :try_end_2f} :catchall_2d

    #@2f
    throw v2
.end method

.method public updateMessageId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "providerId"
    .parameter "datasourceId"
    .parameter "oldMessageId"
    .parameter "newMessageId"

    #@0
    .prologue
    .line 544
    invoke-virtual {p0, p1, p2}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@3
    move-result-object v0

    #@4
    .line 545
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-nez v0, :cond_1f

    #@6
    .line 546
    const-string v4, "BtMap.DataSourceManager"

    #@8
    new-instance v5, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v6, "updateMessageId(): Data source not found for with ID: "

    #@f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v5

    #@1b
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 569
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 551
    :cond_1f
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->getSessionMapCopy()Ljava/util/HashMap;

    #@22
    move-result-object v2

    #@23
    .line 552
    .local v2, mClonedSessions:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/broadcom/bt/service/map/MapClientSession;>;"
    if-eqz v2, :cond_2b

    #@25
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@28
    move-result v4

    #@29
    if-nez v4, :cond_44

    #@2b
    .line 553
    :cond_2b
    const-string v4, "BtMap.DataSourceManager"

    #@2d
    new-instance v5, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v6, "updateMessageId(): no sessions active for MSE "

    #@34
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v5

    #@40
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_1e

    #@44
    .line 558
    :cond_44
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@47
    move-result-object v4

    #@48
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@4b
    move-result-object v1

    #@4c
    .line 560
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/map/MapClientSession;>;"
    if-eqz v1, :cond_54

    #@4e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@51
    move-result v4

    #@52
    if-nez v4, :cond_5c

    #@54
    .line 561
    :cond_54
    const-string v4, "BtMap.DataSourceManager"

    #@56
    const-string v5, "ITerator for MapClientSession is null!"

    #@58
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    goto :goto_1e

    #@5c
    .line 565
    :cond_5c
    :goto_5c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@5f
    move-result v4

    #@60
    if-eqz v4, :cond_1e

    #@62
    .line 566
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@65
    move-result-object v3

    #@66
    check-cast v3, Lcom/broadcom/bt/service/map/MapClientSession;

    #@68
    .line 567
    .local v3, s:Lcom/broadcom/bt/service/map/MapClientSession;
    invoke-virtual {v3, p3, p4}, Lcom/broadcom/bt/service/map/MapClientSession;->updateMessageId(Ljava/lang/String;Ljava/lang/String;)V

    #@6b
    goto :goto_5c
.end method
