.class public Lcom/broadcom/bt/service/sap/SapService;
.super Lcom/android/bluetooth/btservice/ProfileService;
.source "SapService.java"

# interfaces
.implements Lcom/lge/bluetooth/LGBluetoothSapManager$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;
    }
.end annotation


# static fields
.field private static final CONN_STATE_CONNECTED:I = 0x0

.field private static final CONN_STATE_DISCONNECTED:I = 0x1

.field private static final CONN_STATE_UNKNOWN:I = 0x3

.field private static final DBG:Z = true

.field private static final DISCONNECT_TYPE_GRACEFUL:I = 0x0

.field private static final DISCONNECT_TYPE_IMMEDIATE:I = 0x1

.field private static final MESSAGE_CONNECT_STATE_CHANGED:I = 0x2

.field private static final MESSAGE_DISCONNECT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SapService"

.field private static mManager:Lcom/lge/bluetooth/LGBluetoothSapManager;

.field private static mSapDevice:Landroid/bluetooth/BluetoothDevice;


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mNativeAvailable:Z

.field private mSapDevices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .registers 0

    #@0
    .prologue
    .line 68
    invoke-static {}, Lcom/broadcom/bt/service/sap/SapService;->classInitNative()V

    #@3
    .line 69
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 56
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/ProfileService;-><init>()V

    #@3
    .line 118
    new-instance v0, Lcom/broadcom/bt/service/sap/SapService$1;

    #@5
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/sap/SapService$1;-><init>(Lcom/broadcom/bt/service/sap/SapService;)V

    #@8
    iput-object v0, p0, Lcom/broadcom/bt/service/sap/SapService;->mHandler:Landroid/os/Handler;

    #@a
    .line 158
    return-void
.end method

.method static synthetic access$000(Lcom/broadcom/bt/service/sap/SapService;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/sap/SapService;->disconnectSapNative(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Lcom/broadcom/bt/service/sap/SapService;Landroid/bluetooth/BluetoothDevice;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/sap/SapService;->broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;I)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/broadcom/bt/service/sap/SapService;[B)Landroid/bluetooth/BluetoothDevice;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 56
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/sap/SapService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$300(Lcom/broadcom/bt/service/sap/SapService;)Ljava/util/Map;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method static synthetic access$400(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    invoke-static {p0}, Lcom/broadcom/bt/service/sap/SapService;->convertHalState(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Lcom/broadcom/bt/service/sap/SapService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/broadcom/bt/service/sap/SapService;->isAvailable()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;I)V
    .registers 10
    .parameter "device"
    .parameter "newState"

    #@0
    .prologue
    .line 248
    iget-object v4, p0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;

    #@2
    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v3

    #@6
    check-cast v3, Ljava/lang/Integer;

    #@8
    .line 249
    .local v3, prevStateInteger:Ljava/lang/Integer;
    if-nez v3, :cond_26

    #@a
    const/4 v2, 0x0

    #@b
    .line 251
    .local v2, prevState:I
    :goto_b
    if-ne v2, p2, :cond_2b

    #@d
    .line 252
    const-string v4, "SapService"

    #@f
    new-instance v5, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v6, "no state change: "

    #@16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 287
    :cond_25
    :goto_25
    return-void

    #@26
    .line 249
    .end local v2           #prevState:I
    :cond_26
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@29
    move-result v2

    #@2a
    goto :goto_b

    #@2b
    .line 257
    .restart local v2       #prevState:I
    :cond_2b
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothSapManager;->isCardPresent()Z

    #@2e
    move-result v4

    #@2f
    if-nez v4, :cond_4c

    #@31
    .line 258
    const/4 v4, 0x2

    #@32
    if-ne p2, v4, :cond_4c

    #@34
    .line 259
    const-string v4, "SapService"

    #@36
    const-string v5, "[BTUI] no SIM card : disconnect"

    #@38
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 260
    if-eqz p1, :cond_25

    #@3d
    .line 261
    iget-object v4, p0, Lcom/broadcom/bt/service/sap/SapService;->mHandler:Landroid/os/Handler;

    #@3f
    const/4 v5, 0x1

    #@40
    invoke-virtual {v4, v5, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@43
    move-result-object v1

    #@44
    .line 262
    .local v1, msg:Landroid/os/Message;
    iget-object v4, p0, Lcom/broadcom/bt/service/sap/SapService;->mHandler:Landroid/os/Handler;

    #@46
    const-wide/16 v5, 0x64

    #@48
    invoke-virtual {v4, v1, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@4b
    goto :goto_25

    #@4c
    .line 269
    .end local v1           #msg:Landroid/os/Message;
    :cond_4c
    iget-object v4, p0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;

    #@4e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@51
    move-result-object v5

    #@52
    invoke-interface {v4, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@55
    .line 275
    new-instance v4, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v5, "Connection state "

    #@5c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v4

    #@60
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    const-string v5, ": "

    #@66
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v4

    #@6a
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v4

    #@6e
    const-string v5, "->"

    #@70
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    invoke-virtual {p0, v4}, Lcom/broadcom/bt/service/sap/SapService;->log(Ljava/lang/String;)V

    #@7f
    .line 277
    const/16 v4, 0x65

    #@81
    invoke-virtual {p0, p1, v4, p2, v2}, Lcom/broadcom/bt/service/sap/SapService;->notifyProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V

    #@84
    .line 281
    new-instance v0, Landroid/content/Intent;

    #@86
    const-string v4, "com.lge.bluetooth.sap.CONNECTION_STATE_CHANGED"

    #@88
    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@8b
    .line 283
    .local v0, intent:Landroid/content/Intent;
    const-string v4, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    #@8d
    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@90
    .line 284
    const-string v4, "android.bluetooth.profile.extra.STATE"

    #@92
    invoke-virtual {v0, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@95
    .line 285
    const-string v4, "android.bluetooth.device.extra.DEVICE"

    #@97
    invoke-virtual {v0, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@9a
    .line 286
    const-string v4, "android.permission.BLUETOOTH"

    #@9c
    invoke-virtual {p0, v0, v4}, Lcom/broadcom/bt/service/sap/SapService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@9f
    goto :goto_25
.end method

.method private static native classInitNative()V
.end method

.method private native cleanupNative()V
.end method

.method private static convertHalState(I)I
    .registers 5
    .parameter "halState"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 291
    packed-switch p0, :pswitch_data_20

    #@4
    .line 297
    const-string v1, "SapService"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "bad sap connection state: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 298
    :goto_1c
    :pswitch_1c
    return v0

    #@1d
    .line 293
    :pswitch_1d
    const/4 v0, 0x2

    #@1e
    goto :goto_1c

    #@1f
    .line 291
    nop

    #@20
    :pswitch_data_20
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1c
    .end packed-switch
.end method

.method private native disconnectSapNative(I)Z
.end method

.method private native initializeNative()V
.end method

.method private onClientRequestCallback(III[B)V
    .registers 6
    .parameter "req_type"
    .parameter "is_apdu_7816"
    .parameter "req_len"
    .parameter "req_data"

    #@0
    .prologue
    .line 334
    sget-object v0, Lcom/broadcom/bt/service/sap/SapService;->mManager:Lcom/lge/bluetooth/LGBluetoothSapManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 335
    sget-object v0, Lcom/broadcom/bt/service/sap/SapService;->mManager:Lcom/lge/bluetooth/LGBluetoothSapManager;

    #@6
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/lge/bluetooth/LGBluetoothSapManager;->onSapClientRequest(III[B)V

    #@9
    .line 337
    :cond_9
    return-void
.end method

.method private onConnectStateChanged([BI)V
    .registers 6
    .parameter "address"
    .parameter "state"

    #@0
    .prologue
    .line 237
    iget-object v1, p0, Lcom/broadcom/bt/service/sap/SapService;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v2, 0x2

    #@3
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 238
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 239
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@b
    .line 240
    iget-object v1, p0, Lcom/broadcom/bt/service/sap/SapService;->mHandler:Landroid/os/Handler;

    #@d
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@10
    .line 242
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/sap/SapService;->setConnectedDevice([BI)V

    #@13
    .line 244
    return-void
.end method

.method private native sapResponseNative(IIII[B)Z
.end method

.method private setConnectedDevice([BI)V
    .registers 5
    .parameter "address"
    .parameter "state"

    #@0
    .prologue
    .line 325
    invoke-static {p2}, Lcom/broadcom/bt/service/sap/SapService;->convertHalState(I)I

    #@3
    move-result v0

    #@4
    const/4 v1, 0x2

    #@5
    if-ne v0, v1, :cond_f

    #@7
    .line 326
    if-eqz p1, :cond_f

    #@9
    .line 327
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/sap/SapService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@c
    move-result-object v0

    #@d
    sput-object v0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevice:Landroid/bluetooth/BluetoothDevice;

    #@f
    .line 330
    :cond_f
    return-void
.end method


# virtual methods
.method public callbackSapDisconnect(I)V
    .registers 4
    .parameter "disconnect_type"

    #@0
    .prologue
    .line 346
    sget-object v0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    const/4 v1, 0x3

    #@3
    invoke-direct {p0, v0, v1}, Lcom/broadcom/bt/service/sap/SapService;->broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;I)V

    #@6
    .line 347
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/sap/SapService;->disconnectSapNative(I)Z

    #@9
    .line 348
    return-void
.end method

.method public callbackSapResponse(IIII[B)V
    .registers 6
    .parameter "resp_type"
    .parameter "result_code"
    .parameter "is_apdu_7816"
    .parameter "resp_len"
    .parameter "resp_data"

    #@0
    .prologue
    .line 342
    invoke-direct/range {p0 .. p5}, Lcom/broadcom/bt/service/sap/SapService;->sapResponseNative(IIII[B)Z

    #@3
    .line 343
    return-void
.end method

.method protected cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 106
    iget-boolean v0, p0, Lcom/broadcom/bt/service/sap/SapService;->mNativeAvailable:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 107
    invoke-direct {p0}, Lcom/broadcom/bt/service/sap/SapService;->cleanupNative()V

    #@7
    .line 108
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/broadcom/bt/service/sap/SapService;->mNativeAvailable:Z

    #@a
    .line 111
    :cond_a
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;

    #@c
    if-eqz v0, :cond_16

    #@e
    .line 112
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;

    #@10
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    #@13
    .line 113
    const/4 v0, 0x0

    #@14
    iput-object v0, p0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;

    #@16
    .line 115
    :cond_16
    const/4 v0, 0x1

    #@17
    return v0
.end method

.method disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 207
    const-string v1, "android.permission.BLUETOOTH"

    #@3
    const-string v2, "Need BLUETOOTH permission"

    #@5
    invoke-virtual {p0, v1, v2}, Lcom/broadcom/bt/service/sap/SapService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 208
    iget-object v1, p0, Lcom/broadcom/bt/service/sap/SapService;->mHandler:Landroid/os/Handler;

    #@a
    invoke-virtual {v1, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 209
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/broadcom/bt/service/sap/SapService;->mHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 210
    return v3
.end method

.method getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    if-nez v0, :cond_a

    #@8
    .line 215
    const/4 v0, 0x0

    #@9
    .line 217
    :goto_9
    return v0

    #@a
    :cond_a
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;

    #@c
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Ljava/lang/Integer;

    #@12
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@15
    move-result v0

    #@16
    goto :goto_9
.end method

.method getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 12
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 221
    const-string v8, "android.permission.BLUETOOTH"

    #@2
    const-string v9, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v8, v9}, Lcom/broadcom/bt/service/sap/SapService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 222
    new-instance v6, Ljava/util/ArrayList;

    #@9
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@c
    .line 224
    .local v6, sapDevices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    iget-object v8, p0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;

    #@e
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@11
    move-result-object v8

    #@12
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v2

    #@16
    :cond_16
    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v8

    #@1a
    if-eqz v8, :cond_36

    #@1c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@22
    .line 225
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p0, v1}, Lcom/broadcom/bt/service/sap/SapService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@25
    move-result v5

    #@26
    .line 226
    .local v5, sapDeviceState:I
    move-object v0, p1

    #@27
    .local v0, arr$:[I
    array-length v4, v0

    #@28
    .local v4, len$:I
    const/4 v3, 0x0

    #@29
    .local v3, i$:I
    :goto_29
    if-ge v3, v4, :cond_16

    #@2b
    aget v7, v0, v3

    #@2d
    .line 227
    .local v7, state:I
    if-ne v7, v5, :cond_33

    #@2f
    .line 228
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@32
    goto :goto_16

    #@33
    .line 226
    :cond_33
    add-int/lit8 v3, v3, 0x1

    #@35
    goto :goto_29

    #@36
    .line 233
    .end local v0           #arr$:[I
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v5           #sapDeviceState:I
    .end local v7           #state:I
    :cond_36
    return-object v6
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 72
    const-string v0, "SapService"

    #@2
    return-object v0
.end method

.method public initBinder()Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;
    .registers 2

    #@0
    .prologue
    .line 76
    new-instance v0, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;

    #@2
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;-><init>(Lcom/broadcom/bt/service/sap/SapService;)V

    #@5
    return-object v0
.end method

.method protected start()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 80
    new-instance v0, Ljava/util/HashMap;

    #@3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@6
    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;

    #@c
    .line 81
    invoke-direct {p0}, Lcom/broadcom/bt/service/sap/SapService;->initializeNative()V

    #@f
    .line 82
    iput-boolean v1, p0, Lcom/broadcom/bt/service/sap/SapService;->mNativeAvailable:Z

    #@11
    .line 84
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothSapManager;->getInstance(Landroid/content/Context;)Lcom/lge/bluetooth/LGBluetoothSapManager;

    #@14
    move-result-object v0

    #@15
    sput-object v0, Lcom/broadcom/bt/service/sap/SapService;->mManager:Lcom/lge/bluetooth/LGBluetoothSapManager;

    #@17
    .line 85
    sget-object v0, Lcom/broadcom/bt/service/sap/SapService;->mManager:Lcom/lge/bluetooth/LGBluetoothSapManager;

    #@19
    if-eqz v0, :cond_20

    #@1b
    .line 86
    sget-object v0, Lcom/broadcom/bt/service/sap/SapService;->mManager:Lcom/lge/bluetooth/LGBluetoothSapManager;

    #@1d
    invoke-virtual {v0, p0}, Lcom/lge/bluetooth/LGBluetoothSapManager;->registerCallback(Lcom/lge/bluetooth/LGBluetoothSapManager$Callback;)V

    #@20
    .line 89
    :cond_20
    return v1
.end method

.method protected stop()Z
    .registers 2

    #@0
    .prologue
    .line 94
    const-string v0, "Stopping Bluetooth SapService"

    #@2
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/sap/SapService;->log(Ljava/lang/String;)V

    #@5
    .line 97
    sget-object v0, Lcom/broadcom/bt/service/sap/SapService;->mManager:Lcom/lge/bluetooth/LGBluetoothSapManager;

    #@7
    if-eqz v0, :cond_13

    #@9
    .line 98
    sget-object v0, Lcom/broadcom/bt/service/sap/SapService;->mManager:Lcom/lge/bluetooth/LGBluetoothSapManager;

    #@b
    invoke-virtual {v0, p0}, Lcom/lge/bluetooth/LGBluetoothSapManager;->unregisterCallback(Lcom/lge/bluetooth/LGBluetoothSapManager$Callback;)V

    #@e
    .line 99
    sget-object v0, Lcom/broadcom/bt/service/sap/SapService;->mManager:Lcom/lge/bluetooth/LGBluetoothSapManager;

    #@10
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothSapManager;->terminateSapManager()V

    #@13
    .line 102
    :cond_13
    const/4 v0, 0x1

    #@14
    return v0
.end method
