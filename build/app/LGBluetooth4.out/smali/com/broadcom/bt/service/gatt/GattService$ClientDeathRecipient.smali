.class Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;
.super Ljava/lang/Object;
.source "GattService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/gatt/GattService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ClientDeathRecipient"
.end annotation


# instance fields
.field mAppIf:B

.field final synthetic this$0:Lcom/broadcom/bt/service/gatt/GattService;


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/service/gatt/GattService;B)V
    .registers 3
    .parameter
    .parameter "appIf"

    #@0
    .prologue
    .line 275
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;->this$0:Lcom/broadcom/bt/service/gatt/GattService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 276
    iput-byte p2, p0, Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;->mAppIf:B

    #@7
    .line 277
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 5

    #@0
    .prologue
    .line 281
    const-string v1, "BtGatt.GattService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Binder is dead - unregistering client ("

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget-byte v3, p0, Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;->mAppIf:B

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, ")!"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 284
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;->this$0:Lcom/broadcom/bt/service/gatt/GattService;

    #@22
    iget-object v1, v1, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@24
    iget-byte v2, p0, Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;->mAppIf:B

    #@26
    invoke-virtual {v1, v2}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@29
    move-result-object v0

    #@2a
    .line 286
    .local v0, app:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<Lcom/broadcom/bt/gatt/IBluetoothGattCallback;>.App;"
    if-eqz v0, :cond_40

    #@2c
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->uuid:Ljava/util/UUID;

    #@2e
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;->this$0:Lcom/broadcom/bt/service/gatt/GattService;

    #@30
    iget-object v2, v2, Lcom/broadcom/bt/service/gatt/GattService;->oneKeyuuid:Ljava/util/UUID;

    #@32
    invoke-virtual {v1, v2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    #@35
    move-result v1

    #@36
    if-nez v1, :cond_40

    #@38
    .line 287
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;->this$0:Lcom/broadcom/bt/service/gatt/GattService;

    #@3a
    iget-byte v2, p0, Lcom/broadcom/bt/service/gatt/GattService$ClientDeathRecipient;->mAppIf:B

    #@3c
    invoke-virtual {v1, v2}, Lcom/broadcom/bt/service/gatt/GattService;->unregisterClient(B)V

    #@3f
    .line 299
    :goto_3f
    return-void

    #@40
    .line 289
    :cond_40
    const-string v1, "BtGatt.GattService"

    #@42
    const-string v2, "Setting is dead. do not unregister"

    #@44
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    goto :goto_3f
.end method
