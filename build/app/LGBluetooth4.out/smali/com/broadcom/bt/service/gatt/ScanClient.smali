.class Lcom/broadcom/bt/service/gatt/ScanClient;
.super Ljava/lang/Object;
.source "ScanClient.java"


# instance fields
.field appIf:B

.field isServer:Z

.field uuids:[Ljava/util/UUID;


# direct methods
.method constructor <init>(BZ)V
    .registers 4
    .parameter "appIf"
    .parameter "isServer"

    #@0
    .prologue
    .line 60
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 61
    iput-byte p1, p0, Lcom/broadcom/bt/service/gatt/ScanClient;->appIf:B

    #@5
    .line 62
    iput-boolean p2, p0, Lcom/broadcom/bt/service/gatt/ScanClient;->isServer:Z

    #@7
    .line 63
    const/4 v0, 0x0

    #@8
    new-array v0, v0, [Ljava/util/UUID;

    #@a
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/ScanClient;->uuids:[Ljava/util/UUID;

    #@c
    .line 64
    return-void
.end method

.method constructor <init>(BZ[Ljava/util/UUID;)V
    .registers 4
    .parameter "appIf"
    .parameter "isServer"
    .parameter "uuids"

    #@0
    .prologue
    .line 66
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 67
    iput-byte p1, p0, Lcom/broadcom/bt/service/gatt/ScanClient;->appIf:B

    #@5
    .line 68
    iput-boolean p2, p0, Lcom/broadcom/bt/service/gatt/ScanClient;->isServer:Z

    #@7
    .line 69
    iput-object p3, p0, Lcom/broadcom/bt/service/gatt/ScanClient;->uuids:[Ljava/util/UUID;

    #@9
    .line 70
    return-void
.end method
