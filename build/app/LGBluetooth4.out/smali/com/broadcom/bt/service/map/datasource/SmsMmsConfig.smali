.class public Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;
.super Ljava/lang/Object;
.source "SmsMmsConfig.java"


# static fields
.field public static final ALLOW_UNDELETE_MESSAGES:Z = false

.field public static final CFG_SEND_MSG_SIZE_INFO_IN_LISTING:Z = true

.field public static final DEFAULT_MSG_MASK:I = 0x10ff

.field public static final DS_ID:Ljava/lang/String; = "SMS-MMS"

.field public static final DS_MMS_ID:Ljava/lang/String; = "MMS"

.field public static final DS_SMS_ID:Ljava/lang/String; = "SMS"

.field public static final ENABLE_CONVERSATION_DELETE:Z = true

.field public static final ENABLE_CONVERSATION_DELETE_ENHANCED:Z = true

.field static ENABLE_MMS:Z = false

.field static ENABLE_SMS:Z = false

.field public static final ENHANCED_CONVERSATION_DELETE_CONTENT_URI:Landroid/net/Uri; = null

.field public static final IGNORE_PRIORITY_FILTER_FLAG:Z = false

.field public static final MCE_SEND_DELETE_NOTIFICATION:Z = true

.field public static final OWNER_PHONE_TYPE:I = 0x2

.field public static final PROVIDER_ID:Ljava/lang/String; = "com.broadcom.bt.SMSMMS"

.field public static final SEND_OUTBOX_NEW_MESSAGE_NOTIFICATION:Z = false

.field public static final SMS_CFG_REQUEST_DELIVERY_REPORT:Z = true

.field public static final SMS_QUERY_RETRY_COUNT:I = 0xa

.field public static final SMS_QUERY_TIMEOUT_MS:I = 0x3e8

.field public static final USE_SIM_NUMBER_AS_DEFAULT:Z

.field private static sInited:Z


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 64
    sput-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@3
    .line 65
    sput-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@5
    .line 129
    sget-object v0, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const-string v1, "conversations/deleted"

    #@9
    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@c
    move-result-object v0

    #@d
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENHANCED_CONVERSATION_DELETE_CONTENT_URI:Landroid/net/Uri;

    #@f
    .line 146
    const/4 v0, 0x0

    #@10
    sput-boolean v0, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->sInited:Z

    #@12
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 63
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static getMmsHelper()Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;
    .registers 1

    #@0
    .prologue
    .line 168
    new-instance v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;

    #@2
    invoke-direct {v0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;-><init>()V

    #@5
    return-object v0
.end method

.method static getSmsHelper()Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;
    .registers 1

    #@0
    .prologue
    .line 164
    new-instance v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;

    #@2
    invoke-direct {v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;-><init>()V

    #@5
    return-object v0
.end method

.method static declared-synchronized init(Landroid/content/Context;)V
    .registers 6
    .parameter "ctx"

    #@0
    .prologue
    .line 149
    const-class v3, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;

    #@2
    monitor-enter v3

    #@3
    :try_start_3
    sget-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->sInited:Z
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_2d

    #@5
    if-nez v2, :cond_22

    #@7
    .line 151
    :try_start_7
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@a
    move-result-object v0

    #@b
    .line 152
    .local v0, r:Landroid/content/res/Resources;
    if-eqz v0, :cond_1f

    #@d
    .line 153
    const v2, 0x7f060020

    #@10
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@13
    move-result v2

    #@14
    sput-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_SMS:Z

    #@16
    .line 154
    const v2, 0x7f060021

    #@19
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@1c
    move-result v2

    #@1d
    sput-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENABLE_MMS:Z

    #@1f
    .line 156
    :cond_1f
    const/4 v2, 0x1

    #@20
    sput-boolean v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->sInited:Z
    :try_end_22
    .catchall {:try_start_7 .. :try_end_22} :catchall_2d
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_22} :catch_24

    #@22
    .line 161
    .end local v0           #r:Landroid/content/res/Resources;
    :cond_22
    :goto_22
    monitor-exit v3

    #@23
    return-void

    #@24
    .line 157
    :catch_24
    move-exception v1

    #@25
    .line 158
    .local v1, t:Ljava/lang/Throwable;
    :try_start_25
    const-string v2, "SmsMmsConfig"

    #@27
    const-string v4, "Error setting SMS/MMS state"

    #@29
    invoke-static {v2, v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2c
    .catchall {:try_start_25 .. :try_end_2c} :catchall_2d

    #@2c
    goto :goto_22

    #@2d
    .line 149
    .end local v1           #t:Ljava/lang/Throwable;
    :catchall_2d
    move-exception v2

    #@2e
    monitor-exit v3

    #@2f
    throw v2
.end method
