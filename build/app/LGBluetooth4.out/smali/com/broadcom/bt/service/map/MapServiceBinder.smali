.class public Lcom/broadcom/bt/service/map/MapServiceBinder;
.super Lcom/broadcom/bt/map/IBluetoothMap$Stub;
.source "MapServiceBinder.java"

# interfaces
.implements Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;


# static fields
.field public static final BLUETOOTH_ADMIN_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH_ADMIN"

.field public static final BLUETOOTH_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH"

.field private static final TAG:Ljava/lang/String; = "BtMap.MapServiceBinder"


# instance fields
.field private mService:Lcom/broadcom/bt/service/map/MapService;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/map/MapService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/broadcom/bt/map/IBluetoothMap$Stub;-><init>()V

    #@3
    .line 74
    iput-object p1, p0, Lcom/broadcom/bt/service/map/MapServiceBinder;->mService:Lcom/broadcom/bt/service/map/MapService;

    #@5
    .line 75
    return-void
.end method

.method private getService()Lcom/broadcom/bt/service/map/MapService;
    .registers 2

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapServiceBinder;->mService:Lcom/broadcom/bt/service/map/MapService;

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapServiceBinder;->mService:Lcom/broadcom/bt/service/map/MapService;

    #@6
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/MapService;->isAvailable()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_f

    #@c
    .line 79
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapServiceBinder;->mService:Lcom/broadcom/bt/service/map/MapService;

    #@e
    .line 81
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method


# virtual methods
.method public cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 85
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapServiceBinder;->mService:Lcom/broadcom/bt/service/map/MapService;

    #@3
    .line 86
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public registerCallback(Lcom/broadcom/bt/map/IBluetoothMapCallback;)Z
    .registers 3
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 122
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public registerDatasource(Ljava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;)Z
    .registers 20
    .parameter "providerId"
    .parameter "providerDisplayName"
    .parameter "dsType"
    .parameter "dsId"
    .parameter "dsDisplayName"
    .parameter "supportsMessageFilter"
    .parameter "supportsMessageOffsetBrowsing"
    .parameter "virtualFolderMappings"
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 96
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v0

    #@4
    .line 97
    .local v0, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v0, :cond_f

    #@6
    .line 98
    const-string v1, "BtMap.MapServiceBinder"

    #@8
    const-string v2, "registerDatasource(): MapService not available."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 99
    const/4 v1, 0x0

    #@e
    .line 101
    :goto_e
    return v1

    #@f
    :cond_f
    move-object v1, p1

    #@10
    move-object v2, p2

    #@11
    move v3, p3

    #@12
    move-object v4, p4

    #@13
    move-object v5, p5

    #@14
    move/from16 v6, p6

    #@16
    move/from16 v7, p7

    #@18
    move-object/from16 v8, p8

    #@1a
    move-object/from16 v9, p9

    #@1c
    invoke-virtual/range {v0 .. v9}, Lcom/broadcom/bt/service/map/MapService;->registerDatasource(Ljava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;)Z

    #@1f
    move-result v1

    #@20
    goto :goto_e
.end method

.method public returnMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "requestId"
    .parameter "messageHandle"
    .parameter "bMsgFilePath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 185
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v0

    #@4
    .line 186
    .local v0, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v0, :cond_e

    #@6
    .line 187
    const-string v1, "BtMap.MapServiceBinder"

    #@8
    const-string v2, "returnMessage(): MapService not available."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 192
    :goto_d
    return-void

    #@e
    .line 190
    :cond_e
    iget-object v1, v0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@10
    invoke-virtual {v1, p1, p2, p3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->returnMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V

    #@13
    goto :goto_d
.end method

.method public sendNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V
    .registers 17
    .parameter "providerId"
    .parameter "datasourceId"
    .parameter "messageId"
    .parameter "msgType"
    .parameter "notificationType"
    .parameter "dsFolderPath"
    .parameter "dsOldFolderPath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 224
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v8

    #@4
    .line 225
    .local v8, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v8, :cond_e

    #@6
    .line 226
    const-string v0, "BtMap.MapServiceBinder"

    #@8
    const-string v1, "sendNotification(): MapService not available."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 232
    :goto_d
    return-void

    #@e
    .line 229
    :cond_e
    iget-object v0, v8, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@10
    move-object v1, p1

    #@11
    move-object v2, p2

    #@12
    move-object v3, p3

    #@13
    move v4, p4

    #@14
    move v5, p5

    #@15
    move-object v6, p6

    #@16
    move-object/from16 v7, p7

    #@18
    invoke-virtual/range {v0 .. v7}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V

    #@1b
    goto :goto_d
.end method

.method public setDatasourceState(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 7
    .parameter "providerId"
    .parameter "dsId"
    .parameter "enable"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 135
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v0

    #@4
    .line 136
    .local v0, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v0, :cond_e

    #@6
    .line 137
    const-string v1, "BtMap.MapServiceBinder"

    #@8
    const-string v2, "setDatasourceState(): MapService not available."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 143
    :goto_d
    return-void

    #@e
    .line 140
    :cond_e
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@10
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@12
    invoke-virtual {v0, v1, v2}, Lcom/broadcom/bt/service/map/MapService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@15
    .line 142
    iget-object v1, v0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@17
    invoke-virtual {v1, p1, p2, p3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->setDatasourceState(Ljava/lang/String;Ljava/lang/String;Z)V

    #@1a
    goto :goto_d
.end method

.method public setFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;)V
    .registers 7
    .parameter "requestId"
    .parameter "folderPath"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/broadcom/bt/map/RequestId;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/FolderInfo;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 148
    .local p3, folderEntries:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;"
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v0

    #@4
    .line 149
    .local v0, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v0, :cond_e

    #@6
    .line 150
    const-string v1, "BtMap.MapServiceBinder"

    #@8
    const-string v2, "setFolderListing(): MapService not available."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 155
    :goto_d
    return-void

    #@e
    .line 153
    :cond_e
    iget-object v1, v0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@10
    invoke-virtual {v1, p1, p2, p3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->setFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;)V

    #@13
    goto :goto_d
.end method

.method public setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V
    .registers 13
    .parameter "requestId"
    .parameter "messageId"
    .parameter "deleteRequested"
    .parameter "success"
    .parameter "newFolderPath"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 210
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v6

    #@4
    .line 211
    .local v6, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v6, :cond_e

    #@6
    .line 212
    const-string v0, "BtMap.MapServiceBinder"

    #@8
    const-string v1, "setMessageDeletedResult(): MapService not available."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 218
    :goto_d
    return-void

    #@e
    .line 215
    :cond_e
    iget-object v0, v6, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@10
    move-object v1, p1

    #@11
    move-object v2, p2

    #@12
    move v3, p3

    #@13
    move v4, p4

    #@14
    move-object v5, p5

    #@15
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V

    #@18
    goto :goto_d
.end method

.method public setMessageListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .registers 8
    .parameter "requestId"
    .parameter "folderPath"
    .parameter
    .parameter "dateTime"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/broadcom/bt/map/RequestId;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 160
    .local p3, messageInfoList:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;"
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v0

    #@4
    .line 161
    .local v0, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v0, :cond_e

    #@6
    .line 162
    const-string v1, "BtMap.MapServiceBinder"

    #@8
    const-string v2, "setMessageListing(): MapService not available."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 167
    :goto_d
    return-void

    #@e
    .line 165
    :cond_e
    iget-object v1, v0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@10
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->setMessageListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    #@13
    goto :goto_d
.end method

.method public setMessageListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ILjava/lang/String;Z)V
    .registers 13
    .parameter "requestId"
    .parameter "folderPath"
    .parameter "messageCount"
    .parameter "dateTime"
    .parameter "hasNewMessages"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 173
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v6

    #@4
    .line 174
    .local v6, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v6, :cond_e

    #@6
    .line 175
    const-string v0, "BtMap.MapServiceBinder"

    #@8
    const-string v1, "setMessageListingCount(): MapService not available."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 180
    :goto_d
    return-void

    #@e
    .line 178
    :cond_e
    iget-object v0, v6, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@10
    move-object v1, p1

    #@11
    move-object v2, p2

    #@12
    move v3, p3

    #@13
    move-object v4, p4

    #@14
    move v5, p5

    #@15
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->setMessageListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ILjava/lang/String;Z)V

    #@18
    goto :goto_d
.end method

.method public setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "requestId"
    .parameter "folderPath"
    .parameter "messageId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 197
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v0

    #@4
    .line 198
    .local v0, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v0, :cond_e

    #@6
    .line 199
    const-string v1, "BtMap.MapServiceBinder"

    #@8
    const-string v2, "setPushMessageResult(): MapService not available."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 204
    :goto_d
    return-void

    #@e
    .line 202
    :cond_e
    iget-object v1, v0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@10
    invoke-virtual {v1, p1, p2, p3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V

    #@13
    goto :goto_d
.end method

.method public unregisterCallback(Lcom/broadcom/bt/map/IBluetoothMapCallback;)Z
    .registers 3
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 129
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public unregisterDatasource(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 7
    .parameter "providerId"
    .parameter "dsId"
    .parameter "removePermanently"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 109
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v0

    #@4
    .line 110
    .local v0, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v0, :cond_f

    #@6
    .line 111
    const-string v1, "BtMap.MapServiceBinder"

    #@8
    const-string v2, "registerDatasource(): MapService not available."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 112
    const/4 v1, 0x0

    #@e
    .line 114
    :goto_e
    return v1

    #@f
    :cond_f
    invoke-virtual {v0, p1, p2, p3}, Lcom/broadcom/bt/service/map/MapService;->unregisterDatasource(Ljava/lang/String;Ljava/lang/String;Z)Z

    #@12
    move-result v1

    #@13
    goto :goto_e
.end method

.method public updateMessageId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "providerId"
    .parameter "datasourceId"
    .parameter "oldMessageId"
    .parameter "newMessageId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 238
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;->getService()Lcom/broadcom/bt/service/map/MapService;

    #@3
    move-result-object v0

    #@4
    .line 239
    .local v0, svc:Lcom/broadcom/bt/service/map/MapService;
    if-nez v0, :cond_e

    #@6
    .line 240
    const-string v1, "BtMap.MapServiceBinder"

    #@8
    const-string v2, "sendNotification(): MapService not available."

    #@a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 245
    :goto_d
    return-void

    #@e
    .line 243
    :cond_e
    iget-object v1, v0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@10
    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->updateMessageHandle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@13
    goto :goto_d
.end method
