.class Lcom/broadcom/bt/service/gatt/ContextMap;
.super Ljava/lang/Object;
.source "ContextMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/gatt/ContextMap$App;,
        Lcom/broadcom/bt/service/gatt/ContextMap$Connection;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "BtGatt.ContextMap"


# instance fields
.field mApps:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/service/gatt/ContextMap",
            "<TT;>.App;>;"
        }
    .end annotation
.end field

.field mConnections:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/broadcom/bt/service/gatt/ContextMap",
            "<TT;>.Connection;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 67
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 138
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mApps:Ljava/util/List;

    #@a
    .line 141
    new-instance v0, Ljava/util/HashSet;

    #@c
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@f
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mConnections:Ljava/util/Set;

    #@11
    return-void
.end method


# virtual methods
.method add(Ljava/util/UUID;Ljava/lang/Object;)V
    .registers 5
    .parameter "uuid"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            "TT;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 147
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    .local p2, callback:Ljava/lang/Object;,"TT;"
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mApps:Ljava/util/List;

    #@2
    new-instance v1, Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@4
    invoke-direct {v1, p0, p1, p2}, Lcom/broadcom/bt/service/gatt/ContextMap$App;-><init>(Lcom/broadcom/bt/service/gatt/ContextMap;Ljava/util/UUID;Ljava/lang/Object;)V

    #@7
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@a
    .line 148
    return-void
.end method

.method addConnection(BILjava/lang/String;)V
    .registers 7
    .parameter "id"
    .parameter "connId"
    .parameter "address"

    #@0
    .prologue
    .line 169
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/gatt/ContextMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@3
    move-result-object v0

    #@4
    .line 170
    .local v0, entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    if-eqz v0, :cond_10

    #@6
    .line 171
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mConnections:Ljava/util/Set;

    #@8
    new-instance v2, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;

    #@a
    invoke-direct {v2, p0, p2, p3, p1}, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;-><init>(Lcom/broadcom/bt/service/gatt/ContextMap;ILjava/lang/String;B)V

    #@d
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@10
    .line 173
    :cond_10
    return-void
.end method

.method addressByConnId(I)Ljava/lang/String;
    .registers 5
    .parameter "connId"

    #@0
    .prologue
    .line 269
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mConnections:Ljava/util/Set;

    #@2
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .line 270
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_19

    #@c
    .line 271
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;

    #@12
    .line 272
    .local v0, connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    iget v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->connId:I

    #@14
    if-ne v2, p1, :cond_6

    #@16
    .line 273
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->address:Ljava/lang/String;

    #@18
    .line 276
    .end local v0           #connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    :goto_18
    return-object v2

    #@19
    :cond_19
    const/4 v2, 0x0

    #@1a
    goto :goto_18
.end method

.method clear()V
    .registers 5

    #@0
    .prologue
    .line 295
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    iget-object v3, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mApps:Ljava/util/List;

    #@2
    monitor-enter v3

    #@3
    .line 296
    :try_start_3
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mApps:Ljava/util/List;

    #@5
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@8
    move-result-object v1

    #@9
    .line 297
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;>;"
    :goto_9
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_1f

    #@f
    .line 298
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@15
    .line 299
    .local v0, entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    invoke-virtual {v0}, Lcom/broadcom/bt/service/gatt/ContextMap$App;->unlinkToDeath()V

    #@18
    .line 300
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@1b
    goto :goto_9

    #@1c
    .line 302
    .end local v0           #entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    .end local v1           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;>;"
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    #@1e
    throw v2

    #@1f
    .restart local v1       #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;>;"
    :cond_1f
    :try_start_1f
    monitor-exit v3
    :try_end_20
    .catchall {:try_start_1f .. :try_end_20} :catchall_1c

    #@20
    .line 303
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mConnections:Ljava/util/Set;

    #@22
    invoke-interface {v2}, Ljava/util/Set;->clear()V

    #@25
    .line 304
    return-void
.end method

.method connIdByAddress(BLjava/lang/String;)Ljava/lang/Integer;
    .registers 8
    .parameter "id"
    .parameter "address"

    #@0
    .prologue
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    const/4 v3, 0x0

    #@1
    .line 250
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/gatt/ContextMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@4
    move-result-object v1

    #@5
    .line 251
    .local v1, entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    if-nez v1, :cond_8

    #@7
    .line 262
    :cond_7
    :goto_7
    return-object v3

    #@8
    .line 255
    :cond_8
    iget-object v4, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mConnections:Ljava/util/Set;

    #@a
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v2

    #@e
    .line 256
    .local v2, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    :cond_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v4

    #@12
    if-eqz v4, :cond_7

    #@14
    .line 257
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;

    #@1a
    .line 258
    .local v0, connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    iget-object v4, v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->address:Ljava/lang/String;

    #@1c
    invoke-virtual {v4, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_e

    #@22
    iget-byte v4, v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->appId:B

    #@24
    if-ne v4, p1, :cond_e

    #@26
    .line 259
    iget v3, v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->connId:I

    #@28
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b
    move-result-object v3

    #@2c
    goto :goto_7
.end method

.method dump()V
    .registers 9

    #@0
    .prologue
    .line 310
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 311
    .local v0, b:Ljava/lang/StringBuilder;
    const-string v6, "-------------- GATT Context Map ----------------"

    #@7
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 312
    new-instance v6, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v7, "\nEntries: "

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    iget-object v7, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mApps:Ljava/util/List;

    #@17
    invoke-interface {v7}, Ljava/util/List;->size()I

    #@1a
    move-result v7

    #@1b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 314
    iget-object v6, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mApps:Ljava/util/List;

    #@28
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@2b
    move-result-object v4

    #@2c
    .line 315
    .local v4, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;>;"
    :cond_2c
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@2f
    move-result v6

    #@30
    if-eqz v6, :cond_bd

    #@32
    .line 316
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@35
    move-result-object v3

    #@36
    check-cast v3, Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@38
    .line 317
    .local v3, entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    iget-byte v6, v3, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@3a
    invoke-virtual {p0, v6}, Lcom/broadcom/bt/service/gatt/ContextMap;->getConnectionByApp(I)Ljava/util/List;

    #@3d
    move-result-object v2

    #@3e
    .line 319
    .local v2, connections:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    new-instance v6, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v7, "\n\nApplication Id: "

    #@45
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v6

    #@49
    iget-byte v7, v3, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    .line 320
    new-instance v6, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v7, "\nUUID: "

    #@5d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v6

    #@61
    iget-object v7, v3, Lcom/broadcom/bt/service/gatt/ContextMap$App;->uuid:Ljava/util/UUID;

    #@63
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v6

    #@67
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v6

    #@6b
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    .line 321
    new-instance v6, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v7, "\nConnections: "

    #@75
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v6

    #@79
    invoke-interface {v2}, Ljava/util/List;->size()I

    #@7c
    move-result v7

    #@7d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v6

    #@81
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v6

    #@85
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    .line 323
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@8b
    move-result-object v5

    #@8c
    .line 324
    .local v5, ii:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    :goto_8c
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    #@8f
    move-result v6

    #@90
    if-eqz v6, :cond_2c

    #@92
    .line 325
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@95
    move-result-object v1

    #@96
    check-cast v1, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;

    #@98
    .line 326
    .local v1, connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    new-instance v6, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v7, "\n  "

    #@9f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v6

    #@a3
    iget v7, v1, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->connId:I

    #@a5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v6

    #@a9
    const-string v7, ": "

    #@ab
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v6

    #@af
    iget-object v7, v1, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->address:Ljava/lang/String;

    #@b1
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v6

    #@b5
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b8
    move-result-object v6

    #@b9
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    goto :goto_8c

    #@bd
    .line 330
    .end local v1           #connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    .end local v2           #connections:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    .end local v3           #entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    .end local v5           #ii:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    :cond_bd
    const-string v6, "\n------------------------------------------------"

    #@bf
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    .line 331
    const-string v6, "BtGatt.ContextMap"

    #@c4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c7
    move-result-object v7

    #@c8
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cb
    .line 332
    return-void
.end method

.method getByConnId(I)Lcom/broadcom/bt/service/gatt/ContextMap$App;
    .registers 5
    .parameter "connId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/broadcom/bt/service/gatt/ContextMap",
            "<TT;>.App;"
        }
    .end annotation

    #@0
    .prologue
    .line 236
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mConnections:Ljava/util/Set;

    #@2
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .line 237
    .local v1, ii:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1d

    #@c
    .line 238
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;

    #@12
    .line 239
    .local v0, connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    iget v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->connId:I

    #@14
    if-ne v2, p1, :cond_6

    #@16
    .line 240
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->appId:B

    #@18
    invoke-virtual {p0, v2}, Lcom/broadcom/bt/service/gatt/ContextMap;->getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@1b
    move-result-object v2

    #@1c
    .line 243
    .end local v0           #connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    :goto_1c
    return-object v2

    #@1d
    :cond_1d
    const/4 v2, 0x0

    #@1e
    goto :goto_1c
.end method

.method getById(B)Lcom/broadcom/bt/service/gatt/ContextMap$App;
    .registers 7
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)",
            "Lcom/broadcom/bt/service/gatt/ContextMap",
            "<TT;>.App;"
        }
    .end annotation

    #@0
    .prologue
    .line 193
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mApps:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .line 194
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;>;"
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_17

    #@c
    .line 195
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@12
    .line 196
    .local v0, entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@14
    if-ne v2, p1, :cond_6

    #@16
    .line 201
    .end local v0           #entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    :goto_16
    return-object v0

    #@17
    .line 200
    :cond_17
    const-string v2, "BtGatt.ContextMap"

    #@19
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v4, "Context not found for ID "

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v3

    #@2c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 201
    const/4 v0, 0x0

    #@30
    goto :goto_16
.end method

.method getByUuid(Ljava/util/UUID;)Lcom/broadcom/bt/service/gatt/ContextMap$App;
    .registers 7
    .parameter "uuid"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/UUID;",
            ")",
            "Lcom/broadcom/bt/service/gatt/ContextMap",
            "<TT;>.App;"
        }
    .end annotation

    #@0
    .prologue
    .line 208
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mApps:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .line 209
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;>;"
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1b

    #@c
    .line 210
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@12
    .line 211
    .local v0, entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->uuid:Ljava/util/UUID;

    #@14
    invoke-virtual {v2, p1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    #@17
    move-result v2

    #@18
    if-eqz v2, :cond_6

    #@1a
    .line 216
    .end local v0           #entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    :goto_1a
    return-object v0

    #@1b
    .line 215
    :cond_1b
    const-string v2, "BtGatt.ContextMap"

    #@1d
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v4, "Context not found for UUID "

    #@24
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v3

    #@30
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 216
    const/4 v0, 0x0

    #@34
    goto :goto_1a
.end method

.method getConnectedDevices()Ljava/util/Set;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 223
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    new-instance v0, Ljava/util/HashSet;

    #@2
    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    #@5
    .line 224
    .local v0, addresses:Ljava/util/Set;,"Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mConnections:Ljava/util/Set;

    #@7
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v2

    #@b
    .line 225
    .local v2, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1d

    #@11
    .line 226
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;

    #@17
    .line 227
    .local v1, connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    iget-object v3, v1, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->address:Ljava/lang/String;

    #@19
    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    #@1c
    goto :goto_b

    #@1d
    .line 229
    .end local v1           #connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    :cond_1d
    return-object v0
.end method

.method getConnectionByApp(I)Ljava/util/List;
    .registers 6
    .parameter "appId"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/service/gatt/ContextMap",
            "<TT;>.Connection;>;"
        }
    .end annotation

    #@0
    .prologue
    .line 280
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    new-instance v1, Ljava/util/ArrayList;

    #@2
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 281
    .local v1, currentConnections:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    iget-object v3, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mConnections:Ljava/util/Set;

    #@7
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v2

    #@b
    .line 282
    .local v2, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    :cond_b
    :goto_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_1f

    #@11
    .line 283
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;

    #@17
    .line 284
    .local v0, connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    iget-byte v3, v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->appId:B

    #@19
    if-ne v3, p1, :cond_b

    #@1b
    .line 285
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1e
    goto :goto_b

    #@1f
    .line 288
    .end local v0           #connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    :cond_1f
    return-object v1
.end method

.method remove(B)V
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 154
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mApps:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .line 155
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;>;"
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1c

    #@c
    .line 156
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;

    #@12
    .line 157
    .local v0, entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$App;->id:B

    #@14
    if-ne v2, p1, :cond_6

    #@16
    .line 158
    invoke-virtual {v0}, Lcom/broadcom/bt/service/gatt/ContextMap$App;->unlinkToDeath()V

    #@19
    .line 159
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@1c
    .line 163
    .end local v0           #entry:Lcom/broadcom/bt/service/gatt/ContextMap$App;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.App;"
    :cond_1c
    return-void
.end method

.method removeConnection(BI)V
    .registers 6
    .parameter "id"
    .parameter "connId"

    #@0
    .prologue
    .line 179
    .local p0, this:Lcom/broadcom/bt/service/gatt/ContextMap;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>;"
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/ContextMap;->mConnections:Ljava/util/Set;

    #@2
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .line 180
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;>;"
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_19

    #@c
    .line 181
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;

    #@12
    .line 182
    .local v0, connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    iget v2, v0, Lcom/broadcom/bt/service/gatt/ContextMap$Connection;->connId:I

    #@14
    if-ne v2, p2, :cond_6

    #@16
    .line 183
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@19
    .line 187
    .end local v0           #connection:Lcom/broadcom/bt/service/gatt/ContextMap$Connection;,"Lcom/broadcom/bt/service/gatt/ContextMap<TT;>.Connection;"
    :cond_19
    return-void
.end method
