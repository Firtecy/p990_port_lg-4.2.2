.class Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings$SwitchComparator;
.super Ljava/lang/Object;
.source "MapAdvancedSettings.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SwitchComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Landroid/preference/SwitchPreference;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;


# direct methods
.method private constructor <init>(Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 121
    iput-object p1, p0, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings$SwitchComparator;->this$0:Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings$SwitchComparator;-><init>(Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;)V

    #@3
    return-void
.end method


# virtual methods
.method public compare(Landroid/preference/SwitchPreference;Landroid/preference/SwitchPreference;)I
    .registers 6
    .parameter "lhs"
    .parameter "rhs"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 125
    if-nez p1, :cond_c

    #@3
    move-object v0, v1

    #@4
    .line 126
    .local v0, lhskey:Ljava/lang/String;
    :goto_4
    if-nez p2, :cond_11

    #@6
    .line 128
    .local v1, rhskey:Ljava/lang/String;
    :goto_6
    if-nez v0, :cond_16

    #@8
    if-nez v1, :cond_16

    #@a
    .line 129
    const/4 v2, 0x0

    #@b
    .line 134
    :goto_b
    return v2

    #@c
    .line 125
    .end local v0           #lhskey:Ljava/lang/String;
    .end local v1           #rhskey:Ljava/lang/String;
    :cond_c
    invoke-virtual {p1}, Landroid/preference/SwitchPreference;->getKey()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    goto :goto_4

    #@11
    .line 126
    .restart local v0       #lhskey:Ljava/lang/String;
    :cond_11
    invoke-virtual {p2}, Landroid/preference/SwitchPreference;->getKey()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    goto :goto_6

    #@16
    .line 130
    .restart local v1       #rhskey:Ljava/lang/String;
    :cond_16
    if-eqz v0, :cond_1a

    #@18
    if-nez v1, :cond_1c

    #@1a
    .line 131
    :cond_1a
    const/4 v2, 0x1

    #@1b
    goto :goto_b

    #@1c
    .line 134
    :cond_1c
    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1f
    move-result v2

    #@20
    goto :goto_b
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 121
    check-cast p1, Landroid/preference/SwitchPreference;

    #@2
    .end local p1
    check-cast p2, Landroid/preference/SwitchPreference;

    #@4
    .end local p2
    invoke-virtual {p0, p1, p2}, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings$SwitchComparator;->compare(Landroid/preference/SwitchPreference;Landroid/preference/SwitchPreference;)I

    #@7
    move-result v0

    #@8
    return v0
.end method
