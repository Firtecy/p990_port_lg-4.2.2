.class Lcom/broadcom/bt/service/gatt/HandleMap;
.super Ljava/lang/Object;
.source "HandleMap.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "BtGatt.HandleMap"

.field public static final TYPE_CHARACTERISTIC:B = 0x2t

.field public static final TYPE_DESCRIPTOR:B = 0x3t

.field public static final TYPE_SERVICE:B = 0x1t

.field public static final TYPE_UNDEFINED:B


# instance fields
.field mEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/service/gatt/HandleMap$Entry;",
            ">;"
        }
    .end annotation
.end field

.field mLastCharacteristic:I

.field mRequestMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 118
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 114
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@6
    .line 115
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mRequestMap:Ljava/util/Map;

    #@8
    .line 116
    const/4 v0, 0x0

    #@9
    iput v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mLastCharacteristic:I

    #@b
    .line 119
    new-instance v0, Ljava/util/ArrayList;

    #@d
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@10
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@12
    .line 120
    new-instance v0, Ljava/util/HashMap;

    #@14
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@17
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mRequestMap:Ljava/util/Map;

    #@19
    .line 121
    return-void
.end method


# virtual methods
.method addCharacteristic(BILjava/util/UUID;I)V
    .registers 13
    .parameter "serverIf"
    .parameter "handle"
    .parameter "uuid"
    .parameter "serviceHandle"

    #@0
    .prologue
    .line 133
    iput p2, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mLastCharacteristic:I

    #@2
    .line 134
    iget-object v7, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@4
    new-instance v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@6
    const/4 v3, 0x2

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move v4, p2

    #@a
    move-object v5, p3

    #@b
    move v6, p4

    #@c
    invoke-direct/range {v0 .. v6}, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;-><init>(Lcom/broadcom/bt/service/gatt/HandleMap;BBILjava/util/UUID;I)V

    #@f
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@12
    .line 135
    return-void
.end method

.method addDescriptor(BILjava/util/UUID;I)V
    .registers 14
    .parameter "serverIf"
    .parameter "handle"
    .parameter "uuid"
    .parameter "serviceHandle"

    #@0
    .prologue
    .line 138
    iget-object v8, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@2
    new-instance v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@4
    const/4 v3, 0x3

    #@5
    iget v7, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mLastCharacteristic:I

    #@7
    move-object v1, p0

    #@8
    move v2, p1

    #@9
    move v4, p2

    #@a
    move-object v5, p3

    #@b
    move v6, p4

    #@c
    invoke-direct/range {v0 .. v7}, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;-><init>(Lcom/broadcom/bt/service/gatt/HandleMap;BBILjava/util/UUID;II)V

    #@f
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@12
    .line 139
    return-void
.end method

.method addRequest(II)V
    .registers 6
    .parameter "requestId"
    .parameter "handle"

    #@0
    .prologue
    .line 210
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mRequestMap:Ljava/util/Map;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9
    move-result-object v2

    #@a
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    .line 211
    return-void
.end method

.method addService(BILjava/util/UUID;II)V
    .registers 14
    .parameter "serverIf"
    .parameter "handle"
    .parameter "uuid"
    .parameter "serviceType"
    .parameter "instance"

    #@0
    .prologue
    .line 129
    iget-object v7, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@2
    new-instance v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@4
    move-object v1, p0

    #@5
    move v2, p1

    #@6
    move v3, p2

    #@7
    move-object v4, p3

    #@8
    move v5, p4

    #@9
    move v6, p5

    #@a
    invoke-direct/range {v0 .. v6}, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;-><init>(Lcom/broadcom/bt/service/gatt/HandleMap;BILjava/util/UUID;II)V

    #@d
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@10
    .line 130
    return-void
.end method

.method clear()V
    .registers 2

    #@0
    .prologue
    .line 124
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@2
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@5
    .line 125
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mRequestMap:Ljava/util/Map;

    #@7
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    #@a
    .line 126
    return-void
.end method

.method deleteRequest(I)V
    .registers 4
    .parameter "requestId"

    #@0
    .prologue
    .line 214
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mRequestMap:Ljava/util/Map;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    .line 215
    return-void
.end method

.method deleteService(BI)V
    .registers 6
    .parameter "serverIf"
    .parameter "serviceHandle"

    #@0
    .prologue
    .line 192
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/HandleMap$Entry;>;"
    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_22

    #@c
    .line 193
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@12
    .line 194
    .local v0, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@14
    if-ne v2, p1, :cond_6

    #@16
    .line 198
    iget v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@18
    if-eq v2, p2, :cond_1e

    #@1a
    iget v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@1c
    if-ne v2, p2, :cond_6

    #@1e
    .line 200
    :cond_1e
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@21
    goto :goto_6

    #@22
    .line 203
    .end local v0           #entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :cond_22
    return-void
.end method

.method dump()V
    .registers 6

    #@0
    .prologue
    .line 231
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 232
    .local v0, b:Ljava/lang/StringBuilder;
    const-string v3, "-------------- GATT Handle Map -----------------"

    #@7
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 233
    new-instance v3, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v4, "\nEntries: "

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    iget-object v4, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@17
    invoke-interface {v4}, Ljava/util/List;->size()I

    #@1a
    move-result v4

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    .line 234
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v4, "\nRequests: "

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    iget-object v4, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mRequestMap:Ljava/util/Map;

    #@33
    invoke-interface {v4}, Ljava/util/Map;->size()I

    #@36
    move-result v4

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    .line 236
    iget-object v3, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@44
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@47
    move-result-object v2

    #@48
    .local v2, i$:Ljava/util/Iterator;
    :goto_48
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@4b
    move-result v3

    #@4c
    if-eqz v3, :cond_e9

    #@4e
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@51
    move-result-object v1

    #@52
    check-cast v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@54
    .line 237
    .local v1, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    new-instance v3, Ljava/lang/StringBuilder;

    #@56
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@59
    const-string v4, "\n"

    #@5b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v3

    #@5f
    iget-byte v4, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@61
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v3

    #@65
    const-string v4, ": ["

    #@67
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v3

    #@6b
    iget v4, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@6d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    const-string v4, "] "

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7a
    move-result-object v3

    #@7b
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    .line 238
    iget-byte v3, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@80
    packed-switch v3, :pswitch_data_f8

    #@83
    goto :goto_48

    #@84
    .line 240
    :pswitch_84
    new-instance v3, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v4, "Service "

    #@8b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v3

    #@8f
    iget-object v4, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@91
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v3

    #@95
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v3

    #@99
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    .line 241
    new-instance v3, Ljava/lang/StringBuilder;

    #@9e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a1
    const-string v4, ", started "

    #@a3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v3

    #@a7
    iget-boolean v4, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->started:Z

    #@a9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v3

    #@ad
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v3

    #@b1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    goto :goto_48

    #@b5
    .line 245
    :pswitch_b5
    new-instance v3, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v4, "  Characteristic "

    #@bc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v3

    #@c0
    iget-object v4, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@c2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v3

    #@c6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c9
    move-result-object v3

    #@ca
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    goto/16 :goto_48

    #@cf
    .line 249
    :pswitch_cf
    new-instance v3, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v4, "    Descriptor "

    #@d6
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v3

    #@da
    iget-object v4, v1, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@dc
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@df
    move-result-object v3

    #@e0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v3

    #@e4
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    goto/16 :goto_48

    #@e9
    .line 256
    .end local v1           #entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :cond_e9
    const-string v3, "\n------------------------------------------------"

    #@eb
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    .line 257
    const-string v3, "BtGatt.HandleMap"

    #@f0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f3
    move-result-object v4

    #@f4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f7
    .line 258
    return-void

    #@f8
    .line 238
    :pswitch_data_f8
    .packed-switch 0x1
        :pswitch_84
        :pswitch_b5
        :pswitch_cf
    .end packed-switch
.end method

.method getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    .registers 7
    .parameter "handle"

    #@0
    .prologue
    .line 155
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_17

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@12
    .line 156
    .local v0, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    iget v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@14
    if-ne v2, p1, :cond_6

    #@16
    .line 161
    .end local v0           #entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :goto_16
    return-object v0

    #@17
    .line 160
    :cond_17
    const-string v2, "BtGatt.HandleMap"

    #@19
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v4, "getByHandle() - Handle "

    #@20
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    const-string v4, " not found!"

    #@2a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 161
    const/4 v0, 0x0

    #@36
    goto :goto_16
.end method

.method getByRequestId(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    .registers 6
    .parameter "requestId"

    #@0
    .prologue
    .line 218
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mRequestMap:Ljava/util/Map;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Ljava/lang/Integer;

    #@c
    .line 219
    .local v0, handle:Ljava/lang/Integer;
    if-nez v0, :cond_2e

    #@e
    .line 220
    const-string v1, "BtGatt.HandleMap"

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "getByRequestId() - Request ID "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " not found!"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 221
    const/4 v1, 0x0

    #@2d
    .line 223
    :goto_2d
    return-object v1

    #@2e
    :cond_2e
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@31
    move-result v1

    #@32
    invoke-virtual {p0, v1}, Lcom/broadcom/bt/service/gatt/HandleMap;->getByHandle(I)Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@35
    move-result-object v1

    #@36
    goto :goto_2d
.end method

.method getCharacteristicHandle(ILjava/util/UUID;I)I
    .registers 9
    .parameter "serviceHandle"
    .parameter "uuid"
    .parameter "instance"

    #@0
    .prologue
    .line 178
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_2a

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@12
    .line 179
    .local v0, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@14
    const/4 v3, 0x2

    #@15
    if-ne v2, v3, :cond_6

    #@17
    iget v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceHandle:I

    #@19
    if-ne v2, p1, :cond_6

    #@1b
    iget v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@1d
    if-ne v2, p3, :cond_6

    #@1f
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@21
    invoke-virtual {v2, p2}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_6

    #@27
    .line 183
    iget v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@29
    .line 188
    .end local v0           #entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :goto_29
    return v2

    #@2a
    .line 186
    :cond_2a
    const-string v2, "BtGatt.HandleMap"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "getCharacteristicHandle() - Service "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    const-string v4, ", UUID "

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    const-string v4, " not found!"

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 188
    const/4 v2, 0x0

    #@53
    goto :goto_29
.end method

.method getEntries()Ljava/util/List;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/service/gatt/HandleMap$Entry;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 206
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@2
    return-object v0
.end method

.method getServiceHandle(Ljava/util/UUID;II)I
    .registers 9
    .parameter "uuid"
    .parameter "serviceType"
    .parameter "instance"

    #@0
    .prologue
    .line 165
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_2a

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@12
    .line 166
    .local v0, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@14
    const/4 v3, 0x1

    #@15
    if-ne v2, v3, :cond_6

    #@17
    iget v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serviceType:I

    #@19
    if-ne v2, p2, :cond_6

    #@1b
    iget v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->instance:I

    #@1d
    if-ne v2, p3, :cond_6

    #@1f
    iget-object v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->uuid:Ljava/util/UUID;

    #@21
    invoke-virtual {v2, p1}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_6

    #@27
    .line 170
    iget v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@29
    .line 174
    .end local v0           #entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :goto_29
    return v2

    #@2a
    .line 173
    :cond_2a
    const-string v2, "BtGatt.HandleMap"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v4, "getServiceHandle() - UUID "

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    const-string v4, " not found!"

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 174
    const/4 v2, 0x0

    #@49
    goto :goto_29
.end method

.method setStarted(BIZ)V
    .registers 8
    .parameter "serverIf"
    .parameter "handle"
    .parameter "started"

    #@0
    .prologue
    .line 142
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/HandleMap;->mEntries:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_21

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;

    #@12
    .line 143
    .local v0, entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->type:B

    #@14
    const/4 v3, 0x1

    #@15
    if-ne v2, v3, :cond_6

    #@17
    iget-byte v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->serverIf:B

    #@19
    if-ne v2, p1, :cond_6

    #@1b
    iget v2, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->handle:I

    #@1d
    if-ne v2, p2, :cond_6

    #@1f
    .line 149
    iput-boolean p3, v0, Lcom/broadcom/bt/service/gatt/HandleMap$Entry;->started:Z

    #@21
    .line 152
    .end local v0           #entry:Lcom/broadcom/bt/service/gatt/HandleMap$Entry;
    :cond_21
    return-void
.end method
