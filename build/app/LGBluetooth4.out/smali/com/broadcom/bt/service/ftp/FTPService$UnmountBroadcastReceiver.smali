.class Lcom/broadcom/bt/service/ftp/FTPService$UnmountBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "FTPService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/ftp/FTPService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UnmountBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/ftp/FTPService;


# direct methods
.method private constructor <init>(Lcom/broadcom/bt/service/ftp/FTPService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 224
    iput-object p1, p0, Lcom/broadcom/bt/service/ftp/FTPService$UnmountBroadcastReceiver;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/broadcom/bt/service/ftp/FTPService;Lcom/broadcom/bt/service/ftp/FTPService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 224
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/ftp/FTPService$UnmountBroadcastReceiver;-><init>(Lcom/broadcom/bt/service/ftp/FTPService;)V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 6
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 226
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    const-string v1, "android.intent.action.MEDIA_EJECT"

    #@6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_3c

    #@c
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    #@12
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_3c

    #@18
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    const-string v1, "android.intent.action.MEDIA_BAD_REMOVAL"

    #@1e
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_3c

    #@24
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    const-string v1, "android.intent.action.MEDIA_REMOVED"

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v0

    #@2e
    if-nez v0, :cond_3c

    #@30
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@33
    move-result-object v0

    #@34
    const-string v1, "broadcom.android.bluetooth.intent.action.MEDIA_EJECT"

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@39
    move-result v0

    #@3a
    if-eqz v0, :cond_63

    #@3c
    .line 232
    :cond_3c
    invoke-static {}, Lcom/broadcom/bt/service/ftp/FTPService;->access$800()Z

    #@3f
    move-result v0

    #@40
    if-eqz v0, :cond_63

    #@42
    .line 233
    const-string v0, "BluetoothFTPService"

    #@44
    new-instance v1, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v2, "Closing FTP Server during the event : "

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v1

    #@57
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v1

    #@5b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 234
    iget-object v0, p0, Lcom/broadcom/bt/service/ftp/FTPService$UnmountBroadcastReceiver;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@60
    invoke-virtual {v0}, Lcom/broadcom/bt/service/ftp/FTPService;->closeFtpServerWithoutCleanup()V

    #@63
    .line 237
    :cond_63
    return-void
.end method
