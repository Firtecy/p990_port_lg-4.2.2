.class Lcom/broadcom/bt/service/ftp/FTPService$1;
.super Landroid/os/Handler;
.source "FTPService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/ftp/FTPService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/ftp/FTPService;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/ftp/FTPService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 131
    iput-object p1, p0, Lcom/broadcom/bt/service/ftp/FTPService$1;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 21
    .parameter "msg"

    #@0
    .prologue
    .line 135
    move-object/from16 v0, p1

    #@2
    iget v2, v0, Landroid/os/Message;->what:I

    #@4
    packed-switch v2, :pswitch_data_132

    #@7
    .line 214
    :cond_7
    :goto_7
    return-void

    #@8
    .line 138
    :pswitch_8
    move-object/from16 v0, p1

    #@a
    iget-object v9, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    check-cast v9, Landroid/bluetooth/BluetoothDevice;

    #@e
    .line 139
    .local v9, device:Landroid/bluetooth/BluetoothDevice;
    move-object/from16 v0, p1

    #@10
    iget v10, v0, Landroid/os/Message;->arg1:I

    #@12
    .line 140
    .local v10, halState:I
    const/4 v2, 0x1

    #@13
    if-ne v10, v2, :cond_30

    #@15
    const/4 v11, 0x2

    #@16
    .line 142
    .local v11, newState:I
    :goto_16
    move-object/from16 v0, p0

    #@18
    iget-object v2, v0, Lcom/broadcom/bt/service/ftp/FTPService$1;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@1a
    invoke-static {v2}, Lcom/broadcom/bt/service/ftp/FTPService;->access$000(Lcom/broadcom/bt/service/ftp/FTPService;)Ljava/util/Map;

    #@1d
    move-result-object v2

    #@1e
    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_32

    #@24
    const/4 v13, 0x0

    #@25
    .line 144
    .local v13, prevStateInteger:Ljava/lang/Integer;
    :goto_25
    if-nez v13, :cond_42

    #@27
    const/4 v12, 0x0

    #@28
    .line 146
    .local v12, prevState:I
    :goto_28
    move-object/from16 v0, p0

    #@2a
    iget-object v2, v0, Lcom/broadcom/bt/service/ftp/FTPService$1;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@2c
    invoke-static {v2, v9, v11, v12}, Lcom/broadcom/bt/service/ftp/FTPService;->access$100(Lcom/broadcom/bt/service/ftp/FTPService;Landroid/bluetooth/BluetoothDevice;II)V

    #@2f
    goto :goto_7

    #@30
    .line 140
    .end local v11           #newState:I
    .end local v12           #prevState:I
    .end local v13           #prevStateInteger:Ljava/lang/Integer;
    :cond_30
    const/4 v11, 0x0

    #@31
    goto :goto_16

    #@32
    .line 142
    .restart local v11       #newState:I
    :cond_32
    move-object/from16 v0, p0

    #@34
    iget-object v2, v0, Lcom/broadcom/bt/service/ftp/FTPService$1;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@36
    invoke-static {v2}, Lcom/broadcom/bt/service/ftp/FTPService;->access$000(Lcom/broadcom/bt/service/ftp/FTPService;)Ljava/util/Map;

    #@39
    move-result-object v2

    #@3a
    invoke-interface {v2, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    move-result-object v2

    #@3e
    check-cast v2, Ljava/lang/Integer;

    #@40
    move-object v13, v2

    #@41
    goto :goto_25

    #@42
    .line 144
    .restart local v13       #prevStateInteger:Ljava/lang/Integer;
    :cond_42
    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    #@45
    move-result v12

    #@46
    goto :goto_28

    #@47
    .line 151
    .end local v9           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v10           #halState:I
    .end local v11           #newState:I
    .end local v13           #prevStateInteger:Ljava/lang/Integer;
    :pswitch_47
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@4a
    move-result-object v8

    #@4b
    .line 152
    .local v8, data:Landroid/os/Bundle;
    if-eqz v8, :cond_7

    #@4d
    .line 153
    const-string v2, "userid_length"

    #@4f
    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@52
    move-result v16

    #@53
    .line 154
    .local v16, userid_length:B
    const-string v2, "userid"

    #@55
    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@58
    move-result-object v15

    #@59
    .line 155
    .local v15, userid:Ljava/lang/String;
    const-string v2, "required"

    #@5b
    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@5e
    move-result v14

    #@5f
    .line 156
    .local v14, required:Z
    move-object/from16 v0, p0

    #@61
    iget-object v2, v0, Lcom/broadcom/bt/service/ftp/FTPService$1;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@63
    move/from16 v0, v16

    #@65
    invoke-static {v2, v15, v0, v14}, Lcom/broadcom/bt/service/ftp/FTPService;->access$200(Lcom/broadcom/bt/service/ftp/FTPService;Ljava/lang/String;BZ)V

    #@68
    goto :goto_7

    #@69
    .line 162
    .end local v8           #data:Landroid/os/Bundle;
    .end local v14           #required:Z
    .end local v15           #userid:Ljava/lang/String;
    .end local v16           #userid_length:B
    :pswitch_69
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@6c
    move-result-object v8

    #@6d
    .line 163
    .restart local v8       #data:Landroid/os/Bundle;
    const-string v2, "BluetoothFTPService"

    #@6f
    const-string v17, "MESSAGE_ON_FTPS_ACCESS_REQUESTED"

    #@71
    move-object/from16 v0, v17

    #@73
    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    .line 164
    if-eqz v8, :cond_7

    #@78
    .line 165
    const-string v2, "opcode"

    #@7a
    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@7d
    move-result v6

    #@7e
    .line 166
    .local v6, opcode:B
    const-string v2, "filename"

    #@80
    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@83
    move-result-object v3

    #@84
    .line 167
    .local v3, filename:Ljava/lang/String;
    const-string v2, "remotename"

    #@86
    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@89
    move-result-object v5

    #@8a
    .line 168
    .local v5, remotename:Ljava/lang/String;
    const-string v2, "remoteaddr"

    #@8c
    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@8f
    move-result-object v7

    #@90
    .line 170
    .local v7, remoteaddr:Ljava/lang/String;
    if-eqz v3, :cond_96

    #@92
    if-eqz v5, :cond_96

    #@94
    if-nez v7, :cond_a1

    #@96
    .line 171
    :cond_96
    const-string v2, "BluetoothFTPService"

    #@98
    const-string v17, "MESSAGE_ON_FTPS_ACCESS_REQUESTED: filname or remotename or remoteaddr is null!"

    #@9a
    move-object/from16 v0, v17

    #@9c
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    goto/16 :goto_7

    #@a1
    .line 175
    :cond_a1
    const-string v2, "size"

    #@a3
    invoke-virtual {v8, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@a6
    move-result v4

    #@a7
    .line 176
    .local v4, size:I
    move-object/from16 v0, p0

    #@a9
    iget-object v2, v0, Lcom/broadcom/bt/service/ftp/FTPService$1;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@ab
    invoke-static/range {v2 .. v7}, Lcom/broadcom/bt/service/ftp/FTPService;->access$300(Lcom/broadcom/bt/service/ftp/FTPService;Ljava/lang/String;ILjava/lang/String;BLjava/lang/String;)V

    #@ae
    goto/16 :goto_7

    #@b0
    .line 183
    .end local v3           #filename:Ljava/lang/String;
    .end local v4           #size:I
    .end local v5           #remotename:Ljava/lang/String;
    .end local v6           #opcode:B
    .end local v7           #remoteaddr:Ljava/lang/String;
    .end local v8           #data:Landroid/os/Bundle;
    :pswitch_b0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@b3
    move-result-object v8

    #@b4
    .line 184
    .restart local v8       #data:Landroid/os/Bundle;
    if-eqz v8, :cond_7

    #@b6
    .line 185
    move-object/from16 v0, p0

    #@b8
    iget-object v2, v0, Lcom/broadcom/bt/service/ftp/FTPService$1;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@ba
    const-string v17, "filepath"

    #@bc
    move-object/from16 v0, v17

    #@be
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@c1
    move-result-object v17

    #@c2
    const-string v18, "status"

    #@c4
    move-object/from16 v0, v18

    #@c6
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@c9
    move-result v18

    #@ca
    move-object/from16 v0, v17

    #@cc
    move/from16 v1, v18

    #@ce
    invoke-static {v2, v0, v1}, Lcom/broadcom/bt/service/ftp/FTPService;->access$400(Lcom/broadcom/bt/service/ftp/FTPService;Ljava/lang/String;B)V

    #@d1
    goto/16 :goto_7

    #@d3
    .line 192
    .end local v8           #data:Landroid/os/Bundle;
    :pswitch_d3
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d6
    move-result-object v8

    #@d7
    .line 193
    .restart local v8       #data:Landroid/os/Bundle;
    if-eqz v8, :cond_7

    #@d9
    .line 194
    move-object/from16 v0, p0

    #@db
    iget-object v2, v0, Lcom/broadcom/bt/service/ftp/FTPService$1;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@dd
    const-string v17, "filepath"

    #@df
    move-object/from16 v0, v17

    #@e1
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@e4
    move-result-object v17

    #@e5
    const-string v18, "status"

    #@e7
    move-object/from16 v0, v18

    #@e9
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@ec
    move-result v18

    #@ed
    move-object/from16 v0, v17

    #@ef
    move/from16 v1, v18

    #@f1
    invoke-static {v2, v0, v1}, Lcom/broadcom/bt/service/ftp/FTPService;->access$500(Lcom/broadcom/bt/service/ftp/FTPService;Ljava/lang/String;B)V

    #@f4
    goto/16 :goto_7

    #@f6
    .line 201
    .end local v8           #data:Landroid/os/Bundle;
    :pswitch_f6
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@f9
    move-result-object v8

    #@fa
    .line 202
    .restart local v8       #data:Landroid/os/Bundle;
    if-eqz v8, :cond_7

    #@fc
    .line 203
    move-object/from16 v0, p0

    #@fe
    iget-object v2, v0, Lcom/broadcom/bt/service/ftp/FTPService$1;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@100
    const-string v17, "filepath"

    #@102
    move-object/from16 v0, v17

    #@104
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@107
    move-result-object v17

    #@108
    const-string v18, "status"

    #@10a
    move-object/from16 v0, v18

    #@10c
    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@10f
    move-result v18

    #@110
    move-object/from16 v0, v17

    #@112
    move/from16 v1, v18

    #@114
    invoke-static {v2, v0, v1}, Lcom/broadcom/bt/service/ftp/FTPService;->access$600(Lcom/broadcom/bt/service/ftp/FTPService;Ljava/lang/String;B)V

    #@117
    goto/16 :goto_7

    #@119
    .line 209
    .end local v8           #data:Landroid/os/Bundle;
    :pswitch_119
    move-object/from16 v0, p0

    #@11b
    iget-object v2, v0, Lcom/broadcom/bt/service/ftp/FTPService$1;->this$0:Lcom/broadcom/bt/service/ftp/FTPService;

    #@11d
    move-object/from16 v0, p1

    #@11f
    iget v0, v0, Landroid/os/Message;->arg1:I

    #@121
    move/from16 v17, v0

    #@123
    move-object/from16 v0, p1

    #@125
    iget v0, v0, Landroid/os/Message;->arg2:I

    #@127
    move/from16 v18, v0

    #@129
    move/from16 v0, v17

    #@12b
    move/from16 v1, v18

    #@12d
    invoke-static {v2, v0, v1}, Lcom/broadcom/bt/service/ftp/FTPService;->access$700(Lcom/broadcom/bt/service/ftp/FTPService;II)V

    #@130
    goto/16 :goto_7

    #@132
    .line 135
    :pswitch_data_132
    .packed-switch 0x3
        :pswitch_8
        :pswitch_47
        :pswitch_69
        :pswitch_b0
        :pswitch_d3
        :pswitch_f6
        :pswitch_119
    .end packed-switch
.end method
