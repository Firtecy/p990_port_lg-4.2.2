.class public Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;
.super Landroid/preference/PreferenceFragment;
.source "MapAdvancedSettings.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings$1;,
        Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings$SwitchComparator;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MapAdvancedSettings"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    #@3
    .line 121
    return-void
.end method

.method private initDataSources()V
    .registers 16

    #@0
    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@3
    move-result-object v3

    #@4
    .line 82
    .local v3, mgr:Landroid/preference/PreferenceManager;
    invoke-virtual {v3}, Landroid/preference/PreferenceManager;->getSharedPreferences()Landroid/content/SharedPreferences;

    #@7
    move-result-object v10

    #@8
    .line 83
    .local v10, sp:Landroid/content/SharedPreferences;
    if-nez v10, :cond_12

    #@a
    .line 84
    const-string v12, "MapAdvancedSettings"

    #@c
    const-string v13, "initDataSources(): SharedPreferences not available"

    #@e
    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 119
    :cond_11
    :goto_11
    return-void

    #@12
    .line 87
    :cond_12
    invoke-interface {v10}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    #@15
    move-result-object v7

    #@16
    .line 88
    .local v7, prefs:Ljava/util/Map;,"Ljava/util/Map<Ljava/lang/String;*>;"
    if-eqz v7, :cond_1e

    #@18
    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    #@1b
    move-result v12

    #@1c
    if-eqz v12, :cond_26

    #@1e
    .line 89
    :cond_1e
    const-string v12, "MapAdvancedSettings"

    #@20
    const-string v13, "initDataSources(): no preferences"

    #@22
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_11

    #@26
    .line 93
    :cond_26
    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@29
    move-result-object v12

    #@2a
    invoke-interface {v12}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@2d
    move-result-object v1

    #@2e
    .line 94
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    new-instance v9, Ljava/util/ArrayList;

    #@30
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@33
    .line 95
    .local v9, sortedPrefs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/preference/SwitchPreference;>;"
    :cond_33
    :goto_33
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@36
    move-result v12

    #@37
    if-eqz v12, :cond_8e

    #@39
    .line 96
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@3c
    move-result-object v2

    #@3d
    check-cast v2, Ljava/lang/String;

    #@3f
    .line 97
    .local v2, key:Ljava/lang/String;
    const-string v12, "ds/"

    #@41
    invoke-virtual {v2, v12}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@44
    move-result v12

    #@45
    if-eqz v12, :cond_33

    #@47
    .line 99
    :try_start_47
    invoke-static {v2}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceProviderNameKey(Ljava/lang/String;)Ljava/lang/String;

    #@4a
    move-result-object v12

    #@4b
    invoke-interface {v7, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4e
    move-result-object v8

    #@4f
    check-cast v8, Ljava/lang/String;

    #@51
    .line 101
    .local v8, providerName:Ljava/lang/String;
    invoke-static {v2}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toPreferenceDsNameKey(Ljava/lang/String;)Ljava/lang/String;

    #@54
    move-result-object v12

    #@55
    invoke-interface {v7, v12}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@58
    move-result-object v0

    #@59
    check-cast v0, Ljava/lang/String;

    #@5b
    .line 103
    .local v0, dsName:Ljava/lang/String;
    new-instance v6, Landroid/preference/SwitchPreference;

    #@5d
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;->getActivity()Landroid/app/Activity;

    #@60
    move-result-object v12

    #@61
    invoke-direct {v6, v12}, Landroid/preference/SwitchPreference;-><init>(Landroid/content/Context;)V

    #@64
    .line 104
    .local v6, pref:Landroid/preference/SwitchPreference;
    invoke-virtual {v6, v2}, Landroid/preference/SwitchPreference;->setKey(Ljava/lang/String;)V

    #@67
    .line 105
    invoke-virtual {v6, v8}, Landroid/preference/SwitchPreference;->setTitle(Ljava/lang/CharSequence;)V

    #@6a
    .line 106
    invoke-virtual {v6, v0}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    #@6d
    .line 107
    invoke-virtual {v6, p0}, Landroid/preference/SwitchPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    #@70
    .line 108
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_73
    .catch Ljava/lang/Throwable; {:try_start_47 .. :try_end_73} :catch_74

    #@73
    goto :goto_33

    #@74
    .line 109
    .end local v0           #dsName:Ljava/lang/String;
    .end local v6           #pref:Landroid/preference/SwitchPreference;
    .end local v8           #providerName:Ljava/lang/String;
    :catch_74
    move-exception v11

    #@75
    .line 110
    .local v11, t:Ljava/lang/Throwable;
    const-string v12, "MapAdvancedSettings"

    #@77
    new-instance v13, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v14, "initDataSources: error initializing data source "

    #@7e
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v13

    #@82
    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v13

    #@86
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v13

    #@8a
    invoke-static {v12, v13, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8d
    goto :goto_33

    #@8e
    .line 114
    .end local v2           #key:Ljava/lang/String;
    .end local v11           #t:Ljava/lang/Throwable;
    :cond_8e
    new-instance v12, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings$SwitchComparator;

    #@90
    const/4 v13, 0x0

    #@91
    invoke-direct {v12, p0, v13}, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings$SwitchComparator;-><init>(Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings$1;)V

    #@94
    invoke-static {v9, v12}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    #@97
    .line 115
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    #@9a
    move-result-object v5

    #@9b
    .line 116
    .local v5, pScreen:Landroid/preference/PreferenceScreen;
    const/4 v4, 0x0

    #@9c
    .local v4, p:I
    :goto_9c
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    #@9f
    move-result v12

    #@a0
    if-ge v4, v12, :cond_11

    #@a2
    .line 117
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@a5
    move-result-object v12

    #@a6
    check-cast v12, Landroid/preference/Preference;

    #@a8
    invoke-virtual {v5, v12}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    #@ab
    .line 116
    add-int/lit8 v4, v4, 0x1

    #@ad
    goto :goto_9c
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 4
    .parameter "b"

    #@0
    .prologue
    .line 73
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 74
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;->getPreferenceManager()Landroid/preference/PreferenceManager;

    #@6
    move-result-object v0

    #@7
    const-string v1, "MapSharedPreferences"

    #@9
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceManager;->setSharedPreferencesName(Ljava/lang/String;)V

    #@c
    .line 76
    const/high16 v0, 0x7f04

    #@e
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;->addPreferencesFromResource(I)V

    #@11
    .line 77
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;->initDataSources()V

    #@14
    .line 78
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 13
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    .line 140
    const-string v7, "MapAdvancedSettings"

    #@2
    new-instance v8, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v9, "Preference changed..."

    #@9
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v8

    #@d
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@10
    move-result-object v9

    #@11
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v8

    #@15
    const-string v9, ", new Value="

    #@17
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v8

    #@1b
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v8

    #@1f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v8

    #@23
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 142
    :try_start_26
    move-object v0, p2

    #@27
    check-cast v0, Ljava/lang/Boolean;

    #@29
    move-object v2, v0

    #@2a
    .line 143
    .local v2, enabled:Ljava/lang/Boolean;
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    .line 144
    .local v3, key:Ljava/lang/String;
    invoke-static {v3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toProviderId(Ljava/lang/String;)Ljava/lang/String;

    #@31
    move-result-object v5

    #@32
    .line 145
    .local v5, providerId:Ljava/lang/String;
    invoke-static {v3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->toDsId(Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    .line 146
    .local v1, dsId:Ljava/lang/String;
    invoke-static {}, Lcom/broadcom/bt/service/map/MapService;->getLocalBinder()Lcom/broadcom/bt/map/IBluetoothMap;

    #@39
    move-result-object v4

    #@3a
    .line 148
    .local v4, mService:Lcom/broadcom/bt/map/IBluetoothMap;
    if-nez v4, :cond_55

    #@3c
    .line 149
    const-string v7, "MapAdvancedSettings"

    #@3e
    const-string v8, " mService is null!!!"

    #@40
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 150
    new-instance v7, Ljava/lang/Exception;

    #@45
    const-string v8, "mServie is null!"

    #@47
    invoke-direct {v7, v8}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@4a
    throw v7
    :try_end_4b
    .catch Ljava/lang/Throwable; {:try_start_26 .. :try_end_4b} :catch_4b

    #@4b
    .line 155
    .end local v1           #dsId:Ljava/lang/String;
    .end local v2           #enabled:Ljava/lang/Boolean;
    .end local v3           #key:Ljava/lang/String;
    .end local v4           #mService:Lcom/broadcom/bt/map/IBluetoothMap;
    .end local v5           #providerId:Ljava/lang/String;
    :catch_4b
    move-exception v6

    #@4c
    .line 156
    .local v6, t:Ljava/lang/Throwable;
    const-string v7, "MapAdvancedSettings"

    #@4e
    const-string v8, "Unable to set Data source state"

    #@50
    invoke-static {v7, v8, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@53
    .line 157
    const/4 v7, 0x0

    #@54
    .end local v6           #t:Ljava/lang/Throwable;
    :goto_54
    return v7

    #@55
    .line 153
    .restart local v1       #dsId:Ljava/lang/String;
    .restart local v2       #enabled:Ljava/lang/Boolean;
    .restart local v3       #key:Ljava/lang/String;
    .restart local v4       #mService:Lcom/broadcom/bt/map/IBluetoothMap;
    .restart local v5       #providerId:Ljava/lang/String;
    :cond_55
    :try_start_55
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    #@58
    move-result v7

    #@59
    invoke-interface {v4, v5, v1, v7}, Lcom/broadcom/bt/map/IBluetoothMap;->setDatasourceState(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_5c
    .catch Ljava/lang/Throwable; {:try_start_55 .. :try_end_5c} :catch_4b

    #@5c
    .line 154
    const/4 v7, 0x1

    #@5d
    goto :goto_54
.end method
