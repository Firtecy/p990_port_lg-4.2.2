.class Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;
.super Landroid/content/BroadcastReceiver;
.source "MmsEventManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 441
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 444
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 445
    .local v0, action:Ljava/lang/String;
    const-string v4, "com.lge.bluetooth.mms.RECEIVED"

    #@6
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v4

    #@a
    if-eqz v4, :cond_2a

    #@c
    .line 446
    const-string v4, "message_id"

    #@e
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v1

    #@12
    .line 447
    .local v1, messageId:Ljava/lang/String;
    const/4 v3, 0x0

    #@13
    .line 448
    .local v3, oMsgInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    new-instance v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;

    #@15
    const/4 v4, 0x0

    #@16
    invoke-direct {v2, v4}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;-><init>(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;)V

    #@19
    .line 449
    .local v2, nMsgInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    const/4 v4, 0x1

    #@1a
    iput v4, v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@1c
    .line 450
    iget v4, v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@1e
    invoke-static {v4}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@21
    move-result-object v4

    #@22
    iput-object v4, v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@24
    .line 451
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;

    #@26
    invoke-static {v4, v1, v2, v3}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->access$100(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)V

    #@29
    .line 459
    .end local v1           #messageId:Ljava/lang/String;
    .end local v2           #nMsgInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .end local v3           #oMsgInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 453
    :cond_2a
    const-string v4, "com.lge.bluetooth.mms.DELETED"

    #@2c
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f
    move-result v4

    #@30
    if-eqz v4, :cond_29

    #@32
    .line 454
    const-string v4, "message_id"

    #@34
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    .line 455
    .restart local v1       #messageId:Ljava/lang/String;
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;

    #@3a
    invoke-static {v4, v1}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->access$200(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;Ljava/lang/String;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;

    #@3d
    move-result-object v3

    #@3e
    .line 456
    .restart local v3       #oMsgInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    const/4 v2, 0x0

    #@3f
    .line 457
    .restart local v2       #nMsgInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;

    #@41
    invoke-static {v4, v1, v2, v3}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->access$100(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)V

    #@44
    goto :goto_29
.end method
