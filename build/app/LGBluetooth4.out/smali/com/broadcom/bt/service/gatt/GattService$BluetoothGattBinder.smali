.class Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;
.super Lcom/broadcom/bt/gatt/IBluetoothGatt$Stub;
.source "GattService.java"

# interfaces
.implements Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/gatt/GattService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BluetoothGattBinder"
.end annotation


# instance fields
.field private mService:Lcom/broadcom/bt/service/gatt/GattService;


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/service/gatt/GattService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 323
    invoke-direct {p0}, Lcom/broadcom/bt/gatt/IBluetoothGatt$Stub;-><init>()V

    #@3
    .line 324
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->mService:Lcom/broadcom/bt/service/gatt/GattService;

    #@5
    .line 325
    return-void
.end method

.method private getService()Lcom/broadcom/bt/service/gatt/GattService;
    .registers 3

    #@0
    .prologue
    .line 333
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->mService:Lcom/broadcom/bt/service/gatt/GattService;

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->mService:Lcom/broadcom/bt/service/gatt/GattService;

    #@6
    #calls: Lcom/broadcom/bt/service/gatt/GattService;->isAvailable()Z
    invoke-static {v0}, Lcom/broadcom/bt/service/gatt/GattService;->access$000(Lcom/broadcom/bt/service/gatt/GattService;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_f

    #@c
    .line 334
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->mService:Lcom/broadcom/bt/service/gatt/GattService;

    #@e
    .line 337
    :goto_e
    return-object v0

    #@f
    .line 336
    :cond_f
    const-string v0, "BtGatt.GattService"

    #@11
    const-string v1, "getService() - Service requested, but not available!"

    #@13
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 337
    const/4 v0, 0x0

    #@17
    goto :goto_e
.end method


# virtual methods
.method public addCharacteristic(BLandroid/os/ParcelUuid;II)V
    .registers 7
    .parameter "serverIf"
    .parameter "charId"
    .parameter "properties"
    .parameter "permissions"

    #@0
    .prologue
    .line 599
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 600
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 605
    :goto_6
    return-void

    #@7
    .line 603
    :cond_7
    invoke-virtual {p2}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, p1, v1, p3, p4}, Lcom/broadcom/bt/service/gatt/GattService;->addCharacteristic(BLjava/util/UUID;II)V

    #@e
    goto :goto_6
.end method

.method public addDescriptor(BLandroid/os/ParcelUuid;I)V
    .registers 6
    .parameter "serverIf"
    .parameter "descId"
    .parameter "permissions"

    #@0
    .prologue
    .line 609
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 610
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 614
    :goto_6
    return-void

    #@7
    .line 613
    :cond_7
    invoke-virtual {p2}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, p1, v1, p3}, Lcom/broadcom/bt/service/gatt/GattService;->addDescriptor(BLjava/util/UUID;I)V

    #@e
    goto :goto_6
.end method

.method public addIncludedService(BIILandroid/os/ParcelUuid;)V
    .registers 7
    .parameter "serverIf"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcId"

    #@0
    .prologue
    .line 589
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 590
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 595
    :goto_6
    return-void

    #@7
    .line 593
    :cond_7
    invoke-virtual {p4}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/broadcom/bt/service/gatt/GattService;->addIncludedService(BIILjava/util/UUID;)V

    #@e
    goto :goto_6
.end method

.method public beginReliableWrite(BLjava/lang/String;)V
    .registers 4
    .parameter "clientIf"
    .parameter "address"

    #@0
    .prologue
    .line 487
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 488
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 492
    :goto_6
    return-void

    #@7
    .line 491
    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->beginReliableWrite(BLjava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method public beginServiceDeclaration(BIIILandroid/os/ParcelUuid;)V
    .registers 13
    .parameter "serverIf"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "minHandles"
    .parameter "srvcId"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 566
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@4
    move-result-object v0

    #@5
    .line 567
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_8

    #@7
    .line 585
    :goto_7
    return-void

    #@8
    .line 572
    :cond_8
    :goto_8
    invoke-static {}, Lcom/broadcom/bt/service/gatt/GattService;->access$100()Z

    #@b
    move-result v1

    #@c
    if-ne v1, v3, :cond_24

    #@e
    .line 575
    :try_start_e
    const-string v1, "BtGatt.GattService"

    #@10
    const-string v2, "beginServiceDeclaration service is registering wait 10ms"

    #@12
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 576
    const-wide/16 v1, 0xa

    #@17
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1a
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_1a} :catch_1b

    #@1a
    goto :goto_8

    #@1b
    .line 577
    :catch_1b
    move-exception v6

    #@1c
    .line 578
    .local v6, e1:Ljava/lang/InterruptedException;
    const-string v1, "BtGatt.GattService"

    #@1e
    const-string v2, "Client thread was interrupted (1), exiting"

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_8

    #@24
    .line 581
    .end local v6           #e1:Ljava/lang/InterruptedException;
    :cond_24
    invoke-static {v3}, Lcom/broadcom/bt/service/gatt/GattService;->access$102(Z)Z

    #@27
    .line 583
    invoke-virtual {p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@2a
    move-result-object v5

    #@2b
    move v1, p1

    #@2c
    move v2, p2

    #@2d
    move v3, p3

    #@2e
    move v4, p4

    #@2f
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/gatt/GattService;->beginServiceDeclaration(BIIILjava/util/UUID;)V

    #@32
    goto :goto_7
.end method

.method public cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 328
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->mService:Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    .line 329
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public clearServices(B)V
    .registers 3
    .parameter "serverIf"

    #@0
    .prologue
    .line 635
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 636
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 640
    :goto_6
    return-void

    #@7
    .line 639
    :cond_7
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/gatt/GattService;->clearServices(B)V

    #@a
    goto :goto_6
.end method

.method public clientConnect(BLjava/lang/String;Z)V
    .registers 5
    .parameter "clientIf"
    .parameter "address"
    .parameter "isDirect"

    #@0
    .prologue
    .line 399
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 400
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 404
    :goto_6
    return-void

    #@7
    .line 403
    :cond_7
    invoke-virtual {v0, p1, p2, p3}, Lcom/broadcom/bt/service/gatt/GattService;->clientConnect(BLjava/lang/String;Z)V

    #@a
    goto :goto_6
.end method

.method public clientDisconnect(BLjava/lang/String;)V
    .registers 4
    .parameter "clientIf"
    .parameter "address"

    #@0
    .prologue
    .line 407
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 408
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 412
    :goto_6
    return-void

    #@7
    .line 411
    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->clientDisconnect(BLjava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method public discoverServices(BLjava/lang/String;)V
    .registers 4
    .parameter "clientIf"
    .parameter "address"

    #@0
    .prologue
    .line 423
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 424
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 428
    :goto_6
    return-void

    #@7
    .line 427
    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->discoverServices(BLjava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method public endReliableWrite(BLjava/lang/String;Z)V
    .registers 5
    .parameter "clientIf"
    .parameter "address"
    .parameter "execute"

    #@0
    .prologue
    .line 495
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 496
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 500
    :goto_6
    return-void

    #@7
    .line 499
    :cond_7
    invoke-virtual {v0, p1, p2, p3}, Lcom/broadcom/bt/service/gatt/GattService;->endReliableWrite(BLjava/lang/String;Z)V

    #@a
    goto :goto_6
.end method

.method public endServiceDeclaration(B)V
    .registers 3
    .parameter "serverIf"

    #@0
    .prologue
    .line 617
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 618
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 622
    :goto_6
    return-void

    #@7
    .line 621
    :cond_7
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/gatt/GattService;->endServiceDeclaration(B)V

    #@a
    goto :goto_6
.end method

.method public getDeviceType(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 665
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 666
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_8

    #@6
    .line 667
    const/4 v1, 0x0

    #@7
    .line 669
    :goto_7
    return v1

    #@8
    :cond_8
    #calls: Lcom/broadcom/bt/service/gatt/GattService;->getDeviceType(Landroid/bluetooth/BluetoothDevice;)I
    invoke-static {v0, p1}, Lcom/broadcom/bt/service/gatt/GattService;->access$200(Lcom/broadcom/bt/service/gatt/GattService;Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getDevicesMatchingConnectionStates([II)Ljava/util/List;
    .registers 5
    .parameter "states"
    .parameter "role"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 344
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 345
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_c

    #@6
    .line 346
    new-instance v1, Ljava/util/ArrayList;

    #@8
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@b
    .line 350
    :goto_b
    return-object v1

    #@c
    :cond_c
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->getDevicesMatchingConnectionStates([II)Ljava/util/List;

    #@f
    move-result-object v1

    #@10
    goto :goto_b
.end method

.method public readCharacteristic(BLjava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;B)V
    .registers 18
    .parameter "clientIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcId"
    .parameter "charInstanceId"
    .parameter "charId"
    .parameter "authReq"

    #@0
    .prologue
    .line 434
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 435
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 441
    :goto_6
    return-void

    #@7
    .line 438
    :cond_7
    invoke-virtual {p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v5

    #@b
    invoke-virtual/range {p7 .. p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@e
    move-result-object v7

    #@f
    move v1, p1

    #@10
    move-object v2, p2

    #@11
    move v3, p3

    #@12
    move v4, p4

    #@13
    move v6, p6

    #@14
    move/from16 v8, p8

    #@16
    invoke-virtual/range {v0 .. v8}, Lcom/broadcom/bt/service/gatt/GattService;->readCharacteristic(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;B)V

    #@19
    goto :goto_6
.end method

.method public readDescriptor(BLjava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;B)V
    .registers 20
    .parameter "clientIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcId"
    .parameter "charInstanceId"
    .parameter "charId"
    .parameter "descrId"
    .parameter "authReq"

    #@0
    .prologue
    .line 461
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 462
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 469
    :goto_6
    return-void

    #@7
    .line 465
    :cond_7
    invoke-virtual {p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v5

    #@b
    invoke-virtual/range {p7 .. p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@e
    move-result-object v7

    #@f
    invoke-virtual/range {p8 .. p8}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@12
    move-result-object v8

    #@13
    move v1, p1

    #@14
    move-object v2, p2

    #@15
    move v3, p3

    #@16
    move v4, p4

    #@17
    move/from16 v6, p6

    #@19
    move/from16 v9, p9

    #@1b
    invoke-virtual/range {v0 .. v9}, Lcom/broadcom/bt/service/gatt/GattService;->readDescriptor(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;Ljava/util/UUID;B)V

    #@1e
    goto :goto_6
.end method

.method public readRemoteRssi(BLjava/lang/String;)V
    .registers 4
    .parameter "clientIf"
    .parameter "address"

    #@0
    .prologue
    .line 516
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 517
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 521
    :goto_6
    return-void

    #@7
    .line 520
    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->readRemoteRssi(BLjava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method public refreshDevice(BLjava/lang/String;)V
    .registers 4
    .parameter "clientIf"
    .parameter "address"

    #@0
    .prologue
    .line 415
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 416
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 420
    :goto_6
    return-void

    #@7
    .line 419
    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->refreshDevice(BLjava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method public registerClient(Landroid/os/ParcelUuid;Lcom/broadcom/bt/gatt/IBluetoothGattCallback;)V
    .registers 5
    .parameter "uuid"
    .parameter "callback"

    #@0
    .prologue
    .line 355
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 356
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 360
    :goto_6
    return-void

    #@7
    .line 359
    :cond_7
    invoke-virtual {p1}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->registerClient(Ljava/util/UUID;Lcom/broadcom/bt/gatt/IBluetoothGattCallback;)V

    #@e
    goto :goto_6
.end method

.method public registerForNotification(BLjava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Z)V
    .registers 18
    .parameter "clientIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcId"
    .parameter "charInstanceId"
    .parameter "charId"
    .parameter "enable"

    #@0
    .prologue
    .line 506
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 507
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 513
    :goto_6
    return-void

    #@7
    .line 510
    :cond_7
    invoke-virtual {p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v5

    #@b
    invoke-virtual/range {p7 .. p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@e
    move-result-object v7

    #@f
    move v1, p1

    #@10
    move-object v2, p2

    #@11
    move v3, p3

    #@12
    move v4, p4

    #@13
    move v6, p6

    #@14
    move/from16 v8, p8

    #@16
    invoke-virtual/range {v0 .. v8}, Lcom/broadcom/bt/service/gatt/GattService;->registerForNotification(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;Z)V

    #@19
    goto :goto_6
.end method

.method public registerServer(Landroid/os/ParcelUuid;Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;)V
    .registers 5
    .parameter "uuid"
    .parameter "callback"

    #@0
    .prologue
    .line 532
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 533
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 537
    :goto_6
    return-void

    #@7
    .line 536
    :cond_7
    invoke-virtual {p1}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, v1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->registerServer(Ljava/util/UUID;Lcom/broadcom/bt/gatt/IBluetoothGattServerCallback;)V

    #@e
    goto :goto_6
.end method

.method public removeService(BIILandroid/os/ParcelUuid;)V
    .registers 7
    .parameter "serverIf"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcId"

    #@0
    .prologue
    .line 626
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 627
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 632
    :goto_6
    return-void

    #@7
    .line 630
    :cond_7
    invoke-virtual {p4}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/broadcom/bt/service/gatt/GattService;->removeService(BIILjava/util/UUID;)V

    #@e
    goto :goto_6
.end method

.method public sendNotification(BLjava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Z[B)V
    .registers 20
    .parameter "serverIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcId"
    .parameter "charInstanceId"
    .parameter "charId"
    .parameter "confirm"
    .parameter "value"

    #@0
    .prologue
    .line 655
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 656
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 661
    :goto_6
    return-void

    #@7
    .line 659
    :cond_7
    invoke-virtual {p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v5

    #@b
    invoke-virtual/range {p7 .. p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@e
    move-result-object v7

    #@f
    move v1, p1

    #@10
    move-object v2, p2

    #@11
    move v3, p3

    #@12
    move v4, p4

    #@13
    move/from16 v6, p6

    #@15
    move/from16 v8, p8

    #@17
    move-object/from16 v9, p9

    #@19
    invoke-virtual/range {v0 .. v9}, Lcom/broadcom/bt/service/gatt/GattService;->sendNotification(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;Z[B)V

    #@1c
    goto :goto_6
.end method

.method public sendResponse(BLjava/lang/String;III[B)V
    .registers 14
    .parameter "serverIf"
    .parameter "address"
    .parameter "requestId"
    .parameter "status"
    .parameter "offset"
    .parameter "value"

    #@0
    .prologue
    .line 644
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 645
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 649
    :goto_6
    return-void

    #@7
    :cond_7
    move v1, p1

    #@8
    move-object v2, p2

    #@9
    move v3, p3

    #@a
    move v4, p4

    #@b
    move v5, p5

    #@c
    move-object v6, p6

    #@d
    .line 648
    invoke-virtual/range {v0 .. v6}, Lcom/broadcom/bt/service/gatt/GattService;->sendResponse(BLjava/lang/String;III[B)V

    #@10
    goto :goto_6
.end method

.method public serverConnect(BLjava/lang/String;Z)V
    .registers 5
    .parameter "serverIf"
    .parameter "address"
    .parameter "isDirect"

    #@0
    .prologue
    .line 548
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 549
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 553
    :goto_6
    return-void

    #@7
    .line 552
    :cond_7
    invoke-virtual {v0, p1, p2, p3}, Lcom/broadcom/bt/service/gatt/GattService;->serverConnect(BLjava/lang/String;Z)V

    #@a
    goto :goto_6
.end method

.method public serverDisconnect(BLjava/lang/String;)V
    .registers 4
    .parameter "serverIf"
    .parameter "address"

    #@0
    .prologue
    .line 556
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 557
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 561
    :goto_6
    return-void

    #@7
    .line 560
    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->serverDisconnect(BLjava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method public serverReadRemoteRssi(BLjava/lang/String;)V
    .registers 4
    .parameter "serverIf"
    .parameter "address"

    #@0
    .prologue
    .line 524
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 525
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 529
    :goto_6
    return-void

    #@7
    .line 528
    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->serverReadRemoteRssi(BLjava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method public startScan(BZ)V
    .registers 4
    .parameter "appIf"
    .parameter "isServer"

    #@0
    .prologue
    .line 371
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 372
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 376
    :goto_6
    return-void

    #@7
    .line 375
    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->startScan(BZ)V

    #@a
    goto :goto_6
.end method

.method public startScanWithUuids(BZ[Landroid/os/ParcelUuid;)V
    .registers 8
    .parameter "appIf"
    .parameter "isServer"
    .parameter "ids"

    #@0
    .prologue
    .line 379
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v1

    #@4
    .line 380
    .local v1, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v1, :cond_7

    #@6
    .line 388
    :goto_6
    return-void

    #@7
    .line 383
    :cond_7
    array-length v3, p3

    #@8
    new-array v2, v3, [Ljava/util/UUID;

    #@a
    .line 384
    .local v2, uuids:[Ljava/util/UUID;
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    array-length v3, p3

    #@c
    if-eq v0, v3, :cond_19

    #@e
    .line 385
    aget-object v3, p3, v0

    #@10
    invoke-virtual {v3}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@13
    move-result-object v3

    #@14
    aput-object v3, v2, v0

    #@16
    .line 384
    add-int/lit8 v0, v0, 0x1

    #@18
    goto :goto_b

    #@19
    .line 387
    :cond_19
    invoke-virtual {v1, p1, p2, v2}, Lcom/broadcom/bt/service/gatt/GattService;->startScanWithUuids(BZ[Ljava/util/UUID;)V

    #@1c
    goto :goto_6
.end method

.method public stopScan(BZ)V
    .registers 4
    .parameter "appIf"
    .parameter "isServer"

    #@0
    .prologue
    .line 391
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 392
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 396
    :goto_6
    return-void

    #@7
    .line 395
    :cond_7
    invoke-virtual {v0, p1, p2}, Lcom/broadcom/bt/service/gatt/GattService;->stopScan(BZ)V

    #@a
    goto :goto_6
.end method

.method public unregisterClient(B)V
    .registers 3
    .parameter "clientIf"

    #@0
    .prologue
    .line 363
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 364
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 368
    :goto_6
    return-void

    #@7
    .line 367
    :cond_7
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/gatt/GattService;->unregisterClient(B)V

    #@a
    goto :goto_6
.end method

.method public unregisterServer(B)V
    .registers 3
    .parameter "serverIf"

    #@0
    .prologue
    .line 540
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 541
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 545
    :goto_6
    return-void

    #@7
    .line 544
    :cond_7
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/gatt/GattService;->unregisterServer(B)V

    #@a
    goto :goto_6
.end method

.method public writeCharacteristic(BLjava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;IB[B)V
    .registers 22
    .parameter "clientIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcId"
    .parameter "charInstanceId"
    .parameter "charId"
    .parameter "writeType"
    .parameter "authReq"
    .parameter "value"

    #@0
    .prologue
    .line 447
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 448
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 455
    :goto_6
    return-void

    #@7
    .line 451
    :cond_7
    invoke-virtual/range {p5 .. p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v5

    #@b
    invoke-virtual/range {p7 .. p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@e
    move-result-object v7

    #@f
    move v1, p1

    #@10
    move-object v2, p2

    #@11
    move v3, p3

    #@12
    move v4, p4

    #@13
    move/from16 v6, p6

    #@15
    move/from16 v8, p8

    #@17
    move/from16 v9, p9

    #@19
    move-object/from16 v10, p10

    #@1b
    invoke-virtual/range {v0 .. v10}, Lcom/broadcom/bt/service/gatt/GattService;->writeCharacteristic(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;IB[B)V

    #@1e
    goto :goto_6
.end method

.method public writeDescriptor(BLjava/lang/String;IILandroid/os/ParcelUuid;ILandroid/os/ParcelUuid;Landroid/os/ParcelUuid;IB[B)V
    .registers 24
    .parameter "clientIf"
    .parameter "address"
    .parameter "srvcType"
    .parameter "srvcInstanceId"
    .parameter "srvcId"
    .parameter "charInstanceId"
    .parameter "charId"
    .parameter "descrId"
    .parameter "writeType"
    .parameter "authReq"
    .parameter "value"

    #@0
    .prologue
    .line 476
    invoke-direct {p0}, Lcom/broadcom/bt/service/gatt/GattService$BluetoothGattBinder;->getService()Lcom/broadcom/bt/service/gatt/GattService;

    #@3
    move-result-object v0

    #@4
    .line 477
    .local v0, service:Lcom/broadcom/bt/service/gatt/GattService;
    if-nez v0, :cond_7

    #@6
    .line 484
    :goto_6
    return-void

    #@7
    .line 480
    :cond_7
    invoke-virtual/range {p5 .. p5}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@a
    move-result-object v5

    #@b
    invoke-virtual/range {p7 .. p7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@e
    move-result-object v7

    #@f
    invoke-virtual/range {p8 .. p8}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@12
    move-result-object v8

    #@13
    move v1, p1

    #@14
    move-object v2, p2

    #@15
    move v3, p3

    #@16
    move/from16 v4, p4

    #@18
    move/from16 v6, p6

    #@1a
    move/from16 v9, p9

    #@1c
    move/from16 v10, p10

    #@1e
    move-object/from16 v11, p11

    #@20
    invoke-virtual/range {v0 .. v11}, Lcom/broadcom/bt/service/gatt/GattService;->writeDescriptor(BLjava/lang/String;IILjava/util/UUID;ILjava/util/UUID;Ljava/util/UUID;IB[B)V

    #@23
    goto :goto_6
.end method
