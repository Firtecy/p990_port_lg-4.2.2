.class public Lcom/broadcom/bt/service/map/MapServiceConfig;
.super Ljava/lang/Object;
.source "MapServiceConfig.java"


# static fields
.field public static final AUTO_ACCEPT_ACCESS:Z = true

.field public static final DBG:Z = true

.field public static final DEBUG_ADMIN:Z = true

.field public static final DEBUG_KEEP_TMP_BMSG_FILES:Z = false

.field public static final DEBUG_MAX_CACHED_COUNT:I = 0xa

.field public static final DEBUG_MOCK:Z = false

.field public static final DEFAULT_TMP_SUBDIR:Ljava/lang/String; = "map"

.field public static final DS_FOLDER_TIMEOUT_MS:I = 0x2710

.field public static final DS_MSG_TIMEOUT_MS:I = 0x2710

.field public static final MAX_DS_INSTANCES:I = 0x5

.field public static final MSE_TIMEOUT_MS:I = 0xbb8

.field public static final MSG_MAP_MAX_LENGTH:I = 0x7530

.field public static final NOTIFICATION_DELAY_MS:I = 0x320

.field public static final NOTIFICATION_TIMEOUT_MS:I = 0xbb8

.field public static final TAG_PREFIX:Ljava/lang/String; = "BtMap."


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 50
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
