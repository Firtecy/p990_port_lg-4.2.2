.class Lcom/broadcom/bt/service/gatt/GattDebugUtils;
.super Ljava/lang/Object;
.source "GattDebugUtils.java"


# static fields
.field private static final ACTION_DEBUG_DUMP_CLIENTMAP:Ljava/lang/String; = "broadcom.bluetooth.action.DEBUG_DUMP_CLIENTMAP"

.field private static final ACTION_DEBUG_DUMP_HANDLEMAP:Ljava/lang/String; = "broadcom.bluetooth.action.DEBUG_DUMP_HANDLEMAP"

.field private static final ACTION_DEBUG_DUMP_SERVERMAP:Ljava/lang/String; = "broadcom.bluetooth.action.DEBUG_DUMP_SERVERMAP"

.field private static final ACTION_GATT_PAIRING_CONFIG:Ljava/lang/String; = "broadcom.bluetooth.action.GATT_PAIRING_CONFIG"

.field private static final ACTION_GATT_TEST_CONNECT:Ljava/lang/String; = "broadcom.bluetooth.action.GATT_TEST_CONNECT"

.field private static final ACTION_GATT_TEST_DISCONNECT:Ljava/lang/String; = "broadcom.bluetooth.action.GATT_TEST_DISCONNECT"

.field private static final ACTION_GATT_TEST_DISCOVER:Ljava/lang/String; = "broadcom.bluetooth.action.GATT_TEST_DISCOVER"

.field private static final ACTION_GATT_TEST_ENABLE:Ljava/lang/String; = "broadcom.bluetooth.action.GATT_TEST_ENABLE"

.field private static final ACTION_GATT_TEST_USAGE:Ljava/lang/String; = "broadcom.bluetooth.action.GATT_TEST_USAGE"

.field private static final DEBUG_ADMIN:Z = true

.field private static final EXTRA_ADDRESS:Ljava/lang/String; = "address"

.field private static final EXTRA_AUTH_REQ:Ljava/lang/String; = "auth_req"

.field private static final EXTRA_EHANDLE:Ljava/lang/String; = "end"

.field private static final EXTRA_ENABLE:Ljava/lang/String; = "enable"

.field private static final EXTRA_INIT_KEY:Ljava/lang/String; = "init_key"

.field private static final EXTRA_IO_CAP:Ljava/lang/String; = "io_cap"

.field private static final EXTRA_MAX_KEY:Ljava/lang/String; = "max_key"

.field private static final EXTRA_RESP_KEY:Ljava/lang/String; = "resp_key"

.field private static final EXTRA_SHANDLE:Ljava/lang/String; = "start"

.field private static final EXTRA_TYPE:Ljava/lang/String; = "type"

.field private static final EXTRA_UUID:Ljava/lang/String; = "uuid"

.field private static final TAG:Ljava/lang/String; = "BtGatt.DebugUtils"


# direct methods
.method constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 57
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static getHandleExtra(Landroid/content/Intent;Ljava/lang/String;I)I
    .registers 8
    .parameter "intent"
    .parameter "extra"
    .parameter "default_value"

    #@0
    .prologue
    .line 176
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    #@3
    move-result-object v1

    #@4
    .line 177
    .local v1, extras:Landroid/os/Bundle;
    if-eqz v1, :cond_27

    #@6
    invoke-virtual {v1, p1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v2

    #@a
    .line 178
    .local v2, uuid:Ljava/lang/Object;
    :goto_a
    if-eqz v2, :cond_2b

    #@c
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    const-string v4, "java.lang.String"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_2b

    #@1c
    .line 181
    :try_start_1c
    invoke-virtual {v1, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    const/16 v4, 0x10

    #@22
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_25
    .catch Ljava/lang/NumberFormatException; {:try_start_1c .. :try_end_25} :catch_29

    #@25
    move-result p2

    #@26
    .line 187
    .end local p2
    :goto_26
    return p2

    #@27
    .line 177
    .end local v2           #uuid:Ljava/lang/Object;
    .restart local p2
    :cond_27
    const/4 v2, 0x0

    #@28
    goto :goto_a

    #@29
    .line 182
    .restart local v2       #uuid:Ljava/lang/Object;
    :catch_29
    move-exception v0

    #@2a
    .line 183
    .local v0, e:Ljava/lang/NumberFormatException;
    goto :goto_26

    #@2b
    .line 187
    .end local v0           #e:Ljava/lang/NumberFormatException;
    :cond_2b
    invoke-virtual {p0, p1, p2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2e
    move-result p2

    #@2f
    goto :goto_26
.end method

.method private static getUuidExtra(Landroid/content/Intent;)Ljava/util/UUID;
    .registers 5
    .parameter "intent"

    #@0
    .prologue
    .line 197
    const-string v1, "uuid"

    #@2
    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 198
    .local v0, uuidStr:Ljava/lang/String;
    if-eqz v0, :cond_1b

    #@8
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@b
    move-result v1

    #@c
    const/4 v2, 0x4

    #@d
    if-ne v1, v2, :cond_1b

    #@f
    .line 199
    const-string v1, "0000%s-0000-1000-8000-00805f9b34fb"

    #@11
    const/4 v2, 0x1

    #@12
    new-array v2, v2, [Ljava/lang/Object;

    #@14
    const/4 v3, 0x0

    #@15
    aput-object v0, v2, v3

    #@17
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@1a
    move-result-object v0

    #@1b
    .line 201
    :cond_1b
    if-eqz v0, :cond_22

    #@1d
    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    #@20
    move-result-object v1

    #@21
    :goto_21
    return-object v1

    #@22
    :cond_22
    const/4 v1, 0x0

    #@23
    goto :goto_21
.end method

.method static handleDebugAction(Lcom/broadcom/bt/service/gatt/GattService;Landroid/content/Intent;)Z
    .registers 26
    .parameter "svc"
    .parameter "intent"

    #@0
    .prologue
    .line 112
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v22

    #@4
    .line 113
    .local v22, action:Ljava/lang/String;
    const-string v1, "BtGatt.DebugUtils"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "handleDebugAction() action="

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    move-object/from16 v0, v22

    #@13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 119
    const-string v1, "broadcom.bluetooth.action.DEBUG_DUMP_CLIENTMAP"

    #@20
    move-object/from16 v0, v22

    #@22
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v1

    #@26
    if-eqz v1, :cond_31

    #@28
    .line 120
    move-object/from16 v0, p0

    #@2a
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/GattService;->mClientMap:Lcom/broadcom/bt/service/gatt/GattService$ClientMap;

    #@2c
    invoke-virtual {v1}, Lcom/broadcom/bt/service/gatt/GattService$ClientMap;->dump()V

    #@2f
    .line 167
    :goto_2f
    const/4 v1, 0x1

    #@30
    :goto_30
    return v1

    #@31
    .line 122
    :cond_31
    const-string v1, "broadcom.bluetooth.action.DEBUG_DUMP_SERVERMAP"

    #@33
    move-object/from16 v0, v22

    #@35
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v1

    #@39
    if-eqz v1, :cond_43

    #@3b
    .line 123
    move-object/from16 v0, p0

    #@3d
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/GattService;->mServerMap:Lcom/broadcom/bt/service/gatt/GattService$ServerMap;

    #@3f
    invoke-virtual {v1}, Lcom/broadcom/bt/service/gatt/GattService$ServerMap;->dump()V

    #@42
    goto :goto_2f

    #@43
    .line 125
    :cond_43
    const-string v1, "broadcom.bluetooth.action.DEBUG_DUMP_HANDLEMAP"

    #@45
    move-object/from16 v0, v22

    #@47
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v1

    #@4b
    if-eqz v1, :cond_55

    #@4d
    .line 126
    move-object/from16 v0, p0

    #@4f
    iget-object v1, v0, Lcom/broadcom/bt/service/gatt/GattService;->mHandleMap:Lcom/broadcom/bt/service/gatt/HandleMap;

    #@51
    invoke-virtual {v1}, Lcom/broadcom/bt/service/gatt/HandleMap;->dump()V

    #@54
    goto :goto_2f

    #@55
    .line 132
    :cond_55
    const-string v1, "broadcom.bluetooth.action.GATT_TEST_USAGE"

    #@57
    move-object/from16 v0, v22

    #@59
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5c
    move-result v1

    #@5d
    if-eqz v1, :cond_63

    #@5f
    .line 133
    invoke-static {}, Lcom/broadcom/bt/service/gatt/GattDebugUtils;->logUsageInfo()V

    #@62
    goto :goto_2f

    #@63
    .line 135
    :cond_63
    const-string v1, "broadcom.bluetooth.action.GATT_TEST_ENABLE"

    #@65
    move-object/from16 v0, v22

    #@67
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6a
    move-result v1

    #@6b
    if-eqz v1, :cond_88

    #@6d
    .line 136
    const-string v1, "enable"

    #@6f
    const/4 v2, 0x1

    #@70
    move-object/from16 v0, p1

    #@72
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@75
    move-result v23

    #@76
    .line 137
    .local v23, bEnable:Z
    const/4 v2, 0x1

    #@77
    const/4 v3, 0x0

    #@78
    const/4 v4, 0x0

    #@79
    if-eqz v23, :cond_86

    #@7b
    const/4 v5, 0x1

    #@7c
    :goto_7c
    const/4 v6, 0x0

    #@7d
    const/4 v7, 0x0

    #@7e
    const/4 v8, 0x0

    #@7f
    const/4 v9, 0x0

    #@80
    move-object/from16 v1, p0

    #@82
    invoke-virtual/range {v1 .. v9}, Lcom/broadcom/bt/service/gatt/GattService;->gattTestCommand(ILjava/util/UUID;Ljava/lang/String;IIIII)V

    #@85
    goto :goto_2f

    #@86
    :cond_86
    const/4 v5, 0x0

    #@87
    goto :goto_7c

    #@88
    .line 139
    .end local v23           #bEnable:Z
    :cond_88
    const-string v1, "broadcom.bluetooth.action.GATT_TEST_CONNECT"

    #@8a
    move-object/from16 v0, v22

    #@8c
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8f
    move-result v1

    #@90
    if-eqz v1, :cond_af

    #@92
    .line 140
    const-string v1, "address"

    #@94
    move-object/from16 v0, p1

    #@96
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@99
    move-result-object v4

    #@9a
    .line 141
    .local v4, address:Ljava/lang/String;
    const-string v1, "type"

    #@9c
    const/4 v2, 0x2

    #@9d
    move-object/from16 v0, p1

    #@9f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@a2
    move-result v5

    #@a3
    .line 142
    .local v5, type:I
    const/4 v2, 0x2

    #@a4
    const/4 v3, 0x0

    #@a5
    const/4 v6, 0x0

    #@a6
    const/4 v7, 0x0

    #@a7
    const/4 v8, 0x0

    #@a8
    const/4 v9, 0x0

    #@a9
    move-object/from16 v1, p0

    #@ab
    invoke-virtual/range {v1 .. v9}, Lcom/broadcom/bt/service/gatt/GattService;->gattTestCommand(ILjava/util/UUID;Ljava/lang/String;IIIII)V

    #@ae
    goto :goto_2f

    #@af
    .line 144
    .end local v4           #address:Ljava/lang/String;
    .end local v5           #type:I
    :cond_af
    const-string v1, "broadcom.bluetooth.action.GATT_TEST_DISCONNECT"

    #@b1
    move-object/from16 v0, v22

    #@b3
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b6
    move-result v1

    #@b7
    if-eqz v1, :cond_c8

    #@b9
    .line 145
    const/4 v7, 0x3

    #@ba
    const/4 v8, 0x0

    #@bb
    const/4 v9, 0x0

    #@bc
    const/4 v10, 0x0

    #@bd
    const/4 v11, 0x0

    #@be
    const/4 v12, 0x0

    #@bf
    const/4 v13, 0x0

    #@c0
    const/4 v14, 0x0

    #@c1
    move-object/from16 v6, p0

    #@c3
    invoke-virtual/range {v6 .. v14}, Lcom/broadcom/bt/service/gatt/GattService;->gattTestCommand(ILjava/util/UUID;Ljava/lang/String;IIIII)V

    #@c6
    goto/16 :goto_2f

    #@c8
    .line 147
    :cond_c8
    const-string v1, "broadcom.bluetooth.action.GATT_TEST_DISCOVER"

    #@ca
    move-object/from16 v0, v22

    #@cc
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@cf
    move-result v1

    #@d0
    if-eqz v1, :cond_ff

    #@d2
    .line 148
    invoke-static/range {p1 .. p1}, Lcom/broadcom/bt/service/gatt/GattDebugUtils;->getUuidExtra(Landroid/content/Intent;)Ljava/util/UUID;

    #@d5
    move-result-object v8

    #@d6
    .line 149
    .local v8, uuid:Ljava/util/UUID;
    const-string v1, "type"

    #@d8
    const/4 v2, 0x1

    #@d9
    move-object/from16 v0, p1

    #@db
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@de
    move-result v5

    #@df
    .line 150
    .restart local v5       #type:I
    const-string v1, "start"

    #@e1
    const/4 v2, 0x1

    #@e2
    move-object/from16 v0, p1

    #@e4
    invoke-static {v0, v1, v2}, Lcom/broadcom/bt/service/gatt/GattDebugUtils;->getHandleExtra(Landroid/content/Intent;Ljava/lang/String;I)I

    #@e7
    move-result v11

    #@e8
    .line 151
    .local v11, shdl:I
    const-string v1, "end"

    #@ea
    const v2, 0xffff

    #@ed
    move-object/from16 v0, p1

    #@ef
    invoke-static {v0, v1, v2}, Lcom/broadcom/bt/service/gatt/GattDebugUtils;->getHandleExtra(Landroid/content/Intent;Ljava/lang/String;I)I

    #@f2
    move-result v12

    #@f3
    .line 152
    .local v12, ehdl:I
    const/4 v7, 0x4

    #@f4
    const/4 v9, 0x0

    #@f5
    const/4 v13, 0x0

    #@f6
    const/4 v14, 0x0

    #@f7
    move-object/from16 v6, p0

    #@f9
    move v10, v5

    #@fa
    invoke-virtual/range {v6 .. v14}, Lcom/broadcom/bt/service/gatt/GattService;->gattTestCommand(ILjava/util/UUID;Ljava/lang/String;IIIII)V

    #@fd
    goto/16 :goto_2f

    #@ff
    .line 154
    .end local v5           #type:I
    .end local v8           #uuid:Ljava/util/UUID;
    .end local v11           #shdl:I
    .end local v12           #ehdl:I
    :cond_ff
    const-string v1, "broadcom.bluetooth.action.GATT_PAIRING_CONFIG"

    #@101
    move-object/from16 v0, v22

    #@103
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@106
    move-result v1

    #@107
    if-eqz v1, :cond_143

    #@109
    .line 155
    const-string v1, "auth_req"

    #@10b
    const/4 v2, 0x5

    #@10c
    move-object/from16 v0, p1

    #@10e
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@111
    move-result v17

    #@112
    .line 156
    .local v17, auth_req:I
    const-string v1, "io_cap"

    #@114
    const/4 v2, 0x4

    #@115
    move-object/from16 v0, p1

    #@117
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@11a
    move-result v18

    #@11b
    .line 157
    .local v18, io_cap:I
    const-string v1, "init_key"

    #@11d
    const/4 v2, 0x7

    #@11e
    move-object/from16 v0, p1

    #@120
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@123
    move-result v19

    #@124
    .line 158
    .local v19, init_key:I
    const-string v1, "resp_key"

    #@126
    const/4 v2, 0x7

    #@127
    move-object/from16 v0, p1

    #@129
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@12c
    move-result v20

    #@12d
    .line 159
    .local v20, resp_key:I
    const-string v1, "max_key"

    #@12f
    const/16 v2, 0x10

    #@131
    move-object/from16 v0, p1

    #@133
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@136
    move-result v21

    #@137
    .line 160
    .local v21, max_key:I
    const/16 v14, 0xf0

    #@139
    const/4 v15, 0x0

    #@13a
    const/16 v16, 0x0

    #@13c
    move-object/from16 v13, p0

    #@13e
    invoke-virtual/range {v13 .. v21}, Lcom/broadcom/bt/service/gatt/GattService;->gattTestCommand(ILjava/util/UUID;Ljava/lang/String;IIIII)V

    #@141
    goto/16 :goto_2f

    #@143
    .line 164
    .end local v17           #auth_req:I
    .end local v18           #io_cap:I
    .end local v19           #init_key:I
    .end local v20           #resp_key:I
    .end local v21           #max_key:I
    :cond_143
    const/4 v1, 0x0

    #@144
    goto/16 :goto_30
.end method

.method private static logUsageInfo()V
    .registers 3

    #@0
    .prologue
    .line 209
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 210
    .local v0, b:Ljava/lang/StringBuilder;
    const-string v1, "------------ GATT TEST ACTIONS  ----------------"

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 211
    const-string v1, "\nGATT_TEST_ENABLE"

    #@c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    .line 212
    const-string v1, "\n  [--ez enable <bool>] Enable or disable,"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    .line 213
    const-string v1, "\n                       defaults to true (enable).\n"

    #@16
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    .line 214
    const-string v1, "\nGATT_TEST_CONNECT"

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    .line 215
    const-string v1, "\n   --es address <bda>"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    .line 216
    const-string v1, "\n  [--ei type <type>]   Default is 2 (LE Only)\n"

    #@25
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    .line 217
    const-string v1, "\nGATT_TEST_DISCONNECT\n"

    #@2a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    .line 218
    const-string v1, "\nGATT_TEST_DISCOVER"

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    .line 219
    const-string v1, "\n  [--ei type <type>]   Possible values:"

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    .line 220
    const-string v1, "\n                         1 = Discover all services (default)"

    #@39
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    .line 221
    const-string v1, "\n                         2 = Discover services by UUID"

    #@3e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    .line 222
    const-string v1, "\n                         3 = Discover included services"

    #@43
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    .line 223
    const-string v1, "\n                         4 = Discover characteristics"

    #@48
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    .line 224
    const-string v1, "\n                         5 = Discover descriptors\n"

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    .line 225
    const-string v1, "\n  [--es uuid <uuid>]   Optional; Can be either full 128-bit"

    #@52
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    .line 226
    const-string v1, "\n                       UUID hex string, or 4 hex characters"

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    .line 227
    const-string v1, "\n                       for 16-bit UUIDs.\n"

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    .line 228
    const-string v1, "\n  [--ei start <hdl>]   Start of handle range (default 1)"

    #@61
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    .line 229
    const-string v1, "\n  [--ei end <hdl>]     End of handle range (default 65355)"

    #@66
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    .line 230
    const-string v1, "\n    or"

    #@6b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    .line 231
    const-string v1, "\n  [--es start <hdl>]   Start of handle range (hex format)"

    #@70
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    .line 232
    const-string v1, "\n  [--es end <hdl>]     End of handle range (hex format)\n"

    #@75
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    .line 233
    const-string v1, "\nGATT_PAIRING_CONFIG"

    #@7a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    .line 234
    const-string v1, "\n  [--ei auth_req]      Authentication flag (default 5)"

    #@7f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    .line 235
    const-string v1, "\n  [--ei io_cap]        IO capabilities (default 4)"

    #@84
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    .line 236
    const-string v1, "\n  [--ei init_key]      Initial key size (default 7)"

    #@89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    .line 237
    const-string v1, "\n  [--ei resp_key]      Response key size (default 7)"

    #@8e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    .line 238
    const-string v1, "\n  [--ei max_key]       Maximum key size (default 16)"

    #@93
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    .line 239
    const-string v1, "\n------------------------------------------------"

    #@98
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    .line 240
    const-string v1, "BtGatt.DebugUtils"

    #@9d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v2

    #@a1
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 241
    return-void
.end method
