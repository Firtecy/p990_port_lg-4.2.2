.class Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;
.super Ljava/lang/Object;
.source "ServiceDeclaration.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "Entry"
.end annotation


# instance fields
.field instance:I

.field permissions:I

.field properties:I

.field serviceHandle:I

.field serviceType:I

.field final synthetic this$0:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

.field type:B

.field uuid:Ljava/util/UUID;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/gatt/ServiceDeclaration;Ljava/util/UUID;I)V
    .registers 6
    .parameter
    .parameter "uuid"
    .parameter "permissions"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 89
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->this$0:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 66
    iput-byte v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->type:B

    #@8
    .line 67
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@b
    .line 68
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->instance:I

    #@d
    .line 69
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->permissions:I

    #@f
    .line 70
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->properties:I

    #@11
    .line 71
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->serviceType:I

    #@13
    .line 72
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->serviceHandle:I

    #@15
    .line 90
    const/4 v0, 0x3

    #@16
    iput-byte v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->type:B

    #@18
    .line 91
    iput-object p2, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@1a
    .line 92
    iput p3, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->permissions:I

    #@1c
    .line 93
    return-void
.end method

.method constructor <init>(Lcom/broadcom/bt/service/gatt/ServiceDeclaration;Ljava/util/UUID;II)V
    .registers 7
    .parameter
    .parameter "uuid"
    .parameter "serviceType"
    .parameter "instance"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 74
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->this$0:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 66
    iput-byte v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->type:B

    #@8
    .line 67
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@b
    .line 68
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->instance:I

    #@d
    .line 69
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->permissions:I

    #@f
    .line 70
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->properties:I

    #@11
    .line 71
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->serviceType:I

    #@13
    .line 72
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->serviceHandle:I

    #@15
    .line 75
    const/4 v0, 0x1

    #@16
    iput-byte v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->type:B

    #@18
    .line 76
    iput-object p2, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@1a
    .line 77
    iput p4, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->instance:I

    #@1c
    .line 78
    iput p3, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->serviceType:I

    #@1e
    .line 79
    return-void
.end method

.method constructor <init>(Lcom/broadcom/bt/service/gatt/ServiceDeclaration;Ljava/util/UUID;III)V
    .registers 8
    .parameter
    .parameter "uuid"
    .parameter "properties"
    .parameter "permissions"
    .parameter "instance"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 81
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->this$0:Lcom/broadcom/bt/service/gatt/ServiceDeclaration;

    #@3
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 66
    iput-byte v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->type:B

    #@8
    .line 67
    const/4 v0, 0x0

    #@9
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@b
    .line 68
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->instance:I

    #@d
    .line 69
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->permissions:I

    #@f
    .line 70
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->properties:I

    #@11
    .line 71
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->serviceType:I

    #@13
    .line 72
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->serviceHandle:I

    #@15
    .line 82
    const/4 v0, 0x2

    #@16
    iput-byte v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->type:B

    #@18
    .line 83
    iput-object p2, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->uuid:Ljava/util/UUID;

    #@1a
    .line 84
    iput p5, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->instance:I

    #@1c
    .line 85
    iput p4, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->permissions:I

    #@1e
    .line 86
    iput p3, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->properties:I

    #@20
    .line 87
    return-void
.end method
