.class Lcom/broadcom/bt/service/sap/SapService$1;
.super Landroid/os/Handler;
.source "SapService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/sap/SapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/sap/SapService;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/sap/SapService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 118
    iput-object p1, p0, Lcom/broadcom/bt/service/sap/SapService$1;->this$0:Lcom/broadcom/bt/service/sap/SapService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 122
    iget v4, p1, Landroid/os/Message;->what:I

    #@3
    packed-switch v4, :pswitch_data_72

    #@6
    .line 152
    :cond_6
    :goto_6
    return-void

    #@7
    .line 125
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@b
    .line 128
    .local v0, device:Landroid/bluetooth/BluetoothDevice;
    iget-object v4, p0, Lcom/broadcom/bt/service/sap/SapService$1;->this$0:Lcom/broadcom/bt/service/sap/SapService;

    #@d
    const/4 v5, 0x1

    #@e
    #calls: Lcom/broadcom/bt/service/sap/SapService;->disconnectSapNative(I)Z
    invoke-static {v4, v5}, Lcom/broadcom/bt/service/sap/SapService;->access$000(Lcom/broadcom/bt/service/sap/SapService;I)Z

    #@11
    move-result v4

    #@12
    if-nez v4, :cond_6

    #@14
    .line 130
    iget-object v4, p0, Lcom/broadcom/bt/service/sap/SapService$1;->this$0:Lcom/broadcom/bt/service/sap/SapService;

    #@16
    const/4 v5, 0x3

    #@17
    #calls: Lcom/broadcom/bt/service/sap/SapService;->broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;I)V
    invoke-static {v4, v0, v5}, Lcom/broadcom/bt/service/sap/SapService;->access$100(Lcom/broadcom/bt/service/sap/SapService;Landroid/bluetooth/BluetoothDevice;I)V

    #@1a
    .line 131
    iget-object v4, p0, Lcom/broadcom/bt/service/sap/SapService$1;->this$0:Lcom/broadcom/bt/service/sap/SapService;

    #@1c
    #calls: Lcom/broadcom/bt/service/sap/SapService;->broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;I)V
    invoke-static {v4, v0, v2}, Lcom/broadcom/bt/service/sap/SapService;->access$100(Lcom/broadcom/bt/service/sap/SapService;Landroid/bluetooth/BluetoothDevice;I)V

    #@1f
    goto :goto_6

    #@20
    .line 138
    .end local v0           #device:Landroid/bluetooth/BluetoothDevice;
    :pswitch_20
    iget-object v5, p0, Lcom/broadcom/bt/service/sap/SapService$1;->this$0:Lcom/broadcom/bt/service/sap/SapService;

    #@22
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24
    check-cast v4, [B

    #@26
    check-cast v4, [B

    #@28
    #calls: Lcom/broadcom/bt/service/sap/SapService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;
    invoke-static {v5, v4}, Lcom/broadcom/bt/service/sap/SapService;->access$200(Lcom/broadcom/bt/service/sap/SapService;[B)Landroid/bluetooth/BluetoothDevice;

    #@2b
    move-result-object v0

    #@2c
    .line 139
    .restart local v0       #device:Landroid/bluetooth/BluetoothDevice;
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@2e
    .line 140
    .local v1, halState:I
    iget-object v4, p0, Lcom/broadcom/bt/service/sap/SapService$1;->this$0:Lcom/broadcom/bt/service/sap/SapService;

    #@30
    #getter for: Lcom/broadcom/bt/service/sap/SapService;->mSapDevices:Ljava/util/Map;
    invoke-static {v4}, Lcom/broadcom/bt/service/sap/SapService;->access$300(Lcom/broadcom/bt/service/sap/SapService;)Ljava/util/Map;

    #@33
    move-result-object v4

    #@34
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@37
    move-result-object v3

    #@38
    check-cast v3, Ljava/lang/Integer;

    #@3a
    .line 141
    .local v3, prevStateInteger:Ljava/lang/Integer;
    if-nez v3, :cond_6c

    #@3c
    .line 144
    .local v2, prevState:I
    :goto_3c
    const-string v4, "SapService"

    #@3e
    new-instance v5, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v6, "MESSAGE_CONNECT_STATE_CHANGED newState:"

    #@45
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    #calls: Lcom/broadcom/bt/service/sap/SapService;->convertHalState(I)I
    invoke-static {v1}, Lcom/broadcom/bt/service/sap/SapService;->access$400(I)I

    #@4c
    move-result v6

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    const-string v6, ", prevState:"

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v5

    #@5f
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 146
    iget-object v4, p0, Lcom/broadcom/bt/service/sap/SapService$1;->this$0:Lcom/broadcom/bt/service/sap/SapService;

    #@64
    #calls: Lcom/broadcom/bt/service/sap/SapService;->convertHalState(I)I
    invoke-static {v1}, Lcom/broadcom/bt/service/sap/SapService;->access$400(I)I

    #@67
    move-result v5

    #@68
    #calls: Lcom/broadcom/bt/service/sap/SapService;->broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;I)V
    invoke-static {v4, v0, v5}, Lcom/broadcom/bt/service/sap/SapService;->access$100(Lcom/broadcom/bt/service/sap/SapService;Landroid/bluetooth/BluetoothDevice;I)V

    #@6b
    goto :goto_6

    #@6c
    .line 141
    .end local v2           #prevState:I
    :cond_6c
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    #@6f
    move-result v2

    #@70
    goto :goto_3c

    #@71
    .line 122
    nop

    #@72
    :pswitch_data_72
    .packed-switch 0x1
        :pswitch_7
        :pswitch_20
    .end packed-switch
.end method
