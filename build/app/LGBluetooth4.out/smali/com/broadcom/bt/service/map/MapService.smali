.class public Lcom/broadcom/bt/service/map/MapService;
.super Lcom/android/bluetooth/btservice/ProfileService;
.source "MapService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/map/MapService$1;,
        Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;,
        Lcom/broadcom/bt/service/map/MapService$EventReceiver;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final DEBUG_MOCK:Z = false

.field private static final STATUS_MSE_CANNOT_START:I = 0x1

.field private static final STATUS_MSE_CANNOT_STOP:I = 0x1

.field private static final STATUS_MSE_RETURN_CODE_SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "BtMap.MapService"

.field private static mDefaultTmpDir:Ljava/io/File;

.field private static mDefaultTmpPath:Ljava/lang/String;

.field static sService:Lcom/broadcom/bt/service/map/MapService;


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mEventReceiver:Lcom/broadcom/bt/service/map/MapService$EventReceiver;

.field mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

.field private mHasCurrentNotification:Z

.field private mIsStopping:Z

.field private mMapManager:Lcom/lge/bluetooth/LGBluetoothMapManager;

.field private mNativeAvailable:Z

.field private final mNotificationLock:Ljava/lang/Object;

.field private mPendingNotifications:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private mPrevState:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 132
    invoke-static {}, Lcom/broadcom/bt/service/map/MapService;->classInitNative()V

    #@3
    .line 135
    const/4 v0, 0x0

    #@4
    sput-object v0, Lcom/broadcom/bt/service/map/MapService;->sService:Lcom/broadcom/bt/service/map/MapService;

    #@6
    return-void
.end method

.method public constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 92
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/ProfileService;-><init>()V

    #@4
    .line 104
    new-instance v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@6
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;-><init>(Lcom/broadcom/bt/service/map/MapService;)V

    #@9
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@b
    .line 105
    new-instance v0, Lcom/broadcom/bt/service/map/MapService$EventReceiver;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-direct {v0, p0, v1}, Lcom/broadcom/bt/service/map/MapService$EventReceiver;-><init>(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/service/map/MapService$1;)V

    #@11
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mEventReceiver:Lcom/broadcom/bt/service/map/MapService$EventReceiver;

    #@13
    .line 106
    iput-boolean v2, p0, Lcom/broadcom/bt/service/map/MapService;->mIsStopping:Z

    #@15
    .line 107
    new-instance v0, Ljava/lang/Object;

    #@17
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    #@1a
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mNotificationLock:Ljava/lang/Object;

    #@1c
    .line 110
    new-instance v0, Ljava/util/LinkedList;

    #@1e
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@21
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mPendingNotifications:Ljava/util/LinkedList;

    #@23
    .line 1767
    iput v2, p0, Lcom/broadcom/bt/service/map/MapService;->mPrevState:I

    #@25
    return-void
.end method

.method static synthetic access$1000(Lcom/broadcom/bt/service/map/MapService;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mNotificationLock:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/broadcom/bt/service/map/MapService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapService;->hasPendingNotifications()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1200(Lcom/broadcom/bt/service/map/MapService;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/MapService;->enqueuePendingNotification(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$1302(Lcom/broadcom/bt/service/map/MapService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    iput-boolean p1, p0, Lcom/broadcom/bt/service/map/MapService;->mHasCurrentNotification:Z

    #@2
    return p1
.end method

.method static synthetic access$1400(Lcom/broadcom/bt/service/map/MapService;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 92
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapService;->dequeuePendingNotification()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$300(Lcom/broadcom/bt/service/map/MapService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/MapService;->profileStateChanged(I)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/MapService;->datasourceRegistrationChanged(Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/broadcom/bt/service/map/MapService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 92
    invoke-direct/range {p0 .. p5}, Lcom/broadcom/bt/service/map/MapService;->notifyDatasourceStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/broadcom/bt/service/map/MapService;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/MapService;->processEnableDatasource(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/broadcom/bt/service/map/MapService;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/MapService;->processDisableDatasource(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/broadcom/bt/service/map/MapService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/MapService;->onMSEInstanceStarted(II)V

    #@3
    return-void
.end method

.method static synthetic access$900(Lcom/broadcom/bt/service/map/MapService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/MapService;->onMSEInstanceStopped(II)V

    #@3
    return-void
.end method

.method private static native classInitNative()V
.end method

.method private native cleanupNative()V
.end method

.method private declared-synchronized datasourceRegistrationChanged(Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V
    .registers 5
    .parameter "ds"
    .parameter "isRegistered"

    #@0
    .prologue
    .line 365
    monitor-enter p0

    #@1
    if-nez p1, :cond_c

    #@3
    .line 366
    :try_start_3
    const-string v0, "BtMap.MapService"

    #@5
    const-string v1, "datasourceRegistrationChanged(): invalid datasource"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catchall {:try_start_3 .. :try_end_a} :catchall_25

    #@a
    .line 408
    :goto_a
    monitor-exit p0

    #@b
    return-void

    #@c
    .line 372
    :cond_c
    if-eqz p2, :cond_30

    #@e
    .line 373
    :try_start_e
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapService;->isAvailable()Z

    #@11
    move-result v0

    #@12
    if-eqz v0, :cond_28

    #@14
    invoke-virtual {p1}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->prepareToStartMse()Z

    #@17
    move-result v0

    #@18
    if-eqz v0, :cond_28

    #@1a
    .line 375
    const-string v0, "BtMap.MapService"

    #@1c
    const-string v1, "datasourceRegistrationChanged(): Starting MSE Instance..."

    #@1e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 377
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/MapService;->startMseInstance(Lcom/broadcom/bt/service/map/MapDatasourceContext;)V
    :try_end_24
    .catchall {:try_start_e .. :try_end_24} :catchall_25

    #@24
    goto :goto_a

    #@25
    .line 365
    :catchall_25
    move-exception v0

    #@26
    monitor-exit p0

    #@27
    throw v0

    #@28
    .line 380
    :cond_28
    :try_start_28
    const-string v0, "BtMap.MapService"

    #@2a
    const-string v1, "datasourceRegistrationChanged(): Service not available or not prepared to start MSE instance."

    #@2c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    goto :goto_a

    #@30
    .line 388
    :cond_30
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapService;->isAvailable()Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_47

    #@36
    invoke-virtual {p1}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->prepareToStopMse()Z

    #@39
    move-result v0

    #@3a
    if-eqz v0, :cond_47

    #@3c
    .line 390
    const-string v0, "BtMap.MapService"

    #@3e
    const-string v1, "datasourceRegistrationChanged(): stopping MSE Instance"

    #@40
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 392
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/MapService;->stopMseInstance(Lcom/broadcom/bt/service/map/MapDatasourceContext;)V

    #@46
    goto :goto_a

    #@47
    .line 395
    :cond_47
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapService;->isAvailable()Z

    #@4a
    move-result v0

    #@4b
    if-nez v0, :cond_55

    #@4d
    .line 396
    const-string v0, "BtMap.MapService"

    #@4f
    const-string v1, "datasourceRegistrationChanged(): service is stopping...Skipping MSE instance stop..."

    #@51
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    goto :goto_a

    #@55
    .line 399
    :cond_55
    const-string v0, "BtMap.MapService"

    #@57
    const-string v1, "datasourceRegistrationChanged(): not prepared to stop MSE instance."

    #@59
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5c
    .catchall {:try_start_28 .. :try_end_5c} :catchall_25

    #@5c
    goto :goto_a
.end method

.method private dequeuePendingNotification()Landroid/os/Message;
    .registers 3

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mPendingNotifications:Ljava/util/LinkedList;

    #@2
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    #@5
    move-result v0

    #@6
    if-lez v0, :cond_18

    #@8
    .line 117
    const-string v0, "BtMap.MapService"

    #@a
    const-string v1, "dequeuePendingNotification(): dequeuing pending notification"

    #@c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 118
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mPendingNotifications:Ljava/util/LinkedList;

    #@11
    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/os/Message;

    #@17
    .line 120
    :goto_17
    return-object v0

    #@18
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_17
.end method

.method private enqueuePendingNotification(Landroid/os/Message;)V
    .registers 3
    .parameter "m"

    #@0
    .prologue
    .line 112
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mPendingNotifications:Ljava/util/LinkedList;

    #@2
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@5
    .line 113
    return-void
.end method

.method private static fromNativeMessageByteArray([B)J
    .registers 9
    .parameter "messageHandle"

    #@0
    .prologue
    const/16 v7, 0x8

    #@2
    .line 793
    if-nez p0, :cond_e

    #@4
    .line 794
    const-string v3, "BtMap.MapService"

    #@6
    const-string v4, "fromNativeMessageByteArray(): bad messageHandle!!!!!!"

    #@8
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 795
    const-wide/16 v1, -0x1

    #@d
    .line 802
    :cond_d
    return-wide v1

    #@e
    .line 798
    :cond_e
    const-wide/16 v1, 0x0

    #@10
    .line 799
    .local v1, l:J
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    if-ge v0, v7, :cond_d

    #@13
    .line 800
    shl-long v3, v1, v7

    #@15
    aget-byte v5, p0, v0

    #@17
    int-to-long v5, v5

    #@18
    add-long v1, v3, v5

    #@1a
    .line 799
    add-int/lit8 v0, v0, 0x1

    #@1c
    goto :goto_11
.end method

.method public static getDefaultTmpDir()Ljava/io/File;
    .registers 1

    #@0
    .prologue
    .line 244
    sget-object v0, Lcom/broadcom/bt/service/map/MapService;->mDefaultTmpDir:Ljava/io/File;

    #@2
    return-object v0
.end method

.method public static getDefaultTmpPath()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 248
    sget-object v0, Lcom/broadcom/bt/service/map/MapService;->mDefaultTmpPath:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public static declared-synchronized getLocalBinder()Lcom/broadcom/bt/map/IBluetoothMap;
    .registers 2

    #@0
    .prologue
    .line 138
    const-class v1, Lcom/broadcom/bt/service/map/MapService;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Lcom/broadcom/bt/service/map/MapService;->sService:Lcom/broadcom/bt/service/map/MapService;

    #@5
    if-eqz v0, :cond_17

    #@7
    sget-object v0, Lcom/broadcom/bt/service/map/MapService;->sService:Lcom/broadcom/bt/service/map/MapService;

    #@9
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/MapService;->isAvailable()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_17

    #@f
    sget-object v0, Lcom/broadcom/bt/service/map/MapService;->sService:Lcom/broadcom/bt/service/map/MapService;

    #@11
    iget-object v0, v0, Lcom/broadcom/bt/service/map/MapService;->mBinder:Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

    #@13
    check-cast v0, Lcom/broadcom/bt/map/IBluetoothMap;
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_19

    #@15
    :goto_15
    monitor-exit v1

    #@16
    return-object v0

    #@17
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_15

    #@19
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1

    #@1b
    throw v0
.end method

.method private hasPendingNotifications()Z
    .registers 2

    #@0
    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHasCurrentNotification:Z

    #@2
    if-nez v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mPendingNotifications:Ljava/util/LinkedList;

    #@6
    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    #@9
    move-result v0

    #@a
    if-lez v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private native initTmpDirNative(Ljava/lang/String;Z)V
.end method

.method private native initializeNative()V
.end method

.method private notifyDatasourceStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 6
    .parameter "providerId"
    .parameter "providerDisplayName"
    .parameter "dsId"
    .parameter "dsDisplayName"
    .parameter "started"

    #@0
    .prologue
    .line 255
    return-void
.end method

.method private onAccessRequested(ILjava/lang/String;[BILjava/lang/String;[BZ)V
    .registers 12
    .parameter "sessionId"
    .parameter "bdname"
    .parameter "bdaddr"
    .parameter "operation"
    .parameter "path"
    .parameter "messageHandle"
    .parameter "deleteRequested"

    #@0
    .prologue
    .line 589
    const-string v0, "BtMap.MapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onAccessRequested(): sessionId="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", bdname="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", operation="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, ", path="

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    const-string v2, ",messageHandle="

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-static {p6}, Lcom/broadcom/bt/service/map/MapService;->fromNativeMessageByteArray([B)J

    #@38
    move-result-wide v2

    #@39
    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, ",deleteRequested="

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 597
    const-string v0, "BtMap.MapService"

    #@50
    const-string v1, "onAccessRequested(): auto-accepting request...."

    #@52
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 598
    const/4 v0, 0x1

    #@56
    invoke-direct {p0, p1, p4, p5, v0}, Lcom/broadcom/bt/service/map/MapService;->setAccessResponseNative(IILjava/lang/String;Z)V

    #@59
    .line 600
    return-void
.end method

.method private onClientConnected(IILjava/lang/String;[B)V
    .registers 12
    .parameter "mseInstanceId"
    .parameter "sessionId"
    .parameter "deviceName"
    .parameter "bdAddr"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 604
    invoke-static {p4}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 606
    .local v0, addr:Ljava/lang/String;
    const-string v3, "BtMap.MapService"

    #@7
    new-instance v4, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v5, "onClientConnected(): mseInstanceId="

    #@e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    const-string v5, ", sessionId="

    #@18
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v4

    #@20
    const-string v5, ", deviceName="

    #@22
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v4

    #@26
    invoke-static {p3}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    const-string v5, ", address="

    #@30
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v4

    #@3c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 610
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@42
    move-result-object v2

    #@43
    .line 611
    .local v2, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-nez v2, :cond_4d

    #@45
    .line 612
    const-string v3, "BtMap.MapService"

    #@47
    const-string v4, "onClientConnected(): DataSourceManager not available"

    #@49
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 631
    :cond_4c
    :goto_4c
    return-void

    #@4d
    .line 616
    :cond_4d
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v2, p1, p2, v3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->addSession(IILandroid/bluetooth/BluetoothDevice;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@58
    move-result-object v1

    #@59
    .line 618
    .local v1, c:Lcom/broadcom/bt/service/map/MapClientSession;
    if-eqz v1, :cond_62

    #@5b
    .line 619
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@5d
    iget-object v4, v1, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@5f
    invoke-virtual {v3, v4, v6}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendDatasourceClientConnectionChangeNotification(Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V

    #@62
    .line 622
    :cond_62
    const-string v3, "BtMap.MapService"

    #@64
    const-string v4, "[BTUI] ### MAP Connected ###"

    #@66
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 623
    const/4 v3, 0x2

    #@6a
    invoke-virtual {p0, v0, v3}, Lcom/broadcom/bt/service/map/MapService;->broadcastConnectionState(Ljava/lang/String;I)V

    #@6d
    .line 626
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapService;->mMapManager:Lcom/lge/bluetooth/LGBluetoothMapManager;

    #@6f
    if-eqz v3, :cond_4c

    #@71
    .line 627
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapService;->getContentResolver()Landroid/content/ContentResolver;

    #@74
    move-result-object v3

    #@75
    sget-object v4, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@77
    iget-object v5, p0, Lcom/broadcom/bt/service/map/MapService;->mMapManager:Lcom/lge/bluetooth/LGBluetoothMapManager;

    #@79
    invoke-virtual {v5}, Lcom/lge/bluetooth/LGBluetoothMapManager;->getDBObserver()Lcom/lge/bluetooth/LGBluetoothMsgDBObserver;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {v3, v4, v6, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@80
    .line 628
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapService;->mMapManager:Lcom/lge/bluetooth/LGBluetoothMapManager;

    #@82
    invoke-virtual {v3}, Lcom/lge/bluetooth/LGBluetoothMapManager;->getDBObserver()Lcom/lge/bluetooth/LGBluetoothMsgDBObserver;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v3, v6}, Lcom/lge/bluetooth/LGBluetoothMsgDBObserver;->update(Z)V

    #@89
    goto :goto_4c
.end method

.method private onClientDisconnected(IILjava/lang/String;[B)V
    .registers 13
    .parameter "mseInstanceId"
    .parameter "sessionId"
    .parameter "deviceName"
    .parameter "bdAddr"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 635
    invoke-static {p4}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 637
    .local v0, addr:Ljava/lang/String;
    const-string v4, "BtMap.MapService"

    #@7
    new-instance v5, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v6, "onClientDisconnected(): mseInstanceId="

    #@e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v5

    #@16
    const-string v6, ", sessionId="

    #@18
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v5

    #@1c
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v5

    #@20
    const-string v6, ", deviceName="

    #@22
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v5

    #@26
    invoke-static {p3}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v5

    #@2e
    const-string v6, ", address="

    #@30
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v5

    #@3c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 641
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@42
    move-result-object v2

    #@43
    .line 642
    .local v2, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-nez v2, :cond_4d

    #@45
    .line 643
    const-string v4, "BtMap.MapService"

    #@47
    const-string v5, "onClientDisconnected(): DataSourceManager not available"

    #@49
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 665
    :cond_4c
    :goto_4c
    return-void

    #@4d
    .line 646
    :cond_4d
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@54
    move-result-object v4

    #@55
    invoke-virtual {v2, p1, p2, v4}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->removeSession(IILandroid/bluetooth/BluetoothDevice;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@58
    move-result-object v3

    #@59
    .line 648
    .local v3, removed:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v3, :cond_7e

    #@5b
    .line 649
    const-string v4, "BtMap.MapService"

    #@5d
    const-string v5, "onClientDisconnected(): Session not found"

    #@5f
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    .line 657
    :cond_62
    :goto_62
    const-string v4, "BtMap.MapService"

    #@64
    const-string v5, "[BTUI] ### MAP Disconnected ###"

    #@66
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 658
    invoke-virtual {p0, v0, v7}, Lcom/broadcom/bt/service/map/MapService;->broadcastConnectionState(Ljava/lang/String;I)V

    #@6c
    .line 661
    iget-object v4, p0, Lcom/broadcom/bt/service/map/MapService;->mMapManager:Lcom/lge/bluetooth/LGBluetoothMapManager;

    #@6e
    if-eqz v4, :cond_4c

    #@70
    .line 662
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapService;->getContentResolver()Landroid/content/ContentResolver;

    #@73
    move-result-object v4

    #@74
    iget-object v5, p0, Lcom/broadcom/bt/service/map/MapService;->mMapManager:Lcom/lge/bluetooth/LGBluetoothMapManager;

    #@76
    invoke-virtual {v5}, Lcom/lge/bluetooth/LGBluetoothMapManager;->getDBObserver()Lcom/lge/bluetooth/LGBluetoothMsgDBObserver;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v4, v5}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@7d
    goto :goto_4c

    #@7e
    .line 651
    :cond_7e
    iget-object v1, v3, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@80
    .line 652
    .local v1, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-eqz v1, :cond_62

    #@82
    invoke-virtual {v1}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->hasActiveSession()Z

    #@85
    move-result v4

    #@86
    if-nez v4, :cond_62

    #@88
    .line 653
    iget-object v4, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@8a
    invoke-virtual {v4, v1, v7}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendDatasourceClientConnectionChangeNotification(Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V

    #@8d
    goto :goto_62
.end method

.method private onClientNotificationChanged(II[BZ)V
    .registers 9
    .parameter "instanceId"
    .parameter "sessionId"
    .parameter "bdAddr"
    .parameter "enable"

    #@0
    .prologue
    .line 892
    invoke-static {p3}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 894
    .local v0, addr:Ljava/lang/String;
    const-string v1, "BtMap.MapService"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "onClientNotificationChanged(): instanceId="

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, "sessionId="

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, ", bd_addr= "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, ", isRegistered="

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 897
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@3c
    invoke-virtual {v1, p2, v0, p4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->onClientNotificationChanged(ILjava/lang/String;Z)V

    #@3f
    .line 898
    return-void
.end method

.method private onGetFolderEntry(IILjava/lang/String;ZJ)V
    .registers 14
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "path"
    .parameter "isFirstEntry"
    .parameter "entryId"

    #@0
    .prologue
    .line 693
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move-wide v5, p5

    #@7
    invoke-virtual/range {v0 .. v6}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->onGetFolderEntry(IILjava/lang/String;ZJ)V

    #@a
    .line 694
    return-void
.end method

.method private onGetMessage(II[BZB)V
    .registers 13
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "messageHandle"
    .parameter "includeAttachments"
    .parameter "charSet"

    #@0
    .prologue
    .line 815
    invoke-static {p3}, Lcom/broadcom/bt/service/map/MapService;->fromNativeMessageByteArray([B)J

    #@3
    move-result-wide v3

    #@4
    .line 816
    .local v3, mid:J
    const-wide/16 v0, 0x0

    #@6
    cmp-long v0, v3, v0

    #@8
    if-gez v0, :cond_b

    #@a
    .line 820
    :goto_a
    return-void

    #@b
    .line 819
    :cond_b
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@d
    move v1, p1

    #@e
    move v2, p2

    #@f
    move v5, p4

    #@10
    move v6, p5

    #@11
    invoke-virtual/range {v0 .. v6}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->onGetMessage(IIJZB)V

    #@14
    goto :goto_a
.end method

.method private onGetMessageEntry(IILjava/lang/String;ZJLcom/broadcom/bt/map/MessageListFilter;J)V
    .registers 22
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "path"
    .parameter "isFirstEntry"
    .parameter "entryId"
    .parameter "filter"
    .parameter "parameterMask"

    #@0
    .prologue
    .line 751
    const-string v2, "BtMap.MapService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "onGetMessageEntry(): sessionId = "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, ", eventId = "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    const-string v4, ", path= "

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v4, ", isFirstEntry="

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    move/from16 v0, p4

    #@2d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, ", entryId="

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    move-wide/from16 v0, p5

    #@39
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, ", parameterMask="

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    move-wide/from16 v0, p8

    #@45
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@48
    move-result-object v3

    #@49
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 754
    if-eqz p7, :cond_55

    #@52
    .line 755
    invoke-virtual/range {p7 .. p7}, Lcom/broadcom/bt/map/MessageListFilter;->debugDump()V

    #@55
    .line 758
    :cond_55
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@57
    move v3, p1

    #@58
    move v4, p2

    #@59
    move-object v5, p3

    #@5a
    move/from16 v6, p4

    #@5c
    move-wide/from16 v7, p5

    #@5e
    move-object/from16 v9, p7

    #@60
    move-wide/from16 v10, p8

    #@62
    invoke-virtual/range {v2 .. v11}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->onGetMessageEntry(IILjava/lang/String;ZJLcom/broadcom/bt/map/MessageListFilter;J)V

    #@65
    .line 760
    return-void
.end method

.method private onGetMessageListInfo(IILjava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;J)V
    .registers 14
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "path"
    .parameter "filter"
    .parameter "parameterMask"

    #@0
    .prologue
    .line 718
    const-string v0, "BtMap.MapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onGetMessageListInfo(): sessionId = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", eventId = "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", path="

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 720
    if-eqz p4, :cond_31

    #@2e
    .line 721
    invoke-virtual {p4}, Lcom/broadcom/bt/map/MessageListFilter;->debugDump()V

    #@31
    .line 724
    :cond_31
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@33
    move v1, p1

    #@34
    move v2, p2

    #@35
    move-object v3, p3

    #@36
    move-object v4, p4

    #@37
    move-wide v5, p5

    #@38
    invoke-virtual/range {v0 .. v6}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->onGetMessageListInfo(IILjava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;J)V

    #@3b
    .line 725
    return-void
.end method

.method private onMSEInstanceStarted(II)V
    .registers 10
    .parameter "mseInstanceId"
    .parameter "status"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 472
    const-string v2, "BtMap.MapService"

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "onMSEInstanceStarted(): mseInstanceId="

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, ", status="

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 476
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@27
    move-result-object v1

    #@28
    .line 477
    .local v1, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-nez v1, :cond_32

    #@2a
    .line 478
    const-string v2, "BtMap.MapService"

    #@2c
    const-string v3, "onMSEInstanceStarted(): DataSourceManager not available"

    #@2e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 496
    :goto_31
    return-void

    #@32
    .line 481
    :cond_32
    invoke-virtual {v1, p1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getByMseInstanceId(I)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@35
    move-result-object v0

    #@36
    .line 482
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-nez v0, :cond_40

    #@38
    .line 483
    const-string v2, "BtMap.MapService"

    #@3a
    const-string v3, "onMSEInstanceStarted(): DataSource not found"

    #@3c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_31

    #@40
    .line 487
    :cond_40
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@42
    const/16 v3, 0x3e8

    #@44
    invoke-virtual {v2, v3, v0}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->removeMessages(ILjava/lang/Object;)V

    #@47
    .line 489
    if-nez p2, :cond_52

    #@49
    .line 490
    invoke-virtual {v0, v5}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->setMseInstanceStarted(Z)V

    #@4c
    .line 491
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4e
    invoke-virtual {v2, v0, v5, v6}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendDatasourceStateChangeNotifications(Lcom/broadcom/bt/service/map/MapDatasourceContext;ZZ)V

    #@51
    goto :goto_31

    #@52
    .line 493
    :cond_52
    invoke-virtual {v0, v6}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->setMseInstanceStarted(Z)V

    #@55
    .line 494
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@57
    invoke-virtual {v2, v0, v5, v5}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendDatasourceStateChangeNotifications(Lcom/broadcom/bt/service/map/MapDatasourceContext;ZZ)V

    #@5a
    goto :goto_31
.end method

.method private onMSEInstanceStopped(II)V
    .registers 9
    .parameter "mseInstanceId"
    .parameter "status"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 556
    const-string v2, "BtMap.MapService"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "onMSEInstanceStopped(): mseInstanceId="

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    const-string v4, ", status="

    #@14
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 560
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@26
    move-result-object v1

    #@27
    .line 561
    .local v1, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-nez v1, :cond_31

    #@29
    .line 562
    const-string v2, "BtMap.MapService"

    #@2b
    const-string v3, "onMSEInstanceStopped(): DataSourceManager not available"

    #@2d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 581
    :goto_30
    return-void

    #@31
    .line 565
    :cond_31
    invoke-virtual {v1, p1}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getByMseInstanceId(I)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@34
    move-result-object v0

    #@35
    .line 566
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-nez v0, :cond_3f

    #@37
    .line 567
    const-string v2, "BtMap.MapService"

    #@39
    const-string v3, "onMSEInstanceStopped(): DataSource not found"

    #@3b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    goto :goto_30

    #@3f
    .line 572
    :cond_3f
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@41
    const/16 v3, 0x3e9

    #@43
    invoke-virtual {v2, v3, v0}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->removeMessages(ILjava/lang/Object;)V

    #@46
    .line 574
    if-eqz p2, :cond_56

    #@48
    .line 575
    const-string v2, "BtMap.MapService"

    #@4a
    const-string v3, "onMSEInstanceStopped(): MSE stop failed!"

    #@4c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 576
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@51
    const/4 v3, 0x1

    #@52
    invoke-virtual {v2, v0, v5, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendDatasourceStateChangeNotifications(Lcom/broadcom/bt/service/map/MapDatasourceContext;ZZ)V

    #@55
    goto :goto_30

    #@56
    .line 578
    :cond_56
    invoke-virtual {v0, v5}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->setMseInstanceStarted(Z)V

    #@59
    .line 579
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@5b
    invoke-virtual {v2, v0, v5, v5}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendDatasourceStateChangeNotifications(Lcom/broadcom/bt/service/map/MapDatasourceContext;ZZ)V

    #@5e
    goto :goto_30
.end method

.method private onNotificationSent(I[BZ)V
    .registers 7
    .parameter "mseInstanceId"
    .parameter "bdAddr"
    .parameter "success"

    #@0
    .prologue
    .line 911
    const-string v0, "BtMap.MapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onNotificationSent: mseInstanceId="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", success="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 912
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@24
    const/16 v1, 0x58c

    #@26
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendEmptyMessage(I)Z

    #@29
    .line 913
    return-void
.end method

.method private onPushMessage(IILjava/lang/String;ZZI)V
    .registers 14
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "messageFilePath"
    .parameter "saveInSent"
    .parameter "retry"
    .parameter "charSet"

    #@0
    .prologue
    .line 840
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@2
    move v1, p1

    #@3
    move v2, p2

    #@4
    move-object v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    move v6, p6

    #@8
    invoke-virtual/range {v0 .. v6}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->onPushMessage(IILjava/lang/String;ZZI)V

    #@b
    .line 842
    return-void
.end method

.method private onSetFolder(ILjava/lang/String;)V
    .registers 6
    .parameter "sessionId"
    .parameter "path"

    #@0
    .prologue
    .line 683
    const-string v0, "BtMap.MapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onSetFolder(): sessionId="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", path="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 685
    invoke-static {p1, p2}, Lcom/broadcom/bt/service/map/MapSessionManager;->setCurrentPath(ILjava/lang/String;)V

    #@25
    .line 686
    return-void
.end method

.method private onSetMessageStatus(II[BBZ)V
    .registers 13
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "messageHandle"
    .parameter "messageStatusType"
    .parameter "isStatusSet"

    #@0
    .prologue
    .line 873
    invoke-static {p3}, Lcom/broadcom/bt/service/map/MapService;->fromNativeMessageByteArray([B)J

    #@3
    move-result-wide v3

    #@4
    .line 874
    .local v3, mid:J
    const-wide/16 v0, 0x0

    #@6
    cmp-long v0, v3, v0

    #@8
    if-gez v0, :cond_b

    #@a
    .line 878
    :goto_a
    return-void

    #@b
    .line 877
    :cond_b
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@d
    move v1, p1

    #@e
    move v2, p2

    #@f
    move v5, p4

    #@10
    move v6, p5

    #@11
    invoke-virtual/range {v0 .. v6}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->onSetMessageStatus(IIJBZ)V

    #@14
    goto :goto_a
.end method

.method private onUpdateInbox(I)V
    .registers 5
    .parameter "sessionId"

    #@0
    .prologue
    .line 669
    const-string v0, "BtMap.MapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "onUpdateInbox(): sessionId="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 671
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@1a
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->onUpdateInbox(I)V

    #@1d
    .line 673
    return-void
.end method

.method private processDisableDatasource(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "providerId"
    .parameter "dsId"

    #@0
    .prologue
    .line 505
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@3
    move-result-object v1

    #@4
    .line 506
    .local v1, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-nez v1, :cond_e

    #@6
    .line 507
    const-string v3, "BtMap.MapService"

    #@8
    const-string v4, "processDisableDatasource(): DataSourceManager not available"

    #@a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 528
    :goto_d
    return-void

    #@e
    .line 510
    :cond_e
    const/4 v3, 0x0

    #@f
    invoke-virtual {v1, p0, p1, p2, v3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->setEnabled(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    #@12
    move-result v2

    #@13
    .line 511
    .local v2, res:Z
    if-nez v2, :cond_1d

    #@15
    .line 512
    const-string v3, "BtMap.MapService"

    #@17
    const-string v4, "processDisableDatasource(): DataSourceManager cannot update enable state"

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_d

    #@1d
    .line 515
    :cond_1d
    invoke-virtual {v1, p1, p2}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@20
    move-result-object v0

    #@21
    .line 516
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-nez v0, :cond_2b

    #@23
    .line 517
    const-string v3, "BtMap.MapService"

    #@25
    const-string v4, "processDisableDatasource() Datasource not found"

    #@27
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_d

    #@2b
    .line 522
    :cond_2b
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->prepareToStopMse()Z

    #@2e
    move-result v3

    #@2f
    if-eqz v3, :cond_3c

    #@31
    .line 523
    const-string v3, "BtMap.MapService"

    #@33
    const-string v4, "processDisableDatasource: stopping MSE Instance..."

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 524
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/MapService;->stopMseInstance(Lcom/broadcom/bt/service/map/MapDatasourceContext;)V

    #@3b
    goto :goto_d

    #@3c
    .line 526
    :cond_3c
    const-string v3, "BtMap.MapService"

    #@3e
    const-string v4, "processDisableDatasource: not stopping MS Instance..."

    #@40
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_d
.end method

.method private processEnableDatasource(Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "providerId"
    .parameter "dsId"

    #@0
    .prologue
    .line 417
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@3
    move-result-object v1

    #@4
    .line 418
    .local v1, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-nez v1, :cond_e

    #@6
    .line 419
    const-string v3, "BtMap.MapService"

    #@8
    const-string v4, "processEnableDatasource(): DataSourceManager not available"

    #@a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 440
    :goto_d
    return-void

    #@e
    .line 422
    :cond_e
    const/4 v3, 0x1

    #@f
    invoke-virtual {v1, p0, p1, p2, v3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->setEnabled(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    #@12
    move-result v2

    #@13
    .line 423
    .local v2, res:Z
    if-nez v2, :cond_1d

    #@15
    .line 424
    const-string v3, "BtMap.MapService"

    #@17
    const-string v4, "processEnableDatasource(): DataSourceManager cannot update enable state"

    #@19
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    goto :goto_d

    #@1d
    .line 427
    :cond_1d
    invoke-virtual {v1, p1, p2}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@20
    move-result-object v0

    #@21
    .line 428
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    if-nez v0, :cond_2b

    #@23
    .line 429
    const-string v3, "BtMap.MapService"

    #@25
    const-string v4, "processEnableDatasource() Datasource not found"

    #@27
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_d

    #@2b
    .line 434
    :cond_2b
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->prepareToStartMse()Z

    #@2e
    move-result v3

    #@2f
    if-eqz v3, :cond_3c

    #@31
    .line 435
    const-string v3, "BtMap.MapService"

    #@33
    const-string v4, "processEnableDatasource: starting MSE Instance..."

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 436
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/MapService;->startMseInstance(Lcom/broadcom/bt/service/map/MapDatasourceContext;)V

    #@3b
    goto :goto_d

    #@3c
    .line 438
    :cond_3c
    const-string v3, "BtMap.MapService"

    #@3e
    const-string v4, "processEnableDatasource: not starting MS Instance..."

    #@40
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_d
.end method

.method private profileStateChanged(I)V
    .registers 5
    .parameter "state"

    #@0
    .prologue
    .line 258
    const-string v0, "BtMap.MapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "profileStateChanged(): state = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 259
    const/16 v0, 0xc

    #@1a
    if-ne v0, p1, :cond_27

    #@1c
    .line 260
    new-instance v0, Landroid/content/Intent;

    #@1e
    const-string v1, "broadcom.bluetooth.map.START"

    #@20
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@23
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/map/MapService;->sendBroadcast(Landroid/content/Intent;)V

    #@26
    .line 267
    :goto_26
    return-void

    #@27
    .line 261
    :cond_27
    const/16 v0, 0xa

    #@29
    if-ne v0, p1, :cond_36

    #@2b
    .line 262
    new-instance v0, Landroid/content/Intent;

    #@2d
    const-string v1, "broadcom.bluetooth.map.STOP"

    #@2f
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@32
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/map/MapService;->sendBroadcast(Landroid/content/Intent;)V

    #@35
    goto :goto_26

    #@36
    .line 264
    :cond_36
    const-string v0, "BtMap.MapService"

    #@38
    new-instance v1, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v2, "profileStateChanged() Not sending broadcast unhandled state:"

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    goto :goto_26
.end method

.method private synchronized native declared-synchronized sendNotificationNative(I[BBBLjava/lang/String;Ljava/lang/String;)V
.end method

.method private native setAccessResponseNative(IILjava/lang/String;Z)V
.end method

.method private native setGetFolderEntryResultNative(IIZIILjava/lang/String;Ljava/lang/String;Z)V
.end method

.method private native setGetMessageEntryResultNative(IIZI[BBIIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZB)V
.end method

.method private native setGetMessageListInfoResultNative(IIILjava/lang/String;Z)V
.end method

.method private native setGetMessageResultNative(IIZLjava/lang/String;)V
.end method

.method private native setMessageDeletedResultNative(IIZ)V
.end method

.method private native setPushMessageResultNative(IIZ[B)V
.end method

.method private startMseInstance(Lcom/broadcom/bt/service/map/MapDatasourceContext;)V
    .registers 9
    .parameter "ds"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/16 v5, 0x3e8

    #@3
    .line 444
    const-string v1, "BtMap.MapService"

    #@5
    const-string v2, "startMseInstance()"

    #@7
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 446
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@c
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@e
    invoke-virtual {v2, v5, p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@11
    move-result-object v2

    #@12
    const-wide/16 v3, 0xbb8

    #@14
    invoke-virtual {v1, v2, v3, v4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@17
    .line 454
    :try_start_17
    iget-byte v1, p1, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsType:B

    #@19
    iget v2, p1, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@1b
    iget-object v3, p1, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@1d
    const-string v4, "root"

    #@1f
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/broadcom/bt/service/map/MapService;->startMseInstanceNative(BILjava/lang/String;Ljava/lang/String;)Z

    #@22
    move-result v1

    #@23
    if-nez v1, :cond_33

    #@25
    .line 456
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@27
    const/16 v2, 0x3e8

    #@29
    invoke-virtual {v1, v2, p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->removeMessages(ILjava/lang/Object;)V

    #@2c
    .line 457
    const-string v1, "BtMap.MapService"

    #@2e
    const-string v2, "startMseInstance(): Error while starting datasource "

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_33
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_33} :catch_34

    #@33
    .line 465
    :cond_33
    :goto_33
    return-void

    #@34
    .line 460
    :catch_34
    move-exception v0

    #@35
    .line 461
    .local v0, t:Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@37
    invoke-virtual {v1, v5, p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->removeMessages(ILjava/lang/Object;)V

    #@3a
    .line 462
    const-string v1, "BtMap.MapService"

    #@3c
    const-string v2, "startMseInstance(): Error while starting datasource "

    #@3e
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@41
    .line 463
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@43
    invoke-virtual {v1, p1, v6, v6}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendDatasourceStateChangeNotifications(Lcom/broadcom/bt/service/map/MapDatasourceContext;ZZ)V

    #@46
    goto :goto_33
.end method

.method private native startMseInstanceNative(BILjava/lang/String;Ljava/lang/String;)Z
.end method

.method private stopMseInstance(Lcom/broadcom/bt/service/map/MapDatasourceContext;)V
    .registers 8
    .parameter "ds"

    #@0
    .prologue
    const/16 v5, 0x3e9

    #@2
    .line 531
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@6
    invoke-virtual {v2, v5, p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@9
    move-result-object v2

    #@a
    const-wide/16 v3, 0xbb8

    #@c
    invoke-virtual {v1, v2, v3, v4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@f
    .line 539
    :try_start_f
    iget v1, p1, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@11
    invoke-direct {p0, v1}, Lcom/broadcom/bt/service/map/MapService;->stopMseInstanceNative(I)Z

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_25

    #@17
    .line 540
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@19
    const/16 v2, 0x3e9

    #@1b
    invoke-virtual {v1, v2, p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->removeMessages(ILjava/lang/Object;)V

    #@1e
    .line 541
    const-string v1, "BtMap.MapService"

    #@20
    const-string v2, "stopMseInstance(): Error while starting datasource "

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_25
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_25} :catch_26

    #@25
    .line 549
    :cond_25
    :goto_25
    return-void

    #@26
    .line 545
    :catch_26
    move-exception v0

    #@27
    .line 546
    .local v0, t:Ljava/lang/Throwable;
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@29
    invoke-virtual {v1, v5, p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->removeMessages(ILjava/lang/Object;)V

    #@2c
    .line 547
    const-string v1, "BtMap.MapService"

    #@2e
    const-string v2, "stopMseInstance(): Error while stopping datasource "

    #@30
    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    goto :goto_25
.end method

.method private native stopMseInstanceNative(I)Z
.end method

.method private static toNativeMessageByteArray(J)[B
    .registers 8
    .parameter "messageHandle"

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    .line 784
    new-array v0, v5, [B

    #@4
    .line 785
    .local v0, b:[B
    const/4 v1, 0x0

    #@5
    .local v1, i:I
    :goto_5
    array-length v2, v0

    #@6
    if-ge v1, v2, :cond_17

    #@8
    .line 786
    array-length v2, v0

    #@9
    add-int/lit8 v2, v2, -0x1

    #@b
    sub-int/2addr v2, v1

    #@c
    const-wide/16 v3, 0xff

    #@e
    and-long/2addr v3, p0

    #@f
    long-to-int v3, v3

    #@10
    int-to-byte v3, v3

    #@11
    aput-byte v3, v0, v2

    #@13
    .line 787
    shr-long/2addr p0, v5

    #@14
    .line 785
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_5

    #@17
    .line 789
    :cond_17
    return-object v0
.end method


# virtual methods
.method broadcastConnectionState(Ljava/lang/String;I)V
    .registers 7
    .parameter "addr"
    .parameter "newState"

    #@0
    .prologue
    .line 1770
    if-eqz p1, :cond_26

    #@2
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@4
    if-eqz v2, :cond_26

    #@6
    .line 1771
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@8
    invoke-virtual {v2, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@b
    move-result-object v1

    #@c
    .line 1772
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    if-eqz v1, :cond_1e

    #@e
    .line 1773
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@11
    move-result-object v0

    #@12
    .line 1774
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_1b

    #@14
    .line 1775
    const/16 v2, 0x8

    #@16
    iget v3, p0, Lcom/broadcom/bt/service/map/MapService;->mPrevState:I

    #@18
    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->onProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V

    #@1b
    .line 1777
    :cond_1b
    iput p2, p0, Lcom/broadcom/bt/service/map/MapService;->mPrevState:I

    #@1d
    .line 1784
    .end local v0           #adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    :goto_1d
    return-void

    #@1e
    .line 1779
    .restart local v1       #device:Landroid/bluetooth/BluetoothDevice;
    :cond_1e
    const-string v2, "BtMap.MapService"

    #@20
    const-string v3, "[BTUI] broadcastConnectionState : device is null"

    #@22
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_1d

    #@26
    .line 1782
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    :cond_26
    const-string v2, "BtMap.MapService"

    #@28
    const-string v3, "[BTUI] broadcastConnectionState : addr is null"

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_1d
.end method

.method protected cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 220
    const-string v0, "Cleanup Bluetooth MapService"

    #@2
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/map/MapService;->log(Ljava/lang/String;)V

    #@5
    .line 222
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/MapService;->mNativeAvailable:Z

    #@7
    if-eqz v0, :cond_f

    #@9
    .line 223
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapService;->cleanupNative()V

    #@c
    .line 224
    const/4 v0, 0x0

    #@d
    iput-boolean v0, p0, Lcom/broadcom/bt/service/map/MapService;->mNativeAvailable:Z

    #@f
    .line 226
    :cond_f
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->cleanup()V

    #@12
    .line 227
    const/4 v0, 0x1

    #@13
    return v0
.end method

.method protected getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 142
    const-string v0, "BtMap.MapService"

    #@2
    return-object v0
.end method

.method protected initBinder()Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;
    .registers 2

    #@0
    .prologue
    .line 150
    new-instance v0, Lcom/broadcom/bt/service/map/MapServiceBinder;

    #@2
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/map/MapServiceBinder;-><init>(Lcom/broadcom/bt/service/map/MapService;)V

    #@5
    return-object v0
.end method

.method public isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 146
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/MapService;->mIsStopping:Z

    #@2
    if-nez v0, :cond_c

    #@4
    invoke-super {p0}, Lcom/android/bluetooth/btservice/ProfileService;->isAvailable()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_c

    #@a
    const/4 v0, 0x1

    #@b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 5
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 211
    invoke-static {p0, p1}, Lcom/broadcom/bt/service/map/MapDebugUtils;->handleDebugAction(Lcom/broadcom/bt/service/map/MapService;Landroid/content/Intent;)Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_8

    #@6
    .line 212
    const/4 v0, 0x2

    #@7
    .line 215
    :goto_7
    return v0

    #@8
    :cond_8
    invoke-super {p0, p1, p2, p3}, Lcom/android/bluetooth/btservice/ProfileService;->onStartCommand(Landroid/content/Intent;II)I

    #@b
    move-result v0

    #@c
    goto :goto_7
.end method

.method registerDatasource(Ljava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;)Z
    .registers 26
    .parameter "providerId"
    .parameter "providerDisplayName"
    .parameter "dsType"
    .parameter "dsId"
    .parameter "dsDisplayName"
    .parameter "supportsMessageFilter"
    .parameter "supportsMessageOffsetBrowsing"
    .parameter "virtualFolderMappings"
    .parameter "callback"

    #@0
    .prologue
    .line 287
    const-string v4, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v5, "Need BLUETOOTH ADMIN permission"

    #@4
    move-object/from16 v0, p0

    #@6
    invoke-virtual {v0, v4, v5}, Lcom/broadcom/bt/service/map/MapService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 290
    const-string v4, "BtMap.MapService"

    #@b
    new-instance v5, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v6, "registerDatasource(): providerId="

    #@12
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v5

    #@16
    invoke-static/range {p1 .. p1}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    const-string v6, ", providerDisplayName="

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-static/range {p2 .. p2}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v5

    #@2c
    const-string v6, ", dsType="

    #@2e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v5

    #@32
    move/from16 v0, p3

    #@34
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v5

    #@38
    const-string v6, ",dsId="

    #@3a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v5

    #@3e
    invoke-static/range {p4 .. p4}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v6

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    const-string v6, ", dsDisplayName="

    #@48
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v5

    #@4c
    invoke-static/range {p5 .. p5}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@4f
    move-result-object v6

    #@50
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v5

    #@54
    const-string v6, ", supportsMessageFilter="

    #@56
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v5

    #@5a
    move/from16 v0, p6

    #@5c
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v5

    #@60
    const-string v6, ", supportsMessageOffsetBrowsing="

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    move/from16 v0, p7

    #@68
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    const-string v6, ", callback="

    #@6e
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v5

    #@72
    move-object/from16 v0, p9

    #@74
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v5

    #@7c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 297
    const-string v4, "BtMap.MapService"

    #@81
    const-string v5, "virtualFolderMappings = "

    #@83
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 298
    if-eqz p8, :cond_9d

    #@88
    move-object/from16 v0, p8

    #@8a
    array-length v4, v0

    #@8b
    if-lez v4, :cond_9d

    #@8d
    .line 299
    const/4 v14, 0x0

    #@8e
    .local v14, i:I
    :goto_8e
    move-object/from16 v0, p8

    #@90
    array-length v4, v0

    #@91
    if-ge v14, v4, :cond_9d

    #@93
    .line 300
    const-string v4, "BtMap.MapService"

    #@95
    aget-object v5, p8, v14

    #@97
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    .line 299
    add-int/lit8 v14, v14, 0x1

    #@9c
    goto :goto_8e

    #@9d
    .line 306
    .end local v14           #i:I
    :cond_9d
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@a0
    move-result-object v13

    #@a1
    .line 307
    .local v13, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-nez v13, :cond_ac

    #@a3
    .line 308
    const-string v4, "BtMap.MapService"

    #@a5
    const-string v5, "registerDatasource(): DataSourceManager not available"

    #@a7
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 309
    const/4 v15, 0x0

    #@ab
    .line 327
    :cond_ab
    :goto_ab
    return v15

    #@ac
    .line 311
    :cond_ac
    move-object/from16 v0, p1

    #@ae
    move/from16 v1, p3

    #@b0
    move-object/from16 v2, p4

    #@b2
    invoke-virtual {v13, v0, v1, v2}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->isRegistered(Ljava/lang/String;BLjava/lang/String;)Z

    #@b5
    move-result v4

    #@b6
    if-eqz v4, :cond_c1

    #@b8
    .line 312
    const-string v4, "BtMap.MapService"

    #@ba
    const-string v5, "registerDatasource(): DataSource already registered!"

    #@bc
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bf
    .line 313
    const/4 v15, 0x0

    #@c0
    goto :goto_ab

    #@c1
    .line 317
    :cond_c1
    new-instance v3, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@c3
    move-object/from16 v4, p1

    #@c5
    move-object/from16 v5, p2

    #@c7
    move/from16 v6, p3

    #@c9
    move-object/from16 v7, p4

    #@cb
    move-object/from16 v8, p5

    #@cd
    move/from16 v9, p6

    #@cf
    move/from16 v10, p7

    #@d1
    move-object/from16 v11, p8

    #@d3
    move-object/from16 v12, p9

    #@d5
    invoke-direct/range {v3 .. v12}, Lcom/broadcom/bt/service/map/MapDatasourceContext;-><init>(Ljava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;)V

    #@d8
    .line 320
    .local v3, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    move-object/from16 v0, p0

    #@da
    invoke-virtual {v13, v0, v3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->register(Landroid/content/Context;Lcom/broadcom/bt/service/map/MapDatasourceContext;)Z

    #@dd
    move-result v15

    #@de
    .line 322
    .local v15, res:Z
    const-string v4, "BtMap.MapService"

    #@e0
    new-instance v5, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v6, "registerDatasource(): result = "

    #@e7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v5

    #@eb
    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v5

    #@ef
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v5

    #@f3
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f6
    .line 324
    if-eqz v15, :cond_ab

    #@f8
    .line 325
    move-object/from16 v0, p0

    #@fa
    iget-object v4, v0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@fc
    const/4 v5, 0x1

    #@fd
    invoke-virtual {v4, v3, v5}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessageDatasourceRegistrationChanged(Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V

    #@100
    goto :goto_ab
.end method

.method sendNotification(IJBBLjava/lang/String;Ljava/lang/String;)V
    .registers 15
    .parameter "mseInstanceId"
    .parameter "messageHandle"
    .parameter "messageType"
    .parameter "notificationType"
    .parameter "virtualFolderPath"
    .parameter "virtualOldFolderPath"

    #@0
    .prologue
    .line 902
    invoke-static {p2, p3}, Lcom/broadcom/bt/service/map/MapService;->toNativeMessageByteArray(J)[B

    #@3
    move-result-object v2

    #@4
    move-object v0, p0

    #@5
    move v1, p1

    #@6
    move v3, p4

    #@7
    move v4, p5

    #@8
    move-object v5, p6

    #@9
    move-object v6, p7

    #@a
    invoke-direct/range {v0 .. v6}, Lcom/broadcom/bt/service/map/MapService;->sendNotificationNative(I[BBBLjava/lang/String;Ljava/lang/String;)V

    #@d
    .line 904
    return-void
.end method

.method setGetFolderEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIILjava/lang/String;Ljava/lang/String;Z)V
    .registers 18
    .parameter "cancelTimeout"
    .parameter "requestId"
    .parameter "hasEntry"
    .parameter "entryId"
    .parameter "fileSize"
    .parameter "folderName"
    .parameter "createTime"
    .parameter "isReadOnly"

    #@0
    .prologue
    .line 702
    const-string v0, "BtMap.MapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setGetFolderEntryResult(): cancel timeout = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", requestId="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 704
    if-eqz p1, :cond_2b

    #@24
    .line 705
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@26
    const/16 v1, 0x3ea

    #@28
    #calls: Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->stopTimeoutTimer(ILcom/broadcom/bt/map/RequestId;)V
    invoke-static {v0, v1, p2}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->access$200(Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;ILcom/broadcom/bt/map/RequestId;)V

    #@2b
    .line 707
    :cond_2b
    iget v1, p2, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@2d
    iget v2, p2, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@2f
    move-object v0, p0

    #@30
    move v3, p3

    #@31
    move v4, p4

    #@32
    move v5, p5

    #@33
    move-object v6, p6

    #@34
    move-object/from16 v7, p7

    #@36
    move/from16 v8, p8

    #@38
    invoke-direct/range {v0 .. v8}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderEntryResultNative(IIZIILjava/lang/String;Ljava/lang/String;Z)V

    #@3b
    .line 709
    return-void
.end method

.method setGetFolderNoEntryResult(ZLcom/broadcom/bt/map/RequestId;)V
    .registers 12
    .parameter "cancelTimeout"
    .parameter "requestId"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 697
    const/4 v4, -0x1

    #@2
    const-string v6, ""

    #@4
    const-string v7, ""

    #@6
    move-object v0, p0

    #@7
    move v1, p1

    #@8
    move-object v2, p2

    #@9
    move v5, v3

    #@a
    move v8, v3

    #@b
    invoke-virtual/range {v0 .. v8}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIILjava/lang/String;Ljava/lang/String;Z)V

    #@e
    .line 698
    return-void
.end method

.method setGetMessageEntryNoResult(ZLcom/broadcom/bt/map/RequestId;)V
    .registers 28
    .parameter "cancelTimeout"
    .parameter "requestId"

    #@0
    .prologue
    .line 763
    const/4 v3, 0x0

    #@1
    const/4 v4, -0x1

    #@2
    const-wide/16 v5, -0x1

    #@4
    const/4 v7, -0x1

    #@5
    const/4 v8, 0x0

    #@6
    const/4 v9, 0x0

    #@7
    const-wide/16 v10, 0x0

    #@9
    const-string v12, ""

    #@b
    const-string v13, ""

    #@d
    const-string v14, ""

    #@f
    const-string v15, ""

    #@11
    const-string v16, ""

    #@13
    const-string v17, ""

    #@15
    const-string v18, ""

    #@17
    const/16 v19, 0x0

    #@19
    const/16 v20, 0x0

    #@1b
    const/16 v21, 0x0

    #@1d
    const/16 v22, 0x0

    #@1f
    const/16 v23, 0x0

    #@21
    const/16 v24, 0x0

    #@23
    move-object/from16 v0, p0

    #@25
    move/from16 v1, p1

    #@27
    move-object/from16 v2, p2

    #@29
    invoke-virtual/range {v0 .. v24}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIJBIIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZB)V

    #@2c
    .line 765
    return-void
.end method

.method setGetMessageEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIJBIIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZB)V
    .registers 50
    .parameter "cancelTimeout"
    .parameter "requestId"
    .parameter "hasEntry"
    .parameter "nextEntryId"
    .parameter "messageHandle"
    .parameter "messageType"
    .parameter "messageSize"
    .parameter "attachmentSize"
    .parameter "parameterMask"
    .parameter "senderName"
    .parameter "senderAddress"
    .parameter "recipientName"
    .parameter "recipientAddress"
    .parameter "replyToAddress"
    .parameter "subject"
    .parameter "dateTime"
    .parameter "hasText"
    .parameter "isPriority"
    .parameter "isRead"
    .parameter "isSent"
    .parameter "isProtected"
    .parameter "receiveStatus"

    #@0
    .prologue
    .line 773
    if-eqz p1, :cond_d

    #@2
    .line 774
    move-object/from16 v0, p0

    #@4
    iget-object v1, v0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@6
    const/16 v2, 0x3ec

    #@8
    move-object/from16 v0, p2

    #@a
    #calls: Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->stopTimeoutTimer(ILcom/broadcom/bt/map/RequestId;)V
    invoke-static {v1, v2, v0}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->access$200(Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;ILcom/broadcom/bt/map/RequestId;)V

    #@d
    .line 776
    :cond_d
    move-object/from16 v0, p2

    #@f
    iget v2, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@11
    move-object/from16 v0, p2

    #@13
    iget v3, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@15
    invoke-static/range {p5 .. p6}, Lcom/broadcom/bt/service/map/MapService;->toNativeMessageByteArray(J)[B

    #@18
    move-result-object v6

    #@19
    move-object/from16 v1, p0

    #@1b
    move/from16 v4, p3

    #@1d
    move/from16 v5, p4

    #@1f
    move/from16 v7, p7

    #@21
    move/from16 v8, p8

    #@23
    move/from16 v9, p9

    #@25
    move-wide/from16 v10, p10

    #@27
    move-object/from16 v12, p12

    #@29
    move-object/from16 v13, p13

    #@2b
    move-object/from16 v14, p14

    #@2d
    move-object/from16 v15, p15

    #@2f
    move-object/from16 v16, p16

    #@31
    move-object/from16 v17, p17

    #@33
    move-object/from16 v18, p18

    #@35
    move/from16 v19, p19

    #@37
    move/from16 v20, p20

    #@39
    move/from16 v21, p21

    #@3b
    move/from16 v22, p22

    #@3d
    move/from16 v23, p23

    #@3f
    move/from16 v24, p24

    #@41
    invoke-direct/range {v1 .. v24}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryResultNative(IIZI[BBIIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZB)V

    #@44
    .line 781
    return-void
.end method

.method setGetMessageListInfoNoResult(ZLcom/broadcom/bt/map/RequestId;)V
    .registers 9
    .parameter "cancelTimeout"
    .parameter "requestId"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 728
    const-string v4, ""

    #@3
    move-object v0, p0

    #@4
    move v1, p1

    #@5
    move-object v2, p2

    #@6
    move v5, v3

    #@7
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageListInfoResult(ZLcom/broadcom/bt/map/RequestId;ILjava/lang/String;Z)V

    #@a
    .line 729
    return-void
.end method

.method setGetMessageListInfoResult(ZLcom/broadcom/bt/map/RequestId;ILjava/lang/String;Z)V
    .registers 12
    .parameter "cancelTimeout"
    .parameter "requestId"
    .parameter "listSize"
    .parameter "mseTime"
    .parameter "hasNewMessages"

    #@0
    .prologue
    .line 733
    const-string v0, "BtMap.MapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setGetMessageListInfoResult(): requestId= "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", sessionId = "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    iget v2, p2, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, ", eventId = "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    iget v2, p2, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    const-string v2, ", listSize= "

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    const-string v2, ", time="

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-static {p4}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v1

    #@41
    const-string v2, ", hasNewMessages="

    #@43
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v1

    #@4f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 737
    if-eqz p1, :cond_5b

    #@54
    .line 738
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@56
    const/16 v1, 0x3eb

    #@58
    #calls: Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->stopTimeoutTimer(ILcom/broadcom/bt/map/RequestId;)V
    invoke-static {v0, v1, p2}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->access$200(Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;ILcom/broadcom/bt/map/RequestId;)V

    #@5b
    .line 740
    :cond_5b
    iget v1, p2, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@5d
    iget v2, p2, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@5f
    move-object v0, p0

    #@60
    move v3, p3

    #@61
    move-object v4, p4

    #@62
    move v5, p5

    #@63
    invoke-direct/range {v0 .. v5}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageListInfoResultNative(IIILjava/lang/String;Z)V

    #@66
    .line 742
    return-void
.end method

.method setGetMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V
    .registers 5
    .parameter "cancelTimeout"
    .parameter "requestId"

    #@0
    .prologue
    .line 823
    const/4 v0, 0x0

    #@1
    const-string v1, ""

    #@3
    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageResult(ZLcom/broadcom/bt/map/RequestId;ZLjava/lang/String;)V

    #@6
    .line 824
    return-void
.end method

.method setGetMessageResult(ZLcom/broadcom/bt/map/RequestId;ZLjava/lang/String;)V
    .registers 7
    .parameter "cancelTimeout"
    .parameter "requestId"
    .parameter "hasMessage"
    .parameter "messageFilepath"

    #@0
    .prologue
    .line 828
    if-eqz p1, :cond_9

    #@2
    .line 829
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v1, 0x3ed

    #@6
    #calls: Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->stopTimeoutTimer(ILcom/broadcom/bt/map/RequestId;)V
    invoke-static {v0, v1, p2}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->access$200(Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;ILcom/broadcom/bt/map/RequestId;)V

    #@9
    .line 831
    :cond_9
    iget v0, p2, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@b
    iget v1, p2, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@d
    invoke-direct {p0, v0, v1, p3, p4}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageResultNative(IIZLjava/lang/String;)V

    #@10
    .line 833
    return-void
.end method

.method setMessageDeletedResult(ZLcom/broadcom/bt/map/RequestId;Z)V
    .registers 6
    .parameter "cancelTimeout"
    .parameter "requestId"
    .parameter "success"

    #@0
    .prologue
    .line 881
    if-eqz p1, :cond_9

    #@2
    .line 882
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v1, 0x3ef

    #@6
    #calls: Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->stopTimeoutTimer(ILcom/broadcom/bt/map/RequestId;)V
    invoke-static {v0, v1, p2}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->access$200(Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;ILcom/broadcom/bt/map/RequestId;)V

    #@9
    .line 885
    :cond_9
    iget v0, p2, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@b
    iget v1, p2, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@d
    invoke-direct {p0, v0, v1, p3}, Lcom/broadcom/bt/service/map/MapService;->setMessageDeletedResultNative(IIZ)V

    #@10
    .line 886
    return-void
.end method

.method setPushMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V
    .registers 9
    .parameter "cancelTimeout"
    .parameter "requestId"

    #@0
    .prologue
    .line 845
    const/4 v3, 0x0

    #@1
    const-wide/16 v4, 0x0

    #@3
    move-object v0, p0

    #@4
    move v1, p1

    #@5
    move-object v2, p2

    #@6
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/map/MapService;->setPushMessageResult(ZLcom/broadcom/bt/map/RequestId;ZJ)V

    #@9
    .line 846
    return-void
.end method

.method setPushMessageResult(ZLcom/broadcom/bt/map/RequestId;ZJ)V
    .registers 11
    .parameter "cancelTimeout"
    .parameter "requestId"
    .parameter "success"
    .parameter "messageHandle"

    #@0
    .prologue
    .line 850
    if-eqz p1, :cond_9

    #@2
    .line 851
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x3ee

    #@6
    #calls: Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->stopTimeoutTimer(ILcom/broadcom/bt/map/RequestId;)V
    invoke-static {v2, v3, p2}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->access$200(Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;ILcom/broadcom/bt/map/RequestId;)V

    #@9
    .line 855
    :cond_9
    iget-object v0, p2, Lcom/broadcom/bt/map/RequestId;->mRequestData:Ljava/lang/String;

    #@b
    .line 856
    .local v0, bMessageFile:Ljava/lang/String;
    if-eqz v0, :cond_36

    #@d
    .line 857
    new-instance v1, Ljava/io/File;

    #@f
    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@12
    .line 858
    .local v1, f:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@15
    move-result v2

    #@16
    if-eqz v2, :cond_1e

    #@18
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    #@1b
    move-result v2

    #@1c
    if-nez v2, :cond_36

    #@1e
    .line 859
    :cond_1e
    const-string v2, "BtMap.MapService"

    #@20
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "setPushMessageResult(): unable to delete tmp bMessage file "

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 864
    .end local v1           #f:Ljava/io/File;
    :cond_36
    iget v3, p2, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@38
    iget v4, p2, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@3a
    if-eqz p3, :cond_44

    #@3c
    invoke-static {p4, p5}, Lcom/broadcom/bt/service/map/MapService;->toNativeMessageByteArray(J)[B

    #@3f
    move-result-object v2

    #@40
    :goto_40
    invoke-direct {p0, v3, v4, p3, v2}, Lcom/broadcom/bt/service/map/MapService;->setPushMessageResultNative(IIZ[B)V

    #@43
    .line 866
    return-void

    #@44
    .line 864
    :cond_44
    const/4 v2, 0x0

    #@45
    goto :goto_40
.end method

.method protected declared-synchronized start()Z
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 154
    monitor-enter p0

    #@2
    :try_start_2
    const-string v1, ""
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_91

    #@4
    .line 156
    .local v1, version:Ljava/lang/String;
    const/high16 v2, 0x7f07

    #@6
    :try_start_6
    invoke-virtual {p0, v2}, Lcom/broadcom/bt/service/map/MapService;->getString(I)Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_6 .. :try_end_9} :catchall_91
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_9} :catch_87

    #@9
    move-result-object v1

    #@a
    .line 160
    :goto_a
    :try_start_a
    const-string v2, "BtMap.MapService"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "*********Starting MAP Service...Version = "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 163
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/MapService;->initializeNative()V

    #@25
    .line 164
    const/4 v2, 0x1

    #@26
    iput-boolean v2, p0, Lcom/broadcom/bt/service/map/MapService;->mNativeAvailable:Z

    #@28
    .line 167
    new-instance v2, Ljava/io/File;

    #@2a
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapService;->getFilesDir()Ljava/io/File;

    #@2d
    move-result-object v3

    #@2e
    const-string v4, "map"

    #@30
    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@33
    sput-object v2, Lcom/broadcom/bt/service/map/MapService;->mDefaultTmpDir:Ljava/io/File;

    #@35
    .line 168
    sget-object v2, Lcom/broadcom/bt/service/map/MapService;->mDefaultTmpDir:Ljava/io/File;

    #@37
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@3a
    move-result v2

    #@3b
    if-nez v2, :cond_42

    #@3d
    .line 169
    sget-object v2, Lcom/broadcom/bt/service/map/MapService;->mDefaultTmpDir:Ljava/io/File;

    #@3f
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    #@42
    .line 171
    :cond_42
    sget-object v2, Lcom/broadcom/bt/service/map/MapService;->mDefaultTmpDir:Ljava/io/File;

    #@44
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@47
    move-result-object v2

    #@48
    sput-object v2, Lcom/broadcom/bt/service/map/MapService;->mDefaultTmpPath:Ljava/lang/String;

    #@4a
    .line 172
    const-string v2, "BtMap.MapService"

    #@4c
    new-instance v3, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v4, "Tmp directory is "

    #@53
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    sget-object v4, Lcom/broadcom/bt/service/map/MapService;->mDefaultTmpPath:Ljava/lang/String;

    #@59
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 173
    sget-object v2, Lcom/broadcom/bt/service/map/MapService;->mDefaultTmpPath:Ljava/lang/String;

    #@66
    const/4 v3, 0x0

    #@67
    invoke-direct {p0, v2, v3}, Lcom/broadcom/bt/service/map/MapService;->initTmpDirNative(Ljava/lang/String;Z)V

    #@6a
    .line 176
    invoke-static {p0}, Lcom/broadcom/bt/service/map/MapContentProvider;->init(Lcom/broadcom/bt/service/map/MapService;)V

    #@6d
    .line 179
    invoke-static {p0}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->init(Landroid/content/Context;)V

    #@70
    .line 182
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mEventReceiver:Lcom/broadcom/bt/service/map/MapService$EventReceiver;

    #@72
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@74
    #calls: Lcom/broadcom/bt/service/map/MapService$EventReceiver;->init(Landroid/content/Context;Landroid/os/Handler;)V
    invoke-static {v2, p0, v3}, Lcom/broadcom/bt/service/map/MapService$EventReceiver;->access$100(Lcom/broadcom/bt/service/map/MapService$EventReceiver;Landroid/content/Context;Landroid/os/Handler;)V

    #@77
    .line 183
    sput-object p0, Lcom/broadcom/bt/service/map/MapService;->sService:Lcom/broadcom/bt/service/map/MapService;

    #@79
    .line 185
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@7c
    move-result-object v2

    #@7d
    iput-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@7f
    .line 188
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothMapManager;->getInstance(Landroid/content/Context;)Lcom/lge/bluetooth/LGBluetoothMapManager;

    #@82
    move-result-object v2

    #@83
    iput-object v2, p0, Lcom/broadcom/bt/service/map/MapService;->mMapManager:Lcom/lge/bluetooth/LGBluetoothMapManager;
    :try_end_85
    .catchall {:try_start_a .. :try_end_85} :catchall_91

    #@85
    .line 190
    monitor-exit p0

    #@86
    return v5

    #@87
    .line 157
    :catch_87
    move-exception v0

    #@88
    .line 158
    .local v0, t:Ljava/lang/Throwable;
    :try_start_88
    const-string v2, "BtMap.MapService"

    #@8a
    const-string v3, "Error getting version"

    #@8c
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8f
    .catchall {:try_start_88 .. :try_end_8f} :catchall_91

    #@8f
    goto/16 :goto_a

    #@91
    .line 154
    .end local v0           #t:Ljava/lang/Throwable;
    .end local v1           #version:Ljava/lang/String;
    :catchall_91
    move-exception v2

    #@92
    monitor-exit p0

    #@93
    throw v2
.end method

.method protected declared-synchronized stop()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 194
    monitor-enter p0

    #@2
    const/4 v0, 0x1

    #@3
    :try_start_3
    iput-boolean v0, p0, Lcom/broadcom/bt/service/map/MapService;->mIsStopping:Z

    #@5
    .line 196
    const-string v0, "Stopping Bluetooth MapService."

    #@7
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/map/MapService;->log(Ljava/lang/String;)V

    #@a
    .line 198
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mEventReceiver:Lcom/broadcom/bt/service/map/MapService$EventReceiver;

    #@c
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/map/MapService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 199
    invoke-static {p0}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->stopAll(Lcom/broadcom/bt/service/map/MapService;)V

    #@12
    .line 200
    const/4 v0, 0x0

    #@13
    sput-object v0, Lcom/broadcom/bt/service/map/MapService;->sService:Lcom/broadcom/bt/service/map/MapService;

    #@15
    .line 202
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mMapManager:Lcom/lge/bluetooth/LGBluetoothMapManager;

    #@17
    if-eqz v0, :cond_1e

    #@19
    .line 203
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapService;->mMapManager:Lcom/lge/bluetooth/LGBluetoothMapManager;

    #@1b
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothMapManager;->terminateMapManager()V
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_20

    #@1e
    .line 206
    :cond_1e
    monitor-exit p0

    #@1f
    return v1

    #@20
    .line 194
    :catchall_20
    move-exception v0

    #@21
    monitor-exit p0

    #@22
    throw v0
.end method

.method unregisterDatasource(Ljava/lang/String;Ljava/lang/String;Z)Z
    .registers 11
    .parameter "providerId"
    .parameter "dsId"
    .parameter "removePermanently"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 338
    const-string v4, "android.permission.BLUETOOTH_ADMIN"

    #@3
    const-string v5, "Need BLUETOOTH ADMIN permission"

    #@5
    invoke-virtual {p0, v4, v5}, Lcom/broadcom/bt/service/map/MapService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 340
    const-string v4, "BtMap.MapService"

    #@a
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "unregisterDatasource(): providerId="

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-static {p1}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@18
    move-result-object v6

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    const-string v6, ", dsId="

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    invoke-static {p2}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v5

    #@2b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 345
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@35
    move-result-object v1

    #@36
    .line 346
    .local v1, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-nez v1, :cond_41

    #@38
    .line 347
    const-string v4, "BtMap.MapService"

    #@3a
    const-string v5, "unregisterDatasource(): DataSourceManager not available"

    #@3c
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    move v2, v3

    #@40
    .line 358
    :cond_40
    :goto_40
    return v2

    #@41
    .line 350
    :cond_41
    invoke-virtual {v1, p1, p2}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->get(Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@44
    move-result-object v0

    #@45
    .line 351
    .local v0, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    invoke-virtual {v1, p0, p1, p2, p3}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->unregister(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Z

    #@48
    move-result v2

    #@49
    .line 353
    .local v2, res:Z
    const-string v4, "BtMap.MapService"

    #@4b
    new-instance v5, Ljava/lang/StringBuilder;

    #@4d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@50
    const-string v6, "unregisterDatasource(): result = "

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@59
    move-result-object v5

    #@5a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v5

    #@5e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 355
    if-eqz v2, :cond_40

    #@63
    .line 356
    iget-object v4, p0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@65
    invoke-virtual {v4, v0, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessageDatasourceRegistrationChanged(Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V

    #@68
    goto :goto_40
.end method
