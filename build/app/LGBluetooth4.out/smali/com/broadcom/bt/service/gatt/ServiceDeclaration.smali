.class Lcom/broadcom/bt/service/gatt/ServiceDeclaration;
.super Ljava/lang/Object;
.source "ServiceDeclaration.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "BtGatt.ServiceDeclaration"

.field public static final TYPE_CHARACTERISTIC:B = 0x2t

.field public static final TYPE_DESCRIPTOR:B = 0x3t

.field public static final TYPE_INCLUDED_SERVICE:B = 0x4t

.field public static final TYPE_SERVICE:B = 0x1t

.field public static final TYPE_UNDEFINED:B


# instance fields
.field mEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;",
            ">;"
        }
    .end annotation
.end field

.field mNumHandles:I


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 99
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 96
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mEntries:Ljava/util/List;

    #@6
    .line 97
    const/4 v0, 0x0

    #@7
    iput v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@9
    .line 100
    new-instance v0, Ljava/util/ArrayList;

    #@b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@e
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mEntries:Ljava/util/List;

    #@10
    .line 101
    return-void
.end method


# virtual methods
.method addCharacteristic(Ljava/util/UUID;II)V
    .registers 11
    .parameter "uuid"
    .parameter "properties"
    .parameter "permissions"

    #@0
    .prologue
    .line 120
    iget-object v6, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mEntries:Ljava/util/List;

    #@2
    new-instance v0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;

    #@4
    const/4 v5, 0x0

    #@5
    move-object v1, p0

    #@6
    move-object v2, p1

    #@7
    move v3, p2

    #@8
    move v4, p3

    #@9
    invoke-direct/range {v0 .. v5}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;-><init>(Lcom/broadcom/bt/service/gatt/ServiceDeclaration;Ljava/util/UUID;III)V

    #@c
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@f
    .line 121
    iget v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@11
    add-int/lit8 v0, v0, 0x2

    #@13
    iput v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@15
    .line 122
    return-void
.end method

.method addDescriptor(Ljava/util/UUID;I)V
    .registers 5
    .parameter "uuid"
    .parameter "permissions"

    #@0
    .prologue
    .line 125
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mEntries:Ljava/util/List;

    #@2
    new-instance v1, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;

    #@4
    invoke-direct {v1, p0, p1, p2}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;-><init>(Lcom/broadcom/bt/service/gatt/ServiceDeclaration;Ljava/util/UUID;I)V

    #@7
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@a
    .line 126
    iget v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@c
    add-int/lit8 v0, v0, 0x1

    #@e
    iput v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@10
    .line 127
    return-void
.end method

.method addIncludedService(Ljava/util/UUID;II)V
    .registers 6
    .parameter "uuid"
    .parameter "serviceType"
    .parameter "instance"

    #@0
    .prologue
    .line 113
    new-instance v0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;

    #@2
    invoke-direct {v0, p0, p1, p2, p3}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;-><init>(Lcom/broadcom/bt/service/gatt/ServiceDeclaration;Ljava/util/UUID;II)V

    #@5
    .line 114
    .local v0, entry:Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;
    const/4 v1, 0x4

    #@6
    iput-byte v1, v0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;->type:B

    #@8
    .line 115
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mEntries:Ljava/util/List;

    #@a
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@d
    .line 116
    iget v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@f
    add-int/lit8 v1, v1, 0x1

    #@11
    iput v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@13
    .line 117
    return-void
.end method

.method addService(Ljava/util/UUID;III)V
    .registers 7
    .parameter "uuid"
    .parameter "serviceType"
    .parameter "instance"
    .parameter "minHandles"

    #@0
    .prologue
    .line 104
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mEntries:Ljava/util/List;

    #@2
    new-instance v1, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;

    #@4
    invoke-direct {v1, p0, p1, p2, p3}, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;-><init>(Lcom/broadcom/bt/service/gatt/ServiceDeclaration;Ljava/util/UUID;II)V

    #@7
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@a
    .line 105
    if-nez p4, :cond_13

    #@c
    .line 106
    iget v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@e
    add-int/lit8 v0, v0, 0x1

    #@10
    iput v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@12
    .line 110
    :goto_12
    return-void

    #@13
    .line 108
    :cond_13
    iput p4, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@15
    goto :goto_12
.end method

.method getNext()Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 130
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mEntries:Ljava/util/List;

    #@3
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_b

    #@9
    .line 131
    const/4 v0, 0x0

    #@a
    .line 135
    :goto_a
    return-object v0

    #@b
    .line 133
    :cond_b
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mEntries:Ljava/util/List;

    #@d
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@10
    move-result-object v0

    #@11
    check-cast v0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;

    #@13
    .line 134
    .local v0, entry:Lcom/broadcom/bt/service/gatt/ServiceDeclaration$Entry;
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mEntries:Ljava/util/List;

    #@15
    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@18
    goto :goto_a
.end method

.method getNumHandles()I
    .registers 2

    #@0
    .prologue
    .line 139
    iget v0, p0, Lcom/broadcom/bt/service/gatt/ServiceDeclaration;->mNumHandles:I

    #@2
    return v0
.end method
