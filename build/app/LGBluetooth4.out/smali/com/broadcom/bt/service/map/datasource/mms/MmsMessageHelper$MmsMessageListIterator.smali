.class public Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;
.super Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;
.source "MmsMessageHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MmsMessageListIterator"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1247
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;

    #@2
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method protected contactMatchesFilter()Z
    .registers 13

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 1343
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@4
    invoke-interface {v10, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 1344
    .local v3, messageId:Ljava/lang/String;
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@a
    const/4 v11, -0x1

    #@b
    invoke-static {v10, v8, v11}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@e
    move-result v2

    #@f
    .line 1345
    .local v2, mBoxId:I
    const/4 v0, 0x0

    #@10
    .line 1347
    .local v0, addrCursor:Landroid/database/Cursor;
    :try_start_10
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;

    #@12
    invoke-virtual {v10, v3}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->getAddressCursor(Ljava/lang/String;)Landroid/database/Cursor;

    #@15
    move-result-object v0

    #@16
    .line 1348
    invoke-static {v0}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@19
    move-result v10

    #@1a
    if-eqz v10, :cond_4f

    #@1c
    .line 1350
    :cond_1c
    const/4 v10, 0x4

    #@1d
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    .line 1351
    .local v1, contactId:Ljava/lang/String;
    const/4 v10, 0x2

    #@22
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    .line 1353
    .local v5, phoneNumber:Ljava/lang/String;
    const/4 v7, 0x0

    #@27
    .line 1354
    .local v7, tempaddr:Ljava/lang/String;
    const-string v10, "+"

    #@29
    invoke-virtual {v5, v10}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2c
    move-result v10

    #@2d
    if-eqz v10, :cond_34

    #@2f
    .line 1355
    const/4 v10, 0x1

    #@30
    invoke-virtual {v5, v10}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@33
    move-result-object v7

    #@34
    .line 1363
    :cond_34
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;

    #@36
    #getter for: Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->mResolver:Landroid/content/ContentResolver;
    invoke-static {v10}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->access$400(Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;)Landroid/content/ContentResolver;

    #@39
    move-result-object v10

    #@3a
    #calls: Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->getPersonInfo(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;
    invoke-static {v10, v1, v7}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->access$500(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;

    #@3d
    move-result-object v4

    #@3e
    .line 1365
    .local v4, pInfo:Lcom/broadcom/bt/map/PersonInfo;
    if-eqz v4, :cond_49

    #@40
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mContactFilter:Ljava/lang/String;

    #@42
    invoke-static {v4, v10}, Lcom/broadcom/bt/map/ContactsUtil;->contactMatchesFilter(Lcom/broadcom/bt/map/PersonInfo;Ljava/lang/String;)Z

    #@45
    move-result v10

    #@46
    if-eqz v10, :cond_49

    #@48
    .line 1375
    .end local v1           #contactId:Ljava/lang/String;
    .end local v4           #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    .end local v5           #phoneNumber:Ljava/lang/String;
    .end local v7           #tempaddr:Ljava/lang/String;
    :goto_48
    return v8

    #@49
    .line 1369
    .restart local v1       #contactId:Ljava/lang/String;
    .restart local v4       #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    .restart local v5       #phoneNumber:Ljava/lang/String;
    .restart local v7       #tempaddr:Ljava/lang/String;
    :cond_49
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_4c
    .catch Ljava/lang/Throwable; {:try_start_10 .. :try_end_4c} :catch_54

    #@4c
    move-result v10

    #@4d
    if-nez v10, :cond_1c

    #@4f
    .line 1374
    .end local v1           #contactId:Ljava/lang/String;
    .end local v4           #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    .end local v5           #phoneNumber:Ljava/lang/String;
    .end local v7           #tempaddr:Ljava/lang/String;
    :cond_4f
    :goto_4f
    invoke-static {v0}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@52
    move v8, v9

    #@53
    .line 1375
    goto :goto_48

    #@54
    .line 1371
    :catch_54
    move-exception v6

    #@55
    .line 1372
    .local v6, t:Ljava/lang/Throwable;
    const-string v8, "BtMap.MmsMessageHelper"

    #@57
    const-string v10, "contactMatchesFilter: error"

    #@59
    invoke-static {v8, v10, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5c
    goto :goto_4f
.end method

.method public getMessageDateTime()J
    .registers 5

    #@0
    .prologue
    const-wide/16 v0, 0x0

    #@2
    .line 1327
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@4
    if-eqz v2, :cond_10

    #@6
    .line 1328
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@8
    const/4 v3, 0x2

    #@9
    invoke-static {v2, v3, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getLong(Landroid/database/Cursor;IJ)J

    #@c
    move-result-wide v0

    #@d
    const-wide/16 v2, 0x3e8

    #@f
    mul-long/2addr v0, v2

    #@10
    .line 1330
    :cond_10
    return-wide v0
.end method

.method public getMessageInfo()Lcom/broadcom/bt/map/MessageInfo;
    .registers 9

    #@0
    .prologue
    .line 1322
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;

    #@2
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@4
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@6
    iget v3, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mPhoneType:I

    #@8
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mOwnerPhoneNumber:Ljava/lang/String;

    #@a
    iget-boolean v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mIncludeMessageSize:Z

    #@c
    iget v6, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mMaxSubjectLength:I

    #@e
    iget-object v7, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mParams:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@10
    invoke-virtual/range {v0 .. v7}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toMessageInfo(Ljava/lang/String;Landroid/database/Cursor;ILjava/lang/String;ZILcom/broadcom/bt/map/MessageParameterFilter;)Lcom/broadcom/bt/map/MessageInfo;

    #@13
    move-result-object v0

    #@14
    return-object v0
.end method

.method protected getReadStatus()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1335
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@4
    if-eqz v2, :cond_12

    #@6
    .line 1336
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@8
    const/4 v3, 0x3

    #@9
    invoke-static {v2, v3, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@c
    move-result v2

    #@d
    if-ne v2, v0, :cond_10

    #@f
    .line 1338
    :goto_f
    return v0

    #@10
    :cond_10
    move v0, v1

    #@11
    .line 1336
    goto :goto_f

    #@12
    :cond_12
    move v0, v1

    #@13
    .line 1338
    goto :goto_f
.end method

.method public initCountIterator(Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Ljava/lang/String;Lcom/broadcom/bt/map/MessageParameterFilter;)V
    .registers 13
    .parameter "folderPath"
    .parameter "messageListFilter"
    .parameter "contactFilter"
    .parameter "params"

    #@0
    .prologue
    .line 1251
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@2
    .line 1252
    iput-object p2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@4
    .line 1253
    iput-object p3, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mContactFilter:Ljava/lang/String;

    #@6
    .line 1254
    iput-object p4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mParams:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@8
    .line 1256
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;

    #@a
    #getter for: Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->access$000(Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;)Landroid/content/Context;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@11
    move-result-object v0

    #@12
    .line 1262
    .local v0, resolver:Landroid/content/ContentResolver;
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;

    #@14
    #calls: Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->createMessageListSelector(Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;
    invoke-static {v2, p2}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->access$100(Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v6

    #@18
    .line 1264
    .local v6, selection:Ljava/lang/StringBuilder;
    :try_start_18
    sget-object v2, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@1a
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@1c
    const-string v4, "UTF-8"

    #@1e
    invoke-static {v3, v4}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@25
    move-result-object v1

    #@26
    .line 1266
    .local v1, uri:Landroid/net/Uri;
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->access$200()[Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    const/4 v4, 0x0

    #@2f
    const-string v5, "_id desc"

    #@31
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@34
    move-result-object v2

    #@35
    invoke-static {v2}, Lcom/broadcom/bt/util/DBUtil;->getNonEmptyCursorOrClose(Landroid/database/Cursor;)Landroid/database/Cursor;

    #@38
    move-result-object v2

    #@39
    iput-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mCursor:Landroid/database/Cursor;
    :try_end_3b
    .catch Ljava/lang/Throwable; {:try_start_18 .. :try_end_3b} :catch_3c

    #@3b
    .line 1273
    .end local v1           #uri:Landroid/net/Uri;
    :goto_3b
    return-void

    #@3c
    .line 1269
    :catch_3c
    move-exception v7

    #@3d
    .line 1270
    .local v7, t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MmsMessageHelper"

    #@3f
    const-string v3, "Unable to query content provider"

    #@41
    invoke-static {v2, v3, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@44
    goto :goto_3b
.end method

.method public initMessageInfoIterator(Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Ljava/lang/String;Lcom/broadcom/bt/map/MessageParameterFilter;ZILjava/lang/String;I)V
    .registers 18
    .parameter "folderPath"
    .parameter "messageListFilter"
    .parameter "contactFilter"
    .parameter "params"
    .parameter "includeMessageSize"
    .parameter "phoneType"
    .parameter "ownerNumber"
    .parameter "maxSubjectLength"

    #@0
    .prologue
    .line 1278
    const-string v3, "BtMap.MmsMessageHelper"

    #@2
    const-string v4, "initMessageInfoIterator()"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1279
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@9
    .line 1280
    iput-object p2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@b
    .line 1281
    iput-object p3, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mContactFilter:Ljava/lang/String;

    #@d
    .line 1282
    iput-object p4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mParams:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@f
    .line 1283
    iput-boolean p5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mIncludeMessageSize:Z

    #@11
    .line 1284
    iput p6, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mPhoneType:I

    #@13
    .line 1285
    move-object/from16 v0, p7

    #@15
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mOwnerPhoneNumber:Ljava/lang/String;

    #@17
    .line 1286
    move/from16 v0, p8

    #@19
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mMaxSubjectLength:I

    #@1b
    .line 1292
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;

    #@1d
    #getter for: Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->access$000(Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;)Landroid/content/Context;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v1

    #@25
    .line 1293
    .local v1, resolver:Landroid/content/ContentResolver;
    new-instance v7, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    .line 1295
    .local v7, selection:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    #@2b
    .line 1307
    .local v2, uri:Landroid/net/Uri;
    :try_start_2b
    sget-object v3, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@2d
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@2f
    const-string v5, "UTF-8"

    #@31
    invoke-static {v4, v5}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@38
    move-result-object v2

    #@39
    .line 1310
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;

    #@3b
    #calls: Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->createMessageListSelector(Ljava/lang/StringBuilder;Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;
    invoke-static {v3, v7, p2}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->access$300(Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;Ljava/lang/StringBuilder;Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v7

    #@3f
    .line 1311
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->access$200()[Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    const/4 v5, 0x0

    #@48
    const-string v6, "_id desc"

    #@4a
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@4d
    move-result-object v3

    #@4e
    invoke-static {v3}, Lcom/broadcom/bt/util/DBUtil;->getNonEmptyCursorOrClose(Landroid/database/Cursor;)Landroid/database/Cursor;

    #@51
    move-result-object v3

    #@52
    iput-object v3, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper$MmsMessageListIterator;->mCursor:Landroid/database/Cursor;
    :try_end_54
    .catch Ljava/lang/Throwable; {:try_start_2b .. :try_end_54} :catch_55

    #@54
    .line 1319
    :goto_54
    return-void

    #@55
    .line 1315
    :catch_55
    move-exception v8

    #@56
    .line 1316
    .local v8, t:Ljava/lang/Throwable;
    const-string v3, "BtMap.MmsMessageHelper"

    #@58
    const-string v4, "Unable to query content provider"

    #@5a
    invoke-static {v3, v4, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@5d
    goto :goto_54
.end method
