.class Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;
.super Ljava/util/LinkedHashMap;
.source "MapClientSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/MapClientSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CachedHashMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/LinkedHashMap",
        "<TK;TV;>;"
    }
.end annotation


# instance fields
.field private mMaxCount:I


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "maxCount"

    #@0
    .prologue
    .line 73
    .local p0, this:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;,"Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap<TK;TV;>;"
    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    #@3
    .line 70
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->mMaxCount:I

    #@6
    .line 74
    iput p1, p0, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->mMaxCount:I

    #@8
    .line 75
    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .registers 5
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TV;>;)Z"
        }
    .end annotation

    #@0
    .prologue
    .line 78
    .local p0, this:Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;,"Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap<TK;TV;>;"
    .local p1, eldest:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<TK;TV;>;"
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->size()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->mMaxCount:I

    #@6
    if-le v0, v1, :cond_2b

    #@8
    .line 79
    const-string v0, "BtMap.MapClientSession"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Message ID to Handle cache maximum length reached: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget v2, p0, Lcom/broadcom/bt/service/map/MapClientSession$CachedHashMap;->mMaxCount:I

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 80
    const-string v0, "BtMap.MapClientSession"

    #@24
    const-string v1, "Removing last cached entry..."

    #@26
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 81
    const/4 v0, 0x1

    #@2a
    .line 83
    :goto_2a
    return v0

    #@2b
    :cond_2b
    const/4 v0, 0x0

    #@2c
    goto :goto_2a
.end method
