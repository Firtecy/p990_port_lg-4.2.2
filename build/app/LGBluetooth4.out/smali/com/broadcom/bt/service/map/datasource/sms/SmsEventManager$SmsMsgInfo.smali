.class Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
.super Ljava/lang/Object;
.source "SmsEventManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SmsMsgInfo"
.end annotation


# instance fields
.field mFolderPath:Ljava/lang/String;

.field mMceCreated:I

.field mMceDeleted:I

.field mMessageDate:J

.field mMessageId:I

.field mMessageType:I

.field mThreadId:I


# direct methods
.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v0, -0x1

    #@2
    .line 114
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 115
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageId:I

    #@7
    .line 116
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mThreadId:I

    #@9
    .line 117
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@b
    .line 118
    const-wide/16 v0, 0x0

    #@d
    iput-wide v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageDate:J

    #@f
    .line 119
    iput v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMceCreated:I

    #@11
    .line 120
    iput v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMceDeleted:I

    #@13
    return-void
.end method

.method synthetic constructor <init>(Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 114
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;-><init>()V

    #@3
    return-void
.end method
