.class abstract Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;
.super Ljava/lang/Object;
.source "SmsMmsDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "MessageListProcessor"
.end annotation


# instance fields
.field protected mContactFilter:Ljava/lang/String;

.field protected mFolderPath:Ljava/lang/String;

.field protected mIsInboundMessage:Z

.field protected mMaxListCount:I

.field protected mMessageCount:I

.field protected mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

.field protected mMessageOffset:I

.field protected mParamFilter:Lcom/broadcom/bt/map/MessageParameterFilter;

.field protected mRequestId:Lcom/broadcom/bt/map/RequestId;

.field protected mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

.field protected smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

.field final synthetic this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;


# direct methods
.method private constructor <init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)V
    .registers 4
    .parameter

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 735
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 742
    const/4 v0, -0x1

    #@7
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMaxListCount:I

    #@9
    .line 744
    iput-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@b
    .line 745
    iput-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@d
    return-void
.end method

.method synthetic constructor <init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 735
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;-><init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)V

    #@3
    return-void
.end method


# virtual methods
.method public init(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V
    .registers 5
    .parameter "requestId"
    .parameter "folderPath"
    .parameter "messageListFilter"
    .parameter "paramFilter"

    #@0
    .prologue
    .line 750
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mRequestId:Lcom/broadcom/bt/map/RequestId;

    #@2
    .line 751
    iput-object p2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mFolderPath:Ljava/lang/String;

    #@4
    .line 752
    iput-object p3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@6
    .line 753
    iput-object p4, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mParamFilter:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@8
    .line 754
    return-void
.end method

.method protected initMessageOffset()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 767
    iput v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageOffset:I

    #@3
    .line 768
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@5
    invoke-virtual {v0}, Lcom/broadcom/bt/map/MessageListFilter;->listStartOffsetSet()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_11

    #@b
    .line 769
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@d
    iget v0, v0, Lcom/broadcom/bt/map/MessageListFilter;->mListStartOffset:I

    #@f
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageOffset:I

    #@11
    .line 771
    :cond_11
    iget v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageOffset:I

    #@13
    if-gez v0, :cond_17

    #@15
    .line 772
    iput v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageOffset:I

    #@17
    .line 774
    :cond_17
    return-void
.end method

.method protected abstract initMmsMessageIterator()V
.end method

.method protected abstract initSmsMessageIterator()V
.end method

.method protected processAllMmsEntries()Z
    .registers 2

    #@0
    .prologue
    .line 781
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected processAllSmsEntries()Z
    .registers 2

    #@0
    .prologue
    .line 777
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected abstract processOneMmsEntry()V
.end method

.method protected abstract processOneSmsEntry()V
.end method

.method public run()V
    .registers 14

    #@0
    .prologue
    .line 789
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mParamFilter:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@2
    if-nez v8, :cond_d

    #@4
    .line 790
    new-instance v8, Lcom/broadcom/bt/map/MessageParameterFilter;

    #@6
    const-wide/16 v9, 0x0

    #@8
    invoke-direct {v8, v9, v10}, Lcom/broadcom/bt/map/MessageParameterFilter;-><init>(J)V

    #@b
    iput-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mParamFilter:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@d
    .line 792
    :cond_d
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@f
    if-nez v8, :cond_18

    #@11
    .line 793
    new-instance v8, Lcom/broadcom/bt/map/MessageListFilter;

    #@13
    invoke-direct {v8}, Lcom/broadcom/bt/map/MessageListFilter;-><init>()V

    #@16
    iput-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@18
    .line 795
    :cond_18
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@1b
    move-result v8

    #@1c
    if-eqz v8, :cond_f0

    #@1e
    .line 796
    const-string v8, "BtMap.SmsMmsDataSource"

    #@20
    new-instance v9, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v10, "run(): sessionId="

    #@27
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v9

    #@2b
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mRequestId:Lcom/broadcom/bt/map/RequestId;

    #@2d
    iget v10, v10, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@2f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v9

    #@33
    const-string v10, ", eventId="

    #@35
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v9

    #@39
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mRequestId:Lcom/broadcom/bt/map/RequestId;

    #@3b
    iget v10, v10, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@3d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    const-string v10, ", sfolderPath="

    #@43
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v9

    #@47
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mFolderPath:Ljava/lang/String;

    #@49
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v9

    #@4d
    const-string v10, ", providerType = "

    #@4f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v9

    #@53
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@55
    #getter for: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I
    invoke-static {v10}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$400(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)I

    #@58
    move-result v10

    #@59
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v9

    #@5d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v9

    #@61
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 799
    const-string v8, "BtMap.SmsMmsDataSource"

    #@66
    new-instance v9, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v10, " parameterMask="

    #@6d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v9

    #@71
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mParamFilter:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@73
    iget-wide v10, v10, Lcom/broadcom/bt/map/MessageParameterFilter;->mParameterMask:J

    #@75
    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@78
    move-result-object v9

    #@79
    const-string v10, " maxListCnt="

    #@7b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v9

    #@7f
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@81
    iget v10, v10, Lcom/broadcom/bt/map/MessageListFilter;->mMaxListCount:I

    #@83
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@86
    move-result-object v9

    #@87
    const-string v10, ", periodBegin="

    #@89
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v9

    #@8d
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@8f
    iget-object v10, v10, Lcom/broadcom/bt/map/MessageListFilter;->mPeriodBegin:Ljava/lang/String;

    #@91
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v9

    #@95
    const-string v10, " periodEnd="

    #@97
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9a
    move-result-object v9

    #@9b
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@9d
    iget-object v10, v10, Lcom/broadcom/bt/map/MessageListFilter;->mPeriodEnd:Ljava/lang/String;

    #@9f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v9

    #@a3
    const-string v10, " readStatus="

    #@a5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v9

    #@a9
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@ab
    iget-byte v10, v10, Lcom/broadcom/bt/map/MessageListFilter;->mReadStatus:B

    #@ad
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v9

    #@b1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v9

    #@b5
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b8
    .line 804
    const-string v8, "BtMap.SmsMmsDataSource"

    #@ba
    new-instance v9, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v10, "recipient="

    #@c1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v9

    #@c5
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@c7
    iget-object v10, v10, Lcom/broadcom/bt/map/MessageListFilter;->mRecipient:Ljava/lang/String;

    #@c9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v9

    #@cd
    const-string v10, " originator="

    #@cf
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v9

    #@d3
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@d5
    iget-object v10, v10, Lcom/broadcom/bt/map/MessageListFilter;->mOriginator:Ljava/lang/String;

    #@d7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v9

    #@db
    const-string v10, " msgMask = "

    #@dd
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v9

    #@e1
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@e3
    iget-byte v10, v10, Lcom/broadcom/bt/map/MessageListFilter;->mMsgMask:B

    #@e5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v9

    #@e9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v9

    #@ed
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f0
    .line 810
    :cond_f0
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@f2
    iget-object v9, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mFolderPath:Ljava/lang/String;

    #@f4
    #calls: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->isValidMsgFolder(Ljava/lang/String;)Z
    invoke-static {v8, v9}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$600(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Ljava/lang/String;)Z

    #@f7
    move-result v8

    #@f8
    if-nez v8, :cond_118

    #@fa
    .line 811
    const-string v8, "BtMap.SmsMmsDataSource"

    #@fc
    new-instance v9, Ljava/lang/StringBuilder;

    #@fe
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@101
    const-string v10, "run(): Invalid folder path:"

    #@103
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v9

    #@107
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mFolderPath:Ljava/lang/String;

    #@109
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v9

    #@10d
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v9

    #@111
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    .line 812
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->setResultNotSuccess()V

    #@117
    .line 973
    :cond_117
    :goto_117
    return-void

    #@118
    .line 818
    :cond_118
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mFolderPath:Ljava/lang/String;

    #@11a
    if-eqz v8, :cond_157

    #@11c
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mFolderPath:Ljava/lang/String;

    #@11e
    invoke-virtual {v8}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@121
    move-result-object v8

    #@122
    const-string v9, "inbox"

    #@124
    invoke-virtual {v8, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@127
    move-result v8

    #@128
    if-eqz v8, :cond_157

    #@12a
    const/4 v8, 0x1

    #@12b
    :goto_12b
    iput-boolean v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mIsInboundMessage:Z

    #@12d
    .line 819
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@12f
    iget-boolean v9, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mIsInboundMessage:Z

    #@131
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@133
    #calls: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->matchesOwnerFilter(ZLcom/broadcom/bt/map/MessageListFilter;)Z
    invoke-static {v8, v9, v10}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$700(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;ZLcom/broadcom/bt/map/MessageListFilter;)Z

    #@136
    move-result v8

    #@137
    if-nez v8, :cond_159

    #@139
    .line 820
    const-string v8, "BtMap.SmsMmsDataSource"

    #@13b
    new-instance v9, Ljava/lang/StringBuilder;

    #@13d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@140
    const-string v10, "run(): Owner does not match set filters:"

    #@142
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v9

    #@146
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mFolderPath:Ljava/lang/String;

    #@148
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v9

    #@14c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14f
    move-result-object v9

    #@150
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@153
    .line 821
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->setResultNoEntries()V

    #@156
    goto :goto_117

    #@157
    .line 818
    :cond_157
    const/4 v8, 0x0

    #@158
    goto :goto_12b

    #@159
    .line 826
    :cond_159
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@15b
    iget-object v9, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@15d
    #calls: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->checkGetSmsMessages(Lcom/broadcom/bt/map/MessageListFilter;)Z
    invoke-static {v8, v9}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$800(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Lcom/broadcom/bt/map/MessageListFilter;)Z

    #@160
    move-result v1

    #@161
    .line 827
    .local v1, getSms:Z
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@163
    iget-object v9, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@165
    #calls: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->checkGetMmsMessages(Lcom/broadcom/bt/map/MessageListFilter;)Z
    invoke-static {v8, v9}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$900(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Lcom/broadcom/bt/map/MessageListFilter;)Z

    #@168
    move-result v0

    #@169
    .line 828
    .local v0, getMms:Z
    if-nez v0, :cond_178

    #@16b
    if-nez v1, :cond_178

    #@16d
    .line 829
    const-string v8, "BtMap.SmsMmsDataSource"

    #@16f
    const-string v9, "run(): Message filters out all sms and mms messages"

    #@171
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@174
    .line 830
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->setResultNoEntries()V

    #@177
    goto :goto_117

    #@178
    .line 838
    :cond_178
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@17a
    invoke-virtual {v8}, Lcom/broadcom/bt/map/MessageListFilter;->priorityStatusFilterSet()Z

    #@17d
    move-result v8

    #@17e
    if-eqz v8, :cond_18c

    #@180
    .line 839
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@182
    invoke-virtual {v8}, Lcom/broadcom/bt/map/MessageListFilter;->filterIsPriority()Z

    #@185
    move-result v8

    #@186
    if-eqz v8, :cond_18c

    #@188
    .line 841
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->setResultNoEntries()V

    #@18b
    goto :goto_117

    #@18c
    .line 849
    :cond_18c
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@18e
    iget-boolean v9, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mIsInboundMessage:Z

    #@190
    iget-object v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@192
    #calls: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getContactFilter(ZLcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/String;
    invoke-static {v8, v9, v10}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$1000(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;ZLcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/String;

    #@195
    move-result-object v8

    #@196
    iput-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mContactFilter:Ljava/lang/String;

    #@198
    .line 851
    const/4 v8, -0x1

    #@199
    iput v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMaxListCount:I

    #@19b
    .line 852
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@19d
    invoke-virtual {v8}, Lcom/broadcom/bt/map/MessageListFilter;->maxListCountSet()Z

    #@1a0
    move-result v8

    #@1a1
    if-eqz v8, :cond_1a9

    #@1a3
    .line 853
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@1a5
    iget v8, v8, Lcom/broadcom/bt/map/MessageListFilter;->mMaxListCount:I

    #@1a7
    iput v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMaxListCount:I

    #@1a9
    .line 855
    :cond_1a9
    iget v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMaxListCount:I

    #@1ab
    if-gtz v8, :cond_1ef

    #@1ad
    const/4 v7, 0x1

    #@1ae
    .line 856
    .local v7, returnAllMsgs:Z
    :goto_1ae
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->initMessageOffset()V

    #@1b1
    .line 858
    if-eqz v1, :cond_1bc

    #@1b3
    .line 859
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->initSmsMessageIterator()V

    #@1b6
    .line 860
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@1b8
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->moveToNext()Z

    #@1bb
    move-result v1

    #@1bc
    .line 862
    :cond_1bc
    if-eqz v0, :cond_1c7

    #@1be
    .line 863
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->initMmsMessageIterator()V

    #@1c1
    .line 864
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@1c3
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->moveToNext()Z

    #@1c6
    move-result v0

    #@1c7
    .line 867
    :cond_1c7
    if-nez v1, :cond_1f1

    #@1c9
    if-nez v0, :cond_1f1

    #@1cb
    .line 868
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@1cd
    if-eqz v8, :cond_1d4

    #@1cf
    .line 869
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@1d1
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->close()V

    #@1d4
    .line 871
    :cond_1d4
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@1d6
    if-eqz v8, :cond_1dd

    #@1d8
    .line 872
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@1da
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->close()V

    #@1dd
    .line 874
    :cond_1dd
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@1e0
    move-result v8

    #@1e1
    if-eqz v8, :cond_1ea

    #@1e3
    .line 875
    const-string v8, "BtMap.SmsMmsDataSource"

    #@1e5
    const-string v9, "getMsgListingCount(): no messages..."

    #@1e7
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ea
    .line 877
    :cond_1ea
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->setResultNoEntries()V

    #@1ed
    goto/16 :goto_117

    #@1ef
    .line 855
    .end local v7           #returnAllMsgs:Z
    :cond_1ef
    const/4 v7, 0x0

    #@1f0
    goto :goto_1ae

    #@1f1
    .line 882
    .restart local v7       #returnAllMsgs:Z
    :cond_1f1
    if-eqz v1, :cond_1f5

    #@1f3
    if-nez v0, :cond_1fd

    #@1f5
    .line 883
    :cond_1f5
    if-eqz v1, :cond_372

    #@1f7
    .line 884
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->processAllSmsEntries()Z

    #@1fa
    move-result v8

    #@1fb
    if-nez v8, :cond_117

    #@1fd
    .line 901
    :cond_1fd
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@1ff
    if-nez v8, :cond_37c

    #@201
    const-wide/16 v4, 0x0

    #@203
    .line 902
    .local v4, lastSmsMessageDate:J
    :goto_203
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@205
    if-nez v8, :cond_384

    #@207
    const-wide/16 v2, 0x0

    #@209
    .line 903
    .local v2, lastMmsMessageDate:J
    :goto_209
    iget v6, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageOffset:I

    #@20b
    .line 904
    .local v6, offsetCount:I
    const/4 v8, 0x0

    #@20c
    iput v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageCount:I

    #@20e
    .line 905
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@211
    move-result v8

    #@212
    if-eqz v8, :cond_258

    #@214
    .line 906
    const-string v8, "BtMap.SmsMmsDataSource"

    #@216
    new-instance v9, Ljava/lang/StringBuilder;

    #@218
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@21b
    const-string v10, "lastSmsMesageDate = "

    #@21d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v9

    #@221
    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@224
    move-result-object v9

    #@225
    const-string v10, ", lastMmsMessageDate="

    #@227
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22a
    move-result-object v9

    #@22b
    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v9

    #@22f
    const-string v10, ", returnAllMsgs="

    #@231
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@234
    move-result-object v9

    #@235
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@238
    move-result-object v9

    #@239
    const-string v10, ",mMaxListCount="

    #@23b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23e
    move-result-object v9

    #@23f
    iget v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMaxListCount:I

    #@241
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@244
    move-result-object v9

    #@245
    const-string v10, ", mMessageCount="

    #@247
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24a
    move-result-object v9

    #@24b
    iget v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageCount:I

    #@24d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@250
    move-result-object v9

    #@251
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@254
    move-result-object v9

    #@255
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@258
    .line 911
    :cond_258
    :goto_258
    const-wide/16 v8, 0x0

    #@25a
    cmp-long v8, v4, v8

    #@25c
    if-gtz v8, :cond_264

    #@25e
    const-wide/16 v8, 0x0

    #@260
    cmp-long v8, v2, v8

    #@262
    if-lez v8, :cond_3f3

    #@264
    :cond_264
    if-nez v7, :cond_26c

    #@266
    iget v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageCount:I

    #@268
    iget v9, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMaxListCount:I

    #@26a
    if-ge v8, v9, :cond_3f3

    #@26c
    .line 912
    :cond_26c
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@26f
    move-result v8

    #@270
    if-eqz v8, :cond_32b

    #@272
    .line 913
    const-string v8, "BtMap.SmsMmsDataSource"

    #@274
    new-instance v9, Ljava/lang/StringBuilder;

    #@276
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@279
    const-string v10, "lastSmsMesageDate = "

    #@27b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27e
    move-result-object v9

    #@27f
    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@282
    move-result-object v9

    #@283
    const-string v10, ", lastMmsMessageDate="

    #@285
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@288
    move-result-object v9

    #@289
    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@28c
    move-result-object v9

    #@28d
    const-string v10, ", returnAllMsgs="

    #@28f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@292
    move-result-object v9

    #@293
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@296
    move-result-object v9

    #@297
    const-string v10, ",messageCount="

    #@299
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29c
    move-result-object v9

    #@29d
    iget v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageCount:I

    #@29f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a2
    move-result-object v9

    #@2a3
    const-string v10, ",mMaxListCount="

    #@2a5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a8
    move-result-object v9

    #@2a9
    iget v10, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMaxListCount:I

    #@2ab
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2ae
    move-result-object v9

    #@2af
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b2
    move-result-object v9

    #@2b3
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b6
    .line 917
    const-string v9, "BtMap.SmsMmsDataSource"

    #@2b8
    new-instance v8, Ljava/lang/StringBuilder;

    #@2ba
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2bd
    const-string v10, "lastSmsMesageDate >0 "

    #@2bf
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c2
    move-result-object v10

    #@2c3
    const-wide/16 v11, 0x0

    #@2c5
    cmp-long v8, v4, v11

    #@2c7
    if-lez v8, :cond_38c

    #@2c9
    const/4 v8, 0x1

    #@2ca
    :goto_2ca
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2cd
    move-result-object v8

    #@2ce
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d1
    move-result-object v8

    #@2d2
    invoke-static {v9, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d5
    .line 918
    const-string v9, "BtMap.SmsMmsDataSource"

    #@2d7
    new-instance v8, Ljava/lang/StringBuilder;

    #@2d9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2dc
    const-string v10, "lastMmsMessageDate >0 "

    #@2de
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e1
    move-result-object v10

    #@2e2
    const-wide/16 v11, 0x0

    #@2e4
    cmp-long v8, v2, v11

    #@2e6
    if-lez v8, :cond_38f

    #@2e8
    const/4 v8, 0x1

    #@2e9
    :goto_2e9
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2ec
    move-result-object v8

    #@2ed
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f0
    move-result-object v8

    #@2f1
    invoke-static {v9, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f4
    .line 919
    const-string v8, "BtMap.SmsMmsDataSource"

    #@2f6
    new-instance v9, Ljava/lang/StringBuilder;

    #@2f8
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@2fb
    const-string v10, "returnAllMsgs "

    #@2fd
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@300
    move-result-object v9

    #@301
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@304
    move-result-object v9

    #@305
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@308
    move-result-object v9

    #@309
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30c
    .line 921
    const-string v9, "BtMap.SmsMmsDataSource"

    #@30e
    new-instance v8, Ljava/lang/StringBuilder;

    #@310
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@313
    const-string v10, "mMessageCount < mMaxListCount "

    #@315
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@318
    move-result-object v10

    #@319
    iget v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageCount:I

    #@31b
    iget v11, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMaxListCount:I

    #@31d
    if-ge v8, v11, :cond_392

    #@31f
    const/4 v8, 0x1

    #@320
    :goto_320
    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@323
    move-result-object v8

    #@324
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@327
    move-result-object v8

    #@328
    invoke-static {v9, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32b
    .line 924
    :cond_32b
    cmp-long v8, v4, v2

    #@32d
    if-ltz v8, :cond_3a2

    #@32f
    .line 925
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@332
    move-result v8

    #@333
    if-eqz v8, :cond_33c

    #@335
    .line 926
    const-string v8, "BtMap.SmsMmsDataSource"

    #@337
    const-string v9, "Selecting SMS Message.."

    #@339
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33c
    .line 929
    :cond_33c
    if-lez v6, :cond_394

    #@33e
    .line 930
    add-int/lit8 v6, v6, -0x1

    #@340
    .line 931
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@343
    move-result v8

    #@344
    if-eqz v8, :cond_35e

    #@346
    .line 932
    const-string v8, "BtMap.SmsMmsDataSource"

    #@348
    new-instance v9, Ljava/lang/StringBuilder;

    #@34a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@34d
    const-string v10, "Skipping offset: "

    #@34f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@352
    move-result-object v9

    #@353
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@356
    move-result-object v9

    #@357
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35a
    move-result-object v9

    #@35b
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35e
    .line 939
    :cond_35e
    :goto_35e
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@360
    if-eqz v8, :cond_39e

    #@362
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@364
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->moveToNext()Z

    #@367
    move-result v8

    #@368
    if-eqz v8, :cond_39e

    #@36a
    .line 940
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@36c
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->getMessageDateTime()J

    #@36f
    move-result-wide v4

    #@370
    goto/16 :goto_258

    #@372
    .line 887
    .end local v2           #lastMmsMessageDate:J
    .end local v4           #lastSmsMessageDate:J
    .end local v6           #offsetCount:I
    :cond_372
    if-eqz v0, :cond_1fd

    #@374
    .line 888
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->processAllMmsEntries()Z

    #@377
    move-result v8

    #@378
    if-eqz v8, :cond_1fd

    #@37a
    goto/16 :goto_117

    #@37c
    .line 901
    :cond_37c
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@37e
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->getMessageDateTime()J

    #@381
    move-result-wide v4

    #@382
    goto/16 :goto_203

    #@384
    .line 902
    .restart local v4       #lastSmsMessageDate:J
    :cond_384
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@386
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->getMessageDateTime()J

    #@389
    move-result-wide v2

    #@38a
    goto/16 :goto_209

    #@38c
    .line 917
    .restart local v2       #lastMmsMessageDate:J
    .restart local v6       #offsetCount:I
    :cond_38c
    const/4 v8, 0x0

    #@38d
    goto/16 :goto_2ca

    #@38f
    .line 918
    :cond_38f
    const/4 v8, 0x0

    #@390
    goto/16 :goto_2e9

    #@392
    .line 921
    :cond_392
    const/4 v8, 0x0

    #@393
    goto :goto_320

    #@394
    .line 935
    :cond_394
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->processOneSmsEntry()V

    #@397
    .line 936
    iget v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageCount:I

    #@399
    add-int/lit8 v8, v8, 0x1

    #@39b
    iput v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageCount:I

    #@39d
    goto :goto_35e

    #@39e
    .line 942
    :cond_39e
    const-wide/16 v4, 0x0

    #@3a0
    goto/16 :goto_258

    #@3a2
    .line 945
    :cond_3a2
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@3a5
    move-result v8

    #@3a6
    if-eqz v8, :cond_3af

    #@3a8
    .line 946
    const-string v8, "BtMap.SmsMmsDataSource"

    #@3aa
    const-string v9, "Selecting MMS Message.."

    #@3ac
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3af
    .line 949
    :cond_3af
    if-lez v6, :cond_3e5

    #@3b1
    .line 950
    add-int/lit8 v6, v6, -0x1

    #@3b3
    .line 951
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@3b6
    move-result v8

    #@3b7
    if-eqz v8, :cond_3d1

    #@3b9
    .line 952
    const-string v8, "BtMap.SmsMmsDataSource"

    #@3bb
    new-instance v9, Ljava/lang/StringBuilder;

    #@3bd
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3c0
    const-string v10, "Skipping offset: "

    #@3c2
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c5
    move-result-object v9

    #@3c6
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c9
    move-result-object v9

    #@3ca
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3cd
    move-result-object v9

    #@3ce
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d1
    .line 959
    :cond_3d1
    :goto_3d1
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@3d3
    if-eqz v8, :cond_3ef

    #@3d5
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@3d7
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->moveToNext()Z

    #@3da
    move-result v8

    #@3db
    if-eqz v8, :cond_3ef

    #@3dd
    .line 960
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@3df
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->getMessageDateTime()J

    #@3e2
    move-result-wide v2

    #@3e3
    goto/16 :goto_258

    #@3e5
    .line 955
    :cond_3e5
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->processOneMmsEntry()V

    #@3e8
    .line 956
    iget v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageCount:I

    #@3ea
    add-int/lit8 v8, v8, 0x1

    #@3ec
    iput v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mMessageCount:I

    #@3ee
    goto :goto_3d1

    #@3ef
    .line 962
    :cond_3ef
    const-wide/16 v2, 0x0

    #@3f1
    goto/16 :goto_258

    #@3f3
    .line 966
    :cond_3f3
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@3f5
    if-eqz v8, :cond_3fc

    #@3f7
    .line 967
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@3f9
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->close()V

    #@3fc
    .line 969
    :cond_3fc
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@3fe
    if-eqz v8, :cond_405

    #@400
    .line 970
    iget-object v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@402
    invoke-virtual {v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->close()V

    #@405
    .line 972
    :cond_405
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->setResultSuccess()V

    #@408
    goto/16 :goto_117
.end method

.method protected abstract setResultNoEntries()V
.end method

.method protected abstract setResultNotSuccess()V
.end method

.method protected abstract setResultSuccess()V
.end method
