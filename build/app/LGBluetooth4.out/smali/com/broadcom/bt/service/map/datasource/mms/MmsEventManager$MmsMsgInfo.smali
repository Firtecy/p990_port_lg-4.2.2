.class Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
.super Ljava/lang/Object;
.source "MmsEventManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MmsMsgInfo"
.end annotation


# instance fields
.field mFolderPath:Ljava/lang/String;

.field mMceCreated:I

.field mMceDeleted:I

.field mMessageDate:J

.field mMessageId:I

.field mMessageType:I

.field mThreadId:I


# direct methods
.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v0, -0x1

    #@2
    .line 122
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 123
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageId:I

    #@7
    .line 124
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mThreadId:I

    #@9
    .line 125
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@b
    .line 126
    const-wide/16 v0, 0x0

    #@d
    iput-wide v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageDate:J

    #@f
    .line 127
    iput v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMceCreated:I

    #@11
    .line 128
    iput v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMceDeleted:I

    #@13
    return-void
.end method

.method synthetic constructor <init>(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 122
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;-><init>()V

    #@3
    return-void
.end method
