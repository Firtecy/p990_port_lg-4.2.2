.class Lcom/broadcom/bt/service/gatt/SearchQueue;
.super Ljava/lang/Object;
.source "SearchQueue.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    }
.end annotation


# instance fields
.field private mEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 70
    new-instance v0, Ljava/util/ArrayList;

    #@5
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@8
    iput-object v0, p0, Lcom/broadcom/bt/service/gatt/SearchQueue;->mEntries:Ljava/util/List;

    #@a
    return-void
.end method


# virtual methods
.method add(IIIJJ)V
    .registers 11
    .parameter "connId"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"

    #@0
    .prologue
    .line 74
    new-instance v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;

    #@2
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;-><init>(Lcom/broadcom/bt/service/gatt/SearchQueue;)V

    #@5
    .line 75
    .local v0, entry:Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    iput p1, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->connId:I

    #@7
    .line 76
    iput p2, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcType:I

    #@9
    .line 77
    iput p3, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcInstId:I

    #@b
    .line 78
    iput-wide p4, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcUuidLsb:J

    #@d
    .line 79
    iput-wide p6, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcUuidMsb:J

    #@f
    .line 80
    const-wide/16 v1, 0x0

    #@11
    iput-wide v1, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->charUuidLsb:J

    #@13
    .line 81
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/SearchQueue;->mEntries:Ljava/util/List;

    #@15
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@18
    .line 82
    return-void
.end method

.method add(IIIJJIJJ)V
    .registers 15
    .parameter "connId"
    .parameter "srvcType"
    .parameter "srvcInstId"
    .parameter "srvcUuidLsb"
    .parameter "srvcUuidMsb"
    .parameter "charInstId"
    .parameter "charUuidLsb"
    .parameter "charUuidMsb"

    #@0
    .prologue
    .line 88
    new-instance v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;

    #@2
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;-><init>(Lcom/broadcom/bt/service/gatt/SearchQueue;)V

    #@5
    .line 89
    .local v0, entry:Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    iput p1, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->connId:I

    #@7
    .line 90
    iput p2, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcType:I

    #@9
    .line 91
    iput p3, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcInstId:I

    #@b
    .line 92
    iput-wide p4, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcUuidLsb:J

    #@d
    .line 93
    iput-wide p6, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->srvcUuidMsb:J

    #@f
    .line 94
    iput p8, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->charInstId:I

    #@11
    .line 95
    iput-wide p9, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->charUuidLsb:J

    #@13
    .line 96
    iput-wide p11, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->charUuidMsb:J

    #@15
    .line 97
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/SearchQueue;->mEntries:Ljava/util/List;

    #@17
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@1a
    .line 98
    return-void
.end method

.method clear()V
    .registers 2

    #@0
    .prologue
    .line 120
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/SearchQueue;->mEntries:Ljava/util/List;

    #@2
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@5
    .line 121
    return-void
.end method

.method isEmpty()Z
    .registers 2

    #@0
    .prologue
    .line 116
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/SearchQueue;->mEntries:Ljava/util/List;

    #@2
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method pop()Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 101
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/SearchQueue;->mEntries:Ljava/util/List;

    #@3
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;

    #@9
    .line 102
    .local v0, entry:Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    iget-object v1, p0, Lcom/broadcom/bt/service/gatt/SearchQueue;->mEntries:Ljava/util/List;

    #@b
    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    #@e
    .line 103
    return-object v0
.end method

.method removeConnId(I)V
    .registers 5
    .parameter "connId"

    #@0
    .prologue
    .line 107
    iget-object v2, p0, Lcom/broadcom/bt/service/gatt/SearchQueue;->mEntries:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, it:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;>;"
    :cond_6
    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_1a

    #@c
    .line 108
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;

    #@12
    .line 109
    .local v0, entry:Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    iget v2, v0, Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;->connId:I

    #@14
    if-ne v2, p1, :cond_6

    #@16
    .line 110
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    #@19
    goto :goto_6

    #@1a
    .line 113
    .end local v0           #entry:Lcom/broadcom/bt/service/gatt/SearchQueue$Entry;
    :cond_1a
    return-void
.end method
