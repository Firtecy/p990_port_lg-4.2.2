.class public Lcom/broadcom/bt/service/map/MapContentProvider;
.super Landroid/content/ContentProvider;
.source "MapContentProvider.java"


# static fields
.field public static final CONTENT_URL:Landroid/net/Uri; = null

.field public static DEFAULT_TMP_DIR:Ljava/io/File; = null

.field private static final TAG:Ljava/lang/String; = "BtMap.MapContentProvider"

.field public static final URI_PREFIX:Ljava/lang/String; = "content://com.broadcom.bt.map/"


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 65
    const-string v0, "content://com.broadcom.bt.map/"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/broadcom/bt/service/map/MapContentProvider;->CONTENT_URL:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 61
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    return-void
.end method

.method static init(Lcom/broadcom/bt/service/map/MapService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 69
    invoke-static {}, Lcom/broadcom/bt/service/map/MapService;->getDefaultTmpDir()Ljava/io/File;

    #@3
    move-result-object v0

    #@4
    sput-object v0, Lcom/broadcom/bt/service/map/MapContentProvider;->DEFAULT_TMP_DIR:Ljava/io/File;

    #@6
    .line 70
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 6
    .parameter "uri"
    .parameter "s"
    .parameter "as"

    #@0
    .prologue
    .line 123
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Not supported by this provider"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 4
    .parameter "uri"

    #@0
    .prologue
    .line 128
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Not supported by this provider"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 5
    .parameter "uri"
    .parameter "contentvalues"

    #@0
    .prologue
    .line 133
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Not supported by this provider"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public onCreate()Z
    .registers 2

    #@0
    .prologue
    .line 118
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .registers 11
    .parameter "uri"
    .parameter "mode"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/FileNotFoundException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    const/4 v0, 0x0

    #@1
    .line 75
    .local v0, file:Ljava/io/File;
    :try_start_1
    new-instance v1, Ljava/io/File;

    #@3
    sget-object v5, Lcom/broadcom/bt/service/map/MapContentProvider;->DEFAULT_TMP_DIR:Ljava/io/File;

    #@5
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@8
    move-result-object v6

    #@9
    const-string v7, "UTF-8"

    #@b
    invoke-static {v6, v7}, Ljava/net/URLDecoder;->decode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@e
    move-result-object v6

    #@f
    invoke-direct {v1, v5, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_12
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_12} :catch_89

    #@12
    .line 80
    .end local v0           #file:Ljava/io/File;
    .local v1, file:Ljava/io/File;
    const-string v5, "BtMap.MapContentProvider"

    #@14
    new-instance v6, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v7, "Opening file...."

    #@1b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v6

    #@1f
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@22
    move-result-object v7

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v6

    #@2b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 81
    const/4 v3, 0x0

    #@2f
    .line 82
    .local v3, parcelMode:I
    if-eqz p2, :cond_7f

    #@31
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@34
    move-result v5

    #@35
    if-lez v5, :cond_7f

    #@37
    .line 83
    const/16 v5, 0x72

    #@39
    invoke-virtual {p2, v5}, Ljava/lang/String;->indexOf(I)I

    #@3c
    move-result v5

    #@3d
    if-ltz v5, :cond_49

    #@3f
    .line 84
    const-string v5, "BtMap.MapContentProvider"

    #@41
    const-string v6, "Adding read flag...."

    #@43
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 85
    const/high16 v5, 0x1000

    #@48
    or-int/2addr v3, v5

    #@49
    .line 87
    :cond_49
    const/16 v5, 0x63

    #@4b
    invoke-virtual {p2, v5}, Ljava/lang/String;->indexOf(I)I

    #@4e
    move-result v5

    #@4f
    if-ltz v5, :cond_5b

    #@51
    .line 88
    const-string v5, "BtMap.MapContentProvider"

    #@53
    const-string v6, "Adding create flag...."

    #@55
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 89
    const/high16 v5, 0x800

    #@5a
    or-int/2addr v3, v5

    #@5b
    .line 91
    :cond_5b
    const/16 v5, 0x77

    #@5d
    invoke-virtual {p2, v5}, Ljava/lang/String;->indexOf(I)I

    #@60
    move-result v5

    #@61
    if-ltz v5, :cond_6d

    #@63
    .line 92
    const-string v5, "BtMap.MapContentProvider"

    #@65
    const-string v6, "Adding write flag...."

    #@67
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 93
    const/high16 v5, 0x2000

    #@6c
    or-int/2addr v3, v5

    #@6d
    .line 95
    :cond_6d
    const/16 v5, 0x74

    #@6f
    invoke-virtual {p2, v5}, Ljava/lang/String;->indexOf(I)I

    #@72
    move-result v5

    #@73
    if-ltz v5, :cond_7f

    #@75
    .line 96
    const-string v5, "BtMap.MapContentProvider"

    #@77
    const-string v6, "Adding truncate flag...."

    #@79
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 97
    const/high16 v5, 0x400

    #@7e
    or-int/2addr v3, v5

    #@7f
    .line 109
    :cond_7f
    if-nez v3, :cond_83

    #@81
    .line 110
    const/high16 v3, 0x1000

    #@83
    .line 112
    :cond_83
    invoke-static {v1, v3}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    #@86
    move-result-object v2

    #@87
    .local v2, parcel:Landroid/os/ParcelFileDescriptor;
    move-object v0, v1

    #@88
    .line 113
    .end local v1           #file:Ljava/io/File;
    .end local v2           #parcel:Landroid/os/ParcelFileDescriptor;
    .end local v3           #parcelMode:I
    .restart local v0       #file:Ljava/io/File;
    :goto_88
    return-object v2

    #@89
    .line 76
    :catch_89
    move-exception v4

    #@8a
    .line 77
    .local v4, t:Ljava/lang/Throwable;
    const-string v5, "BtMap.MapContentProvider"

    #@8c
    const-string v6, "Error opening internal file for MapContentProvider"

    #@8e
    invoke-static {v5, v6, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@91
    .line 78
    const/4 v2, 0x0

    #@92
    goto :goto_88
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 8
    .parameter "uri"
    .parameter "as"
    .parameter "s"
    .parameter "as1"
    .parameter "s1"

    #@0
    .prologue
    .line 138
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Not supported by this provider"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 7
    .parameter "uri"
    .parameter "contentvalues"
    .parameter "s"
    .parameter "as"

    #@0
    .prologue
    .line 143
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Not supported by this provider"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method
