.class Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;
.super Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;
.source "SmsMmsDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getMsgListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mInfos:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;"
        }
    .end annotation
.end field

.field mMaxSubjectLength:I

.field final synthetic this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 628
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, p1, v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;-><init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;)V

    #@6
    .line 629
    new-instance v0, Ljava/util/LinkedList;

    #@8
    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    #@b
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mInfos:Ljava/util/LinkedList;

    #@d
    return-void
.end method


# virtual methods
.method public init(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V
    .registers 8
    .parameter "requestId"
    .parameter "folderPath"
    .parameter "messageListFilter"
    .parameter "paramFilter"

    #@0
    .prologue
    const/16 v1, 0x14

    #@2
    .line 646
    invoke-super {p0, p1, p2, p3, p4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;->init(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V

    #@5
    .line 649
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@7
    iget-byte v0, v0, Lcom/broadcom/bt/map/MessageListFilter;->mSubjectLength:B

    #@9
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mMaxSubjectLength:I

    #@b
    .line 650
    iget v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mMaxSubjectLength:I

    #@d
    if-le v0, v1, :cond_11

    #@f
    .line 651
    iput v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mMaxSubjectLength:I

    #@11
    .line 653
    :cond_11
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_31

    #@17
    .line 654
    const-string v0, "BtMap.SmsMmsDataSource"

    #@19
    new-instance v1, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v2, "run(): maxSubjectLength="

    #@20
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v1

    #@24
    iget v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mMaxSubjectLength:I

    #@26
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v1

    #@2e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 656
    :cond_31
    return-void
.end method

.method protected initMmsMessageIterator()V
    .registers 10

    #@0
    .prologue
    .line 668
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@2
    #getter for: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;
    invoke-static {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$200(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->createMessageListIterator()Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@c
    .line 669
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@e
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mFolderPath:Ljava/lang/String;

    #@10
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@12
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mContactFilter:Ljava/lang/String;

    #@14
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mParamFilter:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@16
    const/4 v5, 0x1

    #@17
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@19
    #getter for: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I
    invoke-static {v6}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$400(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)I

    #@1c
    move-result v6

    #@1d
    iget-object v7, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@1f
    #calls: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getOwnerNumber()Ljava/lang/String;
    invoke-static {v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$500(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)Ljava/lang/String;

    #@22
    move-result-object v7

    #@23
    iget v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mMaxSubjectLength:I

    #@25
    invoke-virtual/range {v0 .. v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->initMessageInfoIterator(Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Ljava/lang/String;Lcom/broadcom/bt/map/MessageParameterFilter;ZILjava/lang/String;I)V

    #@28
    .line 673
    return-void
.end method

.method protected initSmsMessageIterator()V
    .registers 10

    #@0
    .prologue
    .line 659
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@2
    #getter for: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;
    invoke-static {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$100(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->createMessageListIterator()Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@c
    .line 660
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@e
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mFolderPath:Ljava/lang/String;

    #@10
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@12
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mContactFilter:Ljava/lang/String;

    #@14
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mParamFilter:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@16
    const/4 v5, 0x1

    #@17
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@19
    #getter for: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mPhoneType:I
    invoke-static {v6}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$400(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)I

    #@1c
    move-result v6

    #@1d
    iget-object v7, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@1f
    #calls: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getOwnerNumber()Ljava/lang/String;
    invoke-static {v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$500(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)Ljava/lang/String;

    #@22
    move-result-object v7

    #@23
    iget v8, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mMaxSubjectLength:I

    #@25
    invoke-virtual/range {v0 .. v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->initMessageInfoIterator(Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Ljava/lang/String;Lcom/broadcom/bt/map/MessageParameterFilter;ZILjava/lang/String;I)V

    #@28
    .line 665
    return-void
.end method

.method public processOneMmsEntry()V
    .registers 3

    #@0
    .prologue
    .line 683
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@2
    invoke-virtual {v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->getMessageInfo()Lcom/broadcom/bt/map/MessageInfo;

    #@5
    move-result-object v0

    #@6
    .line 684
    .local v0, m:Lcom/broadcom/bt/map/MessageInfo;
    if-eqz v0, :cond_d

    #@8
    .line 685
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mInfos:Ljava/util/LinkedList;

    #@a
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@d
    .line 687
    :cond_d
    return-void
.end method

.method public processOneSmsEntry()V
    .registers 3

    #@0
    .prologue
    .line 676
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@2
    invoke-virtual {v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->getMessageInfo()Lcom/broadcom/bt/map/MessageInfo;

    #@5
    move-result-object v0

    #@6
    .line 677
    .local v0, m:Lcom/broadcom/bt/map/MessageInfo;
    if-eqz v0, :cond_d

    #@8
    .line 678
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mInfos:Ljava/util/LinkedList;

    #@a
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    #@d
    .line 680
    :cond_d
    return-void
.end method

.method public setResultNoEntries()V
    .registers 6

    #@0
    .prologue
    .line 637
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@2
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mRequestId:Lcom/broadcom/bt/map/RequestId;

    #@4
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mFolderPath:Ljava/lang/String;

    #@6
    const/4 v3, 0x1

    #@7
    const/4 v4, 0x0

    #@8
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLjava/util/List;)V

    #@b
    .line 638
    return-void
.end method

.method public setResultNotSuccess()V
    .registers 6

    #@0
    .prologue
    .line 633
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@2
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mRequestId:Lcom/broadcom/bt/map/RequestId;

    #@4
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mFolderPath:Ljava/lang/String;

    #@6
    const/4 v3, 0x0

    #@7
    const/4 v4, 0x0

    #@8
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLjava/util/List;)V

    #@b
    .line 634
    return-void
.end method

.method public setResultSuccess()V
    .registers 6

    #@0
    .prologue
    .line 641
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@2
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mRequestId:Lcom/broadcom/bt/map/RequestId;

    #@4
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mFolderPath:Ljava/lang/String;

    #@6
    const/4 v3, 0x1

    #@7
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$2;->mInfos:Ljava/util/LinkedList;

    #@9
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageListingResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZLjava/util/List;)V

    #@c
    .line 642
    return-void
.end method
