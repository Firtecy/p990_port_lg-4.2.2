.class Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;
.super Lcom/broadcom/bt/service/sap/IBluetoothSap$Stub;
.source "SapService.java"

# interfaces
.implements Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/sap/SapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BluetoothSapBinder"
.end annotation


# instance fields
.field private mService:Lcom/broadcom/bt/service/sap/SapService;


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/service/sap/SapService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 160
    invoke-direct {p0}, Lcom/broadcom/bt/service/sap/IBluetoothSap$Stub;-><init>()V

    #@3
    .line 161
    iput-object p1, p0, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;->mService:Lcom/broadcom/bt/service/sap/SapService;

    #@5
    .line 162
    return-void
.end method

.method private getService()Lcom/broadcom/bt/service/sap/SapService;
    .registers 2

    #@0
    .prologue
    .line 170
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;->mService:Lcom/broadcom/bt/service/sap/SapService;

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;->mService:Lcom/broadcom/bt/service/sap/SapService;

    #@6
    #calls: Lcom/broadcom/bt/service/sap/SapService;->isAvailable()Z
    invoke-static {v0}, Lcom/broadcom/bt/service/sap/SapService;->access$500(Lcom/broadcom/bt/service/sap/SapService;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_f

    #@c
    .line 171
    iget-object v0, p0, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;->mService:Lcom/broadcom/bt/service/sap/SapService;

    #@e
    .line 173
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method


# virtual methods
.method public cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 165
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;->mService:Lcom/broadcom/bt/service/sap/SapService;

    #@3
    .line 166
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 177
    invoke-direct {p0}, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;->getService()Lcom/broadcom/bt/service/sap/SapService;

    #@3
    move-result-object v0

    #@4
    .line 178
    .local v0, service:Lcom/broadcom/bt/service/sap/SapService;
    if-nez v0, :cond_8

    #@6
    .line 179
    const/4 v1, 0x0

    #@7
    .line 181
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/sap/SapService;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 193
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [I

    #@3
    const/4 v1, 0x0

    #@4
    const/4 v2, 0x2

    #@5
    aput v2, v0, v1

    #@7
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 185
    invoke-direct {p0}, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;->getService()Lcom/broadcom/bt/service/sap/SapService;

    #@3
    move-result-object v0

    #@4
    .line 186
    .local v0, service:Lcom/broadcom/bt/service/sap/SapService;
    if-nez v0, :cond_8

    #@6
    .line 187
    const/4 v1, 0x0

    #@7
    .line 189
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/sap/SapService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 5
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 198
    invoke-direct {p0}, Lcom/broadcom/bt/service/sap/SapService$BluetoothSapBinder;->getService()Lcom/broadcom/bt/service/sap/SapService;

    #@3
    move-result-object v0

    #@4
    .line 199
    .local v0, service:Lcom/broadcom/bt/service/sap/SapService;
    if-nez v0, :cond_d

    #@6
    .line 200
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 202
    :goto_c
    return-object v1

    #@d
    :cond_d
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/sap/SapService;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method
