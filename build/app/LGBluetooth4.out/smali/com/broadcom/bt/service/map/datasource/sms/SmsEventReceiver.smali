.class public Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SmsEventReceiver.java"


# static fields
.field static final ACTION_MESSAGE_SENT:Ljava/lang/String; = "com.broadcom.bt.service.map.sms.MESSAGE_SENT"

.field static final ACTION_MESSAGE_STATUS:Ljava/lang/String; = "com.broadcom.bt.service.map.sms.MESSAGE_STATUS"

.field public static final DBG:Z = true

.field static final EXTRA_URI:Ljava/lang/String; = "MapUri"

.field private static final TAG:Ljava/lang/String; = "BtMap.SmsEventReceiver"


# instance fields
.field private mDataSource:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

.field private mMsgType:B


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)V
    .registers 3
    .parameter "datasource"

    #@0
    .prologue
    .line 90
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    .line 91
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->mDataSource:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@5
    .line 92
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->mDataSource:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@7
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getPhoneType()I

    #@a
    move-result v0

    #@b
    invoke-static {v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getSMSMessageType(I)B

    #@e
    move-result v0

    #@f
    iput-byte v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->mMsgType:B

    #@11
    .line 93
    return-void
.end method

.method private processMessageSentAction(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 8
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 117
    const-string v2, "MapUri"

    #@2
    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/net/Uri;

    #@8
    .line 118
    .local v0, messageUri:Landroid/net/Uri;
    if-nez v0, :cond_12

    #@a
    .line 119
    const-string v2, "BtMap.SmsEventReceiver"

    #@c
    const-string v3, "processMessageSentAction():Unable to get Message URI for message"

    #@e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 136
    :goto_11
    return-void

    #@12
    .line 122
    :cond_12
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->getResultCode()I

    #@15
    move-result v1

    #@16
    .line 124
    .local v1, resultCode:I
    const-string v2, "BtMap.SmsEventReceiver"

    #@18
    new-instance v3, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v4, "processMessageSentAction(): URI="

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, ",resultCode="

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 127
    const/4 v2, -0x1

    #@39
    if-ne v1, v2, :cond_48

    #@3b
    .line 128
    const-string v2, "BtMap.SmsEventReceiver"

    #@3d
    const-string v3, "processMessageSentAction(): message send ok"

    #@3f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 130
    const/4 v2, 0x2

    #@43
    const/4 v3, 0x0

    #@44
    invoke-static {p1, v0, v2, v3}, Landroid/provider/Telephony$Sms;->moveMessageToFolder(Landroid/content/Context;Landroid/net/Uri;II)Z

    #@47
    goto :goto_11

    #@48
    .line 132
    :cond_48
    const-string v2, "BtMap.SmsEventReceiver"

    #@4a
    const-string v3, "processMessageSentAction(): message send failed"

    #@4c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 134
    const/4 v2, 0x5

    #@50
    invoke-static {p1, v0, v2, v1}, Landroid/provider/Telephony$Sms;->moveMessageToFolder(Landroid/content/Context;Landroid/net/Uri;II)Z

    #@53
    goto :goto_11
.end method

.method private processMessageStatusAction(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 13
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 140
    const-string v7, "BtMap.SmsEventReceiver"

    #@2
    const-string v8, "processMessageStatusAction()"

    #@4
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 143
    iget-object v7, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->mDataSource:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@9
    invoke-virtual {v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->isNotificationEnabled()Z

    #@c
    move-result v7

    #@d
    if-nez v7, :cond_17

    #@f
    .line 145
    const-string v7, "BtMap.SmsEventReceiver"

    #@11
    const-string v8, "MAP notification not enabled...Skipping event"

    #@13
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 190
    :goto_16
    return-void

    #@17
    .line 151
    :cond_17
    const-string v7, "MapUri"

    #@19
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@1c
    move-result-object v3

    #@1d
    check-cast v3, Landroid/net/Uri;

    #@1f
    .line 152
    .local v3, messageUri:Landroid/net/Uri;
    if-nez v3, :cond_29

    #@21
    .line 153
    const-string v7, "BtMap.SmsEventReceiver"

    #@23
    const-string v8, "processMessageStatusAction(): invalid messageUri"

    #@25
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_16

    #@29
    .line 156
    :cond_29
    const-string v7, "BtMap.SmsEventReceiver"

    #@2b
    new-instance v8, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v9, "Got message URI = "

    #@32
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v8

    #@36
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@39
    move-result-object v9

    #@3a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v8

    #@3e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v8

    #@42
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 157
    invoke-static {p1, v3}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getSmsMsgFolderType(Landroid/content/Context;Landroid/net/Uri;)I

    #@48
    move-result v5

    #@49
    .line 158
    .local v5, smsMsgType:I
    invoke-static {v5}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@4c
    move-result-object v0

    #@4d
    .line 159
    .local v0, folderPath:Ljava/lang/String;
    if-nez v0, :cond_68

    #@4f
    .line 160
    const-string v7, "BtMap.SmsEventReceiver"

    #@51
    new-instance v8, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v9, "Invalid folder path for SMS message type:"

    #@58
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v8

    #@5c
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v8

    #@60
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v8

    #@64
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    goto :goto_16

    #@68
    .line 163
    :cond_68
    invoke-virtual {v3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@6b
    move-result-object v7

    #@6c
    invoke-static {v7}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->encodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@6f
    move-result-object v2

    #@70
    .line 165
    .local v2, messageId:Ljava/lang/String;
    const-string v7, "pdu"

    #@72
    invoke-virtual {p2, v7}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    #@75
    move-result-object v4

    #@76
    check-cast v4, [B

    #@78
    .line 166
    .local v4, pdu:[B
    if-eqz v4, :cond_c1

    #@7a
    .line 167
    invoke-static {v4}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    #@7d
    move-result-object v1

    #@7e
    .line 169
    .local v1, message:Landroid/telephony/SmsMessage;
    if-nez v1, :cond_88

    #@80
    .line 171
    const-string v7, "BtMap.SmsEventReceiver"

    #@82
    const-string v8, "processMessageStatusAction(): message is null!"

    #@84
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    goto :goto_16

    #@88
    .line 175
    :cond_88
    invoke-virtual {v1}, Landroid/telephony/SmsMessage;->getStatus()I

    #@8b
    move-result v6

    #@8c
    .line 176
    .local v6, status:I
    if-nez v6, :cond_9f

    #@8e
    .line 178
    const-string v7, "BtMap.SmsEventReceiver"

    #@90
    const-string v8, "processMessageStatusAction(): sending devliery success notification"

    #@92
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 180
    iget-object v7, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->mDataSource:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@97
    iget-byte v8, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->mMsgType:B

    #@99
    const/4 v9, 0x1

    #@9a
    invoke-virtual {v7, v2, v8, v0, v9}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyDeliveryStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@9d
    goto/16 :goto_16

    #@9f
    .line 183
    :cond_9f
    const-string v7, "BtMap.SmsEventReceiver"

    #@a1
    new-instance v8, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v9, "processMessageStatusAction(): error deliverying message. ErrorCode= "

    #@a8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v8

    #@ac
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@af
    move-result-object v8

    #@b0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v8

    #@b4
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 185
    iget-object v7, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->mDataSource:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@b9
    iget-byte v8, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->mMsgType:B

    #@bb
    const/4 v9, 0x0

    #@bc
    invoke-virtual {v7, v2, v8, v0, v9}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyDeliveryStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@bf
    goto/16 :goto_16

    #@c1
    .line 188
    .end local v1           #message:Landroid/telephony/SmsMessage;
    .end local v6           #status:I
    :cond_c1
    const-string v7, "BtMap.SmsEventReceiver"

    #@c3
    const-string v8, "processMessageStatusAction(): error delivery report with no pdus"

    #@c5
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c8
    goto/16 :goto_16
.end method


# virtual methods
.method public createFilter()Landroid/content/IntentFilter;
    .registers 3

    #@0
    .prologue
    .line 96
    new-instance v0, Landroid/content/IntentFilter;

    #@2
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@5
    .line 97
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v1, "com.broadcom.bt.service.map.sms.MESSAGE_SENT"

    #@7
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@a
    .line 98
    const-string v1, "com.broadcom.bt.service.map.sms.MESSAGE_STATUS"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@f
    .line 99
    return-object v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 103
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 105
    .local v0, action:Ljava/lang/String;
    const-string v1, "BtMap.SmsEventReceiver"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "onReceive(): action = "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 107
    const-string v1, "com.broadcom.bt.service.map.sms.MESSAGE_SENT"

    #@1e
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_28

    #@24
    .line 108
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->processMessageSentAction(Landroid/content/Context;Landroid/content/Intent;)V

    #@27
    .line 114
    :goto_27
    return-void

    #@28
    .line 109
    :cond_28
    const-string v1, "com.broadcom.bt.service.map.sms.MESSAGE_STATUS"

    #@2a
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d
    move-result v1

    #@2e
    if-eqz v1, :cond_34

    #@30
    .line 110
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventReceiver;->processMessageStatusAction(Landroid/content/Context;Landroid/content/Intent;)V

    #@33
    goto :goto_27

    #@34
    .line 112
    :cond_34
    const-string v1, "BtMap.SmsEventReceiver"

    #@36
    new-instance v2, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v3, "Unable to process action: "

    #@3d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v2

    #@49
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_27
.end method
