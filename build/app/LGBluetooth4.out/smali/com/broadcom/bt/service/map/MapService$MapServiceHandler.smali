.class Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;
.super Landroid/os/Handler;
.source "MapService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/MapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MapServiceHandler"
.end annotation


# static fields
.field private static final MSG_DISABLE_DS_TIMEOUT:I = 0x3e9

.field private static final MSG_ENABLE_DS_TIMEOUT:I = 0x3e8

.field private static final MSG_GET_FOLDER_ENTRY:I = 0xc8

.field private static final MSG_GET_FOLDER_ENTRY_TIMEOUT:I = 0x3ea

.field private static final MSG_GET_MSG:I = 0xfc

.field private static final MSG_GET_MSG_ENTRY:I = 0xfb

.field private static final MSG_GET_MSG_ENTRY_TIMEOUT:I = 0x3ec

.field private static final MSG_GET_MSG_LISTING:I = 0xfa

.field private static final MSG_GET_MSG_LISTING_TIMEOUT:I = 0x3eb

.field private static final MSG_GET_MSG_TIMEOUT:I = 0x3ed

.field private static final MSG_NOTIFICATION:I = 0x19a

.field private static final MSG_NOTIFICATION_CHANGED:I = 0x199

.field private static final MSG_NOTIFICATION_COMPLETED:I = 0x58c

.field private static final MSG_NOTIFICATION_TIMEOUT:I = 0x582

.field private static final MSG_NOTIFY_DS_CLIENT_CONNECTION_CHANGED:I = 0x4

.field private static final MSG_NOTIFY_DS_REG_CHANGED:I = 0x2

.field private static final MSG_NOTIFY_DS_STATE_CHANGED:I = 0x3

.field private static final MSG_NOTIFY_PROFILE_STATE_CHANGED:I = 0x1

.field private static final MSG_PUSH_MSG:I = 0x194

.field private static final MSG_PUSH_MSG_RESULT:I = 0x195

.field private static final MSG_PUSH_MSG_TIMEOUT:I = 0x3ee

.field private static final MSG_RETURN_MSG:I = 0x193

.field private static final MSG_SET_DS_STATE:I = 0x64

.field private static final MSG_SET_FOLDER_LISTING:I = 0x190

.field private static final MSG_SET_MSG_DELETED_RESULT:I = 0x198

.field private static final MSG_SET_MSG_DELETED_TIMEOUT:I = 0x3ef

.field private static final MSG_SET_MSG_LISTING:I = 0x191

.field private static final MSG_SET_MSG_LISTING_COUNT:I = 0x192

.field private static final MSG_SET_MSG_STATUS:I = 0x197

.field private static final MSG_UPDATE_HANDLE:I = 0x19b

.field private static final MSG_UPDATE_INBOX:I = 0x196


# instance fields
.field private mTimeoutMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/broadcom/bt/map/RequestId;",
            "Lcom/broadcom/bt/map/RequestId;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/broadcom/bt/service/map/MapService;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/map/MapService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 938
    iput-object p1, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    .line 1247
    new-instance v0, Ljava/util/HashMap;

    #@7
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@a
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->mTimeoutMap:Ljava/util/HashMap;

    #@c
    return-void
.end method

.method static synthetic access$200(Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;ILcom/broadcom/bt/map/RequestId;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 938
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->stopTimeoutTimer(ILcom/broadcom/bt/map/RequestId;)V

    #@3
    return-void
.end method

.method private dispatchNotification(Landroid/os/Message;)V
    .registers 12
    .parameter "m"

    #@0
    .prologue
    const/4 v7, -0x1

    #@1
    .line 1744
    const-string v1, "BtMap.MapService"

    #@3
    const-string v2, "dispatchNotification(): dispatching notification..."

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 1745
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@b
    move-result-object v9

    #@c
    .line 1748
    .local v9, b:Landroid/os/Bundle;
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@f
    move-result-object v0

    #@10
    .line 1751
    .local v0, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-eqz v0, :cond_22

    #@12
    const-string v1, "p"

    #@14
    invoke-virtual {v9, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    if-eqz v1, :cond_22

    #@1a
    const-string v1, "d"

    #@1c
    invoke-virtual {v9, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    if-nez v1, :cond_2a

    #@22
    .line 1753
    :cond_22
    const-string v1, "BtMap.MapService"

    #@24
    const-string v2, "MSG_NOTIFICATION: error. DataSourceManager not available"

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1762
    :goto_29
    return-void

    #@2a
    .line 1757
    :cond_2a
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2c
    iget-object v1, v1, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@2e
    const/16 v2, 0x582

    #@30
    const-wide/16 v3, 0xbb8

    #@32
    invoke-virtual {v1, v2, v3, v4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendEmptyMessageDelayed(IJ)Z

    #@35
    .line 1759
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@37
    const-string v2, "p"

    #@39
    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    const-string v3, "d"

    #@3f
    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    const-string v4, "m"

    #@45
    invoke-virtual {v9, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    const-string v5, "t"

    #@4b
    invoke-virtual {v9, v5, v7}, Landroid/os/Bundle;->getByte(Ljava/lang/String;B)Ljava/lang/Byte;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/Byte;->byteValue()B

    #@52
    move-result v5

    #@53
    const-string v6, "n"

    #@55
    invoke-virtual {v9, v6, v7}, Landroid/os/Bundle;->getByte(Ljava/lang/String;B)Ljava/lang/Byte;

    #@58
    move-result-object v6

    #@59
    invoke-virtual {v6}, Ljava/lang/Byte;->byteValue()B

    #@5c
    move-result v6

    #@5d
    const-string v7, "f"

    #@5f
    invoke-virtual {v9, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@62
    move-result-object v7

    #@63
    const-string v8, "o"

    #@65
    invoke-virtual {v9, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@68
    move-result-object v8

    #@69
    invoke-virtual/range {v0 .. v8}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->sendNotification(Lcom/broadcom/bt/service/map/MapService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V

    #@6c
    goto :goto_29
.end method

.method private getRequestIdFromTimeoutMessage(Landroid/os/Message;)Lcom/broadcom/bt/map/RequestId;
    .registers 3
    .parameter "m"

    #@0
    .prologue
    .line 1262
    if-eqz p1, :cond_7

    #@2
    .line 1263
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4
    check-cast v0, Lcom/broadcom/bt/map/RequestId;

    #@6
    .line 1265
    :goto_6
    return-object v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method private startTimeoutTimer(IILcom/broadcom/bt/map/RequestId;)V
    .registers 7
    .parameter "timeoutType"
    .parameter "timeoutMs"
    .parameter "requestId"

    #@0
    .prologue
    .line 1250
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->mTimeoutMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p3, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    .line 1251
    invoke-virtual {p0, p1, p3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 1252
    .local v0, timeoutMsg:Landroid/os/Message;
    int-to-long v1, p2

    #@a
    invoke-virtual {p0, v0, v1, v2}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@d
    .line 1253
    return-void
.end method

.method private stopTimeoutTimer(ILcom/broadcom/bt/map/RequestId;)V
    .registers 5
    .parameter "timeoutType"
    .parameter "requestId"

    #@0
    .prologue
    .line 1256
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->mTimeoutMap:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/broadcom/bt/map/RequestId;

    #@8
    .line 1257
    .local v0, timerMatchValue:Lcom/broadcom/bt/map/RequestId;
    if-eqz v0, :cond_d

    #@a
    .line 1258
    invoke-virtual {p0, p1, v0}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->removeMessages(ILjava/lang/Object;)V

    #@d
    .line 1260
    :cond_d
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 30
    .parameter "m"

    #@0
    .prologue
    .line 1270
    move-object/from16 v0, p1

    #@2
    iget v2, v0, Landroid/os/Message;->what:I

    #@4
    sparse-switch v2, :sswitch_data_802

    #@7
    .line 1741
    :cond_7
    :goto_7
    return-void

    #@8
    .line 1272
    :sswitch_8
    move-object/from16 v0, p0

    #@a
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@c
    move-object/from16 v0, p1

    #@e
    iget v5, v0, Landroid/os/Message;->arg1:I

    #@10
    #calls: Lcom/broadcom/bt/service/map/MapService;->profileStateChanged(I)V
    invoke-static {v2, v5}, Lcom/broadcom/bt/service/map/MapService;->access$300(Lcom/broadcom/bt/service/map/MapService;I)V

    #@13
    goto :goto_7

    #@14
    .line 1276
    :sswitch_14
    move-object/from16 v0, p1

    #@16
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18
    move-object/from16 v16, v0

    #@1a
    check-cast v16, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@1c
    .line 1277
    .local v16, ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    move-object/from16 v0, p1

    #@1e
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@20
    const/4 v5, 0x1

    #@21
    if-ne v2, v5, :cond_31

    #@23
    const/16 v21, 0x1

    #@25
    .line 1278
    .local v21, isRegistered:Z
    :goto_25
    move-object/from16 v0, p0

    #@27
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@29
    move-object/from16 v0, v16

    #@2b
    move/from16 v1, v21

    #@2d
    #calls: Lcom/broadcom/bt/service/map/MapService;->datasourceRegistrationChanged(Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V
    invoke-static {v2, v0, v1}, Lcom/broadcom/bt/service/map/MapService;->access$400(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V

    #@30
    goto :goto_7

    #@31
    .line 1277
    .end local v21           #isRegistered:Z
    :cond_31
    const/16 v21, 0x0

    #@33
    goto :goto_25

    #@34
    .line 1282
    .end local v16           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :sswitch_34
    move-object/from16 v0, p1

    #@36
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@38
    move-object/from16 v16, v0

    #@3a
    check-cast v16, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@3c
    .line 1283
    .restart local v16       #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    move-object/from16 v0, p1

    #@3e
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@40
    const/4 v5, 0x1

    #@41
    if-ne v2, v5, :cond_a9

    #@43
    const/16 v22, 0x1

    #@45
    .line 1284
    .local v22, isStartRequest:Z
    :goto_45
    move-object/from16 v0, p1

    #@47
    iget v2, v0, Landroid/os/Message;->arg2:I

    #@49
    const/4 v5, 0x1

    #@4a
    if-ne v2, v5, :cond_ac

    #@4c
    const/16 v20, 0x1

    #@4e
    .line 1286
    .local v20, hasError:Z
    :goto_4e
    const-string v2, "BtMap.MapService"

    #@50
    new-instance v5, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v6, "MSG_NOTIFY_DS_STATE_CHANGED: isStartRequest="

    #@57
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v5

    #@5b
    move/from16 v0, v22

    #@5d
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@60
    move-result-object v5

    #@61
    const-string v6, ", hasError="

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    move/from16 v0, v20

    #@69
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    const-string v6, ",ds="

    #@6f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v5

    #@73
    move-object/from16 v0, v16

    #@75
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v5

    #@79
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 1289
    if-eqz v22, :cond_ba

    #@82
    .line 1291
    move-object/from16 v0, v16

    #@84
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@86
    .line 1292
    .local v13, callback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;
    if-eqz v13, :cond_8d

    #@88
    .line 1294
    :try_start_88
    move/from16 v0, v20

    #@8a
    invoke-interface {v13, v0}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onStartCompleted(Z)V
    :try_end_8d
    .catch Ljava/lang/Throwable; {:try_start_88 .. :try_end_8d} :catch_af

    #@8d
    .line 1301
    :cond_8d
    :goto_8d
    if-nez v20, :cond_7

    #@8f
    .line 1302
    move-object/from16 v0, p0

    #@91
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@93
    move-object/from16 v0, v16

    #@95
    iget-object v3, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@97
    move-object/from16 v0, v16

    #@99
    iget-object v4, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderDisplayName:Ljava/lang/String;

    #@9b
    move-object/from16 v0, v16

    #@9d
    iget-object v5, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@9f
    move-object/from16 v0, v16

    #@a1
    iget-object v6, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsDisplayName:Ljava/lang/String;

    #@a3
    const/4 v7, 0x1

    #@a4
    #calls: Lcom/broadcom/bt/service/map/MapService;->notifyDatasourceStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    invoke-static/range {v2 .. v7}, Lcom/broadcom/bt/service/map/MapService;->access$500(Lcom/broadcom/bt/service/map/MapService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    #@a7
    goto/16 :goto_7

    #@a9
    .line 1283
    .end local v13           #callback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;
    .end local v20           #hasError:Z
    .end local v22           #isStartRequest:Z
    :cond_a9
    const/16 v22, 0x0

    #@ab
    goto :goto_45

    #@ac
    .line 1284
    .restart local v22       #isStartRequest:Z
    :cond_ac
    const/16 v20, 0x0

    #@ae
    goto :goto_4e

    #@af
    .line 1295
    .restart local v13       #callback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;
    .restart local v20       #hasError:Z
    :catch_af
    move-exception v27

    #@b0
    .line 1296
    .local v27, t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@b2
    const-string v5, "MapServiceHandler: MSG_NOTIFY_DS_MSE_STATE_CHANGED: error calling datasource onStartCompleted() callback"

    #@b4
    move-object/from16 v0, v27

    #@b6
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b9
    goto :goto_8d

    #@ba
    .line 1307
    .end local v13           #callback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;
    .end local v27           #t:Ljava/lang/Throwable;
    :cond_ba
    move-object/from16 v0, v16

    #@bc
    iget-object v13, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@be
    .line 1308
    .restart local v13       #callback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;
    if-eqz v13, :cond_c5

    #@c0
    .line 1310
    :try_start_c0
    move/from16 v0, v20

    #@c2
    invoke-interface {v13, v0}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onStopCompleted(Z)V
    :try_end_c5
    .catch Ljava/lang/Throwable; {:try_start_c0 .. :try_end_c5} :catch_e1

    #@c5
    .line 1317
    :cond_c5
    :goto_c5
    if-nez v20, :cond_7

    #@c7
    .line 1318
    move-object/from16 v0, p0

    #@c9
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@cb
    move-object/from16 v0, v16

    #@cd
    iget-object v3, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@cf
    move-object/from16 v0, v16

    #@d1
    iget-object v4, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderDisplayName:Ljava/lang/String;

    #@d3
    move-object/from16 v0, v16

    #@d5
    iget-object v5, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@d7
    move-object/from16 v0, v16

    #@d9
    iget-object v6, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsDisplayName:Ljava/lang/String;

    #@db
    const/4 v7, 0x0

    #@dc
    #calls: Lcom/broadcom/bt/service/map/MapService;->notifyDatasourceStateChanged(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    invoke-static/range {v2 .. v7}, Lcom/broadcom/bt/service/map/MapService;->access$500(Lcom/broadcom/bt/service/map/MapService;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    #@df
    goto/16 :goto_7

    #@e1
    .line 1311
    :catch_e1
    move-exception v27

    #@e2
    .line 1312
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@e4
    const-string v5, "MapServiceHandler: MSG_NOTIFY_DS_MSE_STATE_CHANGED: error calling datasource onStopCompleted() callback"

    #@e6
    move-object/from16 v0, v27

    #@e8
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@eb
    goto :goto_c5

    #@ec
    .line 1326
    .end local v13           #callback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;
    .end local v16           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    .end local v20           #hasError:Z
    .end local v22           #isStartRequest:Z
    .end local v27           #t:Ljava/lang/Throwable;
    :sswitch_ec
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@ef
    move-result-object v14

    #@f0
    .line 1328
    .local v14, data:Landroid/os/Bundle;
    if-nez v14, :cond_fb

    #@f2
    .line 1329
    const-string v2, "BtMap.MapService"

    #@f4
    const-string v5, "MSG_SET_DS_STATE: data is null"

    #@f6
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f9
    goto/16 :goto_7

    #@fb
    .line 1333
    :cond_fb
    const-string v2, "providerId"

    #@fd
    invoke-virtual {v14, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@100
    move-result-object v23

    #@101
    .line 1334
    .local v23, providerId:Ljava/lang/String;
    const-string v2, "dsId"

    #@103
    invoke-virtual {v14, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@106
    move-result-object v17

    #@107
    .line 1336
    .local v17, dsId:Ljava/lang/String;
    if-eqz v17, :cond_10b

    #@109
    if-nez v23, :cond_114

    #@10b
    .line 1337
    :cond_10b
    const-string v2, "BtMap.MapService"

    #@10d
    const-string v5, "MSG_SET_DS_STATE: dsId or providerId is null"

    #@10f
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@112
    goto/16 :goto_7

    #@114
    .line 1341
    :cond_114
    move-object/from16 v0, p1

    #@116
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@118
    const/4 v5, 0x1

    #@119
    if-ne v2, v5, :cond_12c

    #@11b
    const/16 v19, 0x1

    #@11d
    .line 1342
    .local v19, enable:Z
    :goto_11d
    if-eqz v19, :cond_12f

    #@11f
    .line 1343
    move-object/from16 v0, p0

    #@121
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@123
    move-object/from16 v0, v23

    #@125
    move-object/from16 v1, v17

    #@127
    #calls: Lcom/broadcom/bt/service/map/MapService;->processEnableDatasource(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v0, v1}, Lcom/broadcom/bt/service/map/MapService;->access$600(Lcom/broadcom/bt/service/map/MapService;Ljava/lang/String;Ljava/lang/String;)V

    #@12a
    goto/16 :goto_7

    #@12c
    .line 1341
    .end local v19           #enable:Z
    :cond_12c
    const/16 v19, 0x0

    #@12e
    goto :goto_11d

    #@12f
    .line 1345
    .restart local v19       #enable:Z
    :cond_12f
    move-object/from16 v0, p0

    #@131
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@133
    move-object/from16 v0, v23

    #@135
    move-object/from16 v1, v17

    #@137
    #calls: Lcom/broadcom/bt/service/map/MapService;->processDisableDatasource(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v2, v0, v1}, Lcom/broadcom/bt/service/map/MapService;->access$700(Lcom/broadcom/bt/service/map/MapService;Ljava/lang/String;Ljava/lang/String;)V

    #@13a
    goto/16 :goto_7

    #@13c
    .line 1351
    .end local v14           #data:Landroid/os/Bundle;
    .end local v17           #dsId:Ljava/lang/String;
    .end local v19           #enable:Z
    .end local v23           #providerId:Ljava/lang/String;
    :sswitch_13c
    move-object/from16 v0, p1

    #@13e
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@140
    move-object/from16 v16, v0

    #@142
    check-cast v16, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@144
    .line 1352
    .restart local v16       #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    const-string v2, "BtMap.MapService"

    #@146
    new-instance v5, Ljava/lang/StringBuilder;

    #@148
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@14b
    const-string v6, "Timeout while enabling datasource "

    #@14d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v5

    #@151
    move-object/from16 v0, v16

    #@153
    iget-object v6, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@155
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@158
    move-result-object v5

    #@159
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v5

    #@15d
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@160
    .line 1353
    move-object/from16 v0, p0

    #@162
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@164
    move-object/from16 v0, v16

    #@166
    iget v5, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@168
    const/4 v6, 0x1

    #@169
    #calls: Lcom/broadcom/bt/service/map/MapService;->onMSEInstanceStarted(II)V
    invoke-static {v2, v5, v6}, Lcom/broadcom/bt/service/map/MapService;->access$800(Lcom/broadcom/bt/service/map/MapService;II)V

    #@16c
    goto/16 :goto_7

    #@16e
    .line 1358
    .end local v16           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :sswitch_16e
    move-object/from16 v0, p1

    #@170
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@172
    move-object/from16 v16, v0

    #@174
    check-cast v16, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@176
    .line 1359
    .restart local v16       #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    const-string v2, "BtMap.MapService"

    #@178
    new-instance v5, Ljava/lang/StringBuilder;

    #@17a
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17d
    const-string v6, "Timeout while disabling datasource "

    #@17f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v5

    #@183
    move-object/from16 v0, v16

    #@185
    iget-object v6, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@187
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v5

    #@18b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18e
    move-result-object v5

    #@18f
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@192
    .line 1360
    move-object/from16 v0, p0

    #@194
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@196
    move-object/from16 v0, v16

    #@198
    iget v5, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@19a
    const/4 v6, 0x1

    #@19b
    #calls: Lcom/broadcom/bt/service/map/MapService;->onMSEInstanceStopped(II)V
    invoke-static {v2, v5, v6}, Lcom/broadcom/bt/service/map/MapService;->access$900(Lcom/broadcom/bt/service/map/MapService;II)V

    #@19e
    goto/16 :goto_7

    #@1a0
    .line 1365
    .end local v16           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :sswitch_1a0
    move-object/from16 v0, p1

    #@1a2
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a4
    move-object/from16 v16, v0

    #@1a6
    check-cast v16, Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@1a8
    .line 1367
    .restart local v16       #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :try_start_1a8
    move-object/from16 v0, v16

    #@1aa
    iget-object v5, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@1ac
    move-object/from16 v0, p1

    #@1ae
    iget v2, v0, Landroid/os/Message;->arg1:I

    #@1b0
    const/4 v6, 0x1

    #@1b1
    if-ne v2, v6, :cond_1c5

    #@1b3
    const/4 v2, 0x1

    #@1b4
    :goto_1b4
    invoke-interface {v5, v2}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onClientConnectionStateChanged(Z)V
    :try_end_1b7
    .catch Ljava/lang/Throwable; {:try_start_1a8 .. :try_end_1b7} :catch_1b9

    #@1b7
    goto/16 :goto_7

    #@1b9
    .line 1368
    :catch_1b9
    move-exception v27

    #@1ba
    .line 1369
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@1bc
    const-string v5, "MSG_NOTIFY_DS_CLIENT_CONNECTIONS_CHANGED: unable to call DS onClientConnectionStateChanged"

    #@1be
    move-object/from16 v0, v27

    #@1c0
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@1c3
    goto/16 :goto_7

    #@1c5
    .line 1367
    .end local v27           #t:Ljava/lang/Throwable;
    :cond_1c5
    const/4 v2, 0x0

    #@1c6
    goto :goto_1b4

    #@1c7
    .line 1376
    .end local v16           #ds:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    :sswitch_1c7
    :try_start_1c7
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1ca
    move-result-object v12

    #@1cb
    .line 1377
    .local v12, b:Landroid/os/Bundle;
    new-instance v3, Lcom/broadcom/bt/map/RequestId;

    #@1cd
    const-string v2, "s"

    #@1cf
    const/4 v5, -0x1

    #@1d0
    invoke-virtual {v12, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@1d3
    move-result v2

    #@1d4
    const-string v5, "e"

    #@1d6
    const/4 v6, -0x1

    #@1d7
    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@1da
    move-result v5

    #@1db
    invoke-direct {v3, v2, v5}, Lcom/broadcom/bt/map/RequestId;-><init>(II)V

    #@1de
    .line 1380
    .local v3, requestId:Lcom/broadcom/bt/map/RequestId;
    const/16 v2, 0x3ea

    #@1e0
    const/16 v5, 0x2710

    #@1e2
    move-object/from16 v0, p0

    #@1e4
    invoke-direct {v0, v2, v5, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->startTimeoutTimer(IILcom/broadcom/bt/map/RequestId;)V

    #@1e7
    .line 1381
    move-object/from16 v0, p0

    #@1e9
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@1eb
    const-string v5, "p"

    #@1ed
    invoke-virtual {v12, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1f0
    move-result-object v4

    #@1f1
    const-string v5, "f"

    #@1f3
    const/4 v6, 0x0

    #@1f4
    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@1f7
    move-result v5

    #@1f8
    const-string v6, "n"

    #@1fa
    const-wide/16 v7, -0x1

    #@1fc
    invoke-virtual {v12, v6, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    #@1ff
    move-result-wide v6

    #@200
    invoke-static/range {v2 .. v7}, Lcom/broadcom/bt/service/map/MapSessionManager;->getFolderEntry(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZJ)V
    :try_end_203
    .catch Ljava/lang/Throwable; {:try_start_1c7 .. :try_end_203} :catch_205

    #@203
    goto/16 :goto_7

    #@205
    .line 1383
    .end local v3           #requestId:Lcom/broadcom/bt/map/RequestId;
    .end local v12           #b:Landroid/os/Bundle;
    :catch_205
    move-exception v27

    #@206
    .line 1384
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@208
    const-string v5, "MSG_GET_MESSAGE_ENTRY: error"

    #@20a
    move-object/from16 v0, v27

    #@20c
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@20f
    goto/16 :goto_7

    #@211
    .line 1390
    .end local v27           #t:Ljava/lang/Throwable;
    :sswitch_211
    :try_start_211
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@214
    move-result-object v12

    #@215
    .line 1392
    .restart local v12       #b:Landroid/os/Bundle;
    if-eqz v12, :cond_227

    #@217
    const-string v2, "r"

    #@219
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@21c
    move-result-object v2

    #@21d
    if-eqz v2, :cond_227

    #@21f
    const-string v2, "p"

    #@221
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@224
    move-result-object v2

    #@225
    if-nez v2, :cond_23c

    #@227
    .line 1393
    :cond_227
    const-string v2, "BtMap.MapService"

    #@229
    const-string v5, "MSG_SET_FOLDER_LISTING: b or getparcelabel or getString is null"

    #@22b
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_22e
    .catch Ljava/lang/Throwable; {:try_start_211 .. :try_end_22e} :catch_230

    #@22e
    goto/16 :goto_7

    #@230
    .line 1399
    .end local v12           #b:Landroid/os/Bundle;
    :catch_230
    move-exception v27

    #@231
    .line 1400
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@233
    const-string v5, "MSG_SET_FOLDER_LISTING: error"

    #@235
    move-object/from16 v0, v27

    #@237
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@23a
    goto/16 :goto_7

    #@23c
    .line 1397
    .end local v27           #t:Ljava/lang/Throwable;
    .restart local v12       #b:Landroid/os/Bundle;
    :cond_23c
    :try_start_23c
    move-object/from16 v0, p0

    #@23e
    iget-object v6, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@240
    const-string v2, "r"

    #@242
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@245
    move-result-object v2

    #@246
    check-cast v2, Lcom/broadcom/bt/map/RequestId;

    #@248
    const-string v5, "p"

    #@24a
    invoke-virtual {v12, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@24d
    move-result-object v7

    #@24e
    move-object/from16 v0, p1

    #@250
    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@252
    check-cast v5, Ljava/util/List;

    #@254
    invoke-static {v6, v2, v7, v5}, Lcom/broadcom/bt/service/map/MapSessionManager;->setFolderListing(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;)V
    :try_end_257
    .catch Ljava/lang/Throwable; {:try_start_23c .. :try_end_257} :catch_230

    #@257
    goto/16 :goto_7

    #@259
    .line 1405
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_259
    invoke-direct/range {p0 .. p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->getRequestIdFromTimeoutMessage(Landroid/os/Message;)Lcom/broadcom/bt/map/RequestId;

    #@25c
    move-result-object v24

    #@25d
    .line 1407
    .local v24, r:Lcom/broadcom/bt/map/RequestId;
    if-nez v24, :cond_268

    #@25f
    .line 1408
    const-string v2, "BtMap.MapService"

    #@261
    const-string v5, "MSG_GET_FOLDER_ENTRY_TIMEOUT: r is null"

    #@263
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@266
    goto/16 :goto_7

    #@268
    .line 1412
    :cond_268
    const-string v2, "BtMap.MapService"

    #@26a
    new-instance v5, Ljava/lang/StringBuilder;

    #@26c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@26f
    const-string v6, "Timeout while getting folder for request "

    #@271
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@274
    move-result-object v5

    #@275
    move-object/from16 v0, v24

    #@277
    iget v6, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@279
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@27c
    move-result-object v5

    #@27d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@280
    move-result-object v5

    #@281
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@284
    .line 1414
    :try_start_284
    move-object/from16 v0, p0

    #@286
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@288
    const/4 v5, 0x0

    #@289
    move-object/from16 v0, v24

    #@28b
    invoke-virtual {v2, v5, v0}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderNoEntryResult(ZLcom/broadcom/bt/map/RequestId;)V
    :try_end_28e
    .catch Ljava/lang/Throwable; {:try_start_284 .. :try_end_28e} :catch_290

    #@28e
    goto/16 :goto_7

    #@290
    .line 1415
    :catch_290
    move-exception v27

    #@291
    .line 1416
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@293
    const-string v5, "MSG_GET_FOLDER_ENTRY_TIMEOUT: error"

    #@295
    move-object/from16 v0, v27

    #@297
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@29a
    goto/16 :goto_7

    #@29c
    .line 1421
    .end local v24           #r:Lcom/broadcom/bt/map/RequestId;
    .end local v27           #t:Ljava/lang/Throwable;
    :sswitch_29c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@29f
    move-result-object v12

    #@2a0
    .line 1422
    .restart local v12       #b:Landroid/os/Bundle;
    new-instance v3, Lcom/broadcom/bt/map/RequestId;

    #@2a2
    const-string v2, "s"

    #@2a4
    const/4 v5, -0x1

    #@2a5
    invoke-virtual {v12, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@2a8
    move-result v2

    #@2a9
    const-string v5, "e"

    #@2ab
    const/4 v6, -0x1

    #@2ac
    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@2af
    move-result v5

    #@2b0
    invoke-direct {v3, v2, v5}, Lcom/broadcom/bt/map/RequestId;-><init>(II)V

    #@2b3
    .line 1424
    .restart local v3       #requestId:Lcom/broadcom/bt/map/RequestId;
    const/16 v2, 0x3eb

    #@2b5
    const/16 v5, 0x2710

    #@2b7
    move-object/from16 v0, p0

    #@2b9
    invoke-direct {v0, v2, v5, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->startTimeoutTimer(IILcom/broadcom/bt/map/RequestId;)V

    #@2bc
    .line 1425
    move-object/from16 v0, p0

    #@2be
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2c0
    const-string v5, "p"

    #@2c2
    invoke-virtual {v12, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@2c5
    move-result-object v4

    #@2c6
    move-object/from16 v0, p1

    #@2c8
    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2ca
    check-cast v5, Lcom/broadcom/bt/map/MessageListFilter;

    #@2cc
    const-string v6, "m"

    #@2ce
    const-wide/16 v7, -0x1

    #@2d0
    invoke-virtual {v12, v6, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    #@2d3
    move-result-wide v6

    #@2d4
    invoke-static/range {v2 .. v7}, Lcom/broadcom/bt/service/map/MapSessionManager;->getMessageListInfo(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;J)V

    #@2d7
    goto/16 :goto_7

    #@2d9
    .line 1431
    .end local v3           #requestId:Lcom/broadcom/bt/map/RequestId;
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_2d9
    invoke-direct/range {p0 .. p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->getRequestIdFromTimeoutMessage(Landroid/os/Message;)Lcom/broadcom/bt/map/RequestId;

    #@2dc
    move-result-object v24

    #@2dd
    .line 1433
    .restart local v24       #r:Lcom/broadcom/bt/map/RequestId;
    if-nez v24, :cond_2e8

    #@2df
    .line 1434
    const-string v2, "BtMap.MapService"

    #@2e1
    const-string v5, "MSG_GET_MSG_LISTING_TIMEOUT: r is null"

    #@2e3
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e6
    goto/16 :goto_7

    #@2e8
    .line 1438
    :cond_2e8
    const-string v2, "BtMap.MapService"

    #@2ea
    new-instance v5, Ljava/lang/StringBuilder;

    #@2ec
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2ef
    const-string v6, "Timeout while getting message list info for request "

    #@2f1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f4
    move-result-object v5

    #@2f5
    move-object/from16 v0, v24

    #@2f7
    iget v6, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@2f9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2fc
    move-result-object v5

    #@2fd
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@300
    move-result-object v5

    #@301
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@304
    .line 1440
    :try_start_304
    move-object/from16 v0, p0

    #@306
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@308
    const/4 v5, 0x0

    #@309
    move-object/from16 v0, v24

    #@30b
    invoke-virtual {v2, v5, v0}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageListInfoNoResult(ZLcom/broadcom/bt/map/RequestId;)V
    :try_end_30e
    .catch Ljava/lang/Throwable; {:try_start_304 .. :try_end_30e} :catch_310

    #@30e
    goto/16 :goto_7

    #@310
    .line 1441
    :catch_310
    move-exception v27

    #@311
    .line 1442
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@313
    const-string v5, "MSG_GET_MSG_LISTING_TIMEOUT: error"

    #@315
    move-object/from16 v0, v27

    #@317
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@31a
    goto/16 :goto_7

    #@31c
    .line 1449
    .end local v24           #r:Lcom/broadcom/bt/map/RequestId;
    .end local v27           #t:Ljava/lang/Throwable;
    :sswitch_31c
    :try_start_31c
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@31f
    move-result-object v12

    #@320
    .line 1451
    .restart local v12       #b:Landroid/os/Bundle;
    if-eqz v12, :cond_33a

    #@322
    const-string v2, "r"

    #@324
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@327
    move-result-object v2

    #@328
    if-eqz v2, :cond_33a

    #@32a
    const-string v2, "p"

    #@32c
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@32f
    move-result-object v2

    #@330
    if-eqz v2, :cond_33a

    #@332
    const-string v2, "d"

    #@334
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@337
    move-result-object v2

    #@338
    if-nez v2, :cond_34f

    #@33a
    .line 1453
    :cond_33a
    const-string v2, "BtMap.MapService"

    #@33c
    const-string v5, "MSG_SET_MSG_LISTING: b or getparcelabel or getString is null"

    #@33e
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_341
    .catch Ljava/lang/Throwable; {:try_start_31c .. :try_end_341} :catch_343

    #@341
    goto/16 :goto_7

    #@343
    .line 1460
    .end local v12           #b:Landroid/os/Bundle;
    :catch_343
    move-exception v27

    #@344
    .line 1461
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@346
    const-string v5, "MSG_SET_FOLDER_LISTING: error"

    #@348
    move-object/from16 v0, v27

    #@34a
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34d
    goto/16 :goto_7

    #@34f
    .line 1457
    .end local v27           #t:Ljava/lang/Throwable;
    .restart local v12       #b:Landroid/os/Bundle;
    :cond_34f
    :try_start_34f
    move-object/from16 v0, p0

    #@351
    iget-object v4, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@353
    const-string v2, "r"

    #@355
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@358
    move-result-object v5

    #@359
    check-cast v5, Lcom/broadcom/bt/map/RequestId;

    #@35b
    const-string v2, "p"

    #@35d
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@360
    move-result-object v6

    #@361
    move-object/from16 v0, p1

    #@363
    iget-object v7, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@365
    check-cast v7, Ljava/util/List;

    #@367
    const-string v2, "d"

    #@369
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@36c
    move-result-object v8

    #@36d
    const/4 v9, 0x0

    #@36e
    const/4 v10, 0x0

    #@36f
    const/4 v11, 0x1

    #@370
    invoke-static/range {v4 .. v11}, Lcom/broadcom/bt/service/map/MapSessionManager;->setMessageListInfo(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZZZ)V
    :try_end_373
    .catch Ljava/lang/Throwable; {:try_start_34f .. :try_end_373} :catch_343

    #@373
    goto/16 :goto_7

    #@375
    .line 1468
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_375
    :try_start_375
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@378
    move-result-object v12

    #@379
    .line 1470
    .restart local v12       #b:Landroid/os/Bundle;
    if-eqz v12, :cond_393

    #@37b
    const-string v2, "r"

    #@37d
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@380
    move-result-object v2

    #@381
    if-eqz v2, :cond_393

    #@383
    const-string v2, "p"

    #@385
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@388
    move-result-object v2

    #@389
    if-eqz v2, :cond_393

    #@38b
    const-string v2, "d"

    #@38d
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@390
    move-result-object v2

    #@391
    if-nez v2, :cond_3a8

    #@393
    .line 1472
    :cond_393
    const-string v2, "BtMap.MapService"

    #@395
    const-string v5, "MSG_SET_MSG_LISTING_COUNT: b or getparcelabel or getString is null"

    #@397
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_39a
    .catch Ljava/lang/Throwable; {:try_start_375 .. :try_end_39a} :catch_39c

    #@39a
    goto/16 :goto_7

    #@39c
    .line 1480
    .end local v12           #b:Landroid/os/Bundle;
    :catch_39c
    move-exception v27

    #@39d
    .line 1481
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@39f
    const-string v5, "MSG_SET_MSG_LISTING_COUNT: error"

    #@3a1
    move-object/from16 v0, v27

    #@3a3
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3a6
    goto/16 :goto_7

    #@3a8
    .line 1476
    .end local v27           #t:Ljava/lang/Throwable;
    .restart local v12       #b:Landroid/os/Bundle;
    :cond_3a8
    :try_start_3a8
    move-object/from16 v0, p0

    #@3aa
    iget-object v4, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@3ac
    const-string v2, "r"

    #@3ae
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@3b1
    move-result-object v5

    #@3b2
    check-cast v5, Lcom/broadcom/bt/map/RequestId;

    #@3b4
    const-string v2, "p"

    #@3b6
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3b9
    move-result-object v6

    #@3ba
    move-object/from16 v0, p1

    #@3bc
    iget v7, v0, Landroid/os/Message;->arg1:I

    #@3be
    const-string v2, "d"

    #@3c0
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3c3
    move-result-object v8

    #@3c4
    const-string v2, "n"

    #@3c6
    const/4 v9, 0x0

    #@3c7
    invoke-virtual {v12, v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@3ca
    move-result v9

    #@3cb
    invoke-static/range {v4 .. v9}, Lcom/broadcom/bt/service/map/MapSessionManager;->setMessageListInfoCount(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ILjava/lang/String;Z)V
    :try_end_3ce
    .catch Ljava/lang/Throwable; {:try_start_3a8 .. :try_end_3ce} :catch_39c

    #@3ce
    goto/16 :goto_7

    #@3d0
    .line 1486
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_3d0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@3d3
    move-result-object v12

    #@3d4
    .line 1487
    .restart local v12       #b:Landroid/os/Bundle;
    new-instance v3, Lcom/broadcom/bt/map/RequestId;

    #@3d6
    const-string v2, "s"

    #@3d8
    const/4 v5, -0x1

    #@3d9
    invoke-virtual {v12, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@3dc
    move-result v2

    #@3dd
    const-string v5, "e"

    #@3df
    const/4 v6, -0x1

    #@3e0
    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@3e3
    move-result v5

    #@3e4
    invoke-direct {v3, v2, v5}, Lcom/broadcom/bt/map/RequestId;-><init>(II)V

    #@3e7
    .line 1489
    .restart local v3       #requestId:Lcom/broadcom/bt/map/RequestId;
    const/16 v2, 0x3ec

    #@3e9
    const/16 v5, 0x2710

    #@3eb
    move-object/from16 v0, p0

    #@3ed
    invoke-direct {v0, v2, v5, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->startTimeoutTimer(IILcom/broadcom/bt/map/RequestId;)V

    #@3f0
    .line 1490
    move-object/from16 v0, p0

    #@3f2
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@3f4
    const-string v5, "p"

    #@3f6
    invoke-virtual {v12, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@3f9
    move-result-object v4

    #@3fa
    const-string v5, "f"

    #@3fc
    const/4 v6, 0x1

    #@3fd
    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@400
    move-result v5

    #@401
    const-string v6, "n"

    #@403
    const-wide/16 v7, -0x1

    #@405
    invoke-virtual {v12, v6, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    #@408
    move-result-wide v6

    #@409
    move-object/from16 v0, p1

    #@40b
    iget-object v8, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@40d
    check-cast v8, Lcom/broadcom/bt/map/MessageListFilter;

    #@40f
    const-string v9, "m"

    #@411
    const-wide/16 v10, -0x1

    #@413
    invoke-virtual {v12, v9, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    #@416
    move-result-wide v9

    #@417
    invoke-static/range {v2 .. v10}, Lcom/broadcom/bt/service/map/MapSessionManager;->getMessageInfo(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZJLcom/broadcom/bt/map/MessageListFilter;J)V

    #@41a
    goto/16 :goto_7

    #@41c
    .line 1497
    .end local v3           #requestId:Lcom/broadcom/bt/map/RequestId;
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_41c
    invoke-direct/range {p0 .. p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->getRequestIdFromTimeoutMessage(Landroid/os/Message;)Lcom/broadcom/bt/map/RequestId;

    #@41f
    move-result-object v24

    #@420
    .line 1499
    .restart local v24       #r:Lcom/broadcom/bt/map/RequestId;
    if-nez v24, :cond_42b

    #@422
    .line 1500
    const-string v2, "BtMap.MapService"

    #@424
    const-string v5, "MSG_GET_MSG_ENTRY_TIMEOUT: r is null"

    #@426
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@429
    goto/16 :goto_7

    #@42b
    .line 1504
    :cond_42b
    const-string v2, "BtMap.MapService"

    #@42d
    new-instance v5, Ljava/lang/StringBuilder;

    #@42f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@432
    const-string v6, "Timeout while getting message entry for request "

    #@434
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@437
    move-result-object v5

    #@438
    move-object/from16 v0, v24

    #@43a
    iget v6, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@43c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43f
    move-result-object v5

    #@440
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@443
    move-result-object v5

    #@444
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@447
    .line 1506
    :try_start_447
    move-object/from16 v0, p0

    #@449
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@44b
    const/4 v5, 0x0

    #@44c
    move-object/from16 v0, v24

    #@44e
    invoke-virtual {v2, v5, v0}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryNoResult(ZLcom/broadcom/bt/map/RequestId;)V
    :try_end_451
    .catch Ljava/lang/Throwable; {:try_start_447 .. :try_end_451} :catch_453

    #@451
    goto/16 :goto_7

    #@453
    .line 1507
    :catch_453
    move-exception v27

    #@454
    .line 1508
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@456
    const-string v5, "MSG_GET_MSG_ENTRY_TIMEOUT: error"

    #@458
    move-object/from16 v0, v27

    #@45a
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@45d
    goto/16 :goto_7

    #@45f
    .line 1515
    .end local v24           #r:Lcom/broadcom/bt/map/RequestId;
    .end local v27           #t:Ljava/lang/Throwable;
    :sswitch_45f
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@462
    move-result-object v12

    #@463
    .line 1516
    .restart local v12       #b:Landroid/os/Bundle;
    new-instance v3, Lcom/broadcom/bt/map/RequestId;

    #@465
    const-string v2, "s"

    #@467
    const/4 v5, -0x1

    #@468
    invoke-virtual {v12, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@46b
    move-result v2

    #@46c
    const-string v5, "e"

    #@46e
    const/4 v6, -0x1

    #@46f
    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@472
    move-result v5

    #@473
    invoke-direct {v3, v2, v5}, Lcom/broadcom/bt/map/RequestId;-><init>(II)V

    #@476
    .line 1518
    .restart local v3       #requestId:Lcom/broadcom/bt/map/RequestId;
    const/16 v2, 0x3ed

    #@478
    const/16 v5, 0x2710

    #@47a
    move-object/from16 v0, p0

    #@47c
    invoke-direct {v0, v2, v5, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->startTimeoutTimer(IILcom/broadcom/bt/map/RequestId;)V

    #@47f
    .line 1519
    move-object/from16 v0, p0

    #@481
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@483
    const-string v5, "m"

    #@485
    const-wide/16 v6, -0x1

    #@487
    invoke-virtual {v12, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    #@48a
    move-result-wide v4

    #@48b
    const-string v6, "a"

    #@48d
    const/4 v7, 0x0

    #@48e
    invoke-virtual {v12, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@491
    move-result v6

    #@492
    const-string v7, "c"

    #@494
    const/4 v8, 0x0

    #@495
    invoke-virtual {v12, v7, v8}, Landroid/os/Bundle;->getByte(Ljava/lang/String;B)Ljava/lang/Byte;

    #@498
    move-result-object v7

    #@499
    invoke-virtual {v7}, Ljava/lang/Byte;->byteValue()B

    #@49c
    move-result v7

    #@49d
    invoke-static/range {v2 .. v7}, Lcom/broadcom/bt/service/map/MapSessionManager;->getMessage(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;JZB)V

    #@4a0
    goto/16 :goto_7

    #@4a2
    .line 1525
    .end local v3           #requestId:Lcom/broadcom/bt/map/RequestId;
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_4a2
    invoke-direct/range {p0 .. p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->getRequestIdFromTimeoutMessage(Landroid/os/Message;)Lcom/broadcom/bt/map/RequestId;

    #@4a5
    move-result-object v24

    #@4a6
    .line 1527
    .restart local v24       #r:Lcom/broadcom/bt/map/RequestId;
    if-nez v24, :cond_4b1

    #@4a8
    .line 1528
    const-string v2, "BtMap.MapService"

    #@4aa
    const-string v5, "MSG_GET_MSG_TIMEOUT: r is null"

    #@4ac
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4af
    goto/16 :goto_7

    #@4b1
    .line 1532
    :cond_4b1
    const-string v2, "BtMap.MapService"

    #@4b3
    new-instance v5, Ljava/lang/StringBuilder;

    #@4b5
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4b8
    const-string v6, "Timeout while getting message for request "

    #@4ba
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4bd
    move-result-object v5

    #@4be
    move-object/from16 v0, v24

    #@4c0
    iget v6, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@4c2
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c5
    move-result-object v5

    #@4c6
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c9
    move-result-object v5

    #@4ca
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4cd
    .line 1534
    :try_start_4cd
    move-object/from16 v0, p0

    #@4cf
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@4d1
    const/4 v5, 0x0

    #@4d2
    move-object/from16 v0, v24

    #@4d4
    invoke-virtual {v2, v5, v0}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V
    :try_end_4d7
    .catch Ljava/lang/Throwable; {:try_start_4cd .. :try_end_4d7} :catch_4d9

    #@4d7
    goto/16 :goto_7

    #@4d9
    .line 1535
    :catch_4d9
    move-exception v27

    #@4da
    .line 1536
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@4dc
    const-string v5, "MSG_GET_MSG_TIMEOUT: error"

    #@4de
    move-object/from16 v0, v27

    #@4e0
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4e3
    goto/16 :goto_7

    #@4e5
    .line 1543
    .end local v24           #r:Lcom/broadcom/bt/map/RequestId;
    .end local v27           #t:Ljava/lang/Throwable;
    :sswitch_4e5
    :try_start_4e5
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@4e8
    move-result-object v12

    #@4e9
    .line 1545
    .restart local v12       #b:Landroid/os/Bundle;
    if-eqz v12, :cond_503

    #@4eb
    const-string v2, "r"

    #@4ed
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@4f0
    move-result-object v2

    #@4f1
    if-eqz v2, :cond_503

    #@4f3
    const-string v2, "p"

    #@4f5
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@4f8
    move-result-object v2

    #@4f9
    if-eqz v2, :cond_503

    #@4fb
    const-string v2, "m"

    #@4fd
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@500
    move-result-object v2

    #@501
    if-nez v2, :cond_518

    #@503
    .line 1547
    :cond_503
    const-string v2, "BtMap.MapService"

    #@505
    const-string v5, "MSG_RETURN_MSG: b or getparcelabel or getString is null"

    #@507
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_50a
    .catch Ljava/lang/Throwable; {:try_start_4e5 .. :try_end_50a} :catch_50c

    #@50a
    goto/16 :goto_7

    #@50c
    .line 1553
    .end local v12           #b:Landroid/os/Bundle;
    :catch_50c
    move-exception v27

    #@50d
    .line 1554
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@50f
    const-string v5, "MSG_RETURN_MSG: error"

    #@511
    move-object/from16 v0, v27

    #@513
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@516
    goto/16 :goto_7

    #@518
    .line 1551
    .end local v27           #t:Ljava/lang/Throwable;
    .restart local v12       #b:Landroid/os/Bundle;
    :cond_518
    :try_start_518
    move-object/from16 v0, p0

    #@51a
    iget-object v5, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@51c
    const-string v2, "r"

    #@51e
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@521
    move-result-object v2

    #@522
    check-cast v2, Lcom/broadcom/bt/map/RequestId;

    #@524
    const-string v6, "m"

    #@526
    invoke-virtual {v12, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@529
    move-result-object v6

    #@52a
    const-string v7, "p"

    #@52c
    invoke-virtual {v12, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@52f
    move-result-object v7

    #@530
    invoke-static {v5, v2, v6, v7}, Lcom/broadcom/bt/service/map/MapSessionManager;->returnMessage(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_533
    .catch Ljava/lang/Throwable; {:try_start_518 .. :try_end_533} :catch_50c

    #@533
    goto/16 :goto_7

    #@535
    .line 1560
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_535
    :try_start_535
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@538
    move-result-object v12

    #@539
    .line 1562
    .restart local v12       #b:Landroid/os/Bundle;
    if-nez v12, :cond_550

    #@53b
    .line 1563
    const-string v2, "BtMap.MapService"

    #@53d
    const-string v5, "MSG_PUSH_MSG: error. Bundle not available"

    #@53f
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_542
    .catch Ljava/lang/Throwable; {:try_start_535 .. :try_end_542} :catch_544

    #@542
    goto/16 :goto_7

    #@544
    .line 1582
    .end local v12           #b:Landroid/os/Bundle;
    :catch_544
    move-exception v27

    #@545
    .line 1583
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@547
    const-string v5, "MSG_PUSH_MSG: error"

    #@549
    move-object/from16 v0, v27

    #@54b
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@54e
    goto/16 :goto_7

    #@550
    .line 1567
    .end local v27           #t:Ljava/lang/Throwable;
    .restart local v12       #b:Landroid/os/Bundle;
    :cond_550
    :try_start_550
    new-instance v3, Lcom/broadcom/bt/map/RequestId;

    #@552
    const-string v2, "s"

    #@554
    const/4 v5, -0x1

    #@555
    invoke-virtual {v12, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@558
    move-result v2

    #@559
    const-string v5, "e"

    #@55b
    const/4 v6, -0x1

    #@55c
    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@55f
    move-result v5

    #@560
    invoke-direct {v3, v2, v5}, Lcom/broadcom/bt/map/RequestId;-><init>(II)V

    #@563
    .line 1570
    .restart local v3       #requestId:Lcom/broadcom/bt/map/RequestId;
    const-string v2, "p"

    #@565
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@568
    move-result-object v4

    #@569
    .line 1572
    .local v4, bMessageFilepath:Ljava/lang/String;
    if-nez v4, :cond_574

    #@56b
    .line 1573
    const-string v2, "BtMap.MapService"

    #@56d
    const-string v5, "MSG_PUSH_MSG: bMessageFilepath is null!!!"

    #@56f
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@572
    goto/16 :goto_7

    #@574
    .line 1577
    :cond_574
    iput-object v4, v3, Lcom/broadcom/bt/map/RequestId;->mRequestData:Ljava/lang/String;

    #@576
    .line 1579
    const/16 v2, 0x3ee

    #@578
    const/16 v5, 0x2710

    #@57a
    move-object/from16 v0, p0

    #@57c
    invoke-direct {v0, v2, v5, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->startTimeoutTimer(IILcom/broadcom/bt/map/RequestId;)V

    #@57f
    .line 1580
    move-object/from16 v0, p0

    #@581
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@583
    const-string v5, "n"

    #@585
    const/4 v6, 0x0

    #@586
    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@589
    move-result v5

    #@58a
    const-string v6, "r"

    #@58c
    const/4 v7, 0x0

    #@58d
    invoke-virtual {v12, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@590
    move-result v6

    #@591
    const-string v7, "c"

    #@593
    const/4 v8, 0x0

    #@594
    invoke-virtual {v12, v7, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@597
    move-result v7

    #@598
    invoke-static/range {v2 .. v7}, Lcom/broadcom/bt/service/map/MapSessionManager;->pushMessage(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZI)V
    :try_end_59b
    .catch Ljava/lang/Throwable; {:try_start_550 .. :try_end_59b} :catch_544

    #@59b
    goto/16 :goto_7

    #@59d
    .line 1588
    .end local v3           #requestId:Lcom/broadcom/bt/map/RequestId;
    .end local v4           #bMessageFilepath:Ljava/lang/String;
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_59d
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@5a0
    move-result-object v12

    #@5a1
    .line 1590
    .restart local v12       #b:Landroid/os/Bundle;
    if-eqz v12, :cond_5bb

    #@5a3
    const-string v2, "r"

    #@5a5
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@5a8
    move-result-object v2

    #@5a9
    if-eqz v2, :cond_5bb

    #@5ab
    const-string v2, "p"

    #@5ad
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@5b0
    move-result-object v2

    #@5b1
    if-eqz v2, :cond_5bb

    #@5b3
    const-string v2, "m"

    #@5b5
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@5b8
    move-result-object v2

    #@5b9
    if-nez v2, :cond_5c4

    #@5bb
    .line 1592
    :cond_5bb
    const-string v2, "BtMap.MapService"

    #@5bd
    const-string v5, "MSG_PUSH_MSG_RESULT: b or getparcelabel or getString is null"

    #@5bf
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5c2
    goto/16 :goto_7

    #@5c4
    .line 1596
    :cond_5c4
    move-object/from16 v0, p0

    #@5c6
    iget-object v5, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@5c8
    const-string v2, "r"

    #@5ca
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@5cd
    move-result-object v2

    #@5ce
    check-cast v2, Lcom/broadcom/bt/map/RequestId;

    #@5d0
    const-string v6, "p"

    #@5d2
    invoke-virtual {v12, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@5d5
    move-result-object v6

    #@5d6
    const-string v7, "m"

    #@5d8
    invoke-virtual {v12, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@5db
    move-result-object v7

    #@5dc
    invoke-static {v5, v2, v6, v7}, Lcom/broadcom/bt/service/map/MapSessionManager;->setPushMessageResult(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V

    #@5df
    goto/16 :goto_7

    #@5e1
    .line 1601
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_5e1
    invoke-direct/range {p0 .. p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->getRequestIdFromTimeoutMessage(Landroid/os/Message;)Lcom/broadcom/bt/map/RequestId;

    #@5e4
    move-result-object v24

    #@5e5
    .line 1603
    .restart local v24       #r:Lcom/broadcom/bt/map/RequestId;
    if-nez v24, :cond_5f0

    #@5e7
    .line 1604
    const-string v2, "BtMap.MapService"

    #@5e9
    const-string v5, "MSG_PUSH_MSG_TIMEOUT: error. RequestId not available"

    #@5eb
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5ee
    goto/16 :goto_7

    #@5f0
    .line 1609
    :cond_5f0
    :try_start_5f0
    move-object/from16 v0, p0

    #@5f2
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@5f4
    const/4 v5, 0x0

    #@5f5
    move-object/from16 v0, v24

    #@5f7
    invoke-virtual {v2, v5, v0}, Lcom/broadcom/bt/service/map/MapService;->setPushMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V
    :try_end_5fa
    .catch Ljava/lang/Throwable; {:try_start_5f0 .. :try_end_5fa} :catch_5fc

    #@5fa
    goto/16 :goto_7

    #@5fc
    .line 1610
    :catch_5fc
    move-exception v27

    #@5fd
    .line 1611
    .restart local v27       #t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapService"

    #@5ff
    const-string v5, "MSG_PUSH_MSG_TIMEOUTs: error"

    #@601
    move-object/from16 v0, v27

    #@603
    invoke-static {v2, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@606
    goto/16 :goto_7

    #@608
    .line 1616
    .end local v24           #r:Lcom/broadcom/bt/map/RequestId;
    .end local v27           #t:Ljava/lang/Throwable;
    :sswitch_608
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@60b
    move-result-object v12

    #@60c
    .line 1617
    .restart local v12       #b:Landroid/os/Bundle;
    const-string v2, "s"

    #@60e
    const/4 v5, -0x1

    #@60f
    invoke-virtual {v12, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@612
    move-result v25

    #@613
    .line 1618
    .local v25, sessionId:I
    move-object/from16 v0, p0

    #@615
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@617
    move/from16 v0, v25

    #@619
    invoke-static {v2, v0}, Lcom/broadcom/bt/service/map/MapSessionManager;->updateInbox(Lcom/broadcom/bt/service/map/MapService;I)V

    #@61c
    goto/16 :goto_7

    #@61e
    .line 1623
    .end local v12           #b:Landroid/os/Bundle;
    .end local v25           #sessionId:I
    :sswitch_61e
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@621
    move-result-object v12

    #@622
    .line 1624
    .restart local v12       #b:Landroid/os/Bundle;
    new-instance v3, Lcom/broadcom/bt/map/RequestId;

    #@624
    const-string v2, "s"

    #@626
    const/4 v5, -0x1

    #@627
    invoke-virtual {v12, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@62a
    move-result v2

    #@62b
    const-string v5, "e"

    #@62d
    const/4 v6, -0x1

    #@62e
    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@631
    move-result v5

    #@632
    invoke-direct {v3, v2, v5}, Lcom/broadcom/bt/map/RequestId;-><init>(II)V

    #@635
    .line 1625
    .restart local v3       #requestId:Lcom/broadcom/bt/map/RequestId;
    const-string v2, "t"

    #@637
    const/4 v5, -0x1

    #@638
    invoke-virtual {v12, v2, v5}, Landroid/os/Bundle;->getByte(Ljava/lang/String;B)Ljava/lang/Byte;

    #@63b
    move-result-object v2

    #@63c
    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    #@63f
    move-result v26

    #@640
    .line 1626
    .local v26, statusType:B
    const/4 v2, 0x1

    #@641
    move/from16 v0, v26

    #@643
    if-ne v0, v2, :cond_64e

    #@645
    .line 1629
    const/16 v2, 0x3ef

    #@647
    const/16 v5, 0x2710

    #@649
    move-object/from16 v0, p0

    #@64b
    invoke-direct {v0, v2, v5, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->startTimeoutTimer(IILcom/broadcom/bt/map/RequestId;)V

    #@64e
    .line 1631
    :cond_64e
    move-object/from16 v0, p0

    #@650
    iget-object v5, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@652
    const-string v2, "m"

    #@654
    const-wide/16 v6, -0x1

    #@656
    invoke-virtual {v12, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    #@659
    move-result-wide v7

    #@65a
    const-string v2, "t"

    #@65c
    const/4 v6, -0x1

    #@65d
    invoke-virtual {v12, v2, v6}, Landroid/os/Bundle;->getByte(Ljava/lang/String;B)Ljava/lang/Byte;

    #@660
    move-result-object v2

    #@661
    invoke-virtual {v2}, Ljava/lang/Byte;->byteValue()B

    #@664
    move-result v9

    #@665
    const-string v2, "i"

    #@667
    const/4 v6, 0x0

    #@668
    invoke-virtual {v12, v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@66b
    move-result v10

    #@66c
    move-object v6, v3

    #@66d
    invoke-static/range {v5 .. v10}, Lcom/broadcom/bt/service/map/MapSessionManager;->setMessageStatus(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;JBZ)V

    #@670
    goto/16 :goto_7

    #@672
    .line 1637
    .end local v3           #requestId:Lcom/broadcom/bt/map/RequestId;
    .end local v12           #b:Landroid/os/Bundle;
    .end local v26           #statusType:B
    :sswitch_672
    invoke-direct/range {p0 .. p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->getRequestIdFromTimeoutMessage(Landroid/os/Message;)Lcom/broadcom/bt/map/RequestId;

    #@675
    move-result-object v24

    #@676
    .line 1639
    .restart local v24       #r:Lcom/broadcom/bt/map/RequestId;
    if-nez v24, :cond_681

    #@678
    .line 1640
    const-string v2, "BtMap.MapService"

    #@67a
    const-string v5, "MSG_SET_MSG_DELETED_TIMEOUT: error. RequestId not available"

    #@67c
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@67f
    goto/16 :goto_7

    #@681
    .line 1644
    :cond_681
    move-object/from16 v0, p0

    #@683
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@685
    const/4 v5, 0x0

    #@686
    const/4 v6, 0x0

    #@687
    move-object/from16 v0, v24

    #@689
    invoke-virtual {v2, v5, v0, v6}, Lcom/broadcom/bt/service/map/MapService;->setMessageDeletedResult(ZLcom/broadcom/bt/map/RequestId;Z)V

    #@68c
    goto/16 :goto_7

    #@68e
    .line 1648
    .end local v24           #r:Lcom/broadcom/bt/map/RequestId;
    :sswitch_68e
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@691
    move-result-object v12

    #@692
    .line 1650
    .restart local v12       #b:Landroid/os/Bundle;
    if-eqz v12, :cond_6ac

    #@694
    const-string v2, "r"

    #@696
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@699
    move-result-object v2

    #@69a
    if-eqz v2, :cond_6ac

    #@69c
    const-string v2, "m"

    #@69e
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@6a1
    move-result-object v2

    #@6a2
    if-eqz v2, :cond_6ac

    #@6a4
    const-string v2, "f"

    #@6a6
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@6a9
    move-result-object v2

    #@6aa
    if-nez v2, :cond_6b5

    #@6ac
    .line 1652
    :cond_6ac
    const-string v2, "BtMap.MapService"

    #@6ae
    const-string v5, "MSG_SET_MSG_DELETED_RESULT: b or getparcelabel or getString is null"

    #@6b0
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6b3
    goto/16 :goto_7

    #@6b5
    .line 1656
    :cond_6b5
    move-object/from16 v0, p0

    #@6b7
    iget-object v5, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@6b9
    const-string v2, "r"

    #@6bb
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    #@6be
    move-result-object v6

    #@6bf
    check-cast v6, Lcom/broadcom/bt/map/RequestId;

    #@6c1
    const-string v2, "m"

    #@6c3
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@6c6
    move-result-object v7

    #@6c7
    const-string v2, "d"

    #@6c9
    const/4 v8, 0x0

    #@6ca
    invoke-virtual {v12, v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@6cd
    move-result v8

    #@6ce
    const-string v2, "s"

    #@6d0
    const/4 v9, 0x0

    #@6d1
    invoke-virtual {v12, v2, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@6d4
    move-result v9

    #@6d5
    const-string v2, "f"

    #@6d7
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@6da
    move-result-object v10

    #@6db
    invoke-static/range {v5 .. v10}, Lcom/broadcom/bt/service/map/MapSessionManager;->setMessageDeletedResult(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V

    #@6de
    goto/16 :goto_7

    #@6e0
    .line 1662
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_6e0
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@6e3
    move-result-object v12

    #@6e4
    .line 1663
    .restart local v12       #b:Landroid/os/Bundle;
    move-object/from16 v0, p0

    #@6e6
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@6e8
    const-string v5, "s"

    #@6ea
    const/4 v6, -0x1

    #@6eb
    invoke-virtual {v12, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@6ee
    move-result v5

    #@6ef
    const-string v6, "a"

    #@6f1
    invoke-virtual {v12, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@6f4
    move-result-object v6

    #@6f5
    const-string v7, "e"

    #@6f7
    const/4 v8, 0x0

    #@6f8
    invoke-virtual {v12, v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    #@6fb
    move-result v7

    #@6fc
    invoke-static {v2, v5, v6, v7}, Lcom/broadcom/bt/service/map/MapSessionManager;->updateNotificationState(Lcom/broadcom/bt/service/map/MapService;ILjava/lang/String;Z)V

    #@6ff
    goto/16 :goto_7

    #@701
    .line 1669
    .end local v12           #b:Landroid/os/Bundle;
    :sswitch_701
    const-string v2, "BtMap.MapService"

    #@703
    const-string v5, "handleMessage(MSG_NOTIFICATION)"

    #@705
    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@708
    .line 1671
    move-object/from16 v0, p0

    #@70a
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@70c
    #getter for: Lcom/broadcom/bt/service/map/MapService;->mNotificationLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/broadcom/bt/service/map/MapService;->access$1000(Lcom/broadcom/bt/service/map/MapService;)Ljava/lang/Object;

    #@70f
    move-result-object v5

    #@710
    monitor-enter v5

    #@711
    .line 1672
    :try_start_711
    move-object/from16 v0, p0

    #@713
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@715
    #calls: Lcom/broadcom/bt/service/map/MapService;->hasPendingNotifications()Z
    invoke-static {v2}, Lcom/broadcom/bt/service/map/MapService;->access$1100(Lcom/broadcom/bt/service/map/MapService;)Z

    #@718
    move-result v2

    #@719
    if-eqz v2, :cond_733

    #@71b
    .line 1673
    const-string v2, "BtMap.MapService"

    #@71d
    const-string v6, "handleMessage(MSG_NOTIFICATION): queuing notification request"

    #@71f
    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@722
    .line 1674
    move-object/from16 v0, p0

    #@724
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@726
    invoke-static/range {p1 .. p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    #@729
    move-result-object v6

    #@72a
    #calls: Lcom/broadcom/bt/service/map/MapService;->enqueuePendingNotification(Landroid/os/Message;)V
    invoke-static {v2, v6}, Lcom/broadcom/bt/service/map/MapService;->access$1200(Lcom/broadcom/bt/service/map/MapService;Landroid/os/Message;)V

    #@72d
    .line 1675
    monitor-exit v5

    #@72e
    goto/16 :goto_7

    #@730
    .line 1679
    :catchall_730
    move-exception v2

    #@731
    monitor-exit v5
    :try_end_732
    .catchall {:try_start_711 .. :try_end_732} :catchall_730

    #@732
    throw v2

    #@733
    .line 1678
    :cond_733
    :try_start_733
    move-object/from16 v0, p0

    #@735
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@737
    const/4 v6, 0x1

    #@738
    #setter for: Lcom/broadcom/bt/service/map/MapService;->mHasCurrentNotification:Z
    invoke-static {v2, v6}, Lcom/broadcom/bt/service/map/MapService;->access$1302(Lcom/broadcom/bt/service/map/MapService;Z)Z

    #@73b
    .line 1679
    monitor-exit v5
    :try_end_73c
    .catchall {:try_start_733 .. :try_end_73c} :catchall_730

    #@73c
    .line 1680
    invoke-direct/range {p0 .. p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->dispatchNotification(Landroid/os/Message;)V

    #@73f
    goto/16 :goto_7

    #@741
    .line 1684
    :sswitch_741
    const-string v2, "BtMap.MapService"

    #@743
    const-string v5, "handleMessage(MSG_NOTIFICATION_TIMEOUT)"

    #@745
    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@748
    .line 1685
    const/4 v15, 0x0

    #@749
    .line 1686
    .local v15, dispatchNotification:Z
    move-object/from16 v0, p0

    #@74b
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@74d
    #getter for: Lcom/broadcom/bt/service/map/MapService;->mNotificationLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/broadcom/bt/service/map/MapService;->access$1000(Lcom/broadcom/bt/service/map/MapService;)Ljava/lang/Object;

    #@750
    move-result-object v5

    #@751
    monitor-enter v5

    #@752
    .line 1687
    :try_start_752
    move-object/from16 v0, p0

    #@754
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@756
    #calls: Lcom/broadcom/bt/service/map/MapService;->dequeuePendingNotification()Landroid/os/Message;
    invoke-static {v2}, Lcom/broadcom/bt/service/map/MapService;->access$1400(Lcom/broadcom/bt/service/map/MapService;)Landroid/os/Message;

    #@759
    move-result-object p1

    #@75a
    .line 1688
    if-eqz p1, :cond_765

    #@75c
    .line 1690
    const/4 v15, 0x1

    #@75d
    .line 1694
    :goto_75d
    monitor-exit v5
    :try_end_75e
    .catchall {:try_start_752 .. :try_end_75e} :catchall_76e

    #@75e
    .line 1695
    if-eqz v15, :cond_7

    #@760
    .line 1696
    invoke-direct/range {p0 .. p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->dispatchNotification(Landroid/os/Message;)V

    #@763
    goto/16 :goto_7

    #@765
    .line 1692
    :cond_765
    :try_start_765
    move-object/from16 v0, p0

    #@767
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@769
    const/4 v6, 0x0

    #@76a
    #setter for: Lcom/broadcom/bt/service/map/MapService;->mHasCurrentNotification:Z
    invoke-static {v2, v6}, Lcom/broadcom/bt/service/map/MapService;->access$1302(Lcom/broadcom/bt/service/map/MapService;Z)Z

    #@76d
    goto :goto_75d

    #@76e
    .line 1694
    :catchall_76e
    move-exception v2

    #@76f
    monitor-exit v5
    :try_end_770
    .catchall {:try_start_765 .. :try_end_770} :catchall_76e

    #@770
    throw v2

    #@771
    .line 1701
    .end local v15           #dispatchNotification:Z
    :sswitch_771
    move-object/from16 v0, p0

    #@773
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@775
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@777
    const/16 v5, 0x582

    #@779
    invoke-virtual {v2, v5}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->removeMessages(I)V

    #@77c
    .line 1702
    const-string v2, "BtMap.MapService"

    #@77e
    const-string v5, "handleMessage(MSG_NOTIFICATION_COMPLETED)"

    #@780
    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@783
    .line 1703
    const/4 v15, 0x0

    #@784
    .line 1704
    .restart local v15       #dispatchNotification:Z
    move-object/from16 v0, p0

    #@786
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@788
    #getter for: Lcom/broadcom/bt/service/map/MapService;->mNotificationLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/broadcom/bt/service/map/MapService;->access$1000(Lcom/broadcom/bt/service/map/MapService;)Ljava/lang/Object;

    #@78b
    move-result-object v5

    #@78c
    monitor-enter v5

    #@78d
    .line 1705
    :try_start_78d
    move-object/from16 v0, p0

    #@78f
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@791
    #calls: Lcom/broadcom/bt/service/map/MapService;->dequeuePendingNotification()Landroid/os/Message;
    invoke-static {v2}, Lcom/broadcom/bt/service/map/MapService;->access$1400(Lcom/broadcom/bt/service/map/MapService;)Landroid/os/Message;

    #@794
    move-result-object p1

    #@795
    .line 1706
    if-eqz p1, :cond_7a0

    #@797
    .line 1707
    const/4 v15, 0x1

    #@798
    .line 1711
    :goto_798
    monitor-exit v5
    :try_end_799
    .catchall {:try_start_78d .. :try_end_799} :catchall_7a9

    #@799
    .line 1712
    if-eqz v15, :cond_7

    #@79b
    .line 1713
    invoke-direct/range {p0 .. p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->dispatchNotification(Landroid/os/Message;)V

    #@79e
    goto/16 :goto_7

    #@7a0
    .line 1709
    :cond_7a0
    :try_start_7a0
    move-object/from16 v0, p0

    #@7a2
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@7a4
    const/4 v6, 0x0

    #@7a5
    #setter for: Lcom/broadcom/bt/service/map/MapService;->mHasCurrentNotification:Z
    invoke-static {v2, v6}, Lcom/broadcom/bt/service/map/MapService;->access$1302(Lcom/broadcom/bt/service/map/MapService;Z)Z

    #@7a8
    goto :goto_798

    #@7a9
    .line 1711
    :catchall_7a9
    move-exception v2

    #@7aa
    monitor-exit v5
    :try_end_7ab
    .catchall {:try_start_7a0 .. :try_end_7ab} :catchall_7a9

    #@7ab
    throw v2

    #@7ac
    .line 1719
    .end local v15           #dispatchNotification:Z
    :sswitch_7ac
    invoke-virtual/range {p1 .. p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@7af
    move-result-object v12

    #@7b0
    .line 1721
    .restart local v12       #b:Landroid/os/Bundle;
    if-nez v12, :cond_7bb

    #@7b2
    .line 1722
    const-string v2, "BtMap.MapService"

    #@7b4
    const-string v5, "MSG_UPDATE_HANDLE: error. Bundle not available"

    #@7b6
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7b9
    goto/16 :goto_7

    #@7bb
    .line 1725
    :cond_7bb
    const-string v2, "p"

    #@7bd
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@7c0
    move-result-object v2

    #@7c1
    if-eqz v2, :cond_7cb

    #@7c3
    const-string v2, "d"

    #@7c5
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@7c8
    move-result-object v2

    #@7c9
    if-nez v2, :cond_7d4

    #@7cb
    .line 1726
    :cond_7cb
    const-string v2, "BtMap.MapService"

    #@7cd
    const-string v5, "MSG_UPDATE_HANDLE: error. providerId or datasourceId not available"

    #@7cf
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7d2
    goto/16 :goto_7

    #@7d4
    .line 1731
    :cond_7d4
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@7d7
    move-result-object v18

    #@7d8
    .line 1732
    .local v18, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-nez v18, :cond_7e3

    #@7da
    .line 1733
    const-string v2, "BtMap.MapService"

    #@7dc
    const-string v5, "MSG_UPDATE_HANDLE: error. DataSourceManager not available"

    #@7de
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7e1
    goto/16 :goto_7

    #@7e3
    .line 1736
    :cond_7e3
    const-string v2, "p"

    #@7e5
    invoke-virtual {v12, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@7e8
    move-result-object v2

    #@7e9
    const-string v5, "d"

    #@7eb
    invoke-virtual {v12, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@7ee
    move-result-object v5

    #@7ef
    const-string v6, "o"

    #@7f1
    invoke-virtual {v12, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@7f4
    move-result-object v6

    #@7f5
    const-string v7, "n"

    #@7f7
    invoke-virtual {v12, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@7fa
    move-result-object v7

    #@7fb
    move-object/from16 v0, v18

    #@7fd
    invoke-virtual {v0, v2, v5, v6, v7}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->updateMessageId(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@800
    goto/16 :goto_7

    #@802
    .line 1270
    :sswitch_data_802
    .sparse-switch
        0x1 -> :sswitch_8
        0x2 -> :sswitch_14
        0x3 -> :sswitch_34
        0x4 -> :sswitch_1a0
        0x64 -> :sswitch_ec
        0xc8 -> :sswitch_1c7
        0xfa -> :sswitch_29c
        0xfb -> :sswitch_3d0
        0xfc -> :sswitch_45f
        0x190 -> :sswitch_211
        0x191 -> :sswitch_31c
        0x192 -> :sswitch_375
        0x193 -> :sswitch_4e5
        0x194 -> :sswitch_535
        0x195 -> :sswitch_59d
        0x196 -> :sswitch_608
        0x197 -> :sswitch_61e
        0x198 -> :sswitch_68e
        0x199 -> :sswitch_6e0
        0x19a -> :sswitch_701
        0x19b -> :sswitch_7ac
        0x3e8 -> :sswitch_13c
        0x3e9 -> :sswitch_16e
        0x3ea -> :sswitch_259
        0x3eb -> :sswitch_2d9
        0x3ec -> :sswitch_41c
        0x3ed -> :sswitch_4a2
        0x3ee -> :sswitch_5e1
        0x3ef -> :sswitch_672
        0x582 -> :sswitch_741
        0x58c -> :sswitch_771
    .end sparse-switch
.end method

.method onClientNotificationChanged(ILjava/lang/String;Z)V
    .registers 8
    .parameter "sessionId"
    .parameter "remoteAddr"
    .parameter "enable"

    #@0
    .prologue
    .line 1145
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x199

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1146
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1147
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "s"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@13
    .line 1148
    const-string v2, "a"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1149
    const-string v2, "e"

    #@1a
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@1d
    .line 1150
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@1f
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@21
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@24
    .line 1151
    return-void
.end method

.method onGetFolderEntry(IILjava/lang/String;ZJ)V
    .registers 11
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "path"
    .parameter "isFirstEntry"
    .parameter "entryId"

    #@0
    .prologue
    .line 1013
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0xc8

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1014
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1015
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "s"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@13
    .line 1016
    const-string v2, "e"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@18
    .line 1017
    const-string v2, "p"

    #@1a
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1018
    const-string v2, "f"

    #@1f
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@22
    .line 1019
    const-string v2, "n"

    #@24
    invoke-virtual {v0, v2, p5, p6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@27
    .line 1020
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@29
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@2b
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@2e
    .line 1021
    return-void
.end method

.method onGetMessage(IIJZB)V
    .registers 11
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "messageHandle"
    .parameter "includeAttachments"
    .parameter "charSet"

    #@0
    .prologue
    .line 1084
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0xfc

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1085
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1086
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "s"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@13
    .line 1087
    const-string v2, "e"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@18
    .line 1088
    const-string v2, "c"

    #@1a
    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@1d
    .line 1089
    const-string v2, "a"

    #@1f
    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@22
    .line 1090
    const-string v2, "m"

    #@24
    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@27
    .line 1091
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@29
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@2b
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@2e
    .line 1092
    return-void
.end method

.method onGetMessageEntry(IILjava/lang/String;ZJLcom/broadcom/bt/map/MessageListFilter;J)V
    .registers 14
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "path"
    .parameter "isFirstEntry"
    .parameter "entryId"
    .parameter "filter"
    .parameter "parameterMask"

    #@0
    .prologue
    .line 1070
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0xfb

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1071
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1072
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "s"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@13
    .line 1073
    const-string v2, "e"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@18
    .line 1074
    const-string v2, "p"

    #@1a
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1075
    const-string v2, "f"

    #@1f
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@22
    .line 1076
    const-string v2, "n"

    #@24
    invoke-virtual {v0, v2, p5, p6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@27
    .line 1077
    const-string v2, "m"

    #@29
    invoke-virtual {v0, v2, p8, p9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@2c
    .line 1078
    iput-object p7, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2e
    .line 1079
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@30
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@32
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@35
    .line 1080
    return-void
.end method

.method onGetMessageListInfo(IILjava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;J)V
    .registers 11
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "path"
    .parameter "filter"
    .parameter "parameterMask"

    #@0
    .prologue
    .line 1035
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0xfa

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1036
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1037
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "s"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@13
    .line 1038
    const-string v2, "e"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@18
    .line 1039
    const-string v2, "p"

    #@1a
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1040
    const-string v2, "m"

    #@1f
    invoke-virtual {v0, v2, p5, p6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@22
    .line 1041
    iput-object p4, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24
    .line 1042
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@26
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@28
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@2b
    .line 1043
    return-void
.end method

.method onPushMessage(IILjava/lang/String;ZZI)V
    .registers 11
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "messageFilePath"
    .parameter "saveInSent"
    .parameter "retry"
    .parameter "charSet"

    #@0
    .prologue
    .line 1105
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x194

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1106
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1107
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "s"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@13
    .line 1108
    const-string v2, "e"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@18
    .line 1109
    const-string v2, "p"

    #@1a
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1110
    const-string v2, "n"

    #@1f
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@22
    .line 1111
    const-string v2, "r"

    #@24
    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@27
    .line 1112
    const-string v2, "c"

    #@29
    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2c
    .line 1113
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2e
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@30
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@33
    .line 1114
    return-void
.end method

.method onSetMessageStatus(IIJBZ)V
    .registers 11
    .parameter "sessionId"
    .parameter "eventId"
    .parameter "messageHandle"
    .parameter "messageStatusType"
    .parameter "isStatusSet"

    #@0
    .prologue
    .line 1134
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x197

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1135
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1136
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "s"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@13
    .line 1137
    const-string v2, "e"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@18
    .line 1138
    const-string v2, "m"

    #@1a
    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    #@1d
    .line 1139
    const-string v2, "t"

    #@1f
    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@22
    .line 1140
    const-string v2, "i"

    #@24
    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@27
    .line 1141
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@29
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@2b
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@2e
    .line 1142
    return-void
.end method

.method onUpdateInbox(I)V
    .registers 6
    .parameter "sessionId"

    #@0
    .prologue
    .line 1126
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x196

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1127
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1128
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "s"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@13
    .line 1129
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@15
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@17
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@1a
    .line 1130
    return-void
.end method

.method returnMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "requestId"
    .parameter "messageHandle"
    .parameter "bMsgFilePath"

    #@0
    .prologue
    .line 1095
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x193

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1096
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1097
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "r"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@13
    .line 1098
    const-string v2, "m"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1099
    const-string v2, "p"

    #@1a
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1100
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@1f
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@21
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@24
    .line 1101
    return-void
.end method

.method sendDatasourceClientConnectionChangeNotification(Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V
    .registers 6
    .parameter "ds"
    .parameter "hasClientsConnected"

    #@0
    .prologue
    .line 1004
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v1, v1, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/4 v2, 0x4

    #@5
    invoke-virtual {v1, v2}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    .line 1006
    .local v0, m:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@b
    .line 1007
    if-eqz p2, :cond_18

    #@d
    const/4 v1, 0x1

    #@e
    :goto_e
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@10
    .line 1008
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@12
    iget-object v1, v1, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@14
    invoke-virtual {v1, v0}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@17
    .line 1009
    return-void

    #@18
    .line 1007
    :cond_18
    const/4 v1, 0x0

    #@19
    goto :goto_e
.end method

.method sendDatasourceStateChangeNotifications(Lcom/broadcom/bt/service/map/MapDatasourceContext;ZZ)V
    .registers 9
    .parameter "ds"
    .parameter "isStartRequest"
    .parameter "hasError"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 995
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@4
    iget-object v1, v1, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@6
    const/4 v4, 0x3

    #@7
    invoke-virtual {v1, v4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@a
    move-result-object v0

    #@b
    .line 996
    .local v0, m:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d
    .line 997
    if-eqz p2, :cond_1e

    #@f
    move v1, v2

    #@10
    :goto_10
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@12
    .line 998
    if-eqz p3, :cond_20

    #@14
    :goto_14
    iput v2, v0, Landroid/os/Message;->arg2:I

    #@16
    .line 999
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@18
    iget-object v1, v1, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@1a
    invoke-virtual {v1, v0}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@1d
    .line 1000
    return-void

    #@1e
    :cond_1e
    move v1, v3

    #@1f
    .line 997
    goto :goto_10

    #@20
    :cond_20
    move v2, v3

    #@21
    .line 998
    goto :goto_14
.end method

.method public sendMessageDatasourceRegistrationChanged(Lcom/broadcom/bt/service/map/MapDatasourceContext;Z)V
    .registers 6
    .parameter "ds"
    .parameter "isRegistered"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 981
    const/4 v2, 0x2

    #@2
    if-eqz p2, :cond_d

    #@4
    move v0, v1

    #@5
    :goto_5
    invoke-virtual {p0, v2, v0, v1, p1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@8
    move-result-object v0

    #@9
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@c
    .line 982
    return-void

    #@d
    .line 981
    :cond_d
    const/4 v0, 0x0

    #@e
    goto :goto_5
.end method

.method public sendMessageProfileStateChanged(I)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 976
    const/4 v0, 0x1

    #@1
    const/4 v1, -0x1

    #@2
    invoke-virtual {p0, v0, p1, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(III)Landroid/os/Message;

    #@5
    move-result-object v0

    #@6
    invoke-virtual {p0, v0}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@9
    .line 977
    return-void
.end method

.method sendNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;BBLjava/lang/String;Ljava/lang/String;)V
    .registers 14
    .parameter "providerId"
    .parameter "datasourceId"
    .parameter "messageHandle"
    .parameter "messageType"
    .parameter "notificationType"
    .parameter "dsFolderPath"
    .parameter "dsOldFolderPath"

    #@0
    .prologue
    const/16 v5, 0x19a

    #@2
    .line 1174
    const-string v2, "BtMap.MapService"

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "sendNotification():  datasourceId= "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, ", messageHandle="

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, ", messageType="

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    const-string v4, ", notificationType="

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v4, ", folderPath="

    #@33
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    invoke-static {p6}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    const-string v4, ", oldFolderPath="

    #@41
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-static {p7}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v3

    #@51
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 1181
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@56
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@58
    invoke-virtual {v2, v5}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@5b
    move-result-object v1

    #@5c
    .line 1182
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@5f
    move-result-object v0

    #@60
    .line 1183
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "p"

    #@62
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@65
    .line 1184
    const-string v2, "d"

    #@67
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@6a
    .line 1185
    const-string v2, "m"

    #@6c
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@6f
    .line 1186
    const-string v2, "t"

    #@71
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@74
    .line 1187
    const-string v2, "n"

    #@76
    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@79
    .line 1188
    const-string v2, "f"

    #@7b
    invoke-virtual {v0, v2, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@7e
    .line 1189
    const-string v2, "o"

    #@80
    invoke-virtual {v0, v2, p7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@83
    .line 1197
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@85
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@87
    invoke-virtual {v2, v5}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->hasMessages(I)Z

    #@8a
    move-result v2

    #@8b
    if-nez v2, :cond_97

    #@8d
    .line 1198
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@8f
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@91
    const-wide/16 v3, 0x320

    #@93
    invoke-virtual {v2, v1, v3, v4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@96
    .line 1203
    :goto_96
    return-void

    #@97
    .line 1200
    :cond_97
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@99
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@9b
    const-wide/16 v3, 0x640

    #@9d
    invoke-virtual {v2, v1, v3, v4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@a0
    goto :goto_96
.end method

.method setDatasourceState(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 7
    .parameter "providerId"
    .parameter "dsId"
    .parameter "enable"

    #@0
    .prologue
    .line 985
    const/16 v2, 0x64

    #@2
    invoke-virtual {p0, v2}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@5
    move-result-object v1

    #@6
    .line 986
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@9
    move-result-object v0

    #@a
    .line 987
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "providerId"

    #@c
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@f
    .line 988
    const-string v2, "dsId"

    #@11
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 989
    if-eqz p3, :cond_21

    #@16
    const/4 v2, 0x1

    #@17
    :goto_17
    iput v2, v1, Landroid/os/Message;->arg1:I

    #@19
    .line 990
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@1b
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@1d
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@20
    .line 991
    return-void

    #@21
    .line 989
    :cond_21
    const/4 v2, 0x0

    #@22
    goto :goto_17
.end method

.method setFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;)V
    .registers 8
    .parameter "requestId"
    .parameter "path"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/broadcom/bt/map/RequestId;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/FolderInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .line 1024
    .local p3, folderEntries:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;"
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x190

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1025
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1026
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "r"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@13
    .line 1027
    const-string v2, "p"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1028
    iput-object p3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a
    .line 1030
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@1c
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@1e
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@21
    .line 1031
    return-void
.end method

.method setMessageDeletedResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V
    .registers 11
    .parameter "requestId"
    .parameter "messageHandle"
    .parameter "deleteRequested"
    .parameter "success"
    .parameter "newFolderPath"

    #@0
    .prologue
    .line 1156
    const-string v2, "BtMap.MapService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "setMessageDeletedResult():  sessionId= "

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget v4, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, ", eventId="

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    iget v4, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, ", messageHandle="

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    const-string v4, ", deleteRequested="

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, ", success="

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    const-string v4, ", newFolderPath="

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 1161
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@50
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@52
    const/16 v3, 0x198

    #@54
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@57
    move-result-object v1

    #@58
    .line 1162
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@5b
    move-result-object v0

    #@5c
    .line 1163
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "r"

    #@5e
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@61
    .line 1164
    const-string v2, "m"

    #@63
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@66
    .line 1165
    const-string v2, "d"

    #@68
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@6b
    .line 1166
    const-string v2, "s"

    #@6d
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@70
    .line 1167
    const-string v2, "f"

    #@72
    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@75
    .line 1168
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@77
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@79
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@7c
    .line 1169
    return-void
.end method

.method setMessageListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .registers 9
    .parameter "requestId"
    .parameter "path"
    .parameter
    .parameter "dateTime"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/broadcom/bt/map/RequestId;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    #@0
    .prologue
    .line 1047
    .local p3, messageEntries:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;"
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x191

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1048
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1049
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "r"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@13
    .line 1050
    const-string v2, "p"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1051
    const-string v2, "d"

    #@1a
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1052
    iput-object p3, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1f
    .line 1053
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@21
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@23
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@26
    .line 1054
    return-void
.end method

.method setMessageListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ILjava/lang/String;Z)V
    .registers 10
    .parameter "requestId"
    .parameter "path"
    .parameter "messageCount"
    .parameter "dateTime"
    .parameter "hasNewMessages"

    #@0
    .prologue
    .line 1058
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x192

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1059
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1060
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "r"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@13
    .line 1061
    const-string v2, "p"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1062
    const-string v2, "d"

    #@1a
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1063
    const-string v2, "n"

    #@1f
    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@22
    .line 1064
    iput p3, v1, Landroid/os/Message;->arg1:I

    #@24
    .line 1065
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@26
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@28
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@2b
    .line 1066
    return-void
.end method

.method setPushMessageResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    .registers 8
    .parameter "requestId"
    .parameter "folderPath"
    .parameter "messageHandle"

    #@0
    .prologue
    .line 1117
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x195

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1118
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1119
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "r"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    #@13
    .line 1120
    const-string v2, "p"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1121
    const-string v2, "m"

    #@1a
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1122
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@1f
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@21
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@24
    .line 1123
    return-void
.end method

.method updateMessageHandle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 10
    .parameter "providerId"
    .parameter "datasourceId"
    .parameter "oldMessageId"
    .parameter "newMessageId"

    #@0
    .prologue
    .line 1207
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@2
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@4
    const/16 v3, 0x19b

    #@6
    invoke-virtual {v2, v3}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    .line 1208
    .local v1, m:Landroid/os/Message;
    invoke-virtual {v1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@d
    move-result-object v0

    #@e
    .line 1209
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "p"

    #@10
    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@13
    .line 1210
    const-string v2, "d"

    #@15
    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 1211
    const-string v2, "o"

    #@1a
    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@1d
    .line 1212
    const-string v2, "n"

    #@1f
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@22
    .line 1217
    const-string v2, "BtMap.MapService"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "updateMessageHandle():  providerId="

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, ", datasourceId= "

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 1218
    if-eqz p3, :cond_5e

    #@46
    .line 1219
    const-string v2, "BtMap.MapService"

    #@48
    new-instance v3, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v4, "updateMessageHandle():  oldMessageId="

    #@4f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v3

    #@53
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v3

    #@5b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 1221
    :cond_5e
    if-eqz p4, :cond_84

    #@60
    .line 1222
    const-string v2, "BtMap.MapService"

    #@62
    new-instance v3, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v4, "updateMessageHandle():  newMessageId="

    #@69
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v3

    #@71
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 1226
    :goto_78
    if-nez p4, :cond_8c

    #@7a
    .line 1227
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@7c
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@7e
    const-wide/16 v3, 0x320

    #@80
    invoke-virtual {v2, v1, v3, v4}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@83
    .line 1232
    :goto_83
    return-void

    #@84
    .line 1224
    :cond_84
    const-string v2, "BtMap.MapService"

    #@86
    const-string v3, "updateMessageHandle():  newMessageId= NULL"

    #@88
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    goto :goto_78

    #@8c
    .line 1229
    :cond_8c
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->this$0:Lcom/broadcom/bt/service/map/MapService;

    #@8e
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@90
    invoke-virtual {v2, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->sendMessage(Landroid/os/Message;)Z

    #@93
    goto :goto_83
.end method
