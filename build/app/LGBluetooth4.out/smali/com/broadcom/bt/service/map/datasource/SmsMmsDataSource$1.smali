.class Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;
.super Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;
.source "SmsMmsDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getMsgListingCount(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Lcom/broadcom/bt/map/MessageParameterFilter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mHasNewMessages:Z

.field final synthetic this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;


# direct methods
.method constructor <init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 540
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@2
    const/4 v0, 0x0

    #@3
    invoke-direct {p0, p1, v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListProcessor;-><init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;)V

    #@6
    .line 541
    const/4 v0, 0x0

    #@7
    iput-boolean v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mHasNewMessages:Z

    #@9
    return-void
.end method


# virtual methods
.method protected initMessageOffset()V
    .registers 2

    #@0
    .prologue
    .line 559
    iget v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMaxListCount:I

    #@2
    if-eqz v0, :cond_12

    #@4
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@6
    invoke-virtual {v0}, Lcom/broadcom/bt/map/MessageListFilter;->listStartOffsetSet()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 560
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@e
    iget v0, v0, Lcom/broadcom/bt/map/MessageListFilter;->mListStartOffset:I

    #@10
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageOffset:I

    #@12
    .line 562
    :cond_12
    iget v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageOffset:I

    #@14
    if-gez v0, :cond_19

    #@16
    .line 563
    const/4 v0, 0x0

    #@17
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageOffset:I

    #@19
    .line 565
    :cond_19
    return-void
.end method

.method protected initMmsMessageIterator()V
    .registers 6

    #@0
    .prologue
    .line 575
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@2
    #getter for: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mMmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;
    invoke-static {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$200(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->createMessageListIterator()Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@c
    .line 576
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@e
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mFolderPath:Ljava/lang/String;

    #@10
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@12
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mContactFilter:Ljava/lang/String;

    #@14
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mParamFilter:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@16
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->initCountIterator(Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Ljava/lang/String;Lcom/broadcom/bt/map/MessageParameterFilter;)V

    #@19
    .line 579
    return-void
.end method

.method protected initSmsMessageIterator()V
    .registers 6

    #@0
    .prologue
    .line 568
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@2
    #getter for: Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->mSmsMessageHelper:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;
    invoke-static {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$100(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;)Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;

    #@5
    move-result-object v0

    #@6
    invoke-interface {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;->createMessageListIterator()Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@c
    .line 569
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@e
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mFolderPath:Ljava/lang/String;

    #@10
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@12
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mContactFilter:Ljava/lang/String;

    #@14
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mParamFilter:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@16
    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->initCountIterator(Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Ljava/lang/String;Lcom/broadcom/bt/map/MessageParameterFilter;)V

    #@19
    .line 572
    return-void
.end method

.method protected processAllMmsEntries()Z
    .registers 3

    #@0
    .prologue
    .line 595
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@2
    iget v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageOffset:I

    #@4
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->skip(I)Z

    #@7
    .line 596
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@9
    iget v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMaxListCount:I

    #@b
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->countAll(I)V

    #@e
    .line 597
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@10
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->getCount()I

    #@13
    move-result v0

    #@14
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageCount:I

    #@16
    .line 598
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@18
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->hasNewMessages()Z

    #@1b
    move-result v0

    #@1c
    iput-boolean v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mHasNewMessages:Z

    #@1e
    .line 599
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@20
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->close()V

    #@23
    .line 600
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->setResultSuccess()V

    #@26
    .line 601
    const/4 v0, 0x1

    #@27
    return v0
.end method

.method protected processAllSmsEntries()Z
    .registers 3

    #@0
    .prologue
    .line 582
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->access$300()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 583
    const-string v0, "BtMap.SmsMmsDataSource"

    #@8
    const-string v1, "Processing all sms entries..."

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 585
    :cond_d
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@f
    iget v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageOffset:I

    #@11
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->skip(I)Z

    #@14
    .line 586
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@16
    iget v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMaxListCount:I

    #@18
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->countAll(I)V

    #@1b
    .line 587
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@1d
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->getCount()I

    #@20
    move-result v0

    #@21
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageCount:I

    #@23
    .line 588
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@25
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->hasNewMessages()Z

    #@28
    move-result v0

    #@29
    iput-boolean v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mHasNewMessages:Z

    #@2b
    .line 589
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@2d
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->close()V

    #@30
    .line 590
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->setResultSuccess()V

    #@33
    .line 591
    const/4 v0, 0x1

    #@34
    return v0
.end method

.method protected processOneMmsEntry()V
    .registers 2

    #@0
    .prologue
    .line 611
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mHasNewMessages:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 612
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mmsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@6
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->hasNewMessages()Z

    #@9
    move-result v0

    #@a
    iput-boolean v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mHasNewMessages:Z

    #@c
    .line 614
    :cond_c
    return-void
.end method

.method protected processOneSmsEntry()V
    .registers 2

    #@0
    .prologue
    .line 605
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mHasNewMessages:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 606
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->smsIterator:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;

    #@6
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;->hasNewMessages()Z

    #@9
    move-result v0

    #@a
    iput-boolean v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mHasNewMessages:Z

    #@c
    .line 608
    :cond_c
    return-void
.end method

.method protected setResultNoEntries()V
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 548
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@3
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mRequestId:Lcom/broadcom/bt/map/RequestId;

    #@5
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mFolderPath:Ljava/lang/String;

    #@7
    const/4 v3, 0x1

    #@8
    move v5, v4

    #@9
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageListingCountResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZIZ)V

    #@c
    .line 549
    return-void
.end method

.method protected setResultNotSuccess()V
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 544
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@3
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mRequestId:Lcom/broadcom/bt/map/RequestId;

    #@5
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mFolderPath:Ljava/lang/String;

    #@7
    move v4, v3

    #@8
    move v5, v3

    #@9
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageListingCountResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZIZ)V

    #@c
    .line 545
    return-void
.end method

.method protected setResultSuccess()V
    .registers 7

    #@0
    .prologue
    .line 552
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->this$0:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@2
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mRequestId:Lcom/broadcom/bt/map/RequestId;

    #@4
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mFolderPath:Ljava/lang/String;

    #@6
    const/4 v3, 0x1

    #@7
    iget v4, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mMessageCount:I

    #@9
    iget-boolean v5, p0, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$1;->mHasNewMessages:Z

    #@b
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->setMessageListingCountResult(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZIZ)V

    #@e
    .line 555
    return-void
.end method
