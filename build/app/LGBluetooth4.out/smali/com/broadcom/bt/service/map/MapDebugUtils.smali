.class public Lcom/broadcom/bt/service/map/MapDebugUtils;
.super Ljava/lang/Object;
.source "MapDebugUtils.java"


# static fields
.field private static final ACTION_DEBUG_DUMP_DS_STATE:Ljava/lang/String; = "broadcom.bluetooth.action.DEBUG_DUMP_DS_STATE"

.field private static final ACTION_DEBUG_DUMP_SESSION:Ljava/lang/String; = "broadcom.bluetooth.action.DEBUG_DUMP_SESSION"

.field private static final ACTION_DEBUG_GET_ADAPTER_STATE:Ljava/lang/String; = "broadcom.bluetooth.action.DEBUG_GET_ADAPTER_STATE"

.field private static final ACTION_DEBUG_SET_DISCOVERABLE:Ljava/lang/String; = "broadcom.bluetooth.action.DEBUG_SET_DISCOVERABLE"

.field private static final ACTION_DEBUG_SET_DS_STATE:Ljava/lang/String; = "broadcom.bluetooth.action.DEBUG_SET_DS_STATE"

.field private static final DEBUG_ADMIN:Z = true

.field private static final EXTRA_DEBUG_DS_ID:Ljava/lang/String; = "DS_ID"

.field private static final EXTRA_DEBUG_PROVIDER_ID:Ljava/lang/String; = "PROVIDER_ID"

.field private static final EXTRA_DEBUG_STATE:Ljava/lang/String; = "STATE"

.field private static final EXTRA_SESSION_ID:Ljava/lang/String; = "SESSION_ID"

.field private static final TAG:Ljava/lang/String; = "BtMap.MapDebugUtils"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 55
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static handleDebugAction(Lcom/broadcom/bt/service/map/MapService;Landroid/content/Intent;)Z
    .registers 22
    .parameter "svc"
    .parameter "intent"

    #@0
    .prologue
    .line 74
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v3

    #@4
    .line 75
    .local v3, action:Ljava/lang/String;
    const-string v17, "broadcom.bluetooth.action.DEBUG_DUMP_DS_STATE"

    #@6
    move-object/from16 v0, v17

    #@8
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v17

    #@c
    if-eqz v17, :cond_18

    #@e
    .line 76
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@11
    move-result-object v17

    #@12
    invoke-virtual/range {v17 .. v17}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->debugDumpState()V

    #@15
    .line 77
    const/16 v17, 0x1

    #@17
    .line 143
    :goto_17
    return v17

    #@18
    .line 78
    :cond_18
    const-string v17, "broadcom.bluetooth.action.DEBUG_SET_DS_STATE"

    #@1a
    move-object/from16 v0, v17

    #@1c
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v17

    #@20
    if-eqz v17, :cond_c0

    #@22
    .line 79
    const-string v17, "DS_ID"

    #@24
    move-object/from16 v0, p1

    #@26
    move-object/from16 v1, v17

    #@28
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@2b
    move-result-object v8

    #@2c
    .line 80
    .local v8, dsId:Ljava/lang/String;
    const-string v17, "PROVIDER_ID"

    #@2e
    move-object/from16 v0, p1

    #@30
    move-object/from16 v1, v17

    #@32
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@35
    move-result-object v11

    #@36
    .line 81
    .local v11, providerId:Ljava/lang/String;
    const-string v17, "STATE"

    #@38
    const/16 v18, -0x1

    #@3a
    move-object/from16 v0, p1

    #@3c
    move-object/from16 v1, v17

    #@3e
    move/from16 v2, v18

    #@40
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@43
    move-result v16

    #@44
    .line 82
    .local v16, state:I
    const-string v17, "BtMap.MapDebugUtils"

    #@46
    new-instance v18, Ljava/lang/StringBuilder;

    #@48
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v19, "providerId ="

    #@4d
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v18

    #@51
    move-object/from16 v0, v18

    #@53
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v18

    #@57
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v18

    #@5b
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 83
    const-string v17, "BtMap.MapDebugUtils"

    #@60
    new-instance v18, Ljava/lang/StringBuilder;

    #@62
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v19, "dsId ="

    #@67
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v18

    #@6b
    move-object/from16 v0, v18

    #@6d
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v18

    #@71
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74
    move-result-object v18

    #@75
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@78
    .line 84
    const-string v17, "BtMap.MapDebugUtils"

    #@7a
    new-instance v18, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v19, "state ="

    #@81
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v18

    #@85
    move-object/from16 v0, v18

    #@87
    move/from16 v1, v16

    #@89
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v18

    #@8d
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v18

    #@91
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 86
    if-eqz v8, :cond_b9

    #@96
    if-eqz v11, :cond_b9

    #@98
    if-eqz v16, :cond_a2

    #@9a
    const/16 v17, 0x1

    #@9c
    move/from16 v0, v16

    #@9e
    move/from16 v1, v17

    #@a0
    if-ne v0, v1, :cond_b9

    #@a2
    .line 88
    :cond_a2
    move-object/from16 v0, p0

    #@a4
    iget-object v0, v0, Lcom/broadcom/bt/service/map/MapService;->mHandler:Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;

    #@a6
    move-object/from16 v18, v0

    #@a8
    const/16 v17, 0x1

    #@aa
    move/from16 v0, v16

    #@ac
    move/from16 v1, v17

    #@ae
    if-ne v0, v1, :cond_bd

    #@b0
    const/16 v17, 0x1

    #@b2
    :goto_b2
    move-object/from16 v0, v18

    #@b4
    move/from16 v1, v17

    #@b6
    invoke-virtual {v0, v11, v8, v1}, Lcom/broadcom/bt/service/map/MapService$MapServiceHandler;->setDatasourceState(Ljava/lang/String;Ljava/lang/String;Z)V

    #@b9
    .line 90
    :cond_b9
    const/16 v17, 0x1

    #@bb
    goto/16 :goto_17

    #@bd
    .line 88
    :cond_bd
    const/16 v17, 0x0

    #@bf
    goto :goto_b2

    #@c0
    .line 91
    .end local v8           #dsId:Ljava/lang/String;
    .end local v11           #providerId:Ljava/lang/String;
    .end local v16           #state:I
    :cond_c0
    const-string v17, "broadcom.bluetooth.action.DEBUG_GET_ADAPTER_STATE"

    #@c2
    move-object/from16 v0, v17

    #@c4
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c7
    move-result v17

    #@c8
    if-eqz v17, :cond_160

    #@ca
    .line 92
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@cd
    move-result-object v4

    #@ce
    .line 93
    .local v4, adapter:Landroid/bluetooth/BluetoothAdapter;
    new-instance v5, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    .line 94
    .local v5, b:Ljava/lang/StringBuilder;
    const-string v17, "********Bluetooth Adapter*********************"

    #@d5
    move-object/from16 v0, v17

    #@d7
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    .line 95
    if-eqz v4, :cond_14c

    #@dc
    .line 96
    new-instance v17, Ljava/lang/StringBuilder;

    #@de
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@e1
    const-string v18, "\nLocal Address: "

    #@e3
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e6
    move-result-object v17

    #@e7
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    #@ea
    move-result-object v18

    #@eb
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v17

    #@ef
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v17

    #@f3
    move-object/from16 v0, v17

    #@f5
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    .line 97
    new-instance v17, Ljava/lang/StringBuilder;

    #@fa
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@fd
    const-string v18, "\nLocal Name: "

    #@ff
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v17

    #@103
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->getName()Ljava/lang/String;

    #@106
    move-result-object v18

    #@107
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v17

    #@10b
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10e
    move-result-object v17

    #@10f
    move-object/from16 v0, v17

    #@111
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    .line 98
    new-instance v17, Ljava/lang/StringBuilder;

    #@116
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@119
    const-string v18, "\nEnabled: "

    #@11b
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v17

    #@11f
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@122
    move-result v18

    #@123
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@126
    move-result-object v17

    #@127
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12a
    move-result-object v17

    #@12b
    move-object/from16 v0, v17

    #@12d
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    .line 99
    new-instance v17, Ljava/lang/StringBuilder;

    #@132
    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    #@135
    const-string v18, "\nIs Discovering: "

    #@137
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v17

    #@13b
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothAdapter;->isDiscovering()Z

    #@13e
    move-result v18

    #@13f
    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@142
    move-result-object v17

    #@143
    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@146
    move-result-object v17

    #@147
    move-object/from16 v0, v17

    #@149
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    .line 101
    :cond_14c
    const-string v17, "\n**********************************************"

    #@14e
    move-object/from16 v0, v17

    #@150
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    .line 102
    const-string v17, "BtMap.MapDebugUtils"

    #@155
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@158
    move-result-object v18

    #@159
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15c
    .line 103
    const/16 v17, 0x1

    #@15e
    goto/16 :goto_17

    #@160
    .line 104
    .end local v4           #adapter:Landroid/bluetooth/BluetoothAdapter;
    .end local v5           #b:Ljava/lang/StringBuilder;
    :cond_160
    const-string v17, "broadcom.bluetooth.action.DEBUG_SET_DISCOVERABLE"

    #@162
    move-object/from16 v0, v17

    #@164
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@167
    move-result v17

    #@168
    if-eqz v17, :cond_1a0

    #@16a
    .line 105
    const-string v17, "STATE"

    #@16c
    const/16 v18, -0x1

    #@16e
    move-object/from16 v0, p1

    #@170
    move-object/from16 v1, v17

    #@172
    move/from16 v2, v18

    #@174
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@177
    move-result v16

    #@178
    .line 106
    .restart local v16       #state:I
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@17b
    move-result-object v4

    #@17c
    .line 107
    .restart local v4       #adapter:Landroid/bluetooth/BluetoothAdapter;
    const-string v17, "BtMap.MapDebugUtils"

    #@17e
    const-string v18, "********Bluetooth Adapter*********************"

    #@180
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@183
    .line 108
    if-eqz v4, :cond_194

    #@185
    .line 109
    const/16 v17, 0x1

    #@187
    move/from16 v0, v16

    #@189
    move/from16 v1, v17

    #@18b
    if-ne v0, v1, :cond_198

    #@18d
    .line 110
    const/16 v17, 0x17

    #@18f
    move/from16 v0, v17

    #@191
    invoke-virtual {v4, v0}, Landroid/bluetooth/BluetoothAdapter;->setScanMode(I)Z

    #@194
    .line 115
    :cond_194
    :goto_194
    const/16 v17, 0x1

    #@196
    goto/16 :goto_17

    #@198
    .line 112
    :cond_198
    const/16 v17, 0x15

    #@19a
    move/from16 v0, v17

    #@19c
    invoke-virtual {v4, v0}, Landroid/bluetooth/BluetoothAdapter;->setScanMode(I)Z

    #@19f
    goto :goto_194

    #@1a0
    .line 116
    .end local v4           #adapter:Landroid/bluetooth/BluetoothAdapter;
    .end local v16           #state:I
    :cond_1a0
    const-string v17, "broadcom.bluetooth.action.DEBUG_DUMP_SESSION"

    #@1a2
    move-object/from16 v0, v17

    #@1a4
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a7
    move-result v17

    #@1a8
    if-eqz v17, :cond_20c

    #@1aa
    .line 117
    const-string v17, "SESSION_ID"

    #@1ac
    move-object/from16 v0, p1

    #@1ae
    move-object/from16 v1, v17

    #@1b0
    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@1b3
    move-result-object v14

    #@1b4
    .line 118
    .local v14, sessionIdString:Ljava/lang/String;
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1b7
    move-result v13

    #@1b8
    .line 119
    .local v13, sessionId:I
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@1bb
    move-result-object v17

    #@1bc
    move-object/from16 v0, v17

    #@1be
    invoke-virtual {v0, v13}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getSession(I)Lcom/broadcom/bt/service/map/MapClientSession;

    #@1c1
    move-result-object v12

    #@1c2
    .line 120
    .local v12, s:Lcom/broadcom/bt/service/map/MapClientSession;
    new-instance v5, Ljava/lang/StringBuilder;

    #@1c4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1c7
    .line 121
    .restart local v5       #b:Ljava/lang/StringBuilder;
    if-eqz v12, :cond_1d0

    #@1c9
    .line 122
    const-string v17, ""

    #@1cb
    move-object/from16 v0, v17

    #@1cd
    invoke-virtual {v12, v5, v0}, Lcom/broadcom/bt/service/map/MapClientSession;->dumpState(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@1d0
    .line 124
    :cond_1d0
    const-string v17, "BtMap.MapDebugUtils"

    #@1d2
    const-string v18, "***********MSE Session*******************"

    #@1d4
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d7
    .line 125
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1da
    move-result-object v7

    #@1db
    .line 126
    .local v7, debugStr:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@1de
    move-result v6

    #@1df
    .line 127
    .local v6, dLength:I
    const/4 v15, 0x0

    #@1e0
    .line 128
    .local v15, start:I
    :goto_1e0
    if-ge v15, v6, :cond_205

    #@1e2
    .line 129
    add-int/lit16 v9, v15, 0x1f4

    #@1e4
    .line 130
    .local v9, end:I
    if-lt v9, v6, :cond_1f8

    #@1e6
    .line 131
    add-int/lit8 v9, v6, -0x1

    #@1e8
    .line 138
    :cond_1e8
    :goto_1e8
    const-string v17, "BtMap.MapDebugUtils"

    #@1ea
    add-int/lit8 v18, v9, 0x1

    #@1ec
    move/from16 v0, v18

    #@1ee
    invoke-virtual {v7, v15, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@1f1
    move-result-object v18

    #@1f2
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f5
    .line 139
    add-int/lit8 v15, v9, 0x1

    #@1f7
    .line 140
    goto :goto_1e0

    #@1f8
    .line 133
    :cond_1f8
    const/16 v17, 0xa

    #@1fa
    move/from16 v0, v17

    #@1fc
    invoke-virtual {v7, v0, v9}, Ljava/lang/String;->indexOf(II)I

    #@1ff
    move-result v10

    #@200
    .line 134
    .local v10, index:I
    if-ltz v10, :cond_1e8

    #@202
    .line 135
    add-int/lit8 v9, v10, -0x1

    #@204
    goto :goto_1e8

    #@205
    .line 141
    .end local v9           #end:I
    .end local v10           #index:I
    :cond_205
    const-string v17, "BtMap.MapDebugUtils"

    #@207
    const-string v18, "*****************************************"

    #@209
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20c
    .line 143
    .end local v5           #b:Ljava/lang/StringBuilder;
    .end local v6           #dLength:I
    .end local v7           #debugStr:Ljava/lang/String;
    .end local v12           #s:Lcom/broadcom/bt/service/map/MapClientSession;
    .end local v13           #sessionId:I
    .end local v14           #sessionIdString:Ljava/lang/String;
    .end local v15           #start:I
    :cond_20c
    const/16 v17, 0x0

    #@20e
    goto/16 :goto_17
.end method
