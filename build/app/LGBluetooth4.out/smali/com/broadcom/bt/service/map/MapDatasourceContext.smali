.class public Lcom/broadcom/bt/service/map/MapDatasourceContext;
.super Ljava/lang/Object;
.source "MapDatasourceContext.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field static final INVALID_MSE_INSTANCE:I = -0x1

.field static final OP_NONE:I = -0x1

.field static final OP_START:I = 0x1

.field static final OP_STOP:I = 0x0

.field private static final TAG:Ljava/lang/String; = "BtMap.MapDatasourceContext"


# instance fields
.field mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

.field mDsDisplayName:Ljava/lang/String;

.field mDsId:Ljava/lang/String;

.field mDsType:B

.field mFeatureMessageFilter:Z

.field mFeatureMessageOffsetBrowsing:Z

.field mFolderMappings:[Ljava/lang/String;

.field mIsEnabled:Z

.field mMseInstanceId:I

.field mMseInstanceNotificationEnabled:Z

.field mMseInstanceStarted:Z

.field mPendingOperation:I

.field mProviderDisplayName:Ljava/lang/String;

.field mProviderId:Ljava/lang/String;

.field mSessions:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/broadcom/bt/service/map/MapClientSession;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;ZZ[Ljava/lang/String;Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;)V
    .registers 11
    .parameter "providerId"
    .parameter "providerDisplayName"
    .parameter "dsType"
    .parameter "dsId"
    .parameter "dsDisplayName"
    .parameter "supportsMessagefilter"
    .parameter "supportsMessageOffsetBrowsing"
    .parameter "virtualFolderMappings"
    .parameter "callback"

    #@0
    .prologue
    const/4 v0, -0x1

    #@1
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 83
    iput v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@6
    .line 86
    iput v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I

    #@8
    .line 101
    iput-byte p3, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsType:B

    #@a
    .line 102
    iput-object p1, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@c
    .line 103
    iput-object p2, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderDisplayName:Ljava/lang/String;

    #@e
    .line 104
    iput-object p4, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@10
    .line 105
    iput-object p5, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsDisplayName:Ljava/lang/String;

    #@12
    .line 106
    iput-boolean p6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mFeatureMessageFilter:Z

    #@14
    .line 107
    iput-boolean p7, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mFeatureMessageOffsetBrowsing:Z

    #@16
    .line 108
    iput-object p8, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mFolderMappings:[Ljava/lang/String;

    #@18
    .line 109
    iput-object p9, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@1a
    .line 110
    new-instance v0, Ljava/util/HashMap;

    #@1c
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@1f
    iput-object v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mSessions:Ljava/util/HashMap;

    #@21
    .line 111
    return-void
.end method

.method private static toDebugMessageType(I)Ljava/lang/String;
    .registers 3
    .parameter "msgType"

    #@0
    .prologue
    .line 288
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 289
    .local v0, m:Ljava/lang/StringBuilder;
    and-int/lit8 v1, p0, 0x1

    #@7
    if-lez v1, :cond_e

    #@9
    .line 290
    const-string v1, "EMAIL"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    .line 292
    :cond_e
    and-int/lit8 v1, p0, 0x8

    #@10
    if-lez v1, :cond_22

    #@12
    .line 293
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@15
    move-result v1

    #@16
    if-lez v1, :cond_1d

    #@18
    const-string v1, ", "

    #@1a
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 294
    :cond_1d
    const-string v1, "MMS"

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 296
    :cond_22
    and-int/lit8 v1, p0, 0x2

    #@24
    if-lez v1, :cond_36

    #@26
    .line 297
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@29
    move-result v1

    #@2a
    if-lez v1, :cond_31

    #@2c
    const-string v1, ", "

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    .line 298
    :cond_31
    const-string v1, "SMS_GSM"

    #@33
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    .line 300
    :cond_36
    and-int/lit8 v1, p0, 0x4

    #@38
    if-lez v1, :cond_4a

    #@3a
    .line 301
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@3d
    move-result v1

    #@3e
    if-lez v1, :cond_45

    #@40
    const-string v1, ", "

    #@42
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    .line 302
    :cond_45
    const-string v1, "SMS_CDMA"

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    .line 304
    :cond_4a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    return-object v1
.end method

.method static final toSessionKey(ILandroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .registers 3
    .parameter "sessionId"
    .parameter "remoteDevice"

    #@0
    .prologue
    .line 203
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method


# virtual methods
.method public addSession(ILandroid/bluetooth/BluetoothDevice;)Lcom/broadcom/bt/service/map/MapClientSession;
    .registers 9
    .parameter "sessionId"
    .parameter "remoteDevice"

    #@0
    .prologue
    .line 209
    invoke-static {p1, p2}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->toSessionKey(ILandroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 210
    .local v0, key:Ljava/lang/String;
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mSessions:Ljava/util/HashMap;

    #@6
    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Lcom/broadcom/bt/service/map/MapClientSession;

    #@c
    .line 211
    .local v1, s:Lcom/broadcom/bt/service/map/MapClientSession;
    if-eqz v1, :cond_26

    #@e
    .line 212
    const-string v3, "BtMap.MapDatasourceContext"

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "addSession: session already exists with key "

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 214
    :cond_26
    new-instance v2, Lcom/broadcom/bt/service/map/MapClientSession;

    #@28
    invoke-direct {v2, p0, p1, p2}, Lcom/broadcom/bt/service/map/MapClientSession;-><init>(Lcom/broadcom/bt/service/map/MapDatasourceContext;ILandroid/bluetooth/BluetoothDevice;)V

    #@2b
    .line 216
    .local v2, session:Lcom/broadcom/bt/service/map/MapClientSession;
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mSessions:Ljava/util/HashMap;

    #@2d
    invoke-virtual {v3, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@30
    .line 217
    return-object v2
.end method

.method public debugDumpActiveSessions(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .registers 7
    .parameter "builder"
    .parameter "prefix"

    #@0
    .prologue
    .line 404
    iget-object v3, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mSessions:Ljava/util/HashMap;

    #@2
    invoke-virtual {v3}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/util/HashMap;

    #@8
    .line 406
    .local v0, copy:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/Integer;Lcom/broadcom/bt/service/map/MapClientSession;>;"
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    .line 407
    const-string v3, "Total=  "

    #@d
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 408
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@13
    move-result v3

    #@14
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    .line 409
    const-string v3, "\n"

    #@19
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    .line 411
    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@1f
    move-result-object v3

    #@20
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@23
    move-result-object v1

    #@24
    .line 412
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/map/MapClientSession;>;"
    :goto_24
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@27
    move-result v3

    #@28
    if-eqz v3, :cond_65

    #@2a
    .line 413
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2d
    move-result-object v2

    #@2e
    check-cast v2, Lcom/broadcom/bt/service/map/MapClientSession;

    #@30
    .line 414
    .local v2, session:Lcom/broadcom/bt/service/map/MapClientSession;
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    .line 415
    const-string v3, "Session "

    #@35
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 416
    iget v3, v2, Lcom/broadcom/bt/service/map/MapClientSession;->mSessionId:I

    #@3a
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    .line 417
    const-string v3, ":    "

    #@3f
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    .line 418
    iget-object v3, v2, Lcom/broadcom/bt/service/map/MapClientSession;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@44
    if-eqz v3, :cond_53

    #@46
    .line 419
    iget-object v3, v2, Lcom/broadcom/bt/service/map/MapClientSession;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@48
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    invoke-static {v3}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@4f
    move-result-object v3

    #@50
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    .line 422
    :cond_53
    const-string v3, " :    Notification Enabled?: "

    #@55
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    .line 423
    invoke-virtual {v2}, Lcom/broadcom/bt/service/map/MapClientSession;->isNotificationEnabled()Z

    #@5b
    move-result v3

    #@5c
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5f
    .line 424
    const-string v3, "\n"

    #@61
    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    goto :goto_24

    #@65
    .line 426
    .end local v2           #session:Lcom/broadcom/bt/service/map/MapClientSession;
    :cond_65
    return-void
.end method

.method public debugDumpState(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .registers 11
    .parameter "buffer"
    .parameter "prefix"

    #@0
    .prologue
    .line 309
    if-nez p2, :cond_4

    #@2
    .line 310
    :try_start_2
    const-string p2, ""

    #@4
    .line 313
    :cond_4
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7
    .line 314
    const-string v6, "ProviderId:     "

    #@9
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    .line 315
    iget-object v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@e
    invoke-static {v6}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@11
    move-result-object v6

    #@12
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 317
    const-string v6, "\n"

    #@17
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 318
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    .line 319
    const-string v6, "ProviderName:   "

    #@1f
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    .line 320
    iget-object v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderDisplayName:Ljava/lang/String;

    #@24
    invoke-static {v6}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    .line 322
    const-string v6, "\n"

    #@2d
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    .line 323
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    .line 324
    const-string v6, "DatasourceId:   "

    #@35
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    .line 325
    iget-object v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@3a
    invoke-static {v6}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    .line 327
    const-string v6, "\n"

    #@43
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    .line 328
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    .line 329
    const-string v6, "DatasourceName: "

    #@4b
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    .line 330
    iget-object v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsDisplayName:Ljava/lang/String;

    #@50
    invoke-static {v6}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    .line 332
    const-string v6, "\n"

    #@59
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    .line 333
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    .line 334
    const-string v6, "DatasourceType: ("

    #@61
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    .line 335
    iget-byte v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsType:B

    #@66
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    .line 336
    const-string v6, "): "

    #@6b
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    .line 337
    iget-byte v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsType:B

    #@70
    invoke-static {v6}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->toDebugMessageType(I)Ljava/lang/String;

    #@73
    move-result-object v6

    #@74
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    .line 339
    const-string v6, "\n"

    #@79
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    .line 340
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    .line 341
    const-string v6, "MsgFiltering?:  "

    #@81
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    .line 342
    iget-boolean v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mFeatureMessageFilter:Z

    #@86
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@89
    .line 344
    const-string v6, "\n"

    #@8b
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    .line 345
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    .line 346
    const-string v6, "OffsetBrowsing?:"

    #@93
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    .line 347
    iget-boolean v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mFeatureMessageOffsetBrowsing:Z

    #@98
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@9b
    .line 349
    const-string v6, "\n"

    #@9d
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    .line 350
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    .line 351
    const-string v6, "Notifications?: "

    #@a5
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    .line 352
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->isNotificationEnabled()Z

    #@ab
    move-result v6

    #@ac
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@af
    .line 354
    const-string v6, "\n"

    #@b1
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    .line 355
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    .line 356
    const-string v6, "FolderMappings:\n"

    #@b9
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    .line 357
    iget-object v2, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mFolderMappings:[Ljava/lang/String;

    #@be
    .line 358
    .local v2, folderMappings:[Ljava/lang/String;
    if-eqz v2, :cond_e1

    #@c0
    array-length v6, v2

    #@c1
    if-lez v6, :cond_e1

    #@c3
    .line 359
    move-object v0, v2

    #@c4
    .local v0, arr$:[Ljava/lang/String;
    array-length v4, v0

    #@c5
    .local v4, len$:I
    const/4 v3, 0x0

    #@c6
    .local v3, i$:I
    :goto_c6
    if-ge v3, v4, :cond_e1

    #@c8
    aget-object v1, v0, v3

    #@ca
    .line 360
    .local v1, folderMapping:Ljava/lang/String;
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    .line 361
    const-string v6, "                "

    #@cf
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    .line 362
    invoke-static {v1}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@d5
    move-result-object v6

    #@d6
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    .line 363
    const-string v6, "\n"

    #@db
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    .line 359
    add-int/lit8 v3, v3, 0x1

    #@e0
    goto :goto_c6

    #@e1
    .line 366
    .end local v0           #arr$:[Ljava/lang/String;
    .end local v1           #folderMapping:Ljava/lang/String;
    .end local v3           #i$:I
    .end local v4           #len$:I
    :cond_e1
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    .line 367
    const-string v6, "Callback:       "

    #@e6
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    .line 368
    iget-object v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@eb
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ee
    .line 370
    const-string v6, "\n"

    #@f0
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    .line 371
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    .line 372
    const-string v6, "IsEnabled:      "

    #@f8
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    .line 373
    iget-boolean v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mIsEnabled:Z

    #@fd
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@100
    .line 375
    const-string v6, "\n"

    #@102
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    .line 376
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    .line 377
    const-string v6, "PendingOp  :    "

    #@10a
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    .line 378
    iget v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I

    #@10f
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@112
    .line 380
    const-string v6, "\n"

    #@114
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    .line 381
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    .line 382
    const-string v6, "MseStarted?:    "

    #@11c
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    .line 383
    iget-boolean v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceStarted:Z

    #@121
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@124
    .line 385
    const-string v6, "\n"

    #@126
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    .line 386
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    .line 387
    const-string v6, "MseInstanceId:  "

    #@12e
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    .line 388
    iget v6, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@133
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@136
    .line 390
    const-string v6, "\n"

    #@138
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    .line 392
    const-string v6, "\n"

    #@13d
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    .line 393
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    .line 394
    const-string v6, "Active Sessions: \n"

    #@145
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@148
    .line 395
    new-instance v6, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v6

    #@151
    const-string v7, "                "

    #@153
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v6

    #@157
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v6

    #@15b
    invoke-virtual {p0, p1, v6}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->debugDumpActiveSessions(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    #@15e
    .line 396
    const-string v6, "\n"

    #@160
    invoke-virtual {p1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_163
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_163} :catch_164

    #@163
    .line 400
    .end local v2           #folderMappings:[Ljava/lang/String;
    :goto_163
    return-void

    #@164
    .line 397
    :catch_164
    move-exception v5

    #@165
    .line 398
    .local v5, t:Ljava/lang/Throwable;
    const-string v6, "BtMap.MapDatasourceContext"

    #@167
    const-string v7, "DEBUG: error dumping datasource state"

    #@169
    invoke-static {v6, v7, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16c
    goto :goto_163
.end method

.method public describeContents()I
    .registers 2

    #@0
    .prologue
    .line 191
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method getPendingOperation()I
    .registers 2

    #@0
    .prologue
    .line 122
    iget v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I

    #@2
    return v0
.end method

.method public getSession(I)Lcom/broadcom/bt/service/map/MapClientSession;
    .registers 4
    .parameter "sessionId"

    #@0
    .prologue
    .line 221
    const/4 v1, 0x0

    #@1
    invoke-static {p1, v1}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->toSessionKey(ILandroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 222
    .local v0, key:Ljava/lang/String;
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mSessions:Ljava/util/HashMap;

    #@7
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v1

    #@b
    check-cast v1, Lcom/broadcom/bt/service/map/MapClientSession;

    #@d
    return-object v1
.end method

.method public getSessionMapCopy()Ljava/util/HashMap;
    .registers 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/broadcom/bt/service/map/MapClientSession;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 284
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mSessions:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Ljava/util/HashMap;

    #@8
    return-object v0
.end method

.method public hasActiveSession()Z
    .registers 2

    #@0
    .prologue
    .line 232
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mSessions:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    #@5
    move-result v0

    #@6
    if-lez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method isEqual(Ljava/lang/String;B)Z
    .registers 4
    .parameter "providerId"
    .parameter "dsType"

    #@0
    .prologue
    .line 183
    iget-byte v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsType:B

    #@2
    if-ne p2, v0, :cond_e

    #@4
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@6
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method isEqual(Ljava/lang/String;BLjava/lang/String;)Z
    .registers 5
    .parameter "providerId"
    .parameter "dsType"
    .parameter "dsId"

    #@0
    .prologue
    .line 178
    iget-byte v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsType:B

    #@2
    if-ne p2, v0, :cond_16

    #@4
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@6
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_16

    #@c
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@e
    invoke-virtual {p3, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_16

    #@14
    const/4 v0, 0x1

    #@15
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_15
.end method

.method isEqual(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 4
    .parameter "providerId"
    .parameter "dsId"

    #@0
    .prologue
    .line 174
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mProviderId:Ljava/lang/String;

    #@2
    invoke-virtual {p1, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_12

    #@8
    iget-object v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mDsId:Ljava/lang/String;

    #@a
    invoke-virtual {p2, v0}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_12

    #@10
    const/4 v0, 0x1

    #@11
    :goto_11
    return v0

    #@12
    :cond_12
    const/4 v0, 0x0

    #@13
    goto :goto_11
.end method

.method isMseInstanceStarted()Z
    .registers 3

    #@0
    .prologue
    .line 114
    iget v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceId:I

    #@2
    const/4 v1, -0x1

    #@3
    if-le v0, v1, :cond_b

    #@5
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceStarted:Z

    #@7
    if-eqz v0, :cond_b

    #@9
    const/4 v0, 0x1

    #@a
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public isNotificationEnabled()Z
    .registers 2

    #@0
    .prologue
    .line 279
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceNotificationEnabled:Z

    #@2
    return v0
.end method

.method declared-synchronized prepareToStartMse()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 126
    monitor-enter p0

    #@3
    :try_start_3
    iget-boolean v2, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mIsEnabled:Z

    #@5
    if-nez v2, :cond_10

    #@7
    .line 127
    const-string v1, "BtMap.MapDatasourceContext"

    #@9
    const-string v2, "prepareToStartMse(): Cannot start MSE for datasource. Datasource not enabled."

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_3 .. :try_end_e} :catchall_1e

    #@e
    .line 142
    :goto_e
    monitor-exit p0

    #@f
    return v0

    #@10
    .line 131
    :cond_10
    :try_start_10
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->isMseInstanceStarted()Z

    #@13
    move-result v2

    #@14
    if-eqz v2, :cond_21

    #@16
    .line 132
    const-string v1, "BtMap.MapDatasourceContext"

    #@18
    const-string v2, "prepareToStartMse(): Cannot start MSE for datasource. MSE already started."

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d
    .catchall {:try_start_10 .. :try_end_1d} :catchall_1e

    #@1d
    goto :goto_e

    #@1e
    .line 126
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit p0

    #@20
    throw v0

    #@21
    .line 136
    :cond_21
    :try_start_21
    iget v2, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I

    #@23
    const/4 v3, -0x1

    #@24
    if-eq v2, v3, :cond_2e

    #@26
    .line 137
    const-string v1, "BtMap.MapDatasourceContext"

    #@28
    const-string v2, "prepareToStartMse(): Cannot start MSE for datasource. MSE already has pending operation."

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_e

    #@2e
    .line 141
    :cond_2e
    const/4 v0, 0x1

    #@2f
    iput v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I
    :try_end_31
    .catchall {:try_start_21 .. :try_end_31} :catchall_1e

    #@31
    move v0, v1

    #@32
    .line 142
    goto :goto_e
.end method

.method declared-synchronized prepareToStopMse()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 146
    monitor-enter p0

    #@2
    :try_start_2
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->isMseInstanceStarted()Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_11

    #@8
    .line 147
    const-string v1, "BtMap.MapDatasourceContext"

    #@a
    const-string v2, "prepareToStopMse(): Cannot stop MSE for datasource. MSE already STOPPED."

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catchall {:try_start_2 .. :try_end_f} :catchall_1e

    #@f
    .line 157
    :goto_f
    monitor-exit p0

    #@10
    return v0

    #@11
    .line 151
    :cond_11
    :try_start_11
    iget v1, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I

    #@13
    const/4 v2, -0x1

    #@14
    if-eq v1, v2, :cond_21

    #@16
    .line 152
    const-string v1, "BtMap.MapDatasourceContext"

    #@18
    const-string v2, "prepareToStopMse(): Cannot stop MSE for datasource. MSE already has pending operation."

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d
    .catchall {:try_start_11 .. :try_end_1d} :catchall_1e

    #@1d
    goto :goto_f

    #@1e
    .line 146
    :catchall_1e
    move-exception v0

    #@1f
    monitor-exit p0

    #@20
    throw v0

    #@21
    .line 156
    :cond_21
    const/4 v0, 0x0

    #@22
    :try_start_22
    iput v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I
    :try_end_24
    .catchall {:try_start_22 .. :try_end_24} :catchall_1e

    #@24
    .line 157
    const/4 v0, 0x1

    #@25
    goto :goto_f
.end method

.method public removeSession(ILandroid/bluetooth/BluetoothDevice;)Lcom/broadcom/bt/service/map/MapClientSession;
    .registers 5
    .parameter "sessionId"
    .parameter "remoteDevice"

    #@0
    .prologue
    .line 227
    invoke-static {p1, p2}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->toSessionKey(ILandroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 228
    .local v0, key:Ljava/lang/String;
    iget-object v1, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mSessions:Ljava/util/HashMap;

    #@6
    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@9
    move-result-object v1

    #@a
    check-cast v1, Lcom/broadcom/bt/service/map/MapClientSession;

    #@c
    return-object v1
.end method

.method setMseInstanceStarted(Z)V
    .registers 5
    .parameter "isStarted"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 161
    iput-boolean p1, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceStarted:Z

    #@3
    .line 162
    iget-boolean v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceStarted:Z

    #@5
    if-eqz v0, :cond_f

    #@7
    .line 163
    iget v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I

    #@9
    const/4 v1, 0x1

    #@a
    if-ne v0, v1, :cond_e

    #@c
    .line 164
    iput v2, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I

    #@e
    .line 171
    :cond_e
    :goto_e
    return-void

    #@f
    .line 167
    :cond_f
    iget v0, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I

    #@11
    if-nez v0, :cond_e

    #@13
    .line 168
    iput v2, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I

    #@15
    goto :goto_e
.end method

.method setPendingOperation(I)V
    .registers 2
    .parameter "op"

    #@0
    .prologue
    .line 118
    iput p1, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mPendingOperation:I

    #@2
    .line 119
    return-void
.end method

.method public updateNotificationState(IZ)V
    .registers 14
    .parameter "sessionId"
    .parameter "enabled"

    #@0
    .prologue
    .line 236
    invoke-virtual {p0, p1}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->getSession(I)Lcom/broadcom/bt/service/map/MapClientSession;

    #@3
    move-result-object v5

    #@4
    .line 237
    .local v5, s:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v5, :cond_1f

    #@6
    .line 238
    const-string v8, "BtMap.MapDatasourceContext"

    #@8
    new-instance v9, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v10, "updateNotificationRegistration() session not found with sessionId "

    #@f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v9

    #@13
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v9

    #@17
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v9

    #@1b
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 276
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 243
    :cond_1f
    invoke-virtual {v5, p2}, Lcom/broadcom/bt/service/map/MapClientSession;->setNotificationState(Z)V

    #@22
    .line 244
    iget-boolean v4, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceNotificationEnabled:Z

    #@24
    .line 247
    .local v4, oldNotfEnabled:Z
    const/4 v3, 0x0

    #@25
    .line 249
    .local v3, newNotfEnabled:Z
    iget-object v8, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mSessions:Ljava/util/HashMap;

    #@27
    invoke-virtual {v8}, Ljava/util/HashMap;->clone()Ljava/lang/Object;

    #@2a
    move-result-object v2

    #@2b
    check-cast v2, Ljava/util/HashMap;

    #@2d
    .line 251
    .local v2, mSessionsCopy:Ljava/util/HashMap;,"Ljava/util/HashMap<Ljava/lang/String;Lcom/broadcom/bt/service/map/MapClientSession;>;"
    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@30
    move-result-object v0

    #@31
    .line 252
    .local v0, c:Ljava/util/Collection;,"Ljava/util/Collection<Lcom/broadcom/bt/service/map/MapClientSession;>;"
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@34
    move-result-object v1

    #@35
    .line 253
    .local v1, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Lcom/broadcom/bt/service/map/MapClientSession;>;"
    :cond_35
    :goto_35
    if-nez v3, :cond_4b

    #@37
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@3a
    move-result v8

    #@3b
    if-eqz v8, :cond_4b

    #@3d
    .line 254
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@40
    move-result-object v6

    #@41
    check-cast v6, Lcom/broadcom/bt/service/map/MapClientSession;

    #@43
    .line 255
    .local v6, session:Lcom/broadcom/bt/service/map/MapClientSession;
    invoke-virtual {v6}, Lcom/broadcom/bt/service/map/MapClientSession;->isNotificationEnabled()Z

    #@46
    move-result v8

    #@47
    if-eqz v8, :cond_35

    #@49
    .line 256
    const/4 v3, 0x1

    #@4a
    goto :goto_35

    #@4b
    .line 259
    .end local v6           #session:Lcom/broadcom/bt/service/map/MapClientSession;
    :cond_4b
    iput-boolean v3, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceNotificationEnabled:Z

    #@4d
    .line 261
    const-string v8, "BtMap.MapDatasourceContext"

    #@4f
    new-instance v9, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v10, "updateNotificationState() new state = "

    #@56
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v9

    #@5a
    iget-boolean v10, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceNotificationEnabled:Z

    #@5c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v9

    #@60
    const-string v10, ", old state = "

    #@62
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v9

    #@66
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@69
    move-result-object v9

    #@6a
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v9

    #@6e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 264
    iget-boolean v8, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mMseInstanceNotificationEnabled:Z

    #@73
    if-eq v8, v4, :cond_1e

    #@75
    .line 266
    const-string v8, "BtMap.MapDatasourceContext"

    #@77
    const-string v9, "updateNotificationRegistration(): sending notification changed to datasource"

    #@79
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 269
    :try_start_7c
    iget-object v8, p0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@7e
    invoke-interface {v8, v3}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onClientRegistrationChanged(Z)V
    :try_end_81
    .catch Ljava/lang/Throwable; {:try_start_7c .. :try_end_81} :catch_82

    #@81
    goto :goto_1e

    #@82
    .line 270
    :catch_82
    move-exception v7

    #@83
    .line 271
    .local v7, t:Ljava/lang/Throwable;
    const-string v8, "BtMap.MapDatasourceContext"

    #@85
    const-string v9, "Unable to send notification state change event to datasource"

    #@87
    invoke-static {v8, v9, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8a
    goto :goto_1e
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .registers 3
    .parameter "dest"
    .parameter "flags"

    #@0
    .prologue
    .line 198
    return-void
.end method
