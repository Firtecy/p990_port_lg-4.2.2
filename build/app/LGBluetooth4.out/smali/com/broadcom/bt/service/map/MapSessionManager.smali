.class public Lcom/broadcom/bt/service/map/MapSessionManager;
.super Ljava/lang/Object;
.source "MapSessionManager.java"


# static fields
.field static final DATE_TIME_FORMATTER:Ljava/text/SimpleDateFormat; = null

.field private static final DBG:Z = true

.field public static final TAG:Ljava/lang/String; = "BtMap.MapSessionManager"


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 70
    new-instance v0, Ljava/text/SimpleDateFormat;

    #@2
    const-string v1, "yyyyMMddHHmmss"

    #@4
    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    #@7
    sput-object v0, Lcom/broadcom/bt/service/map/MapSessionManager;->DATE_TIME_FORMATTER:Ljava/text/SimpleDateFormat;

    #@9
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method private static final getDestFolderFromBMessage(Ljava/lang/String;)Ljava/lang/String;
    .registers 11
    .parameter "bmsgFilePath"

    #@0
    .prologue
    const/4 v9, 0x7

    #@1
    .line 400
    const/4 v1, 0x0

    #@2
    .line 402
    .local v1, folder:Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    #@4
    invoke-direct {v0, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@7
    .line 403
    .local v0, f:Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    #@a
    move-result v6

    #@b
    if-eqz v6, :cond_73

    #@d
    .line 404
    const/4 v3, 0x0

    #@e
    .line 405
    .local v3, r:Ljava/io/BufferedReader;
    const/4 v2, 0x0

    #@f
    .line 407
    .local v2, line:Ljava/lang/String;
    :try_start_f
    new-instance v4, Ljava/io/BufferedReader;

    #@11
    new-instance v6, Ljava/io/FileReader;

    #@13
    invoke-direct {v6, v0}, Ljava/io/FileReader;-><init>(Ljava/io/File;)V

    #@16
    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_19
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_19} :catch_79

    #@19
    .line 408
    .end local v3           #r:Ljava/io/BufferedReader;
    .local v4, r:Ljava/io/BufferedReader;
    :cond_19
    :goto_19
    :try_start_19
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    if-eqz v2, :cond_77

    #@1f
    .line 409
    if-nez v1, :cond_19

    #@21
    if-eqz v2, :cond_19

    #@23
    const-string v6, "FOLDER:"

    #@25
    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@28
    move-result v6

    #@29
    if-eqz v6, :cond_19

    #@2b
    .line 411
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@2e
    move-result v6

    #@2f
    if-le v6, v9, :cond_74

    #@31
    const/4 v6, 0x7

    #@32
    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    .line 413
    :goto_36
    const-string v6, "BtMap.MapSessionManager"

    #@38
    new-instance v7, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v8, "getDestFolderFromBMessage(): Parsed BMessage with "

    #@3f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v7

    #@4b
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 416
    const-string v6, "BtMap.MapSessionManager"

    #@50
    new-instance v7, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v8, "Folder = "

    #@57
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v7

    #@5f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v7

    #@63
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_66
    .catch Ljava/lang/Throwable; {:try_start_19 .. :try_end_66} :catch_67

    #@66
    goto :goto_19

    #@67
    .line 420
    :catch_67
    move-exception v5

    #@68
    move-object v3, v4

    #@69
    .line 421
    .end local v4           #r:Ljava/io/BufferedReader;
    .restart local v3       #r:Ljava/io/BufferedReader;
    .local v5, t:Ljava/lang/Throwable;
    :goto_69
    const-string v6, "BtMap.MapSessionManager"

    #@6b
    const-string v7, "getDestFolderFromBMessage(): error reading file"

    #@6d
    invoke-static {v6, v7, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@70
    .line 423
    .end local v5           #t:Ljava/lang/Throwable;
    :goto_70
    invoke-static {v3}, Lcom/broadcom/bt/util/io/IOUtils;->closeQuietly(Ljava/io/Reader;)V

    #@73
    .line 425
    .end local v2           #line:Ljava/lang/String;
    .end local v3           #r:Ljava/io/BufferedReader;
    :cond_73
    return-object v1

    #@74
    .line 411
    .restart local v2       #line:Ljava/lang/String;
    .restart local v4       #r:Ljava/io/BufferedReader;
    :cond_74
    :try_start_74
    const-string v1, ""
    :try_end_76
    .catch Ljava/lang/Throwable; {:try_start_74 .. :try_end_76} :catch_67

    #@76
    goto :goto_36

    #@77
    :cond_77
    move-object v3, v4

    #@78
    .line 422
    .end local v4           #r:Ljava/io/BufferedReader;
    .restart local v3       #r:Ljava/io/BufferedReader;
    goto :goto_70

    #@79
    .line 420
    :catch_79
    move-exception v5

    #@7a
    goto :goto_69
.end method

.method static getFolderEntry(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZJ)V
    .registers 25
    .parameter "svc"
    .parameter "requestId"
    .parameter "path"
    .parameter "isFirstEntry"
    .parameter "entryId"

    #@0
    .prologue
    .line 99
    const-string v4, "BtMap.MapSessionManager"

    #@2
    new-instance v5, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v6, "getFolderEntry(): sessionId="

    #@9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v5

    #@d
    move-object/from16 v0, p1

    #@f
    iget v6, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    const-string v6, ", eventId="

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    move-object/from16 v0, p1

    #@1d
    iget v6, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    const-string v6, ", path="

    #@25
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    invoke-static/range {p2 .. p2}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v5

    #@31
    const-string v6, ", isFirstEntry="

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    move/from16 v0, p3

    #@39
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    const-string v6, ", entryId="

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    move-wide/from16 v0, p4

    #@45
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 105
    move-object/from16 v0, p1

    #@52
    iget v4, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@54
    const-string v5, "getFolderEntry"

    #@56
    invoke-static {v4, v5}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@59
    move-result-object v17

    #@5a
    .line 107
    .local v17, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v17, :cond_65

    #@5c
    .line 108
    const/4 v4, 0x1

    #@5d
    move-object/from16 v0, p0

    #@5f
    move-object/from16 v1, p1

    #@61
    invoke-virtual {v0, v4, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderNoEntryResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@64
    .line 157
    :goto_64
    return-void

    #@65
    .line 112
    :cond_65
    move-object/from16 v0, v17

    #@67
    move-object/from16 v1, p2

    #@69
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/MapClientSession;->getPathOrReturnCurrent(Ljava/lang/String;)Ljava/lang/String;

    #@6c
    move-result-object v15

    #@6d
    .line 113
    .local v15, resolvedPath:Ljava/lang/String;
    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@70
    move-result-object v16

    #@71
    .line 114
    .local v16, resolvedPathLC:Ljava/lang/String;
    const-string v4, "root"

    #@73
    move-object/from16 v0, v16

    #@75
    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@78
    move-result v4

    #@79
    if-eqz v4, :cond_97

    #@7b
    .line 115
    if-eqz p3, :cond_8e

    #@7d
    .line 116
    const/4 v5, 0x1

    #@7e
    const/4 v7, 0x1

    #@7f
    const/4 v8, 0x1

    #@80
    const/4 v9, 0x0

    #@81
    const-string v10, "telecom"

    #@83
    const-string v11, ""

    #@85
    const/4 v12, 0x0

    #@86
    move-object/from16 v4, p0

    #@88
    move-object/from16 v6, p1

    #@8a
    invoke-virtual/range {v4 .. v12}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIILjava/lang/String;Ljava/lang/String;Z)V

    #@8d
    goto :goto_64

    #@8e
    .line 119
    :cond_8e
    const/4 v4, 0x1

    #@8f
    move-object/from16 v0, p0

    #@91
    move-object/from16 v1, p1

    #@93
    invoke-virtual {v0, v4, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderNoEntryResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@96
    goto :goto_64

    #@97
    .line 122
    :cond_97
    const-string v4, "root/telecom"

    #@99
    move-object/from16 v0, v16

    #@9b
    invoke-virtual {v4, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@9e
    move-result v4

    #@9f
    if-eqz v4, :cond_bd

    #@a1
    .line 124
    if-eqz p3, :cond_b4

    #@a3
    .line 125
    const/4 v5, 0x1

    #@a4
    const/4 v7, 0x1

    #@a5
    const/4 v8, 0x1

    #@a6
    const/4 v9, 0x0

    #@a7
    const-string v10, "msg"

    #@a9
    const-string v11, ""

    #@ab
    const/4 v12, 0x0

    #@ac
    move-object/from16 v4, p0

    #@ae
    move-object/from16 v6, p1

    #@b0
    invoke-virtual/range {v4 .. v12}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIILjava/lang/String;Ljava/lang/String;Z)V

    #@b3
    goto :goto_64

    #@b4
    .line 128
    :cond_b4
    const/4 v4, 0x1

    #@b5
    move-object/from16 v0, p0

    #@b7
    move-object/from16 v1, p1

    #@b9
    invoke-virtual {v0, v4, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderNoEntryResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@bc
    goto :goto_64

    #@bd
    .line 132
    :cond_bd
    if-eqz p3, :cond_dc

    #@bf
    .line 134
    :try_start_bf
    move-object/from16 v0, v17

    #@c1
    invoke-virtual {v0, v15}, Lcom/broadcom/bt/service/map/MapClientSession;->getDatasourcePath(Ljava/lang/String;)Ljava/lang/String;

    #@c4
    move-result-object v13

    #@c5
    .line 135
    .local v13, dsPath:Ljava/lang/String;
    move-object/from16 v0, v17

    #@c7
    iget-object v4, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@c9
    iget-object v4, v4, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@cb
    move-object/from16 v0, p1

    #@cd
    invoke-interface {v4, v0, v13}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onGetFolderListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;)V
    :try_end_d0
    .catch Ljava/lang/Throwable; {:try_start_bf .. :try_end_d0} :catch_d1

    #@d0
    goto :goto_64

    #@d1
    .line 137
    .end local v13           #dsPath:Ljava/lang/String;
    :catch_d1
    move-exception v18

    #@d2
    .line 138
    .local v18, t:Ljava/lang/Throwable;
    const-string v4, "BtMap.MapSessionManager"

    #@d4
    const-string v5, "getFolderEntry(): error calling datsource onGetFolderListing"

    #@d6
    move-object/from16 v0, v18

    #@d8
    invoke-static {v4, v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@db
    goto :goto_64

    #@dc
    .line 143
    .end local v18           #t:Ljava/lang/Throwable;
    :cond_dc
    move-object/from16 v0, p1

    #@de
    iget v4, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@e0
    move-object/from16 v0, v17

    #@e2
    move-object/from16 v1, p2

    #@e4
    move-wide/from16 v2, p4

    #@e6
    invoke-virtual {v0, v4, v1, v2, v3}, Lcom/broadcom/bt/service/map/MapClientSession;->getFolderEntry(ILjava/lang/String;J)Lcom/broadcom/bt/map/FolderInfo;

    #@e9
    move-result-object v14

    #@ea
    .line 145
    .local v14, fInfo:Lcom/broadcom/bt/map/FolderInfo;
    if-eqz v14, :cond_107

    #@ec
    .line 146
    const/4 v5, 0x1

    #@ed
    const/4 v7, 0x1

    #@ee
    move-wide/from16 v0, p4

    #@f0
    long-to-int v4, v0

    #@f1
    add-int/lit8 v8, v4, 0x1

    #@f3
    iget-wide v9, v14, Lcom/broadcom/bt/map/FolderInfo;->mFolderSizeBytes:J

    #@f5
    long-to-int v9, v9

    #@f6
    iget-object v10, v14, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    #@f8
    iget-object v11, v14, Lcom/broadcom/bt/map/FolderInfo;->mCreatedDateTimeMS:Ljava/lang/String;

    #@fa
    invoke-virtual {v14}, Lcom/broadcom/bt/map/FolderInfo;->isReadOnly()Z

    #@fd
    move-result v12

    #@fe
    move-object/from16 v4, p0

    #@100
    move-object/from16 v6, p1

    #@102
    invoke-virtual/range {v4 .. v12}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIILjava/lang/String;Ljava/lang/String;Z)V

    #@105
    goto/16 :goto_64

    #@107
    .line 152
    :cond_107
    move-object/from16 v0, p1

    #@109
    iget v4, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@10b
    move-object/from16 v0, v17

    #@10d
    move-object/from16 v1, p2

    #@10f
    invoke-virtual {v0, v4, v1}, Lcom/broadcom/bt/service/map/MapClientSession;->unloadFolderEntries(ILjava/lang/String;)V

    #@112
    .line 153
    const/4 v4, 0x1

    #@113
    move-object/from16 v0, p0

    #@115
    move-object/from16 v1, p1

    #@117
    invoke-virtual {v0, v4, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderNoEntryResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@11a
    goto/16 :goto_64
.end method

.method static getMessage(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;JZB)V
    .registers 16
    .parameter "svc"
    .parameter "requestId"
    .parameter "messageHandle"
    .parameter "includeAttachments"
    .parameter "charSet"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    .line 358
    const-string v0, "BtMap.MapSessionManager"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v5, "getMessage: sessionId="

    #@a
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget v5, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@10
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    const-string v5, ", eventId="

    #@16
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    iget v5, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1c
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v5, ", includeAttachments="

    #@22
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v5, ", charSet= "

    #@2c
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 363
    iget v0, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@3d
    const-string v1, "getMessage"

    #@3f
    invoke-static {v0, v1}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@42
    move-result-object v7

    #@43
    .line 365
    .local v7, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v7, :cond_49

    #@45
    .line 366
    invoke-virtual {p0, v9, p1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@48
    .line 388
    :goto_48
    return-void

    #@49
    .line 370
    :cond_49
    invoke-virtual {v7, p2, p3}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageIdFromHandle(J)Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    .line 371
    .local v4, messageId:Ljava/lang/String;
    invoke-virtual {v7, v4}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageFolderPath(Ljava/lang/String;)Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    .line 372
    .local v2, dsFolderPath:Ljava/lang/String;
    if-eqz v4, :cond_55

    #@53
    if-nez v2, :cond_71

    #@55
    .line 373
    :cond_55
    const-string v0, "BtMap.MapSessionManager"

    #@57
    new-instance v1, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v5, "Message Handle not found for messageId"

    #@5e
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v1

    #@6a
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 374
    invoke-virtual {p0, v9, p1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@70
    goto :goto_48

    #@71
    .line 378
    :cond_71
    const/4 v0, 0x0

    #@72
    :try_start_72
    invoke-virtual {v7, v2, v0}, Lcom/broadcom/bt/service/map/MapClientSession;->getVirtualPath(Ljava/lang/String;Z)Ljava/lang/String;

    #@75
    move-result-object v3

    #@76
    .line 379
    .local v3, virtualPath:Ljava/lang/String;
    const-string v0, "BtMap.MapSessionManager"

    #@78
    new-instance v1, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v5, "getMessage(): virtual path = "

    #@7f
    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v1

    #@83
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v1

    #@8b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 380
    iget-object v0, v7, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@90
    iget-object v0, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@92
    move-object v1, p1

    #@93
    move v5, p4

    #@94
    move v6, p5

    #@95
    invoke-interface/range {v0 .. v6}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onGetMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZB)V
    :try_end_98
    .catch Ljava/lang/Throwable; {:try_start_72 .. :try_end_98} :catch_99

    #@98
    goto :goto_48

    #@99
    .line 382
    .end local v3           #virtualPath:Ljava/lang/String;
    :catch_99
    move-exception v8

    #@9a
    .line 383
    .local v8, t:Ljava/lang/Throwable;
    const-string v0, "BtMap.MapSessionManager"

    #@9c
    const-string v1, "getMessage(): error calling datsource onGetFolderListing"

    #@9e
    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a1
    .line 386
    invoke-virtual {p0, v9, p1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@a4
    goto :goto_48
.end method

.method static getMessageInfo(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZJLcom/broadcom/bt/map/MessageListFilter;J)V
    .registers 45
    .parameter "svc"
    .parameter "requestId"
    .parameter "path"
    .parameter "isFirstEntry"
    .parameter "entryId"
    .parameter "filter"
    .parameter "parameterMask"

    #@0
    .prologue
    .line 295
    move-object/from16 v0, p1

    #@2
    iget v2, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@4
    const-string v3, "getMessageInfo"

    #@6
    invoke-static {v2, v3}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@9
    move-result-object v34

    #@a
    .line 297
    .local v34, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v34, :cond_15

    #@c
    .line 298
    const/4 v2, 0x1

    #@d
    move-object/from16 v0, p0

    #@f
    move-object/from16 v1, p1

    #@11
    invoke-virtual {v0, v2, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@14
    .line 353
    .end local p4
    :goto_14
    return-void

    #@15
    .line 301
    .restart local p4
    :cond_15
    move-object/from16 v0, v34

    #@17
    move-object/from16 v1, p2

    #@19
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/MapClientSession;->getAppendedPath(Ljava/lang/String;)Ljava/lang/String;

    #@1c
    move-result-object v30

    #@1d
    .line 302
    .local v30, fullPath:Ljava/lang/String;
    invoke-virtual/range {v30 .. v30}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@20
    move-result-object v31

    #@21
    .line 303
    .local v31, fullPathLC:Ljava/lang/String;
    const-string v2, "root"

    #@23
    move-object/from16 v0, v31

    #@25
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@28
    move-result v2

    #@29
    if-nez v2, :cond_35

    #@2b
    const-string v2, "root/telecom"

    #@2d
    move-object/from16 v0, v31

    #@2f
    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_58

    #@35
    .line 306
    :cond_35
    const-string v2, "BtMap.MapSessionManager"

    #@37
    new-instance v3, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v5, "No messages in MAP standard folder "

    #@3e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    move-object/from16 v0, v31

    #@44
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 307
    const/4 v2, 0x1

    #@50
    move-object/from16 v0, p0

    #@52
    move-object/from16 v1, p1

    #@54
    invoke-virtual {v0, v2, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@57
    goto :goto_14

    #@58
    .line 310
    :cond_58
    move-object/from16 v0, v34

    #@5a
    move-object/from16 v1, v30

    #@5c
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/map/MapClientSession;->getDatasourcePath(Ljava/lang/String;)Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    .line 312
    .local v4, dsPath:Ljava/lang/String;
    if-eqz p3, :cond_8b

    #@62
    .line 316
    const-string v2, "BtMap.MapSessionManager"

    #@64
    const-string v3, "getMessageInfo(): first entry query..loading message infos"

    #@66
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 320
    :try_start_69
    move-object/from16 v0, v34

    #@6b
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@6d
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@6f
    const/4 v8, 0x0

    #@70
    move-object/from16 v3, p1

    #@72
    move-object/from16 v5, p6

    #@74
    move-wide/from16 v6, p7

    #@76
    invoke-interface/range {v2 .. v8}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onGetMessageListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;JZ)V
    :try_end_79
    .catch Ljava/lang/Throwable; {:try_start_69 .. :try_end_79} :catch_7a

    #@79
    goto :goto_14

    #@7a
    .line 322
    :catch_7a
    move-exception v35

    #@7b
    .line 323
    .local v35, t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapSessionManager"

    #@7d
    const-string v3, "getMessageListInfo: error calling datasource "

    #@7f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 324
    const/4 v2, 0x1

    #@83
    move-object/from16 v0, p0

    #@85
    move-object/from16 v1, p1

    #@87
    invoke-virtual {v0, v2, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@8a
    goto :goto_14

    #@8b
    .line 329
    .end local v35           #t:Ljava/lang/Throwable;
    :cond_8b
    if-eqz p3, :cond_8f

    #@8d
    const-wide/16 p4, 0x0

    #@8f
    .end local p4
    :cond_8f
    move-wide/from16 v0, p4

    #@91
    long-to-int v0, v0

    #@92
    move/from16 v32, v0

    #@94
    .line 330
    .local v32, mEntryId:I
    move-object/from16 v0, p1

    #@96
    iget v2, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@98
    move/from16 v0, v32

    #@9a
    int-to-long v5, v0

    #@9b
    move-object/from16 v0, v34

    #@9d
    invoke-virtual {v0, v2, v4, v5, v6}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageInfo(ILjava/lang/String;J)Lcom/broadcom/bt/map/MessageInfo;

    #@a0
    move-result-object v33

    #@a1
    .line 332
    .local v33, mInfo:Lcom/broadcom/bt/map/MessageInfo;
    if-eqz v33, :cond_133

    #@a3
    .line 334
    move-object/from16 v0, v33

    #@a5
    iget-object v2, v0, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    #@a7
    move-object/from16 v0, v34

    #@a9
    invoke-virtual {v0, v2}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageHandleFromId(Ljava/lang/String;)J

    #@ac
    move-result-wide v10

    #@ad
    .line 336
    .local v10, messageHandle:J
    const-wide/16 v2, 0x0

    #@af
    cmp-long v2, v10, v2

    #@b1
    if-gez v2, :cond_c2

    #@b3
    .line 337
    const-string v2, "BtMap.MapSessionManager"

    #@b5
    const-string v3, "getMessageInfo(): invalid message handle"

    #@b7
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ba
    .line 338
    const/4 v2, 0x1

    #@bb
    move-object/from16 v0, p0

    #@bd
    move-object/from16 v1, p1

    #@bf
    invoke-virtual {v0, v2, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@c2
    .line 340
    :cond_c2
    const/4 v6, 0x1

    #@c3
    const/4 v8, 0x1

    #@c4
    add-int/lit8 v9, v32, 0x1

    #@c6
    move-object/from16 v0, v33

    #@c8
    iget-byte v12, v0, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    #@ca
    move-object/from16 v0, v33

    #@cc
    iget v13, v0, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I

    #@ce
    move-object/from16 v0, v33

    #@d0
    iget v14, v0, Lcom/broadcom/bt/map/MessageInfo;->mAttachmentSize:I

    #@d2
    move-object/from16 v0, v33

    #@d4
    iget-wide v15, v0, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    #@d6
    const/4 v2, 0x1

    #@d7
    move-object/from16 v0, v33

    #@d9
    invoke-virtual {v0, v2}, Lcom/broadcom/bt/map/MessageInfo;->getSenderDisplayName(Z)Ljava/lang/String;

    #@dc
    move-result-object v17

    #@dd
    const/4 v2, 0x1

    #@de
    move-object/from16 v0, v33

    #@e0
    invoke-virtual {v0, v2}, Lcom/broadcom/bt/map/MessageInfo;->getSenderAddress(Z)Ljava/lang/String;

    #@e3
    move-result-object v18

    #@e4
    const/4 v2, 0x1

    #@e5
    move-object/from16 v0, v33

    #@e7
    invoke-virtual {v0, v2}, Lcom/broadcom/bt/map/MessageInfo;->getRecipientDisplayName(Z)Ljava/lang/String;

    #@ea
    move-result-object v19

    #@eb
    const/4 v2, 0x1

    #@ec
    move-object/from16 v0, v33

    #@ee
    invoke-virtual {v0, v2}, Lcom/broadcom/bt/map/MessageInfo;->getRecipientAddress(Z)Ljava/lang/String;

    #@f1
    move-result-object v20

    #@f2
    const/4 v2, 0x1

    #@f3
    move-object/from16 v0, v33

    #@f5
    invoke-virtual {v0, v2}, Lcom/broadcom/bt/map/MessageInfo;->getReplyToAddress(Z)Ljava/lang/String;

    #@f8
    move-result-object v21

    #@f9
    const/4 v2, 0x1

    #@fa
    move-object/from16 v0, v33

    #@fc
    invoke-virtual {v0, v2}, Lcom/broadcom/bt/map/MessageInfo;->getSubject(Z)Ljava/lang/String;

    #@ff
    move-result-object v22

    #@100
    move-object/from16 v0, v33

    #@102
    iget-object v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    #@104
    move-object/from16 v23, v0

    #@106
    move-object/from16 v0, v33

    #@108
    iget-boolean v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    #@10a
    move/from16 v24, v0

    #@10c
    move-object/from16 v0, v33

    #@10e
    iget-boolean v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    #@110
    move/from16 v25, v0

    #@112
    move-object/from16 v0, v33

    #@114
    iget-boolean v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    #@116
    move/from16 v26, v0

    #@118
    move-object/from16 v0, v33

    #@11a
    iget-boolean v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    #@11c
    move/from16 v27, v0

    #@11e
    move-object/from16 v0, v33

    #@120
    iget-boolean v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    #@122
    move/from16 v28, v0

    #@124
    move-object/from16 v0, v33

    #@126
    iget-byte v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mReceptionStatus:B

    #@128
    move/from16 v29, v0

    #@12a
    move-object/from16 v5, p0

    #@12c
    move-object/from16 v7, p1

    #@12e
    invoke-virtual/range {v5 .. v29}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIJBIIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZB)V

    #@131
    goto/16 :goto_14

    #@133
    .line 350
    .end local v10           #messageHandle:J
    :cond_133
    move-object/from16 v0, p1

    #@135
    iget v2, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@137
    move-object/from16 v0, v34

    #@139
    invoke-virtual {v0, v2, v4}, Lcom/broadcom/bt/service/map/MapClientSession;->unloadMessageEntries(ILjava/lang/String;)V

    #@13c
    .line 351
    const/4 v2, 0x1

    #@13d
    move-object/from16 v0, p0

    #@13f
    move-object/from16 v1, p1

    #@141
    invoke-virtual {v0, v2, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@144
    goto/16 :goto_14
.end method

.method static getMessageListInfo(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;J)V
    .registers 17
    .parameter "svc"
    .parameter "requestId"
    .parameter "path"
    .parameter "filter"
    .parameter "parameterMask"

    #@0
    .prologue
    .line 188
    const-string v0, "BtMap.MapSessionManager"

    #@2
    const-string v1, "getMessageListInfo()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 190
    iget v0, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@9
    const-string v1, "getMessageListInfo"

    #@b
    invoke-static {v0, v1}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@e
    move-result-object v9

    #@f
    .line 192
    .local v9, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v9, :cond_16

    #@11
    .line 193
    const/4 v0, 0x1

    #@12
    invoke-virtual {p0, v0, p1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageListInfoNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@15
    .line 214
    :goto_15
    return-void

    #@16
    .line 197
    :cond_16
    invoke-virtual {v9, p2}, Lcom/broadcom/bt/service/map/MapClientSession;->getAppendedPath(Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v7

    #@1a
    .line 198
    .local v7, fullPath:Ljava/lang/String;
    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@1d
    move-result-object v8

    #@1e
    .line 199
    .local v8, fullPathLC:Ljava/lang/String;
    const-string v0, "root"

    #@20
    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@23
    move-result v0

    #@24
    if-nez v0, :cond_2e

    #@26
    const-string v0, "root/telecom"

    #@28
    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2b
    move-result v0

    #@2c
    if-eqz v0, :cond_4b

    #@2e
    .line 202
    :cond_2e
    const-string v0, "BtMap.MapSessionManager"

    #@30
    new-instance v1, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v3, "No messages in MAP standard folder "

    #@37
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v1

    #@43
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 203
    const/4 v0, 0x1

    #@47
    invoke-virtual {p0, v0, p1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageListInfoNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@4a
    goto :goto_15

    #@4b
    .line 208
    :cond_4b
    :try_start_4b
    invoke-virtual {v9, v7}, Lcom/broadcom/bt/service/map/MapClientSession;->getDatasourcePath(Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    .line 209
    .local v2, dsPath:Ljava/lang/String;
    iget-object v0, v9, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@51
    iget-object v0, v0, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@53
    const/4 v6, 0x1

    #@54
    move-object v1, p1

    #@55
    move-object v3, p3

    #@56
    move-wide v4, p4

    #@57
    invoke-interface/range {v0 .. v6}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onGetMessageListing(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;JZ)V
    :try_end_5a
    .catch Ljava/lang/Throwable; {:try_start_4b .. :try_end_5a} :catch_5b

    #@5a
    goto :goto_15

    #@5b
    .line 210
    .end local v2           #dsPath:Ljava/lang/String;
    :catch_5b
    move-exception v10

    #@5c
    .line 211
    .local v10, t:Ljava/lang/Throwable;
    const-string v0, "BtMap.MapSessionManager"

    #@5e
    const-string v1, "getMessageListInfo: error calling datasource "

    #@60
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 212
    const/4 v0, 0x1

    #@64
    invoke-virtual {p0, v0, p1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageListInfoNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@67
    goto :goto_15
.end method

.method private static getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;
    .registers 8
    .parameter "sessionId"
    .parameter "methodName"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 74
    invoke-static {}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getInstance()Lcom/broadcom/bt/service/map/MapDataSourceManager;

    #@4
    move-result-object v0

    #@5
    .line 75
    .local v0, dsMgr:Lcom/broadcom/bt/service/map/MapDataSourceManager;
    if-nez v0, :cond_21

    #@7
    .line 76
    const-string v3, "BtMap.MapSessionManager"

    #@9
    new-instance v4, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    const-string v5, "(): DataSourceManager not available"

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    move-object v1, v2

    #@20
    .line 85
    :cond_20
    :goto_20
    return-object v1

    #@21
    .line 79
    :cond_21
    invoke-virtual {v0, p0}, Lcom/broadcom/bt/service/map/MapDataSourceManager;->getSession(I)Lcom/broadcom/bt/service/map/MapClientSession;

    #@24
    move-result-object v1

    #@25
    .line 80
    .local v1, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v1, :cond_20

    #@27
    .line 81
    const-string v3, "BtMap.MapSessionManager"

    #@29
    new-instance v4, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    const-string v5, "() session not found with sessionId "

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v4

    #@40
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    move-object v1, v2

    #@44
    .line 83
    goto :goto_20
.end method

.method static pushMessage(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZI)V
    .registers 22
    .parameter "svc"
    .parameter "requestId"
    .parameter "bmsgFilePath"
    .parameter "saveInSent"
    .parameter "retry"
    .parameter "charset"

    #@0
    .prologue
    .line 431
    const-string v2, "BtMap.MapSessionManager"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "pushMessage: sessionId="

    #@9
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    move-object/from16 v0, p1

    #@f
    iget v7, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@11
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    const-string v7, ", eventId="

    #@17
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    move-object/from16 v0, p1

    #@1d
    iget v7, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1f
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v7, ", saveInSent="

    #@25
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    move/from16 v0, p3

    #@2b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    const-string v7, ", retry="

    #@31
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    move/from16 v0, p4

    #@37
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    const-string v7, ", charSet= "

    #@3d
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    move/from16 v0, p5

    #@43
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v3

    #@4b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    .line 437
    move-object/from16 v0, p2

    #@50
    move-object/from16 v1, p1

    #@52
    iput-object v0, v1, Lcom/broadcom/bt/map/RequestId;->mRequestData:Ljava/lang/String;

    #@54
    .line 439
    move-object/from16 v0, p1

    #@56
    iget v2, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@58
    const-string v3, "pushMessage"

    #@5a
    invoke-static {v2, v3}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@5d
    move-result-object v13

    #@5e
    .line 441
    .local v13, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v13, :cond_69

    #@60
    .line 442
    const/4 v2, 0x1

    #@61
    move-object/from16 v0, p0

    #@63
    move-object/from16 v1, p1

    #@65
    invoke-virtual {v0, v2, v1}, Lcom/broadcom/bt/service/map/MapService;->setPushMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@68
    .line 515
    :goto_68
    return-void

    #@69
    .line 446
    :cond_69
    invoke-static/range {p2 .. p2}, Lcom/broadcom/bt/service/map/MapSessionManager;->getDestFolderFromBMessage(Ljava/lang/String;)Ljava/lang/String;

    #@6c
    move-result-object v10

    #@6d
    .line 451
    .local v10, destFolderPath:Ljava/lang/String;
    iget-object v6, v13, Lcom/broadcom/bt/service/map/MapClientSession;->mCurrentPath:Ljava/lang/String;

    #@6f
    .line 452
    .local v6, fullPath:Ljava/lang/String;
    if-eqz v10, :cond_77

    #@71
    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    #@74
    move-result v2

    #@75
    if-eqz v2, :cond_b5

    #@77
    .line 454
    :cond_77
    const-string v2, "BtMap.MapSessionManager"

    #@79
    const-string v3, "pushMessage: BMessage folder path is empty..Setting push path to current.."

    #@7b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    .line 456
    iget-object v6, v13, Lcom/broadcom/bt/service/map/MapClientSession;->mCurrentPath:Ljava/lang/String;

    #@80
    .line 487
    :goto_80
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@83
    move-result-object v12

    #@84
    .line 488
    .local v12, fullPathLC:Ljava/lang/String;
    const-string v2, "root"

    #@86
    invoke-virtual {v2, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@89
    move-result v2

    #@8a
    if-nez v2, :cond_94

    #@8c
    const-string v2, "root/telecom"

    #@8e
    invoke-virtual {v2, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@91
    move-result v2

    #@92
    if-eqz v2, :cond_10e

    #@94
    .line 491
    :cond_94
    const-string v2, "BtMap.MapSessionManager"

    #@96
    new-instance v3, Ljava/lang/StringBuilder;

    #@98
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9b
    const-string v7, "pushMessage(): No message push to MAP standard folder "

    #@9d
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a0
    move-result-object v3

    #@a1
    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v3

    #@a9
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 493
    const/4 v2, 0x1

    #@ad
    move-object/from16 v0, p0

    #@af
    move-object/from16 v1, p1

    #@b1
    invoke-virtual {v0, v2, v1}, Lcom/broadcom/bt/service/map/MapService;->setPushMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@b4
    goto :goto_68

    #@b5
    .line 462
    .end local v12           #fullPathLC:Ljava/lang/String;
    :cond_b5
    const-string v2, "/"

    #@b7
    invoke-virtual {v10, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@ba
    move-result v2

    #@bb
    if-eqz v2, :cond_c9

    #@bd
    .line 464
    const-string v2, "BtMap.MapSessionManager"

    #@bf
    const-string v3, "pushMessage: BMessage folder path starts with /. Removing it..."

    #@c1
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c4
    .line 466
    const/4 v2, 0x1

    #@c5
    invoke-virtual {v10, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@c8
    move-result-object v10

    #@c9
    .line 469
    :cond_c9
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@cc
    move-result-object v11

    #@cd
    .line 470
    .local v11, destFolderPathLC:Ljava/lang/String;
    const-string v2, "root"

    #@cf
    invoke-virtual {v11, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d2
    move-result v2

    #@d3
    if-eqz v2, :cond_de

    #@d5
    .line 472
    const-string v2, "BtMap.MapSessionManager"

    #@d7
    const-string v3, "pushMessage: BMessage folder path starts with root. Using it as push path.."

    #@d9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    .line 474
    move-object v6, v10

    #@dd
    goto :goto_80

    #@de
    .line 475
    :cond_de
    const-string v2, "telecom"

    #@e0
    invoke-virtual {v11, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@e3
    move-result v2

    #@e4
    if-eqz v2, :cond_101

    #@e6
    .line 477
    const-string v2, "BtMap.MapSessionManager"

    #@e8
    const-string v3, "pushMessage: BMessage folder path starts with telecom. Appending root/ and using it as push path.."

    #@ea
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ed
    .line 479
    new-instance v2, Ljava/lang/StringBuilder;

    #@ef
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f2
    const-string v3, "root/"

    #@f4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v2

    #@f8
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v2

    #@fc
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ff
    move-result-object v6

    #@100
    goto :goto_80

    #@101
    .line 482
    :cond_101
    const-string v2, "BtMap.MapSessionManager"

    #@103
    const-string v3, "pushMessage: BMessage folder is child path..Appending it to current.."

    #@105
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@108
    .line 484
    invoke-virtual {v13, v10}, Lcom/broadcom/bt/service/map/MapClientSession;->getAppendedPath(Ljava/lang/String;)Ljava/lang/String;

    #@10b
    move-result-object v6

    #@10c
    goto/16 :goto_80

    #@10e
    .line 496
    .end local v11           #destFolderPathLC:Ljava/lang/String;
    .restart local v12       #fullPathLC:Ljava/lang/String;
    :cond_10e
    invoke-virtual {v13, v6}, Lcom/broadcom/bt/service/map/MapClientSession;->getDatasourcePath(Ljava/lang/String;)Ljava/lang/String;

    #@111
    move-result-object v5

    #@112
    .line 498
    .local v5, datasourceFolderPath:Ljava/lang/String;
    :try_start_112
    const-string v2, "BtMap.MapSessionManager"

    #@114
    new-instance v3, Ljava/lang/StringBuilder;

    #@116
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@119
    const-string v7, "pushMessage(): datasource path = "

    #@11b
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v3

    #@11f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v3

    #@123
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@126
    move-result-object v3

    #@127
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12a
    .line 500
    const/4 v4, 0x0

    #@12b
    .line 501
    .local v4, contentUri:Ljava/lang/String;
    invoke-static {}, Lcom/broadcom/bt/service/map/MapService;->getDefaultTmpPath()Ljava/lang/String;

    #@12e
    move-result-object v15

    #@12f
    .line 502
    .local v15, tmpDirPath:Ljava/lang/String;
    if-eqz p2, :cond_179

    #@131
    move-object/from16 v0, p2

    #@133
    invoke-virtual {v0, v15}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@136
    move-result v2

    #@137
    if-eqz v2, :cond_179

    #@139
    .line 503
    new-instance v2, Ljava/lang/StringBuilder;

    #@13b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13e
    const-string v3, "content://com.broadcom.bt.map//"

    #@140
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v2

    #@144
    invoke-virtual {v15}, Ljava/lang/String;->length()I

    #@147
    move-result v3

    #@148
    move-object/from16 v0, p2

    #@14a
    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@14d
    move-result-object v3

    #@14e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v2

    #@152
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@155
    move-result-object v4

    #@156
    .line 508
    :goto_156
    iget-object v2, v13, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@158
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@15a
    move-object/from16 v3, p1

    #@15c
    move/from16 v7, p3

    #@15e
    move/from16 v8, p4

    #@160
    move/from16 v9, p5

    #@162
    invoke-interface/range {v2 .. v9}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onPushMessage(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZI)V
    :try_end_165
    .catch Ljava/lang/Throwable; {:try_start_112 .. :try_end_165} :catch_167

    #@165
    goto/16 :goto_68

    #@167
    .line 510
    .end local v4           #contentUri:Ljava/lang/String;
    .end local v15           #tmpDirPath:Ljava/lang/String;
    :catch_167
    move-exception v14

    #@168
    .line 511
    .local v14, t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapSessionManager"

    #@16a
    const-string v3, "pushMessage(): error calling datasource pushMessage"

    #@16c
    invoke-static {v2, v3, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16f
    .line 512
    const/4 v2, 0x1

    #@170
    move-object/from16 v0, p0

    #@172
    move-object/from16 v1, p1

    #@174
    invoke-virtual {v0, v2, v1}, Lcom/broadcom/bt/service/map/MapService;->setPushMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@177
    goto/16 :goto_68

    #@179
    .line 506
    .end local v14           #t:Ljava/lang/Throwable;
    .restart local v4       #contentUri:Ljava/lang/String;
    .restart local v15       #tmpDirPath:Ljava/lang/String;
    :cond_179
    move-object/from16 v4, p2

    #@17b
    goto :goto_156
.end method

.method static returnMessage(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    .registers 5
    .parameter "svc"
    .parameter "requestId"
    .parameter "dsMessageHandle"
    .parameter "filepath"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 392
    if-nez p3, :cond_7

    #@3
    .line 393
    invoke-virtual {p0, v0, p1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@6
    .line 397
    :goto_6
    return-void

    #@7
    .line 395
    :cond_7
    invoke-virtual {p0, v0, p1, v0, p3}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageResult(ZLcom/broadcom/bt/map/RequestId;ZLjava/lang/String;)V

    #@a
    goto :goto_6
.end method

.method static setCurrentPath(ILjava/lang/String;)V
    .registers 4
    .parameter "sessionId"
    .parameter "path"

    #@0
    .prologue
    .line 89
    const-string v1, "setFolder"

    #@2
    invoke-static {p0, v1}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@5
    move-result-object v0

    #@6
    .line 90
    .local v0, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v0, :cond_9

    #@8
    .line 94
    :goto_8
    return-void

    #@9
    .line 93
    :cond_9
    invoke-virtual {v0, p1}, Lcom/broadcom/bt/service/map/MapClientSession;->setCurrentPath(Ljava/lang/String;)V

    #@c
    goto :goto_8
.end method

.method static setFolderListing(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;)V
    .registers 16
    .parameter "svc"
    .parameter "requestId"
    .parameter "folderPath"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/broadcom/bt/service/map/MapService;",
            "Lcom/broadcom/bt/map/RequestId;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/FolderInfo;",
            ">;)V"
        }
    .end annotation

    #@0
    .prologue
    .local p3, folderEntries:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/FolderInfo;>;"
    const/4 v4, -0x1

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v3, 0x0

    #@4
    .line 161
    iget v9, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@6
    .line 163
    .local v9, eventId:I
    iget v0, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@8
    const-string v2, "setFolderListing"

    #@a
    invoke-static {v0, v2}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@d
    move-result-object v11

    #@e
    .line 165
    .local v11, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v11, :cond_19

    #@10
    move-object v0, p0

    #@11
    move-object v2, p1

    #@12
    move v5, v3

    #@13
    move-object v7, v6

    #@14
    move v8, v3

    #@15
    .line 166
    invoke-virtual/range {v0 .. v8}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIILjava/lang/String;Ljava/lang/String;Z)V

    #@18
    .line 183
    :goto_18
    return-void

    #@19
    .line 171
    :cond_19
    invoke-virtual {v11, v9, p2, p3}, Lcom/broadcom/bt/service/map/MapClientSession;->loadFolderEntries(ILjava/lang/String;Ljava/util/List;)V

    #@1c
    .line 174
    const-wide/16 v7, 0x0

    #@1e
    invoke-virtual {v11, v9, p2, v7, v8}, Lcom/broadcom/bt/service/map/MapClientSession;->getFolderEntry(ILjava/lang/String;J)Lcom/broadcom/bt/map/FolderInfo;

    #@21
    move-result-object v10

    #@22
    .line 175
    .local v10, fInfo:Lcom/broadcom/bt/map/FolderInfo;
    if-eqz v10, :cond_37

    #@24
    .line 176
    iget-wide v2, v10, Lcom/broadcom/bt/map/FolderInfo;->mFolderSizeBytes:J

    #@26
    long-to-int v5, v2

    #@27
    iget-object v6, v10, Lcom/broadcom/bt/map/FolderInfo;->mFolderName:Ljava/lang/String;

    #@29
    iget-object v7, v10, Lcom/broadcom/bt/map/FolderInfo;->mCreatedDateTimeMS:Ljava/lang/String;

    #@2b
    invoke-virtual {v10}, Lcom/broadcom/bt/map/FolderInfo;->isReadOnly()Z

    #@2e
    move-result v8

    #@2f
    move-object v0, p0

    #@30
    move-object v2, p1

    #@31
    move v3, v1

    #@32
    move v4, v1

    #@33
    invoke-virtual/range {v0 .. v8}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIILjava/lang/String;Ljava/lang/String;Z)V

    #@36
    goto :goto_18

    #@37
    :cond_37
    move-object v0, p0

    #@38
    move-object v2, p1

    #@39
    move v5, v3

    #@3a
    move-object v7, v6

    #@3b
    move v8, v3

    #@3c
    .line 180
    invoke-virtual/range {v0 .. v8}, Lcom/broadcom/bt/service/map/MapService;->setGetFolderEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIILjava/lang/String;Ljava/lang/String;Z)V

    #@3f
    goto :goto_18
.end method

.method static setMessageDeletedResult(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ZZLjava/lang/String;)V
    .registers 16
    .parameter "svc"
    .parameter "requestId"
    .parameter "messageId"
    .parameter "deleteRequested"
    .parameter "success"
    .parameter "newFolderPath"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v8, 0x1

    #@2
    .line 597
    const-string v5, "BtMap.MapSessionManager"

    #@4
    new-instance v6, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v7, "setMessageDeletedResult():  sessionId= "

    #@b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    iget v7, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    const-string v7, ", eventId="

    #@17
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v6

    #@1b
    iget v7, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v6

    #@21
    const-string v7, ", messageId="

    #@23
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v6

    #@2b
    const-string v7, ", deleteRequested="

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@34
    move-result-object v6

    #@35
    const-string v7, ", success="

    #@37
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, ", newFolderPath="

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-static {p5}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v7

    #@49
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v6

    #@51
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 605
    iget v5, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@56
    const-string v6, "setMessageDeletedResult"

    #@58
    invoke-static {v5, v6}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@5b
    move-result-object v4

    #@5c
    .line 607
    .local v4, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v4, :cond_62

    #@5e
    .line 608
    invoke-virtual {p0, v8, p1, v9}, Lcom/broadcom/bt/service/map/MapService;->setMessageDeletedResult(ZLcom/broadcom/bt/map/RequestId;Z)V

    #@61
    .line 649
    :goto_61
    return-void

    #@62
    .line 611
    :cond_62
    if-nez p4, :cond_6f

    #@64
    .line 612
    const-string v5, "BtMap.MapSessionManager"

    #@66
    const-string v6, "setMessageDeleteResult(): did not complete successfully"

    #@68
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 614
    invoke-virtual {p0, v8, p1, v9}, Lcom/broadcom/bt/service/map/MapService;->setMessageDeletedResult(ZLcom/broadcom/bt/map/RequestId;Z)V

    #@6e
    goto :goto_61

    #@6f
    .line 618
    :cond_6f
    invoke-virtual {v4, p2}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageHandleFromId(Ljava/lang/String;)J

    #@72
    move-result-wide v0

    #@73
    .line 619
    .local v0, messageHandle:J
    const-wide/16 v5, -0x1

    #@75
    cmp-long v5, v0, v5

    #@77
    if-nez v5, :cond_84

    #@79
    .line 620
    const-string v5, "BtMap.MapSessionManager"

    #@7b
    const-string v6, "setMessageDeleteResult(): messageId not found."

    #@7d
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 621
    invoke-virtual {p0, v8, p1, v9}, Lcom/broadcom/bt/service/map/MapService;->setMessageDeletedResult(ZLcom/broadcom/bt/map/RequestId;Z)V

    #@83
    goto :goto_61

    #@84
    .line 624
    :cond_84
    if-eqz p3, :cond_a1

    #@86
    .line 630
    const/4 v3, 0x0

    #@87
    .line 631
    .local v3, originalVirtualPath:Ljava/lang/String;
    invoke-virtual {v4, p2}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageFolderPath(Ljava/lang/String;)Ljava/lang/String;

    #@8a
    move-result-object v2

    #@8b
    .line 632
    .local v2, originalFolderPath:Ljava/lang/String;
    if-eqz v2, :cond_91

    #@8d
    .line 633
    invoke-virtual {v4, v2, v8}, Lcom/broadcom/bt/service/map/MapClientSession;->getVirtualPath(Ljava/lang/String;Z)Ljava/lang/String;

    #@90
    move-result-object v3

    #@91
    .line 637
    :cond_91
    invoke-static {v3}, Lcom/broadcom/bt/service/map/MapClientSession;->isVirtualDeletedFolder(Ljava/lang/String;)Z

    #@94
    move-result v5

    #@95
    if-eqz v5, :cond_a5

    #@97
    .line 638
    const-string v5, "BtMap.MapSessionManager"

    #@99
    const-string v6, "Deleted message was in deleted folder. Message is now permanently deleted..."

    #@9b
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    .line 640
    invoke-virtual {v4, p2}, Lcom/broadcom/bt/service/map/MapClientSession;->removeMessageIdMap(Ljava/lang/String;)V

    #@a1
    .line 648
    .end local v2           #originalFolderPath:Ljava/lang/String;
    .end local v3           #originalVirtualPath:Ljava/lang/String;
    :cond_a1
    :goto_a1
    invoke-virtual {p0, v8, p1, v8}, Lcom/broadcom/bt/service/map/MapService;->setMessageDeletedResult(ZLcom/broadcom/bt/map/RequestId;Z)V

    #@a4
    goto :goto_61

    #@a5
    .line 642
    .restart local v2       #originalFolderPath:Ljava/lang/String;
    .restart local v3       #originalVirtualPath:Ljava/lang/String;
    :cond_a5
    const-string v5, "BtMap.MapSessionManager"

    #@a7
    const-string v6, "Deleted message was not in deleted folder. Updating it\'s location..."

    #@a9
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 644
    invoke-virtual {v4, v0, v1, p2, p5}, Lcom/broadcom/bt/service/map/MapClientSession;->updateMessageHandleToMap(JLjava/lang/String;Ljava/lang/String;)V

    #@af
    goto :goto_a1
.end method

.method static setMessageListInfo(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;ZZZ)V
    .registers 38
    .parameter "svc"
    .parameter "requestId"
    .parameter "folderPath"
    .parameter
    .parameter "dateTime"
    .parameter "hasNewMessages"
    .parameter "sendListResponse"
    .parameter "sendEntryResponse"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/broadcom/bt/service/map/MapService;",
            "Lcom/broadcom/bt/map/RequestId;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/map/MessageInfo;",
            ">;",
            "Ljava/lang/String;",
            "ZZZ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 221
    .local p3, messageEntries:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/map/MessageInfo;>;"
    const-string v3, "BtMap.MapSessionManager"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "setMessageListInfo: sessionId="

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    move-object/from16 v0, p1

    #@f
    iget v5, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@11
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    const-string v5, ", eventId="

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    move-object/from16 v0, p1

    #@1d
    iget v5, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    const-string v5, ", folderPath="

    #@25
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    invoke-static/range {p2 .. p2}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    const-string v5, ", messageCount= "

    #@33
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v4

    #@37
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    #@3a
    move-result v5

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    const-string v5, ", dateTime"

    #@41
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v4

    #@45
    invoke-static/range {p4 .. p4}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    const-string v5, ", hasNewMessages="

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    move/from16 v0, p5

    #@55
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 228
    move-object/from16 v0, p1

    #@62
    iget v3, v0, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@64
    const-string v4, "setMessageListInfo"

    #@66
    invoke-static {v3, v4}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@69
    move-result-object v29

    #@6a
    .line 230
    .local v29, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-eqz v29, :cond_74

    #@6c
    if-eqz p3, :cond_74

    #@6e
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    #@71
    move-result v3

    #@72
    if-gtz v3, :cond_89

    #@74
    .line 232
    :cond_74
    if-eqz p6, :cond_7e

    #@76
    .line 233
    const/4 v3, 0x1

    #@77
    move-object/from16 v0, p0

    #@79
    move-object/from16 v1, p1

    #@7b
    invoke-virtual {v0, v3, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageListInfoNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@7e
    .line 235
    :cond_7e
    if-eqz p7, :cond_88

    #@80
    .line 236
    const/4 v3, 0x1

    #@81
    move-object/from16 v0, p0

    #@83
    move-object/from16 v1, p1

    #@85
    invoke-virtual {v0, v3, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@88
    .line 274
    :cond_88
    :goto_88
    return-void

    #@89
    .line 241
    :cond_89
    move-object/from16 v0, p1

    #@8b
    iget v3, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@8d
    move-object/from16 v0, v29

    #@8f
    move-object/from16 v1, p2

    #@91
    move-object/from16 v2, p3

    #@93
    invoke-virtual {v0, v3, v1, v2}, Lcom/broadcom/bt/service/map/MapClientSession;->loadMessageEntries(ILjava/lang/String;Ljava/util/List;)V

    #@96
    .line 244
    if-eqz p6, :cond_a9

    #@98
    .line 245
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    #@9b
    move-result v6

    #@9c
    move-object/from16 v3, p0

    #@9e
    move-object/from16 v4, p1

    #@a0
    move-object/from16 v5, p2

    #@a2
    move-object/from16 v7, p4

    #@a4
    move/from16 v8, p5

    #@a6
    invoke-static/range {v3 .. v8}, Lcom/broadcom/bt/service/map/MapSessionManager;->setMessageListInfoCount(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ILjava/lang/String;Z)V

    #@a9
    .line 248
    :cond_a9
    if-eqz p7, :cond_88

    #@ab
    .line 249
    move-object/from16 v0, p1

    #@ad
    iget v3, v0, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@af
    const-wide/16 v4, 0x0

    #@b1
    move-object/from16 v0, v29

    #@b3
    move-object/from16 v1, p2

    #@b5
    invoke-virtual {v0, v3, v1, v4, v5}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageInfo(ILjava/lang/String;J)Lcom/broadcom/bt/map/MessageInfo;

    #@b8
    move-result-object v28

    #@b9
    .line 251
    .local v28, mInfo:Lcom/broadcom/bt/map/MessageInfo;
    if-eqz v28, :cond_14a

    #@bb
    .line 253
    move-object/from16 v0, v28

    #@bd
    iget-object v3, v0, Lcom/broadcom/bt/map/MessageInfo;->mMsgHandle:Ljava/lang/String;

    #@bf
    move-object/from16 v0, v29

    #@c1
    invoke-virtual {v0, v3}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageHandleFromId(Ljava/lang/String;)J

    #@c4
    move-result-wide v8

    #@c5
    .line 255
    .local v8, messageHandle:J
    const-wide/16 v3, 0x0

    #@c7
    cmp-long v3, v8, v3

    #@c9
    if-gez v3, :cond_da

    #@cb
    .line 256
    const-string v3, "BtMap.MapSessionManager"

    #@cd
    const-string v4, "setMessageListInfo(): invalid message handle"

    #@cf
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    .line 257
    const/4 v3, 0x1

    #@d3
    move-object/from16 v0, p0

    #@d5
    move-object/from16 v1, p1

    #@d7
    invoke-virtual {v0, v3, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@da
    .line 259
    :cond_da
    const/4 v4, 0x1

    #@db
    const/4 v6, 0x1

    #@dc
    const/4 v7, 0x1

    #@dd
    move-object/from16 v0, v28

    #@df
    iget-byte v10, v0, Lcom/broadcom/bt/map/MessageInfo;->mMsgType:B

    #@e1
    move-object/from16 v0, v28

    #@e3
    iget v11, v0, Lcom/broadcom/bt/map/MessageInfo;->mMsgSize:I

    #@e5
    move-object/from16 v0, v28

    #@e7
    iget v12, v0, Lcom/broadcom/bt/map/MessageInfo;->mAttachmentSize:I

    #@e9
    move-object/from16 v0, v28

    #@eb
    iget-wide v13, v0, Lcom/broadcom/bt/map/MessageInfo;->mParameterMask:J

    #@ed
    const/4 v3, 0x1

    #@ee
    move-object/from16 v0, v28

    #@f0
    invoke-virtual {v0, v3}, Lcom/broadcom/bt/map/MessageInfo;->getSenderDisplayName(Z)Ljava/lang/String;

    #@f3
    move-result-object v15

    #@f4
    const/4 v3, 0x1

    #@f5
    move-object/from16 v0, v28

    #@f7
    invoke-virtual {v0, v3}, Lcom/broadcom/bt/map/MessageInfo;->getSenderAddress(Z)Ljava/lang/String;

    #@fa
    move-result-object v16

    #@fb
    const/4 v3, 0x1

    #@fc
    move-object/from16 v0, v28

    #@fe
    invoke-virtual {v0, v3}, Lcom/broadcom/bt/map/MessageInfo;->getRecipientDisplayName(Z)Ljava/lang/String;

    #@101
    move-result-object v17

    #@102
    const/4 v3, 0x1

    #@103
    move-object/from16 v0, v28

    #@105
    invoke-virtual {v0, v3}, Lcom/broadcom/bt/map/MessageInfo;->getRecipientAddress(Z)Ljava/lang/String;

    #@108
    move-result-object v18

    #@109
    const/4 v3, 0x1

    #@10a
    move-object/from16 v0, v28

    #@10c
    invoke-virtual {v0, v3}, Lcom/broadcom/bt/map/MessageInfo;->getReplyToAddress(Z)Ljava/lang/String;

    #@10f
    move-result-object v19

    #@110
    const/4 v3, 0x1

    #@111
    move-object/from16 v0, v28

    #@113
    invoke-virtual {v0, v3}, Lcom/broadcom/bt/map/MessageInfo;->getSubject(Z)Ljava/lang/String;

    #@116
    move-result-object v20

    #@117
    move-object/from16 v0, v28

    #@119
    iget-object v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mDateTime:Ljava/lang/String;

    #@11b
    move-object/from16 v21, v0

    #@11d
    move-object/from16 v0, v28

    #@11f
    iget-boolean v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mIsText:Z

    #@121
    move/from16 v22, v0

    #@123
    move-object/from16 v0, v28

    #@125
    iget-boolean v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mIsHighPriority:Z

    #@127
    move/from16 v23, v0

    #@129
    move-object/from16 v0, v28

    #@12b
    iget-boolean v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mIsRead:Z

    #@12d
    move/from16 v24, v0

    #@12f
    move-object/from16 v0, v28

    #@131
    iget-boolean v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mIsSent:Z

    #@133
    move/from16 v25, v0

    #@135
    move-object/from16 v0, v28

    #@137
    iget-boolean v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mIsProtected:Z

    #@139
    move/from16 v26, v0

    #@13b
    move-object/from16 v0, v28

    #@13d
    iget-byte v0, v0, Lcom/broadcom/bt/map/MessageInfo;->mReceptionStatus:B

    #@13f
    move/from16 v27, v0

    #@141
    move-object/from16 v3, p0

    #@143
    move-object/from16 v5, p1

    #@145
    invoke-virtual/range {v3 .. v27}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryResult(ZLcom/broadcom/bt/map/RequestId;ZIJBIIJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZZB)V

    #@148
    goto/16 :goto_88

    #@14a
    .line 270
    .end local v8           #messageHandle:J
    :cond_14a
    const/4 v3, 0x1

    #@14b
    move-object/from16 v0, p0

    #@14d
    move-object/from16 v1, p1

    #@14f
    invoke-virtual {v0, v3, v1}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageEntryNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@152
    goto/16 :goto_88
.end method

.method static setMessageListInfoCount(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;ILjava/lang/String;Z)V
    .registers 12
    .parameter "svc"
    .parameter "requestId"
    .parameter "folderPath"
    .parameter "messageCount"
    .parameter "dateTime"
    .parameter "hasNewMessages"

    #@0
    .prologue
    .line 280
    const-string v0, "BtMap.MapSessionManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "setMessageListInfoCount: sessionId="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, ", eventId="

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget v2, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, ", folderPath="

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-static {p2}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    const-string v2, ", messageCount= "

    #@2f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    const-string v2, ", dateTime"

    #@39
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    invoke-static {p4}, Lcom/broadcom/bt/util/StringUtil;->toNonNullString(Ljava/lang/String;)Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v1

    #@45
    const-string v2, ", hasNewMessages="

    #@47
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v1

    #@4b
    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 287
    const/4 v1, 0x1

    #@57
    move-object v0, p0

    #@58
    move-object v2, p1

    #@59
    move v3, p3

    #@5a
    move-object v4, p4

    #@5b
    move v5, p5

    #@5c
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/map/MapService;->setGetMessageListInfoResult(ZLcom/broadcom/bt/map/RequestId;ILjava/lang/String;Z)V

    #@5f
    .line 289
    return-void
.end method

.method static setMessageStatus(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;JBZ)V
    .registers 12
    .parameter "svc"
    .parameter "requestId"
    .parameter "messageId"
    .parameter "messageStatusType"
    .parameter "isStatusSet"

    #@0
    .prologue
    .line 563
    const-string v3, "BtMap.MapSessionManager"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "setMessageStatus(): sessionId="

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    iget v5, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    const-string v5, ", eventId="

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    iget v5, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    const-string v5, ", messageId="

    #@21
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@28
    move-result-object v4

    #@29
    const-string v5, ", messageStatusType="

    #@2b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v4

    #@2f
    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    const-string v5, ",isStatusSet="

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 568
    iget v3, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@46
    const-string v4, "setMessageStatus"

    #@48
    invoke-static {v3, v4}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@4b
    move-result-object v1

    #@4c
    .line 570
    .local v1, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v1, :cond_4f

    #@4e
    .line 591
    :goto_4e
    return-void

    #@4f
    .line 575
    :cond_4f
    invoke-virtual {v1, p2, p3}, Lcom/broadcom/bt/service/map/MapClientSession;->getMessageIdFromHandle(J)Ljava/lang/String;

    #@52
    move-result-object v0

    #@53
    .line 576
    .local v0, messageHandle:Ljava/lang/String;
    if-eqz v0, :cond_5b

    #@55
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@58
    move-result v3

    #@59
    if-nez v3, :cond_74

    #@5b
    .line 577
    :cond_5b
    const-string v3, "BtMap.MapSessionManager"

    #@5d
    new-instance v4, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v5, "setMessageStatus(): message handle not found for messageId "

    #@64
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v4

    #@68
    invoke-virtual {v4, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v4

    #@6c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v4

    #@70
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    goto :goto_4e

    #@74
    .line 583
    :cond_74
    :try_start_74
    iget-object v3, v1, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@76
    iget-object v3, v3, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@78
    invoke-interface {v3, p1, v0, p4, p5}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onSetMessageStatus(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;IZ)V
    :try_end_7b
    .catch Ljava/lang/Throwable; {:try_start_74 .. :try_end_7b} :catch_7c

    #@7b
    goto :goto_4e

    #@7c
    .line 585
    :catch_7c
    move-exception v2

    #@7d
    .line 586
    .local v2, t:Ljava/lang/Throwable;
    const-string v3, "BtMap.MapSessionManager"

    #@7f
    const-string v4, "setMessageStatus(): error calling datasource setMessageStatus"

    #@81
    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@84
    goto :goto_4e
.end method

.method static setPushMessageResult(Lcom/broadcom/bt/service/map/MapService;Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "svc"
    .parameter "requestId"
    .parameter "dsFolderPath"
    .parameter "dsMessageHandle"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 520
    const-string v0, "BtMap.MapSessionManager"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "setPushMessageResult: sessionId="

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    iget v3, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    const-string v3, ", eventId="

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    iget v3, p1, Lcom/broadcom/bt/map/RequestId;->mEventId:I

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const-string v3, ", folderPath="

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    const-string v3, ", dsMessageHandle="

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 525
    iget v0, p1, Lcom/broadcom/bt/map/RequestId;->mSessionId:I

    #@3d
    const-string v2, "setPushMessageResult"

    #@3f
    invoke-static {v0, v2}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@42
    move-result-object v6

    #@43
    .line 527
    .local v6, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v6, :cond_49

    #@45
    .line 528
    invoke-virtual {p0, v1, p1}, Lcom/broadcom/bt/service/map/MapService;->setPushMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@48
    .line 542
    :goto_48
    return-void

    #@49
    .line 532
    :cond_49
    if-nez p3, :cond_56

    #@4b
    .line 533
    const-string v0, "BtMap.MapSessionManager"

    #@4d
    const-string v2, "setPushMessageResult(): datsource handle not returned.."

    #@4f
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 535
    invoke-virtual {p0, v1, p1}, Lcom/broadcom/bt/service/map/MapService;->setPushMessageNoResult(ZLcom/broadcom/bt/map/RequestId;)V

    #@55
    goto :goto_48

    #@56
    .line 538
    :cond_56
    invoke-virtual {v6, p3, p2}, Lcom/broadcom/bt/service/map/MapClientSession;->addNewMessageHandleToMap(Ljava/lang/String;Ljava/lang/String;)J

    #@59
    move-result-wide v4

    #@5a
    .local v4, messageId:J
    move-object v0, p0

    #@5b
    move-object v2, p1

    #@5c
    move v3, v1

    #@5d
    .line 540
    invoke-virtual/range {v0 .. v5}, Lcom/broadcom/bt/service/map/MapService;->setPushMessageResult(ZLcom/broadcom/bt/map/RequestId;ZJ)V

    #@60
    goto :goto_48
.end method

.method static updateInbox(Lcom/broadcom/bt/service/map/MapService;I)V
    .registers 7
    .parameter "svc"
    .parameter "sessionId"

    #@0
    .prologue
    .line 546
    const-string v2, "BtMap.MapSessionManager"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "updateInbox: sessionId="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 548
    const-string v2, "updateInbox"

    #@1a
    invoke-static {p1, v2}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@1d
    move-result-object v0

    #@1e
    .line 549
    .local v0, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v0, :cond_21

    #@20
    .line 558
    :goto_20
    return-void

    #@21
    .line 553
    :cond_21
    :try_start_21
    iget-object v2, v0, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@23
    iget-object v2, v2, Lcom/broadcom/bt/service/map/MapDatasourceContext;->mCallback:Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;

    #@25
    invoke-interface {v2}, Lcom/broadcom/bt/map/IBluetoothMapDatasourceCallback;->onUpdateInbox()V
    :try_end_28
    .catch Ljava/lang/Throwable; {:try_start_21 .. :try_end_28} :catch_29

    #@28
    goto :goto_20

    #@29
    .line 554
    :catch_29
    move-exception v1

    #@2a
    .line 555
    .local v1, t:Ljava/lang/Throwable;
    const-string v2, "BtMap.MapSessionManager"

    #@2c
    const-string v3, "updateInbox(): error calling datasource updateInbox"

    #@2e
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@31
    goto :goto_20
.end method

.method static updateNotificationState(Lcom/broadcom/bt/service/map/MapService;ILjava/lang/String;Z)V
    .registers 9
    .parameter "svc"
    .parameter "sessionId"
    .parameter "address"
    .parameter "enabled"

    #@0
    .prologue
    .line 654
    const-string v2, "BtMap.MapSessionManager"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "updateNotificationRegistration():sessionId="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v4, ", address="

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    const-string v4, ",enabled="

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 658
    const-string v2, "updateNotificationRegistration"

    #@2e
    invoke-static {p1, v2}, Lcom/broadcom/bt/service/map/MapSessionManager;->getSession(ILjava/lang/String;)Lcom/broadcom/bt/service/map/MapClientSession;

    #@31
    move-result-object v1

    #@32
    .line 660
    .local v1, session:Lcom/broadcom/bt/service/map/MapClientSession;
    if-nez v1, :cond_35

    #@34
    .line 665
    :goto_34
    return-void

    #@35
    .line 663
    :cond_35
    iget-object v0, v1, Lcom/broadcom/bt/service/map/MapClientSession;->mCtx:Lcom/broadcom/bt/service/map/MapDatasourceContext;

    #@37
    .line 664
    .local v0, ctx:Lcom/broadcom/bt/service/map/MapDatasourceContext;
    invoke-virtual {v0, p1, p3}, Lcom/broadcom/bt/service/map/MapDatasourceContext;->updateNotificationState(IZ)V

    #@3a
    goto :goto_34
.end method
