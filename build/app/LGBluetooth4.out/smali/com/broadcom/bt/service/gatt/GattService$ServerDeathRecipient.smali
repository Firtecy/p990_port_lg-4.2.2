.class Lcom/broadcom/bt/service/gatt/GattService$ServerDeathRecipient;
.super Ljava/lang/Object;
.source "GattService.java"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/gatt/GattService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ServerDeathRecipient"
.end annotation


# instance fields
.field mAppIf:B

.field final synthetic this$0:Lcom/broadcom/bt/service/gatt/GattService;


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/service/gatt/GattService;B)V
    .registers 3
    .parameter
    .parameter "appIf"

    #@0
    .prologue
    .line 305
    iput-object p1, p0, Lcom/broadcom/bt/service/gatt/GattService$ServerDeathRecipient;->this$0:Lcom/broadcom/bt/service/gatt/GattService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 306
    iput-byte p2, p0, Lcom/broadcom/bt/service/gatt/GattService$ServerDeathRecipient;->mAppIf:B

    #@7
    .line 307
    return-void
.end method


# virtual methods
.method public binderDied()V
    .registers 4

    #@0
    .prologue
    .line 311
    const-string v0, "BtGatt.GattService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Binder is dead - unregistering server ("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-byte v2, p0, Lcom/broadcom/bt/service/gatt/GattService$ServerDeathRecipient;->mAppIf:B

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, ")!"

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 313
    iget-object v0, p0, Lcom/broadcom/bt/service/gatt/GattService$ServerDeathRecipient;->this$0:Lcom/broadcom/bt/service/gatt/GattService;

    #@22
    iget-byte v1, p0, Lcom/broadcom/bt/service/gatt/GattService$ServerDeathRecipient;->mAppIf:B

    #@24
    invoke-virtual {v0, v1}, Lcom/broadcom/bt/service/gatt/GattService;->unregisterServer(B)V

    #@27
    .line 314
    return-void
.end method
