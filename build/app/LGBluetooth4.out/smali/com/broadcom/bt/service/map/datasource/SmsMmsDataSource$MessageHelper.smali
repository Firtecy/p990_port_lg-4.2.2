.class public interface abstract Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageHelper;
.super Ljava/lang/Object;
.source "SmsMmsDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MessageHelper"
.end annotation


# virtual methods
.method public abstract createMessageListIterator()Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;
.end method

.method public abstract finish()V
.end method

.method public abstract getFolderPath(I)Ljava/lang/String;
.end method

.method public abstract getMessage(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZB)Lcom/broadcom/bt/util/bmsg/BMessage;
.end method

.method public abstract getMessageType(Ljava/lang/String;)I
.end method

.method public abstract init(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Landroid/content/Context;)V
.end method

.method public abstract isEncodedMessageId(Ljava/lang/String;)Z
.end method

.method public abstract pushMessage(Lcom/broadcom/bt/map/RequestId;ILcom/broadcom/bt/util/bmsg/BMessage;Ljava/lang/String;Ljava/lang/String;ZZI[Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract setMessageDeleted(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z[Ljava/lang/String;[Ljava/lang/String;)Z
.end method

.method public abstract setMessageRead(Lcom/broadcom/bt/map/RequestId;Ljava/lang/String;Z)V
.end method
