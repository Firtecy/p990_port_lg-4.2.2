.class public Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;
.super Landroid/database/ContentObserver;
.source "SmsEventManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$1;,
        Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    }
.end annotation


# static fields
.field private static final COL_ID:I = 0x0

.field private static final COL_MCE_CREATED:I = 0x4

.field private static final COL_MCE_DELETED:I = 0x5

.field private static final COL_MSG_DATE:I = 0x1

.field private static final COL_MSG_TYPE:I = 0x2

.field private static final COL_OLD_MSG_TYPE:I = 0x6

.field private static final COL_THREAD_ID:I = 0x3

.field private static final DBG:Z = true

.field private static final DB_CREATE:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS sms (_id INTEGER PRIMARY KEY, date INTEGER, type INTEGER, thread_id INTEGER DEFAULT -1,  mce_created INTEGER DEFAULT 0, mce_deleted INTEGER DEFAULT 0, old_msg_type INTEGER DEFAULT -1)"

.field private static final DB_NAME:Ljava/lang/String; = "SmsTempStore"

.field private static final MMS_SMS_CONVERSATIONS:Landroid/net/Uri; = null

.field private static final SMS_PROJ:[Ljava/lang/String; = null

.field private static final SMS_TMP_PROJ:[Ljava/lang/String; = null

.field private static final TABLE_NAME:Ljava/lang/String; = "sms"

.field private static final TAG:Ljava/lang/String; = "BtMap.SmsEventManager"

.field private static final URL_MATCHER:Landroid/content/UriMatcher; = null

.field static final URL_PROCESS_ALL_CONVERSATIONS:I = 0x1

.field static final URL_PROCESS_DELETE_CONVERSATION:I = 0x2

.field static final URL_PROCESS_SMS:I = 0x3


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v5, 0x3

    #@3
    const/4 v4, 0x2

    #@4
    const/4 v3, 0x1

    #@5
    .line 79
    new-array v0, v6, [Ljava/lang/String;

    #@7
    const-string v1, "_id"

    #@9
    aput-object v1, v0, v2

    #@b
    const-string v1, "date"

    #@d
    aput-object v1, v0, v3

    #@f
    const-string v1, "type"

    #@11
    aput-object v1, v0, v4

    #@13
    const-string v1, "thread_id"

    #@15
    aput-object v1, v0, v5

    #@17
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_PROJ:[Ljava/lang/String;

    #@19
    .line 80
    const/4 v0, 0x7

    #@1a
    new-array v0, v0, [Ljava/lang/String;

    #@1c
    const-string v1, "_id"

    #@1e
    aput-object v1, v0, v2

    #@20
    const-string v1, "date"

    #@22
    aput-object v1, v0, v3

    #@24
    const-string v1, "type"

    #@26
    aput-object v1, v0, v4

    #@28
    const-string v1, "thread_id"

    #@2a
    aput-object v1, v0, v5

    #@2c
    const-string v1, "mce_created"

    #@2e
    aput-object v1, v0, v6

    #@30
    const/4 v1, 0x5

    #@31
    const-string v2, "mce_deleted"

    #@33
    aput-object v2, v0, v1

    #@35
    const/4 v1, 0x6

    #@36
    const-string v2, "old_msg_type"

    #@38
    aput-object v2, v0, v1

    #@3a
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_TMP_PROJ:[Ljava/lang/String;

    #@3c
    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@43
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    const-string v1, "/"

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    const-string v1, "conversations?simple=true"

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5e
    move-result-object v0

    #@5f
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->MMS_SMS_CONVERSATIONS:Landroid/net/Uri;

    #@61
    .line 97
    new-instance v0, Landroid/content/UriMatcher;

    #@63
    const/4 v1, -0x1

    #@64
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@67
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@69
    .line 100
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@6b
    const-string v1, "mms-sms"

    #@6d
    const/4 v2, 0x0

    #@6e
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@71
    .line 101
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@73
    const-string v1, "mms-sms"

    #@75
    const-string v2, "conversations/deleted"

    #@77
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@7a
    .line 102
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@7c
    const-string v1, "mms-sms"

    #@7e
    const-string v2, "conversations/deleted/#"

    #@80
    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@83
    .line 103
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@85
    const-string v1, "sms"

    #@87
    const-string v2, "#"

    #@89
    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@8c
    .line 104
    return-void
.end method

.method public constructor <init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Landroid/content/Context;Landroid/os/Handler;)V
    .registers 4
    .parameter "ds"
    .parameter "ctx"
    .parameter "handler"

    #@0
    .prologue
    .line 130
    invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@3
    .line 131
    iput-object p2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mContext:Landroid/content/Context;

    #@5
    .line 132
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@7
    .line 133
    return-void
.end method

.method private declared-synchronized addOrUpdateToTmpStore(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;)Z
    .registers 16
    .parameter "messageId"
    .parameter "info"

    #@0
    .prologue
    .line 340
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtMap.SmsEventManager"

    #@3
    const-string v1, "addOrUpdateToTmpStore"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 343
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@a
    if-eqz v0, :cond_2e

    #@c
    .line 344
    const/4 v0, 0x1

    #@d
    new-array v4, v0, [Ljava/lang/String;

    #@f
    const/4 v0, 0x0

    #@10
    aput-object p1, v4, v0
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_46

    #@12
    .line 345
    .local v4, msgIdArg:[Ljava/lang/String;
    const/4 v11, 0x0

    #@13
    .line 347
    .local v11, c:Landroid/database/Cursor;
    :try_start_13
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@15
    const-string v1, "sms"

    #@17
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_TMP_PROJ:[Ljava/lang/String;

    #@19
    const-string v3, "_id=?"

    #@1b
    const/4 v5, 0x0

    #@1c
    const/4 v6, 0x0

    #@1d
    const/4 v7, 0x0

    #@1e
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@21
    move-result-object v11

    #@22
    .line 348
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_31

    #@28
    .line 349
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->updateTmpStore(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;)Z
    :try_end_2b
    .catchall {:try_start_13 .. :try_end_2b} :catchall_46
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_2b} :catch_3d

    #@2b
    .line 356
    :goto_2b
    :try_start_2b
    invoke-static {v11}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_2e
    .catchall {:try_start_2b .. :try_end_2e} :catchall_46

    #@2e
    .line 358
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v11           #c:Landroid/database/Cursor;
    :cond_2e
    const/4 v0, 0x1

    #@2f
    monitor-exit p0

    #@30
    return v0

    #@31
    .line 351
    .restart local v4       #msgIdArg:[Ljava/lang/String;
    .restart local v11       #c:Landroid/database/Cursor;
    :cond_31
    :try_start_31
    iget-wide v7, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageDate:J

    #@33
    iget v9, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@35
    iget v10, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mThreadId:I

    #@37
    move-object v5, p0

    #@38
    move-object v6, p1

    #@39
    invoke-direct/range {v5 .. v10}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->addToTmpStore(Ljava/lang/String;JII)V
    :try_end_3c
    .catchall {:try_start_31 .. :try_end_3c} :catchall_46
    .catch Ljava/lang/Throwable; {:try_start_31 .. :try_end_3c} :catch_3d

    #@3c
    goto :goto_2b

    #@3d
    .line 353
    :catch_3d
    move-exception v12

    #@3e
    .line 354
    .local v12, t:Ljava/lang/Throwable;
    :try_start_3e
    const-string v0, "BtMap.SmsEventManager"

    #@40
    const-string v1, "addOrUpdateToTmpStore: error"

    #@42
    invoke-static {v0, v1, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_45
    .catchall {:try_start_3e .. :try_end_45} :catchall_46

    #@45
    goto :goto_2b

    #@46
    .line 340
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v11           #c:Landroid/database/Cursor;
    .end local v12           #t:Ljava/lang/Throwable;
    :catchall_46
    move-exception v0

    #@47
    monitor-exit p0

    #@48
    throw v0
.end method

.method private declared-synchronized addToTmpStore(Ljava/lang/String;JII)V
    .registers 11
    .parameter "messageId"
    .parameter "messageDateTime"
    .parameter "messageType"
    .parameter "threadId"

    #@0
    .prologue
    .line 261
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_3e

    #@3
    if-eqz v2, :cond_33

    #@5
    .line 263
    :try_start_5
    new-instance v1, Landroid/content/ContentValues;

    #@7
    const/4 v2, 0x4

    #@8
    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    #@b
    .line 264
    .local v1, v:Landroid/content/ContentValues;
    const-string v2, "_id"

    #@d
    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 265
    const-string v2, "date"

    #@12
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@19
    .line 266
    const-string v2, "type"

    #@1b
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@22
    .line 267
    const-string v2, "thread_id"

    #@24
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2b
    .line 268
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@2d
    const-string v3, "sms"

    #@2f
    const/4 v4, 0x0

    #@30
    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_33
    .catchall {:try_start_5 .. :try_end_33} :catchall_3e
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_33} :catch_35

    #@33
    .line 273
    .end local v1           #v:Landroid/content/ContentValues;
    :cond_33
    :goto_33
    monitor-exit p0

    #@34
    return-void

    #@35
    .line 269
    :catch_35
    move-exception v0

    #@36
    .line 270
    .local v0, t:Ljava/lang/Throwable;
    :try_start_36
    const-string v2, "BtMap.SmsEventManager"

    #@38
    const-string v3, "addToTmpStore(): error"

    #@3a
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3d
    .catchall {:try_start_36 .. :try_end_3d} :catchall_3e

    #@3d
    goto :goto_33

    #@3e
    .line 261
    .end local v0           #t:Ljava/lang/Throwable;
    :catchall_3e
    move-exception v2

    #@3f
    monitor-exit p0

    #@40
    throw v2
.end method

.method private declared-synchronized cleanupTmpStore(Z)V
    .registers 6
    .parameter "close"

    #@0
    .prologue
    .line 158
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@3
    if-eqz v0, :cond_15

    #@5
    .line 159
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@7
    const-string v1, "sms"

    #@9
    const/4 v2, 0x0

    #@a
    const/4 v3, 0x0

    #@b
    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@e
    .line 160
    if-eqz p1, :cond_15

    #@10
    .line 161
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@12
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    #@15
    .line 164
    :cond_15
    monitor-exit p0

    #@16
    return-void

    #@17
    .line 158
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method private getFromSmsProvider(Landroid/net/Uri;)Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    .registers 12
    .parameter "uri"

    #@0
    .prologue
    .line 362
    const/4 v7, 0x0

    #@1
    .line 363
    .local v7, mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@3
    if-eqz v0, :cond_2f

    #@5
    .line 364
    const/4 v6, 0x0

    #@6
    .line 366
    .local v6, c:Landroid/database/Cursor;
    :try_start_6
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@8
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_PROJ:[Ljava/lang/String;

    #@a
    const/4 v3, 0x0

    #@b
    const/4 v4, 0x0

    #@c
    const-string v5, "_id ASC"

    #@e
    move-object v1, p1

    #@f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@12
    move-result-object v6

    #@13
    .line 368
    if-nez v6, :cond_30

    #@15
    .line 370
    const-string v0, "BtMap.SmsEventManager"

    #@17
    const-string v1, "c is null!"

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 371
    new-instance v0, Ljava/lang/Exception;

    #@1e
    const-string v1, "c is nul!"

    #@20
    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    #@23
    throw v0
    :try_end_24
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_24} :catch_24

    #@24
    .line 382
    :catch_24
    move-exception v9

    #@25
    .line 383
    .local v9, t:Ljava/lang/Throwable;
    :goto_25
    const-string v0, "BtMap.SmsEventManager"

    #@27
    const-string v1, "addToTmpStore(): error"

    #@29
    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2c
    .line 385
    .end local v9           #t:Ljava/lang/Throwable;
    :cond_2c
    :goto_2c
    invoke-static {v6}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@2f
    .line 387
    .end local v6           #c:Landroid/database/Cursor;
    :cond_2f
    return-object v7

    #@30
    .line 375
    .restart local v6       #c:Landroid/database/Cursor;
    :cond_30
    :try_start_30
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@33
    move-result v0

    #@34
    if-eqz v0, :cond_2c

    #@36
    .line 376
    new-instance v8, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;

    #@38
    const/4 v0, 0x0

    #@39
    invoke-direct {v8, v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;-><init>(Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$1;)V
    :try_end_3c
    .catch Ljava/lang/Throwable; {:try_start_30 .. :try_end_3c} :catch_24

    #@3c
    .line 377
    .end local v7           #mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    .local v8, mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    const/4 v0, 0x2

    #@3d
    const/4 v1, -0x1

    #@3e
    :try_start_3e
    invoke-static {v6, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@41
    move-result v0

    #@42
    iput v0, v8, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@44
    .line 378
    iget v0, v8, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@46
    invoke-static {v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    iput-object v0, v8, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@4c
    .line 379
    const/4 v0, 0x1

    #@4d
    const-wide/16 v1, 0x0

    #@4f
    invoke-static {v6, v0, v1, v2}, Lcom/broadcom/bt/util/DBUtil;->getLong(Landroid/database/Cursor;IJ)J

    #@52
    move-result-wide v0

    #@53
    iput-wide v0, v8, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageDate:J

    #@55
    .line 380
    const/4 v0, 0x3

    #@56
    const/4 v1, -0x1

    #@57
    invoke-static {v6, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@5a
    move-result v0

    #@5b
    iput v0, v8, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mThreadId:I
    :try_end_5d
    .catch Ljava/lang/Throwable; {:try_start_3e .. :try_end_5d} :catch_5f

    #@5d
    move-object v7, v8

    #@5e
    .end local v8           #mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    .restart local v7       #mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    goto :goto_2c

    #@5f
    .line 382
    .end local v7           #mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    .restart local v8       #mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    :catch_5f
    move-exception v9

    #@60
    move-object v7, v8

    #@61
    .end local v8           #mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    .restart local v7       #mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    goto :goto_25
.end method

.method private declared-synchronized getFromTmpStore(Ljava/lang/String;)Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    .registers 13
    .parameter "messageId"

    #@0
    .prologue
    .line 288
    monitor-enter p0

    #@1
    const/4 v9, 0x0

    #@2
    .line 289
    .local v9, mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    :try_start_2
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_34

    #@4
    if-eqz v0, :cond_29

    #@6
    .line 290
    const/4 v8, 0x0

    #@7
    .line 292
    .local v8, c:Landroid/database/Cursor;
    :try_start_7
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@9
    const-string v1, "sms"

    #@b
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_TMP_PROJ:[Ljava/lang/String;

    #@d
    const-string v3, "_id=?"

    #@f
    const/4 v4, 0x1

    #@10
    new-array v4, v4, [Ljava/lang/String;

    #@12
    const/4 v5, 0x0

    #@13
    aput-object p1, v4, v5

    #@15
    const/4 v5, 0x0

    #@16
    const/4 v6, 0x0

    #@17
    const/4 v7, 0x0

    #@18
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1b
    move-result-object v8

    #@1c
    .line 294
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_26

    #@22
    .line 295
    invoke-direct {p0, v8}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->toTmpSmsMsgInfo(Landroid/database/Cursor;)Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    :try_end_25
    .catchall {:try_start_7 .. :try_end_25} :catchall_34
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_25} :catch_2b

    #@25
    move-result-object v9

    #@26
    .line 300
    :cond_26
    :goto_26
    :try_start_26
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_34

    #@29
    .line 302
    .end local v8           #c:Landroid/database/Cursor;
    :cond_29
    monitor-exit p0

    #@2a
    return-object v9

    #@2b
    .line 297
    .restart local v8       #c:Landroid/database/Cursor;
    :catch_2b
    move-exception v10

    #@2c
    .line 298
    .local v10, t:Ljava/lang/Throwable;
    :try_start_2c
    const-string v0, "BtMap.SmsEventManager"

    #@2e
    const-string v1, "getFromTmpStore(): error"

    #@30
    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_33
    .catchall {:try_start_2c .. :try_end_33} :catchall_34

    #@33
    goto :goto_26

    #@34
    .line 288
    .end local v8           #c:Landroid/database/Cursor;
    .end local v10           #t:Ljava/lang/Throwable;
    :catchall_34
    move-exception v0

    #@35
    monitor-exit p0

    #@36
    throw v0
.end method

.method private declared-synchronized initTmpStore()V
    .registers 8

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 136
    monitor-enter p0

    #@2
    :try_start_2
    invoke-static {}, Lcom/broadcom/bt/service/map/MapService;->getDefaultTmpDir()Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    .line 137
    .local v1, dbDirectory:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@9
    move-result v4

    #@a
    if-nez v4, :cond_30

    #@c
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    #@f
    move-result v4

    #@10
    if-nez v4, :cond_30

    #@12
    .line 138
    const-string v4, "BtMap.SmsEventManager"

    #@14
    new-instance v5, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v6, "Cannot create tmpstore directory: "

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2e
    .catchall {:try_start_2 .. :try_end_2e} :catchall_62

    #@2e
    .line 155
    :goto_2e
    monitor-exit p0

    #@2f
    return-void

    #@30
    .line 143
    :cond_30
    :try_start_30
    new-instance v2, Ljava/io/File;

    #@32
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    const-string v5, "SmsTempStore"

    #@38
    invoke-direct {v2, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 144
    .local v2, dbFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@3e
    move-result v4

    #@3f
    if-nez v4, :cond_42

    #@41
    const/4 v0, 0x1

    #@42
    .line 145
    .local v0, createNew:Z
    :cond_42
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    const/4 v5, 0x0

    #@47
    const/high16 v6, 0x1000

    #@49
    invoke-static {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    #@4c
    move-result-object v4

    #@4d
    iput-object v4, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@4f
    .line 147
    if-eqz v0, :cond_65

    #@51
    .line 148
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@53
    const-string v5, "CREATE TABLE IF NOT EXISTS sms (_id INTEGER PRIMARY KEY, date INTEGER, type INTEGER, thread_id INTEGER DEFAULT -1,  mce_created INTEGER DEFAULT 0, mce_deleted INTEGER DEFAULT 0, old_msg_type INTEGER DEFAULT -1)"

    #@55
    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_58
    .catchall {:try_start_30 .. :try_end_58} :catchall_62
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_30 .. :try_end_58} :catch_59

    #@58
    goto :goto_2e

    #@59
    .line 152
    .end local v0           #createNew:Z
    .end local v2           #dbFile:Ljava/io/File;
    :catch_59
    move-exception v3

    #@5a
    .line 153
    .local v3, e:Landroid/database/sqlite/SQLiteException;
    :try_start_5a
    const-string v4, "BtMap.SmsEventManager"

    #@5c
    const-string v5, "Error creating tmpstore"

    #@5e
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_61
    .catchall {:try_start_5a .. :try_end_61} :catchall_62

    #@61
    goto :goto_2e

    #@62
    .line 136
    .end local v1           #dbDirectory:Ljava/io/File;
    .end local v3           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_62
    move-exception v4

    #@63
    monitor-exit p0

    #@64
    throw v4

    #@65
    .line 150
    .restart local v0       #createNew:Z
    .restart local v1       #dbDirectory:Ljava/io/File;
    .restart local v2       #dbFile:Ljava/io/File;
    :cond_65
    const/4 v4, 0x0

    #@66
    :try_start_66
    invoke-direct {p0, v4}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->cleanupTmpStore(Z)V
    :try_end_69
    .catchall {:try_start_66 .. :try_end_69} :catchall_62
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_66 .. :try_end_69} :catch_59

    #@69
    goto :goto_2e
.end method

.method private processAllMessageChanges()V
    .registers 16

    #@0
    .prologue
    .line 613
    const-string v0, "BtMap.SmsEventManager"

    #@2
    const-string v1, "processAllMessageChanges()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 615
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@9
    invoke-virtual {v0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getPhoneType()I

    #@c
    move-result v0

    #@d
    invoke-static {v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getSMSMessageType(I)B

    #@10
    move-result v12

    #@11
    .line 617
    .local v12, smsMsgType:B
    const/4 v8, 0x0

    #@12
    .line 618
    .local v8, c:Landroid/database/Cursor;
    new-instance v10, Landroid/util/SparseBooleanArray;

    #@14
    invoke-direct {v10}, Landroid/util/SparseBooleanArray;-><init>()V

    #@17
    .line 620
    .local v10, mThreadIds:Landroid/util/SparseBooleanArray;
    :try_start_17
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@19
    sget-object v1, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->MMS_SMS_CONVERSATIONS:Landroid/net/Uri;

    #@1b
    const/4 v2, 0x1

    #@1c
    new-array v2, v2, [Ljava/lang/String;

    #@1e
    const/4 v3, 0x0

    #@1f
    const-string v4, "_id"

    #@21
    aput-object v4, v2, v3

    #@23
    const/4 v3, 0x0

    #@24
    const/4 v4, 0x0

    #@25
    const-string v5, "_id asc "

    #@27
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@2a
    move-result-object v8

    #@2b
    .line 622
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@2e
    move-result v0

    #@2f
    if-eqz v0, :cond_59

    #@31
    .line 624
    :cond_31
    const/4 v0, 0x0

    #@32
    const/4 v1, -0x1

    #@33
    invoke-static {v8, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@36
    move-result v14

    #@37
    .line 625
    .local v14, threadId:I
    const-string v0, "BtMap.SmsEventManager"

    #@39
    new-instance v1, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v2, "adding thread id to cache: "

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v1

    #@48
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v1

    #@4c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 626
    const/4 v0, 0x1

    #@50
    invoke-virtual {v10, v14, v0}, Landroid/util/SparseBooleanArray;->append(IZ)V

    #@53
    .line 627
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_56
    .catch Ljava/lang/Throwable; {:try_start_17 .. :try_end_56} :catch_ce

    #@56
    move-result v0

    #@57
    if-nez v0, :cond_31

    #@59
    .line 635
    .end local v14           #threadId:I
    :cond_59
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@5c
    .line 637
    :try_start_5c
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@5e
    const-string v1, "sms"

    #@60
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_TMP_PROJ:[Ljava/lang/String;

    #@62
    const/4 v3, 0x0

    #@63
    const/4 v4, 0x0

    #@64
    const/4 v5, 0x0

    #@65
    const/4 v6, 0x0

    #@66
    const-string v7, "_id ASC"

    #@68
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@6b
    move-result-object v8

    #@6c
    .line 638
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@6f
    move-result v0

    #@70
    if-eqz v0, :cond_ca

    #@72
    .line 640
    :cond_72
    const/4 v0, 0x3

    #@73
    const/4 v1, -0x1

    #@74
    invoke-static {v8, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@77
    move-result v14

    #@78
    .line 641
    .restart local v14       #threadId:I
    if-lez v14, :cond_c4

    #@7a
    invoke-virtual {v10, v14}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@7d
    move-result v0

    #@7e
    if-nez v0, :cond_c4

    #@80
    .line 642
    const-string v0, "BtMap.SmsEventManager"

    #@82
    new-instance v1, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v2, "Removing message with threadId "

    #@89
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v1

    #@8d
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v1

    #@91
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v1

    #@95
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 645
    invoke-direct {p0, v8}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->toTmpSmsMsgInfo(Landroid/database/Cursor;)Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;

    #@9b
    move-result-object v9

    #@9c
    .line 646
    .local v9, mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    iget v0, v9, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageId:I

    #@9e
    if-lez v0, :cond_c4

    #@a0
    iget-object v0, v9, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@a2
    if-eqz v0, :cond_c4

    #@a4
    .line 647
    iget v0, v9, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageId:I

    #@a6
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@a9
    move-result-object v11

    #@aa
    .line 648
    .local v11, msgId:Ljava/lang/String;
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@ac
    invoke-static {v11}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->encodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@af
    move-result-object v1

    #@b0
    iget-object v2, v9, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@b2
    invoke-virtual {v0, v1, v12, v2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageDeleted(Ljava/lang/String;BLjava/lang/String;)V

    #@b5
    .line 650
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@b7
    const-string v1, "sms"

    #@b9
    const-string v2, "_id=?"

    #@bb
    const/4 v3, 0x1

    #@bc
    new-array v3, v3, [Ljava/lang/String;

    #@be
    const/4 v4, 0x0

    #@bf
    aput-object v11, v3, v4

    #@c1
    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@c4
    .line 653
    .end local v9           #mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    .end local v11           #msgId:Ljava/lang/String;
    :cond_c4
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_c7
    .catch Ljava/lang/Throwable; {:try_start_5c .. :try_end_c7} :catch_da

    #@c7
    move-result v0

    #@c8
    if-nez v0, :cond_72

    #@ca
    .line 658
    .end local v14           #threadId:I
    :cond_ca
    :goto_ca
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@cd
    .line 659
    :goto_cd
    return-void

    #@ce
    .line 629
    :catch_ce
    move-exception v13

    #@cf
    .line 630
    .local v13, t:Ljava/lang/Throwable;
    const-string v0, "BtMap.SmsEventManager"

    #@d1
    const-string v1, "processAllMessageChanges(): error with query. Exiting..."

    #@d3
    invoke-static {v0, v1, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d6
    .line 631
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@d9
    goto :goto_cd

    #@da
    .line 655
    .end local v13           #t:Ljava/lang/Throwable;
    :catch_da
    move-exception v13

    #@db
    .line 656
    .restart local v13       #t:Ljava/lang/Throwable;
    const-string v0, "BtMap.SmsEventManager"

    #@dd
    const-string v1, "Error cleaning..."

    #@df
    invoke-static {v0, v1, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e2
    goto :goto_ca
.end method

.method private processDeleteConversation(Ljava/lang/String;)V
    .registers 18
    .parameter "conversationId"

    #@0
    .prologue
    .line 663
    const/4 v9, 0x0

    #@1
    .line 664
    .local v9, c:Landroid/database/Cursor;
    const/4 v11, 0x0

    #@2
    .line 666
    .local v11, hasConversation:Z
    :try_start_2
    move-object/from16 v0, p0

    #@4
    iget-object v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@6
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->MMS_SMS_CONVERSATIONS:Landroid/net/Uri;

    #@8
    const/4 v3, 0x1

    #@9
    new-array v3, v3, [Ljava/lang/String;

    #@b
    const/4 v4, 0x0

    #@c
    const-string v5, "_id"

    #@e
    aput-object v5, v3, v4

    #@10
    const-string v4, "_id = ?"

    #@12
    const/4 v5, 0x1

    #@13
    new-array v5, v5, [Ljava/lang/String;

    #@15
    const/4 v6, 0x0

    #@16
    aput-object p1, v5, v6

    #@18
    const/4 v6, 0x0

    #@19
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1c
    move-result-object v9

    #@1d
    .line 668
    invoke-static {v9}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_20} :catch_2e

    #@20
    move-result v11

    #@21
    .line 672
    :goto_21
    invoke-static {v9}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@24
    .line 673
    if-eqz v11, :cond_37

    #@26
    .line 674
    const-string v1, "BtMap.SmsEventManager"

    #@28
    const-string v2, "processDeleteConversation(): conversation still exists...Ignoring delete request..."

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 707
    :goto_2d
    return-void

    #@2e
    .line 669
    :catch_2e
    move-exception v15

    #@2f
    .line 670
    .local v15, t:Ljava/lang/Throwable;
    const-string v1, "BtMap.SmsEventManager"

    #@31
    const-string v2, "processDeleteConversation(): error with query. Exiting..."

    #@33
    invoke-static {v1, v2, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@36
    goto :goto_21

    #@37
    .line 679
    .end local v15           #t:Ljava/lang/Throwable;
    :cond_37
    const-string v1, "BtMap.SmsEventManager"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "processDeleteConversation(): deleting conversation "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    move-object/from16 v0, p1

    #@46
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 682
    move-object/from16 v0, p0

    #@53
    iget-object v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@55
    invoke-virtual {v1}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getPhoneType()I

    #@58
    move-result v1

    #@59
    invoke-static {v1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getSMSMessageType(I)B

    #@5c
    move-result v14

    #@5d
    .line 684
    .local v14, smsMsgType:B
    :try_start_5d
    move-object/from16 v0, p0

    #@5f
    iget-object v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@61
    const-string v2, "sms"

    #@63
    sget-object v3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_TMP_PROJ:[Ljava/lang/String;

    #@65
    const-string v4, "thread_id=?"

    #@67
    const/4 v5, 0x1

    #@68
    new-array v5, v5, [Ljava/lang/String;

    #@6a
    const/4 v6, 0x0

    #@6b
    aput-object p1, v5, v6

    #@6d
    const/4 v6, 0x0

    #@6e
    const/4 v7, 0x0

    #@6f
    const-string v8, "_id ASC"

    #@71
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@74
    move-result-object v9

    #@75
    .line 686
    invoke-static {v9}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@78
    move-result v1

    #@79
    if-eqz v1, :cond_ce

    #@7b
    .line 689
    :cond_7b
    move-object/from16 v0, p0

    #@7d
    invoke-direct {v0, v9}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->toTmpSmsMsgInfo(Landroid/database/Cursor;)Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;

    #@80
    move-result-object v12

    #@81
    .line 690
    .local v12, mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    iget v1, v12, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageId:I

    #@83
    if-lez v1, :cond_c8

    #@85
    iget-object v1, v12, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@87
    if-eqz v1, :cond_c8

    #@89
    .line 691
    iget v1, v12, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageId:I

    #@8b
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@8e
    move-result-object v13

    #@8f
    .line 693
    .local v13, msgId:Ljava/lang/String;
    const-string v1, "BtMap.SmsEventManager"

    #@91
    new-instance v2, Ljava/lang/StringBuilder;

    #@93
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v3, "Deleting message "

    #@98
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v2

    #@9c
    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v2

    #@a0
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a3
    move-result-object v2

    #@a4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 695
    move-object/from16 v0, p0

    #@a9
    iget-object v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@ab
    const-string v2, "sms"

    #@ad
    const-string v3, "_id=?"

    #@af
    const/4 v4, 0x1

    #@b0
    new-array v4, v4, [Ljava/lang/String;

    #@b2
    const/4 v5, 0x0

    #@b3
    aput-object v13, v4, v5

    #@b5
    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@b8
    move-result v10

    #@b9
    .line 696
    .local v10, deleted:I
    if-lez v10, :cond_c8

    #@bb
    .line 697
    move-object/from16 v0, p0

    #@bd
    iget-object v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@bf
    invoke-static {v13}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->encodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@c2
    move-result-object v2

    #@c3
    iget-object v3, v12, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@c5
    invoke-virtual {v1, v2, v14, v3}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageDeleted(Ljava/lang/String;BLjava/lang/String;)V

    #@c8
    .line 701
    .end local v10           #deleted:I
    .end local v13           #msgId:Ljava/lang/String;
    :cond_c8
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_cb
    .catch Ljava/lang/Throwable; {:try_start_5d .. :try_end_cb} :catch_d3

    #@cb
    move-result v1

    #@cc
    if-nez v1, :cond_7b

    #@ce
    .line 706
    .end local v12           #mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    :cond_ce
    :goto_ce
    invoke-static {v9}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@d1
    goto/16 :goto_2d

    #@d3
    .line 703
    :catch_d3
    move-exception v15

    #@d4
    .line 704
    .restart local v15       #t:Ljava/lang/Throwable;
    const-string v1, "BtMap.SmsEventManager"

    #@d6
    const-string v2, "Error deleting conversation..."

    #@d8
    invoke-static {v1, v2, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@db
    goto :goto_ce
.end method

.method private processMessageChange(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;)V
    .registers 13
    .parameter "messageId"
    .parameter "msgInfo"
    .parameter "oldMsgInfo"

    #@0
    .prologue
    .line 479
    const-string v6, "BtMap.SmsEventManager"

    #@2
    new-instance v7, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v8, "processMessageChange: messageId="

    #@9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v7

    #@d
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v7

    #@11
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v7

    #@15
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 480
    const-string v6, "BtMap.SmsEventManager"

    #@1a
    new-instance v7, Ljava/lang/StringBuilder;

    #@1c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1f
    const-string v8, "processMessageChange: msgInfo="

    #@21
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v7

    #@25
    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v7

    #@2d
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 481
    const-string v7, "BtMap.SmsEventManager"

    #@32
    new-instance v6, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v8, "processMessageChange: msgInfo.messageType="

    #@39
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    if-nez p2, :cond_ba

    #@3f
    const-string v6, "unknown"

    #@41
    :goto_41
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v6

    #@49
    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 483
    const-string v6, "BtMap.SmsEventManager"

    #@4e
    new-instance v7, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v8, "processMessageChange: oldMsgInfo="

    #@55
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v7

    #@59
    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v7

    #@5d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v7

    #@61
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 484
    const-string v7, "BtMap.SmsEventManager"

    #@66
    new-instance v6, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v8, "processMessageChange: oldMsgInfo.messageType="

    #@6d
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v8

    #@71
    if-nez p3, :cond_c1

    #@73
    const-string v6, "unknown"

    #@75
    :goto_75
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v6

    #@79
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v6

    #@7d
    invoke-static {v7, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 487
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@82
    invoke-virtual {v6}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->getPhoneType()I

    #@85
    move-result v4

    #@86
    .line 488
    .local v4, phoneType:I
    invoke-static {v4}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getSMSMessageType(I)B

    #@89
    move-result v5

    #@8a
    .line 489
    .local v5, smsMsgType:B
    invoke-static {p1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->encodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@8d
    move-result-object v0

    #@8e
    .line 491
    .local v0, encodedMessageId:Ljava/lang/String;
    if-nez p2, :cond_d0

    #@90
    .line 493
    const-string v6, "BtMap.SmsEventManager"

    #@92
    const-string v7, "Message deleted!"

    #@94
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@97
    .line 496
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->removeFromTmpStore(Ljava/lang/String;)Z

    #@9a
    .line 500
    if-eqz p3, :cond_c8

    #@9c
    iget-object v6, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@9e
    if-eqz v6, :cond_c8

    #@a0
    .line 501
    iget v6, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMceDeleted:I

    #@a2
    const/4 v7, 0x1

    #@a3
    if-eq v6, v7, :cond_b9

    #@a5
    .line 503
    const-string v6, "BtMap.SmsEventManager"

    #@a7
    const-string v7, "Delete was not requested from MCE...Sending notification..."

    #@a9
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 505
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@ae
    iget-object v7, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@b0
    invoke-virtual {v6, v0, v5, v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageDeleted(Ljava/lang/String;BLjava/lang/String;)V

    #@b3
    .line 506
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@b5
    const/4 v7, 0x0

    #@b6
    invoke-virtual {v6, v0, v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->updateMessageId(Ljava/lang/String;Ljava/lang/String;)V

    #@b9
    .line 609
    :cond_b9
    :goto_b9
    return-void

    #@ba
    .line 481
    .end local v0           #encodedMessageId:Ljava/lang/String;
    .end local v4           #phoneType:I
    .end local v5           #smsMsgType:B
    :cond_ba
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@bc
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@bf
    move-result-object v6

    #@c0
    goto :goto_41

    #@c1
    .line 484
    :cond_c1
    iget v6, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@c3
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c6
    move-result-object v6

    #@c7
    goto :goto_75

    #@c8
    .line 509
    .restart local v0       #encodedMessageId:Ljava/lang/String;
    .restart local v4       #phoneType:I
    .restart local v5       #smsMsgType:B
    :cond_c8
    const-string v6, "BtMap.SmsEventManager"

    #@ca
    const-string v7, "Not sending message deleted notification: folderPath not set"

    #@cc
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    goto :goto_b9

    #@d0
    .line 515
    :cond_d0
    if-eqz p3, :cond_d7

    #@d2
    iget v6, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMceCreated:I

    #@d4
    const/4 v7, 0x1

    #@d5
    if-ne v6, v7, :cond_164

    #@d7
    .line 517
    :cond_d7
    const-string v6, "BtMap.SmsEventManager"

    #@d9
    const-string v7, "New message!"

    #@db
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@de
    .line 519
    if-eqz p3, :cond_f2

    #@e0
    iget v6, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMceCreated:I

    #@e2
    const/4 v7, 0x1

    #@e3
    if-ne v6, v7, :cond_f2

    #@e5
    const/4 v1, 0x1

    #@e6
    .line 520
    .local v1, mceCreated:Z
    :goto_e6
    iget-object v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@e8
    if-nez v6, :cond_f4

    #@ea
    .line 521
    const-string v6, "BtMap.SmsEventManager"

    #@ec
    const-string v7, "Ignoring message with invalid folderpath..."

    #@ee
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f1
    goto :goto_b9

    #@f2
    .line 519
    .end local v1           #mceCreated:Z
    :cond_f2
    const/4 v1, 0x0

    #@f3
    goto :goto_e6

    #@f4
    .line 526
    .restart local v1       #mceCreated:Z
    :cond_f4
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->addOrUpdateToTmpStore(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;)Z

    #@f7
    .line 528
    const/4 v6, 0x0

    #@f8
    invoke-virtual {p0, p1, v6}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->setMessageCreatedByMce(Ljava/lang/String;Z)Z

    #@fb
    .line 533
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@fd
    const/4 v7, 0x1

    #@fe
    if-ne v6, v7, :cond_10a

    #@100
    .line 534
    if-nez v1, :cond_b9

    #@102
    .line 536
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@104
    iget-object v7, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@106
    invoke-virtual {v6, v0, v5, v7}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyNewMessage(Ljava/lang/String;BLjava/lang/String;)V

    #@109
    goto :goto_b9

    #@10a
    .line 544
    :cond_10a
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@10c
    const/4 v7, 0x5

    #@10d
    if-ne v6, v7, :cond_11b

    #@10f
    .line 546
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@111
    const/4 v7, 0x4

    #@112
    invoke-static {v7}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@115
    move-result-object v7

    #@116
    const/4 v8, 0x0

    #@117
    invoke-virtual {v6, v0, v5, v7, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifySendStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@11a
    goto :goto_b9

    #@11b
    .line 548
    :cond_11b
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@11d
    const/4 v7, 0x2

    #@11e
    if-ne v6, v7, :cond_138

    #@120
    .line 552
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@122
    iget-object v7, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@124
    const/4 v8, 0x4

    #@125
    invoke-static {v8}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@128
    move-result-object v8

    #@129
    invoke-virtual {v6, v0, v5, v7, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageMoved(Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;)V

    #@12c
    .line 554
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@12e
    const/4 v7, 0x2

    #@12f
    invoke-static {v7}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@132
    move-result-object v7

    #@133
    const/4 v8, 0x1

    #@134
    invoke-virtual {v6, v0, v5, v7, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifySendStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@137
    goto :goto_b9

    #@138
    .line 558
    :cond_138
    const-string v6, "BtMap.SmsEventManager"

    #@13a
    new-instance v7, Ljava/lang/StringBuilder;

    #@13c
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@13f
    const-string v8, "Ignoring new message type "

    #@141
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v7

    #@145
    iget v8, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@147
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v7

    #@14b
    const-string v8, ", folder "

    #@14d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v7

    #@151
    iget v8, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@153
    invoke-static {v8}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@156
    move-result-object v8

    #@157
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v7

    #@15b
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15e
    move-result-object v7

    #@15f
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@162
    goto/16 :goto_b9

    #@164
    .line 567
    .end local v1           #mceCreated:Z
    :cond_164
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@166
    iget v7, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@168
    if-eq v6, v7, :cond_b9

    #@16a
    .line 568
    iget v6, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@16c
    invoke-static {v6}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@16f
    move-result-object v3

    #@170
    .line 569
    .local v3, oldFolderPath:Ljava/lang/String;
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@172
    invoke-static {v6}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@175
    move-result-object v2

    #@176
    .line 571
    .local v2, newFolderPath:Ljava/lang/String;
    const-string v6, "BtMap.SmsEventManager"

    #@178
    new-instance v7, Ljava/lang/StringBuilder;

    #@17a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@17d
    const-string v8, "Message shifted from "

    #@17f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@182
    move-result-object v7

    #@183
    if-nez v3, :cond_187

    #@185
    const-string v3, "(null)"

    #@187
    .end local v3           #oldFolderPath:Ljava/lang/String;
    :cond_187
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v7

    #@18b
    const-string v8, " to "

    #@18d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    move-result-object v7

    #@191
    if-nez v2, :cond_195

    #@193
    const-string v2, "(null)"

    #@195
    .end local v2           #newFolderPath:Ljava/lang/String;
    :cond_195
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@198
    move-result-object v7

    #@199
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19c
    move-result-object v7

    #@19d
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a0
    .line 576
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->updateTmpStore(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;)Z

    #@1a3
    .line 580
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@1a5
    const/4 v7, 0x5

    #@1a6
    if-ne v6, v7, :cond_1bc

    #@1a8
    .line 581
    const-string v6, "BtMap.SmsEventManager"

    #@1aa
    const-string v7, "Send failed"

    #@1ac
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1af
    .line 582
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@1b1
    const/4 v7, 0x4

    #@1b2
    invoke-static {v7}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@1b5
    move-result-object v7

    #@1b6
    const/4 v8, 0x0

    #@1b7
    invoke-virtual {v6, v0, v5, v7, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifySendStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@1ba
    goto/16 :goto_b9

    #@1bc
    .line 584
    :cond_1bc
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@1be
    const/4 v7, 0x2

    #@1bf
    if-ne v6, v7, :cond_1e3

    #@1c1
    .line 585
    const-string v6, "BtMap.SmsEventManager"

    #@1c3
    const-string v7, "Send succeeded"

    #@1c5
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c8
    .line 586
    iget v6, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@1ca
    const/4 v7, 0x4

    #@1cb
    if-ne v6, v7, :cond_1d6

    #@1cd
    .line 587
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@1cf
    iget-object v7, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@1d1
    iget-object v8, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@1d3
    invoke-virtual {v6, v0, v5, v7, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageMoved(Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;)V

    #@1d6
    .line 590
    :cond_1d6
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@1d8
    const/4 v7, 0x2

    #@1d9
    invoke-static {v7}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@1dc
    move-result-object v7

    #@1dd
    const/4 v8, 0x1

    #@1de
    invoke-virtual {v6, v0, v5, v7, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifySendStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@1e1
    goto/16 :goto_b9

    #@1e3
    .line 599
    :cond_1e3
    iget v6, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@1e5
    const/4 v7, 0x6

    #@1e6
    if-eq v6, v7, :cond_1f8

    #@1e8
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@1ea
    const/4 v7, 0x6

    #@1eb
    if-eq v6, v7, :cond_1f8

    #@1ed
    .line 601
    iget-object v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@1ef
    iget-object v7, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@1f1
    iget-object v8, p3, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@1f3
    invoke-virtual {v6, v0, v5, v7, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageMoved(Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;)V

    #@1f6
    goto/16 :goto_b9

    #@1f8
    .line 605
    :cond_1f8
    const-string v6, "BtMap.SmsEventManager"

    #@1fa
    const-string v7, "Ignoring unused message shift..."

    #@1fc
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ff
    goto/16 :goto_b9
.end method

.method private declared-synchronized removeFromTmpStore(Ljava/lang/String;)Z
    .registers 10
    .parameter "messageId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 306
    monitor-enter p0

    #@3
    :try_start_3
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_27

    #@5
    if-eqz v3, :cond_25

    #@7
    .line 308
    :try_start_7
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@9
    const-string v4, "sms"

    #@b
    const-string v5, "_id=?"

    #@d
    const/4 v6, 0x1

    #@e
    new-array v6, v6, [Ljava/lang/String;

    #@10
    const/4 v7, 0x0

    #@11
    aput-object p1, v6, v7

    #@13
    invoke-virtual {v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_16
    .catchall {:try_start_7 .. :try_end_16} :catchall_27
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_16} :catch_1d

    #@16
    move-result v3

    #@17
    if-lez v3, :cond_1b

    #@19
    .line 313
    :goto_19
    monitor-exit p0

    #@1a
    return v1

    #@1b
    :cond_1b
    move v1, v2

    #@1c
    .line 308
    goto :goto_19

    #@1d
    .line 309
    :catch_1d
    move-exception v0

    #@1e
    .line 310
    .local v0, t:Ljava/lang/Throwable;
    :try_start_1e
    const-string v1, "BtMap.SmsEventManager"

    #@20
    const-string v3, "removeFromTempStore: error"

    #@22
    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_25
    .catchall {:try_start_1e .. :try_end_25} :catchall_27

    #@25
    .end local v0           #t:Ljava/lang/Throwable;
    :cond_25
    move v1, v2

    #@26
    .line 313
    goto :goto_19

    #@27
    .line 306
    :catchall_27
    move-exception v1

    #@28
    monitor-exit p0

    #@29
    throw v1
.end method

.method private toTmpSmsMsgInfo(Landroid/database/Cursor;)Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    .registers 7
    .parameter "c"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 276
    new-instance v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {v0, v1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;-><init>(Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$1;)V

    #@8
    .line 277
    .local v0, mInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    invoke-static {p1, v4, v2}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@b
    move-result v1

    #@c
    iput v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageId:I

    #@e
    .line 278
    const/4 v1, 0x2

    #@f
    invoke-static {p1, v1, v2}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@12
    move-result v1

    #@13
    iput v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@15
    .line 279
    iget v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@17
    invoke-static {v1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    iput-object v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@1d
    .line 280
    const/4 v1, 0x1

    #@1e
    const-wide/16 v2, 0x0

    #@20
    invoke-static {p1, v1, v2, v3}, Lcom/broadcom/bt/util/DBUtil;->getLong(Landroid/database/Cursor;IJ)J

    #@23
    move-result-wide v1

    #@24
    iput-wide v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageDate:J

    #@26
    .line 281
    const/4 v1, 0x4

    #@27
    invoke-static {p1, v1, v4}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@2a
    move-result v1

    #@2b
    iput v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMceCreated:I

    #@2d
    .line 282
    const/4 v1, 0x5

    #@2e
    invoke-static {p1, v1, v4}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@31
    move-result v1

    #@32
    iput v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMceDeleted:I

    #@34
    .line 283
    const/4 v1, 0x3

    #@35
    invoke-static {p1, v1, v4}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@38
    move-result v1

    #@39
    iput v1, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mThreadId:I

    #@3b
    .line 284
    return-object v0
.end method

.method private declared-synchronized updateTmpStore(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;)Z
    .registers 11
    .parameter "messageId"
    .parameter "info"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 318
    monitor-enter p0

    #@3
    :try_start_3
    const-string v5, "BtMap.SmsEventManager"

    #@5
    const-string v6, "updateTmpStore"

    #@7
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 320
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@c
    if-eqz v5, :cond_58

    #@e
    .line 321
    const/4 v5, 0x1

    #@f
    new-array v0, v5, [Ljava/lang/String;

    #@11
    const/4 v5, 0x0

    #@12
    aput-object p1, v0, v5
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_5a

    #@14
    .line 323
    .local v0, msgIdArg:[Ljava/lang/String;
    :try_start_14
    new-instance v2, Landroid/content/ContentValues;

    #@16
    const/4 v5, 0x4

    #@17
    invoke-direct {v2, v5}, Landroid/content/ContentValues;-><init>(I)V

    #@1a
    .line 324
    .local v2, v:Landroid/content/ContentValues;
    const-string v5, "_id"

    #@1c
    invoke-virtual {v2, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 325
    const-string v5, "date"

    #@21
    iget-wide v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageDate:J

    #@23
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@2a
    .line 326
    const-string v5, "type"

    #@2c
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mMessageType:I

    #@2e
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@35
    .line 327
    const-string v5, "thread_id"

    #@37
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;->mThreadId:I

    #@39
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3c
    move-result-object v6

    #@3d
    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@40
    .line 329
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@42
    const-string v6, "sms"

    #@44
    const-string v7, "_id=?"

    #@46
    invoke-virtual {v5, v6, v2, v7, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_49
    .catchall {:try_start_14 .. :try_end_49} :catchall_5a
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_49} :catch_50

    #@49
    move-result v5

    #@4a
    if-lez v5, :cond_4e

    #@4c
    .line 334
    .end local v0           #msgIdArg:[Ljava/lang/String;
    .end local v2           #v:Landroid/content/ContentValues;
    :goto_4c
    monitor-exit p0

    #@4d
    return v3

    #@4e
    .restart local v0       #msgIdArg:[Ljava/lang/String;
    .restart local v2       #v:Landroid/content/ContentValues;
    :cond_4e
    move v3, v4

    #@4f
    .line 329
    goto :goto_4c

    #@50
    .line 330
    .end local v2           #v:Landroid/content/ContentValues;
    :catch_50
    move-exception v1

    #@51
    .line 331
    .local v1, t:Ljava/lang/Throwable;
    :try_start_51
    const-string v3, "BtMap.SmsEventManager"

    #@53
    const-string v5, "updateTmpStore: error"

    #@55
    invoke-static {v3, v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_58
    .catchall {:try_start_51 .. :try_end_58} :catchall_5a

    #@58
    .end local v0           #msgIdArg:[Ljava/lang/String;
    .end local v1           #t:Ljava/lang/Throwable;
    :cond_58
    move v3, v4

    #@59
    .line 334
    goto :goto_4c

    #@5a
    .line 318
    :catchall_5a
    move-exception v3

    #@5b
    monitor-exit p0

    #@5c
    throw v3
.end method


# virtual methods
.method public finish()V
    .registers 2

    #@0
    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->releaseContentObserver()Landroid/database/IContentObserver;

    #@3
    .line 423
    const/4 v0, 0x1

    #@4
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->cleanupTmpStore(Z)V

    #@7
    .line 424
    return-void
.end method

.method public declared-synchronized getOldMessageType(Ljava/lang/String;)I
    .registers 13
    .parameter "messageId"

    #@0
    .prologue
    .line 240
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtMap.SmsEventManager"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "getOldMessageType: messageId="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 242
    const/4 v9, -0x1

    #@1a
    .line 243
    .local v9, oldMsgType:I
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@1c
    if-eqz v0, :cond_43

    #@1e
    .line 244
    const/4 v0, 0x1

    #@1f
    new-array v4, v0, [Ljava/lang/String;

    #@21
    const/4 v0, 0x0

    #@22
    aput-object p1, v4, v0
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_4e

    #@24
    .line 245
    .local v4, msgIdArg:[Ljava/lang/String;
    const/4 v8, 0x0

    #@25
    .line 247
    .local v8, c:Landroid/database/Cursor;
    :try_start_25
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@27
    const-string v1, "sms"

    #@29
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_TMP_PROJ:[Ljava/lang/String;

    #@2b
    const-string v3, "_id=?"

    #@2d
    const/4 v5, 0x0

    #@2e
    const/4 v6, 0x0

    #@2f
    const/4 v7, 0x0

    #@30
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@33
    move-result-object v8

    #@34
    .line 248
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_40

    #@3a
    .line 249
    const/4 v0, 0x6

    #@3b
    const/4 v1, -0x1

    #@3c
    invoke-static {v8, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I
    :try_end_3f
    .catchall {:try_start_25 .. :try_end_3f} :catchall_4e
    .catch Ljava/lang/Throwable; {:try_start_25 .. :try_end_3f} :catch_45

    #@3f
    move-result v9

    #@40
    .line 254
    :cond_40
    :goto_40
    :try_start_40
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_4e

    #@43
    .line 256
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    :cond_43
    monitor-exit p0

    #@44
    return v9

    #@45
    .line 251
    .restart local v4       #msgIdArg:[Ljava/lang/String;
    .restart local v8       #c:Landroid/database/Cursor;
    :catch_45
    move-exception v10

    #@46
    .line 252
    .local v10, t:Ljava/lang/Throwable;
    :try_start_46
    const-string v0, "BtMap.SmsEventManager"

    #@48
    const-string v1, "getOldMessageType(): error"

    #@4a
    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4d
    .catchall {:try_start_46 .. :try_end_4d} :catchall_4e

    #@4d
    goto :goto_40

    #@4e
    .line 240
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v9           #oldMsgType:I
    .end local v10           #t:Ljava/lang/Throwable;
    :catchall_4e
    move-exception v0

    #@4f
    monitor-exit p0

    #@50
    throw v0
.end method

.method public declared-synchronized init()V
    .registers 8

    #@0
    .prologue
    .line 391
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@9
    .line 392
    const-string v0, "BtMap.SmsEventManager"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Registering content observer for "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 393
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@25
    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@27
    const/4 v2, 0x1

    #@28
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@2b
    .line 395
    const-string v0, "BtMap.SmsEventManager"

    #@2d
    new-instance v1, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v2, "Registering content observer for "

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    sget-object v2, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 396
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@47
    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@49
    const/4 v2, 0x0

    #@4a
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@4d
    .line 399
    const-string v0, "BtMap.SmsEventManager"

    #@4f
    new-instance v1, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v2, "Registering content observer for "

    #@56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENHANCED_CONVERSATION_DELETE_CONTENT_URI:Landroid/net/Uri;

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 401
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@69
    sget-object v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENHANCED_CONVERSATION_DELETE_CONTENT_URI:Landroid/net/Uri;

    #@6b
    const/4 v2, 0x1

    #@6c
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@6f
    .line 405
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->initTmpStore()V
    :try_end_72
    .catchall {:try_start_1 .. :try_end_72} :catchall_ad

    #@72
    .line 406
    const/4 v6, 0x0

    #@73
    .line 408
    .local v6, c:Landroid/database/Cursor;
    :try_start_73
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@75
    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@77
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_PROJ:[Ljava/lang/String;

    #@79
    const/4 v3, 0x0

    #@7a
    const/4 v4, 0x0

    #@7b
    const/4 v5, 0x0

    #@7c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@7f
    move-result-object v6

    #@80
    .line 409
    invoke-static {v6}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@83
    move-result v0

    #@84
    if-eqz v0, :cond_a8

    #@86
    .line 411
    :cond_86
    const/4 v0, 0x0

    #@87
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@8a
    move-result-object v1

    #@8b
    const/4 v0, 0x1

    #@8c
    const-wide/16 v2, -0x1

    #@8e
    invoke-static {v6, v0, v2, v3}, Lcom/broadcom/bt/util/DBUtil;->getLong(Landroid/database/Cursor;IJ)J

    #@91
    move-result-wide v2

    #@92
    const/4 v0, 0x2

    #@93
    const/4 v4, -0x1

    #@94
    invoke-static {v6, v0, v4}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@97
    move-result v4

    #@98
    const/4 v0, 0x3

    #@99
    const/4 v5, -0x1

    #@9a
    invoke-static {v6, v0, v5}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@9d
    move-result v5

    #@9e
    move-object v0, p0

    #@9f
    invoke-direct/range {v0 .. v5}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->addToTmpStore(Ljava/lang/String;JII)V

    #@a2
    .line 413
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_a5
    .catchall {:try_start_73 .. :try_end_a5} :catchall_ad
    .catch Ljava/lang/Throwable; {:try_start_73 .. :try_end_a5} :catch_b0

    #@a5
    move-result v0

    #@a6
    if-nez v0, :cond_86

    #@a8
    .line 418
    :cond_a8
    :goto_a8
    :try_start_a8
    invoke-static {v6}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_ab
    .catchall {:try_start_a8 .. :try_end_ab} :catchall_ad

    #@ab
    .line 419
    monitor-exit p0

    #@ac
    return-void

    #@ad
    .line 391
    .end local v6           #c:Landroid/database/Cursor;
    :catchall_ad
    move-exception v0

    #@ae
    monitor-exit p0

    #@af
    throw v0

    #@b0
    .line 415
    .restart local v6       #c:Landroid/database/Cursor;
    :catch_b0
    move-exception v0

    #@b1
    goto :goto_a8
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .registers 11
    .parameter "selfChange"
    .parameter "uri"

    #@0
    .prologue
    .line 428
    if-nez p2, :cond_3

    #@2
    .line 475
    :goto_2
    return-void

    #@3
    .line 431
    :cond_3
    sget-object v5, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@5
    invoke-virtual {v5, p2}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@8
    move-result v4

    #@9
    .line 433
    .local v4, uriMatch:I
    const-string v5, "BtMap.SmsEventManager"

    #@b
    new-instance v6, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v7, "onChange(): uri ="

    #@12
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    const-string v7, ", selfChange="

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    const-string v7, ", uriMatch="

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 436
    const/4 v5, 0x1

    #@36
    if-ne v4, v5, :cond_3c

    #@38
    .line 437
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->processAllMessageChanges()V

    #@3b
    goto :goto_2

    #@3c
    .line 439
    :cond_3c
    const/4 v5, 0x2

    #@3d
    if-ne v4, v5, :cond_56

    #@3f
    .line 443
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    .line 445
    .local v0, messageId:Ljava/lang/String;
    :try_start_43
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@46
    .line 446
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->processDeleteConversation(Ljava/lang/String;)V
    :try_end_49
    .catch Ljava/lang/Throwable; {:try_start_43 .. :try_end_49} :catch_4a

    #@49
    goto :goto_2

    #@4a
    .line 447
    :catch_4a
    move-exception v3

    #@4b
    .line 448
    .local v3, t:Ljava/lang/Throwable;
    const-string v5, "BtMap.SmsEventManager"

    #@4d
    const-string v6, "onChange(): conversationId not found. Processing for all message changes...."

    #@4f
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 450
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->processAllMessageChanges()V

    #@55
    goto :goto_2

    #@56
    .line 455
    .end local v0           #messageId:Ljava/lang/String;
    .end local v3           #t:Ljava/lang/Throwable;
    :cond_56
    const/4 v5, 0x3

    #@57
    if-eq v4, v5, :cond_72

    #@59
    .line 456
    const-string v5, "BtMap.SmsEventManager"

    #@5b
    new-instance v6, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v7, "onChange: ignoring "

    #@62
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v6

    #@6e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    goto :goto_2

    #@72
    .line 460
    :cond_72
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    .line 462
    .restart local v0       #messageId:Ljava/lang/String;
    :try_start_76
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_79
    .catch Ljava/lang/Throwable; {:try_start_76 .. :try_end_79} :catch_9e

    #@79
    .line 468
    const-string v5, "BtMap.SmsEventManager"

    #@7b
    new-instance v6, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v7, "messageId="

    #@82
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v6

    #@86
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v6

    #@8a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v6

    #@8e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 472
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->getFromTmpStore(Ljava/lang/String;)Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;

    #@94
    move-result-object v2

    #@95
    .line 473
    .local v2, oldMsgInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    invoke-direct {p0, p2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->getFromSmsProvider(Landroid/net/Uri;)Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;

    #@98
    move-result-object v1

    #@99
    .line 474
    .local v1, msgInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    invoke-direct {p0, v0, v1, v2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->processMessageChange(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;)V

    #@9c
    goto/16 :goto_2

    #@9e
    .line 463
    .end local v1           #msgInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    .end local v2           #oldMsgInfo:Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager$SmsMsgInfo;
    :catch_9e
    move-exception v3

    #@9f
    .line 464
    .restart local v3       #t:Ljava/lang/Throwable;
    const-string v5, "BtMap.SmsEventManager"

    #@a1
    const-string v6, "onChange(): messageId not found. Skipping...."

    #@a3
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    goto/16 :goto_2
.end method

.method public declared-synchronized setMessageCreatedByMce(Ljava/lang/String;Z)Z
    .registers 15
    .parameter "messageId"
    .parameter "createdByMce"

    #@0
    .prologue
    .line 186
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtMap.SmsEventManager"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "setMessageCreatedByMce: messageId="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ", createdByMce="

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 189
    const/4 v9, 0x0

    #@24
    .line 190
    .local v9, success:Z
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@26
    if-eqz v0, :cond_6b

    #@28
    .line 191
    const/4 v0, 0x1

    #@29
    new-array v4, v0, [Ljava/lang/String;

    #@2b
    const/4 v0, 0x0

    #@2c
    aput-object p1, v4, v0

    #@2e
    .line 192
    .local v4, msgIdArg:[Ljava/lang/String;
    new-instance v11, Landroid/content/ContentValues;

    #@30
    const/4 v0, 0x2

    #@31
    invoke-direct {v11, v0}, Landroid/content/ContentValues;-><init>(I)V

    #@34
    .line 193
    .local v11, v:Landroid/content/ContentValues;
    const-string v0, "_id"

    #@36
    invoke-virtual {v11, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 194
    const-string v1, "mce_created"

    #@3b
    if-eqz p2, :cond_6d

    #@3d
    const/4 v0, 0x1

    #@3e
    :goto_3e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v0

    #@42
    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_45
    .catchall {:try_start_1 .. :try_end_45} :catchall_83

    #@45
    .line 195
    const/4 v8, 0x0

    #@46
    .line 197
    .local v8, c:Landroid/database/Cursor;
    :try_start_46
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@48
    const-string v1, "sms"

    #@4a
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_TMP_PROJ:[Ljava/lang/String;

    #@4c
    const-string v3, "_id=?"

    #@4e
    const/4 v5, 0x0

    #@4f
    const/4 v6, 0x0

    #@50
    const/4 v7, 0x0

    #@51
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@54
    move-result-object v8

    #@55
    .line 198
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@58
    move-result v0

    #@59
    if-eqz v0, :cond_71

    #@5b
    .line 199
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@5d
    const-string v1, "sms"

    #@5f
    const-string v2, "_id=?"

    #@61
    invoke-virtual {v0, v1, v11, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_64
    .catchall {:try_start_46 .. :try_end_64} :catchall_83
    .catch Ljava/lang/Throwable; {:try_start_46 .. :try_end_64} :catch_7a

    #@64
    move-result v0

    #@65
    if-lez v0, :cond_6f

    #@67
    const/4 v9, 0x1

    #@68
    .line 206
    :goto_68
    :try_start_68
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_6b
    .catchall {:try_start_68 .. :try_end_6b} :catchall_83

    #@6b
    .line 208
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v11           #v:Landroid/content/ContentValues;
    :cond_6b
    monitor-exit p0

    #@6c
    return v9

    #@6d
    .line 194
    .restart local v4       #msgIdArg:[Ljava/lang/String;
    .restart local v11       #v:Landroid/content/ContentValues;
    :cond_6d
    const/4 v0, 0x0

    #@6e
    goto :goto_3e

    #@6f
    .line 199
    .restart local v8       #c:Landroid/database/Cursor;
    :cond_6f
    const/4 v9, 0x0

    #@70
    goto :goto_68

    #@71
    .line 201
    :cond_71
    :try_start_71
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@73
    const-string v1, "sms"

    #@75
    const/4 v2, 0x0

    #@76
    invoke-virtual {v0, v1, v2, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_79
    .catchall {:try_start_71 .. :try_end_79} :catchall_83
    .catch Ljava/lang/Throwable; {:try_start_71 .. :try_end_79} :catch_7a

    #@79
    goto :goto_68

    #@7a
    .line 203
    :catch_7a
    move-exception v10

    #@7b
    .line 204
    .local v10, t:Ljava/lang/Throwable;
    :try_start_7b
    const-string v0, "BtMap.SmsEventManager"

    #@7d
    const-string v1, "setMessageCreatedByMce(): error"

    #@7f
    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_82
    .catchall {:try_start_7b .. :try_end_82} :catchall_83

    #@82
    goto :goto_68

    #@83
    .line 186
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v9           #success:Z
    .end local v10           #t:Ljava/lang/Throwable;
    .end local v11           #v:Landroid/content/ContentValues;
    :catchall_83
    move-exception v0

    #@84
    monitor-exit p0

    #@85
    throw v0
.end method

.method public declared-synchronized setMessageDeletedByMce(Ljava/lang/String;Z)Z
    .registers 12
    .parameter "messageId"
    .parameter "deletedByMce"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 168
    monitor-enter p0

    #@3
    :try_start_3
    const-string v4, "BtMap.SmsEventManager"

    #@5
    new-instance v5, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v6, "setMessageDeletedByMce: messageId="

    #@c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v5

    #@10
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    const-string v6, ", deletedByMce="

    #@16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 172
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_5d

    #@27
    if-eqz v4, :cond_5b

    #@29
    .line 174
    :try_start_29
    new-instance v1, Landroid/content/ContentValues;

    #@2b
    const/4 v4, 0x1

    #@2c
    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    #@2f
    .line 175
    .local v1, v:Landroid/content/ContentValues;
    const-string v5, "mce_deleted"

    #@31
    if-eqz p2, :cond_4f

    #@33
    move v4, v2

    #@34
    :goto_34
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3b
    .line 176
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@3d
    const-string v5, "sms"

    #@3f
    const-string v6, "_id=?"

    #@41
    const/4 v7, 0x1

    #@42
    new-array v7, v7, [Ljava/lang/String;

    #@44
    const/4 v8, 0x0

    #@45
    aput-object p1, v7, v8

    #@47
    invoke-virtual {v4, v5, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4a
    .catchall {:try_start_29 .. :try_end_4a} :catchall_5d
    .catch Ljava/lang/Throwable; {:try_start_29 .. :try_end_4a} :catch_53

    #@4a
    move-result v4

    #@4b
    if-lez v4, :cond_51

    #@4d
    .line 181
    .end local v1           #v:Landroid/content/ContentValues;
    :goto_4d
    monitor-exit p0

    #@4e
    return v2

    #@4f
    .restart local v1       #v:Landroid/content/ContentValues;
    :cond_4f
    move v4, v3

    #@50
    .line 175
    goto :goto_34

    #@51
    :cond_51
    move v2, v3

    #@52
    .line 176
    goto :goto_4d

    #@53
    .line 177
    .end local v1           #v:Landroid/content/ContentValues;
    :catch_53
    move-exception v0

    #@54
    .line 178
    .local v0, t:Ljava/lang/Throwable;
    :try_start_54
    const-string v2, "BtMap.SmsEventManager"

    #@56
    const-string v4, "addToTmpStore(): error"

    #@58
    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5b
    .catchall {:try_start_54 .. :try_end_5b} :catchall_5d

    #@5b
    .end local v0           #t:Ljava/lang/Throwable;
    :cond_5b
    move v2, v3

    #@5c
    .line 181
    goto :goto_4d

    #@5d
    .line 168
    :catchall_5d
    move-exception v2

    #@5e
    monitor-exit p0

    #@5f
    throw v2
.end method

.method public declared-synchronized setOldMessageType(Ljava/lang/String;I)Z
    .registers 15
    .parameter "messageId"
    .parameter "oldMessageType"

    #@0
    .prologue
    .line 213
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtMap.SmsEventManager"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "setOldMessageType: messageId="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ", oldMessageType="

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 216
    const/4 v9, 0x0

    #@24
    .line 217
    .local v9, success:Z
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@26
    if-eqz v0, :cond_68

    #@28
    .line 218
    const/4 v0, 0x1

    #@29
    new-array v4, v0, [Ljava/lang/String;

    #@2b
    const/4 v0, 0x0

    #@2c
    aput-object p1, v4, v0

    #@2e
    .line 219
    .local v4, msgIdArg:[Ljava/lang/String;
    new-instance v11, Landroid/content/ContentValues;

    #@30
    const/4 v0, 0x2

    #@31
    invoke-direct {v11, v0}, Landroid/content/ContentValues;-><init>(I)V

    #@34
    .line 220
    .local v11, v:Landroid/content/ContentValues;
    const-string v0, "_id"

    #@36
    invoke-virtual {v11, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 221
    const-string v0, "old_msg_type"

    #@3b
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_42
    .catchall {:try_start_1 .. :try_end_42} :catchall_88

    #@42
    .line 222
    const/4 v8, 0x0

    #@43
    .line 224
    .local v8, c:Landroid/database/Cursor;
    :try_start_43
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@45
    const-string v1, "sms"

    #@47
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->SMS_TMP_PROJ:[Ljava/lang/String;

    #@49
    const-string v3, "_id=?"

    #@4b
    const/4 v5, 0x0

    #@4c
    const/4 v6, 0x0

    #@4d
    const/4 v7, 0x0

    #@4e
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@51
    move-result-object v8

    #@52
    .line 225
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@55
    move-result v0

    #@56
    if-eqz v0, :cond_6c

    #@58
    .line 226
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@5a
    const-string v1, "sms"

    #@5c
    const-string v2, "_id=?"

    #@5e
    invoke-virtual {v0, v1, v11, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_61
    .catchall {:try_start_43 .. :try_end_61} :catchall_88
    .catch Ljava/lang/Throwable; {:try_start_43 .. :try_end_61} :catch_7f

    #@61
    move-result v0

    #@62
    if-lez v0, :cond_6a

    #@64
    const/4 v9, 0x1

    #@65
    .line 233
    :goto_65
    :try_start_65
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_68
    .catchall {:try_start_65 .. :try_end_68} :catchall_88

    #@68
    .line 235
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v11           #v:Landroid/content/ContentValues;
    :cond_68
    monitor-exit p0

    #@69
    return v9

    #@6a
    .line 226
    .restart local v4       #msgIdArg:[Ljava/lang/String;
    .restart local v8       #c:Landroid/database/Cursor;
    .restart local v11       #v:Landroid/content/ContentValues;
    :cond_6a
    const/4 v9, 0x0

    #@6b
    goto :goto_65

    #@6c
    .line 228
    :cond_6c
    const-wide/16 v0, 0x0

    #@6e
    :try_start_6e
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@70
    const-string v3, "sms"

    #@72
    const/4 v5, 0x0

    #@73
    invoke-virtual {v2, v3, v5, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_76
    .catchall {:try_start_6e .. :try_end_76} :catchall_88
    .catch Ljava/lang/Throwable; {:try_start_6e .. :try_end_76} :catch_7f

    #@76
    move-result-wide v2

    #@77
    cmp-long v0, v0, v2

    #@79
    if-gez v0, :cond_7d

    #@7b
    const/4 v9, 0x1

    #@7c
    :goto_7c
    goto :goto_65

    #@7d
    :cond_7d
    const/4 v9, 0x0

    #@7e
    goto :goto_7c

    #@7f
    .line 230
    :catch_7f
    move-exception v10

    #@80
    .line 231
    .local v10, t:Ljava/lang/Throwable;
    :try_start_80
    const-string v0, "BtMap.SmsEventManager"

    #@82
    const-string v1, "setOldMessageType(): error"

    #@84
    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_87
    .catchall {:try_start_80 .. :try_end_87} :catchall_88

    #@87
    goto :goto_65

    #@88
    .line 213
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v9           #success:Z
    .end local v10           #t:Ljava/lang/Throwable;
    .end local v11           #v:Landroid/content/ContentValues;
    :catchall_88
    move-exception v0

    #@89
    monitor-exit p0

    #@8a
    throw v0
.end method
