.class public Lcom/broadcom/bt/service/ProfileConfig;
.super Lcom/android/bluetooth/btservice/Config;
.source "ProfileConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;
    }
.end annotation


# static fields
.field private static final INDEX_CLASS_NAME:I = 0x0

.field private static final INDEX_CONFIGURABLE:I = 0x5

.field private static final INDEX_DEFAULT_START:I = 0x4

.field private static final INDEX_DESCRIPTION:I = 0x2

.field private static final INDEX_DETAILED_FRAGMENT:I = 0x6

.field private static final INDEX_NAME:I = 0x1

.field private static final INDEX_SUPPORTED:I = 0x3

#the value of this static final field might be set in the static constructor
.field private static final LG_DBG:Z = false

.field private static final PROFILE_SERVICES:[[Ljava/lang/Object; = null

.field private static final SETTINGS_PREFIX:Ljava/lang/String; = "bt_svcst_"

.field private static SUPPORTED_PROFILES:[Ljava/lang/Class; = null

.field private static SUPPORTED_PROFILES_CFG:[Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg; = null

.field private static final TAG:Ljava/lang/String; = "BtSettings.ProfileConfig"

.field private static mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .registers 10

    #@0
    .prologue
    const/4 v9, 0x4

    #@1
    const/4 v8, 0x3

    #@2
    const/4 v7, 0x2

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v5, 0x0

    #@5
    .line 96
    const/16 v0, 0x9

    #@7
    new-array v0, v0, [[Ljava/lang/Object;

    #@9
    const/4 v1, 0x7

    #@a
    new-array v1, v1, [Ljava/lang/Object;

    #@c
    const-class v2, Lcom/android/bluetooth/hfp/HeadsetService;

    #@e
    aput-object v2, v1, v5

    #@10
    const v2, 0x7f07007d

    #@13
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@16
    move-result-object v2

    #@17
    aput-object v2, v1, v6

    #@19
    const v2, 0x7f07007e

    #@1c
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f
    move-result-object v2

    #@20
    aput-object v2, v1, v7

    #@22
    const v2, 0x7f060002

    #@25
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@28
    move-result-object v2

    #@29
    aput-object v2, v1, v8

    #@2b
    const v2, 0x7f06000a

    #@2e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v2

    #@32
    aput-object v2, v1, v9

    #@34
    const/4 v2, 0x5

    #@35
    const v3, 0x7f06000b

    #@38
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3b
    move-result-object v3

    #@3c
    aput-object v3, v1, v2

    #@3e
    const/4 v2, 0x6

    #@3f
    const/4 v3, 0x0

    #@40
    aput-object v3, v1, v2

    #@42
    aput-object v1, v0, v5

    #@44
    const/4 v1, 0x7

    #@45
    new-array v1, v1, [Ljava/lang/Object;

    #@47
    const-class v2, Lcom/android/bluetooth/a2dp/A2dpService;

    #@49
    aput-object v2, v1, v5

    #@4b
    const v2, 0x7f07007f

    #@4e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@51
    move-result-object v2

    #@52
    aput-object v2, v1, v6

    #@54
    const v2, 0x7f070080

    #@57
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5a
    move-result-object v2

    #@5b
    aput-object v2, v1, v7

    #@5d
    const/high16 v2, 0x7f06

    #@5f
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@62
    move-result-object v2

    #@63
    aput-object v2, v1, v8

    #@65
    const v2, 0x7f06000c

    #@68
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6b
    move-result-object v2

    #@6c
    aput-object v2, v1, v9

    #@6e
    const/4 v2, 0x5

    #@6f
    const v3, 0x7f06000d

    #@72
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@75
    move-result-object v3

    #@76
    aput-object v3, v1, v2

    #@78
    const/4 v2, 0x6

    #@79
    const/4 v3, 0x0

    #@7a
    aput-object v3, v1, v2

    #@7c
    aput-object v1, v0, v6

    #@7e
    const/4 v1, 0x7

    #@7f
    new-array v1, v1, [Ljava/lang/Object;

    #@81
    const-class v2, Lcom/android/bluetooth/hid/HidService;

    #@83
    aput-object v2, v1, v5

    #@85
    const v2, 0x7f070081

    #@88
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8b
    move-result-object v2

    #@8c
    aput-object v2, v1, v6

    #@8e
    const v2, 0x7f070082

    #@91
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@94
    move-result-object v2

    #@95
    aput-object v2, v1, v7

    #@97
    const v2, 0x7f060003

    #@9a
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@9d
    move-result-object v2

    #@9e
    aput-object v2, v1, v8

    #@a0
    const v2, 0x7f06000e

    #@a3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a6
    move-result-object v2

    #@a7
    aput-object v2, v1, v9

    #@a9
    const/4 v2, 0x5

    #@aa
    const v3, 0x7f06000f

    #@ad
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@b0
    move-result-object v3

    #@b1
    aput-object v3, v1, v2

    #@b3
    const/4 v2, 0x6

    #@b4
    const/4 v3, 0x0

    #@b5
    aput-object v3, v1, v2

    #@b7
    aput-object v1, v0, v7

    #@b9
    const/4 v1, 0x7

    #@ba
    new-array v1, v1, [Ljava/lang/Object;

    #@bc
    const-class v2, Lcom/android/bluetooth/hdp/HealthService;

    #@be
    aput-object v2, v1, v5

    #@c0
    const v2, 0x7f070083

    #@c3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c6
    move-result-object v2

    #@c7
    aput-object v2, v1, v6

    #@c9
    const v2, 0x7f070084

    #@cc
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@cf
    move-result-object v2

    #@d0
    aput-object v2, v1, v7

    #@d2
    const v2, 0x7f060001

    #@d5
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@d8
    move-result-object v2

    #@d9
    aput-object v2, v1, v8

    #@db
    const v2, 0x7f060010

    #@de
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e1
    move-result-object v2

    #@e2
    aput-object v2, v1, v9

    #@e4
    const/4 v2, 0x5

    #@e5
    const v3, 0x7f060011

    #@e8
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@eb
    move-result-object v3

    #@ec
    aput-object v3, v1, v2

    #@ee
    const/4 v2, 0x6

    #@ef
    const/4 v3, 0x0

    #@f0
    aput-object v3, v1, v2

    #@f2
    aput-object v1, v0, v8

    #@f4
    const/4 v1, 0x7

    #@f5
    new-array v1, v1, [Ljava/lang/Object;

    #@f7
    const-class v2, Lcom/android/bluetooth/pan/PanService;

    #@f9
    aput-object v2, v1, v5

    #@fb
    const v2, 0x7f070085

    #@fe
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@101
    move-result-object v2

    #@102
    aput-object v2, v1, v6

    #@104
    const v2, 0x7f070086

    #@107
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@10a
    move-result-object v2

    #@10b
    aput-object v2, v1, v7

    #@10d
    const v2, 0x7f060005

    #@110
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@113
    move-result-object v2

    #@114
    aput-object v2, v1, v8

    #@116
    const v2, 0x7f060012

    #@119
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11c
    move-result-object v2

    #@11d
    aput-object v2, v1, v9

    #@11f
    const/4 v2, 0x5

    #@120
    const v3, 0x7f060013

    #@123
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@126
    move-result-object v3

    #@127
    aput-object v3, v1, v2

    #@129
    const/4 v2, 0x6

    #@12a
    const/4 v3, 0x0

    #@12b
    aput-object v3, v1, v2

    #@12d
    aput-object v1, v0, v9

    #@12f
    const/4 v1, 0x5

    #@130
    const/4 v2, 0x7

    #@131
    new-array v2, v2, [Ljava/lang/Object;

    #@133
    const-class v3, Lcom/broadcom/bt/service/map/MapService;

    #@135
    aput-object v3, v2, v5

    #@137
    const v3, 0x7f07008d

    #@13a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13d
    move-result-object v3

    #@13e
    aput-object v3, v2, v6

    #@140
    const v3, 0x7f07008e

    #@143
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@146
    move-result-object v3

    #@147
    aput-object v3, v2, v7

    #@149
    const v3, 0x7f06001d

    #@14c
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@14f
    move-result-object v3

    #@150
    aput-object v3, v2, v8

    #@152
    const v3, 0x7f06001e

    #@155
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@158
    move-result-object v3

    #@159
    aput-object v3, v2, v9

    #@15b
    const/4 v3, 0x5

    #@15c
    const v4, 0x7f06001f

    #@15f
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@162
    move-result-object v4

    #@163
    aput-object v4, v2, v3

    #@165
    const/4 v3, 0x6

    #@166
    const-class v4, Lcom/broadcom/bt/service/map/settings/MapAdvancedSettings;

    #@168
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@16b
    move-result-object v4

    #@16c
    aput-object v4, v2, v3

    #@16e
    aput-object v2, v0, v1

    #@170
    const/4 v1, 0x6

    #@171
    const/4 v2, 0x7

    #@172
    new-array v2, v2, [Ljava/lang/Object;

    #@174
    const-class v3, Lcom/broadcom/bt/service/gatt/GattService;

    #@176
    aput-object v3, v2, v5

    #@178
    const v3, 0x7f070087

    #@17b
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@17e
    move-result-object v3

    #@17f
    aput-object v3, v2, v6

    #@181
    const v3, 0x7f070088

    #@184
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@187
    move-result-object v3

    #@188
    aput-object v3, v2, v7

    #@18a
    const v3, 0x7f060017

    #@18d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@190
    move-result-object v3

    #@191
    aput-object v3, v2, v8

    #@193
    const v3, 0x7f060018

    #@196
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@199
    move-result-object v3

    #@19a
    aput-object v3, v2, v9

    #@19c
    const/4 v3, 0x5

    #@19d
    const v4, 0x7f060019

    #@1a0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a3
    move-result-object v4

    #@1a4
    aput-object v4, v2, v3

    #@1a6
    const/4 v3, 0x6

    #@1a7
    const/4 v4, 0x0

    #@1a8
    aput-object v4, v2, v3

    #@1aa
    aput-object v2, v0, v1

    #@1ac
    const/4 v1, 0x7

    #@1ad
    const/4 v2, 0x7

    #@1ae
    new-array v2, v2, [Ljava/lang/Object;

    #@1b0
    const-class v3, Lcom/broadcom/bt/service/sap/SapService;

    #@1b2
    aput-object v3, v2, v5

    #@1b4
    const v3, 0x7f070089

    #@1b7
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1ba
    move-result-object v3

    #@1bb
    aput-object v3, v2, v6

    #@1bd
    const v3, 0x7f07008a

    #@1c0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c3
    move-result-object v3

    #@1c4
    aput-object v3, v2, v7

    #@1c6
    const v3, 0x7f06001a

    #@1c9
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1cc
    move-result-object v3

    #@1cd
    aput-object v3, v2, v8

    #@1cf
    const v3, 0x7f06001b

    #@1d2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1d5
    move-result-object v3

    #@1d6
    aput-object v3, v2, v9

    #@1d8
    const/4 v3, 0x5

    #@1d9
    const v4, 0x7f06001c

    #@1dc
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1df
    move-result-object v4

    #@1e0
    aput-object v4, v2, v3

    #@1e2
    const/4 v3, 0x6

    #@1e3
    const/4 v4, 0x0

    #@1e4
    aput-object v4, v2, v3

    #@1e6
    aput-object v2, v0, v1

    #@1e8
    const/16 v1, 0x8

    #@1ea
    const/4 v2, 0x7

    #@1eb
    new-array v2, v2, [Ljava/lang/Object;

    #@1ed
    const-class v3, Lcom/broadcom/bt/service/ftp/FTPService;

    #@1ef
    aput-object v3, v2, v5

    #@1f1
    const v3, 0x7f07008b

    #@1f4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1f7
    move-result-object v3

    #@1f8
    aput-object v3, v2, v6

    #@1fa
    const v3, 0x7f07008c

    #@1fd
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@200
    move-result-object v3

    #@201
    aput-object v3, v2, v7

    #@203
    const v3, 0x7f060014

    #@206
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@209
    move-result-object v3

    #@20a
    aput-object v3, v2, v8

    #@20c
    const v3, 0x7f060015

    #@20f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@212
    move-result-object v3

    #@213
    aput-object v3, v2, v9

    #@215
    const/4 v3, 0x5

    #@216
    const v4, 0x7f060016

    #@219
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21c
    move-result-object v4

    #@21d
    aput-object v4, v2, v3

    #@21f
    const/4 v3, 0x6

    #@220
    const/4 v4, 0x0

    #@221
    aput-object v4, v2, v3

    #@223
    aput-object v2, v0, v1

    #@225
    sput-object v0, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@227
    .line 199
    new-array v0, v5, [Ljava/lang/Class;

    #@229
    sput-object v0, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES:[Ljava/lang/Class;

    #@22b
    .line 200
    new-array v0, v5, [Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;

    #@22d
    sput-object v0, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES_CFG:[Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;

    #@22f
    .line 349
    const-string v0, "persist.service.btui.debug"

    #@231
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@234
    move-result-object v0

    #@235
    const-string v1, "1"

    #@237
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23a
    move-result v0

    #@23b
    sput-boolean v0, Lcom/broadcom/bt/service/ProfileConfig;->LG_DBG:Z

    #@23d
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 81
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/Config;-><init>()V

    #@3
    .line 189
    return-void
.end method

.method private static BtUiLog(Ljava/lang/String;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 383
    sget-boolean v0, Lcom/broadcom/bt/service/ProfileConfig;->LG_DBG:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 384
    const-string v0, "BtSettings.ProfileConfig"

    #@6
    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 386
    :cond_9
    return-void
.end method

.method private static checkProfile(ILjava/lang/String;)Z
    .registers 6
    .parameter "index"
    .parameter "profileName"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 373
    sget-object v3, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@3
    aget-object v3, v3, p0

    #@5
    aget-object v1, v3, v2

    #@7
    check-cast v1, Ljava/lang/Class;

    #@9
    .line 374
    .local v1, profile:Ljava/lang/Class;
    if-eqz v1, :cond_18

    #@b
    .line 375
    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    .line 376
    .local v0, name:Ljava/lang/String;
    if-eqz v0, :cond_18

    #@11
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v3

    #@15
    if-eqz v3, :cond_18

    #@17
    .line 377
    const/4 v2, 0x1

    #@18
    .line 380
    .end local v0           #name:Ljava/lang/String;
    :cond_18
    return v2
.end method

.method private static findProfileIndex(Ljava/lang/String;)I
    .registers 3
    .parameter "className"

    #@0
    .prologue
    .line 289
    const/4 v0, 0x0

    #@1
    .local v0, i:I
    :goto_1
    sget-object v1, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES:[Ljava/lang/Class;

    #@3
    array-length v1, v1

    #@4
    if-ge v0, v1, :cond_18

    #@6
    .line 290
    sget-object v1, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES:[Ljava/lang/Class;

    #@8
    aget-object v1, v1, v0

    #@a
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_15

    #@14
    .line 294
    .end local v0           #i:I
    :goto_14
    return v0

    #@15
    .line 289
    .restart local v0       #i:I
    :cond_15
    add-int/lit8 v0, v0, 0x1

    #@17
    goto :goto_1

    #@18
    .line 294
    :cond_18
    const/4 v0, -0x1

    #@19
    goto :goto_14
.end method

.method public static getSupportedProfileCfgs()[Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;
    .registers 1

    #@0
    .prologue
    .line 303
    sget-object v0, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES_CFG:[Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;

    #@2
    return-object v0
.end method

.method public static getSupportedProfiles()[Ljava/lang/Class;
    .registers 1

    #@0
    .prologue
    .line 299
    sget-object v0, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES:[Ljava/lang/Class;

    #@2
    return-object v0
.end method

.method public static init(Landroid/content/Context;)V
    .registers 15
    .parameter "ctx"

    #@0
    .prologue
    .line 222
    if-nez p0, :cond_3

    #@2
    .line 286
    :cond_2
    :goto_2
    return-void

    #@3
    .line 225
    :cond_3
    sput-object p0, Lcom/broadcom/bt/service/ProfileConfig;->mContext:Landroid/content/Context;

    #@5
    .line 226
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@8
    move-result-object v7

    #@9
    .line 227
    .local v7, resources:Landroid/content/res/Resources;
    if-eqz v7, :cond_2

    #@b
    .line 230
    new-instance v6, Ljava/util/ArrayList;

    #@d
    sget-object v11, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@f
    array-length v11, v11

    #@10
    invoke-direct {v6, v11}, Ljava/util/ArrayList;-><init>(I)V

    #@13
    .line 231
    .local v6, profiles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Class;>;"
    new-instance v1, Ljava/util/ArrayList;

    #@15
    sget-object v11, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@17
    array-length v11, v11

    #@18
    invoke-direct {v1, v11}, Ljava/util/ArrayList;-><init>(I)V

    #@1b
    .line 232
    .local v1, cfgs:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;>;"
    const/4 v4, 0x0

    #@1c
    .local v4, i:I
    :goto_1c
    sget-object v11, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@1e
    array-length v11, v11

    #@1f
    if-ge v4, v11, :cond_e9

    #@21
    .line 233
    sget-object v11, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@23
    aget-object v11, v11, v4

    #@25
    const/4 v12, 0x3

    #@26
    aget-object v11, v11, v12

    #@28
    check-cast v11, Ljava/lang/Integer;

    #@2a
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@2d
    move-result v11

    #@2e
    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@31
    move-result v8

    #@32
    .line 235
    .local v8, supported:Z
    invoke-static {v4, v8}, Lcom/broadcom/bt/service/ProfileConfig;->overrideProfileSetting(IZ)Z

    #@35
    move-result v8

    #@36
    .line 237
    if-eqz v8, :cond_d3

    #@38
    .line 238
    sget-object v11, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@3a
    aget-object v11, v11, v4

    #@3c
    const/4 v12, 0x0

    #@3d
    aget-object v5, v11, v12

    #@3f
    check-cast v5, Ljava/lang/Class;

    #@41
    .line 239
    .local v5, profile:Ljava/lang/Class;
    const-string v11, "BtSettings.ProfileConfig"

    #@43
    new-instance v12, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v13, "Adding "

    #@4a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v12

    #@4e
    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@51
    move-result-object v13

    #@52
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v12

    #@56
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v12

    #@5a
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 240
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@60
    .line 242
    new-instance v0, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;

    #@62
    invoke-direct {v0}, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;-><init>()V

    #@65
    .line 243
    .local v0, cfg:Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@68
    move-result-object v11

    #@69
    iput-object v11, v0, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;->mName:Ljava/lang/String;

    #@6b
    .line 245
    const/4 v3, 0x0

    #@6c
    .line 247
    .local v3, displayName:Ljava/lang/String;
    :try_start_6c
    sget-object v11, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@6e
    aget-object v11, v11, v4

    #@70
    const/4 v12, 0x1

    #@71
    aget-object v11, v11, v12

    #@73
    check-cast v11, Ljava/lang/Integer;

    #@75
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@78
    move-result v11

    #@79
    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_7c
    .catch Ljava/lang/Throwable; {:try_start_6c .. :try_end_7c} :catch_d7

    #@7c
    move-result-object v3

    #@7d
    .line 251
    :goto_7d
    if-nez v3, :cond_83

    #@7f
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@82
    move-result-object v3

    #@83
    .end local v3           #displayName:Ljava/lang/String;
    :cond_83
    iput-object v3, v0, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;->mDisplayName:Ljava/lang/String;

    #@85
    .line 255
    const/4 v2, 0x0

    #@86
    .line 257
    .local v2, description:Ljava/lang/String;
    :try_start_86
    sget-object v11, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@88
    aget-object v11, v11, v4

    #@8a
    const/4 v12, 0x2

    #@8b
    aget-object v11, v11, v12

    #@8d
    check-cast v11, Ljava/lang/Integer;

    #@8f
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@92
    move-result v11

    #@93
    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_96
    .catch Ljava/lang/Throwable; {:try_start_86 .. :try_end_96} :catch_e0

    #@96
    move-result-object v2

    #@97
    .line 262
    :goto_97
    if-nez v2, :cond_9d

    #@99
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@9c
    move-result-object v2

    #@9d
    .end local v2           #description:Ljava/lang/String;
    :cond_9d
    iput-object v2, v0, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;->mDescription:Ljava/lang/String;

    #@9f
    .line 266
    sget-object v11, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@a1
    aget-object v11, v11, v4

    #@a3
    const/4 v12, 0x4

    #@a4
    aget-object v11, v11, v12

    #@a6
    check-cast v11, Ljava/lang/Integer;

    #@a8
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@ab
    move-result v11

    #@ac
    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@af
    move-result v11

    #@b0
    iput-boolean v11, v0, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;->mDefaultStarted:Z

    #@b2
    .line 268
    sget-object v11, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@b4
    aget-object v11, v11, v4

    #@b6
    const/4 v12, 0x5

    #@b7
    aget-object v11, v11, v12

    #@b9
    check-cast v11, Ljava/lang/Integer;

    #@bb
    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    #@be
    move-result v11

    #@bf
    invoke-virtual {v7, v11}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c2
    move-result v11

    #@c3
    iput-boolean v11, v0, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;->mUserConfigurable:Z

    #@c5
    .line 272
    sget-object v11, Lcom/broadcom/bt/service/ProfileConfig;->PROFILE_SERVICES:[[Ljava/lang/Object;

    #@c7
    aget-object v11, v11, v4

    #@c9
    const/4 v12, 0x6

    #@ca
    aget-object v11, v11, v12

    #@cc
    check-cast v11, Ljava/lang/String;

    #@ce
    iput-object v11, v0, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;->mSettingsActivityPkgClassName:Ljava/lang/String;

    #@d0
    .line 274
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@d3
    .line 232
    .end local v0           #cfg:Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;
    .end local v5           #profile:Ljava/lang/Class;
    :cond_d3
    add-int/lit8 v4, v4, 0x1

    #@d5
    goto/16 :goto_1c

    #@d7
    .line 248
    .restart local v0       #cfg:Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;
    .restart local v3       #displayName:Ljava/lang/String;
    .restart local v5       #profile:Ljava/lang/Class;
    :catch_d7
    move-exception v9

    #@d8
    .line 249
    .local v9, t:Ljava/lang/Throwable;
    const-string v11, "BtSettings.ProfileConfig"

    #@da
    const-string v12, "Profile display name not found"

    #@dc
    invoke-static {v11, v12, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@df
    goto :goto_7d

    #@e0
    .line 259
    .end local v3           #displayName:Ljava/lang/String;
    .end local v9           #t:Ljava/lang/Throwable;
    .restart local v2       #description:Ljava/lang/String;
    :catch_e0
    move-exception v9

    #@e1
    .line 260
    .restart local v9       #t:Ljava/lang/Throwable;
    const-string v11, "BtSettings.ProfileConfig"

    #@e3
    const-string v12, "Profile display name not found"

    #@e5
    invoke-static {v11, v12, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e8
    goto :goto_97

    #@e9
    .line 277
    .end local v0           #cfg:Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;
    .end local v2           #description:Ljava/lang/String;
    .end local v5           #profile:Ljava/lang/Class;
    .end local v8           #supported:Z
    .end local v9           #t:Ljava/lang/Throwable;
    :cond_e9
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@ec
    move-result v10

    #@ed
    .line 278
    .local v10, totalProfiles:I
    new-array v11, v10, [Ljava/lang/Class;

    #@ef
    sput-object v11, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES:[Ljava/lang/Class;

    #@f1
    .line 279
    new-array v11, v10, [Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;

    #@f3
    sput-object v11, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES_CFG:[Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;

    #@f5
    .line 280
    const/4 v4, 0x0

    #@f6
    :goto_f6
    if-ge v4, v10, :cond_10f

    #@f8
    .line 281
    sget-object v12, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES:[Ljava/lang/Class;

    #@fa
    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@fd
    move-result-object v11

    #@fe
    check-cast v11, Ljava/lang/Class;

    #@100
    aput-object v11, v12, v4

    #@102
    .line 282
    sget-object v12, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES_CFG:[Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;

    #@104
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@107
    move-result-object v11

    #@108
    check-cast v11, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;

    #@10a
    aput-object v11, v12, v4

    #@10c
    .line 280
    add-int/lit8 v4, v4, 0x1

    #@10e
    goto :goto_f6

    #@10f
    .line 285
    :cond_10f
    invoke-static {}, Lcom/broadcom/bt/service/ProfileConfig;->initSettings()V

    #@112
    goto/16 :goto_2
.end method

.method private static initSettings()V
    .registers 8

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 205
    sget-object v4, Lcom/broadcom/bt/service/ProfileConfig;->mContext:Landroid/content/Context;

    #@4
    if-eqz v4, :cond_51

    #@6
    .line 206
    sget-object v4, Lcom/broadcom/bt/service/ProfileConfig;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v1

    #@c
    .line 207
    .local v1, cr:Landroid/content/ContentResolver;
    const-string v4, "bt_svcst_init"

    #@e
    invoke-static {v1, v4, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@11
    move-result v4

    #@12
    if-eqz v4, :cond_48

    #@14
    move v3, v5

    #@15
    .line 208
    .local v3, settingsExist:Z
    :goto_15
    if-nez v3, :cond_51

    #@17
    .line 209
    const-string v4, "BtSettings.ProfileConfig"

    #@19
    const-string v7, "*********Initializing Bluetooth Profile Settings*******"

    #@1b
    invoke-static {v4, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 210
    const/4 v2, 0x0

    #@1f
    .local v2, i:I
    :goto_1f
    sget-object v4, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES_CFG:[Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;

    #@21
    array-length v4, v4

    #@22
    if-ge v2, v4, :cond_4c

    #@24
    .line 211
    sget-object v4, Lcom/broadcom/bt/service/ProfileConfig;->SUPPORTED_PROFILES_CFG:[Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;

    #@26
    aget-object v0, v4, v2

    #@28
    .line 212
    .local v0, cfg:Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v7, "bt_svcst_"

    #@2f
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v4

    #@33
    iget-object v7, v0, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;->mName:Ljava/lang/String;

    #@35
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v7

    #@3d
    iget-boolean v4, v0, Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;->mDefaultStarted:Z

    #@3f
    if-eqz v4, :cond_4a

    #@41
    move v4, v5

    #@42
    :goto_42
    invoke-static {v1, v7, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@45
    .line 210
    add-int/lit8 v2, v2, 0x1

    #@47
    goto :goto_1f

    #@48
    .end local v0           #cfg:Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;
    .end local v2           #i:I
    .end local v3           #settingsExist:Z
    :cond_48
    move v3, v6

    #@49
    .line 207
    goto :goto_15

    #@4a
    .restart local v0       #cfg:Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;
    .restart local v2       #i:I
    .restart local v3       #settingsExist:Z
    :cond_4a
    move v4, v6

    #@4b
    .line 212
    goto :goto_42

    #@4c
    .line 215
    .end local v0           #cfg:Lcom/broadcom/bt/service/ProfileConfig$ProfileCfg;
    :cond_4c
    const-string v4, "bt_svcst_init"

    #@4e
    invoke-static {v1, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@51
    .line 218
    .end local v2           #i:I
    :cond_51
    return-void
.end method

.method public static isProfileConfiguredEnabled(Ljava/lang/String;)Z
    .registers 9
    .parameter "profileName"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 331
    invoke-static {p0}, Lcom/broadcom/bt/service/ProfileConfig;->findProfileIndex(Ljava/lang/String;)I

    #@5
    move-result v1

    #@6
    .line 332
    .local v1, profileIndex:I
    if-gez v1, :cond_9

    #@8
    .line 345
    :cond_8
    :goto_8
    return v4

    #@9
    .line 336
    :cond_9
    sget-object v5, Lcom/broadcom/bt/service/ProfileConfig;->mContext:Landroid/content/Context;

    #@b
    if-eqz v5, :cond_8

    #@d
    .line 337
    sget-object v5, Lcom/broadcom/bt/service/ProfileConfig;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12
    move-result-object v0

    #@13
    .line 339
    .local v0, cr:Landroid/content/ContentResolver;
    :try_start_13
    const-string v5, "BtSettings.ProfileConfig"

    #@15
    new-instance v6, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v7, "getProfileSaveSetting: "

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v6

    #@28
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 340
    new-instance v5, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v6, "bt_svcst_"

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v5

    #@3e
    invoke-static {v0, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;)I
    :try_end_41
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_41} :catch_48

    #@41
    move-result v5

    #@42
    if-ne v5, v3, :cond_46

    #@44
    :goto_44
    move v4, v3

    #@45
    goto :goto_8

    #@46
    :cond_46
    move v3, v4

    #@47
    goto :goto_44

    #@48
    .line 341
    :catch_48
    move-exception v2

    #@49
    .line 342
    .local v2, t:Ljava/lang/Throwable;
    const-string v3, "BtSettings.ProfileConfig"

    #@4b
    const-string v5, "Unable to read settings"

    #@4d
    invoke-static {v3, v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@50
    goto :goto_8
.end method

.method private static overrideProfileSetting(IZ)Z
    .registers 5
    .parameter "index"
    .parameter "oldValue"

    #@0
    .prologue
    .line 351
    move v0, p1

    #@1
    .line 353
    .local v0, supported:Z
    const-string v1, "PanService"

    #@3
    invoke-static {p0, v1}, Lcom/broadcom/bt/service/ProfileConfig;->checkProfile(ILjava/lang/String;)Z

    #@6
    move-result v1

    #@7
    if-eqz v1, :cond_39

    #@9
    .line 354
    new-instance v1, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v2, "[BTUI] ProfileConfig : PAN (b) = "

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v1}, Lcom/broadcom/bt/service/ProfileConfig;->BtUiLog(Ljava/lang/String;)V

    #@1f
    .line 355
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isSupportPan()Z

    #@22
    move-result v0

    #@23
    .line 356
    new-instance v1, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v2, "[BTUI] ProfileConfig : PAN (a) = "

    #@2a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v1

    #@36
    invoke-static {v1}, Lcom/broadcom/bt/service/ProfileConfig;->BtUiLog(Ljava/lang/String;)V

    #@39
    .line 359
    :cond_39
    const-string v1, "SapService"

    #@3b
    invoke-static {p0, v1}, Lcom/broadcom/bt/service/ProfileConfig;->checkProfile(ILjava/lang/String;)Z

    #@3e
    move-result v1

    #@3f
    if-eqz v1, :cond_71

    #@41
    .line 360
    new-instance v1, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v2, "[BTUI] ProfileConfig : SAP (b) = "

    #@48
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v1

    #@4c
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v1

    #@50
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v1

    #@54
    invoke-static {v1}, Lcom/broadcom/bt/service/ProfileConfig;->BtUiLog(Ljava/lang/String;)V

    #@57
    .line 361
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isSupportSap()Z

    #@5a
    move-result v0

    #@5b
    .line 362
    new-instance v1, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v2, "[BTUI] ProfileConfig : SAP (a) = "

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@69
    move-result-object v1

    #@6a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v1

    #@6e
    invoke-static {v1}, Lcom/broadcom/bt/service/ProfileConfig;->BtUiLog(Ljava/lang/String;)V

    #@71
    .line 365
    :cond_71
    const-string v1, "MapService"

    #@73
    invoke-static {p0, v1}, Lcom/broadcom/bt/service/ProfileConfig;->checkProfile(ILjava/lang/String;)Z

    #@76
    move-result v1

    #@77
    if-eqz v1, :cond_a9

    #@79
    .line 366
    new-instance v1, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v2, "[BTUI] ProfileConfig : MAP (b) = "

    #@80
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v1

    #@84
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@87
    move-result-object v1

    #@88
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v1

    #@8c
    invoke-static {v1}, Lcom/broadcom/bt/service/ProfileConfig;->BtUiLog(Ljava/lang/String;)V

    #@8f
    .line 367
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isSupportMap()Z

    #@92
    move-result v0

    #@93
    .line 368
    new-instance v1, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v2, "[BTUI] ProfileConfig : MAP (a) = "

    #@9a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v1

    #@9e
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v1

    #@a2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a5
    move-result-object v1

    #@a6
    invoke-static {v1}, Lcom/broadcom/bt/service/ProfileConfig;->BtUiLog(Ljava/lang/String;)V

    #@a9
    .line 370
    :cond_a9
    return v0
.end method

.method public static saveProfileSetting(Ljava/lang/String;Z)V
    .registers 7
    .parameter "profileName"
    .parameter "enabled"

    #@0
    .prologue
    .line 318
    sget-object v2, Lcom/broadcom/bt/service/ProfileConfig;->mContext:Landroid/content/Context;

    #@2
    if-eqz v2, :cond_28

    #@4
    .line 319
    sget-object v2, Lcom/broadcom/bt/service/ProfileConfig;->mContext:Landroid/content/Context;

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v0

    #@a
    .line 320
    .local v0, cr:Landroid/content/ContentResolver;
    invoke-static {p0}, Lcom/broadcom/bt/service/ProfileConfig;->findProfileIndex(Ljava/lang/String;)I

    #@d
    move-result v1

    #@e
    .line 321
    .local v1, profileIndex:I
    if-gez v1, :cond_29

    #@10
    .line 322
    const-string v2, "BtSettings.ProfileConfig"

    #@12
    new-instance v3, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v4, "Profile not supported: "

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 328
    .end local v0           #cr:Landroid/content/ContentResolver;
    .end local v1           #profileIndex:I
    :cond_28
    :goto_28
    return-void

    #@29
    .line 326
    .restart local v0       #cr:Landroid/content/ContentResolver;
    .restart local v1       #profileIndex:I
    :cond_29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "bt_svcst_"

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    if-eqz p1, :cond_43

    #@3e
    const/4 v2, 0x1

    #@3f
    :goto_3f
    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@42
    goto :goto_28

    #@43
    :cond_43
    const/4 v2, 0x0

    #@44
    goto :goto_3f
.end method


# virtual methods
.method public isProfileSupported(Ljava/lang/String;)Z
    .registers 3
    .parameter "profileName"

    #@0
    .prologue
    .line 307
    invoke-static {p1}, Lcom/broadcom/bt/service/ProfileConfig;->findProfileIndex(Ljava/lang/String;)I

    #@3
    move-result v0

    #@4
    if-ltz v0, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method
