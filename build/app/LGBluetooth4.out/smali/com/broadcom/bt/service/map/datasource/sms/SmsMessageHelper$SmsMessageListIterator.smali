.class public Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;
.super Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;
.source "SmsMessageHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SmsMessageListIterator"
.end annotation


# instance fields
.field private mAddressColumn:I

.field private mContactIdColumn:I

.field private mMessageDateTimeColumn:I

.field private mReadDbColumn:I

.field private mThreadIdColumn:I

.field final synthetic this$0:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;


# direct methods
.method public constructor <init>(Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1015
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;

    #@2
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource$MessageListIterator;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method protected contactMatchesFilter()Z
    .registers 15

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 1135
    iget-object v11, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;

    #@4
    #getter for: Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;
    invoke-static {v11}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$200(Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;)Landroid/content/Context;

    #@7
    move-result-object v11

    #@8
    invoke-virtual {v11}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v6

    #@c
    .line 1136
    .local v6, resolver:Landroid/content/ContentResolver;
    const-string v11, "draft"

    #@e
    iget-object v12, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@10
    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@13
    move-result v11

    #@14
    if-eqz v11, :cond_5a

    #@16
    .line 1137
    iget-object v11, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@18
    iget v12, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mThreadIdColumn:I

    #@1a
    const/4 v13, -0x1

    #@1b
    invoke-static {v11, v12, v13}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@1e
    move-result v8

    #@1f
    .line 1138
    .local v8, threadId:I
    if-lez v8, :cond_58

    #@21
    .line 1139
    #calls: Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getThreadRecipientAddresses(Landroid/content/ContentResolver;I)Ljava/util/List;
    invoke-static {v6, v8}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$500(Landroid/content/ContentResolver;I)Ljava/util/List;

    #@24
    move-result-object v1

    #@25
    .line 1140
    .local v1, addresses:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v1, :cond_58

    #@27
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@2a
    move-result v11

    #@2b
    if-lez v11, :cond_58

    #@2d
    .line 1141
    const/4 v3, 0x0

    #@2e
    .local v3, i:I
    :goto_2e
    invoke-interface {v1}, Ljava/util/List;->size()I

    #@31
    move-result v11

    #@32
    if-ge v3, v11, :cond_58

    #@34
    .line 1142
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@37
    move-result-object v0

    #@38
    check-cast v0, Ljava/lang/String;

    #@3a
    .line 1144
    .local v0, address:Ljava/lang/String;
    const/4 v7, 0x0

    #@3b
    .line 1146
    .local v7, tempaddr:Ljava/lang/String;
    const-string v11, "+"

    #@3d
    invoke-virtual {v0, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@40
    move-result v11

    #@41
    if-eqz v11, :cond_47

    #@43
    .line 1151
    invoke-virtual {v0, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@46
    move-result-object v7

    #@47
    .line 1158
    :cond_47
    const/4 v11, 0x0

    #@48
    #calls: Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getPersonInfo(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;
    invoke-static {v6, v11, v7}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$600(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;

    #@4b
    move-result-object v4

    #@4c
    .line 1160
    .local v4, pInfo:Lcom/broadcom/bt/map/PersonInfo;
    iget-object v11, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mContactFilter:Ljava/lang/String;

    #@4e
    invoke-static {v4, v11}, Lcom/broadcom/bt/map/ContactsUtil;->contactMatchesFilter(Lcom/broadcom/bt/map/PersonInfo;Ljava/lang/String;)Z

    #@51
    move-result v11

    #@52
    if-eqz v11, :cond_55

    #@54
    .line 1186
    .end local v0           #address:Ljava/lang/String;
    .end local v1           #addresses:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v3           #i:I
    .end local v4           #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    .end local v7           #tempaddr:Ljava/lang/String;
    .end local v8           #threadId:I
    :goto_54
    return v9

    #@55
    .line 1141
    .restart local v0       #address:Ljava/lang/String;
    .restart local v1       #addresses:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .restart local v3       #i:I
    .restart local v4       #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    .restart local v7       #tempaddr:Ljava/lang/String;
    .restart local v8       #threadId:I
    :cond_55
    add-int/lit8 v3, v3, 0x1

    #@57
    goto :goto_2e

    #@58
    .end local v0           #address:Ljava/lang/String;
    .end local v1           #addresses:Ljava/util/List;,"Ljava/util/List<Ljava/lang/String;>;"
    .end local v3           #i:I
    .end local v4           #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    .end local v7           #tempaddr:Ljava/lang/String;
    :cond_58
    move v9, v10

    #@59
    .line 1166
    goto :goto_54

    #@5a
    .line 1168
    .end local v8           #threadId:I
    :cond_5a
    iget-object v11, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@5c
    iget v12, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mContactIdColumn:I

    #@5e
    invoke-interface {v11, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@61
    move-result-object v2

    #@62
    .line 1169
    .local v2, contactId:Ljava/lang/String;
    iget-object v11, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@64
    iget v12, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mAddressColumn:I

    #@66
    invoke-interface {v11, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v5

    #@6a
    .line 1171
    .local v5, phoneNumber:Ljava/lang/String;
    const/4 v7, 0x0

    #@6b
    .line 1172
    .restart local v7       #tempaddr:Ljava/lang/String;
    const-string v11, "+"

    #@6d
    invoke-virtual {v5, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@70
    move-result v11

    #@71
    if-eqz v11, :cond_77

    #@73
    .line 1173
    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@76
    move-result-object v7

    #@77
    .line 1180
    :cond_77
    #calls: Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->getPersonInfo(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;
    invoke-static {v6, v2, v7}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$600(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Lcom/broadcom/bt/map/PersonInfo;

    #@7a
    move-result-object v4

    #@7b
    .line 1182
    .restart local v4       #pInfo:Lcom/broadcom/bt/map/PersonInfo;
    if-nez v4, :cond_86

    #@7d
    .line 1183
    const-string v9, "BtMap.SmsMessageHelper"

    #@7f
    const-string v11, "Person not found. return false"

    #@81
    invoke-static {v9, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    move v9, v10

    #@85
    .line 1184
    goto :goto_54

    #@86
    .line 1186
    :cond_86
    iget-object v9, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mContactFilter:Ljava/lang/String;

    #@88
    invoke-static {v4, v9}, Lcom/broadcom/bt/map/ContactsUtil;->contactMatchesFilter(Lcom/broadcom/bt/map/PersonInfo;Ljava/lang/String;)Z

    #@8b
    move-result v9

    #@8c
    goto :goto_54
.end method

.method public getMessageDateTime()J
    .registers 3

    #@0
    .prologue
    .line 1120
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@2
    if-eqz v0, :cond_d

    #@4
    .line 1121
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@6
    iget v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mMessageDateTimeColumn:I

    #@8
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    #@b
    move-result-wide v0

    #@c
    .line 1123
    :goto_c
    return-wide v0

    #@d
    :cond_d
    const-wide/16 v0, 0x0

    #@f
    goto :goto_c
.end method

.method public getMessageInfo()Lcom/broadcom/bt/map/MessageInfo;
    .registers 9

    #@0
    .prologue
    .line 1115
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;

    #@2
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@4
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@6
    iget v3, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mPhoneType:I

    #@8
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mOwnerPhoneNumber:Ljava/lang/String;

    #@a
    iget-boolean v5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mIncludeMessageSize:Z

    #@c
    iget v6, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mMaxSubjectLength:I

    #@e
    iget-object v7, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mParams:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@10
    invoke-virtual/range {v0 .. v7}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toMessageInfo(Ljava/lang/String;Landroid/database/Cursor;ILjava/lang/String;ZILcom/broadcom/bt/map/MessageParameterFilter;)Lcom/broadcom/bt/map/MessageInfo;

    #@13
    move-result-object v0

    #@14
    return-object v0
.end method

.method protected getReadStatus()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1128
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@4
    if-eqz v2, :cond_13

    #@6
    .line 1129
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mCursor:Landroid/database/Cursor;

    #@8
    iget v3, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mReadDbColumn:I

    #@a
    invoke-static {v2, v3, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_11

    #@10
    .line 1131
    :goto_10
    return v0

    #@11
    :cond_11
    move v0, v1

    #@12
    .line 1129
    goto :goto_10

    #@13
    :cond_13
    move v0, v1

    #@14
    .line 1131
    goto :goto_10
.end method

.method public initCountIterator(Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Ljava/lang/String;Lcom/broadcom/bt/map/MessageParameterFilter;)V
    .registers 14
    .parameter "folderPath"
    .parameter "messageListFilter"
    .parameter "contactFilter"
    .parameter "params"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1024
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@3
    .line 1025
    iput-object p2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@5
    .line 1026
    iput-object p3, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mContactFilter:Ljava/lang/String;

    #@7
    .line 1027
    iput-object p4, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mParams:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@9
    .line 1028
    const/4 v0, 0x3

    #@a
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mContactIdColumn:I

    #@c
    .line 1029
    const/4 v0, 0x5

    #@d
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mThreadIdColumn:I

    #@f
    .line 1030
    const/4 v0, 0x4

    #@10
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mAddressColumn:I

    #@12
    .line 1031
    const/4 v0, 0x1

    #@13
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mReadDbColumn:I

    #@15
    .line 1032
    const/4 v0, 0x2

    #@16
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mMessageDateTimeColumn:I

    #@18
    .line 1034
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;

    #@1a
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@1c
    invoke-virtual {v0, v2}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toMessageType(Ljava/lang/String;)I

    #@1f
    move-result v6

    #@20
    .line 1035
    .local v6, smsFolderType:I
    if-gez v6, :cond_23

    #@22
    .line 1061
    :goto_22
    return-void

    #@23
    .line 1039
    :cond_23
    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@25
    .line 1040
    .local v1, uri:Landroid/net/Uri;
    new-instance v8, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    .line 1041
    .local v8, where:Ljava/lang/StringBuilder;
    const-string v0, "type"

    #@2c
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    .line 1042
    const-string v0, "="

    #@31
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    .line 1043
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    .line 1045
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@39
    #calls: Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->createMessageListSelector(Ljava/lang/StringBuilder;Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;
    invoke-static {v8, v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$000(Ljava/lang/StringBuilder;Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;

    #@3c
    .line 1050
    :try_start_3c
    const-string v0, "BtMap.SmsMessageHelper"

    #@3e
    new-instance v2, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v4, "getMessageListCountCursor(): Querying "

    #@45
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v2

    #@51
    const-string v4, " with filter "

    #@53
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v4

    #@5b
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v2

    #@5f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v2

    #@63
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 1053
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;

    #@68
    #getter for: Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$200(Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;)Landroid/content/Context;

    #@6b
    move-result-object v0

    #@6c
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6f
    move-result-object v0

    #@70
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$100()[Ljava/lang/String;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->length()I

    #@77
    move-result v4

    #@78
    if-lez v4, :cond_7e

    #@7a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v3

    #@7e
    :cond_7e
    const/4 v4, 0x0

    #@7f
    const-string v5, "_id desc "

    #@81
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@84
    move-result-object v0

    #@85
    invoke-static {v0}, Lcom/broadcom/bt/util/DBUtil;->getNonEmptyCursorOrClose(Landroid/database/Cursor;)Landroid/database/Cursor;

    #@88
    move-result-object v0

    #@89
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mCursor:Landroid/database/Cursor;
    :try_end_8b
    .catch Ljava/lang/Throwable; {:try_start_3c .. :try_end_8b} :catch_8c

    #@8b
    goto :goto_22

    #@8c
    .line 1056
    :catch_8c
    move-exception v7

    #@8d
    .line 1057
    .local v7, t:Ljava/lang/Throwable;
    const-string v0, "BtMap.SmsMessageHelper"

    #@8f
    const-string v2, "onGetMsgListing(): Unable to query for SMS messages"

    #@91
    invoke-static {v0, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@94
    goto :goto_22
.end method

.method public initMessageInfoIterator(Ljava/lang/String;Lcom/broadcom/bt/map/MessageListFilter;Ljava/lang/String;Lcom/broadcom/bt/map/MessageParameterFilter;ZILjava/lang/String;I)V
    .registers 19
    .parameter "folderPath"
    .parameter "messageListFilter"
    .parameter "contactFilter"
    .parameter "params"
    .parameter "includeMessageSize"
    .parameter "phoneType"
    .parameter "ownerNumber"
    .parameter "maxSubjectLength"

    #@0
    .prologue
    .line 1066
    const-string v1, "BtMap.SmsMessageHelper"

    #@2
    const-string v4, "initMessageInfoIterator()"

    #@4
    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1067
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@9
    .line 1068
    iput-object p2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@b
    .line 1069
    iput-object p3, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mContactFilter:Ljava/lang/String;

    #@d
    .line 1070
    iput-object p4, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mParams:Lcom/broadcom/bt/map/MessageParameterFilter;

    #@f
    .line 1071
    iput-boolean p5, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mIncludeMessageSize:Z

    #@11
    .line 1072
    move/from16 v0, p6

    #@13
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mPhoneType:I

    #@15
    .line 1073
    move-object/from16 v0, p7

    #@17
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mOwnerPhoneNumber:Ljava/lang/String;

    #@19
    .line 1074
    move/from16 v0, p8

    #@1b
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mMaxSubjectLength:I

    #@1d
    .line 1075
    const/4 v1, 0x4

    #@1e
    iput v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mContactIdColumn:I

    #@20
    .line 1076
    const/16 v1, 0x8

    #@22
    iput v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mThreadIdColumn:I

    #@24
    .line 1077
    const/4 v1, 0x1

    #@25
    iput v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mAddressColumn:I

    #@27
    .line 1078
    const/4 v1, 0x5

    #@28
    iput v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mReadDbColumn:I

    #@2a
    .line 1079
    const/4 v1, 0x2

    #@2b
    iput v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mMessageDateTimeColumn:I

    #@2d
    .line 1081
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;

    #@2f
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mFolderPath:Ljava/lang/String;

    #@31
    invoke-virtual {v1, v4}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->toMessageType(Ljava/lang/String;)I

    #@34
    move-result v7

    #@35
    .line 1082
    .local v7, smsFolderType:I
    if-gez v7, :cond_38

    #@37
    .line 1112
    :goto_37
    return-void

    #@38
    .line 1085
    :cond_38
    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@3a
    .line 1086
    .local v2, uri:Landroid/net/Uri;
    new-instance v9, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    .line 1087
    .local v9, where:Ljava/lang/StringBuilder;
    const-string v1, "type"

    #@41
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    .line 1088
    const-string v1, "="

    #@46
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    .line 1089
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    .line 1091
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mMessageListFilter:Lcom/broadcom/bt/map/MessageListFilter;

    #@4e
    #calls: Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->createMessageListSelector(Ljava/lang/StringBuilder;Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;
    invoke-static {v9, v1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$000(Ljava/lang/StringBuilder;Lcom/broadcom/bt/map/MessageListFilter;)Ljava/lang/StringBuilder;

    #@51
    .line 1095
    if-eqz p5, :cond_ac

    #@53
    :try_start_53
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$300()[Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    .line 1101
    .local v3, projections:[Ljava/lang/String;
    :goto_57
    const-string v1, "BtMap.SmsMessageHelper"

    #@59
    new-instance v4, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v5, "getMessageListCursor(): Querying "

    #@60
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v4

    #@64
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@67
    move-result-object v5

    #@68
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v4

    #@6c
    const-string v5, " with filter "

    #@6e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v4

    #@72
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v4

    #@7e
    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 1104
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->this$0:Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;

    #@83
    #getter for: Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$200(Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;)Landroid/content/Context;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8a
    move-result-object v1

    #@8b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    #@8e
    move-result v4

    #@8f
    if-lez v4, :cond_b1

    #@91
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v4

    #@95
    :goto_95
    const/4 v5, 0x0

    #@96
    const-string v6, "_id desc "

    #@98
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@9b
    move-result-object v1

    #@9c
    invoke-static {v1}, Lcom/broadcom/bt/util/DBUtil;->getNonEmptyCursorOrClose(Landroid/database/Cursor;)Landroid/database/Cursor;

    #@9f
    move-result-object v1

    #@a0
    iput-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper$SmsMessageListIterator;->mCursor:Landroid/database/Cursor;
    :try_end_a2
    .catch Ljava/lang/Throwable; {:try_start_53 .. :try_end_a2} :catch_a3

    #@a2
    goto :goto_37

    #@a3
    .line 1107
    .end local v3           #projections:[Ljava/lang/String;
    :catch_a3
    move-exception v8

    #@a4
    .line 1108
    .local v8, t:Ljava/lang/Throwable;
    const-string v1, "BtMap.SmsMessageHelper"

    #@a6
    const-string v4, "getMessageListCursor(): Unable to query for SMS messages"

    #@a8
    invoke-static {v1, v4, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@ab
    goto :goto_37

    #@ac
    .line 1095
    .end local v8           #t:Ljava/lang/Throwable;
    :cond_ac
    :try_start_ac
    invoke-static {}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageHelper;->access$400()[Ljava/lang/String;
    :try_end_af
    .catch Ljava/lang/Throwable; {:try_start_ac .. :try_end_af} :catch_a3

    #@af
    move-result-object v3

    #@b0
    goto :goto_57

    #@b1
    .line 1104
    .restart local v3       #projections:[Ljava/lang/String;
    :cond_b1
    const/4 v4, 0x0

    #@b2
    goto :goto_95
.end method
