.class public Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;
.super Landroid/database/ContentObserver;
.source "MmsEventManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    }
.end annotation


# static fields
.field private static final COL_ID:I = 0x0

.field private static final COL_MCE_CREATED:I = 0x4

.field private static final COL_MCE_DELETED:I = 0x5

.field private static final COL_MSG_DATE:I = 0x1

.field private static final COL_MSG_TYPE:I = 0x2

.field private static final COL_OLD_MSG_TYPE:I = 0x6

.field private static final COL_THREAD_ID:I = 0x3

.field private static final DBG:Z = true

.field private static final DB_CREATE:Ljava/lang/String; = "CREATE TABLE IF NOT EXISTS mms (_id INTEGER PRIMARY KEY, date INTEGER, msg_box INTEGER, thread_id INTEGER DEFAULT -1,  mce_created INTEGER DEFAULT 0, mce_deleted INTEGER DEFAULT 0, old_msg_type INTEGER DEFAULT -1)"

.field private static final DB_NAME:Ljava/lang/String; = "MmsTempStore"

.field private static final MMS_PROJ:[Ljava/lang/String; = null

.field private static final MMS_SMS_CONVERSATIONS:Landroid/net/Uri; = null

.field private static final MMS_TMP_PROJ:[Ljava/lang/String; = null

.field private static final TABLE_NAME:Ljava/lang/String; = "mms"

.field private static final TAG:Ljava/lang/String; = "BtMap.MmsEventManager"

.field private static final URL_MATCHER:Landroid/content/UriMatcher; = null

.field static final URL_PROCESS_ALL_CONVERSATIONS:I = 0x1

.field static final URL_PROCESS_DELETE_CONVERSATION:I = 0x2

.field static final URL_PROCESS_MMS:I = 0x3


# instance fields
.field private final mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mDb:Landroid/database/sqlite/SQLiteDatabase;

.field private mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

.field private mIntentFilter:Landroid/content/IntentFilter;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v5, 0x3

    #@3
    const/4 v4, 0x2

    #@4
    const/4 v3, 0x1

    #@5
    .line 87
    new-array v0, v6, [Ljava/lang/String;

    #@7
    const-string v1, "_id"

    #@9
    aput-object v1, v0, v2

    #@b
    const-string v1, "date"

    #@d
    aput-object v1, v0, v3

    #@f
    const-string v1, "msg_box"

    #@11
    aput-object v1, v0, v4

    #@13
    const-string v1, "thread_id"

    #@15
    aput-object v1, v0, v5

    #@17
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_PROJ:[Ljava/lang/String;

    #@19
    .line 88
    const/4 v0, 0x7

    #@1a
    new-array v0, v0, [Ljava/lang/String;

    #@1c
    const-string v1, "_id"

    #@1e
    aput-object v1, v0, v2

    #@20
    const-string v1, "date"

    #@22
    aput-object v1, v0, v3

    #@24
    const-string v1, "msg_box"

    #@26
    aput-object v1, v0, v4

    #@28
    const-string v1, "thread_id"

    #@2a
    aput-object v1, v0, v5

    #@2c
    const-string v1, "mce_created"

    #@2e
    aput-object v1, v0, v6

    #@30
    const/4 v1, 0x5

    #@31
    const-string v2, "mce_deleted"

    #@33
    aput-object v2, v0, v1

    #@35
    const/4 v1, 0x6

    #@36
    const-string v2, "old_msg_type"

    #@38
    aput-object v2, v0, v1

    #@3a
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_TMP_PROJ:[Ljava/lang/String;

    #@3c
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@43
    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v0

    #@4b
    const-string v1, "/"

    #@4d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v0

    #@51
    const-string v1, "conversations?simple=true"

    #@53
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5e
    move-result-object v0

    #@5f
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_SMS_CONVERSATIONS:Landroid/net/Uri;

    #@61
    .line 105
    new-instance v0, Landroid/content/UriMatcher;

    #@63
    const/4 v1, -0x1

    #@64
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@67
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@69
    .line 108
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@6b
    const-string v1, "mms-sms"

    #@6d
    const/4 v2, 0x0

    #@6e
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@71
    .line 109
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@73
    const-string v1, "mms-sms"

    #@75
    const-string v2, "conversations/deleted"

    #@77
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@7a
    .line 110
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@7c
    const-string v1, "mms-sms"

    #@7e
    const-string v2, "conversations/deleted/#"

    #@80
    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@83
    .line 111
    sget-object v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@85
    const-string v1, "mms"

    #@87
    const-string v2, "#"

    #@89
    invoke-virtual {v0, v1, v2, v5}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@8c
    .line 112
    return-void
.end method

.method public constructor <init>(Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;Landroid/content/Context;Landroid/os/Handler;)V
    .registers 5
    .parameter "ds"
    .parameter "ctx"
    .parameter "handler"

    #@0
    .prologue
    .line 138
    invoke-direct {p0, p3}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@3
    .line 441
    new-instance v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;

    #@5
    invoke-direct {v0, p0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;-><init>(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;)V

    #@8
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@a
    .line 139
    iput-object p2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mContext:Landroid/content/Context;

    #@c
    .line 140
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@e
    .line 141
    return-void
.end method

.method static synthetic access$100(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1, p2, p3}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->processMessageChange(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;Ljava/lang/String;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 82
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->getFromTmpStore(Ljava/lang/String;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private declared-synchronized addOrUpdateToTmpStore(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)Z
    .registers 16
    .parameter "messageId"
    .parameter "info"

    #@0
    .prologue
    .line 348
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtMap.MmsEventManager"

    #@3
    const-string v1, "addOrUpdateToTmpStore"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 351
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@a
    if-eqz v0, :cond_2e

    #@c
    .line 352
    const/4 v0, 0x1

    #@d
    new-array v4, v0, [Ljava/lang/String;

    #@f
    const/4 v0, 0x0

    #@10
    aput-object p1, v4, v0
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_46

    #@12
    .line 353
    .local v4, msgIdArg:[Ljava/lang/String;
    const/4 v11, 0x0

    #@13
    .line 355
    .local v11, c:Landroid/database/Cursor;
    :try_start_13
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@15
    const-string v1, "mms"

    #@17
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_TMP_PROJ:[Ljava/lang/String;

    #@19
    const-string v3, "_id=?"

    #@1b
    const/4 v5, 0x0

    #@1c
    const/4 v6, 0x0

    #@1d
    const/4 v7, 0x0

    #@1e
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@21
    move-result-object v11

    #@22
    .line 356
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    #@25
    move-result v0

    #@26
    if-eqz v0, :cond_31

    #@28
    .line 357
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->updateTmpStore(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)Z
    :try_end_2b
    .catchall {:try_start_13 .. :try_end_2b} :catchall_46
    .catch Ljava/lang/Throwable; {:try_start_13 .. :try_end_2b} :catch_3d

    #@2b
    .line 364
    :goto_2b
    :try_start_2b
    invoke-static {v11}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_2e
    .catchall {:try_start_2b .. :try_end_2e} :catchall_46

    #@2e
    .line 366
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v11           #c:Landroid/database/Cursor;
    :cond_2e
    const/4 v0, 0x1

    #@2f
    monitor-exit p0

    #@30
    return v0

    #@31
    .line 359
    .restart local v4       #msgIdArg:[Ljava/lang/String;
    .restart local v11       #c:Landroid/database/Cursor;
    :cond_31
    :try_start_31
    iget-wide v7, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageDate:J

    #@33
    iget v9, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@35
    iget v10, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mThreadId:I

    #@37
    move-object v5, p0

    #@38
    move-object v6, p1

    #@39
    invoke-direct/range {v5 .. v10}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->addToTmpStore(Ljava/lang/String;JII)V
    :try_end_3c
    .catchall {:try_start_31 .. :try_end_3c} :catchall_46
    .catch Ljava/lang/Throwable; {:try_start_31 .. :try_end_3c} :catch_3d

    #@3c
    goto :goto_2b

    #@3d
    .line 361
    :catch_3d
    move-exception v12

    #@3e
    .line 362
    .local v12, t:Ljava/lang/Throwable;
    :try_start_3e
    const-string v0, "BtMap.MmsEventManager"

    #@40
    const-string v1, "addOrUpdateToTmpStore: error"

    #@42
    invoke-static {v0, v1, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_45
    .catchall {:try_start_3e .. :try_end_45} :catchall_46

    #@45
    goto :goto_2b

    #@46
    .line 348
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v11           #c:Landroid/database/Cursor;
    .end local v12           #t:Ljava/lang/Throwable;
    :catchall_46
    move-exception v0

    #@47
    monitor-exit p0

    #@48
    throw v0
.end method

.method private declared-synchronized addToTmpStore(Ljava/lang/String;JII)V
    .registers 11
    .parameter "messageId"
    .parameter "messageDateTime"
    .parameter "messageType"
    .parameter "threadId"

    #@0
    .prologue
    .line 269
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_3e

    #@3
    if-eqz v2, :cond_33

    #@5
    .line 271
    :try_start_5
    new-instance v1, Landroid/content/ContentValues;

    #@7
    const/4 v2, 0x4

    #@8
    invoke-direct {v1, v2}, Landroid/content/ContentValues;-><init>(I)V

    #@b
    .line 272
    .local v1, v:Landroid/content/ContentValues;
    const-string v2, "_id"

    #@d
    invoke-virtual {v1, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@10
    .line 273
    const-string v2, "date"

    #@12
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@19
    .line 274
    const-string v2, "msg_box"

    #@1b
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@22
    .line 275
    const-string v2, "thread_id"

    #@24
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2b
    .line 276
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@2d
    const-string v3, "mms"

    #@2f
    const/4 v4, 0x0

    #@30
    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_33
    .catchall {:try_start_5 .. :try_end_33} :catchall_3e
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_33} :catch_35

    #@33
    .line 281
    .end local v1           #v:Landroid/content/ContentValues;
    :cond_33
    :goto_33
    monitor-exit p0

    #@34
    return-void

    #@35
    .line 277
    :catch_35
    move-exception v0

    #@36
    .line 278
    .local v0, t:Ljava/lang/Throwable;
    :try_start_36
    const-string v2, "BtMap.MmsEventManager"

    #@38
    const-string v3, "addToTmpStore(): error"

    #@3a
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3d
    .catchall {:try_start_36 .. :try_end_3d} :catchall_3e

    #@3d
    goto :goto_33

    #@3e
    .line 269
    .end local v0           #t:Ljava/lang/Throwable;
    :catchall_3e
    move-exception v2

    #@3f
    monitor-exit p0

    #@40
    throw v2
.end method

.method private declared-synchronized cleanupTmpStore(Z)V
    .registers 6
    .parameter "close"

    #@0
    .prologue
    .line 166
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@3
    if-eqz v0, :cond_15

    #@5
    .line 167
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@7
    const-string v1, "mms"

    #@9
    const/4 v2, 0x0

    #@a
    const/4 v3, 0x0

    #@b
    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@e
    .line 168
    if-eqz p1, :cond_15

    #@10
    .line 169
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@12
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_15
    .catchall {:try_start_1 .. :try_end_15} :catchall_17

    #@15
    .line 172
    :cond_15
    monitor-exit p0

    #@16
    return-void

    #@17
    .line 166
    :catchall_17
    move-exception v0

    #@18
    monitor-exit p0

    #@19
    throw v0
.end method

.method private getFromMmsProvider(Landroid/net/Uri;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .registers 12
    .parameter "uri"

    #@0
    .prologue
    .line 370
    const/4 v7, 0x0

    #@1
    .line 371
    .local v7, mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@3
    if-eqz v0, :cond_48

    #@5
    .line 372
    const/4 v6, 0x0

    #@6
    .line 374
    .local v6, c:Landroid/database/Cursor;
    :try_start_6
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@8
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_PROJ:[Ljava/lang/String;

    #@a
    const/4 v3, 0x0

    #@b
    const/4 v4, 0x0

    #@c
    const-string v5, "_id ASC"

    #@e
    move-object v1, p1

    #@f
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@12
    move-result-object v6

    #@13
    .line 376
    if-eqz v6, :cond_43

    #@15
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@18
    move-result v0

    #@19
    if-eqz v0, :cond_43

    #@1b
    .line 377
    new-instance v8, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;

    #@1d
    const/4 v0, 0x0

    #@1e
    invoke-direct {v8, v0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;-><init>(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;)V
    :try_end_21
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_21} :catch_49

    #@21
    .line 378
    .end local v7           #mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .local v8, mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    const/4 v0, 0x2

    #@22
    const/4 v1, -0x1

    #@23
    :try_start_23
    invoke-static {v6, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@26
    move-result v0

    #@27
    iput v0, v8, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@29
    .line 379
    iget v0, v8, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@2b
    invoke-static {v0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    iput-object v0, v8, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@31
    .line 380
    const/4 v0, 0x1

    #@32
    const-wide/16 v1, 0x0

    #@34
    invoke-static {v6, v0, v1, v2}, Lcom/broadcom/bt/util/DBUtil;->getLong(Landroid/database/Cursor;IJ)J

    #@37
    move-result-wide v0

    #@38
    iput-wide v0, v8, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageDate:J

    #@3a
    .line 381
    const/4 v0, 0x3

    #@3b
    const/4 v1, -0x1

    #@3c
    invoke-static {v6, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@3f
    move-result v0

    #@40
    iput v0, v8, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mThreadId:I
    :try_end_42
    .catch Ljava/lang/Throwable; {:try_start_23 .. :try_end_42} :catch_52

    #@42
    move-object v7, v8

    #@43
    .line 386
    .end local v8           #mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .restart local v7       #mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    :cond_43
    :goto_43
    if-eqz v6, :cond_48

    #@45
    .line 387
    invoke-static {v6}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@48
    .line 391
    .end local v6           #c:Landroid/database/Cursor;
    :cond_48
    return-object v7

    #@49
    .line 383
    .restart local v6       #c:Landroid/database/Cursor;
    :catch_49
    move-exception v9

    #@4a
    .line 384
    .local v9, t:Ljava/lang/Throwable;
    :goto_4a
    const-string v0, "BtMap.MmsEventManager"

    #@4c
    const-string v1, "addToTmpStore(): error"

    #@4e
    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@51
    goto :goto_43

    #@52
    .line 383
    .end local v7           #mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .end local v9           #t:Ljava/lang/Throwable;
    .restart local v8       #mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    :catch_52
    move-exception v9

    #@53
    move-object v7, v8

    #@54
    .end local v8           #mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .restart local v7       #mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    goto :goto_4a
.end method

.method private declared-synchronized getFromTmpStore(Ljava/lang/String;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .registers 13
    .parameter "messageId"

    #@0
    .prologue
    .line 296
    monitor-enter p0

    #@1
    const/4 v9, 0x0

    #@2
    .line 297
    .local v9, mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    :try_start_2
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_4
    .catchall {:try_start_2 .. :try_end_4} :catchall_34

    #@4
    if-eqz v0, :cond_29

    #@6
    .line 298
    const/4 v8, 0x0

    #@7
    .line 300
    .local v8, c:Landroid/database/Cursor;
    :try_start_7
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@9
    const-string v1, "mms"

    #@b
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_TMP_PROJ:[Ljava/lang/String;

    #@d
    const-string v3, "_id=?"

    #@f
    const/4 v4, 0x1

    #@10
    new-array v4, v4, [Ljava/lang/String;

    #@12
    const/4 v5, 0x0

    #@13
    aput-object p1, v4, v5

    #@15
    const/4 v5, 0x0

    #@16
    const/4 v6, 0x0

    #@17
    const/4 v7, 0x0

    #@18
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1b
    move-result-object v8

    #@1c
    .line 302
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@1f
    move-result v0

    #@20
    if-eqz v0, :cond_26

    #@22
    .line 303
    invoke-direct {p0, v8}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->toTmpSmsMsgInfo(Landroid/database/Cursor;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    :try_end_25
    .catchall {:try_start_7 .. :try_end_25} :catchall_34
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_25} :catch_2b

    #@25
    move-result-object v9

    #@26
    .line 308
    :cond_26
    :goto_26
    :try_start_26
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_29
    .catchall {:try_start_26 .. :try_end_29} :catchall_34

    #@29
    .line 310
    .end local v8           #c:Landroid/database/Cursor;
    :cond_29
    monitor-exit p0

    #@2a
    return-object v9

    #@2b
    .line 305
    .restart local v8       #c:Landroid/database/Cursor;
    :catch_2b
    move-exception v10

    #@2c
    .line 306
    .local v10, t:Ljava/lang/Throwable;
    :try_start_2c
    const-string v0, "BtMap.MmsEventManager"

    #@2e
    const-string v1, "getFromTmpStore(): error"

    #@30
    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_33
    .catchall {:try_start_2c .. :try_end_33} :catchall_34

    #@33
    goto :goto_26

    #@34
    .line 296
    .end local v8           #c:Landroid/database/Cursor;
    .end local v10           #t:Ljava/lang/Throwable;
    :catchall_34
    move-exception v0

    #@35
    monitor-exit p0

    #@36
    throw v0
.end method

.method private getMapMessageType()B
    .registers 2

    #@0
    .prologue
    .line 514
    const/16 v0, 0x8

    #@2
    return v0
.end method

.method private declared-synchronized initTmpStore()V
    .registers 8

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 144
    monitor-enter p0

    #@2
    :try_start_2
    invoke-static {}, Lcom/broadcom/bt/service/map/MapService;->getDefaultTmpDir()Ljava/io/File;

    #@5
    move-result-object v1

    #@6
    .line 145
    .local v1, dbDirectory:Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    #@9
    move-result v4

    #@a
    if-nez v4, :cond_30

    #@c
    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    #@f
    move-result v4

    #@10
    if-nez v4, :cond_30

    #@12
    .line 146
    const-string v4, "BtMap.MmsEventManager"

    #@14
    new-instance v5, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v6, "Cannot create tmpstore directory: "

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@22
    move-result-object v6

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2e
    .catchall {:try_start_2 .. :try_end_2e} :catchall_62

    #@2e
    .line 163
    :goto_2e
    monitor-exit p0

    #@2f
    return-void

    #@30
    .line 151
    :cond_30
    :try_start_30
    new-instance v2, Ljava/io/File;

    #@32
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    const-string v5, "MmsTempStore"

    #@38
    invoke-direct {v2, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 152
    .local v2, dbFile:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@3e
    move-result v4

    #@3f
    if-nez v4, :cond_42

    #@41
    const/4 v0, 0x1

    #@42
    .line 153
    .local v0, createNew:Z
    :cond_42
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    const/4 v5, 0x0

    #@47
    const/high16 v6, 0x1000

    #@49
    invoke-static {v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    #@4c
    move-result-object v4

    #@4d
    iput-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@4f
    .line 155
    if-eqz v0, :cond_65

    #@51
    .line 156
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@53
    const-string v5, "CREATE TABLE IF NOT EXISTS mms (_id INTEGER PRIMARY KEY, date INTEGER, msg_box INTEGER, thread_id INTEGER DEFAULT -1,  mce_created INTEGER DEFAULT 0, mce_deleted INTEGER DEFAULT 0, old_msg_type INTEGER DEFAULT -1)"

    #@55
    invoke-virtual {v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_58
    .catchall {:try_start_30 .. :try_end_58} :catchall_62
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_30 .. :try_end_58} :catch_59

    #@58
    goto :goto_2e

    #@59
    .line 160
    .end local v0           #createNew:Z
    .end local v2           #dbFile:Ljava/io/File;
    :catch_59
    move-exception v3

    #@5a
    .line 161
    .local v3, e:Landroid/database/sqlite/SQLiteException;
    :try_start_5a
    const-string v4, "BtMap.MmsEventManager"

    #@5c
    const-string v5, "Error creating tmpstore"

    #@5e
    invoke-static {v4, v5, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_61
    .catchall {:try_start_5a .. :try_end_61} :catchall_62

    #@61
    goto :goto_2e

    #@62
    .line 144
    .end local v1           #dbDirectory:Ljava/io/File;
    .end local v3           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_62
    move-exception v4

    #@63
    monitor-exit p0

    #@64
    throw v4

    #@65
    .line 158
    .restart local v0       #createNew:Z
    .restart local v1       #dbDirectory:Ljava/io/File;
    .restart local v2       #dbFile:Ljava/io/File;
    :cond_65
    const/4 v4, 0x0

    #@66
    :try_start_66
    invoke-direct {p0, v4}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->cleanupTmpStore(Z)V
    :try_end_69
    .catchall {:try_start_66 .. :try_end_69} :catchall_62
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_66 .. :try_end_69} :catch_59

    #@69
    goto :goto_2e
.end method

.method private processAllMessageChanges()V
    .registers 16

    #@0
    .prologue
    .line 649
    const-string v0, "BtMap.MmsEventManager"

    #@2
    const-string v1, "processAllMessageChanges()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 651
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->getMapMessageType()B

    #@a
    move-result v11

    #@b
    .line 653
    .local v11, mapMsgType:B
    const/4 v8, 0x0

    #@c
    .line 654
    .local v8, c:Landroid/database/Cursor;
    new-instance v10, Landroid/util/SparseBooleanArray;

    #@e
    invoke-direct {v10}, Landroid/util/SparseBooleanArray;-><init>()V

    #@11
    .line 656
    .local v10, mThreadIds:Landroid/util/SparseBooleanArray;
    :try_start_11
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@13
    sget-object v1, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_SMS_CONVERSATIONS:Landroid/net/Uri;

    #@15
    const/4 v2, 0x1

    #@16
    new-array v2, v2, [Ljava/lang/String;

    #@18
    const/4 v3, 0x0

    #@19
    const-string v4, "_id"

    #@1b
    aput-object v4, v2, v3

    #@1d
    const/4 v3, 0x0

    #@1e
    const/4 v4, 0x0

    #@1f
    const-string v5, "_id asc "

    #@21
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@24
    move-result-object v8

    #@25
    .line 658
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@28
    move-result v0

    #@29
    if-eqz v0, :cond_53

    #@2b
    .line 660
    :cond_2b
    const/4 v0, 0x0

    #@2c
    const/4 v1, -0x1

    #@2d
    invoke-static {v8, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@30
    move-result v14

    #@31
    .line 661
    .local v14, threadId:I
    const-string v0, "BtMap.MmsEventManager"

    #@33
    new-instance v1, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v2, "adding thread id to cache: "

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v1

    #@46
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 662
    const/4 v0, 0x1

    #@4a
    invoke-virtual {v10, v14, v0}, Landroid/util/SparseBooleanArray;->append(IZ)V

    #@4d
    .line 663
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_50
    .catch Ljava/lang/Throwable; {:try_start_11 .. :try_end_50} :catch_c8

    #@50
    move-result v0

    #@51
    if-nez v0, :cond_2b

    #@53
    .line 671
    .end local v14           #threadId:I
    :cond_53
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@56
    .line 673
    :try_start_56
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@58
    const-string v1, "mms"

    #@5a
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_TMP_PROJ:[Ljava/lang/String;

    #@5c
    const/4 v3, 0x0

    #@5d
    const/4 v4, 0x0

    #@5e
    const/4 v5, 0x0

    #@5f
    const/4 v6, 0x0

    #@60
    const-string v7, "_id ASC"

    #@62
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@65
    move-result-object v8

    #@66
    .line 674
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@69
    move-result v0

    #@6a
    if-eqz v0, :cond_c4

    #@6c
    .line 676
    :cond_6c
    const/4 v0, 0x3

    #@6d
    const/4 v1, -0x1

    #@6e
    invoke-static {v8, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@71
    move-result v14

    #@72
    .line 677
    .restart local v14       #threadId:I
    if-lez v14, :cond_be

    #@74
    invoke-virtual {v10, v14}, Landroid/util/SparseBooleanArray;->get(I)Z

    #@77
    move-result v0

    #@78
    if-nez v0, :cond_be

    #@7a
    .line 678
    const-string v0, "BtMap.MmsEventManager"

    #@7c
    new-instance v1, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v2, "Removing message with threadId "

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v1

    #@8b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v1

    #@8f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 681
    invoke-direct {p0, v8}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->toTmpSmsMsgInfo(Landroid/database/Cursor;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;

    #@95
    move-result-object v9

    #@96
    .line 682
    .local v9, mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    iget v0, v9, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageId:I

    #@98
    if-lez v0, :cond_be

    #@9a
    iget-object v0, v9, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@9c
    if-eqz v0, :cond_be

    #@9e
    .line 683
    iget v0, v9, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageId:I

    #@a0
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@a3
    move-result-object v12

    #@a4
    .line 684
    .local v12, msgId:Ljava/lang/String;
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@a6
    invoke-static {v12}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->encodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@a9
    move-result-object v1

    #@aa
    iget-object v2, v9, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@ac
    invoke-virtual {v0, v1, v11, v2}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageDeleted(Ljava/lang/String;BLjava/lang/String;)V

    #@af
    .line 686
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@b1
    const-string v1, "mms"

    #@b3
    const-string v2, "_id=?"

    #@b5
    const/4 v3, 0x1

    #@b6
    new-array v3, v3, [Ljava/lang/String;

    #@b8
    const/4 v4, 0x0

    #@b9
    aput-object v12, v3, v4

    #@bb
    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@be
    .line 689
    .end local v9           #mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .end local v12           #msgId:Ljava/lang/String;
    :cond_be
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_c1
    .catch Ljava/lang/Throwable; {:try_start_56 .. :try_end_c1} :catch_d4

    #@c1
    move-result v0

    #@c2
    if-nez v0, :cond_6c

    #@c4
    .line 694
    .end local v14           #threadId:I
    :cond_c4
    :goto_c4
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@c7
    .line 695
    :goto_c7
    return-void

    #@c8
    .line 665
    :catch_c8
    move-exception v13

    #@c9
    .line 666
    .local v13, t:Ljava/lang/Throwable;
    const-string v0, "BtMap.MmsEventManager"

    #@cb
    const-string v1, "processAllMessageChanges(): error with query. Exiting..."

    #@cd
    invoke-static {v0, v1, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d0
    .line 667
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@d3
    goto :goto_c7

    #@d4
    .line 691
    .end local v13           #t:Ljava/lang/Throwable;
    :catch_d4
    move-exception v13

    #@d5
    .line 692
    .restart local v13       #t:Ljava/lang/Throwable;
    const-string v0, "BtMap.MmsEventManager"

    #@d7
    const-string v1, "Error cleaning..."

    #@d9
    invoke-static {v0, v1, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@dc
    goto :goto_c4
.end method

.method private processDeleteConversation(Ljava/lang/String;)V
    .registers 18
    .parameter "conversationId"

    #@0
    .prologue
    .line 699
    const/4 v9, 0x0

    #@1
    .line 700
    .local v9, c:Landroid/database/Cursor;
    const/4 v11, 0x0

    #@2
    .line 702
    .local v11, hasConversation:Z
    :try_start_2
    move-object/from16 v0, p0

    #@4
    iget-object v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@6
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_SMS_CONVERSATIONS:Landroid/net/Uri;

    #@8
    const/4 v3, 0x1

    #@9
    new-array v3, v3, [Ljava/lang/String;

    #@b
    const/4 v4, 0x0

    #@c
    const-string v5, "_id"

    #@e
    aput-object v5, v3, v4

    #@10
    const-string v4, "_id = ?"

    #@12
    const/4 v5, 0x1

    #@13
    new-array v5, v5, [Ljava/lang/String;

    #@15
    const/4 v6, 0x0

    #@16
    aput-object p1, v5, v6

    #@18
    const/4 v6, 0x0

    #@19
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1c
    move-result-object v9

    #@1d
    .line 704
    invoke-static {v9}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z
    :try_end_20
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_20} :catch_2e

    #@20
    move-result v11

    #@21
    .line 708
    :goto_21
    invoke-static {v9}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@24
    .line 709
    if-eqz v11, :cond_37

    #@26
    .line 710
    const-string v1, "BtMap.MmsEventManager"

    #@28
    const-string v2, "processDeleteConversation(): conversation still exists...Ignoring delete request..."

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 743
    :goto_2d
    return-void

    #@2e
    .line 705
    :catch_2e
    move-exception v15

    #@2f
    .line 706
    .local v15, t:Ljava/lang/Throwable;
    const-string v1, "BtMap.MmsEventManager"

    #@31
    const-string v2, "processDeleteConversation(): error with query. Exiting..."

    #@33
    invoke-static {v1, v2, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@36
    goto :goto_21

    #@37
    .line 715
    .end local v15           #t:Ljava/lang/Throwable;
    :cond_37
    const-string v1, "BtMap.MmsEventManager"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "processDeleteConversation(): deleting conversation "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    move-object/from16 v0, p1

    #@46
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 718
    invoke-direct/range {p0 .. p0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->getMapMessageType()B

    #@54
    move-result v13

    #@55
    .line 720
    .local v13, mapMsgType:B
    :try_start_55
    move-object/from16 v0, p0

    #@57
    iget-object v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@59
    const-string v2, "mms"

    #@5b
    sget-object v3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_TMP_PROJ:[Ljava/lang/String;

    #@5d
    const-string v4, "thread_id=?"

    #@5f
    const/4 v5, 0x1

    #@60
    new-array v5, v5, [Ljava/lang/String;

    #@62
    const/4 v6, 0x0

    #@63
    aput-object p1, v5, v6

    #@65
    const/4 v6, 0x0

    #@66
    const/4 v7, 0x0

    #@67
    const-string v8, "_id ASC"

    #@69
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@6c
    move-result-object v9

    #@6d
    .line 722
    invoke-static {v9}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@70
    move-result v1

    #@71
    if-eqz v1, :cond_c6

    #@73
    .line 725
    :cond_73
    move-object/from16 v0, p0

    #@75
    invoke-direct {v0, v9}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->toTmpSmsMsgInfo(Landroid/database/Cursor;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;

    #@78
    move-result-object v12

    #@79
    .line 726
    .local v12, mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    iget v1, v12, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageId:I

    #@7b
    if-lez v1, :cond_c0

    #@7d
    iget-object v1, v12, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@7f
    if-eqz v1, :cond_c0

    #@81
    .line 727
    iget v1, v12, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageId:I

    #@83
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    #@86
    move-result-object v14

    #@87
    .line 729
    .local v14, msgId:Ljava/lang/String;
    const-string v1, "BtMap.MmsEventManager"

    #@89
    new-instance v2, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v3, "Deleting message "

    #@90
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v2

    #@94
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v2

    #@98
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v2

    #@9c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    .line 731
    move-object/from16 v0, p0

    #@a1
    iget-object v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@a3
    const-string v2, "mms"

    #@a5
    const-string v3, "_id=?"

    #@a7
    const/4 v4, 0x1

    #@a8
    new-array v4, v4, [Ljava/lang/String;

    #@aa
    const/4 v5, 0x0

    #@ab
    aput-object v14, v4, v5

    #@ad
    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    #@b0
    move-result v10

    #@b1
    .line 732
    .local v10, deleted:I
    if-lez v10, :cond_c0

    #@b3
    .line 733
    move-object/from16 v0, p0

    #@b5
    iget-object v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@b7
    invoke-static {v14}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->encodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@ba
    move-result-object v2

    #@bb
    iget-object v3, v12, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@bd
    invoke-virtual {v1, v2, v13, v3}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageDeleted(Ljava/lang/String;BLjava/lang/String;)V

    #@c0
    .line 737
    .end local v10           #deleted:I
    .end local v14           #msgId:Ljava/lang/String;
    :cond_c0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_c3
    .catch Ljava/lang/Throwable; {:try_start_55 .. :try_end_c3} :catch_cb

    #@c3
    move-result v1

    #@c4
    if-nez v1, :cond_73

    #@c6
    .line 742
    .end local v12           #mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    :cond_c6
    :goto_c6
    invoke-static {v9}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@c9
    goto/16 :goto_2d

    #@cb
    .line 739
    :catch_cb
    move-exception v15

    #@cc
    .line 740
    .restart local v15       #t:Ljava/lang/Throwable;
    const-string v1, "BtMap.MmsEventManager"

    #@ce
    const-string v2, "Error deleting conversation..."

    #@d0
    invoke-static {v1, v2, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@d3
    goto :goto_c6
.end method

.method private processMessageChange(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)V
    .registers 16
    .parameter "messageId"
    .parameter "msgInfo"
    .parameter "oldMsgInfo"

    #@0
    .prologue
    const/4 v11, 0x4

    #@1
    const/4 v7, 0x0

    #@2
    const/4 v10, 0x2

    #@3
    const/4 v6, 0x1

    #@4
    .line 519
    const-string v5, "BtMap.MmsEventManager"

    #@6
    new-instance v8, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v9, "processMessageChange: messageId="

    #@d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v8

    #@11
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v8

    #@15
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v8

    #@19
    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 520
    const-string v5, "BtMap.MmsEventManager"

    #@1e
    new-instance v8, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v9, "processMessageChange: msgInfo="

    #@25
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v8

    #@29
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v8

    #@31
    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 521
    const-string v8, "BtMap.MmsEventManager"

    #@36
    new-instance v5, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v9, "processMessageChange: msgInfo.messageType="

    #@3d
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v9

    #@41
    if-nez p2, :cond_b7

    #@43
    const-string v5, "unknown"

    #@45
    :goto_45
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v5

    #@49
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v5

    #@4d
    invoke-static {v8, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 523
    const-string v5, "BtMap.MmsEventManager"

    #@52
    new-instance v8, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v9, "processMessageChange: oldMsgInfo="

    #@59
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v8

    #@5d
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v8

    #@61
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v8

    #@65
    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    .line 524
    const-string v8, "BtMap.MmsEventManager"

    #@6a
    new-instance v5, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v9, "processMessageChange: oldMsgInfo.messageType="

    #@71
    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v9

    #@75
    if-nez p3, :cond_be

    #@77
    const-string v5, "unknown"

    #@79
    :goto_79
    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v5

    #@7d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v5

    #@81
    invoke-static {v8, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 527
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->getMapMessageType()B

    #@87
    move-result v1

    #@88
    .line 528
    .local v1, mapMsgType:B
    invoke-static {p1}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->encodeMessageId(Ljava/lang/String;)Ljava/lang/String;

    #@8b
    move-result-object v0

    #@8c
    .line 530
    .local v0, encodedMessageId:Ljava/lang/String;
    if-nez p2, :cond_cd

    #@8e
    .line 532
    const-string v5, "BtMap.MmsEventManager"

    #@90
    const-string v7, "Message deleted!"

    #@92
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 535
    invoke-direct {p0, p1}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->removeFromTmpStore(Ljava/lang/String;)Z

    #@98
    .line 539
    if-eqz p3, :cond_c5

    #@9a
    iget-object v5, p3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@9c
    if-eqz v5, :cond_c5

    #@9e
    .line 540
    iget v5, p3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMceDeleted:I

    #@a0
    if-eq v5, v6, :cond_b6

    #@a2
    .line 542
    const-string v5, "BtMap.MmsEventManager"

    #@a4
    const-string v6, "Delete was not requested from MCE...Sending notification..."

    #@a6
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 544
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@ab
    iget-object v6, p3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@ad
    invoke-virtual {v5, v0, v1, v6}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageDeleted(Ljava/lang/String;BLjava/lang/String;)V

    #@b0
    .line 545
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@b2
    const/4 v6, 0x0

    #@b3
    invoke-virtual {v5, v0, v6}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->updateMessageId(Ljava/lang/String;Ljava/lang/String;)V

    #@b6
    .line 645
    :cond_b6
    :goto_b6
    return-void

    #@b7
    .line 521
    .end local v0           #encodedMessageId:Ljava/lang/String;
    .end local v1           #mapMsgType:B
    :cond_b7
    iget v5, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@b9
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@bc
    move-result-object v5

    #@bd
    goto :goto_45

    #@be
    .line 524
    :cond_be
    iget v5, p3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@c0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c3
    move-result-object v5

    #@c4
    goto :goto_79

    #@c5
    .line 548
    .restart local v0       #encodedMessageId:Ljava/lang/String;
    .restart local v1       #mapMsgType:B
    :cond_c5
    const-string v5, "BtMap.MmsEventManager"

    #@c7
    const-string v6, "Not sending message deleted notification: folderPath not set"

    #@c9
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@cc
    goto :goto_b6

    #@cd
    .line 554
    :cond_cd
    if-eqz p3, :cond_d3

    #@cf
    iget v5, p3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMceCreated:I

    #@d1
    if-ne v5, v6, :cond_148

    #@d3
    .line 556
    :cond_d3
    const-string v5, "BtMap.MmsEventManager"

    #@d5
    const-string v8, "New message!"

    #@d7
    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@da
    .line 558
    if-eqz p3, :cond_ed

    #@dc
    iget v5, p3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMceCreated:I

    #@de
    if-ne v5, v6, :cond_ed

    #@e0
    move v2, v6

    #@e1
    .line 559
    .local v2, mceCreated:Z
    :goto_e1
    iget-object v5, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@e3
    if-nez v5, :cond_ef

    #@e5
    .line 560
    const-string v5, "BtMap.MmsEventManager"

    #@e7
    const-string v6, "Ignoring message created by MCE..."

    #@e9
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ec
    goto :goto_b6

    #@ed
    .end local v2           #mceCreated:Z
    :cond_ed
    move v2, v7

    #@ee
    .line 558
    goto :goto_e1

    #@ef
    .line 564
    .restart local v2       #mceCreated:Z
    :cond_ef
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->addOrUpdateToTmpStore(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)Z

    #@f2
    .line 566
    invoke-virtual {p0, p1, v7}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->setMessageCreatedByMce(Ljava/lang/String;Z)Z

    #@f5
    .line 571
    iget v5, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@f7
    if-ne v5, v6, :cond_103

    #@f9
    .line 572
    if-nez v2, :cond_b6

    #@fb
    .line 573
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@fd
    iget-object v6, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@ff
    invoke-virtual {v5, v0, v1, v6}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyNewMessage(Ljava/lang/String;BLjava/lang/String;)V

    #@102
    goto :goto_b6

    #@103
    .line 588
    :cond_103
    iget v5, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@105
    if-ne v5, v10, :cond_11c

    #@107
    .line 592
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@109
    iget-object v7, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@10b
    invoke-static {v11}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@10e
    move-result-object v8

    #@10f
    invoke-virtual {v5, v0, v1, v7, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageMoved(Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;)V

    #@112
    .line 594
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@114
    invoke-static {v10}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@117
    move-result-object v7

    #@118
    invoke-virtual {v5, v0, v1, v7, v6}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifySendStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@11b
    goto :goto_b6

    #@11c
    .line 598
    :cond_11c
    const-string v5, "BtMap.MmsEventManager"

    #@11e
    new-instance v6, Ljava/lang/StringBuilder;

    #@120
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@123
    const-string v7, "Ignoring new message type "

    #@125
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v6

    #@129
    iget v7, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@12b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12e
    move-result-object v6

    #@12f
    const-string v7, ", folder "

    #@131
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v6

    #@135
    iget v7, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@137
    invoke-static {v7}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@13a
    move-result-object v7

    #@13b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v6

    #@13f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@142
    move-result-object v6

    #@143
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    goto/16 :goto_b6

    #@148
    .line 607
    .end local v2           #mceCreated:Z
    :cond_148
    iget v5, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@14a
    iget v7, p3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@14c
    if-eq v5, v7, :cond_b6

    #@14e
    .line 608
    iget v5, p3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@150
    invoke-static {v5}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@153
    move-result-object v4

    #@154
    .line 609
    .local v4, oldFolderPath:Ljava/lang/String;
    iget v5, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@156
    invoke-static {v5}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@159
    move-result-object v3

    #@15a
    .line 611
    .local v3, newFolderPath:Ljava/lang/String;
    const-string v5, "BtMap.MmsEventManager"

    #@15c
    new-instance v7, Ljava/lang/StringBuilder;

    #@15e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@161
    const-string v8, "Message shifted from "

    #@163
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v7

    #@167
    if-nez v4, :cond_16b

    #@169
    const-string v4, "(null)"

    #@16b
    .end local v4           #oldFolderPath:Ljava/lang/String;
    :cond_16b
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v7

    #@16f
    const-string v8, " to "

    #@171
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@174
    move-result-object v7

    #@175
    if-nez v3, :cond_179

    #@177
    const-string v3, "(null)"

    #@179
    .end local v3           #newFolderPath:Ljava/lang/String;
    :cond_179
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v7

    #@17d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@180
    move-result-object v7

    #@181
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@184
    .line 616
    invoke-direct {p0, p1, p2}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->updateTmpStore(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)Z

    #@187
    .line 621
    iget v5, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@189
    if-ne v5, v10, :cond_1aa

    #@18b
    .line 622
    const-string v5, "BtMap.MmsEventManager"

    #@18d
    const-string v7, "Send succeeded"

    #@18f
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@192
    .line 623
    iget v5, p3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@194
    if-ne v5, v11, :cond_19f

    #@196
    .line 624
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@198
    iget-object v7, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@19a
    iget-object v8, p3, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@19c
    invoke-virtual {v5, v0, v1, v7, v8}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifyMessageMoved(Ljava/lang/String;BLjava/lang/String;Ljava/lang/String;)V

    #@19f
    .line 627
    :cond_19f
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDs:Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;

    #@1a1
    invoke-static {v10}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@1a4
    move-result-object v7

    #@1a5
    invoke-virtual {v5, v0, v1, v7, v6}, Lcom/broadcom/bt/service/map/datasource/SmsMmsDataSource;->notifySendStateChanged(Ljava/lang/String;BLjava/lang/String;Z)V

    #@1a8
    goto/16 :goto_b6

    #@1aa
    .line 641
    :cond_1aa
    const-string v5, "BtMap.MmsEventManager"

    #@1ac
    const-string v6, "Ignoring unused message shift..."

    #@1ae
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b1
    goto/16 :goto_b6
.end method

.method private declared-synchronized removeFromTmpStore(Ljava/lang/String;)Z
    .registers 10
    .parameter "messageId"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 314
    monitor-enter p0

    #@3
    :try_start_3
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_5
    .catchall {:try_start_3 .. :try_end_5} :catchall_27

    #@5
    if-eqz v3, :cond_25

    #@7
    .line 316
    :try_start_7
    iget-object v3, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@9
    const-string v4, "mms"

    #@b
    const-string v5, "_id=?"

    #@d
    const/4 v6, 0x1

    #@e
    new-array v6, v6, [Ljava/lang/String;

    #@10
    const/4 v7, 0x0

    #@11
    aput-object p1, v6, v7

    #@13
    invoke-virtual {v3, v4, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_16
    .catchall {:try_start_7 .. :try_end_16} :catchall_27
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_16} :catch_1d

    #@16
    move-result v3

    #@17
    if-lez v3, :cond_1b

    #@19
    .line 321
    :goto_19
    monitor-exit p0

    #@1a
    return v1

    #@1b
    :cond_1b
    move v1, v2

    #@1c
    .line 316
    goto :goto_19

    #@1d
    .line 317
    :catch_1d
    move-exception v0

    #@1e
    .line 318
    .local v0, t:Ljava/lang/Throwable;
    :try_start_1e
    const-string v1, "BtMap.MmsEventManager"

    #@20
    const-string v3, "removeFromTempStore: error"

    #@22
    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_25
    .catchall {:try_start_1e .. :try_end_25} :catchall_27

    #@25
    .end local v0           #t:Ljava/lang/Throwable;
    :cond_25
    move v1, v2

    #@26
    .line 321
    goto :goto_19

    #@27
    .line 314
    :catchall_27
    move-exception v1

    #@28
    monitor-exit p0

    #@29
    throw v1
.end method

.method private toTmpSmsMsgInfo(Landroid/database/Cursor;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .registers 7
    .parameter "c"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 284
    new-instance v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;

    #@4
    const/4 v1, 0x0

    #@5
    invoke-direct {v0, v1}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;-><init>(Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$1;)V

    #@8
    .line 285
    .local v0, mInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    invoke-static {p1, v4, v2}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@b
    move-result v1

    #@c
    iput v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageId:I

    #@e
    .line 286
    const/4 v1, 0x2

    #@f
    invoke-static {p1, v1, v2}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@12
    move-result v1

    #@13
    iput v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@15
    .line 287
    iget v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@17
    invoke-static {v1}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toFolderPath(I)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    iput-object v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mFolderPath:Ljava/lang/String;

    #@1d
    .line 288
    const/4 v1, 0x1

    #@1e
    const-wide/16 v2, 0x0

    #@20
    invoke-static {p1, v1, v2, v3}, Lcom/broadcom/bt/util/DBUtil;->getLong(Landroid/database/Cursor;IJ)J

    #@23
    move-result-wide v1

    #@24
    iput-wide v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageDate:J

    #@26
    .line 289
    const/4 v1, 0x4

    #@27
    invoke-static {p1, v1, v4}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@2a
    move-result v1

    #@2b
    iput v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMceCreated:I

    #@2d
    .line 290
    const/4 v1, 0x5

    #@2e
    invoke-static {p1, v1, v4}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@31
    move-result v1

    #@32
    iput v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMceDeleted:I

    #@34
    .line 291
    const/4 v1, 0x3

    #@35
    invoke-static {p1, v1, v4}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@38
    move-result v1

    #@39
    iput v1, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mThreadId:I

    #@3b
    .line 292
    return-object v0
.end method

.method private declared-synchronized updateTmpStore(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)Z
    .registers 11
    .parameter "messageId"
    .parameter "info"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 326
    monitor-enter p0

    #@3
    :try_start_3
    const-string v5, "BtMap.MmsEventManager"

    #@5
    const-string v6, "updateTmpStore"

    #@7
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 328
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@c
    if-eqz v5, :cond_58

    #@e
    .line 329
    const/4 v5, 0x1

    #@f
    new-array v0, v5, [Ljava/lang/String;

    #@11
    const/4 v5, 0x0

    #@12
    aput-object p1, v0, v5
    :try_end_14
    .catchall {:try_start_3 .. :try_end_14} :catchall_5a

    #@14
    .line 331
    .local v0, msgIdArg:[Ljava/lang/String;
    :try_start_14
    new-instance v2, Landroid/content/ContentValues;

    #@16
    const/4 v5, 0x4

    #@17
    invoke-direct {v2, v5}, Landroid/content/ContentValues;-><init>(I)V

    #@1a
    .line 332
    .local v2, v:Landroid/content/ContentValues;
    const-string v5, "_id"

    #@1c
    invoke-virtual {v2, v5, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1f
    .line 333
    const-string v5, "date"

    #@21
    iget-wide v6, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageDate:J

    #@23
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@26
    move-result-object v6

    #@27
    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@2a
    .line 334
    const-string v5, "msg_box"

    #@2c
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mMessageType:I

    #@2e
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v6

    #@32
    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@35
    .line 335
    const-string v5, "thread_id"

    #@37
    iget v6, p2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;->mThreadId:I

    #@39
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3c
    move-result-object v6

    #@3d
    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@40
    .line 337
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@42
    const-string v6, "mms"

    #@44
    const-string v7, "_id=?"

    #@46
    invoke-virtual {v5, v6, v2, v7, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_49
    .catchall {:try_start_14 .. :try_end_49} :catchall_5a
    .catch Ljava/lang/Throwable; {:try_start_14 .. :try_end_49} :catch_50

    #@49
    move-result v5

    #@4a
    if-lez v5, :cond_4e

    #@4c
    .line 342
    .end local v0           #msgIdArg:[Ljava/lang/String;
    .end local v2           #v:Landroid/content/ContentValues;
    :goto_4c
    monitor-exit p0

    #@4d
    return v3

    #@4e
    .restart local v0       #msgIdArg:[Ljava/lang/String;
    .restart local v2       #v:Landroid/content/ContentValues;
    :cond_4e
    move v3, v4

    #@4f
    .line 337
    goto :goto_4c

    #@50
    .line 338
    .end local v2           #v:Landroid/content/ContentValues;
    :catch_50
    move-exception v1

    #@51
    .line 339
    .local v1, t:Ljava/lang/Throwable;
    :try_start_51
    const-string v3, "BtMap.MmsEventManager"

    #@53
    const-string v5, "updateTmpStore: error"

    #@55
    invoke-static {v3, v5, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_58
    .catchall {:try_start_51 .. :try_end_58} :catchall_5a

    #@58
    .end local v0           #msgIdArg:[Ljava/lang/String;
    .end local v1           #t:Ljava/lang/Throwable;
    :cond_58
    move v3, v4

    #@59
    .line 342
    goto :goto_4c

    #@5a
    .line 326
    :catchall_5a
    move-exception v3

    #@5b
    monitor-exit p0

    #@5c
    throw v3
.end method


# virtual methods
.method public finish()V
    .registers 3

    #@0
    .prologue
    .line 432
    invoke-virtual {p0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->releaseContentObserver()Landroid/database/IContentObserver;

    #@3
    .line 433
    const/4 v0, 0x1

    #@4
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->cleanupTmpStore(Z)V

    #@7
    .line 435
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mContext:Landroid/content/Context;

    #@9
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@b
    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@e
    .line 437
    return-void
.end method

.method public declared-synchronized getOldMessageType(Ljava/lang/String;)I
    .registers 13
    .parameter "messageId"

    #@0
    .prologue
    .line 248
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtMap.MmsEventManager"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "getOldMessageType: messageId="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v1

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 250
    const/4 v9, -0x1

    #@1a
    .line 251
    .local v9, oldMsgType:I
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@1c
    if-eqz v0, :cond_43

    #@1e
    .line 252
    const/4 v0, 0x1

    #@1f
    new-array v4, v0, [Ljava/lang/String;

    #@21
    const/4 v0, 0x0

    #@22
    aput-object p1, v4, v0
    :try_end_24
    .catchall {:try_start_1 .. :try_end_24} :catchall_4e

    #@24
    .line 253
    .local v4, msgIdArg:[Ljava/lang/String;
    const/4 v8, 0x0

    #@25
    .line 255
    .local v8, c:Landroid/database/Cursor;
    :try_start_25
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@27
    const-string v1, "mms"

    #@29
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_TMP_PROJ:[Ljava/lang/String;

    #@2b
    const-string v3, "_id=?"

    #@2d
    const/4 v5, 0x0

    #@2e
    const/4 v6, 0x0

    #@2f
    const/4 v7, 0x0

    #@30
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@33
    move-result-object v8

    #@34
    .line 256
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_40

    #@3a
    .line 257
    const/4 v0, 0x6

    #@3b
    const/4 v1, -0x1

    #@3c
    invoke-static {v8, v0, v1}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I
    :try_end_3f
    .catchall {:try_start_25 .. :try_end_3f} :catchall_4e
    .catch Ljava/lang/Throwable; {:try_start_25 .. :try_end_3f} :catch_45

    #@3f
    move-result v9

    #@40
    .line 262
    :cond_40
    :goto_40
    :try_start_40
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_43
    .catchall {:try_start_40 .. :try_end_43} :catchall_4e

    #@43
    .line 264
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    :cond_43
    monitor-exit p0

    #@44
    return v9

    #@45
    .line 259
    .restart local v4       #msgIdArg:[Ljava/lang/String;
    .restart local v8       #c:Landroid/database/Cursor;
    :catch_45
    move-exception v10

    #@46
    .line 260
    .local v10, t:Ljava/lang/Throwable;
    :try_start_46
    const-string v0, "BtMap.MmsEventManager"

    #@48
    const-string v1, "getOldMessageType(): error"

    #@4a
    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4d
    .catchall {:try_start_46 .. :try_end_4d} :catchall_4e

    #@4d
    goto :goto_40

    #@4e
    .line 248
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v9           #oldMsgType:I
    .end local v10           #t:Ljava/lang/Throwable;
    :catchall_4e
    move-exception v0

    #@4f
    monitor-exit p0

    #@50
    throw v0
.end method

.method public declared-synchronized init()V
    .registers 8

    #@0
    .prologue
    .line 395
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@9
    .line 396
    const-string v0, "BtMap.MmsEventManager"

    #@b
    new-instance v1, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v2, "Registering content observer for "

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    sget-object v2, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 397
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@25
    sget-object v1, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@27
    const/4 v2, 0x1

    #@28
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@2b
    .line 399
    const-string v0, "BtMap.MmsEventManager"

    #@2d
    new-instance v1, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v2, "Registering content observer for "

    #@34
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    sget-object v2, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v1

    #@42
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 400
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@47
    sget-object v1, Landroid/provider/Telephony$MmsSms;->CONTENT_URI:Landroid/net/Uri;

    #@49
    const/4 v2, 0x0

    #@4a
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@4d
    .line 403
    const-string v0, "BtMap.MmsEventManager"

    #@4f
    new-instance v1, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v2, "Registering content observer for "

    #@56
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENHANCED_CONVERSATION_DELETE_CONTENT_URI:Landroid/net/Uri;

    #@5c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v1

    #@60
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v1

    #@64
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    .line 405
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@69
    sget-object v1, Lcom/broadcom/bt/service/map/datasource/SmsMmsConfig;->ENHANCED_CONVERSATION_DELETE_CONTENT_URI:Landroid/net/Uri;

    #@6b
    const/4 v2, 0x1

    #@6c
    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@6f
    .line 409
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->initTmpStore()V
    :try_end_72
    .catchall {:try_start_1 .. :try_end_72} :catchall_cb

    #@72
    .line 410
    const/4 v6, 0x0

    #@73
    .line 412
    .local v6, c:Landroid/database/Cursor;
    :try_start_73
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mResolver:Landroid/content/ContentResolver;

    #@75
    sget-object v1, Landroid/provider/Telephony$Mms;->CONTENT_URI:Landroid/net/Uri;

    #@77
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_PROJ:[Ljava/lang/String;

    #@79
    const/4 v3, 0x0

    #@7a
    const/4 v4, 0x0

    #@7b
    const/4 v5, 0x0

    #@7c
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@7f
    move-result-object v6

    #@80
    .line 413
    invoke-static {v6}, Lcom/broadcom/bt/util/DBUtil;->safeMoveToFirst(Landroid/database/Cursor;)Z

    #@83
    move-result v0

    #@84
    if-eqz v0, :cond_a8

    #@86
    .line 415
    :cond_86
    const/4 v0, 0x0

    #@87
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@8a
    move-result-object v1

    #@8b
    const/4 v0, 0x1

    #@8c
    const-wide/16 v2, -0x1

    #@8e
    invoke-static {v6, v0, v2, v3}, Lcom/broadcom/bt/util/DBUtil;->getLong(Landroid/database/Cursor;IJ)J

    #@91
    move-result-wide v2

    #@92
    const/4 v0, 0x2

    #@93
    const/4 v4, -0x1

    #@94
    invoke-static {v6, v0, v4}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@97
    move-result v4

    #@98
    const/4 v0, 0x3

    #@99
    const/4 v5, -0x1

    #@9a
    invoke-static {v6, v0, v5}, Lcom/broadcom/bt/util/DBUtil;->getInt(Landroid/database/Cursor;II)I

    #@9d
    move-result v5

    #@9e
    move-object v0, p0

    #@9f
    invoke-direct/range {v0 .. v5}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->addToTmpStore(Ljava/lang/String;JII)V

    #@a2
    .line 417
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_a5
    .catchall {:try_start_73 .. :try_end_a5} :catchall_cb
    .catch Ljava/lang/Throwable; {:try_start_73 .. :try_end_a5} :catch_ce

    #@a5
    move-result v0

    #@a6
    if-nez v0, :cond_86

    #@a8
    .line 422
    :cond_a8
    :goto_a8
    :try_start_a8
    invoke-static {v6}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V

    #@ab
    .line 424
    new-instance v0, Landroid/content/IntentFilter;

    #@ad
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@b0
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@b2
    .line 425
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@b4
    const-string v1, "com.lge.bluetooth.mms.RECEIVED"

    #@b6
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@b9
    .line 426
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@bb
    const-string v1, "com.lge.bluetooth.mms.DELETED"

    #@bd
    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c0
    .line 427
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mContext:Landroid/content/Context;

    #@c2
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    #@c4
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mIntentFilter:Landroid/content/IntentFilter;

    #@c6
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_c9
    .catchall {:try_start_a8 .. :try_end_c9} :catchall_cb

    #@c9
    .line 429
    monitor-exit p0

    #@ca
    return-void

    #@cb
    .line 395
    .end local v6           #c:Landroid/database/Cursor;
    :catchall_cb
    move-exception v0

    #@cc
    monitor-exit p0

    #@cd
    throw v0

    #@ce
    .line 419
    .restart local v6       #c:Landroid/database/Cursor;
    :catch_ce
    move-exception v0

    #@cf
    goto :goto_a8
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .registers 11
    .parameter "selfChange"
    .parameter "uri"

    #@0
    .prologue
    .line 465
    if-nez p2, :cond_3

    #@2
    .line 511
    :goto_2
    return-void

    #@3
    .line 468
    :cond_3
    sget-object v5, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->URL_MATCHER:Landroid/content/UriMatcher;

    #@5
    invoke-virtual {v5, p2}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@8
    move-result v4

    #@9
    .line 471
    .local v4, uriMatch:I
    const-string v5, "BtMap.MmsEventManager"

    #@b
    new-instance v6, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v7, "onChange(): uri ="

    #@12
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v6

    #@1a
    const-string v7, ", selfChange="

    #@1c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v6

    #@20
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23
    move-result-object v6

    #@24
    const-string v7, ", uriMatch="

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 474
    const/4 v5, 0x1

    #@36
    if-ne v4, v5, :cond_3c

    #@38
    .line 475
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->processAllMessageChanges()V

    #@3b
    goto :goto_2

    #@3c
    .line 477
    :cond_3c
    const/4 v5, 0x2

    #@3d
    if-ne v4, v5, :cond_56

    #@3f
    .line 480
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@42
    move-result-object v0

    #@43
    .line 482
    .local v0, messageId:Ljava/lang/String;
    :try_start_43
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@46
    .line 483
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->processDeleteConversation(Ljava/lang/String;)V
    :try_end_49
    .catch Ljava/lang/Throwable; {:try_start_43 .. :try_end_49} :catch_4a

    #@49
    goto :goto_2

    #@4a
    .line 484
    :catch_4a
    move-exception v3

    #@4b
    .line 485
    .local v3, t:Ljava/lang/Throwable;
    const-string v5, "BtMap.MmsEventManager"

    #@4d
    const-string v6, "onChange(): conversationId not found. Processing for all message changes...."

    #@4f
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 487
    invoke-direct {p0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->processAllMessageChanges()V

    #@55
    goto :goto_2

    #@56
    .line 491
    .end local v0           #messageId:Ljava/lang/String;
    .end local v3           #t:Ljava/lang/Throwable;
    :cond_56
    const/4 v5, 0x3

    #@57
    if-eq v4, v5, :cond_72

    #@59
    .line 492
    const-string v5, "BtMap.MmsEventManager"

    #@5b
    new-instance v6, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v7, "onChange: ignoring "

    #@62
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v6

    #@6e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    goto :goto_2

    #@72
    .line 496
    :cond_72
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@75
    move-result-object v0

    #@76
    .line 498
    .restart local v0       #messageId:Ljava/lang/String;
    :try_start_76
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_79
    .catch Ljava/lang/Throwable; {:try_start_76 .. :try_end_79} :catch_9e

    #@79
    .line 504
    const-string v5, "BtMap.MmsEventManager"

    #@7b
    new-instance v6, Ljava/lang/StringBuilder;

    #@7d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@80
    const-string v7, "messageId="

    #@82
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v6

    #@86
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v6

    #@8a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v6

    #@8e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 508
    invoke-direct {p0, v0}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->getFromTmpStore(Ljava/lang/String;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;

    #@94
    move-result-object v2

    #@95
    .line 509
    .local v2, oldMsgInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    invoke-direct {p0, p2}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->getFromMmsProvider(Landroid/net/Uri;)Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;

    #@98
    move-result-object v1

    #@99
    .line 510
    .local v1, msgInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    invoke-direct {p0, v0, v1, v2}, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->processMessageChange(Ljava/lang/String;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;)V

    #@9c
    goto/16 :goto_2

    #@9e
    .line 499
    .end local v1           #msgInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    .end local v2           #oldMsgInfo:Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager$MmsMsgInfo;
    :catch_9e
    move-exception v3

    #@9f
    .line 500
    .restart local v3       #t:Ljava/lang/Throwable;
    const-string v5, "BtMap.MmsEventManager"

    #@a1
    const-string v6, "onChange(): messageId not found. Skipping...."

    #@a3
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    goto/16 :goto_2
.end method

.method public declared-synchronized setMessageCreatedByMce(Ljava/lang/String;Z)Z
    .registers 15
    .parameter "messageId"
    .parameter "createdByMce"

    #@0
    .prologue
    .line 194
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtMap.MmsEventManager"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "setMessageCreatedByMce: messageId="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ", createdByMce="

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 197
    const/4 v9, 0x0

    #@24
    .line 198
    .local v9, success:Z
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@26
    if-eqz v0, :cond_6b

    #@28
    .line 199
    const/4 v0, 0x1

    #@29
    new-array v4, v0, [Ljava/lang/String;

    #@2b
    const/4 v0, 0x0

    #@2c
    aput-object p1, v4, v0

    #@2e
    .line 200
    .local v4, msgIdArg:[Ljava/lang/String;
    new-instance v11, Landroid/content/ContentValues;

    #@30
    const/4 v0, 0x2

    #@31
    invoke-direct {v11, v0}, Landroid/content/ContentValues;-><init>(I)V

    #@34
    .line 201
    .local v11, v:Landroid/content/ContentValues;
    const-string v0, "_id"

    #@36
    invoke-virtual {v11, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 202
    const-string v1, "mce_created"

    #@3b
    if-eqz p2, :cond_6d

    #@3d
    const/4 v0, 0x1

    #@3e
    :goto_3e
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v0

    #@42
    invoke-virtual {v11, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_45
    .catchall {:try_start_1 .. :try_end_45} :catchall_83

    #@45
    .line 203
    const/4 v8, 0x0

    #@46
    .line 205
    .local v8, c:Landroid/database/Cursor;
    :try_start_46
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@48
    const-string v1, "mms"

    #@4a
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_TMP_PROJ:[Ljava/lang/String;

    #@4c
    const-string v3, "_id=?"

    #@4e
    const/4 v5, 0x0

    #@4f
    const/4 v6, 0x0

    #@50
    const/4 v7, 0x0

    #@51
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@54
    move-result-object v8

    #@55
    .line 206
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@58
    move-result v0

    #@59
    if-eqz v0, :cond_71

    #@5b
    .line 207
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@5d
    const-string v1, "mms"

    #@5f
    const-string v2, "_id=?"

    #@61
    invoke-virtual {v0, v1, v11, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_64
    .catchall {:try_start_46 .. :try_end_64} :catchall_83
    .catch Ljava/lang/Throwable; {:try_start_46 .. :try_end_64} :catch_7a

    #@64
    move-result v0

    #@65
    if-lez v0, :cond_6f

    #@67
    const/4 v9, 0x1

    #@68
    .line 214
    :goto_68
    :try_start_68
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_6b
    .catchall {:try_start_68 .. :try_end_6b} :catchall_83

    #@6b
    .line 216
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v11           #v:Landroid/content/ContentValues;
    :cond_6b
    monitor-exit p0

    #@6c
    return v9

    #@6d
    .line 202
    .restart local v4       #msgIdArg:[Ljava/lang/String;
    .restart local v11       #v:Landroid/content/ContentValues;
    :cond_6d
    const/4 v0, 0x0

    #@6e
    goto :goto_3e

    #@6f
    .line 207
    .restart local v8       #c:Landroid/database/Cursor;
    :cond_6f
    const/4 v9, 0x0

    #@70
    goto :goto_68

    #@71
    .line 209
    :cond_71
    :try_start_71
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@73
    const-string v1, "mms"

    #@75
    const/4 v2, 0x0

    #@76
    invoke-virtual {v0, v1, v2, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_79
    .catchall {:try_start_71 .. :try_end_79} :catchall_83
    .catch Ljava/lang/Throwable; {:try_start_71 .. :try_end_79} :catch_7a

    #@79
    goto :goto_68

    #@7a
    .line 211
    :catch_7a
    move-exception v10

    #@7b
    .line 212
    .local v10, t:Ljava/lang/Throwable;
    :try_start_7b
    const-string v0, "BtMap.MmsEventManager"

    #@7d
    const-string v1, "setMessageCreatedByMce(): error"

    #@7f
    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_82
    .catchall {:try_start_7b .. :try_end_82} :catchall_83

    #@82
    goto :goto_68

    #@83
    .line 194
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v9           #success:Z
    .end local v10           #t:Ljava/lang/Throwable;
    .end local v11           #v:Landroid/content/ContentValues;
    :catchall_83
    move-exception v0

    #@84
    monitor-exit p0

    #@85
    throw v0
.end method

.method public declared-synchronized setMessageDeletedByMce(Ljava/lang/String;Z)Z
    .registers 12
    .parameter "messageId"
    .parameter "deletedByMce"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 176
    monitor-enter p0

    #@3
    :try_start_3
    const-string v4, "BtMap.MmsEventManager"

    #@5
    new-instance v5, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v6, "setMessageDeletedByMce: messageId="

    #@c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v5

    #@10
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v5

    #@14
    const-string v6, ", deletedByMce="

    #@16
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v5

    #@1a
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v5

    #@1e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v5

    #@22
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 180
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_27
    .catchall {:try_start_3 .. :try_end_27} :catchall_5d

    #@27
    if-eqz v4, :cond_5b

    #@29
    .line 182
    :try_start_29
    new-instance v1, Landroid/content/ContentValues;

    #@2b
    const/4 v4, 0x1

    #@2c
    invoke-direct {v1, v4}, Landroid/content/ContentValues;-><init>(I)V

    #@2f
    .line 183
    .local v1, v:Landroid/content/ContentValues;
    const-string v5, "mce_deleted"

    #@31
    if-eqz p2, :cond_4f

    #@33
    move v4, v2

    #@34
    :goto_34
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v1, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3b
    .line 184
    iget-object v4, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@3d
    const-string v5, "mms"

    #@3f
    const-string v6, "_id=?"

    #@41
    const/4 v7, 0x1

    #@42
    new-array v7, v7, [Ljava/lang/String;

    #@44
    const/4 v8, 0x0

    #@45
    aput-object p1, v7, v8

    #@47
    invoke-virtual {v4, v5, v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4a
    .catchall {:try_start_29 .. :try_end_4a} :catchall_5d
    .catch Ljava/lang/Throwable; {:try_start_29 .. :try_end_4a} :catch_53

    #@4a
    move-result v4

    #@4b
    if-lez v4, :cond_51

    #@4d
    .line 189
    .end local v1           #v:Landroid/content/ContentValues;
    :goto_4d
    monitor-exit p0

    #@4e
    return v2

    #@4f
    .restart local v1       #v:Landroid/content/ContentValues;
    :cond_4f
    move v4, v3

    #@50
    .line 183
    goto :goto_34

    #@51
    :cond_51
    move v2, v3

    #@52
    .line 184
    goto :goto_4d

    #@53
    .line 185
    .end local v1           #v:Landroid/content/ContentValues;
    :catch_53
    move-exception v0

    #@54
    .line 186
    .local v0, t:Ljava/lang/Throwable;
    :try_start_54
    const-string v2, "BtMap.MmsEventManager"

    #@56
    const-string v4, "addToTmpStore(): error"

    #@58
    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5b
    .catchall {:try_start_54 .. :try_end_5b} :catchall_5d

    #@5b
    .end local v0           #t:Ljava/lang/Throwable;
    :cond_5b
    move v2, v3

    #@5c
    .line 189
    goto :goto_4d

    #@5d
    .line 176
    :catchall_5d
    move-exception v2

    #@5e
    monitor-exit p0

    #@5f
    throw v2
.end method

.method public declared-synchronized setOldMessageType(Ljava/lang/String;I)Z
    .registers 15
    .parameter "messageId"
    .parameter "oldMessageType"

    #@0
    .prologue
    .line 221
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "BtMap.MmsEventManager"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "setOldMessageType: messageId="

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, ", oldMessageType="

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v1

    #@20
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 224
    const/4 v9, 0x0

    #@24
    .line 225
    .local v9, success:Z
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@26
    if-eqz v0, :cond_68

    #@28
    .line 226
    const/4 v0, 0x1

    #@29
    new-array v4, v0, [Ljava/lang/String;

    #@2b
    const/4 v0, 0x0

    #@2c
    aput-object p1, v4, v0

    #@2e
    .line 227
    .local v4, msgIdArg:[Ljava/lang/String;
    new-instance v11, Landroid/content/ContentValues;

    #@30
    const/4 v0, 0x2

    #@31
    invoke-direct {v11, v0}, Landroid/content/ContentValues;-><init>(I)V

    #@34
    .line 228
    .local v11, v:Landroid/content/ContentValues;
    const-string v0, "_id"

    #@36
    invoke-virtual {v11, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 229
    const-string v0, "old_msg_type"

    #@3b
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3e
    move-result-object v1

    #@3f
    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V
    :try_end_42
    .catchall {:try_start_1 .. :try_end_42} :catchall_88

    #@42
    .line 230
    const/4 v8, 0x0

    #@43
    .line 232
    .local v8, c:Landroid/database/Cursor;
    :try_start_43
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@45
    const-string v1, "mms"

    #@47
    sget-object v2, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->MMS_TMP_PROJ:[Ljava/lang/String;

    #@49
    const-string v3, "_id=?"

    #@4b
    const/4 v5, 0x0

    #@4c
    const/4 v6, 0x0

    #@4d
    const/4 v7, 0x0

    #@4e
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@51
    move-result-object v8

    #@52
    .line 233
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@55
    move-result v0

    #@56
    if-eqz v0, :cond_6c

    #@58
    .line 234
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@5a
    const-string v1, "mms"

    #@5c
    const-string v2, "_id=?"

    #@5e
    invoke-virtual {v0, v1, v11, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_61
    .catchall {:try_start_43 .. :try_end_61} :catchall_88
    .catch Ljava/lang/Throwable; {:try_start_43 .. :try_end_61} :catch_7f

    #@61
    move-result v0

    #@62
    if-lez v0, :cond_6a

    #@64
    const/4 v9, 0x1

    #@65
    .line 241
    :goto_65
    :try_start_65
    invoke-static {v8}, Lcom/broadcom/bt/util/DBUtil;->safeClose(Landroid/database/Cursor;)V
    :try_end_68
    .catchall {:try_start_65 .. :try_end_68} :catchall_88

    #@68
    .line 243
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v11           #v:Landroid/content/ContentValues;
    :cond_68
    monitor-exit p0

    #@69
    return v9

    #@6a
    .line 234
    .restart local v4       #msgIdArg:[Ljava/lang/String;
    .restart local v8       #c:Landroid/database/Cursor;
    .restart local v11       #v:Landroid/content/ContentValues;
    :cond_6a
    const/4 v9, 0x0

    #@6b
    goto :goto_65

    #@6c
    .line 236
    :cond_6c
    const-wide/16 v0, 0x0

    #@6e
    :try_start_6e
    iget-object v2, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsEventManager;->mDb:Landroid/database/sqlite/SQLiteDatabase;

    #@70
    const-string v3, "mms"

    #@72
    const/4 v5, 0x0

    #@73
    invoke-virtual {v2, v3, v5, v11}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_76
    .catchall {:try_start_6e .. :try_end_76} :catchall_88
    .catch Ljava/lang/Throwable; {:try_start_6e .. :try_end_76} :catch_7f

    #@76
    move-result-wide v2

    #@77
    cmp-long v0, v0, v2

    #@79
    if-gez v0, :cond_7d

    #@7b
    const/4 v9, 0x1

    #@7c
    :goto_7c
    goto :goto_65

    #@7d
    :cond_7d
    const/4 v9, 0x0

    #@7e
    goto :goto_7c

    #@7f
    .line 238
    :catch_7f
    move-exception v10

    #@80
    .line 239
    .local v10, t:Ljava/lang/Throwable;
    :try_start_80
    const-string v0, "BtMap.MmsEventManager"

    #@82
    const-string v1, "setOldMessageType(): error"

    #@84
    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_87
    .catchall {:try_start_80 .. :try_end_87} :catchall_88

    #@87
    goto :goto_65

    #@88
    .line 221
    .end local v4           #msgIdArg:[Ljava/lang/String;
    .end local v8           #c:Landroid/database/Cursor;
    .end local v9           #success:Z
    .end local v10           #t:Ljava/lang/Throwable;
    .end local v11           #v:Landroid/content/ContentValues;
    :catchall_88
    move-exception v0

    #@89
    monitor-exit p0

    #@8a
    throw v0
.end method
