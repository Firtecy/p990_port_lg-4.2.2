.class public Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;
.super Ljava/lang/Object;
.source "MmsMessageSender.java"


# static fields
.field private static final DBG:Z = true

.field private static final DEFAULT_EXPIRY_TIME:J = 0x93a80L

.field private static final DEFAULT_MESSAGE_CLASS:Ljava/lang/String; = "personal"

.field private static final DEFAULT_PRIORITY:I = 0x81

.field private static final TAG:Ljava/lang/String; = "BtMap.MmsMessageSender"


# instance fields
.field mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "ctx"

    #@0
    .prologue
    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 178
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->mContext:Landroid/content/Context;

    #@5
    .line 179
    return-void
.end method

.method private static getByteBody(Lcom/broadcom/bt/util/mime4j/message/Entity;)[B
    .registers 4
    .parameter "part"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/broadcom/bt/util/mime4j/message/Entity;->getBody()Lcom/broadcom/bt/util/mime4j/message/Body;

    #@3
    move-result-object v0

    #@4
    .line 158
    .local v0, b:Lcom/broadcom/bt/util/mime4j/message/Body;
    if-eqz v0, :cond_16

    #@6
    .line 159
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    #@8
    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@b
    .line 160
    .local v1, baos:Ljava/io/ByteArrayOutputStream;
    invoke-interface {v0, v1}, Lcom/broadcom/bt/util/mime4j/message/Body;->writeTo(Ljava/io/OutputStream;)V

    #@e
    .line 161
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->close()V

    #@11
    .line 162
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@14
    move-result-object v2

    #@15
    .line 164
    .end local v1           #baos:Ljava/io/ByteArrayOutputStream;
    :goto_15
    return-object v2

    #@16
    :cond_16
    const/4 v2, 0x0

    #@17
    goto :goto_15
.end method

.method private getFilenameFromContentDisposition(Ljava/lang/String;)Ljava/lang/String;
    .registers 8
    .parameter "contentDisposition"

    #@0
    .prologue
    const/16 v5, 0x22

    #@2
    .line 110
    if-eqz p1, :cond_41

    #@4
    .line 111
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    .line 112
    .local v0, cdLc:Ljava/lang/String;
    const-string v4, "attachment;"

    #@a
    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_41

    #@10
    .line 113
    const-string v4, "filename="

    #@12
    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@15
    move-result v3

    #@16
    .line 114
    .local v3, index:I
    if-ltz v3, :cond_41

    #@18
    .line 115
    const-string v4, "filename="

    #@1a
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@1d
    move-result v4

    #@1e
    add-int v2, v3, v4

    #@20
    .line 116
    .local v2, fnStart:I
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@23
    move-result v4

    #@24
    if-ge v2, v4, :cond_41

    #@26
    .line 117
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@29
    move-result v4

    #@2a
    if-ne v4, v5, :cond_2e

    #@2c
    .line 118
    add-int/lit8 v2, v2, 0x1

    #@2e
    .line 120
    :cond_2e
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@31
    move-result v1

    #@32
    .line 121
    .local v1, fnEnd:I
    add-int/lit8 v4, v1, -0x1

    #@34
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@37
    move-result v4

    #@38
    if-ne v4, v5, :cond_3c

    #@3a
    .line 122
    add-int/lit8 v1, v1, -0x1

    #@3c
    .line 124
    :cond_3c
    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3f
    move-result-object v4

    #@40
    .line 129
    .end local v0           #cdLc:Ljava/lang/String;
    .end local v1           #fnEnd:I
    .end local v2           #fnStart:I
    .end local v3           #index:I
    :goto_40
    return-object v4

    #@41
    :cond_41
    const/4 v4, 0x0

    #@42
    goto :goto_40
.end method

.method private getHeaderFieldValue(Lcom/broadcom/bt/util/mime4j/message/Header;Ljava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "header"
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 97
    if-nez p1, :cond_b

    #@3
    .line 98
    const-string v2, "BtMap.MmsMessageSender"

    #@5
    const-string v3, "getHeaderFieldValue(): header is null"

    #@7
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 106
    :goto_a
    return-object v1

    #@b
    .line 101
    :cond_b
    invoke-virtual {p1, p2}, Lcom/broadcom/bt/util/mime4j/message/Header;->getField(Ljava/lang/String;)Lcom/broadcom/bt/util/mime4j/field/Field;

    #@e
    move-result-object v0

    #@f
    .line 102
    .local v0, f:Lcom/broadcom/bt/util/mime4j/field/Field;
    if-nez v0, :cond_19

    #@11
    .line 103
    const-string v2, "BtMap.MmsMessageSender"

    #@13
    const-string v3, "getHeaderFieldValue(): field is null"

    #@15
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    goto :goto_a

    #@19
    .line 106
    :cond_19
    invoke-virtual {v0}, Lcom/broadcom/bt/util/mime4j/field/Field;->getBody()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    goto :goto_a
.end method

.method private static getTextBody(Lcom/broadcom/bt/util/mime4j/message/Entity;)Ljava/lang/String;
    .registers 6
    .parameter "part"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 167
    invoke-virtual {p0}, Lcom/broadcom/bt/util/mime4j/message/Entity;->getBody()Lcom/broadcom/bt/util/mime4j/message/Body;

    #@3
    move-result-object v0

    #@4
    .line 168
    .local v0, b:Lcom/broadcom/bt/util/mime4j/message/Body;
    instance-of v3, v0, Lcom/broadcom/bt/util/mime4j/message/TextBody;

    #@6
    if-eqz v3, :cond_1d

    #@8
    move-object v2, v0

    #@9
    .line 169
    check-cast v2, Lcom/broadcom/bt/util/mime4j/message/TextBody;

    #@b
    .line 170
    .local v2, tb:Lcom/broadcom/bt/util/mime4j/message/TextBody;
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    #@d
    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    #@10
    .line 171
    .local v1, baos:Ljava/io/ByteArrayOutputStream;
    invoke-interface {v2, v1}, Lcom/broadcom/bt/util/mime4j/message/TextBody;->writeTo(Ljava/io/OutputStream;)V

    #@13
    .line 172
    new-instance v3, Ljava/lang/String;

    #@15
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    #@18
    move-result-object v4

    #@19
    invoke-direct {v3, v4}, Ljava/lang/String;-><init>([B)V

    #@1c
    .line 174
    .end local v1           #baos:Ljava/io/ByteArrayOutputStream;
    .end local v2           #tb:Lcom/broadcom/bt/util/mime4j/message/TextBody;
    :goto_1c
    return-object v3

    #@1d
    :cond_1d
    const/4 v3, 0x0

    #@1e
    goto :goto_1c
.end method


# virtual methods
.method public openReadableOutputStream(Ljava/lang/String;[Landroid/net/Uri;)Ljava/io/OutputStream;
    .registers 10
    .parameter "fileName"
    .parameter "returnUri"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 133
    const/4 v3, 0x0

    #@2
    .line 135
    .local v3, uri:Landroid/net/Uri;
    :try_start_2
    new-instance v5, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v6, "content://com.broadcom.bt.map/"

    #@9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v5

    #@d
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v5

    #@15
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_18
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_18} :catch_2c

    #@18
    move-result-object v3

    #@19
    .line 141
    const/4 v0, 0x0

    #@1a
    .line 143
    .local v0, fos:Ljava/io/OutputStream;
    :try_start_1a
    iget-object v5, p0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->mContext:Landroid/content/Context;

    #@1c
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1f
    move-result-object v1

    #@20
    .line 144
    .local v1, r:Landroid/content/ContentResolver;
    const-string v5, "cwtR"

    #@22
    invoke-virtual {v1, v3, v5}, Landroid/content/ContentResolver;->openOutputStream(Landroid/net/Uri;Ljava/lang/String;)Ljava/io/OutputStream;
    :try_end_25
    .catch Ljava/lang/Throwable; {:try_start_1a .. :try_end_25} :catch_36

    #@25
    move-result-object v0

    #@26
    .line 150
    if-eqz p2, :cond_2b

    #@28
    .line 151
    const/4 v4, 0x0

    #@29
    aput-object v3, p2, v4

    #@2b
    .line 153
    .end local v0           #fos:Ljava/io/OutputStream;
    .end local v1           #r:Landroid/content/ContentResolver;
    :cond_2b
    :goto_2b
    return-object v0

    #@2c
    .line 136
    :catch_2c
    move-exception v2

    #@2d
    .line 137
    .local v2, t:Ljava/lang/Throwable;
    const-string v5, "BtMap.MmsMessageSender"

    #@2f
    const-string v6, "Unable to parse URI for writing BMessage"

    #@31
    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@34
    move-object v0, v4

    #@35
    .line 138
    goto :goto_2b

    #@36
    .line 145
    .end local v2           #t:Ljava/lang/Throwable;
    .restart local v0       #fos:Ljava/io/OutputStream;
    :catch_36
    move-exception v2

    #@37
    .line 146
    .restart local v2       #t:Ljava/lang/Throwable;
    const-string v5, "BtMap.MmsMessageSender"

    #@39
    const-string v6, "Unable to open content stream"

    #@3b
    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@3e
    move-object v0, v4

    #@3f
    .line 147
    goto :goto_2b
.end method

.method public sendOrSaveMms(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/lang/String;
    .registers 42
    .parameter "folderPath"
    .parameter "addresses"
    .parameter "subject"
    .parameter "bMsgFilename"
    .parameter "contents"
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/broadcom/bt/util/mime4j/message/Entity;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    #@0
    .prologue
    .line 183
    .local p6, entities:Ljava/util/List;,"Ljava/util/List<Lcom/broadcom/bt/util/mime4j/message/Entity;>;"
    const/16 v22, 0x0

    #@2
    .line 184
    .local v22, persister:Lcom/google/android/mms/pdu/PduPersister;
    const/16 v16, 0x0

    #@4
    .line 185
    .local v16, messageUri:Landroid/net/Uri;
    const/16 v23, 0x0

    #@6
    .line 186
    .local v23, request:Lcom/google/android/mms/pdu/SendReq;
    const-wide/16 v14, -0x1

    #@8
    .line 187
    .local v14, messageId:J
    invoke-static/range {p1 .. p1}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toMessageType(Ljava/lang/String;)I

    #@b
    move-result v12

    #@c
    .line 188
    .local v12, mBox:I
    invoke-static {v12}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageHelper;->toMmsMboxUri(I)Landroid/net/Uri;

    #@f
    move-result-object v13

    #@10
    .line 189
    .local v13, mBoxContentUri:Landroid/net/Uri;
    const/16 v31, -0x1

    #@12
    move/from16 v0, v31

    #@14
    if-eq v12, v0, :cond_18

    #@16
    if-nez v13, :cond_22

    #@18
    .line 190
    :cond_18
    const-string v31, "BtMap.MmsMessageSender"

    #@1a
    const-string v32, "sendOrSaveMms(): invalid folderPath"

    #@1c
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 191
    const/16 v31, 0x0

    #@21
    .line 327
    :goto_21
    return-object v31

    #@22
    .line 195
    :cond_22
    :try_start_22
    move-object/from16 v0, p0

    #@24
    iget-object v0, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->mContext:Landroid/content/Context;

    #@26
    move-object/from16 v31, v0

    #@28
    invoke-virtual/range {v31 .. v31}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2b
    move-result-object v25

    #@2c
    .line 197
    .local v25, resolver:Landroid/content/ContentResolver;
    move-object/from16 v0, p0

    #@2e
    iget-object v0, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->mContext:Landroid/content/Context;

    #@30
    move-object/from16 v31, v0

    #@32
    invoke-static/range {v31 .. v31}, Lcom/google/android/mms/pdu/PduPersister;->getPduPersister(Landroid/content/Context;)Lcom/google/android/mms/pdu/PduPersister;

    #@35
    move-result-object v22

    #@36
    .line 198
    new-instance v24, Lcom/google/android/mms/pdu/SendReq;

    #@38
    invoke-direct/range {v24 .. v24}, Lcom/google/android/mms/pdu/SendReq;-><init>()V
    :try_end_3b
    .catch Ljava/lang/Throwable; {:try_start_22 .. :try_end_3b} :catch_3d7

    #@3b
    .line 199
    .end local v23           #request:Lcom/google/android/mms/pdu/SendReq;
    .local v24, request:Lcom/google/android/mms/pdu/SendReq;
    const-wide/32 v31, 0x93a80

    #@3e
    :try_start_3e
    move-object/from16 v0, v24

    #@40
    move-wide/from16 v1, v31

    #@42
    invoke-virtual {v0, v1, v2}, Lcom/google/android/mms/pdu/SendReq;->setExpiry(J)V

    #@45
    .line 200
    const/16 v31, 0x81

    #@47
    move-object/from16 v0, v24

    #@49
    move/from16 v1, v31

    #@4b
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/SendReq;->setPriority(I)V

    #@4e
    .line 201
    const/16 v31, 0x80

    #@50
    move-object/from16 v0, v24

    #@52
    move/from16 v1, v31

    #@54
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/SendReq;->setDeliveryReport(I)V

    #@57
    .line 202
    const/16 v31, 0x80

    #@59
    move-object/from16 v0, v24

    #@5b
    move/from16 v1, v31

    #@5d
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/SendReq;->setReadReport(I)V

    #@60
    .line 203
    const-string v31, "personal"

    #@62
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->getBytes()[B

    #@65
    move-result-object v31

    #@66
    move-object/from16 v0, v24

    #@68
    move-object/from16 v1, v31

    #@6a
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/SendReq;->setMessageClass([B)V

    #@6d
    .line 204
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@70
    move-result-wide v31

    #@71
    const-wide/16 v33, 0x3e8

    #@73
    div-long v31, v31, v33

    #@75
    move-object/from16 v0, v24

    #@77
    move-wide/from16 v1, v31

    #@79
    invoke-virtual {v0, v1, v2}, Lcom/google/android/mms/pdu/SendReq;->setDate(J)V

    #@7c
    .line 206
    invoke-static/range {p2 .. p2}, Lcom/google/android/mms/pdu/EncodedStringValue;->encodeStrings([Ljava/lang/String;)[Lcom/google/android/mms/pdu/EncodedStringValue;

    #@7f
    move-result-object v7

    #@80
    .line 207
    .local v7, encodedNumbers:[Lcom/google/android/mms/pdu/EncodedStringValue;
    if-eqz v7, :cond_87

    #@82
    .line 208
    move-object/from16 v0, v24

    #@84
    invoke-virtual {v0, v7}, Lcom/google/android/mms/pdu/SendReq;->setTo([Lcom/google/android/mms/pdu/EncodedStringValue;)V

    #@87
    .line 210
    :cond_87
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8a
    move-result v31

    #@8b
    if-nez v31, :cond_9d

    #@8d
    .line 211
    new-instance v31, Lcom/google/android/mms/pdu/EncodedStringValue;

    #@8f
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->toString()Ljava/lang/String;

    #@92
    move-result-object v32

    #@93
    invoke-direct/range {v31 .. v32}, Lcom/google/android/mms/pdu/EncodedStringValue;-><init>(Ljava/lang/String;)V

    #@96
    move-object/from16 v0, v24

    #@98
    move-object/from16 v1, v31

    #@9a
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/SendReq;->setSubject(Lcom/google/android/mms/pdu/EncodedStringValue;)V

    #@9d
    .line 214
    :cond_9d
    new-instance v21, Lcom/google/android/mms/pdu/PduBody;

    #@9f
    invoke-direct/range {v21 .. v21}, Lcom/google/android/mms/pdu/PduBody;-><init>()V

    #@a2
    .line 215
    .local v21, pb:Lcom/google/android/mms/pdu/PduBody;
    const/16 v19, 0x0

    #@a4
    .line 217
    .local v19, part:Lcom/google/android/mms/pdu/PduPart;
    if-nez p6, :cond_104

    #@a6
    const/16 v20, 0x0

    #@a8
    .line 218
    .local v20, partCount:I
    :goto_a8
    if-nez v20, :cond_109

    #@aa
    .line 220
    new-instance v19, Lcom/google/android/mms/pdu/PduPart;

    #@ac
    .end local v19           #part:Lcom/google/android/mms/pdu/PduPart;
    invoke-direct/range {v19 .. v19}, Lcom/google/android/mms/pdu/PduPart;-><init>()V

    #@af
    .line 221
    .restart local v19       #part:Lcom/google/android/mms/pdu/PduPart;
    const-string v31, "text/plain"

    #@b1
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->getBytes()[B

    #@b4
    move-result-object v31

    #@b5
    move-object/from16 v0, v19

    #@b7
    move-object/from16 v1, v31

    #@b9
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentType([B)V

    #@bc
    .line 222
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->getBytes()[B

    #@bf
    move-result-object v31

    #@c0
    move-object/from16 v0, v19

    #@c2
    move-object/from16 v1, v31

    #@c4
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentLocation([B)V

    #@c7
    .line 223
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->getBytes()[B

    #@ca
    move-result-object v31

    #@cb
    move-object/from16 v0, v19

    #@cd
    move-object/from16 v1, v31

    #@cf
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentId([B)V

    #@d2
    .line 224
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->getBytes()[B

    #@d5
    move-result-object v31

    #@d6
    move-object/from16 v0, v19

    #@d8
    move-object/from16 v1, v31

    #@da
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setData([B)V

    #@dd
    .line 225
    move-object/from16 v0, v21

    #@df
    move-object/from16 v1, v19

    #@e1
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduBody;->addPart(Lcom/google/android/mms/pdu/PduPart;)Z

    #@e4
    .line 298
    :cond_e4
    move-object/from16 v0, v24

    #@e6
    move-object/from16 v1, v21

    #@e8
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/SendReq;->setBody(Lcom/google/android/mms/pdu/PduBody;)V

    #@eb
    .line 299
    new-instance v3, Ljava/util/HashSet;

    #@ed
    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    #@f0
    .line 300
    .local v3, addressesSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    const/4 v11, 0x0

    #@f1
    .local v11, i:I
    :goto_f1
    move-object/from16 v0, p2

    #@f3
    array-length v0, v0

    #@f4
    move/from16 v31, v0

    #@f6
    move/from16 v0, v31

    #@f8
    if-ge v11, v0, :cond_35f

    #@fa
    .line 301
    aget-object v31, p2, v11

    #@fc
    move-object/from16 v0, v31

    #@fe
    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    #@101
    .line 300
    add-int/lit8 v11, v11, 0x1

    #@103
    goto :goto_f1

    #@104
    .line 217
    .end local v3           #addressesSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v11           #i:I
    .end local v20           #partCount:I
    :cond_104
    invoke-interface/range {p6 .. p6}, Ljava/util/List;->size()I

    #@107
    move-result v20

    #@108
    goto :goto_a8

    #@109
    .line 227
    .restart local v20       #partCount:I
    :cond_109
    const/4 v11, 0x0

    #@10a
    .restart local v11       #i:I
    :goto_10a
    move/from16 v0, v20

    #@10c
    if-ge v11, v0, :cond_e4

    #@10e
    .line 228
    move-object/from16 v0, p6

    #@110
    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@113
    move-result-object v8

    #@114
    check-cast v8, Lcom/broadcom/bt/util/mime4j/message/Entity;

    #@116
    .line 229
    .local v8, entity:Lcom/broadcom/bt/util/mime4j/message/Entity;
    invoke-virtual {v8}, Lcom/broadcom/bt/util/mime4j/message/Entity;->getMimeType()Ljava/lang/String;

    #@119
    move-result-object v17

    #@11a
    .line 230
    .local v17, mimeType:Ljava/lang/String;
    invoke-virtual {v8}, Lcom/broadcom/bt/util/mime4j/message/Entity;->getHeader()Lcom/broadcom/bt/util/mime4j/message/Header;

    #@11d
    move-result-object v31

    #@11e
    const-string v32, "Content-Disposition"

    #@120
    move-object/from16 v0, p0

    #@122
    move-object/from16 v1, v31

    #@124
    move-object/from16 v2, v32

    #@126
    invoke-direct {v0, v1, v2}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->getHeaderFieldValue(Lcom/broadcom/bt/util/mime4j/message/Header;Ljava/lang/String;)Ljava/lang/String;

    #@129
    move-result-object v6

    #@12a
    .line 231
    .local v6, cd:Ljava/lang/String;
    move-object/from16 v0, p0

    #@12c
    invoke-direct {v0, v6}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->getFilenameFromContentDisposition(Ljava/lang/String;)Ljava/lang/String;

    #@12f
    move-result-object v4

    #@130
    .line 233
    .local v4, attachmentFilename:Ljava/lang/String;
    const-string v31, "BtMap.MmsMessageSender"

    #@132
    new-instance v32, Ljava/lang/StringBuilder;

    #@134
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@137
    const-string v33, "sendOrSaveMms(): MAP Message Entity # "

    #@139
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v32

    #@13d
    move-object/from16 v0, v32

    #@13f
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@142
    move-result-object v32

    #@143
    const-string v33, " mime type = "

    #@145
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@148
    move-result-object v32

    #@149
    move-object/from16 v0, v32

    #@14b
    move-object/from16 v1, v17

    #@14d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v32

    #@151
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@154
    move-result-object v32

    #@155
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@158
    .line 234
    const-string v31, "BtMap.MmsMessageSender"

    #@15a
    new-instance v32, Ljava/lang/StringBuilder;

    #@15c
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@15f
    const-string v33, "sendOrSaveMms(): Content disposition = "

    #@161
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v32

    #@165
    move-object/from16 v0, v32

    #@167
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v32

    #@16b
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16e
    move-result-object v32

    #@16f
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@172
    .line 235
    const-string v31, "BtMap.MmsMessageSender"

    #@174
    new-instance v32, Ljava/lang/StringBuilder;

    #@176
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@179
    const-string v33, "sendOrSaveMms(): Attachment filename = "

    #@17b
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v32

    #@17f
    move-object/from16 v0, v32

    #@181
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@184
    move-result-object v32

    #@185
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@188
    move-result-object v32

    #@189
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18c
    .line 237
    if-nez v4, :cond_253

    #@18e
    .line 238
    if-eqz v17, :cond_213

    #@190
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@193
    move-result-object v31

    #@194
    const-string v32, "text"

    #@196
    invoke-virtual/range {v31 .. v32}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@199
    move-result v31

    #@19a
    if-eqz v31, :cond_213

    #@19c
    .line 239
    invoke-static {v8}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->getTextBody(Lcom/broadcom/bt/util/mime4j/message/Entity;)Ljava/lang/String;

    #@19f
    move-result-object v27

    #@1a0
    .line 240
    .local v27, textContent:Ljava/lang/String;
    if-eqz v27, :cond_1a8

    #@1a2
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->length()I

    #@1a5
    move-result v31

    #@1a6
    if-nez v31, :cond_1b3

    #@1a8
    .line 241
    :cond_1a8
    const-string v31, "BtMap.MmsMessageSender"

    #@1aa
    const-string v32, "sendOrSaveMms(): no inline text content found!"

    #@1ac
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1af
    .line 227
    .end local v27           #textContent:Ljava/lang/String;
    :goto_1af
    add-int/lit8 v11, v11, 0x1

    #@1b1
    goto/16 :goto_10a

    #@1b3
    .line 243
    .restart local v27       #textContent:Ljava/lang/String;
    :cond_1b3
    new-instance v19, Lcom/google/android/mms/pdu/PduPart;

    #@1b5
    .end local v19           #part:Lcom/google/android/mms/pdu/PduPart;
    invoke-direct/range {v19 .. v19}, Lcom/google/android/mms/pdu/PduPart;-><init>()V

    #@1b8
    .line 244
    .restart local v19       #part:Lcom/google/android/mms/pdu/PduPart;
    invoke-virtual {v8}, Lcom/broadcom/bt/util/mime4j/message/Entity;->getCharset()Ljava/lang/String;

    #@1bb
    move-result-object v31

    #@1bc
    invoke-static/range {v31 .. v31}, Lcom/google/android/mms/pdu/CharacterSets;->getMibEnumValue(Ljava/lang/String;)I

    #@1bf
    move-result v31

    #@1c0
    move-object/from16 v0, v19

    #@1c2
    move/from16 v1, v31

    #@1c4
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setCharset(I)V

    #@1c7
    .line 245
    invoke-virtual {v8}, Lcom/broadcom/bt/util/mime4j/message/Entity;->getMimeType()Ljava/lang/String;

    #@1ca
    move-result-object v31

    #@1cb
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->getBytes()[B

    #@1ce
    move-result-object v31

    #@1cf
    move-object/from16 v0, v19

    #@1d1
    move-object/from16 v1, v31

    #@1d3
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentType([B)V

    #@1d6
    .line 246
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->getBytes()[B

    #@1d9
    move-result-object v31

    #@1da
    move-object/from16 v0, v19

    #@1dc
    move-object/from16 v1, v31

    #@1de
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentLocation([B)V

    #@1e1
    .line 247
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->getBytes()[B

    #@1e4
    move-result-object v31

    #@1e5
    move-object/from16 v0, v19

    #@1e7
    move-object/from16 v1, v31

    #@1e9
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentId([B)V

    #@1ec
    .line 248
    invoke-virtual/range {v27 .. v27}, Ljava/lang/String;->getBytes()[B

    #@1ef
    move-result-object v31

    #@1f0
    move-object/from16 v0, v19

    #@1f2
    move-object/from16 v1, v31

    #@1f4
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setData([B)V

    #@1f7
    .line 249
    move-object/from16 v0, v21

    #@1f9
    move-object/from16 v1, v19

    #@1fb
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduBody;->addPart(Lcom/google/android/mms/pdu/PduPart;)Z
    :try_end_1fe
    .catch Ljava/lang/Throwable; {:try_start_3e .. :try_end_1fe} :catch_1ff

    #@1fe
    goto :goto_1af

    #@1ff
    .line 325
    .end local v4           #attachmentFilename:Ljava/lang/String;
    .end local v6           #cd:Ljava/lang/String;
    .end local v7           #encodedNumbers:[Lcom/google/android/mms/pdu/EncodedStringValue;
    .end local v8           #entity:Lcom/broadcom/bt/util/mime4j/message/Entity;
    .end local v11           #i:I
    .end local v17           #mimeType:Ljava/lang/String;
    .end local v19           #part:Lcom/google/android/mms/pdu/PduPart;
    .end local v20           #partCount:I
    .end local v21           #pb:Lcom/google/android/mms/pdu/PduBody;
    .end local v27           #textContent:Ljava/lang/String;
    :catch_1ff
    move-exception v26

    #@200
    move-object/from16 v23, v24

    #@202
    .line 326
    .end local v24           #request:Lcom/google/android/mms/pdu/SendReq;
    .end local v25           #resolver:Landroid/content/ContentResolver;
    .restart local v23       #request:Lcom/google/android/mms/pdu/SendReq;
    .local v26, t:Ljava/lang/Throwable;
    :goto_202
    const-string v31, "BtMap.MmsMessageSender"

    #@204
    const-string v32, "Error saving MMS message"

    #@206
    move-object/from16 v0, v31

    #@208
    move-object/from16 v1, v32

    #@20a
    move-object/from16 v2, v26

    #@20c
    invoke-static {v0, v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@20f
    .line 327
    const/16 v31, 0x0

    #@211
    goto/16 :goto_21

    #@213
    .line 252
    .end local v23           #request:Lcom/google/android/mms/pdu/SendReq;
    .end local v26           #t:Ljava/lang/Throwable;
    .restart local v4       #attachmentFilename:Ljava/lang/String;
    .restart local v6       #cd:Ljava/lang/String;
    .restart local v7       #encodedNumbers:[Lcom/google/android/mms/pdu/EncodedStringValue;
    .restart local v8       #entity:Lcom/broadcom/bt/util/mime4j/message/Entity;
    .restart local v11       #i:I
    .restart local v17       #mimeType:Ljava/lang/String;
    .restart local v19       #part:Lcom/google/android/mms/pdu/PduPart;
    .restart local v20       #partCount:I
    .restart local v21       #pb:Lcom/google/android/mms/pdu/PduBody;
    .restart local v24       #request:Lcom/google/android/mms/pdu/SendReq;
    .restart local v25       #resolver:Landroid/content/ContentResolver;
    :cond_213
    :try_start_213
    const-string v31, "BtMap.MmsMessageSender"

    #@215
    const-string v32, "sendOrSaveMms(): inline non-text content found!"

    #@217
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@21a
    .line 253
    invoke-virtual {v8}, Lcom/broadcom/bt/util/mime4j/message/Entity;->getMimeType()Ljava/lang/String;

    #@21d
    move-result-object v31

    #@21e
    invoke-virtual/range {v31 .. v31}, Ljava/lang/String;->getBytes()[B

    #@221
    move-result-object v31

    #@222
    move-object/from16 v0, v19

    #@224
    move-object/from16 v1, v31

    #@226
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentType([B)V

    #@229
    .line 254
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->getBytes()[B

    #@22c
    move-result-object v31

    #@22d
    move-object/from16 v0, v19

    #@22f
    move-object/from16 v1, v31

    #@231
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentLocation([B)V

    #@234
    .line 255
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->getBytes()[B

    #@237
    move-result-object v31

    #@238
    move-object/from16 v0, v19

    #@23a
    move-object/from16 v1, v31

    #@23c
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentId([B)V

    #@23f
    .line 256
    invoke-static {v8}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->getByteBody(Lcom/broadcom/bt/util/mime4j/message/Entity;)[B

    #@242
    move-result-object v31

    #@243
    move-object/from16 v0, v19

    #@245
    move-object/from16 v1, v31

    #@247
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setData([B)V

    #@24a
    .line 257
    move-object/from16 v0, v21

    #@24c
    move-object/from16 v1, v19

    #@24e
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduBody;->addPart(Lcom/google/android/mms/pdu/PduPart;)Z

    #@251
    goto/16 :goto_1af

    #@253
    .line 260
    :cond_253
    new-instance v31, Ljava/lang/StringBuilder;

    #@255
    invoke-direct/range {v31 .. v31}, Ljava/lang/StringBuilder;-><init>()V

    #@258
    move-object/from16 v0, v31

    #@25a
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25d
    move-result-object v31

    #@25e
    const-string v32, "_MMS_"

    #@260
    invoke-virtual/range {v31 .. v32}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@263
    move-result-object v31

    #@264
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@267
    move-result-wide v32

    #@268
    invoke-virtual/range {v31 .. v33}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@26b
    move-result-object v31

    #@26c
    invoke-virtual/range {v31 .. v31}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26f
    move-result-object v9

    #@270
    .line 263
    .local v9, fileName:Ljava/lang/String;
    new-instance v19, Lcom/google/android/mms/pdu/PduPart;

    #@272
    .end local v19           #part:Lcom/google/android/mms/pdu/PduPart;
    invoke-direct/range {v19 .. v19}, Lcom/google/android/mms/pdu/PduPart;-><init>()V

    #@275
    .line 264
    .restart local v19       #part:Lcom/google/android/mms/pdu/PduPart;
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->getBytes()[B

    #@278
    move-result-object v31

    #@279
    move-object/from16 v0, v19

    #@27b
    move-object/from16 v1, v31

    #@27d
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentType([B)V

    #@280
    .line 265
    invoke-virtual {v8}, Lcom/broadcom/bt/util/mime4j/message/Entity;->getCharset()Ljava/lang/String;

    #@283
    move-result-object v31

    #@284
    invoke-static/range {v31 .. v31}, Lcom/google/android/mms/pdu/CharacterSets;->getMibEnumValue(Ljava/lang/String;)I

    #@287
    move-result v31

    #@288
    move-object/from16 v0, v19

    #@28a
    move/from16 v1, v31

    #@28c
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setCharset(I)V

    #@28f
    .line 266
    const-string v31, "text/plain"

    #@291
    move-object/from16 v0, v31

    #@293
    move-object/from16 v1, v17

    #@295
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@298
    move-result v31

    #@299
    if-nez v31, :cond_2b3

    #@29b
    const-string v31, "application/smil"

    #@29d
    move-object/from16 v0, v31

    #@29f
    move-object/from16 v1, v17

    #@2a1
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a4
    move-result v31

    #@2a5
    if-nez v31, :cond_2b3

    #@2a7
    const-string v31, "text/html"

    #@2a9
    move-object/from16 v0, v31

    #@2ab
    move-object/from16 v1, v17

    #@2ad
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b0
    move-result v31

    #@2b1
    if-eqz v31, :cond_2dd

    #@2b3
    .line 269
    :cond_2b3
    invoke-static {v8}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->getByteBody(Lcom/broadcom/bt/util/mime4j/message/Entity;)[B

    #@2b6
    move-result-object v31

    #@2b7
    move-object/from16 v0, v19

    #@2b9
    move-object/from16 v1, v31

    #@2bb
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setData([B)V

    #@2be
    .line 270
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    #@2c1
    move-result-object v31

    #@2c2
    move-object/from16 v0, v19

    #@2c4
    move-object/from16 v1, v31

    #@2c6
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentLocation([B)V

    #@2c9
    .line 271
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    #@2cc
    move-result-object v31

    #@2cd
    move-object/from16 v0, v19

    #@2cf
    move-object/from16 v1, v31

    #@2d1
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentId([B)V

    #@2d4
    .line 272
    move-object/from16 v0, v21

    #@2d6
    move-object/from16 v1, v19

    #@2d8
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduBody;->addPart(Lcom/google/android/mms/pdu/PduPart;)Z

    #@2db
    goto/16 :goto_1af

    #@2dd
    .line 274
    :cond_2dd
    const/16 v31, 0x1

    #@2df
    move/from16 v0, v31

    #@2e1
    new-array v5, v0, [Landroid/net/Uri;
    :try_end_2e3
    .catch Ljava/lang/Throwable; {:try_start_213 .. :try_end_2e3} :catch_1ff

    #@2e3
    .line 275
    .local v5, attachmentUri:[Landroid/net/Uri;
    const/4 v10, 0x0

    #@2e4
    .line 277
    .local v10, fos:Ljava/io/OutputStream;
    :try_start_2e4
    move-object/from16 v0, p0

    #@2e6
    invoke-virtual {v0, v9, v5}, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->openReadableOutputStream(Ljava/lang/String;[Landroid/net/Uri;)Ljava/io/OutputStream;

    #@2e9
    move-result-object v10

    #@2ea
    .line 279
    const-string v31, "BtMap.MmsMessageSender"

    #@2ec
    new-instance v32, Ljava/lang/StringBuilder;

    #@2ee
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@2f1
    const-string v33, "sendOrSaveMms(): writing attachment "

    #@2f3
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f6
    move-result-object v32

    #@2f7
    move-object/from16 v0, v32

    #@2f9
    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fc
    move-result-object v32

    #@2fd
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@300
    move-result-object v32

    #@301
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@304
    .line 281
    invoke-virtual {v8}, Lcom/broadcom/bt/util/mime4j/message/Entity;->getBody()Lcom/broadcom/bt/util/mime4j/message/Body;

    #@307
    move-result-object v31

    #@308
    move-object/from16 v0, v31

    #@30a
    invoke-interface {v0, v10}, Lcom/broadcom/bt/util/mime4j/message/Body;->writeTo(Ljava/io/OutputStream;)V

    #@30d
    .line 282
    invoke-virtual {v10}, Ljava/io/OutputStream;->flush()V

    #@310
    .line 283
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    #@313
    move-result-object v31

    #@314
    move-object/from16 v0, v19

    #@316
    move-object/from16 v1, v31

    #@318
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentLocation([B)V

    #@31b
    .line 284
    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    #@31e
    move-result-object v31

    #@31f
    move-object/from16 v0, v19

    #@321
    move-object/from16 v1, v31

    #@323
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setContentId([B)V

    #@326
    .line 285
    move-object/from16 v0, v21

    #@328
    move-object/from16 v1, v19

    #@32a
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduBody;->addPart(Lcom/google/android/mms/pdu/PduPart;)Z
    :try_end_32d
    .catch Ljava/lang/Throwable; {:try_start_2e4 .. :try_end_32d} :catch_33d

    #@32d
    .line 289
    :goto_32d
    :try_start_32d
    invoke-static {v10}, Lcom/broadcom/bt/util/io/IOUtils;->closeQuietly(Ljava/io/OutputStream;)V

    #@330
    .line 290
    const/16 v31, 0x0

    #@332
    aget-object v31, v5, v31

    #@334
    move-object/from16 v0, v19

    #@336
    move-object/from16 v1, v31

    #@338
    invoke-virtual {v0, v1}, Lcom/google/android/mms/pdu/PduPart;->setDataUri(Landroid/net/Uri;)V

    #@33b
    goto/16 :goto_1af

    #@33d
    .line 286
    :catch_33d
    move-exception v26

    #@33e
    .line 287
    .restart local v26       #t:Ljava/lang/Throwable;
    const-string v31, "BtMap.MmsMessageSender"

    #@340
    new-instance v32, Ljava/lang/StringBuilder;

    #@342
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@345
    const-string v33, "sendOrSaveMms(): error adding attachment #"

    #@347
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34a
    move-result-object v32

    #@34b
    move-object/from16 v0, v32

    #@34d
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@350
    move-result-object v32

    #@351
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@354
    move-result-object v32

    #@355
    move-object/from16 v0, v31

    #@357
    move-object/from16 v1, v32

    #@359
    move-object/from16 v2, v26

    #@35b
    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@35e
    goto :goto_32d

    #@35f
    .line 303
    .end local v4           #attachmentFilename:Ljava/lang/String;
    .end local v5           #attachmentUri:[Landroid/net/Uri;
    .end local v6           #cd:Ljava/lang/String;
    .end local v8           #entity:Lcom/broadcom/bt/util/mime4j/message/Entity;
    .end local v9           #fileName:Ljava/lang/String;
    .end local v10           #fos:Ljava/io/OutputStream;
    .end local v17           #mimeType:Ljava/lang/String;
    .end local v26           #t:Ljava/lang/Throwable;
    .restart local v3       #addressesSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    :cond_35f
    move-object/from16 v0, p0

    #@361
    iget-object v0, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->mContext:Landroid/content/Context;

    #@363
    move-object/from16 v31, v0

    #@365
    move-object/from16 v0, v31

    #@367
    invoke-static {v0, v3}, Landroid/provider/Telephony$Threads;->getOrCreateThreadId(Landroid/content/Context;Ljava/util/Set;)J

    #@36a
    move-result-wide v28

    #@36b
    .line 306
    .local v28, threadId:J
    new-instance v30, Landroid/content/ContentValues;

    #@36d
    invoke-direct/range {v30 .. v30}, Landroid/content/ContentValues;-><init>()V

    #@370
    .line 307
    .local v30, values:Landroid/content/ContentValues;
    const-string v31, "msg_box"

    #@372
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@375
    move-result-object v32

    #@376
    invoke-virtual/range {v30 .. v32}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@379
    .line 308
    const-string v31, "thread_id"

    #@37b
    invoke-static/range {v28 .. v29}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@37e
    move-result-object v32

    #@37f
    invoke-virtual/range {v30 .. v32}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@382
    .line 309
    const/16 v31, 0x4

    #@384
    move/from16 v0, v31

    #@386
    if-ne v12, v0, :cond_393

    #@388
    .line 310
    const-string v31, "m_type"

    #@38a
    const/16 v32, 0x80

    #@38c
    invoke-static/range {v32 .. v32}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38f
    move-result-object v32

    #@390
    invoke-virtual/range {v30 .. v32}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@393
    .line 312
    :cond_393
    move-object/from16 v0, p0

    #@395
    iget-object v0, v0, Lcom/broadcom/bt/service/map/datasource/mms/MmsMessageSender;->mContext:Landroid/content/Context;

    #@397
    move-object/from16 v31, v0

    #@399
    move-object/from16 v0, v31

    #@39b
    move-object/from16 v1, v25

    #@39d
    move-object/from16 v2, v30

    #@39f
    invoke-static {v0, v1, v13, v2}, Landroid/database/sqlite/SqliteWrapper;->insert(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@3a2
    move-result-object v18

    #@3a3
    .line 314
    .local v18, mmsUri:Landroid/net/Uri;
    if-eqz v18, :cond_3d1

    #@3a5
    .line 316
    invoke-static/range {v18 .. v18}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    #@3a8
    move-result-wide v14

    #@3a9
    .line 317
    const-string v31, "BtMap.MmsMessageSender"

    #@3ab
    new-instance v32, Ljava/lang/StringBuilder;

    #@3ad
    invoke-direct/range {v32 .. v32}, Ljava/lang/StringBuilder;-><init>()V

    #@3b0
    const-string v33, "sendOrSaveMms(): URI = "

    #@3b2
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b5
    move-result-object v32

    #@3b6
    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@3b9
    move-result-object v33

    #@3ba
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3bd
    move-result-object v32

    #@3be
    const-string v33, ", messageId="

    #@3c0
    invoke-virtual/range {v32 .. v33}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c3
    move-result-object v32

    #@3c4
    move-object/from16 v0, v32

    #@3c6
    invoke-virtual {v0, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3c9
    move-result-object v32

    #@3ca
    invoke-virtual/range {v32 .. v32}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3cd
    move-result-object v32

    #@3ce
    invoke-static/range {v31 .. v32}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3d1
    .catch Ljava/lang/Throwable; {:try_start_32d .. :try_end_3d1} :catch_1ff

    #@3d1
    .line 323
    :cond_3d1
    const/16 v31, 0x0

    #@3d3
    move-object/from16 v23, v24

    #@3d5
    .end local v24           #request:Lcom/google/android/mms/pdu/SendReq;
    .restart local v23       #request:Lcom/google/android/mms/pdu/SendReq;
    goto/16 :goto_21

    #@3d7
    .line 325
    .end local v3           #addressesSet:Ljava/util/HashSet;,"Ljava/util/HashSet<Ljava/lang/String;>;"
    .end local v7           #encodedNumbers:[Lcom/google/android/mms/pdu/EncodedStringValue;
    .end local v11           #i:I
    .end local v18           #mmsUri:Landroid/net/Uri;
    .end local v19           #part:Lcom/google/android/mms/pdu/PduPart;
    .end local v20           #partCount:I
    .end local v21           #pb:Lcom/google/android/mms/pdu/PduBody;
    .end local v25           #resolver:Landroid/content/ContentResolver;
    .end local v28           #threadId:J
    .end local v30           #values:Landroid/content/ContentValues;
    :catch_3d7
    move-exception v26

    #@3d8
    goto/16 :goto_202
.end method
