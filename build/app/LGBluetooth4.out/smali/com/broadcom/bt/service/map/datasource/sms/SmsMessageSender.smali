.class public Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;
.super Ljava/lang/Object;
.source "SmsMessageSender.java"


# static fields
.field private static final COLUMN_REPLY_PATH_PRESENT:I = 0x0

.field private static final COLUMN_SERVICE_CENTER:I = 0x1

.field private static final DBG:Z = true

.field private static final SERVICE_CENTER_PROJECTION:[Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "BtMap.SmsMessageSender"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mDests:[Ljava/lang/String;

.field private mMessageText:Ljava/lang/String;

.field private mNumberOfDests:I

.field private mServiceCenter:Ljava/lang/String;

.field private mThreadId:J

.field private mTimestamp:J


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 92
    const/4 v0, 0x2

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "reply_path_present"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "service_center"

    #@b
    aput-object v2, v0, v1

    #@d
    sput-object v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->SERVICE_CENTER_PROJECTION:[Ljava/lang/String;

    #@f
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 2
    .parameter "ctx"

    #@0
    .prologue
    .line 112
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 113
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mContext:Landroid/content/Context;

    #@5
    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;J)V
    .registers 9
    .parameter "context"
    .parameter "dests"
    .parameter "msgText"
    .parameter "threadId"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 100
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 101
    iput-object p1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mContext:Landroid/content/Context;

    #@6
    .line 102
    iput-object p3, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mMessageText:Ljava/lang/String;

    #@8
    .line 103
    array-length v0, p2

    #@9
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mNumberOfDests:I

    #@b
    .line 104
    iget v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mNumberOfDests:I

    #@d
    new-array v0, v0, [Ljava/lang/String;

    #@f
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mDests:[Ljava/lang/String;

    #@11
    .line 105
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mDests:[Ljava/lang/String;

    #@13
    iget v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mNumberOfDests:I

    #@15
    invoke-static {p2, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@18
    .line 106
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@1b
    move-result-wide v0

    #@1c
    iput-wide v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mTimestamp:J

    #@1e
    .line 107
    iput-wide p4, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mThreadId:J

    #@20
    .line 108
    iget-wide v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mThreadId:J

    #@22
    invoke-direct {p0, v0, v1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->getOutgoingServiceCenter(J)Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mServiceCenter:Ljava/lang/String;

    #@28
    .line 109
    return-void
.end method

.method private getOutgoingServiceCenter(J)Ljava/lang/String;
    .registers 14
    .parameter "threadId"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x0

    #@2
    const/4 v8, 0x1

    #@3
    .line 247
    const/4 v7, 0x0

    #@4
    .line 250
    .local v7, cursor:Landroid/database/Cursor;
    :try_start_4
    iget-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mContext:Landroid/content/Context;

    #@6
    iget-object v1, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mContext:Landroid/content/Context;

    #@8
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b
    move-result-object v1

    #@c
    sget-object v2, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    #@e
    sget-object v3, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->SERVICE_CENTER_PROJECTION:[Ljava/lang/String;

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "thread_id = "

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    const/4 v5, 0x0

    #@24
    const-string v6, "date DESC"

    #@26
    invoke-static/range {v0 .. v6}, Lcom/google/android/mms/util/SqliteWrapper;->query(Landroid/content/Context;Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@29
    move-result-object v7

    #@2a
    .line 255
    if-eqz v7, :cond_32

    #@2c
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_2f
    .catchall {:try_start_4 .. :try_end_2f} :catchall_51

    #@2f
    move-result v0

    #@30
    if-nez v0, :cond_39

    #@32
    .line 264
    :cond_32
    if-eqz v7, :cond_37

    #@34
    .line 265
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@37
    :cond_37
    move-object v0, v9

    #@38
    .line 261
    :cond_38
    :goto_38
    return-object v0

    #@39
    .line 259
    :cond_39
    const/4 v0, 0x0

    #@3a
    :try_start_3a
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    #@3d
    move-result v0

    #@3e
    if-ne v8, v0, :cond_4d

    #@40
    .line 261
    .local v8, replyPathPresent:Z
    :goto_40
    if-eqz v8, :cond_4f

    #@42
    const/4 v0, 0x1

    #@43
    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_46
    .catchall {:try_start_3a .. :try_end_46} :catchall_51

    #@46
    move-result-object v0

    #@47
    .line 264
    :goto_47
    if-eqz v7, :cond_38

    #@49
    .line 265
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@4c
    goto :goto_38

    #@4d
    .end local v8           #replyPathPresent:Z
    :cond_4d
    move v8, v10

    #@4e
    .line 259
    goto :goto_40

    #@4f
    .restart local v8       #replyPathPresent:Z
    :cond_4f
    move-object v0, v9

    #@50
    .line 261
    goto :goto_47

    #@51
    .line 264
    .end local v8           #replyPathPresent:Z
    :catchall_51
    move-exception v0

    #@52
    if-eqz v7, :cond_57

    #@54
    .line 265
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@57
    .line 264
    :cond_57
    throw v0
.end method


# virtual methods
.method public sendMessage(J)Landroid/net/Uri;
    .registers 25
    .parameter "token"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/mms/MmsException;
        }
    .end annotation

    #@0
    .prologue
    .line 132
    const/16 v21, 0x0

    #@2
    .line 134
    .local v21, uri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@4
    iget-object v2, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mMessageText:Ljava/lang/String;

    #@6
    if-eqz v2, :cond_e

    #@8
    move-object/from16 v0, p0

    #@a
    iget v2, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mNumberOfDests:I

    #@c
    if-nez v2, :cond_16

    #@e
    .line 136
    :cond_e
    new-instance v2, Lcom/google/android/mms/MmsException;

    #@10
    const-string v3, "Null message body or dest."

    #@12
    invoke-direct {v2, v3}, Lcom/google/android/mms/MmsException;-><init>(Ljava/lang/String;)V

    #@15
    throw v2

    #@16
    .line 139
    :cond_16
    invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;

    #@19
    move-result-object v20

    #@1a
    .line 141
    .local v20, smsManager:Landroid/telephony/SmsManager;
    const/16 v16, 0x0

    #@1c
    .local v16, i:I
    :goto_1c
    move-object/from16 v0, p0

    #@1e
    iget v2, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mNumberOfDests:I

    #@20
    move/from16 v0, v16

    #@22
    if-ge v0, v2, :cond_175

    #@24
    .line 142
    const/4 v11, 0x0

    #@25
    .line 154
    .local v11, messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    #@27
    iget-object v2, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mMessageText:Ljava/lang/String;

    #@29
    move-object/from16 v0, v20

    #@2b
    invoke-virtual {v0, v2}, Landroid/telephony/SmsManager;->divideMessage(Ljava/lang/String;)Ljava/util/ArrayList;

    #@2e
    move-result-object v11

    #@2f
    .line 157
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    #@32
    move-result v19

    #@33
    .line 159
    .local v19, messageCount:I
    const/4 v7, 0x1

    #@34
    .line 161
    .local v7, requestDeliveryReport:Z
    if-nez v19, :cond_59

    #@36
    .line 163
    new-instance v2, Lcom/google/android/mms/MmsException;

    #@38
    new-instance v3, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v4, "SmsMessageSender.sendMessage: divideMessage returned empty messages. Original message is \""

    #@3f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    move-object/from16 v0, p0

    #@45
    iget-object v4, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mMessageText:Ljava/lang/String;

    #@47
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    const-string v4, "\""

    #@4d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v3

    #@51
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v3

    #@55
    invoke-direct {v2, v3}, Lcom/google/android/mms/MmsException;-><init>(Ljava/lang/String;)V

    #@58
    throw v2

    #@59
    .line 169
    :cond_59
    :try_start_59
    move-object/from16 v0, p0

    #@5b
    iget-object v2, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mContext:Landroid/content/Context;

    #@5d
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@60
    move-result-object v2

    #@61
    move-object/from16 v0, p0

    #@63
    iget-object v3, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mDests:[Ljava/lang/String;

    #@65
    aget-object v3, v3, v16

    #@67
    move-object/from16 v0, p0

    #@69
    iget-object v4, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mMessageText:Ljava/lang/String;

    #@6b
    const/4 v5, 0x0

    #@6c
    move-object/from16 v0, p0

    #@6e
    iget-wide v8, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mTimestamp:J

    #@70
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@73
    move-result-object v6

    #@74
    move-object/from16 v0, p0

    #@76
    iget-wide v8, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mThreadId:J

    #@78
    invoke-static/range {v2 .. v9}, Landroid/provider/Telephony$Sms$Outbox;->addMessage(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;ZJ)Landroid/net/Uri;
    :try_end_7b
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_59 .. :try_end_7b} :catch_e2

    #@7b
    move-result-object v21

    #@7c
    .line 176
    :goto_7c
    new-instance v13, Ljava/util/ArrayList;

    #@7e
    move/from16 v0, v19

    #@80
    invoke-direct {v13, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@83
    .line 178
    .local v13, deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    new-instance v12, Ljava/util/ArrayList;

    #@85
    move/from16 v0, v19

    #@87
    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V

    #@8a
    .line 181
    .local v12, sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    const/16 v18, 0x0

    #@8c
    .local v18, j:I
    :goto_8c
    move/from16 v0, v18

    #@8e
    move/from16 v1, v19

    #@90
    if-ge v0, v1, :cond_eb

    #@92
    .line 182
    if-eqz v7, :cond_bd

    #@94
    .line 186
    const-string v2, "BtMap.SmsMessageSender"

    #@96
    const-string v3, "Adding delivery intent..."

    #@98
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9b
    .line 192
    new-instance v17, Landroid/content/Intent;

    #@9d
    const-string v2, "com.broadcom.bt.service.map.sms.MESSAGE_STATUS"

    #@9f
    move-object/from16 v0, v17

    #@a1
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a4
    .line 194
    .local v17, intent:Landroid/content/Intent;
    const-string v2, "MapUri"

    #@a6
    move-object/from16 v0, v17

    #@a8
    move-object/from16 v1, v21

    #@aa
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@ad
    .line 195
    move-object/from16 v0, p0

    #@af
    iget-object v2, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mContext:Landroid/content/Context;

    #@b1
    const/4 v3, 0x0

    #@b2
    const/high16 v4, 0x4000

    #@b4
    move-object/from16 v0, v17

    #@b6
    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@b9
    move-result-object v2

    #@ba
    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@bd
    .line 207
    .end local v17           #intent:Landroid/content/Intent;
    :cond_bd
    new-instance v17, Landroid/content/Intent;

    #@bf
    const-string v2, "com.broadcom.bt.service.map.sms.MESSAGE_SENT"

    #@c1
    move-object/from16 v0, v17

    #@c3
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@c6
    .line 208
    .restart local v17       #intent:Landroid/content/Intent;
    const-string v2, "MapUri"

    #@c8
    move-object/from16 v0, v17

    #@ca
    move-object/from16 v1, v21

    #@cc
    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@cf
    .line 209
    move-object/from16 v0, p0

    #@d1
    iget-object v2, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mContext:Landroid/content/Context;

    #@d3
    const/4 v3, 0x0

    #@d4
    const/high16 v4, 0x4000

    #@d6
    move-object/from16 v0, v17

    #@d8
    invoke-static {v2, v3, v0, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@db
    move-result-object v2

    #@dc
    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@df
    .line 181
    add-int/lit8 v18, v18, 0x1

    #@e1
    goto :goto_8c

    #@e2
    .line 172
    .end local v12           #sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .end local v13           #deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .end local v17           #intent:Landroid/content/Intent;
    .end local v18           #j:I
    :catch_e2
    move-exception v14

    #@e3
    .line 173
    .local v14, e:Landroid/database/sqlite/SQLiteException;
    move-object/from16 v0, p0

    #@e5
    iget-object v2, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mContext:Landroid/content/Context;

    #@e7
    invoke-static {v2, v14}, Lcom/google/android/mms/util/SqliteWrapper;->checkSQLiteException(Landroid/content/Context;Landroid/database/sqlite/SQLiteException;)V

    #@ea
    goto :goto_7c

    #@eb
    .line 216
    .end local v14           #e:Landroid/database/sqlite/SQLiteException;
    .restart local v12       #sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .restart local v13       #deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .restart local v18       #j:I
    :cond_eb
    const-string v2, "BtMap.SmsMessageSender"

    #@ed
    new-instance v3, Ljava/lang/StringBuilder;

    #@ef
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@f2
    const-string v4, "sendMessage(): address["

    #@f4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v3

    #@f8
    move/from16 v0, v16

    #@fa
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v3

    #@fe
    const-string v4, "]="

    #@100
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v3

    #@104
    move-object/from16 v0, p0

    #@106
    iget-object v4, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mDests:[Ljava/lang/String;

    #@108
    aget-object v4, v4, v16

    #@10a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v3

    #@10e
    const-string v4, ", threadId="

    #@110
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v3

    #@114
    move-object/from16 v0, p0

    #@116
    iget-wide v4, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mThreadId:J

    #@118
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v3

    #@11c
    const-string v4, ", uri="

    #@11e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v3

    #@122
    move-object/from16 v0, v21

    #@124
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@127
    move-result-object v3

    #@128
    const-string v4, ", msgs.count="

    #@12a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v3

    #@12e
    move/from16 v0, v19

    #@130
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@133
    move-result-object v3

    #@134
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@137
    move-result-object v3

    #@138
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13b
    .line 222
    :try_start_13b
    move-object/from16 v0, p0

    #@13d
    iget-object v2, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mDests:[Ljava/lang/String;

    #@13f
    aget-object v9, v2, v16

    #@141
    move-object/from16 v0, p0

    #@143
    iget-object v10, v0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mServiceCenter:Ljava/lang/String;

    #@145
    move-object/from16 v8, v20

    #@147
    invoke-virtual/range {v8 .. v13}, Landroid/telephony/SmsManager;->sendMultipartTextMessage(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    :try_end_14a
    .catch Ljava/lang/Exception; {:try_start_13b .. :try_end_14a} :catch_14e

    #@14a
    .line 141
    add-int/lit8 v16, v16, 0x1

    #@14c
    goto/16 :goto_1c

    #@14e
    .line 224
    :catch_14e
    move-exception v15

    #@14f
    .line 225
    .local v15, ex:Ljava/lang/Exception;
    const-string v2, "BtMap.SmsMessageSender"

    #@151
    const-string v3, "sendMessage(): error sending message"

    #@153
    invoke-static {v2, v3, v15}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@156
    .line 226
    new-instance v2, Lcom/google/android/mms/MmsException;

    #@158
    new-instance v3, Ljava/lang/StringBuilder;

    #@15a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@15d
    const-string v4, "SmsMessageSender.sendMessage: caught "

    #@15f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    move-result-object v3

    #@163
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v3

    #@167
    const-string v4, " from SmsManager.sendMultipartTextMessage()"

    #@169
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16c
    move-result-object v3

    #@16d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@170
    move-result-object v3

    #@171
    invoke-direct {v2, v3}, Lcom/google/android/mms/MmsException;-><init>(Ljava/lang/String;)V

    #@174
    throw v2

    #@175
    .line 231
    .end local v7           #requestDeliveryReport:Z
    .end local v11           #messages:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12           #sentIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .end local v13           #deliveryIntents:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/app/PendingIntent;>;"
    .end local v15           #ex:Ljava/lang/Exception;
    .end local v18           #j:I
    .end local v19           #messageCount:I
    :cond_175
    return-object v21
.end method

.method public sendMessage(Ljava/lang/String;Ljava/lang/String;J)Landroid/net/Uri;
    .registers 7
    .parameter "dest"
    .parameter "msgText"
    .parameter "threadId"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/mms/MmsException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 118
    iput-object p2, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mMessageText:Ljava/lang/String;

    #@3
    .line 119
    iput v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mNumberOfDests:I

    #@5
    .line 120
    new-array v0, v0, [Ljava/lang/String;

    #@7
    const/4 v1, 0x0

    #@8
    aput-object p1, v0, v1

    #@a
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mDests:[Ljava/lang/String;

    #@c
    .line 121
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f
    move-result-wide v0

    #@10
    iput-wide v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mTimestamp:J

    #@12
    .line 122
    iput-wide p3, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mThreadId:J

    #@14
    .line 123
    iget-wide v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mThreadId:J

    #@16
    invoke-direct {p0, v0, v1}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->getOutgoingServiceCenter(J)Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    iput-object v0, p0, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->mServiceCenter:Ljava/lang/String;

    #@1c
    .line 124
    invoke-virtual {p0, p3, p4}, Lcom/broadcom/bt/service/map/datasource/sms/SmsMessageSender;->sendMessage(J)Landroid/net/Uri;

    #@1f
    move-result-object v0

    #@20
    return-object v0
.end method
