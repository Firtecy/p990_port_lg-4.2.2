.class Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
.super Ljava/lang/Object;
.source "HealthService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hdp/HealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HealthChannel"
.end annotation


# instance fields
.field private mChannelFd:Landroid/os/ParcelFileDescriptor;

.field private mChannelId:I

.field private mChannelType:I

.field private mConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;

.field private mDevice:Landroid/bluetooth/BluetoothDevice;

.field private mState:I

.field final synthetic this$0:Lcom/android/bluetooth/hdp/HealthService;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V
    .registers 6
    .parameter
    .parameter "device"
    .parameter "config"
    .parameter "channelType"

    #@0
    .prologue
    .line 1058
    iput-object p1, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1059
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mChannelFd:Landroid/os/ParcelFileDescriptor;

    #@8
    .line 1060
    iput-object p2, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mDevice:Landroid/bluetooth/BluetoothDevice;

    #@a
    .line 1061
    iput-object p3, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@c
    .line 1062
    const/4 v0, 0x0

    #@d
    iput v0, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mState:I

    #@f
    .line 1063
    iput p4, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mChannelType:I

    #@11
    .line 1064
    const/4 v0, -0x1

    #@12
    iput v0, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mChannelId:I

    #@14
    .line 1065
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;ILcom/android/bluetooth/hdp/HealthService$1;)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 1048
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;-><init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1048
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1048
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@2
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1048
    iget v0, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mChannelId:I

    #@2
    return v0
.end method

.method static synthetic access$1402(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1048
    iput p1, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mChannelId:I

    #@2
    return p1
.end method

.method static synthetic access$1600(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/os/ParcelFileDescriptor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1048
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mChannelFd:Landroid/os/ParcelFileDescriptor;

    #@2
    return-object v0
.end method

.method static synthetic access$1602(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;Landroid/os/ParcelFileDescriptor;)Landroid/os/ParcelFileDescriptor;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1048
    iput-object p1, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mChannelFd:Landroid/os/ParcelFileDescriptor;

    #@2
    return-object p1
.end method

.method static synthetic access$3300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1048
    iget v0, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mState:I

    #@2
    return v0
.end method

.method static synthetic access$3302(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1048
    iput p1, p0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->mState:I

    #@2
    return p1
.end method
