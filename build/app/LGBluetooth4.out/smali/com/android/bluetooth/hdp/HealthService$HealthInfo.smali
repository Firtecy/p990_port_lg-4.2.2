.class Lcom/android/bluetooth/hdp/HealthService$HealthInfo;
.super Ljava/lang/Object;
.source "HealthService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hdp/HealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HealthInfo"
.end annotation


# instance fields
.field private mConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;

.field private mDevice:Landroid/bluetooth/BluetoothDevice;

.field final synthetic this$0:Lcom/android/bluetooth/hdp/HealthService;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)V
    .registers 4
    .parameter
    .parameter "device"
    .parameter "config"

    #@0
    .prologue
    .line 1095
    iput-object p1, p0, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1096
    iput-object p2, p0, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;->mDevice:Landroid/bluetooth/BluetoothDevice;

    #@7
    .line 1097
    iput-object p3, p0, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;->mConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@9
    .line 1098
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;Lcom/android/bluetooth/hdp/HealthService$1;)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 1090
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;-><init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/bluetooth/hdp/HealthService$HealthInfo;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1090
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;->mDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/bluetooth/hdp/HealthService$HealthInfo;)Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1090
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;->mConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@2
    return-object v0
.end method
