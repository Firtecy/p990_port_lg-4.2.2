.class Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;
.super Ljava/lang/Object;
.source "HealthService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hdp/HealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ChannelStateEvent"
.end annotation


# instance fields
.field mAddr:[B

.field mAppId:I

.field mCfgIndex:I

.field mChannelId:I

.field mFd:Ljava/io/FileDescriptor;

.field mState:I

.field final synthetic this$0:Lcom/android/bluetooth/hdp/HealthService;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/hdp/HealthService;I[BIIILjava/io/FileDescriptor;)V
    .registers 8
    .parameter
    .parameter "appId"
    .parameter "addr"
    .parameter "cfgIndex"
    .parameter "channelId"
    .parameter "state"
    .parameter "fileDescriptor"

    #@0
    .prologue
    .line 1078
    iput-object p1, p0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 1079
    iput p2, p0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mAppId:I

    #@7
    .line 1080
    iput-object p3, p0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mAddr:[B

    #@9
    .line 1081
    iput p4, p0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mCfgIndex:I

    #@b
    .line 1082
    iput p6, p0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mState:I

    #@d
    .line 1083
    iput p5, p0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mChannelId:I

    #@f
    .line 1084
    iput-object p7, p0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mFd:Ljava/io/FileDescriptor;

    #@11
    .line 1085
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/hdp/HealthService;I[BIIILjava/io/FileDescriptor;Lcom/android/bluetooth/hdp/HealthService$1;)V
    .registers 9
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"

    #@0
    .prologue
    .line 1069
    invoke-direct/range {p0 .. p7}, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;-><init>(Lcom/android/bluetooth/hdp/HealthService;I[BIIILjava/io/FileDescriptor;)V

    #@3
    return-void
.end method
