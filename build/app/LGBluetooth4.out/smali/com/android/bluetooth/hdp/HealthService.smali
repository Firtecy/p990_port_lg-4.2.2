.class public Lcom/android/bluetooth/hdp/HealthService;
.super Lcom/android/bluetooth/btservice/ProfileService;
.source "HealthService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/hdp/HealthService$1;,
        Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;,
        Lcom/android/bluetooth/hdp/HealthService$HealthInfo;,
        Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;,
        Lcom/android/bluetooth/hdp/HealthService$HealthChannel;,
        Lcom/android/bluetooth/hdp/HealthService$AppInfo;,
        Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;,
        Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;,
        Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;
    }
.end annotation


# static fields
.field private static final APP_REG_STATE_DEREG_FAILED:I = 0x3

.field private static final APP_REG_STATE_DEREG_SUCCESS:I = 0x2

.field private static final APP_REG_STATE_REG_FAILED:I = 0x1

.field private static final APP_REG_STATE_REG_SUCCESS:I = 0x0

.field private static final CHANNEL_TYPE_ANY:I = 0x2

.field private static final CHANNEL_TYPE_RELIABLE:I = 0x0

.field private static final CHANNEL_TYPE_STREAMING:I = 0x1

.field private static final CONN_STATE_CONNECTED:I = 0x1

.field private static final CONN_STATE_CONNECTING:I = 0x0

.field private static final CONN_STATE_DESTROYED:I = 0x4

.field private static final CONN_STATE_DISCONNECTED:I = 0x3

.field private static final CONN_STATE_DISCONNECTING:I = 0x2

.field private static final DBG:Z = true

.field private static final MDEP_ROLE_SINK:I = 0x1

.field private static final MDEP_ROLE_SOURCE:I = 0x0

.field private static final MESSAGE_APP_REGISTRATION_CALLBACK:I = 0xb

.field private static final MESSAGE_CHANNEL_STATE_CALLBACK:I = 0xc

.field private static final MESSAGE_CONNECT_CHANNEL:I = 0x3

.field private static final MESSAGE_DEVICE_SINK_DATATYPE_CALLBACK:I = 0xe

.field private static final MESSAGE_DEVICE_SOURCE_DATATYPE_CALLBACK:I = 0xd

.field private static final MESSAGE_DISCONNECT_CHANNEL:I = 0x4

.field private static final MESSAGE_GET_DATATYPE:I = 0x5

.field private static final MESSAGE_REGISTER_APPLICATION:I = 0x1

.field private static final MESSAGE_UNREGISTER_APPLICATION:I = 0x2

.field private static final TAG:Ljava/lang/String; = "HealthService"


# instance fields
.field private mApps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/bluetooth/BluetoothHealthAppConfiguration;",
            "Lcom/android/bluetooth/hdp/HealthService$AppInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

.field private mHealthChannels:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/hdp/HealthService$HealthChannel;",
            ">;"
        }
    .end annotation
.end field

.field private mHealthDevices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNativeAvailable:Z


# direct methods
.method static constructor <clinit>()V
    .registers 0

    #@0
    .prologue
    .line 82
    invoke-static {}, Lcom/android/bluetooth/hdp/HealthService;->classInitNative()V

    #@3
    .line 83
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 58
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/ProfileService;-><init>()V

    #@3
    .line 1103
    return-void
.end method

.method static synthetic access$1100(Lcom/android/bluetooth/hdp/HealthService;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->unregisterHealthAppNative(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1500(Lcom/android/bluetooth/hdp/HealthService;[BI)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hdp/HealthService;->connectChannelNative([BI)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V
    .registers 7
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"

    #@0
    .prologue
    .line 58
    invoke-direct/range {p0 .. p6}, Lcom/android/bluetooth/hdp/HealthService;->callHealthChannelCallback(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/android/bluetooth/hdp/HealthService;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->disconnectChannelNative(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/Map;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/bluetooth/hdp/HealthService;[BI)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hdp/HealthService;->getDeviceDatatypeNative([BI)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2200(Lcom/android/bluetooth/hdp/HealthService;I)Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->findAppConfigByAppId(I)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/bluetooth/hdp/HealthService;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->convertHalRegStatus(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2400(Lcom/android/bluetooth/hdp/HealthService;[B)Landroid/bluetooth/BluetoothDevice;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/hdp/HealthService;->callDeviceSourceDataTypeCallback(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/android/bluetooth/hdp/HealthService;[B)Landroid/bluetooth/BluetoothDevice;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/hdp/HealthService;->callDeviceSinkDataTypeCallback(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/bluetooth/hdp/HealthService;I)Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->findChannelById(I)Lcom/android/bluetooth/hdp/HealthService$HealthChannel;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/bluetooth/hdp/HealthService;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->convertHalChannelState(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Lcom/android/bluetooth/hdp/HealthService;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->convertRoleToHal(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3000(Lcom/android/bluetooth/hdp/HealthService;[B)Landroid/bluetooth/BluetoothDevice;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$3200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/List;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthChannels:Ljava/util/List;

    #@2
    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/bluetooth/hdp/HealthService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/android/bluetooth/hdp/HealthService;->isAvailable()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Lcom/android/bluetooth/hdp/HealthService;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->convertChannelTypeToHal(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Lcom/android/bluetooth/hdp/HealthService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->log(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/bluetooth/hdp/HealthService;IILjava/lang/String;I)I
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/bluetooth/hdp/HealthService;->registerHealthAppNative(IILjava/lang/String;I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hdp/HealthService;->callStatusCallback(Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V

    #@3
    return-void
.end method

.method private broadcastHealthDeviceStateChange(Landroid/bluetooth/BluetoothDevice;I)V
    .registers 10
    .parameter "device"
    .parameter "newChannelState"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    .line 847
    iget-object v4, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@3
    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v4

    #@7
    if-nez v4, :cond_13

    #@9
    .line 848
    iget-object v4, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@b
    const/4 v5, 0x0

    #@c
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@f
    move-result-object v5

    #@10
    invoke-interface {v4, p1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@13
    .line 851
    :cond_13
    iget-object v4, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@15
    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v4

    #@19
    check-cast v4, Ljava/lang/Integer;

    #@1b
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@1e
    move-result v1

    #@1f
    .line 852
    .local v1, currDeviceState:I
    invoke-direct {p0, p2}, Lcom/android/bluetooth/hdp/HealthService;->convertState(I)I

    #@22
    move-result v2

    #@23
    .line 854
    .local v2, newDeviceState:I
    if-ne v1, v2, :cond_26

    #@25
    .line 910
    :cond_25
    :goto_25
    return-void

    #@26
    .line 858
    :cond_26
    const/4 v3, 0x0

    #@27
    .line 860
    .local v3, sendIntent:Z
    packed-switch v1, :pswitch_data_6c

    #@2a
    .line 907
    :cond_2a
    :goto_2a
    if-eqz v3, :cond_25

    #@2c
    .line 908
    invoke-direct {p0, p1, v2, v1}, Lcom/android/bluetooth/hdp/HealthService;->updateAndSendIntent(Landroid/bluetooth/BluetoothDevice;II)V

    #@2f
    goto :goto_25

    #@30
    .line 863
    :pswitch_30
    const/4 v3, 0x1

    #@31
    .line 864
    goto :goto_2a

    #@32
    .line 869
    :pswitch_32
    if-ne v2, v6, :cond_36

    #@34
    .line 870
    const/4 v3, 0x1

    #@35
    goto :goto_2a

    #@36
    .line 873
    :cond_36
    new-array v4, v6, [I

    #@38
    fill-array-data v4, :array_78

    #@3b
    invoke-direct {p0, p1, v4}, Lcom/android/bluetooth/hdp/HealthService;->findChannelByStates(Landroid/bluetooth/BluetoothDevice;[I)Ljava/util/List;

    #@3e
    move-result-object v0

    #@3f
    .line 876
    .local v0, chan:Ljava/util/List;,"Ljava/util/List<Lcom/android/bluetooth/hdp/HealthService$HealthChannel;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@42
    move-result v4

    #@43
    if-eqz v4, :cond_2a

    #@45
    .line 877
    const/4 v3, 0x1

    #@46
    goto :goto_2a

    #@47
    .line 885
    .end local v0           #chan:Ljava/util/List;,"Ljava/util/List<Lcom/android/bluetooth/hdp/HealthService$HealthChannel;>;"
    :pswitch_47
    new-array v4, v6, [I

    #@49
    fill-array-data v4, :array_80

    #@4c
    invoke-direct {p0, p1, v4}, Lcom/android/bluetooth/hdp/HealthService;->findChannelByStates(Landroid/bluetooth/BluetoothDevice;[I)Ljava/util/List;

    #@4f
    move-result-object v0

    #@50
    .line 888
    .restart local v0       #chan:Ljava/util/List;,"Ljava/util/List<Lcom/android/bluetooth/hdp/HealthService$HealthChannel;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@53
    move-result v4

    #@54
    if-eqz v4, :cond_2a

    #@56
    .line 889
    const/4 v3, 0x1

    #@57
    goto :goto_2a

    #@58
    .line 897
    .end local v0           #chan:Ljava/util/List;,"Ljava/util/List<Lcom/android/bluetooth/hdp/HealthService$HealthChannel;>;"
    :pswitch_58
    new-array v4, v6, [I

    #@5a
    fill-array-data v4, :array_88

    #@5d
    invoke-direct {p0, p1, v4}, Lcom/android/bluetooth/hdp/HealthService;->findChannelByStates(Landroid/bluetooth/BluetoothDevice;[I)Ljava/util/List;

    #@60
    move-result-object v0

    #@61
    .line 900
    .restart local v0       #chan:Ljava/util/List;,"Ljava/util/List<Lcom/android/bluetooth/hdp/HealthService$HealthChannel;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@64
    move-result v4

    #@65
    if-eqz v4, :cond_2a

    #@67
    .line 901
    invoke-direct {p0, p1, v2, v1}, Lcom/android/bluetooth/hdp/HealthService;->updateAndSendIntent(Landroid/bluetooth/BluetoothDevice;II)V

    #@6a
    goto :goto_2a

    #@6b
    .line 860
    nop

    #@6c
    :pswitch_data_6c
    .packed-switch 0x0
        :pswitch_30
        :pswitch_32
        :pswitch_47
        :pswitch_58
    .end packed-switch

    #@78
    .line 873
    :array_78
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data

    #@80
    .line 885
    :array_80
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x2t 0x0t 0x0t 0x0t
    .end array-data

    #@88
    .line 897
    :array_88
    .array-data 0x4
        0x1t 0x0t 0x0t 0x0t
        0x3t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method private callDeviceSinkDataTypeCallback(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V
    .registers 9
    .parameter "config"
    .parameter "device"
    .parameter "dataTypes"

    #@0
    .prologue
    .line 702
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "Health Device Application: "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/hdp/HealthService;->log(Ljava/lang/String;)V

    #@16
    .line 704
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@18
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@1e
    invoke-static {v2}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$900(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)Landroid/bluetooth/IBluetoothHealthCallback;

    #@21
    move-result-object v0

    #@22
    .line 707
    .local v0, callback:Landroid/bluetooth/IBluetoothHealthCallback;
    if-eqz v0, :cond_42

    #@24
    .line 709
    :try_start_24
    invoke-interface {v0, p1, p2, p3}, Landroid/bluetooth/IBluetoothHealthCallback;->onHealthDeviceSinkDataTypeResult(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_27} :catch_28

    #@27
    .line 728
    :goto_27
    return-void

    #@28
    .line 710
    :catch_28
    move-exception v1

    #@29
    .line 711
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "HealthService"

    #@2b
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v4, "Remote Exception:"

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    goto :goto_27

    #@42
    .line 714
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_42
    const-string v2, "HealthService"

    #@44
    const-string v3, "Callback object null"

    #@46
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_27
.end method

.method private callDeviceSourceDataTypeCallback(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V
    .registers 9
    .parameter "config"
    .parameter "device"
    .parameter "dataTypes"

    #@0
    .prologue
    .line 671
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "Health Device Application: "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/hdp/HealthService;->log(Ljava/lang/String;)V

    #@16
    .line 673
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@18
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@1b
    move-result-object v2

    #@1c
    check-cast v2, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@1e
    invoke-static {v2}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$900(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)Landroid/bluetooth/IBluetoothHealthCallback;

    #@21
    move-result-object v0

    #@22
    .line 676
    .local v0, callback:Landroid/bluetooth/IBluetoothHealthCallback;
    if-eqz v0, :cond_42

    #@24
    .line 678
    :try_start_24
    invoke-interface {v0, p1, p2, p3}, Landroid/bluetooth/IBluetoothHealthCallback;->onHealthDeviceSourceDataTypeResult(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V
    :try_end_27
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_27} :catch_28

    #@27
    .line 697
    :goto_27
    return-void

    #@28
    .line 679
    :catch_28
    move-exception v1

    #@29
    .line 680
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "HealthService"

    #@2b
    new-instance v3, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v4, "Remote Exception:"

    #@32
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    goto :goto_27

    #@42
    .line 683
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_42
    const-string v2, "HealthService"

    #@44
    const-string v3, "Callback object null"

    #@46
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_27
.end method

.method private callHealthChannelCallback(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V
    .registers 15
    .parameter "config"
    .parameter "device"
    .parameter "state"
    .parameter "prevState"
    .parameter "fd"
    .parameter "id"

    #@0
    .prologue
    .line 799
    invoke-direct {p0, p2, p3}, Lcom/android/bluetooth/hdp/HealthService;->broadcastHealthDeviceStateChange(Landroid/bluetooth/BluetoothDevice;I)V

    #@3
    .line 801
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "Health Device Callback: "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v2, " State Change: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    const-string v2, "->"

    #@1e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v1

    #@22
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/hdp/HealthService;->log(Ljava/lang/String;)V

    #@2d
    .line 804
    const/4 v5, 0x0

    #@2e
    .line 805
    .local v5, dupedFd:Landroid/os/ParcelFileDescriptor;
    if-eqz p5, :cond_34

    #@30
    .line 807
    :try_start_30
    invoke-virtual {p5}, Landroid/os/ParcelFileDescriptor;->dup()Landroid/os/ParcelFileDescriptor;
    :try_end_33
    .catch Ljava/io/IOException; {:try_start_30 .. :try_end_33} :catch_5b

    #@33
    move-result-object v5

    #@34
    .line 814
    :cond_34
    :goto_34
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@36
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@39
    move-result-object v1

    #@3a
    check-cast v1, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@3c
    invoke-static {v1}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$900(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)Landroid/bluetooth/IBluetoothHealthCallback;

    #@3f
    move-result-object v0

    #@40
    .line 815
    .local v0, callback:Landroid/bluetooth/IBluetoothHealthCallback;
    if-nez v0, :cond_76

    #@42
    .line 816
    const-string v1, "HealthService"

    #@44
    new-instance v2, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v3, "No callback found for config: "

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v2

    #@57
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 825
    :goto_5a
    return-void

    #@5b
    .line 808
    .end local v0           #callback:Landroid/bluetooth/IBluetoothHealthCallback;
    :catch_5b
    move-exception v7

    #@5c
    .line 809
    .local v7, e:Ljava/io/IOException;
    const/4 v5, 0x0

    #@5d
    .line 810
    const-string v1, "HealthService"

    #@5f
    new-instance v2, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v3, "Exception while duping: "

    #@66
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v2

    #@6a
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v2

    #@6e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v2

    #@72
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    goto :goto_34

    #@76
    .end local v7           #e:Ljava/io/IOException;
    .restart local v0       #callback:Landroid/bluetooth/IBluetoothHealthCallback;
    :cond_76
    move-object v1, p1

    #@77
    move-object v2, p2

    #@78
    move v3, p4

    #@79
    move v4, p3

    #@7a
    move v6, p6

    #@7b
    .line 821
    :try_start_7b
    invoke-interface/range {v0 .. v6}, Landroid/bluetooth/IBluetoothHealthCallback;->onHealthChannelStateChange(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V
    :try_end_7e
    .catch Landroid/os/RemoteException; {:try_start_7b .. :try_end_7e} :catch_7f

    #@7e
    goto :goto_5a

    #@7f
    .line 822
    :catch_7f
    move-exception v7

    #@80
    .line 823
    .local v7, e:Landroid/os/RemoteException;
    const-string v1, "HealthService"

    #@82
    new-instance v2, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v3, "Remote Exception:"

    #@89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v2

    #@8d
    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v2

    #@91
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v2

    #@95
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    goto :goto_5a
.end method

.method private callStatusCallback(Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V
    .registers 8
    .parameter "config"
    .parameter "status"

    #@0
    .prologue
    .line 653
    new-instance v2, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v3, "Health Device Application: "

    #@7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    const-string v3, " State Change: status:"

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/hdp/HealthService;->log(Ljava/lang/String;)V

    #@20
    .line 655
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@22
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    move-result-object v2

    #@26
    check-cast v2, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@28
    invoke-static {v2}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$900(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)Landroid/bluetooth/IBluetoothHealthCallback;

    #@2b
    move-result-object v0

    #@2c
    .line 656
    .local v0, callback:Landroid/bluetooth/IBluetoothHealthCallback;
    if-nez v0, :cond_35

    #@2e
    .line 657
    const-string v2, "HealthService"

    #@30
    const-string v3, "Callback object null"

    #@32
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 661
    :cond_35
    :try_start_35
    invoke-interface {v0, p1, p2}, Landroid/bluetooth/IBluetoothHealthCallback;->onHealthAppConfigurationStatusChange(Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V
    :try_end_38
    .catch Landroid/os/RemoteException; {:try_start_35 .. :try_end_38} :catch_39

    #@38
    .line 665
    :goto_38
    return-void

    #@39
    .line 662
    :catch_39
    move-exception v1

    #@3a
    .line 663
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "HealthService"

    #@3c
    new-instance v3, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v4, "Remote Exception:"

    #@43
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v3

    #@47
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v3

    #@4b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v3

    #@4f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    goto :goto_38
.end method

.method private static native classInitNative()V
.end method

.method private cleanupApps()V
    .registers 8

    #@0
    .prologue
    .line 119
    iget-object v4, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@2
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@5
    move-result-object v4

    #@6
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v3

    #@a
    .local v3, i$:Ljava/util/Iterator;
    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_47

    #@10
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Ljava/util/Map$Entry;

    #@16
    .line 121
    .local v2, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/bluetooth/BluetoothHealthAppConfiguration;Lcom/android/bluetooth/hdp/HealthService$AppInfo;>;"
    :try_start_16
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@19
    move-result-object v0

    #@1a
    check-cast v0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@1c
    .line 122
    .local v0, appInfo:Lcom/android/bluetooth/hdp/HealthService$AppInfo;
    invoke-static {v0}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$100(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)V

    #@1f
    .line 123
    iget-object v4, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@21
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@24
    move-result-object v5

    #@25
    invoke-interface {v4, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_28
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_28} :catch_29

    #@28
    goto :goto_a

    #@29
    .line 124
    .end local v0           #appInfo:Lcom/android/bluetooth/hdp/HealthService$AppInfo;
    :catch_29
    move-exception v1

    #@2a
    .line 125
    .local v1, e:Ljava/lang/Exception;
    const-string v4, "HealthService"

    #@2c
    new-instance v5, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v6, "Failed to cleanup appInfo for appid ="

    #@33
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@3a
    move-result-object v6

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_a

    #@47
    .line 128
    .end local v1           #e:Ljava/lang/Exception;
    .end local v2           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/bluetooth/BluetoothHealthAppConfiguration;Lcom/android/bluetooth/hdp/HealthService$AppInfo;>;"
    :cond_47
    return-void
.end method

.method private native cleanupNative()V
.end method

.method private connectChannel(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z
    .registers 11
    .parameter "device"
    .parameter "config"
    .parameter "channelType"

    #@0
    .prologue
    .line 783
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@2
    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    if-nez v1, :cond_11

    #@8
    .line 784
    const-string v1, "HealthService"

    #@a
    const-string v2, "connectChannel fail to get a app id from config"

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 785
    const/4 v1, 0x0

    #@10
    .line 794
    :goto_10
    return v1

    #@11
    .line 788
    :cond_11
    new-instance v0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;

    #@13
    const/4 v5, 0x0

    #@14
    move-object v1, p0

    #@15
    move-object v2, p1

    #@16
    move-object v3, p2

    #@17
    move v4, p3

    #@18
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;-><init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;ILcom/android/bluetooth/hdp/HealthService$1;)V

    #@1b
    .line 790
    .local v0, chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@1d
    const/4 v2, 0x3

    #@1e
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->obtainMessage(I)Landroid/os/Message;

    #@21
    move-result-object v6

    #@22
    .line 791
    .local v6, msg:Landroid/os/Message;
    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@24
    .line 792
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@26
    invoke-virtual {v1, v6}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->sendMessage(Landroid/os/Message;)Z

    #@29
    .line 794
    const/4 v1, 0x1

    #@2a
    goto :goto_10
.end method

.method private native connectChannelNative([BI)I
.end method

.method private convertChannelTypeToHal(I)I
    .registers 6
    .parameter "channelType"

    #@0
    .prologue
    const/4 v0, 0x2

    #@1
    .line 957
    const/16 v1, 0xa

    #@3
    if-ne p1, v1, :cond_7

    #@5
    .line 958
    const/4 v0, 0x0

    #@6
    .line 967
    :cond_6
    :goto_6
    return v0

    #@7
    .line 960
    :cond_7
    const/16 v1, 0xb

    #@9
    if-ne p1, v1, :cond_d

    #@b
    .line 961
    const/4 v0, 0x1

    #@c
    goto :goto_6

    #@d
    .line 963
    :cond_d
    const/16 v1, 0xc

    #@f
    if-eq p1, v1, :cond_6

    #@11
    .line 966
    const-string v1, "HealthService"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "unkonw channel type: "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_6
.end method

.method private convertHalChannelState(I)I
    .registers 6
    .parameter "halChannelState"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 763
    packed-switch p1, :pswitch_data_24

    #@4
    .line 776
    const-string v1, "HealthService"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Unexpected channel state: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 777
    :goto_1c
    :pswitch_1c
    return v0

    #@1d
    .line 765
    :pswitch_1d
    const/4 v0, 0x2

    #@1e
    goto :goto_1c

    #@1f
    .line 767
    :pswitch_1f
    const/4 v0, 0x1

    #@20
    goto :goto_1c

    #@21
    .line 769
    :pswitch_21
    const/4 v0, 0x3

    #@22
    goto :goto_1c

    #@23
    .line 763
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_1f
        :pswitch_1d
        :pswitch_21
        :pswitch_1c
        :pswitch_1c
    .end packed-switch
.end method

.method private convertHalRegStatus(I)I
    .registers 6
    .parameter "halRegStatus"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 746
    packed-switch p1, :pswitch_data_24

    #@4
    .line 758
    const-string v1, "HealthService"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Unexpected App Registration state: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 759
    :goto_1c
    :pswitch_1c
    return v0

    #@1d
    .line 748
    :pswitch_1d
    const/4 v0, 0x0

    #@1e
    goto :goto_1c

    #@1f
    .line 752
    :pswitch_1f
    const/4 v0, 0x2

    #@20
    goto :goto_1c

    #@21
    .line 754
    :pswitch_21
    const/4 v0, 0x3

    #@22
    goto :goto_1c

    #@23
    .line 746
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1c
        :pswitch_1f
        :pswitch_21
    .end packed-switch
.end method

.method private convertRoleToHal(I)I
    .registers 6
    .parameter "role"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 946
    if-ne p1, v0, :cond_5

    #@3
    .line 947
    const/4 v0, 0x0

    #@4
    .line 953
    :cond_4
    :goto_4
    return v0

    #@5
    .line 949
    :cond_5
    const/4 v1, 0x2

    #@6
    if-eq p1, v1, :cond_4

    #@8
    .line 952
    const-string v1, "HealthService"

    #@a
    new-instance v2, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v3, "unkonw role: "

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    goto :goto_4
.end method

.method private convertState(I)I
    .registers 6
    .parameter "state"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 929
    packed-switch p1, :pswitch_data_24

    #@4
    .line 941
    const-string v1, "HealthService"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Mismatch in Channel and Health Device State: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 942
    :goto_1c
    :pswitch_1c
    return v0

    #@1d
    .line 931
    :pswitch_1d
    const/4 v0, 0x2

    #@1e
    goto :goto_1c

    #@1f
    .line 933
    :pswitch_1f
    const/4 v0, 0x1

    #@20
    goto :goto_1c

    #@21
    .line 935
    :pswitch_21
    const/4 v0, 0x3

    #@22
    goto :goto_1c

    #@23
    .line 929
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_1c
        :pswitch_1f
        :pswitch_1d
        :pswitch_21
    .end packed-switch
.end method

.method private native disconnectChannelNative(I)Z
.end method

.method private findAppConfigByAppId(I)Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .registers 8
    .parameter "appId"

    #@0
    .prologue
    .line 732
    const/4 v0, 0x0

    #@1
    .line 733
    .local v0, appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    iget-object v3, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@3
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    #@6
    move-result-object v3

    #@7
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v2

    #@b
    .local v2, i$:Ljava/util/Iterator;
    :cond_b
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v3

    #@f
    if-eqz v3, :cond_29

    #@11
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Ljava/util/Map$Entry;

    #@17
    .line 734
    .local v1, e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/bluetooth/BluetoothHealthAppConfiguration;Lcom/android/bluetooth/hdp/HealthService$AppInfo;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@1a
    move-result-object v3

    #@1b
    check-cast v3, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@1d
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$1000(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)I

    #@20
    move-result v3

    #@21
    if-ne p1, v3, :cond_b

    #@23
    .line 735
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@26
    move-result-object v0

    #@27
    .end local v0           #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    check-cast v0, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@29
    .line 739
    .end local v1           #e:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Landroid/bluetooth/BluetoothHealthAppConfiguration;Lcom/android/bluetooth/hdp/HealthService$AppInfo;>;"
    .restart local v0       #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :cond_29
    if-nez v0, :cond_43

    #@2b
    .line 740
    const-string v3, "HealthService"

    #@2d
    new-instance v4, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v5, "No appConfig found for "

    #@34
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v4

    #@40
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 742
    :cond_43
    return-object v0
.end method

.method private findChannelById(I)Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    .registers 7
    .parameter "id"

    #@0
    .prologue
    .line 971
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthChannels:Ljava/util/List;

    #@2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@5
    move-result-object v1

    #@6
    .local v1, i$:Ljava/util/Iterator;
    :cond_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_19

    #@c
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;

    #@12
    .line 972
    .local v0, chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    invoke-static {v0}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1400(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I

    #@15
    move-result v2

    #@16
    if-ne v2, p1, :cond_6

    #@18
    .line 977
    .end local v0           #chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    :goto_18
    return-object v0

    #@19
    .line 976
    :cond_19
    const-string v2, "HealthService"

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "No channel found by id: "

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v3

    #@2e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 977
    const/4 v0, 0x0

    #@32
    goto :goto_18
.end method

.method private findChannelByStates(Landroid/bluetooth/BluetoothDevice;[I)Ljava/util/List;
    .registers 11
    .parameter "device"
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/bluetooth/BluetoothDevice;",
            "[I)",
            "Ljava/util/List",
            "<",
            "Lcom/android/bluetooth/hdp/HealthService$HealthChannel;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 981
    new-instance v2, Ljava/util/ArrayList;

    #@2
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 982
    .local v2, channels:Ljava/util/List;,"Ljava/util/List<Lcom/android/bluetooth/hdp/HealthService$HealthChannel;>;"
    iget-object v7, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthChannels:Ljava/util/List;

    #@7
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v3

    #@b
    :cond_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v7

    #@f
    if-eqz v7, :cond_34

    #@11
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;

    #@17
    .line 983
    .local v1, chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    invoke-static {v1}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1200(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothDevice;

    #@1a
    move-result-object v7

    #@1b
    invoke-virtual {v7, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v7

    #@1f
    if-eqz v7, :cond_b

    #@21
    .line 984
    move-object v0, p2

    #@22
    .local v0, arr$:[I
    array-length v5, v0

    #@23
    .local v5, len$:I
    const/4 v4, 0x0

    #@24
    .local v4, i$:I
    :goto_24
    if-ge v4, v5, :cond_b

    #@26
    aget v6, v0, v4

    #@28
    .line 985
    .local v6, state:I
    invoke-static {v1}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$3300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I

    #@2b
    move-result v7

    #@2c
    if-ne v7, v6, :cond_31

    #@2e
    .line 986
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@31
    .line 984
    :cond_31
    add-int/lit8 v4, v4, 0x1

    #@33
    goto :goto_24

    #@34
    .line 991
    .end local v0           #arr$:[I
    .end local v1           #chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    .end local v4           #i$:I
    .end local v5           #len$:I
    .end local v6           #state:I
    :cond_34
    return-object v2
.end method

.method private getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 995
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    if-nez v0, :cond_a

    #@8
    .line 996
    const/4 v0, 0x0

    #@9
    .line 998
    :goto_9
    return v0

    #@a
    :cond_a
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@c
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Ljava/lang/Integer;

    #@12
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@15
    move-result v0

    #@16
    goto :goto_9
.end method

.method private native getDeviceDatatypeNative([BI)Z
.end method

.method private getStringChannelType(I)Ljava/lang/String;
    .registers 3
    .parameter "type"

    #@0
    .prologue
    .line 642
    const/16 v0, 0xa

    #@2
    if-ne p1, v0, :cond_7

    #@4
    .line 643
    const-string v0, "Reliable"

    #@6
    .line 647
    :goto_6
    return-object v0

    #@7
    .line 644
    :cond_7
    const/16 v0, 0xb

    #@9
    if-ne p1, v0, :cond_e

    #@b
    .line 645
    const-string v0, "Streaming"

    #@d
    goto :goto_6

    #@e
    .line 647
    :cond_e
    const-string v0, "Any"

    #@10
    goto :goto_6
.end method

.method private native initializeNative()V
.end method

.method private onAppRegistrationState(II)V
    .registers 6
    .parameter "appId"
    .parameter "state"

    #@0
    .prologue
    .line 607
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@2
    const/16 v2, 0xb

    #@4
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 608
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    #@a
    .line 609
    iput p2, v0, Landroid/os/Message;->arg2:I

    #@c
    .line 610
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@e
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 611
    return-void
.end method

.method private onChannelStateChanged(I[BIIILjava/io/FileDescriptor;)V
    .registers 17
    .parameter "appId"
    .parameter "addr"
    .parameter "cfgIndex"
    .parameter "channelId"
    .parameter "state"
    .parameter "pfd"

    #@0
    .prologue
    .line 615
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@2
    const/16 v2, 0xc

    #@4
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v9

    #@8
    .line 616
    .local v9, msg:Landroid/os/Message;
    new-instance v0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;

    #@a
    const/4 v8, 0x0

    #@b
    move-object v1, p0

    #@c
    move v2, p1

    #@d
    move-object v3, p2

    #@e
    move v4, p3

    #@f
    move v5, p4

    #@10
    move v6, p5

    #@11
    move-object/from16 v7, p6

    #@13
    invoke-direct/range {v0 .. v8}, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;-><init>(Lcom/android/bluetooth/hdp/HealthService;I[BIIILjava/io/FileDescriptor;Lcom/android/bluetooth/hdp/HealthService$1;)V

    #@16
    .line 618
    .local v0, channelStateEvent:Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;
    iput-object v0, v9, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18
    .line 619
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@1a
    invoke-virtual {v1, v9}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->sendMessage(Landroid/os/Message;)Z

    #@1d
    .line 620
    return-void
.end method

.method private onDeviceSinkDataType(I[B[I)V
    .registers 11
    .parameter "appId"
    .parameter "addr"
    .parameter "dataTypes"

    #@0
    .prologue
    .line 633
    const-string v1, "HealthService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "onDeviceSinkDataType  appid ="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 634
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@1a
    const/16 v2, 0xe

    #@1c
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->obtainMessage(I)Landroid/os/Message;

    #@1f
    move-result-object v6

    #@20
    .line 635
    .local v6, msg:Landroid/os/Message;
    new-instance v0, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;

    #@22
    const/4 v5, 0x0

    #@23
    move-object v1, p0

    #@24
    move v2, p1

    #@25
    move-object v3, p2

    #@26
    move-object v4, p3

    #@27
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;-><init>(Lcom/android/bluetooth/hdp/HealthService;I[B[ILcom/android/bluetooth/hdp/HealthService$1;)V

    #@2a
    .line 636
    .local v0, deviceConfig:Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;
    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2c
    .line 637
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@2e
    invoke-virtual {v1, v6}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->sendMessage(Landroid/os/Message;)Z

    #@31
    .line 638
    return-void
.end method

.method private onDeviceSourceDataType(I[B[I)V
    .registers 11
    .parameter "appId"
    .parameter "addr"
    .parameter "dataTypes"

    #@0
    .prologue
    .line 624
    const-string v1, "HealthService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "onDeviceSourceDataType  appid ="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 625
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@1a
    const/16 v2, 0xd

    #@1c
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->obtainMessage(I)Landroid/os/Message;

    #@1f
    move-result-object v6

    #@20
    .line 626
    .local v6, msg:Landroid/os/Message;
    new-instance v0, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;

    #@22
    const/4 v5, 0x0

    #@23
    move-object v1, p0

    #@24
    move v2, p1

    #@25
    move-object v3, p2

    #@26
    move-object v4, p3

    #@27
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;-><init>(Lcom/android/bluetooth/hdp/HealthService;I[B[ILcom/android/bluetooth/hdp/HealthService$1;)V

    #@2a
    .line 627
    .local v0, deviceConfig:Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;
    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2c
    .line 628
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@2e
    invoke-virtual {v1, v6}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->sendMessage(Landroid/os/Message;)Z

    #@31
    .line 630
    return-void
.end method

.method private native registerHealthAppNative(IILjava/lang/String;I)I
.end method

.method private native unregisterHealthAppNative(I)Z
.end method

.method private updateAndSendIntent(Landroid/bluetooth/BluetoothDevice;II)V
    .registers 6
    .parameter "device"
    .parameter "newDeviceState"
    .parameter "prevDeviceState"

    #@0
    .prologue
    .line 914
    if-nez p2, :cond_c

    #@2
    .line 915
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@4
    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@7
    .line 919
    :goto_7
    const/4 v0, 0x3

    #@8
    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/android/bluetooth/hdp/HealthService;->notifyProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V

    #@b
    .line 920
    return-void

    #@c
    .line 917
    :cond_c
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11
    move-result-object v1

    #@12
    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15
    goto :goto_7
.end method


# virtual methods
.method protected cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 130
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@3
    .line 132
    iget-boolean v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mNativeAvailable:Z

    #@5
    if-eqz v0, :cond_d

    #@7
    .line 133
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService;->cleanupNative()V

    #@a
    .line 134
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mNativeAvailable:Z

    #@d
    .line 136
    :cond_d
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthChannels:Ljava/util/List;

    #@f
    if-eqz v0, :cond_16

    #@11
    .line 137
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthChannels:Ljava/util/List;

    #@13
    invoke-interface {v0}, Ljava/util/List;->clear()V

    #@16
    .line 139
    :cond_16
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@18
    if-eqz v0, :cond_1f

    #@1a
    .line 140
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@1c
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    #@1f
    .line 142
    :cond_1f
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@21
    if-eqz v0, :cond_28

    #@23
    .line 143
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@25
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    #@28
    .line 145
    :cond_28
    const/4 v0, 0x1

    #@29
    return v0
.end method

.method connectChannelToSink(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z
    .registers 6
    .parameter "device"
    .parameter "config"
    .parameter "channelType"

    #@0
    .prologue
    .line 536
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hdp/HealthService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 537
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/hdp/HealthService;->connectChannel(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method connectChannelToSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    .registers 5
    .parameter "device"
    .parameter "config"

    #@0
    .prologue
    .line 530
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hdp/HealthService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 531
    const/16 v0, 0xc

    #@9
    invoke-direct {p0, p1, p2, v0}, Lcom/android/bluetooth/hdp/HealthService;->connectChannel(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method disconnectChannel(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z
    .registers 8
    .parameter "device"
    .parameter "config"
    .parameter "channelId"

    #@0
    .prologue
    .line 542
    const-string v2, "android.permission.BLUETOOTH"

    #@2
    const-string v3, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/hdp/HealthService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 543
    invoke-direct {p0, p3}, Lcom/android/bluetooth/hdp/HealthService;->findChannelById(I)Lcom/android/bluetooth/hdp/HealthService$HealthChannel;

    #@a
    move-result-object v0

    #@b
    .line 544
    .local v0, chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    if-nez v0, :cond_16

    #@d
    .line 546
    const-string v2, "HealthService"

    #@f
    const-string v3, "disconnectChannel: no channel found"

    #@11
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 548
    const/4 v2, 0x0

    #@15
    .line 552
    :goto_15
    return v2

    #@16
    .line 550
    :cond_16
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@18
    const/4 v3, 0x4

    #@19
    invoke-virtual {v2, v3, v0}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1c
    move-result-object v1

    #@1d
    .line 551
    .local v1, msg:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@1f
    invoke-virtual {v2, v1}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->sendMessage(Landroid/os/Message;)Z

    #@22
    .line 552
    const/4 v2, 0x1

    #@23
    goto :goto_15
.end method

.method getConnectedHealthDevices()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 594
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/hdp/HealthService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 595
    const/4 v1, 0x1

    #@8
    new-array v1, v1, [I

    #@a
    const/4 v2, 0x0

    #@b
    const/4 v3, 0x2

    #@c
    aput v3, v1, v2

    #@e
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/hdp/HealthService;->lookupHealthDevicesMatchingStates([I)Ljava/util/List;

    #@11
    move-result-object v0

    #@12
    .line 597
    .local v0, devices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    return-object v0
.end method

.method getDeviceDatatype(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    .registers 7
    .parameter "device"
    .parameter "config"

    #@0
    .prologue
    .line 558
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@2
    invoke-interface {v2, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    if-nez v2, :cond_11

    #@8
    .line 559
    const-string v2, "HealthService"

    #@a
    const-string v3, "getDeviceDatatype fail to get a app id from config"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 560
    const/4 v2, 0x0

    #@10
    .line 568
    :goto_10
    return v2

    #@11
    .line 563
    :cond_11
    new-instance v0, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;

    #@13
    const/4 v2, 0x0

    #@14
    invoke-direct {v0, p0, p1, p2, v2}, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;-><init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;Lcom/android/bluetooth/hdp/HealthService$1;)V

    #@17
    .line 565
    .local v0, info:Lcom/android/bluetooth/hdp/HealthService$HealthInfo;
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@19
    const/4 v3, 0x5

    #@1a
    invoke-virtual {v2, v3}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->obtainMessage(I)Landroid/os/Message;

    #@1d
    move-result-object v1

    #@1e
    .line 566
    .local v1, msg:Landroid/os/Message;
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@20
    .line 567
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@22
    invoke-virtual {v2, v1}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->sendMessage(Landroid/os/Message;)Z

    #@25
    .line 568
    const/4 v2, 0x1

    #@26
    goto :goto_10
.end method

.method getHealthDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 589
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hdp/HealthService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 590
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@a
    move-result v0

    #@b
    return v0
.end method

.method getHealthDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 5
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 601
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/hdp/HealthService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 602
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hdp/HealthService;->lookupHealthDevicesMatchingStates([I)Ljava/util/List;

    #@a
    move-result-object v0

    #@b
    .line 603
    .local v0, devices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    return-object v0
.end method

.method getMainChannelFd(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/os/ParcelFileDescriptor;
    .registers 9
    .parameter "device"
    .parameter "config"

    #@0
    .prologue
    .line 574
    const-string v3, "android.permission.BLUETOOTH"

    #@2
    const-string v4, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v3, v4}, Lcom/android/bluetooth/hdp/HealthService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 575
    const/4 v1, 0x0

    #@8
    .line 576
    .local v1, healthChan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    iget-object v3, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthChannels:Ljava/util/List;

    #@a
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v2

    #@e
    .local v2, i$:Ljava/util/Iterator;
    :cond_e
    :goto_e
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v3

    #@12
    if-eqz v3, :cond_30

    #@14
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v0

    #@18
    check-cast v0, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;

    #@1a
    .line 577
    .local v0, chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    invoke-static {v0}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1200(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothDevice;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v3

    #@22
    if-eqz v3, :cond_e

    #@24
    invoke-static {v0}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@27
    move-result-object v3

    #@28
    invoke-virtual {v3, p2}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v3

    #@2c
    if-eqz v3, :cond_e

    #@2e
    .line 578
    move-object v1, v0

    #@2f
    goto :goto_e

    #@30
    .line 581
    .end local v0           #chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    :cond_30
    if-nez v1, :cond_56

    #@32
    .line 582
    const-string v3, "HealthService"

    #@34
    new-instance v4, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v5, "No channel found for device: "

    #@3b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v4

    #@3f
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    const-string v5, " config: "

    #@45
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v4

    #@49
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v4

    #@4d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 583
    const/4 v3, 0x0

    #@55
    .line 585
    :goto_55
    return-object v3

    #@56
    :cond_56
    invoke-static {v1}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1600(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/os/ParcelFileDescriptor;

    #@59
    move-result-object v3

    #@5a
    goto :goto_55
.end method

.method protected getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 86
    const-string v0, "HealthService"

    #@2
    return-object v0
.end method

.method protected initBinder()Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;
    .registers 2

    #@0
    .prologue
    .line 90
    new-instance v0, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;

    #@2
    invoke-direct {v0, p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;-><init>(Lcom/android/bluetooth/hdp/HealthService;)V

    #@5
    return-object v0
.end method

.method lookupHealthDevicesMatchingStates([I)Ljava/util/List;
    .registers 11
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1002
    new-instance v3, Ljava/util/ArrayList;

    #@2
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1004
    .local v3, healthDevices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    iget-object v8, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@7
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@a
    move-result-object v8

    #@b
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v4

    #@f
    :cond_f
    :goto_f
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v8

    #@13
    if-eqz v8, :cond_2f

    #@15
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@1b
    .line 1005
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hdp/HealthService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@1e
    move-result v2

    #@1f
    .line 1006
    .local v2, healthDeviceState:I
    move-object v0, p1

    #@20
    .local v0, arr$:[I
    array-length v6, v0

    #@21
    .local v6, len$:I
    const/4 v5, 0x0

    #@22
    .local v5, i$:I
    :goto_22
    if-ge v5, v6, :cond_f

    #@24
    aget v7, v0, v5

    #@26
    .line 1007
    .local v7, state:I
    if-ne v7, v2, :cond_2c

    #@28
    .line 1008
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@2b
    goto :goto_f

    #@2c
    .line 1006
    :cond_2c
    add-int/lit8 v5, v5, 0x1

    #@2e
    goto :goto_22

    #@2f
    .line 1013
    .end local v0           #arr$:[I
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v2           #healthDeviceState:I
    .end local v5           #i$:I
    .end local v6           #len$:I
    .end local v7           #state:I
    :cond_2f
    return-object v3
.end method

.method registerAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/IBluetoothHealthCallback;)Z
    .registers 8
    .parameter "config"
    .parameter "callback"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 501
    const-string v2, "android.permission.BLUETOOTH"

    #@3
    const-string v3, "Need BLUETOOTH permission"

    #@5
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/hdp/HealthService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 503
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@a
    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@d
    move-result-object v2

    #@e
    if-eqz v2, :cond_19

    #@10
    .line 505
    const-string v1, "HealthService"

    #@12
    const-string v2, "Config has already been registered"

    #@14
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 507
    const/4 v1, 0x0

    #@18
    .line 512
    :goto_18
    return v1

    #@19
    .line 509
    :cond_19
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@1b
    new-instance v3, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@1d
    const/4 v4, 0x0

    #@1e
    invoke-direct {v3, p2, v4}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;-><init>(Landroid/bluetooth/IBluetoothHealthCallback;Lcom/android/bluetooth/hdp/HealthService$1;)V

    #@21
    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@24
    .line 510
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@26
    invoke-virtual {v2, v1, p1}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@29
    move-result-object v0

    #@2a
    .line 511
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@2c
    invoke-virtual {v2, v0}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->sendMessage(Landroid/os/Message;)Z

    #@2f
    goto :goto_18
.end method

.method protected start()Z
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 94
    new-instance v2, Ljava/util/ArrayList;

    #@3
    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    #@6
    invoke-static {v2}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    #@9
    move-result-object v2

    #@a
    iput-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthChannels:Ljava/util/List;

    #@c
    .line 95
    new-instance v2, Ljava/util/HashMap;

    #@e
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@11
    invoke-static {v2}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    #@14
    move-result-object v2

    #@15
    iput-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@17
    .line 97
    new-instance v2, Ljava/util/HashMap;

    #@19
    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    #@1c
    invoke-static {v2}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    #@1f
    move-result-object v2

    #@20
    iput-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mHealthDevices:Ljava/util/Map;

    #@22
    .line 99
    new-instance v1, Landroid/os/HandlerThread;

    #@24
    const-string v2, "BluetoothHdpHandler"

    #@26
    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    #@29
    .line 100
    .local v1, thread:Landroid/os/HandlerThread;
    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    #@2c
    .line 101
    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    #@2f
    move-result-object v0

    #@30
    .line 102
    .local v0, looper:Landroid/os/Looper;
    new-instance v2, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@32
    const/4 v3, 0x0

    #@33
    invoke-direct {v2, p0, v0, v3}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;-><init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/os/Looper;Lcom/android/bluetooth/hdp/HealthService$1;)V

    #@36
    iput-object v2, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@38
    .line 103
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService;->initializeNative()V

    #@3b
    .line 104
    iput-boolean v4, p0, Lcom/android/bluetooth/hdp/HealthService;->mNativeAvailable:Z

    #@3d
    .line 105
    return v4
.end method

.method protected stop()Z
    .registers 4

    #@0
    .prologue
    .line 109
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@2
    const/4 v2, 0x0

    #@3
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@6
    .line 110
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@8
    invoke-virtual {v1}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->getLooper()Landroid/os/Looper;

    #@b
    move-result-object v0

    #@c
    .line 111
    .local v0, looper:Landroid/os/Looper;
    if-eqz v0, :cond_11

    #@e
    .line 112
    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    #@11
    .line 114
    :cond_11
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService;->cleanupApps()V

    #@14
    .line 115
    const/4 v1, 0x1

    #@15
    return v1
.end method

.method unregisterAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    .registers 5
    .parameter "config"

    #@0
    .prologue
    .line 516
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/hdp/HealthService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 517
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mApps:Ljava/util/Map;

    #@9
    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v1

    #@d
    if-nez v1, :cond_18

    #@f
    .line 519
    const-string v1, "HealthService"

    #@11
    const-string v2, "unregisterAppConfiguration: no app found"

    #@13
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 521
    const/4 v1, 0x0

    #@17
    .line 525
    :goto_17
    return v1

    #@18
    .line 523
    :cond_18
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@1a
    const/4 v2, 0x2

    #@1b
    invoke-virtual {v1, v2, p1}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@1e
    move-result-object v0

    #@1f
    .line 524
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService;->mHandler:Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;

    #@21
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->sendMessage(Landroid/os/Message;)Z

    #@24
    .line 525
    const/4 v1, 0x1

    #@25
    goto :goto_17
.end method
