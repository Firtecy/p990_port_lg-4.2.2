.class final Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;
.super Landroid/os/Handler;
.source "HealthService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hdp/HealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HealthServiceMessageHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/hdp/HealthService;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 149
    iput-object p1, p0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2
    .line 150
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 151
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/os/Looper;Lcom/android/bluetooth/hdp/HealthService$1;)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 148
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;-><init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/os/Looper;)V

    #@3
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 28
    .parameter "msg"

    #@0
    .prologue
    .line 155
    move-object/from16 v0, p1

    #@2
    iget v3, v0, Landroid/os/Message;->what:I

    #@4
    packed-switch v3, :pswitch_data_370

    #@7
    .line 359
    :cond_7
    :goto_7
    :pswitch_7
    return-void

    #@8
    .line 158
    :pswitch_8
    move-object/from16 v0, p1

    #@a
    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@c
    check-cast v5, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@e
    .line 160
    .local v5, appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    move-object/from16 v0, p0

    #@10
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@12
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService;->access$200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/Map;

    #@15
    move-result-object v3

    #@16
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@19
    move-result-object v14

    #@1a
    check-cast v14, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@1c
    .line 161
    .local v14, appInfo:Lcom/android/bluetooth/hdp/HealthService$AppInfo;
    if-eqz v14, :cond_7

    #@1e
    .line 164
    move-object/from16 v0, p0

    #@20
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@22
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getRole()I

    #@25
    move-result v6

    #@26
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$300(Lcom/android/bluetooth/hdp/HealthService;I)I

    #@29
    move-result v22

    #@2a
    .line 165
    .local v22, halRole:I
    move-object/from16 v0, p0

    #@2c
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2e
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getChannelType()I

    #@31
    move-result v6

    #@32
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$400(Lcom/android/bluetooth/hdp/HealthService;I)I

    #@35
    move-result v21

    #@36
    .line 167
    .local v21, halChannelType:I
    move-object/from16 v0, p0

    #@38
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@3a
    new-instance v6, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v7, "register datatype: "

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getDataType()I

    #@48
    move-result v7

    #@49
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    const-string v7, " role: "

    #@4f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    move/from16 v0, v22

    #@55
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v6

    #@59
    const-string v7, " name: "

    #@5b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v6

    #@5f
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getName()Ljava/lang/String;

    #@62
    move-result-object v7

    #@63
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v6

    #@67
    const-string v7, " channeltype: "

    #@69
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v6

    #@6d
    move/from16 v0, v21

    #@6f
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v6

    #@77
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$500(Lcom/android/bluetooth/hdp/HealthService;Ljava/lang/String;)V

    #@7a
    .line 171
    move-object/from16 v0, p0

    #@7c
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@7e
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getDataType()I

    #@81
    move-result v6

    #@82
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getName()Ljava/lang/String;

    #@85
    move-result-object v7

    #@86
    move/from16 v0, v22

    #@88
    move/from16 v1, v21

    #@8a
    invoke-static {v3, v6, v0, v7, v1}, Lcom/android/bluetooth/hdp/HealthService;->access$600(Lcom/android/bluetooth/hdp/HealthService;IILjava/lang/String;I)I

    #@8d
    move-result v13

    #@8e
    .line 173
    .local v13, appId:I
    const/4 v3, -0x1

    #@8f
    if-ne v13, v3, :cond_a9

    #@91
    .line 174
    move-object/from16 v0, p0

    #@93
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@95
    const/4 v6, 0x1

    #@96
    invoke-static {v3, v5, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V

    #@99
    .line 176
    invoke-static {v14}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$100(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)V

    #@9c
    .line 177
    move-object/from16 v0, p0

    #@9e
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@a0
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService;->access$200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/Map;

    #@a3
    move-result-object v3

    #@a4
    invoke-interface {v3, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@a7
    goto/16 :goto_7

    #@a9
    .line 180
    :cond_a9
    new-instance v3, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@ab
    move-object/from16 v0, p0

    #@ad
    iget-object v6, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@af
    invoke-direct {v3, v6, v5}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;-><init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;)V

    #@b2
    invoke-static {v14, v3}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$802(Lcom/android/bluetooth/hdp/HealthService$AppInfo;Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;)Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@b5
    .line 181
    invoke-static {v14}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$900(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)Landroid/bluetooth/IBluetoothHealthCallback;

    #@b8
    move-result-object v3

    #@b9
    invoke-interface {v3}, Landroid/bluetooth/IBluetoothHealthCallback;->asBinder()Landroid/os/IBinder;

    #@bc
    move-result-object v15

    #@bd
    .line 183
    .local v15, binder:Landroid/os/IBinder;
    :try_start_bd
    invoke-static {v14}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$800(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@c0
    move-result-object v3

    #@c1
    const/4 v6, 0x0

    #@c2
    invoke-interface {v15, v3, v6}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_c5
    .catch Landroid/os/RemoteException; {:try_start_bd .. :try_end_c5} :catch_ca

    #@c5
    .line 187
    :goto_c5
    invoke-static {v14, v13}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$1002(Lcom/android/bluetooth/hdp/HealthService$AppInfo;I)I

    #@c8
    goto/16 :goto_7

    #@ca
    .line 184
    :catch_ca
    move-exception v20

    #@cb
    .line 185
    .local v20, e:Landroid/os/RemoteException;
    const-string v3, "HealthService"

    #@cd
    new-instance v6, Ljava/lang/StringBuilder;

    #@cf
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@d2
    const-string v7, "LinktoDeath Exception:"

    #@d4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v6

    #@d8
    move-object/from16 v0, v20

    #@da
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v6

    #@de
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v6

    #@e2
    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    goto :goto_c5

    #@e6
    .line 199
    .end local v5           #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v13           #appId:I
    .end local v14           #appInfo:Lcom/android/bluetooth/hdp/HealthService$AppInfo;
    .end local v15           #binder:Landroid/os/IBinder;
    .end local v20           #e:Landroid/os/RemoteException;
    .end local v21           #halChannelType:I
    .end local v22           #halRole:I
    :pswitch_e6
    move-object/from16 v0, p1

    #@e8
    iget-object v5, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ea
    check-cast v5, Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@ec
    .line 201
    .restart local v5       #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    move-object/from16 v0, p0

    #@ee
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@f0
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService;->access$200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/Map;

    #@f3
    move-result-object v3

    #@f4
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f7
    move-result-object v3

    #@f8
    check-cast v3, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@fa
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$1000(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)I

    #@fd
    move-result v13

    #@fe
    .line 202
    .restart local v13       #appId:I
    move-object/from16 v0, p0

    #@100
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@102
    invoke-static {v3, v13}, Lcom/android/bluetooth/hdp/HealthService;->access$1100(Lcom/android/bluetooth/hdp/HealthService;I)Z

    #@105
    move-result v3

    #@106
    if-nez v3, :cond_7

    #@108
    .line 203
    const-string v3, "HealthService"

    #@10a
    new-instance v6, Ljava/lang/StringBuilder;

    #@10c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@10f
    const-string v7, "Failed to unregister application: id: "

    #@111
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v6

    #@115
    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@118
    move-result-object v6

    #@119
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v6

    #@11d
    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@120
    .line 204
    move-object/from16 v0, p0

    #@122
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@124
    const/4 v6, 0x3

    #@125
    invoke-static {v3, v5, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V

    #@128
    goto/16 :goto_7

    #@12a
    .line 211
    .end local v5           #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v13           #appId:I
    :pswitch_12a
    move-object/from16 v0, p1

    #@12c
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12e
    move-object/from16 v16, v0

    #@130
    check-cast v16, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;

    #@132
    .line 212
    .local v16, chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1200(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothDevice;

    #@135
    move-result-object v3

    #@136
    invoke-static {v3}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@139
    move-result-object v18

    #@13a
    .line 213
    .local v18, devAddr:[B
    move-object/from16 v0, p0

    #@13c
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@13e
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService;->access$200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/Map;

    #@141
    move-result-object v3

    #@142
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@145
    move-result-object v6

    #@146
    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@149
    move-result-object v3

    #@14a
    check-cast v3, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@14c
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$1000(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)I

    #@14f
    move-result v13

    #@150
    .line 214
    .restart local v13       #appId:I
    move-object/from16 v0, p0

    #@152
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@154
    move-object/from16 v0, v18

    #@156
    invoke-static {v3, v0, v13}, Lcom/android/bluetooth/hdp/HealthService;->access$1500(Lcom/android/bluetooth/hdp/HealthService;[BI)I

    #@159
    move-result v3

    #@15a
    move-object/from16 v0, v16

    #@15c
    invoke-static {v0, v3}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1402(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;I)I

    #@15f
    .line 215
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1400(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I

    #@162
    move-result v3

    #@163
    const/4 v6, -0x1

    #@164
    if-ne v3, v6, :cond_7

    #@166
    .line 216
    move-object/from16 v0, p0

    #@168
    iget-object v2, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@16a
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@16d
    move-result-object v3

    #@16e
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1200(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothDevice;

    #@171
    move-result-object v4

    #@172
    const/4 v5, 0x3

    #@173
    const/4 v6, 0x0

    #@174
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1600(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/os/ParcelFileDescriptor;

    #@177
    move-result-object v7

    #@178
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1400(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I

    #@17b
    move-result v8

    #@17c
    invoke-static/range {v2 .. v8}, Lcom/android/bluetooth/hdp/HealthService;->access$1700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V

    #@17f
    .line 220
    move-object/from16 v0, p0

    #@181
    iget-object v2, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@183
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@186
    move-result-object v3

    #@187
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1200(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothDevice;

    #@18a
    move-result-object v4

    #@18b
    const/4 v5, 0x0

    #@18c
    const/4 v6, 0x3

    #@18d
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1600(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/os/ParcelFileDescriptor;

    #@190
    move-result-object v7

    #@191
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1400(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I

    #@194
    move-result v8

    #@195
    invoke-static/range {v2 .. v8}, Lcom/android/bluetooth/hdp/HealthService;->access$1700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V

    #@198
    goto/16 :goto_7

    #@19a
    .line 229
    .end local v13           #appId:I
    .end local v16           #chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    .end local v18           #devAddr:[B
    :pswitch_19a
    move-object/from16 v0, p1

    #@19c
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19e
    move-object/from16 v16, v0

    #@1a0
    check-cast v16, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;

    #@1a2
    .line 230
    .restart local v16       #chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    move-object/from16 v0, p0

    #@1a4
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@1a6
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1400(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I

    #@1a9
    move-result v6

    #@1aa
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$1800(Lcom/android/bluetooth/hdp/HealthService;I)Z

    #@1ad
    move-result v3

    #@1ae
    if-nez v3, :cond_7

    #@1b0
    .line 231
    move-object/from16 v0, p0

    #@1b2
    iget-object v2, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@1b4
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@1b7
    move-result-object v3

    #@1b8
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1200(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothDevice;

    #@1bb
    move-result-object v4

    #@1bc
    const/4 v5, 0x3

    #@1bd
    const/4 v6, 0x2

    #@1be
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1600(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/os/ParcelFileDescriptor;

    #@1c1
    move-result-object v7

    #@1c2
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1400(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I

    #@1c5
    move-result v8

    #@1c6
    invoke-static/range {v2 .. v8}, Lcom/android/bluetooth/hdp/HealthService;->access$1700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V

    #@1c9
    .line 235
    move-object/from16 v0, p0

    #@1cb
    iget-object v2, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@1cd
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@1d0
    move-result-object v3

    #@1d1
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1200(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothDevice;

    #@1d4
    move-result-object v4

    #@1d5
    const/4 v5, 0x2

    #@1d6
    const/4 v6, 0x3

    #@1d7
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1600(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/os/ParcelFileDescriptor;

    #@1da
    move-result-object v7

    #@1db
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1400(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I

    #@1de
    move-result v8

    #@1df
    invoke-static/range {v2 .. v8}, Lcom/android/bluetooth/hdp/HealthService;->access$1700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V

    #@1e2
    goto/16 :goto_7

    #@1e4
    .line 245
    .end local v16           #chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    :pswitch_1e4
    move-object/from16 v0, p1

    #@1e6
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1e8
    move-object/from16 v23, v0

    #@1ea
    check-cast v23, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;

    #@1ec
    .line 246
    .local v23, info:Lcom/android/bluetooth/hdp/HealthService$HealthInfo;
    invoke-static/range {v23 .. v23}, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;->access$1900(Lcom/android/bluetooth/hdp/HealthService$HealthInfo;)Landroid/bluetooth/BluetoothDevice;

    #@1ef
    move-result-object v3

    #@1f0
    invoke-static {v3}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@1f3
    move-result-object v18

    #@1f4
    .line 247
    .restart local v18       #devAddr:[B
    move-object/from16 v0, p0

    #@1f6
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@1f8
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService;->access$200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/Map;

    #@1fb
    move-result-object v3

    #@1fc
    invoke-static/range {v23 .. v23}, Lcom/android/bluetooth/hdp/HealthService$HealthInfo;->access$2000(Lcom/android/bluetooth/hdp/HealthService$HealthInfo;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@1ff
    move-result-object v6

    #@200
    invoke-interface {v3, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@203
    move-result-object v3

    #@204
    check-cast v3, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@206
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$1000(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)I

    #@209
    move-result v13

    #@20a
    .line 248
    .restart local v13       #appId:I
    move-object/from16 v0, p0

    #@20c
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@20e
    move-object/from16 v0, v18

    #@210
    invoke-static {v3, v0, v13}, Lcom/android/bluetooth/hdp/HealthService;->access$2100(Lcom/android/bluetooth/hdp/HealthService;[BI)Z

    #@213
    move-result v25

    #@214
    .line 249
    .local v25, status:Z
    if-nez v25, :cond_7

    #@216
    .line 250
    const-string v3, "HealthService"

    #@218
    const-string v6, "GetDeviceDataType Failed"

    #@21a
    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21d
    goto/16 :goto_7

    #@21f
    .line 257
    .end local v13           #appId:I
    .end local v18           #devAddr:[B
    .end local v23           #info:Lcom/android/bluetooth/hdp/HealthService$HealthInfo;
    .end local v25           #status:Z
    :pswitch_21f
    move-object/from16 v0, p0

    #@221
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@223
    move-object/from16 v0, p1

    #@225
    iget v6, v0, Landroid/os/Message;->arg1:I

    #@227
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2200(Lcom/android/bluetooth/hdp/HealthService;I)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@22a
    move-result-object v5

    #@22b
    .line 258
    .restart local v5       #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    if-eqz v5, :cond_7

    #@22d
    .line 262
    move-object/from16 v0, p0

    #@22f
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@231
    move-object/from16 v0, p1

    #@233
    iget v6, v0, Landroid/os/Message;->arg2:I

    #@235
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2300(Lcom/android/bluetooth/hdp/HealthService;I)I

    #@238
    move-result v24

    #@239
    .line 263
    .local v24, regStatus:I
    move-object/from16 v0, p0

    #@23b
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@23d
    move/from16 v0, v24

    #@23f
    invoke-static {v3, v5, v0}, Lcom/android/bluetooth/hdp/HealthService;->access$700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)V

    #@242
    .line 264
    const/4 v3, 0x1

    #@243
    move/from16 v0, v24

    #@245
    if-eq v0, v3, :cond_24c

    #@247
    const/4 v3, 0x2

    #@248
    move/from16 v0, v24

    #@24a
    if-ne v0, v3, :cond_7

    #@24c
    .line 267
    :cond_24c
    move-object/from16 v0, p0

    #@24e
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@250
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService;->access$200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/Map;

    #@253
    move-result-object v3

    #@254
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@257
    move-result-object v14

    #@258
    check-cast v14, Lcom/android/bluetooth/hdp/HealthService$AppInfo;

    #@25a
    .line 268
    .restart local v14       #appInfo:Lcom/android/bluetooth/hdp/HealthService$AppInfo;
    invoke-static {v14}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->access$100(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)V

    #@25d
    .line 269
    move-object/from16 v0, p0

    #@25f
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@261
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService;->access$200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/Map;

    #@264
    move-result-object v3

    #@265
    invoke-interface {v3, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@268
    goto/16 :goto_7

    #@26a
    .line 276
    .end local v5           #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v14           #appInfo:Lcom/android/bluetooth/hdp/HealthService$AppInfo;
    .end local v24           #regStatus:I
    :pswitch_26a
    move-object/from16 v0, p1

    #@26c
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@26e
    move-object/from16 v19, v0

    #@270
    check-cast v19, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;

    #@272
    .line 277
    .local v19, deviceConfig:Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;
    move-object/from16 v0, p0

    #@274
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@276
    move-object/from16 v0, v19

    #@278
    iget v6, v0, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;->mAppId:I

    #@27a
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2200(Lcom/android/bluetooth/hdp/HealthService;I)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@27d
    move-result-object v5

    #@27e
    .line 279
    .restart local v5       #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    if-eqz v5, :cond_7

    #@280
    .line 283
    move-object/from16 v0, p0

    #@282
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@284
    move-object/from16 v0, v19

    #@286
    iget-object v6, v0, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;->mAddr:[B

    #@288
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2400(Lcom/android/bluetooth/hdp/HealthService;[B)Landroid/bluetooth/BluetoothDevice;

    #@28b
    move-result-object v4

    #@28c
    .line 284
    .local v4, device:Landroid/bluetooth/BluetoothDevice;
    move-object/from16 v0, p0

    #@28e
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@290
    move-object/from16 v0, v19

    #@292
    iget-object v6, v0, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;->mDataTypes:[I

    #@294
    invoke-static {v3, v5, v4, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2500(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V

    #@297
    goto/16 :goto_7

    #@299
    .line 289
    .end local v4           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v5           #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v19           #deviceConfig:Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;
    :pswitch_299
    move-object/from16 v0, p1

    #@29b
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@29d
    move-object/from16 v19, v0

    #@29f
    check-cast v19, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;

    #@2a1
    .line 290
    .restart local v19       #deviceConfig:Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;
    move-object/from16 v0, p0

    #@2a3
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2a5
    move-object/from16 v0, v19

    #@2a7
    iget v6, v0, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;->mAppId:I

    #@2a9
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2200(Lcom/android/bluetooth/hdp/HealthService;I)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@2ac
    move-result-object v5

    #@2ad
    .line 292
    .restart local v5       #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    if-eqz v5, :cond_7

    #@2af
    .line 296
    move-object/from16 v0, p0

    #@2b1
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2b3
    move-object/from16 v0, v19

    #@2b5
    iget-object v6, v0, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;->mAddr:[B

    #@2b7
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2600(Lcom/android/bluetooth/hdp/HealthService;[B)Landroid/bluetooth/BluetoothDevice;

    #@2ba
    move-result-object v4

    #@2bb
    .line 297
    .restart local v4       #device:Landroid/bluetooth/BluetoothDevice;
    move-object/from16 v0, p0

    #@2bd
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2bf
    move-object/from16 v0, v19

    #@2c1
    iget-object v6, v0, Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;->mDataTypes:[I

    #@2c3
    invoke-static {v3, v5, v4, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;[I)V

    #@2c6
    goto/16 :goto_7

    #@2c8
    .line 303
    .end local v4           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v5           #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    .end local v19           #deviceConfig:Lcom/android/bluetooth/hdp/HealthService$RemoteDeviceConfig;
    :pswitch_2c8
    move-object/from16 v0, p1

    #@2ca
    iget-object v0, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2cc
    move-object/from16 v17, v0

    #@2ce
    check-cast v17, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;

    #@2d0
    .line 304
    .local v17, channelStateEvent:Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;
    move-object/from16 v0, p0

    #@2d2
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2d4
    move-object/from16 v0, v17

    #@2d6
    iget v6, v0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mChannelId:I

    #@2d8
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2800(Lcom/android/bluetooth/hdp/HealthService;I)Lcom/android/bluetooth/hdp/HealthService$HealthChannel;

    #@2db
    move-result-object v2

    #@2dc
    .line 307
    .local v2, chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    move-object/from16 v0, p0

    #@2de
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2e0
    move-object/from16 v0, v17

    #@2e2
    iget v6, v0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mState:I

    #@2e4
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2900(Lcom/android/bluetooth/hdp/HealthService;I)I

    #@2e7
    move-result v9

    #@2e8
    .line 309
    .local v9, newState:I
    if-nez v2, :cond_325

    #@2ea
    .line 311
    move-object/from16 v0, p0

    #@2ec
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2ee
    move-object/from16 v0, v17

    #@2f0
    iget v6, v0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mAppId:I

    #@2f2
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$2200(Lcom/android/bluetooth/hdp/HealthService;I)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@2f5
    move-result-object v5

    #@2f6
    .line 313
    .restart local v5       #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    move-object/from16 v0, p0

    #@2f8
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@2fa
    move-object/from16 v0, v17

    #@2fc
    iget-object v6, v0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mAddr:[B

    #@2fe
    invoke-static {v3, v6}, Lcom/android/bluetooth/hdp/HealthService;->access$3000(Lcom/android/bluetooth/hdp/HealthService;[B)Landroid/bluetooth/BluetoothDevice;

    #@301
    move-result-object v4

    #@302
    .line 314
    .restart local v4       #device:Landroid/bluetooth/BluetoothDevice;
    new-instance v2, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;

    #@304
    .end local v2           #chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    move-object/from16 v0, p0

    #@306
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@308
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothHealthAppConfiguration;->getChannelType()I

    #@30b
    move-result v6

    #@30c
    const/4 v7, 0x0

    #@30d
    invoke-direct/range {v2 .. v7}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;-><init>(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;ILcom/android/bluetooth/hdp/HealthService$1;)V

    #@310
    .line 315
    .restart local v2       #chan:Lcom/android/bluetooth/hdp/HealthService$HealthChannel;
    move-object/from16 v0, v17

    #@312
    iget v3, v0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mChannelId:I

    #@314
    invoke-static {v2, v3}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1402(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;I)I

    #@317
    .line 322
    const/4 v3, 0x2

    #@318
    if-ne v9, v3, :cond_325

    #@31a
    .line 323
    move-object/from16 v0, p0

    #@31c
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@31e
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService;->access$3200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/List;

    #@321
    move-result-object v3

    #@322
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@325
    .line 327
    .end local v4           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v5           #appConfig:Landroid/bluetooth/BluetoothHealthAppConfiguration;
    :cond_325
    const/4 v3, 0x2

    #@326
    if-ne v9, v3, :cond_36a

    #@328
    .line 329
    :try_start_328
    move-object/from16 v0, v17

    #@32a
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$ChannelStateEvent;->mFd:Ljava/io/FileDescriptor;

    #@32c
    invoke-static {v3}, Landroid/os/ParcelFileDescriptor;->dup(Ljava/io/FileDescriptor;)Landroid/os/ParcelFileDescriptor;

    #@32f
    move-result-object v3

    #@330
    invoke-static {v2, v3}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1602(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;Landroid/os/ParcelFileDescriptor;)Landroid/os/ParcelFileDescriptor;
    :try_end_333
    .catch Ljava/io/IOException; {:try_start_328 .. :try_end_333} :catch_360

    #@333
    .line 340
    :goto_333
    if-nez v9, :cond_340

    #@335
    .line 341
    move-object/from16 v0, p0

    #@337
    iget-object v3, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@339
    invoke-static {v3}, Lcom/android/bluetooth/hdp/HealthService;->access$3200(Lcom/android/bluetooth/hdp/HealthService;)Ljava/util/List;

    #@33c
    move-result-object v3

    #@33d
    invoke-interface {v3, v2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    #@340
    .line 344
    :cond_340
    move-object/from16 v0, p0

    #@342
    iget-object v6, v0, Lcom/android/bluetooth/hdp/HealthService$HealthServiceMessageHandler;->this$0:Lcom/android/bluetooth/hdp/HealthService;

    #@344
    invoke-static {v2}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothHealthAppConfiguration;

    #@347
    move-result-object v7

    #@348
    invoke-static {v2}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1200(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/bluetooth/BluetoothDevice;

    #@34b
    move-result-object v8

    #@34c
    invoke-static {v2}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$3300(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I

    #@34f
    move-result v10

    #@350
    invoke-static {v2}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1600(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)Landroid/os/ParcelFileDescriptor;

    #@353
    move-result-object v11

    #@354
    invoke-static {v2}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1400(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;)I

    #@357
    move-result v12

    #@358
    invoke-static/range {v6 .. v12}, Lcom/android/bluetooth/hdp/HealthService;->access$1700(Lcom/android/bluetooth/hdp/HealthService;Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/BluetoothDevice;IILandroid/os/ParcelFileDescriptor;I)V

    #@35b
    .line 346
    invoke-static {v2, v9}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$3302(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;I)I

    #@35e
    goto/16 :goto_7

    #@360
    .line 330
    :catch_360
    move-exception v20

    #@361
    .line 331
    .local v20, e:Ljava/io/IOException;
    const-string v3, "HealthService"

    #@363
    const-string v6, "failed to dup ParcelFileDescriptor"

    #@365
    invoke-static {v3, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@368
    goto/16 :goto_7

    #@36a
    .line 337
    .end local v20           #e:Ljava/io/IOException;
    :cond_36a
    const/4 v3, 0x0

    #@36b
    invoke-static {v2, v3}, Lcom/android/bluetooth/hdp/HealthService$HealthChannel;->access$1602(Lcom/android/bluetooth/hdp/HealthService$HealthChannel;Landroid/os/ParcelFileDescriptor;)Landroid/os/ParcelFileDescriptor;

    #@36e
    goto :goto_333

    #@36f
    .line 155
    nop

    #@370
    :pswitch_data_370
    .packed-switch 0x1
        :pswitch_8
        :pswitch_e6
        :pswitch_12a
        :pswitch_19a
        :pswitch_1e4
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_21f
        :pswitch_2c8
        :pswitch_26a
        :pswitch_299
    .end packed-switch
.end method
