.class Lcom/android/bluetooth/hdp/HealthService$AppInfo;
.super Ljava/lang/Object;
.source "HealthService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hdp/HealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AppInfo"
.end annotation


# instance fields
.field private mAppId:I

.field private mCallback:Landroid/bluetooth/IBluetoothHealthCallback;

.field private mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;


# direct methods
.method private constructor <init>(Landroid/bluetooth/IBluetoothHealthCallback;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 1021
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1022
    iput-object p1, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mCallback:Landroid/bluetooth/IBluetoothHealthCallback;

    #@5
    .line 1023
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@8
    .line 1024
    const/4 v0, -0x1

    #@9
    iput v0, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mAppId:I

    #@b
    .line 1025
    return-void
.end method

.method synthetic constructor <init>(Landroid/bluetooth/IBluetoothHealthCallback;Lcom/android/bluetooth/hdp/HealthService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1016
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;-><init>(Landroid/bluetooth/IBluetoothHealthCallback;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 1016
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->cleanup()V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1016
    iget v0, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mAppId:I

    #@2
    return v0
.end method

.method static synthetic access$1002(Lcom/android/bluetooth/hdp/HealthService$AppInfo;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1016
    iput p1, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mAppId:I

    #@2
    return p1
.end method

.method static synthetic access$800(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1016
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@2
    return-object v0
.end method

.method static synthetic access$802(Lcom/android/bluetooth/hdp/HealthService$AppInfo;Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;)Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1016
    iput-object p1, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@2
    return-object p1
.end method

.method static synthetic access$900(Lcom/android/bluetooth/hdp/HealthService$AppInfo;)Landroid/bluetooth/IBluetoothHealthCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 1016
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mCallback:Landroid/bluetooth/IBluetoothHealthCallback;

    #@2
    return-object v0
.end method

.method private cleanup()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 1028
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mCallback:Landroid/bluetooth/IBluetoothHealthCallback;

    #@3
    if-eqz v2, :cond_39

    #@5
    .line 1029
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@7
    if-eqz v2, :cond_1c

    #@9
    .line 1030
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mCallback:Landroid/bluetooth/IBluetoothHealthCallback;

    #@b
    invoke-interface {v2}, Landroid/bluetooth/IBluetoothHealthCallback;->asBinder()Landroid/os/IBinder;

    #@e
    move-result-object v0

    #@f
    .line 1032
    .local v0, binder:Landroid/os/IBinder;
    :try_start_f
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@11
    const/4 v3, 0x0

    #@12
    invoke-interface {v0, v2, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_15
    .catch Ljava/util/NoSuchElementException; {:try_start_f .. :try_end_15} :catch_1f

    #@15
    .line 1036
    :goto_15
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@17
    invoke-virtual {v2}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;->cleanup()V

    #@1a
    .line 1037
    iput-object v5, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@1c
    .line 1039
    .end local v0           #binder:Landroid/os/IBinder;
    :cond_1c
    iput-object v5, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mCallback:Landroid/bluetooth/IBluetoothHealthCallback;

    #@1e
    .line 1045
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 1033
    .restart local v0       #binder:Landroid/os/IBinder;
    :catch_1f
    move-exception v1

    #@20
    .line 1034
    .local v1, e:Ljava/util/NoSuchElementException;
    const-string v2, "HealthService"

    #@22
    new-instance v3, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v4, "No death recipient registered"

    #@29
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_15

    #@39
    .line 1041
    .end local v0           #binder:Landroid/os/IBinder;
    .end local v1           #e:Ljava/util/NoSuchElementException;
    :cond_39
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@3b
    if-eqz v2, :cond_1e

    #@3d
    .line 1042
    iget-object v2, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@3f
    invoke-virtual {v2}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;->cleanup()V

    #@42
    .line 1043
    iput-object v5, p0, Lcom/android/bluetooth/hdp/HealthService$AppInfo;->mRcpObj:Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthDeathRecipient;

    #@44
    goto :goto_1e
.end method
