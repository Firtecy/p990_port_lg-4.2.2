.class Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;
.super Landroid/bluetooth/IBluetoothHealth$Stub;
.source "HealthService.java"

# interfaces
.implements Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hdp/HealthService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BluetoothHealthBinder"
.end annotation


# instance fields
.field private mService:Lcom/android/bluetooth/hdp/HealthService;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/hdp/HealthService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 389
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothHealth$Stub;-><init>()V

    #@3
    .line 390
    iput-object p1, p0, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->mService:Lcom/android/bluetooth/hdp/HealthService;

    #@5
    .line 391
    return-void
.end method

.method private getService()Lcom/android/bluetooth/hdp/HealthService;
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 399
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_f

    #@7
    .line 400
    const-string v1, "HealthService"

    #@9
    const-string v2, "Health call not allowed for non-active user"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 407
    :cond_e
    :goto_e
    return-object v0

    #@f
    .line 404
    :cond_f
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->mService:Lcom/android/bluetooth/hdp/HealthService;

    #@11
    if-eqz v1, :cond_e

    #@13
    iget-object v1, p0, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->mService:Lcom/android/bluetooth/hdp/HealthService;

    #@15
    invoke-static {v1}, Lcom/android/bluetooth/hdp/HealthService;->access$3400(Lcom/android/bluetooth/hdp/HealthService;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_e

    #@1b
    .line 405
    iget-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->mService:Lcom/android/bluetooth/hdp/HealthService;

    #@1d
    goto :goto_e
.end method


# virtual methods
.method public cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 394
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->mService:Lcom/android/bluetooth/hdp/HealthService;

    #@3
    .line 395
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public connectChannelToSink(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z
    .registers 6
    .parameter "device"
    .parameter "config"
    .parameter "channelType"

    #@0
    .prologue
    .line 438
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->getService()Lcom/android/bluetooth/hdp/HealthService;

    #@3
    move-result-object v0

    #@4
    .line 439
    .local v0, service:Lcom/android/bluetooth/hdp/HealthService;
    if-nez v0, :cond_8

    #@6
    .line 440
    const/4 v1, 0x0

    #@7
    .line 442
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/bluetooth/hdp/HealthService;->connectChannelToSink(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public connectChannelToSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    .registers 5
    .parameter "device"
    .parameter "config"

    #@0
    .prologue
    .line 429
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->getService()Lcom/android/bluetooth/hdp/HealthService;

    #@3
    move-result-object v0

    #@4
    .line 430
    .local v0, service:Lcom/android/bluetooth/hdp/HealthService;
    if-nez v0, :cond_8

    #@6
    .line 431
    const/4 v1, 0x0

    #@7
    .line 433
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hdp/HealthService;->connectChannelToSource(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public disconnectChannel(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z
    .registers 6
    .parameter "device"
    .parameter "config"
    .parameter "channelId"

    #@0
    .prologue
    .line 447
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->getService()Lcom/android/bluetooth/hdp/HealthService;

    #@3
    move-result-object v0

    #@4
    .line 448
    .local v0, service:Lcom/android/bluetooth/hdp/HealthService;
    if-nez v0, :cond_8

    #@6
    .line 449
    const/4 v1, 0x0

    #@7
    .line 451
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/bluetooth/hdp/HealthService;->disconnectChannel(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;I)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getConnectedHealthDevices()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 483
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->getService()Lcom/android/bluetooth/hdp/HealthService;

    #@3
    move-result-object v0

    #@4
    .line 484
    .local v0, service:Lcom/android/bluetooth/hdp/HealthService;
    if-nez v0, :cond_d

    #@6
    .line 485
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 487
    :goto_c
    return-object v1

    #@d
    :cond_d
    invoke-virtual {v0}, Lcom/android/bluetooth/hdp/HealthService;->getConnectedHealthDevices()Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method

.method public getDeviceDatatype(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    .registers 5
    .parameter "device"
    .parameter "config"

    #@0
    .prologue
    .line 457
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->getService()Lcom/android/bluetooth/hdp/HealthService;

    #@3
    move-result-object v0

    #@4
    .line 458
    .local v0, service:Lcom/android/bluetooth/hdp/HealthService;
    if-nez v0, :cond_8

    #@6
    .line 459
    const/4 v1, 0x0

    #@7
    .line 461
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hdp/HealthService;->getDeviceDatatype(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getHealthDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 475
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->getService()Lcom/android/bluetooth/hdp/HealthService;

    #@3
    move-result-object v0

    #@4
    .line 476
    .local v0, service:Lcom/android/bluetooth/hdp/HealthService;
    if-nez v0, :cond_8

    #@6
    .line 477
    const/4 v1, 0x0

    #@7
    .line 479
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hdp/HealthService;->getHealthDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getHealthDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 5
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 491
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->getService()Lcom/android/bluetooth/hdp/HealthService;

    #@3
    move-result-object v0

    #@4
    .line 492
    .local v0, service:Lcom/android/bluetooth/hdp/HealthService;
    if-nez v0, :cond_d

    #@6
    .line 493
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 495
    :goto_c
    return-object v1

    #@d
    :cond_d
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hdp/HealthService;->getHealthDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method

.method public getMainChannelFd(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/os/ParcelFileDescriptor;
    .registers 5
    .parameter "device"
    .parameter "config"

    #@0
    .prologue
    .line 467
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->getService()Lcom/android/bluetooth/hdp/HealthService;

    #@3
    move-result-object v0

    #@4
    .line 468
    .local v0, service:Lcom/android/bluetooth/hdp/HealthService;
    if-nez v0, :cond_8

    #@6
    .line 469
    const/4 v1, 0x0

    #@7
    .line 471
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hdp/HealthService;->getMainChannelFd(Landroid/bluetooth/BluetoothDevice;Landroid/bluetooth/BluetoothHealthAppConfiguration;)Landroid/os/ParcelFileDescriptor;

    #@b
    move-result-object v1

    #@c
    goto :goto_7
.end method

.method public registerAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/IBluetoothHealthCallback;)Z
    .registers 5
    .parameter "config"
    .parameter "callback"

    #@0
    .prologue
    .line 412
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->getService()Lcom/android/bluetooth/hdp/HealthService;

    #@3
    move-result-object v0

    #@4
    .line 413
    .local v0, service:Lcom/android/bluetooth/hdp/HealthService;
    if-nez v0, :cond_8

    #@6
    .line 414
    const/4 v1, 0x0

    #@7
    .line 416
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hdp/HealthService;->registerAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;Landroid/bluetooth/IBluetoothHealthCallback;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public unregisterAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z
    .registers 4
    .parameter "config"

    #@0
    .prologue
    .line 420
    invoke-direct {p0}, Lcom/android/bluetooth/hdp/HealthService$BluetoothHealthBinder;->getService()Lcom/android/bluetooth/hdp/HealthService;

    #@3
    move-result-object v0

    #@4
    .line 421
    .local v0, service:Lcom/android/bluetooth/hdp/HealthService;
    if-nez v0, :cond_8

    #@6
    .line 422
    const/4 v1, 0x0

    #@7
    .line 424
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hdp/HealthService;->unregisterAppConfiguration(Landroid/bluetooth/BluetoothHealthAppConfiguration;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method
