.class public final Lcom/android/bluetooth/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final ErrorCanceled:I = 0x7f0700ac

.field public static final ErrorTooManyRequests:I = 0x7f070047

.field public static final ack_record:I = 0x7f07012e

.field public static final add_as_new_contact_toast:I = 0x7f070094

.field public static final add_as_new_object_toast:I = 0x7f070095

.field public static final airplane_error_msg:I = 0x7f07000b

.field public static final airplane_error_title:I = 0x7f07000a

.field public static final app_name:I = 0x7f07012b

.field public static final auth_notif_message:I = 0x7f070125

.field public static final auth_notif_ticker:I = 0x7f070123

.field public static final auth_notif_title:I = 0x7f070124

.field public static final authdlg_auto_reply:I = 0x7f070064

.field public static final bluetooth_share_file_name:I = 0x7f070061

.field public static final bluetooth_title:I = 0x7f0700c6

.field public static final bt_advanced_settings:I = 0x7f07007c

.field public static final bt_enable_cancel:I = 0x7f07000f

.field public static final bt_enable_line1:I = 0x7f07000d

.field public static final bt_enable_line2:I = 0x7f07000e

.field public static final bt_enable_ok:I = 0x7f070010

.field public static final bt_enable_title:I = 0x7f07000c

.field public static final bt_error_btn_ok:I = 0x7f070038

.field public static final bt_share_label:I = 0x7f0700c7

.field public static final bt_share_picker_label:I = 0x7f070007

.field public static final bt_sm_2_1:I = 0x7f070045

.field public static final bt_sm_2_2:I = 0x7f070046

.field public static final bt_toast_1:I = 0x7f07003f

.field public static final bt_toast_2:I = 0x7f070040

.field public static final bt_toast_3:I = 0x7f070041

.field public static final bt_toast_4:I = 0x7f070042

.field public static final bt_toast_5:I = 0x7f070043

.field public static final bt_toast_6:I = 0x7f070044

.field public static final bt_toast_contact_exchange_success:I = 0x7f070105

.field public static final bt_toast_contact_receive_fail:I = 0x7f070102

.field public static final bt_toast_contact_received:I = 0x7f070101

.field public static final bt_toast_contact_send_fail:I = 0x7f070104

.field public static final bt_toast_contact_sent:I = 0x7f070103

.field public static final bt_toast_exchanging:I = 0x7f0700fe

.field public static final bt_toast_limitation_bt_wifi_wifidirect:I = 0x7f070107

.field public static final bt_toast_receive_success_n:I = 0x7f070106

.field public static final bt_toast_receiving:I = 0x7f0700fd

.field public static final bt_toast_send_fail:I = 0x7f070100

.field public static final bt_toast_send_success:I = 0x7f0700ff

.field public static final bt_toast_sending_1:I = 0x7f0700f9

.field public static final bt_toast_sending_n:I = 0x7f0700fa

.field public static final bt_toast_stop_sending_1:I = 0x7f0700fb

.field public static final bt_toast_stop_sending_n:I = 0x7f0700fc

.field public static final btopp_live_folder:I = 0x7f070053

.field public static final btui_error_myprofile_not_set:I = 0x7f07011e

.field public static final btui_error_no_sdcard:I = 0x7f07011c

.field public static final button_apply:I = 0x7f0700a8

.field public static final button_cancel:I = 0x7f0700aa

.field public static final button_discard:I = 0x7f0700ab

.field public static final button_save:I = 0x7f0700a9

.field public static final calendar_entries:I = 0x7f07009c

.field public static final calendar_entry:I = 0x7f07009b

.field public static final contact:I = 0x7f070099

.field public static final contacts:I = 0x7f07009a

.field public static final default_contact_not_set_toast:I = 0x7f0700f7

.field public static final default_contact_set_toast:I = 0x7f0700f6

.field public static final default_result:I = 0x7f07010c

.field public static final defaultname:I = 0x7f070126

.field public static final defaultnumber:I = 0x7f070129

.field public static final deleteAll_record:I = 0x7f07012f

.field public static final delete_record:I = 0x7f070131

.field public static final download_cancel:I = 0x7f070027

.field public static final download_cancel_all:I = 0x7f0700d3

.field public static final download_fail_line1:I = 0x7f070029

.field public static final download_fail_line1_with_param:I = 0x7f0700a7

.field public static final download_fail_line2:I = 0x7f07002a

.field public static final download_fail_line3:I = 0x7f07002b

.field public static final download_fail_ok:I = 0x7f07002c

.field public static final download_line1:I = 0x7f070022

.field public static final download_line2:I = 0x7f070023

.field public static final download_line3:I = 0x7f070024

.field public static final download_line4:I = 0x7f070025

.field public static final download_line5:I = 0x7f070026

.field public static final download_line5_with_param:I = 0x7f0700a5

.field public static final download_ok:I = 0x7f070028

.field public static final download_queued_with_nfiles:I = 0x7f0700d2

.field public static final download_succ_line5:I = 0x7f07002d

.field public static final download_succ_line5_with_param:I = 0x7f0700a6

.field public static final download_succ_ok:I = 0x7f07002e

.field public static final download_success:I = 0x7f070054

.field public static final download_title:I = 0x7f070021

.field public static final dup_action_add:I = 0x7f0700e9

.field public static final dup_action_ignore:I = 0x7f0700eb

.field public static final dup_action_prompt:I = 0x7f07009e

.field public static final dup_action_update:I = 0x7f0700ea

.field public static final dup_dlg_msg_added:I = 0x7f0700e4

.field public static final dup_dlg_msg_ignored:I = 0x7f0700e7

.field public static final dup_dlg_msg_reading:I = 0x7f0700e5

.field public static final dup_dlg_msg_save_error:I = 0x7f0700e8

.field public static final dup_dlg_msg_updated:I = 0x7f0700e6

.field public static final dup_dlg_title_saving:I = 0x7f0700e3

.field public static final dup_file_update_message:I = 0x7f07009d

.field public static final dup_file_update_title:I = 0x7f0700e0

.field public static final dup_file_update_title_with_param:I = 0x7f0700e1

.field public static final dup_file_updatemessage:I = 0x7f0700e2

.field public static final dup_menu_add_all:I = 0x7f0700dd

.field public static final dup_menu_ignore_all:I = 0x7f0700df

.field public static final dup_menu_update_all:I = 0x7f0700de

.field public static final dup_res_added:I = 0x7f0700ec

.field public static final dup_res_updated:I = 0x7f0700ed

.field public static final duplicate_calendar_event_list_title:I = 0x7f0700f4

.field public static final duplicate_calendar_events_list_title:I = 0x7f0700f5

.field public static final duplicate_contact_list_title:I = 0x7f0700f2

.field public static final duplicate_contacts_list_title:I = 0x7f0700f3

.field public static final duplicate_dlg_button_ok:I = 0x7f0700f0

.field public static final duplicate_dlg_msg:I = 0x7f0700ef

.field public static final duplicate_dlg_title:I = 0x7f0700ee

.field public static final duplicate_list_title:I = 0x7f0700f1

.field public static final enabling_progress_content:I = 0x7f07003e

.field public static final enabling_progress_title:I = 0x7f07003d

.field public static final error_canceled:I = 0x7f07010e

.field public static final error_content_create_to_send:I = 0x7f070110

.field public static final error_content_not_supported:I = 0x7f07010f

.field public static final error_dup_file:I = 0x7f070115

.field public static final error_exceed_max_file_size:I = 0x7f07006f

.field public static final error_file_move:I = 0x7f070114

.field public static final error_max_requests:I = 0x7f07011a

.field public static final error_no_default_vcard:I = 0x7f070118

.field public static final error_remote_connection_end:I = 0x7f070112

.field public static final error_remote_reject_request:I = 0x7f070113

.field public static final error_remote_timeout:I = 0x7f070111

.field public static final error_sdcard_not_mounted:I = 0x7f070119

.field public static final error_unsupported_action:I = 0x7f07011b

.field public static final error_vcal_store:I = 0x7f070117

.field public static final error_vcard_store:I = 0x7f070116

.field public static final exchange_contacts:I = 0x7f0700ca

.field public static final get_contact:I = 0x7f0700c9

.field public static final groups:I = 0x7f0700c8

.field public static final hello:I = 0x7f07012a

.field public static final inbound_history_title:I = 0x7f070056

.field public static final inbound_noti_title:I = 0x7f07005b

.field public static final inbound_pending_title:I = 0x7f0700d5

.field public static final incoming_file_confirm_Notification_caption:I = 0x7f070018

.field public static final incoming_file_confirm_Notification_title:I = 0x7f070017

.field public static final incoming_file_confirm_cancel:I = 0x7f070013

.field public static final incoming_file_confirm_content:I = 0x7f070012

.field public static final incoming_file_confirm_ok:I = 0x7f070014

.field public static final incoming_file_confirm_timeout_content:I = 0x7f070016

.field public static final incoming_file_confirm_timeout_ok:I = 0x7f070015

.field public static final incoming_file_confirm_title:I = 0x7f070011

.field public static final incoming_file_toast_msg:I = 0x7f070019

.field public static final insert_record:I = 0x7f07012c

.field public static final label_filetype_unknown:I = 0x7f07010b

.field public static final localPhoneName:I = 0x7f070128

.field public static final map_datasource_mms_name:I = 0x7f070092

.field public static final map_datasource_sms_name:I = 0x7f070091

.field public static final map_datasource_smsmms_name:I = 0x7f070093

.field public static final map_provider_smsmms_name:I = 0x7f070090

.field public static final no_contact_in_group:I = 0x7f0700f8

.field public static final no_duplicates:I = 0x7f0700dc

.field public static final no_pending_transfers:I = 0x7f0700d7

.field public static final no_transfers:I = 0x7f070058

.field public static final not_exist_file:I = 0x7f07003b

.field public static final not_exist_file_desc:I = 0x7f07003c

.field public static final noti_caption:I = 0x7f07005c

.field public static final notification_download_queued_title:I = 0x7f0700cf

.field public static final notification_exchange_fail:I = 0x7f0700d0

.field public static final notification_file_receiving_ready_msg:I = 0x7f070071

.field public static final notification_multi_files_received_msg:I = 0x7f070073

.field public static final notification_multi_files_sent_msg:I = 0x7f070075

.field public static final notification_queued:I = 0x7f0700cb

.field public static final notification_received:I = 0x7f07001b

.field public static final notification_received_fail:I = 0x7f07001c

.field public static final notification_receiving:I = 0x7f07001a

.field public static final notification_sending:I = 0x7f07001d

.field public static final notification_sent:I = 0x7f07001e

.field public static final notification_sent_complete:I = 0x7f07001f

.field public static final notification_sent_fail:I = 0x7f070020

.field public static final notification_single_file_not_sent_msg:I = 0x7f070076

.field public static final notification_single_file_received_msg:I = 0x7f070072

.field public static final notification_some_files_not_received_msg:I = 0x7f070074

.field public static final notification_some_files_not_sent_msg:I = 0x7f070077

.field public static final notification_upload_queued_title:I = 0x7f0700ce

.field public static final notify_server:I = 0x7f070133

.field public static final obj_type_calendar:I = 0x7f0700ad

.field public static final obj_type_calendar_2:I = 0x7f0700b9

.field public static final obj_type_calendars:I = 0x7f0700b3

.field public static final obj_type_calendars_2:I = 0x7f0700bf

.field public static final obj_type_contact:I = 0x7f0700ae

.field public static final obj_type_contact_2:I = 0x7f0700ba

.field public static final obj_type_contacts:I = 0x7f0700b4

.field public static final obj_type_contacts_2:I = 0x7f0700c0

.field public static final obj_type_image:I = 0x7f0700af

.field public static final obj_type_image_2:I = 0x7f0700bb

.field public static final obj_type_images:I = 0x7f0700b5

.field public static final obj_type_images_2:I = 0x7f0700c1

.field public static final obj_type_misc:I = 0x7f0700b2

.field public static final obj_type_misc_2:I = 0x7f0700be

.field public static final obj_type_miscs:I = 0x7f0700b8

.field public static final obj_type_miscs_2:I = 0x7f0700c4

.field public static final obj_type_music:I = 0x7f0700b0

.field public static final obj_type_music_2:I = 0x7f0700bc

.field public static final obj_type_musics:I = 0x7f0700b6

.field public static final obj_type_musics_2:I = 0x7f0700c2

.field public static final obj_type_video:I = 0x7f0700b1

.field public static final obj_type_video_2:I = 0x7f0700bd

.field public static final obj_type_videos:I = 0x7f0700b7

.field public static final obj_type_videos_2:I = 0x7f0700c3

.field public static final ok_button:I = 0x7f070130

.field public static final open_received_contacts:I = 0x7f0700d4

.field public static final outbound_history_title:I = 0x7f070057

.field public static final outbound_noti_title:I = 0x7f07005a

.field public static final outbound_pending_title:I = 0x7f0700d6

.field public static final pbap_acceptance_timeout_message:I = 0x7f070121

.field public static final pbap_authentication_timeout_message:I = 0x7f070122

.field public static final pbap_session_key_dialog_header:I = 0x7f070120

.field public static final pbap_session_key_dialog_title:I = 0x7f07011f

.field public static final permdesc_bluetoothMap:I = 0x7f070079

.field public static final permdesc_bluetoothShareManager:I = 0x7f070002

.field public static final permdesc_bluetoothWhitelist:I = 0x7f070004

.field public static final permdesc_fm_receiver:I = 0x7f07007b

.field public static final permdesc_handoverStatus:I = 0x7f070006

.field public static final permlab_bluetoothMap:I = 0x7f070078

.field public static final permlab_bluetoothShareManager:I = 0x7f070001

.field public static final permlab_bluetoothWhitelist:I = 0x7f070003

.field public static final permlab_fm_receiver:I = 0x7f07007a

.field public static final permlab_handoverStatus:I = 0x7f070005

.field public static final process:I = 0x7f070063

.field public static final profile_a2dp:I = 0x7f07007f

.field public static final profile_description_a2dp:I = 0x7f070080

.field public static final profile_description_ftp:I = 0x7f07008c

.field public static final profile_description_gatt:I = 0x7f070088

.field public static final profile_description_hdp:I = 0x7f070084

.field public static final profile_description_hid:I = 0x7f070082

.field public static final profile_description_hs_hfp:I = 0x7f07007e

.field public static final profile_description_mse:I = 0x7f07008e

.field public static final profile_description_pan:I = 0x7f070086

.field public static final profile_description_sap:I = 0x7f07008a

.field public static final profile_ftp:I = 0x7f07008b

.field public static final profile_gatt:I = 0x7f070087

.field public static final profile_hdp:I = 0x7f070083

.field public static final profile_hid:I = 0x7f070081

.field public static final profile_hs_hfp:I = 0x7f07007d

.field public static final profile_map_version:I = 0x7f070000

.field public static final profile_mse:I = 0x7f07008d

.field public static final profile_pan:I = 0x7f070085

.field public static final profile_sap:I = 0x7f070089

.field public static final select_default_contact:I = 0x7f070098

.field public static final server_vcard_pull_confirm_Notification_caption:I = 0x7f0700cd

.field public static final server_vcard_pull_confirm_Notification_title:I = 0x7f0700cc

.field public static final set_default_contact:I = 0x7f070097

.field public static final sharedUserId:I = 0x7f070062

.field public static final sp_bt_drm_dialog_NORMAL:I = 0x7f070070

.field public static final sp_bt_enable_opp_NORMAL:I = 0x7f07006e

.field public static final sp_btui_error_unsupported_format_NORMAL:I = 0x7f07011d

.field public static final sp_no_NORMAL:I = 0x7f070068

.field public static final sp_no_history_opp_SHORT:I = 0x7f07006b

.field public static final sp_no_mounted_internal_sdcard_NORMAL:I = 0x7f070069

.field public static final sp_not_enough_internal_storage_NORMAL:I = 0x7f070066

.field public static final sp_not_enouth_memory_NORMAL:I = 0x7f07006a

.field public static final sp_storage_full_dialog_title_NORMAL:I = 0x7f070065

.field public static final sp_transfer_clear_dlg_question_NORMAL:I = 0x7f07006c

.field public static final sp_transfer_menu_clear_all_SHORT:I = 0x7f07006d

.field public static final sp_yes_NORMAL:I = 0x7f070067

.field public static final start_server:I = 0x7f070132

.field public static final status_canceled:I = 0x7f07004d

.field public static final status_canceled_exchange:I = 0x7f0700da

.field public static final status_connection_error:I = 0x7f070050

.field public static final status_error_exchange:I = 0x7f0700db

.field public static final status_file_error:I = 0x7f07004e

.field public static final status_forbidden:I = 0x7f07004c

.field public static final status_no_sd_card:I = 0x7f07004f

.field public static final status_not_accept:I = 0x7f07004b

.field public static final status_pending:I = 0x7f070048

.field public static final status_protocol_error:I = 0x7f070051

.field public static final status_received_for_exchange:I = 0x7f0700d9

.field public static final status_running:I = 0x7f070049

.field public static final status_sent_for_exchange:I = 0x7f0700d8

.field public static final status_success:I = 0x7f07004a

.field public static final status_unknown_error:I = 0x7f070052

.field public static final text_value_unselected:I = 0x7f07009f

.field public static final title_bt_mse_settings:I = 0x7f07008f

.field public static final transfer_clear_dlg_msg:I = 0x7f070059

.field public static final transfer_clear_dlg_title:I = 0x7f070060

.field public static final transfer_menu_clear:I = 0x7f07005f

.field public static final transfer_menu_clear_all:I = 0x7f07005d

.field public static final transfer_menu_open:I = 0x7f07005e

.field public static final unknownName:I = 0x7f070127

.field public static final unknownNumber:I = 0x7f070009

.field public static final unknown_content:I = 0x7f07010d

.field public static final unknown_device:I = 0x7f070008

.field public static final unknown_file:I = 0x7f070039

.field public static final unknown_file_desc:I = 0x7f07003a

.field public static final unknown_file_label:I = 0x7f0700c5

.field public static final update_object_toast:I = 0x7f070096

.field public static final update_record:I = 0x7f07012d

.field public static final upload_fail_cancel:I = 0x7f070037

.field public static final upload_fail_line1:I = 0x7f070034

.field public static final upload_fail_line1_2:I = 0x7f070035

.field public static final upload_fail_line1_with_param:I = 0x7f0700a4

.field public static final upload_fail_ok:I = 0x7f070036

.field public static final upload_line1:I = 0x7f07002f

.field public static final upload_line3:I = 0x7f070030

.field public static final upload_line3_no_file_size:I = 0x7f0700a0

.field public static final upload_line5:I = 0x7f070031

.field public static final upload_line5_with_param:I = 0x7f0700a1

.field public static final upload_queued_with_nfiles:I = 0x7f0700d1

.field public static final upload_queued_with_param:I = 0x7f0700a2

.field public static final upload_succ_line5:I = 0x7f070032

.field public static final upload_succ_line5_with_param:I = 0x7f0700a3

.field public static final upload_succ_ok:I = 0x7f070033

.field public static final upload_success:I = 0x7f070055

.field public static final vcard_pull_confirm_content:I = 0x7f070109

.field public static final vcard_pull_confirm_title:I = 0x7f070108

.field public static final vcard_pull_timeout_content:I = 0x7f07010a


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 177
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
