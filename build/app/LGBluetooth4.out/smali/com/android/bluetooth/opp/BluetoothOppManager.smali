.class public Lcom/android/bluetooth/opp/BluetoothOppManager;
.super Ljava/lang/Object;
.source "BluetoothOppManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/opp/BluetoothOppManager$InsertShareInfoThread;
    }
.end annotation


# static fields
.field private static final ALLOWED_INSERT_SHARE_THREAD_NUMBER:I = 0x3

.field private static final ARRAYLIST_ITEM_SEPERATOR:Ljava/lang/String; = ";"

.field private static final FILE_URI:Ljava/lang/String; = "FILE_URI"

.field private static final FILE_URIS:Ljava/lang/String; = "FILE_URIS"

.field private static INSTANCE:Lcom/android/bluetooth/opp/BluetoothOppManager; = null

.field private static INSTANCE_LOCK:Ljava/lang/Object; = null

.field private static final MIME_TYPE:Ljava/lang/String; = "MIMETYPE"

.field private static final MIME_TYPE_MULTIPLE:Ljava/lang/String; = "MIMETYPE_MULTIPLE"

.field private static final MULTIPLE_FLAG:Ljava/lang/String; = "MULTIPLE_FLAG"

.field private static final OPP_PREFERENCE_FILE:Ljava/lang/String; = "OPPMGR"

.field private static final SENDING_FLAG:Ljava/lang/String; = "SENDINGFLAG"

.field public static final START_SEND_SERVICE:Ljava/lang/String; = "com.lge.bluetooth.opp.sendservice"

.field private static final TAG:Ljava/lang/String; = "BluetoothOppManager"

.field private static final V:Z = true

.field private static final WHITELIST_DURATION_MS:I = 0x3a98


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mContext:Landroid/content/Context;

.field private mInitialized:Z

.field private mInsertShareThreadNum:I

.field private mIsHandoverInitiated:Z

.field private mMimeTypeOfSendingFile:Ljava/lang/String;

.field private mMimeTypeOfSendingFiles:Ljava/lang/String;

.field public mMultipleFlag:Z

.field public mSendTypeIntent:Landroid/content/Intent;

.field public mSendingFlag:Z

.field private mUriOfSendingFile:Ljava/lang/String;

.field private mUrisOfSendingFiles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mWhitelist:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;>;"
        }
    .end annotation
.end field

.field private mfileNumInBatch:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 67
    new-instance v0, Ljava/lang/Object;

    #@2
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@5
    sput-object v0, Lcom/android/bluetooth/opp/BluetoothOppManager;->INSTANCE_LOCK:Ljava/lang/Object;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 60
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 111
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mInsertShareThreadNum:I

    #@6
    .line 115
    new-instance v0, Ljava/util/ArrayList;

    #@8
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@b
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mWhitelist:Ljava/util/List;

    #@d
    .line 410
    return-void
.end method

.method static synthetic access$008(Lcom/android/bluetooth/opp/BluetoothOppManager;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mInsertShareThreadNum:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mInsertShareThreadNum:I

    #@6
    return v0
.end method

.method static synthetic access$010(Lcom/android/bluetooth/opp/BluetoothOppManager;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mInsertShareThreadNum:I

    #@2
    add-int/lit8 v1, v0, -0x1

    #@4
    iput v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mInsertShareThreadNum:I

    #@6
    return v0
.end method

.method static synthetic access$100(Lcom/android/bluetooth/opp/BluetoothOppManager;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method private cleanupWhitelist()V
    .registers 9

    #@0
    .prologue
    .line 166
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@3
    move-result-wide v0

    #@4
    .line 167
    .local v0, curTime:J
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mWhitelist:Ljava/util/List;

    #@6
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v3

    #@a
    .local v3, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Long;>;>;"
    :cond_a
    :goto_a
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_46

    #@10
    .line 168
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v2

    #@14
    check-cast v2, Landroid/util/Pair;

    #@16
    .line 169
    .local v2, entry:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Long;>;"
    iget-object v4, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@18
    check-cast v4, Ljava/lang/Long;

    #@1a
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    #@1d
    move-result-wide v4

    #@1e
    sub-long v4, v0, v4

    #@20
    const-wide/16 v6, 0x3a98

    #@22
    cmp-long v4, v4, v6

    #@24
    if-lez v4, :cond_a

    #@26
    .line 171
    const-string v5, "BluetoothOppManager"

    #@28
    new-instance v4, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v6, "Cleaning out whitelist entry "

    #@2f
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v6

    #@33
    iget-object v4, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@35
    check-cast v4, Ljava/lang/String;

    #@37
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v4

    #@3b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v4

    #@3f
    invoke-static {v5, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 173
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    #@45
    goto :goto_a

    #@46
    .line 176
    .end local v2           #entry:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Long;>;"
    :cond_46
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 129
    sget-object v1, Lcom/android/bluetooth/opp/BluetoothOppManager;->INSTANCE_LOCK:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 130
    :try_start_3
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppManager;->INSTANCE:Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@5
    if-nez v0, :cond_e

    #@7
    .line 131
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@9
    invoke-direct {v0}, Lcom/android/bluetooth/opp/BluetoothOppManager;-><init>()V

    #@c
    sput-object v0, Lcom/android/bluetooth/opp/BluetoothOppManager;->INSTANCE:Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@e
    .line 133
    :cond_e
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppManager;->INSTANCE:Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@10
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->init(Landroid/content/Context;)Z

    #@13
    .line 135
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppManager;->INSTANCE:Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@15
    monitor-exit v1

    #@16
    return-object v0

    #@17
    .line 136
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method private init(Landroid/content/Context;)Z
    .registers 5
    .parameter "context"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 143
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mInitialized:Z

    #@3
    if-eqz v0, :cond_6

    #@5
    .line 160
    :goto_5
    return v2

    #@6
    .line 146
    :cond_6
    iput-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mInitialized:Z

    #@8
    .line 148
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@a
    .line 150
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@d
    move-result-object v0

    #@e
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@10
    .line 151
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@12
    if-nez v0, :cond_1b

    #@14
    .line 153
    const-string v0, "BluetoothOppManager"

    #@16
    const-string v1, "BLUETOOTH_SERVICE is not started! "

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 158
    :cond_1b
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->restoreApplicationData()V

    #@1e
    goto :goto_5
.end method

.method private restoreApplicationData()V
    .registers 10

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    .line 206
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@4
    const-string v5, "OPPMGR"

    #@6
    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@9
    move-result-object v1

    #@a
    .line 209
    .local v1, settings:Landroid/content/SharedPreferences;
    const-string v4, "SENDINGFLAG"

    #@c
    invoke-interface {v1, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@f
    move-result v4

    #@10
    iput-boolean v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mSendingFlag:Z

    #@12
    .line 210
    const-string v4, "MIMETYPE"

    #@14
    invoke-interface {v1, v4, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@17
    move-result-object v4

    #@18
    iput-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMimeTypeOfSendingFile:Ljava/lang/String;

    #@1a
    .line 211
    const-string v4, "FILE_URI"

    #@1c
    invoke-interface {v1, v4, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    iput-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUriOfSendingFile:Ljava/lang/String;

    #@22
    .line 212
    const-string v4, "MIMETYPE_MULTIPLE"

    #@24
    invoke-interface {v1, v4, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    iput-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMimeTypeOfSendingFiles:Ljava/lang/String;

    #@2a
    .line 213
    const-string v4, "MULTIPLE_FLAG"

    #@2c
    invoke-interface {v1, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    #@2f
    move-result v4

    #@30
    iput-boolean v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMultipleFlag:Z

    #@32
    .line 216
    const-string v4, "BluetoothOppManager"

    #@34
    new-instance v5, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v6, "restoreApplicationData! "

    #@3b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    iget-boolean v6, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mSendingFlag:Z

    #@41
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@44
    move-result-object v5

    #@45
    iget-boolean v6, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMultipleFlag:Z

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMimeTypeOfSendingFile:Ljava/lang/String;

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUriOfSendingFile:Ljava/lang/String;

    #@53
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v5

    #@5b
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 220
    const-string v4, "FILE_URIS"

    #@60
    invoke-interface {v1, v4, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@63
    move-result-object v3

    #@64
    .line 221
    .local v3, strUris:Ljava/lang/String;
    new-instance v4, Ljava/util/ArrayList;

    #@66
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@69
    iput-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUrisOfSendingFiles:Ljava/util/ArrayList;

    #@6b
    .line 222
    if-eqz v3, :cond_a3

    #@6d
    .line 223
    const-string v4, ";"

    #@6f
    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@72
    move-result-object v2

    #@73
    .line 224
    .local v2, splitUri:[Ljava/lang/String;
    const/4 v0, 0x0

    #@74
    .local v0, i:I
    :goto_74
    array-length v4, v2

    #@75
    if-ge v0, v4, :cond_a3

    #@77
    .line 225
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUrisOfSendingFiles:Ljava/util/ArrayList;

    #@79
    aget-object v5, v2, v0

    #@7b
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@82
    .line 227
    const-string v4, "BluetoothOppManager"

    #@84
    new-instance v5, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v6, "Uri in batch:  "

    #@8b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v5

    #@8f
    aget-object v6, v2, v0

    #@91
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@94
    move-result-object v6

    #@95
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v5

    #@99
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9c
    move-result-object v5

    #@9d
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    .line 224
    add-int/lit8 v0, v0, 0x1

    #@a2
    goto :goto_74

    #@a3
    .line 232
    .end local v0           #i:I
    .end local v2           #splitUri:[Ljava/lang/String;
    :cond_a3
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@a5
    const-string v5, "OPPMGR"

    #@a7
    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@aa
    move-result-object v4

    #@ab
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@ae
    move-result-object v4

    #@af
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    #@b2
    move-result-object v4

    #@b3
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@b6
    .line 233
    return-void
.end method

.method private storeApplicationData()V
    .registers 10

    #@0
    .prologue
    .line 239
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@2
    const-string v7, "OPPMGR"

    #@4
    const/4 v8, 0x0

    #@5
    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    #@8
    move-result-object v6

    #@9
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    #@c
    move-result-object v1

    #@d
    .line 241
    .local v1, editor:Landroid/content/SharedPreferences$Editor;
    const-string v6, "SENDINGFLAG"

    #@f
    iget-boolean v7, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mSendingFlag:Z

    #@11
    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@14
    .line 242
    const-string v6, "MULTIPLE_FLAG"

    #@16
    iget-boolean v7, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMultipleFlag:Z

    #@18
    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    #@1b
    .line 243
    iget-boolean v6, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMultipleFlag:Z

    #@1d
    if-eqz v6, :cond_65

    #@1f
    .line 244
    const-string v6, "MIMETYPE_MULTIPLE"

    #@21
    iget-object v7, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMimeTypeOfSendingFiles:Ljava/lang/String;

    #@23
    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@26
    .line 245
    new-instance v3, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    .line 246
    .local v3, sb:Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    #@2c
    .local v2, i:I
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUrisOfSendingFiles:Ljava/util/ArrayList;

    #@2e
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@31
    move-result v0

    #@32
    .local v0, count:I
    :goto_32
    if-ge v2, v0, :cond_47

    #@34
    .line 247
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUrisOfSendingFiles:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@39
    move-result-object v5

    #@3a
    check-cast v5, Landroid/net/Uri;

    #@3c
    .line 248
    .local v5, uriContent:Landroid/net/Uri;
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    .line 249
    const-string v6, ";"

    #@41
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    .line 246
    add-int/lit8 v2, v2, 0x1

    #@46
    goto :goto_32

    #@47
    .line 251
    .end local v5           #uriContent:Landroid/net/Uri;
    :cond_47
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v4

    #@4b
    .line 252
    .local v4, strUris:Ljava/lang/String;
    const-string v6, "FILE_URIS"

    #@4d
    invoke-interface {v1, v6, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@50
    .line 254
    const-string v6, "MIMETYPE"

    #@52
    invoke-interface {v1, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@55
    .line 255
    const-string v6, "FILE_URI"

    #@57
    invoke-interface {v1, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@5a
    .line 266
    .end local v0           #count:I
    .end local v2           #i:I
    .end local v3           #sb:Ljava/lang/StringBuilder;
    .end local v4           #strUris:Ljava/lang/String;
    :goto_5a
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    #@5d
    .line 268
    const-string v6, "BluetoothOppManager"

    #@5f
    const-string v7, "Application data stored to SharedPreference! "

    #@61
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 270
    return-void

    #@65
    .line 258
    :cond_65
    const/4 v6, 0x1

    #@66
    iput v6, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mfileNumInBatch:I

    #@68
    .line 260
    const-string v6, "MIMETYPE"

    #@6a
    iget-object v7, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMimeTypeOfSendingFile:Ljava/lang/String;

    #@6c
    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@6f
    .line 261
    const-string v6, "FILE_URI"

    #@71
    iget-object v7, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUriOfSendingFile:Ljava/lang/String;

    #@73
    invoke-interface {v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@76
    .line 263
    const-string v6, "MIMETYPE_MULTIPLE"

    #@78
    invoke-interface {v1, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@7b
    .line 264
    const-string v6, "FILE_URIS"

    #@7d
    invoke-interface {v1, v6}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    #@80
    goto :goto_5a
.end method


# virtual methods
.method public declared-synchronized addToWhitelist(Ljava/lang/String;)V
    .registers 8
    .parameter "address"

    #@0
    .prologue
    .line 179
    monitor-enter p0

    #@1
    if-nez p1, :cond_5

    #@3
    .line 190
    :goto_3
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 183
    :cond_5
    :try_start_5
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mWhitelist:Ljava/util/List;

    #@7
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@a
    move-result-object v1

    #@b
    .local v1, iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Long;>;>;"
    :cond_b
    :goto_b
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@e
    move-result v2

    #@f
    if-eqz v2, :cond_28

    #@11
    .line 184
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/util/Pair;

    #@17
    .line 185
    .local v0, entry:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Long;>;"
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@19
    check-cast v2, Ljava/lang/String;

    #@1b
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_b

    #@21
    .line 186
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_24
    .catchall {:try_start_5 .. :try_end_24} :catchall_25

    #@24
    goto :goto_b

    #@25
    .line 179
    .end local v0           #entry:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Long;>;"
    .end local v1           #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Long;>;>;"
    :catchall_25
    move-exception v2

    #@26
    monitor-exit p0

    #@27
    throw v2

    #@28
    .line 189
    .restart local v1       #iter:Ljava/util/Iterator;,"Ljava/util/Iterator<Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Long;>;>;"
    :cond_28
    :try_start_28
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mWhitelist:Ljava/util/List;

    #@2a
    new-instance v3, Landroid/util/Pair;

    #@2c
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@2f
    move-result-wide v4

    #@30
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@33
    move-result-object v4

    #@34
    invoke-direct {v3, p1, v4}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    #@37
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3a
    .catchall {:try_start_28 .. :try_end_3a} :catchall_25

    #@3a
    goto :goto_3
.end method

.method public disableBluetooth()V
    .registers 2

    #@0
    .prologue
    .line 327
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 328
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@6
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->disable()Z

    #@9
    .line 330
    :cond_9
    return-void
.end method

.method public enableBluetooth()V
    .registers 2

    #@0
    .prologue
    .line 318
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 319
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@6
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    #@9
    .line 321
    :cond_9
    return-void
.end method

.method public getBatchSize()I
    .registers 2

    #@0
    .prologue
    .line 365
    monitor-enter p0

    #@1
    .line 366
    :try_start_1
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mfileNumInBatch:I

    #@3
    monitor-exit p0

    #@4
    return v0

    #@5
    .line 367
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_1 .. :try_end_7} :catchall_5

    #@7
    throw v0
.end method

.method public getDeviceName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 342
    const/4 v0, 0x0

    #@1
    .line 344
    .local v0, deviceName:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@3
    if-eqz v1, :cond_9

    #@5
    .line 345
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAlias()Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 348
    :cond_9
    if-nez v0, :cond_15

    #@b
    .line 349
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@d
    invoke-static {v1}, Lcom/android/bluetooth/opp/BluetoothOppPreference;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppPreference;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/opp/BluetoothOppPreference;->getName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    .line 353
    :cond_15
    if-nez v0, :cond_1f

    #@17
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@19
    if-eqz v1, :cond_1f

    #@1b
    .line 354
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 357
    :cond_1f
    if-nez v0, :cond_2a

    #@21
    .line 358
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@23
    const v2, 0x7f070008

    #@26
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@29
    move-result-object v0

    #@2a
    .line 361
    :cond_2a
    return-object v0
.end method

.method public isEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 304
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 305
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@6
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@9
    move-result v0

    #@a
    .line 310
    :goto_a
    return v0

    #@b
    .line 308
    :cond_b
    const-string v0, "BluetoothOppManager"

    #@d
    const-string v1, "BLUETOOTH_SERVICE is not available! "

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 310
    const/4 v0, 0x0

    #@13
    goto :goto_a
.end method

.method public declared-synchronized isWhitelisted(Ljava/lang/String;)Z
    .registers 5
    .parameter "address"

    #@0
    .prologue
    .line 193
    monitor-enter p0

    #@1
    :try_start_1
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->cleanupWhitelist()V

    #@4
    .line 194
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mWhitelist:Ljava/util/List;

    #@6
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .local v1, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_23

    #@10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/util/Pair;

    #@16
    .line 195
    .local v0, entry:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Long;>;"
    iget-object v2, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@18
    check-cast v2, Ljava/lang/String;

    #@1a
    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1d
    .catchall {:try_start_1 .. :try_end_1d} :catchall_25

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_a

    #@20
    .line 196
    const/4 v2, 0x1

    #@21
    .line 199
    .end local v0           #entry:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Long;>;"
    :goto_21
    monitor-exit p0

    #@22
    return v2

    #@23
    :cond_23
    const/4 v2, 0x0

    #@24
    goto :goto_21

    #@25
    .line 193
    .end local v1           #i$:Ljava/util/Iterator;
    :catchall_25
    move-exception v2

    #@26
    monitor-exit p0

    #@27
    throw v2
.end method

.method public saveSendingFileInfo(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 6
    .parameter "mimeType"
    .parameter "uriString"
    .parameter "isHandover"

    #@0
    .prologue
    .line 273
    monitor-enter p0

    #@1
    .line 274
    const/4 v1, 0x0

    #@2
    :try_start_2
    iput-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMultipleFlag:Z

    #@4
    .line 275
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMimeTypeOfSendingFile:Ljava/lang/String;

    #@6
    .line 276
    iput-object p2, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUriOfSendingFile:Ljava/lang/String;

    #@8
    .line 277
    iput-boolean p3, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mIsHandoverInitiated:Z

    #@a
    .line 278
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@d
    move-result-object v0

    #@e
    .line 279
    .local v0, uri:Landroid/net/Uri;
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@10
    invoke-static {v1, v0, p1}, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->generateFileInfo(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@13
    move-result-object v1

    #@14
    invoke-static {v0, v1}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->putSendFileInfo(Landroid/net/Uri;Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;)V

    #@17
    .line 281
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->storeApplicationData()V

    #@1a
    .line 282
    monitor-exit p0

    #@1b
    .line 283
    return-void

    #@1c
    .line 282
    .end local v0           #uri:Landroid/net/Uri;
    :catchall_1c
    move-exception v1

    #@1d
    monitor-exit p0
    :try_end_1e
    .catchall {:try_start_2 .. :try_end_1e} :catchall_1c

    #@1e
    throw v1
.end method

.method public saveSendingFileInfo(Ljava/lang/String;Ljava/util/ArrayList;Z)V
    .registers 7
    .parameter "mimeType"
    .parameter
    .parameter "isHandover"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;Z)V"
        }
    .end annotation

    #@0
    .prologue
    .line 286
    .local p2, uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    monitor-enter p0

    #@1
    .line 287
    const/4 v2, 0x1

    #@2
    :try_start_2
    iput-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMultipleFlag:Z

    #@4
    .line 288
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMimeTypeOfSendingFiles:Ljava/lang/String;

    #@6
    .line 289
    iput-object p2, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUrisOfSendingFiles:Ljava/util/ArrayList;

    #@8
    .line 290
    iput-boolean p3, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mIsHandoverInitiated:Z

    #@a
    .line 291
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    #@d
    move-result-object v0

    #@e
    .local v0, i$:Ljava/util/Iterator;
    :goto_e
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_27

    #@14
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@17
    move-result-object v1

    #@18
    check-cast v1, Landroid/net/Uri;

    #@1a
    .line 292
    .local v1, uri:Landroid/net/Uri;
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@1c
    invoke-static {v2, v1, p1}, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->generateFileInfo(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->putSendFileInfo(Landroid/net/Uri;Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;)V

    #@23
    goto :goto_e

    #@24
    .line 296
    .end local v0           #i$:Ljava/util/Iterator;
    .end local v1           #uri:Landroid/net/Uri;
    :catchall_24
    move-exception v2

    #@25
    monitor-exit p0
    :try_end_26
    .catchall {:try_start_2 .. :try_end_26} :catchall_24

    #@26
    throw v2

    #@27
    .line 295
    .restart local v0       #i$:Ljava/util/Iterator;
    :cond_27
    :try_start_27
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->storeApplicationData()V

    #@2a
    .line 296
    monitor-exit p0
    :try_end_2b
    .catchall {:try_start_27 .. :try_end_2b} :catchall_24

    #@2b
    .line 297
    return-void
.end method

.method public startTransfer(Landroid/bluetooth/BluetoothDevice;)V
    .registers 12
    .parameter "device"

    #@0
    .prologue
    .line 375
    const-string v1, "BluetoothOppManager"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Active InsertShareThread number is : "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget v3, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mInsertShareThreadNum:I

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 378
    monitor-enter p0

    #@1b
    .line 379
    :try_start_1b
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mInsertShareThreadNum:I

    #@1d
    const/4 v2, 0x3

    #@1e
    if-le v1, v2, :cond_58

    #@20
    .line 380
    const-string v1, "BluetoothOppManager"

    #@22
    const-string v2, "Too many shares user triggered concurrently!"

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 383
    new-instance v9, Landroid/content/Intent;

    #@29
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@2b
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;

    #@2d
    invoke-direct {v9, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@30
    .line 384
    .local v9, in:Landroid/content/Intent;
    const/high16 v1, 0x1000

    #@32
    invoke-virtual {v9, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@35
    .line 385
    const-string v1, "title"

    #@37
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@39
    const v3, 0x7f07003d

    #@3c
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@43
    .line 386
    const-string v1, "content"

    #@45
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@47
    const v3, 0x7f070047

    #@4a
    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v9, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@51
    .line 387
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mContext:Landroid/content/Context;

    #@53
    invoke-virtual {v1, v9}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@56
    .line 389
    monitor-exit p0

    #@57
    .line 400
    .end local v9           #in:Landroid/content/Intent;
    :goto_57
    return-void

    #@58
    .line 391
    :cond_58
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppManager$InsertShareInfoThread;

    #@5a
    iget-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMultipleFlag:Z

    #@5c
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMimeTypeOfSendingFile:Ljava/lang/String;

    #@5e
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUriOfSendingFile:Ljava/lang/String;

    #@60
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMimeTypeOfSendingFiles:Ljava/lang/String;

    #@62
    iget-object v7, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUrisOfSendingFiles:Ljava/util/ArrayList;

    #@64
    iget-boolean v8, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mIsHandoverInitiated:Z

    #@66
    move-object v1, p0

    #@67
    move-object v2, p1

    #@68
    invoke-direct/range {v0 .. v8}, Lcom/android/bluetooth/opp/BluetoothOppManager$InsertShareInfoThread;-><init>(Lcom/android/bluetooth/opp/BluetoothOppManager;Landroid/bluetooth/BluetoothDevice;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Z)V

    #@6b
    .line 394
    .local v0, insertThread:Lcom/android/bluetooth/opp/BluetoothOppManager$InsertShareInfoThread;
    iget-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMultipleFlag:Z

    #@6d
    if-eqz v1, :cond_77

    #@6f
    .line 395
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mUrisOfSendingFiles:Ljava/util/ArrayList;

    #@71
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@74
    move-result v1

    #@75
    iput v1, p0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mfileNumInBatch:I

    #@77
    .line 397
    :cond_77
    monitor-exit p0
    :try_end_78
    .catchall {:try_start_1b .. :try_end_78} :catchall_7c

    #@78
    .line 399
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppManager$InsertShareInfoThread;->start()V

    #@7b
    goto :goto_57

    #@7c
    .line 397
    .end local v0           #insertThread:Lcom/android/bluetooth/opp/BluetoothOppManager$InsertShareInfoThread;
    :catchall_7c
    move-exception v1

    #@7d
    :try_start_7d
    monitor-exit p0
    :try_end_7e
    .catchall {:try_start_7d .. :try_end_7e} :catchall_7c

    #@7e
    throw v1
.end method
