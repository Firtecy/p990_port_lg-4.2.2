.class public Lcom/android/bluetooth/opp/BluetoothOppHandoverReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothOppHandoverReceiver.java"


# static fields
.field private static final D:Z = true

.field public static final TAG:Ljava/lang/String; = "BluetoothOppHandoverReceiver"

.field private static final V:Z = true


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 28
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 15
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/4 v10, 0x1

    #@2
    const/4 v9, -0x1

    #@3
    .line 35
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@6
    move-result-object v0

    #@7
    .line 37
    .local v0, action:Ljava/lang/String;
    const-string v8, "android.btopp.intent.action.HANDOVER_SEND"

    #@9
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c
    move-result v8

    #@d
    if-nez v8, :cond_17

    #@f
    const-string v8, "android.btopp.intent.action.HANDOVER_SEND_MULTIPLE"

    #@11
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v8

    #@15
    if-eqz v8, :cond_87

    #@17
    .line 40
    :cond_17
    const-string v8, "android.bluetooth.device.extra.DEVICE"

    #@19
    invoke-virtual {p2, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@1c
    move-result-object v2

    #@1d
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@1f
    .line 42
    .local v2, device:Landroid/bluetooth/BluetoothDevice;
    if-nez v2, :cond_29

    #@21
    .line 44
    const-string v8, "BluetoothOppHandoverReceiver"

    #@23
    const-string v9, "No device attached to handover intent."

    #@25
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 102
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    :cond_28
    :goto_28
    return-void

    #@29
    .line 48
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    :cond_29
    const-string v8, "android.btopp.intent.action.HANDOVER_SEND"

    #@2b
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v8

    #@2f
    if-eqz v8, :cond_5c

    #@31
    .line 49
    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@34
    move-result-object v6

    #@35
    .line 50
    .local v6, type:Ljava/lang/String;
    const-string v8, "android.intent.extra.STREAM"

    #@37
    invoke-virtual {p2, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@3a
    move-result-object v5

    #@3b
    check-cast v5, Landroid/net/Uri;

    #@3d
    .line 51
    .local v5, stream:Landroid/net/Uri;
    if-eqz v5, :cond_54

    #@3f
    if-eqz v6, :cond_54

    #@41
    .line 54
    invoke-static {p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@44
    move-result-object v8

    #@45
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@48
    move-result-object v9

    #@49
    invoke-virtual {v8, v6, v9, v10}, Lcom/android/bluetooth/opp/BluetoothOppManager;->saveSendingFileInfo(Ljava/lang/String;Ljava/lang/String;Z)V

    #@4c
    .line 76
    .end local v5           #stream:Landroid/net/Uri;
    .end local v6           #type:Ljava/lang/String;
    :cond_4c
    :goto_4c
    invoke-static {p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@4f
    move-result-object v8

    #@50
    invoke-virtual {v8, v2}, Lcom/android/bluetooth/opp/BluetoothOppManager;->startTransfer(Landroid/bluetooth/BluetoothDevice;)V

    #@53
    goto :goto_28

    #@54
    .line 58
    .restart local v5       #stream:Landroid/net/Uri;
    .restart local v6       #type:Ljava/lang/String;
    :cond_54
    const-string v8, "BluetoothOppHandoverReceiver"

    #@56
    const-string v9, "No mimeType or stream attached to handover request"

    #@58
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    goto :goto_4c

    #@5c
    .line 61
    .end local v5           #stream:Landroid/net/Uri;
    .end local v6           #type:Ljava/lang/String;
    :cond_5c
    const-string v8, "android.btopp.intent.action.HANDOVER_SEND_MULTIPLE"

    #@5e
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v8

    #@62
    if-eqz v8, :cond_4c

    #@64
    .line 62
    new-instance v7, Ljava/util/ArrayList;

    #@66
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@69
    .line 63
    .local v7, uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@6c
    move-result-object v4

    #@6d
    .line 64
    .local v4, mimeType:Ljava/lang/String;
    const-string v8, "android.intent.extra.STREAM"

    #@6f
    invoke-virtual {p2, v8}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    #@72
    move-result-object v7

    #@73
    .line 65
    if-eqz v4, :cond_7f

    #@75
    if-eqz v7, :cond_7f

    #@77
    .line 66
    invoke-static {p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@7a
    move-result-object v8

    #@7b
    invoke-virtual {v8, v4, v7, v10}, Lcom/android/bluetooth/opp/BluetoothOppManager;->saveSendingFileInfo(Ljava/lang/String;Ljava/util/ArrayList;Z)V

    #@7e
    goto :goto_4c

    #@7f
    .line 70
    :cond_7f
    const-string v8, "BluetoothOppHandoverReceiver"

    #@81
    const-string v9, "No mimeType or stream attached to handover request"

    #@83
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    goto :goto_28

    #@87
    .line 77
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #mimeType:Ljava/lang/String;
    .end local v7           #uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_87
    const-string v8, "android.btopp.intent.action.WHITELIST_DEVICE"

    #@89
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v8

    #@8d
    if-eqz v8, :cond_c4

    #@8f
    .line 78
    const-string v8, "android.bluetooth.device.extra.DEVICE"

    #@91
    invoke-virtual {p2, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@94
    move-result-object v2

    #@95
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@97
    .line 81
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    const-string v8, "BluetoothOppHandoverReceiver"

    #@99
    new-instance v9, Ljava/lang/StringBuilder;

    #@9b
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9e
    const-string v10, "Adding "

    #@a0
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v9

    #@a4
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v9

    #@a8
    const-string v10, " to whitelist"

    #@aa
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v9

    #@ae
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v9

    #@b2
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 83
    if-eqz v2, :cond_28

    #@b7
    .line 86
    invoke-static {p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@ba
    move-result-object v8

    #@bb
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@be
    move-result-object v9

    #@bf
    invoke-virtual {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppManager;->addToWhitelist(Ljava/lang/String;)V

    #@c2
    goto/16 :goto_28

    #@c4
    .line 87
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    :cond_c4
    const-string v8, "android.btopp.intent.action.STOP_HANDOVER_TRANSFER"

    #@c6
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c9
    move-result v8

    #@ca
    if-eqz v8, :cond_112

    #@cc
    .line 88
    const-string v8, "android.btopp.intent.extra.BT_OPP_TRANSFER_ID"

    #@ce
    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@d1
    move-result v3

    #@d2
    .line 89
    .local v3, id:I
    if-eq v3, v9, :cond_28

    #@d4
    .line 90
    new-instance v8, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    sget-object v9, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@db
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v8

    #@df
    const-string v9, "/"

    #@e1
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v8

    #@e5
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v8

    #@e9
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ec
    move-result-object v8

    #@ed
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@f0
    move-result-object v1

    #@f1
    .line 93
    .local v1, contentUri:Landroid/net/Uri;
    const-string v8, "BluetoothOppHandoverReceiver"

    #@f3
    new-instance v9, Ljava/lang/StringBuilder;

    #@f5
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@f8
    const-string v10, "Stopping handover transfer with Uri "

    #@fa
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v9

    #@fe
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v9

    #@102
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v9

    #@106
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@109
    .line 95
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@10c
    move-result-object v8

    #@10d
    invoke-virtual {v8, v1, v11, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@110
    goto/16 :goto_28

    #@112
    .line 99
    .end local v1           #contentUri:Landroid/net/Uri;
    .end local v3           #id:I
    :cond_112
    const-string v8, "BluetoothOppHandoverReceiver"

    #@114
    new-instance v9, Ljava/lang/StringBuilder;

    #@116
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@119
    const-string v10, "Unknown action: "

    #@11b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v9

    #@11f
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v9

    #@123
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@126
    move-result-object v9

    #@127
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12a
    goto/16 :goto_28
.end method
