.class Lcom/android/bluetooth/opp/TestTcpListener;
.super Ljava/lang/Object;
.source "TestActivity.java"


# static fields
.field private static final ACCEPT_WAIT_TIMEOUT:I = 0x1388

.field private static final D:Z = true

.field public static final DEFAULT_OPP_CHANNEL:I = 0xc

.field public static final MSG_INCOMING_BTOPP_CONNECTION:I = 0x64

.field private static final TAG:Ljava/lang/String; = "BtOppRfcommListener"

.field private static final V:Z = true


# instance fields
.field private mBtOppRfcommChannel:I

.field private mCallback:Landroid/os/Handler;

.field private volatile mInterrupted:Z

.field private mSocketAcceptThread:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 375
    const/16 v0, 0xc

    #@2
    invoke-direct {p0, v0}, Lcom/android/bluetooth/opp/TestTcpListener;-><init>(I)V

    #@5
    .line 376
    return-void
.end method

.method public constructor <init>(I)V
    .registers 3
    .parameter "channel"

    #@0
    .prologue
    .line 378
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 372
    const/4 v0, -0x1

    #@4
    iput v0, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mBtOppRfcommChannel:I

    #@6
    .line 379
    iput p1, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mBtOppRfcommChannel:I

    #@8
    .line 380
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/opp/TestTcpListener;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 352
    iget v0, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mBtOppRfcommChannel:I

    #@2
    return v0
.end method

.method static synthetic access$100(Lcom/android/bluetooth/opp/TestTcpListener;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 352
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mInterrupted:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/android/bluetooth/opp/TestTcpListener;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 352
    iput-boolean p1, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mInterrupted:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/bluetooth/opp/TestTcpListener;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 352
    iget-object v0, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mCallback:Landroid/os/Handler;

    #@2
    return-object v0
.end method


# virtual methods
.method public declared-synchronized start(Landroid/os/Handler;)Z
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 383
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@3
    if-nez v0, :cond_18

    #@5
    .line 384
    iput-object p1, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mCallback:Landroid/os/Handler;

    #@7
    .line 385
    new-instance v0, Lcom/android/bluetooth/opp/TestTcpListener$1;

    #@9
    const-string v1, "BtOppRfcommListener"

    #@b
    invoke-direct {v0, p0, v1}, Lcom/android/bluetooth/opp/TestTcpListener$1;-><init>(Lcom/android/bluetooth/opp/TestTcpListener;Ljava/lang/String;)V

    #@e
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@10
    .line 438
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mInterrupted:Z

    #@13
    .line 439
    iget-object v0, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@15
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_1b

    #@18
    .line 442
    :cond_18
    const/4 v0, 0x1

    #@19
    monitor-exit p0

    #@1a
    return v0

    #@1b
    .line 383
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0

    #@1d
    throw v0
.end method

.method public declared-synchronized stop()V
    .registers 4

    #@0
    .prologue
    .line 447
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@3
    if-eqz v1, :cond_26

    #@5
    .line 449
    const-string v1, "BtOppRfcommListener"

    #@7
    const-string v2, "stopping Connect Thread"

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 451
    const/4 v1, 0x1

    #@d
    iput-boolean v1, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mInterrupted:Z
    :try_end_f
    .catchall {:try_start_1 .. :try_end_f} :catchall_31

    #@f
    .line 453
    :try_start_f
    iget-object v1, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@11
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    #@14
    .line 455
    const-string v1, "BtOppRfcommListener"

    #@16
    const-string v2, "waiting for thread to terminate"

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 457
    iget-object v1, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@1d
    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    #@20
    .line 458
    const/4 v1, 0x0

    #@21
    iput-object v1, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@23
    .line 459
    const/4 v1, 0x0

    #@24
    iput-object v1, p0, Lcom/android/bluetooth/opp/TestTcpListener;->mCallback:Landroid/os/Handler;
    :try_end_26
    .catchall {:try_start_f .. :try_end_26} :catchall_31
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_26} :catch_28

    #@26
    .line 466
    :cond_26
    :goto_26
    monitor-exit p0

    #@27
    return-void

    #@28
    .line 460
    :catch_28
    move-exception v0

    #@29
    .line 462
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_29
    const-string v1, "BtOppRfcommListener"

    #@2b
    const-string v2, "Interrupted waiting for Accept Thread to join"

    #@2d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_30
    .catchall {:try_start_29 .. :try_end_30} :catchall_31

    #@30
    goto :goto_26

    #@31
    .line 447
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_31
    move-exception v1

    #@32
    monitor-exit p0

    #@33
    throw v1
.end method
