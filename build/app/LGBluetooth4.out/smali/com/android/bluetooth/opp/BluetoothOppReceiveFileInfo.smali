.class public Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;
.super Ljava/lang/Object;
.source "BluetoothOppReceiveFileInfo.java"


# static fields
.field private static final D:Z = true

.field private static final V:Z = true

.field private static sDesiredStoragePath:Ljava/lang/String;


# instance fields
.field public mData:Ljava/lang/String;

.field public mFileName:Ljava/lang/String;

.field public mLength:J

.field public mOutputStream:Ljava/io/FileOutputStream;

.field public mStatus:I


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 61
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->sDesiredStoragePath:Ljava/lang/String;

    #@3
    return-void
.end method

.method public constructor <init>(I)V
    .registers 8
    .parameter "status"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 89
    const-wide/16 v2, 0x0

    #@3
    move-object v0, p0

    #@4
    move-object v4, v1

    #@5
    move v5, p1

    #@6
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(Ljava/lang/String;JLjava/io/FileOutputStream;I)V

    #@9
    .line 90
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JI)V
    .registers 5
    .parameter "data"
    .parameter "length"
    .parameter "status"

    #@0
    .prologue
    .line 74
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 75
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mData:Ljava/lang/String;

    #@5
    .line 76
    iput p4, p0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mStatus:I

    #@7
    .line 77
    iput-wide p2, p0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mLength:J

    #@9
    .line 78
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JLjava/io/FileOutputStream;I)V
    .registers 6
    .parameter "filename"
    .parameter "length"
    .parameter "outputStream"
    .parameter "status"

    #@0
    .prologue
    .line 81
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 82
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mFileName:Ljava/lang/String;

    #@5
    .line 83
    iput-object p4, p0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mOutputStream:Ljava/io/FileOutputStream;

    #@7
    .line 84
    iput p5, p0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mStatus:I

    #@9
    .line 85
    iput-wide p2, p0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mLength:J

    #@b
    .line 86
    return-void
.end method

.method private static chooseUniquefilename(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "filename"
    .parameter "extension"

    #@0
    .prologue
    .line 264
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8
    move-result-object v5

    #@9
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v5

    #@d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10
    move-result-object v0

    #@11
    .line 265
    .local v0, fullfilename:Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    #@13
    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@16
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    #@19
    move-result v5

    #@1a
    if-nez v5, :cond_1e

    #@1c
    move-object v5, v0

    #@1d
    .line 294
    :goto_1d
    return-object v5

    #@1e
    .line 268
    :cond_1e
    new-instance v5, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    sget-object v6, Lcom/android/bluetooth/opp/Constants;->filename_SEQUENCE_SEPARATOR:Ljava/lang/String;

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object p0

    #@31
    .line 280
    new-instance v3, Ljava/util/Random;

    #@33
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@36
    move-result-wide v5

    #@37
    invoke-direct {v3, v5, v6}, Ljava/util/Random;-><init>(J)V

    #@3a
    .line 281
    .local v3, rnd:Ljava/util/Random;
    const/4 v4, 0x1

    #@3b
    .line 282
    .local v4, sequence:I
    const/4 v2, 0x1

    #@3c
    .local v2, magnitude:I
    :goto_3c
    const v5, 0x3b9aca00

    #@3f
    if-ge v2, v5, :cond_93

    #@41
    .line 283
    const/4 v1, 0x0

    #@42
    .local v1, iteration:I
    :goto_42
    const/16 v5, 0x9

    #@44
    if-ge v1, v5, :cond_90

    #@46
    .line 284
    new-instance v5, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52
    move-result-object v5

    #@53
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v5

    #@57
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5a
    move-result-object v0

    #@5b
    .line 285
    new-instance v5, Ljava/io/File;

    #@5d
    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@60
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    #@63
    move-result v5

    #@64
    if-nez v5, :cond_68

    #@66
    move-object v5, v0

    #@67
    .line 286
    goto :goto_1d

    #@68
    .line 289
    :cond_68
    const-string v5, "BluetoothOpp"

    #@6a
    new-instance v6, Ljava/lang/StringBuilder;

    #@6c
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6f
    const-string v7, "file with sequence number "

    #@71
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v6

    #@75
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@78
    move-result-object v6

    #@79
    const-string v7, " exists"

    #@7b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v6

    #@7f
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v6

    #@83
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 291
    invoke-virtual {v3, v2}, Ljava/util/Random;->nextInt(I)I

    #@89
    move-result v5

    #@8a
    add-int/lit8 v5, v5, 0x1

    #@8c
    add-int/2addr v4, v5

    #@8d
    .line 283
    add-int/lit8 v1, v1, 0x1

    #@8f
    goto :goto_42

    #@90
    .line 282
    :cond_90
    mul-int/lit8 v2, v2, 0xa

    #@92
    goto :goto_3c

    #@93
    .line 294
    .end local v1           #iteration:I
    :cond_93
    const/4 v5, 0x0

    #@94
    goto :goto_1d
.end method

.method private static choosefilename(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "hint"

    #@0
    .prologue
    const/16 v4, 0x2f

    #@2
    .line 298
    const/4 v0, 0x0

    #@3
    .line 301
    .local v0, filename:Ljava/lang/String;
    if-nez v0, :cond_40

    #@5
    if-eqz p0, :cond_40

    #@7
    const-string v2, "/"

    #@9
    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_40

    #@f
    const-string v2, "\\"

    #@11
    invoke-virtual {p0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_40

    #@17
    .line 304
    const/16 v2, 0x5c

    #@19
    invoke-virtual {p0, v2, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@1c
    move-result-object p0

    #@1d
    .line 306
    const-string v2, "\\s"

    #@1f
    const-string v3, " "

    #@21
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object p0

    #@25
    .line 309
    const-string v2, "[:\"<>*?|]"

    #@27
    const-string v3, "_"

    #@29
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2c
    move-result-object p0

    #@2d
    .line 311
    const-string v2, "BluetoothOpp"

    #@2f
    const-string v3, "getting filename from hint"

    #@31
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 313
    invoke-virtual {p0, v4}, Ljava/lang/String;->lastIndexOf(I)I

    #@37
    move-result v2

    #@38
    add-int/lit8 v1, v2, 0x1

    #@3a
    .line 314
    .local v1, index:I
    if-lez v1, :cond_41

    #@3c
    .line 315
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 320
    .end local v1           #index:I
    :cond_40
    :goto_40
    return-object v0

    #@41
    .line 317
    .restart local v1       #index:I
    :cond_41
    move-object v0, p0

    #@42
    goto :goto_40
.end method

.method public static generateFileInfo(Landroid/content/Context;I)Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;
    .registers 27
    .parameter "context"
    .parameter "id"

    #@0
    .prologue
    .line 95
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v2

    #@4
    .line 96
    .local v2, contentResolver:Landroid/content/ContentResolver;
    new-instance v4, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    sget-object v8, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@b
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v4

    #@f
    const-string v8, "/"

    #@11
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v4

    #@15
    move/from16 v0, p1

    #@17
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@22
    move-result-object v3

    #@23
    .line 97
    .local v3, contentUri:Landroid/net/Uri;
    const/4 v15, 0x0

    #@24
    .line 98
    .local v15, filename:Ljava/lang/String;
    const/16 v16, 0x0

    #@26
    .line 99
    .local v16, hint:Ljava/lang/String;
    const/16 v21, 0x0

    #@28
    .line 100
    .local v21, mimeType:Ljava/lang/String;
    const-wide/16 v18, 0x0

    #@2a
    .line 101
    .local v18, length:J
    const/4 v4, 0x3

    #@2b
    new-array v4, v4, [Ljava/lang/String;

    #@2d
    const/4 v8, 0x0

    #@2e
    const-string v9, "hint"

    #@30
    aput-object v9, v4, v8

    #@32
    const/4 v8, 0x1

    #@33
    const-string v9, "total_bytes"

    #@35
    aput-object v9, v4, v8

    #@37
    const/4 v8, 0x2

    #@38
    const-string v9, "mimetype"

    #@3a
    aput-object v9, v4, v8

    #@3c
    const/4 v5, 0x0

    #@3d
    const/4 v6, 0x0

    #@3e
    const/4 v7, 0x0

    #@3f
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@42
    move-result-object v20

    #@43
    .line 104
    .local v20, metadataCursor:Landroid/database/Cursor;
    if-eqz v20, :cond_1f8

    #@45
    .line 106
    :try_start_45
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->moveToFirst()Z

    #@48
    move-result v4

    #@49
    if-eqz v4, :cond_1f4

    #@4b
    .line 107
    const/4 v4, 0x0

    #@4c
    move-object/from16 v0, v20

    #@4e
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@51
    move-result-object v16

    #@52
    .line 108
    const/4 v4, 0x1

    #@53
    move-object/from16 v0, v20

    #@55
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I
    :try_end_58
    .catchall {:try_start_45 .. :try_end_58} :catchall_90

    #@58
    move-result v4

    #@59
    int-to-long v6, v4

    #@5a
    .line 109
    .end local v18           #length:J
    .local v6, length:J
    const/4 v4, 0x2

    #@5b
    :try_start_5b
    move-object/from16 v0, v20

    #@5d
    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_60
    .catchall {:try_start_5b .. :try_end_60} :catchall_1f1

    #@60
    move-result-object v21

    #@61
    .line 112
    :goto_61
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    #@64
    .line 117
    :goto_64
    new-instance v23, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;

    #@66
    move-object/from16 v0, v23

    #@68
    move-object/from16 v1, p0

    #@6a
    invoke-direct {v0, v1, v6, v7}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;-><init>(Landroid/content/Context;J)V

    #@6d
    .line 118
    .local v23, storage:Lcom/lge/bluetooth/opp/BluetoothStorageUtility;
    invoke-virtual/range {v23 .. v23}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->checkInternalStorage()I

    #@70
    move-result v22

    #@71
    .line 119
    .local v22, status:I
    const/16 v4, 0x1ee

    #@73
    move/from16 v0, v22

    #@75
    if-ne v0, v4, :cond_bf

    #@77
    .line 121
    invoke-virtual/range {v23 .. v23}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->checkExternalStorage()I

    #@7a
    move-result v22

    #@7b
    .line 122
    const/16 v4, 0x1ed

    #@7d
    move/from16 v0, v22

    #@7f
    if-ne v0, v4, :cond_97

    #@81
    .line 123
    const-string v4, "BluetoothOpp"

    #@83
    const-string v8, "Not enough free space of internal storage and No external sdcard"

    #@85
    invoke-static {v4, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 124
    new-instance v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@8a
    const/16 v8, 0x1ee

    #@8c
    invoke-direct {v4, v8}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(I)V

    #@8f
    .line 232
    :goto_8f
    return-object v4

    #@90
    .line 112
    .end local v6           #length:J
    .end local v22           #status:I
    .end local v23           #storage:Lcom/lge/bluetooth/opp/BluetoothStorageUtility;
    .restart local v18       #length:J
    :catchall_90
    move-exception v4

    #@91
    move-wide/from16 v6, v18

    #@93
    .end local v18           #length:J
    .restart local v6       #length:J
    :goto_93
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    #@96
    throw v4

    #@97
    .line 125
    .restart local v22       #status:I
    .restart local v23       #storage:Lcom/lge/bluetooth/opp/BluetoothStorageUtility;
    :cond_97
    const/16 v4, 0xc8

    #@99
    move/from16 v0, v22

    #@9b
    if-eq v0, v4, :cond_e7

    #@9d
    .line 126
    const-string v4, "BluetoothOpp"

    #@9f
    new-instance v8, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v9, "Checked status of external storage : "

    #@a6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v8

    #@aa
    move/from16 v0, v22

    #@ac
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@af
    move-result-object v8

    #@b0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v8

    #@b4
    invoke-static {v4, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b7
    .line 127
    new-instance v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@b9
    move/from16 v0, v22

    #@bb
    invoke-direct {v4, v0}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(I)V

    #@be
    goto :goto_8f

    #@bf
    .line 130
    :cond_bf
    const/16 v4, 0xc8

    #@c1
    move/from16 v0, v22

    #@c3
    if-eq v0, v4, :cond_e7

    #@c5
    .line 131
    const-string v4, "BluetoothOpp"

    #@c7
    new-instance v8, Ljava/lang/StringBuilder;

    #@c9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@cc
    const-string v9, "Checked status of internal storage : "

    #@ce
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v8

    #@d2
    move/from16 v0, v22

    #@d4
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d7
    move-result-object v8

    #@d8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@db
    move-result-object v8

    #@dc
    invoke-static {v4, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 132
    new-instance v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@e1
    move/from16 v0, v22

    #@e3
    invoke-direct {v4, v0}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(I)V

    #@e6
    goto :goto_8f

    #@e7
    .line 135
    :cond_e7
    invoke-static {}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->getBtFile()Ljava/io/File;

    #@ea
    move-result-object v10

    #@eb
    .line 136
    .local v10, base:Ljava/io/File;
    if-nez v10, :cond_fc

    #@ed
    .line 137
    const-string v4, "BluetoothOpp"

    #@ef
    const-string v8, "Receive File aborted - NO storage"

    #@f1
    invoke-static {v4, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f4
    .line 138
    new-instance v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@f6
    const/16 v8, 0x1ed

    #@f8
    invoke-direct {v4, v8}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(I)V

    #@fb
    goto :goto_8f

    #@fc
    .line 178
    :cond_fc
    invoke-static/range {v16 .. v16}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->choosefilename(Ljava/lang/String;)Ljava/lang/String;

    #@ff
    move-result-object v15

    #@100
    .line 179
    if-nez v15, :cond_10a

    #@102
    .line 181
    new-instance v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@104
    const/16 v8, 0x1ec

    #@106
    invoke-direct {v4, v8}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(I)V

    #@109
    goto :goto_8f

    #@10a
    .line 183
    :cond_10a
    const/4 v14, 0x0

    #@10b
    .line 184
    .local v14, extension:Ljava/lang/String;
    const-string v4, "."

    #@10d
    invoke-virtual {v15, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@110
    move-result v12

    #@111
    .line 185
    .local v12, dotIndex:I
    if-gez v12, :cond_14e

    #@113
    .line 186
    if-nez v21, :cond_11e

    #@115
    .line 188
    new-instance v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@117
    const/16 v8, 0x1ec

    #@119
    invoke-direct {v4, v8}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(I)V

    #@11c
    goto/16 :goto_8f

    #@11e
    .line 190
    :cond_11e
    const-string v14, ""

    #@120
    .line 196
    :goto_120
    new-instance v4, Ljava/lang/StringBuilder;

    #@122
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@125
    invoke-virtual {v10}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@128
    move-result-object v8

    #@129
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v4

    #@12d
    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    #@12f
    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@132
    move-result-object v4

    #@133
    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v4

    #@137
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13a
    move-result-object v15

    #@13b
    .line 198
    invoke-static {v15, v14}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->chooseUniquefilename(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@13e
    move-result-object v5

    #@13f
    .line 200
    .local v5, fullfilename:Ljava/lang/String;
    invoke-static {v5}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->safeCanonicalPath(Ljava/lang/String;)Z

    #@142
    move-result v4

    #@143
    if-nez v4, :cond_158

    #@145
    .line 202
    new-instance v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@147
    const/16 v8, 0x1ec

    #@149
    invoke-direct {v4, v8}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(I)V

    #@14c
    goto/16 :goto_8f

    #@14e
    .line 193
    .end local v5           #fullfilename:Ljava/lang/String;
    :cond_14e
    invoke-virtual {v15, v12}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@151
    move-result-object v14

    #@152
    .line 194
    const/4 v4, 0x0

    #@153
    invoke-virtual {v15, v4, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@156
    move-result-object v15

    #@157
    goto :goto_120

    #@158
    .line 205
    .restart local v5       #fullfilename:Ljava/lang/String;
    :cond_158
    const-string v4, "BluetoothOpp"

    #@15a
    new-instance v8, Ljava/lang/StringBuilder;

    #@15c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@15f
    const-string v9, "Generated received filename "

    #@161
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v8

    #@165
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v8

    #@169
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16c
    move-result-object v8

    #@16d
    invoke-static {v4, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@170
    .line 208
    if-eqz v5, :cond_1e8

    #@172
    .line 210
    :try_start_172
    new-instance v4, Ljava/io/FileOutputStream;

    #@174
    invoke-direct {v4, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@177
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    #@17a
    .line 211
    const/16 v4, 0x2f

    #@17c
    invoke-virtual {v5, v4}, Ljava/lang/String;->lastIndexOf(I)I

    #@17f
    move-result v4

    #@180
    add-int/lit8 v17, v4, 0x1

    #@182
    .line 213
    .local v17, index:I
    if-lez v17, :cond_1b9

    #@184
    .line 214
    move/from16 v0, v17

    #@186
    invoke-virtual {v5, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@189
    move-result-object v11

    #@18a
    .line 216
    .local v11, displayName:Ljava/lang/String;
    const-string v4, "BluetoothOpp"

    #@18c
    new-instance v8, Ljava/lang/StringBuilder;

    #@18e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@191
    const-string v9, "New display name "

    #@193
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@196
    move-result-object v8

    #@197
    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19a
    move-result-object v8

    #@19b
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19e
    move-result-object v8

    #@19f
    invoke-static {v4, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a2
    .line 218
    new-instance v24, Landroid/content/ContentValues;

    #@1a4
    invoke-direct/range {v24 .. v24}, Landroid/content/ContentValues;-><init>()V

    #@1a7
    .line 219
    .local v24, updateValues:Landroid/content/ContentValues;
    const-string v4, "hint"

    #@1a9
    move-object/from16 v0, v24

    #@1ab
    invoke-virtual {v0, v4, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@1ae
    .line 220
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b1
    move-result-object v4

    #@1b2
    const/4 v8, 0x0

    #@1b3
    const/4 v9, 0x0

    #@1b4
    move-object/from16 v0, v24

    #@1b6
    invoke-virtual {v4, v3, v0, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@1b9
    .line 223
    .end local v11           #displayName:Ljava/lang/String;
    .end local v24           #updateValues:Landroid/content/ContentValues;
    :cond_1b9
    new-instance v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@1bb
    new-instance v8, Ljava/io/FileOutputStream;

    #@1bd
    invoke-direct {v8, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    #@1c0
    const/4 v9, 0x0

    #@1c1
    invoke-direct/range {v4 .. v9}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(Ljava/lang/String;JLjava/io/FileOutputStream;I)V
    :try_end_1c4
    .catch Ljava/io/IOException; {:try_start_172 .. :try_end_1c4} :catch_1c6

    #@1c4
    goto/16 :goto_8f

    #@1c6
    .line 225
    .end local v17           #index:I
    :catch_1c6
    move-exception v13

    #@1c7
    .line 227
    .local v13, e:Ljava/io/IOException;
    const-string v4, "BluetoothOpp"

    #@1c9
    new-instance v8, Ljava/lang/StringBuilder;

    #@1cb
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@1ce
    const-string v9, "Error when creating file "

    #@1d0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d3
    move-result-object v8

    #@1d4
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d7
    move-result-object v8

    #@1d8
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1db
    move-result-object v8

    #@1dc
    invoke-static {v4, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1df
    .line 229
    new-instance v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@1e1
    const/16 v8, 0x1ec

    #@1e3
    invoke-direct {v4, v8}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(I)V

    #@1e6
    goto/16 :goto_8f

    #@1e8
    .line 232
    .end local v13           #e:Ljava/io/IOException;
    :cond_1e8
    new-instance v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@1ea
    const/16 v8, 0x1ec

    #@1ec
    invoke-direct {v4, v8}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;-><init>(I)V

    #@1ef
    goto/16 :goto_8f

    #@1f1
    .line 112
    .end local v5           #fullfilename:Ljava/lang/String;
    .end local v10           #base:Ljava/io/File;
    .end local v12           #dotIndex:I
    .end local v14           #extension:Ljava/lang/String;
    .end local v22           #status:I
    .end local v23           #storage:Lcom/lge/bluetooth/opp/BluetoothStorageUtility;
    :catchall_1f1
    move-exception v4

    #@1f2
    goto/16 :goto_93

    #@1f4
    .end local v6           #length:J
    .restart local v18       #length:J
    :cond_1f4
    move-wide/from16 v6, v18

    #@1f6
    .end local v18           #length:J
    .restart local v6       #length:J
    goto/16 :goto_61

    #@1f8
    .end local v6           #length:J
    .restart local v18       #length:J
    :cond_1f8
    move-wide/from16 v6, v18

    #@1fa
    .end local v18           #length:J
    .restart local v6       #length:J
    goto/16 :goto_64
.end method

.method private static safeCanonicalPath(Ljava/lang/String;)Z
    .registers 6
    .parameter "uniqueFileName"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 239
    :try_start_1
    new-instance v2, Ljava/io/File;

    #@3
    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6
    .line 240
    .local v2, receiveFile:Ljava/io/File;
    sget-object v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->sDesiredStoragePath:Ljava/lang/String;

    #@8
    if-nez v4, :cond_10

    #@a
    .line 242
    invoke-static {}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->getBtStoragePath()Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    sput-object v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->sDesiredStoragePath:Ljava/lang/String;

    #@10
    .line 249
    :cond_10
    invoke-virtual {v2}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    .line 252
    .local v0, canonicalPath:Ljava/lang/String;
    sget-object v4, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->sDesiredStoragePath:Ljava/lang/String;

    #@16
    invoke-virtual {v0, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_19} :catch_1f

    #@19
    move-result v4

    #@1a
    if-nez v4, :cond_1d

    #@1c
    .line 259
    .end local v0           #canonicalPath:Ljava/lang/String;
    .end local v2           #receiveFile:Ljava/io/File;
    :goto_1c
    return v3

    #@1d
    .line 256
    .restart local v0       #canonicalPath:Ljava/lang/String;
    .restart local v2       #receiveFile:Ljava/io/File;
    :cond_1d
    const/4 v3, 0x1

    #@1e
    goto :goto_1c

    #@1f
    .line 257
    .end local v0           #canonicalPath:Ljava/lang/String;
    .end local v2           #receiveFile:Ljava/io/File;
    :catch_1f
    move-exception v1

    #@20
    .line 259
    .local v1, ioe:Ljava/io/IOException;
    goto :goto_1c
.end method
