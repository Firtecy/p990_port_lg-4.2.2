.class Lcom/android/bluetooth/opp/TestActivity$1;
.super Ljava/lang/Object;
.source "TestActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/opp/TestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/opp/TestActivity;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/opp/TestActivity;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 216
    iput-object p1, p0, Lcom/android/bluetooth/opp/TestActivity$1;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 13
    .parameter "view"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 219
    const/4 v0, 0x0

    #@2
    .line 220
    .local v0, address:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/bluetooth/opp/TestActivity$1;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@4
    iget-object v7, v7, Lcom/android/bluetooth/opp/TestActivity;->mAddressView:Landroid/widget/EditText;

    #@6
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@9
    move-result-object v7

    #@a
    invoke-interface {v7}, Landroid/text/Editable;->length()I

    #@d
    move-result v7

    #@e
    if-eqz v7, :cond_34

    #@10
    .line 221
    iget-object v7, p0, Lcom/android/bluetooth/opp/TestActivity$1;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@12
    iget-object v7, v7, Lcom/android/bluetooth/opp/TestActivity;->mAddressView:Landroid/widget/EditText;

    #@14
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@17
    move-result-object v7

    #@18
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 222
    const-string v7, "BluetoothOpp"

    #@1e
    new-instance v8, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v9, "Send to address  "

    #@25
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v8

    #@29
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v8

    #@2d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v8

    #@31
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 224
    :cond_34
    if-nez v0, :cond_38

    #@36
    .line 225
    const-string v0, "00:17:83:58:5D:CC"

    #@38
    .line 228
    :cond_38
    const/4 v3, 0x0

    #@39
    .line 229
    .local v3, media:Ljava/lang/Integer;
    iget-object v7, p0, Lcom/android/bluetooth/opp/TestActivity$1;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@3b
    iget-object v7, v7, Lcom/android/bluetooth/opp/TestActivity;->mMediaView:Landroid/widget/EditText;

    #@3d
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@40
    move-result-object v7

    #@41
    invoke-interface {v7}, Landroid/text/Editable;->length()I

    #@44
    move-result v7

    #@45
    if-eqz v7, :cond_77

    #@47
    .line 230
    iget-object v7, p0, Lcom/android/bluetooth/opp/TestActivity$1;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@49
    iget-object v7, v7, Lcom/android/bluetooth/opp/TestActivity;->mMediaView:Landroid/widget/EditText;

    #@4b
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@4e
    move-result-object v7

    #@4f
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@5a
    move-result v7

    #@5b
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5e
    move-result-object v3

    #@5f
    .line 231
    const-string v7, "BluetoothOpp"

    #@61
    new-instance v8, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v9, "Send media no.  "

    #@68
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v8

    #@6c
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v8

    #@70
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v8

    #@74
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 233
    :cond_77
    if-nez v3, :cond_7d

    #@79
    .line 234
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@7c
    move-result-object v3

    #@7d
    .line 236
    :cond_7d
    new-instance v6, Landroid/content/ContentValues;

    #@7f
    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    #@82
    .line 237
    .local v6, values:Landroid/content/ContentValues;
    const-string v7, "uri"

    #@84
    new-instance v8, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v9, "content://media/external/images/media/"

    #@8b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v8

    #@8f
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v8

    #@93
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v8

    #@97
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9a
    .line 252
    const-string v7, "destination"

    #@9c
    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9f
    .line 254
    const-string v7, "direction"

    #@a1
    const/4 v8, 0x0

    #@a2
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a5
    move-result-object v8

    #@a6
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@a9
    .line 256
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@ac
    move-result-wide v7

    #@ad
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@b0
    move-result-object v5

    #@b1
    .line 257
    .local v5, ts:Ljava/lang/Long;
    const-string v7, "timestamp"

    #@b3
    invoke-virtual {v6, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@b6
    .line 259
    const/4 v4, 0x0

    #@b7
    .line 260
    .local v4, records:Ljava/lang/Integer;
    iget-object v7, p0, Lcom/android/bluetooth/opp/TestActivity$1;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@b9
    iget-object v7, v7, Lcom/android/bluetooth/opp/TestActivity;->mInsertView:Landroid/widget/EditText;

    #@bb
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@be
    move-result-object v7

    #@bf
    invoke-interface {v7}, Landroid/text/Editable;->length()I

    #@c2
    move-result v7

    #@c3
    if-eqz v7, :cond_f5

    #@c5
    .line 261
    iget-object v7, p0, Lcom/android/bluetooth/opp/TestActivity$1;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@c7
    iget-object v7, v7, Lcom/android/bluetooth/opp/TestActivity;->mInsertView:Landroid/widget/EditText;

    #@c9
    invoke-virtual {v7}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@cc
    move-result-object v7

    #@cd
    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d0
    move-result-object v7

    #@d1
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@d4
    move-result-object v7

    #@d5
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@d8
    move-result v7

    #@d9
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@dc
    move-result-object v4

    #@dd
    .line 262
    const-string v7, "BluetoothOpp"

    #@df
    new-instance v8, Ljava/lang/StringBuilder;

    #@e1
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@e4
    const-string v9, "parseInt  "

    #@e6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v8

    #@ea
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v8

    #@ee
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f1
    move-result-object v8

    #@f2
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f5
    .line 264
    :cond_f5
    if-nez v4, :cond_fb

    #@f7
    .line 265
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@fa
    move-result-object v4

    #@fb
    .line 267
    :cond_fb
    const/4 v2, 0x0

    #@fc
    .local v2, i:I
    :goto_fc
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    #@ff
    move-result v7

    #@100
    if-ge v2, v7, :cond_153

    #@102
    .line 268
    iget-object v7, p0, Lcom/android/bluetooth/opp/TestActivity$1;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@104
    invoke-virtual {v7}, Lcom/android/bluetooth/opp/TestActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@107
    move-result-object v7

    #@108
    sget-object v8, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@10a
    invoke-virtual {v7, v8, v6}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@10d
    move-result-object v1

    #@10e
    .line 269
    .local v1, contentUri:Landroid/net/Uri;
    const-string v7, "BluetoothOpp"

    #@110
    new-instance v8, Ljava/lang/StringBuilder;

    #@112
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@115
    const-string v9, "insert contentUri: "

    #@117
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v8

    #@11b
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11e
    move-result-object v8

    #@11f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@122
    move-result-object v8

    #@123
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@126
    .line 270
    iget-object v8, p0, Lcom/android/bluetooth/opp/TestActivity$1;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@128
    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@12b
    move-result-object v7

    #@12c
    invoke-interface {v7, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@12f
    move-result-object v7

    #@130
    check-cast v7, Ljava/lang/String;

    #@132
    iput-object v7, v8, Lcom/android/bluetooth/opp/TestActivity;->currentInsert:Ljava/lang/String;

    #@134
    .line 271
    const-string v7, "BluetoothOpp"

    #@136
    new-instance v8, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v9, "currentInsert = "

    #@13d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v8

    #@141
    iget-object v9, p0, Lcom/android/bluetooth/opp/TestActivity$1;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@143
    iget-object v9, v9, Lcom/android/bluetooth/opp/TestActivity;->currentInsert:Ljava/lang/String;

    #@145
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@148
    move-result-object v8

    #@149
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14c
    move-result-object v8

    #@14d
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@150
    .line 267
    add-int/lit8 v2, v2, 0x1

    #@152
    goto :goto_fc

    #@153
    .line 274
    .end local v1           #contentUri:Landroid/net/Uri;
    :cond_153
    return-void
.end method
