.class Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;
.super Ljava/lang/Thread;
.source "BluetoothOppRfcommListener.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->start(Landroid/os/Handler;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 80
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@2
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 14

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    .line 115
    const/4 v6, 0x1

    #@2
    .line 121
    .local v6, serverOK:Z
    const/4 v3, 0x0

    #@3
    .local v3, i:I
    :goto_3
    const/16 v8, 0xa

    #@5
    if-ge v3, v8, :cond_74

    #@7
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@9
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mInterrupted:Z
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$000(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Z

    #@c
    move-result v8

    #@d
    if-nez v8, :cond_74

    #@f
    .line 124
    :try_start_f
    const-string v8, "BtOppRfcommListener"

    #@11
    const-string v9, "Starting RFCOMM listener...."

    #@13
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 126
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@18
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@1a
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$200(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Landroid/bluetooth/BluetoothAdapter;

    #@1d
    move-result-object v9

    #@1e
    const-string v10, "OBEX Object Push"

    #@20
    sget-object v11, Landroid/bluetooth/BluetoothUuid;->ObexObjectPush:Landroid/os/ParcelUuid;

    #@22
    invoke-virtual {v11}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@25
    move-result-object v11

    #@26
    invoke-virtual {v9, v10, v11}, Landroid/bluetooth/BluetoothAdapter;->listenUsingInsecureRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;

    #@29
    move-result-object v9

    #@2a
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$102(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;Landroid/bluetooth/BluetoothServerSocket;)Landroid/bluetooth/BluetoothServerSocket;

    #@2d
    .line 128
    const-string v8, "BtOppRfcommListener"

    #@2f
    const-string v9, "Started RFCOMM listener...."

    #@31
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_34} :catch_47

    #@34
    .line 135
    :goto_34
    if-nez v6, :cond_74

    #@36
    .line 136
    monitor-enter p0

    #@37
    .line 139
    :try_start_37
    const-string v8, "BtOppRfcommListener"

    #@39
    const-string v9, "Wait 300 ms"

    #@3b
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 141
    const-wide/16 v8, 0x12c

    #@40
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_43
    .catchall {:try_start_37 .. :try_end_43} :catchall_71
    .catch Ljava/lang/InterruptedException; {:try_start_37 .. :try_end_43} :catch_62

    #@43
    .line 146
    :goto_43
    :try_start_43
    monitor-exit p0
    :try_end_44
    .catchall {:try_start_43 .. :try_end_44} :catchall_71

    #@44
    .line 121
    add-int/lit8 v3, v3, 0x1

    #@46
    goto :goto_3

    #@47
    .line 130
    :catch_47
    move-exception v2

    #@48
    .line 131
    .local v2, e1:Ljava/io/IOException;
    const-string v8, "BtOppRfcommListener"

    #@4a
    new-instance v9, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v10, "Error create RfcommServerSocket "

    #@51
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v9

    #@55
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v9

    #@59
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v9

    #@5d
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 132
    const/4 v6, 0x0

    #@61
    goto :goto_34

    #@62
    .line 142
    .end local v2           #e1:Ljava/io/IOException;
    :catch_62
    move-exception v1

    #@63
    .line 143
    .local v1, e:Ljava/lang/InterruptedException;
    :try_start_63
    const-string v8, "BtOppRfcommListener"

    #@65
    const-string v9, "socketAcceptThread thread was interrupted (3)"

    #@67
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 144
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@6c
    const/4 v9, 0x1

    #@6d
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mInterrupted:Z
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$002(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;Z)Z

    #@70
    goto :goto_43

    #@71
    .line 146
    .end local v1           #e:Ljava/lang/InterruptedException;
    :catchall_71
    move-exception v8

    #@72
    monitor-exit p0
    :try_end_73
    .catchall {:try_start_63 .. :try_end_73} :catchall_71

    #@73
    throw v8

    #@74
    .line 151
    :cond_74
    if-nez v6, :cond_82

    #@76
    .line 152
    const-string v8, "BtOppRfcommListener"

    #@78
    const-string v9, "Error start listening after 10 try"

    #@7a
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 153
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@7f
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mInterrupted:Z
    invoke-static {v8, v12}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$002(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;Z)Z

    #@82
    .line 155
    :cond_82
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@84
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mInterrupted:Z
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$000(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Z

    #@87
    move-result v8

    #@88
    if-nez v8, :cond_91

    #@8a
    .line 156
    const-string v8, "BtOppRfcommListener"

    #@8c
    const-string v9, "Accept thread started."

    #@8e
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 159
    :cond_91
    :goto_91
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@93
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mInterrupted:Z
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$000(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Z

    #@96
    move-result v8

    #@97
    if-nez v8, :cond_115

    #@99
    .line 162
    :try_start_99
    const-string v8, "BtOppRfcommListener"

    #@9b
    const-string v9, "Accepting connection..."

    #@9d
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a0
    .line 164
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@a2
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$100(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Landroid/bluetooth/BluetoothServerSocket;

    #@a5
    move-result-object v8

    #@a6
    if-nez v8, :cond_a8

    #@a8
    .line 167
    :cond_a8
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@aa
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$100(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Landroid/bluetooth/BluetoothServerSocket;

    #@ad
    move-result-object v5

    #@ae
    .line 168
    .local v5, sSocket:Landroid/bluetooth/BluetoothServerSocket;
    if-nez v5, :cond_d8

    #@b0
    .line 169
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@b2
    const/4 v9, 0x1

    #@b3
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mInterrupted:Z
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$002(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;Z)Z
    :try_end_b6
    .catch Ljava/io/IOException; {:try_start_99 .. :try_end_b6} :catch_b7

    #@b6
    goto :goto_91

    #@b7
    .line 185
    .end local v5           #sSocket:Landroid/bluetooth/BluetoothServerSocket;
    :catch_b7
    move-exception v1

    #@b8
    .line 186
    .local v1, e:Ljava/io/IOException;
    const-string v8, "BtOppRfcommListener"

    #@ba
    new-instance v9, Ljava/lang/StringBuilder;

    #@bc
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@bf
    const-string v10, "Error accept connection "

    #@c1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v9

    #@c5
    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c8
    move-result-object v9

    #@c9
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cc
    move-result-object v9

    #@cd
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d0
    .line 188
    const-wide/16 v8, 0x1f4

    #@d2
    :try_start_d2
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_d5
    .catch Ljava/lang/InterruptedException; {:try_start_d2 .. :try_end_d5} :catch_d6

    #@d5
    goto :goto_91

    #@d6
    .line 189
    :catch_d6
    move-exception v8

    #@d7
    goto :goto_91

    #@d8
    .line 172
    .end local v1           #e:Ljava/io/IOException;
    .restart local v5       #sSocket:Landroid/bluetooth/BluetoothServerSocket;
    :cond_d8
    :try_start_d8
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothServerSocket;->accept()Landroid/bluetooth/BluetoothSocket;

    #@db
    move-result-object v0

    #@dc
    .line 174
    .local v0, clientSocket:Landroid/bluetooth/BluetoothSocket;
    const-string v8, "BtOppRfcommListener"

    #@de
    new-instance v9, Ljava/lang/StringBuilder;

    #@e0
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@e3
    const-string v10, "Accepted connection from "

    #@e5
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v9

    #@e9
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    #@ec
    move-result-object v10

    #@ed
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v9

    #@f1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f4
    move-result-object v9

    #@f5
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f8
    .line 177
    new-instance v7, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;

    #@fa
    invoke-direct {v7, v0}, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;-><init>(Landroid/bluetooth/BluetoothSocket;)V

    #@fd
    .line 179
    .local v7, transport:Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@100
    move-result-object v4

    #@101
    .line 180
    .local v4, msg:Landroid/os/Message;
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@103
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->access$300(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Landroid/os/Handler;

    #@106
    move-result-object v8

    #@107
    invoke-virtual {v4, v8}, Landroid/os/Message;->setTarget(Landroid/os/Handler;)V

    #@10a
    .line 181
    const/16 v8, 0x64

    #@10c
    iput v8, v4, Landroid/os/Message;->what:I

    #@10e
    .line 182
    iput-object v7, v4, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@110
    .line 183
    invoke-virtual {v4}, Landroid/os/Message;->sendToTarget()V
    :try_end_113
    .catch Ljava/io/IOException; {:try_start_d8 .. :try_end_113} :catch_b7

    #@113
    goto/16 :goto_91

    #@115
    .line 193
    .end local v0           #clientSocket:Landroid/bluetooth/BluetoothSocket;
    .end local v4           #msg:Landroid/os/Message;
    .end local v5           #sSocket:Landroid/bluetooth/BluetoothServerSocket;
    .end local v7           #transport:Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;
    :cond_115
    const-string v8, "BtOppRfcommListener"

    #@117
    const-string v9, "BluetoothSocket listen thread finished"

    #@119
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@11c
    .line 195
    return-void
.end method
