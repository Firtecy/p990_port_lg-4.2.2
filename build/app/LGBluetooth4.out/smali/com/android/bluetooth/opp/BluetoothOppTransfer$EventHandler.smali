.class Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;
.super Landroid/os/Handler;
.source "BluetoothOppTransfer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/opp/BluetoothOppTransfer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Landroid/os/Looper;)V
    .registers 3
    .parameter
    .parameter "looper"

    #@0
    .prologue
    .line 131
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@2
    .line 132
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@5
    .line 133
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 16
    .parameter "msg"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    const/16 v13, 0x1f1

    #@3
    const/4 v12, 0x3

    #@4
    .line 137
    iget v9, p1, Landroid/os/Message;->what:I

    #@6
    packed-switch v9, :pswitch_data_2a0

    #@9
    .line 302
    :cond_9
    :goto_9
    :pswitch_9
    return-void

    #@a
    .line 139
    :pswitch_a
    iget-object v10, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@c
    new-instance v11, Lcom/android/bluetooth/opp/BluetoothOppTransfer$SocketConnectThread;

    #@e
    iget-object v12, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@10
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12
    check-cast v9, Landroid/bluetooth/BluetoothDevice;

    #@14
    const/4 v13, 0x1

    #@15
    invoke-direct {v11, v12, v9, v13}, Lcom/android/bluetooth/opp/BluetoothOppTransfer$SocketConnectThread;-><init>(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Landroid/bluetooth/BluetoothDevice;Z)V

    #@18
    invoke-static {v10, v11}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$002(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Lcom/android/bluetooth/opp/BluetoothOppTransfer$SocketConnectThread;)Lcom/android/bluetooth/opp/BluetoothOppTransfer$SocketConnectThread;

    #@1b
    .line 142
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1d
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$000(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppTransfer$SocketConnectThread;

    #@20
    move-result-object v9

    #@21
    invoke-virtual {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer$SocketConnectThread;->start()V

    #@24
    goto :goto_9

    #@25
    .line 150
    :pswitch_25
    const-string v9, "BtOppTransfer"

    #@27
    const-string v10, "receive RFCOMM_ERROR msg"

    #@29
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 152
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@2e
    invoke-static {v9, v11}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$002(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Lcom/android/bluetooth/opp/BluetoothOppTransfer$SocketConnectThread;)Lcom/android/bluetooth/opp/BluetoothOppTransfer$SocketConnectThread;

    #@31
    .line 153
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@33
    #calls: Lcom/android/bluetooth/opp/BluetoothOppTransfer;->markBatchFailed(I)V
    invoke-static {v9, v13}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$100(Lcom/android/bluetooth/opp/BluetoothOppTransfer;I)V

    #@36
    .line 154
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@38
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@3b
    move-result-object v9

    #@3c
    iput v12, v9, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mStatus:I

    #@3e
    .line 156
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@40
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$300(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Landroid/content/Context;

    #@43
    move-result-object v9

    #@44
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@47
    move-result-object v6

    #@48
    .line 157
    .local v6, mOppManager:Lcom/android/bluetooth/opp/BluetoothOppManager;
    invoke-virtual {v6}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getBatchSize()I

    #@4b
    move-result v8

    #@4c
    .line 158
    .local v8, send_file_cnt:I
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@4e
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$300(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Landroid/content/Context;

    #@51
    move-result-object v9

    #@52
    const-string v10, "OUTBOUND_TRANSFER"

    #@54
    const/4 v11, 0x0

    #@55
    invoke-static {v9, v10, v8, v11, v13}, Lcom/android/bluetooth/opp/Constants;->updateTransferStatusForMessage(Landroid/content/Context;Ljava/lang/String;III)V

    #@58
    goto :goto_9

    #@59
    .line 167
    .end local v6           #mOppManager:Lcom/android/bluetooth/opp/BluetoothOppManager;
    .end local v8           #send_file_cnt:I
    :pswitch_59
    const-string v9, "BtOppTransfer"

    #@5b
    const-string v10, "Transfer receive RFCOMM_CONNECTED msg"

    #@5d
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 169
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@62
    invoke-static {v9, v11}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$002(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Lcom/android/bluetooth/opp/BluetoothOppTransfer$SocketConnectThread;)Lcom/android/bluetooth/opp/BluetoothOppTransfer$SocketConnectThread;

    #@65
    .line 170
    iget-object v10, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@67
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@69
    check-cast v9, Ljavax/obex/ObexTransport;

    #@6b
    invoke-static {v10, v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$402(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Ljavax/obex/ObexTransport;)Ljavax/obex/ObexTransport;

    #@6e
    .line 171
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@70
    #calls: Lcom/android/bluetooth/opp/BluetoothOppTransfer;->startObexSession()V
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$500(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)V

    #@73
    goto :goto_9

    #@74
    .line 182
    :pswitch_74
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@76
    check-cast v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@78
    .line 184
    .local v2, info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    const-string v9, "BtOppTransfer"

    #@7a
    new-instance v10, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    const-string v11, "receive MSG_SHARE_COMPLETE for info "

    #@81
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v10

    #@85
    iget v11, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@87
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v10

    #@8b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8e
    move-result-object v10

    #@8f
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@92
    .line 186
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@94
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@97
    move-result-object v9

    #@98
    iget v9, v9, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mDirection:I

    #@9a
    if-nez v9, :cond_9

    #@9c
    .line 187
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@9e
    iget-object v10, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@a0
    invoke-static {v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@a3
    move-result-object v10

    #@a4
    invoke-virtual {v10}, Lcom/android/bluetooth/opp/BluetoothOppBatch;->getPendingShare()Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@a7
    move-result-object v10

    #@a8
    invoke-static {v9, v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$602(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@ab
    .line 189
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@ad
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$600(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@b0
    move-result-object v9

    #@b1
    if-eqz v9, :cond_ec

    #@b3
    .line 192
    const-string v9, "BtOppTransfer"

    #@b5
    new-instance v10, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v11, "continue session for info "

    #@bc
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v10

    #@c0
    iget-object v11, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@c2
    invoke-static {v11}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$600(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@c5
    move-result-object v11

    #@c6
    iget v11, v11, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@c8
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v10

    #@cc
    const-string v11, " from batch "

    #@ce
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v10

    #@d2
    iget-object v11, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@d4
    invoke-static {v11}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@d7
    move-result-object v11

    #@d8
    iget v11, v11, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@da
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v10

    #@de
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v10

    #@e2
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e5
    .line 195
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@e7
    #calls: Lcom/android/bluetooth/opp/BluetoothOppTransfer;->processCurrentShare()V
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$700(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)V

    #@ea
    goto/16 :goto_9

    #@ec
    .line 199
    :cond_ec
    const-string v9, "BtOppTransfer"

    #@ee
    new-instance v10, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string v11, "Batch "

    #@f5
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v10

    #@f9
    iget-object v11, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@fb
    invoke-static {v11}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@fe
    move-result-object v11

    #@ff
    iget v11, v11, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@101
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@104
    move-result-object v10

    #@105
    const-string v11, " is done"

    #@107
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10a
    move-result-object v10

    #@10b
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10e
    move-result-object v10

    #@10f
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@112
    .line 201
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@114
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$800(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppObexSession;

    #@117
    move-result-object v9

    #@118
    invoke-interface {v9}, Lcom/android/bluetooth/opp/BluetoothOppObexSession;->stop()V

    #@11b
    goto/16 :goto_9

    #@11d
    .line 210
    .end local v2           #info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    :pswitch_11d
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@11f
    check-cast v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@121
    .line 212
    .local v3, info1:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    const-string v9, "BtOppTransfer"

    #@123
    new-instance v10, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v11, "receive MSG_SESSION_COMPLETE for batch "

    #@12a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v10

    #@12e
    iget-object v11, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@130
    invoke-static {v11}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@133
    move-result-object v11

    #@134
    iget v11, v11, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@136
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@139
    move-result-object v10

    #@13a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13d
    move-result-object v10

    #@13e
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@141
    .line 214
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@143
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@146
    move-result-object v9

    #@147
    const/4 v10, 0x2

    #@148
    iput v10, v9, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mStatus:I

    #@14a
    .line 218
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@14c
    #calls: Lcom/android/bluetooth/opp/BluetoothOppTransfer;->tickShareStatus(Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V
    invoke-static {v9, v3}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$900(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V

    #@14f
    goto/16 :goto_9

    #@151
    .line 224
    .end local v3           #info1:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    :pswitch_151
    const-string v9, "BtOppTransfer"

    #@153
    new-instance v10, Ljava/lang/StringBuilder;

    #@155
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@158
    const-string v11, "receive MSG_SESSION_ERROR for batch "

    #@15a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    move-result-object v10

    #@15e
    iget-object v11, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@160
    invoke-static {v11}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@163
    move-result-object v11

    #@164
    iget v11, v11, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@166
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@169
    move-result-object v10

    #@16a
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16d
    move-result-object v10

    #@16e
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@171
    .line 226
    iget-object v4, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@173
    check-cast v4, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@175
    .line 227
    .local v4, info2:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@177
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$800(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppObexSession;

    #@17a
    move-result-object v9

    #@17b
    invoke-interface {v9}, Lcom/android/bluetooth/opp/BluetoothOppObexSession;->stop()V

    #@17e
    .line 228
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@180
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@183
    move-result-object v9

    #@184
    iput v12, v9, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mStatus:I

    #@186
    .line 229
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@188
    iget v10, v4, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@18a
    #calls: Lcom/android/bluetooth/opp/BluetoothOppTransfer;->markBatchFailed(I)V
    invoke-static {v9, v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$100(Lcom/android/bluetooth/opp/BluetoothOppTransfer;I)V

    #@18d
    .line 230
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@18f
    iget-object v10, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@191
    invoke-static {v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$600(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@194
    move-result-object v10

    #@195
    #calls: Lcom/android/bluetooth/opp/BluetoothOppTransfer;->tickShareStatus(Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V
    invoke-static {v9, v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$900(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V

    #@198
    goto/16 :goto_9

    #@19a
    .line 235
    .end local v4           #info2:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    :pswitch_19a
    const-string v9, "BtOppTransfer"

    #@19c
    new-instance v10, Ljava/lang/StringBuilder;

    #@19e
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@1a1
    const-string v11, "receive MSG_SHARE_INTERRUPTED for batch "

    #@1a3
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a6
    move-result-object v10

    #@1a7
    iget-object v11, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1a9
    invoke-static {v11}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@1ac
    move-result-object v11

    #@1ad
    iget v11, v11, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@1af
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b2
    move-result-object v10

    #@1b3
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b6
    move-result-object v10

    #@1b7
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1ba
    .line 237
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1bc
    check-cast v5, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@1be
    .line 238
    .local v5, info3:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1c0
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@1c3
    move-result-object v9

    #@1c4
    iget v9, v9, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mDirection:I

    #@1c6
    if-nez v9, :cond_9

    #@1c8
    .line 240
    :try_start_1c8
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1ca
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$400(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Ljavax/obex/ObexTransport;

    #@1cd
    move-result-object v9

    #@1ce
    if-nez v9, :cond_1fc

    #@1d0
    .line 241
    const-string v9, "BtOppTransfer"

    #@1d2
    const-string v10, "receive MSG_SHARE_INTERRUPTED but mTransport = null"

    #@1d4
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1d7
    .catch Ljava/io/IOException; {:try_start_1c8 .. :try_end_1d7} :catch_206

    #@1d7
    .line 249
    :goto_1d7
    const-string v9, "BtOppTransfer"

    #@1d9
    const-string v10, "mTransport closed "

    #@1db
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1de
    .line 251
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1e0
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@1e3
    move-result-object v9

    #@1e4
    iput v12, v9, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mStatus:I

    #@1e6
    .line 252
    if-eqz v5, :cond_20f

    #@1e8
    .line 253
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1ea
    iget v10, v5, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@1ec
    #calls: Lcom/android/bluetooth/opp/BluetoothOppTransfer;->markBatchFailed(I)V
    invoke-static {v9, v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$100(Lcom/android/bluetooth/opp/BluetoothOppTransfer;I)V

    #@1ef
    .line 257
    :goto_1ef
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1f1
    iget-object v10, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1f3
    invoke-static {v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$600(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@1f6
    move-result-object v10

    #@1f7
    #calls: Lcom/android/bluetooth/opp/BluetoothOppTransfer;->tickShareStatus(Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V
    invoke-static {v9, v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$900(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V

    #@1fa
    goto/16 :goto_9

    #@1fc
    .line 243
    :cond_1fc
    :try_start_1fc
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1fe
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$400(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Ljavax/obex/ObexTransport;

    #@201
    move-result-object v9

    #@202
    invoke-interface {v9}, Ljavax/obex/ObexTransport;->close()V
    :try_end_205
    .catch Ljava/io/IOException; {:try_start_1fc .. :try_end_205} :catch_206

    #@205
    goto :goto_1d7

    #@206
    .line 245
    :catch_206
    move-exception v0

    #@207
    .line 246
    .local v0, e:Ljava/io/IOException;
    const-string v9, "BtOppTransfer"

    #@209
    const-string v10, "failed to close mTransport"

    #@20b
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20e
    goto :goto_1d7

    #@20f
    .line 255
    .end local v0           #e:Ljava/io/IOException;
    :cond_20f
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@211
    #calls: Lcom/android/bluetooth/opp/BluetoothOppTransfer;->markBatchFailed()V
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$1000(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)V

    #@214
    goto :goto_1ef

    #@215
    .line 263
    .end local v5           #info3:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    :pswitch_215
    const-string v9, "BtOppTransfer"

    #@217
    new-instance v10, Ljava/lang/StringBuilder;

    #@219
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@21c
    const-string v11, "receive MSG_CONNECT_TIMEOUT for batch "

    #@21e
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@221
    move-result-object v10

    #@222
    iget-object v11, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@224
    invoke-static {v11}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@227
    move-result-object v11

    #@228
    iget v11, v11, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@22a
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22d
    move-result-object v10

    #@22e
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@231
    move-result-object v10

    #@232
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@235
    .line 268
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@237
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$200(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@23a
    move-result-object v9

    #@23b
    iget v9, v9, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mDirection:I

    #@23d
    if-nez v9, :cond_26a

    #@23f
    .line 270
    :try_start_23f
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@241
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$400(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Ljavax/obex/ObexTransport;

    #@244
    move-result-object v9

    #@245
    if-nez v9, :cond_257

    #@247
    .line 271
    const-string v9, "BtOppTransfer"

    #@249
    const-string v10, "receive MSG_SHARE_INTERRUPTED but mTransport = null"

    #@24b
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_24e
    .catch Ljava/io/IOException; {:try_start_23f .. :try_end_24e} :catch_261

    #@24e
    .line 279
    :goto_24e
    const-string v9, "BtOppTransfer"

    #@250
    const-string v10, "mTransport closed "

    #@252
    invoke-static {v9, v10}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@255
    goto/16 :goto_9

    #@257
    .line 273
    :cond_257
    :try_start_257
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@259
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$400(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Ljavax/obex/ObexTransport;

    #@25c
    move-result-object v9

    #@25d
    invoke-interface {v9}, Ljavax/obex/ObexTransport;->close()V
    :try_end_260
    .catch Ljava/io/IOException; {:try_start_257 .. :try_end_260} :catch_261

    #@260
    goto :goto_24e

    #@261
    .line 275
    :catch_261
    move-exception v0

    #@262
    .line 276
    .restart local v0       #e:Ljava/io/IOException;
    const-string v9, "BtOppTransfer"

    #@264
    const-string v10, "failed to close mTransport"

    #@266
    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@269
    goto :goto_24e

    #@26a
    .line 288
    .end local v0           #e:Ljava/io/IOException;
    :cond_26a
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@26c
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$300(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Landroid/content/Context;

    #@26f
    move-result-object v9

    #@270
    const-string v10, "notification"

    #@272
    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@275
    move-result-object v7

    #@276
    check-cast v7, Landroid/app/NotificationManager;

    #@278
    .line 290
    .local v7, nm:Landroid/app/NotificationManager;
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@27a
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$600(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@27d
    move-result-object v9

    #@27e
    iget v9, v9, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@280
    invoke-virtual {v7, v9}, Landroid/app/NotificationManager;->cancel(I)V

    #@283
    .line 292
    new-instance v1, Landroid/content/Intent;

    #@285
    const-string v9, "android.btopp.intent.action.USER_CONFIRMATION_TIMEOUT"

    #@287
    invoke-direct {v1, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@28a
    .line 293
    .local v1, in:Landroid/content/Intent;
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@28c
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$300(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Landroid/content/Context;

    #@28f
    move-result-object v9

    #@290
    invoke-virtual {v9, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@293
    .line 295
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@295
    iget-object v10, p0, Lcom/android/bluetooth/opp/BluetoothOppTransfer$EventHandler;->this$0:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@297
    invoke-static {v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$600(Lcom/android/bluetooth/opp/BluetoothOppTransfer;)Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@29a
    move-result-object v10

    #@29b
    #calls: Lcom/android/bluetooth/opp/BluetoothOppTransfer;->markShareTimeout(Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V
    invoke-static {v9, v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->access$1100(Lcom/android/bluetooth/opp/BluetoothOppTransfer;Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V

    #@29e
    goto/16 :goto_9

    #@2a0
    .line 137
    :pswitch_data_2a0
    .packed-switch 0x0
        :pswitch_74
        :pswitch_11d
        :pswitch_151
        :pswitch_19a
        :pswitch_215
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_9
        :pswitch_25
        :pswitch_59
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
