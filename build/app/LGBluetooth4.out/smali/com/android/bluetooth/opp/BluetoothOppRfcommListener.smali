.class public Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;
.super Ljava/lang/Object;
.source "BluetoothOppRfcommListener.java"


# static fields
.field private static final CREATE_RETRY_TIME:I = 0xa

.field public static final MSG_INCOMING_BTOPP_CONNECTION:I = 0x64

.field private static final TAG:Ljava/lang/String; = "BtOppRfcommListener"

.field private static final V:Z = true


# instance fields
.field private final mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBtServerSocket:Landroid/bluetooth/BluetoothServerSocket;

.field private mCallback:Landroid/os/Handler;

.field private volatile mInterrupted:Z

.field private mSocketAcceptThread:Ljava/lang/Thread;

.field private mTcpServerSocket:Ljava/net/ServerSocket;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothAdapter;)V
    .registers 3
    .parameter "adapter"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 71
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 67
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mBtServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    #@6
    .line 69
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mTcpServerSocket:Ljava/net/ServerSocket;

    #@8
    .line 72
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@a
    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mInterrupted:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-boolean p1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mInterrupted:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Landroid/bluetooth/BluetoothServerSocket;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mBtServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;Landroid/bluetooth/BluetoothServerSocket;)Landroid/bluetooth/BluetoothServerSocket;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 50
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mBtServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Landroid/bluetooth/BluetoothAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mCallback:Landroid/os/Handler;

    #@2
    return-object v0
.end method


# virtual methods
.method public declared-synchronized start(Landroid/os/Handler;)Z
    .registers 4
    .parameter "callback"

    #@0
    .prologue
    .line 77
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@3
    if-nez v0, :cond_18

    #@5
    .line 78
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mCallback:Landroid/os/Handler;

    #@7
    .line 80
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;

    #@9
    const-string v1, "BtOppRfcommListener"

    #@b
    invoke-direct {v0, p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener$1;-><init>(Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;Ljava/lang/String;)V

    #@e
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@10
    .line 197
    const/4 v0, 0x0

    #@11
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mInterrupted:Z

    #@13
    .line 199
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@15
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_1b

    #@18
    .line 202
    :cond_18
    const/4 v0, 0x1

    #@19
    monitor-exit p0

    #@1a
    return v0

    #@1b
    .line 77
    :catchall_1b
    move-exception v0

    #@1c
    monitor-exit p0

    #@1d
    throw v0
.end method

.method public declared-synchronized stop()V
    .registers 4

    #@0
    .prologue
    .line 206
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@3
    if-eqz v1, :cond_40

    #@5
    .line 207
    const-string v1, "BtOppRfcommListener"

    #@7
    const-string v2, "stopping Accept Thread"

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 209
    const/4 v1, 0x1

    #@d
    iput-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mInterrupted:Z

    #@f
    .line 224
    const-string v1, "BtOppRfcommListener"

    #@11
    const-string v2, "close mBtServerSocket"

    #@13
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 227
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mBtServerSocket:Landroid/bluetooth/BluetoothServerSocket;
    :try_end_18
    .catchall {:try_start_1 .. :try_end_18} :catchall_4b

    #@18
    if-eqz v1, :cond_22

    #@1a
    .line 229
    :try_start_1a
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mBtServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    #@1c
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothServerSocket;->close()V

    #@1f
    .line 230
    const/4 v1, 0x0

    #@20
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mBtServerSocket:Landroid/bluetooth/BluetoothServerSocket;
    :try_end_22
    .catchall {:try_start_1a .. :try_end_22} :catchall_4b
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_22} :catch_42

    #@22
    .line 237
    :cond_22
    :goto_22
    :try_start_22
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@24
    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    #@27
    .line 239
    const-string v1, "BtOppRfcommListener"

    #@29
    const-string v2, "waiting for thread to terminate"

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 242
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@30
    invoke-virtual {v1}, Ljava/lang/Thread;->join()V

    #@33
    .line 244
    const-string v1, "BtOppRfcommListener"

    #@35
    const-string v2, "done waiting for thread to terminate"

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 246
    const/4 v1, 0x0

    #@3b
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mSocketAcceptThread:Ljava/lang/Thread;

    #@3d
    .line 247
    const/4 v1, 0x0

    #@3e
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->mCallback:Landroid/os/Handler;
    :try_end_40
    .catchall {:try_start_22 .. :try_end_40} :catchall_4b
    .catch Ljava/lang/InterruptedException; {:try_start_22 .. :try_end_40} :catch_4e

    #@40
    .line 254
    :cond_40
    :goto_40
    monitor-exit p0

    #@41
    return-void

    #@42
    .line 231
    :catch_42
    move-exception v0

    #@43
    .line 232
    .local v0, e:Ljava/io/IOException;
    :try_start_43
    const-string v1, "BtOppRfcommListener"

    #@45
    const-string v2, "Error close mBtServerSocket"

    #@47
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4a
    .catchall {:try_start_43 .. :try_end_4a} :catchall_4b

    #@4a
    goto :goto_22

    #@4b
    .line 206
    .end local v0           #e:Ljava/io/IOException;
    :catchall_4b
    move-exception v1

    #@4c
    monitor-exit p0

    #@4d
    throw v1

    #@4e
    .line 248
    :catch_4e
    move-exception v0

    #@4f
    .line 250
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_4f
    const-string v1, "BtOppRfcommListener"

    #@51
    const-string v2, "Interrupted waiting for Accept Thread to join"

    #@53
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_56
    .catchall {:try_start_4f .. :try_end_56} :catchall_4b

    #@56
    goto :goto_40
.end method
