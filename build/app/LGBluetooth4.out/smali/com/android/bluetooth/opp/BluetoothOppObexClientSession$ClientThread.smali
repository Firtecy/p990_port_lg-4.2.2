.class Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;
.super Ljava/lang/Thread;
.source "BluetoothOppObexClientSession.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ClientThread"
.end annotation


# static fields
.field private static final sSleepTime:I = 0x1f4


# instance fields
.field private mConnected:Z

.field private mContext1:Landroid/content/Context;

.field private mCs:Ljavax/obex/ClientSession;

.field private mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

.field private mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

.field private mTransport1:Ljavax/obex/ObexTransport;

.field final synthetic this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

.field private volatile waitingForShare:Z

.field private wakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Landroid/content/Context;Ljavax/obex/ObexTransport;)V
    .registers 8
    .parameter
    .parameter "context"
    .parameter "transport"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 168
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@4
    .line 169
    const-string v1, "BtOpp ClientThread"

    #@6
    invoke-direct {p0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@9
    .line 164
    const/4 v1, 0x0

    #@a
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@c
    .line 166
    iput-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mConnected:Z

    #@e
    .line 170
    iput-object p2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@10
    .line 171
    iput-object p3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mTransport1:Ljavax/obex/ObexTransport;

    #@12
    .line 172
    iput-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->waitingForShare:Z

    #@14
    .line 173
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z
    invoke-static {p1, v2}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$002(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z

    #@17
    .line 175
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@19
    const-string v2, "power"

    #@1b
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1e
    move-result-object v0

    #@1f
    check-cast v0, Landroid/os/PowerManager;

    #@21
    .line 176
    .local v0, pm:Landroid/os/PowerManager;
    const-string v1, "BtOppObexClient"

    #@23
    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@26
    move-result-object v1

    #@27
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->wakeLock:Landroid/os/PowerManager$WakeLock;

    #@29
    .line 177
    return-void
.end method

.method private connect()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 275
    const-string v3, "BtOppObexClient"

    #@3
    new-instance v4, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v5, "Create ClientSession with transport "

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mTransport1:Ljavax/obex/ObexTransport;

    #@10
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@13
    move-result-object v5

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v4

    #@1c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 278
    :try_start_1f
    new-instance v3, Ljavax/obex/ClientSession;

    #@21
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mTransport1:Ljavax/obex/ObexTransport;

    #@23
    invoke-direct {v3, v4}, Ljavax/obex/ClientSession;-><init>(Ljavax/obex/ObexTransport;)V

    #@26
    iput-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mCs:Ljavax/obex/ClientSession;

    #@28
    .line 279
    const/4 v3, 0x1

    #@29
    iput-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mConnected:Z
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_2b} :catch_56

    #@2b
    .line 283
    :goto_2b
    iget-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mConnected:Z

    #@2d
    if-eqz v3, :cond_4d

    #@2f
    .line 284
    iput-boolean v6, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mConnected:Z

    #@31
    .line 285
    new-instance v2, Ljavax/obex/HeaderSet;

    #@33
    invoke-direct {v2}, Ljavax/obex/HeaderSet;-><init>()V

    #@36
    .line 286
    .local v2, hs:Ljavax/obex/HeaderSet;
    monitor-enter p0

    #@37
    .line 287
    :try_start_37
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@39
    const/4 v4, 0x1

    #@3a
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z
    invoke-static {v3, v4}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$002(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z

    #@3d
    .line 288
    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_37 .. :try_end_3e} :catchall_5f

    #@3e
    .line 290
    :try_start_3e
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mCs:Ljavax/obex/ClientSession;

    #@40
    invoke-virtual {v3, v2}, Ljavax/obex/ClientSession;->connect(Ljavax/obex/HeaderSet;)Ljavax/obex/HeaderSet;

    #@43
    .line 292
    const-string v3, "BtOppObexClient"

    #@45
    const-string v4, "OBEX session created"

    #@47
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    .line 294
    const/4 v3, 0x1

    #@4b
    iput-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mConnected:Z
    :try_end_4d
    .catch Ljava/io/IOException; {:try_start_3e .. :try_end_4d} :catch_62

    #@4d
    .line 299
    .end local v2           #hs:Ljavax/obex/HeaderSet;
    :cond_4d
    :goto_4d
    monitor-enter p0

    #@4e
    .line 300
    :try_start_4e
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@50
    const/4 v4, 0x0

    #@51
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z
    invoke-static {v3, v4}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$002(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z

    #@54
    .line 301
    monitor-exit p0
    :try_end_55
    .catchall {:try_start_4e .. :try_end_55} :catchall_6b

    #@55
    .line 302
    return-void

    #@56
    .line 280
    :catch_56
    move-exception v1

    #@57
    .line 281
    .local v1, e1:Ljava/io/IOException;
    const-string v3, "BtOppObexClient"

    #@59
    const-string v4, "OBEX session create error"

    #@5b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    goto :goto_2b

    #@5f
    .line 288
    .end local v1           #e1:Ljava/io/IOException;
    .restart local v2       #hs:Ljavax/obex/HeaderSet;
    :catchall_5f
    move-exception v3

    #@60
    :try_start_60
    monitor-exit p0
    :try_end_61
    .catchall {:try_start_60 .. :try_end_61} :catchall_5f

    #@61
    throw v3

    #@62
    .line 295
    :catch_62
    move-exception v0

    #@63
    .line 296
    .local v0, e:Ljava/io/IOException;
    const-string v3, "BtOppObexClient"

    #@65
    const-string v4, "OBEX session connect error"

    #@67
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    goto :goto_4d

    #@6b
    .line 301
    .end local v0           #e:Ljava/io/IOException;
    .end local v2           #hs:Ljavax/obex/HeaderSet;
    :catchall_6b
    move-exception v3

    #@6c
    :try_start_6c
    monitor-exit p0
    :try_end_6d
    .catchall {:try_start_6c .. :try_end_6d} :catchall_6b

    #@6d
    throw v3
.end method

.method private disconnect()V
    .registers 5

    #@0
    .prologue
    .line 240
    :try_start_0
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mCs:Ljavax/obex/ClientSession;

    #@2
    if-eqz v1, :cond_a

    #@4
    .line 241
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mCs:Ljavax/obex/ClientSession;

    #@6
    const/4 v2, 0x0

    #@7
    invoke-virtual {v1, v2}, Ljavax/obex/ClientSession;->disconnect(Ljavax/obex/HeaderSet;)Ljavax/obex/HeaderSet;

    #@a
    .line 243
    :cond_a
    const/4 v1, 0x0

    #@b
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mCs:Ljavax/obex/ClientSession;

    #@d
    .line 245
    const-string v1, "BtOppObexClient"

    #@f
    const-string v2, "OBEX session disconnected"

    #@11
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_14
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_14} :catch_35

    #@14
    .line 251
    :goto_14
    :try_start_14
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mCs:Ljavax/obex/ClientSession;

    #@16
    if-eqz v1, :cond_2b

    #@18
    .line 253
    const-string v1, "BtOppObexClient"

    #@1a
    const-string v2, "OBEX session close mCs"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 255
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mCs:Ljavax/obex/ClientSession;

    #@21
    invoke-virtual {v1}, Ljavax/obex/ClientSession;->close()V

    #@24
    .line 257
    const-string v1, "BtOppObexClient"

    #@26
    const-string v2, "OBEX session closed"

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2b
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_2b} :catch_4f

    #@2b
    .line 263
    :cond_2b
    :goto_2b
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mTransport1:Ljavax/obex/ObexTransport;

    #@2d
    if-eqz v1, :cond_34

    #@2f
    .line 265
    :try_start_2f
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mTransport1:Ljavax/obex/ObexTransport;

    #@31
    invoke-interface {v1}, Ljavax/obex/ObexTransport;->close()V
    :try_end_34
    .catch Ljava/io/IOException; {:try_start_2f .. :try_end_34} :catch_69

    #@34
    .line 271
    :cond_34
    :goto_34
    return-void

    #@35
    .line 247
    :catch_35
    move-exception v0

    #@36
    .line 248
    .local v0, e:Ljava/io/IOException;
    const-string v1, "BtOppObexClient"

    #@38
    new-instance v2, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v3, "OBEX session disconnect error"

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    goto :goto_14

    #@4f
    .line 260
    .end local v0           #e:Ljava/io/IOException;
    :catch_4f
    move-exception v0

    #@50
    .line 261
    .restart local v0       #e:Ljava/io/IOException;
    const-string v1, "BtOppObexClient"

    #@52
    new-instance v2, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v3, "OBEX session close error"

    #@59
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v2

    #@61
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@64
    move-result-object v2

    #@65
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@68
    goto :goto_2b

    #@69
    .line 266
    .end local v0           #e:Ljava/io/IOException;
    :catch_69
    move-exception v0

    #@6a
    .line 267
    .restart local v0       #e:Ljava/io/IOException;
    const-string v1, "BtOppObexClient"

    #@6c
    const-string v2, "mTransport.close error"

    #@6e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    goto :goto_34
.end method

.method private doSend()V
    .registers 12

    #@0
    .prologue
    const/16 v10, 0xc8

    #@2
    const/4 v9, 0x1

    #@3
    .line 306
    const/16 v4, 0xc8

    #@5
    .line 309
    .local v4, status:I
    :goto_5
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@7
    if-nez v5, :cond_13

    #@9
    .line 311
    const-wide/16 v5, 0x32

    #@b
    :try_start_b
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_b .. :try_end_e} :catch_f

    #@e
    goto :goto_5

    #@f
    .line 312
    :catch_f
    move-exception v0

    #@10
    .line 313
    .local v0, e:Ljava/lang/InterruptedException;
    const/16 v4, 0x1ea

    #@12
    .line 314
    goto :goto_5

    #@13
    .line 316
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_13
    iget-boolean v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mConnected:Z

    #@15
    if-nez v5, :cond_52

    #@17
    .line 318
    const/16 v4, 0x1f1

    #@19
    .line 332
    :cond_19
    :goto_19
    if-ne v4, v10, :cond_9f

    #@1b
    .line 334
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@1d
    iget-object v5, v5, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@1f
    if-eqz v5, :cond_9a

    #@21
    .line 335
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@23
    invoke-direct {p0, v5}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->sendFile(Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;)I

    #@26
    move-result v4

    #@27
    .line 341
    :goto_27
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@29
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->result_status:I
    invoke-static {v5, v4}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$602(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;I)I

    #@2c
    .line 343
    iput-boolean v9, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->waitingForShare:Z

    #@2e
    .line 348
    :goto_2e
    if-ne v4, v10, :cond_be

    #@30
    .line 350
    const/4 v1, 0x0

    #@31
    .local v1, i:I
    :goto_31
    const/16 v5, 0x28

    #@33
    if-ge v1, v5, :cond_3d

    #@35
    .line 351
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@37
    iget v5, v5, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@39
    const/16 v6, 0xbe

    #@3b
    if-eq v5, v6, :cond_a9

    #@3d
    .line 362
    :cond_3d
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@3f
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;
    invoke-static {v5}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$200(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/os/Handler;

    #@42
    move-result-object v5

    #@43
    invoke-static {v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@46
    move-result-object v3

    #@47
    .line 363
    .local v3, msg:Landroid/os/Message;
    const/4 v5, 0x0

    #@48
    iput v5, v3, Landroid/os/Message;->what:I

    #@4a
    .line 364
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@4c
    iput-object v5, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4e
    .line 365
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@51
    .line 373
    .end local v1           #i:I
    :goto_51
    return-void

    #@52
    .line 320
    .end local v3           #msg:Landroid/os/Message;
    :cond_52
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@54
    iget-wide v5, v5, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@56
    const-wide v7, 0xffffffffL

    #@5b
    cmp-long v5, v5, v7

    #@5d
    if-lez v5, :cond_19

    #@5f
    .line 321
    new-instance v2, Landroid/content/Intent;

    #@61
    const-string v5, "com.lge.btopp.intent.action.4GB_INCOMING_FILE_NOTIFICATION"

    #@63
    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@66
    .line 322
    .local v2, in:Landroid/content/Intent;
    const-string v5, "com.android.bluetooth"

    #@68
    const-class v6, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@6a
    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@6d
    move-result-object v6

    #@6e
    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@71
    .line 323
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@73
    invoke-virtual {v5, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@76
    .line 325
    const/16 v4, 0x1f0

    #@78
    .line 326
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@7a
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mInterrupted:Z
    invoke-static {v5, v9}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$102(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z

    #@7d
    .line 328
    const-string v5, "BtOppObexClient"

    #@7f
    new-instance v6, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v7, "doSend file size is larger than 4GB. filesize : "

    #@86
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v6

    #@8a
    iget-object v7, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@8c
    iget-wide v7, v7, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@8e
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@91
    move-result-object v6

    #@92
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v6

    #@96
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    goto :goto_19

    #@9a
    .line 338
    .end local v2           #in:Landroid/content/Intent;
    :cond_9a
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@9c
    iget v4, v5, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mStatus:I

    #@9e
    goto :goto_27

    #@9f
    .line 345
    :cond_9f
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@a1
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@a3
    iget v6, v6, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@a5
    invoke-static {v5, v6, v4}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@a8
    goto :goto_2e

    #@a9
    .line 355
    .restart local v1       #i:I
    :cond_a9
    const-wide/16 v5, 0x32

    #@ab
    :try_start_ab
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V

    #@ae
    .line 356
    const-string v5, "BtOppObexClient"

    #@b0
    const-string v6, "Thread.sleep(50) is called due to BluetoothShare.STATUS_PENDING"

    #@b2
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b5
    .catch Ljava/lang/InterruptedException; {:try_start_ab .. :try_end_b5} :catch_b9

    #@b5
    .line 350
    :goto_b5
    add-int/lit8 v1, v1, 0x1

    #@b7
    goto/16 :goto_31

    #@b9
    .line 357
    :catch_b9
    move-exception v0

    #@ba
    .line 358
    .restart local v0       #e:Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    #@bd
    goto :goto_b5

    #@be
    .line 367
    .end local v0           #e:Ljava/lang/InterruptedException;
    .end local v1           #i:I
    :cond_be
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@c0
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;
    invoke-static {v5}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$200(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/os/Handler;

    #@c3
    move-result-object v5

    #@c4
    invoke-static {v5}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@c7
    move-result-object v3

    #@c8
    .line 368
    .restart local v3       #msg:Landroid/os/Message;
    const/4 v5, 0x2

    #@c9
    iput v5, v3, Landroid/os/Message;->what:I

    #@cb
    .line 369
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@cd
    iput v4, v5, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@cf
    .line 370
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@d1
    iput-object v5, v3, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d3
    .line 371
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@d6
    goto/16 :goto_51
.end method

.method private handleSendException(Ljava/lang/String;)V
    .registers 6
    .parameter "exception"

    #@0
    .prologue
    .line 633
    const-string v1, "BtOppObexClient"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Error when sending file: "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 634
    const/16 v0, 0x1f0

    #@1a
    .line 635
    .local v0, status:I
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@1c
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@1e
    iget v2, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@20
    invoke-static {v1, v2, v0}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@23
    .line 636
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@25
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;
    invoke-static {v1}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$200(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/os/Handler;

    #@28
    move-result-object v1

    #@29
    const/4 v2, 0x4

    #@2a
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@2d
    .line 637
    return-void
.end method

.method private processShareInfo()Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 380
    const-string v3, "BtOppObexClient"

    #@3
    new-instance v4, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v5, "Client thread processShareInfo() "

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@10
    iget v5, v5, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@12
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v4

    #@16
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v4

    #@1a
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 383
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@1f
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@21
    invoke-static {v3}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->getSendFileInfo(Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@24
    move-result-object v1

    #@25
    .line 384
    .local v1, fileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    iget-object v3, v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@27
    if-eqz v3, :cond_31

    #@29
    iget-wide v3, v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@2b
    const-wide/16 v5, 0x0

    #@2d
    cmp-long v3, v3, v5

    #@2f
    if-nez v3, :cond_44

    #@31
    .line 386
    :cond_31
    const-string v3, "BtOppObexClient"

    #@33
    const-string v4, "BluetoothOppSendFileInfo get invalid file"

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 388
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@3a
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@3c
    iget v4, v4, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@3e
    iget v5, v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mStatus:I

    #@40
    invoke-static {v3, v4, v5}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@43
    .line 408
    :goto_43
    return-object v1

    #@44
    .line 392
    :cond_44
    const-string v3, "BtOppObexClient"

    #@46
    const-string v4, "Generate BluetoothOppSendFileInfo:"

    #@48
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 393
    const-string v3, "BtOppObexClient"

    #@4d
    new-instance v4, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v5, "filename  :"

    #@54
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v4

    #@58
    iget-object v5, v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v4

    #@62
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 394
    const-string v3, "BtOppObexClient"

    #@67
    new-instance v4, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v5, "length    :"

    #@6e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v4

    #@72
    iget-wide v5, v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@74
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7b
    move-result-object v4

    #@7c
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 395
    const-string v3, "BtOppObexClient"

    #@81
    new-instance v4, Ljava/lang/StringBuilder;

    #@83
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@86
    const-string v5, "mimetype  :"

    #@88
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v4

    #@8c
    iget-object v5, v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mMimetype:Ljava/lang/String;

    #@8e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v4

    #@92
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@95
    move-result-object v4

    #@96
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@99
    .line 398
    new-instance v2, Landroid/content/ContentValues;

    #@9b
    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    #@9e
    .line 399
    .local v2, updateValues:Landroid/content/ContentValues;
    new-instance v3, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    sget-object v4, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@a5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v3

    #@a9
    const-string v4, "/"

    #@ab
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v3

    #@af
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@b1
    iget v4, v4, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@b3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v3

    #@b7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v3

    #@bb
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@be
    move-result-object v0

    #@bf
    .line 401
    .local v0, contentUri:Landroid/net/Uri;
    const-string v3, "hint"

    #@c1
    iget-object v4, v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@c3
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@c6
    .line 402
    const-string v3, "total_bytes"

    #@c8
    iget-wide v4, v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@ca
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@cd
    move-result-object v4

    #@ce
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@d1
    .line 403
    const-string v3, "mimetype"

    #@d3
    iget-object v4, v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mMimetype:Ljava/lang/String;

    #@d5
    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@d8
    .line 405
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@da
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@dd
    move-result-object v3

    #@de
    invoke-virtual {v3, v0, v2, v7, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@e1
    goto/16 :goto_43
.end method

.method private sendFile(Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;)I
    .registers 28
    .parameter "fileInfo"

    #@0
    .prologue
    .line 412
    const/4 v8, 0x0

    #@1
    .line 413
    .local v8, error:Z
    const/16 v17, -0x1

    #@3
    .line 414
    .local v17, responseCode:I
    const/16 v18, 0xc8

    #@5
    .line 415
    .local v18, status:I
    new-instance v22, Ljava/lang/StringBuilder;

    #@7
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    sget-object v23, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@c
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v22

    #@10
    const-string v23, "/"

    #@12
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v22

    #@16
    move-object/from16 v0, p0

    #@18
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@1a
    move-object/from16 v23, v0

    #@1c
    move-object/from16 v0, v23

    #@1e
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@20
    move/from16 v23, v0

    #@22
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v22

    #@26
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v22

    #@2a
    invoke-static/range {v22 .. v22}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@2d
    move-result-object v6

    #@2e
    .line 418
    .local v6, contentUri:Landroid/net/Uri;
    new-instance v16, Ljavax/obex/HeaderSet;

    #@30
    invoke-direct/range {v16 .. v16}, Ljavax/obex/HeaderSet;-><init>()V

    #@33
    .line 419
    .local v16, request:Ljavax/obex/HeaderSet;
    const/16 v22, 0x1

    #@35
    move-object/from16 v0, p1

    #@37
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@39
    move-object/from16 v23, v0

    #@3b
    move-object/from16 v0, v16

    #@3d
    move/from16 v1, v22

    #@3f
    move-object/from16 v2, v23

    #@41
    invoke-virtual {v0, v1, v2}, Ljavax/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    #@44
    .line 420
    const/16 v22, 0x42

    #@46
    move-object/from16 v0, p1

    #@48
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mMimetype:Ljava/lang/String;

    #@4a
    move-object/from16 v23, v0

    #@4c
    move-object/from16 v0, v16

    #@4e
    move/from16 v1, v22

    #@50
    move-object/from16 v2, v23

    #@52
    invoke-virtual {v0, v1, v2}, Ljavax/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    #@55
    .line 422
    move-object/from16 v0, p0

    #@57
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@59
    move-object/from16 v22, v0

    #@5b
    move-object/from16 v0, v22

    #@5d
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDestination:Ljava/lang/String;

    #@5f
    move-object/from16 v22, v0

    #@61
    move-object/from16 v0, p1

    #@63
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@65
    move-object/from16 v23, v0

    #@67
    move-object/from16 v0, v16

    #@69
    move-object/from16 v1, v22

    #@6b
    move-object/from16 v2, v23

    #@6d
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->applyRemoteDeviceQuirks(Ljavax/obex/HeaderSet;Ljava/lang/String;Ljava/lang/String;)V

    #@70
    .line 424
    move-object/from16 v0, p0

    #@72
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@74
    move-object/from16 v22, v0

    #@76
    move-object/from16 v0, p0

    #@78
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@7a
    move-object/from16 v23, v0

    #@7c
    move-object/from16 v0, v23

    #@7e
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@80
    move/from16 v23, v0

    #@82
    const/16 v24, 0xc0

    #@84
    invoke-static/range {v22 .. v24}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@87
    .line 426
    const/16 v22, 0xc3

    #@89
    move-object/from16 v0, p1

    #@8b
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@8d
    move-wide/from16 v23, v0

    #@8f
    invoke-static/range {v23 .. v24}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@92
    move-result-object v23

    #@93
    move-object/from16 v0, v16

    #@95
    move/from16 v1, v22

    #@97
    move-object/from16 v2, v23

    #@99
    invoke-virtual {v0, v1, v2}, Ljavax/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    #@9c
    .line 427
    const/4 v14, 0x0

    #@9d
    .line 428
    .local v14, putOperation:Ljavax/obex/ClientOperation;
    const/4 v12, 0x0

    #@9e
    .line 429
    .local v12, outputStream:Ljava/io/OutputStream;
    const/4 v9, 0x0

    #@9f
    .line 431
    .local v9, inputStream:Ljava/io/InputStream;
    :try_start_9f
    monitor-enter p0
    :try_end_a0
    .catchall {:try_start_9f .. :try_end_a0} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_9f .. :try_end_a0} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_9f .. :try_end_a0} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_9f .. :try_end_a0} :catch_442

    #@a0
    .line 432
    :try_start_a0
    move-object/from16 v0, p0

    #@a2
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@a4
    move-object/from16 v22, v0

    #@a6
    const/16 v23, 0x1

    #@a8
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z
    invoke-static/range {v22 .. v23}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$002(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z

    #@ab
    .line 433
    monitor-exit p0
    :try_end_ac
    .catchall {:try_start_a0 .. :try_end_ac} :catchall_284

    #@ac
    .line 436
    :try_start_ac
    const-string v22, "BtOppObexClient"

    #@ae
    new-instance v23, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v24, "put headerset for "

    #@b5
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v23

    #@b9
    move-object/from16 v0, p1

    #@bb
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@bd
    move-object/from16 v24, v0

    #@bf
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v23

    #@c3
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v23

    #@c7
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    .line 438
    move-object/from16 v0, p0

    #@cc
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mCs:Ljavax/obex/ClientSession;

    #@ce
    move-object/from16 v22, v0

    #@d0
    move-object/from16 v0, v22

    #@d2
    move-object/from16 v1, v16

    #@d4
    invoke-virtual {v0, v1}, Ljavax/obex/ClientSession;->put(Ljavax/obex/HeaderSet;)Ljavax/obex/Operation;

    #@d7
    move-result-object v22

    #@d8
    move-object/from16 v0, v22

    #@da
    check-cast v0, Ljavax/obex/ClientOperation;

    #@dc
    move-object v14, v0
    :try_end_dd
    .catchall {:try_start_ac .. :try_end_dd} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_ac .. :try_end_dd} :catch_334
    .catch Ljava/lang/NullPointerException; {:try_start_ac .. :try_end_dd} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_ac .. :try_end_dd} :catch_442

    #@dd
    .line 446
    :goto_dd
    :try_start_dd
    monitor-enter p0
    :try_end_de
    .catchall {:try_start_dd .. :try_end_de} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_dd .. :try_end_de} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_dd .. :try_end_de} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_dd .. :try_end_de} :catch_442

    #@de
    .line 447
    :try_start_de
    move-object/from16 v0, p0

    #@e0
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@e2
    move-object/from16 v22, v0

    #@e4
    const/16 v23, 0x0

    #@e6
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z
    invoke-static/range {v22 .. v23}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$002(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z

    #@e9
    .line 448
    monitor-exit p0
    :try_end_ea
    .catchall {:try_start_de .. :try_end_ea} :catchall_35c

    #@ea
    .line 450
    if-nez v8, :cond_112

    #@ec
    .line 453
    :try_start_ec
    const-string v22, "BtOppObexClient"

    #@ee
    new-instance v23, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string v24, "openOutputStream "

    #@f5
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v23

    #@f9
    move-object/from16 v0, p1

    #@fb
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@fd
    move-object/from16 v24, v0

    #@ff
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v23

    #@103
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v23

    #@107
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    .line 455
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->openOutputStream()Ljava/io/OutputStream;

    #@10d
    move-result-object v12

    #@10e
    .line 456
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->openInputStream()Ljava/io/InputStream;
    :try_end_111
    .catchall {:try_start_ec .. :try_end_111} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_ec .. :try_end_111} :catch_417
    .catch Ljava/lang/NullPointerException; {:try_start_ec .. :try_end_111} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_ec .. :try_end_111} :catch_442

    #@111
    move-result-object v9

    #@112
    .line 464
    :cond_112
    :goto_112
    if-nez v8, :cond_148

    #@114
    .line 465
    :try_start_114
    new-instance v21, Landroid/content/ContentValues;

    #@116
    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    #@119
    .line 466
    .local v21, updateValues:Landroid/content/ContentValues;
    const-string v22, "current_bytes"

    #@11b
    const/16 v23, 0x0

    #@11d
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@120
    move-result-object v23

    #@121
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@124
    .line 467
    const-string v22, "status"

    #@126
    const/16 v23, 0xc0

    #@128
    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12b
    move-result-object v23

    #@12c
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@12f
    .line 468
    move-object/from16 v0, p0

    #@131
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@133
    move-object/from16 v22, v0

    #@135
    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@138
    move-result-object v22

    #@139
    const/16 v23, 0x0

    #@13b
    const/16 v24, 0x0

    #@13d
    move-object/from16 v0, v22

    #@13f
    move-object/from16 v1, v21

    #@141
    move-object/from16 v2, v23

    #@143
    move-object/from16 v3, v24

    #@145
    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@148
    .line 471
    .end local v21           #updateValues:Landroid/content/ContentValues;
    :cond_148
    if-nez v8, :cond_684

    #@14a
    .line 472
    const/4 v13, 0x0

    #@14b
    .line 473
    .local v13, position:I
    const/4 v15, 0x0

    #@14c
    .line 474
    .local v15, readLength:I
    const/4 v10, 0x0

    #@14d
    .line 475
    .local v10, okToProceed:Z
    const-wide/16 v19, 0x0

    #@14f
    .line 476
    .local v19, timestamp:J
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->getMaxPacketSize()I

    #@152
    move-result v11

    #@153
    .line 477
    .local v11, outputBufferSize:I
    new-array v5, v11, [B

    #@155
    .line 478
    .local v5, buffer:[B
    new-instance v4, Ljava/io/BufferedInputStream;

    #@157
    move-object/from16 v0, p1

    #@159
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mInputStream:Ljava/io/FileInputStream;

    #@15b
    move-object/from16 v22, v0

    #@15d
    const/16 v23, 0x4000

    #@15f
    move-object/from16 v0, v22

    #@161
    move/from16 v1, v23

    #@163
    invoke-direct {v4, v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    #@166
    .line 480
    .local v4, a:Ljava/io/BufferedInputStream;
    move-object/from16 v0, p0

    #@168
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@16a
    move-object/from16 v22, v0

    #@16c
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mInterrupted:Z
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$100(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Z

    #@16f
    move-result v22

    #@170
    if-nez v22, :cond_224

    #@172
    int-to-long v0, v13

    #@173
    move-wide/from16 v22, v0

    #@175
    move-object/from16 v0, p1

    #@177
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@179
    move-wide/from16 v24, v0

    #@17b
    cmp-long v22, v22, v24

    #@17d
    if-eqz v22, :cond_224

    #@17f
    .line 481
    #calls: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->readFully(Ljava/io/InputStream;[BI)I
    invoke-static {v4, v5, v11}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$700(Ljava/io/InputStream;[BI)I

    #@182
    move-result v15

    #@183
    .line 483
    move-object/from16 v0, p0

    #@185
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@187
    move-object/from16 v22, v0

    #@189
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$200(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/os/Handler;

    #@18c
    move-result-object v22

    #@18d
    move-object/from16 v0, p0

    #@18f
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@191
    move-object/from16 v23, v0

    #@193
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;
    invoke-static/range {v23 .. v23}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$200(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/os/Handler;

    #@196
    move-result-object v23

    #@197
    const/16 v24, 0x4

    #@199
    invoke-virtual/range {v23 .. v24}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@19c
    move-result-object v23

    #@19d
    const-wide/32 v24, 0xc350

    #@1a0
    invoke-virtual/range {v22 .. v25}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1a3
    .line 486
    monitor-enter p0
    :try_end_1a4
    .catchall {:try_start_114 .. :try_end_1a4} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_114 .. :try_end_1a4} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_114 .. :try_end_1a4} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_114 .. :try_end_1a4} :catch_442

    #@1a4
    .line 487
    :try_start_1a4
    move-object/from16 v0, p0

    #@1a6
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@1a8
    move-object/from16 v22, v0

    #@1aa
    const/16 v23, 0x1

    #@1ac
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z
    invoke-static/range {v22 .. v23}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$002(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z

    #@1af
    .line 488
    monitor-exit p0
    :try_end_1b0
    .catchall {:try_start_1a4 .. :try_end_1b0} :catchall_43f

    #@1b0
    .line 491
    const/16 v22, 0x0

    #@1b2
    :try_start_1b2
    move/from16 v0, v22

    #@1b4
    invoke-virtual {v12, v5, v0, v15}, Ljava/io/OutputStream;->write([BII)V

    #@1b7
    .line 493
    add-int/2addr v13, v15

    #@1b8
    .line 495
    int-to-long v0, v13

    #@1b9
    move-wide/from16 v22, v0

    #@1bb
    move-object/from16 v0, p1

    #@1bd
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@1bf
    move-wide/from16 v24, v0

    #@1c1
    cmp-long v22, v22, v24

    #@1c3
    if-eqz v22, :cond_59c

    #@1c5
    .line 496
    move-object/from16 v0, p0

    #@1c7
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@1c9
    move-object/from16 v22, v0

    #@1cb
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$200(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/os/Handler;

    #@1ce
    move-result-object v22

    #@1cf
    const/16 v23, 0x4

    #@1d1
    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->removeMessages(I)V

    #@1d4
    .line 497
    monitor-enter p0
    :try_end_1d5
    .catchall {:try_start_1b2 .. :try_end_1d5} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_1b2 .. :try_end_1d5} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_1b2 .. :try_end_1d5} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1b2 .. :try_end_1d5} :catch_442

    #@1d5
    .line 498
    :try_start_1d5
    move-object/from16 v0, p0

    #@1d7
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@1d9
    move-object/from16 v22, v0

    #@1db
    const/16 v23, 0x0

    #@1dd
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z
    invoke-static/range {v22 .. v23}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$002(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z

    #@1e0
    .line 499
    monitor-exit p0
    :try_end_1e1
    .catchall {:try_start_1d5 .. :try_end_1e1} :catchall_4f7

    #@1e1
    .line 510
    :goto_1e1
    :try_start_1e1
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->getResponseCode()I

    #@1e4
    move-result v17

    #@1e5
    .line 512
    const/16 v22, 0x90

    #@1e7
    move/from16 v0, v17

    #@1e9
    move/from16 v1, v22

    #@1eb
    if-eq v0, v1, :cond_1f5

    #@1ed
    const/16 v22, 0xa0

    #@1ef
    move/from16 v0, v17

    #@1f1
    move/from16 v1, v22

    #@1f3
    if-ne v0, v1, :cond_5c0

    #@1f5
    .line 515
    :cond_1f5
    const-string v22, "BtOppObexClient"

    #@1f7
    const-string v23, "Remote accept"

    #@1f9
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1fc
    .line 517
    const/4 v10, 0x1

    #@1fd
    .line 518
    new-instance v21, Landroid/content/ContentValues;

    #@1ff
    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    #@202
    .line 519
    .restart local v21       #updateValues:Landroid/content/ContentValues;
    const-string v22, "current_bytes"

    #@204
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@207
    move-result-object v23

    #@208
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@20b
    .line 520
    move-object/from16 v0, p0

    #@20d
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@20f
    move-object/from16 v22, v0

    #@211
    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@214
    move-result-object v22

    #@215
    const/16 v23, 0x0

    #@217
    const/16 v24, 0x0

    #@219
    move-object/from16 v0, v22

    #@21b
    move-object/from16 v1, v21

    #@21d
    move-object/from16 v2, v23

    #@21f
    move-object/from16 v3, v24

    #@221
    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@224
    .line 527
    .end local v21           #updateValues:Landroid/content/ContentValues;
    :cond_224
    :goto_224
    move-object/from16 v0, p0

    #@226
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@228
    move-object/from16 v22, v0

    #@22a
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mInterrupted:Z
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$100(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Z

    #@22d
    move-result v22

    #@22e
    if-nez v22, :cond_644

    #@230
    if-eqz v10, :cond_644

    #@232
    int-to-long v0, v13

    #@233
    move-wide/from16 v22, v0

    #@235
    move-object/from16 v0, p1

    #@237
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@239
    move-wide/from16 v24, v0

    #@23b
    cmp-long v22, v22, v24

    #@23d
    if-eqz v22, :cond_644

    #@23f
    .line 530
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@242
    move-result-wide v19

    #@243
    .line 533
    const/16 v22, 0x0

    #@245
    move/from16 v0, v22

    #@247
    invoke-virtual {v4, v5, v0, v11}, Ljava/io/BufferedInputStream;->read([BII)I

    #@24a
    move-result v15

    #@24b
    .line 534
    const/16 v22, 0x0

    #@24d
    move/from16 v0, v22

    #@24f
    invoke-virtual {v12, v5, v0, v15}, Ljava/io/OutputStream;->write([BII)V

    #@252
    .line 537
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->getResponseCode()I

    #@255
    move-result v17

    #@256
    .line 539
    const-string v22, "BtOppObexClient"

    #@258
    new-instance v23, Ljava/lang/StringBuilder;

    #@25a
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@25d
    const-string v24, "Response code is "

    #@25f
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@262
    move-result-object v23

    #@263
    move-object/from16 v0, v23

    #@265
    move/from16 v1, v17

    #@267
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26a
    move-result-object v23

    #@26b
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26e
    move-result-object v23

    #@26f
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_272
    .catchall {:try_start_1e1 .. :try_end_272} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_1e1 .. :try_end_272} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_1e1 .. :try_end_272} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1e1 .. :try_end_272} :catch_442

    #@272
    .line 541
    const/16 v22, 0x90

    #@274
    move/from16 v0, v17

    #@276
    move/from16 v1, v22

    #@278
    if-eq v0, v1, :cond_5de

    #@27a
    const/16 v22, 0xa0

    #@27c
    move/from16 v0, v17

    #@27e
    move/from16 v1, v22

    #@280
    if-eq v0, v1, :cond_5de

    #@282
    .line 544
    const/4 v10, 0x0

    #@283
    goto :goto_224

    #@284
    .line 433
    .end local v4           #a:Ljava/io/BufferedInputStream;
    .end local v5           #buffer:[B
    .end local v10           #okToProceed:Z
    .end local v11           #outputBufferSize:I
    .end local v13           #position:I
    .end local v15           #readLength:I
    .end local v19           #timestamp:J
    :catchall_284
    move-exception v22

    #@285
    :try_start_285
    monitor-exit p0
    :try_end_286
    .catchall {:try_start_285 .. :try_end_286} :catchall_284

    #@286
    :try_start_286
    throw v22
    :try_end_287
    .catchall {:try_start_286 .. :try_end_287} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_286 .. :try_end_287} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_286 .. :try_end_287} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_286 .. :try_end_287} :catch_442

    #@287
    .line 584
    :catch_287
    move-exception v7

    #@288
    .line 585
    .local v7, e:Ljava/io/IOException;
    :try_start_288
    invoke-virtual {v7}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@28b
    move-result-object v22

    #@28c
    move-object/from16 v0, p0

    #@28e
    move-object/from16 v1, v22

    #@290
    invoke-direct {v0, v1}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->handleSendException(Ljava/lang/String;)V
    :try_end_293
    .catchall {:try_start_288 .. :try_end_293} :catchall_4fa

    #@293
    .line 593
    :try_start_293
    move-object/from16 v0, p0

    #@295
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@297
    move-object/from16 v22, v0

    #@299
    move-object/from16 v0, v22

    #@29b
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@29d
    move-object/from16 v22, v0

    #@29f
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->closeSendFileInfo(Landroid/net/Uri;)V

    #@2a2
    .line 594
    if-nez v8, :cond_30e

    #@2a4
    .line 595
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->getResponseCode()I

    #@2a7
    move-result v17

    #@2a8
    .line 596
    const/16 v22, -0x1

    #@2aa
    move/from16 v0, v17

    #@2ac
    move/from16 v1, v22

    #@2ae
    if-eq v0, v1, :cond_803

    #@2b0
    .line 598
    const-string v22, "BtOppObexClient"

    #@2b2
    new-instance v23, Ljava/lang/StringBuilder;

    #@2b4
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@2b7
    const-string v24, "Get response code "

    #@2b9
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2bc
    move-result-object v23

    #@2bd
    move-object/from16 v0, v23

    #@2bf
    move/from16 v1, v17

    #@2c1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c4
    move-result-object v23

    #@2c5
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c8
    move-result-object v23

    #@2c9
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2cc
    .line 600
    const/16 v22, 0xa0

    #@2ce
    move/from16 v0, v17

    #@2d0
    move/from16 v1, v22

    #@2d2
    if-eq v0, v1, :cond_30e

    #@2d4
    .line 601
    const-string v22, "BtOppObexClient"

    #@2d6
    new-instance v23, Ljava/lang/StringBuilder;

    #@2d8
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@2db
    const-string v24, "Response error code is "

    #@2dd
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e0
    move-result-object v23

    #@2e1
    move-object/from16 v0, v23

    #@2e3
    move/from16 v1, v17

    #@2e5
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e8
    move-result-object v23

    #@2e9
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2ec
    move-result-object v23

    #@2ed
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2f0
    .line 602
    const/16 v18, 0x1ef

    #@2f2
    .line 603
    const/16 v22, 0xcf

    #@2f4
    move/from16 v0, v17

    #@2f6
    move/from16 v1, v22

    #@2f8
    if-ne v0, v1, :cond_2fc

    #@2fa
    .line 604
    const/16 v18, 0x196

    #@2fc
    .line 606
    :cond_2fc
    const/16 v22, 0xc3

    #@2fe
    move/from16 v0, v17

    #@300
    move/from16 v1, v22

    #@302
    if-eq v0, v1, :cond_30c

    #@304
    const/16 v22, 0xc6

    #@306
    move/from16 v0, v17

    #@308
    move/from16 v1, v22

    #@30a
    if-ne v0, v1, :cond_30e

    #@30c
    .line 608
    :cond_30c
    const/16 v18, 0x193

    #@30e
    .line 617
    :cond_30e
    :goto_30e
    move-object/from16 v0, p0

    #@310
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@312
    move-object/from16 v22, v0

    #@314
    move-object/from16 v0, p0

    #@316
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@318
    move-object/from16 v23, v0

    #@31a
    move-object/from16 v0, v23

    #@31c
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@31e
    move/from16 v23, v0

    #@320
    move-object/from16 v0, v22

    #@322
    move/from16 v1, v23

    #@324
    move/from16 v2, v18

    #@326
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@329
    .line 619
    if-eqz v9, :cond_32e

    #@32b
    .line 620
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    #@32e
    .line 622
    :cond_32e
    if-eqz v14, :cond_333

    #@330
    .line 623
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->close()V
    :try_end_333
    .catch Ljava/io/IOException; {:try_start_293 .. :try_end_333} :catch_7fc

    #@333
    .line 629
    .end local v7           #e:Ljava/io/IOException;
    :cond_333
    :goto_333
    return v18

    #@334
    .line 439
    :catch_334
    move-exception v7

    #@335
    .line 440
    .restart local v7       #e:Ljava/io/IOException;
    const/16 v18, 0x1f0

    #@337
    .line 441
    :try_start_337
    move-object/from16 v0, p0

    #@339
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@33b
    move-object/from16 v22, v0

    #@33d
    move-object/from16 v0, p0

    #@33f
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@341
    move-object/from16 v23, v0

    #@343
    move-object/from16 v0, v23

    #@345
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@347
    move/from16 v23, v0

    #@349
    move-object/from16 v0, v22

    #@34b
    move/from16 v1, v23

    #@34d
    move/from16 v2, v18

    #@34f
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@352
    .line 443
    const-string v22, "BtOppObexClient"

    #@354
    const-string v23, "Error when put HeaderSet "

    #@356
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_359
    .catchall {:try_start_337 .. :try_end_359} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_337 .. :try_end_359} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_337 .. :try_end_359} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_337 .. :try_end_359} :catch_442

    #@359
    .line 444
    const/4 v8, 0x1

    #@35a
    goto/16 :goto_dd

    #@35c
    .line 448
    .end local v7           #e:Ljava/io/IOException;
    :catchall_35c
    move-exception v22

    #@35d
    :try_start_35d
    monitor-exit p0
    :try_end_35e
    .catchall {:try_start_35d .. :try_end_35e} :catchall_35c

    #@35e
    :try_start_35e
    throw v22
    :try_end_35f
    .catchall {:try_start_35e .. :try_end_35f} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_35e .. :try_end_35f} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_35e .. :try_end_35f} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_35e .. :try_end_35f} :catch_442

    #@35f
    .line 586
    :catch_35f
    move-exception v7

    #@360
    .line 587
    .local v7, e:Ljava/lang/NullPointerException;
    :try_start_360
    invoke-virtual {v7}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    #@363
    move-result-object v22

    #@364
    move-object/from16 v0, p0

    #@366
    move-object/from16 v1, v22

    #@368
    invoke-direct {v0, v1}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->handleSendException(Ljava/lang/String;)V
    :try_end_36b
    .catchall {:try_start_360 .. :try_end_36b} :catchall_4fa

    #@36b
    .line 593
    :try_start_36b
    move-object/from16 v0, p0

    #@36d
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@36f
    move-object/from16 v22, v0

    #@371
    move-object/from16 v0, v22

    #@373
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@375
    move-object/from16 v22, v0

    #@377
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->closeSendFileInfo(Landroid/net/Uri;)V

    #@37a
    .line 594
    if-nez v8, :cond_3e6

    #@37c
    .line 595
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->getResponseCode()I

    #@37f
    move-result v17

    #@380
    .line 596
    const/16 v22, -0x1

    #@382
    move/from16 v0, v17

    #@384
    move/from16 v1, v22

    #@386
    if-eq v0, v1, :cond_807

    #@388
    .line 598
    const-string v22, "BtOppObexClient"

    #@38a
    new-instance v23, Ljava/lang/StringBuilder;

    #@38c
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@38f
    const-string v24, "Get response code "

    #@391
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@394
    move-result-object v23

    #@395
    move-object/from16 v0, v23

    #@397
    move/from16 v1, v17

    #@399
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39c
    move-result-object v23

    #@39d
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a0
    move-result-object v23

    #@3a1
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a4
    .line 600
    const/16 v22, 0xa0

    #@3a6
    move/from16 v0, v17

    #@3a8
    move/from16 v1, v22

    #@3aa
    if-eq v0, v1, :cond_3e6

    #@3ac
    .line 601
    const-string v22, "BtOppObexClient"

    #@3ae
    new-instance v23, Ljava/lang/StringBuilder;

    #@3b0
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@3b3
    const-string v24, "Response error code is "

    #@3b5
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b8
    move-result-object v23

    #@3b9
    move-object/from16 v0, v23

    #@3bb
    move/from16 v1, v17

    #@3bd
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c0
    move-result-object v23

    #@3c1
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c4
    move-result-object v23

    #@3c5
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3c8
    .line 602
    const/16 v18, 0x1ef

    #@3ca
    .line 603
    const/16 v22, 0xcf

    #@3cc
    move/from16 v0, v17

    #@3ce
    move/from16 v1, v22

    #@3d0
    if-ne v0, v1, :cond_3d4

    #@3d2
    .line 604
    const/16 v18, 0x196

    #@3d4
    .line 606
    :cond_3d4
    const/16 v22, 0xc3

    #@3d6
    move/from16 v0, v17

    #@3d8
    move/from16 v1, v22

    #@3da
    if-eq v0, v1, :cond_3e4

    #@3dc
    const/16 v22, 0xc6

    #@3de
    move/from16 v0, v17

    #@3e0
    move/from16 v1, v22

    #@3e2
    if-ne v0, v1, :cond_3e6

    #@3e4
    .line 608
    :cond_3e4
    const/16 v18, 0x193

    #@3e6
    .line 617
    :cond_3e6
    :goto_3e6
    move-object/from16 v0, p0

    #@3e8
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@3ea
    move-object/from16 v22, v0

    #@3ec
    move-object/from16 v0, p0

    #@3ee
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@3f0
    move-object/from16 v23, v0

    #@3f2
    move-object/from16 v0, v23

    #@3f4
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@3f6
    move/from16 v23, v0

    #@3f8
    move-object/from16 v0, v22

    #@3fa
    move/from16 v1, v23

    #@3fc
    move/from16 v2, v18

    #@3fe
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@401
    .line 619
    if-eqz v9, :cond_406

    #@403
    .line 620
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    #@406
    .line 622
    :cond_406
    if-eqz v14, :cond_333

    #@408
    .line 623
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->close()V
    :try_end_40b
    .catch Ljava/io/IOException; {:try_start_36b .. :try_end_40b} :catch_40d

    #@40b
    goto/16 :goto_333

    #@40d
    .line 625
    :catch_40d
    move-exception v7

    #@40e
    .line 626
    .local v7, e:Ljava/io/IOException;
    const-string v22, "BtOppObexClient"

    #@410
    const-string v23, "Error when closing stream after send"

    #@412
    :goto_412
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@415
    goto/16 :goto_333

    #@417
    .line 457
    .end local v7           #e:Ljava/io/IOException;
    :catch_417
    move-exception v7

    #@418
    .line 458
    .restart local v7       #e:Ljava/io/IOException;
    const/16 v18, 0x1f0

    #@41a
    .line 459
    :try_start_41a
    move-object/from16 v0, p0

    #@41c
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@41e
    move-object/from16 v22, v0

    #@420
    move-object/from16 v0, p0

    #@422
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@424
    move-object/from16 v23, v0

    #@426
    move-object/from16 v0, v23

    #@428
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@42a
    move/from16 v23, v0

    #@42c
    move-object/from16 v0, v22

    #@42e
    move/from16 v1, v23

    #@430
    move/from16 v2, v18

    #@432
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@435
    .line 460
    const-string v22, "BtOppObexClient"

    #@437
    const-string v23, "Error when openOutputStream"

    #@439
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_43c
    .catchall {:try_start_41a .. :try_end_43c} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_41a .. :try_end_43c} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_41a .. :try_end_43c} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_41a .. :try_end_43c} :catch_442

    #@43c
    .line 461
    const/4 v8, 0x1

    #@43d
    goto/16 :goto_112

    #@43f
    .line 488
    .end local v7           #e:Ljava/io/IOException;
    .restart local v4       #a:Ljava/io/BufferedInputStream;
    .restart local v5       #buffer:[B
    .restart local v10       #okToProceed:Z
    .restart local v11       #outputBufferSize:I
    .restart local v13       #position:I
    .restart local v15       #readLength:I
    .restart local v19       #timestamp:J
    :catchall_43f
    move-exception v22

    #@440
    :try_start_440
    monitor-exit p0
    :try_end_441
    .catchall {:try_start_440 .. :try_end_441} :catchall_43f

    #@441
    :try_start_441
    throw v22
    :try_end_442
    .catchall {:try_start_441 .. :try_end_442} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_441 .. :try_end_442} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_441 .. :try_end_442} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_441 .. :try_end_442} :catch_442

    #@442
    .line 588
    .end local v4           #a:Ljava/io/BufferedInputStream;
    .end local v5           #buffer:[B
    .end local v10           #okToProceed:Z
    .end local v11           #outputBufferSize:I
    .end local v13           #position:I
    .end local v15           #readLength:I
    .end local v19           #timestamp:J
    :catch_442
    move-exception v7

    #@443
    .line 589
    .local v7, e:Ljava/lang/IndexOutOfBoundsException;
    :try_start_443
    invoke-virtual {v7}, Ljava/lang/IndexOutOfBoundsException;->toString()Ljava/lang/String;

    #@446
    move-result-object v22

    #@447
    move-object/from16 v0, p0

    #@449
    move-object/from16 v1, v22

    #@44b
    invoke-direct {v0, v1}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->handleSendException(Ljava/lang/String;)V
    :try_end_44e
    .catchall {:try_start_443 .. :try_end_44e} :catchall_4fa

    #@44e
    .line 593
    :try_start_44e
    move-object/from16 v0, p0

    #@450
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@452
    move-object/from16 v22, v0

    #@454
    move-object/from16 v0, v22

    #@456
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@458
    move-object/from16 v22, v0

    #@45a
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->closeSendFileInfo(Landroid/net/Uri;)V

    #@45d
    .line 594
    if-nez v8, :cond_4c9

    #@45f
    .line 595
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->getResponseCode()I

    #@462
    move-result v17

    #@463
    .line 596
    const/16 v22, -0x1

    #@465
    move/from16 v0, v17

    #@467
    move/from16 v1, v22

    #@469
    if-eq v0, v1, :cond_80b

    #@46b
    .line 598
    const-string v22, "BtOppObexClient"

    #@46d
    new-instance v23, Ljava/lang/StringBuilder;

    #@46f
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@472
    const-string v24, "Get response code "

    #@474
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@477
    move-result-object v23

    #@478
    move-object/from16 v0, v23

    #@47a
    move/from16 v1, v17

    #@47c
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47f
    move-result-object v23

    #@480
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@483
    move-result-object v23

    #@484
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@487
    .line 600
    const/16 v22, 0xa0

    #@489
    move/from16 v0, v17

    #@48b
    move/from16 v1, v22

    #@48d
    if-eq v0, v1, :cond_4c9

    #@48f
    .line 601
    const-string v22, "BtOppObexClient"

    #@491
    new-instance v23, Ljava/lang/StringBuilder;

    #@493
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@496
    const-string v24, "Response error code is "

    #@498
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49b
    move-result-object v23

    #@49c
    move-object/from16 v0, v23

    #@49e
    move/from16 v1, v17

    #@4a0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a3
    move-result-object v23

    #@4a4
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a7
    move-result-object v23

    #@4a8
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4ab
    .line 602
    const/16 v18, 0x1ef

    #@4ad
    .line 603
    const/16 v22, 0xcf

    #@4af
    move/from16 v0, v17

    #@4b1
    move/from16 v1, v22

    #@4b3
    if-ne v0, v1, :cond_4b7

    #@4b5
    .line 604
    const/16 v18, 0x196

    #@4b7
    .line 606
    :cond_4b7
    const/16 v22, 0xc3

    #@4b9
    move/from16 v0, v17

    #@4bb
    move/from16 v1, v22

    #@4bd
    if-eq v0, v1, :cond_4c7

    #@4bf
    const/16 v22, 0xc6

    #@4c1
    move/from16 v0, v17

    #@4c3
    move/from16 v1, v22

    #@4c5
    if-ne v0, v1, :cond_4c9

    #@4c7
    .line 608
    :cond_4c7
    const/16 v18, 0x193

    #@4c9
    .line 617
    :cond_4c9
    :goto_4c9
    move-object/from16 v0, p0

    #@4cb
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@4cd
    move-object/from16 v22, v0

    #@4cf
    move-object/from16 v0, p0

    #@4d1
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@4d3
    move-object/from16 v23, v0

    #@4d5
    move-object/from16 v0, v23

    #@4d7
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@4d9
    move/from16 v23, v0

    #@4db
    move-object/from16 v0, v22

    #@4dd
    move/from16 v1, v23

    #@4df
    move/from16 v2, v18

    #@4e1
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@4e4
    .line 619
    if-eqz v9, :cond_4e9

    #@4e6
    .line 620
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    #@4e9
    .line 622
    :cond_4e9
    if-eqz v14, :cond_333

    #@4eb
    .line 623
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->close()V
    :try_end_4ee
    .catch Ljava/io/IOException; {:try_start_44e .. :try_end_4ee} :catch_4f0

    #@4ee
    goto/16 :goto_333

    #@4f0
    .line 625
    :catch_4f0
    move-exception v7

    #@4f1
    .line 626
    .local v7, e:Ljava/io/IOException;
    const-string v22, "BtOppObexClient"

    #@4f3
    const-string v23, "Error when closing stream after send"

    #@4f5
    goto/16 :goto_412

    #@4f7
    .line 499
    .end local v7           #e:Ljava/io/IOException;
    .restart local v4       #a:Ljava/io/BufferedInputStream;
    .restart local v5       #buffer:[B
    .restart local v10       #okToProceed:Z
    .restart local v11       #outputBufferSize:I
    .restart local v13       #position:I
    .restart local v15       #readLength:I
    .restart local v19       #timestamp:J
    :catchall_4f7
    move-exception v22

    #@4f8
    :try_start_4f8
    monitor-exit p0
    :try_end_4f9
    .catchall {:try_start_4f8 .. :try_end_4f9} :catchall_4f7

    #@4f9
    :try_start_4f9
    throw v22
    :try_end_4fa
    .catchall {:try_start_4f9 .. :try_end_4fa} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_4f9 .. :try_end_4fa} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_4f9 .. :try_end_4fa} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_4f9 .. :try_end_4fa} :catch_442

    #@4fa
    .line 591
    .end local v4           #a:Ljava/io/BufferedInputStream;
    .end local v5           #buffer:[B
    .end local v10           #okToProceed:Z
    .end local v11           #outputBufferSize:I
    .end local v13           #position:I
    .end local v15           #readLength:I
    .end local v19           #timestamp:J
    :catchall_4fa
    move-exception v22

    #@4fb
    .line 593
    :try_start_4fb
    move-object/from16 v0, p0

    #@4fd
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@4ff
    move-object/from16 v23, v0

    #@501
    move-object/from16 v0, v23

    #@503
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@505
    move-object/from16 v23, v0

    #@507
    invoke-static/range {v23 .. v23}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->closeSendFileInfo(Landroid/net/Uri;)V

    #@50a
    .line 594
    if-nez v8, :cond_576

    #@50c
    .line 595
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->getResponseCode()I

    #@50f
    move-result v17

    #@510
    .line 596
    const/16 v23, -0x1

    #@512
    move/from16 v0, v17

    #@514
    move/from16 v1, v23

    #@516
    if-eq v0, v1, :cond_7f8

    #@518
    .line 598
    const-string v23, "BtOppObexClient"

    #@51a
    new-instance v24, Ljava/lang/StringBuilder;

    #@51c
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@51f
    const-string v25, "Get response code "

    #@521
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@524
    move-result-object v24

    #@525
    move-object/from16 v0, v24

    #@527
    move/from16 v1, v17

    #@529
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@52c
    move-result-object v24

    #@52d
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@530
    move-result-object v24

    #@531
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@534
    .line 600
    const/16 v23, 0xa0

    #@536
    move/from16 v0, v17

    #@538
    move/from16 v1, v23

    #@53a
    if-eq v0, v1, :cond_576

    #@53c
    .line 601
    const-string v23, "BtOppObexClient"

    #@53e
    new-instance v24, Ljava/lang/StringBuilder;

    #@540
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@543
    const-string v25, "Response error code is "

    #@545
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@548
    move-result-object v24

    #@549
    move-object/from16 v0, v24

    #@54b
    move/from16 v1, v17

    #@54d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@550
    move-result-object v24

    #@551
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@554
    move-result-object v24

    #@555
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@558
    .line 602
    const/16 v18, 0x1ef

    #@55a
    .line 603
    const/16 v23, 0xcf

    #@55c
    move/from16 v0, v17

    #@55e
    move/from16 v1, v23

    #@560
    if-ne v0, v1, :cond_564

    #@562
    .line 604
    const/16 v18, 0x196

    #@564
    .line 606
    :cond_564
    const/16 v23, 0xc3

    #@566
    move/from16 v0, v17

    #@568
    move/from16 v1, v23

    #@56a
    if-eq v0, v1, :cond_574

    #@56c
    const/16 v23, 0xc6

    #@56e
    move/from16 v0, v17

    #@570
    move/from16 v1, v23

    #@572
    if-ne v0, v1, :cond_576

    #@574
    .line 608
    :cond_574
    const/16 v18, 0x193

    #@576
    .line 617
    :cond_576
    :goto_576
    move-object/from16 v0, p0

    #@578
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@57a
    move-object/from16 v23, v0

    #@57c
    move-object/from16 v0, p0

    #@57e
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@580
    move-object/from16 v24, v0

    #@582
    move-object/from16 v0, v24

    #@584
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@586
    move/from16 v24, v0

    #@588
    move-object/from16 v0, v23

    #@58a
    move/from16 v1, v24

    #@58c
    move/from16 v2, v18

    #@58e
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@591
    .line 619
    if-eqz v9, :cond_596

    #@593
    .line 620
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    #@596
    .line 622
    :cond_596
    if-eqz v14, :cond_59b

    #@598
    .line 623
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->close()V
    :try_end_59b
    .catch Ljava/io/IOException; {:try_start_4fb .. :try_end_59b} :catch_7ee

    #@59b
    .line 591
    :cond_59b
    :goto_59b
    throw v22

    #@59c
    .line 503
    .restart local v4       #a:Ljava/io/BufferedInputStream;
    .restart local v5       #buffer:[B
    .restart local v10       #okToProceed:Z
    .restart local v11       #outputBufferSize:I
    .restart local v13       #position:I
    .restart local v15       #readLength:I
    .restart local v19       #timestamp:J
    :cond_59c
    :try_start_59c
    invoke-virtual {v12}, Ljava/io/OutputStream;->close()V

    #@59f
    .line 504
    move-object/from16 v0, p0

    #@5a1
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@5a3
    move-object/from16 v22, v0

    #@5a5
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$200(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/os/Handler;

    #@5a8
    move-result-object v22

    #@5a9
    const/16 v23, 0x4

    #@5ab
    invoke-virtual/range {v22 .. v23}, Landroid/os/Handler;->removeMessages(I)V

    #@5ae
    .line 505
    monitor-enter p0
    :try_end_5af
    .catchall {:try_start_59c .. :try_end_5af} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_59c .. :try_end_5af} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_59c .. :try_end_5af} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_59c .. :try_end_5af} :catch_442

    #@5af
    .line 506
    :try_start_5af
    move-object/from16 v0, p0

    #@5b1
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@5b3
    move-object/from16 v22, v0

    #@5b5
    const/16 v23, 0x0

    #@5b7
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z
    invoke-static/range {v22 .. v23}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$002(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z

    #@5ba
    .line 507
    monitor-exit p0

    #@5bb
    goto/16 :goto_1e1

    #@5bd
    :catchall_5bd
    move-exception v22

    #@5be
    monitor-exit p0
    :try_end_5bf
    .catchall {:try_start_5af .. :try_end_5bf} :catchall_5bd

    #@5bf
    :try_start_5bf
    throw v22

    #@5c0
    .line 523
    :cond_5c0
    const-string v22, "BtOppObexClient"

    #@5c2
    new-instance v23, Ljava/lang/StringBuilder;

    #@5c4
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@5c7
    const-string v24, "Remote reject, Response code is "

    #@5c9
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5cc
    move-result-object v23

    #@5cd
    move-object/from16 v0, v23

    #@5cf
    move/from16 v1, v17

    #@5d1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5d4
    move-result-object v23

    #@5d5
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d8
    move-result-object v23

    #@5d9
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5dc
    goto/16 :goto_224

    #@5de
    .line 546
    :cond_5de
    add-int/2addr v13, v15

    #@5df
    .line 548
    const-string v22, "BtOppObexClient"

    #@5e1
    new-instance v23, Ljava/lang/StringBuilder;

    #@5e3
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@5e6
    const-string v24, "Sending file position = "

    #@5e8
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5eb
    move-result-object v23

    #@5ec
    move-object/from16 v0, v23

    #@5ee
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5f1
    move-result-object v23

    #@5f2
    const-string v24, " readLength "

    #@5f4
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f7
    move-result-object v23

    #@5f8
    move-object/from16 v0, v23

    #@5fa
    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5fd
    move-result-object v23

    #@5fe
    const-string v24, " bytes took "

    #@600
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@603
    move-result-object v23

    #@604
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@607
    move-result-wide v24

    #@608
    sub-long v24, v24, v19

    #@60a
    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@60d
    move-result-object v23

    #@60e
    const-string v24, " ms"

    #@610
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@613
    move-result-object v23

    #@614
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@617
    move-result-object v23

    #@618
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@61b
    .line 552
    new-instance v21, Landroid/content/ContentValues;

    #@61d
    invoke-direct/range {v21 .. v21}, Landroid/content/ContentValues;-><init>()V

    #@620
    .line 553
    .restart local v21       #updateValues:Landroid/content/ContentValues;
    const-string v22, "current_bytes"

    #@622
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@625
    move-result-object v23

    #@626
    invoke-virtual/range {v21 .. v23}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@629
    .line 554
    move-object/from16 v0, p0

    #@62b
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@62d
    move-object/from16 v22, v0

    #@62f
    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@632
    move-result-object v22

    #@633
    const/16 v23, 0x0

    #@635
    const/16 v24, 0x0

    #@637
    move-object/from16 v0, v22

    #@639
    move-object/from16 v1, v21

    #@63b
    move-object/from16 v2, v23

    #@63d
    move-object/from16 v3, v24

    #@63f
    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@642
    goto/16 :goto_224

    #@644
    .line 560
    .end local v21           #updateValues:Landroid/content/ContentValues;
    :cond_644
    const/16 v22, 0xc3

    #@646
    move/from16 v0, v17

    #@648
    move/from16 v1, v22

    #@64a
    if-eq v0, v1, :cond_654

    #@64c
    const/16 v22, 0xc6

    #@64e
    move/from16 v0, v17

    #@650
    move/from16 v1, v22

    #@652
    if-ne v0, v1, :cond_72d

    #@654
    .line 562
    :cond_654
    const-string v22, "BtOppObexClient"

    #@656
    new-instance v23, Ljava/lang/StringBuilder;

    #@658
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@65b
    const-string v24, "Remote reject file "

    #@65d
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@660
    move-result-object v23

    #@661
    move-object/from16 v0, p1

    #@663
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@665
    move-object/from16 v24, v0

    #@667
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66a
    move-result-object v23

    #@66b
    const-string v24, " length "

    #@66d
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@670
    move-result-object v23

    #@671
    move-object/from16 v0, p1

    #@673
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@675
    move-wide/from16 v24, v0

    #@677
    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@67a
    move-result-object v23

    #@67b
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67e
    move-result-object v23

    #@67f
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_682
    .catchall {:try_start_5bf .. :try_end_682} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_5bf .. :try_end_682} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_5bf .. :try_end_682} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_5bf .. :try_end_682} :catch_442

    #@682
    .line 564
    const/16 v18, 0x193

    #@684
    .line 593
    .end local v4           #a:Ljava/io/BufferedInputStream;
    .end local v5           #buffer:[B
    .end local v10           #okToProceed:Z
    .end local v11           #outputBufferSize:I
    .end local v13           #position:I
    .end local v15           #readLength:I
    .end local v19           #timestamp:J
    :cond_684
    :goto_684
    :try_start_684
    move-object/from16 v0, p0

    #@686
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@688
    move-object/from16 v22, v0

    #@68a
    move-object/from16 v0, v22

    #@68c
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@68e
    move-object/from16 v22, v0

    #@690
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->closeSendFileInfo(Landroid/net/Uri;)V

    #@693
    .line 594
    if-nez v8, :cond_6ff

    #@695
    .line 595
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->getResponseCode()I

    #@698
    move-result v17

    #@699
    .line 596
    const/16 v22, -0x1

    #@69b
    move/from16 v0, v17

    #@69d
    move/from16 v1, v22

    #@69f
    if-eq v0, v1, :cond_80f

    #@6a1
    .line 598
    const-string v22, "BtOppObexClient"

    #@6a3
    new-instance v23, Ljava/lang/StringBuilder;

    #@6a5
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@6a8
    const-string v24, "Get response code "

    #@6aa
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6ad
    move-result-object v23

    #@6ae
    move-object/from16 v0, v23

    #@6b0
    move/from16 v1, v17

    #@6b2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6b5
    move-result-object v23

    #@6b6
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b9
    move-result-object v23

    #@6ba
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6bd
    .line 600
    const/16 v22, 0xa0

    #@6bf
    move/from16 v0, v17

    #@6c1
    move/from16 v1, v22

    #@6c3
    if-eq v0, v1, :cond_6ff

    #@6c5
    .line 601
    const-string v22, "BtOppObexClient"

    #@6c7
    new-instance v23, Ljava/lang/StringBuilder;

    #@6c9
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@6cc
    const-string v24, "Response error code is "

    #@6ce
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d1
    move-result-object v23

    #@6d2
    move-object/from16 v0, v23

    #@6d4
    move/from16 v1, v17

    #@6d6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d9
    move-result-object v23

    #@6da
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6dd
    move-result-object v23

    #@6de
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6e1
    .line 602
    const/16 v18, 0x1ef

    #@6e3
    .line 603
    const/16 v22, 0xcf

    #@6e5
    move/from16 v0, v17

    #@6e7
    move/from16 v1, v22

    #@6e9
    if-ne v0, v1, :cond_6ed

    #@6eb
    .line 604
    const/16 v18, 0x196

    #@6ed
    .line 606
    :cond_6ed
    const/16 v22, 0xc3

    #@6ef
    move/from16 v0, v17

    #@6f1
    move/from16 v1, v22

    #@6f3
    if-eq v0, v1, :cond_6fd

    #@6f5
    const/16 v22, 0xc6

    #@6f7
    move/from16 v0, v17

    #@6f9
    move/from16 v1, v22

    #@6fb
    if-ne v0, v1, :cond_6ff

    #@6fd
    .line 608
    :cond_6fd
    const/16 v18, 0x193

    #@6ff
    .line 617
    :cond_6ff
    :goto_6ff
    move-object/from16 v0, p0

    #@701
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mContext1:Landroid/content/Context;

    #@703
    move-object/from16 v22, v0

    #@705
    move-object/from16 v0, p0

    #@707
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@709
    move-object/from16 v23, v0

    #@70b
    move-object/from16 v0, v23

    #@70d
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@70f
    move/from16 v23, v0

    #@711
    move-object/from16 v0, v22

    #@713
    move/from16 v1, v23

    #@715
    move/from16 v2, v18

    #@717
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@71a
    .line 619
    if-eqz v9, :cond_71f

    #@71c
    .line 620
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    #@71f
    .line 622
    :cond_71f
    if-eqz v14, :cond_333

    #@721
    .line 623
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->close()V
    :try_end_724
    .catch Ljava/io/IOException; {:try_start_684 .. :try_end_724} :catch_726

    #@724
    goto/16 :goto_333

    #@726
    .line 625
    :catch_726
    move-exception v7

    #@727
    .line 626
    .restart local v7       #e:Ljava/io/IOException;
    const-string v22, "BtOppObexClient"

    #@729
    const-string v23, "Error when closing stream after send"

    #@72b
    goto/16 :goto_412

    #@72d
    .line 565
    .end local v7           #e:Ljava/io/IOException;
    .restart local v4       #a:Ljava/io/BufferedInputStream;
    .restart local v5       #buffer:[B
    .restart local v10       #okToProceed:Z
    .restart local v11       #outputBufferSize:I
    .restart local v13       #position:I
    .restart local v15       #readLength:I
    .restart local v19       #timestamp:J
    :cond_72d
    const/16 v22, 0xcf

    #@72f
    move/from16 v0, v17

    #@731
    move/from16 v1, v22

    #@733
    if-ne v0, v1, :cond_757

    #@735
    .line 566
    :try_start_735
    const-string v22, "BtOppObexClient"

    #@737
    new-instance v23, Ljava/lang/StringBuilder;

    #@739
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@73c
    const-string v24, "Remote reject file type "

    #@73e
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@741
    move-result-object v23

    #@742
    move-object/from16 v0, p1

    #@744
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mMimetype:Ljava/lang/String;

    #@746
    move-object/from16 v24, v0

    #@748
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@74b
    move-result-object v23

    #@74c
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@74f
    move-result-object v23

    #@750
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@753
    .line 567
    const/16 v18, 0x196

    #@755
    goto/16 :goto_684

    #@757
    .line 568
    :cond_757
    move-object/from16 v0, p0

    #@759
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@75b
    move-object/from16 v22, v0

    #@75d
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mInterrupted:Z
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$100(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Z

    #@760
    move-result v22

    #@761
    if-nez v22, :cond_7ac

    #@763
    int-to-long v0, v13

    #@764
    move-wide/from16 v22, v0

    #@766
    move-object/from16 v0, p1

    #@768
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@76a
    move-wide/from16 v24, v0

    #@76c
    cmp-long v22, v22, v24

    #@76e
    if-nez v22, :cond_7ac

    #@770
    .line 569
    const-string v22, "BtOppObexClient"

    #@772
    new-instance v23, Ljava/lang/StringBuilder;

    #@774
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@777
    const-string v24, "SendFile finished send out file "

    #@779
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77c
    move-result-object v23

    #@77d
    move-object/from16 v0, p1

    #@77f
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@781
    move-object/from16 v24, v0

    #@783
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@786
    move-result-object v23

    #@787
    const-string v24, " length "

    #@789
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78c
    move-result-object v23

    #@78d
    move-object/from16 v0, p1

    #@78f
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@791
    move-wide/from16 v24, v0

    #@793
    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@796
    move-result-object v23

    #@797
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79a
    move-result-object v23

    #@79b
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@79e
    .line 571
    invoke-virtual {v12}, Ljava/io/OutputStream;->close()V

    #@7a1
    .line 573
    move-object/from16 v0, p0

    #@7a3
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@7a5
    move-object/from16 v22, v0

    #@7a7
    invoke-static/range {v22 .. v22}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$508(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)I

    #@7aa
    goto/16 :goto_684

    #@7ac
    .line 576
    :cond_7ac
    const/4 v8, 0x1

    #@7ad
    .line 577
    const/16 v18, 0x1ea

    #@7af
    .line 578
    invoke-virtual {v14}, Ljavax/obex/ClientOperation;->abort()V

    #@7b2
    .line 580
    const-string v22, "BtOppObexClient"

    #@7b4
    new-instance v23, Ljava/lang/StringBuilder;

    #@7b6
    invoke-direct/range {v23 .. v23}, Ljava/lang/StringBuilder;-><init>()V

    #@7b9
    const-string v24, "SendFile interrupted when send out file "

    #@7bb
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7be
    move-result-object v23

    #@7bf
    move-object/from16 v0, p1

    #@7c1
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@7c3
    move-object/from16 v24, v0

    #@7c5
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c8
    move-result-object v23

    #@7c9
    const-string v24, " at "

    #@7cb
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7ce
    move-result-object v23

    #@7cf
    move-object/from16 v0, v23

    #@7d1
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d4
    move-result-object v23

    #@7d5
    const-string v24, " of "

    #@7d7
    invoke-virtual/range {v23 .. v24}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7da
    move-result-object v23

    #@7db
    move-object/from16 v0, p1

    #@7dd
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@7df
    move-wide/from16 v24, v0

    #@7e1
    invoke-virtual/range {v23 .. v25}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@7e4
    move-result-object v23

    #@7e5
    invoke-virtual/range {v23 .. v23}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e8
    move-result-object v23

    #@7e9
    invoke-static/range {v22 .. v23}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7ec
    .catchall {:try_start_735 .. :try_end_7ec} :catchall_4fa
    .catch Ljava/io/IOException; {:try_start_735 .. :try_end_7ec} :catch_287
    .catch Ljava/lang/NullPointerException; {:try_start_735 .. :try_end_7ec} :catch_35f
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_735 .. :try_end_7ec} :catch_442

    #@7ec
    goto/16 :goto_684

    #@7ee
    .line 625
    .end local v4           #a:Ljava/io/BufferedInputStream;
    .end local v5           #buffer:[B
    .end local v10           #okToProceed:Z
    .end local v11           #outputBufferSize:I
    .end local v13           #position:I
    .end local v15           #readLength:I
    .end local v19           #timestamp:J
    :catch_7ee
    move-exception v7

    #@7ef
    .line 626
    .restart local v7       #e:Ljava/io/IOException;
    const-string v23, "BtOppObexClient"

    #@7f1
    const-string v24, "Error when closing stream after send"

    #@7f3
    invoke-static/range {v23 .. v24}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7f6
    goto/16 :goto_59b

    #@7f8
    .line 613
    .end local v7           #e:Ljava/io/IOException;
    :cond_7f8
    const/16 v18, 0x1f1

    #@7fa
    goto/16 :goto_576

    #@7fc
    .line 625
    .restart local v7       #e:Ljava/io/IOException;
    :catch_7fc
    move-exception v7

    #@7fd
    .line 626
    const-string v22, "BtOppObexClient"

    #@7ff
    const-string v23, "Error when closing stream after send"

    #@801
    goto/16 :goto_412

    #@803
    .line 613
    :cond_803
    const/16 v18, 0x1f1

    #@805
    goto/16 :goto_30e

    #@807
    .local v7, e:Ljava/lang/NullPointerException;
    :cond_807
    const/16 v18, 0x1f1

    #@809
    goto/16 :goto_3e6

    #@80b
    .local v7, e:Ljava/lang/IndexOutOfBoundsException;
    :cond_80b
    const/16 v18, 0x1f1

    #@80d
    goto/16 :goto_4c9

    #@80f
    .end local v7           #e:Ljava/lang/IndexOutOfBoundsException;
    :cond_80f
    const/16 v18, 0x1f1

    #@811
    goto/16 :goto_6ff
.end method


# virtual methods
.method public addShare(Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V
    .registers 3
    .parameter "info"

    #@0
    .prologue
    .line 180
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@2
    .line 181
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->processShareInfo()Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@5
    move-result-object v0

    #@6
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@8
    .line 182
    const/4 v0, 0x0

    #@9
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->waitingForShare:Z

    #@b
    .line 183
    return-void
.end method

.method public interrupt()V
    .registers 5

    #@0
    .prologue
    .line 641
    invoke-super {p0}, Ljava/lang/Thread;->interrupt()V

    #@3
    .line 642
    monitor-enter p0

    #@4
    .line 643
    :try_start_4
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@6
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z
    invoke-static {v2}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$000(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_30

    #@c
    .line 645
    const-string v2, "BtOppObexClient"

    #@e
    const-string v3, "Interrupted when waitingForRemote"

    #@10
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_13
    .catchall {:try_start_4 .. :try_end_13} :catchall_3b

    #@13
    .line 648
    :try_start_13
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mTransport1:Ljavax/obex/ObexTransport;

    #@15
    invoke-interface {v2}, Ljavax/obex/ObexTransport;->close()V
    :try_end_18
    .catchall {:try_start_13 .. :try_end_18} :catchall_3b
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_18} :catch_32

    #@18
    .line 652
    :goto_18
    :try_start_18
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@1a
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;
    invoke-static {v2}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$200(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/os/Handler;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@21
    move-result-object v1

    #@22
    .line 653
    .local v1, msg:Landroid/os/Message;
    const/4 v2, 0x3

    #@23
    iput v2, v1, Landroid/os/Message;->what:I

    #@25
    .line 654
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@27
    if-eqz v2, :cond_2d

    #@29
    .line 655
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@2b
    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2d
    .line 657
    :cond_2d
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@30
    .line 659
    .end local v1           #msg:Landroid/os/Message;
    :cond_30
    monitor-exit p0

    #@31
    .line 660
    return-void

    #@32
    .line 649
    :catch_32
    move-exception v0

    #@33
    .line 650
    .local v0, e:Ljava/io/IOException;
    const-string v2, "BtOppObexClient"

    #@35
    const-string v3, "mTransport.close error"

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    goto :goto_18

    #@3b
    .line 659
    .end local v0           #e:Ljava/io/IOException;
    :catchall_3b
    move-exception v2

    #@3c
    monitor-exit p0
    :try_end_3d
    .catchall {:try_start_18 .. :try_end_3d} :catchall_3b

    #@3d
    throw v2
.end method

.method public run()V
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 187
    const/16 v2, 0xa

    #@3
    invoke-static {v2}, Landroid/os/Process;->setThreadPriority(I)V

    #@6
    .line 190
    const-string v2, "BtOppObexClient"

    #@8
    const-string v3, "acquire partial WakeLock"

    #@a
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 192
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->wakeLock:Landroid/os/PowerManager$WakeLock;

    #@f
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@12
    .line 195
    const-wide/16 v2, 0x64

    #@14
    :try_start_14
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_17
    .catch Ljava/lang/InterruptedException; {:try_start_14 .. :try_end_17} :catch_32

    #@17
    .line 202
    :goto_17
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@19
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mInterrupted:Z
    invoke-static {v2}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$100(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Z

    #@1c
    move-result v2

    #@1d
    if-nez v2, :cond_22

    #@1f
    .line 203
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->connect()V

    #@22
    .line 206
    :cond_22
    :goto_22
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@24
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mInterrupted:Z
    invoke-static {v2}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$100(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Z

    #@27
    move-result v2

    #@28
    if-nez v2, :cond_4f

    #@2a
    .line 207
    iget-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->waitingForShare:Z

    #@2c
    if-nez v2, :cond_40

    #@2e
    .line 208
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->doSend()V

    #@31
    goto :goto_22

    #@32
    .line 196
    :catch_32
    move-exception v0

    #@33
    .line 198
    .local v0, e1:Ljava/lang/InterruptedException;
    const-string v2, "BtOppObexClient"

    #@35
    const-string v3, "Client thread was interrupted (1), exiting"

    #@37
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 200
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@3c
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mInterrupted:Z
    invoke-static {v2, v4}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$102(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z

    #@3f
    goto :goto_17

    #@40
    .line 212
    .end local v0           #e1:Ljava/lang/InterruptedException;
    :cond_40
    :try_start_40
    const-string v2, "BtOppObexClient"

    #@42
    const-string v3, "Client thread waiting for next share, sleep for 500"

    #@44
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 214
    const-wide/16 v2, 0x1f4

    #@49
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4c
    .catch Ljava/lang/InterruptedException; {:try_start_40 .. :try_end_4c} :catch_4d

    #@4c
    goto :goto_22

    #@4d
    .line 215
    :catch_4d
    move-exception v2

    #@4e
    goto :goto_22

    #@4f
    .line 220
    :cond_4f
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->disconnect()V

    #@52
    .line 222
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->wakeLock:Landroid/os/PowerManager$WakeLock;

    #@54
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@57
    move-result v2

    #@58
    if-eqz v2, :cond_66

    #@5a
    .line 224
    const-string v2, "BtOppObexClient"

    #@5c
    const-string v3, "release partial WakeLock"

    #@5e
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 226
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->wakeLock:Landroid/os/PowerManager$WakeLock;

    #@63
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@66
    .line 228
    :cond_66
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@68
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;
    invoke-static {v2}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$200(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/os/Handler;

    #@6b
    move-result-object v2

    #@6c
    invoke-static {v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@6f
    move-result-object v1

    #@70
    .line 229
    .local v1, msg:Landroid/os/Message;
    iput v4, v1, Landroid/os/Message;->what:I

    #@72
    .line 230
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@74
    iput-object v2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@76
    .line 231
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@79
    .line 234
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@7b
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$300(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/content/Context;

    #@7e
    move-result-object v2

    #@7f
    const-string v3, "OUTBOUND_TRANSFER"

    #@81
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@83
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->send_file_cnt:I
    invoke-static {v4}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$400(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)I

    #@86
    move-result v4

    #@87
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@89
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->sent_file_cnt:I
    invoke-static {v5}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$500(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)I

    #@8c
    move-result v5

    #@8d
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;

    #@8f
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->result_status:I
    invoke-static {v6}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->access$600(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)I

    #@92
    move-result v6

    #@93
    invoke-static {v2, v3, v4, v5, v6}, Lcom/android/bluetooth/opp/Constants;->updateTransferStatusForMessage(Landroid/content/Context;Ljava/lang/String;III)V

    #@96
    .line 236
    return-void
.end method
