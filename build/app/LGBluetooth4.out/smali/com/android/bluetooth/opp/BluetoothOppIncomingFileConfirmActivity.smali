.class public Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "BluetoothOppIncomingFileConfirmActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final D:Z = true

.field private static final DISMISS_TIMEOUT_DIALOG:I = 0x0

.field private static final DISMISS_TIMEOUT_DIALOG_VALUE:I = 0x7d0

.field private static final PREFERENCE_USER_TIMEOUT:Ljava/lang/String; = "user_timeout"

.field private static final TAG:Ljava/lang/String; = "BluetoothIncomingFileConfirmActivity"

.field private static final V:Z = true


# instance fields
.field private mContentView:Landroid/widget/TextView;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

.field private mTimeout:Z

.field private final mTimeoutHandler:Landroid/os/Handler;

.field private mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

.field private mUpdateValues:Landroid/content/ContentValues;

.field private mUri:Landroid/net/Uri;

.field private mUserConfirmed:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    #@3
    .line 106
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@6
    .line 112
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@9
    .line 115
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity$1;

    #@b
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity$1;-><init>(Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;)V

    #@e
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    #@10
    .line 383
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity$2;

    #@12
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity$2;-><init>(Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;)V

    #@15
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeoutHandler:Landroid/os/Handler;

    #@17
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 85
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->onTimeout()V

    #@3
    return-void
.end method

.method private createView()Landroid/view/View;
    .registers 12

    #@0
    .prologue
    const/16 v10, 0x8

    #@2
    const/4 v9, 0x1

    #@3
    const/4 v8, 0x0

    #@4
    .line 200
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@7
    move-result-object v3

    #@8
    const v4, 0x7f030005

    #@b
    const/4 v5, 0x0

    #@c
    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@f
    move-result-object v2

    #@10
    .line 206
    .local v2, view:Landroid/view/View;
    const v3, 0x7f0b000a

    #@13
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@16
    move-result-object v3

    #@17
    check-cast v3, Landroid/widget/TextView;

    #@19
    iput-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mContentView:Landroid/widget/TextView;

    #@1b
    .line 208
    const v3, 0x7f070012

    #@1e
    const/4 v4, 0x3

    #@1f
    new-array v4, v4, [Ljava/lang/Object;

    #@21
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@23
    iget-object v5, v5, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@25
    aput-object v5, v4, v8

    #@27
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@29
    iget-object v5, v5, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@2b
    aput-object v5, v4, v9

    #@2d
    const/4 v5, 0x2

    #@2e
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@30
    iget-wide v6, v6, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@32
    invoke-static {p0, v6, v7}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@35
    move-result-object v6

    #@36
    aput-object v6, v4, v5

    #@38
    invoke-virtual {p0, v3, v4}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    .line 211
    .local v1, text:Ljava/lang/String;
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mContentView:Landroid/widget/TextView;

    #@3e
    const/4 v4, 0x5

    #@3f
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextDirection(I)V

    #@42
    .line 213
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mContentView:Landroid/widget/TextView;

    #@44
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@47
    .line 216
    const v3, 0x7f0b000b

    #@4a
    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@4d
    move-result-object v0

    #@4e
    check-cast v0, Landroid/widget/CheckBox;

    #@50
    .line 217
    .local v0, rememberChoice:Landroid/widget/CheckBox;
    invoke-virtual {v0, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    #@53
    .line 219
    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@56
    .line 222
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@58
    invoke-static {v3}, Lcom/lge/bluetooth/LGBluetoothServiceManager;->isBondedDevice(Landroid/bluetooth/BluetoothDevice;)Z

    #@5b
    move-result v3

    #@5c
    if-nez v3, :cond_69

    #@5e
    .line 223
    const-string v3, "BluetoothIncomingFileConfirmActivity"

    #@60
    const-string v4, "[BTUI] createView() : isBondedDevice(mRemoteDevice) is false."

    #@62
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@65
    .line 224
    invoke-virtual {v0, v10}, Landroid/widget/CheckBox;->setVisibility(I)V

    #@68
    .line 234
    :cond_68
    :goto_68
    return-object v2

    #@69
    .line 227
    :cond_69
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@6b
    if-eqz v3, :cond_68

    #@6d
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@6f
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getJustWorks()Z

    #@72
    move-result v3

    #@73
    if-ne v3, v9, :cond_68

    #@75
    .line 228
    const-string v3, "BluetoothIncomingFileConfirmActivity"

    #@77
    const-string v4, "[BTUI] createView() : mRemoteDevice.getJustWorks() is true."

    #@79
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7c
    .line 229
    invoke-virtual {v0, v10}, Landroid/widget/CheckBox;->setVisibility(I)V

    #@7f
    goto :goto_68
.end method

.method private isAutoReply()Z
    .registers 4

    #@0
    .prologue
    .line 401
    const v2, 0x7f0b000b

    #@3
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Landroid/widget/CheckBox;

    #@9
    .line 402
    .local v0, checkbox:Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    #@c
    move-result v1

    #@d
    .line 403
    .local v1, isChecked:Z
    return v1
.end method

.method private onNegative()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 298
    const-string v0, "BluetoothIncomingFileConfirmActivity"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "onPositive Authorization mUserConfirmed - value: "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUserConfirmed:Z

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 300
    new-instance v0, Landroid/content/ContentValues;

    #@1d
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@20
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@22
    .line 301
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@24
    const-string v1, "confirm"

    #@26
    const/4 v2, 0x3

    #@27
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2e
    .line 303
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@31
    move-result-object v0

    #@32
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUri:Landroid/net/Uri;

    #@34
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@36
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@39
    .line 304
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->finish()V

    #@3c
    .line 305
    return-void
.end method

.method private onPositive()V
    .registers 9

    #@0
    .prologue
    const v7, 0x7f070071

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v5, 0x0

    #@5
    const/4 v4, 0x0

    #@6
    .line 255
    const-string v1, "BluetoothIncomingFileConfirmActivity"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "onPositive Authorization mUserConfirmed - value: "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    iget-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUserConfirmed:Z

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 256
    iget-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@22
    if-nez v1, :cond_90

    #@24
    .line 258
    invoke-static {}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->isInternalStorageFull()Z

    #@27
    move-result v1

    #@28
    if-eqz v1, :cond_5b

    #@2a
    .line 259
    const-string v1, "BluetoothIncomingFileConfirmActivity"

    #@2c
    const-string v2, "the Internal storage is full"

    #@2e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 260
    invoke-static {}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->isNoExternalStorage()Z

    #@34
    move-result v1

    #@35
    if-nez v1, :cond_3d

    #@37
    invoke-static {}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->isExternalStorageFull()Z

    #@3a
    move-result v1

    #@3b
    if-eqz v1, :cond_94

    #@3d
    .line 261
    :cond_3d
    const-string v1, "BluetoothIncomingFileConfirmActivity"

    #@3f
    const-string v2, "the External storage is full"

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 262
    new-instance v0, Landroid/content/Intent;

    #@46
    const-class v1, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;

    #@48
    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@4b
    .line 263
    .local v0, intent:Landroid/content/Intent;
    const/high16 v1, 0x1000

    #@4d
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@50
    .line 264
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUri:Landroid/net/Uri;

    #@52
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@55
    .line 265
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->startActivity(Landroid/content/Intent;)V

    #@58
    .line 266
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->finish()V

    #@5b
    .line 283
    .end local v0           #intent:Landroid/content/Intent;
    :cond_5b
    :goto_5b
    new-instance v1, Landroid/content/ContentValues;

    #@5d
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@60
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@62
    .line 284
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@64
    const-string v2, "confirm"

    #@66
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6d
    .line 286
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@70
    move-result-object v1

    #@71
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUri:Landroid/net/Uri;

    #@73
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@75
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@78
    .line 287
    invoke-virtual {p0, v7}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getString(I)Ljava/lang/String;

    #@7b
    move-result-object v1

    #@7c
    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@7f
    move-result-object v1

    #@80
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    #@83
    .line 289
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@85
    if-eqz v1, :cond_90

    #@87
    .line 290
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@89
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->isAutoReply()Z

    #@8c
    move-result v2

    #@8d
    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothDevice;->setTrust(Z)Z

    #@90
    .line 295
    :cond_90
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->finish()V

    #@93
    .line 296
    return-void

    #@94
    .line 269
    :cond_94
    new-instance v1, Landroid/content/ContentValues;

    #@96
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@99
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@9b
    .line 270
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@9d
    const-string v2, "confirm"

    #@9f
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a2
    move-result-object v3

    #@a3
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@a6
    .line 272
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@a9
    move-result-object v1

    #@aa
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUri:Landroid/net/Uri;

    #@ac
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@ae
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@b1
    .line 273
    invoke-virtual {p0, v7}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getString(I)Ljava/lang/String;

    #@b4
    move-result-object v1

    #@b5
    invoke-static {p0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@b8
    move-result-object v1

    #@b9
    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    #@bc
    .line 275
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@be
    if-eqz v1, :cond_5b

    #@c0
    .line 276
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@c2
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->isAutoReply()Z

    #@c5
    move-result v2

    #@c6
    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothDevice;->setTrust(Z)Z

    #@c9
    goto :goto_5b
.end method

.method private onTimeout()V
    .registers 6

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 372
    iput-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@4
    .line 373
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mContentView:Landroid/widget/TextView;

    #@6
    const v1, 0x7f070016

    #@9
    new-array v2, v2, [Ljava/lang/Object;

    #@b
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@d
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@f
    aput-object v3, v2, v4

    #@11
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@18
    .line 375
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@1a
    const/4 v1, -0x2

    #@1b
    invoke-virtual {v0, v1}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@1e
    move-result-object v0

    #@1f
    const/16 v1, 0x8

    #@21
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    #@24
    .line 376
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@26
    const/4 v1, -0x1

    #@27
    invoke-virtual {v0, v1}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@2a
    move-result-object v0

    #@2b
    const v1, 0x7f070015

    #@2e
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getString(I)Ljava/lang/String;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@35
    .line 379
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeoutHandler:Landroid/os/Handler;

    #@37
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeoutHandler:Landroid/os/Handler;

    #@39
    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@3c
    move-result-object v1

    #@3d
    const-wide/16 v2, 0x7d0

    #@3f
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@42
    .line 381
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 5
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 239
    packed-switch p2, :pswitch_data_16

    #@4
    .line 253
    :cond_4
    :goto_4
    return-void

    #@5
    .line 241
    :pswitch_5
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@7
    if-nez v0, :cond_4

    #@9
    .line 242
    iput-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUserConfirmed:Z

    #@b
    .line 243
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->onPositive()V

    #@e
    goto :goto_4

    #@f
    .line 247
    :pswitch_f
    iput-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUserConfirmed:Z

    #@11
    .line 248
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->onNegative()V

    #@14
    goto :goto_4

    #@15
    .line 239
    nop

    #@16
    :pswitch_data_16
    .packed-switch -0x2
        :pswitch_f
        :pswitch_5
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .registers 2
    .parameter "arg0"

    #@0
    .prologue
    .line 410
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 141
    const-string v3, "BluetoothIncomingFileConfirmActivity"

    #@2
    new-instance v4, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "onCreate(): action = "

    #@9
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v4

    #@d
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getIntent()Landroid/content/Intent;

    #@10
    move-result-object v5

    #@11
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 143
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    #@23
    .line 146
    const/4 v3, 0x0

    #@24
    iput-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUserConfirmed:Z

    #@26
    .line 148
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getIntent()Landroid/content/Intent;

    #@29
    move-result-object v1

    #@2a
    .line 149
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@2d
    move-result-object v3

    #@2e
    iput-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUri:Landroid/net/Uri;

    #@30
    .line 151
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    #@32
    invoke-virtual {v1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@35
    move-result-object v3

    #@36
    check-cast v3, Landroid/bluetooth/BluetoothDevice;

    #@38
    iput-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@3a
    .line 153
    new-instance v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@3c
    invoke-direct {v3}, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;-><init>()V

    #@3f
    iput-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@41
    .line 154
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUri:Landroid/net/Uri;

    #@43
    invoke-static {p0, v3}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->queryRecord(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@46
    move-result-object v3

    #@47
    iput-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@49
    .line 155
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@4b
    if-nez v3, :cond_58

    #@4d
    .line 157
    const-string v3, "BluetoothIncomingFileConfirmActivity"

    #@4f
    const-string v4, "Error: Can not get data from db"

    #@51
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 159
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->finish()V

    #@57
    .line 196
    :goto_57
    return-void

    #@58
    .line 164
    :cond_58
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    #@5a
    .line 168
    .local v2, p:Lcom/android/internal/app/AlertController$AlertParams;
    const v3, 0x7f070011

    #@5d
    invoke-virtual {p0, v3}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getString(I)Ljava/lang/String;

    #@60
    move-result-object v3

    #@61
    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    #@63
    .line 169
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->createView()Landroid/view/View;

    #@66
    move-result-object v3

    #@67
    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    #@69
    .line 170
    const v3, 0x7f070014

    #@6c
    invoke-virtual {p0, v3}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getString(I)Ljava/lang/String;

    #@6f
    move-result-object v3

    #@70
    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@72
    .line 171
    iput-object p0, v2, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@74
    .line 172
    const v3, 0x7f070013

    #@77
    invoke-virtual {p0, v3}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getString(I)Ljava/lang/String;

    #@7a
    move-result-object v3

    #@7b
    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@7d
    .line 173
    iput-object p0, v2, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@7f
    .line 174
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->setupAlert()V

    #@82
    .line 176
    const-string v3, "BluetoothIncomingFileConfirmActivity"

    #@84
    new-instance v4, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v5, "mTimeout: "

    #@8b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v4

    #@8f
    iget-boolean v5, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@91
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v4

    #@99
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    .line 178
    iget-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@9e
    if-eqz v3, :cond_a3

    #@a0
    .line 179
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->onTimeout()V

    #@a3
    .line 183
    :cond_a3
    const-string v3, "BluetoothIncomingFileConfirmActivity"

    #@a5
    new-instance v4, Ljava/lang/StringBuilder;

    #@a7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@aa
    const-string v5, "BluetoothIncomingFileConfirmActivity: Got uri:"

    #@ac
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v4

    #@b0
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUri:Landroid/net/Uri;

    #@b2
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v4

    #@b6
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b9
    move-result-object v4

    #@ba
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@bd
    .line 191
    new-instance v0, Landroid/content/IntentFilter;

    #@bf
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    #@c2
    .line 192
    .local v0, filter:Landroid/content/IntentFilter;
    const-string v3, "android.btopp.intent.action.USER_CONFIRMATION_TIMEOUT"

    #@c4
    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@c7
    .line 193
    const-string v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@c9
    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@cc
    .line 194
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    #@ce
    invoke-virtual {p0, v3, v0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@d1
    goto :goto_57
.end method

.method protected onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 333
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUserConfirmed:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 334
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->onNegative()V

    #@7
    .line 337
    :cond_7
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onDestroy()V

    #@a
    .line 338
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    #@c
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 339
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 310
    const/4 v1, 0x4

    #@3
    if-ne p1, v1, :cond_3c

    #@5
    .line 312
    const-string v1, "BluetoothIncomingFileConfirmActivity"

    #@7
    const-string v2, "onKeyDown() called; Key: back key"

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 314
    new-instance v1, Landroid/content/ContentValues;

    #@e
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@11
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@13
    .line 319
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@15
    const-string v2, "confirm"

    #@17
    const/4 v3, 0x3

    #@18
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1f
    .line 321
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@22
    move-result-object v1

    #@23
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUri:Landroid/net/Uri;

    #@25
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@27
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@2a
    .line 323
    const v1, 0x7f070040

    #@2d
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->getString(I)Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@38
    .line 324
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->finish()V

    #@3b
    .line 325
    const/4 v0, 0x1

    #@3c
    .line 327
    :cond_3c
    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 352
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 353
    const-string v0, "user_timeout"

    #@5
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@b
    .line 355
    const-string v0, "BluetoothIncomingFileConfirmActivity"

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "onRestoreInstanceState() mTimeout: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    iget-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 357
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@27
    if-eqz v0, :cond_2c

    #@29
    .line 358
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->onTimeout()V

    #@2c
    .line 360
    :cond_2c
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "outState"

    #@0
    .prologue
    .line 364
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 366
    const-string v0, "BluetoothIncomingFileConfirmActivity"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "onSaveInstanceState() mTimeout: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    iget-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@12
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v1

    #@1a
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 368
    const-string v0, "user_timeout"

    #@1f
    iget-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->mTimeout:Z

    #@21
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@24
    .line 369
    return-void
.end method

.method protected onUserLeaveHint()V
    .registers 3

    #@0
    .prologue
    .line 344
    const-string v0, "BluetoothIncomingFileConfirmActivity"

    #@2
    const-string v1, "User pressed Home key, Destroying the Opp Authorize process"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 345
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;->onNegative()V

    #@a
    .line 346
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onUserLeaveHint()V

    #@d
    .line 347
    return-void
.end method
