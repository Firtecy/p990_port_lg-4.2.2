.class Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothOppBtEnableActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 144
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 9
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 147
    const-string v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v4

    #@6
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v3

    #@a
    if-eqz v3, :cond_65

    #@c
    .line 148
    const/16 v3, 0xc

    #@e
    const-string v4, "android.bluetooth.adapter.extra.STATE"

    #@10
    const/high16 v5, -0x8000

    #@12
    invoke-virtual {p2, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@15
    move-result v4

    #@16
    if-ne v3, v4, :cond_65

    #@18
    .line 149
    invoke-static {p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@1b
    move-result-object v1

    #@1c
    .line 150
    .local v1, mOppManager:Lcom/android/bluetooth/opp/BluetoothOppManager;
    iget-boolean v3, v1, Lcom/android/bluetooth/opp/BluetoothOppManager;->mSendingFlag:Z

    #@1e
    if-nez v3, :cond_65

    #@20
    .line 152
    new-instance v0, Landroid/content/Intent;

    #@22
    const-string v3, "android.bluetooth.devicepicker.action.LAUNCH"

    #@24
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@27
    .line 153
    .local v0, in:Landroid/content/Intent;
    const/high16 v3, 0x80

    #@29
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@2c
    .line 154
    const-string v3, "android.bluetooth.devicepicker.extra.NEED_AUTH"

    #@2e
    const/4 v4, 0x0

    #@2f
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@32
    .line 155
    const-string v3, "android.bluetooth.devicepicker.extra.FILTER_TYPE"

    #@34
    const/4 v4, 0x2

    #@35
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@38
    .line 157
    const-string v3, "android.bluetooth.devicepicker.extra.LAUNCH_PACKAGE"

    #@3a
    const-string v4, "com.android.bluetooth"

    #@3c
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3f
    .line 158
    const-string v3, "android.bluetooth.devicepicker.extra.DEVICE_PICKER_LAUNCH_CLASS"

    #@41
    const-class v4, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@43
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4a
    .line 159
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@4d
    .line 162
    new-instance v2, Landroid/content/Intent;

    #@4f
    const-string v3, "com.lge.bluetooth.opp.sendservice"

    #@51
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@54
    .line 163
    .local v2, serviceIntent:Landroid/content/Intent;
    const-string v3, "intent"

    #@56
    iget-object v4, v1, Lcom/android/bluetooth/opp/BluetoothOppManager;->mSendTypeIntent:Landroid/content/Intent;

    #@58
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@5b
    .line 164
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;

    #@5d
    invoke-virtual {v3, v2}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@60
    .line 167
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity$1;->this$0:Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;

    #@62
    invoke-virtual {v3}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->finish()V

    #@65
    .line 171
    .end local v0           #in:Landroid/content/Intent;
    .end local v1           #mOppManager:Lcom/android/bluetooth/opp/BluetoothOppManager;
    .end local v2           #serviceIntent:Landroid/content/Intent;
    :cond_65
    return-void
.end method
