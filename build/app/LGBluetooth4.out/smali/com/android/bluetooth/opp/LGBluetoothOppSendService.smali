.class public Lcom/android/bluetooth/opp/LGBluetoothOppSendService;
.super Landroid/app/Service;
.source "LGBluetoothOppSendService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "LGBluetoothOppSendService"

.field private static mContext:Landroid/content/Context;


# instance fields
.field private mIntent:Landroid/content/Intent;

.field private mThread:Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/opp/LGBluetoothOppSendService;)Landroid/content/Intent;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 32
    iget-object v0, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->mIntent:Landroid/content/Intent;

    #@2
    return-object v0
.end method

.method static synthetic access$100()Landroid/content/Context;
    .registers 1

    #@0
    .prologue
    .line 32
    sget-object v0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/bluetooth/opp/LGBluetoothOppSendService;Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/net/Uri;
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->creatFileForSharedContent(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/net/Uri;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private creatFileForSharedContent(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/net/Uri;
    .registers 13
    .parameter "context"
    .parameter "shareContent"

    #@0
    .prologue
    .line 124
    if-nez p2, :cond_4

    #@2
    .line 125
    const/4 v4, 0x0

    #@3
    .line 169
    :cond_3
    :goto_3
    return-object v4

    #@4
    .line 128
    :cond_4
    const/4 v4, 0x0

    #@5
    .line 129
    .local v4, fileUri:Landroid/net/Uri;
    const/4 v5, 0x0

    #@6
    .line 131
    .local v5, outStream:Ljava/io/FileOutputStream;
    :try_start_6
    new-instance v7, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const v8, 0x7f070061

    #@e
    invoke-virtual {p0, v8}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->getString(I)Ljava/lang/String;

    #@11
    move-result-object v8

    #@12
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v7

    #@16
    const-string v8, ".html"

    #@18
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v7

    #@1c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    .line 132
    .local v3, fileName:Ljava/lang/String;
    invoke-virtual {p1, v3}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    #@23
    .line 134
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    .line 136
    .local v6, uri:Ljava/lang/String;
    invoke-static {v6}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->makeHtmlCodeToString(Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    .line 143
    .local v1, content:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@2e
    move-result-object v0

    #@2f
    .line 145
    .local v0, byteBuff:[B
    const/4 v7, 0x0

    #@30
    invoke-virtual {p1, v3, v7}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    #@33
    move-result-object v5

    #@34
    .line 146
    if-eqz v5, :cond_66

    #@36
    .line 147
    const/4 v7, 0x0

    #@37
    array-length v8, v0

    #@38
    invoke-virtual {v5, v0, v7, v8}, Ljava/io/FileOutputStream;->write([BII)V

    #@3b
    .line 148
    new-instance v7, Ljava/io/File;

    #@3d
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    #@40
    move-result-object v8

    #@41
    invoke-direct {v7, v8, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@44
    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@47
    move-result-object v4

    #@48
    .line 149
    if-eqz v4, :cond_66

    #@4a
    .line 150
    const-string v7, "LGBluetoothOppSendService"

    #@4c
    new-instance v8, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v9, "Created one file for shared content: "

    #@53
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v8

    #@57
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@5a
    move-result-object v9

    #@5b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v8

    #@63
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_66
    .catchall {:try_start_6 .. :try_end_66} :catchall_e6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_66} :catch_71
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_66} :catch_9a
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_66} :catch_c0

    #@66
    .line 162
    :cond_66
    if-eqz v5, :cond_3

    #@68
    .line 163
    :try_start_68
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6b
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_6b} :catch_6c

    #@6b
    goto :goto_3

    #@6c
    .line 165
    :catch_6c
    move-exception v2

    #@6d
    .line 166
    .end local v0           #byteBuff:[B
    .end local v1           #content:Ljava/lang/String;
    .end local v3           #fileName:Ljava/lang/String;
    .end local v6           #uri:Ljava/lang/String;
    .local v2, e:Ljava/io/IOException;
    :goto_6d
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@70
    goto :goto_3

    #@71
    .line 153
    .end local v2           #e:Ljava/io/IOException;
    :catch_71
    move-exception v2

    #@72
    .line 154
    .local v2, e:Ljava/io/FileNotFoundException;
    :try_start_72
    const-string v7, "LGBluetoothOppSendService"

    #@74
    new-instance v8, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v9, "FileNotFoundException: "

    #@7b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v8

    #@7f
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    #@82
    move-result-object v9

    #@83
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v8

    #@87
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v8

    #@8b
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 155
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_91
    .catchall {:try_start_72 .. :try_end_91} :catchall_e6

    #@91
    .line 162
    if-eqz v5, :cond_3

    #@93
    .line 163
    :try_start_93
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_96
    .catch Ljava/io/IOException; {:try_start_93 .. :try_end_96} :catch_98

    #@96
    goto/16 :goto_3

    #@98
    .line 165
    :catch_98
    move-exception v2

    #@99
    goto :goto_6d

    #@9a
    .line 156
    .end local v2           #e:Ljava/io/FileNotFoundException;
    :catch_9a
    move-exception v2

    #@9b
    .line 157
    .local v2, e:Ljava/io/IOException;
    :try_start_9b
    const-string v7, "LGBluetoothOppSendService"

    #@9d
    new-instance v8, Ljava/lang/StringBuilder;

    #@9f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a2
    const-string v9, "IOException: "

    #@a4
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v8

    #@a8
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@ab
    move-result-object v9

    #@ac
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@af
    move-result-object v8

    #@b0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v8

    #@b4
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b7
    .catchall {:try_start_9b .. :try_end_b7} :catchall_e6

    #@b7
    .line 162
    if-eqz v5, :cond_3

    #@b9
    .line 163
    :try_start_b9
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_bc
    .catch Ljava/io/IOException; {:try_start_b9 .. :try_end_bc} :catch_be

    #@bc
    goto/16 :goto_3

    #@be
    .line 165
    :catch_be
    move-exception v2

    #@bf
    goto :goto_6d

    #@c0
    .line 158
    .end local v2           #e:Ljava/io/IOException;
    :catch_c0
    move-exception v2

    #@c1
    .line 159
    .local v2, e:Ljava/lang/Exception;
    :try_start_c1
    const-string v7, "LGBluetoothOppSendService"

    #@c3
    new-instance v8, Ljava/lang/StringBuilder;

    #@c5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@c8
    const-string v9, "Exception: "

    #@ca
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cd
    move-result-object v8

    #@ce
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@d1
    move-result-object v9

    #@d2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v8

    #@d6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v8

    #@da
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_dd
    .catchall {:try_start_c1 .. :try_end_dd} :catchall_e6

    #@dd
    .line 162
    if-eqz v5, :cond_3

    #@df
    .line 163
    :try_start_df
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_e2
    .catch Ljava/io/IOException; {:try_start_df .. :try_end_e2} :catch_e4

    #@e2
    goto/16 :goto_3

    #@e4
    .line 165
    :catch_e4
    move-exception v2

    #@e5
    goto :goto_6d

    #@e6
    .line 161
    .end local v2           #e:Ljava/lang/Exception;
    :catchall_e6
    move-exception v7

    #@e7
    .line 162
    if-eqz v5, :cond_ec

    #@e9
    .line 163
    :try_start_e9
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_ec
    .catch Ljava/io/IOException; {:try_start_e9 .. :try_end_ec} :catch_ed

    #@ec
    .line 161
    :cond_ec
    :goto_ec
    throw v7

    #@ed
    .line 165
    :catch_ed
    move-exception v2

    #@ee
    .line 166
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@f1
    goto :goto_ec
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 49
    const-string v0, "LGBluetoothOppSendService"

    #@2
    const-string v1, "[BTUI] onBind()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 50
    const/4 v0, 0x0

    #@8
    return-object v0
.end method

.method public onCreate()V
    .registers 1

    #@0
    .prologue
    .line 43
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@3
    .line 44
    sput-object p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->mContext:Landroid/content/Context;

    #@5
    .line 45
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 118
    const-string v0, "LGBluetoothOppSendService"

    #@2
    const-string v1, "[BTUI] onDestroy()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 119
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@a
    .line 120
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->mThread:Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;

    #@d
    .line 121
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .registers 5
    .parameter "intent"
    .parameter "startId"

    #@0
    .prologue
    .line 55
    const-string v0, "LGBluetoothOppSendService"

    #@2
    const-string v1, "[BTUI] onStart()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 56
    if-eqz p1, :cond_1b

    #@9
    .line 57
    iput-object p1, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->mIntent:Landroid/content/Intent;

    #@b
    .line 58
    iget-object v0, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->mThread:Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;

    #@d
    if-nez v0, :cond_1b

    #@f
    .line 59
    new-instance v0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;

    #@11
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;-><init>(Lcom/android/bluetooth/opp/LGBluetoothOppSendService;)V

    #@14
    iput-object v0, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->mThread:Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;

    #@16
    .line 60
    iget-object v0, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->mThread:Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;

    #@18
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;->start()V

    #@1b
    .line 63
    :cond_1b
    return-void
.end method
