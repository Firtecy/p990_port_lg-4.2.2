.class public Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;
.super Landroid/app/Activity;
.source "BluetoothOppTransferHistory.java"

# interfaces
.implements Landroid/view/View$OnCreateContextMenuListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "BluetoothOppTransferHistory"

.field private static final V:Z = true


# instance fields
.field private mContextMenuPosition:I

.field private mIdColumnId:I

.field private mListView:Landroid/widget/ListView;

.field private mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;

.field private mShowAllIncoming:Z

.field private mTransferAdapter:Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;

.field private mTransferCursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 76
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 76
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->clearAllDownloads()V

    #@3
    return-void
.end method

.method private blockClearList()Z
    .registers 5

    #@0
    .prologue
    .line 439
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@3
    move-result-object v0

    #@4
    .line 440
    .local v0, adapter:Landroid/bluetooth/BluetoothAdapter;
    sget v1, Lcom/android/bluetooth/opp/BluetoothOppService;->mState:I

    #@6
    const/4 v2, 0x2

    #@7
    if-ne v1, v2, :cond_37

    #@9
    .line 442
    const-string v1, "BluetoothOppTransferHistory"

    #@b
    const-string v2, "OPP is connected."

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 444
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@12
    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@15
    const v2, 0x7f070060

    #@18
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@1b
    move-result-object v1

    #@1c
    const v2, 0x7f070047

    #@1f
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@22
    move-result-object v1

    #@23
    const v2, 0x104000a

    #@26
    const/4 v3, 0x0

    #@27
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@2a
    move-result-object v1

    #@2b
    const v2, 0x202026e

    #@2e
    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@31
    move-result-object v1

    #@32
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    #@35
    .line 450
    const/4 v1, 0x1

    #@36
    .line 452
    :goto_36
    return v1

    #@37
    :cond_37
    const/4 v1, 0x0

    #@38
    goto :goto_36
.end method

.method private clearAllDownloads()V
    .registers 5

    #@0
    .prologue
    .line 361
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->blockClearList()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_7

    #@6
    .line 382
    :cond_6
    :goto_6
    return-void

    #@7
    .line 366
    :cond_7
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@9
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    #@c
    move-result v2

    #@d
    if-eqz v2, :cond_6

    #@f
    .line 367
    :goto_f
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@11
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_4d

    #@17
    .line 368
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@19
    iget v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mIdColumnId:I

    #@1b
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    #@1e
    move-result v1

    #@1f
    .line 369
    .local v1, sessionId:I
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    const-string v3, "/"

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@3b
    move-result-object v0

    #@3c
    .line 371
    .local v0, contentUri:Landroid/net/Uri;
    iget-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mShowAllIncoming:Z

    #@3e
    if-eqz v2, :cond_49

    #@40
    .line 372
    invoke-static {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToRemoved(Landroid/content/Context;Landroid/net/Uri;)V

    #@43
    .line 378
    :goto_43
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@45
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    #@48
    goto :goto_f

    #@49
    .line 375
    :cond_49
    invoke-static {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToHidden(Landroid/content/Context;Landroid/net/Uri;)V

    #@4c
    goto :goto_43

    #@4d
    .line 380
    .end local v0           #contentUri:Landroid/net/Uri;
    .end local v1           #sessionId:I
    :cond_4d
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->updateNotificationWhenBtDisabled()V

    #@50
    goto :goto_6
.end method

.method private getClearableCount()I
    .registers 8

    #@0
    .prologue
    .line 321
    const/4 v0, 0x0

    #@1
    .line 325
    .local v0, count:I
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@3
    invoke-interface {v5}, Landroid/database/Cursor;->requery()Z

    #@6
    .line 327
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@8
    invoke-interface {v5}, Landroid/database/Cursor;->moveToFirst()Z

    #@b
    move-result v5

    #@c
    if-eqz v5, :cond_3c

    #@e
    .line 328
    :goto_e
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@10
    invoke-interface {v5}, Landroid/database/Cursor;->isAfterLast()Z

    #@13
    move-result v5

    #@14
    if-nez v5, :cond_3c

    #@16
    .line 329
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@18
    const-string v6, "status"

    #@1a
    invoke-interface {v5, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@1d
    move-result v4

    #@1e
    .line 340
    .local v4, statusColumnId:I
    :try_start_1e
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@20
    invoke-interface {v5, v4}, Landroid/database/Cursor;->getInt(I)I

    #@23
    move-result v3

    #@24
    .line 341
    .local v3, status:I
    invoke-static {v3}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z
    :try_end_27
    .catch Landroid/database/StaleDataException; {:try_start_1e .. :try_end_27} :catch_32

    #@27
    move-result v5

    #@28
    if-eqz v5, :cond_2c

    #@2a
    .line 342
    add-int/lit8 v0, v0, 0x1

    #@2c
    .line 350
    :cond_2c
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@2e
    invoke-interface {v5}, Landroid/database/Cursor;->moveToNext()Z

    #@31
    goto :goto_e

    #@32
    .line 344
    .end local v3           #status:I
    :catch_32
    move-exception v2

    #@33
    .line 345
    .local v2, e:Landroid/database/StaleDataException;
    const-string v5, "BluetoothOppTransferHistory"

    #@35
    const-string v6, "Failed to get BluetoothShare.STATUS colum from mTransferCursor since mTransferCursor was already deactivated in prior"

    #@37
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    move v1, v0

    #@3b
    .line 353
    .end local v0           #count:I
    .end local v2           #e:Landroid/database/StaleDataException;
    .end local v4           #statusColumnId:I
    .local v1, count:I
    :goto_3b
    return v1

    #@3c
    .end local v1           #count:I
    .restart local v0       #count:I
    :cond_3c
    move v1, v0

    #@3d
    .end local v0           #count:I
    .restart local v1       #count:I
    goto :goto_3b
.end method

.method private openCompleteTransfer()V
    .registers 8

    #@0
    .prologue
    .line 402
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@2
    iget v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mIdColumnId:I

    #@4
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    #@7
    move-result v2

    #@8
    .line 403
    .local v2, sessionId:I
    new-instance v4, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    sget-object v5, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v4

    #@13
    const-string v5, "/"

    #@15
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v4

    #@21
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@24
    move-result-object v0

    #@25
    .line 404
    .local v0, contentUri:Landroid/net/Uri;
    invoke-static {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->queryRecord(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@28
    move-result-object v3

    #@29
    .line 405
    .local v3, transInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    if-nez v3, :cond_33

    #@2b
    .line 406
    const-string v4, "BluetoothOppTransferHistory"

    #@2d
    const-string v5, "Error: Can not get data from db"

    #@2f
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 421
    :goto_32
    return-void

    #@33
    .line 409
    :cond_33
    iget v4, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDirection:I

    #@35
    const/4 v5, 0x1

    #@36
    if-ne v4, v5, :cond_4d

    #@38
    iget v4, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@3a
    invoke-static {v4}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusSuccess(I)Z

    #@3d
    move-result v4

    #@3e
    if-eqz v4, :cond_4d

    #@40
    .line 412
    invoke-static {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToHidden(Landroid/content/Context;Landroid/net/Uri;)V

    #@43
    .line 413
    iget-object v4, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@45
    iget-object v5, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@47
    iget-object v6, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTimeStamp:Ljava/lang/Long;

    #@49
    invoke-static {p0, v4, v5, v6, v0}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->openReceivedFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Landroid/net/Uri;)V

    #@4c
    goto :goto_32

    #@4d
    .line 416
    :cond_4d
    new-instance v1, Landroid/content/Intent;

    #@4f
    const-class v4, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;

    #@51
    invoke-direct {v1, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@54
    .line 417
    .local v1, in:Landroid/content/Intent;
    const/high16 v4, 0x1000

    #@56
    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@59
    .line 418
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@5c
    .line 419
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->startActivity(Landroid/content/Intent;)V

    #@5f
    goto :goto_32
.end method

.method private promptClearList()V
    .registers 4

    #@0
    .prologue
    .line 300
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@2
    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@5
    const v1, 0x7f070060

    #@8
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@b
    move-result-object v0

    #@c
    const v1, 0x7f07006c

    #@f
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@12
    move-result-object v0

    #@13
    const v1, 0x7f070067

    #@16
    new-instance v2, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory$1;

    #@18
    invoke-direct {v2, p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory$1;-><init>(Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;)V

    #@1b
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@1e
    move-result-object v0

    #@1f
    const v1, 0x7f070068

    #@22
    const/4 v2, 0x0

    #@23
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@26
    move-result-object v0

    #@27
    const v1, 0x202026e

    #@2a
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@2d
    move-result-object v0

    #@2e
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    #@31
    .line 315
    return-void
.end method

.method private updateNotificationWhenBtDisabled()V
    .registers 4

    #@0
    .prologue
    .line 428
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@3
    move-result-object v0

    #@4
    .line 429
    .local v0, adapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_16

    #@a
    .line 431
    const-string v1, "BluetoothOppTransferHistory"

    #@c
    const-string v2, "Bluetooth is not enabled, update notification manually."

    #@e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 433
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@13
    invoke-virtual {v1}, Lcom/android/bluetooth/opp/BluetoothOppNotification;->updateNotification()V

    #@16
    .line 435
    :cond_16
    return-void
.end method

.method private updateOptionsMenu()V
    .registers 3

    #@0
    .prologue
    .line 194
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mListView:Landroid/widget/ListView;

    #@2
    if-eqz v0, :cond_10

    #@4
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mListView:Landroid/widget/ListView;

    #@6
    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    #@9
    move-result v0

    #@a
    const/4 v1, 0x1

    #@b
    if-gt v0, v1, :cond_10

    #@d
    .line 195
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->invalidateOptionsMenu()V

    #@10
    .line 197
    :cond_10
    return-void
.end method


# virtual methods
.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter "parent"
    .parameter "child"

    #@0
    .prologue
    .line 185
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->updateOptionsMenu()V

    #@3
    .line 186
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .registers 3
    .parameter "parent"
    .parameter "child"

    #@0
    .prologue
    .line 190
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->updateOptionsMenu()V

    #@3
    .line 191
    return-void
.end method

.method public onContextItemSelected(Landroid/view/MenuItem;)Z
    .registers 7
    .parameter "item"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 247
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@3
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mContextMenuPosition:I

    #@5
    invoke-interface {v3, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@8
    .line 248
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    #@b
    move-result v3

    #@c
    packed-switch v3, :pswitch_data_4c

    #@f
    .line 270
    :goto_f
    :pswitch_f
    const/4 v2, 0x0

    #@10
    :goto_10
    return v2

    #@11
    .line 250
    :pswitch_11
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->openCompleteTransfer()V

    #@14
    .line 251
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->updateNotificationWhenBtDisabled()V

    #@17
    goto :goto_10

    #@18
    .line 255
    :pswitch_18
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@1a
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mIdColumnId:I

    #@1c
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getInt(I)I

    #@1f
    move-result v1

    #@20
    .line 256
    .local v1, sessionId:I
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    sget-object v4, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    const-string v4, "/"

    #@2d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@3c
    move-result-object v0

    #@3d
    .line 258
    .local v0, contentUri:Landroid/net/Uri;
    iget-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mShowAllIncoming:Z

    #@3f
    if-eqz v3, :cond_45

    #@41
    .line 259
    invoke-static {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToRemoved(Landroid/content/Context;Landroid/net/Uri;)V

    #@44
    goto :goto_f

    #@45
    .line 262
    :cond_45
    invoke-static {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToHidden(Landroid/content/Context;Landroid/net/Uri;)V

    #@48
    .line 263
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->updateNotificationWhenBtDisabled()V

    #@4b
    goto :goto_10

    #@4c
    .line 248
    :pswitch_data_4c
    .packed-switch 0x7f0b0035
        :pswitch_11
        :pswitch_f
        :pswitch_18
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .registers 13
    .parameter "icicle"

    #@0
    .prologue
    const/4 v10, 0x2

    #@1
    const/4 v9, 0x1

    #@2
    const/4 v8, 0x0

    #@3
    .line 104
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@6
    .line 105
    const v6, 0x7f030002

    #@9
    invoke-virtual {p0, v6}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->setContentView(I)V

    #@c
    .line 106
    const v6, 0x7f0b0007

    #@f
    invoke-virtual {p0, v6}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->findViewById(I)Landroid/view/View;

    #@12
    move-result-object v6

    #@13
    check-cast v6, Landroid/widget/ListView;

    #@15
    iput-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mListView:Landroid/widget/ListView;

    #@17
    .line 107
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mListView:Landroid/widget/ListView;

    #@19
    const v7, 0x7f0b0008

    #@1c
    invoke-virtual {p0, v7}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->findViewById(I)Landroid/view/View;

    #@1f
    move-result-object v7

    #@20
    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    #@23
    .line 109
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->getIntent()Landroid/content/Intent;

    #@26
    move-result-object v6

    #@27
    const-string v7, "android.btopp.intent.extra.SHOW_ALL"

    #@29
    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@2c
    move-result v6

    #@2d
    iput-boolean v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mShowAllIncoming:Z

    #@2f
    .line 112
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->getActionBar()Landroid/app/ActionBar;

    #@32
    move-result-object v0

    #@33
    .line 113
    .local v0, ActionBarCompat:Landroid/app/ActionBar;
    invoke-virtual {v0, v9}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    #@36
    .line 117
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->getIntent()Landroid/content/Intent;

    #@39
    move-result-object v6

    #@3a
    const-string v7, "direction"

    #@3c
    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@3f
    move-result v1

    #@40
    .line 118
    .local v1, dir:I
    if-nez v1, :cond_117

    #@42
    .line 119
    const v6, 0x7f070057

    #@45
    invoke-virtual {p0, v6}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->getText(I)Ljava/lang/CharSequence;

    #@48
    move-result-object v6

    #@49
    invoke-virtual {p0, v6}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->setTitle(Ljava/lang/CharSequence;)V

    #@4c
    .line 120
    const-string v2, "(direction == 0)"

    #@4e
    .line 132
    .local v2, direction:Ljava/lang/String;
    :goto_4e
    new-instance v6, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v7, "status >= \'200\' AND "

    #@55
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v6

    #@59
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v6

    #@5d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v4

    #@61
    .line 134
    .local v4, selection:Ljava/lang/String;
    iget-boolean v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mShowAllIncoming:Z

    #@63
    if-nez v6, :cond_134

    #@65
    .line 135
    new-instance v6, Ljava/lang/StringBuilder;

    #@67
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@6a
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v6

    #@6e
    const-string v7, " AND ("

    #@70
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v6

    #@74
    const-string v7, "visibility"

    #@76
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v6

    #@7a
    const-string v7, " IS NULL OR "

    #@7c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    const-string v7, "visibility"

    #@82
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v6

    #@86
    const-string v7, " == \'"

    #@88
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v6

    #@8c
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v6

    #@90
    const-string v7, "\')"

    #@92
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v6

    #@96
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v4

    #@9a
    .line 149
    :goto_9a
    const-string v5, "timestamp DESC"

    #@9c
    .line 153
    .local v5, sortOrder:Ljava/lang/String;
    :try_start_9c
    sget-object v6, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@9e
    const/16 v7, 0x9

    #@a0
    new-array v7, v7, [Ljava/lang/String;

    #@a2
    const/4 v8, 0x0

    #@a3
    const-string v9, "_id"

    #@a5
    aput-object v9, v7, v8

    #@a7
    const/4 v8, 0x1

    #@a8
    const-string v9, "hint"

    #@aa
    aput-object v9, v7, v8

    #@ac
    const/4 v8, 0x2

    #@ad
    const-string v9, "status"

    #@af
    aput-object v9, v7, v8

    #@b1
    const/4 v8, 0x3

    #@b2
    const-string v9, "total_bytes"

    #@b4
    aput-object v9, v7, v8

    #@b6
    const/4 v8, 0x4

    #@b7
    const-string v9, "_data"

    #@b9
    aput-object v9, v7, v8

    #@bb
    const/4 v8, 0x5

    #@bc
    const-string v9, "timestamp"

    #@be
    aput-object v9, v7, v8

    #@c0
    const/4 v8, 0x6

    #@c1
    const-string v9, "visibility"

    #@c3
    aput-object v9, v7, v8

    #@c5
    const/4 v8, 0x7

    #@c6
    const-string v9, "destination"

    #@c8
    aput-object v9, v7, v8

    #@ca
    const/16 v8, 0x8

    #@cc
    const-string v9, "direction"

    #@ce
    aput-object v9, v7, v8

    #@d0
    const-string v8, "timestamp DESC"

    #@d2
    invoke-virtual {p0, v6, v7, v4, v8}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->managedQuery(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@d5
    move-result-object v6

    #@d6
    iput-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;
    :try_end_d8
    .catch Ljava/lang/Exception; {:try_start_9c .. :try_end_d8} :catch_16b

    #@d8
    .line 165
    :goto_d8
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@da
    if-eqz v6, :cond_10f

    #@dc
    .line 166
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@de
    const-string v7, "_id"

    #@e0
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@e3
    move-result v6

    #@e4
    iput v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mIdColumnId:I

    #@e6
    .line 168
    new-instance v6, Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;

    #@e8
    const v7, 0x7f030001

    #@eb
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@ed
    invoke-direct {v6, p0, v7, v8}, Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    #@f0
    iput-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferAdapter:Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;

    #@f2
    .line 170
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mListView:Landroid/widget/ListView;

    #@f4
    iget-object v7, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferAdapter:Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;

    #@f6
    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    #@f9
    .line 171
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mListView:Landroid/widget/ListView;

    #@fb
    const/high16 v7, 0x100

    #@fd
    invoke-virtual {v6, v7}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    #@100
    .line 172
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mListView:Landroid/widget/ListView;

    #@102
    invoke-virtual {v6, p0}, Landroid/widget/ListView;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    #@105
    .line 173
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mListView:Landroid/widget/ListView;

    #@107
    invoke-virtual {v6, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    #@10a
    .line 175
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mListView:Landroid/widget/ListView;

    #@10c
    invoke-virtual {v6, p0}, Landroid/widget/ListView;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    #@10f
    .line 179
    :cond_10f
    new-instance v6, Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@111
    invoke-direct {v6, p0}, Lcom/android/bluetooth/opp/BluetoothOppNotification;-><init>(Landroid/content/Context;)V

    #@114
    iput-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@116
    .line 180
    return-void

    #@117
    .line 123
    .end local v2           #direction:Ljava/lang/String;
    .end local v4           #selection:Ljava/lang/String;
    .end local v5           #sortOrder:Ljava/lang/String;
    :cond_117
    iget-boolean v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mShowAllIncoming:Z

    #@119
    if-eqz v6, :cond_129

    #@11b
    .line 124
    const v6, 0x7f070053

    #@11e
    invoke-virtual {p0, v6}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->getText(I)Ljava/lang/CharSequence;

    #@121
    move-result-object v6

    #@122
    invoke-virtual {p0, v6}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->setTitle(Ljava/lang/CharSequence;)V

    #@125
    .line 128
    :goto_125
    const-string v2, "(direction == 1)"

    #@127
    .restart local v2       #direction:Ljava/lang/String;
    goto/16 :goto_4e

    #@129
    .line 126
    .end local v2           #direction:Ljava/lang/String;
    :cond_129
    const v6, 0x7f070056

    #@12c
    invoke-virtual {p0, v6}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->getText(I)Ljava/lang/CharSequence;

    #@12f
    move-result-object v6

    #@130
    invoke-virtual {p0, v6}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->setTitle(Ljava/lang/CharSequence;)V

    #@133
    goto :goto_125

    #@134
    .line 142
    .restart local v2       #direction:Ljava/lang/String;
    .restart local v4       #selection:Ljava/lang/String;
    :cond_134
    new-instance v6, Ljava/lang/StringBuilder;

    #@136
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@139
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v6

    #@13d
    const-string v7, " AND ("

    #@13f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@142
    move-result-object v6

    #@143
    const-string v7, "visibility"

    #@145
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@148
    move-result-object v6

    #@149
    const-string v7, " IS NULL OR "

    #@14b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v6

    #@14f
    const-string v7, "visibility"

    #@151
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@154
    move-result-object v6

    #@155
    const-string v7, " < \'"

    #@157
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15a
    move-result-object v6

    #@15b
    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v6

    #@15f
    const-string v7, "\')"

    #@161
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v6

    #@165
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@168
    move-result-object v4

    #@169
    goto/16 :goto_9a

    #@16b
    .line 158
    .restart local v5       #sortOrder:Ljava/lang/String;
    :catch_16b
    move-exception v3

    #@16c
    .line 159
    .local v3, e:Ljava/lang/Exception;
    const-string v6, "BluetoothOppTransferHistory"

    #@16e
    const-string v7, "[BTUI] Error: Can not open mTransferCursor"

    #@170
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@173
    goto/16 :goto_d8
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .registers 10
    .parameter "menu"
    .parameter "v"
    .parameter "menuInfo"

    #@0
    .prologue
    .line 275
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@2
    if-eqz v3, :cond_3a

    #@4
    move-object v2, p3

    #@5
    .line 276
    check-cast v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    #@7
    .line 277
    .local v2, info:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@9
    iget v4, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    #@b
    invoke-interface {v3, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@e
    .line 278
    iget v3, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    #@10
    iput v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mContextMenuPosition:I

    #@12
    .line 280
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@14
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@16
    const-string v5, "hint"

    #@18
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@1b
    move-result v4

    #@1c
    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@1f
    move-result-object v0

    #@20
    .line 282
    .local v0, fileName:Ljava/lang/String;
    if-nez v0, :cond_29

    #@22
    .line 283
    const v3, 0x7f070039

    #@25
    invoke-virtual {p0, v3}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->getString(I)Ljava/lang/String;

    #@28
    move-result-object v0

    #@29
    .line 285
    :cond_29
    invoke-interface {p1, v0}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    #@2c
    .line 287
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->getMenuInflater()Landroid/view/MenuInflater;

    #@2f
    move-result-object v1

    #@30
    .line 288
    .local v1, inflater:Landroid/view/MenuInflater;
    iget-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mShowAllIncoming:Z

    #@32
    if-eqz v3, :cond_3b

    #@34
    .line 289
    const v3, 0x7f0a0001

    #@37
    invoke-virtual {v1, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    #@3a
    .line 294
    .end local v0           #fileName:Ljava/lang/String;
    .end local v1           #inflater:Landroid/view/MenuInflater;
    .end local v2           #info:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    :cond_3a
    :goto_3a
    return-void

    #@3b
    .line 291
    .restart local v0       #fileName:Ljava/lang/String;
    .restart local v1       #inflater:Landroid/view/MenuInflater;
    .restart local v2       #info:Landroid/widget/AdapterView$AdapterContextMenuInfo;
    :cond_3b
    const v3, 0x7f0a0003

    #@3e
    invoke-virtual {v1, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    #@41
    goto :goto_3a
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    #@0
    .prologue
    .line 206
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@2
    if-eqz v1, :cond_e

    #@4
    .line 208
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->getMenuInflater()Landroid/view/MenuInflater;

    #@7
    move-result-object v0

    #@8
    .line 209
    .local v0, inflater:Landroid/view/MenuInflater;
    const v1, 0x7f0a0002

    #@b
    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    #@e
    .line 211
    .end local v0           #inflater:Landroid/view/MenuInflater;
    :cond_e
    const/4 v1, 0x1

    #@f
    return v1
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .registers 7
    .parameter
    .parameter "view"
    .parameter "position"
    .parameter "id"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    #@0
    .prologue
    .line 392
    .local p1, parent:Landroid/widget/AdapterView;,"Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->mTransferCursor:Landroid/database/Cursor;

    #@2
    invoke-interface {v0, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@5
    .line 393
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->openCompleteTransfer()V

    #@8
    .line 394
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->updateNotificationWhenBtDisabled()V

    #@b
    .line 395
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .registers 4
    .parameter "item"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 230
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    #@4
    move-result v1

    #@5
    sparse-switch v1, :sswitch_data_12

    #@8
    .line 242
    const/4 v0, 0x0

    #@9
    :goto_9
    return v0

    #@a
    .line 232
    :sswitch_a
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->promptClearList()V

    #@d
    goto :goto_9

    #@e
    .line 236
    :sswitch_e
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->finish()V

    #@11
    goto :goto_9

    #@12
    .line 230
    :sswitch_data_12
    .sparse-switch
        0x102002c -> :sswitch_e
        0x7f0b0036 -> :sswitch_a
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .registers 4
    .parameter "menu"

    #@0
    .prologue
    .line 222
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;->getClearableCount()I

    #@3
    move-result v1

    #@4
    if-lez v1, :cond_16

    #@6
    const/4 v0, 0x1

    #@7
    .line 223
    .local v0, showClear:Z
    :goto_7
    const v1, 0x7f0b0036

    #@a
    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    #@d
    move-result-object v1

    #@e
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    #@11
    .line 225
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    #@14
    move-result v1

    #@15
    return v1

    #@16
    .line 222
    .end local v0           #showClear:Z
    :cond_16
    const/4 v0, 0x0

    #@17
    goto :goto_7
.end method
