.class public Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;
.super Ljavax/obex/ServerRequestHandler;
.source "BluetoothOppObexServerSession.java"

# interfaces
.implements Lcom/android/bluetooth/opp/BluetoothOppObexSession;


# static fields
.field private static final D:Z = true

.field private static final TAG:Ljava/lang/String; = "BtOppObexServer"

.field private static final V:Z = true


# instance fields
.field private mAccepted:I

.field private mCallback:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

.field private mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

.field private mInterrupted:Z

.field private mLocalShareInfoId:I

.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mServerBlocking:Z

.field private mSession:Ljavax/obex/ServerSession;

.field mTimeoutMsgSent:Z

.field private mTimestamp:J

.field private mTransport:Ljavax/obex/ObexTransport;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private recevied_file_cnt:I

.field private result_status:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljavax/obex/ObexTransport;)V
    .registers 7
    .parameter "context"
    .parameter "transport"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 111
    invoke-direct {p0}, Ljavax/obex/ServerRequestHandler;-><init>()V

    #@5
    .line 80
    const/4 v1, 0x0

    #@6
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@8
    .line 83
    iput-boolean v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mServerBlocking:Z

    #@a
    .line 91
    iput v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mAccepted:I

    #@c
    .line 93
    iput-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInterrupted:Z

    #@e
    .line 105
    iput-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTimeoutMsgSent:Z

    #@10
    .line 112
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@12
    .line 113
    iput-object p2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTransport:Ljavax/obex/ObexTransport;

    #@14
    .line 114
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@16
    const-string v2, "power"

    #@18
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/os/PowerManager;

    #@1e
    .line 115
    .local v0, pm:Landroid/os/PowerManager;
    const v1, 0x3000001a

    #@21
    const-string v2, "BtOppObexServer"

    #@23
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@26
    move-result-object v1

    #@27
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@29
    .line 117
    const-string v1, "BtOppObexServer"

    #@2b
    invoke-virtual {v0, v3, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@2e
    move-result-object v1

    #@2f
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@31
    .line 118
    return-void
.end method

.method private processShareInfo()Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;
    .registers 6

    #@0
    .prologue
    .line 630
    const-string v1, "BtOppObexServer"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "processShareInfo() "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@f
    iget v3, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 632
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@1e
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@20
    iget v2, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@22
    invoke-static {v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->generateFileInfo(Landroid/content/Context;I)Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@25
    move-result-object v0

    #@26
    .line 635
    .local v0, fileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;
    const-string v1, "BtOppObexServer"

    #@28
    const-string v2, "Generate BluetoothOppReceiveFileInfo:"

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 636
    const-string v1, "BtOppObexServer"

    #@2f
    new-instance v2, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v3, "filename  :"

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mFileName:Ljava/lang/String;

    #@3c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    .line 637
    const-string v1, "BtOppObexServer"

    #@49
    new-instance v2, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v3, "length    :"

    #@50
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v2

    #@54
    iget-wide v3, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mLength:J

    #@56
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@59
    move-result-object v2

    #@5a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v2

    #@5e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@61
    .line 638
    const-string v1, "BtOppObexServer"

    #@63
    new-instance v2, Ljava/lang/StringBuilder;

    #@65
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@68
    const-string v3, "status    :"

    #@6a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v2

    #@6e
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mStatus:I

    #@70
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v2

    #@74
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v2

    #@78
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 640
    return-object v0
.end method

.method private receiveFile(Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;Ljavax/obex/Operation;)I
    .registers 25
    .parameter "fileInfo"
    .parameter "op"

    #@0
    .prologue
    .line 522
    const/4 v14, -0x1

    #@1
    .line 523
    .local v14, status:I
    const/4 v5, 0x0

    #@2
    .line 525
    .local v5, bos:Ljava/io/BufferedOutputStream;
    const/4 v10, 0x0

    #@3
    .line 526
    .local v10, is:Ljava/io/InputStream;
    const/4 v9, 0x0

    #@4
    .line 528
    .local v9, error:Z
    :try_start_4
    invoke-interface/range {p2 .. p2}, Ljavax/obex/Operation;->openInputStream()Ljava/io/InputStream;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_7} :catch_cf

    #@7
    move-result-object v10

    #@8
    .line 535
    :goto_8
    new-instance v18, Ljava/lang/StringBuilder;

    #@a
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    sget-object v19, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@f
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v18

    #@13
    const-string v19, "/"

    #@15
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v18

    #@19
    move-object/from16 v0, p0

    #@1b
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@1d
    move-object/from16 v19, v0

    #@1f
    move-object/from16 v0, v19

    #@21
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@23
    move/from16 v19, v0

    #@25
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v18

    #@29
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v18

    #@2d
    invoke-static/range {v18 .. v18}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@30
    move-result-object v6

    #@31
    .line 537
    .local v6, contentUri:Landroid/net/Uri;
    if-nez v9, :cond_5c

    #@33
    .line 538
    new-instance v17, Landroid/content/ContentValues;

    #@35
    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    #@38
    .line 539
    .local v17, updateValues:Landroid/content/ContentValues;
    const-string v18, "_data"

    #@3a
    move-object/from16 v0, p1

    #@3c
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mFileName:Ljava/lang/String;

    #@3e
    move-object/from16 v19, v0

    #@40
    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@43
    .line 540
    move-object/from16 v0, p0

    #@45
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@47
    move-object/from16 v18, v0

    #@49
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4c
    move-result-object v18

    #@4d
    const/16 v19, 0x0

    #@4f
    const/16 v20, 0x0

    #@51
    move-object/from16 v0, v18

    #@53
    move-object/from16 v1, v17

    #@55
    move-object/from16 v2, v19

    #@57
    move-object/from16 v3, v20

    #@59
    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@5c
    .line 543
    .end local v17           #updateValues:Landroid/content/ContentValues;
    :cond_5c
    const/4 v12, 0x0

    #@5d
    .line 544
    .local v12, position:I
    if-nez v9, :cond_70

    #@5f
    .line 545
    new-instance v5, Ljava/io/BufferedOutputStream;

    #@61
    .end local v5           #bos:Ljava/io/BufferedOutputStream;
    move-object/from16 v0, p1

    #@63
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mOutputStream:Ljava/io/FileOutputStream;

    #@65
    move-object/from16 v18, v0

    #@67
    const/high16 v19, 0x1

    #@69
    move-object/from16 v0, v18

    #@6b
    move/from16 v1, v19

    #@6d
    invoke-direct {v5, v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    #@70
    .line 548
    .restart local v5       #bos:Ljava/io/BufferedOutputStream;
    :cond_70
    if-nez v9, :cond_b8

    #@72
    .line 549
    invoke-interface/range {p2 .. p2}, Ljavax/obex/Operation;->getMaxPacketSize()I

    #@75
    move-result v11

    #@76
    .line 550
    .local v11, outputBufferSize:I
    new-array v4, v11, [B

    #@78
    .line 551
    .local v4, b:[B
    const/4 v13, 0x0

    #@79
    .line 552
    .local v13, readLength:I
    const-wide/16 v15, 0x0

    #@7b
    .line 554
    .local v15, timestamp:J
    :goto_7b
    :try_start_7b
    move-object/from16 v0, p0

    #@7d
    iget-boolean v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInterrupted:Z

    #@7f
    move/from16 v18, v0

    #@81
    if-nez v18, :cond_b8

    #@83
    int-to-long v0, v12

    #@84
    move-wide/from16 v18, v0

    #@86
    move-object/from16 v0, p1

    #@88
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mLength:J

    #@8a
    move-wide/from16 v20, v0

    #@8c
    cmp-long v18, v18, v20

    #@8e
    if-eqz v18, :cond_b8

    #@90
    .line 557
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@93
    move-result-wide v15

    #@94
    .line 560
    invoke-virtual {v10, v4}, Ljava/io/InputStream;->read([B)I

    #@97
    move-result v13

    #@98
    .line 562
    const/16 v18, -0x1

    #@9a
    move/from16 v0, v18

    #@9c
    if-ne v13, v0, :cond_dc

    #@9e
    .line 564
    const-string v18, "BtOppObexServer"

    #@a0
    new-instance v19, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v20, "Receive file reached stream end at position"

    #@a7
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v19

    #@ab
    move-object/from16 v0, v19

    #@ad
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v19

    #@b1
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b4
    move-result-object v19

    #@b5
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_b8
    .catch Ljava/io/IOException; {:try_start_7b .. :try_end_b8} :catch_149

    #@b8
    .line 594
    .end local v4           #b:[B
    .end local v11           #outputBufferSize:I
    .end local v13           #readLength:I
    .end local v15           #timestamp:J
    :cond_b8
    :goto_b8
    move-object/from16 v0, p0

    #@ba
    iget-boolean v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInterrupted:Z

    #@bc
    move/from16 v18, v0

    #@be
    if-eqz v18, :cond_165

    #@c0
    .line 596
    const-string v18, "BtOppObexServer"

    #@c2
    const-string v19, "receiving file interrupted by user."

    #@c4
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c7
    .line 598
    const/16 v14, 0x1ea

    #@c9
    .line 618
    :cond_c9
    :goto_c9
    if-eqz v5, :cond_ce

    #@cb
    .line 620
    :try_start_cb
    invoke-virtual {v5}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_ce
    .catch Ljava/io/IOException; {:try_start_cb .. :try_end_ce} :catch_1d6

    #@ce
    .line 625
    :cond_ce
    :goto_ce
    return v14

    #@cf
    .line 529
    .end local v6           #contentUri:Landroid/net/Uri;
    .end local v12           #position:I
    :catch_cf
    move-exception v8

    #@d0
    .line 530
    .local v8, e1:Ljava/io/IOException;
    const-string v18, "BtOppObexServer"

    #@d2
    const-string v19, "Error when openInputStream"

    #@d4
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d7
    .line 531
    const/16 v14, 0x1f0

    #@d9
    .line 532
    const/4 v9, 0x1

    #@da
    goto/16 :goto_8

    #@dc
    .line 569
    .end local v8           #e1:Ljava/io/IOException;
    .restart local v4       #b:[B
    .restart local v6       #contentUri:Landroid/net/Uri;
    .restart local v11       #outputBufferSize:I
    .restart local v12       #position:I
    .restart local v13       #readLength:I
    .restart local v15       #timestamp:J
    :cond_dc
    const/16 v18, 0x0

    #@de
    :try_start_de
    move/from16 v0, v18

    #@e0
    invoke-virtual {v5, v4, v0, v13}, Ljava/io/BufferedOutputStream;->write([BII)V

    #@e3
    .line 570
    add-int/2addr v12, v13

    #@e4
    .line 573
    const-string v18, "BtOppObexServer"

    #@e6
    new-instance v19, Ljava/lang/StringBuilder;

    #@e8
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@eb
    const-string v20, "Receive file position = "

    #@ed
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v19

    #@f1
    move-object/from16 v0, v19

    #@f3
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v19

    #@f7
    const-string v20, " readLength "

    #@f9
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v19

    #@fd
    move-object/from16 v0, v19

    #@ff
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@102
    move-result-object v19

    #@103
    const-string v20, " bytes took "

    #@105
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v19

    #@109
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@10c
    move-result-wide v20

    #@10d
    sub-long v20, v20, v15

    #@10f
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@112
    move-result-object v19

    #@113
    const-string v20, " ms"

    #@115
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@118
    move-result-object v19

    #@119
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11c
    move-result-object v19

    #@11d
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@120
    .line 578
    new-instance v17, Landroid/content/ContentValues;

    #@122
    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    #@125
    .line 579
    .restart local v17       #updateValues:Landroid/content/ContentValues;
    const-string v18, "current_bytes"

    #@127
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12a
    move-result-object v19

    #@12b
    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@12e
    .line 580
    move-object/from16 v0, p0

    #@130
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@132
    move-object/from16 v18, v0

    #@134
    invoke-virtual/range {v18 .. v18}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@137
    move-result-object v18

    #@138
    const/16 v19, 0x0

    #@13a
    const/16 v20, 0x0

    #@13c
    move-object/from16 v0, v18

    #@13e
    move-object/from16 v1, v17

    #@140
    move-object/from16 v2, v19

    #@142
    move-object/from16 v3, v20

    #@144
    invoke-virtual {v0, v6, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_147
    .catch Ljava/io/IOException; {:try_start_de .. :try_end_147} :catch_149

    #@147
    goto/16 :goto_7b

    #@149
    .line 582
    .end local v17           #updateValues:Landroid/content/ContentValues;
    :catch_149
    move-exception v8

    #@14a
    .line 583
    .restart local v8       #e1:Ljava/io/IOException;
    const-string v18, "BtOppObexServer"

    #@14c
    const-string v19, "Error when receiving file"

    #@14e
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@151
    .line 585
    const-string v18, "Abort Received"

    #@153
    invoke-virtual {v8}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    #@156
    move-result-object v19

    #@157
    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@15a
    move-result v18

    #@15b
    if-eqz v18, :cond_162

    #@15d
    .line 586
    const/16 v14, 0x1ea

    #@15f
    .line 590
    :goto_15f
    const/4 v9, 0x1

    #@160
    goto/16 :goto_b8

    #@162
    .line 588
    :cond_162
    const/16 v14, 0x1f0

    #@164
    goto :goto_15f

    #@165
    .line 600
    .end local v4           #b:[B
    .end local v8           #e1:Ljava/io/IOException;
    .end local v11           #outputBufferSize:I
    .end local v13           #readLength:I
    .end local v15           #timestamp:J
    :cond_165
    int-to-long v0, v12

    #@166
    move-wide/from16 v18, v0

    #@168
    move-object/from16 v0, p1

    #@16a
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mLength:J

    #@16c
    move-wide/from16 v20, v0

    #@16e
    cmp-long v18, v18, v20

    #@170
    if-nez v18, :cond_1a2

    #@172
    .line 602
    const-string v18, "BtOppObexServer"

    #@174
    new-instance v19, Ljava/lang/StringBuilder;

    #@176
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@179
    const-string v20, "Receiving file completed for "

    #@17b
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17e
    move-result-object v19

    #@17f
    move-object/from16 v0, p1

    #@181
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mFileName:Ljava/lang/String;

    #@183
    move-object/from16 v20, v0

    #@185
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v19

    #@189
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18c
    move-result-object v19

    #@18d
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@190
    .line 604
    const/16 v14, 0xc8

    #@192
    .line 606
    move-object/from16 v0, p0

    #@194
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->recevied_file_cnt:I

    #@196
    move/from16 v18, v0

    #@198
    add-int/lit8 v18, v18, 0x1

    #@19a
    move/from16 v0, v18

    #@19c
    move-object/from16 v1, p0

    #@19e
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->recevied_file_cnt:I

    #@1a0
    goto/16 :goto_c9

    #@1a2
    .line 610
    :cond_1a2
    const-string v18, "BtOppObexServer"

    #@1a4
    new-instance v19, Ljava/lang/StringBuilder;

    #@1a6
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@1a9
    const-string v20, "Reading file failed at "

    #@1ab
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ae
    move-result-object v19

    #@1af
    move-object/from16 v0, v19

    #@1b1
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b4
    move-result-object v19

    #@1b5
    const-string v20, " of "

    #@1b7
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ba
    move-result-object v19

    #@1bb
    move-object/from16 v0, p1

    #@1bd
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mLength:J

    #@1bf
    move-wide/from16 v20, v0

    #@1c1
    invoke-virtual/range {v19 .. v21}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1c4
    move-result-object v19

    #@1c5
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c8
    move-result-object v19

    #@1c9
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1cc
    .line 612
    const/16 v18, -0x1

    #@1ce
    move/from16 v0, v18

    #@1d0
    if-ne v14, v0, :cond_c9

    #@1d2
    .line 613
    const/16 v14, 0x1eb

    #@1d4
    goto/16 :goto_c9

    #@1d6
    .line 621
    :catch_1d6
    move-exception v7

    #@1d7
    .line 622
    .local v7, e:Ljava/io/IOException;
    const-string v18, "BtOppObexServer"

    #@1d9
    const-string v19, "Error when closing stream after send"

    #@1db
    invoke-static/range {v18 .. v19}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1de
    goto/16 :goto_ce
.end method

.method private declared-synchronized releaseWakeLocks()V
    .registers 2

    #@0
    .prologue
    .line 681
    monitor-enter p0

    #@1
    :try_start_1
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_e

    #@9
    .line 682
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@b
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@e
    .line 684
    :cond_e
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@10
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@13
    move-result v0

    #@14
    if-eqz v0, :cond_1b

    #@16
    .line 685
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@18
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_1b
    .catchall {:try_start_1 .. :try_end_1b} :catchall_1d

    #@1b
    .line 687
    :cond_1b
    monitor-exit p0

    #@1c
    return-void

    #@1d
    .line 681
    :catchall_1d
    move-exception v0

    #@1e
    monitor-exit p0

    #@1f
    throw v0
.end method


# virtual methods
.method public addShare(Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V
    .registers 5
    .parameter "info"

    #@0
    .prologue
    .line 186
    const-string v0, "BtOppObexServer"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "addShare for id "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p1, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 188
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@1c
    .line 189
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->processShareInfo()Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@1f
    move-result-object v0

    #@20
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@22
    .line 190
    return-void
.end method

.method public onClose()V
    .registers 7

    #@0
    .prologue
    .line 692
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "INBOUND_TRANSFER"

    #@4
    const/4 v3, 0x0

    #@5
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->recevied_file_cnt:I

    #@7
    iget v5, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->result_status:I

    #@9
    invoke-static {v1, v2, v3, v4, v5}, Lcom/android/bluetooth/opp/Constants;->updateTransferStatusForMessage(Landroid/content/Context;Ljava/lang/String;III)V

    #@c
    .line 695
    const-string v1, "BtOppObexServer"

    #@e
    const-string v2, "release WakeLock"

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 697
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->releaseWakeLocks()V

    #@16
    .line 700
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@18
    if-eqz v1, :cond_2a

    #@1a
    .line 701
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@1c
    invoke-static {v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@1f
    move-result-object v0

    #@20
    .line 702
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x1

    #@21
    iput v1, v0, Landroid/os/Message;->what:I

    #@23
    .line 703
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@25
    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@27
    .line 704
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@2a
    .line 706
    .end local v0           #msg:Landroid/os/Message;
    :cond_2a
    return-void
.end method

.method public onConnect(Ljavax/obex/HeaderSet;Ljavax/obex/HeaderSet;)I
    .registers 9
    .parameter "request"
    .parameter "reply"

    #@0
    .prologue
    .line 647
    const-string v3, "BtOppObexServer"

    #@2
    const-string v4, "onConnect"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 650
    invoke-static {p1}, Lcom/android/bluetooth/opp/Constants;->logHeader(Ljavax/obex/HeaderSet;)V

    #@a
    .line 653
    const/16 v3, 0x46

    #@c
    :try_start_c
    invoke-virtual {p1, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@f
    move-result-object v3

    #@10
    check-cast v3, [B

    #@12
    move-object v0, v3

    #@13
    check-cast v0, [B

    #@15
    move-object v2, v0

    #@16
    .line 655
    .local v2, uuid:[B
    const-string v3, "BtOppObexServer"

    #@18
    new-instance v4, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v5, "onConnect(): uuid ="

    #@1f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v4

    #@23
    invoke-static {v2}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@26
    move-result-object v5

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v4

    #@2f
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_32
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_32} :catch_37

    #@32
    .line 657
    if-eqz v2, :cond_44

    #@34
    .line 658
    const/16 v3, 0xc6

    #@36
    .line 669
    .end local v2           #uuid:[B
    :goto_36
    return v3

    #@37
    .line 660
    :catch_37
    move-exception v1

    #@38
    .line 661
    .local v1, e:Ljava/io/IOException;
    const-string v3, "BtOppObexServer"

    #@3a
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 662
    const/16 v3, 0xd0

    #@43
    goto :goto_36

    #@44
    .line 664
    .end local v1           #e:Ljava/io/IOException;
    .restart local v2       #uuid:[B
    :cond_44
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@47
    move-result-wide v3

    #@48
    iput-wide v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTimestamp:J

    #@4a
    .line 666
    const/4 v3, 0x0

    #@4b
    iput v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->recevied_file_cnt:I

    #@4d
    .line 667
    const/16 v3, 0x1eb

    #@4f
    iput v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->result_status:I

    #@51
    .line 669
    const/16 v3, 0xa0

    #@53
    goto :goto_36
.end method

.method public onDisconnect(Ljavax/obex/HeaderSet;Ljavax/obex/HeaderSet;)V
    .registers 5
    .parameter "req"
    .parameter "resp"

    #@0
    .prologue
    .line 675
    const-string v0, "BtOppObexServer"

    #@2
    const-string v1, "onDisconnect"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 677
    const/16 v0, 0xa0

    #@9
    iput v0, p2, Ljavax/obex/HeaderSet;->responseCode:I

    #@b
    .line 678
    return-void
.end method

.method public onPut(Ljavax/obex/Operation;)I
    .registers 30
    .parameter "op"

    #@0
    .prologue
    .line 195
    const-string v24, "BtOppObexServer"

    #@2
    new-instance v25, Ljava/lang/StringBuilder;

    #@4
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v26, "onPut "

    #@9
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v25

    #@d
    invoke-virtual/range {p1 .. p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10
    move-result-object v26

    #@11
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v25

    #@15
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v25

    #@19
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 202
    const/16 v17, 0xa0

    #@1e
    .line 205
    .local v17, obexResponse:I
    const/16 v24, 0x1eb

    #@20
    move/from16 v0, v24

    #@22
    move-object/from16 v1, p0

    #@24
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->result_status:I

    #@26
    .line 212
    move-object/from16 v0, p0

    #@28
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mAccepted:I

    #@2a
    move/from16 v24, v0

    #@2c
    const/16 v25, 0x3

    #@2e
    move/from16 v0, v24

    #@30
    move/from16 v1, v25

    #@32
    if-ne v0, v1, :cond_37

    #@34
    .line 213
    const/16 v24, 0xc3

    #@36
    .line 515
    :goto_36
    return v24

    #@37
    .line 217
    :cond_37
    move-object/from16 v0, p0

    #@39
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTransport:Ljavax/obex/ObexTransport;

    #@3b
    move-object/from16 v24, v0

    #@3d
    move-object/from16 v0, v24

    #@3f
    instance-of v0, v0, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;

    #@41
    move/from16 v24, v0

    #@43
    if-eqz v24, :cond_f0

    #@45
    .line 218
    move-object/from16 v0, p0

    #@47
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTransport:Ljavax/obex/ObexTransport;

    #@49
    move-object/from16 v24, v0

    #@4b
    check-cast v24, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;

    #@4d
    invoke-virtual/range {v24 .. v24}, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;->getRemoteAddress()Ljava/lang/String;

    #@50
    move-result-object v5

    #@51
    .line 222
    .local v5, destination:Ljava/lang/String;
    :goto_51
    move-object/from16 v0, p0

    #@53
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@55
    move-object/from16 v24, v0

    #@57
    invoke-static/range {v24 .. v24}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@5a
    move-result-object v24

    #@5b
    move-object/from16 v0, v24

    #@5d
    invoke-virtual {v0, v5}, Lcom/android/bluetooth/opp/BluetoothOppManager;->isWhitelisted(Ljava/lang/String;)Z

    #@60
    move-result v10

    #@61
    .line 226
    .local v10, isWhitelisted:Z
    const/16 v18, 0x0

    #@63
    .line 228
    .local v18, pre_reject:Z
    :try_start_63
    invoke-interface/range {p1 .. p1}, Ljavax/obex/Operation;->getReceivedHeader()Ljavax/obex/HeaderSet;

    #@66
    move-result-object v19

    #@67
    .line 230
    .local v19, request:Ljavax/obex/HeaderSet;
    invoke-static/range {v19 .. v19}, Lcom/android/bluetooth/opp/Constants;->logHeader(Ljavax/obex/HeaderSet;)V

    #@6a
    .line 232
    const/16 v24, 0x1

    #@6c
    move-object/from16 v0, v19

    #@6e
    move/from16 v1, v24

    #@70
    invoke-virtual {v0, v1}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@73
    move-result-object v15

    #@74
    check-cast v15, Ljava/lang/String;

    #@76
    .line 233
    .local v15, name:Ljava/lang/String;
    const/16 v24, 0xc3

    #@78
    move-object/from16 v0, v19

    #@7a
    move/from16 v1, v24

    #@7c
    invoke-virtual {v0, v1}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@7f
    move-result-object v11

    #@80
    check-cast v11, Ljava/lang/Long;

    #@82
    .line 234
    .local v11, length:Ljava/lang/Long;
    const/16 v24, 0x42

    #@84
    move-object/from16 v0, v19

    #@86
    move/from16 v1, v24

    #@88
    invoke-virtual {v0, v1}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@8b
    move-result-object v13

    #@8c
    check-cast v13, Ljava/lang/String;

    #@8e
    .line 236
    .local v13, mimeType:Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    #@91
    move-result-wide v24

    #@92
    const-wide/16 v26, 0x0

    #@94
    cmp-long v24, v24, v26

    #@96
    if-nez v24, :cond_a3

    #@98
    .line 238
    const-string v24, "BtOppObexServer"

    #@9a
    const-string v25, "length is 0, reject the transfer"

    #@9c
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    .line 240
    const/16 v18, 0x1

    #@a1
    .line 241
    const/16 v17, 0xcb

    #@a3
    .line 244
    :cond_a3
    if-eqz v15, :cond_af

    #@a5
    const-string v24, ""

    #@a7
    move-object/from16 v0, v24

    #@a9
    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ac
    move-result v24

    #@ad
    if-eqz v24, :cond_ba

    #@af
    .line 246
    :cond_af
    const-string v24, "BtOppObexServer"

    #@b1
    const-string v25, "name is null or empty, reject the transfer"

    #@b3
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 248
    const/16 v18, 0x1

    #@b8
    .line 249
    const/16 v17, 0xc0

    #@ba
    .line 252
    :cond_ba
    if-nez v18, :cond_d3

    #@bc
    .line 256
    const-string v24, "."

    #@be
    move-object/from16 v0, v24

    #@c0
    invoke-virtual {v15, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@c3
    move-result v6

    #@c4
    .line 257
    .local v6, dotIndex:I
    if-gez v6, :cond_f4

    #@c6
    if-nez v13, :cond_f4

    #@c8
    .line 259
    const-string v24, "BtOppObexServer"

    #@ca
    const-string v25, "There is no file extension or mime type,reject the transfer"

    #@cc
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    .line 262
    const/16 v18, 0x1

    #@d1
    .line 263
    const/16 v17, 0xc0

    #@d3
    .line 291
    .end local v6           #dotIndex:I
    :cond_d3
    :goto_d3
    if-nez v18, :cond_e2

    #@d5
    if-nez v13, :cond_e2

    #@d7
    .line 304
    const-string v24, "BtOppObexServer"

    #@d9
    const-string v25, "mimeType is null or in unacceptable list, reject the transfer"

    #@db
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_de
    .catch Ljava/io/IOException; {:try_start_63 .. :try_end_de} :catch_149

    #@de
    .line 306
    const/16 v18, 0x1

    #@e0
    .line 307
    const/16 v17, 0xcf

    #@e2
    .line 310
    :cond_e2
    if-eqz v18, :cond_168

    #@e4
    const/16 v24, 0xa0

    #@e6
    move/from16 v0, v17

    #@e8
    move/from16 v1, v24

    #@ea
    if-eq v0, v1, :cond_168

    #@ec
    move/from16 v24, v17

    #@ee
    .line 312
    goto/16 :goto_36

    #@f0
    .line 220
    .end local v5           #destination:Ljava/lang/String;
    .end local v10           #isWhitelisted:Z
    .end local v11           #length:Ljava/lang/Long;
    .end local v13           #mimeType:Ljava/lang/String;
    .end local v15           #name:Ljava/lang/String;
    .end local v18           #pre_reject:Z
    .end local v19           #request:Ljavax/obex/HeaderSet;
    :cond_f0
    const-string v5, "FF:FF:FF:00:00:00"

    #@f2
    .restart local v5       #destination:Ljava/lang/String;
    goto/16 :goto_51

    #@f4
    .line 265
    .restart local v6       #dotIndex:I
    .restart local v10       #isWhitelisted:Z
    .restart local v11       #length:Ljava/lang/Long;
    .restart local v13       #mimeType:Ljava/lang/String;
    .restart local v15       #name:Ljava/lang/String;
    .restart local v18       #pre_reject:Z
    .restart local v19       #request:Ljavax/obex/HeaderSet;
    :cond_f4
    add-int/lit8 v24, v6, 0x1

    #@f6
    :try_start_f6
    move/from16 v0, v24

    #@f8
    invoke-virtual {v15, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@fb
    move-result-object v24

    #@fc
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@ff
    move-result-object v8

    #@100
    .line 266
    .local v8, extension:Ljava/lang/String;
    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    #@103
    move-result-object v12

    #@104
    .line 267
    .local v12, map:Landroid/webkit/MimeTypeMap;
    invoke-virtual {v12, v8}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    #@107
    move-result-object v21

    #@108
    .line 269
    .local v21, type:Ljava/lang/String;
    const-string v24, "BtOppObexServer"

    #@10a
    new-instance v25, Ljava/lang/StringBuilder;

    #@10c
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@10f
    const-string v26, "Mimetype guessed from extension "

    #@111
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@114
    move-result-object v25

    #@115
    move-object/from16 v0, v25

    #@117
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v25

    #@11b
    const-string v26, " is "

    #@11d
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v25

    #@121
    move-object/from16 v0, v25

    #@123
    move-object/from16 v1, v21

    #@125
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v25

    #@129
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12c
    move-result-object v25

    #@12d
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@130
    .line 271
    if-eqz v21, :cond_13b

    #@132
    .line 272
    move-object/from16 v13, v21

    #@134
    .line 283
    :cond_134
    :goto_134
    if-eqz v13, :cond_d3

    #@136
    .line 284
    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@139
    move-result-object v13

    #@13a
    goto :goto_d3

    #@13b
    .line 275
    :cond_13b
    if-nez v13, :cond_134

    #@13d
    .line 277
    const-string v24, "BtOppObexServer"

    #@13f
    const-string v25, "Can\'t get mimetype, reject the transfer"

    #@141
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_144
    .catch Ljava/io/IOException; {:try_start_f6 .. :try_end_144} :catch_149

    #@144
    .line 279
    const/16 v18, 0x1

    #@146
    .line 280
    const/16 v17, 0xcf

    #@148
    goto :goto_134

    #@149
    .line 315
    .end local v6           #dotIndex:I
    .end local v8           #extension:Ljava/lang/String;
    .end local v11           #length:Ljava/lang/Long;
    .end local v12           #map:Landroid/webkit/MimeTypeMap;
    .end local v13           #mimeType:Ljava/lang/String;
    .end local v15           #name:Ljava/lang/String;
    .end local v19           #request:Ljavax/obex/HeaderSet;
    .end local v21           #type:Ljava/lang/String;
    :catch_149
    move-exception v7

    #@14a
    .line 316
    .local v7, e:Ljava/io/IOException;
    const-string v24, "BtOppObexServer"

    #@14c
    new-instance v25, Ljava/lang/StringBuilder;

    #@14e
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@151
    const-string v26, "get getReceivedHeaders error "

    #@153
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v25

    #@157
    move-object/from16 v0, v25

    #@159
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v25

    #@15d
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v25

    #@161
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@164
    .line 317
    const/16 v24, 0xc0

    #@166
    goto/16 :goto_36

    #@168
    .line 320
    .end local v7           #e:Ljava/io/IOException;
    .restart local v11       #length:Ljava/lang/Long;
    .restart local v13       #mimeType:Ljava/lang/String;
    .restart local v15       #name:Ljava/lang/String;
    .restart local v19       #request:Ljavax/obex/HeaderSet;
    :cond_168
    new-instance v23, Landroid/content/ContentValues;

    #@16a
    invoke-direct/range {v23 .. v23}, Landroid/content/ContentValues;-><init>()V

    #@16d
    .line 322
    .local v23, values:Landroid/content/ContentValues;
    const-string v24, "hint"

    #@16f
    move-object/from16 v0, v23

    #@171
    move-object/from16 v1, v24

    #@173
    invoke-virtual {v0, v1, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@176
    .line 325
    const-string v24, "total_bytes"

    #@178
    move-object/from16 v0, v23

    #@17a
    move-object/from16 v1, v24

    #@17c
    invoke-virtual {v0, v1, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@17f
    .line 327
    const-string v24, "mimetype"

    #@181
    move-object/from16 v0, v23

    #@183
    move-object/from16 v1, v24

    #@185
    invoke-virtual {v0, v1, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@188
    .line 329
    const-string v24, "destination"

    #@18a
    move-object/from16 v0, v23

    #@18c
    move-object/from16 v1, v24

    #@18e
    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@191
    .line 331
    const-string v24, "direction"

    #@193
    const/16 v25, 0x1

    #@195
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@198
    move-result-object v25

    #@199
    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@19c
    .line 332
    const-string v24, "timestamp"

    #@19e
    move-object/from16 v0, p0

    #@1a0
    iget-wide v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTimestamp:J

    #@1a2
    move-wide/from16 v25, v0

    #@1a4
    invoke-static/range {v25 .. v26}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@1a7
    move-result-object v25

    #@1a8
    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@1ab
    .line 334
    const/16 v16, 0x1

    #@1ad
    .line 336
    .local v16, needConfirm:Z
    move-object/from16 v0, p0

    #@1af
    iget-boolean v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mServerBlocking:Z

    #@1b1
    move/from16 v24, v0

    #@1b3
    if-nez v24, :cond_1c2

    #@1b5
    .line 337
    const-string v24, "confirm"

    #@1b7
    const/16 v25, 0x2

    #@1b9
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1bc
    move-result-object v25

    #@1bd
    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1c0
    .line 339
    const/16 v16, 0x0

    #@1c2
    .line 342
    :cond_1c2
    if-eqz v10, :cond_1d1

    #@1c4
    .line 343
    const-string v24, "confirm"

    #@1c6
    const/16 v25, 0x5

    #@1c8
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1cb
    move-result-object v25

    #@1cc
    invoke-virtual/range {v23 .. v25}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1cf
    .line 345
    const/16 v16, 0x0

    #@1d1
    .line 348
    :cond_1d1
    move-object/from16 v0, p0

    #@1d3
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@1d5
    move-object/from16 v24, v0

    #@1d7
    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1da
    move-result-object v24

    #@1db
    sget-object v25, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@1dd
    move-object/from16 v0, v24

    #@1df
    move-object/from16 v1, v25

    #@1e1
    move-object/from16 v2, v23

    #@1e3
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@1e6
    move-result-object v4

    #@1e7
    .line 349
    .local v4, contentUri:Landroid/net/Uri;
    invoke-virtual {v4}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@1ea
    move-result-object v24

    #@1eb
    const/16 v25, 0x1

    #@1ed
    invoke-interface/range {v24 .. v25}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1f0
    move-result-object v24

    #@1f1
    check-cast v24, Ljava/lang/String;

    #@1f3
    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@1f6
    move-result v24

    #@1f7
    move/from16 v0, v24

    #@1f9
    move-object/from16 v1, p0

    #@1fb
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mLocalShareInfoId:I

    #@1fd
    .line 351
    if-eqz v16, :cond_222

    #@1ff
    .line 352
    new-instance v9, Landroid/content/Intent;

    #@201
    const-string v24, "android.btopp.intent.action.INCOMING_FILE_NOTIFICATION"

    #@203
    move-object/from16 v0, v24

    #@205
    invoke-direct {v9, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@208
    .line 353
    .local v9, in:Landroid/content/Intent;
    const-string v24, "com.android.bluetooth"

    #@20a
    const-class v25, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@20c
    invoke-virtual/range {v25 .. v25}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@20f
    move-result-object v25

    #@210
    move-object/from16 v0, v24

    #@212
    move-object/from16 v1, v25

    #@214
    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@217
    .line 354
    move-object/from16 v0, p0

    #@219
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@21b
    move-object/from16 v24, v0

    #@21d
    move-object/from16 v0, v24

    #@21f
    invoke-virtual {v0, v9}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@222
    .line 358
    .end local v9           #in:Landroid/content/Intent;
    :cond_222
    const-string v24, "BtOppObexServer"

    #@224
    new-instance v25, Ljava/lang/StringBuilder;

    #@226
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@229
    const-string v26, "insert contentUri: "

    #@22b
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v25

    #@22f
    move-object/from16 v0, v25

    #@231
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@234
    move-result-object v25

    #@235
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@238
    move-result-object v25

    #@239
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@23c
    .line 361
    const-string v24, "BtOppObexServer"

    #@23e
    new-instance v25, Ljava/lang/StringBuilder;

    #@240
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@243
    const-string v26, "mLocalShareInfoId = "

    #@245
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@248
    move-result-object v25

    #@249
    move-object/from16 v0, p0

    #@24b
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mLocalShareInfoId:I

    #@24d
    move/from16 v26, v0

    #@24f
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@252
    move-result-object v25

    #@253
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@256
    move-result-object v25

    #@257
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25a
    .line 365
    const-string v24, "BtOppObexServer"

    #@25c
    const-string v25, "acquire partial WakeLock"

    #@25e
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@261
    .line 369
    monitor-enter p0

    #@262
    .line 370
    :try_start_262
    move-object/from16 v0, p0

    #@264
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@266
    move-object/from16 v24, v0

    #@268
    invoke-virtual/range {v24 .. v24}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@26b
    move-result v24

    #@26c
    if-eqz v24, :cond_277

    #@26e
    .line 371
    move-object/from16 v0, p0

    #@270
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

    #@272
    move-object/from16 v24, v0

    #@274
    invoke-virtual/range {v24 .. v24}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@277
    .line 376
    :cond_277
    const/16 v24, 0x1

    #@279
    move/from16 v0, v24

    #@27b
    move-object/from16 v1, p0

    #@27d
    iput-boolean v0, v1, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mServerBlocking:Z
    :try_end_27f
    .catchall {:try_start_262 .. :try_end_27f} :catchall_4ba

    #@27f
    .line 379
    :cond_27f
    :goto_27f
    :try_start_27f
    move-object/from16 v0, p0

    #@281
    iget-boolean v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mServerBlocking:Z

    #@283
    move/from16 v24, v0

    #@285
    if-eqz v24, :cond_2d0

    #@287
    .line 380
    const-wide/16 v24, 0x3e8

    #@289
    move-object/from16 v0, p0

    #@28b
    move-wide/from16 v1, v24

    #@28d
    invoke-virtual {v0, v1, v2}, Ljava/lang/Object;->wait(J)V

    #@290
    .line 381
    move-object/from16 v0, p0

    #@292
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@294
    move-object/from16 v24, v0

    #@296
    if-eqz v24, :cond_27f

    #@298
    move-object/from16 v0, p0

    #@29a
    iget-boolean v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTimeoutMsgSent:Z

    #@29c
    move/from16 v24, v0

    #@29e
    if-nez v24, :cond_27f

    #@2a0
    .line 382
    move-object/from16 v0, p0

    #@2a2
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@2a4
    move-object/from16 v24, v0

    #@2a6
    move-object/from16 v0, p0

    #@2a8
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@2aa
    move-object/from16 v25, v0

    #@2ac
    const/16 v26, 0x4

    #@2ae
    invoke-virtual/range {v25 .. v26}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@2b1
    move-result-object v25

    #@2b2
    const-wide/32 v26, 0xc350

    #@2b5
    invoke-virtual/range {v24 .. v27}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@2b8
    .line 385
    const/16 v24, 0x1

    #@2ba
    move/from16 v0, v24

    #@2bc
    move-object/from16 v1, p0

    #@2be
    iput-boolean v0, v1, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTimeoutMsgSent:Z

    #@2c0
    .line 387
    const-string v24, "BtOppObexServer"

    #@2c2
    const-string v25, "MSG_CONNECT_TIMEOUT sent"

    #@2c4
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2c7
    .catchall {:try_start_27f .. :try_end_2c7} :catchall_4ba
    .catch Ljava/lang/InterruptedException; {:try_start_27f .. :try_end_2c7} :catch_2c8

    #@2c7
    goto :goto_27f

    #@2c8
    .line 391
    :catch_2c8
    move-exception v7

    #@2c9
    .line 393
    .local v7, e:Ljava/lang/InterruptedException;
    :try_start_2c9
    const-string v24, "BtOppObexServer"

    #@2cb
    const-string v25, "Interrupted in onPut blocking"

    #@2cd
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2d0
    .line 396
    .end local v7           #e:Ljava/lang/InterruptedException;
    :cond_2d0
    monitor-exit p0
    :try_end_2d1
    .catchall {:try_start_2c9 .. :try_end_2d1} :catchall_4ba

    #@2d1
    .line 398
    const-string v24, "BtOppObexServer"

    #@2d3
    const-string v25, "Server unblocked "

    #@2d5
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d8
    .line 400
    monitor-enter p0

    #@2d9
    .line 401
    :try_start_2d9
    move-object/from16 v0, p0

    #@2db
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@2dd
    move-object/from16 v24, v0

    #@2df
    if-eqz v24, :cond_2f4

    #@2e1
    move-object/from16 v0, p0

    #@2e3
    iget-boolean v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTimeoutMsgSent:Z

    #@2e5
    move/from16 v24, v0

    #@2e7
    if-eqz v24, :cond_2f4

    #@2e9
    .line 402
    move-object/from16 v0, p0

    #@2eb
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@2ed
    move-object/from16 v24, v0

    #@2ef
    const/16 v25, 0x4

    #@2f1
    invoke-virtual/range {v24 .. v25}, Landroid/os/Handler;->removeMessages(I)V

    #@2f4
    .line 404
    :cond_2f4
    monitor-exit p0
    :try_end_2f5
    .catchall {:try_start_2d9 .. :try_end_2f5} :catchall_4bd

    #@2f5
    .line 412
    move-object/from16 v0, p0

    #@2f7
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@2f9
    move-object/from16 v24, v0

    #@2fb
    move-object/from16 v0, v24

    #@2fd
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@2ff
    move/from16 v24, v0

    #@301
    move-object/from16 v0, p0

    #@303
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mLocalShareInfoId:I

    #@305
    move/from16 v25, v0

    #@307
    move/from16 v0, v24

    #@309
    move/from16 v1, v25

    #@30b
    if-eq v0, v1, :cond_314

    #@30d
    .line 413
    const-string v24, "BtOppObexServer"

    #@30f
    const-string v25, "Unexpected error!"

    #@311
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@314
    .line 415
    :cond_314
    move-object/from16 v0, p0

    #@316
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@318
    move-object/from16 v24, v0

    #@31a
    move-object/from16 v0, v24

    #@31c
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mConfirm:I

    #@31e
    move/from16 v24, v0

    #@320
    move/from16 v0, v24

    #@322
    move-object/from16 v1, p0

    #@324
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mAccepted:I

    #@326
    .line 418
    const-string v24, "BtOppObexServer"

    #@328
    new-instance v25, Ljava/lang/StringBuilder;

    #@32a
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@32d
    const-string v26, "after confirm: userAccepted="

    #@32f
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@332
    move-result-object v25

    #@333
    move-object/from16 v0, p0

    #@335
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mAccepted:I

    #@337
    move/from16 v26, v0

    #@339
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33c
    move-result-object v25

    #@33d
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@340
    move-result-object v25

    #@341
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@344
    .line 420
    const/16 v20, 0xc8

    #@346
    .line 422
    .local v20, status:I
    move-object/from16 v0, p0

    #@348
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mAccepted:I

    #@34a
    move/from16 v24, v0

    #@34c
    const/16 v25, 0x1

    #@34e
    move/from16 v0, v24

    #@350
    move/from16 v1, v25

    #@352
    if-eq v0, v1, :cond_370

    #@354
    move-object/from16 v0, p0

    #@356
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mAccepted:I

    #@358
    move/from16 v24, v0

    #@35a
    const/16 v25, 0x2

    #@35c
    move/from16 v0, v24

    #@35e
    move/from16 v1, v25

    #@360
    if-eq v0, v1, :cond_370

    #@362
    move-object/from16 v0, p0

    #@364
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mAccepted:I

    #@366
    move/from16 v24, v0

    #@368
    const/16 v25, 0x5

    #@36a
    move/from16 v0, v24

    #@36c
    move/from16 v1, v25

    #@36e
    if-ne v0, v1, :cond_4ee

    #@370
    .line 427
    :cond_370
    move-object/from16 v0, p0

    #@372
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@374
    move-object/from16 v24, v0

    #@376
    move-object/from16 v0, v24

    #@378
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mFileName:Ljava/lang/String;

    #@37a
    move-object/from16 v24, v0

    #@37c
    if-nez v24, :cond_3bf

    #@37e
    .line 428
    move-object/from16 v0, p0

    #@380
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@382
    move-object/from16 v24, v0

    #@384
    move-object/from16 v0, v24

    #@386
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mStatus:I

    #@388
    move/from16 v20, v0

    #@38a
    .line 430
    move-object/from16 v0, p0

    #@38c
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@38e
    move-object/from16 v24, v0

    #@390
    move-object/from16 v0, p0

    #@392
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@394
    move-object/from16 v25, v0

    #@396
    move-object/from16 v0, v25

    #@398
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mStatus:I

    #@39a
    move/from16 v25, v0

    #@39c
    move/from16 v0, v25

    #@39e
    move-object/from16 v1, v24

    #@3a0
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@3a2
    .line 431
    move-object/from16 v0, p0

    #@3a4
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@3a6
    move-object/from16 v24, v0

    #@3a8
    move-object/from16 v0, p0

    #@3aa
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@3ac
    move-object/from16 v25, v0

    #@3ae
    move-object/from16 v0, v25

    #@3b0
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@3b2
    move/from16 v25, v0

    #@3b4
    move-object/from16 v0, v24

    #@3b6
    move/from16 v1, v25

    #@3b8
    move/from16 v2, v20

    #@3ba
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@3bd
    .line 432
    const/16 v17, 0xd0

    #@3bf
    .line 436
    :cond_3bf
    move-object/from16 v0, p0

    #@3c1
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@3c3
    move-object/from16 v24, v0

    #@3c5
    move-object/from16 v0, v24

    #@3c7
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mFileName:Ljava/lang/String;

    #@3c9
    move-object/from16 v24, v0

    #@3cb
    if-eqz v24, :cond_471

    #@3cd
    .line 438
    new-instance v22, Landroid/content/ContentValues;

    #@3cf
    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    #@3d2
    .line 439
    .local v22, updateValues:Landroid/content/ContentValues;
    new-instance v24, Ljava/lang/StringBuilder;

    #@3d4
    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    #@3d7
    sget-object v25, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@3d9
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3dc
    move-result-object v24

    #@3dd
    const-string v25, "/"

    #@3df
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e2
    move-result-object v24

    #@3e3
    move-object/from16 v0, p0

    #@3e5
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@3e7
    move-object/from16 v25, v0

    #@3e9
    move-object/from16 v0, v25

    #@3eb
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@3ed
    move/from16 v25, v0

    #@3ef
    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3f2
    move-result-object v24

    #@3f3
    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f6
    move-result-object v24

    #@3f7
    invoke-static/range {v24 .. v24}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@3fa
    move-result-object v4

    #@3fb
    .line 440
    const-string v24, "_data"

    #@3fd
    move-object/from16 v0, p0

    #@3ff
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@401
    move-object/from16 v25, v0

    #@403
    move-object/from16 v0, v25

    #@405
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mFileName:Ljava/lang/String;

    #@407
    move-object/from16 v25, v0

    #@409
    move-object/from16 v0, v22

    #@40b
    move-object/from16 v1, v24

    #@40d
    move-object/from16 v2, v25

    #@40f
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@412
    .line 441
    const-string v24, "status"

    #@414
    const/16 v25, 0xc0

    #@416
    invoke-static/range {v25 .. v25}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@419
    move-result-object v25

    #@41a
    move-object/from16 v0, v22

    #@41c
    move-object/from16 v1, v24

    #@41e
    move-object/from16 v2, v25

    #@420
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@423
    .line 442
    move-object/from16 v0, p0

    #@425
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@427
    move-object/from16 v24, v0

    #@429
    invoke-virtual/range {v24 .. v24}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@42c
    move-result-object v24

    #@42d
    const/16 v25, 0x0

    #@42f
    const/16 v26, 0x0

    #@431
    move-object/from16 v0, v24

    #@433
    move-object/from16 v1, v22

    #@435
    move-object/from16 v2, v25

    #@437
    move-object/from16 v3, v26

    #@439
    invoke-virtual {v0, v4, v1, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@43c
    .line 444
    move-object/from16 v0, p0

    #@43e
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@440
    move-object/from16 v24, v0

    #@442
    move-object/from16 v0, p0

    #@444
    move-object/from16 v1, v24

    #@446
    move-object/from16 v2, p1

    #@448
    invoke-direct {v0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->receiveFile(Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;Ljavax/obex/Operation;)I

    #@44b
    move-result v20

    #@44c
    .line 448
    const/16 v24, 0xc8

    #@44e
    move/from16 v0, v20

    #@450
    move/from16 v1, v24

    #@452
    if-eq v0, v1, :cond_456

    #@454
    .line 449
    const/16 v17, 0xd0

    #@456
    .line 451
    :cond_456
    move-object/from16 v0, p0

    #@458
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@45a
    move-object/from16 v24, v0

    #@45c
    move-object/from16 v0, p0

    #@45e
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@460
    move-object/from16 v25, v0

    #@462
    move-object/from16 v0, v25

    #@464
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@466
    move/from16 v25, v0

    #@468
    move-object/from16 v0, v24

    #@46a
    move/from16 v1, v25

    #@46c
    move/from16 v2, v20

    #@46e
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@471
    .line 454
    .end local v22           #updateValues:Landroid/content/ContentValues;
    :cond_471
    const/16 v24, 0xc8

    #@473
    move/from16 v0, v20

    #@475
    move/from16 v1, v24

    #@477
    if-ne v0, v1, :cond_4c0

    #@479
    .line 455
    move-object/from16 v0, p0

    #@47b
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@47d
    move-object/from16 v24, v0

    #@47f
    const/16 v25, 0x0

    #@481
    invoke-static/range {v24 .. v25}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@484
    move-result-object v14

    #@485
    .line 456
    .local v14, msg:Landroid/os/Message;
    move-object/from16 v0, p0

    #@487
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@489
    move-object/from16 v24, v0

    #@48b
    move-object/from16 v0, v24

    #@48d
    iput-object v0, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@48f
    .line 457
    invoke-virtual {v14}, Landroid/os/Message;->sendToTarget()V

    #@492
    .line 512
    .end local v14           #msg:Landroid/os/Message;
    :cond_492
    :goto_492
    move/from16 v0, v20

    #@494
    move-object/from16 v1, p0

    #@496
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->result_status:I

    #@498
    .line 513
    const-string v24, "BtOppObexServer"

    #@49a
    new-instance v25, Ljava/lang/StringBuilder;

    #@49c
    invoke-direct/range {v25 .. v25}, Ljava/lang/StringBuilder;-><init>()V

    #@49f
    const-string v26, "onPut result_status : "

    #@4a1
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a4
    move-result-object v25

    #@4a5
    move-object/from16 v0, p0

    #@4a7
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->result_status:I

    #@4a9
    move/from16 v26, v0

    #@4ab
    invoke-virtual/range {v25 .. v26}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4ae
    move-result-object v25

    #@4af
    invoke-virtual/range {v25 .. v25}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b2
    move-result-object v25

    #@4b3
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4b6
    move/from16 v24, v17

    #@4b8
    .line 515
    goto/16 :goto_36

    #@4ba
    .line 396
    .end local v20           #status:I
    :catchall_4ba
    move-exception v24

    #@4bb
    :try_start_4bb
    monitor-exit p0
    :try_end_4bc
    .catchall {:try_start_4bb .. :try_end_4bc} :catchall_4ba

    #@4bc
    throw v24

    #@4bd
    .line 404
    :catchall_4bd
    move-exception v24

    #@4be
    :try_start_4be
    monitor-exit p0
    :try_end_4bf
    .catchall {:try_start_4be .. :try_end_4bf} :catchall_4bd

    #@4bf
    throw v24

    #@4c0
    .line 459
    .restart local v20       #status:I
    :cond_4c0
    move-object/from16 v0, p0

    #@4c2
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@4c4
    move-object/from16 v24, v0

    #@4c6
    if-eqz v24, :cond_492

    #@4c8
    .line 460
    move-object/from16 v0, p0

    #@4ca
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@4cc
    move-object/from16 v24, v0

    #@4ce
    const/16 v25, 0x2

    #@4d0
    invoke-static/range {v24 .. v25}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    #@4d3
    move-result-object v14

    #@4d4
    .line 462
    .restart local v14       #msg:Landroid/os/Message;
    move-object/from16 v0, p0

    #@4d6
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@4d8
    move-object/from16 v24, v0

    #@4da
    move/from16 v0, v20

    #@4dc
    move-object/from16 v1, v24

    #@4de
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@4e0
    .line 463
    move-object/from16 v0, p0

    #@4e2
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@4e4
    move-object/from16 v24, v0

    #@4e6
    move-object/from16 v0, v24

    #@4e8
    iput-object v0, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4ea
    .line 464
    invoke-virtual {v14}, Landroid/os/Message;->sendToTarget()V

    #@4ed
    goto :goto_492

    #@4ee
    .line 467
    .end local v14           #msg:Landroid/os/Message;
    :cond_4ee
    move-object/from16 v0, p0

    #@4f0
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mAccepted:I

    #@4f2
    move/from16 v24, v0

    #@4f4
    const/16 v25, 0x3

    #@4f6
    move/from16 v0, v24

    #@4f8
    move/from16 v1, v25

    #@4fa
    if-eq v0, v1, :cond_50a

    #@4fc
    move-object/from16 v0, p0

    #@4fe
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mAccepted:I

    #@500
    move/from16 v24, v0

    #@502
    const/16 v25, 0x4

    #@504
    move/from16 v0, v24

    #@506
    move/from16 v1, v25

    #@508
    if-ne v0, v1, :cond_492

    #@50a
    .line 477
    :cond_50a
    const-string v24, "BtOppObexServer"

    #@50c
    const-string v25, "Rejected incoming request"

    #@50e
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@511
    .line 478
    move-object/from16 v0, p0

    #@513
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@515
    move-object/from16 v24, v0

    #@517
    move-object/from16 v0, v24

    #@519
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mFileName:Ljava/lang/String;

    #@51b
    move-object/from16 v24, v0

    #@51d
    if-eqz v24, :cond_542

    #@51f
    .line 480
    :try_start_51f
    move-object/from16 v0, p0

    #@521
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@523
    move-object/from16 v24, v0

    #@525
    move-object/from16 v0, v24

    #@527
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mOutputStream:Ljava/io/FileOutputStream;

    #@529
    move-object/from16 v24, v0

    #@52b
    invoke-virtual/range {v24 .. v24}, Ljava/io/FileOutputStream;->close()V
    :try_end_52e
    .catch Ljava/io/IOException; {:try_start_51f .. :try_end_52e} :catch_59e

    #@52e
    .line 484
    :goto_52e
    new-instance v24, Ljava/io/File;

    #@530
    move-object/from16 v0, p0

    #@532
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mFileInfo:Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;

    #@534
    move-object/from16 v25, v0

    #@536
    move-object/from16 v0, v25

    #@538
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppReceiveFileInfo;->mFileName:Ljava/lang/String;

    #@53a
    move-object/from16 v25, v0

    #@53c
    invoke-direct/range {v24 .. v25}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@53f
    invoke-virtual/range {v24 .. v24}, Ljava/io/File;->delete()Z

    #@542
    .line 488
    :cond_542
    invoke-static {}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->isInternalStorageFull()Z

    #@545
    move-result v24

    #@546
    if-eqz v24, :cond_5aa

    #@548
    .line 489
    invoke-static {}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->isNoExternalStorage()Z

    #@54b
    move-result v24

    #@54c
    if-nez v24, :cond_554

    #@54e
    invoke-static {}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->isExternalStorageFull()Z

    #@551
    move-result v24

    #@552
    if-eqz v24, :cond_5a7

    #@554
    .line 490
    :cond_554
    const/16 v20, 0x1ee

    #@556
    .line 501
    :goto_556
    move-object/from16 v0, p0

    #@558
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@55a
    move-object/from16 v24, v0

    #@55c
    move-object/from16 v0, p0

    #@55e
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@560
    move-object/from16 v25, v0

    #@562
    move-object/from16 v0, v25

    #@564
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@566
    move/from16 v25, v0

    #@568
    move-object/from16 v0, v24

    #@56a
    move/from16 v1, v25

    #@56c
    move/from16 v2, v20

    #@56e
    invoke-static {v0, v1, v2}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@571
    .line 502
    const/16 v17, 0xc3

    #@573
    .line 504
    move-object/from16 v0, p0

    #@575
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@577
    move-object/from16 v24, v0

    #@579
    invoke-static/range {v24 .. v24}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@57c
    move-result-object v14

    #@57d
    .line 505
    .restart local v14       #msg:Landroid/os/Message;
    const/16 v24, 0x3

    #@57f
    move/from16 v0, v24

    #@581
    iput v0, v14, Landroid/os/Message;->what:I

    #@583
    .line 506
    move-object/from16 v0, p0

    #@585
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@587
    move-object/from16 v24, v0

    #@589
    move/from16 v0, v20

    #@58b
    move-object/from16 v1, v24

    #@58d
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@58f
    .line 507
    move-object/from16 v0, p0

    #@591
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@593
    move-object/from16 v24, v0

    #@595
    move-object/from16 v0, v24

    #@597
    iput-object v0, v14, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@599
    .line 508
    invoke-virtual {v14}, Landroid/os/Message;->sendToTarget()V

    #@59c
    goto/16 :goto_492

    #@59e
    .line 481
    .end local v14           #msg:Landroid/os/Message;
    :catch_59e
    move-exception v7

    #@59f
    .line 482
    .local v7, e:Ljava/io/IOException;
    const-string v24, "BtOppObexServer"

    #@5a1
    const-string v25, "error close file stream"

    #@5a3
    invoke-static/range {v24 .. v25}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a6
    goto :goto_52e

    #@5a7
    .line 492
    .end local v7           #e:Ljava/io/IOException;
    :cond_5a7
    const/16 v20, 0x1ea

    #@5a9
    goto :goto_556

    #@5aa
    .line 495
    :cond_5aa
    const/16 v20, 0x1ea

    #@5ac
    goto :goto_556
.end method

.method public preStart()V
    .registers 5

    #@0
    .prologue
    .line 137
    :try_start_0
    const-string v1, "BtOppObexServer"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Create ServerSession with transport "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTransport:Ljavax/obex/ObexTransport;

    #@f
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 139
    new-instance v1, Ljavax/obex/ServerSession;

    #@20
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTransport:Ljavax/obex/ObexTransport;

    #@22
    const/4 v3, 0x0

    #@23
    invoke-direct {v1, v2, p0, v3}, Ljavax/obex/ServerSession;-><init>(Ljavax/obex/ObexTransport;Ljavax/obex/ServerRequestHandler;Ljavax/obex/Authenticator;)V

    #@26
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mSession:Ljavax/obex/ServerSession;

    #@28
    .line 141
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mContext:Landroid/content/Context;

    #@2a
    invoke-static {v1}, Lcom/lge/bluetooth/LGBluetoothServiceManager;->checkBtWifiCoex(Landroid/content/Context;)V
    :try_end_2d
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_2d} :catch_2e

    #@2d
    .line 146
    :goto_2d
    return-void

    #@2e
    .line 143
    :catch_2e
    move-exception v0

    #@2f
    .line 144
    .local v0, e:Ljava/io/IOException;
    const-string v1, "BtOppObexServer"

    #@31
    new-instance v2, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v3, "Create server session error"

    #@38
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v2

    #@3c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v2

    #@40
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@43
    move-result-object v2

    #@44
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@47
    goto :goto_2d
.end method

.method public start(Landroid/os/Handler;)V
    .registers 4
    .parameter "handler"

    #@0
    .prologue
    .line 153
    const-string v0, "BtOppObexServer"

    #@2
    const-string v1, "Start!"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 155
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@9
    .line 157
    return-void
.end method

.method public stop()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 169
    const-string v1, "BtOppObexServer"

    #@3
    const-string v2, "Stop!"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 171
    const/4 v1, 0x1

    #@9
    iput-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mInterrupted:Z

    #@b
    .line 172
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mSession:Ljavax/obex/ServerSession;

    #@d
    if-eqz v1, :cond_19

    #@f
    .line 174
    :try_start_f
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mSession:Ljavax/obex/ServerSession;

    #@11
    invoke-virtual {v1}, Ljavax/obex/ServerSession;->close()V

    #@14
    .line 175
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mTransport:Ljavax/obex/ObexTransport;

    #@16
    invoke-interface {v1}, Ljavax/obex/ObexTransport;->close()V
    :try_end_19
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_19} :catch_1e

    #@19
    .line 180
    :cond_19
    :goto_19
    iput-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mCallback:Landroid/os/Handler;

    #@1b
    .line 181
    iput-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mSession:Ljavax/obex/ServerSession;

    #@1d
    .line 182
    return-void

    #@1e
    .line 176
    :catch_1e
    move-exception v0

    #@1f
    .line 177
    .local v0, e:Ljava/io/IOException;
    const-string v1, "BtOppObexServer"

    #@21
    new-instance v2, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v3, "close mTransport error"

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_19
.end method

.method public unblock()V
    .registers 2

    #@0
    .prologue
    .line 121
    const/4 v0, 0x0

    #@1
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->mServerBlocking:Z

    #@3
    .line 122
    return-void
.end method
