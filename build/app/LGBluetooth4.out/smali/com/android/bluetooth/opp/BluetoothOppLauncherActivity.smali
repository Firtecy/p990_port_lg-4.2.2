.class public Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;
.super Landroid/app/Activity;
.source "BluetoothOppLauncherActivity.java"


# static fields
.field private static final D:Z = true

.field private static final TAG:Ljava/lang/String; = "BluetoothOppLauncherActivity"

.field private static final V:Z = true


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 66
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    return-void
.end method

.method private creatFileForSharedContent(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/net/Uri;
    .registers 13
    .parameter "context"
    .parameter "shareContent"

    #@0
    .prologue
    .line 312
    if-nez p2, :cond_4

    #@2
    .line 313
    const/4 v4, 0x0

    #@3
    .line 359
    :cond_3
    :goto_3
    return-object v4

    #@4
    .line 316
    :cond_4
    const/4 v4, 0x0

    #@5
    .line 317
    .local v4, fileUri:Landroid/net/Uri;
    const/4 v5, 0x0

    #@6
    .line 319
    .local v5, outStream:Ljava/io/FileOutputStream;
    :try_start_6
    new-instance v7, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const v8, 0x7f070061

    #@e
    invoke-virtual {p0, v8}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->getString(I)Ljava/lang/String;

    #@11
    move-result-object v8

    #@12
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v7

    #@16
    const-string v8, ".html"

    #@18
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v7

    #@1c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    .line 320
    .local v3, fileName:Ljava/lang/String;
    invoke-virtual {p1, v3}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    #@23
    .line 322
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@26
    move-result-object v6

    #@27
    .line 324
    .local v6, uri:Ljava/lang/String;
    invoke-static {v6}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->makeHtmlCodeToString(Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    .line 331
    .local v1, content:Ljava/lang/String;
    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    #@2e
    move-result-object v0

    #@2f
    .line 333
    .local v0, byteBuff:[B
    const/4 v7, 0x0

    #@30
    invoke-virtual {p1, v3, v7}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    #@33
    move-result-object v5

    #@34
    .line 334
    if-eqz v5, :cond_66

    #@36
    .line 335
    const/4 v7, 0x0

    #@37
    array-length v8, v0

    #@38
    invoke-virtual {v5, v0, v7, v8}, Ljava/io/FileOutputStream;->write([BII)V

    #@3b
    .line 336
    new-instance v7, Ljava/io/File;

    #@3d
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    #@40
    move-result-object v8

    #@41
    invoke-direct {v7, v8, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    #@44
    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@47
    move-result-object v4

    #@48
    .line 337
    if-eqz v4, :cond_66

    #@4a
    .line 339
    const-string v7, "BluetoothOppLauncherActivity"

    #@4c
    new-instance v8, Ljava/lang/StringBuilder;

    #@4e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@51
    const-string v9, "Created one file for shared content: "

    #@53
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@56
    move-result-object v8

    #@57
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@5a
    move-result-object v9

    #@5b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v8

    #@63
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_66
    .catchall {:try_start_6 .. :try_end_66} :catchall_f2
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_66} :catch_71
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_66} :catch_9e
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_66} :catch_c8

    #@66
    .line 352
    :cond_66
    if-eqz v5, :cond_3

    #@68
    .line 353
    :try_start_68
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_6b
    .catch Ljava/io/IOException; {:try_start_68 .. :try_end_6b} :catch_6c

    #@6b
    goto :goto_3

    #@6c
    .line 355
    :catch_6c
    move-exception v2

    #@6d
    .line 356
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@70
    goto :goto_3

    #@71
    .line 343
    .end local v0           #byteBuff:[B
    .end local v1           #content:Ljava/lang/String;
    .end local v2           #e:Ljava/io/IOException;
    .end local v3           #fileName:Ljava/lang/String;
    .end local v6           #uri:Ljava/lang/String;
    :catch_71
    move-exception v2

    #@72
    .line 344
    .local v2, e:Ljava/io/FileNotFoundException;
    :try_start_72
    const-string v7, "BluetoothOppLauncherActivity"

    #@74
    new-instance v8, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v9, "FileNotFoundException: "

    #@7b
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v8

    #@7f
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    #@82
    move-result-object v9

    #@83
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v8

    #@87
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v8

    #@8b
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 345
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_91
    .catchall {:try_start_72 .. :try_end_91} :catchall_f2

    #@91
    .line 352
    if-eqz v5, :cond_3

    #@93
    .line 353
    :try_start_93
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_96
    .catch Ljava/io/IOException; {:try_start_93 .. :try_end_96} :catch_98

    #@96
    goto/16 :goto_3

    #@98
    .line 355
    :catch_98
    move-exception v2

    #@99
    .line 356
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@9c
    goto/16 :goto_3

    #@9e
    .line 346
    .end local v2           #e:Ljava/io/IOException;
    :catch_9e
    move-exception v2

    #@9f
    .line 347
    .restart local v2       #e:Ljava/io/IOException;
    :try_start_9f
    const-string v7, "BluetoothOppLauncherActivity"

    #@a1
    new-instance v8, Ljava/lang/StringBuilder;

    #@a3
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a6
    const-string v9, "IOException: "

    #@a8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v8

    #@ac
    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@af
    move-result-object v9

    #@b0
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b3
    move-result-object v8

    #@b4
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b7
    move-result-object v8

    #@b8
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_bb
    .catchall {:try_start_9f .. :try_end_bb} :catchall_f2

    #@bb
    .line 352
    if-eqz v5, :cond_3

    #@bd
    .line 353
    :try_start_bd
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_c0
    .catch Ljava/io/IOException; {:try_start_bd .. :try_end_c0} :catch_c2

    #@c0
    goto/16 :goto_3

    #@c2
    .line 355
    :catch_c2
    move-exception v2

    #@c3
    .line 356
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@c6
    goto/16 :goto_3

    #@c8
    .line 348
    .end local v2           #e:Ljava/io/IOException;
    :catch_c8
    move-exception v2

    #@c9
    .line 349
    .local v2, e:Ljava/lang/Exception;
    :try_start_c9
    const-string v7, "BluetoothOppLauncherActivity"

    #@cb
    new-instance v8, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v9, "Exception: "

    #@d2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v8

    #@d6
    invoke-virtual {v2}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@d9
    move-result-object v9

    #@da
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v8

    #@de
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e1
    move-result-object v8

    #@e2
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e5
    .catchall {:try_start_c9 .. :try_end_e5} :catchall_f2

    #@e5
    .line 352
    if-eqz v5, :cond_3

    #@e7
    .line 353
    :try_start_e7
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_ea
    .catch Ljava/io/IOException; {:try_start_e7 .. :try_end_ea} :catch_ec

    #@ea
    goto/16 :goto_3

    #@ec
    .line 355
    :catch_ec
    move-exception v2

    #@ed
    .line 356
    .local v2, e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@f0
    goto/16 :goto_3

    #@f2
    .line 351
    .end local v2           #e:Ljava/io/IOException;
    :catchall_f2
    move-exception v7

    #@f3
    .line 352
    if-eqz v5, :cond_f8

    #@f5
    .line 353
    :try_start_f5
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_f8
    .catch Ljava/io/IOException; {:try_start_f5 .. :try_end_f8} :catch_f9

    #@f8
    .line 357
    :cond_f8
    :goto_f8
    throw v7

    #@f9
    .line 355
    :catch_f9
    move-exception v2

    #@fa
    .line 356
    .restart local v2       #e:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@fd
    goto :goto_f8
.end method

.method private final isBluetoothAllowed()Z
    .registers 10

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 280
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v5

    #@6
    .line 283
    .local v5, resolver:Landroid/content/ContentResolver;
    const-string v8, "airplane_mode_on"

    #@8
    invoke-static {v5, v8, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@b
    move-result v8

    #@c
    if-ne v8, v6, :cond_12

    #@e
    move v2, v6

    #@f
    .line 285
    .local v2, isAirplaneModeOn:Z
    :goto_f
    if-nez v2, :cond_14

    #@11
    .line 308
    :cond_11
    :goto_11
    return v6

    #@12
    .end local v2           #isAirplaneModeOn:Z
    :cond_12
    move v2, v7

    #@13
    .line 283
    goto :goto_f

    #@14
    .line 290
    .restart local v2       #isAirplaneModeOn:Z
    :cond_14
    const-string v8, "airplane_mode_radios"

    #@16
    invoke-static {v5, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@19
    move-result-object v0

    #@1a
    .line 292
    .local v0, airplaneModeRadios:Ljava/lang/String;
    if-nez v0, :cond_2c

    #@1c
    move v3, v6

    #@1d
    .line 294
    .local v3, isAirplaneSensitive:Z
    :goto_1d
    if-eqz v3, :cond_11

    #@1f
    .line 299
    const-string v8, "airplane_mode_toggleable_radios"

    #@21
    invoke-static {v5, v8}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    .line 301
    .local v1, airplaneModeToggleableRadios:Ljava/lang/String;
    if-nez v1, :cond_33

    #@27
    move v4, v7

    #@28
    .line 303
    .local v4, isAirplaneToggleable:Z
    :goto_28
    if-nez v4, :cond_11

    #@2a
    move v6, v7

    #@2b
    .line 308
    goto :goto_11

    #@2c
    .line 292
    .end local v1           #airplaneModeToggleableRadios:Ljava/lang/String;
    .end local v3           #isAirplaneSensitive:Z
    .end local v4           #isAirplaneToggleable:Z
    :cond_2c
    const-string v8, "bluetooth"

    #@2e
    invoke-virtual {v0, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@31
    move-result v3

    #@32
    goto :goto_1d

    #@33
    .line 301
    .restart local v1       #airplaneModeToggleableRadios:Ljava/lang/String;
    .restart local v3       #isAirplaneSensitive:Z
    :cond_33
    const-string v8, "bluetooth"

    #@35
    invoke-virtual {v1, v8}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@38
    move-result v4

    #@39
    goto :goto_28
.end method

.method private final launchDevicePicker()V
    .registers 6

    #@0
    .prologue
    const/high16 v4, 0x1000

    #@2
    .line 249
    invoke-static {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v2}, Lcom/android/bluetooth/opp/BluetoothOppManager;->isEnabled()Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_21

    #@c
    .line 251
    const-string v2, "BluetoothOppLauncherActivity"

    #@e
    const-string v3, "Prepare Enable BT!! "

    #@10
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 253
    new-instance v0, Landroid/content/Intent;

    #@15
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;

    #@17
    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@1a
    .line 254
    .local v0, in:Landroid/content/Intent;
    invoke-virtual {v0, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@1d
    .line 255
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->startActivity(Landroid/content/Intent;)V

    #@20
    .line 277
    .end local v0           #in:Landroid/content/Intent;
    :goto_20
    return-void

    #@21
    .line 258
    :cond_21
    const-string v2, "BluetoothOppLauncherActivity"

    #@23
    const-string v3, "BT already enabled!! "

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 260
    new-instance v1, Landroid/content/Intent;

    #@2a
    const-string v2, "android.bluetooth.devicepicker.action.LAUNCH"

    #@2c
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2f
    .line 261
    .local v1, in1:Landroid/content/Intent;
    const/high16 v2, 0x80

    #@31
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@34
    .line 263
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@37
    .line 265
    const-string v2, "android.bluetooth.devicepicker.extra.NEED_AUTH"

    #@39
    const/4 v3, 0x0

    #@3a
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@3d
    .line 266
    const-string v2, "android.bluetooth.devicepicker.extra.FILTER_TYPE"

    #@3f
    const/4 v3, 0x2

    #@40
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@43
    .line 268
    const-string v2, "android.bluetooth.devicepicker.extra.LAUNCH_PACKAGE"

    #@45
    const-string v3, "com.android.bluetooth"

    #@47
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@4a
    .line 270
    const-string v2, "android.bluetooth.devicepicker.extra.DEVICE_PICKER_LAUNCH_CLASS"

    #@4c
    const-class v3, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@4e
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@51
    move-result-object v3

    #@52
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@55
    .line 273
    const-string v2, "BluetoothOppLauncherActivity"

    #@57
    const-string v3, "Launching android.bluetooth.devicepicker.action.LAUNCH"

    #@59
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 275
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->startActivity(Landroid/content/Intent;)V

    #@5f
    goto :goto_20
.end method

.method private startOppSendService(Landroid/content/Intent;)V
    .registers 4
    .parameter "sendTypeIntent"

    #@0
    .prologue
    .line 364
    invoke-static {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@3
    move-result-object v1

    #@4
    iput-object p1, v1, Lcom/android/bluetooth/opp/BluetoothOppManager;->mSendTypeIntent:Landroid/content/Intent;

    #@6
    .line 365
    invoke-static {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->isEnabled()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_17

    #@10
    .line 366
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->launchDevicePicker()V

    #@13
    .line 377
    :goto_13
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@16
    .line 378
    return-void

    #@17
    .line 369
    :cond_17
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothServiceManager;->checkBtWifiCoex(Landroid/content/Context;)V

    #@1a
    .line 372
    new-instance v0, Landroid/content/Intent;

    #@1c
    const-string v1, "com.lge.bluetooth.opp.sendservice"

    #@1e
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@21
    .line 373
    .local v0, serviceIntent:Landroid/content/Intent;
    const-string v1, "intent"

    #@23
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@26
    .line 374
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@29
    .line 375
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->launchDevicePicker()V

    #@2c
    goto :goto_13
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 16
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v11, 0x0

    #@1
    .line 73
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@4
    .line 75
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->getIntent()Landroid/content/Intent;

    #@7
    move-result-object v4

    #@8
    .line 80
    .local v4, intent:Landroid/content/Intent;
    if-nez v4, :cond_18

    #@a
    move-object v0, v11

    #@b
    .line 81
    .local v0, action:Ljava/lang/String;
    :goto_b
    if-nez v0, :cond_1d

    #@d
    .line 82
    const-string v11, "BluetoothOppLauncherActivity"

    #@f
    const-string v12, "Unexpected error! action is null"

    #@11
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 83
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@17
    .line 239
    :cond_17
    :goto_17
    return-void

    #@18
    .line 80
    .end local v0           #action:Ljava/lang/String;
    :cond_18
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    goto :goto_b

    #@1d
    .line 89
    .restart local v0       #action:Ljava/lang/String;
    :cond_1d
    sget-boolean v12, Lcom/lge/config/ConfigBuildFlags;->CAPP_MDM:Z

    #@1f
    if-eqz v12, :cond_33

    #@21
    invoke-static {}, Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;->getInstance()Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;

    #@24
    move-result-object v12

    #@25
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->getApplicationContext()Landroid/content/Context;

    #@28
    move-result-object v13

    #@29
    invoke-virtual {v12, v11, v13}, Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;->checkBluetoothAllowed(Landroid/content/ComponentName;Landroid/content/Context;)Z

    #@2c
    move-result v11

    #@2d
    if-nez v11, :cond_33

    #@2f
    .line 92
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@32
    goto :goto_17

    #@33
    .line 97
    :cond_33
    const-string v11, "android.intent.action.SEND"

    #@35
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@38
    move-result v11

    #@39
    if-nez v11, :cond_43

    #@3b
    const-string v11, "android.intent.action.SEND_MULTIPLE"

    #@3d
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@40
    move-result v11

    #@41
    if-eqz v11, :cond_167

    #@43
    .line 99
    :cond_43
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->isBluetoothAllowed()Z

    #@46
    move-result v11

    #@47
    if-nez v11, :cond_74

    #@49
    .line 100
    new-instance v3, Landroid/content/Intent;

    #@4b
    const-class v11, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;

    #@4d
    invoke-direct {v3, p0, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@50
    .line 101
    .local v3, in:Landroid/content/Intent;
    const/high16 v11, 0x1000

    #@52
    invoke-virtual {v3, v11}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@55
    .line 102
    const-string v11, "title"

    #@57
    const v12, 0x7f07000a

    #@5a
    invoke-virtual {p0, v12}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->getString(I)Ljava/lang/String;

    #@5d
    move-result-object v12

    #@5e
    invoke-virtual {v3, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@61
    .line 103
    const-string v11, "content"

    #@63
    const v12, 0x7f07000b

    #@66
    invoke-virtual {p0, v12}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->getString(I)Ljava/lang/String;

    #@69
    move-result-object v12

    #@6a
    invoke-virtual {v3, v11, v12}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@6d
    .line 104
    invoke-virtual {p0, v3}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->startActivity(Landroid/content/Intent;)V

    #@70
    .line 105
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@73
    goto :goto_17

    #@74
    .line 114
    .end local v3           #in:Landroid/content/Intent;
    :cond_74
    const-string v11, "android.intent.action.SEND"

    #@76
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@79
    move-result v11

    #@7a
    if-eqz v11, :cond_112

    #@7c
    .line 116
    invoke-virtual {v4}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@7f
    move-result-object v8

    #@80
    .line 117
    .local v8, type:Ljava/lang/String;
    const-string v11, "android.intent.extra.STREAM"

    #@82
    invoke-virtual {v4, v11}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@85
    move-result-object v7

    #@86
    check-cast v7, Landroid/net/Uri;

    #@88
    .line 118
    .local v7, stream:Landroid/net/Uri;
    const-string v11, "android.intent.extra.TEXT"

    #@8a
    invoke-virtual {v4, v11}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    #@8d
    move-result-object v1

    #@8e
    .line 121
    .local v1, extra_text:Ljava/lang/CharSequence;
    const/4 v11, 0x0

    #@8f
    invoke-static {v4, v11, p0}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->isDrmFileCheck(Landroid/content/Intent;ZLandroid/content/Context;)Z

    #@92
    move-result v11

    #@93
    if-eqz v11, :cond_9a

    #@95
    .line 122
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@98
    goto/16 :goto_17

    #@9a
    .line 132
    :cond_9a
    if-eqz v7, :cond_c5

    #@9c
    if-eqz v8, :cond_c5

    #@9e
    .line 134
    const-string v11, "BluetoothOppLauncherActivity"

    #@a0
    new-instance v12, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v13, "Get ACTION_SEND intent: Uri = "

    #@a7
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v12

    #@ab
    invoke-virtual {v12, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v12

    #@af
    const-string v13, "; mimetype = "

    #@b1
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v12

    #@b5
    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v12

    #@b9
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bc
    move-result-object v12

    #@bd
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    .line 151
    invoke-direct {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->startOppSendService(Landroid/content/Intent;)V

    #@c3
    goto/16 :goto_17

    #@c5
    .line 154
    :cond_c5
    if-eqz v1, :cond_106

    #@c7
    if-eqz v8, :cond_106

    #@c9
    .line 156
    const-string v11, "BluetoothOppLauncherActivity"

    #@cb
    new-instance v12, Ljava/lang/StringBuilder;

    #@cd
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@d0
    const-string v13, "Get ACTION_SEND intent with Extra_text = "

    #@d2
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v12

    #@d6
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d9
    move-result-object v13

    #@da
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v12

    #@de
    const-string v13, "; mimetype = "

    #@e0
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v12

    #@e4
    invoke-virtual {v12, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v12

    #@e8
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v12

    #@ec
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    .line 158
    invoke-direct {p0, p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->creatFileForSharedContent(Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/net/Uri;

    #@f2
    move-result-object v2

    #@f3
    .line 159
    .local v2, fileUri:Landroid/net/Uri;
    if-eqz v2, :cond_fa

    #@f5
    .line 174
    invoke-direct {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->startOppSendService(Landroid/content/Intent;)V

    #@f8
    goto/16 :goto_17

    #@fa
    .line 178
    :cond_fa
    const-string v11, "BluetoothOppLauncherActivity"

    #@fc
    const-string v12, "Error trying to do set text...File not created!"

    #@fe
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    .line 179
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@104
    goto/16 :goto_17

    #@106
    .line 183
    .end local v2           #fileUri:Landroid/net/Uri;
    :cond_106
    const-string v11, "BluetoothOppLauncherActivity"

    #@108
    const-string v12, "type is null; or sending file URI is null"

    #@10a
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10d
    .line 184
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@110
    goto/16 :goto_17

    #@112
    .line 187
    .end local v1           #extra_text:Ljava/lang/CharSequence;
    .end local v7           #stream:Landroid/net/Uri;
    .end local v8           #type:Ljava/lang/String;
    :cond_112
    const-string v11, "android.intent.action.SEND_MULTIPLE"

    #@114
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@117
    move-result v11

    #@118
    if-eqz v11, :cond_17

    #@11a
    .line 189
    const/4 v11, 0x1

    #@11b
    invoke-static {v4, v11, p0}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->isDrmFileCheck(Landroid/content/Intent;ZLandroid/content/Context;)Z

    #@11e
    move-result v11

    #@11f
    if-eqz v11, :cond_126

    #@121
    .line 190
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@124
    goto/16 :goto_17

    #@126
    .line 194
    :cond_126
    invoke-virtual {v4}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@129
    move-result-object v6

    #@12a
    .line 195
    .local v6, mimeType:Ljava/lang/String;
    const-string v11, "android.intent.extra.STREAM"

    #@12c
    invoke-virtual {v4, v11}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    #@12f
    move-result-object v10

    #@130
    .line 196
    .local v10, uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v6, :cond_15b

    #@132
    if-eqz v10, :cond_15b

    #@134
    .line 198
    const-string v11, "BluetoothOppLauncherActivity"

    #@136
    new-instance v12, Ljava/lang/StringBuilder;

    #@138
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@13b
    const-string v13, "Get ACTION_SHARE_MULTIPLE intent: uris "

    #@13d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@140
    move-result-object v12

    #@141
    invoke-virtual {v12, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v12

    #@145
    const-string v13, "\n Type= "

    #@147
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14a
    move-result-object v12

    #@14b
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14e
    move-result-object v12

    #@14f
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@152
    move-result-object v12

    #@153
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@156
    .line 214
    invoke-direct {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->startOppSendService(Landroid/content/Intent;)V

    #@159
    goto/16 :goto_17

    #@15b
    .line 218
    :cond_15b
    const-string v11, "BluetoothOppLauncherActivity"

    #@15d
    const-string v12, "type is null; or sending files URIs are null"

    #@15f
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@162
    .line 219
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@165
    goto/16 :goto_17

    #@167
    .line 223
    .end local v6           #mimeType:Ljava/lang/String;
    .end local v10           #uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_167
    const-string v11, "android.btopp.intent.action.OPEN"

    #@169
    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16c
    move-result v11

    #@16d
    if-eqz v11, :cond_1ad

    #@16f
    .line 224
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->getIntent()Landroid/content/Intent;

    #@172
    move-result-object v11

    #@173
    invoke-virtual {v11}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@176
    move-result-object v9

    #@177
    .line 226
    .local v9, uri:Landroid/net/Uri;
    const-string v11, "BluetoothOppLauncherActivity"

    #@179
    new-instance v12, Ljava/lang/StringBuilder;

    #@17b
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@17e
    const-string v13, "Get ACTION_OPEN intent: Uri = "

    #@180
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@183
    move-result-object v12

    #@184
    invoke-virtual {v12, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v12

    #@188
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18b
    move-result-object v12

    #@18c
    invoke-static {v11, v12}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18f
    .line 229
    new-instance v5, Landroid/content/Intent;

    #@191
    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    #@194
    .line 230
    .local v5, intent1:Landroid/content/Intent;
    invoke-virtual {v5, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@197
    .line 231
    const-string v11, "com.android.bluetooth"

    #@199
    const-class v12, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@19b
    invoke-virtual {v12}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@19e
    move-result-object v12

    #@19f
    invoke-virtual {v5, v11, v12}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1a2
    .line 232
    invoke-virtual {v5, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@1a5
    .line 233
    invoke-virtual {p0, v5}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->sendBroadcast(Landroid/content/Intent;)V

    #@1a8
    .line 234
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@1ab
    goto/16 :goto_17

    #@1ad
    .line 236
    .end local v5           #intent1:Landroid/content/Intent;
    .end local v9           #uri:Landroid/net/Uri;
    :cond_1ad
    const-string v11, "BluetoothOppLauncherActivity"

    #@1af
    new-instance v12, Ljava/lang/StringBuilder;

    #@1b1
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@1b4
    const-string v13, "Unsupported action: "

    #@1b6
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b9
    move-result-object v12

    #@1ba
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bd
    move-result-object v12

    #@1be
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c1
    move-result-object v12

    #@1c2
    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1c5
    .line 237
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLauncherActivity;->finish()V

    #@1c8
    goto/16 :goto_17
.end method
