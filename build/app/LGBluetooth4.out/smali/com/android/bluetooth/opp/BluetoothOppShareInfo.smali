.class public Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
.super Ljava/lang/Object;
.source "BluetoothOppShareInfo.java"


# instance fields
.field public mConfirm:I

.field public mCurrentBytes:I

.field public mDestination:Ljava/lang/String;

.field public mDirection:I

.field public mFilename:Ljava/lang/String;

.field public mHint:Ljava/lang/String;

.field public mId:I

.field public mMediaScanned:Z

.field public mMimetype:Ljava/lang/String;

.field public mStatus:I

.field public mTimestamp:J

.field public mTotalBytes:I

.field public mUri:Landroid/net/Uri;

.field public mVisibility:I


# direct methods
.method public constructor <init>(ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IIIIIIZ)V
    .registers 18
    .parameter "id"
    .parameter "uri"
    .parameter "hint"
    .parameter "filename"
    .parameter "mimetype"
    .parameter "direction"
    .parameter "destination"
    .parameter "visibility"
    .parameter "confirm"
    .parameter "status"
    .parameter "totalBytes"
    .parameter "currentBytes"
    .parameter "timestamp"
    .parameter "mediaScanned"

    #@0
    .prologue
    .line 73
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 74
    iput p1, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@5
    .line 75
    iput-object p2, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@7
    .line 76
    iput-object p3, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mHint:Ljava/lang/String;

    #@9
    .line 77
    iput-object p4, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mFilename:Ljava/lang/String;

    #@b
    .line 78
    iput-object p5, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mMimetype:Ljava/lang/String;

    #@d
    .line 79
    iput p6, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@f
    .line 80
    iput-object p7, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDestination:Ljava/lang/String;

    #@11
    .line 81
    iput p8, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mVisibility:I

    #@13
    .line 82
    iput p9, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mConfirm:I

    #@15
    .line 83
    iput p10, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@17
    .line 84
    iput p11, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mTotalBytes:I

    #@19
    .line 85
    iput p12, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mCurrentBytes:I

    #@1b
    .line 86
    move/from16 v0, p13

    #@1d
    int-to-long v1, v0

    #@1e
    iput-wide v1, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mTimestamp:J

    #@20
    .line 87
    move/from16 v0, p14

    #@22
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mMediaScanned:Z

    #@24
    .line 88
    return-void
.end method


# virtual methods
.method public hasCompletionNotification()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 109
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@3
    invoke-static {v1}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z

    #@6
    move-result v1

    #@7
    if-nez v1, :cond_a

    #@9
    .line 115
    :cond_9
    :goto_9
    return v0

    #@a
    .line 112
    :cond_a
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mVisibility:I

    #@c
    if-nez v1, :cond_9

    #@e
    .line 113
    const/4 v0, 0x1

    #@f
    goto :goto_9
.end method

.method public isObsolete()Z
    .registers 3

    #@0
    .prologue
    .line 122
    const/16 v0, 0xc0

    #@2
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@4
    if-ne v0, v1, :cond_8

    #@6
    .line 123
    const/4 v0, 0x1

    #@7
    .line 125
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public isReadyToStart()Z
    .registers 4

    #@0
    .prologue
    const/16 v2, 0xbe

    #@2
    const/4 v0, 0x1

    #@3
    .line 95
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@5
    if-nez v1, :cond_10

    #@7
    .line 96
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@9
    if-ne v1, v2, :cond_18

    #@b
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@d
    if-eqz v1, :cond_18

    #@f
    .line 105
    :cond_f
    :goto_f
    return v0

    #@10
    .line 99
    :cond_10
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@12
    if-ne v1, v0, :cond_18

    #@14
    .line 100
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@16
    if-eq v1, v2, :cond_f

    #@18
    .line 105
    :cond_18
    const/4 v0, 0x0

    #@19
    goto :goto_f
.end method
