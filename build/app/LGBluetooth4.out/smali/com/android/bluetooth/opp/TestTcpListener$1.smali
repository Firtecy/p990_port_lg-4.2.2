.class Lcom/android/bluetooth/opp/TestTcpListener$1;
.super Ljava/lang/Thread;
.source "TestActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/bluetooth/opp/TestTcpListener;->start(Landroid/os/Handler;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mServerSocket:Ljava/net/ServerSocket;

.field final synthetic this$0:Lcom/android/bluetooth/opp/TestTcpListener;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/opp/TestTcpListener;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 385
    iput-object p1, p0, Lcom/android/bluetooth/opp/TestTcpListener$1;->this$0:Lcom/android/bluetooth/opp/TestTcpListener;

    #@2
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 390
    const-string v4, "BtOppRfcommListener"

    #@3
    const-string v5, "RfcommSocket listen thread starting"

    #@5
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 394
    :try_start_8
    const-string v4, "BtOppRfcommListener"

    #@a
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "Create server RfcommSocket on channel"

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    iget-object v6, p0, Lcom/android/bluetooth/opp/TestTcpListener$1;->this$0:Lcom/android/bluetooth/opp/TestTcpListener;

    #@17
    invoke-static {v6}, Lcom/android/bluetooth/opp/TestTcpListener;->access$000(Lcom/android/bluetooth/opp/TestTcpListener;)I

    #@1a
    move-result v6

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v5

    #@23
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 397
    new-instance v4, Ljava/net/ServerSocket;

    #@28
    const/16 v5, 0x1964

    #@2a
    const/4 v6, 0x1

    #@2b
    invoke-direct {v4, v5, v6}, Ljava/net/ServerSocket;-><init>(II)V

    #@2e
    iput-object v4, p0, Lcom/android/bluetooth/opp/TestTcpListener$1;->mServerSocket:Ljava/net/ServerSocket;
    :try_end_30
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_30} :catch_5e

    #@30
    .line 402
    :cond_30
    :goto_30
    iget-object v4, p0, Lcom/android/bluetooth/opp/TestTcpListener$1;->this$0:Lcom/android/bluetooth/opp/TestTcpListener;

    #@32
    #getter for: Lcom/android/bluetooth/opp/TestTcpListener;->mInterrupted:Z
    invoke-static {v4}, Lcom/android/bluetooth/opp/TestTcpListener;->access$100(Lcom/android/bluetooth/opp/TestTcpListener;)Z

    #@35
    move-result v4

    #@36
    if-nez v4, :cond_f8

    #@38
    .line 404
    :try_start_38
    iget-object v4, p0, Lcom/android/bluetooth/opp/TestTcpListener$1;->mServerSocket:Ljava/net/ServerSocket;

    #@3a
    const/16 v5, 0x1388

    #@3c
    invoke-virtual {v4, v5}, Ljava/net/ServerSocket;->setSoTimeout(I)V

    #@3f
    .line 405
    iget-object v4, p0, Lcom/android/bluetooth/opp/TestTcpListener$1;->mServerSocket:Ljava/net/ServerSocket;

    #@41
    invoke-virtual {v4}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    #@44
    move-result-object v0

    #@45
    .line 406
    .local v0, clientSocket:Ljava/net/Socket;
    if-nez v0, :cond_83

    #@47
    .line 408
    const-string v4, "BtOppRfcommListener"

    #@49
    const-string v5, "incomming connection time out"

    #@4b
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4e
    .catch Ljava/net/SocketException; {:try_start_38 .. :try_end_4e} :catch_c2
    .catch Ljava/io/IOException; {:try_start_38 .. :try_end_4e} :catch_dd

    #@4e
    .line 429
    .end local v0           #clientSocket:Ljava/net/Socket;
    :goto_4e
    iget-object v4, p0, Lcom/android/bluetooth/opp/TestTcpListener$1;->this$0:Lcom/android/bluetooth/opp/TestTcpListener;

    #@50
    #getter for: Lcom/android/bluetooth/opp/TestTcpListener;->mInterrupted:Z
    invoke-static {v4}, Lcom/android/bluetooth/opp/TestTcpListener;->access$100(Lcom/android/bluetooth/opp/TestTcpListener;)Z

    #@53
    move-result v4

    #@54
    if-eqz v4, :cond_30

    #@56
    .line 430
    const-string v4, "BtOppRfcommListener"

    #@58
    const-string v5, "socketAcceptThread thread was interrupted (2), exiting"

    #@5a
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    goto :goto_30

    #@5e
    .line 398
    :catch_5e
    move-exception v1

    #@5f
    .line 399
    .local v1, e:Ljava/io/IOException;
    const-string v4, "BtOppRfcommListener"

    #@61
    new-instance v5, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v6, "Error listing on channel"

    #@68
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v5

    #@6c
    iget-object v6, p0, Lcom/android/bluetooth/opp/TestTcpListener$1;->this$0:Lcom/android/bluetooth/opp/TestTcpListener;

    #@6e
    invoke-static {v6}, Lcom/android/bluetooth/opp/TestTcpListener;->access$000(Lcom/android/bluetooth/opp/TestTcpListener;)I

    #@71
    move-result v6

    #@72
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@75
    move-result-object v5

    #@76
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v5

    #@7a
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 400
    iget-object v4, p0, Lcom/android/bluetooth/opp/TestTcpListener$1;->this$0:Lcom/android/bluetooth/opp/TestTcpListener;

    #@7f
    #setter for: Lcom/android/bluetooth/opp/TestTcpListener;->mInterrupted:Z
    invoke-static {v4, v7}, Lcom/android/bluetooth/opp/TestTcpListener;->access$102(Lcom/android/bluetooth/opp/TestTcpListener;Z)Z

    #@82
    goto :goto_30

    #@83
    .line 412
    .end local v1           #e:Ljava/io/IOException;
    .restart local v0       #clientSocket:Ljava/net/Socket;
    :cond_83
    :try_start_83
    const-string v4, "BtOppRfcommListener"

    #@85
    const-string v5, "RfcommSocket connected!"

    #@87
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    .line 414
    const-string v4, "BtOppRfcommListener"

    #@8c
    new-instance v5, Ljava/lang/StringBuilder;

    #@8e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@91
    const-string v6, "remote addr is "

    #@93
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@96
    move-result-object v5

    #@97
    invoke-virtual {v0}, Ljava/net/Socket;->getRemoteSocketAddress()Ljava/net/SocketAddress;

    #@9a
    move-result-object v6

    #@9b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v5

    #@9f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v5

    #@a3
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 416
    new-instance v3, Lcom/android/bluetooth/opp/TestTcpTransport;

    #@a8
    invoke-direct {v3, v0}, Lcom/android/bluetooth/opp/TestTcpTransport;-><init>(Ljava/net/Socket;)V

    #@ab
    .line 417
    .local v3, transport:Lcom/android/bluetooth/opp/TestTcpTransport;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@ae
    move-result-object v2

    #@af
    .line 418
    .local v2, msg:Landroid/os/Message;
    iget-object v4, p0, Lcom/android/bluetooth/opp/TestTcpListener$1;->this$0:Lcom/android/bluetooth/opp/TestTcpListener;

    #@b1
    invoke-static {v4}, Lcom/android/bluetooth/opp/TestTcpListener;->access$200(Lcom/android/bluetooth/opp/TestTcpListener;)Landroid/os/Handler;

    #@b4
    move-result-object v4

    #@b5
    invoke-virtual {v2, v4}, Landroid/os/Message;->setTarget(Landroid/os/Handler;)V

    #@b8
    .line 419
    const/16 v4, 0x64

    #@ba
    iput v4, v2, Landroid/os/Message;->what:I

    #@bc
    .line 420
    iput-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@be
    .line 421
    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V
    :try_end_c1
    .catch Ljava/net/SocketException; {:try_start_83 .. :try_end_c1} :catch_c2
    .catch Ljava/io/IOException; {:try_start_83 .. :try_end_c1} :catch_dd

    #@c1
    goto :goto_4e

    #@c2
    .line 423
    .end local v0           #clientSocket:Ljava/net/Socket;
    .end local v2           #msg:Landroid/os/Message;
    .end local v3           #transport:Lcom/android/bluetooth/opp/TestTcpTransport;
    :catch_c2
    move-exception v1

    #@c3
    .line 424
    .local v1, e:Ljava/net/SocketException;
    const-string v4, "BtOppRfcommListener"

    #@c5
    new-instance v5, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v6, "Error accept connection "

    #@cc
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v5

    #@d0
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v5

    #@d4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d7
    move-result-object v5

    #@d8
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@db
    goto/16 :goto_4e

    #@dd
    .line 425
    .end local v1           #e:Ljava/net/SocketException;
    :catch_dd
    move-exception v1

    #@de
    .line 426
    .local v1, e:Ljava/io/IOException;
    const-string v4, "BtOppRfcommListener"

    #@e0
    new-instance v5, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v6, "Error accept connection "

    #@e7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v5

    #@eb
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v5

    #@ef
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f2
    move-result-object v5

    #@f3
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f6
    goto/16 :goto_4e

    #@f8
    .line 434
    .end local v1           #e:Ljava/io/IOException;
    :cond_f8
    const-string v4, "BtOppRfcommListener"

    #@fa
    const-string v5, "RfcommSocket listen thread finished"

    #@fc
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    .line 436
    return-void
.end method
