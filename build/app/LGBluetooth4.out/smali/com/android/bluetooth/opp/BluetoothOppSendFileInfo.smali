.class public Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
.super Ljava/lang/Object;
.source "BluetoothOppSendFileInfo.java"


# static fields
.field private static final D:Z = true

.field static final SEND_FILE_INFO_ERROR:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo; = null

.field private static final TAG:Ljava/lang/String; = "BluetoothOppSendFileInfo"

.field private static final V:Z = true


# instance fields
.field public final mData:Ljava/lang/String;

.field public final mFileName:Ljava/lang/String;

.field public final mInputStream:Ljava/io/FileInputStream;

.field public final mLength:J

.field public final mMimetype:Ljava/lang/String;

.field public final mStatus:I


# direct methods
.method static constructor <clinit>()V
    .registers 7

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 64
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@3
    const-wide/16 v3, 0x0

    #@5
    const/16 v6, 0x1ec

    #@7
    move-object v2, v1

    #@8
    move-object v5, v1

    #@9
    invoke-direct/range {v0 .. v6}, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/io/FileInputStream;I)V

    #@c
    sput-object v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->SEND_FILE_INFO_ERROR:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@e
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JI)V
    .registers 7
    .parameter "data"
    .parameter "type"
    .parameter "length"
    .parameter "status"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 94
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 95
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@6
    .line 96
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mInputStream:Ljava/io/FileInputStream;

    #@8
    .line 97
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mData:Ljava/lang/String;

    #@a
    .line 98
    iput-object p2, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mMimetype:Ljava/lang/String;

    #@c
    .line 99
    iput-wide p3, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@e
    .line 100
    iput p5, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mStatus:I

    #@10
    .line 101
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/io/FileInputStream;I)V
    .registers 8
    .parameter "fileName"
    .parameter "type"
    .parameter "length"
    .parameter "inputStream"
    .parameter "status"

    #@0
    .prologue
    .line 84
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 85
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mFileName:Ljava/lang/String;

    #@5
    .line 86
    iput-object p2, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mMimetype:Ljava/lang/String;

    #@7
    .line 87
    iput-wide p3, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mLength:J

    #@9
    .line 88
    iput-object p5, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mInputStream:Ljava/io/FileInputStream;

    #@b
    .line 89
    iput p6, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mStatus:I

    #@d
    .line 90
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mData:Ljava/lang/String;

    #@10
    .line 91
    return-void
.end method

.method public static generateFileInfo(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    .registers 26
    .parameter "context"
    .parameter "uri"
    .parameter "type"

    #@0
    .prologue
    .line 105
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v2

    #@4
    .line 106
    .local v2, contentResolver:Landroid/content/ContentResolver;
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@7
    move-result-object v18

    #@8
    .line 107
    .local v18, scheme:Ljava/lang/String;
    const/4 v14, 0x0

    #@9
    .line 109
    .local v14, fileName:Ljava/lang/String;
    const-wide/16 v15, 0x0

    #@b
    .line 113
    .local v15, length:J
    const-string v3, "content"

    #@d
    move-object/from16 v0, v18

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_11f

    #@15
    .line 114
    move-object/from16 v0, p1

    #@17
    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    #@1a
    move-result-object v10

    #@1b
    .line 117
    .local v10, contentType:Ljava/lang/String;
    const/4 v3, 0x2

    #@1c
    :try_start_1c
    new-array v4, v3, [Ljava/lang/String;

    #@1e
    const/4 v3, 0x0

    #@1f
    const-string v9, "_display_name"

    #@21
    aput-object v9, v4, v3

    #@23
    const/4 v3, 0x1

    #@24
    const-string v9, "_size"

    #@26
    aput-object v9, v4, v3

    #@28
    const/4 v5, 0x0

    #@29
    const/4 v6, 0x0

    #@2a
    const/4 v7, 0x0

    #@2b
    move-object/from16 v3, p1

    #@2d
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_30
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1c .. :try_end_30} :catch_10e

    #@30
    move-result-object v17

    #@31
    .line 124
    .local v17, metadataCursor:Landroid/database/Cursor;
    :goto_31
    if-eqz v17, :cond_162

    #@33
    .line 126
    :try_start_33
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->moveToFirst()Z

    #@36
    move-result v3

    #@37
    if-eqz v3, :cond_15e

    #@39
    .line 127
    const/4 v3, 0x0

    #@3a
    move-object/from16 v0, v17

    #@3c
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_3f
    .catchall {:try_start_33 .. :try_end_3f} :catchall_113

    #@3f
    move-result-object v4

    #@40
    .line 128
    .end local v14           #fileName:Ljava/lang/String;
    .local v4, fileName:Ljava/lang/String;
    const/4 v3, 0x1

    #@41
    :try_start_41
    move-object/from16 v0, v17

    #@43
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I
    :try_end_46
    .catchall {:try_start_41 .. :try_end_46} :catchall_15c

    #@46
    move-result v3

    #@47
    int-to-long v6, v3

    #@48
    .line 138
    .end local v15           #length:J
    .local v6, length:J
    :goto_48
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@4b
    .line 141
    :goto_4b
    if-nez v4, :cond_76

    #@4d
    .line 147
    const-string v3, "text/x-vcard"

    #@4f
    move-object/from16 v0, p2

    #@51
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v3

    #@55
    if-eqz v3, :cond_119

    #@57
    .line 148
    new-instance v3, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v9, "tx_vcard"

    #@5e
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v3

    #@62
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@65
    move-result-wide v21

    #@66
    move-wide/from16 v0, v21

    #@68
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    const-string v9, ".vcf"

    #@6e
    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@75
    move-result-object v4

    #@76
    :cond_76
    :goto_76
    move-object v5, v10

    #@77
    .line 164
    .end local v10           #contentType:Ljava/lang/String;
    .end local v17           #metadataCursor:Landroid/database/Cursor;
    .local v5, contentType:Ljava/lang/String;
    :goto_77
    const/4 v8, 0x0

    #@78
    .line 165
    .local v8, is:Ljava/io/FileInputStream;
    const-string v3, "content"

    #@7a
    move-object/from16 v0, v18

    #@7c
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7f
    move-result v3

    #@80
    if-eqz v3, :cond_d8

    #@82
    .line 171
    :try_start_82
    const-string v3, "r"

    #@84
    move-object/from16 v0, p1

    #@86
    invoke-virtual {v2, v0, v3}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@89
    move-result-object v13

    #@8a
    .line 172
    .local v13, fd:Landroid/content/res/AssetFileDescriptor;
    invoke-virtual {v13}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    #@8d
    move-result-wide v19

    #@8e
    .line 173
    .local v19, statLength:J
    cmp-long v3, v6, v19

    #@90
    if-eqz v3, :cond_d4

    #@92
    const-wide/16 v21, 0x0

    #@94
    cmp-long v3, v19, v21

    #@96
    if-lez v3, :cond_d4

    #@98
    .line 174
    const-string v3, "BluetoothOppSendFileInfo"

    #@9a
    new-instance v9, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v21, "Content provider length is wrong ("

    #@a1
    move-object/from16 v0, v21

    #@a3
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v9

    #@a7
    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@aa
    move-result-object v21

    #@ab
    move-object/from16 v0, v21

    #@ad
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v9

    #@b1
    const-string v21, "), using stat length ("

    #@b3
    move-object/from16 v0, v21

    #@b5
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v9

    #@b9
    invoke-static/range {v19 .. v20}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@bc
    move-result-object v21

    #@bd
    move-object/from16 v0, v21

    #@bf
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v9

    #@c3
    const-string v21, ")"

    #@c5
    move-object/from16 v0, v21

    #@c7
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v9

    #@cb
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v9

    #@cf
    invoke-static {v3, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d2
    .catch Ljava/io/FileNotFoundException; {:try_start_82 .. :try_end_d2} :catch_159

    #@d2
    .line 176
    move-wide/from16 v6, v19

    #@d4
    .line 182
    :cond_d4
    :try_start_d4
    invoke-virtual {v13}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;
    :try_end_d7
    .catch Ljava/io/IOException; {:try_start_d4 .. :try_end_d7} :catch_143
    .catch Ljava/io/FileNotFoundException; {:try_start_d4 .. :try_end_d7} :catch_159

    #@d7
    move-result-object v8

    #@d8
    .line 194
    .end local v13           #fd:Landroid/content/res/AssetFileDescriptor;
    .end local v19           #statLength:J
    :cond_d8
    :goto_d8
    if-nez v8, :cond_e2

    #@da
    .line 196
    :try_start_da
    move-object/from16 v0, p1

    #@dc
    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    #@df
    move-result-object v8

    #@e0
    .end local v8           #is:Ljava/io/FileInputStream;
    check-cast v8, Ljava/io/FileInputStream;
    :try_end_e2
    .catch Ljava/io/FileNotFoundException; {:try_start_da .. :try_end_e2} :catch_14a

    #@e2
    .line 203
    .restart local v8       #is:Ljava/io/FileInputStream;
    :cond_e2
    const-wide/16 v21, 0x0

    #@e4
    cmp-long v3, v6, v21

    #@e6
    if-nez v3, :cond_107

    #@e8
    .line 205
    :try_start_e8
    invoke-virtual {v8}, Ljava/io/FileInputStream;->available()I

    #@eb
    move-result v3

    #@ec
    int-to-long v6, v3

    #@ed
    .line 207
    const-string v3, "BluetoothOppSendFileInfo"

    #@ef
    new-instance v9, Ljava/lang/StringBuilder;

    #@f1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@f4
    const-string v21, "file length is "

    #@f6
    move-object/from16 v0, v21

    #@f8
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v9

    #@fc
    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v9

    #@100
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v9

    #@104
    invoke-static {v3, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_107
    .catch Ljava/io/IOException; {:try_start_e8 .. :try_end_107} :catch_14e

    #@107
    .line 215
    :cond_107
    new-instance v3, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@109
    const/4 v9, 0x0

    #@10a
    invoke-direct/range {v3 .. v9}, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/io/FileInputStream;I)V

    #@10d
    .end local v5           #contentType:Ljava/lang/String;
    .end local v8           #is:Ljava/io/FileInputStream;
    :goto_10d
    return-object v3

    #@10e
    .line 120
    .end local v4           #fileName:Ljava/lang/String;
    .end local v6           #length:J
    .restart local v10       #contentType:Ljava/lang/String;
    .restart local v14       #fileName:Ljava/lang/String;
    .restart local v15       #length:J
    :catch_10e
    move-exception v11

    #@10f
    .line 122
    .local v11, e:Landroid/database/sqlite/SQLiteException;
    const/16 v17, 0x0

    #@111
    .restart local v17       #metadataCursor:Landroid/database/Cursor;
    goto/16 :goto_31

    #@113
    .line 138
    .end local v11           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_113
    move-exception v3

    #@114
    move-object v4, v14

    #@115
    .end local v14           #fileName:Ljava/lang/String;
    .restart local v4       #fileName:Ljava/lang/String;
    :goto_115
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    #@118
    throw v3

    #@119
    .line 151
    .end local v15           #length:J
    .restart local v6       #length:J
    :cond_119
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@11c
    move-result-object v4

    #@11d
    goto/16 :goto_76

    #@11f
    .line 155
    .end local v4           #fileName:Ljava/lang/String;
    .end local v6           #length:J
    .end local v10           #contentType:Ljava/lang/String;
    .end local v17           #metadataCursor:Landroid/database/Cursor;
    .restart local v14       #fileName:Ljava/lang/String;
    .restart local v15       #length:J
    :cond_11f
    const-string v3, "file"

    #@121
    move-object/from16 v0, v18

    #@123
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@126
    move-result v3

    #@127
    if-eqz v3, :cond_13e

    #@129
    .line 156
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@12c
    move-result-object v4

    #@12d
    .line 157
    .end local v14           #fileName:Ljava/lang/String;
    .restart local v4       #fileName:Ljava/lang/String;
    move-object/from16 v5, p2

    #@12f
    .line 158
    .restart local v5       #contentType:Ljava/lang/String;
    new-instance v12, Ljava/io/File;

    #@131
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    #@134
    move-result-object v3

    #@135
    invoke-direct {v12, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@138
    .line 159
    .local v12, f:Ljava/io/File;
    invoke-virtual {v12}, Ljava/io/File;->length()J

    #@13b
    move-result-wide v6

    #@13c
    .line 160
    .end local v15           #length:J
    .restart local v6       #length:J
    goto/16 :goto_77

    #@13e
    .line 162
    .end local v4           #fileName:Ljava/lang/String;
    .end local v5           #contentType:Ljava/lang/String;
    .end local v6           #length:J
    .end local v12           #f:Ljava/io/File;
    .restart local v14       #fileName:Ljava/lang/String;
    .restart local v15       #length:J
    :cond_13e
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->SEND_FILE_INFO_ERROR:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@140
    move-wide v6, v15

    #@141
    .end local v15           #length:J
    .restart local v6       #length:J
    move-object v4, v14

    #@142
    .end local v14           #fileName:Ljava/lang/String;
    .restart local v4       #fileName:Ljava/lang/String;
    goto :goto_10d

    #@143
    .line 183
    .restart local v5       #contentType:Ljava/lang/String;
    .restart local v8       #is:Ljava/io/FileInputStream;
    .restart local v13       #fd:Landroid/content/res/AssetFileDescriptor;
    .restart local v19       #statLength:J
    :catch_143
    move-exception v11

    #@144
    .line 185
    .local v11, e:Ljava/io/IOException;
    :try_start_144
    invoke-virtual {v13}, Landroid/content/res/AssetFileDescriptor;->close()V
    :try_end_147
    .catch Ljava/io/IOException; {:try_start_144 .. :try_end_147} :catch_148
    .catch Ljava/io/FileNotFoundException; {:try_start_144 .. :try_end_147} :catch_159

    #@147
    goto :goto_d8

    #@148
    .line 186
    :catch_148
    move-exception v3

    #@149
    goto :goto_d8

    #@14a
    .line 197
    .end local v8           #is:Ljava/io/FileInputStream;
    .end local v11           #e:Ljava/io/IOException;
    .end local v13           #fd:Landroid/content/res/AssetFileDescriptor;
    .end local v19           #statLength:J
    :catch_14a
    move-exception v11

    #@14b
    .line 198
    .local v11, e:Ljava/io/FileNotFoundException;
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->SEND_FILE_INFO_ERROR:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@14d
    goto :goto_10d

    #@14e
    .line 209
    .end local v11           #e:Ljava/io/FileNotFoundException;
    .restart local v8       #is:Ljava/io/FileInputStream;
    :catch_14e
    move-exception v11

    #@14f
    .line 210
    .local v11, e:Ljava/io/IOException;
    const-string v3, "BluetoothOppSendFileInfo"

    #@151
    const-string v9, "Read stream exception: "

    #@153
    invoke-static {v3, v9, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@156
    .line 211
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->SEND_FILE_INFO_ERROR:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@158
    goto :goto_10d

    #@159
    .line 190
    .end local v11           #e:Ljava/io/IOException;
    :catch_159
    move-exception v3

    #@15a
    goto/16 :goto_d8

    #@15c
    .line 138
    .end local v5           #contentType:Ljava/lang/String;
    .end local v6           #length:J
    .end local v8           #is:Ljava/io/FileInputStream;
    .restart local v10       #contentType:Ljava/lang/String;
    .restart local v15       #length:J
    .restart local v17       #metadataCursor:Landroid/database/Cursor;
    :catchall_15c
    move-exception v3

    #@15d
    goto :goto_115

    #@15e
    .end local v4           #fileName:Ljava/lang/String;
    .restart local v14       #fileName:Ljava/lang/String;
    :cond_15e
    move-wide v6, v15

    #@15f
    .end local v15           #length:J
    .restart local v6       #length:J
    move-object v4, v14

    #@160
    .end local v14           #fileName:Ljava/lang/String;
    .restart local v4       #fileName:Ljava/lang/String;
    goto/16 :goto_48

    #@162
    .end local v4           #fileName:Ljava/lang/String;
    .end local v6           #length:J
    .restart local v14       #fileName:Ljava/lang/String;
    .restart local v15       #length:J
    :cond_162
    move-wide v6, v15

    #@163
    .end local v15           #length:J
    .restart local v6       #length:J
    move-object v4, v14

    #@164
    .end local v14           #fileName:Ljava/lang/String;
    .restart local v4       #fileName:Ljava/lang/String;
    goto/16 :goto_4b
.end method
