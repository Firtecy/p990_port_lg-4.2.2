.class Lcom/android/bluetooth/opp/BluetoothOppService$2;
.super Landroid/os/Handler;
.source "BluetoothOppService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/opp/BluetoothOppService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/opp/BluetoothOppService;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/opp/BluetoothOppService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 262
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 13
    .parameter "msg"

    #@0
    .prologue
    .line 265
    iget v8, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v8, :sswitch_data_27e

    #@5
    .line 383
    :cond_5
    :goto_5
    return-void

    #@6
    .line 267
    :sswitch_6
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@8
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mSocketListener:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$200(Lcom/android/bluetooth/opp/BluetoothOppService;)Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@b
    move-result-object v8

    #@c
    invoke-virtual {v8}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->stop()V

    #@f
    .line 268
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@11
    const/4 v9, 0x0

    #@12
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mListenStarted:Z
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$302(Lcom/android/bluetooth/opp/BluetoothOppService;Z)Z

    #@15
    .line 269
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@17
    monitor-enter v9

    #@18
    .line 270
    :try_start_18
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1a
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mUpdateThread:Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$400(Lcom/android/bluetooth/opp/BluetoothOppService;)Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;

    #@1d
    move-result-object v8

    #@1e
    if-nez v8, :cond_25

    #@20
    .line 271
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@22
    invoke-virtual {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->stopSelf()V

    #@25
    .line 273
    :cond_25
    monitor-exit v9

    #@26
    goto :goto_5

    #@27
    :catchall_27
    move-exception v8

    #@28
    monitor-exit v9
    :try_end_29
    .catchall {:try_start_18 .. :try_end_29} :catchall_27

    #@29
    throw v8

    #@2a
    .line 276
    :sswitch_2a
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@2c
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$500(Lcom/android/bluetooth/opp/BluetoothOppService;)Landroid/bluetooth/BluetoothAdapter;

    #@2f
    move-result-object v8

    #@30
    invoke-virtual {v8}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@33
    move-result v8

    #@34
    if-eqz v8, :cond_5

    #@36
    .line 277
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@38
    #calls: Lcom/android/bluetooth/opp/BluetoothOppService;->startSocketListener()V
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$600(Lcom/android/bluetooth/opp/BluetoothOppService;)V

    #@3b
    goto :goto_5

    #@3c
    .line 282
    :sswitch_3c
    const-string v8, "BtOppService"

    #@3e
    new-instance v9, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v10, "Update mInfo.id "

    #@45
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v9

    #@49
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@4b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v9

    #@4f
    const-string v10, " for data uri= "

    #@51
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v9

    #@55
    iget-object v10, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@57
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@5a
    move-result-object v10

    #@5b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v9

    #@5f
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v9

    #@63
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 285
    new-instance v6, Landroid/content/ContentValues;

    #@68
    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    #@6b
    .line 286
    .local v6, updateValues:Landroid/content/ContentValues;
    new-instance v8, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    sget-object v9, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@72
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v8

    #@76
    const-string v9, "/"

    #@78
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v8

    #@7c
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@7e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v8

    #@82
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v8

    #@86
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@89
    move-result-object v0

    #@8a
    .line 287
    .local v0, contentUri:Landroid/net/Uri;
    const-string v8, "scanned"

    #@8c
    const/4 v9, 0x1

    #@8d
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@90
    move-result-object v9

    #@91
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@94
    .line 288
    const-string v8, "uri"

    #@96
    iget-object v9, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@98
    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@9b
    move-result-object v9

    #@9c
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9f
    .line 289
    const-string v8, "mimetype"

    #@a1
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@a3
    invoke-virtual {v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->getContentResolver()Landroid/content/ContentResolver;

    #@a6
    move-result-object v9

    #@a7
    iget-object v10, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a9
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@ac
    move-result-object v10

    #@ad
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@b0
    move-result-object v10

    #@b1
    invoke-virtual {v9, v10}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    #@b4
    move-result-object v9

    #@b5
    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@b8
    .line 291
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@ba
    invoke-virtual {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->getContentResolver()Landroid/content/ContentResolver;

    #@bd
    move-result-object v8

    #@be
    const/4 v9, 0x0

    #@bf
    const/4 v10, 0x0

    #@c0
    invoke-virtual {v8, v0, v6, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@c3
    .line 292
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@c5
    monitor-enter v9

    #@c6
    .line 293
    :try_start_c6
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@c8
    const/4 v10, 0x0

    #@c9
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mMediaScanInProgress:Z
    invoke-static {v8, v10}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$702(Lcom/android/bluetooth/opp/BluetoothOppService;Z)Z

    #@cc
    .line 294
    monitor-exit v9

    #@cd
    goto/16 :goto_5

    #@cf
    :catchall_cf
    move-exception v8

    #@d0
    monitor-exit v9
    :try_end_d1
    .catchall {:try_start_c6 .. :try_end_d1} :catchall_cf

    #@d1
    throw v8

    #@d2
    .line 297
    .end local v0           #contentUri:Landroid/net/Uri;
    .end local v6           #updateValues:Landroid/content/ContentValues;
    :sswitch_d2
    const-string v8, "BtOppService"

    #@d4
    new-instance v9, Ljava/lang/StringBuilder;

    #@d6
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@d9
    const-string v10, "Update mInfo.id "

    #@db
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v9

    #@df
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@e1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v9

    #@e5
    const-string v10, " for MEDIA_SCANNED_FAILED"

    #@e7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v9

    #@eb
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ee
    move-result-object v9

    #@ef
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f2
    .line 298
    new-instance v7, Landroid/content/ContentValues;

    #@f4
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    #@f7
    .line 299
    .local v7, updateValues1:Landroid/content/ContentValues;
    new-instance v8, Ljava/lang/StringBuilder;

    #@f9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@fc
    sget-object v9, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@fe
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v8

    #@102
    const-string v9, "/"

    #@104
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v8

    #@108
    iget v9, p1, Landroid/os/Message;->arg1:I

    #@10a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v8

    #@10e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@111
    move-result-object v8

    #@112
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@115
    move-result-object v1

    #@116
    .line 300
    .local v1, contentUri1:Landroid/net/Uri;
    const-string v8, "scanned"

    #@118
    const/4 v9, 0x2

    #@119
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@11c
    move-result-object v9

    #@11d
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@120
    .line 302
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@122
    invoke-virtual {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->getContentResolver()Landroid/content/ContentResolver;

    #@125
    move-result-object v8

    #@126
    const/4 v9, 0x0

    #@127
    const/4 v10, 0x0

    #@128
    invoke-virtual {v8, v1, v7, v9, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@12b
    .line 303
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@12d
    monitor-enter v9

    #@12e
    .line 304
    :try_start_12e
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@130
    const/4 v10, 0x0

    #@131
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mMediaScanInProgress:Z
    invoke-static {v8, v10}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$702(Lcom/android/bluetooth/opp/BluetoothOppService;Z)Z

    #@134
    .line 305
    monitor-exit v9

    #@135
    goto/16 :goto_5

    #@137
    :catchall_137
    move-exception v8

    #@138
    monitor-exit v9
    :try_end_139
    .catchall {:try_start_12e .. :try_end_139} :catchall_137

    #@139
    throw v8

    #@13a
    .line 309
    .end local v1           #contentUri1:Landroid/net/Uri;
    .end local v7           #updateValues1:Landroid/content/ContentValues;
    :sswitch_13a
    const-string v8, "BtOppService"

    #@13c
    const-string v9, "Get incoming connection"

    #@13e
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@141
    .line 313
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@144
    move-result-object v8

    #@145
    if-eqz v8, :cond_17b

    #@147
    .line 314
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@14a
    move-result-object v8

    #@14b
    const/4 v9, 0x0

    #@14c
    invoke-interface {v8, v9}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@14f
    move-result v8

    #@150
    if-nez v8, :cond_15d

    #@152
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@155
    move-result-object v8

    #@156
    const/4 v9, 0x2

    #@157
    invoke-interface {v8, v9}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@15a
    move-result v8

    #@15b
    if-eqz v8, :cond_17b

    #@15d
    .line 317
    :cond_15d
    const-string v8, "BtOppService"

    #@15f
    const-string v9, "onCreate is blocked by LG MDM Server Policy"

    #@161
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@164
    .line 318
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@166
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$800(Lcom/android/bluetooth/opp/BluetoothOppService;)Landroid/os/Handler;

    #@169
    move-result-object v8

    #@16a
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@16c
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$800(Lcom/android/bluetooth/opp/BluetoothOppService;)Landroid/os/Handler;

    #@16f
    move-result-object v9

    #@170
    const/16 v10, 0xc8

    #@172
    invoke-virtual {v9, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@175
    move-result-object v9

    #@176
    invoke-virtual {v8, v9}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@179
    goto/16 :goto_5

    #@17b
    .line 324
    :cond_17b
    iget-object v5, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@17d
    check-cast v5, Ljavax/obex/ObexTransport;

    #@17f
    .line 331
    .local v5, transport:Ljavax/obex/ObexTransport;
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@181
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$900(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljava/util/ArrayList;

    #@184
    move-result-object v8

    #@185
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@188
    move-result v8

    #@189
    if-nez v8, :cond_1a1

    #@18b
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@18d
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingConnection:Ljavax/obex/ObexTransport;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1000(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljavax/obex/ObexTransport;

    #@190
    move-result-object v8

    #@191
    if-nez v8, :cond_1a1

    #@193
    .line 332
    const-string v8, "BtOppService"

    #@195
    const-string v9, "Start Obex Server"

    #@197
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@19a
    .line 333
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@19c
    #calls: Lcom/android/bluetooth/opp/BluetoothOppService;->createServerSession(Ljavax/obex/ObexTransport;)V
    invoke-static {v8, v5}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1100(Lcom/android/bluetooth/opp/BluetoothOppService;Ljavax/obex/ObexTransport;)V

    #@19f
    goto/16 :goto_5

    #@1a1
    .line 335
    :cond_1a1
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1a3
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingConnection:Ljavax/obex/ObexTransport;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1000(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljavax/obex/ObexTransport;

    #@1a6
    move-result-object v8

    #@1a7
    if-eqz v8, :cond_1bf

    #@1a9
    .line 336
    const-string v8, "BtOppService"

    #@1ab
    const-string v9, "OPP busy! Reject connection"

    #@1ad
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1b0
    .line 338
    :try_start_1b0
    invoke-interface {v5}, Ljavax/obex/ObexTransport;->close()V
    :try_end_1b3
    .catch Ljava/io/IOException; {:try_start_1b0 .. :try_end_1b3} :catch_1b5

    #@1b3
    goto/16 :goto_5

    #@1b5
    .line 339
    :catch_1b5
    move-exception v2

    #@1b6
    .line 340
    .local v2, e:Ljava/io/IOException;
    const-string v8, "BtOppService"

    #@1b8
    const-string v9, "close tranport error"

    #@1ba
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1bd
    goto/16 :goto_5

    #@1bf
    .line 346
    .end local v2           #e:Ljava/io/IOException;
    :cond_1bf
    const-string v8, "BtOppService"

    #@1c1
    const-string v9, "OPP busy! Retry after 1 second"

    #@1c3
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1c6
    .line 347
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1c8
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1ca
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mIncomingRetries:I
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1200(Lcom/android/bluetooth/opp/BluetoothOppService;)I

    #@1cd
    move-result v9

    #@1ce
    add-int/lit8 v9, v9, 0x1

    #@1d0
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mIncomingRetries:I
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1202(Lcom/android/bluetooth/opp/BluetoothOppService;I)I

    #@1d3
    .line 348
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1d5
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingConnection:Ljavax/obex/ObexTransport;
    invoke-static {v8, v5}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1002(Lcom/android/bluetooth/opp/BluetoothOppService;Ljavax/obex/ObexTransport;)Ljavax/obex/ObexTransport;

    #@1d8
    .line 349
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1da
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$800(Lcom/android/bluetooth/opp/BluetoothOppService;)Landroid/os/Handler;

    #@1dd
    move-result-object v8

    #@1de
    invoke-static {v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@1e1
    move-result-object v3

    #@1e2
    .line 350
    .local v3, msg1:Landroid/os/Message;
    const/4 v8, 0x4

    #@1e3
    iput v8, v3, Landroid/os/Message;->what:I

    #@1e5
    .line 351
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1e7
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$800(Lcom/android/bluetooth/opp/BluetoothOppService;)Landroid/os/Handler;

    #@1ea
    move-result-object v8

    #@1eb
    const-wide/16 v9, 0x3e8

    #@1ed
    invoke-virtual {v8, v3, v9, v10}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1f0
    goto/16 :goto_5

    #@1f2
    .line 356
    .end local v3           #msg1:Landroid/os/Message;
    .end local v5           #transport:Ljavax/obex/ObexTransport;
    :sswitch_1f2
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1f4
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$900(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljava/util/ArrayList;

    #@1f7
    move-result-object v8

    #@1f8
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    #@1fb
    move-result v8

    #@1fc
    if-nez v8, :cond_21e

    #@1fe
    .line 357
    const-string v8, "BtOppService"

    #@200
    const-string v9, "Start Obex Server"

    #@202
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@205
    .line 358
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@207
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@209
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingConnection:Ljavax/obex/ObexTransport;
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1000(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljavax/obex/ObexTransport;

    #@20c
    move-result-object v9

    #@20d
    #calls: Lcom/android/bluetooth/opp/BluetoothOppService;->createServerSession(Ljavax/obex/ObexTransport;)V
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1100(Lcom/android/bluetooth/opp/BluetoothOppService;Ljavax/obex/ObexTransport;)V

    #@210
    .line 359
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@212
    const/4 v9, 0x0

    #@213
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mIncomingRetries:I
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1202(Lcom/android/bluetooth/opp/BluetoothOppService;I)I

    #@216
    .line 360
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@218
    const/4 v9, 0x0

    #@219
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingConnection:Ljavax/obex/ObexTransport;
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1002(Lcom/android/bluetooth/opp/BluetoothOppService;Ljavax/obex/ObexTransport;)Ljavax/obex/ObexTransport;

    #@21c
    goto/16 :goto_5

    #@21e
    .line 362
    :cond_21e
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@220
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mIncomingRetries:I
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1200(Lcom/android/bluetooth/opp/BluetoothOppService;)I

    #@223
    move-result v8

    #@224
    const/16 v9, 0x14

    #@226
    if-ne v8, v9, :cond_24f

    #@228
    .line 363
    const-string v8, "BtOppService"

    #@22a
    const-string v9, "Retried 20 seconds, reject connection"

    #@22c
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22f
    .line 365
    :try_start_22f
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@231
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingConnection:Ljavax/obex/ObexTransport;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1000(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljavax/obex/ObexTransport;

    #@234
    move-result-object v8

    #@235
    invoke-interface {v8}, Ljavax/obex/ObexTransport;->close()V
    :try_end_238
    .catch Ljava/io/IOException; {:try_start_22f .. :try_end_238} :catch_246

    #@238
    .line 369
    :goto_238
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@23a
    const/4 v9, 0x0

    #@23b
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mIncomingRetries:I
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1202(Lcom/android/bluetooth/opp/BluetoothOppService;I)I

    #@23e
    .line 370
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@240
    const/4 v9, 0x0

    #@241
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingConnection:Ljavax/obex/ObexTransport;
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1002(Lcom/android/bluetooth/opp/BluetoothOppService;Ljavax/obex/ObexTransport;)Ljavax/obex/ObexTransport;

    #@244
    goto/16 :goto_5

    #@246
    .line 366
    :catch_246
    move-exception v2

    #@247
    .line 367
    .restart local v2       #e:Ljava/io/IOException;
    const-string v8, "BtOppService"

    #@249
    const-string v9, "close tranport error"

    #@24b
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24e
    goto :goto_238

    #@24f
    .line 372
    .end local v2           #e:Ljava/io/IOException;
    :cond_24f
    const-string v8, "BtOppService"

    #@251
    const-string v9, "OPP busy! Retry after 1 second"

    #@253
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@256
    .line 373
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@258
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@25a
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mIncomingRetries:I
    invoke-static {v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1200(Lcom/android/bluetooth/opp/BluetoothOppService;)I

    #@25d
    move-result v9

    #@25e
    add-int/lit8 v9, v9, 0x1

    #@260
    #setter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mIncomingRetries:I
    invoke-static {v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1202(Lcom/android/bluetooth/opp/BluetoothOppService;I)I

    #@263
    .line 374
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@265
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$800(Lcom/android/bluetooth/opp/BluetoothOppService;)Landroid/os/Handler;

    #@268
    move-result-object v8

    #@269
    invoke-static {v8}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@26c
    move-result-object v4

    #@26d
    .line 375
    .local v4, msg2:Landroid/os/Message;
    const/4 v8, 0x4

    #@26e
    iput v8, v4, Landroid/os/Message;->what:I

    #@270
    .line 376
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService$2;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@272
    #getter for: Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$800(Lcom/android/bluetooth/opp/BluetoothOppService;)Landroid/os/Handler;

    #@275
    move-result-object v8

    #@276
    const-wide/16 v9, 0x3e8

    #@278
    invoke-virtual {v8, v4, v9, v10}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@27b
    goto/16 :goto_5

    #@27d
    .line 265
    nop

    #@27e
    :sswitch_data_27e
    .sparse-switch
        0x1 -> :sswitch_2a
        0x2 -> :sswitch_3c
        0x3 -> :sswitch_d2
        0x4 -> :sswitch_1f2
        0x64 -> :sswitch_13a
        0xc8 -> :sswitch_6
    .end sparse-switch
.end method
