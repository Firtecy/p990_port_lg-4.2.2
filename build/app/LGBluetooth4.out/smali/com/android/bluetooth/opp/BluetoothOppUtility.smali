.class public Lcom/android/bluetooth/opp/BluetoothOppUtility;
.super Ljava/lang/Object;
.source "BluetoothOppUtility.java"


# static fields
.field private static final D:Z = true

.field private static final SEND_FILE_HASH_MAP:Ljava/util/concurrent/ConcurrentHashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private static final SEND_FILE_MAP:Ljava/util/concurrent/ConcurrentHashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Landroid/net/Uri;",
            "Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;",
            ">;"
        }
    .end annotation
.end field

.field private static final TAG:Ljava/lang/String; = "BluetoothOppUtility"

.field private static final V:Z = true


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 71
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    #@2
    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    #@5
    sput-object v0, Lcom/android/bluetooth/opp/BluetoothOppUtility;->SEND_FILE_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    #@7
    .line 74
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    #@9
    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    #@c
    sput-object v0, Lcom/android/bluetooth/opp/BluetoothOppUtility;->SEND_FILE_HASH_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    #@e
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 66
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static closeSendFileInfo(Landroid/net/Uri;)V
    .registers 10
    .parameter "uri"

    #@0
    .prologue
    .line 455
    const-string v6, "BluetoothOppUtility"

    #@2
    new-instance v7, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v8, "closeSendFileInfo: uri="

    #@9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v7

    #@d
    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v7

    #@11
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v7

    #@15
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 461
    const/4 v3, 0x0

    #@19
    .line 462
    .local v3, info:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    sget-object v6, Lcom/android/bluetooth/opp/BluetoothOppUtility;->SEND_FILE_HASH_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    #@1b
    invoke-virtual {v6}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@1e
    move-result-object v6

    #@1f
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@22
    move-result-object v2

    #@23
    .local v2, i:Ljava/util/Iterator;
    :cond_23
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@26
    move-result v6

    #@27
    if-eqz v6, :cond_74

    #@29
    .line 463
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    check-cast v0, Ljava/util/Map$Entry;

    #@2f
    .line 464
    .local v0, entry:Ljava/util/Map$Entry;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@35
    .line 465
    .local v1, fileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@38
    move-result-object v4

    #@39
    check-cast v4, Landroid/net/Uri;

    #@3b
    .line 466
    .local v4, internalUri:Landroid/net/Uri;
    invoke-virtual {v4, p0}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    #@3e
    move-result v6

    #@3f
    if-nez v6, :cond_23

    #@41
    .line 467
    sget-object v6, Lcom/android/bluetooth/opp/BluetoothOppUtility;->SEND_FILE_HASH_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    #@43
    invoke-virtual {v6, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@46
    move-result-object v5

    #@47
    check-cast v5, Landroid/net/Uri;

    #@49
    .line 468
    .local v5, removeUri:Landroid/net/Uri;
    if-eqz v5, :cond_74

    #@4b
    .line 469
    move-object v3, v1

    #@4c
    .line 470
    if-eqz v3, :cond_74

    #@4e
    .line 471
    const-string v6, "BluetoothOppUtility"

    #@50
    new-instance v7, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v8, "[BTUI] closeSendFileInfo("

    #@57
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v7

    #@5b
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@5e
    move-result-object v8

    #@5f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v7

    #@63
    const-string v8, ") : info = "

    #@65
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v7

    #@69
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v7

    #@6d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v7

    #@71
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 481
    .end local v0           #entry:Ljava/util/Map$Entry;
    .end local v1           #fileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    .end local v4           #internalUri:Landroid/net/Uri;
    .end local v5           #removeUri:Landroid/net/Uri;
    :cond_74
    if-eqz v3, :cond_7f

    #@76
    iget-object v6, v3, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mInputStream:Ljava/io/FileInputStream;

    #@78
    if-eqz v6, :cond_7f

    #@7a
    .line 484
    :try_start_7a
    iget-object v6, v3, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mInputStream:Ljava/io/FileInputStream;

    #@7c
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_7f
    .catch Ljava/io/IOException; {:try_start_7a .. :try_end_7f} :catch_80

    #@7f
    .line 488
    :cond_7f
    :goto_7f
    return-void

    #@80
    .line 485
    :catch_80
    move-exception v6

    #@81
    goto :goto_7f
.end method

.method public static formatProgressText(JJ)Ljava/lang/String;
    .registers 14
    .parameter "totalBytes"
    .parameter "currentBytes"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 322
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@5
    move-result-object v4

    #@6
    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 324
    .local v0, currentLanguage:Ljava/lang/String;
    const-wide/16 v4, 0x0

    #@c
    cmp-long v4, p0, v4

    #@e
    if-gtz v4, :cond_4b

    #@10
    .line 326
    if-eqz v0, :cond_48

    #@12
    const-string v4, "ar"

    #@14
    invoke-virtual {v0, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@17
    move-result v4

    #@18
    if-eqz v4, :cond_22

    #@1a
    const-string v4, "fa"

    #@1c
    invoke-virtual {v0, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@1f
    move-result v4

    #@20
    if-nez v4, :cond_48

    #@22
    .line 328
    :cond_22
    new-instance v4, Ljava/lang/StringBuilder;

    #@24
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@27
    const-string v5, "%"

    #@29
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@30
    move-result-object v5

    #@31
    const-string v6, "%d"

    #@33
    new-array v7, v7, [Ljava/lang/Object;

    #@35
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38
    move-result-object v8

    #@39
    aput-object v8, v7, v9

    #@3b
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v4

    #@43
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v4

    #@47
    .line 343
    :goto_47
    return-object v4

    #@48
    .line 331
    :cond_48
    const-string v4, "0%"

    #@4a
    goto :goto_47

    #@4b
    .line 333
    :cond_4b
    const-wide/16 v4, 0x64

    #@4d
    mul-long/2addr v4, p2

    #@4e
    div-long v1, v4, p0

    #@50
    .line 335
    .local v1, progress:J
    if-eqz v0, :cond_88

    #@52
    const-string v4, "ar"

    #@54
    invoke-virtual {v0, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@57
    move-result v4

    #@58
    if-eqz v4, :cond_62

    #@5a
    const-string v4, "fa"

    #@5c
    invoke-virtual {v0, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    #@5f
    move-result v4

    #@60
    if-nez v4, :cond_88

    #@62
    .line 337
    :cond_62
    new-instance v4, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    const-string v5, "%"

    #@69
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v4

    #@6d
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    #@70
    move-result-object v5

    #@71
    const-string v6, "%d"

    #@73
    new-array v7, v7, [Ljava/lang/Object;

    #@75
    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@78
    move-result-object v8

    #@79
    aput-object v8, v7, v9

    #@7b
    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@7e
    move-result-object v5

    #@7f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v4

    #@83
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@86
    move-result-object v4

    #@87
    goto :goto_47

    #@88
    .line 340
    :cond_88
    new-instance v3, Ljava/lang/StringBuilder;

    #@8a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8d
    .line 341
    .local v3, sb:Ljava/lang/StringBuilder;
    invoke-virtual {v3, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@90
    .line 342
    const/16 v4, 0x25

    #@92
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@95
    .line 343
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@98
    move-result-object v4

    #@99
    goto :goto_47
.end method

.method static getSendFileInfo(Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    .registers 9
    .parameter "uri"

    #@0
    .prologue
    .line 430
    const-string v5, "BluetoothOppUtility"

    #@2
    new-instance v6, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v7, "getSendFileInfo: uri="

    #@9
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v6

    #@d
    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v6

    #@11
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 436
    const/4 v3, 0x0

    #@19
    .line 437
    .local v3, info:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    sget-object v5, Lcom/android/bluetooth/opp/BluetoothOppUtility;->SEND_FILE_HASH_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    #@1b
    invoke-virtual {v5}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    #@1e
    move-result-object v5

    #@1f
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@22
    move-result-object v2

    #@23
    .local v2, i:Ljava/util/Iterator;
    :cond_23
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@26
    move-result v5

    #@27
    if-eqz v5, :cond_6c

    #@29
    .line 438
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2c
    move-result-object v0

    #@2d
    check-cast v0, Ljava/util/Map$Entry;

    #@2f
    .line 439
    .local v0, entry:Ljava/util/Map$Entry;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@32
    move-result-object v1

    #@33
    check-cast v1, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@35
    .line 440
    .local v1, fileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@38
    move-result-object v4

    #@39
    check-cast v4, Landroid/net/Uri;

    #@3b
    .line 441
    .local v4, internalUri:Landroid/net/Uri;
    invoke-virtual {v4, p0}, Landroid/net/Uri;->compareTo(Landroid/net/Uri;)I

    #@3e
    move-result v5

    #@3f
    if-nez v5, :cond_23

    #@41
    .line 442
    move-object v3, v1

    #@42
    .line 443
    if-eqz p0, :cond_6c

    #@44
    if-eqz v3, :cond_6c

    #@46
    .line 444
    const-string v5, "BluetoothOppUtility"

    #@48
    new-instance v6, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v7, "[BTUI] getSendFileInfo("

    #@4f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@56
    move-result-object v7

    #@57
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v6

    #@5b
    const-string v7, ") : info = "

    #@5d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v6

    #@61
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v6

    #@65
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v6

    #@69
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 450
    .end local v0           #entry:Ljava/util/Map$Entry;
    .end local v1           #fileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    .end local v4           #internalUri:Landroid/net/Uri;
    :cond_6c
    if-eqz v3, :cond_6f

    #@6e
    .end local v3           #info:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    :goto_6e
    return-object v3

    #@6f
    .restart local v3       #info:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    :cond_6f
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->SEND_FILE_INFO_ERROR:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@71
    goto :goto_6e
.end method

.method public static getStatusDescription(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .registers 7
    .parameter "context"
    .parameter "statusCode"
    .parameter "deviceName"

    #@0
    .prologue
    .line 351
    const/16 v1, 0xbe

    #@2
    if-ne p1, v1, :cond_c

    #@4
    .line 352
    const v1, 0x7f070048

    #@7
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 380
    .local v0, ret:Ljava/lang/String;
    :goto_b
    return-object v0

    #@c
    .line 353
    .end local v0           #ret:Ljava/lang/String;
    :cond_c
    const/16 v1, 0xc0

    #@e
    if-ne p1, v1, :cond_18

    #@10
    .line 354
    const v1, 0x7f070049

    #@13
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .restart local v0       #ret:Ljava/lang/String;
    goto :goto_b

    #@18
    .line 355
    .end local v0           #ret:Ljava/lang/String;
    :cond_18
    const/16 v1, 0xc8

    #@1a
    if-ne p1, v1, :cond_24

    #@1c
    .line 356
    const v1, 0x7f07004a

    #@1f
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    .restart local v0       #ret:Ljava/lang/String;
    goto :goto_b

    #@24
    .line 357
    .end local v0           #ret:Ljava/lang/String;
    :cond_24
    const/16 v1, 0x196

    #@26
    if-ne p1, v1, :cond_30

    #@28
    .line 358
    const v1, 0x7f07004b

    #@2b
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@2e
    move-result-object v0

    #@2f
    .restart local v0       #ret:Ljava/lang/String;
    goto :goto_b

    #@30
    .line 359
    .end local v0           #ret:Ljava/lang/String;
    :cond_30
    const/16 v1, 0x193

    #@32
    if-ne p1, v1, :cond_3c

    #@34
    .line 360
    const v1, 0x7f07004c

    #@37
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@3a
    move-result-object v0

    #@3b
    .restart local v0       #ret:Ljava/lang/String;
    goto :goto_b

    #@3c
    .line 361
    .end local v0           #ret:Ljava/lang/String;
    :cond_3c
    const/16 v1, 0x1ea

    #@3e
    if-ne p1, v1, :cond_48

    #@40
    .line 362
    const v1, 0x7f07004d

    #@43
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    .restart local v0       #ret:Ljava/lang/String;
    goto :goto_b

    #@48
    .line 363
    .end local v0           #ret:Ljava/lang/String;
    :cond_48
    const/16 v1, 0x1ec

    #@4a
    if-ne p1, v1, :cond_54

    #@4c
    .line 364
    const v1, 0x7f07004e

    #@4f
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@52
    move-result-object v0

    #@53
    .restart local v0       #ret:Ljava/lang/String;
    goto :goto_b

    #@54
    .line 365
    .end local v0           #ret:Ljava/lang/String;
    :cond_54
    const/16 v1, 0x1ed

    #@56
    if-ne p1, v1, :cond_60

    #@58
    .line 366
    const v1, 0x7f07004f

    #@5b
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5e
    move-result-object v0

    #@5f
    .restart local v0       #ret:Ljava/lang/String;
    goto :goto_b

    #@60
    .line 367
    .end local v0           #ret:Ljava/lang/String;
    :cond_60
    const/16 v1, 0x1f1

    #@62
    if-ne p1, v1, :cond_6c

    #@64
    .line 368
    const v1, 0x7f070050

    #@67
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@6a
    move-result-object v0

    #@6b
    .restart local v0       #ret:Ljava/lang/String;
    goto :goto_b

    #@6c
    .line 369
    .end local v0           #ret:Ljava/lang/String;
    :cond_6c
    const/16 v1, 0x1ee

    #@6e
    if-ne p1, v1, :cond_7e

    #@70
    .line 370
    const v1, 0x7f070045

    #@73
    const/4 v2, 0x1

    #@74
    new-array v2, v2, [Ljava/lang/Object;

    #@76
    const/4 v3, 0x0

    #@77
    aput-object p2, v2, v3

    #@79
    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@7c
    move-result-object v0

    #@7d
    .restart local v0       #ret:Ljava/lang/String;
    goto :goto_b

    #@7e
    .line 371
    .end local v0           #ret:Ljava/lang/String;
    :cond_7e
    const/16 v1, 0x190

    #@80
    if-eq p1, v1, :cond_92

    #@82
    const/16 v1, 0x19b

    #@84
    if-eq p1, v1, :cond_92

    #@86
    const/16 v1, 0x19c

    #@88
    if-eq p1, v1, :cond_92

    #@8a
    const/16 v1, 0x1ef

    #@8c
    if-eq p1, v1, :cond_92

    #@8e
    const/16 v1, 0x1f0

    #@90
    if-ne p1, v1, :cond_9b

    #@92
    .line 376
    :cond_92
    const v1, 0x7f070051

    #@95
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@98
    move-result-object v0

    #@99
    .restart local v0       #ret:Ljava/lang/String;
    goto/16 :goto_b

    #@9b
    .line 378
    .end local v0           #ret:Ljava/lang/String;
    :cond_9b
    const v1, 0x7f070052

    #@9e
    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@a1
    move-result-object v0

    #@a2
    .restart local v0       #ret:Ljava/lang/String;
    goto/16 :goto_b
.end method

.method public static isRecognizedFileType(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Z
    .registers 9
    .parameter "context"
    .parameter "fileUri"
    .parameter "mimetype"

    #@0
    .prologue
    .line 266
    const/4 v2, 0x1

    #@1
    .line 269
    .local v2, ret:Z
    const-string v3, "BluetoothOppUtility"

    #@3
    new-instance v4, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v5, "RecognizedFileType() fileUri: "

    #@a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    const-string v5, " mimetype: "

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v4

    #@1c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v4

    #@20
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 272
    new-instance v1, Landroid/content/Intent;

    #@25
    const-string v3, "android.intent.action.VIEW"

    #@27
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2a
    .line 273
    .local v1, mimetypeIntent:Landroid/content/Intent;
    invoke-virtual {v1, p1, p2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    #@2d
    .line 274
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@30
    move-result-object v3

    #@31
    const/high16 v4, 0x1

    #@33
    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    #@36
    move-result-object v0

    #@37
    .line 277
    .local v0, list:Ljava/util/List;,"Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    #@3a
    move-result v3

    #@3b
    if-nez v3, :cond_56

    #@3d
    .line 279
    const-string v3, "BluetoothOppUtility"

    #@3f
    new-instance v4, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v5, "NO application to handle MIME type "

    #@46
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v4

    #@4a
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v4

    #@4e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v4

    #@52
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 281
    const/4 v2, 0x0

    #@56
    .line 283
    :cond_56
    return v2
.end method

.method public static openReceivedFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Landroid/net/Uri;)V
    .registers 14
    .parameter "context"
    .parameter "fileName"
    .parameter "mimetype"
    .parameter "timeStamp"
    .parameter "uri"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/high16 v6, 0x1000

    #@3
    .line 204
    if-eqz p1, :cond_7

    #@5
    if-nez p2, :cond_f

    #@7
    .line 205
    :cond_7
    const-string v5, "BluetoothOppUtility"

    #@9
    const-string v6, "ERROR: Para fileName ==null, or mimetype == null"

    #@b
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 259
    :goto_e
    return-void

    #@f
    .line 209
    :cond_f
    new-instance v2, Ljava/io/File;

    #@11
    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@14
    .line 210
    .local v2, f:Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    #@17
    move-result v5

    #@18
    if-nez v5, :cond_5f

    #@1a
    .line 211
    new-instance v3, Landroid/content/Intent;

    #@1c
    const-class v5, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;

    #@1e
    invoke-direct {v3, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@21
    .line 212
    .local v3, in:Landroid/content/Intent;
    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@24
    .line 213
    const-string v5, "title"

    #@26
    const v6, 0x7f07003b

    #@29
    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@2c
    move-result-object v6

    #@2d
    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@30
    .line 214
    const-string v5, "content"

    #@32
    const v6, 0x7f07003c

    #@35
    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3c
    .line 215
    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@3f
    .line 220
    const-string v5, "BluetoothOppUtility"

    #@41
    new-instance v6, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v7, "This uri will be deleted: "

    #@48
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v6

    #@4c
    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v6

    #@50
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v6

    #@54
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 222
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5a
    move-result-object v5

    #@5b
    invoke-virtual {v5, p4, v8, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@5e
    goto :goto_e

    #@5f
    .line 226
    .end local v3           #in:Landroid/content/Intent;
    :cond_5f
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@62
    move-result-object v4

    #@63
    .line 228
    .local v4, path:Landroid/net/Uri;
    invoke-virtual {v4}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@66
    move-result-object v5

    #@67
    if-nez v5, :cond_72

    #@69
    .line 229
    new-instance v5, Ljava/io/File;

    #@6b
    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6e
    invoke-static {v5}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@71
    move-result-object v4

    #@72
    .line 232
    :cond_72
    invoke-static {p0, v4, p2}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->isRecognizedFileType(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Z

    #@75
    move-result v5

    #@76
    if-eqz v5, :cond_c4

    #@78
    .line 233
    new-instance v0, Landroid/content/Intent;

    #@7a
    const-string v5, "android.intent.action.VIEW"

    #@7c
    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7f
    .line 234
    .local v0, activityIntent:Landroid/content/Intent;
    invoke-virtual {v0, v4, p2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    #@82
    .line 244
    :try_start_82
    const-string v5, "BluetoothOppUtility"

    #@84
    new-instance v6, Ljava/lang/StringBuilder;

    #@86
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@89
    const-string v7, "ACTION_VIEW intent sent out: "

    #@8b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v6

    #@8f
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v6

    #@93
    const-string v7, " / "

    #@95
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v6

    #@99
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v6

    #@9d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v6

    #@a1
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a4
    .line 246
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_a7
    .catch Landroid/content/ActivityNotFoundException; {:try_start_82 .. :try_end_a7} :catch_a9

    #@a7
    goto/16 :goto_e

    #@a9
    .line 247
    :catch_a9
    move-exception v1

    #@aa
    .line 249
    .local v1, ex:Landroid/content/ActivityNotFoundException;
    const-string v5, "BluetoothOppUtility"

    #@ac
    new-instance v6, Ljava/lang/StringBuilder;

    #@ae
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b1
    const-string v7, "no activity for handling ACTION_VIEW intent:  "

    #@b3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v6

    #@b7
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v6

    #@bb
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v6

    #@bf
    invoke-static {v5, v6, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@c2
    goto/16 :goto_e

    #@c4
    .line 253
    .end local v0           #activityIntent:Landroid/content/Intent;
    .end local v1           #ex:Landroid/content/ActivityNotFoundException;
    :cond_c4
    new-instance v3, Landroid/content/Intent;

    #@c6
    const-class v5, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;

    #@c8
    invoke-direct {v3, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@cb
    .line 254
    .restart local v3       #in:Landroid/content/Intent;
    invoke-virtual {v3, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@ce
    .line 255
    const-string v5, "title"

    #@d0
    const v6, 0x7f070039

    #@d3
    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@d6
    move-result-object v6

    #@d7
    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@da
    .line 256
    const-string v5, "content"

    #@dc
    const v6, 0x7f07003a

    #@df
    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@e2
    move-result-object v6

    #@e3
    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@e6
    .line 257
    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@e9
    goto/16 :goto_e
.end method

.method static putSendFileInfo(Landroid/net/Uri;Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;)V
    .registers 5
    .parameter "uri"
    .parameter "sendFileInfo"

    #@0
    .prologue
    .line 418
    const-string v0, "BluetoothOppUtility"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "putSendFileInfo: uri="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " sendFileInfo="

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 424
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppUtility;->SEND_FILE_HASH_MAP:Ljava/util/concurrent/ConcurrentHashMap;

    #@24
    invoke-virtual {v0, p1, p0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@27
    .line 426
    return-void
.end method

.method public static queryRecord(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    .registers 14
    .parameter "context"
    .parameter "uri"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 79
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@4
    move-result-object v6

    #@5
    .line 80
    .local v6, adapter:Landroid/bluetooth/BluetoothAdapter;
    new-instance v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@7
    invoke-direct {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;-><init>()V

    #@a
    .line 81
    .local v9, info:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v0

    #@e
    move-object v1, p1

    #@f
    move-object v3, v2

    #@10
    move-object v4, v2

    #@11
    move-object v5, v2

    #@12
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@15
    move-result-object v8

    #@16
    .line 82
    .local v8, cursor:Landroid/database/Cursor;
    if-eqz v8, :cond_12c

    #@18
    .line 83
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_115

    #@1e
    .line 84
    const-string v0, "_id"

    #@20
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@23
    move-result v0

    #@24
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    #@27
    move-result v0

    #@28
    iput v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mID:I

    #@2a
    .line 85
    const-string v0, "status"

    #@2c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@2f
    move-result v0

    #@30
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    #@33
    move-result v0

    #@34
    iput v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@36
    .line 86
    const-string v0, "direction"

    #@38
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@3b
    move-result v0

    #@3c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    #@3f
    move-result v0

    #@40
    iput v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDirection:I

    #@42
    .line 95
    const-string v0, "total_bytes"

    #@44
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@47
    move-result v0

    #@48
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@4b
    move-result-wide v0

    #@4c
    iput-wide v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@4e
    .line 97
    const-string v0, "current_bytes"

    #@50
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@53
    move-result v0

    #@54
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@57
    move-result-wide v0

    #@58
    iput-wide v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mCurrentBytes:J

    #@5a
    .line 100
    const-string v0, "timestamp"

    #@5c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@5f
    move-result v0

    #@60
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    #@63
    move-result-wide v0

    #@64
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@67
    move-result-object v0

    #@68
    iput-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTimeStamp:Ljava/lang/Long;

    #@6a
    .line 102
    const-string v0, "destination"

    #@6c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@6f
    move-result v0

    #@70
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@73
    move-result-object v0

    #@74
    iput-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDestAddr:Ljava/lang/String;

    #@76
    .line 105
    const-string v0, "_data"

    #@78
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@7b
    move-result v0

    #@7c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@7f
    move-result-object v0

    #@80
    iput-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@82
    .line 107
    iget-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@84
    if-nez v0, :cond_92

    #@86
    .line 108
    const-string v0, "hint"

    #@88
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@8b
    move-result v0

    #@8c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@8f
    move-result-object v0

    #@90
    iput-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@92
    .line 111
    :cond_92
    iget-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@94
    if-nez v0, :cond_9f

    #@96
    .line 112
    const v0, 0x7f070039

    #@99
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@9c
    move-result-object v0

    #@9d
    iput-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@9f
    .line 115
    :cond_9f
    const-string v0, "uri"

    #@a1
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@a4
    move-result v0

    #@a5
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@a8
    move-result-object v0

    #@a9
    iput-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileUri:Ljava/lang/String;

    #@ab
    .line 117
    iget-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileUri:Ljava/lang/String;

    #@ad
    if-eqz v0, :cond_119

    #@af
    .line 118
    iget-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileUri:Ljava/lang/String;

    #@b1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@b4
    move-result-object v11

    #@b5
    .line 119
    .local v11, u:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@b8
    move-result-object v0

    #@b9
    invoke-virtual {v0, v11}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    #@bc
    move-result-object v0

    #@bd
    iput-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@bf
    .line 124
    :goto_bf
    iget-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@c1
    if-nez v0, :cond_cf

    #@c3
    .line 125
    const-string v0, "mimetype"

    #@c5
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@c8
    move-result v0

    #@c9
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@cc
    move-result-object v0

    #@cd
    iput-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@cf
    .line 129
    :cond_cf
    iget-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDestAddr:Ljava/lang/String;

    #@d1
    invoke-virtual {v6, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@d4
    move-result-object v10

    #@d5
    .line 130
    .local v10, remoteDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@d8
    move-result-object v0

    #@d9
    invoke-virtual {v0, v10}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getDeviceName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@dc
    move-result-object v0

    #@dd
    iput-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@df
    .line 133
    const-string v0, "confirm"

    #@e1
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@e4
    move-result v0

    #@e5
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    #@e8
    move-result v7

    #@e9
    .line 135
    .local v7, confirmationType:I
    const/4 v0, 0x5

    #@ea
    if-ne v7, v0, :cond_12a

    #@ec
    const/4 v0, 0x1

    #@ed
    :goto_ed
    iput-boolean v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mHandoverInitiated:Z

    #@ef
    .line 139
    const-string v0, "BluetoothOppUtility"

    #@f1
    new-instance v1, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v2, "Get data from db:"

    #@f8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v1

    #@fc
    iget-object v2, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@fe
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@101
    move-result-object v1

    #@102
    iget-object v2, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@104
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@107
    move-result-object v1

    #@108
    iget-object v2, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDestAddr:Ljava/lang/String;

    #@10a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v1

    #@10e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@111
    move-result-object v1

    #@112
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@115
    .line 143
    .end local v7           #confirmationType:I
    .end local v10           #remoteDevice:Landroid/bluetooth/BluetoothDevice;
    .end local v11           #u:Landroid/net/Uri;
    :cond_115
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@118
    .line 150
    :goto_118
    return-object v9

    #@119
    .line 121
    :cond_119
    iget-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@11b
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@11e
    move-result-object v11

    #@11f
    .line 122
    .restart local v11       #u:Landroid/net/Uri;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@122
    move-result-object v0

    #@123
    invoke-virtual {v0, v11}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    #@126
    move-result-object v0

    #@127
    iput-object v0, v9, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@129
    goto :goto_bf

    #@12a
    .line 135
    .restart local v7       #confirmationType:I
    .restart local v10       #remoteDevice:Landroid/bluetooth/BluetoothDevice;
    :cond_12a
    const/4 v0, 0x0

    #@12b
    goto :goto_ed

    #@12c
    .line 145
    .end local v7           #confirmationType:I
    .end local v10           #remoteDevice:Landroid/bluetooth/BluetoothDevice;
    .end local v11           #u:Landroid/net/Uri;
    :cond_12c
    const/4 v9, 0x0

    #@12d
    .line 147
    const-string v0, "BluetoothOppUtility"

    #@12f
    new-instance v1, Ljava/lang/StringBuilder;

    #@131
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@134
    const-string v2, "BluetoothOppManager Error: not got data from db for uri:"

    #@136
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v1

    #@13a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v1

    #@13e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@141
    move-result-object v1

    #@142
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@145
    goto :goto_118
.end method

.method public static queryTransfersInBatch(Landroid/content/Context;Ljava/lang/Long;)Ljava/util/ArrayList;
    .registers 15
    .parameter "context"
    .parameter "timeStamp"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Long;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v12, 0x0

    #@1
    const/4 v11, 0x0

    #@2
    .line 158
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@5
    move-result-object v10

    #@6
    .line 159
    .local v10, uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v1, "timestamp == "

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    .line 162
    .local v3, WHERE:Ljava/lang/String;
    const/4 v8, 0x0

    #@1a
    .line 164
    .local v8, metadataCursor:Landroid/database/Cursor;
    :try_start_1a
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1d
    move-result-object v0

    #@1e
    sget-object v1, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@20
    const/4 v2, 0x1

    #@21
    new-array v2, v2, [Ljava/lang/String;

    #@23
    const/4 v4, 0x0

    #@24
    const-string v5, "_data"

    #@26
    aput-object v5, v2, v4

    #@28
    const/4 v4, 0x0

    #@29
    const-string v5, "_id"

    #@2b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2e
    .catch Ljava/lang/Exception; {:try_start_1a .. :try_end_2e} :catch_33

    #@2e
    move-result-object v8

    #@2f
    .line 177
    if-nez v8, :cond_53

    #@31
    move-object v10, v11

    #@32
    .line 195
    .end local v10           #uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_32
    return-object v10

    #@33
    .line 168
    .restart local v10       #uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_33
    move-exception v6

    #@34
    .line 169
    .local v6, e:Ljava/lang/Exception;
    if-eqz v8, :cond_39

    #@36
    .line 170
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@39
    .line 172
    :cond_39
    const-string v0, "BluetoothOppUtility"

    #@3b
    new-instance v1, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v2, "[BTUI] queryTransfersInBatch: "

    #@42
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v1

    #@46
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    move-object v10, v11

    #@52
    .line 173
    goto :goto_32

    #@53
    .line 181
    .end local v6           #e:Ljava/lang/Exception;
    :cond_53
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@56
    :goto_56
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    #@59
    move-result v0

    #@5a
    if-nez v0, :cond_9a

    #@5c
    .line 183
    invoke-interface {v8, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@5f
    move-result-object v7

    #@60
    .line 184
    .local v7, fileName:Ljava/lang/String;
    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@63
    move-result-object v9

    #@64
    .line 186
    .local v9, path:Landroid/net/Uri;
    invoke-virtual {v9}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    #@67
    move-result-object v0

    #@68
    if-nez v0, :cond_73

    #@6a
    .line 187
    new-instance v0, Ljava/io/File;

    #@6c
    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@6f
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    #@72
    move-result-object v9

    #@73
    .line 189
    :cond_73
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@76
    move-result-object v0

    #@77
    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@7a
    .line 191
    const-string v0, "BluetoothOppUtility"

    #@7c
    new-instance v1, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v2, "Uri in this batch: "

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    invoke-virtual {v9}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@8a
    move-result-object v2

    #@8b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v1

    #@8f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v1

    #@93
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    .line 181
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@99
    goto :goto_56

    #@9a
    .line 194
    .end local v7           #fileName:Ljava/lang/String;
    .end local v9           #path:Landroid/net/Uri;
    :cond_9a
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@9d
    goto :goto_32
.end method

.method public static retryTransfer(Landroid/content/Context;Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;)V
    .registers 7
    .parameter "context"
    .parameter "transInfo"

    #@0
    .prologue
    .line 404
    invoke-static {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@3
    move-result-object v1

    #@4
    .line 405
    .local v1, btOppManager:Lcom/android/bluetooth/opp/BluetoothOppManager;
    iget-object v2, p1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@6
    iget-object v3, p1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileUri:Ljava/lang/String;

    #@8
    const/4 v4, 0x0

    #@9
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/bluetooth/opp/BluetoothOppManager;->saveSendingFileInfo(Ljava/lang/String;Ljava/lang/String;Z)V

    #@c
    .line 407
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@f
    move-result-object v0

    #@10
    .line 408
    .local v0, adapter:Landroid/bluetooth/BluetoothAdapter;
    iget-object v2, p1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDestAddr:Ljava/lang/String;

    #@12
    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppManager;->startTransfer(Landroid/bluetooth/BluetoothDevice;)V

    #@19
    .line 411
    const-string v2, "BluetoothOppUtility"

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "Insert retryTransfer: "

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    iget-object v4, p1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileUri:Ljava/lang/String;

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    const-string v4, "  to device: "

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    iget-object v4, p1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@34
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 414
    return-void
.end method

.method public static updateVisibilityToHidden(Landroid/content/Context;Landroid/net/Uri;)V
    .registers 7
    .parameter "context"
    .parameter "uri"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x1

    #@2
    .line 290
    new-instance v0, Landroid/content/ContentValues;

    #@4
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@7
    .line 292
    .local v0, updateValues:Landroid/content/ContentValues;
    const-string v1, "visibility"

    #@9
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    #@c
    move-result-object v1

    #@d
    if-eqz v1, :cond_28

    #@f
    const-string v1, "visibility"

    #@11
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@18
    move-result v1

    #@19
    if-lez v1, :cond_28

    #@1b
    .line 294
    const-string v1, "visibility"

    #@1d
    const/4 v2, 0x2

    #@1e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@25
    .line 301
    :goto_25
    sput-boolean v3, Lcom/android/bluetooth/opp/BluetoothOppNotification;->shouldNotificationUpdated:Z

    #@27
    .line 303
    return-void

    #@28
    .line 297
    :cond_28
    const-string v1, "visibility"

    #@2a
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@31
    .line 298
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1, p1, v0, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@38
    goto :goto_25
.end method

.method public static updateVisibilityToRemoved(Landroid/content/Context;Landroid/net/Uri;)V
    .registers 6
    .parameter "context"
    .parameter "uri"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 311
    new-instance v0, Landroid/content/ContentValues;

    #@3
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@6
    .line 312
    .local v0, updateValues:Landroid/content/ContentValues;
    const-string v1, "visibility"

    #@8
    const/4 v2, 0x2

    #@9
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@10
    .line 313
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, p1, v0, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@17
    .line 314
    return-void
.end method
