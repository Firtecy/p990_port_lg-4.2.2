.class Lcom/android/bluetooth/opp/BluetoothOppNotification;
.super Ljava/lang/Object;
.source "BluetoothOppNotification.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationUpdateThread;,
        Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;
    }
.end annotation


# static fields
.field private static final ACCESS_AUTHORITY_CLASS:Ljava/lang/String; = "com.android.settings.bluetooth.BluetoothPermissionRequest"

.field private static final ACCESS_AUTHORITY_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field private static final AUTH_POPUP:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private static final NOTIFICATION_ID_INBOUND:I = -0xf4246

.field private static final NOTIFICATION_ID_OUTBOUND:I = -0xf4245

.field private static final NOTIFY:I = 0x0

.field private static final TAG:Ljava/lang/String; = "BluetoothOppNotification"

.field private static final V:Z = true

.field static final WHERE_COMPLETED:Ljava/lang/String; = "status >= \'200\' AND (visibility IS NULL OR visibility == \'0\') AND (confirm != \'5\')"

.field private static final WHERE_COMPLETED_INBOUND:Ljava/lang/String; = "status >= \'200\' AND (visibility IS NULL OR visibility == \'0\') AND (confirm != \'5\') AND (direction == 1)"

.field private static final WHERE_COMPLETED_OUTBOUND:Ljava/lang/String; = "status >= \'200\' AND (visibility IS NULL OR visibility == \'0\') AND (confirm != \'5\') AND (direction == 0)"

.field static final WHERE_CONFIRM_PENDING:Ljava/lang/String; = "confirm == \'0\' AND (visibility IS NULL OR visibility == \'0\')"

.field static final WHERE_RUNNING:Ljava/lang/String; = "(status == \'192\') AND (visibility IS NULL OR visibility == \'0\') AND (confirm == \'1\' OR confirm == \'2\' OR confirm == \'5\')"

.field static final confirm:Ljava/lang/String; = "(confirm == \'1\' OR confirm == \'2\' OR confirm == \'5\')"

.field static final not_through_handover:Ljava/lang/String; = "(confirm != \'5\')"

.field public static shouldNotificationUpdated:Z = false

.field static final status:Ljava/lang/String; = "(status == \'192\')"

.field static final visible:Ljava/lang/String; = "(visibility IS NULL OR visibility == \'0\')"


# instance fields
.field private mActiveNotificationId:I

.field private final mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field public mNotificationMgr:Landroid/app/NotificationManager;

.field private mNotifications:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;",
            ">;"
        }
    .end annotation
.end field

.field private mPendingUpdate:I

.field private mUpdateCompleteNotification:Z

.field private mUpdateNotificationThread:Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationUpdateThread;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 116
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->AUTH_POPUP:Ljava/util/HashMap;

    #@7
    .line 122
    const/4 v0, 0x0

    #@8
    sput-boolean v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->shouldNotificationUpdated:Z

    #@a
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .registers 4
    .parameter "ctx"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 152
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 103
    iput v1, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mPendingUpdate:I

    #@6
    .line 109
    const/4 v0, 0x1

    #@7
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mUpdateCompleteNotification:Z

    #@9
    .line 111
    iput v1, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mActiveNotificationId:I

    #@b
    .line 193
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$1;

    #@d
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/BluetoothOppNotification$1;-><init>(Lcom/android/bluetooth/opp/BluetoothOppNotification;)V

    #@10
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mHandler:Landroid/os/Handler;

    #@12
    .line 153
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@14
    .line 154
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@16
    const-string v1, "notification"

    #@18
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1b
    move-result-object v0

    #@1c
    check-cast v0, Landroid/app/NotificationManager;

    #@1e
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@20
    .line 156
    new-instance v0, Ljava/util/HashMap;

    #@22
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@25
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotifications:Ljava/util/HashMap;

    #@27
    .line 160
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@2a
    move-result-object v0

    #@2b
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2d
    .line 162
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/opp/BluetoothOppNotification;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mPendingUpdate:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/bluetooth/opp/BluetoothOppNotification;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput p1, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mPendingUpdate:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/bluetooth/opp/BluetoothOppNotification;)Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationUpdateThread;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mUpdateNotificationThread:Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationUpdateThread;

    #@2
    return-object v0
.end method

.method static synthetic access$102(Lcom/android/bluetooth/opp/BluetoothOppNotification;Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationUpdateThread;)Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationUpdateThread;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mUpdateNotificationThread:Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationUpdateThread;

    #@2
    return-object p1
.end method

.method static synthetic access$200(Lcom/android/bluetooth/opp/BluetoothOppNotification;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/bluetooth/opp/BluetoothOppNotification;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppNotification;->updateActiveNotification()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/bluetooth/opp/BluetoothOppNotification;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppNotification;->updateCompletedNotification()V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/bluetooth/opp/BluetoothOppNotification;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppNotification;->updateIncomingFileConfirmNotification()V

    #@3
    return-void
.end method

.method private putAuthPopupId(I)Z
    .registers 6
    .parameter "id"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 703
    sget-object v1, Lcom/android/bluetooth/opp/BluetoothOppNotification;->AUTH_POPUP:Ljava/util/HashMap;

    #@3
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@6
    move-result-object v2

    #@7
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_27

    #@d
    .line 704
    const-string v0, "BluetoothOppNotification"

    #@f
    new-instance v1, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v2, "Already the id contains : "

    #@16
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 705
    const/4 v0, 0x0

    #@26
    .line 710
    :goto_26
    return v0

    #@27
    .line 708
    :cond_27
    const-string v1, "BluetoothOppNotification"

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "AUTH_POPUP.put( "

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    const-string v3, " )"

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 709
    sget-object v1, Lcom/android/bluetooth/opp/BluetoothOppNotification;->AUTH_POPUP:Ljava/util/HashMap;

    #@47
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4a
    move-result-object v2

    #@4b
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@4e
    move-result-object v3

    #@4f
    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@52
    goto :goto_26
.end method

.method private updateActiveNotification()V
    .registers 36

    #@0
    .prologue
    .line 254
    const/4 v15, 0x0

    #@1
    .line 256
    .local v15, cursor:Landroid/database/Cursor;
    :try_start_1
    move-object/from16 v0, p0

    #@3
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v3

    #@9
    sget-object v4, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@b
    const/4 v5, 0x0

    #@c
    const-string v6, "(status == \'192\') AND (visibility IS NULL OR visibility == \'0\') AND (confirm == \'1\' OR confirm == \'2\' OR confirm == \'5\')"

    #@e
    const/4 v7, 0x0

    #@f
    const-string v8, "_id"

    #@11
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_14} :catch_18

    #@14
    move-result-object v15

    #@15
    .line 266
    if-nez v15, :cond_39

    #@17
    .line 416
    :cond_17
    :goto_17
    return-void

    #@18
    .line 258
    :catch_18
    move-exception v21

    #@19
    .line 259
    .local v21, e:Ljava/lang/Exception;
    if-eqz v15, :cond_1e

    #@1b
    .line 260
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    #@1e
    .line 262
    :cond_1e
    const-string v3, "BluetoothOppNotification"

    #@20
    new-instance v4, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v5, "[BTUI] updateActiveNotification : "

    #@27
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v4

    #@2b
    move-object/from16 v0, v21

    #@2d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v4

    #@31
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    goto :goto_17

    #@39
    .line 272
    .end local v21           #e:Ljava/lang/Exception;
    :cond_39
    invoke-interface {v15}, Landroid/database/Cursor;->getCount()I

    #@3c
    move-result v3

    #@3d
    if-lez v3, :cond_f7

    #@3f
    .line 273
    const/4 v3, 0x0

    #@40
    move-object/from16 v0, p0

    #@42
    iput-boolean v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mUpdateCompleteNotification:Z

    #@44
    .line 278
    :goto_44
    const-string v3, "BluetoothOppNotification"

    #@46
    new-instance v4, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v5, "mUpdateCompleteNotification = "

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    move-object/from16 v0, p0

    #@53
    iget-boolean v5, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mUpdateCompleteNotification:Z

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 282
    const-string v3, "timestamp"

    #@62
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@65
    move-result v32

    #@66
    .line 283
    .local v32, timestampIndex:I
    const-string v3, "direction"

    #@68
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@6b
    move-result v20

    #@6c
    .line 284
    .local v20, directionIndex:I
    const-string v3, "_id"

    #@6e
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@71
    move-result v26

    #@72
    .line 285
    .local v26, idIndex:I
    const-string v3, "total_bytes"

    #@74
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@77
    move-result v34

    #@78
    .line 286
    .local v34, totalBytesIndex:I
    const-string v3, "current_bytes"

    #@7a
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@7d
    move-result v14

    #@7e
    .line 287
    .local v14, currentBytesIndex:I
    const-string v3, "_data"

    #@80
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@83
    move-result v16

    #@84
    .line 288
    .local v16, dataIndex:I
    const-string v3, "hint"

    #@86
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@89
    move-result v23

    #@8a
    .line 289
    .local v23, filenameHintIndex:I
    const-string v3, "confirm"

    #@8c
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@8f
    move-result v11

    #@90
    .line 290
    .local v11, confirmIndex:I
    const-string v3, "destination"

    #@92
    invoke-interface {v15, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@95
    move-result v18

    #@96
    .line 292
    .local v18, destinationIndex:I
    move-object/from16 v0, p0

    #@98
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotifications:Ljava/util/HashMap;

    #@9a
    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    #@9d
    .line 293
    invoke-interface {v15}, Landroid/database/Cursor;->moveToFirst()Z

    #@a0
    :goto_a0
    invoke-interface {v15}, Landroid/database/Cursor;->isAfterLast()Z

    #@a3
    move-result v3

    #@a4
    if-nez v3, :cond_1d7

    #@a6
    .line 294
    move/from16 v0, v32

    #@a8
    invoke-interface {v15, v0}, Landroid/database/Cursor;->getLong(I)J

    #@ab
    move-result-wide v30

    #@ac
    .line 295
    .local v30, timeStamp:J
    move/from16 v0, v20

    #@ae
    invoke-interface {v15, v0}, Landroid/database/Cursor;->getInt(I)I

    #@b1
    move-result v19

    #@b2
    .line 296
    .local v19, dir:I
    move/from16 v0, v26

    #@b4
    invoke-interface {v15, v0}, Landroid/database/Cursor;->getInt(I)I

    #@b7
    move-result v25

    #@b8
    .line 297
    .local v25, id:I
    move/from16 v0, v34

    #@ba
    invoke-interface {v15, v0}, Landroid/database/Cursor;->getInt(I)I

    #@bd
    move-result v33

    #@be
    .line 298
    .local v33, total:I
    invoke-interface {v15, v14}, Landroid/database/Cursor;->getInt(I)I

    #@c1
    move-result v13

    #@c2
    .line 299
    .local v13, current:I
    invoke-interface {v15, v11}, Landroid/database/Cursor;->getInt(I)I

    #@c5
    move-result v12

    #@c6
    .line 301
    .local v12, confirmation:I
    move/from16 v0, v18

    #@c8
    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@cb
    move-result-object v17

    #@cc
    .line 302
    .local v17, destination:Ljava/lang/String;
    invoke-interface/range {v15 .. v16}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@cf
    move-result-object v22

    #@d0
    .line 303
    .local v22, fileName:Ljava/lang/String;
    if-nez v22, :cond_d8

    #@d2
    .line 304
    move/from16 v0, v23

    #@d4
    invoke-interface {v15, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@d7
    move-result-object v22

    #@d8
    .line 306
    :cond_d8
    if-nez v22, :cond_e5

    #@da
    .line 307
    move-object/from16 v0, p0

    #@dc
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@de
    const v4, 0x7f070039

    #@e1
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@e4
    move-result-object v22

    #@e5
    .line 310
    :cond_e5
    invoke-static/range {v30 .. v31}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    #@e8
    move-result-object v10

    #@e9
    .line 313
    .local v10, batchID:Ljava/lang/String;
    move-object/from16 v0, p0

    #@eb
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotifications:Ljava/util/HashMap;

    #@ed
    invoke-virtual {v3, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@f0
    move-result v3

    #@f1
    if-eqz v3, :cond_fe

    #@f3
    .line 293
    :goto_f3
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    #@f6
    goto :goto_a0

    #@f7
    .line 275
    .end local v10           #batchID:Ljava/lang/String;
    .end local v11           #confirmIndex:I
    .end local v12           #confirmation:I
    .end local v13           #current:I
    .end local v14           #currentBytesIndex:I
    .end local v16           #dataIndex:I
    .end local v17           #destination:Ljava/lang/String;
    .end local v18           #destinationIndex:I
    .end local v19           #dir:I
    .end local v20           #directionIndex:I
    .end local v22           #fileName:Ljava/lang/String;
    .end local v23           #filenameHintIndex:I
    .end local v25           #id:I
    .end local v26           #idIndex:I
    .end local v30           #timeStamp:J
    .end local v32           #timestampIndex:I
    .end local v33           #total:I
    .end local v34           #totalBytesIndex:I
    :cond_f7
    const/4 v3, 0x1

    #@f8
    move-object/from16 v0, p0

    #@fa
    iput-boolean v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mUpdateCompleteNotification:Z

    #@fc
    goto/16 :goto_44

    #@fe
    .line 317
    .restart local v10       #batchID:Ljava/lang/String;
    .restart local v11       #confirmIndex:I
    .restart local v12       #confirmation:I
    .restart local v13       #current:I
    .restart local v14       #currentBytesIndex:I
    .restart local v16       #dataIndex:I
    .restart local v17       #destination:Ljava/lang/String;
    .restart local v18       #destinationIndex:I
    .restart local v19       #dir:I
    .restart local v20       #directionIndex:I
    .restart local v22       #fileName:Ljava/lang/String;
    .restart local v23       #filenameHintIndex:I
    .restart local v25       #id:I
    .restart local v26       #idIndex:I
    .restart local v30       #timeStamp:J
    .restart local v32       #timestampIndex:I
    .restart local v33       #total:I
    .restart local v34       #totalBytesIndex:I
    :cond_fe
    new-instance v28, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;

    #@100
    invoke-direct/range {v28 .. v28}, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;-><init>()V

    #@103
    .line 318
    .local v28, item:Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;
    move-wide/from16 v0, v30

    #@105
    move-object/from16 v2, v28

    #@107
    iput-wide v0, v2, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->timeStamp:J

    #@109
    .line 319
    move/from16 v0, v25

    #@10b
    move-object/from16 v1, v28

    #@10d
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->id:I

    #@10f
    .line 320
    move/from16 v0, v19

    #@111
    move-object/from16 v1, v28

    #@113
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->direction:I

    #@115
    .line 321
    move-object/from16 v0, v28

    #@117
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->direction:I

    #@119
    if-nez v3, :cond_1a1

    #@11b
    .line 322
    move-object/from16 v0, p0

    #@11d
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@11f
    const v4, 0x7f07001d

    #@122
    const/4 v5, 0x1

    #@123
    new-array v5, v5, [Ljava/lang/Object;

    #@125
    const/4 v6, 0x0

    #@126
    aput-object v22, v5, v6

    #@128
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@12b
    move-result-object v3

    #@12c
    move-object/from16 v0, v28

    #@12e
    iput-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->description:Ljava/lang/String;

    #@130
    .line 336
    :goto_130
    if-gez v33, :cond_1c8

    #@132
    .line 337
    const v3, 0x7fffffff

    #@135
    shr-int/lit8 v4, v13, 0x1

    #@137
    and-int/2addr v3, v4

    #@138
    move-object/from16 v0, v28

    #@13a
    iput v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalCurrent:I

    #@13c
    .line 338
    const v3, 0x7fffffff

    #@13f
    shr-int/lit8 v4, v33, 0x1

    #@141
    and-int/2addr v3, v4

    #@142
    move-object/from16 v0, v28

    #@144
    iput v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalTotal:I

    #@146
    .line 344
    :goto_146
    const/4 v3, 0x5

    #@147
    if-ne v12, v3, :cond_1d4

    #@149
    const/4 v3, 0x1

    #@14a
    :goto_14a
    move-object/from16 v0, v28

    #@14c
    iput-boolean v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->handoverInitiated:Z

    #@14e
    .line 346
    move-object/from16 v0, v17

    #@150
    move-object/from16 v1, v28

    #@152
    iput-object v0, v1, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->destination:Ljava/lang/String;

    #@154
    .line 347
    move-object/from16 v0, p0

    #@156
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotifications:Ljava/util/HashMap;

    #@158
    move-object/from16 v0, v28

    #@15a
    invoke-virtual {v3, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@15d
    .line 350
    const-string v3, "BluetoothOppNotification"

    #@15f
    new-instance v4, Ljava/lang/StringBuilder;

    #@161
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@164
    const-string v5, "ID="

    #@166
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@169
    move-result-object v4

    #@16a
    move-object/from16 v0, v28

    #@16c
    iget v5, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->id:I

    #@16e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@171
    move-result-object v4

    #@172
    const-string v5, "; batchID="

    #@174
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@177
    move-result-object v4

    #@178
    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v4

    #@17c
    const-string v5, "; totoalCurrent"

    #@17e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v4

    #@182
    move-object/from16 v0, v28

    #@184
    iget v5, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalCurrent:I

    #@186
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@189
    move-result-object v4

    #@18a
    const-string v5, "; totalTotal="

    #@18c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v4

    #@190
    move-object/from16 v0, v28

    #@192
    iget v5, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalTotal:I

    #@194
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@197
    move-result-object v4

    #@198
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19b
    move-result-object v4

    #@19c
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@19f
    goto/16 :goto_f3

    #@1a1
    .line 323
    :cond_1a1
    move-object/from16 v0, v28

    #@1a3
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->direction:I

    #@1a5
    const/4 v4, 0x1

    #@1a6
    if-ne v3, v4, :cond_1bf

    #@1a8
    .line 324
    move-object/from16 v0, p0

    #@1aa
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@1ac
    const v4, 0x7f07001a

    #@1af
    const/4 v5, 0x1

    #@1b0
    new-array v5, v5, [Ljava/lang/Object;

    #@1b2
    const/4 v6, 0x0

    #@1b3
    aput-object v22, v5, v6

    #@1b5
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1b8
    move-result-object v3

    #@1b9
    move-object/from16 v0, v28

    #@1bb
    iput-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->description:Ljava/lang/String;

    #@1bd
    goto/16 :goto_130

    #@1bf
    .line 328
    :cond_1bf
    const-string v3, "BluetoothOppNotification"

    #@1c1
    const-string v4, "mDirection ERROR!"

    #@1c3
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c6
    goto/16 :goto_130

    #@1c8
    .line 340
    :cond_1c8
    move-object/from16 v0, v28

    #@1ca
    iput v13, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalCurrent:I

    #@1cc
    .line 341
    move/from16 v0, v33

    #@1ce
    move-object/from16 v1, v28

    #@1d0
    iput v0, v1, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalTotal:I

    #@1d2
    goto/16 :goto_146

    #@1d4
    .line 344
    :cond_1d4
    const/4 v3, 0x0

    #@1d5
    goto/16 :goto_14a

    #@1d7
    .line 355
    .end local v10           #batchID:Ljava/lang/String;
    .end local v12           #confirmation:I
    .end local v13           #current:I
    .end local v17           #destination:Ljava/lang/String;
    .end local v19           #dir:I
    .end local v22           #fileName:Ljava/lang/String;
    .end local v25           #id:I
    .end local v28           #item:Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;
    .end local v30           #timeStamp:J
    .end local v33           #total:I
    :cond_1d7
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    #@1da
    .line 358
    move-object/from16 v0, p0

    #@1dc
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotifications:Ljava/util/HashMap;

    #@1de
    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    #@1e1
    move-result-object v3

    #@1e2
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@1e5
    move-result-object v24

    #@1e6
    .local v24, i$:Ljava/util/Iterator;
    :goto_1e6
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    #@1e9
    move-result v3

    #@1ea
    if-eqz v3, :cond_17

    #@1ec
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1ef
    move-result-object v28

    #@1f0
    check-cast v28, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;

    #@1f2
    .line 359
    .restart local v28       #item:Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;
    move-object/from16 v0, v28

    #@1f4
    iget-boolean v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->handoverInitiated:Z

    #@1f6
    if-eqz v3, :cond_25c

    #@1f8
    .line 360
    const/16 v29, 0x0

    #@1fa
    .line 361
    .local v29, progress:F
    move-object/from16 v0, v28

    #@1fc
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalTotal:I

    #@1fe
    const/4 v4, -0x1

    #@1ff
    if-ne v3, v4, :cond_246

    #@201
    .line 362
    const/high16 v29, -0x4080

    #@203
    .line 368
    :goto_203
    new-instance v27, Landroid/content/Intent;

    #@205
    const-string v3, "android.btopp.intent.action.BT_OPP_TRANSFER_PROGRESS"

    #@207
    move-object/from16 v0, v27

    #@209
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@20c
    .line 369
    .local v27, intent:Landroid/content/Intent;
    move-object/from16 v0, v28

    #@20e
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->direction:I

    #@210
    const/4 v4, 0x1

    #@211
    if-ne v3, v4, :cond_253

    #@213
    .line 370
    const-string v3, "android.btopp.intent.extra.BT_OPP_TRANSFER_DIRECTION"

    #@215
    const/4 v4, 0x0

    #@216
    move-object/from16 v0, v27

    #@218
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@21b
    .line 376
    :goto_21b
    const-string v3, "android.btopp.intent.extra.BT_OPP_TRANSFER_ID"

    #@21d
    move-object/from16 v0, v28

    #@21f
    iget v4, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->id:I

    #@221
    move-object/from16 v0, v27

    #@223
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@226
    .line 377
    const-string v3, "android.btopp.intent.extra.BT_OPP_TRANSFER_PROGRESS"

    #@228
    move-object/from16 v0, v27

    #@22a
    move/from16 v1, v29

    #@22c
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    #@22f
    .line 378
    const-string v3, "android.btopp.intent.extra.BT_OPP_ADDRESS"

    #@231
    move-object/from16 v0, v28

    #@233
    iget-object v4, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->destination:Ljava/lang/String;

    #@235
    move-object/from16 v0, v27

    #@237
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@23a
    .line 379
    move-object/from16 v0, p0

    #@23c
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@23e
    const-string v4, "com.android.permission.HANDOVER_STATUS"

    #@240
    move-object/from16 v0, v27

    #@242
    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@245
    goto :goto_1e6

    #@246
    .line 364
    .end local v27           #intent:Landroid/content/Intent;
    :cond_246
    move-object/from16 v0, v28

    #@248
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalCurrent:I

    #@24a
    int-to-float v3, v3

    #@24b
    move-object/from16 v0, v28

    #@24d
    iget v4, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalTotal:I

    #@24f
    int-to-float v4, v4

    #@250
    div-float v29, v3, v4

    #@252
    goto :goto_203

    #@253
    .line 373
    .restart local v27       #intent:Landroid/content/Intent;
    :cond_253
    const-string v3, "android.btopp.intent.extra.BT_OPP_TRANSFER_DIRECTION"

    #@255
    const/4 v4, 0x1

    #@256
    move-object/from16 v0, v27

    #@258
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@25b
    goto :goto_21b

    #@25c
    .line 384
    .end local v27           #intent:Landroid/content/Intent;
    .end local v29           #progress:F
    :cond_25c
    new-instance v9, Landroid/app/Notification$Builder;

    #@25e
    move-object/from16 v0, p0

    #@260
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@262
    invoke-direct {v9, v3}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    #@265
    .line 385
    .local v9, b:Landroid/app/Notification$Builder;
    move-object/from16 v0, v28

    #@267
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->description:Ljava/lang/String;

    #@269
    invoke-virtual {v9, v3}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@26c
    .line 386
    move-object/from16 v0, v28

    #@26e
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalTotal:I

    #@270
    int-to-long v3, v3

    #@271
    move-object/from16 v0, v28

    #@273
    iget v5, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalCurrent:I

    #@275
    int-to-long v5, v5

    #@276
    invoke-static {v3, v4, v5, v6}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->formatProgressText(JJ)Ljava/lang/String;

    #@279
    move-result-object v3

    #@27a
    invoke-virtual {v9, v3}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    #@27d
    .line 388
    move-object/from16 v0, v28

    #@27f
    iget v4, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalTotal:I

    #@281
    move-object/from16 v0, v28

    #@283
    iget v5, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalCurrent:I

    #@285
    move-object/from16 v0, v28

    #@287
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->totalTotal:I

    #@289
    const/4 v6, -0x1

    #@28a
    if-ne v3, v6, :cond_311

    #@28c
    const/4 v3, 0x1

    #@28d
    :goto_28d
    invoke-virtual {v9, v4, v5, v3}, Landroid/app/Notification$Builder;->setProgress(IIZ)Landroid/app/Notification$Builder;

    #@290
    .line 389
    move-object/from16 v0, v28

    #@292
    iget-wide v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->timeStamp:J

    #@294
    invoke-virtual {v9, v3, v4}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    #@297
    .line 390
    move-object/from16 v0, v28

    #@299
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->direction:I

    #@29b
    if-nez v3, :cond_314

    #@29d
    .line 391
    const v3, 0x1080088

    #@2a0
    invoke-virtual {v9, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@2a3
    .line 399
    :goto_2a3
    const/4 v3, 0x1

    #@2a4
    invoke-virtual {v9, v3}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    #@2a7
    .line 401
    new-instance v27, Landroid/content/Intent;

    #@2a9
    const-string v3, "android.btopp.intent.action.LIST"

    #@2ab
    move-object/from16 v0, v27

    #@2ad
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@2b0
    .line 402
    .restart local v27       #intent:Landroid/content/Intent;
    const-string v3, "com.android.bluetooth"

    #@2b2
    const-class v4, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@2b4
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@2b7
    move-result-object v4

    #@2b8
    move-object/from16 v0, v27

    #@2ba
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@2bd
    .line 403
    new-instance v3, Ljava/lang/StringBuilder;

    #@2bf
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c2
    sget-object v4, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@2c4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c7
    move-result-object v3

    #@2c8
    const-string v4, "/"

    #@2ca
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cd
    move-result-object v3

    #@2ce
    move-object/from16 v0, v28

    #@2d0
    iget v4, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->id:I

    #@2d2
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d5
    move-result-object v3

    #@2d6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d9
    move-result-object v3

    #@2da
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@2dd
    move-result-object v3

    #@2de
    move-object/from16 v0, v27

    #@2e0
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@2e3
    .line 405
    move-object/from16 v0, p0

    #@2e5
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@2e7
    const/4 v4, 0x0

    #@2e8
    const/4 v5, 0x0

    #@2e9
    move-object/from16 v0, v27

    #@2eb
    invoke-static {v3, v4, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@2ee
    move-result-object v3

    #@2ef
    invoke-virtual {v9, v3}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    #@2f2
    .line 406
    move-object/from16 v0, p0

    #@2f4
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@2f6
    if-eqz v3, :cond_32b

    #@2f8
    .line 407
    move-object/from16 v0, p0

    #@2fa
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@2fc
    move-object/from16 v0, v28

    #@2fe
    iget v4, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->id:I

    #@300
    invoke-virtual {v9}, Landroid/app/Notification$Builder;->getNotification()Landroid/app/Notification;

    #@303
    move-result-object v5

    #@304
    invoke-virtual {v3, v4, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@307
    .line 414
    :goto_307
    move-object/from16 v0, v28

    #@309
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->id:I

    #@30b
    move-object/from16 v0, p0

    #@30d
    iput v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mActiveNotificationId:I

    #@30f
    goto/16 :goto_1e6

    #@311
    .line 388
    .end local v27           #intent:Landroid/content/Intent;
    :cond_311
    const/4 v3, 0x0

    #@312
    goto/16 :goto_28d

    #@314
    .line 392
    :cond_314
    move-object/from16 v0, v28

    #@316
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification$NotificationItem;->direction:I

    #@318
    const/4 v4, 0x1

    #@319
    if-ne v3, v4, :cond_322

    #@31b
    .line 393
    const v3, 0x1080081

    #@31e
    invoke-virtual {v9, v3}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    #@321
    goto :goto_2a3

    #@322
    .line 396
    :cond_322
    const-string v3, "BluetoothOppNotification"

    #@324
    const-string v4, "mDirection ERROR!"

    #@326
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@329
    goto/16 :goto_2a3

    #@32b
    .line 410
    .restart local v27       #intent:Landroid/content/Intent;
    :cond_32b
    const-string v3, "BluetoothOppNotification"

    #@32d
    const-string v4, "mNotificationMgr is NULL!"

    #@32f
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@332
    goto :goto_307
.end method

.method private updateCompletedNotification()V
    .registers 28

    #@0
    .prologue
    .line 421
    const-wide/16 v23, 0x0

    #@2
    .line 422
    .local v23, timeStamp:J
    const/16 v20, 0x0

    #@4
    .line 423
    .local v20, outboundSuccNumber:I
    const/16 v18, 0x0

    #@6
    .line 426
    .local v18, outboundFailNumber:I
    const/4 v15, 0x0

    #@7
    .line 427
    .local v15, inboundSuccNumber:I
    const/4 v13, 0x0

    #@8
    .line 434
    .local v13, inboundFailNumber:I
    move-object/from16 v0, p0

    #@a
    iget-boolean v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mUpdateCompleteNotification:Z

    #@c
    if-nez v3, :cond_1a

    #@e
    sget-boolean v3, Lcom/android/bluetooth/opp/BluetoothOppNotification;->shouldNotificationUpdated:Z

    #@10
    if-nez v3, :cond_1a

    #@12
    .line 437
    const-string v3, "BluetoothOppNotification"

    #@14
    const-string v4, "No need to update complete notification"

    #@16
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 598
    :cond_19
    :goto_19
    return-void

    #@1a
    .line 442
    :cond_1a
    const/4 v3, 0x0

    #@1b
    sput-boolean v3, Lcom/android/bluetooth/opp/BluetoothOppNotification;->shouldNotificationUpdated:Z

    #@1d
    .line 449
    move-object/from16 v0, p0

    #@1f
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@21
    if-eqz v3, :cond_3b

    #@23
    move-object/from16 v0, p0

    #@25
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mActiveNotificationId:I

    #@27
    if-eqz v3, :cond_3b

    #@29
    .line 450
    move-object/from16 v0, p0

    #@2b
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@2d
    move-object/from16 v0, p0

    #@2f
    iget v4, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mActiveNotificationId:I

    #@31
    invoke-virtual {v3, v4}, Landroid/app/NotificationManager;->cancel(I)V

    #@34
    .line 452
    const-string v3, "BluetoothOppNotification"

    #@36
    const-string v4, "ongoing transfer notification was removed"

    #@38
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 462
    :cond_3b
    const/4 v10, 0x0

    #@3c
    .line 464
    .local v10, cursor:Landroid/database/Cursor;
    :try_start_3c
    move-object/from16 v0, p0

    #@3e
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@40
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@43
    move-result-object v3

    #@44
    sget-object v4, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@46
    const/4 v5, 0x0

    #@47
    const-string v6, "status >= \'200\' AND (visibility IS NULL OR visibility == \'0\') AND (confirm != \'5\') AND (direction == 0)"

    #@49
    const/4 v7, 0x0

    #@4a
    const-string v8, "timestamp DESC"

    #@4c
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4f
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_4f} :catch_85

    #@4f
    move-result-object v10

    #@50
    .line 474
    if-eqz v10, :cond_19

    #@52
    .line 478
    const-string v3, "timestamp"

    #@54
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@57
    move-result v25

    #@58
    .line 479
    .local v25, timestampIndex:I
    const-string v3, "status"

    #@5a
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@5d
    move-result v22

    #@5e
    .line 481
    .local v22, statusIndex:I
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    #@61
    :goto_61
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    #@64
    move-result v3

    #@65
    if-nez v3, :cond_a8

    #@67
    .line 482
    invoke-interface {v10}, Landroid/database/Cursor;->isFirst()Z

    #@6a
    move-result v3

    #@6b
    if-eqz v3, :cond_73

    #@6d
    .line 484
    move/from16 v0, v25

    #@6f
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    #@72
    move-result-wide v23

    #@73
    .line 486
    :cond_73
    move/from16 v0, v22

    #@75
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    #@78
    move-result v21

    #@79
    .line 488
    .local v21, status:I
    invoke-static/range {v21 .. v21}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusError(I)Z

    #@7c
    move-result v3

    #@7d
    if-eqz v3, :cond_a5

    #@7f
    .line 489
    add-int/lit8 v18, v18, 0x1

    #@81
    .line 481
    :goto_81
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@84
    goto :goto_61

    #@85
    .line 466
    .end local v21           #status:I
    .end local v22           #statusIndex:I
    .end local v25           #timestampIndex:I
    :catch_85
    move-exception v11

    #@86
    .line 467
    .local v11, e:Ljava/lang/Exception;
    if-eqz v10, :cond_8b

    #@88
    .line 468
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@8b
    .line 470
    :cond_8b
    const-string v3, "BluetoothOppNotification"

    #@8d
    new-instance v4, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v5, "[BTUI] updateCompletedNotification : "

    #@94
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v4

    #@98
    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v4

    #@9c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v4

    #@a0
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    goto/16 :goto_19

    #@a5
    .line 491
    .end local v11           #e:Ljava/lang/Exception;
    .restart local v21       #status:I
    .restart local v22       #statusIndex:I
    .restart local v25       #timestampIndex:I
    :cond_a5
    add-int/lit8 v20, v20, 0x1

    #@a7
    goto :goto_81

    #@a8
    .line 495
    .end local v21           #status:I
    :cond_a8
    const-string v3, "BluetoothOppNotification"

    #@aa
    new-instance v4, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v5, "outbound: succ-"

    #@b1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v4

    #@b5
    move/from16 v0, v20

    #@b7
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    const-string v5, "  fail-"

    #@bd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v4

    #@c1
    move/from16 v0, v18

    #@c3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v4

    #@c7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v4

    #@cb
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    .line 497
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@d1
    .line 499
    add-int v19, v20, v18

    #@d3
    .line 501
    .local v19, outboundNum:I
    if-lez v19, :cond_1b8

    #@d5
    .line 502
    new-instance v17, Landroid/app/Notification;

    #@d7
    invoke-direct/range {v17 .. v17}, Landroid/app/Notification;-><init>()V

    #@da
    .line 503
    .local v17, outNoti:Landroid/app/Notification;
    const v3, 0x1080089

    #@dd
    move-object/from16 v0, v17

    #@df
    iput v3, v0, Landroid/app/Notification;->icon:I

    #@e1
    .line 504
    move-object/from16 v0, p0

    #@e3
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@e5
    const v4, 0x7f07005a

    #@e8
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@eb
    move-result-object v26

    #@ec
    .line 505
    .local v26, title:Ljava/lang/String;
    move-object/from16 v0, p0

    #@ee
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@f0
    const v4, 0x7f07005c

    #@f3
    const/4 v5, 0x2

    #@f4
    new-array v5, v5, [Ljava/lang/Object;

    #@f6
    const/4 v6, 0x0

    #@f7
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@fa
    move-result-object v7

    #@fb
    aput-object v7, v5, v6

    #@fd
    const/4 v6, 0x1

    #@fe
    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@101
    move-result-object v7

    #@102
    aput-object v7, v5, v6

    #@104
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@107
    move-result-object v9

    #@108
    .line 507
    .local v9, caption:Ljava/lang/String;
    new-instance v16, Landroid/content/Intent;

    #@10a
    const-string v3, "android.btopp.intent.action.OPEN_OUTBOUND"

    #@10c
    move-object/from16 v0, v16

    #@10e
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@111
    .line 508
    .local v16, intent:Landroid/content/Intent;
    const-string v3, "com.android.bluetooth"

    #@113
    const-class v4, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@115
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@118
    move-result-object v4

    #@119
    move-object/from16 v0, v16

    #@11b
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@11e
    .line 509
    move-object/from16 v0, p0

    #@120
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@122
    move-object/from16 v0, p0

    #@124
    iget-object v4, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@126
    const/4 v5, 0x0

    #@127
    const/4 v6, 0x0

    #@128
    move-object/from16 v0, v16

    #@12a
    invoke-static {v4, v5, v0, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@12d
    move-result-object v4

    #@12e
    move-object/from16 v0, v17

    #@130
    move-object/from16 v1, v26

    #@132
    invoke-virtual {v0, v3, v1, v9, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@135
    .line 511
    new-instance v16, Landroid/content/Intent;

    #@137
    .end local v16           #intent:Landroid/content/Intent;
    const-string v3, "android.btopp.intent.action.HIDE_COMPLETE"

    #@139
    move-object/from16 v0, v16

    #@13b
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@13e
    .line 512
    .restart local v16       #intent:Landroid/content/Intent;
    const-string v3, "com.android.bluetooth"

    #@140
    const-class v4, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@142
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@145
    move-result-object v4

    #@146
    move-object/from16 v0, v16

    #@148
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@14b
    .line 513
    move-object/from16 v0, p0

    #@14d
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@14f
    const/4 v4, 0x0

    #@150
    const/4 v5, 0x0

    #@151
    move-object/from16 v0, v16

    #@153
    invoke-static {v3, v4, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@156
    move-result-object v3

    #@157
    move-object/from16 v0, v17

    #@159
    iput-object v3, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@15b
    .line 514
    move-wide/from16 v0, v23

    #@15d
    move-object/from16 v2, v17

    #@15f
    iput-wide v0, v2, Landroid/app/Notification;->when:J

    #@161
    .line 515
    move-object/from16 v0, p0

    #@163
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@165
    if-eqz v3, :cond_1b0

    #@167
    .line 516
    move-object/from16 v0, p0

    #@169
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@16b
    const v4, -0xf4245

    #@16e
    move-object/from16 v0, v17

    #@170
    invoke-virtual {v3, v4, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@173
    .line 534
    .end local v9           #caption:Ljava/lang/String;
    .end local v16           #intent:Landroid/content/Intent;
    .end local v17           #outNoti:Landroid/app/Notification;
    .end local v26           #title:Ljava/lang/String;
    :cond_173
    :goto_173
    :try_start_173
    move-object/from16 v0, p0

    #@175
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@177
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@17a
    move-result-object v3

    #@17b
    sget-object v4, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@17d
    const/4 v5, 0x0

    #@17e
    const-string v6, "status >= \'200\' AND (visibility IS NULL OR visibility == \'0\') AND (confirm != \'5\') AND (direction == 1)"

    #@180
    const/4 v7, 0x0

    #@181
    const-string v8, "timestamp DESC"

    #@183
    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_186
    .catch Ljava/lang/Exception; {:try_start_173 .. :try_end_186} :catch_1d0

    #@186
    move-result-object v10

    #@187
    .line 545
    if-eqz v10, :cond_19

    #@189
    .line 549
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    #@18c
    :goto_18c
    invoke-interface {v10}, Landroid/database/Cursor;->isAfterLast()Z

    #@18f
    move-result v3

    #@190
    if-nez v3, :cond_1f3

    #@192
    .line 550
    invoke-interface {v10}, Landroid/database/Cursor;->isFirst()Z

    #@195
    move-result v3

    #@196
    if-eqz v3, :cond_19e

    #@198
    .line 552
    move/from16 v0, v25

    #@19a
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    #@19d
    move-result-wide v23

    #@19e
    .line 554
    :cond_19e
    move/from16 v0, v22

    #@1a0
    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    #@1a3
    move-result v21

    #@1a4
    .line 556
    .restart local v21       #status:I
    invoke-static/range {v21 .. v21}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusError(I)Z

    #@1a7
    move-result v3

    #@1a8
    if-eqz v3, :cond_1f0

    #@1aa
    .line 557
    add-int/lit8 v13, v13, 0x1

    #@1ac
    .line 549
    :goto_1ac
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    #@1af
    goto :goto_18c

    #@1b0
    .line 519
    .end local v21           #status:I
    .restart local v9       #caption:Ljava/lang/String;
    .restart local v16       #intent:Landroid/content/Intent;
    .restart local v17       #outNoti:Landroid/app/Notification;
    .restart local v26       #title:Ljava/lang/String;
    :cond_1b0
    const-string v3, "BluetoothOppNotification"

    #@1b2
    const-string v4, "mNotificationMgr is NULL!"

    #@1b4
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b7
    goto :goto_173

    #@1b8
    .line 523
    .end local v9           #caption:Ljava/lang/String;
    .end local v16           #intent:Landroid/content/Intent;
    .end local v17           #outNoti:Landroid/app/Notification;
    .end local v26           #title:Ljava/lang/String;
    :cond_1b8
    move-object/from16 v0, p0

    #@1ba
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@1bc
    if-eqz v3, :cond_173

    #@1be
    .line 524
    move-object/from16 v0, p0

    #@1c0
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@1c2
    const v4, -0xf4245

    #@1c5
    invoke-virtual {v3, v4}, Landroid/app/NotificationManager;->cancel(I)V

    #@1c8
    .line 526
    const-string v3, "BluetoothOppNotification"

    #@1ca
    const-string v4, "outbound notification was removed."

    #@1cc
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1cf
    goto :goto_173

    #@1d0
    .line 536
    :catch_1d0
    move-exception v11

    #@1d1
    .line 537
    .restart local v11       #e:Ljava/lang/Exception;
    if-eqz v10, :cond_1d6

    #@1d3
    .line 538
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@1d6
    .line 540
    :cond_1d6
    const-string v3, "BluetoothOppNotification"

    #@1d8
    new-instance v4, Ljava/lang/StringBuilder;

    #@1da
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1dd
    const-string v5, "[BTUI] updateActiveNotification : "

    #@1df
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e2
    move-result-object v4

    #@1e3
    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e6
    move-result-object v4

    #@1e7
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ea
    move-result-object v4

    #@1eb
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1ee
    goto/16 :goto_19

    #@1f0
    .line 559
    .end local v11           #e:Ljava/lang/Exception;
    .restart local v21       #status:I
    :cond_1f0
    add-int/lit8 v15, v15, 0x1

    #@1f2
    goto :goto_1ac

    #@1f3
    .line 563
    .end local v21           #status:I
    :cond_1f3
    const-string v3, "BluetoothOppNotification"

    #@1f5
    new-instance v4, Ljava/lang/StringBuilder;

    #@1f7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1fa
    const-string v5, "inbound: succ-"

    #@1fc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ff
    move-result-object v4

    #@200
    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@203
    move-result-object v4

    #@204
    const-string v5, "  fail-"

    #@206
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@209
    move-result-object v4

    #@20a
    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20d
    move-result-object v4

    #@20e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@211
    move-result-object v4

    #@212
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@215
    .line 565
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@218
    .line 567
    add-int v14, v15, v13

    #@21a
    .line 569
    .local v14, inboundNum:I
    if-lez v14, :cond_2bb

    #@21c
    .line 570
    new-instance v12, Landroid/app/Notification;

    #@21e
    invoke-direct {v12}, Landroid/app/Notification;-><init>()V

    #@221
    .line 571
    .local v12, inNoti:Landroid/app/Notification;
    const v3, 0x1080082

    #@224
    iput v3, v12, Landroid/app/Notification;->icon:I

    #@226
    .line 572
    move-object/from16 v0, p0

    #@228
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@22a
    const v4, 0x7f07005b

    #@22d
    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@230
    move-result-object v26

    #@231
    .line 573
    .restart local v26       #title:Ljava/lang/String;
    move-object/from16 v0, p0

    #@233
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@235
    const v4, 0x7f07005c

    #@238
    const/4 v5, 0x2

    #@239
    new-array v5, v5, [Ljava/lang/Object;

    #@23b
    const/4 v6, 0x0

    #@23c
    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23f
    move-result-object v7

    #@240
    aput-object v7, v5, v6

    #@242
    const/4 v6, 0x1

    #@243
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@246
    move-result-object v7

    #@247
    aput-object v7, v5, v6

    #@249
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@24c
    move-result-object v9

    #@24d
    .line 575
    .restart local v9       #caption:Ljava/lang/String;
    new-instance v16, Landroid/content/Intent;

    #@24f
    const-string v3, "android.btopp.intent.action.OPEN_INBOUND"

    #@251
    move-object/from16 v0, v16

    #@253
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@256
    .line 576
    .restart local v16       #intent:Landroid/content/Intent;
    const-string v3, "com.android.bluetooth"

    #@258
    const-class v4, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@25a
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@25d
    move-result-object v4

    #@25e
    move-object/from16 v0, v16

    #@260
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@263
    .line 577
    move-object/from16 v0, p0

    #@265
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@267
    move-object/from16 v0, p0

    #@269
    iget-object v4, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@26b
    const/4 v5, 0x0

    #@26c
    const/4 v6, 0x0

    #@26d
    move-object/from16 v0, v16

    #@26f
    invoke-static {v4, v5, v0, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@272
    move-result-object v4

    #@273
    move-object/from16 v0, v26

    #@275
    invoke-virtual {v12, v3, v0, v9, v4}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@278
    .line 579
    new-instance v16, Landroid/content/Intent;

    #@27a
    .end local v16           #intent:Landroid/content/Intent;
    const-string v3, "android.btopp.intent.action.HIDE_COMPLETE"

    #@27c
    move-object/from16 v0, v16

    #@27e
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@281
    .line 580
    .restart local v16       #intent:Landroid/content/Intent;
    const-string v3, "com.android.bluetooth"

    #@283
    const-class v4, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@285
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@288
    move-result-object v4

    #@289
    move-object/from16 v0, v16

    #@28b
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@28e
    .line 581
    move-object/from16 v0, p0

    #@290
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@292
    const/4 v4, 0x0

    #@293
    const/4 v5, 0x0

    #@294
    move-object/from16 v0, v16

    #@296
    invoke-static {v3, v4, v0, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@299
    move-result-object v3

    #@29a
    iput-object v3, v12, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@29c
    .line 582
    move-wide/from16 v0, v23

    #@29e
    iput-wide v0, v12, Landroid/app/Notification;->when:J

    #@2a0
    .line 583
    move-object/from16 v0, p0

    #@2a2
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@2a4
    if-eqz v3, :cond_2b2

    #@2a6
    .line 584
    move-object/from16 v0, p0

    #@2a8
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@2aa
    const v4, -0xf4246

    #@2ad
    invoke-virtual {v3, v4, v12}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@2b0
    goto/16 :goto_19

    #@2b2
    .line 587
    :cond_2b2
    const-string v3, "BluetoothOppNotification"

    #@2b4
    const-string v4, "mNotificationMgr is NULL!"

    #@2b6
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2b9
    goto/16 :goto_19

    #@2bb
    .line 591
    .end local v9           #caption:Ljava/lang/String;
    .end local v12           #inNoti:Landroid/app/Notification;
    .end local v16           #intent:Landroid/content/Intent;
    .end local v26           #title:Ljava/lang/String;
    :cond_2bb
    move-object/from16 v0, p0

    #@2bd
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@2bf
    if-eqz v3, :cond_19

    #@2c1
    .line 592
    move-object/from16 v0, p0

    #@2c3
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@2c5
    const v4, -0xf4246

    #@2c8
    invoke-virtual {v3, v4}, Landroid/app/NotificationManager;->cancel(I)V

    #@2cb
    .line 594
    const-string v3, "BluetoothOppNotification"

    #@2cd
    const-string v4, "inbound notification was removed."

    #@2cf
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2d2
    goto/16 :goto_19
.end method

.method private updateIncomingFileConfirmNotification()V
    .registers 22

    #@0
    .prologue
    .line 602
    const/4 v14, 0x0

    #@1
    .line 603
    .local v14, remoteDevice:Landroid/bluetooth/BluetoothDevice;
    const/16 v18, 0x0

    #@3
    .line 604
    .local v18, transInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    const/16 v19, 0x0

    #@5
    .line 608
    .local v19, trust:Z
    const/4 v9, 0x0

    #@6
    .line 610
    .local v9, cursor:Landroid/database/Cursor;
    :try_start_6
    move-object/from16 v0, p0

    #@8
    iget-object v1, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@a
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v1

    #@e
    sget-object v2, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@10
    const/4 v3, 0x0

    #@11
    const-string v4, "confirm == \'0\' AND (visibility IS NULL OR visibility == \'0\')"

    #@13
    const/4 v5, 0x0

    #@14
    const-string v6, "_id"

    #@16
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_19} :catch_1d

    #@19
    move-result-object v9

    #@1a
    .line 621
    if-nez v9, :cond_3c

    #@1c
    .line 698
    :goto_1c
    return-void

    #@1d
    .line 612
    :catch_1d
    move-exception v10

    #@1e
    .line 613
    .local v10, e:Ljava/lang/Exception;
    if-eqz v9, :cond_23

    #@20
    .line 614
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@23
    .line 616
    :cond_23
    const-string v1, "BluetoothOppNotification"

    #@25
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v3, "[BTUI] updateActiveNotification : "

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v2

    #@38
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    goto :goto_1c

    #@3c
    .line 625
    .end local v10           #e:Ljava/lang/Exception;
    :cond_3c
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    #@3f
    :goto_3f
    invoke-interface {v9}, Landroid/database/Cursor;->isAfterLast()Z

    #@42
    move-result v1

    #@43
    if-nez v1, :cond_198

    #@45
    .line 626
    move-object/from16 v0, p0

    #@47
    iget-object v1, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@49
    const v2, 0x7f070017

    #@4c
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@4f
    move-result-object v17

    #@50
    .line 628
    .local v17, title:Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    #@52
    iget-object v1, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@54
    const v2, 0x7f070018

    #@57
    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    #@5a
    move-result-object v7

    #@5b
    .line 630
    .local v7, caption:Ljava/lang/CharSequence;
    const-string v1, "_id"

    #@5d
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@60
    move-result v1

    #@61
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    #@64
    move-result v11

    #@65
    .line 631
    .local v11, id:I
    const-string v1, "timestamp"

    #@67
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@6a
    move-result v1

    #@6b
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    #@6e
    move-result-wide v15

    #@6f
    .line 632
    .local v15, timeStamp:J
    new-instance v1, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    sget-object v2, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@76
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v1

    #@7a
    const-string v2, "/"

    #@7c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v1

    #@80
    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@83
    move-result-object v1

    #@84
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v1

    #@88
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@8b
    move-result-object v8

    #@8c
    .line 636
    .local v8, contentUri:Landroid/net/Uri;
    move-object/from16 v0, p0

    #@8e
    iget-object v1, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@90
    const-string v2, "destination"

    #@92
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@95
    move-result v2

    #@96
    invoke-interface {v9, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@99
    move-result-object v2

    #@9a
    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@9d
    move-result-object v14

    #@9e
    .line 637
    if-nez v14, :cond_ab

    #@a0
    .line 638
    const-string v1, "BluetoothOppNotification"

    #@a2
    const-string v2, "remoteDevice is null !!"

    #@a4
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 625
    :cond_a7
    :goto_a7
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    #@aa
    goto :goto_3f

    #@ab
    .line 641
    :cond_ab
    invoke-virtual {v14}, Landroid/bluetooth/BluetoothDevice;->getTrustState()Z

    #@ae
    move-result v19

    #@af
    .line 643
    const-string v1, "BluetoothOppNotification"

    #@b1
    new-instance v2, Ljava/lang/StringBuilder;

    #@b3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b6
    const-string v3, "contentUri: "

    #@b8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v2

    #@bc
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v2

    #@c0
    const-string v3, ", trust: "

    #@c2
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v2

    #@c6
    move/from16 v0, v19

    #@c8
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v2

    #@cc
    const-string v3, ", remoteDevice: "

    #@ce
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d1
    move-result-object v2

    #@d2
    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d5
    move-result-object v2

    #@d6
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d9
    move-result-object v2

    #@da
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@dd
    .line 645
    const-string v1, "confirm"

    #@df
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@e2
    move-result v1

    #@e3
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I

    #@e6
    move-result v13

    #@e7
    .line 647
    .local v13, newConfirm:I
    if-eqz v19, :cond_138

    #@e9
    .line 648
    new-instance v18, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@eb
    .end local v18           #transInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    invoke-direct/range {v18 .. v18}, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;-><init>()V

    #@ee
    .line 649
    .restart local v18       #transInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    move-object/from16 v0, p0

    #@f0
    iget-object v1, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@f2
    invoke-static {v1, v8}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->queryRecord(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@f5
    move-result-object v18

    #@f6
    .line 650
    if-eqz v18, :cond_a7

    #@f8
    .line 651
    new-instance v20, Landroid/content/ContentValues;

    #@fa
    invoke-direct/range {v20 .. v20}, Landroid/content/ContentValues;-><init>()V

    #@fd
    .line 652
    .local v20, value:Landroid/content/ContentValues;
    const-string v1, "confirm"

    #@ff
    const/4 v2, 0x1

    #@100
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@103
    move-result-object v2

    #@104
    move-object/from16 v0, v20

    #@106
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@109
    .line 654
    const-string v1, "BluetoothOppNotification"

    #@10b
    new-instance v2, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string v3, "uri: "

    #@112
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v2

    #@116
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@119
    move-result-object v2

    #@11a
    const-string v3, ", USER_CONFIRMATION_CONFIRMED"

    #@11c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v2

    #@120
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@123
    move-result-object v2

    #@124
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@127
    .line 656
    move-object/from16 v0, p0

    #@129
    iget-object v1, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@12b
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@12e
    move-result-object v1

    #@12f
    const/4 v2, 0x0

    #@130
    const/4 v3, 0x0

    #@131
    move-object/from16 v0, v20

    #@133
    invoke-virtual {v1, v8, v0, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@136
    goto/16 :goto_a7

    #@138
    .line 658
    .end local v20           #value:Landroid/content/ContentValues;
    :cond_138
    move-object/from16 v0, p0

    #@13a
    invoke-direct {v0, v11}, Lcom/android/bluetooth/opp/BluetoothOppNotification;->putAuthPopupId(I)Z

    #@13d
    move-result v1

    #@13e
    if-eqz v1, :cond_a7

    #@140
    .line 660
    const-string v1, "BluetoothOppNotification"

    #@142
    new-instance v2, Ljava/lang/StringBuilder;

    #@144
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@147
    const-string v3, "uri: "

    #@149
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14c
    move-result-object v2

    #@14d
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v2

    #@151
    const-string v3, ", USER_CONFIRMATION_CONFIRMED"

    #@153
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v2

    #@157
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v2

    #@15b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@15e
    .line 662
    new-instance v12, Landroid/content/Intent;

    #@160
    const-string v1, "android.bluetooth.device.action.CONNECTION_ACCESS_REQUEST"

    #@162
    invoke-direct {v12, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@165
    .line 663
    .local v12, intent:Landroid/content/Intent;
    const-string v1, "com.android.settings"

    #@167
    const-string v2, "com.android.settings.bluetooth.BluetoothPermissionRequest"

    #@169
    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@16c
    .line 664
    invoke-virtual {v12, v8}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@16f
    .line 665
    const-string v1, "android.bluetooth.device.extra.ACCESS_REQUEST_TYPE"

    #@171
    const/16 v2, 0x9

    #@173
    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@176
    .line 666
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    #@178
    invoke-virtual {v12, v1, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@17b
    .line 667
    const-string v1, "android.bluetooth.device.extra.PACKAGE_NAME"

    #@17d
    const-string v2, "com.android.bluetooth"

    #@17f
    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@182
    .line 668
    const-string v1, "android.bluetooth.device.extra.CLASS_NAME"

    #@184
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;

    #@186
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@189
    move-result-object v2

    #@18a
    invoke-virtual {v12, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@18d
    .line 669
    move-object/from16 v0, p0

    #@18f
    iget-object v1, v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mContext:Landroid/content/Context;

    #@191
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@193
    invoke-virtual {v1, v12, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@196
    goto/16 :goto_a7

    #@198
    .line 697
    .end local v7           #caption:Ljava/lang/CharSequence;
    .end local v8           #contentUri:Landroid/net/Uri;
    .end local v11           #id:I
    .end local v12           #intent:Landroid/content/Intent;
    .end local v13           #newConfirm:I
    .end local v15           #timeStamp:J
    .end local v17           #title:Ljava/lang/CharSequence;
    :cond_198
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@19b
    goto/16 :goto_1c
.end method


# virtual methods
.method public clearAuthPopupList()V
    .registers 4

    #@0
    .prologue
    .line 724
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->AUTH_POPUP:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_2b

    #@8
    .line 726
    const-string v0, "BluetoothOppNotification"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Clear mAuthPopup List: size: "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    sget-object v2, Lcom/android/bluetooth/opp/BluetoothOppNotification;->AUTH_POPUP:Ljava/util/HashMap;

    #@17
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@1a
    move-result v2

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 728
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->AUTH_POPUP:Ljava/util/HashMap;

    #@28
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@2b
    .line 730
    :cond_2b
    return-void
.end method

.method public removeAuthPopupId(I)V
    .registers 6
    .parameter "id"

    #@0
    .prologue
    .line 714
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->AUTH_POPUP:Ljava/util/HashMap;

    #@2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_69

    #@c
    .line 715
    const-string v0, "BluetoothOppNotification"

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "mAuthPopup.remove( "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    const-string v2, " ), size: "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    sget-object v2, Lcom/android/bluetooth/opp/BluetoothOppNotification;->AUTH_POPUP:Ljava/util/HashMap;

    #@25
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@28
    move-result v2

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 716
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->AUTH_POPUP:Ljava/util/HashMap;

    #@36
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@3d
    .line 718
    const-string v0, "BluetoothOppNotification"

    #@3f
    new-instance v1, Ljava/lang/StringBuilder;

    #@41
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@44
    const-string v2, "mAuthPopup.containsKey( "

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v1

    #@4e
    const-string v2, " ): "

    #@50
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v1

    #@54
    sget-object v2, Lcom/android/bluetooth/opp/BluetoothOppNotification;->AUTH_POPUP:Ljava/util/HashMap;

    #@56
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@59
    move-result-object v3

    #@5a
    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    #@5d
    move-result v2

    #@5e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v1

    #@66
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@69
    .line 721
    :cond_69
    return-void
.end method

.method public updateNotification()V
    .registers 4

    #@0
    .prologue
    .line 168
    monitor-enter p0

    #@1
    .line 169
    :try_start_1
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mPendingUpdate:I

    #@3
    add-int/lit8 v0, v0, 0x1

    #@5
    iput v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mPendingUpdate:I

    #@7
    .line 170
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mPendingUpdate:I

    #@9
    const/4 v1, 0x1

    #@a
    if-le v0, v1, :cond_15

    #@c
    .line 172
    const-string v0, "BluetoothOppNotification"

    #@e
    const-string v1, "update too frequent, put in queue"

    #@10
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 174
    monitor-exit p0

    #@14
    .line 183
    :goto_14
    return-void

    #@15
    .line 176
    :cond_15
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mHandler:Landroid/os/Handler;

    #@17
    const/4 v1, 0x0

    #@18
    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    #@1b
    move-result v0

    #@1c
    if-nez v0, :cond_31

    #@1e
    .line 178
    const-string v0, "BluetoothOppNotification"

    #@20
    const-string v1, "send message"

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 180
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mHandler:Landroid/os/Handler;

    #@27
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mHandler:Landroid/os/Handler;

    #@29
    const/4 v2, 0x0

    #@2a
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@2d
    move-result-object v1

    #@2e
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@31
    .line 182
    :cond_31
    monitor-exit p0

    #@32
    goto :goto_14

    #@33
    :catchall_33
    move-exception v0

    #@34
    monitor-exit p0
    :try_end_35
    .catchall {:try_start_1 .. :try_end_35} :catchall_33

    #@35
    throw v0
.end method
