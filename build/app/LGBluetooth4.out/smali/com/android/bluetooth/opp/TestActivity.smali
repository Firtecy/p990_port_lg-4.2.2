.class public Lcom/android/bluetooth/opp/TestActivity;
.super Landroid/app/Activity;
.source "TestActivity.java"


# instance fields
.field public ackRecordListener:Landroid/view/View$OnClickListener;

.field public currentInsert:Ljava/lang/String;

.field public deleteAllRecordListener:Landroid/view/View$OnClickListener;

.field public deleteRecordListener:Landroid/view/View$OnClickListener;

.field public insertRecordListener:Landroid/view/View$OnClickListener;

.field mAckView:Landroid/widget/EditText;

.field mAddressView:Landroid/widget/EditText;

.field public mCurrentByte:I

.field mDeleteView:Landroid/widget/EditText;

.field mInsertView:Landroid/widget/EditText;

.field mMediaView:Landroid/widget/EditText;

.field mUpdateView:Landroid/widget/EditText;

.field public notifyTcpServerListener:Landroid/view/View$OnClickListener;

.field server:Lcom/android/bluetooth/opp/TestTcpServer;

.field public startTcpServerListener:Landroid/view/View$OnClickListener;

.field public updateRecordListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    .line 75
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/android/bluetooth/opp/TestActivity;->mCurrentByte:I

    #@6
    .line 216
    new-instance v0, Lcom/android/bluetooth/opp/TestActivity$1;

    #@8
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/TestActivity$1;-><init>(Lcom/android/bluetooth/opp/TestActivity;)V

    #@b
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestActivity;->insertRecordListener:Landroid/view/View$OnClickListener;

    #@d
    .line 277
    new-instance v0, Lcom/android/bluetooth/opp/TestActivity$2;

    #@f
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/TestActivity$2;-><init>(Lcom/android/bluetooth/opp/TestActivity;)V

    #@12
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestActivity;->deleteRecordListener:Landroid/view/View$OnClickListener;

    #@14
    .line 285
    new-instance v0, Lcom/android/bluetooth/opp/TestActivity$3;

    #@16
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/TestActivity$3;-><init>(Lcom/android/bluetooth/opp/TestActivity;)V

    #@19
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestActivity;->updateRecordListener:Landroid/view/View$OnClickListener;

    #@1b
    .line 301
    new-instance v0, Lcom/android/bluetooth/opp/TestActivity$4;

    #@1d
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/TestActivity$4;-><init>(Lcom/android/bluetooth/opp/TestActivity;)V

    #@20
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestActivity;->ackRecordListener:Landroid/view/View$OnClickListener;

    #@22
    .line 316
    new-instance v0, Lcom/android/bluetooth/opp/TestActivity$5;

    #@24
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/TestActivity$5;-><init>(Lcom/android/bluetooth/opp/TestActivity;)V

    #@27
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestActivity;->deleteAllRecordListener:Landroid/view/View$OnClickListener;

    #@29
    .line 323
    new-instance v0, Lcom/android/bluetooth/opp/TestActivity$6;

    #@2b
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/TestActivity$6;-><init>(Lcom/android/bluetooth/opp/TestActivity;)V

    #@2e
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestActivity;->startTcpServerListener:Landroid/view/View$OnClickListener;

    #@30
    .line 332
    new-instance v0, Lcom/android/bluetooth/opp/TestActivity$7;

    #@32
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/TestActivity$7;-><init>(Lcom/android/bluetooth/opp/TestActivity;)V

    #@35
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestActivity;->notifyTcpServerListener:Landroid/view/View$OnClickListener;

    #@37
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .registers 21
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 94
    invoke-super/range {p0 .. p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/android/bluetooth/opp/TestActivity;->getIntent()Landroid/content/Intent;

    #@6
    move-result-object v10

    #@7
    .line 98
    .local v10, intent:Landroid/content/Intent;
    invoke-virtual {v10}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a
    move-result-object v7

    #@b
    .line 100
    .local v7, action:Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/android/bluetooth/opp/TestActivity;->getBaseContext()Landroid/content/Context;

    #@e
    move-result-object v8

    #@f
    .line 102
    .local v8, c:Landroid/content/Context;
    const-string v1, "android.intent.action.SEND"

    #@11
    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_5a

    #@17
    .line 109
    invoke-virtual {v10}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@1a
    move-result-object v18

    #@1b
    .line 110
    .local v18, type:Ljava/lang/String;
    const-string v1, "android.intent.extra.STREAM"

    #@1d
    invoke-virtual {v10, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@20
    move-result-object v2

    #@21
    check-cast v2, Landroid/net/Uri;

    #@23
    .line 112
    .local v2, stream:Landroid/net/Uri;
    if-eqz v2, :cond_5a

    #@25
    if-eqz v18, :cond_5a

    #@27
    .line 118
    const-string v1, "BluetoothOpp"

    #@29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, " Get share intent with Uri "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    const-string v4, " mimetype is "

    #@3a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v3

    #@3e
    move-object/from16 v0, v18

    #@40
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v3

    #@48
    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 122
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4e
    move-result-object v1

    #@4f
    const/4 v3, 0x0

    #@50
    const/4 v4, 0x0

    #@51
    const/4 v5, 0x0

    #@52
    const/4 v6, 0x0

    #@53
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@56
    move-result-object v9

    #@57
    .line 123
    .local v9, cursor:Landroid/database/Cursor;
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@5a
    .line 142
    .end local v2           #stream:Landroid/net/Uri;
    .end local v9           #cursor:Landroid/database/Cursor;
    .end local v18           #type:Ljava/lang/String;
    :cond_5a
    const v1, 0x7f03000e

    #@5d
    move-object/from16 v0, p0

    #@5f
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->setContentView(I)V

    #@62
    .line 144
    const v1, 0x7f0b0028

    #@65
    move-object/from16 v0, p0

    #@67
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@6a
    move-result-object v14

    #@6b
    check-cast v14, Landroid/widget/Button;

    #@6d
    .line 145
    .local v14, mInsertRecord:Landroid/widget/Button;
    const v1, 0x7f0b002a

    #@70
    move-object/from16 v0, p0

    #@72
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@75
    move-result-object v13

    #@76
    check-cast v13, Landroid/widget/Button;

    #@78
    .line 146
    .local v13, mDeleteRecord:Landroid/widget/Button;
    const v1, 0x7f0b002c

    #@7b
    move-object/from16 v0, p0

    #@7d
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@80
    move-result-object v17

    #@81
    check-cast v17, Landroid/widget/Button;

    #@83
    .line 148
    .local v17, mUpdateRecord:Landroid/widget/Button;
    const v1, 0x7f0b002e

    #@86
    move-object/from16 v0, p0

    #@88
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@8b
    move-result-object v11

    #@8c
    check-cast v11, Landroid/widget/Button;

    #@8e
    .line 150
    .local v11, mAckRecord:Landroid/widget/Button;
    const v1, 0x7f0b002f

    #@91
    move-object/from16 v0, p0

    #@93
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@96
    move-result-object v12

    #@97
    check-cast v12, Landroid/widget/Button;

    #@99
    .line 151
    .local v12, mDeleteAllRecord:Landroid/widget/Button;
    const v1, 0x7f0b002b

    #@9c
    move-object/from16 v0, p0

    #@9e
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@a1
    move-result-object v1

    #@a2
    check-cast v1, Landroid/widget/EditText;

    #@a4
    move-object/from16 v0, p0

    #@a6
    iput-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->mUpdateView:Landroid/widget/EditText;

    #@a8
    .line 152
    const v1, 0x7f0b002d

    #@ab
    move-object/from16 v0, p0

    #@ad
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@b0
    move-result-object v1

    #@b1
    check-cast v1, Landroid/widget/EditText;

    #@b3
    move-object/from16 v0, p0

    #@b5
    iput-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->mAckView:Landroid/widget/EditText;

    #@b7
    .line 153
    const v1, 0x7f0b0029

    #@ba
    move-object/from16 v0, p0

    #@bc
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@bf
    move-result-object v1

    #@c0
    check-cast v1, Landroid/widget/EditText;

    #@c2
    move-object/from16 v0, p0

    #@c4
    iput-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->mDeleteView:Landroid/widget/EditText;

    #@c6
    .line 154
    const v1, 0x7f0b0025

    #@c9
    move-object/from16 v0, p0

    #@cb
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@ce
    move-result-object v1

    #@cf
    check-cast v1, Landroid/widget/EditText;

    #@d1
    move-object/from16 v0, p0

    #@d3
    iput-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->mInsertView:Landroid/widget/EditText;

    #@d5
    .line 156
    const v1, 0x7f0b0027

    #@d8
    move-object/from16 v0, p0

    #@da
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@dd
    move-result-object v1

    #@de
    check-cast v1, Landroid/widget/EditText;

    #@e0
    move-object/from16 v0, p0

    #@e2
    iput-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->mAddressView:Landroid/widget/EditText;

    #@e4
    .line 157
    const v1, 0x7f0b0026

    #@e7
    move-object/from16 v0, p0

    #@e9
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@ec
    move-result-object v1

    #@ed
    check-cast v1, Landroid/widget/EditText;

    #@ef
    move-object/from16 v0, p0

    #@f1
    iput-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->mMediaView:Landroid/widget/EditText;

    #@f3
    .line 159
    move-object/from16 v0, p0

    #@f5
    iget-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->insertRecordListener:Landroid/view/View$OnClickListener;

    #@f7
    invoke-virtual {v14, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@fa
    .line 160
    move-object/from16 v0, p0

    #@fc
    iget-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->deleteRecordListener:Landroid/view/View$OnClickListener;

    #@fe
    invoke-virtual {v13, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@101
    .line 161
    move-object/from16 v0, p0

    #@103
    iget-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->updateRecordListener:Landroid/view/View$OnClickListener;

    #@105
    move-object/from16 v0, v17

    #@107
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@10a
    .line 162
    move-object/from16 v0, p0

    #@10c
    iget-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->ackRecordListener:Landroid/view/View$OnClickListener;

    #@10e
    invoke-virtual {v11, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@111
    .line 163
    move-object/from16 v0, p0

    #@113
    iget-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->deleteAllRecordListener:Landroid/view/View$OnClickListener;

    #@115
    invoke-virtual {v12, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@118
    .line 165
    const v1, 0x7f0b0030

    #@11b
    move-object/from16 v0, p0

    #@11d
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@120
    move-result-object v16

    #@121
    check-cast v16, Landroid/widget/Button;

    #@123
    .line 166
    .local v16, mStartTcpServer:Landroid/widget/Button;
    move-object/from16 v0, p0

    #@125
    iget-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->startTcpServerListener:Landroid/view/View$OnClickListener;

    #@127
    move-object/from16 v0, v16

    #@129
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@12c
    .line 168
    const v1, 0x7f0b0031

    #@12f
    move-object/from16 v0, p0

    #@131
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/TestActivity;->findViewById(I)Landroid/view/View;

    #@134
    move-result-object v15

    #@135
    check-cast v15, Landroid/widget/Button;

    #@137
    .line 169
    .local v15, mNotifyTcpServer:Landroid/widget/Button;
    move-object/from16 v0, p0

    #@139
    iget-object v1, v0, Lcom/android/bluetooth/opp/TestActivity;->notifyTcpServerListener:Landroid/view/View$OnClickListener;

    #@13b
    invoke-virtual {v15, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@13e
    .line 214
    return-void
.end method
