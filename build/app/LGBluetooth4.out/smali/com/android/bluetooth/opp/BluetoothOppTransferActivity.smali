.class public Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "BluetoothOppTransferActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/opp/BluetoothOppTransferActivity$BluetoothTransferContentObserver;
    }
.end annotation


# static fields
.field private static final D:Z = true

.field public static final DIALOG_RECEIVE_COMPLETE_FAIL:I = 0x2

.field public static final DIALOG_RECEIVE_COMPLETE_SUCCESS:I = 0x1

.field public static final DIALOG_RECEIVE_ONGOING:I = 0x0

.field public static final DIALOG_SEND_COMPLETE_FAIL:I = 0x5

.field public static final DIALOG_SEND_COMPLETE_SUCCESS:I = 0x4

.field public static final DIALOG_SEND_ONGOING:I = 0x3

.field private static final TAG:Ljava/lang/String; = "BluetoothOppTransferActivity"

.field private static final V:Z = true


# instance fields
.field private alertIcon:I

.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field mIsComplete:Z

.field private mLine1View:Landroid/widget/TextView;

.field private mLine2View:Landroid/widget/TextView;

.field private mLine3View:Landroid/widget/TextView;

.field private mLine5View:Landroid/widget/TextView;

.field private mNeedUpdateButton:Z

.field private mObserver:Lcom/android/bluetooth/opp/BluetoothOppTransferActivity$BluetoothTransferContentObserver;

.field private mPara:Lcom/android/internal/app/AlertController$AlertParams;

.field private mPercentView:Landroid/widget/TextView;

.field private mProgressTransfer:Landroid/widget/ProgressBar;

.field private mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

.field private mUri:Landroid/net/Uri;

.field private mView:Landroid/view/View;

.field private mWhichDialog:I


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    #@3
    .line 88
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@6
    .line 97
    const v0, 0x1010355

    #@9
    iput v0, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->alertIcon:I

    #@b
    .line 126
    const/4 v0, 0x0

    #@c
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mNeedUpdateButton:Z

    #@e
    .line 128
    return-void
.end method

.method static synthetic access$002(Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mNeedUpdateButton:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 69
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->updateProgressbar()V

    #@3
    return-void
.end method

.method private createView()Landroid/view/View;
    .registers 5

    #@0
    .prologue
    .line 287
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@3
    move-result-object v1

    #@4
    const v2, 0x7f03000a

    #@7
    const/4 v3, 0x0

    #@8
    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@e
    .line 290
    :try_start_e
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@10
    const v2, 0x7f0b001e

    #@13
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@16
    move-result-object v1

    #@17
    check-cast v1, Landroid/widget/ProgressBar;

    #@19
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mProgressTransfer:Landroid/widget/ProgressBar;

    #@1b
    .line 291
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@1d
    const v2, 0x7f0b001d

    #@20
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@23
    move-result-object v1

    #@24
    check-cast v1, Landroid/widget/TextView;

    #@26
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPercentView:Landroid/widget/TextView;

    #@28
    .line 293
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->customizeViewContent()V

    #@2b
    .line 296
    const/4 v1, 0x0

    #@2c
    iput-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mNeedUpdateButton:Z

    #@2e
    .line 297
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->updateProgressbar()V
    :try_end_31
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_31} :catch_34

    #@31
    .line 304
    :goto_31
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@33
    return-object v1

    #@34
    .line 299
    :catch_34
    move-exception v0

    #@35
    .line 300
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "BluetoothOppTransferActivity"

    #@37
    new-instance v2, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v3, "createView Exception: "

    #@3e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v2

    #@4e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 301
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@54
    goto :goto_31
.end method

.method private customizeViewContent()V
    .registers 9

    #@0
    .prologue
    const v7, 0x7f0b001a

    #@3
    const v4, 0x7f0b0019

    #@6
    const v3, 0x7f0b0018

    #@9
    const/4 v6, 0x0

    #@a
    const/4 v5, 0x1

    #@b
    .line 313
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@d
    if-eqz v1, :cond_13

    #@f
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@11
    if-ne v1, v5, :cond_b3

    #@13
    .line 315
    :cond_13
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@15
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@18
    move-result-object v1

    #@19
    check-cast v1, Landroid/widget/TextView;

    #@1b
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@1d
    .line 316
    const v1, 0x7f070022

    #@20
    new-array v2, v5, [Ljava/lang/Object;

    #@22
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@24
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@26
    aput-object v3, v2, v6

    #@28
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    .line 317
    .local v0, tmp:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@2e
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@31
    .line 318
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@33
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@36
    move-result-object v1

    #@37
    check-cast v1, Landroid/widget/TextView;

    #@39
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine2View:Landroid/widget/TextView;

    #@3b
    .line 319
    const v1, 0x7f070023

    #@3e
    new-array v2, v5, [Ljava/lang/Object;

    #@40
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@42
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@44
    aput-object v3, v2, v6

    #@46
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@49
    move-result-object v0

    #@4a
    .line 320
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine2View:Landroid/widget/TextView;

    #@4c
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@4f
    .line 321
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@51
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@54
    move-result-object v1

    #@55
    check-cast v1, Landroid/widget/TextView;

    #@57
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine3View:Landroid/widget/TextView;

    #@59
    .line 322
    const v1, 0x7f070024

    #@5c
    new-array v2, v5, [Ljava/lang/Object;

    #@5e
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@60
    iget-wide v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@62
    invoke-static {p0, v3, v4}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@65
    move-result-object v3

    #@66
    aput-object v3, v2, v6

    #@68
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@6b
    move-result-object v0

    #@6c
    .line 324
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine3View:Landroid/widget/TextView;

    #@6e
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@71
    .line 325
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@73
    const v2, 0x7f0b001c

    #@76
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@79
    move-result-object v1

    #@7a
    check-cast v1, Landroid/widget/TextView;

    #@7c
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine5View:Landroid/widget/TextView;

    #@7e
    .line 326
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@80
    if-nez v1, :cond_a7

    #@82
    .line 327
    const v1, 0x7f070026

    #@85
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@88
    move-result-object v0

    #@89
    .line 331
    :cond_89
    :goto_89
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine5View:Landroid/widget/TextView;

    #@8b
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@8e
    .line 395
    .end local v0           #tmp:Ljava/lang/String;
    :cond_8e
    :goto_8e
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@90
    iget v1, v1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@92
    invoke-static {v1}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusError(I)Z

    #@95
    move-result v1

    #@96
    if-eqz v1, :cond_a6

    #@98
    .line 396
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mProgressTransfer:Landroid/widget/ProgressBar;

    #@9a
    const/16 v2, 0x8

    #@9c
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    #@9f
    .line 397
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPercentView:Landroid/widget/TextView;

    #@a1
    const/16 v2, 0x8

    #@a3
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    #@a6
    .line 399
    :cond_a6
    return-void

    #@a7
    .line 328
    .restart local v0       #tmp:Ljava/lang/String;
    :cond_a7
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@a9
    if-ne v1, v5, :cond_89

    #@ab
    .line 329
    const v1, 0x7f07002d

    #@ae
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@b1
    move-result-object v0

    #@b2
    goto :goto_89

    #@b3
    .line 332
    .end local v0           #tmp:Ljava/lang/String;
    :cond_b3
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@b5
    const/4 v2, 0x3

    #@b6
    if-eq v1, v2, :cond_bd

    #@b8
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@ba
    const/4 v2, 0x4

    #@bb
    if-ne v1, v2, :cond_14f

    #@bd
    .line 334
    :cond_bd
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@bf
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@c2
    move-result-object v1

    #@c3
    check-cast v1, Landroid/widget/TextView;

    #@c5
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@c7
    .line 335
    const v1, 0x7f07002f

    #@ca
    new-array v2, v5, [Ljava/lang/Object;

    #@cc
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@ce
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@d0
    aput-object v3, v2, v6

    #@d2
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@d5
    move-result-object v0

    #@d6
    .line 336
    .restart local v0       #tmp:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@d8
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@db
    .line 337
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@dd
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@e0
    move-result-object v1

    #@e1
    check-cast v1, Landroid/widget/TextView;

    #@e3
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine2View:Landroid/widget/TextView;

    #@e5
    .line 338
    const v1, 0x7f070023

    #@e8
    new-array v2, v5, [Ljava/lang/Object;

    #@ea
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@ec
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@ee
    aput-object v3, v2, v6

    #@f0
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@f3
    move-result-object v0

    #@f4
    .line 339
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine2View:Landroid/widget/TextView;

    #@f6
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@f9
    .line 340
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@fb
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@fe
    move-result-object v1

    #@ff
    check-cast v1, Landroid/widget/TextView;

    #@101
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine3View:Landroid/widget/TextView;

    #@103
    .line 341
    const v1, 0x7f070030

    #@106
    const/4 v2, 0x2

    #@107
    new-array v2, v2, [Ljava/lang/Object;

    #@109
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@10b
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@10d
    aput-object v3, v2, v6

    #@10f
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@111
    iget-wide v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@113
    invoke-static {p0, v3, v4}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@116
    move-result-object v3

    #@117
    aput-object v3, v2, v5

    #@119
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@11c
    move-result-object v0

    #@11d
    .line 343
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine3View:Landroid/widget/TextView;

    #@11f
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@122
    .line 344
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@124
    const v2, 0x7f0b001c

    #@127
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@12a
    move-result-object v1

    #@12b
    check-cast v1, Landroid/widget/TextView;

    #@12d
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine5View:Landroid/widget/TextView;

    #@12f
    .line 345
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@131
    const/4 v2, 0x3

    #@132
    if-ne v1, v2, :cond_142

    #@134
    .line 346
    const v1, 0x7f070031

    #@137
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@13a
    move-result-object v0

    #@13b
    .line 350
    :cond_13b
    :goto_13b
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine5View:Landroid/widget/TextView;

    #@13d
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@140
    goto/16 :goto_8e

    #@142
    .line 347
    :cond_142
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@144
    const/4 v2, 0x4

    #@145
    if-ne v1, v2, :cond_13b

    #@147
    .line 348
    const v1, 0x7f070032

    #@14a
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@14d
    move-result-object v0

    #@14e
    goto :goto_13b

    #@14f
    .line 351
    .end local v0           #tmp:Ljava/lang/String;
    :cond_14f
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@151
    const/4 v2, 0x2

    #@152
    if-ne v1, v2, :cond_22b

    #@154
    .line 352
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@156
    iget v1, v1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@158
    const/16 v2, 0x1ee

    #@15a
    if-ne v1, v2, :cond_1d0

    #@15c
    .line 353
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@15e
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@161
    move-result-object v1

    #@162
    check-cast v1, Landroid/widget/TextView;

    #@164
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@166
    .line 354
    const v1, 0x7f070045

    #@169
    new-array v2, v5, [Ljava/lang/Object;

    #@16b
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@16d
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@16f
    aput-object v3, v2, v6

    #@171
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@174
    move-result-object v0

    #@175
    .line 355
    .restart local v0       #tmp:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@177
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@17a
    .line 356
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@17c
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@17f
    move-result-object v1

    #@180
    check-cast v1, Landroid/widget/TextView;

    #@182
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine2View:Landroid/widget/TextView;

    #@184
    .line 357
    const v1, 0x7f07002a

    #@187
    new-array v2, v5, [Ljava/lang/Object;

    #@189
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@18b
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@18d
    aput-object v3, v2, v6

    #@18f
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@192
    move-result-object v0

    #@193
    .line 358
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine2View:Landroid/widget/TextView;

    #@195
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@198
    .line 359
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@19a
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@19d
    move-result-object v1

    #@19e
    check-cast v1, Landroid/widget/TextView;

    #@1a0
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine3View:Landroid/widget/TextView;

    #@1a2
    .line 360
    const v1, 0x7f070046

    #@1a5
    new-array v2, v5, [Ljava/lang/Object;

    #@1a7
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@1a9
    iget-wide v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@1ab
    invoke-static {p0, v3, v4}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@1ae
    move-result-object v3

    #@1af
    aput-object v3, v2, v6

    #@1b1
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1b4
    move-result-object v0

    #@1b5
    .line 362
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine3View:Landroid/widget/TextView;

    #@1b7
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1ba
    .line 375
    :goto_1ba
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@1bc
    const v2, 0x7f0b001c

    #@1bf
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1c2
    move-result-object v1

    #@1c3
    check-cast v1, Landroid/widget/TextView;

    #@1c5
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine5View:Landroid/widget/TextView;

    #@1c7
    .line 376
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine5View:Landroid/widget/TextView;

    #@1c9
    const/16 v2, 0x8

    #@1cb
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    #@1ce
    goto/16 :goto_8e

    #@1d0
    .line 364
    .end local v0           #tmp:Ljava/lang/String;
    :cond_1d0
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@1d2
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1d5
    move-result-object v1

    #@1d6
    check-cast v1, Landroid/widget/TextView;

    #@1d8
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@1da
    .line 365
    const v1, 0x7f070029

    #@1dd
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@1e0
    move-result-object v0

    #@1e1
    .line 366
    .restart local v0       #tmp:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@1e3
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1e6
    .line 367
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@1e8
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@1eb
    move-result-object v1

    #@1ec
    check-cast v1, Landroid/widget/TextView;

    #@1ee
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine2View:Landroid/widget/TextView;

    #@1f0
    .line 368
    const v1, 0x7f07002a

    #@1f3
    new-array v2, v5, [Ljava/lang/Object;

    #@1f5
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@1f7
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@1f9
    aput-object v3, v2, v6

    #@1fb
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1fe
    move-result-object v0

    #@1ff
    .line 369
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine2View:Landroid/widget/TextView;

    #@201
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@204
    .line 370
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@206
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@209
    move-result-object v1

    #@20a
    check-cast v1, Landroid/widget/TextView;

    #@20c
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine3View:Landroid/widget/TextView;

    #@20e
    .line 371
    const v1, 0x7f07002b

    #@211
    new-array v2, v5, [Ljava/lang/Object;

    #@213
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@215
    iget v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@217
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@219
    iget-object v4, v4, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@21b
    invoke-static {p0, v3, v4}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->getStatusDescription(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    #@21e
    move-result-object v3

    #@21f
    aput-object v3, v2, v6

    #@221
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@224
    move-result-object v0

    #@225
    .line 373
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine3View:Landroid/widget/TextView;

    #@227
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@22a
    goto :goto_1ba

    #@22b
    .line 377
    .end local v0           #tmp:Ljava/lang/String;
    :cond_22b
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@22d
    const/4 v2, 0x5

    #@22e
    if-ne v1, v2, :cond_8e

    #@230
    .line 378
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@232
    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@235
    move-result-object v1

    #@236
    check-cast v1, Landroid/widget/TextView;

    #@238
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@23a
    .line 379
    const v1, 0x7f070034

    #@23d
    new-array v2, v5, [Ljava/lang/Object;

    #@23f
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@241
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@243
    aput-object v3, v2, v6

    #@245
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@248
    move-result-object v0

    #@249
    .line 381
    .restart local v0       #tmp:Ljava/lang/String;
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@24b
    const/4 v2, 0x5

    #@24c
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextDirection(I)V

    #@24f
    .line 383
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine1View:Landroid/widget/TextView;

    #@251
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@254
    .line 384
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@256
    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@259
    move-result-object v1

    #@25a
    check-cast v1, Landroid/widget/TextView;

    #@25c
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine2View:Landroid/widget/TextView;

    #@25e
    .line 385
    const v1, 0x7f070035

    #@261
    new-array v2, v5, [Ljava/lang/Object;

    #@263
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@265
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@267
    aput-object v3, v2, v6

    #@269
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@26c
    move-result-object v0

    #@26d
    .line 386
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine2View:Landroid/widget/TextView;

    #@26f
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@272
    .line 387
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@274
    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@277
    move-result-object v1

    #@278
    check-cast v1, Landroid/widget/TextView;

    #@27a
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine3View:Landroid/widget/TextView;

    #@27c
    .line 388
    const v1, 0x7f07002b

    #@27f
    new-array v2, v5, [Ljava/lang/Object;

    #@281
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@283
    iget v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@285
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@287
    iget-object v4, v4, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@289
    invoke-static {p0, v3, v4}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->getStatusDescription(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    #@28c
    move-result-object v3

    #@28d
    aput-object v3, v2, v6

    #@28f
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@292
    move-result-object v0

    #@293
    .line 390
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine3View:Landroid/widget/TextView;

    #@295
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@298
    .line 391
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mView:Landroid/view/View;

    #@29a
    const v2, 0x7f0b001c

    #@29d
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@2a0
    move-result-object v1

    #@2a1
    check-cast v1, Landroid/widget/TextView;

    #@2a3
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine5View:Landroid/widget/TextView;

    #@2a5
    .line 392
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mLine5View:Landroid/widget/TextView;

    #@2a7
    const/16 v2, 0x8

    #@2a9
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    #@2ac
    goto/16 :goto_8e
.end method

.method private displayWhichDialog()V
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 194
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@3
    iget v0, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDirection:I

    #@5
    .line 195
    .local v0, direction:I
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@7
    iget v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@9
    invoke-static {v3}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusSuccess(I)Z

    #@c
    move-result v2

    #@d
    .line 196
    .local v2, isSuccess:Z
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@f
    iget v3, v3, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@11
    invoke-static {v3}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z

    #@14
    move-result v1

    #@15
    .line 198
    .local v1, isComplete:Z
    if-ne v0, v4, :cond_50

    #@17
    .line 199
    if-ne v1, v4, :cond_4a

    #@19
    .line 200
    if-ne v2, v4, :cond_44

    #@1b
    .line 202
    iput v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@1d
    .line 223
    :cond_1d
    :goto_1d
    const-string v3, "BluetoothOppTransferActivity"

    #@1f
    new-instance v4, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v5, " WhichDialog/dir/isComplete/failOrSuccess"

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    iget v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@2c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v4

    #@30
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v4

    #@3c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3f
    move-result-object v4

    #@40
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 226
    return-void

    #@44
    .line 203
    :cond_44
    if-nez v2, :cond_1d

    #@46
    .line 204
    const/4 v3, 0x2

    #@47
    iput v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@49
    goto :goto_1d

    #@4a
    .line 206
    :cond_4a
    if-nez v1, :cond_1d

    #@4c
    .line 207
    const/4 v3, 0x0

    #@4d
    iput v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@4f
    goto :goto_1d

    #@50
    .line 209
    :cond_50
    if-nez v0, :cond_1d

    #@52
    .line 210
    if-ne v1, v4, :cond_60

    #@54
    .line 211
    if-ne v2, v4, :cond_5a

    #@56
    .line 212
    const/4 v3, 0x4

    #@57
    iput v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@59
    goto :goto_1d

    #@5a
    .line 214
    :cond_5a
    if-nez v2, :cond_1d

    #@5c
    .line 215
    const/4 v3, 0x5

    #@5d
    iput v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@5f
    goto :goto_1d

    #@60
    .line 217
    :cond_60
    if-nez v1, :cond_1d

    #@62
    .line 218
    const/4 v3, 0x3

    #@63
    iput v3, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@65
    goto :goto_1d
.end method

.method private setUpDialog()V
    .registers 5

    #@0
    .prologue
    .line 230
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    #@2
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@4
    .line 236
    :try_start_4
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@6
    const v2, 0x7f070021

    #@9
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    #@f
    .line 238
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@11
    if-eqz v1, :cond_18

    #@13
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@15
    const/4 v2, 0x3

    #@16
    if-ne v1, v2, :cond_42

    #@18
    .line 239
    :cond_18
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@1a
    const v2, 0x7f070028

    #@1d
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@23
    .line 240
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@25
    iput-object p0, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@27
    .line 241
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@29
    const v2, 0x7f070027

    #@2c
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@2f
    move-result-object v2

    #@30
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@32
    .line 242
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@34
    iput-object p0, v1, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@36
    .line 276
    :cond_36
    :goto_36
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@38
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->createView()Landroid/view/View;

    #@3b
    move-result-object v2

    #@3c
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    #@3e
    .line 277
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->setupAlert()V

    #@41
    .line 283
    :goto_41
    return-void

    #@42
    .line 243
    :cond_42
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@44
    const/4 v2, 0x1

    #@45
    if-ne v1, v2, :cond_78

    #@47
    .line 244
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@49
    const v2, 0x7f07002e

    #@4c
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@4f
    move-result-object v2

    #@50
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@52
    .line 245
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@54
    iput-object p0, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;
    :try_end_56
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_56} :catch_57

    #@56
    goto :goto_36

    #@57
    .line 278
    :catch_57
    move-exception v0

    #@58
    .line 279
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "BluetoothOppTransferActivity"

    #@5a
    new-instance v2, Ljava/lang/StringBuilder;

    #@5c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5f
    const-string v3, "setUpDialog Exception: "

    #@61
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v2

    #@65
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@68
    move-result-object v3

    #@69
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v2

    #@6d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v2

    #@71
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 280
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@77
    goto :goto_41

    #@78
    .line 246
    .end local v0           #e:Ljava/lang/Exception;
    :cond_78
    :try_start_78
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@7a
    const/4 v2, 0x2

    #@7b
    if-ne v1, v2, :cond_93

    #@7d
    .line 249
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@7f
    iget v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->alertIcon:I

    #@81
    iput v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    #@83
    .line 251
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@85
    const v2, 0x7f07002c

    #@88
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@8b
    move-result-object v2

    #@8c
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@8e
    .line 252
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@90
    iput-object p0, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@92
    goto :goto_36

    #@93
    .line 253
    :cond_93
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@95
    const/4 v2, 0x4

    #@96
    if-ne v1, v2, :cond_a8

    #@98
    .line 254
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@9a
    const v2, 0x7f070033

    #@9d
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@a0
    move-result-object v2

    #@a1
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@a3
    .line 255
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@a5
    iput-object p0, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@a7
    goto :goto_36

    #@a8
    .line 256
    :cond_a8
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@aa
    const/4 v2, 0x5

    #@ab
    if-ne v1, v2, :cond_36

    #@ad
    .line 259
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@af
    iget v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->alertIcon:I

    #@b1
    iput v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    #@b3
    .line 267
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@b5
    if-eqz v1, :cond_ce

    #@b7
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@b9
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@bc
    move-result v1

    #@bd
    if-eqz v1, :cond_ce

    #@bf
    .line 268
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@c1
    const v2, 0x7f070036

    #@c4
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@c7
    move-result-object v2

    #@c8
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@ca
    .line 269
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@cc
    iput-object p0, v1, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@ce
    .line 273
    :cond_ce
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@d0
    const v2, 0x7f070037

    #@d3
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@d6
    move-result-object v2

    #@d7
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@d9
    .line 274
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPara:Lcom/android/internal/app/AlertController$AlertParams;

    #@db
    iput-object p0, v1, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;
    :try_end_dd
    .catch Ljava/lang/Exception; {:try_start_78 .. :try_end_dd} :catch_57

    #@dd
    goto/16 :goto_36
.end method

.method private updateButton()V
    .registers 7

    #@0
    .prologue
    const/16 v5, 0x8

    #@2
    const/4 v4, -0x2

    #@3
    const/4 v3, -0x1

    #@4
    .line 541
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@6
    if-nez v1, :cond_9

    #@8
    .line 594
    :cond_8
    :goto_8
    return-void

    #@9
    .line 545
    :cond_9
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@b
    const/4 v2, 0x1

    #@c
    if-ne v1, v2, :cond_28

    #@e
    .line 546
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@10
    invoke-virtual {v1, v4}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    #@17
    .line 547
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@19
    invoke-virtual {v1, v3}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@1c
    move-result-object v1

    #@1d
    const v2, 0x7f07002e

    #@20
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@27
    goto :goto_8

    #@28
    .line 549
    :cond_28
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@2a
    const/4 v2, 0x2

    #@2b
    if-ne v1, v2, :cond_4e

    #@2d
    .line 552
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@2f
    iget v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->alertIcon:I

    #@31
    invoke-virtual {v1, v2}, Lcom/android/internal/app/AlertController;->setIcon(I)V

    #@34
    .line 554
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@36
    invoke-virtual {v1, v4}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@39
    move-result-object v1

    #@3a
    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    #@3d
    .line 555
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@3f
    invoke-virtual {v1, v3}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@42
    move-result-object v1

    #@43
    const v2, 0x7f07002c

    #@46
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@4d
    goto :goto_8

    #@4e
    .line 557
    :cond_4e
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@50
    const/4 v2, 0x4

    #@51
    if-ne v1, v2, :cond_6d

    #@53
    .line 558
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@55
    invoke-virtual {v1, v4}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    #@5c
    .line 559
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@5e
    invoke-virtual {v1, v3}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@61
    move-result-object v1

    #@62
    const v2, 0x7f070033

    #@65
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@68
    move-result-object v2

    #@69
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@6c
    goto :goto_8

    #@6d
    .line 561
    :cond_6d
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@6f
    const/4 v2, 0x5

    #@70
    if-ne v1, v2, :cond_8

    #@72
    .line 564
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@74
    iget v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->alertIcon:I

    #@76
    invoke-virtual {v1, v2}, Lcom/android/internal/app/AlertController;->setIcon(I)V

    #@79
    .line 572
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@7b
    if-eqz v1, :cond_a9

    #@7d
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@7f
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@82
    move-result v1

    #@83
    if-eqz v1, :cond_a9

    #@85
    .line 573
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@87
    invoke-virtual {v1, v3}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@8a
    move-result-object v1

    #@8b
    const v2, 0x7f070036

    #@8e
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@91
    move-result-object v2

    #@92
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@95
    .line 585
    :goto_95
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@97
    invoke-virtual {v1, v4}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@9a
    move-result-object v0

    #@9b
    .line 586
    .local v0, mAlert_Button:Landroid/widget/Button;
    if-eqz v0, :cond_b3

    #@9d
    .line 587
    const v1, 0x7f070037

    #@a0
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I)Ljava/lang/String;

    #@a3
    move-result-object v1

    #@a4
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    #@a7
    goto/16 :goto_8

    #@a9
    .line 576
    .end local v0           #mAlert_Button:Landroid/widget/Button;
    :cond_a9
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@ab
    invoke-virtual {v1, v3}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@ae
    move-result-object v1

    #@af
    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    #@b2
    goto :goto_95

    #@b3
    .line 589
    .restart local v0       #mAlert_Button:Landroid/widget/Button;
    :cond_b3
    const-string v1, "BluetoothOppTransferActivity"

    #@b5
    const-string v2, "[BTUI] mAlert_Button is null"

    #@b7
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ba
    goto/16 :goto_8
.end method

.method private updateProgressbar()V
    .registers 8

    #@0
    .prologue
    const-wide/32 v5, 0x7fffffff

    #@3
    const/4 v4, 0x1

    #@4
    .line 483
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mUri:Landroid/net/Uri;

    #@6
    invoke-static {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->queryRecord(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@9
    move-result-object v1

    #@a
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@c
    .line 484
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@e
    if-nez v1, :cond_18

    #@10
    .line 486
    const-string v1, "BluetoothOppTransferActivity"

    #@12
    const-string v2, "Error: Can not get data from db"

    #@14
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@17
    .line 534
    :cond_17
    :goto_17
    return-void

    #@18
    .line 494
    :cond_18
    :try_start_18
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@1a
    iget-wide v1, v1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@1c
    long-to-int v1, v1

    #@1d
    if-gez v1, :cond_33

    #@1f
    .line 495
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@21
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@23
    iget-wide v2, v2, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mCurrentBytes:J

    #@25
    shr-long/2addr v2, v4

    #@26
    and-long/2addr v2, v5

    #@27
    iput-wide v2, v1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mCurrentBytes:J

    #@29
    .line 496
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@2b
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@2d
    iget-wide v2, v2, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@2f
    shr-long/2addr v2, v4

    #@30
    and-long/2addr v2, v5

    #@31
    iput-wide v2, v1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@33
    .line 500
    :cond_33
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@35
    iget-wide v1, v1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@37
    const-wide/16 v3, 0x0

    #@39
    cmp-long v1, v1, v3

    #@3b
    if-nez v1, :cond_9d

    #@3d
    .line 503
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mProgressTransfer:Landroid/widget/ProgressBar;

    #@3f
    const/16 v2, 0x64

    #@41
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    #@44
    .line 513
    :goto_44
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mProgressTransfer:Landroid/widget/ProgressBar;

    #@46
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@48
    iget-wide v2, v2, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mCurrentBytes:J

    #@4a
    long-to-int v2, v2

    #@4b
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    #@4e
    .line 516
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mPercentView:Landroid/widget/TextView;

    #@50
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@52
    iget-wide v2, v2, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@54
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@56
    iget-wide v4, v4, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mCurrentBytes:J

    #@58
    invoke-static {v2, v3, v4, v5}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->formatProgressText(JJ)Ljava/lang/String;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@5f
    .line 523
    iget-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mIsComplete:Z

    #@61
    if-nez v1, :cond_17

    #@63
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@65
    iget v1, v1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@67
    invoke-static {v1}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z

    #@6a
    move-result v1

    #@6b
    if-eqz v1, :cond_17

    #@6d
    iget-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mNeedUpdateButton:Z

    #@6f
    if-eqz v1, :cond_17

    #@71
    .line 525
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->displayWhichDialog()V

    #@74
    .line 526
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->updateButton()V

    #@77
    .line 527
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->customizeViewContent()V
    :try_end_7a
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_7a} :catch_7b

    #@7a
    goto :goto_17

    #@7b
    .line 529
    :catch_7b
    move-exception v0

    #@7c
    .line 530
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "BluetoothOppTransferActivity"

    #@7e
    new-instance v2, Ljava/lang/StringBuilder;

    #@80
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@83
    const-string v3, "updateProgressbar Exception: "

    #@85
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v2

    #@89
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@8c
    move-result-object v3

    #@8d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v2

    #@91
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v2

    #@95
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 531
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@9b
    goto/16 :goto_17

    #@9d
    .line 507
    .end local v0           #e:Ljava/lang/Exception;
    :cond_9d
    :try_start_9d
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mProgressTransfer:Landroid/widget/ProgressBar;

    #@9f
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@a1
    iget-wide v2, v2, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTotalBytes:J

    #@a3
    long-to-int v2, v2

    #@a4
    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V
    :try_end_a7
    .catch Ljava/lang/Exception; {:try_start_9d .. :try_end_a7} :catch_7b

    #@a7
    goto :goto_44
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 13
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    const/4 v5, 0x5

    #@2
    const/4 v8, 0x3

    #@3
    const/4 v6, 0x1

    #@4
    const/4 v7, 0x0

    #@5
    .line 402
    packed-switch p2, :pswitch_data_10a

    #@8
    .line 476
    :cond_8
    :goto_8
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->finish()V

    #@b
    .line 477
    return-void

    #@c
    .line 404
    :pswitch_c
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@e
    if-ne v4, v6, :cond_36

    #@10
    .line 406
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@12
    iget-object v4, v4, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@14
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@16
    iget-object v5, v5, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@18
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@1a
    iget-object v6, v6, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTimeStamp:Ljava/lang/Long;

    #@1c
    iget-object v7, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mUri:Landroid/net/Uri;

    #@1e
    invoke-static {p0, v4, v5, v6, v7}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->openReceivedFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Landroid/net/Uri;)V

    #@21
    .line 410
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mUri:Landroid/net/Uri;

    #@23
    invoke-static {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToHidden(Landroid/content/Context;Landroid/net/Uri;)V

    #@26
    .line 413
    const-string v4, "notification"

    #@28
    invoke-virtual {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2b
    move-result-object v4

    #@2c
    check-cast v4, Landroid/app/NotificationManager;

    #@2e
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@30
    iget v5, v5, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mID:I

    #@32
    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@35
    goto :goto_8

    #@36
    .line 415
    :cond_36
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@38
    if-ne v4, v5, :cond_78

    #@3a
    .line 419
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mUri:Landroid/net/Uri;

    #@3c
    invoke-static {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToHidden(Landroid/content/Context;Landroid/net/Uri;)V

    #@3f
    .line 422
    const-string v4, "notification"

    #@41
    invoke-virtual {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@44
    move-result-object v4

    #@45
    check-cast v4, Landroid/app/NotificationManager;

    #@47
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@49
    iget v5, v5, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mID:I

    #@4b
    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@4e
    .line 426
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@50
    invoke-static {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->retryTransfer(Landroid/content/Context;Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;)V

    #@53
    .line 428
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@55
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@57
    iget-object v5, v5, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDestAddr:Ljava/lang/String;

    #@59
    invoke-virtual {v4, v5}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@5c
    move-result-object v3

    #@5d
    .line 431
    .local v3, remoteDevice:Landroid/bluetooth/BluetoothDevice;
    const v4, 0x7f070042

    #@60
    new-array v5, v6, [Ljava/lang/Object;

    #@62
    invoke-static {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6, v3}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getDeviceName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    aput-object v6, v5, v7

    #@6c
    invoke-virtual {p0, v4, v5}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@6f
    move-result-object v4

    #@70
    invoke-static {p0, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@73
    move-result-object v4

    #@74
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    #@77
    goto :goto_8

    #@78
    .line 437
    .end local v3           #remoteDevice:Landroid/bluetooth/BluetoothDevice;
    :cond_78
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@7a
    const/4 v5, 0x4

    #@7b
    if-ne v4, v5, :cond_8

    #@7d
    .line 438
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mUri:Landroid/net/Uri;

    #@7f
    invoke-static {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToHidden(Landroid/content/Context;Landroid/net/Uri;)V

    #@82
    .line 439
    const-string v4, "notification"

    #@84
    invoke-virtual {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@87
    move-result-object v4

    #@88
    check-cast v4, Landroid/app/NotificationManager;

    #@8a
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@8c
    iget v5, v5, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mID:I

    #@8e
    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@91
    goto/16 :goto_8

    #@93
    .line 445
    :pswitch_93
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@95
    if-eqz v4, :cond_9b

    #@97
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@99
    if-ne v4, v8, :cond_e5

    #@9b
    .line 447
    :cond_9b
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@9e
    move-result-object v4

    #@9f
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mUri:Landroid/net/Uri;

    #@a1
    invoke-virtual {v4, v5, v9, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@a4
    .line 449
    const-string v2, ""

    #@a6
    .line 450
    .local v2, msg:Ljava/lang/String;
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@a8
    if-nez v4, :cond_d1

    #@aa
    .line 451
    const v4, 0x7f070041

    #@ad
    new-array v5, v6, [Ljava/lang/Object;

    #@af
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@b1
    iget-object v6, v6, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@b3
    aput-object v6, v5, v7

    #@b5
    invoke-virtual {p0, v4, v5}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@b8
    move-result-object v2

    #@b9
    .line 455
    :cond_b9
    :goto_b9
    invoke-static {p0, v2, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@bc
    move-result-object v4

    #@bd
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    #@c0
    .line 457
    const-string v4, "notification"

    #@c2
    invoke-virtual {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c5
    move-result-object v4

    #@c6
    check-cast v4, Landroid/app/NotificationManager;

    #@c8
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@ca
    iget v5, v5, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mID:I

    #@cc
    invoke-virtual {v4, v5}, Landroid/app/NotificationManager;->cancel(I)V

    #@cf
    goto/16 :goto_8

    #@d1
    .line 452
    :cond_d1
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@d3
    if-ne v4, v8, :cond_b9

    #@d5
    .line 453
    const v4, 0x7f070044

    #@d8
    new-array v5, v6, [Ljava/lang/Object;

    #@da
    iget-object v6, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@dc
    iget-object v6, v6, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDeviceName:Ljava/lang/String;

    #@de
    aput-object v6, v5, v7

    #@e0
    invoke-virtual {p0, v4, v5}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@e3
    move-result-object v2

    #@e4
    goto :goto_b9

    #@e5
    .line 459
    .end local v2           #msg:Ljava/lang/String;
    :cond_e5
    iget v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@e7
    if-ne v4, v5, :cond_8

    #@e9
    .line 461
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mUri:Landroid/net/Uri;

    #@eb
    invoke-static {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToHidden(Landroid/content/Context;Landroid/net/Uri;)V

    #@ee
    .line 463
    new-instance v1, Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@f0
    invoke-direct {v1, p0}, Lcom/android/bluetooth/opp/BluetoothOppNotification;-><init>(Landroid/content/Context;)V

    #@f3
    .line 464
    .local v1, mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@f6
    move-result-object v0

    #@f7
    .line 465
    .local v0, adapter:Landroid/bluetooth/BluetoothAdapter;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@fa
    move-result v4

    #@fb
    if-nez v4, :cond_8

    #@fd
    .line 466
    const-string v4, "BluetoothOppTransferActivity"

    #@ff
    const-string v5, "Bluetooth is not enabled, update notification manually."

    #@101
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@104
    .line 467
    invoke-virtual {v1}, Lcom/android/bluetooth/opp/BluetoothOppNotification;->updateNotification()V

    #@107
    goto/16 :goto_8

    #@109
    .line 402
    nop

    #@10a
    :pswitch_data_10a
    .packed-switch -0x2
        :pswitch_93
        :pswitch_c
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 145
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 146
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getIntent()Landroid/content/Intent;

    #@6
    move-result-object v0

    #@7
    .line 147
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@a
    move-result-object v1

    #@b
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mUri:Landroid/net/Uri;

    #@d
    .line 149
    new-instance v1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@f
    invoke-direct {v1}, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;-><init>()V

    #@12
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@14
    .line 150
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mUri:Landroid/net/Uri;

    #@16
    invoke-static {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->queryRecord(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@19
    move-result-object v1

    #@1a
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@1c
    .line 151
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@1e
    if-nez v1, :cond_2b

    #@20
    .line 153
    const-string v1, "BluetoothOppTransferActivity"

    #@22
    const-string v2, "Error: Can not get data from db"

    #@24
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 155
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->finish()V

    #@2a
    .line 179
    :goto_2a
    return-void

    #@2b
    .line 159
    :cond_2b
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mTransInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@2d
    iget v1, v1, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@2f
    invoke-static {v1}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z

    #@32
    move-result v1

    #@33
    iput-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mIsComplete:Z

    #@35
    .line 161
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->displayWhichDialog()V

    #@38
    .line 164
    iget-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mIsComplete:Z

    #@3a
    if-nez v1, :cond_4f

    #@3c
    .line 165
    new-instance v1, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity$BluetoothTransferContentObserver;

    #@3e
    invoke-direct {v1, p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity$BluetoothTransferContentObserver;-><init>(Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;)V

    #@41
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mObserver:Lcom/android/bluetooth/opp/BluetoothOppTransferActivity$BluetoothTransferContentObserver;

    #@43
    .line 166
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@46
    move-result-object v1

    #@47
    sget-object v2, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@49
    const/4 v3, 0x1

    #@4a
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mObserver:Lcom/android/bluetooth/opp/BluetoothOppTransferActivity$BluetoothTransferContentObserver;

    #@4c
    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@4f
    .line 170
    :cond_4f
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@51
    const/4 v2, 0x3

    #@52
    if-eq v1, v2, :cond_5d

    #@54
    iget v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mWhichDialog:I

    #@56
    if-eqz v1, :cond_5d

    #@58
    .line 172
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mUri:Landroid/net/Uri;

    #@5a
    invoke-static {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToHidden(Landroid/content/Context;Landroid/net/Uri;)V

    #@5d
    .line 175
    :cond_5d
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@60
    move-result-object v1

    #@61
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@63
    .line 178
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->setUpDialog()V

    #@66
    goto :goto_2a
.end method

.method protected onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 184
    const-string v0, "BluetoothOppTransferActivity"

    #@2
    const-string v1, "onDestroy()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 187
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mObserver:Lcom/android/bluetooth/opp/BluetoothOppTransferActivity$BluetoothTransferContentObserver;

    #@9
    if-eqz v0, :cond_14

    #@b
    .line 188
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@e
    move-result-object v0

    #@f
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;->mObserver:Lcom/android/bluetooth/opp/BluetoothOppTransferActivity$BluetoothTransferContentObserver;

    #@11
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@14
    .line 190
    :cond_14
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onDestroy()V

    #@17
    .line 191
    return-void
.end method
