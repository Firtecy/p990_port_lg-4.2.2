.class public Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;
.super Ljava/lang/Object;
.source "MDMBluetoothOppAdapter.java"


# static fields
.field private static TAG:Ljava/lang/String;

.field private static mInstance:Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 12
    const-string v0, "MDMBluetoothOppAdapter"

    #@2
    sput-object v0, Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;->TAG:Ljava/lang/String;

    #@4
    return-void
.end method

.method private constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 17
    return-void
.end method

.method public static getInstance()Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;
    .registers 1

    #@0
    .prologue
    .line 20
    sget-object v0, Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;->mInstance:Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;

    #@2
    if-nez v0, :cond_b

    #@4
    .line 21
    new-instance v0, Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;

    #@6
    invoke-direct {v0}, Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;-><init>()V

    #@9
    sput-object v0, Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;->mInstance:Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;

    #@b
    .line 23
    :cond_b
    sget-object v0, Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;->mInstance:Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;

    #@d
    return-object v0
.end method


# virtual methods
.method public checkBluetoothAllowed(Landroid/content/ComponentName;Landroid/content/Context;)Z
    .registers 6
    .parameter "who"
    .parameter "context"

    #@0
    .prologue
    .line 27
    invoke-static {}, Lcom/lge/mdm/LGMDMSystemServer;->getLGMDMDevicePolicyManager()Lcom/lge/mdm/manager/LGMDMDevicePolicyManager;

    #@3
    move-result-object v0

    #@4
    .line 28
    .local v0, mdm:Lcom/lge/mdm/manager/LGMDMDevicePolicyManager;
    invoke-virtual {v0, p1}, Lcom/lge/mdm/manager/LGMDMDevicePolicyManager;->getAllowBluetooth(Landroid/content/ComponentName;)I

    #@7
    move-result v1

    #@8
    const/4 v2, 0x2

    #@9
    if-eq v1, v2, :cond_1a

    #@b
    .line 29
    sget-object v1, Lcom/android/bluetooth/opp/MDMBluetoothOppAdapter;->TAG:Ljava/lang/String;

    #@d
    const-string v2, "LGMDM DevicePolicyManager DisAllow Bluetooth "

    #@f
    invoke-static {v1, v2}, Lcom/lge/mdm/controller/MDMLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 30
    const v1, 0x209005a

    #@15
    invoke-virtual {v0, v1}, Lcom/lge/mdm/manager/LGMDMDevicePolicyManager;->sendToastMessageId(I)V

    #@18
    .line 31
    const/4 v1, 0x0

    #@19
    .line 33
    :goto_19
    return v1

    #@1a
    :cond_1a
    const/4 v1, 0x1

    #@1b
    goto :goto_19
.end method
