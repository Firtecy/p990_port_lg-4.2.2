.class Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;
.super Ljava/lang/Thread;
.source "BluetoothOppService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/opp/BluetoothOppService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/opp/BluetoothOppService;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/opp/BluetoothOppService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 483
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@2
    .line 484
    const-string v0, "Bluetooth Share Service"

    #@4
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@7
    .line 485
    return-void
.end method


# virtual methods
.method public run()V
    .registers 16

    #@0
    .prologue
    const/4 v14, 0x0

    #@1
    .line 489
    const/16 v0, 0xa

    #@3
    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    #@6
    .line 491
    const/4 v13, 0x0

    #@7
    .line 493
    .local v13, keepService:Z
    :goto_7
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@9
    monitor-enter v1

    #@a
    .line 494
    :try_start_a
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@c
    invoke-static {v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$400(Lcom/android/bluetooth/opp/BluetoothOppService;)Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;

    #@f
    move-result-object v0

    #@10
    if-eq v0, p0, :cond_1d

    #@12
    .line 495
    new-instance v0, Ljava/lang/IllegalStateException;

    #@14
    const-string v2, "multiple UpdateThreads in BluetoothOppService"

    #@16
    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@19
    throw v0

    #@1a
    .line 511
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_a .. :try_end_1c} :catchall_1a

    #@1c
    throw v0

    #@1d
    .line 499
    :cond_1d
    :try_start_1d
    const-string v0, "BtOppService"

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "pendingUpdate is "

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@2c
    invoke-static {v3}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1300(Lcom/android/bluetooth/opp/BluetoothOppService;)Z

    #@2f
    move-result v3

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    const-string v3, " keepUpdateThread is "

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    const-string v3, " sListenStarted is "

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@46
    invoke-static {v3}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$300(Lcom/android/bluetooth/opp/BluetoothOppService;)Z

    #@49
    move-result v3

    #@4a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v2

    #@4e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v2

    #@52
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 502
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@57
    invoke-static {v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1300(Lcom/android/bluetooth/opp/BluetoothOppService;)Z

    #@5a
    move-result v0

    #@5b
    if-nez v0, :cond_76

    #@5d
    .line 503
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@5f
    const/4 v2, 0x0

    #@60
    invoke-static {v0, v2}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$402(Lcom/android/bluetooth/opp/BluetoothOppService;Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;)Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;

    #@63
    .line 504
    if-nez v13, :cond_74

    #@65
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@67
    invoke-static {v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$300(Lcom/android/bluetooth/opp/BluetoothOppService;)Z

    #@6a
    move-result v0

    #@6b
    if-nez v0, :cond_74

    #@6d
    .line 505
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@6f
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->stopSelf()V

    #@72
    .line 506
    monitor-exit v1

    #@73
    .line 646
    :cond_73
    :goto_73
    return-void

    #@74
    .line 508
    :cond_74
    monitor-exit v1

    #@75
    goto :goto_73

    #@76
    .line 510
    :cond_76
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@78
    const/4 v2, 0x0

    #@79
    invoke-static {v0, v2}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1302(Lcom/android/bluetooth/opp/BluetoothOppService;Z)Z

    #@7c
    .line 511
    monitor-exit v1
    :try_end_7d
    .catchall {:try_start_1d .. :try_end_7d} :catchall_1a

    #@7d
    .line 513
    const/4 v8, 0x0

    #@7e
    .line 515
    .local v8, cursor:Landroid/database/Cursor;
    :try_start_7e
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@80
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->getContentResolver()Landroid/content/ContentResolver;

    #@83
    move-result-object v0

    #@84
    sget-object v1, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@86
    const/4 v2, 0x0

    #@87
    const/4 v3, 0x0

    #@88
    const/4 v4, 0x0

    #@89
    const-string v5, "_id"

    #@8b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_8e
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_8e} :catch_f3

    #@8e
    move-result-object v8

    #@8f
    .line 526
    if-eqz v8, :cond_73

    #@91
    .line 530
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@94
    .line 532
    const/4 v7, 0x0

    #@95
    .line 534
    .local v7, arrayPos:I
    const/4 v13, 0x0

    #@96
    .line 535
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    #@99
    move-result v12

    #@9a
    .line 537
    .local v12, isAfterLast:Z
    const-string v0, "_id"

    #@9c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@9f
    move-result v11

    #@a0
    .line 553
    .local v11, idColumn:I
    :goto_a0
    if-eqz v12, :cond_ae

    #@a2
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@a4
    invoke-static {v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1400(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljava/util/ArrayList;

    #@a7
    move-result-object v0

    #@a8
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@ab
    move-result v0

    #@ac
    if-ge v7, v0, :cond_24e

    #@ae
    .line 554
    :cond_ae
    if-eqz v12, :cond_113

    #@b0
    .line 559
    const-string v1, "BtOppService"

    #@b2
    new-instance v0, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v2, "Array update: trimming "

    #@b9
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v2

    #@bd
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@bf
    invoke-static {v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1400(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljava/util/ArrayList;

    #@c2
    move-result-object v0

    #@c3
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@c6
    move-result-object v0

    #@c7
    check-cast v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@c9
    iget v0, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@cb
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v0

    #@cf
    const-string v2, " @ "

    #@d1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v0

    #@d5
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v0

    #@d9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dc
    move-result-object v0

    #@dd
    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e0
    .line 563
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@e2
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1500(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@e5
    move-result v0

    #@e6
    if-eqz v0, :cond_ed

    #@e8
    .line 564
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@ea
    invoke-static {v0, v14, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1600(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;I)Z

    #@ed
    .line 566
    :cond_ed
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@ef
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1700(Lcom/android/bluetooth/opp/BluetoothOppService;I)V

    #@f2
    goto :goto_a0

    #@f3
    .line 517
    .end local v7           #arrayPos:I
    .end local v11           #idColumn:I
    .end local v12           #isAfterLast:Z
    :catch_f3
    move-exception v9

    #@f4
    .line 518
    .local v9, e:Ljava/lang/Exception;
    if-eqz v8, :cond_f9

    #@f6
    .line 519
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@f9
    .line 521
    :cond_f9
    const-string v0, "BtOppService"

    #@fb
    new-instance v1, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v2, "[BTUI] UpdateThread : "

    #@102
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v1

    #@106
    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v1

    #@10a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10d
    move-result-object v1

    #@10e
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@111
    goto/16 :goto_73

    #@113
    .line 568
    .end local v9           #e:Ljava/lang/Exception;
    .restart local v7       #arrayPos:I
    .restart local v11       #idColumn:I
    .restart local v12       #isAfterLast:Z
    :cond_113
    invoke-interface {v8, v11}, Landroid/database/Cursor;->getInt(I)I

    #@116
    move-result v10

    #@117
    .line 570
    .local v10, id:I
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@119
    invoke-static {v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1400(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljava/util/ArrayList;

    #@11c
    move-result-object v0

    #@11d
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    #@120
    move-result v0

    #@121
    if-ne v7, v0, :cond_178

    #@123
    .line 571
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@125
    invoke-static {v0, v8, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1800(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;I)V

    #@128
    .line 573
    const-string v0, "BtOppService"

    #@12a
    new-instance v1, Ljava/lang/StringBuilder;

    #@12c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12f
    const-string v2, "Array update: inserting "

    #@131
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v1

    #@135
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@138
    move-result-object v1

    #@139
    const-string v2, " @ "

    #@13b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v1

    #@13f
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@142
    move-result-object v1

    #@143
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@146
    move-result-object v1

    #@147
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@14a
    .line 575
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@14c
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1500(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@14f
    move-result v0

    #@150
    if-eqz v0, :cond_15b

    #@152
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@154
    invoke-static {v0, v8, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1600(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;I)Z

    #@157
    move-result v0

    #@158
    if-nez v0, :cond_15b

    #@15a
    .line 576
    const/4 v13, 0x1

    #@15b
    .line 578
    :cond_15b
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@15d
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1900(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@160
    move-result v0

    #@161
    if-eqz v0, :cond_164

    #@163
    .line 579
    const/4 v13, 0x1

    #@164
    .line 581
    :cond_164
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@166
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$2000(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@169
    move-result v0

    #@16a
    if-eqz v0, :cond_16d

    #@16c
    .line 582
    const/4 v13, 0x1

    #@16d
    .line 585
    :cond_16d
    add-int/lit8 v7, v7, 0x1

    #@16f
    .line 586
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@172
    .line 587
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    #@175
    move-result v12

    #@176
    goto/16 :goto_a0

    #@178
    .line 589
    :cond_178
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@17a
    invoke-static {v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1400(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljava/util/ArrayList;

    #@17d
    move-result-object v0

    #@17e
    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@181
    move-result-object v0

    #@182
    check-cast v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@184
    iget v6, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@186
    .line 591
    .local v6, arrayId:I
    if-ge v6, v10, :cond_1be

    #@188
    .line 593
    const-string v0, "BtOppService"

    #@18a
    new-instance v1, Ljava/lang/StringBuilder;

    #@18c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@18f
    const-string v2, "Array update: removing "

    #@191
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v1

    #@195
    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@198
    move-result-object v1

    #@199
    const-string v2, " @ "

    #@19b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v1

    #@19f
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v1

    #@1a3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a6
    move-result-object v1

    #@1a7
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1aa
    .line 596
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1ac
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1500(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@1af
    move-result v0

    #@1b0
    if-eqz v0, :cond_1b7

    #@1b2
    .line 597
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1b4
    invoke-static {v0, v14, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1600(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;I)Z

    #@1b7
    .line 599
    :cond_1b7
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1b9
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1700(Lcom/android/bluetooth/opp/BluetoothOppService;I)V

    #@1bc
    goto/16 :goto_a0

    #@1be
    .line 600
    :cond_1be
    if-ne v6, v10, :cond_1f9

    #@1c0
    .line 603
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1c2
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1c4
    invoke-static {v1}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$2100(Lcom/android/bluetooth/opp/BluetoothOppService;)Z

    #@1c7
    move-result v1

    #@1c8
    invoke-static {v0, v8, v7, v1}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$2200(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;IZ)V

    #@1cb
    .line 604
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1cd
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1500(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@1d0
    move-result v0

    #@1d1
    if-eqz v0, :cond_1dc

    #@1d3
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1d5
    invoke-static {v0, v8, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1600(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;I)Z

    #@1d8
    move-result v0

    #@1d9
    if-nez v0, :cond_1dc

    #@1db
    .line 605
    const/4 v13, 0x1

    #@1dc
    .line 607
    :cond_1dc
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1de
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1900(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@1e1
    move-result v0

    #@1e2
    if-eqz v0, :cond_1e5

    #@1e4
    .line 608
    const/4 v13, 0x1

    #@1e5
    .line 610
    :cond_1e5
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@1e7
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$2000(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@1ea
    move-result v0

    #@1eb
    if-eqz v0, :cond_1ee

    #@1ed
    .line 611
    const/4 v13, 0x1

    #@1ee
    .line 614
    :cond_1ee
    add-int/lit8 v7, v7, 0x1

    #@1f0
    .line 615
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@1f3
    .line 616
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    #@1f6
    move-result v12

    #@1f7
    goto/16 :goto_a0

    #@1f9
    .line 621
    :cond_1f9
    const-string v0, "BtOppService"

    #@1fb
    new-instance v1, Ljava/lang/StringBuilder;

    #@1fd
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@200
    const-string v2, "Array update: appending "

    #@202
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@205
    move-result-object v1

    #@206
    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@209
    move-result-object v1

    #@20a
    const-string v2, " @ "

    #@20c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20f
    move-result-object v1

    #@210
    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@213
    move-result-object v1

    #@214
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@217
    move-result-object v1

    #@218
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@21b
    .line 623
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@21d
    invoke-static {v0, v8, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1800(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;I)V

    #@220
    .line 625
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@222
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1500(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@225
    move-result v0

    #@226
    if-eqz v0, :cond_231

    #@228
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@22a
    invoke-static {v0, v8, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1600(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;I)Z

    #@22d
    move-result v0

    #@22e
    if-nez v0, :cond_231

    #@230
    .line 626
    const/4 v13, 0x1

    #@231
    .line 628
    :cond_231
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@233
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$1900(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@236
    move-result v0

    #@237
    if-eqz v0, :cond_23a

    #@239
    .line 629
    const/4 v13, 0x1

    #@23a
    .line 631
    :cond_23a
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@23c
    invoke-static {v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$2000(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z

    #@23f
    move-result v0

    #@240
    if-eqz v0, :cond_243

    #@242
    .line 632
    const/4 v13, 0x1

    #@243
    .line 634
    :cond_243
    add-int/lit8 v7, v7, 0x1

    #@245
    .line 635
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@248
    .line 636
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    #@24b
    move-result v12

    #@24c
    goto/16 :goto_a0

    #@24e
    .line 642
    .end local v6           #arrayId:I
    .end local v10           #id:I
    :cond_24e
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->this$0:Lcom/android/bluetooth/opp/BluetoothOppService;

    #@250
    invoke-static {v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->access$2300(Lcom/android/bluetooth/opp/BluetoothOppService;)Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@253
    move-result-object v0

    #@254
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppNotification;->updateNotification()V

    #@257
    .line 644
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@25a
    goto/16 :goto_7
.end method
