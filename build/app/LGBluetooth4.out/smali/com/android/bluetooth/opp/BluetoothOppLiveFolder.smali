.class public Lcom/android/bluetooth/opp/BluetoothOppLiveFolder;
.super Landroid/app/Activity;
.source "BluetoothOppLiveFolder.java"


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 47
    const-string v0, "content://com.android.bluetooth.opp/live_folders/received"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/android/bluetooth/opp/BluetoothOppLiveFolder;->CONTENT_URI:Landroid/net/Uri;

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    #@3
    return-void
.end method

.method private static createLiveFolder(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;
    .registers 9
    .parameter "context"
    .parameter "uri"
    .parameter "name"
    .parameter "icon"

    #@0
    .prologue
    .line 69
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@5
    .line 71
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@8
    .line 72
    const-string v1, "android.intent.extra.livefolder.BASE_INTENT"

    #@a
    new-instance v2, Landroid/content/Intent;

    #@c
    const-string v3, "android.btopp.intent.action.OPEN"

    #@e
    sget-object v4, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@10
    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@13
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@16
    .line 74
    const-string v1, "android.intent.extra.livefolder.NAME"

    #@18
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1b
    .line 75
    const-string v1, "android.intent.extra.livefolder.ICON"

    #@1d
    invoke-static {p0, p3}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@24
    .line 77
    const-string v1, "android.intent.extra.livefolder.DISPLAY_MODE"

    #@26
    const/4 v2, 0x2

    #@27
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2a
    .line 79
    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .registers 8
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 54
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLiveFolder;->getIntent()Landroid/content/Intent;

    #@6
    move-result-object v1

    #@7
    .line 55
    .local v1, intent:Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    .line 57
    .local v0, action:Ljava/lang/String;
    const-string v2, "android.intent.action.CREATE_LIVE_FOLDER"

    #@d
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10
    move-result v2

    #@11
    if-eqz v2, :cond_2b

    #@13
    .line 58
    const/4 v2, -0x1

    #@14
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothOppLiveFolder;->CONTENT_URI:Landroid/net/Uri;

    #@16
    const v4, 0x7f070053

    #@19
    invoke-virtual {p0, v4}, Lcom/android/bluetooth/opp/BluetoothOppLiveFolder;->getString(I)Ljava/lang/String;

    #@1c
    move-result-object v4

    #@1d
    const v5, 0x7f020005

    #@20
    invoke-static {p0, v3, v4, v5}, Lcom/android/bluetooth/opp/BluetoothOppLiveFolder;->createLiveFolder(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/opp/BluetoothOppLiveFolder;->setResult(ILandroid/content/Intent;)V

    #@27
    .line 65
    :goto_27
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppLiveFolder;->finish()V

    #@2a
    .line 66
    return-void

    #@2b
    .line 62
    :cond_2b
    const/4 v2, 0x0

    #@2c
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/BluetoothOppLiveFolder;->setResult(I)V

    #@2f
    goto :goto_27
.end method
