.class public final Lcom/android/bluetooth/opp/BluetoothOppProvider;
.super Landroid/content/ContentProvider;
.source "BluetoothOppProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/opp/BluetoothOppProvider$DatabaseHelper;
    }
.end annotation


# static fields
.field private static final D:Z = true

.field private static final DB_NAME:Ljava/lang/String; = "btopp.db"

.field private static final DB_TABLE:Ljava/lang/String; = "btopp"

.field private static final DB_VERSION:I = 0x1

.field private static final DB_VERSION_NOP_UPGRADE_FROM:I = 0x0

.field private static final DB_VERSION_NOP_UPGRADE_TO:I = 0x1

.field private static final LIVE_FOLDER_PROJECTION_MAP:Ljava/util/HashMap; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final LIVE_FOLDER_RECEIVED_FILES:I = 0x3

.field private static final SHARES:I = 0x1

.field private static final SHARES_ID:I = 0x2

.field private static final SHARE_LIST_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/vnd.android.btopp"

.field private static final SHARE_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/vnd.android.btopp"

.field private static final TAG:Ljava/lang/String; = "BluetoothOppProvider"

.field private static final V:Z = true

.field private static final sURIMatcher:Landroid/content/UriMatcher;


# instance fields
.field private mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    .line 88
    new-instance v0, Landroid/content/UriMatcher;

    #@2
    const/4 v1, -0x1

    #@3
    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    #@6
    sput-object v0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->sURIMatcher:Landroid/content/UriMatcher;

    #@8
    .line 99
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->sURIMatcher:Landroid/content/UriMatcher;

    #@a
    const-string v1, "com.android.bluetooth.opp"

    #@c
    const-string v2, "btopp"

    #@e
    const/4 v3, 0x1

    #@f
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@12
    .line 100
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->sURIMatcher:Landroid/content/UriMatcher;

    #@14
    const-string v1, "com.android.bluetooth.opp"

    #@16
    const-string v2, "btopp/#"

    #@18
    const/4 v3, 0x2

    #@19
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@1c
    .line 101
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->sURIMatcher:Landroid/content/UriMatcher;

    #@1e
    const-string v1, "com.android.bluetooth.opp"

    #@20
    const-string v2, "live_folders/received"

    #@22
    const/4 v3, 0x3

    #@23
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    #@26
    .line 107
    new-instance v0, Ljava/util/HashMap;

    #@28
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@2b
    sput-object v0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->LIVE_FOLDER_PROJECTION_MAP:Ljava/util/HashMap;

    #@2d
    .line 108
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->LIVE_FOLDER_PROJECTION_MAP:Ljava/util/HashMap;

    #@2f
    const-string v1, "_id"

    #@31
    const-string v2, "_id AS _id"

    #@33
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    .line 110
    sget-object v0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->LIVE_FOLDER_PROJECTION_MAP:Ljava/util/HashMap;

    #@38
    const-string v1, "name"

    #@3a
    const-string v2, "hint AS name"

    #@3c
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 112
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    #@3
    .line 115
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    #@6
    .line 123
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/opp/BluetoothOppProvider;Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->createTable(Landroid/database/sqlite/SQLiteDatabase;)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/bluetooth/opp/BluetoothOppProvider;Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->dropTable(Landroid/database/sqlite/SQLiteDatabase;)V

    #@3
    return-void
.end method

.method private static final copyInteger(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    .registers 4
    .parameter "key"
    .parameter "from"
    .parameter "to"

    #@0
    .prologue
    .line 237
    invoke-virtual {p1, p0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    #@3
    move-result-object v0

    #@4
    .line 238
    .local v0, i:Ljava/lang/Integer;
    if-eqz v0, :cond_9

    #@6
    .line 239
    invoke-virtual {p2, p0, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@9
    .line 241
    :cond_9
    return-void
.end method

.method private static final copyLong(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    .registers 4
    .parameter "key"
    .parameter "from"
    .parameter "to"

    #@0
    .prologue
    .line 245
    invoke-virtual {p1, p0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    #@3
    move-result-object v0

    #@4
    .line 246
    .local v0, i:Ljava/lang/Long;
    if-eqz v0, :cond_9

    #@6
    .line 247
    invoke-virtual {p2, p0, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@9
    .line 249
    :cond_9
    return-void
.end method

.method private static final copyString(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V
    .registers 4
    .parameter "key"
    .parameter "from"
    .parameter "to"

    #@0
    .prologue
    .line 230
    invoke-virtual {p1, p0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 231
    .local v0, s:Ljava/lang/String;
    if-eqz v0, :cond_9

    #@6
    .line 232
    invoke-virtual {p2, p0, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 234
    :cond_9
    return-void
.end method

.method private createTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 5
    .parameter "db"

    #@0
    .prologue
    .line 180
    :try_start_0
    const-string v1, "CREATE TABLE btopp(_id INTEGER PRIMARY KEY AUTOINCREMENT,uri TEXT, hint TEXT, _data TEXT, mimetype TEXT, direction INTEGER, destination TEXT, visibility INTEGER, confirm INTEGER, status INTEGER, total_bytes LONG, current_bytes LONG, timestamp INTEGER,scanned INTEGER); "

    #@2
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 199
    return-void

    #@6
    .line 195
    :catch_6
    move-exception v0

    #@7
    .line 196
    .local v0, ex:Landroid/database/SQLException;
    const-string v1, "BluetoothOppProvider"

    #@9
    const-string v2, "couldn\'t create table in downloads database"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 197
    throw v0
.end method

.method private dropTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .registers 5
    .parameter "db"

    #@0
    .prologue
    .line 203
    :try_start_0
    const-string v1, "DROP TABLE IF EXISTS btopp"

    #@2
    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 208
    return-void

    #@6
    .line 204
    :catch_6
    move-exception v0

    #@7
    .line 205
    .local v0, ex:Landroid/database/SQLException;
    const-string v1, "BluetoothOppProvider"

    #@9
    const-string v2, "couldn\'t drop table in downloads database"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 206
    throw v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 15
    .parameter "uri"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 505
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    #@3
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@6
    move-result-object v1

    #@7
    .line 507
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    if-nez v1, :cond_12

    #@9
    .line 508
    const-string v8, "BluetoothOppProvider"

    #@b
    const-string v9, "Can\'t open databse!"

    #@d
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 509
    const/4 v0, -0x1

    #@11
    .line 553
    :goto_11
    return v0

    #@12
    .line 513
    :cond_12
    sget-object v8, Lcom/android/bluetooth/opp/BluetoothOppProvider;->sURIMatcher:Landroid/content/UriMatcher;

    #@14
    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@17
    move-result v3

    #@18
    .line 514
    .local v3, match:I
    packed-switch v3, :pswitch_data_cc

    #@1b
    .line 547
    const-string v8, "BluetoothOppProvider"

    #@1d
    new-instance v9, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v10, "deleting unknown/invalid URI: "

    #@24
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v9

    #@28
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v9

    #@2c
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v9

    #@30
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 549
    new-instance v8, Ljava/lang/UnsupportedOperationException;

    #@35
    new-instance v9, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v10, "Cannot delete URI: "

    #@3c
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v9

    #@40
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v9

    #@44
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v9

    #@48
    invoke-direct {v8, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@4b
    throw v8

    #@4c
    .line 518
    :pswitch_4c
    if-eqz p2, :cond_c5

    #@4e
    .line 519
    if-ne v3, v10, :cond_ab

    #@50
    .line 520
    new-instance v8, Ljava/lang/StringBuilder;

    #@52
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@55
    const-string v9, "( "

    #@57
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v8

    #@5b
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v8

    #@5f
    const-string v9, " )"

    #@61
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v8

    #@65
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    .line 527
    .local v4, myWhere:Ljava/lang/String;
    :goto_69
    const/4 v8, 0x2

    #@6a
    if-ne v3, v8, :cond_97

    #@6c
    .line 528
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@6f
    move-result-object v8

    #@70
    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@73
    move-result-object v7

    #@74
    check-cast v7, Ljava/lang/String;

    #@76
    .line 529
    .local v7, segment:Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@79
    move-result-wide v5

    #@7a
    .line 530
    .local v5, rowId:J
    new-instance v8, Ljava/lang/StringBuilder;

    #@7c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@7f
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v8

    #@83
    const-string v9, " ( _id = "

    #@85
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@88
    move-result-object v8

    #@89
    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v8

    #@8d
    const-string v9, " ) "

    #@8f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92
    move-result-object v8

    #@93
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@96
    move-result-object v4

    #@97
    .line 538
    .end local v5           #rowId:J
    .end local v7           #segment:Ljava/lang/String;
    :cond_97
    :try_start_97
    const-string v8, "btopp"

    #@99
    invoke-virtual {v1, v8, v4, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_9c
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_97 .. :try_end_9c} :catch_c8

    #@9c
    move-result v0

    #@9d
    .line 552
    .local v0, count:I
    :goto_9d
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->getContext()Landroid/content/Context;

    #@a0
    move-result-object v8

    #@a1
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a4
    move-result-object v8

    #@a5
    const/4 v9, 0x0

    #@a6
    invoke-virtual {v8, p1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@a9
    goto/16 :goto_11

    #@ab
    .line 522
    .end local v0           #count:I
    .end local v4           #myWhere:Ljava/lang/String;
    :cond_ab
    new-instance v8, Ljava/lang/StringBuilder;

    #@ad
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b0
    const-string v9, "( "

    #@b2
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v8

    #@b6
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v8

    #@ba
    const-string v9, " ) AND "

    #@bc
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v8

    #@c0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c3
    move-result-object v4

    #@c4
    .restart local v4       #myWhere:Ljava/lang/String;
    goto :goto_69

    #@c5
    .line 525
    .end local v4           #myWhere:Ljava/lang/String;
    :cond_c5
    const-string v4, ""

    #@c7
    .restart local v4       #myWhere:Ljava/lang/String;
    goto :goto_69

    #@c8
    .line 539
    :catch_c8
    move-exception v2

    #@c9
    .line 540
    .local v2, e:Landroid/database/sqlite/SQLiteException;
    const/4 v0, 0x0

    #@ca
    .line 543
    .restart local v0       #count:I
    goto :goto_9d

    #@cb
    .line 514
    nop

    #@cc
    :pswitch_data_cc
    .packed-switch 0x1
        :pswitch_4c
        :pswitch_4c
    .end packed-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .registers 6
    .parameter "uri"

    #@0
    .prologue
    .line 212
    sget-object v1, Lcom/android/bluetooth/opp/BluetoothOppProvider;->sURIMatcher:Landroid/content/UriMatcher;

    #@2
    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@5
    move-result v0

    #@6
    .line 213
    .local v0, match:I
    packed-switch v0, :pswitch_data_40

    #@9
    .line 222
    const-string v1, "BluetoothOppProvider"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "calling getType on an unknown URI: "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 224
    new-instance v1, Ljava/lang/IllegalArgumentException;

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "Unknown URI: "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@39
    throw v1

    #@3a
    .line 215
    :pswitch_3a
    const-string v1, "vnd.android.cursor.dir/vnd.android.btopp"

    #@3c
    .line 218
    :goto_3c
    return-object v1

    #@3d
    :pswitch_3d
    const-string v1, "vnd.android.cursor.item/vnd.android.btopp"

    #@3f
    goto :goto_3c

    #@40
    .line 213
    :pswitch_data_40
    .packed-switch 0x1
        :pswitch_3a
        :pswitch_3d
    .end packed-switch
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .registers 18
    .parameter "uri"
    .parameter "values"

    #@0
    .prologue
    .line 254
    iget-object v12, p0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    #@2
    invoke-virtual {v12}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v4

    #@6
    .line 256
    .local v4, db:Landroid/database/sqlite/SQLiteDatabase;
    sget-object v12, Lcom/android/bluetooth/opp/BluetoothOppProvider;->sURIMatcher:Landroid/content/UriMatcher;

    #@8
    move-object/from16 v0, p1

    #@a
    invoke-virtual {v12, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@d
    move-result v12

    #@e
    const/4 v13, 0x1

    #@f
    if-eq v12, v13, :cond_46

    #@11
    .line 258
    const-string v12, "BluetoothOppProvider"

    #@13
    new-instance v13, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v14, "calling insert on an unknown/invalid URI: "

    #@1a
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v13

    #@1e
    move-object/from16 v0, p1

    #@20
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v13

    #@24
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@27
    move-result-object v13

    #@28
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 260
    new-instance v12, Ljava/lang/IllegalArgumentException;

    #@2d
    new-instance v13, Ljava/lang/StringBuilder;

    #@2f
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@32
    const-string v14, "Unknown/Invalid URI "

    #@34
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v13

    #@38
    move-object/from16 v0, p1

    #@3a
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v13

    #@3e
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v13

    #@42
    invoke-direct {v12, v13}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@45
    throw v12

    #@46
    .line 263
    :cond_46
    new-instance v7, Landroid/content/ContentValues;

    #@48
    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    #@4b
    .line 265
    .local v7, filteredValues:Landroid/content/ContentValues;
    const-string v12, "uri"

    #@4d
    move-object/from16 v0, p2

    #@4f
    invoke-static {v12, v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->copyString(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    #@52
    .line 266
    const-string v12, "hint"

    #@54
    move-object/from16 v0, p2

    #@56
    invoke-static {v12, v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->copyString(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    #@59
    .line 267
    const-string v12, "mimetype"

    #@5b
    move-object/from16 v0, p2

    #@5d
    invoke-static {v12, v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->copyString(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    #@60
    .line 268
    const-string v12, "destination"

    #@62
    move-object/from16 v0, p2

    #@64
    invoke-static {v12, v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->copyString(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    #@67
    .line 270
    const-string v12, "visibility"

    #@69
    move-object/from16 v0, p2

    #@6b
    invoke-static {v12, v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->copyInteger(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    #@6e
    .line 273
    const-string v12, "total_bytes"

    #@70
    move-object/from16 v0, p2

    #@72
    invoke-static {v12, v0, v7}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->copyLong(Ljava/lang/String;Landroid/content/ContentValues;Landroid/content/ContentValues;)V

    #@75
    .line 276
    const-string v12, "visibility"

    #@77
    move-object/from16 v0, p2

    #@79
    invoke-virtual {v0, v12}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    #@7c
    move-result-object v12

    #@7d
    if-nez v12, :cond_89

    #@7f
    .line 277
    const-string v12, "visibility"

    #@81
    const/4 v13, 0x0

    #@82
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@85
    move-result-object v13

    #@86
    invoke-virtual {v7, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@89
    .line 279
    :cond_89
    const-string v12, "direction"

    #@8b
    move-object/from16 v0, p2

    #@8d
    invoke-virtual {v0, v12}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    #@90
    move-result-object v5

    #@91
    .line 280
    .local v5, dir:Ljava/lang/Integer;
    const-string v12, "confirm"

    #@93
    move-object/from16 v0, p2

    #@95
    invoke-virtual {v0, v12}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    #@98
    move-result-object v2

    #@99
    .line 281
    .local v2, con:Ljava/lang/Integer;
    const-string v12, "destination"

    #@9b
    move-object/from16 v0, p2

    #@9d
    invoke-virtual {v0, v12}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    #@a0
    move-result-object v1

    #@a1
    .line 283
    .local v1, address:Ljava/lang/String;
    const-string v12, "direction"

    #@a3
    move-object/from16 v0, p2

    #@a5
    invoke-virtual {v0, v12}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    #@a8
    move-result-object v12

    #@a9
    if-nez v12, :cond_b0

    #@ab
    .line 284
    const/4 v12, 0x0

    #@ac
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@af
    move-result-object v5

    #@b0
    .line 286
    :cond_b0
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@b3
    move-result v12

    #@b4
    if-nez v12, :cond_bd

    #@b6
    if-nez v2, :cond_bd

    #@b8
    .line 287
    const/4 v12, 0x2

    #@b9
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@bc
    move-result-object v2

    #@bd
    .line 289
    :cond_bd
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@c0
    move-result v12

    #@c1
    const/4 v13, 0x1

    #@c2
    if-ne v12, v13, :cond_cb

    #@c4
    if-nez v2, :cond_cb

    #@c6
    .line 290
    const/4 v12, 0x0

    #@c7
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@ca
    move-result-object v2

    #@cb
    .line 292
    :cond_cb
    const-string v12, "confirm"

    #@cd
    invoke-virtual {v7, v12, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@d0
    .line 293
    const-string v12, "direction"

    #@d2
    invoke-virtual {v7, v12, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@d5
    .line 295
    const-string v12, "status"

    #@d7
    const/16 v13, 0xbe

    #@d9
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@dc
    move-result-object v13

    #@dd
    invoke-virtual {v7, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@e0
    .line 296
    const-string v12, "scanned"

    #@e2
    const/4 v13, 0x0

    #@e3
    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e6
    move-result-object v13

    #@e7
    invoke-virtual {v7, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@ea
    .line 298
    const-string v12, "timestamp"

    #@ec
    move-object/from16 v0, p2

    #@ee
    invoke-virtual {v0, v12}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    #@f1
    move-result-object v11

    #@f2
    .line 299
    .local v11, ts:Ljava/lang/Long;
    if-nez v11, :cond_fc

    #@f4
    .line 300
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@f7
    move-result-wide v12

    #@f8
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@fb
    move-result-object v11

    #@fc
    .line 302
    :cond_fc
    const-string v12, "timestamp"

    #@fe
    invoke-virtual {v7, v12, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@101
    .line 304
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->getContext()Landroid/content/Context;

    #@104
    move-result-object v3

    #@105
    .line 305
    .local v3, context:Landroid/content/Context;
    new-instance v12, Landroid/content/Intent;

    #@107
    const-class v13, Lcom/android/bluetooth/opp/BluetoothOppService;

    #@109
    invoke-direct {v12, v3, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@10c
    invoke-virtual {v3, v12}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@10f
    .line 311
    const-wide/16 v9, 0x0

    #@111
    .line 313
    .local v9, rowID:J
    :try_start_111
    const-string v12, "btopp"

    #@113
    const/4 v13, 0x0

    #@114
    invoke-virtual {v4, v12, v13, v7}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_117
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_111 .. :try_end_117} :catch_151

    #@117
    move-result-wide v9

    #@118
    .line 319
    :goto_118
    const/4 v8, 0x0

    #@119
    .line 321
    .local v8, ret:Landroid/net/Uri;
    const-wide/16 v12, -0x1

    #@11b
    cmp-long v12, v9, v12

    #@11d
    if-eqz v12, :cond_155

    #@11f
    .line 322
    new-instance v12, Landroid/content/Intent;

    #@121
    const-class v13, Lcom/android/bluetooth/opp/BluetoothOppService;

    #@123
    invoke-direct {v12, v3, v13}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@126
    invoke-virtual {v3, v12}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@129
    .line 323
    new-instance v12, Ljava/lang/StringBuilder;

    #@12b
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@12e
    sget-object v13, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@130
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v12

    #@134
    const-string v13, "/"

    #@136
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v12

    #@13a
    invoke-virtual {v12, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v12

    #@13e
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@141
    move-result-object v12

    #@142
    invoke-static {v12}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@145
    move-result-object v8

    #@146
    .line 324
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@149
    move-result-object v12

    #@14a
    const/4 v13, 0x0

    #@14b
    move-object/from16 v0, p1

    #@14d
    invoke-virtual {v12, v0, v13}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@150
    .line 331
    :goto_150
    return-object v8

    #@151
    .line 314
    .end local v8           #ret:Landroid/net/Uri;
    :catch_151
    move-exception v6

    #@152
    .line 315
    .local v6, e:Landroid/database/sqlite/SQLiteException;
    const-wide/16 v9, -0x1

    #@154
    goto :goto_118

    #@155
    .line 327
    .end local v6           #e:Landroid/database/sqlite/SQLiteException;
    .restart local v8       #ret:Landroid/net/Uri;
    :cond_155
    const-string v12, "BluetoothOppProvider"

    #@157
    const-string v13, "couldn\'t insert into btopp database"

    #@159
    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15c
    goto :goto_150
.end method

.method public onCreate()Z
    .registers 3

    #@0
    .prologue
    .line 336
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppProvider$DatabaseHelper;

    #@2
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->getContext()Landroid/content/Context;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppProvider$DatabaseHelper;-><init>(Lcom/android/bluetooth/opp/BluetoothOppProvider;Landroid/content/Context;)V

    #@9
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    #@b
    .line 337
    const/4 v0, 0x1

    #@c
    return v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .registers 20
    .parameter "uri"
    .parameter "projection"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    .line 343
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    #@2
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@5
    move-result-object v2

    #@6
    .line 345
    .local v2, db:Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    #@8
    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    #@b
    .line 347
    .local v1, qb:Landroid/database/sqlite/SQLiteQueryBuilder;
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothOppProvider;->sURIMatcher:Landroid/content/UriMatcher;

    #@d
    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@10
    move-result v11

    #@11
    .line 348
    .local v11, match:I
    packed-switch v11, :pswitch_data_182

    #@14
    .line 369
    const-string v3, "BluetoothOppProvider"

    #@16
    new-instance v4, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v5, "querying unknown URI: "

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v4

    #@25
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v4

    #@29
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 371
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@2e
    new-instance v4, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v5, "Unknown URI: "

    #@35
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v4

    #@39
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v4

    #@41
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@44
    throw v3

    #@45
    .line 350
    :pswitch_45
    const-string v3, "btopp"

    #@47
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@4a
    .line 376
    :goto_4a
    new-instance v13, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    .line 377
    .local v13, sb:Ljava/lang/StringBuilder;
    const-string v3, "starting query, database is "

    #@51
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    .line 378
    if-eqz v2, :cond_5b

    #@56
    .line 379
    const-string v3, "not "

    #@58
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    .line 381
    :cond_5b
    const-string v3, "null; "

    #@5d
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    .line 382
    if-nez p2, :cond_112

    #@62
    .line 383
    const-string v3, "projection is null; "

    #@64
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    .line 395
    :cond_67
    :goto_67
    const-string v3, "selection is "

    #@69
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    .line 396
    move-object/from16 v0, p3

    #@6e
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    .line 397
    const-string v3, "; "

    #@73
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    .line 398
    if-nez p4, :cond_13e

    #@78
    .line 399
    const-string v3, "selectionArgs is null; "

    #@7a
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    .line 411
    :cond_7d
    :goto_7d
    const-string v3, "sort is "

    #@7f
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    .line 412
    move-object/from16 v0, p5

    #@84
    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    .line 413
    const-string v3, "."

    #@89
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    .line 414
    const-string v3, "BluetoothOppProvider"

    #@8e
    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v4

    #@92
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 421
    const/4 v12, 0x0

    #@96
    .line 423
    .local v12, ret:Landroid/database/Cursor;
    if-eqz v2, :cond_16a

    #@98
    .line 424
    const/4 v6, 0x0

    #@99
    const/4 v7, 0x0

    #@9a
    move-object/from16 v3, p2

    #@9c
    move-object/from16 v4, p3

    #@9e
    move-object/from16 v5, p4

    #@a0
    move-object/from16 v8, p5

    #@a2
    :try_start_a2
    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_a5
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a2 .. :try_end_a5} :catch_174

    #@a5
    move-result-object v12

    #@a6
    .line 434
    :goto_a6
    if-eqz v12, :cond_178

    #@a8
    .line 435
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->getContext()Landroid/content/Context;

    #@ab
    move-result-object v3

    #@ac
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@af
    move-result-object v3

    #@b0
    invoke-interface {v12, v3, p1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    #@b3
    .line 437
    const-string v3, "BluetoothOppProvider"

    #@b5
    new-instance v4, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v5, "created cursor "

    #@bc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v4

    #@c0
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c3
    move-result-object v4

    #@c4
    const-string v5, " on behalf of "

    #@c6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c9
    move-result-object v4

    #@ca
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cd
    move-result-object v4

    #@ce
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@d1
    .line 445
    :goto_d1
    return-object v12

    #@d2
    .line 354
    .end local v12           #ret:Landroid/database/Cursor;
    .end local v13           #sb:Ljava/lang/StringBuilder;
    :pswitch_d2
    const-string v3, "btopp"

    #@d4
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@d7
    .line 355
    const-string v3, "_id="

    #@d9
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@dc
    .line 356
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@df
    move-result-object v3

    #@e0
    const/4 v4, 0x1

    #@e1
    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@e4
    move-result-object v3

    #@e5
    check-cast v3, Ljava/lang/CharSequence;

    #@e7
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@ea
    goto/16 :goto_4a

    #@ec
    .line 360
    :pswitch_ec
    const-string v3, "btopp"

    #@ee
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    #@f1
    .line 361
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothOppProvider;->LIVE_FOLDER_PROJECTION_MAP:Ljava/util/HashMap;

    #@f3
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    #@f6
    .line 362
    const-string v3, "direction=1 AND status=200"

    #@f8
    invoke-virtual {v1, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    #@fb
    .line 364
    new-instance v3, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v4, "_id DESC, "

    #@102
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v3

    #@106
    move-object/from16 v0, p5

    #@108
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v3

    #@10c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10f
    move-result-object p5

    #@110
    .line 365
    goto/16 :goto_4a

    #@112
    .line 384
    .restart local v13       #sb:Ljava/lang/StringBuilder;
    :cond_112
    move-object/from16 v0, p2

    #@114
    array-length v3, v0

    #@115
    if-nez v3, :cond_11e

    #@117
    .line 385
    const-string v3, "projection is empty; "

    #@119
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    goto/16 :goto_67

    #@11e
    .line 387
    :cond_11e
    const/4 v10, 0x0

    #@11f
    .local v10, i:I
    :goto_11f
    move-object/from16 v0, p2

    #@121
    array-length v3, v0

    #@122
    if-ge v10, v3, :cond_67

    #@124
    .line 388
    const-string v3, "projection["

    #@126
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    .line 389
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12c
    .line 390
    const-string v3, "] is "

    #@12e
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    .line 391
    aget-object v3, p2, v10

    #@133
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    .line 392
    const-string v3, "; "

    #@138
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    .line 387
    add-int/lit8 v10, v10, 0x1

    #@13d
    goto :goto_11f

    #@13e
    .line 400
    .end local v10           #i:I
    :cond_13e
    move-object/from16 v0, p4

    #@140
    array-length v3, v0

    #@141
    if-nez v3, :cond_14a

    #@143
    .line 401
    const-string v3, "selectionArgs is empty; "

    #@145
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@148
    goto/16 :goto_7d

    #@14a
    .line 403
    :cond_14a
    const/4 v10, 0x0

    #@14b
    .restart local v10       #i:I
    :goto_14b
    move-object/from16 v0, p4

    #@14d
    array-length v3, v0

    #@14e
    if-ge v10, v3, :cond_7d

    #@150
    .line 404
    const-string v3, "selectionArgs["

    #@152
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    .line 405
    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@158
    .line 406
    const-string v3, "] is "

    #@15a
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15d
    .line 407
    aget-object v3, p4, v10

    #@15f
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@162
    .line 408
    const-string v3, "; "

    #@164
    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@167
    .line 403
    add-int/lit8 v10, v10, 0x1

    #@169
    goto :goto_14b

    #@16a
    .line 426
    .end local v10           #i:I
    .restart local v12       #ret:Landroid/database/Cursor;
    :cond_16a
    :try_start_16a
    const-string v3, "BluetoothOppProvider"

    #@16c
    const-string v4, "[BTUI] db is null"

    #@16e
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_171
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_16a .. :try_end_171} :catch_174

    #@171
    .line 427
    const/4 v12, 0x0

    #@172
    goto/16 :goto_a6

    #@174
    .line 429
    :catch_174
    move-exception v9

    #@175
    .line 430
    .local v9, e:Landroid/database/sqlite/SQLiteException;
    const/4 v12, 0x0

    #@176
    goto/16 :goto_a6

    #@178
    .line 441
    .end local v9           #e:Landroid/database/sqlite/SQLiteException;
    :cond_178
    const-string v3, "BluetoothOppProvider"

    #@17a
    const-string v4, "query failed in downloads database"

    #@17c
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17f
    goto/16 :goto_d1

    #@181
    .line 348
    nop

    #@182
    :pswitch_data_182
    .packed-switch 0x1
        :pswitch_45
        :pswitch_d2
        :pswitch_ec
    .end packed-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .registers 16
    .parameter "uri"
    .parameter "values"
    .parameter "selection"
    .parameter "selectionArgs"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    .line 450
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppProvider;->mOpenHelper:Landroid/database/sqlite/SQLiteOpenHelper;

    #@3
    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    #@6
    move-result-object v1

    #@7
    .line 453
    .local v1, db:Landroid/database/sqlite/SQLiteDatabase;
    const-wide/16 v5, 0x0

    #@9
    .line 455
    .local v5, rowId:J
    sget-object v8, Lcom/android/bluetooth/opp/BluetoothOppProvider;->sURIMatcher:Landroid/content/UriMatcher;

    #@b
    invoke-virtual {v8, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    #@e
    move-result v3

    #@f
    .line 456
    .local v3, match:I
    packed-switch v3, :pswitch_data_ca

    #@12
    .line 493
    const-string v8, "BluetoothOppProvider"

    #@14
    new-instance v9, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v10, "updating unknown/invalid URI: "

    #@1b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v9

    #@1f
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v9

    #@23
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v9

    #@27
    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 495
    new-instance v8, Ljava/lang/UnsupportedOperationException;

    #@2c
    new-instance v9, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v10, "Cannot update URI: "

    #@33
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v9

    #@37
    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v9

    #@3b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v9

    #@3f
    invoke-direct {v8, v9}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@42
    throw v8

    #@43
    .line 460
    :pswitch_43
    if-eqz p3, :cond_c1

    #@45
    .line 461
    if-ne v3, v10, :cond_a7

    #@47
    .line 462
    new-instance v8, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v9, "( "

    #@4e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v8

    #@52
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v8

    #@56
    const-string v9, " )"

    #@58
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v8

    #@5c
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v4

    #@60
    .line 469
    .local v4, myWhere:Ljava/lang/String;
    :goto_60
    const/4 v8, 0x2

    #@61
    if-ne v3, v8, :cond_8e

    #@63
    .line 470
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    #@66
    move-result-object v8

    #@67
    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@6a
    move-result-object v7

    #@6b
    check-cast v7, Ljava/lang/String;

    #@6d
    .line 471
    .local v7, segment:Ljava/lang/String;
    invoke-static {v7}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    #@70
    move-result-wide v5

    #@71
    .line 472
    new-instance v8, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v8

    #@7a
    const-string v9, " ( _id = "

    #@7c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v8

    #@80
    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@83
    move-result-object v8

    #@84
    const-string v9, " ) "

    #@86
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v8

    #@8a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v4

    #@8e
    .line 475
    .end local v7           #segment:Ljava/lang/String;
    :cond_8e
    invoke-virtual {p2}, Landroid/content/ContentValues;->size()I

    #@91
    move-result v8

    #@92
    if-lez v8, :cond_c7

    #@94
    .line 481
    :try_start_94
    const-string v8, "btopp"

    #@96
    invoke-virtual {v1, v8, p2, v4, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_99
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_94 .. :try_end_99} :catch_c4

    #@99
    move-result v0

    #@9a
    .line 498
    .local v0, count:I
    :goto_9a
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppProvider;->getContext()Landroid/content/Context;

    #@9d
    move-result-object v8

    #@9e
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a1
    move-result-object v8

    #@a2
    const/4 v9, 0x0

    #@a3
    invoke-virtual {v8, p1, v9}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    #@a6
    .line 500
    return v0

    #@a7
    .line 464
    .end local v0           #count:I
    .end local v4           #myWhere:Ljava/lang/String;
    :cond_a7
    new-instance v8, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v9, "( "

    #@ae
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v8

    #@b2
    invoke-virtual {v8, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b5
    move-result-object v8

    #@b6
    const-string v9, " ) AND "

    #@b8
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v8

    #@bc
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v4

    #@c0
    .restart local v4       #myWhere:Ljava/lang/String;
    goto :goto_60

    #@c1
    .line 467
    .end local v4           #myWhere:Ljava/lang/String;
    :cond_c1
    const-string v4, ""

    #@c3
    .restart local v4       #myWhere:Ljava/lang/String;
    goto :goto_60

    #@c4
    .line 482
    :catch_c4
    move-exception v2

    #@c5
    .line 483
    .local v2, e:Landroid/database/sqlite/SQLiteException;
    const/4 v0, 0x0

    #@c6
    .line 484
    .restart local v0       #count:I
    goto :goto_9a

    #@c7
    .line 487
    .end local v0           #count:I
    .end local v2           #e:Landroid/database/sqlite/SQLiteException;
    :cond_c7
    const/4 v0, 0x0

    #@c8
    .line 489
    .restart local v0       #count:I
    goto :goto_9a

    #@c9
    .line 456
    nop

    #@ca
    :pswitch_data_ca
    .packed-switch 0x1
        :pswitch_43
        :pswitch_43
    .end packed-switch
.end method
