.class public Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "BluetoothOppBtErrorActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mErrorContent:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    #@3
    return-void
.end method

.method private createView()Landroid/view/View;
    .registers 6

    #@0
    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@3
    move-result-object v2

    #@4
    const v3, 0x7f030004

    #@7
    const/4 v4, 0x0

    #@8
    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    .line 77
    .local v1, view:Landroid/view/View;
    const v2, 0x7f0b000a

    #@f
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/widget/TextView;

    #@15
    .line 78
    .local v0, contentView:Landroid/widget/TextView;
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;->mErrorContent:Ljava/lang/String;

    #@17
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1a
    .line 79
    return-object v1
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 3
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 83
    packed-switch p2, :pswitch_data_4

    #@3
    .line 89
    :pswitch_3
    return-void

    #@4
    .line 83
    :pswitch_data_4
    .packed-switch -0x1
        :pswitch_3
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 58
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;->getIntent()Landroid/content/Intent;

    #@6
    move-result-object v0

    #@7
    .line 59
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "title"

    #@9
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 60
    .local v1, mErrorTitle:Ljava/lang/String;
    const-string v3, "content"

    #@f
    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v3

    #@13
    iput-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;->mErrorContent:Ljava/lang/String;

    #@15
    .line 63
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    #@17
    .line 66
    .local v2, p:Lcom/android/internal/app/AlertController$AlertParams;
    const v3, 0x1010355

    #@1a
    iput v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    #@1c
    .line 68
    iput-object v1, v2, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    #@1e
    .line 69
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;->createView()Landroid/view/View;

    #@21
    move-result-object v3

    #@22
    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    #@24
    .line 70
    const v3, 0x7f070038

    #@27
    invoke-virtual {p0, v3}, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;->getString(I)Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    iput-object v3, v2, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@2d
    .line 71
    iput-object p0, v2, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@2f
    .line 72
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppBtErrorActivity;->setupAlert()V

    #@32
    .line 73
    return-void
.end method
