.class public Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;
.super Ljava/lang/Object;
.source "BluetoothOppRfcommTransport.java"

# interfaces
.implements Ljavax/obex/ObexTransport;


# instance fields
.field private final mSocket:Landroid/bluetooth/BluetoothSocket;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothSocket;)V
    .registers 2
    .parameter "socket"

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 51
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@5
    .line 52
    return-void
.end method


# virtual methods
.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 55
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V

    #@5
    .line 56
    return-void
.end method

.method public connect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 75
    return-void
.end method

.method public create()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 78
    return-void
.end method

.method public disconnect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 81
    return-void
.end method

.method public getRemoteAddress()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 93
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 94
    const/4 v0, 0x0

    #@5
    .line 96
    :goto_5
    return-object v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@8
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@f
    move-result-object v0

    #@10
    goto :goto_5
.end method

.method public isConnected()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 89
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public listen()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 84
    return-void
.end method

.method public openDataInputStream()Ljava/io/DataInputStream;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 59
    new-instance v0, Ljava/io/DataInputStream;

    #@2
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;->openInputStream()Ljava/io/InputStream;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@9
    return-object v0
.end method

.method public openDataOutputStream()Ljava/io/DataOutputStream;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 63
    new-instance v0, Ljava/io/DataOutputStream;

    #@2
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;->openOutputStream()Ljava/io/OutputStream;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@9
    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 67
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public openOutputStream()Ljava/io/OutputStream;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
