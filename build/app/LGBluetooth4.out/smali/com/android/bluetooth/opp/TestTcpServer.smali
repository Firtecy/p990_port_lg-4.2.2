.class Lcom/android/bluetooth/opp/TestTcpServer;
.super Ljavax/obex/ServerRequestHandler;
.source "TestActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static final PORT:I = 0x1964

.field private static final TAG:Ljava/lang/String; = "ServerRequestHandler"

.field private static final V:Z = true


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 493
    invoke-direct {p0}, Ljavax/obex/ServerRequestHandler;-><init>()V

    #@3
    .line 477
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/TestTcpServer;->a:Z

    #@6
    .line 494
    const-string v0, "enter construtor of TcpServer"

    #@8
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/opp/TestTcpServer;->updateStatus(Ljava/lang/String;)V

    #@b
    .line 495
    return-void
.end method


# virtual methods
.method public onAuthenticationFailure([B)V
    .registers 2
    .parameter "userName"

    #@0
    .prologue
    .line 558
    return-void
.end method

.method public onConnect(Ljavax/obex/HeaderSet;Ljavax/obex/HeaderSet;)I
    .registers 6
    .parameter "request"
    .parameter "reply"

    #@0
    .prologue
    .line 499
    const-string v1, "[server:] The client has created an OBEX session"

    #@2
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/TestTcpServer;->updateStatus(Ljava/lang/String;)V

    #@5
    .line 501
    monitor-enter p0

    #@6
    .line 503
    :goto_6
    :try_start_6
    iget-boolean v1, p0, Lcom/android/bluetooth/opp/TestTcpServer;->a:Z

    #@8
    if-nez v1, :cond_18

    #@a
    .line 504
    const-wide/16 v1, 0x1f4

    #@c
    invoke-virtual {p0, v1, v2}, Ljava/lang/Object;->wait(J)V
    :try_end_f
    .catchall {:try_start_6 .. :try_end_f} :catchall_21
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_f} :catch_10

    #@f
    goto :goto_6

    #@10
    .line 506
    :catch_10
    move-exception v0

    #@11
    .line 508
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_11
    const-string v1, "ServerRequestHandler"

    #@13
    const-string v2, "Interrupted waiting for markBatchFailed"

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 511
    .end local v0           #e:Ljava/lang/InterruptedException;
    :cond_18
    monitor-exit p0
    :try_end_19
    .catchall {:try_start_11 .. :try_end_19} :catchall_21

    #@19
    .line 512
    const-string v1, "[server:] we accpet the seesion"

    #@1b
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/TestTcpServer;->updateStatus(Ljava/lang/String;)V

    #@1e
    .line 513
    const/16 v1, 0xa0

    #@20
    return v1

    #@21
    .line 511
    :catchall_21
    move-exception v1

    #@22
    :try_start_22
    monitor-exit p0
    :try_end_23
    .catchall {:try_start_22 .. :try_end_23} :catchall_21

    #@23
    throw v1
.end method

.method public onDelete(Ljavax/obex/HeaderSet;Ljavax/obex/HeaderSet;)I
    .registers 4
    .parameter "request"
    .parameter "reply"

    #@0
    .prologue
    .line 566
    const/16 v0, 0xd1

    #@2
    return v0
.end method

.method public onDisconnect(Ljavax/obex/HeaderSet;Ljavax/obex/HeaderSet;)V
    .registers 4
    .parameter "req"
    .parameter "resp"

    #@0
    .prologue
    .line 550
    const-string v0, "[server:] The client has disconnected the OBEX session"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/opp/TestTcpServer;->updateStatus(Ljava/lang/String;)V

    #@5
    .line 551
    return-void
.end method

.method public onGet(Ljavax/obex/Operation;)I
    .registers 3
    .parameter "op"

    #@0
    .prologue
    .line 570
    const/16 v0, 0xd1

    #@2
    return v0
.end method

.method public onPut(Ljavax/obex/Operation;)I
    .registers 13
    .parameter "op"

    #@0
    .prologue
    .line 517
    const/4 v4, 0x0

    #@1
    .line 519
    .local v4, fos:Ljava/io/FileOutputStream;
    :try_start_1
    invoke-interface {p1}, Ljavax/obex/Operation;->openInputStream()Ljava/io/InputStream;

    #@4
    move-result-object v6

    #@5
    .line 521
    .local v6, is:Ljava/io/InputStream;
    new-instance v8, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v9, "Got data bytes "

    #@c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v8

    #@10
    invoke-virtual {v6}, Ljava/io/InputStream;->available()I

    #@13
    move-result v9

    #@14
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v8

    #@18
    const-string v9, " name "

    #@1a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v8

    #@1e
    invoke-interface {p1}, Ljavax/obex/Operation;->getReceivedHeader()Ljavax/obex/HeaderSet;

    #@21
    move-result-object v9

    #@22
    const/4 v10, 0x1

    #@23
    invoke-virtual {v9, v10}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@26
    move-result-object v9

    #@27
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v8

    #@2b
    const-string v9, " type "

    #@2d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v8

    #@31
    invoke-interface {p1}, Ljavax/obex/Operation;->getType()Ljava/lang/String;

    #@34
    move-result-object v9

    #@35
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v8

    #@3d
    invoke-virtual {p0, v8}, Lcom/android/bluetooth/opp/TestTcpServer;->updateStatus(Ljava/lang/String;)V

    #@40
    .line 524
    new-instance v3, Ljava/io/File;

    #@42
    invoke-interface {p1}, Ljavax/obex/Operation;->getReceivedHeader()Ljavax/obex/HeaderSet;

    #@45
    move-result-object v8

    #@46
    const/4 v9, 0x1

    #@47
    invoke-virtual {v8, v9}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@4a
    move-result-object v8

    #@4b
    check-cast v8, Ljava/lang/String;

    #@4d
    invoke-direct {v3, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@50
    .line 525
    .local v3, f:Ljava/io/File;
    new-instance v5, Ljava/io/FileOutputStream;

    #@52
    invoke-direct {v5, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_55
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_55} :catch_9e

    #@55
    .line 526
    .end local v4           #fos:Ljava/io/FileOutputStream;
    .local v5, fos:Ljava/io/FileOutputStream;
    const/16 v8, 0x3e8

    #@57
    :try_start_57
    new-array v0, v8, [B

    #@59
    .line 529
    .local v0, b:[B
    :goto_59
    invoke-virtual {v6}, Ljava/io/InputStream;->available()I

    #@5c
    move-result v8

    #@5d
    if-lez v8, :cond_77

    #@5f
    invoke-virtual {v6, v0}, Ljava/io/InputStream;->read([B)I

    #@62
    move-result v7

    #@63
    .local v7, len:I
    if-lez v7, :cond_77

    #@65
    .line 530
    const/4 v8, 0x0

    #@66
    invoke-virtual {v5, v0, v8, v7}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_69
    .catch Ljava/lang/Exception; {:try_start_57 .. :try_end_69} :catch_6a

    #@69
    goto :goto_59

    #@6a
    .line 536
    .end local v0           #b:[B
    .end local v7           #len:I
    :catch_6a
    move-exception v1

    #@6b
    move-object v4, v5

    #@6c
    .line 537
    .end local v3           #f:Ljava/io/File;
    .end local v5           #fos:Ljava/io/FileOutputStream;
    .end local v6           #is:Ljava/io/InputStream;
    .local v1, e:Ljava/lang/Exception;
    .restart local v4       #fos:Ljava/io/FileOutputStream;
    :goto_6c
    if-eqz v4, :cond_71

    #@6e
    .line 539
    :try_start_6e
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_71
    .catch Ljava/io/IOException; {:try_start_6e .. :try_end_71} :catch_99

    #@71
    .line 544
    :cond_71
    :goto_71
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    #@74
    .line 546
    .end local v1           #e:Ljava/lang/Exception;
    :goto_74
    const/16 v8, 0xa0

    #@76
    return v8

    #@77
    .line 533
    .end local v4           #fos:Ljava/io/FileOutputStream;
    .restart local v0       #b:[B
    .restart local v3       #f:Ljava/io/File;
    .restart local v5       #fos:Ljava/io/FileOutputStream;
    .restart local v6       #is:Ljava/io/InputStream;
    :cond_77
    :try_start_77
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    #@7a
    .line 534
    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    #@7d
    .line 535
    new-instance v8, Ljava/lang/StringBuilder;

    #@7f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@82
    const-string v9, "[server:] Wrote data to "

    #@84
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v8

    #@88
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    #@8b
    move-result-object v9

    #@8c
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v8

    #@90
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@93
    move-result-object v8

    #@94
    invoke-virtual {p0, v8}, Lcom/android/bluetooth/opp/TestTcpServer;->updateStatus(Ljava/lang/String;)V
    :try_end_97
    .catch Ljava/lang/Exception; {:try_start_77 .. :try_end_97} :catch_6a

    #@97
    move-object v4, v5

    #@98
    .line 545
    .end local v5           #fos:Ljava/io/FileOutputStream;
    .restart local v4       #fos:Ljava/io/FileOutputStream;
    goto :goto_74

    #@99
    .line 540
    .end local v0           #b:[B
    .end local v3           #f:Ljava/io/File;
    .end local v6           #is:Ljava/io/InputStream;
    .restart local v1       #e:Ljava/lang/Exception;
    :catch_99
    move-exception v2

    #@9a
    .line 541
    .local v2, e1:Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    #@9d
    goto :goto_71

    #@9e
    .line 536
    .end local v1           #e:Ljava/lang/Exception;
    .end local v2           #e1:Ljava/io/IOException;
    :catch_9e
    move-exception v1

    #@9f
    goto :goto_6c
.end method

.method public onSetPath(Ljavax/obex/HeaderSet;Ljavax/obex/HeaderSet;ZZ)I
    .registers 6
    .parameter "request"
    .parameter "reply"
    .parameter "backup"
    .parameter "create"

    #@0
    .prologue
    .line 562
    const/16 v0, 0xd1

    #@2
    return v0
.end method

.method public run()V
    .registers 5

    #@0
    .prologue
    .line 482
    :try_start_0
    const-string v2, "[server:] listen on port 6500"

    #@2
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/TestTcpServer;->updateStatus(Ljava/lang/String;)V

    #@5
    .line 483
    new-instance v1, Lcom/android/bluetooth/opp/TestTcpSessionNotifier;

    #@7
    const/16 v2, 0x1964

    #@9
    invoke-direct {v1, v2}, Lcom/android/bluetooth/opp/TestTcpSessionNotifier;-><init>(I)V

    #@c
    .line 485
    .local v1, rsn:Lcom/android/bluetooth/opp/TestTcpSessionNotifier;
    const-string v2, "[server:] Now waiting for a client to connect"

    #@e
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/TestTcpServer;->updateStatus(Ljava/lang/String;)V

    #@11
    .line 486
    invoke-virtual {v1, p0}, Lcom/android/bluetooth/opp/TestTcpSessionNotifier;->acceptAndOpen(Ljavax/obex/ServerRequestHandler;)Ljavax/obex/ServerSession;

    #@14
    .line 487
    const-string v2, "[server:] A client is now connected"

    #@16
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/TestTcpServer;->updateStatus(Ljava/lang/String;)V
    :try_end_19
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_19} :catch_1a

    #@19
    .line 491
    .end local v1           #rsn:Lcom/android/bluetooth/opp/TestTcpSessionNotifier;
    :goto_19
    return-void

    #@1a
    .line 488
    :catch_1a
    move-exception v0

    #@1b
    .line 489
    .local v0, ex:Ljava/lang/Exception;
    new-instance v2, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v3, "[server:] Caught the error: "

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/opp/TestTcpServer;->updateStatus(Ljava/lang/String;)V

    #@31
    goto :goto_19
.end method

.method public updateStatus(Ljava/lang/String;)V
    .registers 5
    .parameter "message"

    #@0
    .prologue
    .line 554
    const-string v0, "ServerRequestHandler"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "\n"

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 555
    return-void
.end method
