.class Lcom/android/bluetooth/opp/TestTcpSessionNotifier;
.super Ljava/lang/Object;
.source "TestActivity.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TestTcpSessionNotifier"


# instance fields
.field conn:Ljava/net/Socket;

.field server:Ljava/net/ServerSocket;


# direct methods
.method public constructor <init>(I)V
    .registers 3
    .parameter "port"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 578
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestTcpSessionNotifier;->server:Ljava/net/ServerSocket;

    #@6
    .line 580
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestTcpSessionNotifier;->conn:Ljava/net/Socket;

    #@8
    .line 585
    new-instance v0, Ljava/net/ServerSocket;

    #@a
    invoke-direct {v0, p1}, Ljava/net/ServerSocket;-><init>(I)V

    #@d
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestTcpSessionNotifier;->server:Ljava/net/ServerSocket;

    #@f
    .line 586
    return-void
.end method


# virtual methods
.method public acceptAndOpen(Ljavax/obex/ServerRequestHandler;)Ljavax/obex/ServerSession;
    .registers 3
    .parameter "handler"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 605
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, p1, v0}, Lcom/android/bluetooth/opp/TestTcpSessionNotifier;->acceptAndOpen(Ljavax/obex/ServerRequestHandler;Ljavax/obex/Authenticator;)Ljavax/obex/ServerSession;

    #@4
    move-result-object v0

    #@5
    return-object v0
.end method

.method public acceptAndOpen(Ljavax/obex/ServerRequestHandler;Ljavax/obex/Authenticator;)Ljavax/obex/ServerSession;
    .registers 7
    .parameter "handler"
    .parameter "auth"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 591
    :try_start_0
    iget-object v2, p0, Lcom/android/bluetooth/opp/TestTcpSessionNotifier;->server:Ljava/net/ServerSocket;

    #@2
    invoke-virtual {v2}, Ljava/net/ServerSocket;->accept()Ljava/net/Socket;

    #@5
    move-result-object v2

    #@6
    iput-object v2, p0, Lcom/android/bluetooth/opp/TestTcpSessionNotifier;->conn:Ljava/net/Socket;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_8} :catch_15

    #@8
    .line 597
    :goto_8
    new-instance v1, Lcom/android/bluetooth/opp/TestTcpTransport;

    #@a
    iget-object v2, p0, Lcom/android/bluetooth/opp/TestTcpSessionNotifier;->conn:Ljava/net/Socket;

    #@c
    invoke-direct {v1, v2}, Lcom/android/bluetooth/opp/TestTcpTransport;-><init>(Ljava/net/Socket;)V

    #@f
    .line 599
    .local v1, tt:Lcom/android/bluetooth/opp/TestTcpTransport;
    new-instance v2, Ljavax/obex/ServerSession;

    #@11
    invoke-direct {v2, v1, p1, p2}, Ljavax/obex/ServerSession;-><init>(Ljavax/obex/ObexTransport;Ljavax/obex/ServerRequestHandler;Ljavax/obex/Authenticator;)V

    #@14
    return-object v2

    #@15
    .line 593
    .end local v1           #tt:Lcom/android/bluetooth/opp/TestTcpTransport;
    :catch_15
    move-exception v0

    #@16
    .line 594
    .local v0, ex:Ljava/lang/Exception;
    const-string v2, "TestTcpSessionNotifier"

    #@18
    const-string v3, "ex"

    #@1a
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    goto :goto_8
.end method
