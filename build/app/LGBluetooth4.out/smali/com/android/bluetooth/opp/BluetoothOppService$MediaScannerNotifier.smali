.class Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;
.super Ljava/lang/Object;
.source "BluetoothOppService.java"

# interfaces
.implements Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/opp/BluetoothOppService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaScannerNotifier"
.end annotation


# instance fields
.field private mCallback:Landroid/os/Handler;

.field private mConnection:Landroid/media/MediaScannerConnection;

.field private mContext:Landroid/content/Context;

.field private mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/bluetooth/opp/BluetoothOppShareInfo;Landroid/os/Handler;)V
    .registers 6
    .parameter "context"
    .parameter "info"
    .parameter "handler"

    #@0
    .prologue
    .line 1138
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 1139
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mContext:Landroid/content/Context;

    #@5
    .line 1140
    iput-object p2, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@7
    .line 1141
    iput-object p3, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mCallback:Landroid/os/Handler;

    #@9
    .line 1142
    new-instance v0, Landroid/media/MediaScannerConnection;

    #@b
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mContext:Landroid/content/Context;

    #@d
    invoke-direct {v0, v1, p0}, Landroid/media/MediaScannerConnection;-><init>(Landroid/content/Context;Landroid/media/MediaScannerConnection$MediaScannerConnectionClient;)V

    #@10
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mConnection:Landroid/media/MediaScannerConnection;

    #@12
    .line 1144
    const-string v0, "BtOppService"

    #@14
    const-string v1, "Connecting to MediaScannerConnection "

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    .line 1146
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mConnection:Landroid/media/MediaScannerConnection;

    #@1b
    invoke-virtual {v0}, Landroid/media/MediaScannerConnection;->connect()V

    #@1e
    .line 1147
    return-void
.end method


# virtual methods
.method public onMediaScannerConnected()V
    .registers 4

    #@0
    .prologue
    .line 1151
    const-string v0, "BtOppService"

    #@2
    const-string v1, "MediaScannerConnection onMediaScannerConnected"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1153
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mConnection:Landroid/media/MediaScannerConnection;

    #@9
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@b
    iget-object v1, v1, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mFilename:Ljava/lang/String;

    #@d
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@f
    iget-object v2, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mMimetype:Ljava/lang/String;

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/media/MediaScannerConnection;->scanFile(Ljava/lang/String;Ljava/lang/String;)V

    #@14
    .line 1154
    return-void
.end method

.method public onScanCompleted(Ljava/lang/String;Landroid/net/Uri;)V
    .registers 8
    .parameter "path"
    .parameter "uri"

    #@0
    .prologue
    .line 1159
    :try_start_0
    const-string v2, "BtOppService"

    #@2
    const-string v3, "MediaScannerConnection onScanCompleted"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1160
    const-string v2, "BtOppService"

    #@9
    new-instance v3, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v4, "MediaScannerConnection path is "

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v3

    #@18
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v3

    #@1c
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 1161
    const-string v2, "BtOppService"

    #@21
    new-instance v3, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v4, "MediaScannerConnection Uri is "

    #@28
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v3

    #@2c
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 1163
    if-eqz p2, :cond_5d

    #@39
    .line 1164
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@3c
    move-result-object v1

    #@3d
    .line 1165
    .local v1, msg:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mCallback:Landroid/os/Handler;

    #@3f
    invoke-virtual {v1, v2}, Landroid/os/Message;->setTarget(Landroid/os/Handler;)V

    #@42
    .line 1166
    const/4 v2, 0x2

    #@43
    iput v2, v1, Landroid/os/Message;->what:I

    #@45
    .line 1167
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@47
    iget v2, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@49
    iput v2, v1, Landroid/os/Message;->arg1:I

    #@4b
    .line 1168
    iput-object p2, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4d
    .line 1169
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
    :try_end_50
    .catchall {:try_start_0 .. :try_end_50} :catchall_96
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_50} :catch_73

    #@50
    .line 1181
    :goto_50
    const-string v2, "BtOppService"

    #@52
    const-string v3, "MediaScannerConnection disconnect"

    #@54
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 1183
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mConnection:Landroid/media/MediaScannerConnection;

    #@59
    .end local v1           #msg:Landroid/os/Message;
    :goto_59
    invoke-virtual {v2}, Landroid/media/MediaScannerConnection;->disconnect()V

    #@5c
    .line 1185
    return-void

    #@5d
    .line 1171
    :cond_5d
    :try_start_5d
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    #@60
    move-result-object v1

    #@61
    .line 1172
    .restart local v1       #msg:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mCallback:Landroid/os/Handler;

    #@63
    invoke-virtual {v1, v2}, Landroid/os/Message;->setTarget(Landroid/os/Handler;)V

    #@66
    .line 1173
    const/4 v2, 0x3

    #@67
    iput v2, v1, Landroid/os/Message;->what:I

    #@69
    .line 1174
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mInfo:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@6b
    iget v2, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@6d
    iput v2, v1, Landroid/os/Message;->arg1:I

    #@6f
    .line 1175
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V
    :try_end_72
    .catchall {:try_start_5d .. :try_end_72} :catchall_96
    .catch Ljava/lang/Exception; {:try_start_5d .. :try_end_72} :catch_73

    #@72
    goto :goto_50

    #@73
    .line 1177
    .end local v1           #msg:Landroid/os/Message;
    :catch_73
    move-exception v0

    #@74
    .line 1178
    .local v0, ex:Ljava/lang/Exception;
    :try_start_74
    const-string v2, "BtOppService"

    #@76
    new-instance v3, Ljava/lang/StringBuilder;

    #@78
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7b
    const-string v4, "!!!MediaScannerConnection exception: "

    #@7d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v3

    #@81
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v3

    #@85
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@88
    move-result-object v3

    #@89
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8c
    .catchall {:try_start_74 .. :try_end_8c} :catchall_96

    #@8c
    .line 1181
    const-string v2, "BtOppService"

    #@8e
    const-string v3, "MediaScannerConnection disconnect"

    #@90
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 1183
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mConnection:Landroid/media/MediaScannerConnection;

    #@95
    goto :goto_59

    #@96
    .line 1180
    .end local v0           #ex:Ljava/lang/Exception;
    :catchall_96
    move-exception v2

    #@97
    .line 1181
    const-string v3, "BtOppService"

    #@99
    const-string v4, "MediaScannerConnection disconnect"

    #@9b
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    .line 1183
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;->mConnection:Landroid/media/MediaScannerConnection;

    #@a0
    invoke-virtual {v3}, Landroid/media/MediaScannerConnection;->disconnect()V

    #@a3
    .line 1180
    throw v2
.end method
