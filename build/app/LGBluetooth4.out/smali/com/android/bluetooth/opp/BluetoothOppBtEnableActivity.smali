.class public Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "BluetoothOppBtEnableActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private mBluetoothReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 62
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    #@3
    .line 144
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity$1;-><init>(Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;)V

    #@8
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    #@a
    return-void
.end method

.method private createView()Landroid/view/View;
    .registers 6

    #@0
    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@3
    move-result-object v2

    #@4
    const v3, 0x7f030004

    #@7
    const/4 v4, 0x0

    #@8
    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    .line 94
    .local v1, view:Landroid/view/View;
    const v2, 0x7f0b000a

    #@f
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@12
    move-result-object v0

    #@13
    check-cast v0, Landroid/widget/TextView;

    #@15
    .line 100
    .local v0, contentView:Landroid/widget/TextView;
    new-instance v2, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v3, "\n"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    const v3, 0x7f07006e

    #@23
    invoke-virtual {p0, v3}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->getString(I)Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, "\n"

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@38
    .line 103
    return-object v1
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 6
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 107
    packed-switch p2, :pswitch_data_28

    #@3
    .line 134
    :goto_3
    return-void

    #@4
    .line 109
    :pswitch_4
    invoke-static {p0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@7
    move-result-object v1

    #@8
    .line 110
    .local v1, mOppManager:Lcom/android/bluetooth/opp/BluetoothOppManager;
    invoke-virtual {v1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->enableBluetooth()V

    #@b
    .line 111
    const/4 v2, 0x1

    #@c
    iput-boolean v2, v1, Lcom/android/bluetooth/opp/BluetoothOppManager;->mSendingFlag:Z

    #@e
    .line 117
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothServiceManager;->checkBtWifiCoex(Landroid/content/Context;)V

    #@11
    .line 120
    new-instance v0, Landroid/content/Intent;

    #@13
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppBtEnablingActivity;

    #@15
    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@18
    .line 121
    .local v0, in:Landroid/content/Intent;
    const/high16 v2, 0x1000

    #@1a
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@1d
    .line 122
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->startActivity(Landroid/content/Intent;)V

    #@20
    .line 124
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->finish()V

    #@23
    goto :goto_3

    #@24
    .line 128
    .end local v0           #in:Landroid/content/Intent;
    .end local v1           #mOppManager:Lcom/android/bluetooth/opp/BluetoothOppManager;
    :pswitch_24
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->finish()V

    #@27
    goto :goto_3

    #@28
    .line 107
    :pswitch_data_28
    .packed-switch -0x2
        :pswitch_24
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 67
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 70
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    #@5
    .line 74
    .local v0, p:Lcom/android/internal/app/AlertController$AlertParams;
    const v1, 0x7f07000c

    #@8
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->getString(I)Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    #@e
    .line 75
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->createView()Landroid/view/View;

    #@11
    move-result-object v1

    #@12
    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    #@14
    .line 78
    const v1, 0x7f070067

    #@17
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->getString(I)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@1d
    .line 79
    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@1f
    .line 81
    const v1, 0x7f070068

    #@22
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->getString(I)Ljava/lang/String;

    #@25
    move-result-object v1

    #@26
    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@28
    .line 83
    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@2a
    .line 84
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->setupAlert()V

    #@2d
    .line 88
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    #@2f
    new-instance v2, Landroid/content/IntentFilter;

    #@31
    const-string v3, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@33
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@36
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@39
    .line 90
    return-void
.end method

.method protected onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 140
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onDestroy()V

    #@3
    .line 141
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    #@5
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppBtEnableActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@8
    .line 142
    return-void
.end method
