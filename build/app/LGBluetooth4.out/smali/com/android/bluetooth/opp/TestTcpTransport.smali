.class Lcom/android/bluetooth/opp/TestTcpTransport;
.super Ljava/lang/Object;
.source "TestActivity.java"

# interfaces
.implements Ljavax/obex/ObexTransport;


# instance fields
.field s:Ljava/net/Socket;


# direct methods
.method public constructor <init>(Ljava/net/Socket;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 616
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 613
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/bluetooth/opp/TestTcpTransport;->s:Ljava/net/Socket;

    #@6
    .line 617
    iput-object p1, p0, Lcom/android/bluetooth/opp/TestTcpTransport;->s:Ljava/net/Socket;

    #@8
    .line 618
    return-void
.end method


# virtual methods
.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 621
    iget-object v0, p0, Lcom/android/bluetooth/opp/TestTcpTransport;->s:Ljava/net/Socket;

    #@2
    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    #@5
    .line 622
    return-void
.end method

.method public connect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 643
    return-void
.end method

.method public create()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 648
    return-void
.end method

.method public disconnect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 653
    return-void
.end method

.method public isConnected()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 661
    iget-object v0, p0, Lcom/android/bluetooth/opp/TestTcpTransport;->s:Ljava/net/Socket;

    #@2
    invoke-virtual {v0}, Ljava/net/Socket;->isConnected()Z

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public listen()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 658
    return-void
.end method

.method public openDataInputStream()Ljava/io/DataInputStream;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 625
    new-instance v0, Ljava/io/DataInputStream;

    #@2
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/TestTcpTransport;->openInputStream()Ljava/io/InputStream;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@9
    return-object v0
.end method

.method public openDataOutputStream()Ljava/io/DataOutputStream;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 629
    new-instance v0, Ljava/io/DataOutputStream;

    #@2
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/TestTcpTransport;->openOutputStream()Ljava/io/OutputStream;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@9
    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 633
    iget-object v0, p0, Lcom/android/bluetooth/opp/TestTcpTransport;->s:Ljava/net/Socket;

    #@2
    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public openOutputStream()Ljava/io/OutputStream;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 637
    iget-object v0, p0, Lcom/android/bluetooth/opp/TestTcpTransport;->s:Ljava/net/Socket;

    #@2
    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
