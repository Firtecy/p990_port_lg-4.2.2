.class public Lcom/android/bluetooth/opp/BluetoothOppReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothOppReceiver.java"


# static fields
.field private static final D:Z = true

.field private static final TAG:Ljava/lang/String; = "BluetoothOppReceiver"

.field private static final V:Z = true


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 58
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 39
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 65
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v8

    #@4
    .line 68
    .local v8, action:Ljava/lang/String;
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@6
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_c2

    #@c
    .line 69
    const/16 v2, 0xc

    #@e
    const-string v3, "android.bluetooth.adapter.extra.STATE"

    #@10
    const/high16 v4, -0x8000

    #@12
    move-object/from16 v0, p2

    #@14
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@17
    move-result v3

    #@18
    if-ne v2, v3, :cond_44

    #@1a
    .line 72
    const-string v2, "BluetoothOppReceiver"

    #@1c
    const-string v3, "Received BLUETOOTH_STATE_CHANGED_ACTION, BLUETOOTH_STATE_ON"

    #@1e
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 76
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@24
    move-result-object v2

    #@25
    if-eqz v2, :cond_45

    #@27
    .line 77
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2a
    move-result-object v2

    #@2b
    const/4 v3, 0x0

    #@2c
    invoke-interface {v2, v3}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@2f
    move-result v2

    #@30
    if-nez v2, :cond_3d

    #@32
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@35
    move-result-object v2

    #@36
    const/4 v3, 0x2

    #@37
    invoke-interface {v2, v3}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_45

    #@3d
    .line 82
    :cond_3d
    const-string v2, "BluetoothOppReceiver"

    #@3f
    const-string v3, "OPP profile is blocked by LG MDM Server Policy"

    #@41
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 404
    :cond_44
    :goto_44
    return-void

    #@45
    .line 88
    :cond_45
    new-instance v2, Landroid/content/Intent;

    #@47
    const-class v3, Lcom/android/bluetooth/opp/BluetoothOppService;

    #@49
    move-object/from16 v0, p1

    #@4b
    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@4e
    move-object/from16 v0, p1

    #@50
    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@53
    .line 92
    monitor-enter p0

    #@54
    .line 93
    :try_start_54
    invoke-static/range {p1 .. p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@57
    move-result-object v2

    #@58
    iget-boolean v2, v2, Lcom/android/bluetooth/opp/BluetoothOppManager;->mSendingFlag:Z

    #@5a
    if-eqz v2, :cond_bd

    #@5c
    .line 95
    invoke-static/range {p1 .. p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@5f
    move-result-object v2

    #@60
    const/4 v3, 0x0

    #@61
    iput-boolean v3, v2, Lcom/android/bluetooth/opp/BluetoothOppManager;->mSendingFlag:Z

    #@63
    .line 97
    new-instance v16, Landroid/content/Intent;

    #@65
    const-string v2, "android.bluetooth.devicepicker.action.LAUNCH"

    #@67
    move-object/from16 v0, v16

    #@69
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6c
    .line 98
    .local v16, in1:Landroid/content/Intent;
    const-string v2, "android.bluetooth.devicepicker.extra.NEED_AUTH"

    #@6e
    const/4 v3, 0x0

    #@6f
    move-object/from16 v0, v16

    #@71
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@74
    .line 99
    const-string v2, "android.bluetooth.devicepicker.extra.FILTER_TYPE"

    #@76
    const/4 v3, 0x2

    #@77
    move-object/from16 v0, v16

    #@79
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@7c
    .line 101
    const-string v2, "android.bluetooth.devicepicker.extra.LAUNCH_PACKAGE"

    #@7e
    const-string v3, "com.android.bluetooth"

    #@80
    move-object/from16 v0, v16

    #@82
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@85
    .line 103
    const-string v2, "android.bluetooth.devicepicker.extra.DEVICE_PICKER_LAUNCH_CLASS"

    #@87
    const-class v3, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@89
    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@8c
    move-result-object v3

    #@8d
    move-object/from16 v0, v16

    #@8f
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@92
    .line 106
    const/high16 v2, 0x1000

    #@94
    move-object/from16 v0, v16

    #@96
    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@99
    .line 107
    move-object/from16 v0, p1

    #@9b
    move-object/from16 v1, v16

    #@9d
    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@a0
    .line 109
    new-instance v21, Landroid/content/Intent;

    #@a2
    const-string v2, "com.lge.bluetooth.opp.sendservice"

    #@a4
    move-object/from16 v0, v21

    #@a6
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a9
    .line 110
    .local v21, serviceIntent:Landroid/content/Intent;
    const-string v2, "intent"

    #@ab
    invoke-static/range {p1 .. p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@ae
    move-result-object v3

    #@af
    iget-object v3, v3, Lcom/android/bluetooth/opp/BluetoothOppManager;->mSendTypeIntent:Landroid/content/Intent;

    #@b1
    move-object/from16 v0, v21

    #@b3
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@b6
    .line 111
    move-object/from16 v0, p1

    #@b8
    move-object/from16 v1, v21

    #@ba
    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@bd
    .line 114
    .end local v16           #in1:Landroid/content/Intent;
    .end local v21           #serviceIntent:Landroid/content/Intent;
    :cond_bd
    monitor-exit p0

    #@be
    goto :goto_44

    #@bf
    :catchall_bf
    move-exception v2

    #@c0
    monitor-exit p0
    :try_end_c1
    .catchall {:try_start_54 .. :try_end_c1} :catchall_bf

    #@c1
    throw v2

    #@c2
    .line 116
    :cond_c2
    const-string v2, "android.bluetooth.devicepicker.action.DEVICE_SELECTED"

    #@c4
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c7
    move-result v2

    #@c8
    if-eqz v2, :cond_147

    #@ca
    .line 117
    invoke-static/range {p1 .. p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@cd
    move-result-object v17

    #@ce
    .line 119
    .local v17, mOppManager:Lcom/android/bluetooth/opp/BluetoothOppManager;
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    #@d0
    move-object/from16 v0, p2

    #@d2
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@d5
    move-result-object v19

    #@d6
    check-cast v19, Landroid/bluetooth/BluetoothDevice;

    #@d8
    .line 122
    .local v19, remoteDevice:Landroid/bluetooth/BluetoothDevice;
    const-string v2, "BluetoothOppReceiver"

    #@da
    new-instance v3, Ljava/lang/StringBuilder;

    #@dc
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@df
    const-string v4, "Received BT device selected intent, bt device: "

    #@e1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v3

    #@e5
    move-object/from16 v0, v19

    #@e7
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v3

    #@eb
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ee
    move-result-object v3

    #@ef
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f2
    .line 126
    move-object/from16 v0, v17

    #@f4
    move-object/from16 v1, v19

    #@f6
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->startTransfer(Landroid/bluetooth/BluetoothDevice;)V

    #@f9
    .line 129
    move-object/from16 v0, v17

    #@fb
    move-object/from16 v1, v19

    #@fd
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getDeviceName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@100
    move-result-object v13

    #@101
    .line 131
    .local v13, deviceName:Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getBatchSize()I

    #@104
    move-result v9

    #@105
    .line 132
    .local v9, batchSize:I
    move-object/from16 v0, v17

    #@107
    iget-boolean v2, v0, Lcom/android/bluetooth/opp/BluetoothOppManager;->mMultipleFlag:Z

    #@109
    if-eqz v2, :cond_137

    #@10b
    .line 133
    const v2, 0x7f070043

    #@10e
    const/4 v3, 0x2

    #@10f
    new-array v3, v3, [Ljava/lang/Object;

    #@111
    const/4 v4, 0x0

    #@112
    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@115
    move-result-object v5

    #@116
    aput-object v5, v3, v4

    #@118
    const/4 v4, 0x1

    #@119
    aput-object v13, v3, v4

    #@11b
    move-object/from16 v0, p1

    #@11d
    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@120
    move-result-object v25

    #@121
    .line 139
    .local v25, toastMsg:Ljava/lang/String;
    :goto_121
    const/4 v2, 0x0

    #@122
    move-object/from16 v0, p1

    #@124
    move-object/from16 v1, v25

    #@126
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@129
    move-result-object v24

    #@12a
    .line 140
    .local v24, toast:Landroid/widget/Toast;
    invoke-virtual/range {v24 .. v24}, Landroid/widget/Toast;->getView()Landroid/view/View;

    #@12d
    move-result-object v2

    #@12e
    const/4 v3, 0x5

    #@12f
    invoke-virtual {v2, v3}, Landroid/view/View;->setTextDirection(I)V

    #@132
    .line 141
    invoke-virtual/range {v24 .. v24}, Landroid/widget/Toast;->show()V

    #@135
    goto/16 :goto_44

    #@137
    .line 136
    .end local v24           #toast:Landroid/widget/Toast;
    .end local v25           #toastMsg:Ljava/lang/String;
    :cond_137
    const v2, 0x7f070042

    #@13a
    const/4 v3, 0x1

    #@13b
    new-array v3, v3, [Ljava/lang/Object;

    #@13d
    const/4 v4, 0x0

    #@13e
    aput-object v13, v3, v4

    #@140
    move-object/from16 v0, p1

    #@142
    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@145
    move-result-object v25

    #@146
    .restart local v25       #toastMsg:Ljava/lang/String;
    goto :goto_121

    #@147
    .line 144
    .end local v9           #batchSize:I
    .end local v13           #deviceName:Ljava/lang/String;
    .end local v17           #mOppManager:Lcom/android/bluetooth/opp/BluetoothOppManager;
    .end local v19           #remoteDevice:Landroid/bluetooth/BluetoothDevice;
    .end local v25           #toastMsg:Ljava/lang/String;
    :cond_147
    const-string v2, "android.btopp.intent.action.CONFIRM"

    #@149
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14c
    move-result v2

    #@14d
    if-eqz v2, :cond_195

    #@14f
    .line 146
    const-string v2, "BluetoothOppReceiver"

    #@151
    const-string v3, "Receiver ACTION_INCOMING_FILE_CONFIRM"

    #@153
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@156
    .line 149
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@159
    move-result-object v30

    #@15a
    .line 150
    .local v30, uri:Landroid/net/Uri;
    new-instance v15, Landroid/content/Intent;

    #@15c
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppIncomingFileConfirmActivity;

    #@15e
    move-object/from16 v0, p1

    #@160
    invoke-direct {v15, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@163
    .line 151
    .local v15, in:Landroid/content/Intent;
    const/high16 v2, 0x1000

    #@165
    invoke-virtual {v15, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@168
    .line 152
    move-object/from16 v0, v30

    #@16a
    invoke-virtual {v15, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@16d
    .line 153
    move-object/from16 v0, p1

    #@16f
    invoke-virtual {v0, v15}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@172
    .line 155
    const-string v2, "notification"

    #@174
    move-object/from16 v0, p1

    #@176
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@179
    move-result-object v18

    #@17a
    check-cast v18, Landroid/app/NotificationManager;

    #@17c
    .line 157
    .local v18, notMgr:Landroid/app/NotificationManager;
    if-eqz v18, :cond_44

    #@17e
    .line 158
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@181
    move-result-object v2

    #@182
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    #@185
    move-result-wide v2

    #@186
    long-to-int v2, v2

    #@187
    move-object/from16 v0, v18

    #@189
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    #@18c
    .line 160
    const-string v2, "BluetoothOppReceiver"

    #@18e
    const-string v3, "notMgr.cancel called"

    #@190
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@193
    goto/16 :goto_44

    #@195
    .line 163
    .end local v15           #in:Landroid/content/Intent;
    .end local v18           #notMgr:Landroid/app/NotificationManager;
    .end local v30           #uri:Landroid/net/Uri;
    :cond_195
    const-string v2, "android.btopp.intent.action.INCOMING_FILE_NOTIFICATION"

    #@197
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19a
    move-result v2

    #@19b
    if-eqz v2, :cond_1a6

    #@19d
    .line 165
    const-string v2, "BluetoothOppReceiver"

    #@19f
    const-string v3, "Receiver INCOMING_FILE_NOTIFICATION"

    #@1a1
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a4
    goto/16 :goto_44

    #@1a6
    .line 177
    :cond_1a6
    const-string v2, "com.lge.btopp.intent.action.4GB_INCOMING_FILE_NOTIFICATION"

    #@1a8
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1ab
    move-result v2

    #@1ac
    if-eqz v2, :cond_1ca

    #@1ae
    .line 179
    const-string v2, "BluetoothOppReceiver"

    #@1b0
    const-string v3, "Receiver 4GB INCOMING_FILE_NOTIFICATION"

    #@1b2
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b5
    .line 182
    const v2, 0x7f07006f

    #@1b8
    move-object/from16 v0, p1

    #@1ba
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1bd
    move-result-object v2

    #@1be
    const/4 v3, 0x0

    #@1bf
    move-object/from16 v0, p1

    #@1c1
    invoke-static {v0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@1c4
    move-result-object v2

    #@1c5
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    #@1c8
    goto/16 :goto_44

    #@1ca
    .line 186
    :cond_1ca
    const-string v2, "android.btopp.intent.action.OPEN"

    #@1cc
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1cf
    move-result v2

    #@1d0
    if-nez v2, :cond_1da

    #@1d2
    const-string v2, "android.btopp.intent.action.LIST"

    #@1d4
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d7
    move-result v2

    #@1d8
    if-eqz v2, :cond_29e

    #@1da
    .line 188
    :cond_1da
    const-string v2, "android.btopp.intent.action.OPEN"

    #@1dc
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1df
    move-result v2

    #@1e0
    if-eqz v2, :cond_21a

    #@1e2
    .line 189
    const-string v2, "BluetoothOppReceiver"

    #@1e4
    new-instance v3, Ljava/lang/StringBuilder;

    #@1e6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1e9
    const-string v4, "Receiver open for "

    #@1eb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ee
    move-result-object v3

    #@1ef
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@1f2
    move-result-object v4

    #@1f3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f6
    move-result-object v3

    #@1f7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fa
    move-result-object v3

    #@1fb
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1fe
    .line 195
    :goto_1fe
    new-instance v27, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@200
    invoke-direct/range {v27 .. v27}, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;-><init>()V

    #@203
    .line 196
    .local v27, transInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@206
    move-result-object v30

    #@207
    .line 197
    .restart local v30       #uri:Landroid/net/Uri;
    move-object/from16 v0, p1

    #@209
    move-object/from16 v1, v30

    #@20b
    invoke-static {v0, v1}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->queryRecord(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@20e
    move-result-object v27

    #@20f
    .line 198
    if-nez v27, :cond_237

    #@211
    .line 199
    const-string v2, "BluetoothOppReceiver"

    #@213
    const-string v3, "Error: Can not get data from db"

    #@215
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@218
    goto/16 :goto_44

    #@21a
    .line 191
    .end local v27           #transInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    .end local v30           #uri:Landroid/net/Uri;
    :cond_21a
    const-string v2, "BluetoothOppReceiver"

    #@21c
    new-instance v3, Ljava/lang/StringBuilder;

    #@21e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@221
    const-string v4, "Receiver list for "

    #@223
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@226
    move-result-object v3

    #@227
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@22a
    move-result-object v4

    #@22b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v3

    #@22f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@232
    move-result-object v3

    #@233
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@236
    goto :goto_1fe

    #@237
    .line 203
    .restart local v27       #transInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    .restart local v30       #uri:Landroid/net/Uri;
    :cond_237
    move-object/from16 v0, v27

    #@239
    iget v2, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDirection:I

    #@23b
    const/4 v3, 0x1

    #@23c
    if-ne v2, v3, :cond_285

    #@23e
    move-object/from16 v0, v27

    #@240
    iget v2, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@242
    invoke-static {v2}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusSuccess(I)Z

    #@245
    move-result v2

    #@246
    if-eqz v2, :cond_285

    #@248
    .line 206
    move-object/from16 v0, v27

    #@24a
    iget-object v2, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@24c
    move-object/from16 v0, v27

    #@24e
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@250
    move-object/from16 v0, v27

    #@252
    iget-object v4, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mTimeStamp:Ljava/lang/Long;

    #@254
    move-object/from16 v0, p1

    #@256
    move-object/from16 v1, v30

    #@258
    invoke-static {v0, v2, v3, v4, v1}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->openReceivedFile(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;Landroid/net/Uri;)V

    #@25b
    .line 208
    move-object/from16 v0, p1

    #@25d
    move-object/from16 v1, v30

    #@25f
    invoke-static {v0, v1}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->updateVisibilityToHidden(Landroid/content/Context;Landroid/net/Uri;)V

    #@262
    .line 216
    :goto_262
    const-string v2, "notification"

    #@264
    move-object/from16 v0, p1

    #@266
    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@269
    move-result-object v18

    #@26a
    check-cast v18, Landroid/app/NotificationManager;

    #@26c
    .line 218
    .restart local v18       #notMgr:Landroid/app/NotificationManager;
    if-eqz v18, :cond_44

    #@26e
    .line 219
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@271
    move-result-object v2

    #@272
    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    #@275
    move-result-wide v2

    #@276
    long-to-int v2, v2

    #@277
    move-object/from16 v0, v18

    #@279
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    #@27c
    .line 221
    const-string v2, "BluetoothOppReceiver"

    #@27e
    const-string v3, "notMgr.cancel called"

    #@280
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@283
    goto/16 :goto_44

    #@285
    .line 210
    .end local v18           #notMgr:Landroid/app/NotificationManager;
    :cond_285
    new-instance v15, Landroid/content/Intent;

    #@287
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppTransferActivity;

    #@289
    move-object/from16 v0, p1

    #@28b
    invoke-direct {v15, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@28e
    .line 211
    .restart local v15       #in:Landroid/content/Intent;
    const/high16 v2, 0x1000

    #@290
    invoke-virtual {v15, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@293
    .line 212
    move-object/from16 v0, v30

    #@295
    invoke-virtual {v15, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@298
    .line 213
    move-object/from16 v0, p1

    #@29a
    invoke-virtual {v0, v15}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@29d
    goto :goto_262

    #@29e
    .line 224
    .end local v15           #in:Landroid/content/Intent;
    .end local v27           #transInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    .end local v30           #uri:Landroid/net/Uri;
    :cond_29e
    const-string v2, "android.btopp.intent.action.OPEN_OUTBOUND"

    #@2a0
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a3
    move-result v2

    #@2a4
    if-eqz v2, :cond_2c8

    #@2a6
    .line 226
    const-string v2, "BluetoothOppReceiver"

    #@2a8
    const-string v3, "Received ACTION_OPEN_OUTBOUND_TRANSFER."

    #@2aa
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2ad
    .line 229
    new-instance v15, Landroid/content/Intent;

    #@2af
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;

    #@2b1
    move-object/from16 v0, p1

    #@2b3
    invoke-direct {v15, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@2b6
    .line 230
    .restart local v15       #in:Landroid/content/Intent;
    const/high16 v2, 0x1400

    #@2b8
    invoke-virtual {v15, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@2bb
    .line 231
    const-string v2, "direction"

    #@2bd
    const/4 v3, 0x0

    #@2be
    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2c1
    .line 232
    move-object/from16 v0, p1

    #@2c3
    invoke-virtual {v0, v15}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@2c6
    goto/16 :goto_44

    #@2c8
    .line 233
    .end local v15           #in:Landroid/content/Intent;
    :cond_2c8
    const-string v2, "android.btopp.intent.action.OPEN_INBOUND"

    #@2ca
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2cd
    move-result v2

    #@2ce
    if-eqz v2, :cond_2f2

    #@2d0
    .line 235
    const-string v2, "BluetoothOppReceiver"

    #@2d2
    const-string v3, "Received ACTION_OPEN_INBOUND_TRANSFER."

    #@2d4
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2d7
    .line 238
    new-instance v15, Landroid/content/Intent;

    #@2d9
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;

    #@2db
    move-object/from16 v0, p1

    #@2dd
    invoke-direct {v15, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@2e0
    .line 239
    .restart local v15       #in:Landroid/content/Intent;
    const/high16 v2, 0x1400

    #@2e2
    invoke-virtual {v15, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@2e5
    .line 240
    const-string v2, "direction"

    #@2e7
    const/4 v3, 0x1

    #@2e8
    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2eb
    .line 241
    move-object/from16 v0, p1

    #@2ed
    invoke-virtual {v0, v15}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@2f0
    goto/16 :goto_44

    #@2f2
    .line 242
    .end local v15           #in:Landroid/content/Intent;
    :cond_2f2
    const-string v2, "android.btopp.intent.action.OPEN_RECEIVED_FILES"

    #@2f4
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2f7
    move-result v2

    #@2f8
    if-eqz v2, :cond_322

    #@2fa
    .line 244
    const-string v2, "BluetoothOppReceiver"

    #@2fc
    const-string v3, "Received ACTION_OPEN_RECEIVED_FILES."

    #@2fe
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@301
    .line 247
    new-instance v15, Landroid/content/Intent;

    #@303
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppTransferHistory;

    #@305
    move-object/from16 v0, p1

    #@307
    invoke-direct {v15, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@30a
    .line 248
    .restart local v15       #in:Landroid/content/Intent;
    const/high16 v2, 0x1400

    #@30c
    invoke-virtual {v15, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@30f
    .line 249
    const-string v2, "direction"

    #@311
    const/4 v3, 0x1

    #@312
    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@315
    .line 250
    const-string v2, "android.btopp.intent.extra.SHOW_ALL"

    #@317
    const/4 v3, 0x1

    #@318
    invoke-virtual {v15, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@31b
    .line 251
    move-object/from16 v0, p1

    #@31d
    invoke-virtual {v0, v15}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@320
    goto/16 :goto_44

    #@322
    .line 252
    .end local v15           #in:Landroid/content/Intent;
    :cond_322
    const-string v2, "android.btopp.intent.action.HIDE"

    #@324
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@327
    move-result v2

    #@328
    if-eqz v2, :cond_3b2

    #@32a
    .line 254
    const-string v2, "BluetoothOppReceiver"

    #@32c
    new-instance v3, Ljava/lang/StringBuilder;

    #@32e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@331
    const-string v4, "Receiver hide for "

    #@333
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@336
    move-result-object v3

    #@337
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@33a
    move-result-object v4

    #@33b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33e
    move-result-object v3

    #@33f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@342
    move-result-object v3

    #@343
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@346
    .line 256
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@349
    move-result-object v2

    #@34a
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@34d
    move-result-object v3

    #@34e
    const/4 v4, 0x0

    #@34f
    const/4 v5, 0x0

    #@350
    const/4 v6, 0x0

    #@351
    const/4 v7, 0x0

    #@352
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@355
    move-result-object v12

    #@356
    .line 258
    .local v12, cursor:Landroid/database/Cursor;
    if-eqz v12, :cond_44

    #@358
    .line 259
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    #@35b
    move-result v2

    #@35c
    if-eqz v2, :cond_3ad

    #@35e
    .line 260
    const-string v2, "status"

    #@360
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@363
    move-result v23

    #@364
    .line 261
    .local v23, statusColumn:I
    move/from16 v0, v23

    #@366
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    #@369
    move-result v22

    #@36a
    .line 262
    .local v22, status:I
    const-string v2, "visibility"

    #@36c
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@36f
    move-result v35

    #@370
    .line 263
    .local v35, visibilityColumn:I
    move/from16 v0, v35

    #@372
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    #@375
    move-result v34

    #@376
    .line 264
    .local v34, visibility:I
    const-string v2, "confirm"

    #@378
    invoke-interface {v12, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@37b
    move-result v32

    #@37c
    .line 266
    .local v32, userConfirmationColumn:I
    move/from16 v0, v32

    #@37e
    invoke-interface {v12, v0}, Landroid/database/Cursor;->getInt(I)I

    #@381
    move-result v31

    #@382
    .line 267
    .local v31, userConfirmation:I
    if-nez v31, :cond_3ad

    #@384
    if-nez v34, :cond_3ad

    #@386
    .line 269
    new-instance v33, Landroid/content/ContentValues;

    #@388
    invoke-direct/range {v33 .. v33}, Landroid/content/ContentValues;-><init>()V

    #@38b
    .line 270
    .local v33, values:Landroid/content/ContentValues;
    const-string v2, "visibility"

    #@38d
    const/4 v3, 0x1

    #@38e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@391
    move-result-object v3

    #@392
    move-object/from16 v0, v33

    #@394
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@397
    .line 271
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@39a
    move-result-object v2

    #@39b
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@39e
    move-result-object v3

    #@39f
    const/4 v4, 0x0

    #@3a0
    const/4 v5, 0x0

    #@3a1
    move-object/from16 v0, v33

    #@3a3
    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@3a6
    .line 273
    const-string v2, "BluetoothOppReceiver"

    #@3a8
    const-string v3, "Action_hide received and db updated"

    #@3aa
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3ad
    .line 277
    .end local v22           #status:I
    .end local v23           #statusColumn:I
    .end local v31           #userConfirmation:I
    .end local v32           #userConfirmationColumn:I
    .end local v33           #values:Landroid/content/ContentValues;
    .end local v34           #visibility:I
    .end local v35           #visibilityColumn:I
    :cond_3ad
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@3b0
    goto/16 :goto_44

    #@3b2
    .line 279
    .end local v12           #cursor:Landroid/database/Cursor;
    :cond_3b2
    const-string v2, "android.btopp.intent.action.HIDE_COMPLETE"

    #@3b4
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b7
    move-result v2

    #@3b8
    if-eqz v2, :cond_3e2

    #@3ba
    .line 281
    const-string v2, "BluetoothOppReceiver"

    #@3bc
    const-string v3, "Receiver ACTION_COMPLETE_HIDE"

    #@3be
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3c1
    .line 283
    new-instance v29, Landroid/content/ContentValues;

    #@3c3
    invoke-direct/range {v29 .. v29}, Landroid/content/ContentValues;-><init>()V

    #@3c6
    .line 284
    .local v29, updateValues:Landroid/content/ContentValues;
    const-string v2, "visibility"

    #@3c8
    const/4 v3, 0x1

    #@3c9
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3cc
    move-result-object v3

    #@3cd
    move-object/from16 v0, v29

    #@3cf
    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@3d2
    .line 285
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3d5
    move-result-object v2

    #@3d6
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@3d8
    const-string v4, "status >= \'200\' AND (visibility IS NULL OR visibility == \'0\') AND (confirm != \'5\')"

    #@3da
    const/4 v5, 0x0

    #@3db
    move-object/from16 v0, v29

    #@3dd
    invoke-virtual {v2, v3, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@3e0
    goto/16 :goto_44

    #@3e2
    .line 287
    .end local v29           #updateValues:Landroid/content/ContentValues;
    :cond_3e2
    const-string v2, "android.btopp.intent.action.TRANSFER_COMPLETE"

    #@3e4
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e7
    move-result v2

    #@3e8
    if-eqz v2, :cond_487

    #@3ea
    .line 289
    const-string v2, "BluetoothOppReceiver"

    #@3ec
    new-instance v3, Ljava/lang/StringBuilder;

    #@3ee
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3f1
    const-string v4, "Receiver Transfer Complete Intent for "

    #@3f3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f6
    move-result-object v3

    #@3f7
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@3fa
    move-result-object v4

    #@3fb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3fe
    move-result-object v3

    #@3ff
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@402
    move-result-object v3

    #@403
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@406
    .line 292
    const/16 v25, 0x0

    #@408
    .line 293
    .restart local v25       #toastMsg:Ljava/lang/String;
    new-instance v27, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@40a
    invoke-direct/range {v27 .. v27}, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;-><init>()V

    #@40d
    .line 294
    .restart local v27       #transInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@410
    move-result-object v2

    #@411
    move-object/from16 v0, p1

    #@413
    invoke-static {v0, v2}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->queryRecord(Landroid/content/Context;Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;

    #@416
    move-result-object v27

    #@417
    .line 295
    if-nez v27, :cond_422

    #@419
    .line 296
    const-string v2, "BluetoothOppReceiver"

    #@41b
    const-string v3, "Error: Can not get data from db"

    #@41d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@420
    goto/16 :goto_44

    #@422
    .line 300
    :cond_422
    move-object/from16 v0, v27

    #@424
    iget-boolean v2, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mHandoverInitiated:Z

    #@426
    if-eqz v2, :cond_44

    #@428
    .line 302
    new-instance v14, Landroid/content/Intent;

    #@42a
    const-string v2, "android.btopp.intent.action.BT_OPP_TRANSFER_DONE"

    #@42c
    invoke-direct {v14, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@42f
    .line 303
    .local v14, handoverIntent:Landroid/content/Intent;
    move-object/from16 v0, v27

    #@431
    iget v2, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDirection:I

    #@433
    const/4 v3, 0x1

    #@434
    if-ne v2, v3, :cond_479

    #@436
    .line 304
    const-string v2, "android.btopp.intent.extra.BT_OPP_TRANSFER_DIRECTION"

    #@438
    const/4 v3, 0x0

    #@439
    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@43c
    .line 310
    :goto_43c
    const-string v2, "android.btopp.intent.extra.BT_OPP_TRANSFER_ID"

    #@43e
    move-object/from16 v0, v27

    #@440
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mID:I

    #@442
    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@445
    .line 311
    const-string v2, "android.btopp.intent.extra.BT_OPP_ADDRESS"

    #@447
    move-object/from16 v0, v27

    #@449
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mDestAddr:Ljava/lang/String;

    #@44b
    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@44e
    .line 313
    move-object/from16 v0, v27

    #@450
    iget v2, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mStatus:I

    #@452
    invoke-static {v2}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusSuccess(I)Z

    #@455
    move-result v2

    #@456
    if-eqz v2, :cond_480

    #@458
    .line 314
    const-string v2, "android.btopp.intent.extra.BT_OPP_TRANSFER_STATUS"

    #@45a
    const/4 v3, 0x0

    #@45b
    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@45e
    .line 316
    const-string v2, "android.btopp.intent.extra.BT_OPP_TRANSFER_URI"

    #@460
    move-object/from16 v0, v27

    #@462
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileName:Ljava/lang/String;

    #@464
    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@467
    .line 318
    const-string v2, "android.btopp.intent.extra.BT_OPP_TRANSFER_MIMETYPE"

    #@469
    move-object/from16 v0, v27

    #@46b
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;->mFileType:Ljava/lang/String;

    #@46d
    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@470
    .line 324
    :goto_470
    const-string v2, "com.android.permission.HANDOVER_STATUS"

    #@472
    move-object/from16 v0, p1

    #@474
    invoke-virtual {v0, v14, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@477
    goto/16 :goto_44

    #@479
    .line 307
    :cond_479
    const-string v2, "android.btopp.intent.extra.BT_OPP_TRANSFER_DIRECTION"

    #@47b
    const/4 v3, 0x1

    #@47c
    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@47f
    goto :goto_43c

    #@480
    .line 321
    :cond_480
    const-string v2, "android.btopp.intent.extra.BT_OPP_TRANSFER_STATUS"

    #@482
    const/4 v3, 0x1

    #@483
    invoke-virtual {v14, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@486
    goto :goto_470

    #@487
    .line 355
    .end local v14           #handoverIntent:Landroid/content/Intent;
    .end local v25           #toastMsg:Ljava/lang/String;
    .end local v27           #transInfo:Lcom/android/bluetooth/opp/BluetoothOppTransferInfo;
    :cond_487
    const-string v2, "com.lge.btopp.intent.action.UPDATE_TRANSFER_COUNT"

    #@489
    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@48c
    move-result v2

    #@48d
    if-eqz v2, :cond_44

    #@48f
    .line 356
    const/16 v25, 0x0

    #@491
    .line 357
    .restart local v25       #toastMsg:Ljava/lang/String;
    const-string v2, "trasnfer_type"

    #@493
    move-object/from16 v0, p2

    #@495
    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@498
    move-result-object v28

    #@499
    .line 358
    .local v28, trasnfer_type:Ljava/lang/String;
    const-string v2, "total_count"

    #@49b
    const/4 v3, 0x0

    #@49c
    move-object/from16 v0, p2

    #@49e
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@4a1
    move-result v26

    #@4a2
    .line 359
    .local v26, total_count:I
    const-string v2, "file_count"

    #@4a4
    const/4 v3, 0x0

    #@4a5
    move-object/from16 v0, p2

    #@4a7
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@4aa
    move-result v11

    #@4ab
    .line 360
    .local v11, count:I
    const-string v2, "result_status"

    #@4ad
    const/4 v3, 0x0

    #@4ae
    move-object/from16 v0, p2

    #@4b0
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@4b3
    move-result v20

    #@4b4
    .line 362
    .local v20, result_status:I
    const-string v2, "BluetoothOppReceiver"

    #@4b6
    new-instance v3, Ljava/lang/StringBuilder;

    #@4b8
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4bb
    const-string v4, "UPDATE_TRANSFER_COUNT_ACTION total_count : "

    #@4bd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c0
    move-result-object v3

    #@4c1
    move/from16 v0, v26

    #@4c3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c6
    move-result-object v3

    #@4c7
    const-string v4, " count: "

    #@4c9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4cc
    move-result-object v3

    #@4cd
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d0
    move-result-object v3

    #@4d1
    const-string v4, " result_status : "

    #@4d3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d6
    move-result-object v3

    #@4d7
    move/from16 v0, v20

    #@4d9
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4dc
    move-result-object v3

    #@4dd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e0
    move-result-object v3

    #@4e1
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4e4
    .line 363
    if-nez v28, :cond_4ef

    #@4e6
    .line 364
    const-string v2, "BluetoothOppReceiver"

    #@4e8
    const-string v3, "[BTUI] trasnfer_type is null"

    #@4ea
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4ed
    goto/16 :goto_44

    #@4ef
    .line 367
    :cond_4ef
    const-string v2, "INBOUND_TRANSFER"

    #@4f1
    move-object/from16 v0, v28

    #@4f3
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4f6
    move-result v2

    #@4f7
    if-eqz v2, :cond_566

    #@4f9
    .line 368
    const/4 v2, 0x1

    #@4fa
    if-ne v11, v2, :cond_535

    #@4fc
    const/16 v2, 0xc8

    #@4fe
    move/from16 v0, v20

    #@500
    if-ne v0, v2, :cond_535

    #@502
    .line 369
    const v2, 0x7f070072

    #@505
    move-object/from16 v0, p1

    #@507
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@50a
    move-result-object v25

    #@50b
    .line 398
    :cond_50b
    :goto_50b
    if-eqz v25, :cond_44

    #@50d
    .line 399
    const-string v2, "BluetoothOppReceiver"

    #@50f
    new-instance v3, Ljava/lang/StringBuilder;

    #@511
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@514
    const-string v4, "UPDATE_TRANSFER_COUNT_ACTION : "

    #@516
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@519
    move-result-object v3

    #@51a
    move-object/from16 v0, v25

    #@51c
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51f
    move-result-object v3

    #@520
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@523
    move-result-object v3

    #@524
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@527
    .line 400
    const/4 v2, 0x0

    #@528
    move-object/from16 v0, p1

    #@52a
    move-object/from16 v1, v25

    #@52c
    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@52f
    move-result-object v2

    #@530
    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    #@533
    goto/16 :goto_44

    #@535
    .line 370
    :cond_535
    const/4 v2, 0x1

    #@536
    if-lt v11, v2, :cond_55c

    #@538
    .line 371
    const/16 v2, 0xc8

    #@53a
    move/from16 v0, v20

    #@53c
    if-ne v0, v2, :cond_552

    #@53e
    .line 372
    const v2, 0x7f070073

    #@541
    const/4 v3, 0x1

    #@542
    new-array v3, v3, [Ljava/lang/Object;

    #@544
    const/4 v4, 0x0

    #@545
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@548
    move-result-object v5

    #@549
    aput-object v5, v3, v4

    #@54b
    move-object/from16 v0, p1

    #@54d
    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@550
    move-result-object v25

    #@551
    goto :goto_50b

    #@552
    .line 375
    :cond_552
    const v2, 0x7f070074

    #@555
    move-object/from16 v0, p1

    #@557
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@55a
    move-result-object v25

    #@55b
    goto :goto_50b

    #@55c
    .line 378
    :cond_55c
    const v2, 0x7f070029

    #@55f
    move-object/from16 v0, p1

    #@561
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@564
    move-result-object v25

    #@565
    goto :goto_50b

    #@566
    .line 381
    :cond_566
    const/16 v2, 0xc8

    #@568
    move/from16 v0, v20

    #@56a
    if-ne v0, v2, :cond_591

    #@56c
    .line 382
    const/4 v2, 0x1

    #@56d
    if-ne v11, v2, :cond_579

    #@56f
    .line 383
    const v2, 0x7f070032

    #@572
    move-object/from16 v0, p1

    #@574
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@577
    move-result-object v25

    #@578
    goto :goto_50b

    #@579
    .line 384
    :cond_579
    const/4 v2, 0x1

    #@57a
    if-le v11, v2, :cond_50b

    #@57c
    .line 385
    const v2, 0x7f070075

    #@57f
    const/4 v3, 0x1

    #@580
    new-array v3, v3, [Ljava/lang/Object;

    #@582
    const/4 v4, 0x0

    #@583
    invoke-static {v11}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@586
    move-result-object v5

    #@587
    aput-object v5, v3, v4

    #@589
    move-object/from16 v0, p1

    #@58b
    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@58e
    move-result-object v25

    #@58f
    goto/16 :goto_50b

    #@591
    .line 388
    :cond_591
    if-ltz v11, :cond_50b

    #@593
    .line 389
    sub-int v10, v26, v11

    #@595
    .line 390
    .local v10, cnt_not_send_file:I
    const/4 v2, 0x1

    #@596
    if-gt v10, v2, :cond_5a3

    #@598
    .line 391
    const v2, 0x7f070076

    #@59b
    move-object/from16 v0, p1

    #@59d
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5a0
    move-result-object v25

    #@5a1
    goto/16 :goto_50b

    #@5a3
    .line 393
    :cond_5a3
    const v2, 0x7f070077

    #@5a6
    const/4 v3, 0x1

    #@5a7
    new-array v3, v3, [Ljava/lang/Object;

    #@5a9
    const/4 v4, 0x0

    #@5aa
    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@5ad
    move-result-object v5

    #@5ae
    aput-object v5, v3, v4

    #@5b0
    move-object/from16 v0, p1

    #@5b2
    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@5b5
    move-result-object v25

    #@5b6
    goto/16 :goto_50b
.end method
