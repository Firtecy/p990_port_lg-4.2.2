.class public Lcom/android/bluetooth/opp/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final ACCEPTABLE_SHARE_INBOUND_TYPES:[Ljava/lang/String; = null

.field public static final ACCEPTABLE_SHARE_OUTBOUND_TYPES:[Ljava/lang/String; = null

.field public static final ACTION_BT_OPP_TRANSFER_DONE:Ljava/lang/String; = "android.btopp.intent.action.BT_OPP_TRANSFER_DONE"

.field public static final ACTION_BT_OPP_TRANSFER_PROGRESS:Ljava/lang/String; = "android.btopp.intent.action.BT_OPP_TRANSFER_PROGRESS"

.field public static final ACTION_COMPLETE_HIDE:Ljava/lang/String; = "android.btopp.intent.action.HIDE_COMPLETE"

.field public static final ACTION_HANDOVER_SEND:Ljava/lang/String; = "android.btopp.intent.action.HANDOVER_SEND"

.field public static final ACTION_HANDOVER_SEND_MULTIPLE:Ljava/lang/String; = "android.btopp.intent.action.HANDOVER_SEND_MULTIPLE"

.field public static final ACTION_HIDE:Ljava/lang/String; = "android.btopp.intent.action.HIDE"

.field public static final ACTION_INCOMING_FILE_CONFIRM:Ljava/lang/String; = "android.btopp.intent.action.CONFIRM"

.field public static final ACTION_LIST:Ljava/lang/String; = "android.btopp.intent.action.LIST"

.field public static final ACTION_OPEN:Ljava/lang/String; = "android.btopp.intent.action.OPEN"

.field public static final ACTION_OPEN_INBOUND_TRANSFER:Ljava/lang/String; = "android.btopp.intent.action.OPEN_INBOUND"

.field public static final ACTION_OPEN_OUTBOUND_TRANSFER:Ljava/lang/String; = "android.btopp.intent.action.OPEN_OUTBOUND"

.field public static final ACTION_OPEN_RECEIVED_FILES:Ljava/lang/String; = "android.btopp.intent.action.OPEN_RECEIVED_FILES"

.field public static final ACTION_RETRY:Ljava/lang/String; = "android.btopp.intent.action.RETRY"

.field public static final ACTION_STOP_HANDOVER:Ljava/lang/String; = "android.btopp.intent.action.STOP_HANDOVER_TRANSFER"

.field public static final ACTION_WHITELIST_DEVICE:Ljava/lang/String; = "android.btopp.intent.action.WHITELIST_DEVICE"

.field public static final BATCH_STATUS_FAILED:I = 0x3

.field public static final BATCH_STATUS_FINISHED:I = 0x2

.field public static final BATCH_STATUS_PENDING:I = 0x0

.field public static final BATCH_STATUS_RUNNING:I = 0x1

.field public static final BLUETOOTHOPP_CHANNEL_PREFERENCE:Ljava/lang/String; = "btopp_channels"

.field public static final BLUETOOTHOPP_NAME_PREFERENCE:Ljava/lang/String; = "btopp_names"

.field public static final DEBUG:Z = true

.field public static final DEFAULT_STORE_SUBDIR:Ljava/lang/String; = "/Bluetooth"

.field public static final DIRECTION_BLUETOOTH_INCOMING:I = 0x0

.field public static final DIRECTION_BLUETOOTH_OUTGOING:I = 0x1

.field public static final EXTRA_BT_OPP_ADDRESS:Ljava/lang/String; = "android.btopp.intent.extra.BT_OPP_ADDRESS"

.field public static final EXTRA_BT_OPP_TRANSFER_DIRECTION:Ljava/lang/String; = "android.btopp.intent.extra.BT_OPP_TRANSFER_DIRECTION"

.field public static final EXTRA_BT_OPP_TRANSFER_ID:Ljava/lang/String; = "android.btopp.intent.extra.BT_OPP_TRANSFER_ID"

.field public static final EXTRA_BT_OPP_TRANSFER_MIMETYPE:Ljava/lang/String; = "android.btopp.intent.extra.BT_OPP_TRANSFER_MIMETYPE"

.field public static final EXTRA_BT_OPP_TRANSFER_PROGRESS:Ljava/lang/String; = "android.btopp.intent.extra.BT_OPP_TRANSFER_PROGRESS"

.field public static final EXTRA_BT_OPP_TRANSFER_STATUS:Ljava/lang/String; = "android.btopp.intent.extra.BT_OPP_TRANSFER_STATUS"

.field public static final EXTRA_BT_OPP_TRANSFER_URI:Ljava/lang/String; = "android.btopp.intent.extra.BT_OPP_TRANSFER_URI"

.field public static final EXTRA_CONNECTION_HANDOVER:Ljava/lang/String; = "com.android.intent.extra.CONNECTION_HANDOVER"

.field public static final EXTRA_SHOW_ALL_FILES:Ljava/lang/String; = "android.btopp.intent.extra.SHOW_ALL"

.field public static final HANDOVER_STATUS_PERMISSION:Ljava/lang/String; = "com.android.permission.HANDOVER_STATUS"

.field public static final HANDOVER_TRANSFER_STATUS_FAILURE:I = 0x1

.field public static final HANDOVER_TRANSFER_STATUS_SUCCESS:I = 0x0

.field public static final MAX_RECORDS_IN_DATABASE:I = 0x3e8

.field public static final MEDIA_SCANNED:Ljava/lang/String; = "scanned"

.field public static final MEDIA_SCANNED_NOT_SCANNED:I = 0x0

.field public static final MEDIA_SCANNED_SCANNED_FAILED:I = 0x2

.field public static final MEDIA_SCANNED_SCANNED_OK:I = 0x1

.field public static final TAG:Ljava/lang/String; = "BluetoothOpp"

.field public static final TCP_DEBUG_PORT:I = 0x1964

.field public static final THIS_PACKAGE_NAME:Ljava/lang/String; = "com.android.bluetooth"

.field public static final UNACCEPTABLE_SHARE_INBOUND_TYPES:[Ljava/lang/String; = null

.field public static final UNACCEPTABLE_SHARE_OUTBOUND_TYPES:[Ljava/lang/String; = null

.field public static final USE_EMULATOR_DEBUG:Z = false

.field public static final USE_TCP_DEBUG:Z = false

.field public static final USE_TCP_SIMPLE_SERVER:Z = false

.field public static final VERBOSE:Z = true

.field public static filename_SEQUENCE_SEPARATOR:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 184
    new-array v0, v4, [Ljava/lang/String;

    #@5
    const-string v1, "image/*"

    #@7
    aput-object v1, v0, v3

    #@9
    const-string v1, "text/x-vcard"

    #@b
    aput-object v1, v0, v2

    #@d
    sput-object v0, Lcom/android/bluetooth/opp/Constants;->ACCEPTABLE_SHARE_OUTBOUND_TYPES:[Ljava/lang/String;

    #@f
    .line 192
    new-array v0, v2, [Ljava/lang/String;

    #@11
    const-string v1, "virus/*"

    #@13
    aput-object v1, v0, v3

    #@15
    sput-object v0, Lcom/android/bluetooth/opp/Constants;->UNACCEPTABLE_SHARE_OUTBOUND_TYPES:[Ljava/lang/String;

    #@17
    .line 201
    const/16 v0, 0xc

    #@19
    new-array v0, v0, [Ljava/lang/String;

    #@1b
    const-string v1, "image/*"

    #@1d
    aput-object v1, v0, v3

    #@1f
    const-string v1, "video/*"

    #@21
    aput-object v1, v0, v2

    #@23
    const-string v1, "audio/*"

    #@25
    aput-object v1, v0, v4

    #@27
    const/4 v1, 0x3

    #@28
    const-string v2, "text/x-vcard"

    #@2a
    aput-object v2, v0, v1

    #@2c
    const/4 v1, 0x4

    #@2d
    const-string v2, "text/plain"

    #@2f
    aput-object v2, v0, v1

    #@31
    const/4 v1, 0x5

    #@32
    const-string v2, "text/html"

    #@34
    aput-object v2, v0, v1

    #@36
    const/4 v1, 0x6

    #@37
    const-string v2, "application/zip"

    #@39
    aput-object v2, v0, v1

    #@3b
    const/4 v1, 0x7

    #@3c
    const-string v2, "application/vnd.ms-excel"

    #@3e
    aput-object v2, v0, v1

    #@40
    const/16 v1, 0x8

    #@42
    const-string v2, "application/msword"

    #@44
    aput-object v2, v0, v1

    #@46
    const/16 v1, 0x9

    #@48
    const-string v2, "application/vnd.ms-powerpoint"

    #@4a
    aput-object v2, v0, v1

    #@4c
    const/16 v1, 0xa

    #@4e
    const-string v2, "application/pdf"

    #@50
    aput-object v2, v0, v1

    #@52
    const/16 v1, 0xb

    #@54
    const-string v2, "text/x-vcalendar"

    #@56
    aput-object v2, v0, v1

    #@58
    sput-object v0, Lcom/android/bluetooth/opp/Constants;->ACCEPTABLE_SHARE_INBOUND_TYPES:[Ljava/lang/String;

    #@5a
    .line 222
    new-array v0, v3, [Ljava/lang/String;

    #@5c
    sput-object v0, Lcom/android/bluetooth/opp/Constants;->UNACCEPTABLE_SHARE_INBOUND_TYPES:[Ljava/lang/String;

    #@5e
    .line 273
    const-string v0, "-"

    #@60
    sput-object v0, Lcom/android/bluetooth/opp/Constants;->filename_SEQUENCE_SEPARATOR:Ljava/lang/String;

    #@62
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static logHeader(Ljavax/obex/HeaderSet;)V
    .registers 5
    .parameter "hs"

    #@0
    .prologue
    .line 328
    const-string v1, "BluetoothOpp"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Dumping HeaderSet "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 331
    :try_start_1c
    const-string v1, "BluetoothOpp"

    #@1e
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "COUNT : "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const/16 v3, 0xc0

    #@2b
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 332
    const-string v1, "BluetoothOpp"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "NAME : "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    const/4 v3, 0x1

    #@48
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 333
    const-string v1, "BluetoothOpp"

    #@59
    new-instance v2, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v3, "TYPE : "

    #@60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v2

    #@64
    const/16 v3, 0x42

    #@66
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v2

    #@6e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v2

    #@72
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 334
    const-string v1, "BluetoothOpp"

    #@77
    new-instance v2, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v3, "LENGTH : "

    #@7e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v2

    #@82
    const/16 v3, 0xc3

    #@84
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@87
    move-result-object v3

    #@88
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v2

    #@8c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v2

    #@90
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 335
    const-string v1, "BluetoothOpp"

    #@95
    new-instance v2, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v3, "TIME_ISO_8601 : "

    #@9c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v2

    #@a0
    const/16 v3, 0x44

    #@a2
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@a5
    move-result-object v3

    #@a6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v2

    #@aa
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v2

    #@ae
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b1
    .line 336
    const-string v1, "BluetoothOpp"

    #@b3
    new-instance v2, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v3, "TIME_4_BYTE : "

    #@ba
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v2

    #@be
    const/16 v3, 0xc4

    #@c0
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@c3
    move-result-object v3

    #@c4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v2

    #@c8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v2

    #@cc
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    .line 337
    const-string v1, "BluetoothOpp"

    #@d1
    new-instance v2, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    const-string v3, "DESCRIPTION : "

    #@d8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v2

    #@dc
    const/4 v3, 0x5

    #@dd
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@e0
    move-result-object v3

    #@e1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v2

    #@e5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v2

    #@e9
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ec
    .line 338
    const-string v1, "BluetoothOpp"

    #@ee
    new-instance v2, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string v3, "TARGET : "

    #@f5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v2

    #@f9
    const/16 v3, 0x46

    #@fb
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@fe
    move-result-object v3

    #@ff
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v2

    #@103
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v2

    #@107
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    .line 339
    const-string v1, "BluetoothOpp"

    #@10c
    new-instance v2, Ljava/lang/StringBuilder;

    #@10e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@111
    const-string v3, "HTTP : "

    #@113
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v2

    #@117
    const/16 v3, 0x47

    #@119
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@11c
    move-result-object v3

    #@11d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v2

    #@121
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v2

    #@125
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@128
    .line 340
    const-string v1, "BluetoothOpp"

    #@12a
    new-instance v2, Ljava/lang/StringBuilder;

    #@12c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12f
    const-string v3, "WHO : "

    #@131
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v2

    #@135
    const/16 v3, 0x4a

    #@137
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@13a
    move-result-object v3

    #@13b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v2

    #@13f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@142
    move-result-object v2

    #@143
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    .line 341
    const-string v1, "BluetoothOpp"

    #@148
    new-instance v2, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    const-string v3, "OBJECT_CLASS : "

    #@14f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v2

    #@153
    const/16 v3, 0x4f

    #@155
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@158
    move-result-object v3

    #@159
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v2

    #@15d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v2

    #@161
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@164
    .line 342
    const-string v1, "BluetoothOpp"

    #@166
    new-instance v2, Ljava/lang/StringBuilder;

    #@168
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16b
    const-string v3, "APPLICATION_PARAMETER : "

    #@16d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v2

    #@171
    const/16 v3, 0x4c

    #@173
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@176
    move-result-object v3

    #@177
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v2

    #@17b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v2

    #@17f
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_182
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_182} :catch_183

    #@182
    .line 346
    :goto_182
    return-void

    #@183
    .line 343
    :catch_183
    move-exception v0

    #@184
    .line 344
    .local v0, e:Ljava/io/IOException;
    const-string v1, "BluetoothOpp"

    #@186
    new-instance v2, Ljava/lang/StringBuilder;

    #@188
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18b
    const-string v3, "dump HeaderSet error "

    #@18d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    move-result-object v2

    #@191
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v2

    #@195
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v2

    #@199
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19c
    goto :goto_182
.end method

.method public static mimeTypeMatches(Ljava/lang/String;Ljava/lang/String;)Z
    .registers 5
    .parameter "mimeType"
    .parameter "matchAgainst"

    #@0
    .prologue
    .line 322
    const-string v1, "\\*"

    #@2
    const-string v2, "\\.\\*"

    #@4
    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v1

    #@8
    const/4 v2, 0x2

    #@9
    invoke-static {v1, v2}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    #@c
    move-result-object v0

    #@d
    .line 324
    .local v0, p:Ljava/util/regex/Pattern;
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    #@14
    move-result v1

    #@15
    return v1
.end method

.method public static mimeTypeMatches(Ljava/lang/String;[Ljava/lang/String;)Z
    .registers 7
    .parameter "mimeType"
    .parameter "matchAgainst"

    #@0
    .prologue
    .line 313
    move-object v0, p1

    #@1
    .local v0, arr$:[Ljava/lang/String;
    array-length v2, v0

    #@2
    .local v2, len$:I
    const/4 v1, 0x0

    #@3
    .local v1, i$:I
    :goto_3
    if-ge v1, v2, :cond_12

    #@5
    aget-object v3, v0, v1

    #@7
    .line 314
    .local v3, matchType:Ljava/lang/String;
    invoke-static {p0, v3}, Lcom/android/bluetooth/opp/Constants;->mimeTypeMatches(Ljava/lang/String;Ljava/lang/String;)Z

    #@a
    move-result v4

    #@b
    if-eqz v4, :cond_f

    #@d
    .line 315
    const/4 v4, 0x1

    #@e
    .line 318
    .end local v3           #matchType:Ljava/lang/String;
    :goto_e
    return v4

    #@f
    .line 313
    .restart local v3       #matchType:Ljava/lang/String;
    :cond_f
    add-int/lit8 v1, v1, 0x1

    #@11
    goto :goto_3

    #@12
    .line 318
    .end local v3           #matchType:Ljava/lang/String;
    :cond_12
    const/4 v4, 0x0

    #@13
    goto :goto_e
.end method

.method public static sendIntentIfCompleted(Landroid/content/Context;Landroid/net/Uri;I)V
    .registers 6
    .parameter "context"
    .parameter "contentUri"
    .parameter "status"

    #@0
    .prologue
    .line 288
    invoke-static {p2}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_1e

    #@6
    .line 289
    new-instance v0, Landroid/content/Intent;

    #@8
    const-string v1, "android.btopp.intent.action.TRANSFER_COMPLETE"

    #@a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d
    .line 290
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.bluetooth"

    #@f
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@11
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@18
    .line 291
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    #@1b
    .line 292
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1e
    .line 294
    .end local v0           #intent:Landroid/content/Intent;
    :cond_1e
    return-void
.end method

.method public static updateShareStatus(Landroid/content/Context;II)V
    .registers 8
    .parameter "context"
    .parameter "id"
    .parameter "status"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 276
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    const-string v3, "/"

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1d
    move-result-object v0

    #@1e
    .line 277
    .local v0, contentUri:Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    #@20
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@23
    .line 278
    .local v1, updateValues:Landroid/content/ContentValues;
    const-string v2, "status"

    #@25
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2c
    .line 279
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@33
    .line 280
    invoke-static {p0, v0, p2}, Lcom/android/bluetooth/opp/Constants;->sendIntentIfCompleted(Landroid/content/Context;Landroid/net/Uri;I)V

    #@36
    .line 281
    return-void
.end method

.method public static updateTransferStatusForMessage(Landroid/content/Context;Ljava/lang/String;III)V
    .registers 8
    .parameter "context"
    .parameter "trasnfer_type"
    .parameter "total_count"
    .parameter "count"
    .parameter "result_status"

    #@0
    .prologue
    .line 302
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.lge.btopp.intent.action.UPDATE_TRANSFER_COUNT"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 303
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.bluetooth"

    #@9
    const-class v2, Lcom/android/bluetooth/opp/BluetoothOppReceiver;

    #@b
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@12
    .line 304
    const-string v1, "trasnfer_type"

    #@14
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@17
    .line 305
    const-string v1, "total_count"

    #@19
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1c
    .line 306
    const-string v1, "file_count"

    #@1e
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@21
    .line 307
    const-string v1, "result_status"

    #@23
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@26
    .line 308
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@29
    .line 309
    return-void
.end method
