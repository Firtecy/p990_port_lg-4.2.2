.class public Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;
.super Ljava/lang/Object;
.source "BluetoothOppObexClientSession.java"

# interfaces
.implements Lcom/android/bluetooth/opp/BluetoothOppObexSession;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;
    }
.end annotation


# static fields
.field private static final D:Z = true

.field private static final TAG:Ljava/lang/String; = "BtOppObexClient"

.field private static final V:Z = true


# instance fields
.field private mCallback:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private volatile mInterrupted:Z

.field private mThread:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;

.field private mTransport:Ljavax/obex/ObexTransport;

.field private volatile mWaitingForRemote:Z

.field private result_status:I

.field private send_file_cnt:I

.field private sent_file_cnt:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljavax/obex/ObexTransport;)V
    .registers 5
    .parameter "context"
    .parameter "transport"

    #@0
    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 88
    if-nez p2, :cond_d

    #@5
    .line 89
    new-instance v0, Ljava/lang/NullPointerException;

    #@7
    const-string v1, "transport is null"

    #@9
    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    #@c
    throw v0

    #@d
    .line 91
    :cond_d
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mContext:Landroid/content/Context;

    #@f
    .line 92
    iput-object p2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mTransport:Ljavax/obex/ObexTransport;

    #@11
    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mWaitingForRemote:Z

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mInterrupted:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mInterrupted:Z

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->send_file_cnt:I

    #@2
    return v0
.end method

.method static synthetic access$500(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->sent_file_cnt:I

    #@2
    return v0
.end method

.method static synthetic access$508(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)I
    .registers 3
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->sent_file_cnt:I

    #@2
    add-int/lit8 v1, v0, 0x1

    #@4
    iput v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->sent_file_cnt:I

    #@6
    return v0
.end method

.method static synthetic access$600(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 63
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->result_status:I

    #@2
    return v0
.end method

.method static synthetic access$602(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 63
    iput p1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->result_status:I

    #@2
    return p1
.end method

.method static synthetic access$700(Ljava/io/InputStream;[BI)I
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 63
    invoke-static {p0, p1, p2}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->readFully(Ljava/io/InputStream;[BI)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public static applyRemoteDeviceQuirks(Ljavax/obex/HeaderSet;Ljava/lang/String;Ljava/lang/String;)V
    .registers 11
    .parameter "request"
    .parameter "address"
    .parameter "filename"

    #@0
    .prologue
    .line 664
    if-nez p1, :cond_3

    #@2
    .line 692
    :cond_2
    :goto_2
    return-void

    #@3
    .line 667
    :cond_3
    const-string v5, "00:04:48"

    #@5
    invoke-virtual {p1, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@8
    move-result v5

    #@9
    if-eqz v5, :cond_2

    #@b
    .line 672
    invoke-virtual {p2}, Ljava/lang/String;->toCharArray()[C

    #@e
    move-result-object v0

    #@f
    .line 673
    .local v0, c:[C
    const/4 v1, 0x1

    #@10
    .line 674
    .local v1, firstDot:Z
    const/4 v3, 0x0

    #@11
    .line 675
    .local v3, modified:Z
    array-length v5, v0

    #@12
    add-int/lit8 v2, v5, -0x1

    #@14
    .local v2, i:I
    :goto_14
    if-ltz v2, :cond_27

    #@16
    .line 676
    aget-char v5, v0, v2

    #@18
    const/16 v6, 0x2e

    #@1a
    if-ne v5, v6, :cond_24

    #@1c
    .line 677
    if-nez v1, :cond_23

    #@1e
    .line 678
    const/4 v3, 0x1

    #@1f
    .line 679
    const/16 v5, 0x5f

    #@21
    aput-char v5, v0, v2

    #@23
    .line 681
    :cond_23
    const/4 v1, 0x0

    #@24
    .line 675
    :cond_24
    add-int/lit8 v2, v2, -0x1

    #@26
    goto :goto_14

    #@27
    .line 685
    :cond_27
    if-eqz v3, :cond_2

    #@29
    .line 686
    new-instance v4, Ljava/lang/String;

    #@2b
    invoke-direct {v4, v0}, Ljava/lang/String;-><init>([C)V

    #@2e
    .line 687
    .local v4, newFilename:Ljava/lang/String;
    const/4 v5, 0x1

    #@2f
    invoke-virtual {p0, v5, v4}, Ljavax/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    #@32
    .line 688
    const-string v5, "BtOppObexClient"

    #@34
    new-instance v6, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v7, "Sending file \""

    #@3b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    const-string v7, "\" as \""

    #@45
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v6

    #@49
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    const-string v7, "\" to workaround Poloroid filename quirk"

    #@4f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    goto :goto_2
.end method

.method private static readFully(Ljava/io/InputStream;[BI)I
    .registers 6
    .parameter "is"
    .parameter "buffer"
    .parameter "size"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 137
    const/4 v0, 0x0

    #@1
    .line 138
    .local v0, done:I
    :goto_1
    if-ge v0, p2, :cond_b

    #@3
    .line 139
    sub-int v2, p2, v0

    #@5
    invoke-virtual {p0, p1, v0, v2}, Ljava/io/InputStream;->read([BII)I

    #@8
    move-result v1

    #@9
    .line 140
    .local v1, got:I
    if-gtz v1, :cond_c

    #@b
    .line 145
    .end local v1           #got:I
    :cond_b
    return v0

    #@c
    .line 143
    .restart local v1       #got:I
    :cond_c
    add-int/2addr v0, v1

    #@d
    .line 144
    goto :goto_1
.end method


# virtual methods
.method public addShare(Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V
    .registers 3
    .parameter "share"

    #@0
    .prologue
    .line 133
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mThread:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->addShare(Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V

    #@5
    .line 134
    return-void
.end method

.method public start(Landroid/os/Handler;)V
    .registers 6
    .parameter "handler"

    #@0
    .prologue
    .line 97
    const-string v1, "BtOppObexClient"

    #@2
    const-string v2, "Start!"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 100
    const/4 v1, 0x0

    #@8
    iput v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->sent_file_cnt:I

    #@a
    .line 101
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mContext:Landroid/content/Context;

    #@c
    invoke-static {v1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@f
    move-result-object v0

    #@10
    .line 102
    .local v0, mOppManager:Lcom/android/bluetooth/opp/BluetoothOppManager;
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getBatchSize()I

    #@13
    move-result v1

    #@14
    iput v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->send_file_cnt:I

    #@16
    .line 103
    const/16 v1, 0x1eb

    #@18
    iput v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->result_status:I

    #@1a
    .line 105
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;

    #@1c
    .line 106
    new-instance v1, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;

    #@1e
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mContext:Landroid/content/Context;

    #@20
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mTransport:Ljavax/obex/ObexTransport;

    #@22
    invoke-direct {v1, p0, v2, v3}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;-><init>(Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;Landroid/content/Context;Ljavax/obex/ObexTransport;)V

    #@25
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mThread:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;

    #@27
    .line 107
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mThread:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;

    #@29
    invoke-virtual {v1}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->start()V

    #@2c
    .line 108
    return-void
.end method

.method public stop()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 112
    const-string v1, "BtOppObexClient"

    #@3
    const-string v2, "Stop!"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 114
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mThread:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;

    #@a
    if-eqz v1, :cond_23

    #@c
    .line 115
    const/4 v1, 0x1

    #@d
    iput-boolean v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mInterrupted:Z

    #@f
    .line 117
    :try_start_f
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mThread:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;

    #@11
    invoke-virtual {v1}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->interrupt()V

    #@14
    .line 119
    const-string v1, "BtOppObexClient"

    #@16
    const-string v2, "waiting for thread to terminate"

    #@18
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 121
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mThread:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;

    #@1d
    invoke-virtual {v1}, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;->join()V

    #@20
    .line 122
    const/4 v1, 0x0

    #@21
    iput-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mThread:Lcom/android/bluetooth/opp/BluetoothOppObexClientSession$ClientThread;
    :try_end_23
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_23} :catch_26

    #@23
    .line 129
    :cond_23
    :goto_23
    iput-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppObexClientSession;->mCallback:Landroid/os/Handler;

    #@25
    .line 130
    return-void

    #@26
    .line 123
    :catch_26
    move-exception v0

    #@27
    .line 125
    .local v0, e:Ljava/lang/InterruptedException;
    const-string v1, "BtOppObexClient"

    #@29
    const-string v2, "Interrupted waiting for thread to join"

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    goto :goto_23
.end method

.method public unblock()V
    .registers 1

    #@0
    .prologue
    .line 696
    return-void
.end method
