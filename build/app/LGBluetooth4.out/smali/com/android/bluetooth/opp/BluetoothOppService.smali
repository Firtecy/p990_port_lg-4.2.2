.class public Lcom/android/bluetooth/opp/BluetoothOppService;
.super Landroid/app/Service;
.source "BluetoothOppService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;,
        Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;,
        Lcom/android/bluetooth/opp/BluetoothOppService$BluetoothShareContentObserver;
    }
.end annotation


# static fields
.field private static final D:Z = true

.field private static final MEDIA_SCANNED:I = 0x2

.field private static final MEDIA_SCANNED_FAILED:I = 0x3

.field private static final MSG_INCOMING_CONNECTION_RETRY:I = 0x4

.field private static final START_LISTENER:I = 0x1

.field private static final STOP_LISTENER:I = 0xc8

.field private static final TAG:Ljava/lang/String; = "BtOppService"

.field private static final V:Z = true

.field static mState:I


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mBatchId:I

.field private mBatchs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/bluetooth/opp/BluetoothOppBatch;",
            ">;"
        }
    .end annotation
.end field

.field private final mBluetoothReceiver:Landroid/content/BroadcastReceiver;

.field private mHandler:Landroid/os/Handler;

.field private mIncomingRetries:I

.field private mListenStarted:Z

.field private mMediaScanInProgress:Z

.field private mNewChars:Landroid/database/CharArrayBuffer;

.field private mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;

.field private mObserver:Lcom/android/bluetooth/opp/BluetoothOppService$BluetoothShareContentObserver;

.field private mOldChars:Landroid/database/CharArrayBuffer;

.field private mPendingConnection:Ljavax/obex/ObexTransport;

.field private mPendingUpdate:Z

.field private mPowerManager:Landroid/os/PowerManager;

.field private mServerSession:Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;

.field private mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

.field private mShares:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/bluetooth/opp/BluetoothOppShareInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mSocketListener:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

.field private mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

.field private mUpdateThread:Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;

.field private userAccepted:Z


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 73
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@4
    .line 77
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->userAccepted:Z

    #@6
    .line 135
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mListenStarted:Z

    #@8
    .line 139
    iput v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mIncomingRetries:I

    #@a
    .line 141
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingConnection:Ljavax/obex/ObexTransport;

    #@d
    .line 262
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppService$2;

    #@f
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/BluetoothOppService$2;-><init>(Lcom/android/bluetooth/opp/BluetoothOppService;)V

    #@12
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@14
    .line 428
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppService$3;

    #@16
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/BluetoothOppService$3;-><init>(Lcom/android/bluetooth/opp/BluetoothOppService;)V

    #@19
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    #@1b
    .line 1128
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/opp/BluetoothOppService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppService;->updateFromProvider()V

    #@3
    return-void
.end method

.method static synthetic access$100(Landroid/content/ContentResolver;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 73
    invoke-static {p0}, Lcom/android/bluetooth/opp/BluetoothOppService;->trimDatabase(Landroid/content/ContentResolver;)V

    #@3
    return-void
.end method

.method static synthetic access$1000(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljavax/obex/ObexTransport;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingConnection:Ljavax/obex/ObexTransport;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/bluetooth/opp/BluetoothOppService;Ljavax/obex/ObexTransport;)Ljavax/obex/ObexTransport;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingConnection:Ljavax/obex/ObexTransport;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/bluetooth/opp/BluetoothOppService;Ljavax/obex/ObexTransport;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/bluetooth/opp/BluetoothOppService;->createServerSession(Ljavax/obex/ObexTransport;)V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/bluetooth/opp/BluetoothOppService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mIncomingRetries:I

    #@2
    return v0
.end method

.method static synthetic access$1202(Lcom/android/bluetooth/opp/BluetoothOppService;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput p1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mIncomingRetries:I

    #@2
    return p1
.end method

.method static synthetic access$1300(Lcom/android/bluetooth/opp/BluetoothOppService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingUpdate:Z

    #@2
    return v0
.end method

.method static synthetic access$1302(Lcom/android/bluetooth/opp/BluetoothOppService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingUpdate:Z

    #@2
    return p1
.end method

.method static synthetic access$1400(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/bluetooth/opp/BluetoothOppService;->shouldScanFile(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1600(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;I)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/opp/BluetoothOppService;->scanFile(Landroid/database/Cursor;I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1700(Lcom/android/bluetooth/opp/BluetoothOppService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/bluetooth/opp/BluetoothOppService;->deleteShare(I)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/opp/BluetoothOppService;->insertShare(Landroid/database/Cursor;I)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/bluetooth/opp/BluetoothOppService;->visibleNotification(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/bluetooth/opp/BluetoothOppService;)Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mSocketListener:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/bluetooth/opp/BluetoothOppService;I)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/bluetooth/opp/BluetoothOppService;->needAction(I)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2100(Lcom/android/bluetooth/opp/BluetoothOppService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->userAccepted:Z

    #@2
    return v0
.end method

.method static synthetic access$2200(Lcom/android/bluetooth/opp/BluetoothOppService;Landroid/database/Cursor;IZ)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/opp/BluetoothOppService;->updateShare(Landroid/database/Cursor;IZ)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Lcom/android/bluetooth/opp/BluetoothOppService;)Lcom/android/bluetooth/opp/BluetoothOppNotification;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/bluetooth/opp/BluetoothOppService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mListenStarted:Z

    #@2
    return v0
.end method

.method static synthetic access$302(Lcom/android/bluetooth/opp/BluetoothOppService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mListenStarted:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/android/bluetooth/opp/BluetoothOppService;)Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mUpdateThread:Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/android/bluetooth/opp/BluetoothOppService;Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;)Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mUpdateThread:Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Lcom/android/bluetooth/opp/BluetoothOppService;)Landroid/bluetooth/BluetoothAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$600(Lcom/android/bluetooth/opp/BluetoothOppService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppService;->startSocketListener()V

    #@3
    return-void
.end method

.method static synthetic access$702(Lcom/android/bluetooth/opp/BluetoothOppService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mMediaScanInProgress:Z

    #@2
    return p1
.end method

.method static synthetic access$800(Lcom/android/bluetooth/opp/BluetoothOppService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/bluetooth/opp/BluetoothOppService;)Ljava/util/ArrayList;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@2
    return-object v0
.end method

.method private createServerSession(Ljavax/obex/ObexTransport;)V
    .registers 5
    .parameter "transport"

    #@0
    .prologue
    .line 420
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;

    #@2
    invoke-direct {v0, p0, p1}, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;-><init>(Landroid/content/Context;Ljavax/obex/ObexTransport;)V

    #@5
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerSession:Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;

    #@7
    .line 421
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerSession:Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;->preStart()V

    #@c
    .line 423
    const-string v0, "BtOppService"

    #@e
    new-instance v1, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v2, "Get ServerSession "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerSession:Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;

    #@1b
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    const-string v2, " for incoming connection"

    #@25
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 426
    return-void
.end method

.method private deleteShare(I)V
    .registers 8
    .parameter "arrayPos"

    #@0
    .prologue
    .line 907
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v2

    #@6
    check-cast v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@8
    .line 915
    .local v2, info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    iget-wide v3, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mTimestamp:J

    #@a
    invoke-direct {p0, v3, v4}, Lcom/android/bluetooth/opp/BluetoothOppService;->findBatchWithTimeStamp(J)I

    #@d
    move-result v1

    #@e
    .line 916
    .local v1, i:I
    const/4 v3, -0x1

    #@f
    if-eq v1, v3, :cond_63

    #@11
    .line 917
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@13
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@19
    .line 918
    .local v0, batch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    invoke-virtual {v0, v2}, Lcom/android/bluetooth/opp/BluetoothOppBatch;->hasShare(Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)Z

    #@1c
    move-result v3

    #@1d
    if-eqz v3, :cond_40

    #@1f
    .line 920
    const-string v3, "BtOppService"

    #@21
    new-instance v4, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v5, "Service cancel batch for share "

    #@28
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    iget v5, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@2e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    .line 922
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppBatch;->cancelBatch()V

    #@3c
    .line 924
    const/4 v3, 0x0

    #@3d
    invoke-virtual {p0, v3}, Lcom/android/bluetooth/opp/BluetoothOppService;->setState(I)V

    #@40
    .line 927
    :cond_40
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppBatch;->isEmpty()Z

    #@43
    move-result v3

    #@44
    if-eqz v3, :cond_63

    #@46
    .line 929
    const-string v3, "BtOppService"

    #@48
    new-instance v4, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v5, "Service remove batch  "

    #@4f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v4

    #@53
    iget v5, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5c
    move-result-object v4

    #@5d
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    .line 931
    invoke-direct {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->removeBatch(Lcom/android/bluetooth/opp/BluetoothOppBatch;)V

    #@63
    .line 934
    .end local v0           #batch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    :cond_63
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@65
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    #@68
    .line 935
    return-void
.end method

.method private findBatchWithTimeStamp(J)I
    .registers 6
    .parameter "timestamp"

    #@0
    .prologue
    .line 965
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@5
    move-result v1

    #@6
    add-int/lit8 v0, v1, -0x1

    #@8
    .local v0, i:I
    :goto_8
    if-ltz v0, :cond_1c

    #@a
    .line 966
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@c
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f
    move-result-object v1

    #@10
    check-cast v1, Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@12
    iget-wide v1, v1, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mTimestamp:J

    #@14
    cmp-long v1, v1, p1

    #@16
    if-nez v1, :cond_19

    #@18
    .line 970
    .end local v0           #i:I
    :goto_18
    return v0

    #@19
    .line 965
    .restart local v0       #i:I
    :cond_19
    add-int/lit8 v0, v0, -0x1

    #@1b
    goto :goto_8

    #@1c
    .line 970
    :cond_1c
    const/4 v0, -0x1

    #@1d
    goto :goto_18
.end method

.method private insertShare(Landroid/database/Cursor;I)V
    .registers 24
    .parameter "cursor"
    .parameter "arrayPos"

    #@0
    .prologue
    .line 651
    const-string v3, "uri"

    #@2
    move-object/from16 v0, p1

    #@4
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@7
    move-result v3

    #@8
    move-object/from16 v0, p1

    #@a
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@d
    move-result-object v20

    #@e
    .line 653
    .local v20, uriString:Ljava/lang/String;
    if-eqz v20, :cond_2bb

    #@10
    .line 654
    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@13
    move-result-object v4

    #@14
    .line 655
    .local v4, uri:Landroid/net/Uri;
    const-string v3, "BtOppService"

    #@16
    new-instance v5, Ljava/lang/StringBuilder;

    #@18
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1b
    const-string v6, "insertShare parsed URI: "

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v5

    #@25
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v5

    #@29
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 660
    :goto_2c
    new-instance v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@2e
    const-string v3, "_id"

    #@30
    move-object/from16 v0, p1

    #@32
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@35
    move-result v3

    #@36
    move-object/from16 v0, p1

    #@38
    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    #@3b
    move-result v3

    #@3c
    const-string v5, "hint"

    #@3e
    move-object/from16 v0, p1

    #@40
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@43
    move-result v5

    #@44
    move-object/from16 v0, p1

    #@46
    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    const-string v6, "_data"

    #@4c
    move-object/from16 v0, p1

    #@4e
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@51
    move-result v6

    #@52
    move-object/from16 v0, p1

    #@54
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@57
    move-result-object v6

    #@58
    const-string v7, "mimetype"

    #@5a
    move-object/from16 v0, p1

    #@5c
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@5f
    move-result v7

    #@60
    move-object/from16 v0, p1

    #@62
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@65
    move-result-object v7

    #@66
    const-string v8, "direction"

    #@68
    move-object/from16 v0, p1

    #@6a
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@6d
    move-result v8

    #@6e
    move-object/from16 v0, p1

    #@70
    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    #@73
    move-result v8

    #@74
    const-string v9, "destination"

    #@76
    move-object/from16 v0, p1

    #@78
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@7b
    move-result v9

    #@7c
    move-object/from16 v0, p1

    #@7e
    invoke-interface {v0, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@81
    move-result-object v9

    #@82
    const-string v10, "visibility"

    #@84
    move-object/from16 v0, p1

    #@86
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@89
    move-result v10

    #@8a
    move-object/from16 v0, p1

    #@8c
    invoke-interface {v0, v10}, Landroid/database/Cursor;->getInt(I)I

    #@8f
    move-result v10

    #@90
    const-string v11, "confirm"

    #@92
    move-object/from16 v0, p1

    #@94
    invoke-interface {v0, v11}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@97
    move-result v11

    #@98
    move-object/from16 v0, p1

    #@9a
    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    #@9d
    move-result v11

    #@9e
    const-string v12, "status"

    #@a0
    move-object/from16 v0, p1

    #@a2
    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@a5
    move-result v12

    #@a6
    move-object/from16 v0, p1

    #@a8
    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    #@ab
    move-result v12

    #@ac
    const-string v13, "total_bytes"

    #@ae
    move-object/from16 v0, p1

    #@b0
    invoke-interface {v0, v13}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@b3
    move-result v13

    #@b4
    move-object/from16 v0, p1

    #@b6
    invoke-interface {v0, v13}, Landroid/database/Cursor;->getInt(I)I

    #@b9
    move-result v13

    #@ba
    const-string v14, "current_bytes"

    #@bc
    move-object/from16 v0, p1

    #@be
    invoke-interface {v0, v14}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@c1
    move-result v14

    #@c2
    move-object/from16 v0, p1

    #@c4
    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    #@c7
    move-result v14

    #@c8
    const-string v15, "timestamp"

    #@ca
    move-object/from16 v0, p1

    #@cc
    invoke-interface {v0, v15}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@cf
    move-result v15

    #@d0
    move-object/from16 v0, p1

    #@d2
    invoke-interface {v0, v15}, Landroid/database/Cursor;->getInt(I)I

    #@d5
    move-result v15

    #@d6
    const-string v16, "scanned"

    #@d8
    move-object/from16 v0, p1

    #@da
    move-object/from16 v1, v16

    #@dc
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@df
    move-result v16

    #@e0
    move-object/from16 v0, p1

    #@e2
    move/from16 v1, v16

    #@e4
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@e7
    move-result v16

    #@e8
    if-eqz v16, :cond_2c5

    #@ea
    const/16 v16, 0x1

    #@ec
    :goto_ec
    invoke-direct/range {v2 .. v16}, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;-><init>(ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;IIIIIIZ)V

    #@ef
    .line 677
    .local v2, info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    const-string v3, "BtOppService"

    #@f1
    const-string v5, "Service adding new entry"

    #@f3
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@f6
    .line 678
    const-string v3, "BtOppService"

    #@f8
    new-instance v5, Ljava/lang/StringBuilder;

    #@fa
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@fd
    const-string v6, "ID      : "

    #@ff
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v5

    #@103
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@105
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@108
    move-result-object v5

    #@109
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10c
    move-result-object v5

    #@10d
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    .line 680
    const-string v3, "BtOppService"

    #@112
    new-instance v5, Ljava/lang/StringBuilder;

    #@114
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@117
    const-string v6, "URI     : "

    #@119
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v5

    #@11d
    iget-object v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@11f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v5

    #@123
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@126
    move-result-object v5

    #@127
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@12a
    .line 681
    const-string v3, "BtOppService"

    #@12c
    new-instance v5, Ljava/lang/StringBuilder;

    #@12e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@131
    const-string v6, "HINT    : "

    #@133
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v5

    #@137
    iget-object v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mHint:Ljava/lang/String;

    #@139
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13c
    move-result-object v5

    #@13d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@140
    move-result-object v5

    #@141
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@144
    .line 682
    const-string v3, "BtOppService"

    #@146
    new-instance v5, Ljava/lang/StringBuilder;

    #@148
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@14b
    const-string v6, "FILENAME: "

    #@14d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@150
    move-result-object v5

    #@151
    iget-object v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mFilename:Ljava/lang/String;

    #@153
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v5

    #@157
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15a
    move-result-object v5

    #@15b
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@15e
    .line 683
    const-string v3, "BtOppService"

    #@160
    new-instance v5, Ljava/lang/StringBuilder;

    #@162
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@165
    const-string v6, "MIMETYPE: "

    #@167
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v5

    #@16b
    iget-object v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mMimetype:Ljava/lang/String;

    #@16d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v5

    #@171
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@174
    move-result-object v5

    #@175
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@178
    .line 684
    const-string v3, "BtOppService"

    #@17a
    new-instance v5, Ljava/lang/StringBuilder;

    #@17c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@17f
    const-string v6, "DIRECTION: "

    #@181
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@184
    move-result-object v5

    #@185
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@187
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18a
    move-result-object v5

    #@18b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18e
    move-result-object v5

    #@18f
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@192
    .line 685
    const-string v3, "BtOppService"

    #@194
    new-instance v5, Ljava/lang/StringBuilder;

    #@196
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@199
    const-string v6, "DESTINAT: "

    #@19b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19e
    move-result-object v5

    #@19f
    iget-object v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDestination:Ljava/lang/String;

    #@1a1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a4
    move-result-object v5

    #@1a5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a8
    move-result-object v5

    #@1a9
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1ac
    .line 686
    const-string v3, "BtOppService"

    #@1ae
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b0
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1b3
    const-string v6, "VISIBILI: "

    #@1b5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v5

    #@1b9
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mVisibility:I

    #@1bb
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1be
    move-result-object v5

    #@1bf
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c2
    move-result-object v5

    #@1c3
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c6
    .line 687
    const-string v3, "BtOppService"

    #@1c8
    new-instance v5, Ljava/lang/StringBuilder;

    #@1ca
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1cd
    const-string v6, "CONFIRM : "

    #@1cf
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v5

    #@1d3
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mConfirm:I

    #@1d5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v5

    #@1d9
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dc
    move-result-object v5

    #@1dd
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1e0
    .line 688
    const-string v3, "BtOppService"

    #@1e2
    new-instance v5, Ljava/lang/StringBuilder;

    #@1e4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1e7
    const-string v6, "STATUS  : "

    #@1e9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ec
    move-result-object v5

    #@1ed
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@1ef
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f2
    move-result-object v5

    #@1f3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f6
    move-result-object v5

    #@1f7
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1fa
    .line 689
    const-string v3, "BtOppService"

    #@1fc
    new-instance v5, Ljava/lang/StringBuilder;

    #@1fe
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@201
    const-string v6, "TOTAL   : "

    #@203
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@206
    move-result-object v5

    #@207
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mTotalBytes:I

    #@209
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20c
    move-result-object v5

    #@20d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@210
    move-result-object v5

    #@211
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@214
    .line 690
    const-string v3, "BtOppService"

    #@216
    new-instance v5, Ljava/lang/StringBuilder;

    #@218
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@21b
    const-string v6, "CURRENT : "

    #@21d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@220
    move-result-object v5

    #@221
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mCurrentBytes:I

    #@223
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@226
    move-result-object v5

    #@227
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22a
    move-result-object v5

    #@22b
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@22e
    .line 691
    const-string v3, "BtOppService"

    #@230
    new-instance v5, Ljava/lang/StringBuilder;

    #@232
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@235
    const-string v6, "TIMESTAMP : "

    #@237
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23a
    move-result-object v5

    #@23b
    iget-wide v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mTimestamp:J

    #@23d
    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@240
    move-result-object v5

    #@241
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@244
    move-result-object v5

    #@245
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@248
    .line 692
    const-string v3, "BtOppService"

    #@24a
    new-instance v5, Ljava/lang/StringBuilder;

    #@24c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@24f
    const-string v6, "SCANNED : "

    #@251
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@254
    move-result-object v5

    #@255
    iget-boolean v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mMediaScanned:Z

    #@257
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@25a
    move-result-object v5

    #@25b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25e
    move-result-object v5

    #@25f
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@262
    .line 695
    move-object/from16 v0, p0

    #@264
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@266
    move/from16 v0, p2

    #@268
    invoke-virtual {v3, v0, v2}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    #@26b
    .line 698
    invoke-virtual {v2}, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->isObsolete()Z

    #@26e
    move-result v3

    #@26f
    if-eqz v3, :cond_27a

    #@271
    .line 699
    iget v3, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@273
    const/16 v5, 0x1eb

    #@275
    move-object/from16 v0, p0

    #@277
    invoke-static {v0, v3, v5}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@27a
    .line 711
    :cond_27a
    invoke-virtual {v2}, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->isReadyToStart()Z

    #@27d
    move-result v3

    #@27e
    if-eqz v3, :cond_2ba

    #@280
    .line 712
    iget v3, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@282
    if-nez v3, :cond_2c9

    #@284
    .line 714
    iget-object v3, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@286
    invoke-static {v3}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->getSendFileInfo(Landroid/net/Uri;)Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;

    #@289
    move-result-object v19

    #@28a
    .line 716
    .local v19, sendFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    if-eqz v19, :cond_292

    #@28c
    move-object/from16 v0, v19

    #@28e
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;->mInputStream:Ljava/io/FileInputStream;

    #@290
    if-nez v3, :cond_2c9

    #@292
    .line 717
    :cond_292
    const-string v3, "BtOppService"

    #@294
    new-instance v5, Ljava/lang/StringBuilder;

    #@296
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@299
    const-string v6, "Can\'t open file for OUTBOUND info "

    #@29b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29e
    move-result-object v5

    #@29f
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@2a1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a4
    move-result-object v5

    #@2a5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a8
    move-result-object v5

    #@2a9
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2ac
    .line 718
    iget v3, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@2ae
    const/16 v5, 0x190

    #@2b0
    move-object/from16 v0, p0

    #@2b2
    invoke-static {v0, v3, v5}, Lcom/android/bluetooth/opp/Constants;->updateShareStatus(Landroid/content/Context;II)V

    #@2b5
    .line 719
    iget-object v3, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@2b7
    invoke-static {v3}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->closeSendFileInfo(Landroid/net/Uri;)V

    #@2ba
    .line 797
    .end local v19           #sendFileInfo:Lcom/android/bluetooth/opp/BluetoothOppSendFileInfo;
    :cond_2ba
    :goto_2ba
    return-void

    #@2bb
    .line 657
    .end local v2           #info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    .end local v4           #uri:Landroid/net/Uri;
    :cond_2bb
    const/4 v4, 0x0

    #@2bc
    .line 658
    .restart local v4       #uri:Landroid/net/Uri;
    const-string v3, "BtOppService"

    #@2be
    const-string v5, "insertShare found null URI at cursor!"

    #@2c0
    invoke-static {v3, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c3
    goto/16 :goto_2c

    #@2c5
    .line 660
    :cond_2c5
    const/16 v16, 0x0

    #@2c7
    goto/16 :goto_ec

    #@2c9
    .line 723
    .restart local v2       #info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    :cond_2c9
    move-object/from16 v0, p0

    #@2cb
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@2cd
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@2d0
    move-result v3

    #@2d1
    if-nez v3, :cond_3fb

    #@2d3
    .line 724
    new-instance v18, Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@2d5
    move-object/from16 v0, v18

    #@2d7
    move-object/from16 v1, p0

    #@2d9
    invoke-direct {v0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppBatch;-><init>(Landroid/content/Context;Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V

    #@2dc
    .line 725
    .local v18, newBatch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    move-object/from16 v0, p0

    #@2de
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchId:I

    #@2e0
    move-object/from16 v0, v18

    #@2e2
    iput v3, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@2e4
    .line 726
    move-object/from16 v0, p0

    #@2e6
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchId:I

    #@2e8
    add-int/lit8 v3, v3, 0x1

    #@2ea
    move-object/from16 v0, p0

    #@2ec
    iput v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchId:I

    #@2ee
    .line 727
    move-object/from16 v0, p0

    #@2f0
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@2f2
    move-object/from16 v0, v18

    #@2f4
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2f7
    .line 728
    iget v3, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@2f9
    if-nez v3, :cond_375

    #@2fb
    .line 730
    const-string v3, "BtOppService"

    #@2fd
    new-instance v5, Ljava/lang/StringBuilder;

    #@2ff
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@302
    const-string v6, "Service create new Batch "

    #@304
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@307
    move-result-object v5

    #@308
    move-object/from16 v0, v18

    #@30a
    iget v6, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@30c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30f
    move-result-object v5

    #@310
    const-string v6, " for OUTBOUND info "

    #@312
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@315
    move-result-object v5

    #@316
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@318
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31b
    move-result-object v5

    #@31c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31f
    move-result-object v5

    #@320
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@323
    .line 733
    new-instance v3, Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@325
    move-object/from16 v0, p0

    #@327
    iget-object v5, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mPowerManager:Landroid/os/PowerManager;

    #@329
    move-object/from16 v0, p0

    #@32b
    move-object/from16 v1, v18

    #@32d
    invoke-direct {v3, v0, v5, v1}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;-><init>(Landroid/content/Context;Landroid/os/PowerManager;Lcom/android/bluetooth/opp/BluetoothOppBatch;)V

    #@330
    move-object/from16 v0, p0

    #@332
    iput-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@334
    .line 743
    :cond_334
    :goto_334
    iget v3, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@336
    if-nez v3, :cond_3b9

    #@338
    move-object/from16 v0, p0

    #@33a
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@33c
    if-eqz v3, :cond_3b9

    #@33e
    .line 745
    const-string v3, "BtOppService"

    #@340
    new-instance v5, Ljava/lang/StringBuilder;

    #@342
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@345
    const-string v6, "Service start transfer new Batch "

    #@347
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34a
    move-result-object v5

    #@34b
    move-object/from16 v0, v18

    #@34d
    iget v6, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@34f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@352
    move-result-object v5

    #@353
    const-string v6, " for info "

    #@355
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@358
    move-result-object v5

    #@359
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@35b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@35e
    move-result-object v5

    #@35f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@362
    move-result-object v5

    #@363
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@366
    .line 748
    move-object/from16 v0, p0

    #@368
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@36a
    invoke-virtual {v3}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->start()V

    #@36d
    .line 750
    const/4 v3, 0x2

    #@36e
    move-object/from16 v0, p0

    #@370
    invoke-virtual {v0, v3}, Lcom/android/bluetooth/opp/BluetoothOppService;->setState(I)V

    #@373
    goto/16 :goto_2ba

    #@375
    .line 734
    :cond_375
    iget v3, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@377
    const/4 v5, 0x1

    #@378
    if-ne v3, v5, :cond_334

    #@37a
    .line 736
    const-string v3, "BtOppService"

    #@37c
    new-instance v5, Ljava/lang/StringBuilder;

    #@37e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@381
    const-string v6, "Service create new Batch "

    #@383
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@386
    move-result-object v5

    #@387
    move-object/from16 v0, v18

    #@389
    iget v6, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@38b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38e
    move-result-object v5

    #@38f
    const-string v6, " for INBOUND info "

    #@391
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@394
    move-result-object v5

    #@395
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@397
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39a
    move-result-object v5

    #@39b
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@39e
    move-result-object v5

    #@39f
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a2
    .line 739
    new-instance v3, Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@3a4
    move-object/from16 v0, p0

    #@3a6
    iget-object v5, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mPowerManager:Landroid/os/PowerManager;

    #@3a8
    move-object/from16 v0, p0

    #@3aa
    iget-object v6, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerSession:Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;

    #@3ac
    move-object/from16 v0, p0

    #@3ae
    move-object/from16 v1, v18

    #@3b0
    invoke-direct {v3, v0, v5, v1, v6}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;-><init>(Landroid/content/Context;Landroid/os/PowerManager;Lcom/android/bluetooth/opp/BluetoothOppBatch;Lcom/android/bluetooth/opp/BluetoothOppObexSession;)V

    #@3b3
    move-object/from16 v0, p0

    #@3b5
    iput-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@3b7
    goto/16 :goto_334

    #@3b9
    .line 752
    :cond_3b9
    iget v3, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@3bb
    const/4 v5, 0x1

    #@3bc
    if-ne v3, v5, :cond_2ba

    #@3be
    move-object/from16 v0, p0

    #@3c0
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@3c2
    if-eqz v3, :cond_2ba

    #@3c4
    .line 755
    const-string v3, "BtOppService"

    #@3c6
    new-instance v5, Ljava/lang/StringBuilder;

    #@3c8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3cb
    const-string v6, "Service start server transfer new Batch "

    #@3cd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d0
    move-result-object v5

    #@3d1
    move-object/from16 v0, v18

    #@3d3
    iget v6, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@3d5
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d8
    move-result-object v5

    #@3d9
    const-string v6, " for info "

    #@3db
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3de
    move-result-object v5

    #@3df
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@3e1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e4
    move-result-object v5

    #@3e5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e8
    move-result-object v5

    #@3e9
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3ec
    .line 758
    move-object/from16 v0, p0

    #@3ee
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@3f0
    invoke-virtual {v3}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->start()V

    #@3f3
    .line 760
    const/4 v3, 0x2

    #@3f4
    move-object/from16 v0, p0

    #@3f6
    invoke-virtual {v0, v3}, Lcom/android/bluetooth/opp/BluetoothOppService;->setState(I)V

    #@3f9
    goto/16 :goto_2ba

    #@3fb
    .line 765
    .end local v18           #newBatch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    :cond_3fb
    iget-wide v5, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mTimestamp:J

    #@3fd
    move-object/from16 v0, p0

    #@3ff
    invoke-direct {v0, v5, v6}, Lcom/android/bluetooth/opp/BluetoothOppService;->findBatchWithTimeStamp(J)I

    #@402
    move-result v17

    #@403
    .line 766
    .local v17, i:I
    const/4 v3, -0x1

    #@404
    move/from16 v0, v17

    #@406
    if-eq v0, v3, :cond_44b

    #@408
    .line 768
    const-string v5, "BtOppService"

    #@40a
    new-instance v3, Ljava/lang/StringBuilder;

    #@40c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@40f
    const-string v6, "Service add info "

    #@411
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@414
    move-result-object v3

    #@415
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@417
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41a
    move-result-object v3

    #@41b
    const-string v6, " to existing batch "

    #@41d
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@420
    move-result-object v6

    #@421
    move-object/from16 v0, p0

    #@423
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@425
    move/from16 v0, v17

    #@427
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@42a
    move-result-object v3

    #@42b
    check-cast v3, Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@42d
    iget v3, v3, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@42f
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@432
    move-result-object v3

    #@433
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@436
    move-result-object v3

    #@437
    invoke-static {v5, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@43a
    .line 771
    move-object/from16 v0, p0

    #@43c
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@43e
    move/from16 v0, v17

    #@440
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@443
    move-result-object v3

    #@444
    check-cast v3, Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@446
    invoke-virtual {v3, v2}, Lcom/android/bluetooth/opp/BluetoothOppBatch;->addShare(Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V

    #@449
    goto/16 :goto_2ba

    #@44b
    .line 774
    :cond_44b
    new-instance v18, Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@44d
    move-object/from16 v0, v18

    #@44f
    move-object/from16 v1, p0

    #@451
    invoke-direct {v0, v1, v2}, Lcom/android/bluetooth/opp/BluetoothOppBatch;-><init>(Landroid/content/Context;Lcom/android/bluetooth/opp/BluetoothOppShareInfo;)V

    #@454
    .line 775
    .restart local v18       #newBatch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    move-object/from16 v0, p0

    #@456
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchId:I

    #@458
    move-object/from16 v0, v18

    #@45a
    iput v3, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@45c
    .line 776
    move-object/from16 v0, p0

    #@45e
    iget v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchId:I

    #@460
    add-int/lit8 v3, v3, 0x1

    #@462
    move-object/from16 v0, p0

    #@464
    iput v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchId:I

    #@466
    .line 777
    move-object/from16 v0, p0

    #@468
    iget-object v3, v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@46a
    move-object/from16 v0, v18

    #@46c
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@46f
    .line 779
    const-string v3, "BtOppService"

    #@471
    new-instance v5, Ljava/lang/StringBuilder;

    #@473
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@476
    const-string v6, "Service add new Batch "

    #@478
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47b
    move-result-object v5

    #@47c
    move-object/from16 v0, v18

    #@47e
    iget v6, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@480
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@483
    move-result-object v5

    #@484
    const-string v6, " for info "

    #@486
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@489
    move-result-object v5

    #@48a
    iget v6, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@48c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48f
    move-result-object v5

    #@490
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@493
    move-result-object v5

    #@494
    invoke-static {v3, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@497
    goto/16 :goto_2ba
.end method

.method private needAction(I)Z
    .registers 4
    .parameter "arrayPos"

    #@0
    .prologue
    .line 1016
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@8
    .line 1017
    .local v0, info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    iget v1, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@a
    invoke-static {v1}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z

    #@d
    move-result v1

    #@e
    if-eqz v1, :cond_12

    #@10
    .line 1018
    const/4 v1, 0x0

    #@11
    .line 1020
    :goto_11
    return v1

    #@12
    :cond_12
    const/4 v1, 0x1

    #@13
    goto :goto_11
.end method

.method private removeBatch(Lcom/android/bluetooth/opp/BluetoothOppBatch;)V
    .registers 8
    .parameter "batch"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 975
    const-string v2, "BtOppService"

    #@3
    new-instance v3, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "Remove batch "

    #@a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v3

    #@e
    iget v4, p1, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@10
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v3

    #@18
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 977
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@1d
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@20
    .line 979
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@22
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@25
    move-result v2

    #@26
    if-lez v2, :cond_3d

    #@28
    .line 980
    const/4 v0, 0x0

    #@29
    .local v0, i:I
    :goto_29
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@2b
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    #@2e
    move-result v2

    #@2f
    if-ge v0, v2, :cond_3d

    #@31
    .line 982
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@33
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@36
    move-result-object v1

    #@37
    check-cast v1, Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@39
    .line 983
    .local v1, nextBatch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    iget v2, v1, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mStatus:I

    #@3b
    if-ne v2, v5, :cond_3e

    #@3d
    .line 1013
    .end local v0           #i:I
    .end local v1           #nextBatch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    :cond_3d
    :goto_3d
    return-void

    #@3e
    .line 987
    .restart local v0       #i:I
    .restart local v1       #nextBatch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    :cond_3e
    iget v2, v1, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mDirection:I

    #@40
    if-nez v2, :cond_6b

    #@42
    .line 989
    const-string v2, "BtOppService"

    #@44
    new-instance v3, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v4, "Start pending outbound batch "

    #@4b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v3

    #@4f
    iget v4, v1, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@51
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v3

    #@59
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 991
    new-instance v2, Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@5e
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mPowerManager:Landroid/os/PowerManager;

    #@60
    invoke-direct {v2, p0, v3, v1}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;-><init>(Landroid/content/Context;Landroid/os/PowerManager;Lcom/android/bluetooth/opp/BluetoothOppBatch;)V

    #@63
    iput-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@65
    .line 992
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@67
    invoke-virtual {v2}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->start()V

    #@6a
    goto :goto_3d

    #@6b
    .line 994
    :cond_6b
    iget v2, v1, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mDirection:I

    #@6d
    if-ne v2, v5, :cond_ab

    #@6f
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerSession:Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;

    #@71
    if-eqz v2, :cond_ab

    #@73
    .line 999
    const-string v2, "BtOppService"

    #@75
    new-instance v3, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v4, "Start pending inbound batch "

    #@7c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v3

    #@80
    iget v4, v1, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@82
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@85
    move-result-object v3

    #@86
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v3

    #@8a
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    .line 1001
    new-instance v2, Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@8f
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mPowerManager:Landroid/os/PowerManager;

    #@91
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerSession:Lcom/android/bluetooth/opp/BluetoothOppObexServerSession;

    #@93
    invoke-direct {v2, p0, v3, v1, v4}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;-><init>(Landroid/content/Context;Landroid/os/PowerManager;Lcom/android/bluetooth/opp/BluetoothOppBatch;Lcom/android/bluetooth/opp/BluetoothOppObexSession;)V

    #@96
    iput-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@98
    .line 1003
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@9a
    invoke-virtual {v2}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->start()V

    #@9d
    .line 1004
    invoke-virtual {v1}, Lcom/android/bluetooth/opp/BluetoothOppBatch;->getPendingShare()Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@a0
    move-result-object v2

    #@a1
    iget v2, v2, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mConfirm:I

    #@a3
    if-ne v2, v5, :cond_3d

    #@a5
    .line 1006
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@a7
    invoke-virtual {v2}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->setConfirmed()V

    #@aa
    goto :goto_3d

    #@ab
    .line 980
    :cond_ab
    add-int/lit8 v0, v0, 0x1

    #@ad
    goto/16 :goto_29
.end method

.method private scanFile(Landroid/database/Cursor;I)Z
    .registers 8
    .parameter "cursor"
    .parameter "arrayPos"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 1029
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@3
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@9
    .line 1030
    .local v0, info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    monitor-enter p0

    #@a
    .line 1032
    :try_start_a
    const-string v2, "BtOppService"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Scanning file "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    iget-object v4, v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mFilename:Ljava/lang/String;

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1034
    iget-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mMediaScanInProgress:Z

    #@26
    if-nez v2, :cond_34

    #@28
    .line 1035
    const/4 v2, 0x1

    #@29
    iput-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mMediaScanInProgress:Z

    #@2b
    .line 1036
    new-instance v2, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;

    #@2d
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@2f
    invoke-direct {v2, p0, v0, v3}, Lcom/android/bluetooth/opp/BluetoothOppService$MediaScannerNotifier;-><init>(Landroid/content/Context;Lcom/android/bluetooth/opp/BluetoothOppShareInfo;Landroid/os/Handler;)V

    #@32
    .line 1037
    monitor-exit p0

    #@33
    .line 1039
    :goto_33
    return v1

    #@34
    :cond_34
    const/4 v1, 0x0

    #@35
    monitor-exit p0

    #@36
    goto :goto_33

    #@37
    .line 1041
    :catchall_37
    move-exception v1

    #@38
    monitor-exit p0
    :try_end_39
    .catchall {:try_start_a .. :try_end_39} :catchall_37

    #@39
    throw v1
.end method

.method private shouldScanFile(I)Z
    .registers 8
    .parameter "arrayPos"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1051
    :try_start_2
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@4
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@7
    move-result-object v1

    #@8
    check-cast v1, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    :try_end_a
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_2 .. :try_end_a} :catch_20

    #@a
    .line 1057
    .local v1, info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    iget v4, v1, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@c
    invoke-static {v4}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusSuccess(I)Z

    #@f
    move-result v4

    #@10
    if-eqz v4, :cond_3b

    #@12
    iget v4, v1, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@14
    if-ne v4, v2, :cond_3b

    #@16
    iget-boolean v4, v1, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mMediaScanned:Z

    #@18
    if-nez v4, :cond_3b

    #@1a
    iget v4, v1, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mConfirm:I

    #@1c
    const/4 v5, 0x5

    #@1d
    if-eq v4, v5, :cond_3b

    #@1f
    .end local v1           #info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    :goto_1f
    return v2

    #@20
    .line 1052
    :catch_20
    move-exception v0

    #@21
    .line 1053
    .local v0, e:Ljava/lang/IndexOutOfBoundsException;
    const-string v2, "BtOppService"

    #@23
    new-instance v4, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v5, "[BTUI] shouldScanFile : "

    #@2a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v4

    #@32
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    move v2, v3

    #@3a
    .line 1054
    goto :goto_1f

    #@3b
    .end local v0           #e:Ljava/lang/IndexOutOfBoundsException;
    .restart local v1       #info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    :cond_3b
    move v2, v3

    #@3c
    .line 1057
    goto :goto_1f
.end method

.method private startListener()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 241
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mListenStarted:Z

    #@3
    if-nez v0, :cond_21

    #@5
    .line 242
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@7
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@a
    move-result v0

    #@b
    if-eqz v0, :cond_21

    #@d
    .line 244
    const-string v0, "BtOppService"

    #@f
    const-string v1, "Starting RfcommListener"

    #@11
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 246
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@16
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@18
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1f
    .line 247
    iput-boolean v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mListenStarted:Z

    #@21
    .line 250
    :cond_21
    return-void
.end method

.method private startSocketListener()V
    .registers 3

    #@0
    .prologue
    .line 389
    const-string v0, "BtOppService"

    #@2
    const-string v1, "start RfcommListener"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 391
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mSocketListener:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@9
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@b
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->start(Landroid/os/Handler;)Z

    #@e
    .line 393
    const-string v0, "BtOppService"

    #@10
    const-string v1, "RfcommListener started"

    #@12
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 395
    return-void
.end method

.method private stringFromCursor(Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .registers 12
    .parameter "old"
    .parameter "cursor"
    .parameter "column"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 938
    invoke-interface {p2, p3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@4
    move-result v1

    #@5
    .line 939
    .local v1, index:I
    if-nez p1, :cond_c

    #@7
    .line 940
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@a
    move-result-object p1

    #@b
    .line 961
    .end local p1
    :cond_b
    :goto_b
    return-object p1

    #@c
    .line 942
    .restart local p1
    :cond_c
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNewChars:Landroid/database/CharArrayBuffer;

    #@e
    if-nez v5, :cond_19

    #@10
    .line 943
    new-instance v5, Landroid/database/CharArrayBuffer;

    #@12
    const/16 v6, 0x80

    #@14
    invoke-direct {v5, v6}, Landroid/database/CharArrayBuffer;-><init>(I)V

    #@17
    iput-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNewChars:Landroid/database/CharArrayBuffer;

    #@19
    .line 945
    :cond_19
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNewChars:Landroid/database/CharArrayBuffer;

    #@1b
    invoke-interface {p2, v1, v5}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    #@1e
    .line 946
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNewChars:Landroid/database/CharArrayBuffer;

    #@20
    iget v2, v5, Landroid/database/CharArrayBuffer;->sizeCopied:I

    #@22
    .line 947
    .local v2, length:I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@25
    move-result v5

    #@26
    if-eq v2, v5, :cond_2d

    #@28
    .line 948
    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2b
    move-result-object p1

    #@2c
    goto :goto_b

    #@2d
    .line 950
    :cond_2d
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mOldChars:Landroid/database/CharArrayBuffer;

    #@2f
    if-eqz v5, :cond_37

    #@31
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mOldChars:Landroid/database/CharArrayBuffer;

    #@33
    iget v5, v5, Landroid/database/CharArrayBuffer;->sizeCopied:I

    #@35
    if-ge v5, v2, :cond_3e

    #@37
    .line 951
    :cond_37
    new-instance v5, Landroid/database/CharArrayBuffer;

    #@39
    invoke-direct {v5, v2}, Landroid/database/CharArrayBuffer;-><init>(I)V

    #@3c
    iput-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mOldChars:Landroid/database/CharArrayBuffer;

    #@3e
    .line 953
    :cond_3e
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mOldChars:Landroid/database/CharArrayBuffer;

    #@40
    iget-object v4, v5, Landroid/database/CharArrayBuffer;->data:[C

    #@42
    .line 954
    .local v4, oldArray:[C
    iget-object v5, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNewChars:Landroid/database/CharArrayBuffer;

    #@44
    iget-object v3, v5, Landroid/database/CharArrayBuffer;->data:[C

    #@46
    .line 955
    .local v3, newArray:[C
    invoke-virtual {p1, v7, v2, v4, v7}, Ljava/lang/String;->getChars(II[CI)V

    #@49
    .line 956
    add-int/lit8 v0, v2, -0x1

    #@4b
    .local v0, i:I
    :goto_4b
    if-ltz v0, :cond_b

    #@4d
    .line 957
    aget-char v5, v4, v0

    #@4f
    aget-char v6, v3, v0

    #@51
    if-eq v5, v6, :cond_59

    #@53
    .line 958
    new-instance p1, Ljava/lang/String;

    #@55
    .end local p1
    invoke-direct {p1, v3, v7, v2}, Ljava/lang/String;-><init>([CII)V

    #@58
    goto :goto_b

    #@59
    .line 956
    .restart local p1
    :cond_59
    add-int/lit8 v0, v0, -0x1

    #@5b
    goto :goto_4b
.end method

.method private static trimDatabase(Landroid/content/ContentResolver;)V
    .registers 21
    .parameter "contentResolver"

    #@0
    .prologue
    .line 1064
    const-string v8, "visibility=1"

    #@2
    .line 1068
    .local v8, INVISIBLE:Ljava/lang/String;
    const-string v11, "direction=0 AND status>=200 AND visibility=1"

    #@4
    .line 1071
    .local v11, WHERE_INVISIBLE_COMPLETE_OUTBOUND:Ljava/lang/String;
    sget-object v2, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@6
    const-string v3, "direction=0 AND status>=200 AND visibility=1"

    #@8
    const/4 v4, 0x0

    #@9
    move-object/from16 v0, p0

    #@b
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@e
    move-result v14

    #@f
    .line 1074
    .local v14, delNum:I
    const-string v2, "BtOppService"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "Deleted complete outbound shares, number =  "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 1078
    const-string v10, "direction=1 AND status>200 AND visibility=1"

    #@29
    .line 1081
    .local v10, WHERE_INVISIBLE_COMPLETE_INBOUND_FAILED:Ljava/lang/String;
    sget-object v2, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@2b
    const-string v3, "direction=1 AND status>200 AND visibility=1"

    #@2d
    const/4 v4, 0x0

    #@2e
    move-object/from16 v0, p0

    #@30
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@33
    move-result v14

    #@34
    .line 1084
    const-string v2, "BtOppService"

    #@36
    new-instance v3, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v4, "Deleted complete inbound failed shares, number = "

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 1089
    const-string v9, "direction=1 AND status=200 AND visibility=1"

    #@4e
    .line 1093
    .local v9, WHERE_INBOUND_SUCCESS:Ljava/lang/String;
    const/4 v13, 0x0

    #@4f
    .line 1095
    .local v13, cursor:Landroid/database/Cursor;
    :try_start_4f
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@51
    const/4 v2, 0x1

    #@52
    new-array v4, v2, [Ljava/lang/String;

    #@54
    const/4 v2, 0x0

    #@55
    const-string v5, "_id"

    #@57
    aput-object v5, v4, v2

    #@59
    const-string v5, "direction=1 AND status=200 AND visibility=1"

    #@5b
    const/4 v6, 0x0

    #@5c
    const-string v7, "_id"

    #@5e
    move-object/from16 v2, p0

    #@60
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_63
    .catch Ljava/lang/Exception; {:try_start_4f .. :try_end_63} :catch_67

    #@63
    move-result-object v13

    #@64
    .line 1107
    if-nez v13, :cond_86

    #@66
    .line 1126
    :goto_66
    return-void

    #@67
    .line 1098
    :catch_67
    move-exception v15

    #@68
    .line 1099
    .local v15, e:Ljava/lang/Exception;
    if-eqz v13, :cond_6d

    #@6a
    .line 1100
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@6d
    .line 1102
    :cond_6d
    const-string v2, "BtOppService"

    #@6f
    new-instance v3, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v4, "[BTUI] trimDatabase: "

    #@76
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v3

    #@7a
    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v3

    #@7e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v3

    #@82
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    goto :goto_66

    #@86
    .line 1111
    .end local v15           #e:Ljava/lang/Exception;
    :cond_86
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    #@89
    move-result v19

    #@8a
    .line 1112
    .local v19, recordNum:I
    const/16 v2, 0x3e8

    #@8c
    move/from16 v0, v19

    #@8e
    if-le v0, v2, :cond_de

    #@90
    .line 1113
    move/from16 v0, v19

    #@92
    add-int/lit16 v0, v0, -0x3e8

    #@94
    move/from16 v18, v0

    #@96
    .line 1115
    .local v18, numToDelete:I
    move/from16 v0, v18

    #@98
    invoke-interface {v13, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@9b
    move-result v2

    #@9c
    if-eqz v2, :cond_de

    #@9e
    .line 1116
    const-string v2, "_id"

    #@a0
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@a3
    move-result v12

    #@a4
    .line 1117
    .local v12, columnId:I
    invoke-interface {v13, v12}, Landroid/database/Cursor;->getLong(I)J

    #@a7
    move-result-wide v16

    #@a8
    .line 1118
    .local v16, id:J
    sget-object v2, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@aa
    new-instance v3, Ljava/lang/StringBuilder;

    #@ac
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@af
    const-string v4, "_id < "

    #@b1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v3

    #@b5
    move-wide/from16 v0, v16

    #@b7
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v3

    #@bb
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v3

    #@bf
    const/4 v4, 0x0

    #@c0
    move-object/from16 v0, p0

    #@c2
    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@c5
    move-result v14

    #@c6
    .line 1121
    const-string v2, "BtOppService"

    #@c8
    new-instance v3, Ljava/lang/StringBuilder;

    #@ca
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@cd
    const-string v4, "Deleted old inbound success share: "

    #@cf
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v3

    #@d3
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v3

    #@d7
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@da
    move-result-object v3

    #@db
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@de
    .line 1125
    .end local v12           #columnId:I
    .end local v16           #id:J
    .end local v18           #numToDelete:I
    :cond_de
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@e1
    goto :goto_66
.end method

.method private updateFromProvider()V
    .registers 3

    #@0
    .prologue
    .line 466
    monitor-enter p0

    #@1
    .line 468
    :try_start_1
    iget-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mListenStarted:Z

    #@3
    if-nez v0, :cond_e

    #@5
    .line 469
    const-string v0, "BtOppService"

    #@7
    const-string v1, "[BTUI] updateFromProvider : mListenStarted is null"

    #@9
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 470
    monitor-exit p0

    #@d
    .line 480
    :goto_d
    return-void

    #@e
    .line 474
    :cond_e
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mPendingUpdate:Z

    #@11
    .line 475
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mUpdateThread:Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;

    #@13
    if-nez v0, :cond_21

    #@15
    .line 476
    new-instance v0, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;

    #@17
    invoke-direct {v0, p0}, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;-><init>(Lcom/android/bluetooth/opp/BluetoothOppService;)V

    #@1a
    iput-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mUpdateThread:Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;

    #@1c
    .line 477
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mUpdateThread:Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;

    #@1e
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppService$UpdateThread;->start()V

    #@21
    .line 479
    :cond_21
    monitor-exit p0

    #@22
    goto :goto_d

    #@23
    :catchall_23
    move-exception v0

    #@24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_1 .. :try_end_25} :catchall_23

    #@25
    throw v0
.end method

.method private updateShare(Landroid/database/Cursor;IZ)V
    .registers 15
    .parameter "cursor"
    .parameter "arrayPos"
    .parameter "userAccepted"

    #@0
    .prologue
    .line 800
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v8, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v3

    #@6
    check-cast v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@8
    .line 801
    .local v3, info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    const-string v8, "status"

    #@a
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@d
    move-result v7

    #@e
    .line 803
    .local v7, statusColumn:I
    const-string v8, "_id"

    #@10
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@13
    move-result v8

    #@14
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    #@17
    move-result v8

    #@18
    iput v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@1a
    .line 804
    iget-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@1c
    if-eqz v8, :cond_18f

    #@1e
    .line 805
    iget-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@20
    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@23
    move-result-object v8

    #@24
    const-string v9, "uri"

    #@26
    invoke-direct {p0, v8, p1, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->stringFromCursor(Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@29
    move-result-object v8

    #@2a
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@2d
    move-result-object v8

    #@2e
    iput-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mUri:Landroid/net/Uri;

    #@30
    .line 810
    :goto_30
    iget-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mHint:Ljava/lang/String;

    #@32
    const-string v9, "hint"

    #@34
    invoke-direct {p0, v8, p1, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->stringFromCursor(Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@37
    move-result-object v8

    #@38
    iput-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mHint:Ljava/lang/String;

    #@3a
    .line 811
    iget-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mFilename:Ljava/lang/String;

    #@3c
    const-string v9, "_data"

    #@3e
    invoke-direct {p0, v8, p1, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->stringFromCursor(Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@41
    move-result-object v8

    #@42
    iput-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mFilename:Ljava/lang/String;

    #@44
    .line 812
    iget-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mMimetype:Ljava/lang/String;

    #@46
    const-string v9, "mimetype"

    #@48
    invoke-direct {p0, v8, p1, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->stringFromCursor(Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@4b
    move-result-object v8

    #@4c
    iput-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mMimetype:Ljava/lang/String;

    #@4e
    .line 813
    const-string v8, "direction"

    #@50
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@53
    move-result v8

    #@54
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    #@57
    move-result v8

    #@58
    iput v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDirection:I

    #@5a
    .line 814
    iget-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDestination:Ljava/lang/String;

    #@5c
    const-string v9, "destination"

    #@5e
    invoke-direct {p0, v8, p1, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->stringFromCursor(Ljava/lang/String;Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    #@61
    move-result-object v8

    #@62
    iput-object v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mDestination:Ljava/lang/String;

    #@64
    .line 815
    const-string v8, "visibility"

    #@66
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@69
    move-result v8

    #@6a
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    #@6d
    move-result v6

    #@6e
    .line 817
    .local v6, newVisibility:I
    const/4 v1, 0x0

    #@6f
    .line 818
    .local v1, confirmed:Z
    const-string v8, "confirm"

    #@71
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@74
    move-result v8

    #@75
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    #@78
    move-result v4

    #@79
    .line 821
    .local v4, newConfirm:I
    iget v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mVisibility:I

    #@7b
    if-nez v8, :cond_92

    #@7d
    if-eqz v6, :cond_92

    #@7f
    iget v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@81
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z

    #@84
    move-result v8

    #@85
    if-nez v8, :cond_89

    #@87
    if-nez v4, :cond_92

    #@89
    .line 824
    :cond_89
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@8b
    iget-object v8, v8, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@8d
    iget v9, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@8f
    invoke-virtual {v8, v9}, Landroid/app/NotificationManager;->cancel(I)V

    #@92
    .line 827
    :cond_92
    iput v6, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mVisibility:I

    #@94
    .line 829
    iget v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mConfirm:I

    #@96
    if-nez v8, :cond_9b

    #@98
    if-eqz v4, :cond_9b

    #@9a
    .line 831
    const/4 v1, 0x1

    #@9b
    .line 833
    :cond_9b
    const-string v8, "confirm"

    #@9d
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@a0
    move-result v8

    #@a1
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    #@a4
    move-result v8

    #@a5
    iput v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mConfirm:I

    #@a7
    .line 835
    invoke-interface {p1, v7}, Landroid/database/Cursor;->getInt(I)I

    #@aa
    move-result v5

    #@ab
    .line 837
    .local v5, newStatus:I
    iget v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@ad
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z

    #@b0
    move-result v8

    #@b1
    if-nez v8, :cond_c2

    #@b3
    invoke-static {v5}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z

    #@b6
    move-result v8

    #@b7
    if-eqz v8, :cond_c2

    #@b9
    .line 839
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@bb
    iget-object v8, v8, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@bd
    iget v9, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@bf
    invoke-virtual {v8, v9}, Landroid/app/NotificationManager;->cancel(I)V

    #@c2
    .line 842
    :cond_c2
    iput v5, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mStatus:I

    #@c4
    .line 843
    const-string v8, "total_bytes"

    #@c6
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@c9
    move-result v8

    #@ca
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    #@cd
    move-result v8

    #@ce
    iput v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mTotalBytes:I

    #@d0
    .line 844
    const-string v8, "current_bytes"

    #@d2
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@d5
    move-result v8

    #@d6
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    #@d9
    move-result v8

    #@da
    iput v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mCurrentBytes:I

    #@dc
    .line 846
    const-string v8, "timestamp"

    #@de
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@e1
    move-result v8

    #@e2
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    #@e5
    move-result v8

    #@e6
    int-to-long v8, v8

    #@e7
    iput-wide v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mTimestamp:J

    #@e9
    .line 847
    const-string v8, "scanned"

    #@eb
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@ee
    move-result v8

    #@ef
    invoke-interface {p1, v8}, Landroid/database/Cursor;->getInt(I)I

    #@f2
    move-result v8

    #@f3
    if-eqz v8, :cond_1b1

    #@f5
    const/4 v8, 0x1

    #@f6
    :goto_f6
    iput-boolean v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mMediaScanned:Z

    #@f8
    .line 849
    if-eqz v1, :cond_13e

    #@fa
    .line 851
    const-string v8, "BtOppService"

    #@fc
    new-instance v9, Ljava/lang/StringBuilder;

    #@fe
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@101
    const-string v10, "Service handle info "

    #@103
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v9

    #@107
    iget v10, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@109
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v9

    #@10d
    const-string v10, " confirmed"

    #@10f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@112
    move-result-object v9

    #@113
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@116
    move-result-object v9

    #@117
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11a
    .line 854
    iget-wide v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mTimestamp:J

    #@11c
    invoke-direct {p0, v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->findBatchWithTimeStamp(J)I

    #@11f
    move-result v2

    #@120
    .line 855
    .local v2, i:I
    const/4 v8, -0x1

    #@121
    if-eq v2, v8, :cond_13e

    #@123
    .line 856
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@125
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@128
    move-result-object v0

    #@129
    check-cast v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@12b
    .line 857
    .local v0, batch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@12d
    if-eqz v8, :cond_13e

    #@12f
    iget v8, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@131
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@133
    invoke-virtual {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->getBatchId()I

    #@136
    move-result v9

    #@137
    if-ne v8, v9, :cond_13e

    #@139
    .line 858
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@13b
    invoke-virtual {v8}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->setConfirmed()V

    #@13e
    .line 862
    .end local v0           #batch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    .end local v2           #i:I
    :cond_13e
    iget-wide v8, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mTimestamp:J

    #@140
    invoke-direct {p0, v8, v9}, Lcom/android/bluetooth/opp/BluetoothOppService;->findBatchWithTimeStamp(J)I

    #@143
    move-result v2

    #@144
    .line 863
    .restart local v2       #i:I
    const/4 v8, -0x1

    #@145
    if-eq v2, v8, :cond_18e

    #@147
    .line 864
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@149
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@14c
    move-result-object v0

    #@14d
    check-cast v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;

    #@14f
    .line 865
    .restart local v0       #batch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    iget v8, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mStatus:I

    #@151
    const/4 v9, 0x2

    #@152
    if-eq v8, v9, :cond_159

    #@154
    iget v8, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mStatus:I

    #@156
    const/4 v9, 0x3

    #@157
    if-ne v8, v9, :cond_18e

    #@159
    .line 868
    :cond_159
    const-string v8, "BtOppService"

    #@15b
    new-instance v9, Ljava/lang/StringBuilder;

    #@15d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@160
    const-string v10, "Batch "

    #@162
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@165
    move-result-object v9

    #@166
    iget v10, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@168
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16b
    move-result-object v9

    #@16c
    const-string v10, " is finished"

    #@16e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@171
    move-result-object v9

    #@172
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@175
    move-result-object v9

    #@176
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@179
    .line 870
    iget v8, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mDirection:I

    #@17b
    if-nez v8, :cond_1f3

    #@17d
    .line 871
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@17f
    if-nez v8, :cond_1b4

    #@181
    .line 872
    const-string v8, "BtOppService"

    #@183
    const-string v9, "Unexpected error! mTransfer is null"

    #@185
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@188
    .line 882
    :goto_188
    const/4 v8, 0x0

    #@189
    iput-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@18b
    .line 898
    :goto_18b
    invoke-direct {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->removeBatch(Lcom/android/bluetooth/opp/BluetoothOppBatch;)V

    #@18e
    .line 901
    .end local v0           #batch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    :cond_18e
    return-void

    #@18f
    .line 808
    .end local v1           #confirmed:Z
    .end local v2           #i:I
    .end local v4           #newConfirm:I
    .end local v5           #newStatus:I
    .end local v6           #newVisibility:I
    :cond_18f
    const-string v8, "BtOppService"

    #@191
    new-instance v9, Ljava/lang/StringBuilder;

    #@193
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@196
    const-string v10, "updateShare() called for ID "

    #@198
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v9

    #@19c
    iget v10, v3, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->mId:I

    #@19e
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v9

    #@1a2
    const-string v10, " with null URI"

    #@1a4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v9

    #@1a8
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ab
    move-result-object v9

    #@1ac
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1af
    goto/16 :goto_30

    #@1b1
    .line 847
    .restart local v1       #confirmed:Z
    .restart local v4       #newConfirm:I
    .restart local v5       #newStatus:I
    .restart local v6       #newVisibility:I
    :cond_1b1
    const/4 v8, 0x0

    #@1b2
    goto/16 :goto_f6

    #@1b4
    .line 873
    .restart local v0       #batch:Lcom/android/bluetooth/opp/BluetoothOppBatch;
    .restart local v2       #i:I
    :cond_1b4
    iget v8, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@1b6
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1b8
    invoke-virtual {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->getBatchId()I

    #@1bb
    move-result v9

    #@1bc
    if-ne v8, v9, :cond_1c8

    #@1be
    .line 874
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1c0
    invoke-virtual {v8}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->stop()V

    #@1c3
    .line 876
    const/4 v8, 0x0

    #@1c4
    invoke-virtual {p0, v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->setState(I)V

    #@1c7
    goto :goto_188

    #@1c8
    .line 879
    :cond_1c8
    const-string v8, "BtOppService"

    #@1ca
    new-instance v9, Ljava/lang/StringBuilder;

    #@1cc
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@1cf
    const-string v10, "Unexpected error! batch id "

    #@1d1
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d4
    move-result-object v9

    #@1d5
    iget v10, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@1d7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1da
    move-result-object v9

    #@1db
    const-string v10, " doesn\'t match mTransfer id "

    #@1dd
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e0
    move-result-object v9

    #@1e1
    iget-object v10, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1e3
    invoke-virtual {v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->getBatchId()I

    #@1e6
    move-result v10

    #@1e7
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1ea
    move-result-object v9

    #@1eb
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ee
    move-result-object v9

    #@1ef
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f2
    goto :goto_188

    #@1f3
    .line 884
    :cond_1f3
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@1f5
    if-nez v8, :cond_202

    #@1f7
    .line 885
    const-string v8, "BtOppService"

    #@1f9
    const-string v9, "Unexpected error! mServerTransfer is null"

    #@1fb
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1fe
    .line 896
    :goto_1fe
    const/4 v8, 0x0

    #@1ff
    iput-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@201
    goto :goto_18b

    #@202
    .line 886
    :cond_202
    iget v8, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@204
    iget-object v9, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@206
    invoke-virtual {v9}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->getBatchId()I

    #@209
    move-result v9

    #@20a
    if-ne v8, v9, :cond_216

    #@20c
    .line 887
    iget-object v8, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@20e
    invoke-virtual {v8}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->stop()V

    #@211
    .line 889
    const/4 v8, 0x0

    #@212
    invoke-virtual {p0, v8}, Lcom/android/bluetooth/opp/BluetoothOppService;->setState(I)V

    #@215
    goto :goto_1fe

    #@216
    .line 892
    :cond_216
    const-string v8, "BtOppService"

    #@218
    new-instance v9, Ljava/lang/StringBuilder;

    #@21a
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@21d
    const-string v10, "Unexpected error! batch id "

    #@21f
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@222
    move-result-object v9

    #@223
    iget v10, v0, Lcom/android/bluetooth/opp/BluetoothOppBatch;->mId:I

    #@225
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@228
    move-result-object v9

    #@229
    const-string v10, " doesn\'t match mServerTransfer id "

    #@22b
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22e
    move-result-object v9

    #@22f
    iget-object v10, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mServerTransfer:Lcom/android/bluetooth/opp/BluetoothOppTransfer;

    #@231
    invoke-virtual {v10}, Lcom/android/bluetooth/opp/BluetoothOppTransfer;->getBatchId()I

    #@234
    move-result v10

    #@235
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@238
    move-result-object v9

    #@239
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23c
    move-result-object v9

    #@23d
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@240
    goto :goto_1fe
.end method

.method private visibleNotification(I)Z
    .registers 4
    .parameter "arrayPos"

    #@0
    .prologue
    .line 1024
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;

    #@8
    .line 1025
    .local v0, info:Lcom/android/bluetooth/opp/BluetoothOppShareInfo;
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppShareInfo;->hasCompletionNotification()Z

    #@b
    move-result v1

    #@c
    return v1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "arg0"

    #@0
    .prologue
    .line 152
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    #@2
    const-string v1, "Cannot bind to Bluetooth OPP Service"

    #@4
    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    #@7
    throw v0
.end method

.method public onCreate()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 157
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@4
    .line 159
    const-string v2, "BtOppService"

    #@6
    const-string v3, "onCreate"

    #@8
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 161
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@e
    move-result-object v2

    #@f
    iput-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@11
    .line 162
    new-instance v2, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@13
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@15
    invoke-direct {v2, v3}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;-><init>(Landroid/bluetooth/BluetoothAdapter;)V

    #@18
    iput-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mSocketListener:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@1a
    .line 163
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@1d
    move-result-object v2

    #@1e
    iput-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@20
    .line 164
    invoke-static {}, Lcom/google/android/collect/Lists;->newArrayList()Ljava/util/ArrayList;

    #@23
    move-result-object v2

    #@24
    iput-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@26
    .line 165
    new-instance v2, Lcom/android/bluetooth/opp/BluetoothOppService$BluetoothShareContentObserver;

    #@28
    invoke-direct {v2, p0}, Lcom/android/bluetooth/opp/BluetoothOppService$BluetoothShareContentObserver;-><init>(Lcom/android/bluetooth/opp/BluetoothOppService;)V

    #@2b
    iput-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mObserver:Lcom/android/bluetooth/opp/BluetoothOppService$BluetoothShareContentObserver;

    #@2d
    .line 166
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppService;->getContentResolver()Landroid/content/ContentResolver;

    #@30
    move-result-object v2

    #@31
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@33
    iget-object v4, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mObserver:Lcom/android/bluetooth/opp/BluetoothOppService$BluetoothShareContentObserver;

    #@35
    invoke-virtual {v2, v3, v5, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@38
    .line 167
    iput v5, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchId:I

    #@3a
    .line 168
    new-instance v2, Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@3c
    invoke-direct {v2, p0}, Lcom/android/bluetooth/opp/BluetoothOppNotification;-><init>(Landroid/content/Context;)V

    #@3f
    iput-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@41
    .line 169
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@43
    iget-object v2, v2, Lcom/android/bluetooth/opp/BluetoothOppNotification;->mNotificationMgr:Landroid/app/NotificationManager;

    #@45
    invoke-virtual {v2}, Landroid/app/NotificationManager;->cancelAll()V

    #@48
    .line 170
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mNotifier:Lcom/android/bluetooth/opp/BluetoothOppNotification;

    #@4a
    invoke-virtual {v2}, Lcom/android/bluetooth/opp/BluetoothOppNotification;->updateNotification()V

    #@4d
    .line 172
    const/4 v2, 0x0

    #@4e
    sput v2, Lcom/android/bluetooth/opp/BluetoothOppService;->mState:I

    #@50
    .line 175
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppService;->getContentResolver()Landroid/content/ContentResolver;

    #@53
    move-result-object v0

    #@54
    .line 176
    .local v0, contentResolver:Landroid/content/ContentResolver;
    new-instance v2, Lcom/android/bluetooth/opp/BluetoothOppService$1;

    #@56
    const-string v3, "trimDatabase"

    #@58
    invoke-direct {v2, p0, v3, v0}, Lcom/android/bluetooth/opp/BluetoothOppService$1;-><init>(Lcom/android/bluetooth/opp/BluetoothOppService;Ljava/lang/String;Landroid/content/ContentResolver;)V

    #@5b
    invoke-virtual {v2}, Lcom/android/bluetooth/opp/BluetoothOppService$1;->start()V

    #@5e
    .line 182
    new-instance v1, Landroid/content/IntentFilter;

    #@60
    const-string v2, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@62
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@65
    .line 183
    .local v1, filter:Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    #@67
    invoke-virtual {p0, v2, v1}, Lcom/android/bluetooth/opp/BluetoothOppService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@6a
    .line 186
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@6d
    move-result-object v2

    #@6e
    if-eqz v2, :cond_9b

    #@70
    .line 187
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@73
    move-result-object v2

    #@74
    const/4 v3, 0x0

    #@75
    invoke-interface {v2, v3}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@78
    move-result v2

    #@79
    if-nez v2, :cond_86

    #@7b
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@7e
    move-result-object v2

    #@7f
    const/4 v3, 0x2

    #@80
    invoke-interface {v2, v3}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@83
    move-result v2

    #@84
    if-eqz v2, :cond_9b

    #@86
    .line 190
    :cond_86
    const-string v2, "BtOppService"

    #@88
    const-string v3, "onCreate is blocked by LG MDM Server Policy"

    #@8a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    .line 191
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@8f
    iget-object v3, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@91
    const/16 v4, 0xc8

    #@93
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@96
    move-result-object v3

    #@97
    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@9a
    .line 208
    :goto_9a
    return-void

    #@9b
    .line 197
    :cond_9b
    monitor-enter p0

    #@9c
    .line 198
    :try_start_9c
    iget-object v2, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@9e
    if-nez v2, :cond_b3

    #@a0
    .line 199
    const-string v2, "BtOppService"

    #@a2
    const-string v3, "Local BT device is not enabled"

    #@a4
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a7
    .line 203
    :goto_a7
    monitor-exit p0
    :try_end_a8
    .catchall {:try_start_9c .. :try_end_a8} :catchall_b7

    #@a8
    .line 205
    invoke-static {p0}, Lcom/android/bluetooth/opp/BluetoothOppPreference;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppPreference;

    #@ab
    move-result-object v2

    #@ac
    invoke-virtual {v2}, Lcom/android/bluetooth/opp/BluetoothOppPreference;->dump()V

    #@af
    .line 207
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppService;->updateFromProvider()V

    #@b2
    goto :goto_9a

    #@b3
    .line 201
    :cond_b3
    :try_start_b3
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppService;->startListener()V

    #@b6
    goto :goto_a7

    #@b7
    .line 203
    :catchall_b7
    move-exception v2

    #@b8
    monitor-exit p0
    :try_end_b9
    .catchall {:try_start_b3 .. :try_end_b9} :catchall_b7

    #@b9
    throw v2
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 400
    const-string v0, "BtOppService"

    #@2
    const-string v1, "onDestroy"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 402
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@a
    .line 403
    invoke-virtual {p0}, Lcom/android/bluetooth/opp/BluetoothOppService;->getContentResolver()Landroid/content/ContentResolver;

    #@d
    move-result-object v0

    #@e
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mObserver:Lcom/android/bluetooth/opp/BluetoothOppService$BluetoothShareContentObserver;

    #@10
    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@13
    .line 404
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    #@15
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/opp/BluetoothOppService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@18
    .line 405
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mSocketListener:Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;

    #@1a
    invoke-virtual {v0}, Lcom/android/bluetooth/opp/BluetoothOppRfcommListener;->stop()V

    #@1d
    .line 407
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@1f
    if-eqz v0, :cond_26

    #@21
    .line 408
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mBatchs:Ljava/util/ArrayList;

    #@23
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@26
    .line 410
    :cond_26
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@28
    if-eqz v0, :cond_2f

    #@2a
    .line 411
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mShares:Ljava/util/ArrayList;

    #@2c
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@2f
    .line 413
    :cond_2f
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@31
    if-eqz v0, :cond_39

    #@33
    .line 414
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@35
    const/4 v1, 0x0

    #@36
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@39
    .line 416
    :cond_39
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 8
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    .line 213
    const-string v0, "BtOppService"

    #@3
    const-string v1, "onStartCommand"

    #@5
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 219
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@b
    move-result-object v0

    #@c
    if-eqz v0, :cond_38

    #@e
    .line 220
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@11
    move-result-object v0

    #@12
    const/4 v1, 0x0

    #@13
    invoke-interface {v0, v1}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@16
    move-result v0

    #@17
    if-nez v0, :cond_23

    #@19
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1c
    move-result-object v0

    #@1d
    invoke-interface {v0, v3}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_38

    #@23
    .line 223
    :cond_23
    const-string v0, "BtOppService"

    #@25
    const-string v1, "onStartCommand is blocked by LG MDM Server Policy"

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 224
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@2c
    iget-object v1, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mHandler:Landroid/os/Handler;

    #@2e
    const/16 v2, 0xc8

    #@30
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@37
    .line 237
    :goto_37
    return v3

    #@38
    .line 230
    :cond_38
    iget-object v0, p0, Lcom/android/bluetooth/opp/BluetoothOppService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@3a
    if-nez v0, :cond_47

    #@3c
    .line 231
    const-string v0, "BtOppService"

    #@3e
    const-string v1, "Local BT device is not enabled"

    #@40
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 235
    :goto_43
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppService;->updateFromProvider()V

    #@46
    goto :goto_37

    #@47
    .line 233
    :cond_47
    invoke-direct {p0}, Lcom/android/bluetooth/opp/BluetoothOppService;->startListener()V

    #@4a
    goto :goto_43
.end method

.method public declared-synchronized setState(I)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    .line 1190
    monitor-enter p0

    #@1
    :try_start_1
    sget v1, Lcom/android/bluetooth/opp/BluetoothOppService;->mState:I

    #@3
    if-eq p1, v1, :cond_2d

    #@5
    .line 1191
    const-string v1, "BtOppService"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "opp state "

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    sget v3, Lcom/android/bluetooth/opp/BluetoothOppService;->mState:I

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    const-string v3, " -> "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1193
    sget v0, Lcom/android/bluetooth/opp/BluetoothOppService;->mState:I

    #@2b
    .line 1194
    .local v0, prevState:I
    sput p1, Lcom/android/bluetooth/opp/BluetoothOppService;->mState:I
    :try_end_2d
    .catchall {:try_start_1 .. :try_end_2d} :catchall_2f

    #@2d
    .line 1196
    .end local v0           #prevState:I
    :cond_2d
    monitor-exit p0

    #@2e
    return-void

    #@2f
    .line 1190
    :catchall_2f
    move-exception v1

    #@30
    monitor-exit p0

    #@31
    throw v1
.end method
