.class Lcom/android/bluetooth/opp/TestActivity$4;
.super Ljava/lang/Object;
.source "TestActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/opp/TestActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/opp/TestActivity;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/opp/TestActivity;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 301
    iput-object p1, p0, Lcom/android/bluetooth/opp/TestActivity$4;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .registers 7
    .parameter "view"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 303
    new-instance v2, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    sget-object v3, Lcom/android/bluetooth/opp/BluetoothShare;->CONTENT_URI:Landroid/net/Uri;

    #@8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v2

    #@c
    const-string v3, "/"

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    iget-object v3, p0, Lcom/android/bluetooth/opp/TestActivity$4;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@14
    iget-object v3, v3, Lcom/android/bluetooth/opp/TestActivity;->mAckView:Landroid/widget/EditText;

    #@16
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@29
    move-result-object v0

    #@2a
    .line 305
    .local v0, contentUri:Landroid/net/Uri;
    new-instance v1, Landroid/content/ContentValues;

    #@2c
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@2f
    .line 309
    .local v1, updateValues:Landroid/content/ContentValues;
    const-string v2, "visibility"

    #@31
    const/4 v3, 0x1

    #@32
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@39
    .line 312
    iget-object v2, p0, Lcom/android/bluetooth/opp/TestActivity$4;->this$0:Lcom/android/bluetooth/opp/TestActivity;

    #@3b
    invoke-virtual {v2}, Lcom/android/bluetooth/opp/TestActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@42
    .line 313
    return-void
.end method
