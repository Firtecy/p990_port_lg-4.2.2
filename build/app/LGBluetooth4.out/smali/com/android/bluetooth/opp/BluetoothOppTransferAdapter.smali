.class public Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;
.super Landroid/widget/ResourceCursorAdapter;
.source "BluetoothOppTransferAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;)V
    .registers 4
    .parameter "context"
    .parameter "layout"
    .parameter "c"

    #@0
    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    #@3
    .line 62
    iput-object p1, p0, Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;->mContext:Landroid/content/Context;

    #@5
    .line 63
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .registers 29
    .parameter "view"
    .parameter "context"
    .parameter "cursor"

    #@0
    .prologue
    .line 67
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v11

    #@4
    .line 70
    .local v11, r:Landroid/content/res/Resources;
    const v21, 0x7f0b0002

    #@7
    move-object/from16 v0, p1

    #@9
    move/from16 v1, v21

    #@b
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@e
    move-result-object v10

    #@f
    check-cast v10, Landroid/widget/ImageView;

    #@11
    .line 71
    .local v10, iv:Landroid/widget/ImageView;
    const-string v21, "status"

    #@13
    move-object/from16 v0, p3

    #@15
    move-object/from16 v1, v21

    #@17
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@1a
    move-result v21

    #@1b
    move-object/from16 v0, p3

    #@1d
    move/from16 v1, v21

    #@1f
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@22
    move-result v13

    #@23
    .line 72
    .local v13, status:I
    const-string v21, "direction"

    #@25
    move-object/from16 v0, p3

    #@27
    move-object/from16 v1, v21

    #@29
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@2c
    move-result v21

    #@2d
    move-object/from16 v0, p3

    #@2f
    move/from16 v1, v21

    #@31
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    #@34
    move-result v9

    #@35
    .line 73
    .local v9, dir:I
    invoke-static {v13}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusError(I)Z

    #@38
    move-result v21

    #@39
    if-eqz v21, :cond_131

    #@3b
    .line 76
    const v21, 0x202026e

    #@3e
    move/from16 v0, v21

    #@40
    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    #@43
    .line 93
    :goto_43
    const v21, 0x7f0b0003

    #@46
    move-object/from16 v0, p1

    #@48
    move/from16 v1, v21

    #@4a
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@4d
    move-result-object v20

    #@4e
    check-cast v20, Landroid/widget/TextView;

    #@50
    .line 94
    .local v20, tv:Landroid/widget/TextView;
    const-string v21, "hint"

    #@52
    move-object/from16 v0, p3

    #@54
    move-object/from16 v1, v21

    #@56
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@59
    move-result v21

    #@5a
    move-object/from16 v0, p3

    #@5c
    move/from16 v1, v21

    #@5e
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@61
    move-result-object v17

    #@62
    .line 96
    .local v17, title:Ljava/lang/String;
    if-nez v17, :cond_71

    #@64
    .line 97
    move-object/from16 v0, p0

    #@66
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;->mContext:Landroid/content/Context;

    #@68
    move-object/from16 v21, v0

    #@6a
    const v22, 0x7f070039

    #@6d
    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@70
    move-result-object v17

    #@71
    .line 99
    :cond_71
    move-object/from16 v0, v20

    #@73
    move-object/from16 v1, v17

    #@75
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@78
    .line 102
    const v21, 0x7f0b0004

    #@7b
    move-object/from16 v0, p1

    #@7d
    move/from16 v1, v21

    #@7f
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@82
    move-result-object v20

    #@83
    .end local v20           #tv:Landroid/widget/TextView;
    check-cast v20, Landroid/widget/TextView;

    #@85
    .line 103
    .restart local v20       #tv:Landroid/widget/TextView;
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@88
    move-result-object v3

    #@89
    .line 104
    .local v3, adapter:Landroid/bluetooth/BluetoothAdapter;
    const-string v21, "destination"

    #@8b
    move-object/from16 v0, p3

    #@8d
    move-object/from16 v1, v21

    #@8f
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@92
    move-result v7

    #@93
    .line 105
    .local v7, destinationColumnId:I
    move-object/from16 v0, p3

    #@95
    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@98
    move-result-object v21

    #@99
    move-object/from16 v0, v21

    #@9b
    invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@9e
    move-result-object v12

    #@9f
    .line 107
    .local v12, remoteDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static/range {p2 .. p2}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@a2
    move-result-object v21

    #@a3
    move-object/from16 v0, v21

    #@a5
    invoke-virtual {v0, v12}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getDeviceName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@a8
    move-result-object v8

    #@a9
    .line 108
    .local v8, deviceName:Ljava/lang/String;
    move-object/from16 v0, v20

    #@ab
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@ae
    .line 111
    const-string v21, "total_bytes"

    #@b0
    move-object/from16 v0, p3

    #@b2
    move-object/from16 v1, v21

    #@b4
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@b7
    move-result v21

    #@b8
    move-object/from16 v0, p3

    #@ba
    move/from16 v1, v21

    #@bc
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    #@bf
    move-result-wide v18

    #@c0
    .line 112
    .local v18, totalBytes:J
    invoke-static {v13}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusCompleted(I)Z

    #@c3
    move-result v21

    #@c4
    if-eqz v21, :cond_130

    #@c6
    .line 113
    const v21, 0x7f0b0006

    #@c9
    move-object/from16 v0, p1

    #@cb
    move/from16 v1, v21

    #@cd
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@d0
    move-result-object v20

    #@d1
    .end local v20           #tv:Landroid/widget/TextView;
    check-cast v20, Landroid/widget/TextView;

    #@d3
    .line 114
    .restart local v20       #tv:Landroid/widget/TextView;
    const/16 v21, 0x0

    #@d5
    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setVisibility(I)V

    #@d8
    .line 115
    invoke-static {v13}, Lcom/android/bluetooth/opp/BluetoothShare;->isStatusError(I)Z

    #@db
    move-result v21

    #@dc
    if-eqz v21, :cond_147

    #@de
    .line 116
    move-object/from16 v0, p0

    #@e0
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;->mContext:Landroid/content/Context;

    #@e2
    move-object/from16 v21, v0

    #@e4
    move-object/from16 v0, v21

    #@e6
    invoke-static {v0, v13, v8}, Lcom/android/bluetooth/opp/BluetoothOppUtility;->getStatusDescription(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    #@e9
    move-result-object v21

    #@ea
    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@ed
    .line 129
    :goto_ed
    const-string v21, "timestamp"

    #@ef
    move-object/from16 v0, p3

    #@f1
    move-object/from16 v1, v21

    #@f3
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@f6
    move-result v6

    #@f7
    .line 130
    .local v6, dateColumnId:I
    move-object/from16 v0, p3

    #@f9
    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    #@fc
    move-result-wide v15

    #@fd
    .line 131
    .local v15, time:J
    new-instance v5, Ljava/util/Date;

    #@ff
    move-wide v0, v15

    #@100
    invoke-direct {v5, v0, v1}, Ljava/util/Date;-><init>(J)V

    #@103
    .line 132
    .local v5, d:Ljava/util/Date;
    invoke-static/range {v15 .. v16}, Landroid/text/format/DateUtils;->isToday(J)Z

    #@106
    move-result v21

    #@107
    if-eqz v21, :cond_19f

    #@109
    move-object/from16 v0, p0

    #@10b
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;->mContext:Landroid/content/Context;

    #@10d
    move-object/from16 v21, v0

    #@10f
    invoke-static/range {v21 .. v21}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    #@112
    move-result-object v21

    #@113
    move-object/from16 v0, v21

    #@115
    invoke-virtual {v0, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@118
    move-result-object v14

    #@119
    .line 134
    .local v14, str:Ljava/lang/CharSequence;
    :goto_119
    const v21, 0x7f0b0005

    #@11c
    move-object/from16 v0, p1

    #@11e
    move/from16 v1, v21

    #@120
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@123
    move-result-object v20

    #@124
    .end local v20           #tv:Landroid/widget/TextView;
    check-cast v20, Landroid/widget/TextView;

    #@126
    .line 135
    .restart local v20       #tv:Landroid/widget/TextView;
    const/16 v21, 0x0

    #@128
    invoke-virtual/range {v20 .. v21}, Landroid/widget/TextView;->setVisibility(I)V

    #@12b
    .line 136
    move-object/from16 v0, v20

    #@12d
    invoke-virtual {v0, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@130
    .line 138
    .end local v5           #d:Ljava/util/Date;
    .end local v6           #dateColumnId:I
    .end local v14           #str:Ljava/lang/CharSequence;
    .end local v15           #time:J
    :cond_130
    return-void

    #@131
    .line 79
    .end local v3           #adapter:Landroid/bluetooth/BluetoothAdapter;
    .end local v7           #destinationColumnId:I
    .end local v8           #deviceName:Ljava/lang/String;
    .end local v12           #remoteDevice:Landroid/bluetooth/BluetoothDevice;
    .end local v17           #title:Ljava/lang/String;
    .end local v18           #totalBytes:J
    .end local v20           #tv:Landroid/widget/TextView;
    :cond_131
    if-nez v9, :cond_13d

    #@133
    .line 82
    const v21, 0x7f02000a

    #@136
    move/from16 v0, v21

    #@138
    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    #@13b
    goto/16 :goto_43

    #@13d
    .line 87
    :cond_13d
    const v21, 0x7f020008

    #@140
    move/from16 v0, v21

    #@142
    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    #@145
    goto/16 :goto_43

    #@147
    .line 119
    .restart local v3       #adapter:Landroid/bluetooth/BluetoothAdapter;
    .restart local v7       #destinationColumnId:I
    .restart local v8       #deviceName:Ljava/lang/String;
    .restart local v12       #remoteDevice:Landroid/bluetooth/BluetoothDevice;
    .restart local v17       #title:Ljava/lang/String;
    .restart local v18       #totalBytes:J
    .restart local v20       #tv:Landroid/widget/TextView;
    :cond_147
    const/16 v21, 0x1

    #@149
    move/from16 v0, v21

    #@14b
    if-ne v9, v0, :cond_179

    #@14d
    .line 120
    const v21, 0x7f070054

    #@150
    const/16 v22, 0x1

    #@152
    move/from16 v0, v22

    #@154
    new-array v0, v0, [Ljava/lang/Object;

    #@156
    move-object/from16 v22, v0

    #@158
    const/16 v23, 0x0

    #@15a
    move-object/from16 v0, p0

    #@15c
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;->mContext:Landroid/content/Context;

    #@15e
    move-object/from16 v24, v0

    #@160
    move-object/from16 v0, v24

    #@162
    move-wide/from16 v1, v18

    #@164
    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@167
    move-result-object v24

    #@168
    aput-object v24, v22, v23

    #@16a
    move/from16 v0, v21

    #@16c
    move-object/from16 v1, v22

    #@16e
    invoke-virtual {v11, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@171
    move-result-object v4

    #@172
    .line 126
    .local v4, completeText:Ljava/lang/String;
    :goto_172
    move-object/from16 v0, v20

    #@174
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@177
    goto/16 :goto_ed

    #@179
    .line 123
    .end local v4           #completeText:Ljava/lang/String;
    :cond_179
    const v21, 0x7f070055

    #@17c
    const/16 v22, 0x1

    #@17e
    move/from16 v0, v22

    #@180
    new-array v0, v0, [Ljava/lang/Object;

    #@182
    move-object/from16 v22, v0

    #@184
    const/16 v23, 0x0

    #@186
    move-object/from16 v0, p0

    #@188
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;->mContext:Landroid/content/Context;

    #@18a
    move-object/from16 v24, v0

    #@18c
    move-object/from16 v0, v24

    #@18e
    move-wide/from16 v1, v18

    #@190
    invoke-static {v0, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    #@193
    move-result-object v24

    #@194
    aput-object v24, v22, v23

    #@196
    move/from16 v0, v21

    #@198
    move-object/from16 v1, v22

    #@19a
    invoke-virtual {v11, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@19d
    move-result-object v4

    #@19e
    .restart local v4       #completeText:Ljava/lang/String;
    goto :goto_172

    #@19f
    .line 132
    .end local v4           #completeText:Ljava/lang/String;
    .restart local v5       #d:Ljava/util/Date;
    .restart local v6       #dateColumnId:I
    .restart local v15       #time:J
    :cond_19f
    move-object/from16 v0, p0

    #@1a1
    iget-object v0, v0, Lcom/android/bluetooth/opp/BluetoothOppTransferAdapter;->mContext:Landroid/content/Context;

    #@1a3
    move-object/from16 v21, v0

    #@1a5
    invoke-static/range {v21 .. v21}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    #@1a8
    move-result-object v21

    #@1a9
    move-object/from16 v0, v21

    #@1ab
    invoke-virtual {v0, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    #@1ae
    move-result-object v14

    #@1af
    goto/16 :goto_119
.end method
