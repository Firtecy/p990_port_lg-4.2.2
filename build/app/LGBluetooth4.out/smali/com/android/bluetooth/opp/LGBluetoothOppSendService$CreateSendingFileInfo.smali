.class Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;
.super Ljava/lang/Thread;
.source "LGBluetoothOppSendService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/opp/LGBluetoothOppSendService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CreateSendingFileInfo"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/opp/LGBluetoothOppSendService;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/opp/LGBluetoothOppSendService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 66
    iput-object p1, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;->this$0:Lcom/android/bluetooth/opp/LGBluetoothOppSendService;

    #@2
    .line 67
    const-string v0, "Bluetooth Sending File Info"

    #@4
    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@7
    .line 68
    return-void
.end method


# virtual methods
.method public run()V
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    .line 72
    const/16 v8, 0xa

    #@3
    invoke-static {v8}, Landroid/os/Process;->setThreadPriority(I)V

    #@6
    .line 74
    iget-object v8, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;->this$0:Lcom/android/bluetooth/opp/LGBluetoothOppSendService;

    #@8
    invoke-static {v8}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->access$000(Lcom/android/bluetooth/opp/LGBluetoothOppSendService;)Landroid/content/Intent;

    #@b
    move-result-object v8

    #@c
    if-nez v8, :cond_1b

    #@e
    .line 75
    const-string v8, "LGBluetoothOppSendService"

    #@10
    const-string v9, "[BTUI] mHandler : intent is null"

    #@12
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 110
    :cond_15
    :goto_15
    iget-object v8, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;->this$0:Lcom/android/bluetooth/opp/LGBluetoothOppSendService;

    #@17
    invoke-virtual {v8}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->stopSelf()V

    #@1a
    .line 111
    return-void

    #@1b
    .line 77
    :cond_1b
    iget-object v8, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;->this$0:Lcom/android/bluetooth/opp/LGBluetoothOppSendService;

    #@1d
    invoke-static {v8}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->access$000(Lcom/android/bluetooth/opp/LGBluetoothOppSendService;)Landroid/content/Intent;

    #@20
    move-result-object v8

    #@21
    const-string v9, "intent"

    #@23
    invoke-virtual {v8, v9}, Landroid/content/Intent;->getExtra(Ljava/lang/String;)Ljava/lang/Object;

    #@26
    move-result-object v3

    #@27
    check-cast v3, Landroid/content/Intent;

    #@29
    .line 78
    .local v3, intentExtra:Landroid/content/Intent;
    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@2c
    move-result-object v0

    #@2d
    .line 79
    .local v0, action:Ljava/lang/String;
    const-string v8, "android.intent.action.SEND"

    #@2f
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v8

    #@33
    if-eqz v8, :cond_83

    #@35
    .line 80
    invoke-virtual {v3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@38
    move-result-object v6

    #@39
    .line 81
    .local v6, type:Ljava/lang/String;
    const-string v8, "android.intent.extra.STREAM"

    #@3b
    invoke-virtual {v3, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@3e
    move-result-object v5

    #@3f
    check-cast v5, Landroid/net/Uri;

    #@41
    .line 82
    .local v5, stream:Landroid/net/Uri;
    const-string v8, "android.intent.extra.TEXT"

    #@43
    invoke-virtual {v3, v8}, Landroid/content/Intent;->getCharSequenceExtra(Ljava/lang/String;)Ljava/lang/CharSequence;

    #@46
    move-result-object v1

    #@47
    .line 84
    .local v1, extra_text:Ljava/lang/CharSequence;
    if-eqz v5, :cond_5b

    #@49
    if-eqz v6, :cond_5b

    #@4b
    .line 85
    invoke-static {}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->access$100()Landroid/content/Context;

    #@4e
    move-result-object v8

    #@4f
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@52
    move-result-object v8

    #@53
    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@56
    move-result-object v9

    #@57
    invoke-virtual {v8, v6, v9, v10}, Lcom/android/bluetooth/opp/BluetoothOppManager;->saveSendingFileInfo(Ljava/lang/String;Ljava/lang/String;Z)V

    #@5a
    goto :goto_15

    #@5b
    .line 87
    :cond_5b
    if-eqz v1, :cond_15

    #@5d
    if-eqz v6, :cond_15

    #@5f
    .line 88
    iget-object v8, p0, Lcom/android/bluetooth/opp/LGBluetoothOppSendService$CreateSendingFileInfo;->this$0:Lcom/android/bluetooth/opp/LGBluetoothOppSendService;

    #@61
    invoke-static {}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->access$100()Landroid/content/Context;

    #@64
    move-result-object v9

    #@65
    invoke-static {v8, v9, v1}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->access$200(Lcom/android/bluetooth/opp/LGBluetoothOppSendService;Landroid/content/Context;Ljava/lang/CharSequence;)Landroid/net/Uri;

    #@68
    move-result-object v2

    #@69
    .line 90
    .local v2, fileUri:Landroid/net/Uri;
    if-eqz v2, :cond_7b

    #@6b
    .line 91
    invoke-static {}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->access$100()Landroid/content/Context;

    #@6e
    move-result-object v8

    #@6f
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@72
    move-result-object v8

    #@73
    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@76
    move-result-object v9

    #@77
    invoke-virtual {v8, v6, v9, v10}, Lcom/android/bluetooth/opp/BluetoothOppManager;->saveSendingFileInfo(Ljava/lang/String;Ljava/lang/String;Z)V

    #@7a
    goto :goto_15

    #@7b
    .line 94
    :cond_7b
    const-string v8, "LGBluetoothOppSendService"

    #@7d
    const-string v9, "[BTUI] onStart() fileUri is null"

    #@7f
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    goto :goto_15

    #@83
    .line 98
    .end local v1           #extra_text:Ljava/lang/CharSequence;
    .end local v2           #fileUri:Landroid/net/Uri;
    .end local v5           #stream:Landroid/net/Uri;
    .end local v6           #type:Ljava/lang/String;
    :cond_83
    const-string v8, "android.intent.action.SEND_MULTIPLE"

    #@85
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@88
    move-result v8

    #@89
    if-eqz v8, :cond_ad

    #@8b
    .line 99
    invoke-virtual {v3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@8e
    move-result-object v4

    #@8f
    .line 100
    .local v4, mimeType:Ljava/lang/String;
    const-string v8, "android.intent.extra.STREAM"

    #@91
    invoke-virtual {v3, v8}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    #@94
    move-result-object v7

    #@95
    .line 101
    .local v7, uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v7, :cond_a4

    #@97
    .line 102
    invoke-static {}, Lcom/android/bluetooth/opp/LGBluetoothOppSendService;->access$100()Landroid/content/Context;

    #@9a
    move-result-object v8

    #@9b
    invoke-static {v8}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@9e
    move-result-object v8

    #@9f
    invoke-virtual {v8, v4, v7, v10}, Lcom/android/bluetooth/opp/BluetoothOppManager;->saveSendingFileInfo(Ljava/lang/String;Ljava/util/ArrayList;Z)V

    #@a2
    goto/16 :goto_15

    #@a4
    .line 104
    :cond_a4
    const-string v8, "LGBluetoothOppSendService"

    #@a6
    const-string v9, "[BTUI] onStart() uris is null"

    #@a8
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    goto/16 :goto_15

    #@ad
    .line 107
    .end local v4           #mimeType:Ljava/lang/String;
    .end local v7           #uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_ad
    const-string v8, "LGBluetoothOppSendService"

    #@af
    new-instance v9, Ljava/lang/StringBuilder;

    #@b1
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@b4
    const-string v10, "[BTUI] no action on this service, action = "

    #@b6
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v9

    #@ba
    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v9

    #@be
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c1
    move-result-object v9

    #@c2
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c5
    goto/16 :goto_15
.end method
