.class Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;
.super Landroid/os/Handler;
.source "A2dpStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/a2dp/A2dpStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IntentBroadcastHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1059
    iput-object p1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 1059
    invoke-direct {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;-><init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)V

    #@3
    return-void
.end method

.method private onConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;II)V
    .registers 10
    .parameter "device"
    .parameter "prevState"
    .parameter "state"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v5, 0x0

    #@2
    .line 1062
    new-instance v0, Landroid/content/Intent;

    #@4
    const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    #@6
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9
    .line 1063
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    #@b
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@e
    .line 1064
    const-string v1, "android.bluetooth.profile.extra.STATE"

    #@10
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@13
    .line 1065
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    #@15
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@18
    .line 1066
    const/high16 v1, 0x800

    #@1a
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@1d
    .line 1067
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@1f
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$4200(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/content/Context;

    #@22
    move-result-object v1

    #@23
    const-string v2, "android.permission.BLUETOOTH"

    #@25
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@28
    .line 1068
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "Connection state "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    const-string v3, ": "

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    const-string v3, "->"

    #@45
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v2

    #@4d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    #calls: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->log(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$1200(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Ljava/lang/String;)V

    #@54
    .line 1071
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@56
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;
    invoke-static {v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$4800(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@59
    invoke-static {p1, p2, p3}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->checkBlockAvrcpToggle(Landroid/bluetooth/BluetoothDevice;II)V

    #@5c
    .line 1073
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@5e
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mService:Lcom/android/bluetooth/a2dp/A2dpService;
    invoke-static {v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$4900(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/android/bluetooth/a2dp/A2dpService;

    #@61
    move-result-object v1

    #@62
    invoke-virtual {v1, p1, v4, p3, p2}, Lcom/android/bluetooth/a2dp/A2dpService;->notifyProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V

    #@65
    .line 1076
    if-ne p3, v4, :cond_7d

    #@67
    .line 1077
    invoke-static {}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getDefaultChecker()Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@6a
    move-result-object v1

    #@6b
    iget-object v2, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@6d
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$4200(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/content/Context;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@74
    move-result-object v3

    #@75
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->setConnected(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Z

    #@7c
    .line 1084
    :cond_7c
    :goto_7c
    return-void

    #@7d
    .line 1079
    :cond_7d
    if-nez p3, :cond_7c

    #@7f
    .line 1080
    invoke-static {}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getDefaultChecker()Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@82
    move-result-object v1

    #@83
    iget-object v2, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@85
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$4200(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/content/Context;

    #@88
    move-result-object v2

    #@89
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@8c
    move-result-object v3

    #@8d
    invoke-virtual {v1, v2, v5, v3}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->setDisconnected(Landroid/content/Context;ILjava/lang/String;)Z

    #@90
    goto :goto_7c
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 1088
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v0, :pswitch_data_1c

    #@5
    .line 1096
    :goto_5
    return-void

    #@6
    .line 1090
    :pswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@a
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@c
    iget v2, p1, Landroid/os/Message;->arg2:I

    #@e
    invoke-direct {p0, v0, v1, v2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->onConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;II)V

    #@11
    .line 1091
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@13
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mWakeLock:Landroid/os/PowerManager$WakeLock;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$5000(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/os/PowerManager$WakeLock;

    #@16
    move-result-object v0

    #@17
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1a
    goto :goto_5

    #@1b
    .line 1088
    nop

    #@1c
    :pswitch_data_1c
    .packed-switch 0x0
        :pswitch_6
    .end packed-switch
.end method
