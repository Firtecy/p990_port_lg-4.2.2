.class final Lcom/android/bluetooth/a2dp/A2dpStateMachine;
.super Lcom/android/internal/util/StateMachine;
.source "A2dpStateMachine.java"

# interfaces
.implements Lcom/lge/bluetooth/LGBluetoothAvrcpManager$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;,
        Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;,
        Lcom/android/bluetooth/a2dp/A2dpStateMachine$Connected;,
        Lcom/android/bluetooth/a2dp/A2dpStateMachine$Pending;,
        Lcom/android/bluetooth/a2dp/A2dpStateMachine$Disconnected;
    }
.end annotation


# static fields
.field private static final A2DP_UUIDS:[Landroid/os/ParcelUuid; = null

.field static final AUDIO_STATE_REMOTE_SUSPEND:I = 0x0

.field static final AUDIO_STATE_STARTED:I = 0x2

.field static final AUDIO_STATE_STOPPED:I = 0x1

.field private static final AVRCP_HANDLER_EVENT_TIMOUT:I = 0x96

.field private static final AVRCP_PLAY_POSITION_DEFAULT_DELAY:I = 0x7d0

.field static final CONNECT:I = 0x1

.field static final CONNECTION_STATE_CONNECTED:I = 0x2

.field static final CONNECTION_STATE_CONNECTING:I = 0x1

.field static final CONNECTION_STATE_DISCONNECTED:I = 0x0

.field static final CONNECTION_STATE_DISCONNECTING:I = 0x3

.field private static final CONNECT_TIMEOUT:I = 0xc9

.field private static final DBG:Z = true

.field static final DISCONNECT:I = 0x2

.field private static final EVENT_TYPE_AUDIO_STATE_CHANGED:I = 0x2

.field private static final EVENT_TYPE_CONNECTION_STATE_CHANGED:I = 0x1

.field private static final EVENT_TYPE_NONE:I = 0x0

.field private static final MSG_CONNECTION_STATE_CHANGED:I = 0x0

.field private static final STACK_EVENT:I = 0x65

.field private static final TAG:Ljava/lang/String; = "A2dpStateMachine"

.field private static mCPType:I

.field private static mPlayPositionRequested:Z

.field private static mPlayStateRequested:Z

.field private static mReceiverRegistered:Z


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mAudioManager:Landroid/media/AudioManager;

.field private mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

.field private mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

.field private mConnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Connected;

.field private mContext:Landroid/content/Context;

.field private mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

.field private mDisconnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Disconnected;

.field private mHandler:Landroid/os/Handler;

.field private mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

.field private mIntentBroadcastHandler:Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;

.field private mPending:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Pending;

.field private mPlayingA2dpDevice:Landroid/bluetooth/BluetoothDevice;

.field private mService:Lcom/android/bluetooth/a2dp/A2dpService;

.field private mTargetDevice:Landroid/bluetooth/BluetoothDevice;

.field private final mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 110
    new-array v0, v3, [Landroid/os/ParcelUuid;

    #@4
    sget-object v1, Landroid/bluetooth/BluetoothUuid;->AudioSink:Landroid/os/ParcelUuid;

    #@6
    aput-object v1, v0, v2

    #@8
    sput-object v0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->A2DP_UUIDS:[Landroid/os/ParcelUuid;

    #@a
    .line 143
    sput v3, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mCPType:I

    #@c
    .line 146
    sput-boolean v2, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mReceiverRegistered:Z

    #@e
    .line 147
    sput-boolean v2, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayStateRequested:Z

    #@10
    .line 148
    sput-boolean v2, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayPositionRequested:Z

    #@12
    .line 152
    invoke-static {}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->classInitNative()V

    #@15
    .line 154
    invoke-static {}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->classInitNativeAVRCP()V

    #@18
    .line 156
    return-void
.end method

.method private constructor <init>(Lcom/android/bluetooth/a2dp/A2dpService;Landroid/content/Context;)V
    .registers 7
    .parameter "svc"
    .parameter "context"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 159
    const-string v1, "A2dpStateMachine"

    #@3
    invoke-direct {p0, v1}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    #@6
    .line 136
    iput-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@8
    .line 137
    iput-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@a
    .line 138
    iput-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

    #@c
    .line 139
    iput-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayingA2dpDevice:Landroid/bluetooth/BluetoothDevice;

    #@e
    .line 202
    new-instance v1, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;

    #@10
    invoke-direct {v1, p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;-><init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)V

    #@13
    iput-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mHandler:Landroid/os/Handler;

    #@15
    .line 160
    iput-object p1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mService:Lcom/android/bluetooth/a2dp/A2dpService;

    #@17
    .line 161
    iput-object p2, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mContext:Landroid/content/Context;

    #@19
    .line 162
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@1c
    move-result-object v1

    #@1d
    iput-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@1f
    .line 164
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->initNative()V

    #@22
    .line 166
    new-instance v1, Lcom/android/bluetooth/a2dp/A2dpStateMachine$Disconnected;

    #@24
    invoke-direct {v1, p0, v3}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$Disconnected;-><init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;)V

    #@27
    iput-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mDisconnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Disconnected;

    #@29
    .line 167
    new-instance v1, Lcom/android/bluetooth/a2dp/A2dpStateMachine$Pending;

    #@2b
    invoke-direct {v1, p0, v3}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$Pending;-><init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;)V

    #@2e
    iput-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPending:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Pending;

    #@30
    .line 168
    new-instance v1, Lcom/android/bluetooth/a2dp/A2dpStateMachine$Connected;

    #@32
    invoke-direct {v1, p0, v3}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$Connected;-><init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;)V

    #@35
    iput-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mConnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Connected;

    #@37
    .line 170
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mDisconnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Disconnected;

    #@39
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@3c
    .line 171
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPending:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Pending;

    #@3e
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@41
    .line 172
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mConnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Connected;

    #@43
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@46
    .line 174
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mDisconnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Disconnected;

    #@48
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    #@4b
    .line 176
    const-string v1, "power"

    #@4d
    invoke-virtual {p2, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@50
    move-result-object v0

    #@51
    check-cast v0, Landroid/os/PowerManager;

    #@53
    .line 177
    .local v0, pm:Landroid/os/PowerManager;
    const/4 v1, 0x1

    #@54
    const-string v2, "BluetoothA2dpService"

    #@56
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@59
    move-result-object v1

    #@5a
    iput-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@5c
    .line 179
    new-instance v1, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;

    #@5e
    invoke-direct {v1, p0, v3}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;-><init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;)V

    #@61
    iput-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mIntentBroadcastHandler:Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;

    #@63
    .line 181
    invoke-static {p2}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getInstance(Landroid/content/Context;)Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@66
    move-result-object v1

    #@67
    iput-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@69
    .line 184
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@6b
    if-eqz v1, :cond_72

    #@6d
    .line 185
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@6f
    invoke-virtual {v1, p0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->registerCallback(Lcom/lge/bluetooth/LGBluetoothAvrcpManager$Callback;)V

    #@72
    .line 188
    :cond_72
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@74
    invoke-virtual {v1}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->initPlayStatus()V

    #@77
    .line 192
    new-instance v1, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@79
    invoke-direct {v1, p2}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;-><init>(Landroid/content/Context;)V

    #@7c
    iput-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@7e
    .line 195
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->initNativeAVRCP()V

    #@81
    .line 198
    const-string v1, "audio"

    #@83
    invoke-virtual {p2, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@86
    move-result-object v1

    #@87
    check-cast v1, Landroid/media/AudioManager;

    #@89
    iput-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@8b
    .line 200
    return-void
.end method

.method static synthetic access$1000(II)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-static {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onApplicationSettingsChangedNative(II)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->log(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$1302(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    iput-object p1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$1400(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    iput-object p1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$1500(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$1502(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    iput-object p1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$1600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Landroid/bluetooth/BluetoothDevice;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;II)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Landroid/bluetooth/BluetoothDevice;)[B
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/bluetooth/a2dp/A2dpStateMachine;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->connectA2dpNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1900(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/android/bluetooth/a2dp/A2dpStateMachine$Pending;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPending:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Pending;

    #@2
    return-object v0
.end method

.method static synthetic access$2000(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2200(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2300(Lcom/android/bluetooth/a2dp/A2dpStateMachine;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->disconnectA2dpNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2400(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/android/bluetooth/a2dp/A2dpStateMachine$Connected;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mConnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Connected;

    #@2
    return-object v0
.end method

.method static synthetic access$2500(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2700(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/bluetooth/a2dp/A2dpStateMachine;I[B)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onConnectionStateChanged(I[B)V

    #@3
    return-void
.end method

.method static synthetic access$2900(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$3000(Lcom/android/bluetooth/a2dp/A2dpStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->removeMessages(I)V

    #@3
    return-void
.end method

.method static synthetic access$3100(Lcom/android/bluetooth/a2dp/A2dpStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->removeMessages(I)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/android/bluetooth/a2dp/A2dpStateMachine$Disconnected;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mDisconnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Disconnected;

    #@2
    return-object v0
.end method

.method static synthetic access$3300(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3400(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3500(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3700(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3800(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 76
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->playPositionStatusRequest()V

    #@3
    return-void
.end method

.method static synthetic access$4000(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4100(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$4200(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/content/Context;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mContext:Landroid/content/Context;

    #@2
    return-object v0
.end method

.method static synthetic access$4300(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Landroid/bluetooth/BluetoothDevice;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 76
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->broadcastAudioState(Landroid/bluetooth/BluetoothDevice;II)V

    #@3
    return-void
.end method

.method static synthetic access$4400(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4500(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4800(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/lge/bluetooth/LGBluetoothAvrcpManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    return-object v0
.end method

.method static synthetic access$4900(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/android/bluetooth/a2dp/A2dpService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mService:Lcom/android/bluetooth/a2dp/A2dpService;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayingA2dpDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$5000(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/bluetooth/a2dp/A2dpStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 76
    iput-object p1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayingA2dpDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@2
    return-object v0
.end method

.method static synthetic access$700(I)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 76
    invoke-static {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onPlayStateChangedNative(I)V

    #@3
    return-void
.end method

.method static synthetic access$800(J)V
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 76
    invoke-static {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onSongPositionChangedNative(J)V

    #@3
    return-void
.end method

.method static synthetic access$900(IJJ)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 76
    invoke-static {p0, p1, p2, p3, p4}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onPlayStatusAvailableNative(IJJ)V

    #@3
    return-void
.end method

.method private broadcastAudioState(Landroid/bluetooth/BluetoothDevice;II)V
    .registers 7
    .parameter "device"
    .parameter "state"
    .parameter "prevState"

    #@0
    .prologue
    .line 838
    sput p2, Lcom/android/bluetooth/a2dp/A2dpService;->mPlayingStatus:I

    #@2
    .line 840
    new-instance v0, Landroid/content/Intent;

    #@4
    const-string v1, "android.bluetooth.a2dp.profile.action.PLAYING_STATE_CHANGED"

    #@6
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@9
    .line 841
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    #@b
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@e
    .line 842
    const-string v1, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    #@10
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@13
    .line 843
    const-string v1, "android.bluetooth.profile.extra.STATE"

    #@15
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@18
    .line 844
    const/high16 v1, 0x800

    #@1a
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@1d
    .line 845
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mContext:Landroid/content/Context;

    #@1f
    const-string v2, "android.permission.BLUETOOTH"

    #@21
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@24
    .line 847
    new-instance v1, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v2, "A2DP Playing state : device: "

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    const-string v2, " State:"

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, "->"

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-direct {p0, v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->log(Ljava/lang/String;)V

    #@4e
    .line 848
    return-void
.end method

.method private broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;II)V
    .registers 9
    .parameter "device"
    .parameter "newState"
    .parameter "prevState"

    #@0
    .prologue
    .line 825
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@2
    invoke-virtual {v1, p1, p2}, Landroid/media/AudioManager;->setBluetoothA2dpDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;I)I

    #@5
    move-result v0

    #@6
    .line 827
    .local v0, delay:I
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@8
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@b
    .line 828
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mIntentBroadcastHandler:Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;

    #@d
    iget-object v2, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mIntentBroadcastHandler:Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;

    #@f
    const/4 v3, 0x0

    #@10
    invoke-virtual {v2, v3, p3, p2, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    #@13
    move-result-object v2

    #@14
    int-to-long v3, v0

    #@15
    invoke-virtual {v1, v2, v3, v4}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$IntentBroadcastHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@18
    .line 834
    return-void
.end method

.method private static native classInitNative()V
.end method

.method private static native classInitNativeAVRCP()V
.end method

.method private native cleanupNative()V
.end method

.method private native cleanupNativeAVRCP()V
.end method

.method private native configureCPNative(I)Z
.end method

.method private native connectA2dpNative([B)Z
.end method

.method private native disconnectA2dpNative([B)Z
.end method

.method private getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 851
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private getDevice([B)Landroid/bluetooth/BluetoothDevice;
    .registers 4
    .parameter "address"

    #@0
    .prologue
    .line 879
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method private native initNative()V
.end method

.method private native initNativeAVRCP()V
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 1046
    const-string v0, "A2dpStateMachine"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 1047
    return-void
.end method

.method static make(Lcom/android/bluetooth/a2dp/A2dpService;Landroid/content/Context;)Lcom/android/bluetooth/a2dp/A2dpStateMachine;
    .registers 5
    .parameter "svc"
    .parameter "context"

    #@0
    .prologue
    .line 257
    const-string v1, "A2dpStateMachine"

    #@2
    const-string v2, "make"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 258
    new-instance v0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@9
    invoke-direct {v0, p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;-><init>(Lcom/android/bluetooth/a2dp/A2dpService;Landroid/content/Context;)V

    #@c
    .line 259
    .local v0, a2dpSm:Lcom/android/bluetooth/a2dp/A2dpStateMachine;
    invoke-virtual {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->start()V

    #@f
    .line 260
    return-object v0
.end method

.method private static native onApplicationSettingsChangedNative(II)V
.end method

.method private onAudioStateChanged(I[B)V
    .registers 6
    .parameter "state"
    .parameter "address"

    #@0
    .prologue
    .line 862
    new-instance v0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;

    #@2
    const/4 v1, 0x2

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;ILcom/android/bluetooth/a2dp/A2dpStateMachine$1;)V

    #@7
    .line 863
    .local v0, event:Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;
    iput p1, v0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;->valueInt:I

    #@9
    .line 864
    invoke-direct {p0, p2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@c
    move-result-object v1

    #@d
    iput-object v1, v0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;->device:Landroid/bluetooth/BluetoothDevice;

    #@f
    .line 865
    const/16 v1, 0x65

    #@11
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@14
    .line 866
    return-void
.end method

.method private onCPTypeCallback(ZI)V
    .registers 6
    .parameter "cp_enabled"
    .parameter "cp_type"

    #@0
    .prologue
    .line 872
    const-string v0, "A2dpStateMachine"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[BTUI] onCPTypeCallback : cp_enabled = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", type = "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 873
    const-string v0, "A2dpStateMachine"

    #@24
    new-instance v1, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v2, "[BTUI] onCPTypeCallback : mCPType = "

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    sget v2, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mCPType:I

    #@31
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@34
    move-result-object v1

    #@35
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@38
    move-result-object v1

    #@39
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 874
    return-void
.end method

.method private onConnectionStateChanged(I[B)V
    .registers 6
    .parameter "state"
    .parameter "address"

    #@0
    .prologue
    .line 855
    new-instance v0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;ILcom/android/bluetooth/a2dp/A2dpStateMachine$1;)V

    #@7
    .line 856
    .local v0, event:Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;
    iput p1, v0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;->valueInt:I

    #@9
    .line 857
    invoke-direct {p0, p2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@c
    move-result-object v1

    #@d
    iput-object v1, v0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$StackEvent;->device:Landroid/bluetooth/BluetoothDevice;

    #@f
    .line 858
    const/16 v1, 0x65

    #@11
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@14
    .line 859
    return-void
.end method

.method private native onMediaPlayerStateChangedNative(I)V
.end method

.method private static native onPlayStateChangedNative(I)V
.end method

.method private static native onPlayStatusAvailableNative(IJJ)V
.end method

.method private static native onSongPositionChangedNative(J)V
.end method

.method private static native onTrackChangedNative(J)V
.end method

.method private playPositionStatusRequest()V
    .registers 2

    #@0
    .prologue
    .line 1038
    sget-boolean v0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayPositionRequested:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1039
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@6
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->cmdNotifyEventPlaybackPosChanged()V

    #@9
    .line 1042
    :cond_9
    return-void
.end method

.method private stopPlayPosTimer()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1025
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mHandler:Landroid/os/Handler;

    #@3
    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_15

    #@9
    .line 1026
    const-string v0, "A2dpStateMachine"

    #@b
    const-string v1, "Stopping play position changed timer"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1027
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mHandler:Landroid/os/Handler;

    #@12
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@15
    .line 1029
    :cond_15
    return-void
.end method


# virtual methods
.method public callbackAppSettingsChanged(II)V
    .registers 3
    .parameter "repeat"
    .parameter "shuffle"

    #@0
    .prologue
    .line 916
    invoke-static {p1, p2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onApplicationSettingsChangedNative(II)V

    #@3
    .line 917
    return-void
.end method

.method public callbackContentProtectionType(I)V
    .registers 5
    .parameter "cp_type"

    #@0
    .prologue
    .line 922
    const-string v0, "A2dpStateMachine"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[BTUI] callbackContentProtectionType : cp_type = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 923
    sput p1, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mCPType:I

    #@1a
    .line 924
    invoke-direct {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->configureCPNative(I)Z

    #@1d
    .line 925
    return-void
.end method

.method public callbackGetPlayStatusResponse(IJJ)V
    .registers 6
    .parameter "playStatus"
    .parameter "duration"
    .parameter "position"

    #@0
    .prologue
    .line 900
    invoke-static {p1, p2, p3, p4, p5}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onPlayStatusAvailableNative(IJJ)V

    #@3
    .line 901
    return-void
.end method

.method public callbackMediaPlayerStateChanged(I)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 930
    invoke-direct {p0, p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onMediaPlayerStateChangedNative(I)V

    #@3
    .line 931
    return-void
.end method

.method public callbackPlaybackPosChanged(J)V
    .registers 3
    .parameter "position"

    #@0
    .prologue
    .line 912
    invoke-static {p1, p2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onSongPositionChangedNative(J)V

    #@3
    .line 913
    return-void
.end method

.method public callbackPlaybackStatusChanged(I)V
    .registers 2
    .parameter "playStatus"

    #@0
    .prologue
    .line 904
    invoke-static {p1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onPlayStateChangedNative(I)V

    #@3
    .line 905
    return-void
.end method

.method public callbackTrackChanged(J)V
    .registers 3
    .parameter "trackNum"

    #@0
    .prologue
    .line 908
    invoke-static {p1, p2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onTrackChangedNative(J)V

    #@3
    .line 909
    return-void
.end method

.method public cleanup()V
    .registers 2

    #@0
    .prologue
    .line 269
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 270
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@6
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->terminateAvrcpManager()V

    #@9
    .line 274
    :cond_9
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 275
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@f
    invoke-virtual {v0, p0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->unregisterCallback(Lcom/lge/bluetooth/LGBluetoothAvrcpManager$Callback;)V

    #@12
    .line 281
    :cond_12
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->cleanupNative()V

    #@15
    .line 283
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->cleanupNativeAVRCP()V

    #@18
    .line 284
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@1a
    invoke-virtual {v0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->cleanup()V

    #@1d
    .line 286
    return-void
.end method

.method public doQuit()V
    .registers 1

    #@0
    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->quitNow()V

    #@3
    .line 265
    return-void
.end method

.method public getAlbumName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 944
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getAlbumName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getArtistName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 940
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getArtistName()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method getConnectedDevices()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 762
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 763
    .local v0, devices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    monitor-enter p0

    #@6
    .line 764
    :try_start_6
    invoke-virtual {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@9
    move-result-object v1

    #@a
    iget-object v2, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mConnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Connected;

    #@c
    if-ne v1, v2, :cond_13

    #@e
    .line 765
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@10
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@13
    .line 767
    :cond_13
    monitor-exit p0

    #@14
    .line 768
    return-object v0

    #@15
    .line 767
    :catchall_15
    move-exception v1

    #@16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_6 .. :try_end_17} :catchall_15

    #@17
    throw v1
.end method

.method getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 7
    .parameter "device"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 730
    invoke-virtual {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@5
    move-result-object v3

    #@6
    iget-object v4, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mDisconnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Disconnected;

    #@8
    if-ne v3, v4, :cond_b

    #@a
    .line 756
    :goto_a
    return v1

    #@b
    .line 734
    :cond_b
    monitor-enter p0

    #@c
    .line 735
    :try_start_c
    invoke-virtual {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@f
    move-result-object v0

    #@10
    .line 736
    .local v0, currentState:Lcom/android/internal/util/IState;
    iget-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPending:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Pending;

    #@12
    if-ne v0, v3, :cond_46

    #@14
    .line 737
    iget-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@16
    if-eqz v3, :cond_23

    #@18
    iget-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@1a
    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_23

    #@20
    .line 738
    monitor-exit p0

    #@21
    move v1, v2

    #@22
    goto :goto_a

    #@23
    .line 740
    :cond_23
    iget-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@25
    if-eqz v3, :cond_35

    #@27
    iget-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@29
    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_35

    #@2f
    .line 741
    const/4 v1, 0x3

    #@30
    monitor-exit p0

    #@31
    goto :goto_a

    #@32
    .line 758
    .end local v0           #currentState:Lcom/android/internal/util/IState;
    :catchall_32
    move-exception v1

    #@33
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_c .. :try_end_34} :catchall_32

    #@34
    throw v1

    #@35
    .line 743
    .restart local v0       #currentState:Lcom/android/internal/util/IState;
    :cond_35
    :try_start_35
    iget-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

    #@37
    if-eqz v3, :cond_44

    #@39
    iget-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

    #@3b
    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_44

    #@41
    .line 744
    monitor-exit p0

    #@42
    move v1, v2

    #@43
    goto :goto_a

    #@44
    .line 746
    :cond_44
    monitor-exit p0

    #@45
    goto :goto_a

    #@46
    .line 749
    :cond_46
    iget-object v2, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mConnected:Lcom/android/bluetooth/a2dp/A2dpStateMachine$Connected;

    #@48
    if-ne v0, v2, :cond_57

    #@4a
    .line 750
    iget-object v2, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@4c
    invoke-virtual {v2, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@4f
    move-result v2

    #@50
    if-eqz v2, :cond_55

    #@52
    .line 751
    const/4 v1, 0x2

    #@53
    monitor-exit p0

    #@54
    goto :goto_a

    #@55
    .line 753
    :cond_55
    monitor-exit p0

    #@56
    goto :goto_a

    #@57
    .line 755
    :cond_57
    const-string v2, "A2dpStateMachine"

    #@59
    new-instance v3, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v4, "Bad currentState: "

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6b
    move-result-object v3

    #@6c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 756
    monitor-exit p0
    :try_end_70
    .catchall {:try_start_35 .. :try_end_70} :catchall_32

    #@70
    goto :goto_a
.end method

.method declared-synchronized getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 10
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 802
    monitor-enter p0

    #@1
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    #@3
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@6
    .line 803
    .local v3, deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    iget-object v7, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@8
    invoke-virtual {v7}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    #@b
    move-result-object v0

    #@c
    .line 806
    .local v0, bondedDevices:Ljava/util/Set;,"Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v6

    #@10
    .local v6, i$:Ljava/util/Iterator;
    :cond_10
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v7

    #@14
    if-eqz v7, :cond_3a

    #@16
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@1c
    .line 807
    .local v2, device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getUuids()[Landroid/os/ParcelUuid;

    #@1f
    move-result-object v4

    #@20
    .line 808
    .local v4, featureUuids:[Landroid/os/ParcelUuid;
    sget-object v7, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->A2DP_UUIDS:[Landroid/os/ParcelUuid;

    #@22
    invoke-static {v4, v7}, Landroid/bluetooth/BluetoothUuid;->containsAnyUuid([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;)Z

    #@25
    move-result v7

    #@26
    if-eqz v7, :cond_10

    #@28
    .line 811
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@2b
    move-result v1

    #@2c
    .line 812
    .local v1, connectionState:I
    const/4 v5, 0x0

    #@2d
    .local v5, i:I
    :goto_2d
    array-length v7, p1

    #@2e
    if-ge v5, v7, :cond_10

    #@30
    .line 813
    aget v7, p1, v5

    #@32
    if-ne v1, v7, :cond_37

    #@34
    .line 814
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_37
    .catchall {:try_start_1 .. :try_end_37} :catchall_3c

    #@37
    .line 812
    :cond_37
    add-int/lit8 v5, v5, 0x1

    #@39
    goto :goto_2d

    #@3a
    .line 818
    .end local v1           #connectionState:I
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #featureUuids:[Landroid/os/ParcelUuid;
    .end local v5           #i:I
    :cond_3a
    monitor-exit p0

    #@3b
    return-object v3

    #@3c
    .line 802
    .end local v0           #bondedDevices:Ljava/util/Set;,"Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v3           #deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v6           #i$:Ljava/util/Iterator;
    :catchall_3c
    move-exception v7

    #@3d
    monitor-exit p0

    #@3e
    throw v7
.end method

.method public getDuration()J
    .registers 3

    #@0
    .prologue
    .line 956
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getDuration()J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getDurationText()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 960
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getDurationText()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getPlayPosition()J
    .registers 3

    #@0
    .prologue
    .line 964
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getPlayPosition()J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method public getPlayStatus()I
    .registers 2

    #@0
    .prologue
    .line 968
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getPlayStatus()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getRepeatMode()I
    .registers 2

    #@0
    .prologue
    .line 976
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->cmdStatusGetCurrentPlayerAppSettingValueRepeat()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getShuffleMode()I
    .registers 2

    #@0
    .prologue
    .line 984
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->cmdStatusGetCurrentPlayerAppSettingValueShuffle()I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 936
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getTitle()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getTotalTracks()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 952
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getTotalTracksString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getTrackNum()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 948
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getTrackNumString()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public getTrackNumLong()J
    .registers 3

    #@0
    .prologue
    .line 972
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getTrackNumLong()J

    #@5
    move-result-wide v0

    #@6
    return-wide v0
.end method

.method isPlaying(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 772
    monitor-enter p0

    #@1
    .line 773
    :try_start_1
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayingA2dpDevice:Landroid/bluetooth/BluetoothDevice;

    #@3
    invoke-virtual {p1, v0}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_c

    #@9
    .line 774
    const/4 v0, 0x1

    #@a
    monitor-exit p0

    #@b
    .line 777
    :goto_b
    return v0

    #@c
    .line 776
    :cond_c
    monitor-exit p0

    #@d
    .line 777
    const/4 v0, 0x0

    #@e
    goto :goto_b

    #@f
    .line 776
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0
    :try_end_11
    .catchall {:try_start_1 .. :try_end_11} :catchall_f

    #@11
    throw v0
.end method

.method okToConnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 7
    .parameter "device"

    #@0
    .prologue
    .line 781
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 782
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    iget-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mService:Lcom/android/bluetooth/a2dp/A2dpService;

    #@6
    invoke-virtual {v3, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@9
    move-result v1

    #@a
    .line 783
    .local v1, priority:I
    const/4 v2, 0x0

    #@b
    .line 785
    .local v2, ret:Z
    if-eqz v0, :cond_18

    #@d
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->isQuietModeEnabled()Z

    #@10
    move-result v3

    #@11
    const/4 v4, 0x1

    #@12
    if-ne v3, v4, :cond_1a

    #@14
    iget-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@16
    if-nez v3, :cond_1a

    #@18
    .line 788
    :cond_18
    const/4 v2, 0x0

    #@19
    .line 798
    :cond_19
    :goto_19
    return v2

    #@1a
    .line 793
    :cond_1a
    if-gtz v1, :cond_27

    #@1c
    const/4 v3, -0x1

    #@1d
    if-ne v3, v1, :cond_19

    #@1f
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    #@22
    move-result v3

    #@23
    const/16 v4, 0xa

    #@25
    if-eq v3, v4, :cond_19

    #@27
    .line 796
    :cond_27
    const/4 v2, 0x1

    #@28
    goto :goto_19
.end method

.method public playStatusRequest()V
    .registers 2

    #@0
    .prologue
    .line 891
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->cmdStatusGetPlayStatus()V

    #@5
    .line 893
    return-void
.end method

.method public setRepeatMode(I)V
    .registers 3
    .parameter "mode"

    #@0
    .prologue
    .line 980
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->cmdControlSetPlayerAppSettingValueRepeat(I)V

    #@5
    .line 981
    return-void
.end method

.method public setShuffleMode(I)V
    .registers 3
    .parameter "shuffle"

    #@0
    .prologue
    .line 988
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@2
    invoke-virtual {v0, p1}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->cmdControlSetPlayerAppSettingValueShuffle(I)V

    #@5
    .line 989
    return-void
.end method

.method public startTimer(I)V
    .registers 7
    .parameter "delay"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 995
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcpManager:Lcom/lge/bluetooth/LGBluetoothAvrcpManager;

    #@3
    invoke-virtual {v1}, Lcom/lge/bluetooth/LGBluetoothAvrcpManager;->getPlayStatus()I

    #@6
    move-result v1

    #@7
    if-ne v1, v4, :cond_37

    #@9
    .line 997
    invoke-virtual {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->stopTimer()V

    #@c
    .line 998
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    #@11
    move-result-object v0

    #@12
    .line 999
    .local v0, msg:Landroid/os/Message;
    const/4 v1, 0x3

    #@13
    iput v1, v0, Landroid/os/Message;->what:I

    #@15
    .line 1000
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@17
    invoke-virtual {v1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayerIndex()I

    #@1a
    move-result v1

    #@1b
    iput v1, v0, Landroid/os/Message;->arg1:I

    #@1d
    .line 1001
    const-string v1, "A2dpStateMachine"

    #@1f
    const-string v2, "Starting play position changed timer"

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 1002
    const/4 v1, -0x1

    #@25
    if-ne p1, v1, :cond_38

    #@27
    .line 1003
    const-string v1, "A2dpStateMachine"

    #@29
    const-string v2, "Invalid position changed interval. Using the default timeout of 2 seconds"

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 1005
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mHandler:Landroid/os/Handler;

    #@30
    const-wide/16 v2, 0x7d0

    #@32
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@35
    .line 1011
    :goto_35
    sput-boolean v4, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayPositionRequested:Z

    #@37
    .line 1013
    .end local v0           #msg:Landroid/os/Message;
    :cond_37
    return-void

    #@38
    .line 1008
    .restart local v0       #msg:Landroid/os/Message;
    :cond_38
    const-string v1, "A2dpStateMachine"

    #@3a
    new-instance v2, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const-string v3, "Play position changed timer delay: "

    #@41
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v2

    #@45
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@48
    move-result-object v2

    #@49
    const-string v3, " seconds"

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v2

    #@53
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 1009
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mHandler:Landroid/os/Handler;

    #@58
    mul-int/lit16 v2, p1, 0x3e8

    #@5a
    int-to-long v2, v2

    #@5b
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@5e
    goto :goto_35
.end method

.method public stopTimer()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    .line 1016
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mHandler:Landroid/os/Handler;

    #@3
    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    #@6
    move-result v0

    #@7
    if-eqz v0, :cond_15

    #@9
    .line 1017
    const-string v0, "A2dpStateMachine"

    #@b
    const-string v1, "Stopping get play position request timer"

    #@d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1018
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mHandler:Landroid/os/Handler;

    #@12
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    #@15
    .line 1020
    :cond_15
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->stopPlayPosTimer()V

    #@18
    .line 1021
    const/4 v0, 0x0

    #@19
    sput-boolean v0, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayPositionRequested:Z

    #@1b
    .line 1022
    return-void
.end method
