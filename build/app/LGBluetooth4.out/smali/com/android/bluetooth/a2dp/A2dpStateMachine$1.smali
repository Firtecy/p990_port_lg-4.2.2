.class Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;
.super Landroid/os/Handler;
.source "A2dpStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/a2dp/A2dpStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 202
    iput-object p1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const-wide/16 v4, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 205
    iget v0, p1, Landroid/os/Message;->what:I

    #@5
    packed-switch v0, :pswitch_data_13e

    #@8
    .line 249
    const-string v0, "A2dpStateMachine"

    #@a
    new-instance v1, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v2, "Unhandled message : "

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    iget v2, p1, Landroid/os/Message;->what:I

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 252
    :goto_22
    return-void

    #@23
    .line 207
    :pswitch_23
    const-string v0, "A2dpStateMachine"

    #@25
    const-string v1, "Timer expired. Sending request for play state"

    #@27
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 208
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@2c
    #calls: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->playPositionStatusRequest()V
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$400(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)V

    #@2f
    goto :goto_22

    #@30
    .line 211
    :pswitch_30
    const-string v0, "A2dpStateMachine"

    #@32
    const-string v1, "Play position request has timed out"

    #@34
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 212
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@39
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayingA2dpDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$500(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@3c
    move-result-object v0

    #@3d
    if-nez v0, :cond_97

    #@3f
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@41
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@44
    move-result-object v0

    #@45
    invoke-virtual {v0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayStatus()I

    #@48
    move-result v0

    #@49
    const/4 v1, 0x1

    #@4a
    if-eq v0, v1, :cond_59

    #@4c
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@4e
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@51
    move-result-object v0

    #@52
    invoke-virtual {v0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayStatus()I

    #@55
    move-result v0

    #@56
    const/4 v1, 0x2

    #@57
    if-ne v0, v1, :cond_97

    #@59
    .line 215
    :cond_59
    const-string v0, "A2dpStateMachine"

    #@5b
    new-instance v1, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v2, "STATE_NOT_PLAYING. Send play status changed here.Current state:"

    #@62
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v1

    #@66
    iget-object v2, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@68
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v2}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayStatus()I

    #@6f
    move-result v2

    #@70
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v1

    #@74
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77
    move-result-object v1

    #@78
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 217
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@7d
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@80
    move-result-object v0

    #@81
    invoke-virtual {v0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayStatus()I

    #@84
    move-result v0

    #@85
    if-eqz v0, :cond_8a

    #@87
    .line 218
    #calls: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onPlayStateChangedNative(I)V
    invoke-static {v3}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$700(I)V

    #@8a
    .line 220
    :cond_8a
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@8c
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@8f
    move-result-object v0

    #@90
    invoke-virtual {v0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->resetAvrcpPlaybackData()V

    #@93
    .line 221
    #calls: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onSongPositionChangedNative(J)V
    invoke-static {v4, v5}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$800(J)V

    #@96
    goto :goto_22

    #@97
    .line 223
    :cond_97
    const-string v0, "A2dpStateMachine"

    #@99
    const-string v1, "Sending play position changed event for request timeout"

    #@9b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    .line 224
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@a0
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@a3
    move-result-object v0

    #@a4
    invoke-virtual {v0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayPosition()J

    #@a7
    move-result-wide v0

    #@a8
    #calls: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onSongPositionChangedNative(J)V
    invoke-static {v0, v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$800(J)V

    #@ab
    goto/16 :goto_22

    #@ad
    .line 228
    :pswitch_ad
    const-string v0, "A2dpStateMachine"

    #@af
    const-string v1, "Play status update timed out. Sending cached info"

    #@b1
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b4
    .line 229
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@b6
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mPlayingA2dpDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$500(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@b9
    move-result-object v0

    #@ba
    if-eqz v0, :cond_df

    #@bc
    .line 230
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@be
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@c1
    move-result-object v0

    #@c2
    invoke-virtual {v0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayStatus()I

    #@c5
    move-result v0

    #@c6
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@c8
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@cb
    move-result-object v1

    #@cc
    invoke-virtual {v1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getDuration()J

    #@cf
    move-result-wide v1

    #@d0
    iget-object v3, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@d2
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v3}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@d5
    move-result-object v3

    #@d6
    invoke-virtual {v3}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayPosition()J

    #@d9
    move-result-wide v3

    #@da
    #calls: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onPlayStatusAvailableNative(IJJ)V
    invoke-static {v0, v1, v2, v3, v4}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$900(IJJ)V

    #@dd
    goto/16 :goto_22

    #@df
    .line 233
    :cond_df
    const-string v0, "A2dpStateMachine"

    #@e1
    new-instance v1, Ljava/lang/StringBuilder;

    #@e3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@e6
    const-string v2, "STATE_NOT_PLAYING Sending playStatusAvailable here.Current state:"

    #@e8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@eb
    move-result-object v1

    #@ec
    iget-object v2, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@ee
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v2}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@f1
    move-result-object v2

    #@f2
    invoke-virtual {v2}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayStatus()I

    #@f5
    move-result v2

    #@f6
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f9
    move-result-object v1

    #@fa
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@fd
    move-result-object v1

    #@fe
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@101
    .line 235
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@103
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@106
    move-result-object v0

    #@107
    invoke-virtual {v0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getPlayStatus()I

    #@10a
    move-result v0

    #@10b
    if-eqz v0, :cond_110

    #@10d
    .line 236
    #calls: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onPlayStateChangedNative(I)V
    invoke-static {v3}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$700(I)V

    #@110
    .line 238
    :cond_110
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@112
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@115
    move-result-object v0

    #@116
    invoke-virtual {v0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->resetAvrcpPlaybackData()V

    #@119
    .line 239
    #calls: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onPlayStatusAvailableNative(IJJ)V
    invoke-static {v3, v4, v5, v4, v5}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$900(IJJ)V

    #@11c
    goto/16 :goto_22

    #@11e
    .line 243
    :pswitch_11e
    const-string v0, "A2dpStateMachine"

    #@120
    const-string v1, "App settings update timed out. Sending cached info"

    #@122
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@125
    .line 244
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@127
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v0}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@12a
    move-result-object v0

    #@12b
    invoke-virtual {v0}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getRepeatMode()I

    #@12e
    move-result v0

    #@12f
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpStateMachine$1;->this$0:Lcom/android/bluetooth/a2dp/A2dpStateMachine;

    #@131
    #getter for: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->mAvrcp:Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;
    invoke-static {v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$600(Lcom/android/bluetooth/a2dp/A2dpStateMachine;)Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;

    #@134
    move-result-object v1

    #@135
    invoke-virtual {v1}, Lcom/broadcom/bt/service/avrcp/BluetoothAVRCP;->getShuffleMode()I

    #@138
    move-result v1

    #@139
    #calls: Lcom/android/bluetooth/a2dp/A2dpStateMachine;->onApplicationSettingsChangedNative(II)V
    invoke-static {v0, v1}, Lcom/android/bluetooth/a2dp/A2dpStateMachine;->access$1000(II)V

    #@13c
    goto/16 :goto_22

    #@13e
    .line 205
    :pswitch_data_13e
    .packed-switch 0x0
        :pswitch_30
        :pswitch_ad
        :pswitch_11e
        :pswitch_23
    .end packed-switch
.end method
