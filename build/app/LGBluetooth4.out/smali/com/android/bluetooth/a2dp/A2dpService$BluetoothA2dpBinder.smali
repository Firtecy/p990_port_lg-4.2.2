.class Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;
.super Landroid/bluetooth/IBluetoothA2dp$Stub;
.source "A2dpService.java"

# interfaces
.implements Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/a2dp/A2dpService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BluetoothA2dpBinder"
.end annotation


# instance fields
.field private mService:Lcom/android/bluetooth/a2dp/A2dpService;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/a2dp/A2dpService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 214
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothA2dp$Stub;-><init>()V

    #@3
    .line 215
    iput-object p1, p0, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->mService:Lcom/android/bluetooth/a2dp/A2dpService;

    #@5
    .line 216
    return-void
.end method

.method private getService()Lcom/android/bluetooth/a2dp/A2dpService;
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 203
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_f

    #@7
    .line 204
    const-string v1, "A2dpService"

    #@9
    const-string v2, "A2dp call not allowed for non-active user"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 211
    :cond_e
    :goto_e
    return-object v0

    #@f
    .line 208
    :cond_f
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->mService:Lcom/android/bluetooth/a2dp/A2dpService;

    #@11
    if-eqz v1, :cond_e

    #@13
    iget-object v1, p0, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->mService:Lcom/android/bluetooth/a2dp/A2dpService;

    #@15
    invoke-static {v1}, Lcom/android/bluetooth/a2dp/A2dpService;->access$000(Lcom/android/bluetooth/a2dp/A2dpService;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_e

    #@1b
    .line 209
    iget-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->mService:Lcom/android/bluetooth/a2dp/A2dpService;

    #@1d
    goto :goto_e
.end method


# virtual methods
.method public cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 219
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->mService:Lcom/android/bluetooth/a2dp/A2dpService;

    #@3
    .line 220
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 224
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->getService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@3
    move-result-object v0

    #@4
    .line 225
    .local v0, service:Lcom/android/bluetooth/a2dp/A2dpService;
    if-nez v0, :cond_8

    #@6
    .line 226
    const/4 v1, 0x0

    #@7
    .line 228
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 232
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->getService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@3
    move-result-object v0

    #@4
    .line 233
    .local v0, service:Lcom/android/bluetooth/a2dp/A2dpService;
    if-nez v0, :cond_8

    #@6
    .line 234
    const/4 v1, 0x0

    #@7
    .line 236
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 240
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->getService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@3
    move-result-object v0

    #@4
    .line 241
    .local v0, service:Lcom/android/bluetooth/a2dp/A2dpService;
    if-nez v0, :cond_d

    #@6
    .line 242
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 244
    :goto_c
    return-object v1

    #@d
    :cond_d
    invoke-virtual {v0}, Lcom/android/bluetooth/a2dp/A2dpService;->getConnectedDevices()Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 256
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->getService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@3
    move-result-object v0

    #@4
    .line 257
    .local v0, service:Lcom/android/bluetooth/a2dp/A2dpService;
    if-nez v0, :cond_8

    #@6
    .line 258
    const/4 v1, 0x0

    #@7
    .line 260
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 5
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 248
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->getService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@3
    move-result-object v0

    #@4
    .line 249
    .local v0, service:Lcom/android/bluetooth/a2dp/A2dpService;
    if-nez v0, :cond_d

    #@6
    .line 250
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 252
    :goto_c
    return-object v1

    #@d
    :cond_d
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method

.method public getPriority(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 272
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->getService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@3
    move-result-object v0

    #@4
    .line 273
    .local v0, service:Lcom/android/bluetooth/a2dp/A2dpService;
    if-nez v0, :cond_8

    #@6
    .line 274
    const/4 v1, -0x1

    #@7
    .line 276
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public isA2dpPlaying(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 280
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->getService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@3
    move-result-object v0

    #@4
    .line 281
    .local v0, service:Lcom/android/bluetooth/a2dp/A2dpService;
    if-nez v0, :cond_8

    #@6
    .line 282
    const/4 v1, 0x0

    #@7
    .line 284
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->isA2dpPlaying(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public setPriority(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 5
    .parameter "device"
    .parameter "priority"

    #@0
    .prologue
    .line 264
    invoke-direct {p0}, Lcom/android/bluetooth/a2dp/A2dpService$BluetoothA2dpBinder;->getService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@3
    move-result-object v0

    #@4
    .line 265
    .local v0, service:Lcom/android/bluetooth/a2dp/A2dpService;
    if-nez v0, :cond_8

    #@6
    .line 266
    const/4 v1, 0x0

    #@7
    .line 268
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/a2dp/A2dpService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method
