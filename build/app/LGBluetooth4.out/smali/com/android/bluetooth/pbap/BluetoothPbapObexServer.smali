.class public Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;
.super Ljavax/obex/ServerRequestHandler;
.source "BluetoothPbapObexServer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;,
        Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$ContentType;
    }
.end annotation


# static fields
.field private static CALLLOG_NUM_LIMIT:I = 0x0

.field private static final CCH:Ljava/lang/String; = "cch"

.field private static final CCH_PATH:Ljava/lang/String; = "/telecom/cch"

.field private static final D:Z = true

.field private static final ICH:Ljava/lang/String; = "ich"

.field private static final ICH_PATH:Ljava/lang/String; = "/telecom/ich"

.field private static final LEGAL_PATH:[Ljava/lang/String; = null

.field private static final LEGAL_PATH_WITH_SIM:[Ljava/lang/String; = null

.field private static final MCH:Ljava/lang/String; = "mch"

.field private static final MCH_PATH:Ljava/lang/String; = "/telecom/mch"

.field private static final NEED_SEND_BODY:I = -0x1

.field private static final OCH:Ljava/lang/String; = "och"

.field private static final OCH_PATH:Ljava/lang/String; = "/telecom/och"

.field public static ORDER_BY_ALPHABETICAL:I = 0x0

.field public static ORDER_BY_INDEXED:I = 0x0

.field private static final PB:Ljava/lang/String; = "pb"

.field private static final PBAP_TARGET:[B = null

.field private static final PB_PATH:Ljava/lang/String; = "/telecom/pb"

.field private static final SIM1:Ljava/lang/String; = "SIM1"

.field private static final TAG:Ljava/lang/String; = "BluetoothPbapObexServer"

.field private static final TYPE_LISTING:Ljava/lang/String; = "x-bt/vcard-listing"

.field private static final TYPE_PB:Ljava/lang/String; = "x-bt/phonebook"

.field private static final TYPE_VCARD:Ljava/lang/String; = "x-bt/vcard"

.field private static final UUID_LENGTH:I = 0x10

.field private static final V:Z = true

.field private static final VCARD_NAME_SUFFIX_LENGTH:I = 0x5

.field public static sIsAborted:Z


# instance fields
.field private mCallback:Landroid/os/Handler;

.field private mContext:Landroid/content/Context;

.field private mCurrentPath:Ljava/lang/String;

.field private mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

.field private mMissedCallSize:I

.field private mNeedNewMissedCallsNum:Z

.field private mNeedPhonebookSize:Z

.field private mOrderBy:I

.field private mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;


# direct methods
.method static constructor <clinit>()V
    .registers 8

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 80
    const/16 v0, 0x10

    #@7
    new-array v0, v0, [B

    #@9
    fill-array-data v0, :array_7e

    #@c
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->PBAP_TARGET:[B

    #@e
    .line 86
    const/4 v0, 0x6

    #@f
    new-array v0, v0, [Ljava/lang/String;

    #@11
    const-string v1, "/telecom"

    #@13
    aput-object v1, v0, v3

    #@15
    const-string v1, "/telecom/pb"

    #@17
    aput-object v1, v0, v4

    #@19
    const-string v1, "/telecom/ich"

    #@1b
    aput-object v1, v0, v5

    #@1d
    const-string v1, "/telecom/och"

    #@1f
    aput-object v1, v0, v6

    #@21
    const-string v1, "/telecom/mch"

    #@23
    aput-object v1, v0, v7

    #@25
    const/4 v1, 0x5

    #@26
    const-string v2, "/telecom/cch"

    #@28
    aput-object v2, v0, v1

    #@2a
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->LEGAL_PATH:[Ljava/lang/String;

    #@2c
    .line 92
    const/16 v0, 0xd

    #@2e
    new-array v0, v0, [Ljava/lang/String;

    #@30
    const-string v1, "/telecom"

    #@32
    aput-object v1, v0, v3

    #@34
    const-string v1, "/telecom/pb"

    #@36
    aput-object v1, v0, v4

    #@38
    const-string v1, "/telecom/ich"

    #@3a
    aput-object v1, v0, v5

    #@3c
    const-string v1, "/telecom/och"

    #@3e
    aput-object v1, v0, v6

    #@40
    const-string v1, "/telecom/mch"

    #@42
    aput-object v1, v0, v7

    #@44
    const/4 v1, 0x5

    #@45
    const-string v2, "/telecom/cch"

    #@47
    aput-object v2, v0, v1

    #@49
    const/4 v1, 0x6

    #@4a
    const-string v2, "/SIM1"

    #@4c
    aput-object v2, v0, v1

    #@4e
    const/4 v1, 0x7

    #@4f
    const-string v2, "/SIM1/telecom"

    #@51
    aput-object v2, v0, v1

    #@53
    const/16 v1, 0x8

    #@55
    const-string v2, "/SIM1/telecom/ich"

    #@57
    aput-object v2, v0, v1

    #@59
    const/16 v1, 0x9

    #@5b
    const-string v2, "/SIM1/telecom/och"

    #@5d
    aput-object v2, v0, v1

    #@5f
    const/16 v1, 0xa

    #@61
    const-string v2, "/SIM1/telecom/mch"

    #@63
    aput-object v2, v0, v1

    #@65
    const/16 v1, 0xb

    #@67
    const-string v2, "/SIM1/telecom/cch"

    #@69
    aput-object v2, v0, v1

    #@6b
    const/16 v1, 0xc

    #@6d
    const-string v2, "/SIM1/telecom/pb"

    #@6f
    aput-object v2, v0, v1

    #@71
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->LEGAL_PATH_WITH_SIM:[Ljava/lang/String;

    #@73
    .line 162
    const/16 v0, 0x32

    #@75
    sput v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->CALLLOG_NUM_LIMIT:I

    #@77
    .line 164
    sput v3, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->ORDER_BY_INDEXED:I

    #@79
    .line 166
    sput v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->ORDER_BY_ALPHABETICAL:I

    #@7b
    .line 168
    sput-boolean v3, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->sIsAborted:Z

    #@7d
    return-void

    #@7e
    .line 80
    :array_7e
    .array-data 0x1
        0x79t
        0x61t
        0x35t
        0xf0t
        0xf0t
        0xc5t
        0x11t
        0xd8t
        0x9t
        0x66t
        0x8t
        0x0t
        0x20t
        0xct
        0x9at
        0x66t
    .end array-data
.end method

.method public constructor <init>(Landroid/os/Handler;Landroid/content/Context;)V
    .registers 6
    .parameter "callback"
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 187
    invoke-direct {p0}, Ljavax/obex/ServerRequestHandler;-><init>()V

    #@4
    .line 140
    iput-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedPhonebookSize:Z

    #@6
    .line 144
    iput-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedNewMissedCallsNum:Z

    #@8
    .line 146
    iput v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mMissedCallSize:I

    #@a
    .line 149
    const-string v0, ""

    #@c
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@e
    .line 151
    const/4 v0, 0x0

    #@f
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCallback:Landroid/os/Handler;

    #@11
    .line 160
    sget v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->ORDER_BY_INDEXED:I

    #@13
    iput v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mOrderBy:I

    #@15
    .line 188
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCallback:Landroid/os/Handler;

    #@17
    .line 189
    iput-object p2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mContext:Landroid/content/Context;

    #@19
    .line 190
    new-instance v0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@1b
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mContext:Landroid/content/Context;

    #@1d
    invoke-direct {v0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;-><init>(Landroid/content/Context;)V

    #@20
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@22
    .line 192
    invoke-static {p2}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->getInstance(Landroid/content/Context;)Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@25
    move-result-object v0

    #@26
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@28
    .line 196
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@2a
    const/4 v1, 0x4

    #@2b
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->getPhonebookSize(I)I

    #@2e
    move-result v0

    #@2f
    iput v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mMissedCallSize:I

    #@31
    .line 198
    const-string v0, "BluetoothPbapObexServer"

    #@33
    new-instance v1, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v2, "Initialize mMissedCallSize="

    #@3a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v1

    #@3e
    iget v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mMissedCallSize:I

    #@40
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v1

    #@44
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@47
    move-result-object v1

    #@48
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4b
    .line 200
    return-void
.end method

.method private BtUiLog(Ljava/lang/String;)V
    .registers 5
    .parameter "msg"

    #@0
    .prologue
    .line 1362
    const-string v0, "BluetoothPbapObexServer"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[BTUI] [PBAP] "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1364
    return-void
.end method

.method public static closeStream(Ljava/io/OutputStream;Ljavax/obex/Operation;)Z
    .registers 7
    .parameter "out"
    .parameter "op"

    #@0
    .prologue
    .line 1245
    const/4 v1, 0x1

    #@1
    .line 1247
    .local v1, returnvalue:Z
    if-eqz p0, :cond_6

    #@3
    .line 1248
    :try_start_3
    invoke-virtual {p0}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_6} :catch_c

    #@6
    .line 1255
    :cond_6
    :goto_6
    if-eqz p1, :cond_b

    #@8
    .line 1256
    :try_start_8
    invoke-interface {p1}, Ljavax/obex/Operation;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_b} :catch_2b

    #@b
    .line 1262
    :cond_b
    :goto_b
    return v1

    #@c
    .line 1250
    :catch_c
    move-exception v0

    #@d
    .line 1251
    .local v0, e:Ljava/io/IOException;
    const-string v2, "BluetoothPbapObexServer"

    #@f
    new-instance v3, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v4, "outputStream close failed"

    #@16
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v3

    #@1a
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 1252
    const/4 v1, 0x0

    #@2a
    goto :goto_6

    #@2b
    .line 1258
    .end local v0           #e:Ljava/io/IOException;
    :catch_2b
    move-exception v0

    #@2c
    .line 1259
    .restart local v0       #e:Ljava/io/IOException;
    const-string v2, "BluetoothPbapObexServer"

    #@2e
    new-instance v3, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v4, "operation close failed"

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@3c
    move-result-object v4

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v3

    #@45
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@48
    .line 1260
    const/4 v1, 0x0

    #@49
    goto :goto_b
.end method

.method private createList(IILjava/lang/String;Ljava/lang/StringBuilder;Ljava/lang/String;)I
    .registers 19
    .parameter "maxListCount"
    .parameter "listStartOffset"
    .parameter "searchValue"
    .parameter "result"
    .parameter "type"

    #@0
    .prologue
    .line 745
    const/4 v4, 0x0

    #@1
    .line 746
    .local v4, itemsFound:I
    iget-object v10, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@3
    iget v11, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mOrderBy:I

    #@5
    invoke-virtual {v10, v11}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->getPhonebookNameList(I)Ljava/util/ArrayList;

    #@8
    move-result-object v6

    #@9
    .line 747
    .local v6, nameList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@c
    move-result v10

    #@d
    if-lt v10, p1, :cond_be

    #@f
    move v9, p1

    #@10
    .line 748
    .local v9, requestSize:I
    :goto_10
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@13
    move-result v5

    #@14
    .line 749
    .local v5, listSize:I
    const-string v1, ""

    #@16
    .line 753
    .local v1, compareValue:Ljava/lang/String;
    const-string v10, "BluetoothPbapObexServer"

    #@18
    new-instance v11, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v12, "search by "

    #@1f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v11

    #@23
    move-object/from16 v0, p5

    #@25
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v11

    #@29
    const-string v12, ", requestSize="

    #@2b
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v11

    #@2f
    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v11

    #@33
    const-string v12, " offset="

    #@35
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v11

    #@39
    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v11

    #@3d
    const-string v12, " searchValue="

    #@3f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v11

    #@43
    move-object/from16 v0, p3

    #@45
    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v11

    #@49
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v11

    #@4d
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 757
    const-string v10, "number"

    #@52
    move-object/from16 v0, p5

    #@54
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v10

    #@58
    if-eqz v10, :cond_134

    #@5a
    .line 759
    iget-object v10, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@5c
    move-object/from16 v0, p3

    #@5e
    invoke-virtual {v10, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->getContactNamesByNumber(Ljava/lang/String;)Ljava/util/ArrayList;

    #@61
    move-result-object v7

    #@62
    .line 783
    .local v7, names:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v3, 0x0

    #@63
    .local v3, i:I
    :goto_63
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@66
    move-result v10

    #@67
    if-ge v3, v10, :cond_c6

    #@69
    .line 784
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@6c
    move-result-object v10

    #@6d
    check-cast v10, Ljava/lang/String;

    #@6f
    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    .line 786
    const-string v10, "BluetoothPbapObexServer"

    #@75
    new-instance v11, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v12, "[BTUI] [EQUAL] compareValue="

    #@7c
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v11

    #@80
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v11

    #@84
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@87
    move-result-object v11

    #@88
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 788
    move v8, p2

    #@8c
    .line 789
    .local v8, pos:I
    :goto_8c
    if-ge v8, v5, :cond_c4

    #@8e
    if-ge v4, v9, :cond_c4

    #@90
    .line 790
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@93
    move-result-object v2

    #@94
    check-cast v2, Ljava/lang/String;

    #@96
    .line 792
    .local v2, currentValue:Ljava/lang/String;
    const-string v10, "BluetoothPbapObexServer"

    #@98
    new-instance v11, Ljava/lang/StringBuilder;

    #@9a
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@9d
    const-string v12, "[BTUI] [EQUAL] currentValue="

    #@9f
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v11

    #@a3
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v11

    #@a7
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v11

    #@ab
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 794
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b1
    move-result v10

    #@b2
    if-eqz v10, :cond_bb

    #@b4
    .line 795
    add-int/lit8 v4, v4, 0x1

    #@b6
    .line 796
    move-object/from16 v0, p4

    #@b8
    invoke-direct {p0, v8, v2, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->writeVCardEntry(ILjava/lang/String;Ljava/lang/StringBuilder;)V

    #@bb
    .line 789
    :cond_bb
    add-int/lit8 v8, v8, 0x1

    #@bd
    goto :goto_8c

    #@be
    .line 747
    .end local v1           #compareValue:Ljava/lang/String;
    .end local v2           #currentValue:Ljava/lang/String;
    .end local v3           #i:I
    .end local v5           #listSize:I
    .end local v7           #names:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8           #pos:I
    .end local v9           #requestSize:I
    :cond_be
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    #@c1
    move-result v9

    #@c2
    goto/16 :goto_10

    #@c4
    .line 799
    .restart local v1       #compareValue:Ljava/lang/String;
    .restart local v3       #i:I
    .restart local v5       #listSize:I
    .restart local v7       #names:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v8       #pos:I
    .restart local v9       #requestSize:I
    :cond_c4
    if-lt v4, v9, :cond_12a

    #@c6
    .line 803
    .end local v8           #pos:I
    :cond_c6
    if-ge v4, v9, :cond_130

    #@c8
    .line 804
    const/4 v3, 0x0

    #@c9
    :goto_c9
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@cc
    move-result v10

    #@cd
    if-ge v3, v10, :cond_130

    #@cf
    .line 805
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@d2
    move-result-object v10

    #@d3
    check-cast v10, Ljava/lang/String;

    #@d5
    invoke-virtual {v10}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@d8
    move-result-object v1

    #@d9
    .line 807
    const-string v10, "BluetoothPbapObexServer"

    #@db
    new-instance v11, Ljava/lang/StringBuilder;

    #@dd
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@e0
    const-string v12, "[BTUI] [STARTWITH] compareValue="

    #@e2
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v11

    #@e6
    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e9
    move-result-object v11

    #@ea
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ed
    move-result-object v11

    #@ee
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f1
    .line 809
    move v8, p2

    #@f2
    .line 810
    .restart local v8       #pos:I
    :goto_f2
    if-ge v8, v5, :cond_12e

    #@f4
    if-ge v4, v9, :cond_12e

    #@f6
    .line 811
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@f9
    move-result-object v2

    #@fa
    check-cast v2, Ljava/lang/String;

    #@fc
    .line 813
    .restart local v2       #currentValue:Ljava/lang/String;
    const-string v10, "BluetoothPbapObexServer"

    #@fe
    new-instance v11, Ljava/lang/StringBuilder;

    #@100
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@103
    const-string v12, "[BTUI] [STARTWITH] currentValue="

    #@105
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v11

    #@109
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v11

    #@10d
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@110
    move-result-object v11

    #@111
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@114
    .line 815
    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@117
    move-result v10

    #@118
    if-eqz v10, :cond_127

    #@11a
    .line 816
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11d
    move-result v10

    #@11e
    if-nez v10, :cond_127

    #@120
    .line 817
    add-int/lit8 v4, v4, 0x1

    #@122
    .line 818
    move-object/from16 v0, p4

    #@124
    invoke-direct {p0, v8, v2, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->writeVCardEntry(ILjava/lang/String;Ljava/lang/StringBuilder;)V

    #@127
    .line 810
    :cond_127
    add-int/lit8 v8, v8, 0x1

    #@129
    goto :goto_f2

    #@12a
    .line 783
    .end local v2           #currentValue:Ljava/lang/String;
    :cond_12a
    add-int/lit8 v3, v3, 0x1

    #@12c
    goto/16 :goto_63

    #@12e
    .line 822
    :cond_12e
    if-lt v4, v9, :cond_131

    #@130
    .line 874
    .end local v3           #i:I
    .end local v7           #names:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8           #pos:I
    :cond_130
    return v4

    #@131
    .line 804
    .restart local v3       #i:I
    .restart local v7       #names:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v8       #pos:I
    :cond_131
    add-int/lit8 v3, v3, 0x1

    #@133
    goto :goto_c9

    #@134
    .line 829
    .end local v3           #i:I
    .end local v7           #names:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v8           #pos:I
    :cond_134
    if-eqz p3, :cond_13a

    #@136
    .line 830
    invoke-virtual/range {p3 .. p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@139
    move-result-object v1

    #@13a
    .line 846
    :cond_13a
    move v8, p2

    #@13b
    .line 847
    .restart local v8       #pos:I
    :goto_13b
    if-ge v8, v5, :cond_16f

    #@13d
    if-ge v4, v9, :cond_16f

    #@13f
    .line 848
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@142
    move-result-object v2

    #@143
    check-cast v2, Ljava/lang/String;

    #@145
    .line 850
    .restart local v2       #currentValue:Ljava/lang/String;
    const-string v10, "BluetoothPbapObexServer"

    #@147
    new-instance v11, Ljava/lang/StringBuilder;

    #@149
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@14c
    const-string v12, "[BTUI] [EQUAL] currentValue="

    #@14e
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@151
    move-result-object v11

    #@152
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@155
    move-result-object v11

    #@156
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@159
    move-result-object v11

    #@15a
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15d
    .line 852
    if-eqz p3, :cond_165

    #@15f
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@162
    move-result v10

    #@163
    if-eqz v10, :cond_16c

    #@165
    .line 853
    :cond_165
    add-int/lit8 v4, v4, 0x1

    #@167
    .line 854
    move-object/from16 v0, p4

    #@169
    invoke-direct {p0, v8, v2, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->writeVCardEntry(ILjava/lang/String;Ljava/lang/StringBuilder;)V

    #@16c
    .line 847
    :cond_16c
    add-int/lit8 v8, v8, 0x1

    #@16e
    goto :goto_13b

    #@16f
    .line 857
    .end local v2           #currentValue:Ljava/lang/String;
    :cond_16f
    if-ge v4, v9, :cond_130

    #@171
    .line 858
    move v8, p2

    #@172
    .line 859
    :goto_172
    if-ge v8, v5, :cond_130

    #@174
    if-ge v4, v9, :cond_130

    #@176
    .line 860
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@179
    move-result-object v2

    #@17a
    check-cast v2, Ljava/lang/String;

    #@17c
    .line 862
    .restart local v2       #currentValue:Ljava/lang/String;
    const-string v10, "BluetoothPbapObexServer"

    #@17e
    new-instance v11, Ljava/lang/StringBuilder;

    #@180
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@183
    const-string v12, "[BTUI] [STARTWITH] currentValue="

    #@185
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@188
    move-result-object v11

    #@189
    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18c
    move-result-object v11

    #@18d
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@190
    move-result-object v11

    #@191
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@194
    .line 864
    if-eqz p3, :cond_19c

    #@196
    invoke-virtual {v2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@199
    move-result v10

    #@19a
    if-eqz v10, :cond_1a9

    #@19c
    .line 865
    :cond_19c
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@19f
    move-result v10

    #@1a0
    if-nez v10, :cond_1a9

    #@1a2
    .line 866
    add-int/lit8 v4, v4, 0x1

    #@1a4
    .line 867
    move-object/from16 v0, p4

    #@1a6
    invoke-direct {p0, v8, v2, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->writeVCardEntry(ILjava/lang/String;Ljava/lang/StringBuilder;)V

    #@1a9
    .line 859
    :cond_1a9
    add-int/lit8 v8, v8, 0x1

    #@1ab
    goto :goto_172
.end method

.method public static final createSelectionPara(I)Ljava/lang/String;
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 1271
    const/4 v0, 0x0

    #@1
    .line 1272
    .local v0, selection:Ljava/lang/String;
    packed-switch p0, :pswitch_data_26

    #@4
    .line 1292
    :goto_4
    const-string v1, "BluetoothPbapObexServer"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Call log selection: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 1294
    return-object v0

    #@1d
    .line 1278
    :pswitch_1d
    const-string v0, "type=1 OR type=10"

    #@1f
    .line 1281
    goto :goto_4

    #@20
    .line 1283
    :pswitch_20
    const-string v0, "type=2"

    #@22
    .line 1284
    goto :goto_4

    #@23
    .line 1286
    :pswitch_23
    const-string v0, "type=3"

    #@25
    .line 1287
    goto :goto_4

    #@26
    .line 1272
    :pswitch_data_26
    .packed-switch 0x2
        :pswitch_1d
        :pswitch_20
        :pswitch_23
    .end packed-switch
.end method

.method private final handleAppParaForResponse(Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;ILjavax/obex/HeaderSet;Ljavax/obex/Operation;)I
    .registers 13
    .parameter "appParamValue"
    .parameter "size"
    .parameter "reply"
    .parameter "op"

    #@0
    .prologue
    .line 939
    const/4 v5, 0x1

    #@1
    new-array v2, v5, [B

    #@3
    .line 940
    .local v2, misnum:[B
    new-instance v0, Ljavax/obex/ApplicationParameter;

    #@5
    invoke-direct {v0}, Ljavax/obex/ApplicationParameter;-><init>()V

    #@8
    .line 944
    .local v0, ap:Ljavax/obex/ApplicationParameter;
    iget-boolean v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedPhonebookSize:Z

    #@a
    if-eqz v5, :cond_88

    #@c
    .line 946
    const-string v5, "BluetoothPbapObexServer"

    #@e
    const-string v6, "Need Phonebook size in response header."

    #@10
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 948
    const/4 v5, 0x0

    #@14
    iput-boolean v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedPhonebookSize:Z

    #@16
    .line 950
    const/4 v5, 0x2

    #@17
    new-array v4, v5, [B

    #@19
    .line 952
    .local v4, pbsize:[B
    const/4 v5, 0x0

    #@1a
    div-int/lit16 v6, p2, 0x100

    #@1c
    and-int/lit16 v6, v6, 0xff

    #@1e
    int-to-byte v6, v6

    #@1f
    aput-byte v6, v4, v5

    #@21
    .line 953
    const/4 v5, 0x1

    #@22
    rem-int/lit16 v6, p2, 0x100

    #@24
    and-int/lit16 v6, v6, 0xff

    #@26
    int-to-byte v6, v6

    #@27
    aput-byte v6, v4, v5

    #@29
    .line 954
    const/16 v5, 0x8

    #@2b
    const/4 v6, 0x2

    #@2c
    invoke-virtual {v0, v5, v6, v4}, Ljavax/obex/ApplicationParameter;->addAPPHeader(BB[B)V

    #@2f
    .line 957
    iget-boolean v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedNewMissedCallsNum:Z

    #@31
    if-eqz v5, :cond_4c

    #@33
    .line 958
    const/4 v5, 0x0

    #@34
    iput-boolean v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedNewMissedCallsNum:Z

    #@36
    .line 960
    iget-object v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@38
    if-eqz v5, :cond_46

    #@3a
    .line 961
    const/4 v5, 0x0

    #@3b
    iget-object v6, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@3d
    iget-object v6, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mContext:Landroid/content/Context;

    #@3f
    invoke-static {v6}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->getNewMissedCallCount(Landroid/content/Context;)I

    #@42
    move-result v6

    #@43
    int-to-byte v6, v6

    #@44
    aput-byte v6, v2, v5

    #@46
    .line 963
    :cond_46
    const/16 v5, 0x9

    #@48
    const/4 v6, 0x1

    #@49
    invoke-virtual {v0, v5, v6, v2}, Ljavax/obex/ApplicationParameter;->addAPPHeader(BB[B)V

    #@4c
    .line 978
    :cond_4c
    const/16 v5, 0x4c

    #@4e
    invoke-virtual {v0}, Ljavax/obex/ApplicationParameter;->getAPPparam()[B

    #@51
    move-result-object v6

    #@52
    invoke-virtual {p3, v5, v6}, Ljavax/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    #@55
    .line 981
    const-string v5, "BluetoothPbapObexServer"

    #@57
    new-instance v6, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v7, "Send back Phonebook size only, without body info! Size= "

    #@5e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v6

    #@62
    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@65
    move-result-object v6

    #@66
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v6

    #@6a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 983
    new-instance v5, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v6, "### PCE only request pbSize : "

    #@74
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v5

    #@7c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7f
    move-result-object v5

    #@80
    invoke-direct {p0, v5}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@83
    .line 985
    invoke-direct {p0, p4, p3}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->pushHeader(Ljavax/obex/Operation;Ljavax/obex/HeaderSet;)I

    #@86
    move-result v5

    #@87
    .line 1019
    .end local v4           #pbsize:[B
    :goto_87
    return v5

    #@88
    .line 991
    :cond_88
    iget-boolean v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedNewMissedCallsNum:Z

    #@8a
    if-eqz v5, :cond_cc

    #@8c
    .line 993
    const-string v5, "BluetoothPbapObexServer"

    #@8e
    const-string v6, "Need new missed call num in response header."

    #@90
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 995
    const/4 v5, 0x0

    #@94
    iput-boolean v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedNewMissedCallsNum:Z

    #@96
    .line 997
    iget v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mMissedCallSize:I

    #@98
    sub-int v3, p2, v5

    #@9a
    .line 998
    .local v3, nmnum:I
    iput p2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mMissedCallSize:I

    #@9c
    .line 1000
    if-lez v3, :cond_ce

    #@9e
    .line 1001
    :goto_9e
    const/4 v5, 0x0

    #@9f
    int-to-byte v6, v3

    #@a0
    aput-byte v6, v2, v5

    #@a2
    .line 1002
    const/16 v5, 0x9

    #@a4
    const/4 v6, 0x1

    #@a5
    invoke-virtual {v0, v5, v6, v2}, Ljavax/obex/ApplicationParameter;->addAPPHeader(BB[B)V

    #@a8
    .line 1004
    const/16 v5, 0x4c

    #@aa
    invoke-virtual {v0}, Ljavax/obex/ApplicationParameter;->getAPPparam()[B

    #@ad
    move-result-object v6

    #@ae
    invoke-virtual {p3, v5, v6}, Ljavax/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V

    #@b1
    .line 1006
    const-string v5, "BluetoothPbapObexServer"

    #@b3
    new-instance v6, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v7, "handleAppParaForResponse(): mNeedNewMissedCallsNum=true,  num= "

    #@ba
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v6

    #@be
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c1
    move-result-object v6

    #@c2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c5
    move-result-object v6

    #@c6
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c9
    .line 1013
    :try_start_c9
    invoke-interface {p4, p3}, Ljavax/obex/Operation;->sendHeaders(Ljavax/obex/HeaderSet;)V
    :try_end_cc
    .catch Ljava/io/IOException; {:try_start_c9 .. :try_end_cc} :catch_d0

    #@cc
    .line 1019
    .end local v3           #nmnum:I
    :cond_cc
    const/4 v5, -0x1

    #@cd
    goto :goto_87

    #@ce
    .line 1000
    .restart local v3       #nmnum:I
    :cond_ce
    const/4 v3, 0x0

    #@cf
    goto :goto_9e

    #@d0
    .line 1014
    :catch_d0
    move-exception v1

    #@d1
    .line 1015
    .local v1, e:Ljava/io/IOException;
    const-string v5, "BluetoothPbapObexServer"

    #@d3
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@d6
    move-result-object v6

    #@d7
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@da
    .line 1016
    const/16 v5, 0xd0

    #@dc
    goto :goto_87
.end method

.method private final isLegalPath(Ljava/lang/String;)Z
    .registers 5
    .parameter "str"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 515
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_8

    #@7
    .line 540
    :cond_7
    :goto_7
    return v1

    #@8
    .line 526
    :cond_8
    const-string v2, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@a
    invoke-static {v2}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_23

    #@10
    .line 527
    const/4 v0, 0x0

    #@11
    .local v0, i:I
    :goto_11
    sget-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->LEGAL_PATH_WITH_SIM:[Ljava/lang/String;

    #@13
    array-length v2, v2

    #@14
    if-ge v0, v2, :cond_36

    #@16
    .line 528
    sget-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->LEGAL_PATH_WITH_SIM:[Ljava/lang/String;

    #@18
    aget-object v2, v2, v0

    #@1a
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v2

    #@1e
    if-nez v2, :cond_7

    #@20
    .line 527
    add-int/lit8 v0, v0, 0x1

    #@22
    goto :goto_11

    #@23
    .line 533
    .end local v0           #i:I
    :cond_23
    const/4 v0, 0x0

    #@24
    .restart local v0       #i:I
    :goto_24
    sget-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->LEGAL_PATH:[Ljava/lang/String;

    #@26
    array-length v2, v2

    #@27
    if-ge v0, v2, :cond_36

    #@29
    .line 534
    sget-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->LEGAL_PATH:[Ljava/lang/String;

    #@2b
    aget-object v2, v2, v0

    #@2d
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v2

    #@31
    if-nez v2, :cond_7

    #@33
    .line 533
    add-int/lit8 v0, v0, 0x1

    #@35
    goto :goto_24

    #@36
    .line 540
    :cond_36
    const/4 v1, 0x0

    #@37
    goto :goto_7
.end method

.method public static final logHeader(Ljavax/obex/HeaderSet;)V
    .registers 5
    .parameter "hs"

    #@0
    .prologue
    .line 1340
    const-string v1, "BluetoothPbapObexServer"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "Dumping HeaderSet "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@10
    move-result-object v3

    #@11
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 1343
    :try_start_1c
    const-string v1, "BluetoothPbapObexServer"

    #@1e
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "COUNT : "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const/16 v3, 0xc0

    #@2b
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v2

    #@37
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 1344
    const-string v1, "BluetoothPbapObexServer"

    #@3c
    new-instance v2, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v3, "NAME : "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    const/4 v3, 0x1

    #@48
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v2

    #@54
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 1345
    const-string v1, "BluetoothPbapObexServer"

    #@59
    new-instance v2, Ljava/lang/StringBuilder;

    #@5b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5e
    const-string v3, "TYPE : "

    #@60
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v2

    #@64
    const/16 v3, 0x42

    #@66
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v2

    #@6e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v2

    #@72
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 1346
    const-string v1, "BluetoothPbapObexServer"

    #@77
    new-instance v2, Ljava/lang/StringBuilder;

    #@79
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7c
    const-string v3, "LENGTH : "

    #@7e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v2

    #@82
    const/16 v3, 0xc3

    #@84
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@87
    move-result-object v3

    #@88
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v2

    #@8c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8f
    move-result-object v2

    #@90
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 1347
    const-string v1, "BluetoothPbapObexServer"

    #@95
    new-instance v2, Ljava/lang/StringBuilder;

    #@97
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9a
    const-string v3, "TIME_ISO_8601 : "

    #@9c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v2

    #@a0
    const/16 v3, 0x44

    #@a2
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@a5
    move-result-object v3

    #@a6
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v2

    #@aa
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ad
    move-result-object v2

    #@ae
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b1
    .line 1348
    const-string v1, "BluetoothPbapObexServer"

    #@b3
    new-instance v2, Ljava/lang/StringBuilder;

    #@b5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b8
    const-string v3, "TIME_4_BYTE : "

    #@ba
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bd
    move-result-object v2

    #@be
    const/16 v3, 0xc4

    #@c0
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@c3
    move-result-object v3

    #@c4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@c7
    move-result-object v2

    #@c8
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@cb
    move-result-object v2

    #@cc
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    .line 1349
    const-string v1, "BluetoothPbapObexServer"

    #@d1
    new-instance v2, Ljava/lang/StringBuilder;

    #@d3
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d6
    const-string v3, "DESCRIPTION : "

    #@d8
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@db
    move-result-object v2

    #@dc
    const/4 v3, 0x5

    #@dd
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@e0
    move-result-object v3

    #@e1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v2

    #@e5
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e8
    move-result-object v2

    #@e9
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ec
    .line 1350
    const-string v1, "BluetoothPbapObexServer"

    #@ee
    new-instance v2, Ljava/lang/StringBuilder;

    #@f0
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f3
    const-string v3, "TARGET : "

    #@f5
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v2

    #@f9
    const/16 v3, 0x46

    #@fb
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@fe
    move-result-object v3

    #@ff
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v2

    #@103
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v2

    #@107
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    .line 1351
    const-string v1, "BluetoothPbapObexServer"

    #@10c
    new-instance v2, Ljava/lang/StringBuilder;

    #@10e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@111
    const-string v3, "HTTP : "

    #@113
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v2

    #@117
    const/16 v3, 0x47

    #@119
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@11c
    move-result-object v3

    #@11d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v2

    #@121
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v2

    #@125
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@128
    .line 1352
    const-string v1, "BluetoothPbapObexServer"

    #@12a
    new-instance v2, Ljava/lang/StringBuilder;

    #@12c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12f
    const-string v3, "WHO : "

    #@131
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@134
    move-result-object v2

    #@135
    const/16 v3, 0x4a

    #@137
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@13a
    move-result-object v3

    #@13b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v2

    #@13f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@142
    move-result-object v2

    #@143
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    .line 1353
    const-string v1, "BluetoothPbapObexServer"

    #@148
    new-instance v2, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    const-string v3, "OBJECT_CLASS : "

    #@14f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v2

    #@153
    const/16 v3, 0x4f

    #@155
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@158
    move-result-object v3

    #@159
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@15c
    move-result-object v2

    #@15d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@160
    move-result-object v2

    #@161
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@164
    .line 1354
    const-string v1, "BluetoothPbapObexServer"

    #@166
    new-instance v2, Ljava/lang/StringBuilder;

    #@168
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16b
    const-string v3, "APPLICATION_PARAMETER : "

    #@16d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v2

    #@171
    const/16 v3, 0x4c

    #@173
    invoke-virtual {p0, v3}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@176
    move-result-object v3

    #@177
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17a
    move-result-object v2

    #@17b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17e
    move-result-object v2

    #@17f
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_182
    .catch Ljava/io/IOException; {:try_start_1c .. :try_end_182} :catch_183

    #@182
    .line 1358
    :goto_182
    return-void

    #@183
    .line 1355
    :catch_183
    move-exception v0

    #@184
    .line 1356
    .local v0, e:Ljava/io/IOException;
    const-string v1, "BluetoothPbapObexServer"

    #@186
    new-instance v2, Ljava/lang/StringBuilder;

    #@188
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18b
    const-string v3, "dump HeaderSet error "

    #@18d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@190
    move-result-object v2

    #@191
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v2

    #@195
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v2

    #@199
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19c
    goto :goto_182
.end method

.method private final parseApplicationParameter([BLcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;)Z
    .registers 14
    .parameter "appParam"
    .parameter "appParamValue"

    #@0
    .prologue
    const/16 v10, 0xa

    #@2
    .line 584
    const/4 v1, 0x0

    #@3
    .line 585
    .local v1, i:I
    const/4 v6, 0x1

    #@4
    .line 586
    .local v6, parseOk:Z
    const-string v7, "=================================================="

    #@6
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@9
    .line 587
    new-instance v7, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v8, "parseApplicationParameter() : length ("

    #@10
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v7

    #@14
    array-length v8, p1

    #@15
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v7

    #@19
    const-string v8, ")"

    #@1b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v7

    #@1f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v7

    #@23
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@26
    .line 588
    const-string v7, "=================================================="

    #@28
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@2b
    .line 590
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@2d
    if-eqz v7, :cond_34

    #@2f
    .line 591
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@31
    invoke-virtual {v7}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->clearPbapFilter()V

    #@34
    .line 594
    :cond_34
    :goto_34
    array-length v7, p1

    #@35
    if-ge v1, v7, :cond_30e

    #@37
    .line 595
    aget-byte v7, p1, v1

    #@39
    packed-switch v7, :pswitch_data_318

    #@3c
    .line 676
    new-instance v7, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v8, "parseApplicationParameter() : appParam["

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v7

    #@4b
    const-string v8, "] => error"

    #@4d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v7

    #@51
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@54
    move-result-object v7

    #@55
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@58
    .line 677
    const/4 v6, 0x0

    #@59
    .line 678
    const-string v7, "BluetoothPbapObexServer"

    #@5b
    const-string v8, "Parse Application Parameter error"

    #@5d
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    goto :goto_34

    #@61
    .line 597
    :pswitch_61
    new-instance v7, Ljava/lang/StringBuilder;

    #@63
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@66
    const-string v8, "- appParam["

    #@68
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v7

    #@6c
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v7

    #@70
    const-string v8, "] => FILTER_TAGID : 0x"

    #@72
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v7

    #@76
    aget-byte v8, p1, v1

    #@78
    and-int/lit16 v8, v8, 0xff

    #@7a
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@7d
    move-result-object v8

    #@7e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v7

    #@86
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@89
    .line 598
    new-instance v7, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v8, "- appParam["

    #@90
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v7

    #@94
    add-int/lit8 v8, v1, 0x1

    #@96
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@99
    move-result-object v7

    #@9a
    const-string v8, "] => FILTER_TAGID : 0x"

    #@9c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9f
    move-result-object v7

    #@a0
    add-int/lit8 v8, v1, 0x1

    #@a2
    aget-byte v8, p1, v8

    #@a4
    and-int/lit16 v8, v8, 0xff

    #@a6
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@a9
    move-result-object v8

    #@aa
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v7

    #@ae
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v7

    #@b2
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@b5
    .line 599
    new-instance v7, Ljava/lang/StringBuilder;

    #@b7
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@ba
    const-string v8, "- appParam["

    #@bc
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bf
    move-result-object v7

    #@c0
    add-int/lit8 v8, v1, 0x2

    #@c2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c5
    move-result-object v7

    #@c6
    const-string v8, "] => FILTER_TAGID : 0x"

    #@c8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v7

    #@cc
    add-int/lit8 v8, v1, 0x2

    #@ce
    aget-byte v8, p1, v8

    #@d0
    and-int/lit16 v8, v8, 0xff

    #@d2
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@d5
    move-result-object v8

    #@d6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v7

    #@da
    add-int/lit8 v8, v1, 0x3

    #@dc
    aget-byte v8, p1, v8

    #@de
    and-int/lit16 v8, v8, 0xff

    #@e0
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@e3
    move-result-object v8

    #@e4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v7

    #@e8
    add-int/lit8 v8, v1, 0x4

    #@ea
    aget-byte v8, p1, v8

    #@ec
    and-int/lit16 v8, v8, 0xff

    #@ee
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@f1
    move-result-object v8

    #@f2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v7

    #@f6
    add-int/lit8 v8, v1, 0x5

    #@f8
    aget-byte v8, p1, v8

    #@fa
    and-int/lit16 v8, v8, 0xff

    #@fc
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@ff
    move-result-object v8

    #@100
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v7

    #@104
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@107
    move-result-object v7

    #@108
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@10b
    .line 602
    new-instance v7, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string v8, "- appParam["

    #@112
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v7

    #@116
    add-int/lit8 v8, v1, 0x6

    #@118
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v7

    #@11c
    const-string v8, "] => FILTER_TAGID : 0x"

    #@11e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@121
    move-result-object v7

    #@122
    add-int/lit8 v8, v1, 0x6

    #@124
    aget-byte v8, p1, v8

    #@126
    and-int/lit16 v8, v8, 0xff

    #@128
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@12b
    move-result-object v8

    #@12c
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v7

    #@130
    add-int/lit8 v8, v1, 0x7

    #@132
    aget-byte v8, p1, v8

    #@134
    and-int/lit16 v8, v8, 0xff

    #@136
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@139
    move-result-object v8

    #@13a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v7

    #@13e
    add-int/lit8 v8, v1, 0x8

    #@140
    aget-byte v8, p1, v8

    #@142
    and-int/lit16 v8, v8, 0xff

    #@144
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@147
    move-result-object v8

    #@148
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v7

    #@14c
    add-int/lit8 v8, v1, 0x9

    #@14e
    aget-byte v8, p1, v8

    #@150
    and-int/lit16 v8, v8, 0xff

    #@152
    invoke-static {v8}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@155
    move-result-object v8

    #@156
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@159
    move-result-object v7

    #@15a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15d
    move-result-object v7

    #@15e
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@161
    .line 606
    new-array v5, v10, [B

    #@163
    .line 607
    .local v5, paramFilter:[B
    const/4 v2, 0x0

    #@164
    .local v2, j:I
    :goto_164
    if-ge v2, v10, :cond_16f

    #@166
    .line 608
    add-int v7, v1, v2

    #@168
    aget-byte v7, p1, v7

    #@16a
    aput-byte v7, v5, v2

    #@16c
    .line 607
    add-int/lit8 v2, v2, 0x1

    #@16e
    goto :goto_164

    #@16f
    .line 610
    :cond_16f
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@171
    if-eqz v7, :cond_178

    #@173
    .line 611
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@175
    invoke-virtual {v7, v5}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->setPbapFilter([B)V

    #@178
    .line 614
    :cond_178
    add-int/lit8 v1, v1, 0x2

    #@17a
    .line 615
    add-int/lit8 v1, v1, 0x8

    #@17c
    .line 616
    goto/16 :goto_34

    #@17e
    .line 618
    .end local v2           #j:I
    .end local v5           #paramFilter:[B
    :pswitch_17e
    add-int/lit8 v1, v1, 0x2

    #@180
    .line 619
    aget-byte v7, p1, v1

    #@182
    invoke-static {v7}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    #@185
    move-result-object v7

    #@186
    iput-object v7, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->order:Ljava/lang/String;

    #@188
    .line 620
    new-instance v7, Ljava/lang/StringBuilder;

    #@18a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@18d
    const-string v8, "- appParam["

    #@18f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@192
    move-result-object v7

    #@193
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@196
    move-result-object v7

    #@197
    const-string v8, "] => ORDER_TAGID : "

    #@199
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19c
    move-result-object v7

    #@19d
    iget-object v8, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->order:Ljava/lang/String;

    #@19f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a2
    move-result-object v7

    #@1a3
    const-string v8, " [0:indexed/1:alphabetical/2:sound]"

    #@1a5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a8
    move-result-object v7

    #@1a9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ac
    move-result-object v7

    #@1ad
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@1b0
    .line 621
    add-int/lit8 v1, v1, 0x1

    #@1b2
    .line 622
    goto/16 :goto_34

    #@1b4
    .line 624
    :pswitch_1b4
    add-int/lit8 v1, v1, 0x1

    #@1b6
    .line 626
    aget-byte v3, p1, v1

    #@1b8
    .line 627
    .local v3, length:I
    if-nez v3, :cond_1bd

    #@1ba
    .line 628
    const/4 v6, 0x0

    #@1bb
    .line 629
    goto/16 :goto_34

    #@1bd
    .line 631
    :cond_1bd
    add-int v7, v1, v3

    #@1bf
    aget-byte v7, p1, v7

    #@1c1
    if-nez v7, :cond_1f5

    #@1c3
    .line 632
    new-instance v7, Ljava/lang/String;

    #@1c5
    add-int/lit8 v8, v1, 0x1

    #@1c7
    add-int/lit8 v9, v3, -0x1

    #@1c9
    invoke-direct {v7, p1, v8, v9}, Ljava/lang/String;-><init>([BII)V

    #@1cc
    iput-object v7, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->searchValue:Ljava/lang/String;

    #@1ce
    .line 636
    :goto_1ce
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1d3
    const-string v8, "- appParam["

    #@1d5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v7

    #@1d9
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1dc
    move-result-object v7

    #@1dd
    const-string v8, "] => SEARCH_VALUE_TAGID : "

    #@1df
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e2
    move-result-object v7

    #@1e3
    iget-object v8, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->searchValue:Ljava/lang/String;

    #@1e5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e8
    move-result-object v7

    #@1e9
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ec
    move-result-object v7

    #@1ed
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@1f0
    .line 637
    add-int/2addr v1, v3

    #@1f1
    .line 638
    add-int/lit8 v1, v1, 0x1

    #@1f3
    .line 639
    goto/16 :goto_34

    #@1f5
    .line 634
    :cond_1f5
    new-instance v7, Ljava/lang/String;

    #@1f7
    add-int/lit8 v8, v1, 0x1

    #@1f9
    invoke-direct {v7, p1, v8, v3}, Ljava/lang/String;-><init>([BII)V

    #@1fc
    iput-object v7, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->searchValue:Ljava/lang/String;

    #@1fe
    goto :goto_1ce

    #@1ff
    .line 641
    .end local v3           #length:I
    :pswitch_1ff
    add-int/lit8 v1, v1, 0x2

    #@201
    .line 642
    aget-byte v7, p1, v1

    #@203
    invoke-static {v7}, Ljava/lang/Byte;->toString(B)Ljava/lang/String;

    #@206
    move-result-object v7

    #@207
    iput-object v7, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->searchAttr:Ljava/lang/String;

    #@209
    .line 643
    new-instance v7, Ljava/lang/StringBuilder;

    #@20b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@20e
    const-string v8, "- appParam["

    #@210
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@213
    move-result-object v7

    #@214
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@217
    move-result-object v7

    #@218
    const-string v8, "] => SEARCH_ATTRIBUTE_TAGID : "

    #@21a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21d
    move-result-object v7

    #@21e
    iget-object v8, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->searchAttr:Ljava/lang/String;

    #@220
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@223
    move-result-object v7

    #@224
    const-string v8, " [0:name/1:number/2:sound]"

    #@226
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@229
    move-result-object v7

    #@22a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22d
    move-result-object v7

    #@22e
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@231
    .line 644
    add-int/lit8 v1, v1, 0x1

    #@233
    .line 645
    goto/16 :goto_34

    #@235
    .line 647
    :pswitch_235
    add-int/lit8 v1, v1, 0x2

    #@237
    .line 648
    aget-byte v7, p1, v1

    #@239
    if-nez v7, :cond_264

    #@23b
    add-int/lit8 v7, v1, 0x1

    #@23d
    aget-byte v7, p1, v7

    #@23f
    if-nez v7, :cond_264

    #@241
    .line 649
    const/4 v7, 0x1

    #@242
    iput-boolean v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedPhonebookSize:Z

    #@244
    .line 650
    new-instance v7, Ljava/lang/StringBuilder;

    #@246
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@249
    const-string v8, "- appParam["

    #@24b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24e
    move-result-object v7

    #@24f
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@252
    move-result-object v7

    #@253
    const-string v8, "] => MAXLISTCOUNT_TAGID : maxListCount (0)"

    #@255
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@258
    move-result-object v7

    #@259
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25c
    move-result-object v7

    #@25d
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@260
    .line 657
    :goto_260
    add-int/lit8 v1, v1, 0x2

    #@262
    .line 658
    goto/16 :goto_34

    #@264
    .line 652
    :cond_264
    aget-byte v7, p1, v1

    #@266
    and-int/lit16 v0, v7, 0xff

    #@268
    .line 653
    .local v0, highValue:I
    add-int/lit8 v7, v1, 0x1

    #@26a
    aget-byte v7, p1, v7

    #@26c
    and-int/lit16 v4, v7, 0xff

    #@26e
    .line 654
    .local v4, lowValue:I
    mul-int/lit16 v7, v0, 0x100

    #@270
    add-int/2addr v7, v4

    #@271
    iput v7, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->maxListCount:I

    #@273
    .line 655
    new-instance v7, Ljava/lang/StringBuilder;

    #@275
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@278
    const-string v8, "- appParam["

    #@27a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27d
    move-result-object v7

    #@27e
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@281
    move-result-object v7

    #@282
    const-string v8, "] => MAXLISTCOUNT_TAGID : maxListCount ("

    #@284
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@287
    move-result-object v7

    #@288
    iget v8, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->maxListCount:I

    #@28a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28d
    move-result-object v7

    #@28e
    const-string v8, ")"

    #@290
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@293
    move-result-object v7

    #@294
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@297
    move-result-object v7

    #@298
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@29b
    goto :goto_260

    #@29c
    .line 660
    .end local v0           #highValue:I
    .end local v4           #lowValue:I
    :pswitch_29c
    add-int/lit8 v1, v1, 0x2

    #@29e
    .line 661
    aget-byte v7, p1, v1

    #@2a0
    and-int/lit16 v0, v7, 0xff

    #@2a2
    .line 662
    .restart local v0       #highValue:I
    add-int/lit8 v7, v1, 0x1

    #@2a4
    aget-byte v7, p1, v7

    #@2a6
    and-int/lit16 v4, v7, 0xff

    #@2a8
    .line 663
    .restart local v4       #lowValue:I
    mul-int/lit16 v7, v0, 0x100

    #@2aa
    add-int/2addr v7, v4

    #@2ab
    iput v7, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->listStartOffset:I

    #@2ad
    .line 664
    new-instance v7, Ljava/lang/StringBuilder;

    #@2af
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2b2
    const-string v8, "- appParam["

    #@2b4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b7
    move-result-object v7

    #@2b8
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2bb
    move-result-object v7

    #@2bc
    const-string v8, "] => LISTSTARTOFFSET_TAGID : offset ("

    #@2be
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c1
    move-result-object v7

    #@2c2
    iget v8, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->listStartOffset:I

    #@2c4
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c7
    move-result-object v7

    #@2c8
    const-string v8, ")"

    #@2ca
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2cd
    move-result-object v7

    #@2ce
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2d1
    move-result-object v7

    #@2d2
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@2d5
    .line 665
    add-int/lit8 v1, v1, 0x2

    #@2d7
    .line 666
    goto/16 :goto_34

    #@2d9
    .line 668
    .end local v0           #highValue:I
    .end local v4           #lowValue:I
    :pswitch_2d9
    add-int/lit8 v1, v1, 0x2

    #@2db
    .line 669
    aget-byte v7, p1, v1

    #@2dd
    if-eqz v7, :cond_2e2

    #@2df
    .line 670
    const/4 v7, 0x0

    #@2e0
    iput-boolean v7, p2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->vcard21:Z

    #@2e2
    .line 672
    :cond_2e2
    new-instance v7, Ljava/lang/StringBuilder;

    #@2e4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2e7
    const-string v8, "- appParam["

    #@2e9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2ec
    move-result-object v7

    #@2ed
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f0
    move-result-object v7

    #@2f1
    const-string v8, "] => FORMAT_TAGID : "

    #@2f3
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f6
    move-result-object v7

    #@2f7
    aget-byte v8, p1, v1

    #@2f9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2fc
    move-result-object v7

    #@2fd
    const-string v8, " [0:v2.1/1:v3.0]"

    #@2ff
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@302
    move-result-object v7

    #@303
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@306
    move-result-object v7

    #@307
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@30a
    .line 673
    add-int/lit8 v1, v1, 0x1

    #@30c
    .line 674
    goto/16 :goto_34

    #@30e
    .line 682
    :cond_30e
    const-string v7, "=================================================="

    #@310
    invoke-direct {p0, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@313
    .line 685
    invoke-virtual {p2}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->dump()V

    #@316
    .line 688
    return v6

    #@317
    .line 595
    nop

    #@318
    :pswitch_data_318
    .packed-switch 0x1
        :pswitch_17e
        :pswitch_1b4
        :pswitch_1ff
        :pswitch_235
        :pswitch_29c
        :pswitch_61
        :pswitch_2d9
    .end packed-switch
.end method

.method private final pullPhonebook([BLcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;Ljavax/obex/HeaderSet;Ljavax/obex/Operation;Ljava/lang/String;)I
    .registers 31
    .parameter "appParam"
    .parameter "appParamValue"
    .parameter "reply"
    .parameter "op"
    .parameter "name"

    #@0
    .prologue
    .line 1164
    if-eqz p5, :cond_22

    #@2
    .line 1165
    const-string v5, "BluetoothPbapObexServer"

    #@4
    new-instance v6, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v7, "### pullPhonebook() : name("

    #@b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    move-object/from16 v0, p5

    #@11
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    const-string v7, ")"

    #@17
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v6

    #@1b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v6

    #@1f
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 1168
    :cond_22
    if-eqz p5, :cond_53

    #@24
    .line 1169
    const-string v5, "."

    #@26
    move-object/from16 v0, p5

    #@28
    invoke-virtual {v0, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@2b
    move-result v20

    #@2c
    .line 1170
    .local v20, dotIndex:I
    const-string v24, "vcf"

    #@2e
    .line 1171
    .local v24, vcf:Ljava/lang/String;
    if-ltz v20, :cond_53

    #@30
    invoke-virtual/range {p5 .. p5}, Ljava/lang/String;->length()I

    #@33
    move-result v5

    #@34
    move/from16 v0, v20

    #@36
    if-gt v0, v5, :cond_53

    #@38
    .line 1172
    add-int/lit8 v5, v20, 0x1

    #@3a
    const/4 v6, 0x0

    #@3b
    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->length()I

    #@3e
    move-result v7

    #@3f
    move-object/from16 v0, p5

    #@41
    move-object/from16 v1, v24

    #@43
    invoke-virtual {v0, v5, v1, v6, v7}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    #@46
    move-result v5

    #@47
    if-nez v5, :cond_53

    #@49
    .line 1173
    const-string v5, "BluetoothPbapObexServer"

    #@4b
    const-string v6, "name is not .vcf"

    #@4d
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 1174
    const/16 v21, 0xc6

    #@52
    .line 1239
    .end local v20           #dotIndex:I
    .end local v24           #vcf:Ljava/lang/String;
    :cond_52
    :goto_52
    return v21

    #@53
    .line 1179
    :cond_53
    move-object/from16 v0, p0

    #@55
    iget-object v5, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@57
    move-object/from16 v0, p2

    #@59
    iget v6, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@5b
    invoke-virtual {v5, v6}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->getPhonebookSize(I)I

    #@5e
    move-result v22

    #@5f
    .line 1180
    .local v22, pbSize:I
    move-object/from16 v0, p0

    #@61
    move-object/from16 v1, p2

    #@63
    move/from16 v2, v22

    #@65
    move-object/from16 v3, p3

    #@67
    move-object/from16 v4, p4

    #@69
    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->handleAppParaForResponse(Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;ILjavax/obex/HeaderSet;Ljavax/obex/Operation;)I

    #@6c
    move-result v21

    #@6d
    .line 1181
    .local v21, needSendBody:I
    const/4 v5, -0x1

    #@6e
    move/from16 v0, v21

    #@70
    if-ne v0, v5, :cond_52

    #@72
    .line 1185
    if-nez v22, :cond_85

    #@74
    .line 1187
    const-string v5, "BluetoothPbapObexServer"

    #@76
    const-string v6, "PhonebookSize is 0, return."

    #@78
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7b
    .line 1189
    const-string v5, "### pullPhonebook() : pbSize is 0"

    #@7d
    move-object/from16 v0, p0

    #@7f
    invoke-direct {v0, v5}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@82
    .line 1190
    const/16 v21, 0xa0

    #@84
    goto :goto_52

    #@85
    .line 1193
    :cond_85
    move-object/from16 v0, p2

    #@87
    iget v5, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->maxListCount:I

    #@89
    move/from16 v0, v22

    #@8b
    if-lt v0, v5, :cond_b8

    #@8d
    move-object/from16 v0, p2

    #@8f
    iget v0, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->maxListCount:I

    #@91
    move/from16 v23, v0

    #@93
    .line 1195
    .local v23, requestSize:I
    :goto_93
    move-object/from16 v0, p2

    #@95
    iget v13, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->listStartOffset:I

    #@97
    .line 1196
    .local v13, startPoint:I
    if-ltz v13, :cond_9d

    #@99
    move/from16 v0, v22

    #@9b
    if-lt v13, v0, :cond_bb

    #@9d
    .line 1197
    :cond_9d
    const-string v5, "BluetoothPbapObexServer"

    #@9f
    new-instance v6, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v7, "listStartOffset is not correct! "

    #@a6
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v6

    #@aa
    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v6

    #@ae
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v6

    #@b2
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 1198
    const/16 v21, 0xa0

    #@b7
    goto :goto_52

    #@b8
    .end local v13           #startPoint:I
    .end local v23           #requestSize:I
    :cond_b8
    move/from16 v23, v22

    #@ba
    .line 1193
    goto :goto_93

    #@bb
    .line 1203
    .restart local v13       #startPoint:I
    .restart local v23       #requestSize:I
    :cond_bb
    move-object/from16 v0, p2

    #@bd
    iget v5, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@bf
    const/4 v6, 0x1

    #@c0
    if-eq v5, v6, :cond_d1

    #@c2
    move-object/from16 v0, p2

    #@c4
    iget v5, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@c6
    const/4 v6, 0x6

    #@c7
    if-eq v5, v6, :cond_d1

    #@c9
    .line 1205
    sget v5, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->CALLLOG_NUM_LIMIT:I

    #@cb
    move/from16 v0, v23

    #@cd
    if-le v0, v5, :cond_d1

    #@cf
    .line 1206
    sget v23, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->CALLLOG_NUM_LIMIT:I

    #@d1
    .line 1210
    :cond_d1
    add-int v5, v13, v23

    #@d3
    add-int/lit8 v8, v5, -0x1

    #@d5
    .line 1211
    .local v8, endPoint:I
    add-int/lit8 v5, v22, -0x1

    #@d7
    if-le v8, v5, :cond_db

    #@d9
    .line 1212
    add-int/lit8 v8, v22, -0x1

    #@db
    .line 1215
    :cond_db
    const-string v5, "BluetoothPbapObexServer"

    #@dd
    new-instance v6, Ljava/lang/StringBuilder;

    #@df
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@e2
    const-string v7, "pullPhonebook(): requestSize="

    #@e4
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v6

    #@e8
    move/from16 v0, v23

    #@ea
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ed
    move-result-object v6

    #@ee
    const-string v7, " startPoint="

    #@f0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f3
    move-result-object v6

    #@f4
    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v6

    #@f8
    const-string v7, " endPoint="

    #@fa
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v6

    #@fe
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@101
    move-result-object v6

    #@102
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v6

    #@106
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@109
    .line 1218
    new-instance v5, Ljava/lang/StringBuilder;

    #@10b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@10e
    const-string v6, "### pullPhonebook() : requestSize = "

    #@110
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v5

    #@114
    move/from16 v0, v23

    #@116
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@119
    move-result-object v5

    #@11a
    const-string v6, " startPoint = "

    #@11c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v5

    #@120
    invoke-virtual {v5, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@123
    move-result-object v5

    #@124
    const-string v6, " endPoint = "

    #@126
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@129
    move-result-object v5

    #@12a
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v5

    #@12e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@131
    move-result-object v5

    #@132
    move-object/from16 v0, p0

    #@134
    invoke-direct {v0, v5}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@137
    .line 1220
    move-object/from16 v0, p2

    #@139
    iget-boolean v9, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->vcard21:Z

    #@13b
    .line 1223
    .local v9, vcard21:Z
    move-object/from16 v0, p2

    #@13d
    iget v5, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@13f
    const/4 v6, 0x1

    #@140
    if-eq v5, v6, :cond_149

    #@142
    move-object/from16 v0, p2

    #@144
    iget v5, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@146
    const/4 v6, 0x6

    #@147
    if-ne v5, v6, :cond_17d

    #@149
    .line 1226
    :cond_149
    if-nez v13, :cond_16d

    #@14b
    .line 1227
    move-object/from16 v0, p0

    #@14d
    iget-object v5, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@14f
    const/4 v6, 0x0

    #@150
    invoke-virtual {v5, v9, v6}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->getOwnerPhoneNumberVcard(Z[B)Ljava/lang/String;

    #@153
    move-result-object v10

    #@154
    .line 1228
    .local v10, ownerVcard:Ljava/lang/String;
    if-nez v8, :cond_160

    #@156
    .line 1229
    move-object/from16 v0, p0

    #@158
    move-object/from16 v1, p4

    #@15a
    invoke-direct {v0, v1, v10}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->pushBytes(Ljavax/obex/Operation;Ljava/lang/String;)I

    #@15d
    move-result v21

    #@15e
    goto/16 :goto_52

    #@160
    .line 1231
    :cond_160
    move-object/from16 v0, p0

    #@162
    iget-object v5, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@164
    const/4 v7, 0x1

    #@165
    move-object/from16 v6, p4

    #@167
    invoke-virtual/range {v5 .. v10}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->composeAndSendPhonebookVcards(Ljavax/obex/Operation;IIZLjava/lang/String;)I

    #@16a
    move-result v21

    #@16b
    goto/16 :goto_52

    #@16d
    .line 1235
    .end local v10           #ownerVcard:Ljava/lang/String;
    :cond_16d
    move-object/from16 v0, p0

    #@16f
    iget-object v11, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@171
    const/16 v16, 0x0

    #@173
    move-object/from16 v12, p4

    #@175
    move v14, v8

    #@176
    move v15, v9

    #@177
    invoke-virtual/range {v11 .. v16}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->composeAndSendPhonebookVcards(Ljavax/obex/Operation;IIZLjava/lang/String;)I

    #@17a
    move-result v21

    #@17b
    goto/16 :goto_52

    #@17d
    .line 1239
    :cond_17d
    move-object/from16 v0, p0

    #@17f
    iget-object v14, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@181
    move-object/from16 v0, p2

    #@183
    iget v15, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@185
    add-int/lit8 v17, v13, 0x1

    #@187
    add-int/lit8 v18, v8, 0x1

    #@189
    move-object/from16 v16, p4

    #@18b
    move/from16 v19, v9

    #@18d
    invoke-virtual/range {v14 .. v19}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->composeAndSendCallLogVcards(ILjavax/obex/Operation;IIZ)I

    #@190
    move-result v21

    #@191
    goto/16 :goto_52
.end method

.method private final pullVcardEntry([BLcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;Ljavax/obex/Operation;Ljava/lang/String;Ljava/lang/String;)I
    .registers 21
    .parameter "appParam"
    .parameter "appParamValue"
    .parameter "op"
    .parameter "name"
    .parameter "current_path"

    #@0
    .prologue
    .line 1099
    if-eqz p4, :cond_9

    #@2
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    #@5
    move-result v1

    #@6
    const/4 v2, 0x5

    #@7
    if-ge v1, v2, :cond_13

    #@9
    .line 1101
    :cond_9
    const-string v1, "BluetoothPbapObexServer"

    #@b
    const-string v2, "Name is Null, or the length of name < 5 !"

    #@d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 1103
    const/16 v1, 0xc6

    #@12
    .line 1159
    :goto_12
    return v1

    #@13
    .line 1105
    :cond_13
    if-eqz p4, :cond_35

    #@15
    .line 1106
    const-string v1, "BluetoothPbapObexServer"

    #@17
    new-instance v2, Ljava/lang/StringBuilder;

    #@19
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1c
    const-string v5, "### pullVcardEntry() : name("

    #@1e
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    move-object/from16 v0, p4

    #@24
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v2

    #@28
    const-string v5, ")"

    #@2a
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v2

    #@32
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 1108
    :cond_35
    const/4 v1, 0x0

    #@36
    invoke-virtual/range {p4 .. p4}, Ljava/lang/String;->length()I

    #@39
    move-result v2

    #@3a
    add-int/lit8 v2, v2, -0x5

    #@3c
    add-int/lit8 v2, v2, 0x1

    #@3e
    move-object/from16 v0, p4

    #@40
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@43
    move-result-object v14

    #@44
    .line 1109
    .local v14, strIndex:Ljava/lang/String;
    const/4 v3, 0x0

    #@45
    .line 1110
    .local v3, intIndex:I
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@48
    move-result-object v1

    #@49
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@4c
    move-result v1

    #@4d
    if-eqz v1, :cond_53

    #@4f
    .line 1112
    :try_start_4f
    invoke-static {v14}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_52
    .catch Ljava/lang/NumberFormatException; {:try_start_4f .. :try_end_52} :catch_69

    #@52
    move-result v3

    #@53
    .line 1119
    :cond_53
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@55
    move-object/from16 v0, p2

    #@57
    iget v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@59
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->getPhonebookSize(I)I

    #@5c
    move-result v13

    #@5d
    .line 1120
    .local v13, size:I
    if-nez v13, :cond_89

    #@5f
    .line 1122
    const-string v1, "BluetoothPbapObexServer"

    #@61
    const-string v2, "PhonebookSize is 0, return."

    #@63
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 1124
    const/16 v1, 0xc4

    #@68
    goto :goto_12

    #@69
    .line 1113
    .end local v13           #size:I
    :catch_69
    move-exception v11

    #@6a
    .line 1114
    .local v11, e:Ljava/lang/NumberFormatException;
    const-string v1, "BluetoothPbapObexServer"

    #@6c
    new-instance v2, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v5, "catch number format exception "

    #@73
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v2

    #@77
    invoke-virtual {v11}, Ljava/lang/NumberFormatException;->toString()Ljava/lang/String;

    #@7a
    move-result-object v5

    #@7b
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@82
    move-result-object v2

    #@83
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    .line 1115
    const/16 v1, 0xc6

    #@88
    goto :goto_12

    #@89
    .line 1127
    .end local v11           #e:Ljava/lang/NumberFormatException;
    .restart local v13       #size:I
    :cond_89
    move-object/from16 v0, p2

    #@8b
    iget-boolean v4, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->vcard21:Z

    #@8d
    .line 1128
    .local v4, vcard21:Z
    move-object/from16 v0, p2

    #@8f
    iget v1, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@91
    if-nez v1, :cond_9e

    #@93
    .line 1129
    const-string v1, "BluetoothPbapObexServer"

    #@95
    const-string v2, "wrong path!"

    #@97
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@9a
    .line 1130
    const/16 v1, 0xc6

    #@9c
    goto/16 :goto_12

    #@9e
    .line 1133
    :cond_9e
    move-object/from16 v0, p2

    #@a0
    iget v1, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@a2
    const/4 v2, 0x1

    #@a3
    if-eq v1, v2, :cond_ac

    #@a5
    move-object/from16 v0, p2

    #@a7
    iget v1, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@a9
    const/4 v2, 0x6

    #@aa
    if-ne v1, v2, :cond_ec

    #@ac
    .line 1136
    :cond_ac
    if-ltz v3, :cond_b0

    #@ae
    if-lt v3, v13, :cond_ce

    #@b0
    .line 1137
    :cond_b0
    const-string v1, "BluetoothPbapObexServer"

    #@b2
    new-instance v2, Ljava/lang/StringBuilder;

    #@b4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b7
    const-string v5, "The requested vcard is not acceptable! name= "

    #@b9
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v2

    #@bd
    move-object/from16 v0, p4

    #@bf
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v2

    #@c3
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c6
    move-result-object v2

    #@c7
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ca
    .line 1138
    const/16 v1, 0xc4

    #@cc
    goto/16 :goto_12

    #@ce
    .line 1139
    :cond_ce
    if-nez v3, :cond_df

    #@d0
    .line 1141
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@d2
    const/4 v2, 0x0

    #@d3
    invoke-virtual {v1, v4, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->getOwnerPhoneNumberVcard(Z[B)Ljava/lang/String;

    #@d6
    move-result-object v12

    #@d7
    .line 1142
    .local v12, ownerVcard:Ljava/lang/String;
    move-object/from16 v0, p3

    #@d9
    invoke-direct {p0, v0, v12}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->pushBytes(Ljavax/obex/Operation;Ljava/lang/String;)I

    #@dc
    move-result v1

    #@dd
    goto/16 :goto_12

    #@df
    .line 1144
    .end local v12           #ownerVcard:Ljava/lang/String;
    :cond_df
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@e1
    const/4 v5, 0x0

    #@e2
    iget v6, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mOrderBy:I

    #@e4
    move-object/from16 v2, p3

    #@e6
    invoke-virtual/range {v1 .. v6}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->composeAndSendPhonebookOneVcard(Ljavax/obex/Operation;IZLjava/lang/String;I)I

    #@e9
    move-result v1

    #@ea
    goto/16 :goto_12

    #@ec
    .line 1148
    :cond_ec
    if-lez v3, :cond_f0

    #@ee
    if-le v3, v13, :cond_10e

    #@f0
    .line 1149
    :cond_f0
    const-string v1, "BluetoothPbapObexServer"

    #@f2
    new-instance v2, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v5, "The requested vcard is not acceptable! name= "

    #@f9
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v2

    #@fd
    move-object/from16 v0, p4

    #@ff
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@102
    move-result-object v2

    #@103
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v2

    #@107
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    .line 1150
    const/16 v1, 0xc4

    #@10c
    goto/16 :goto_12

    #@10e
    .line 1154
    :cond_10e
    const/4 v1, 0x1

    #@10f
    if-lt v3, v1, :cond_122

    #@111
    .line 1155
    iget-object v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@113
    move-object/from16 v0, p2

    #@115
    iget v6, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@117
    move-object/from16 v7, p3

    #@119
    move v8, v3

    #@11a
    move v9, v3

    #@11b
    move v10, v4

    #@11c
    invoke-virtual/range {v5 .. v10}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->composeAndSendCallLogVcards(ILjavax/obex/Operation;IIZ)I

    #@11f
    move-result v1

    #@120
    goto/16 :goto_12

    #@122
    .line 1159
    :cond_122
    const/16 v1, 0xa0

    #@124
    goto/16 :goto_12
.end method

.method private final pullVcardListing([BLcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;Ljavax/obex/HeaderSet;Ljavax/obex/Operation;)I
    .registers 20
    .parameter "appParam"
    .parameter "appParamValue"
    .parameter "reply"
    .parameter "op"

    #@0
    .prologue
    .line 1024
    move-object/from16 v0, p2

    #@2
    iget-object v3, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->searchAttr:Ljava/lang/String;

    #@4
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@7
    move-result-object v12

    #@8
    .line 1025
    .local v12, searchAttr:Ljava/lang/String;
    if-eqz v12, :cond_2e

    #@a
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@d
    move-result v3

    #@e
    if-lez v3, :cond_2e

    #@10
    .line 1026
    const-string v3, "BluetoothPbapObexServer"

    #@12
    new-instance v4, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v5, "### pullVcardListing() : searchAttr("

    #@19
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v4

    #@1d
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    const-string v5, ")"

    #@23
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v4

    #@27
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v4

    #@2b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 1029
    :cond_2e
    if-eqz v12, :cond_36

    #@30
    invoke-virtual {v12}, Ljava/lang/String;->length()I

    #@33
    move-result v3

    #@34
    if-nez v3, :cond_5b

    #@36
    .line 1031
    :cond_36
    const-string v3, "0"

    #@38
    move-object/from16 v0, p2

    #@3a
    iput-object v3, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->searchAttr:Ljava/lang/String;

    #@3c
    .line 1033
    const-string v3, "BluetoothPbapObexServer"

    #@3e
    const-string v4, "searchAttr is not set by PCE, assume search by name by default"

    #@40
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 1047
    :goto_43
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@45
    move-object/from16 v0, p2

    #@47
    iget v4, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@49
    invoke-virtual {v3, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->getPhonebookSize(I)I

    #@4c
    move-result v14

    #@4d
    .line 1048
    .local v14, size:I
    move-object/from16 v0, p2

    #@4f
    move-object/from16 v1, p3

    #@51
    move-object/from16 v2, p4

    #@53
    invoke-direct {p0, v0, v14, v1, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->handleAppParaForResponse(Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;ILjavax/obex/HeaderSet;Ljavax/obex/Operation;)I

    #@56
    move-result v10

    #@57
    .line 1049
    .local v10, needSendBody:I
    const/4 v3, -0x1

    #@58
    if-eq v10, v3, :cond_a0

    #@5a
    .line 1094
    .end local v10           #needSendBody:I
    .end local v14           #size:I
    :goto_5a
    return v10

    #@5b
    .line 1035
    :cond_5b
    const-string v3, "0"

    #@5d
    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@60
    move-result v3

    #@61
    if-nez v3, :cond_87

    #@63
    const-string v3, "1"

    #@65
    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@68
    move-result v3

    #@69
    if-nez v3, :cond_87

    #@6b
    .line 1036
    const-string v3, "BluetoothPbapObexServer"

    #@6d
    const-string v4, "search attr not supported"

    #@6f
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 1037
    const-string v3, "2"

    #@74
    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@77
    move-result v3

    #@78
    if-eqz v3, :cond_84

    #@7a
    .line 1039
    const-string v3, "BluetoothPbapObexServer"

    #@7c
    const-string v4, "do not support search by sound"

    #@7e
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 1040
    const/16 v10, 0xd1

    #@83
    goto :goto_5a

    #@84
    .line 1042
    :cond_84
    const/16 v10, 0xcc

    #@86
    goto :goto_5a

    #@87
    .line 1044
    :cond_87
    const-string v3, "BluetoothPbapObexServer"

    #@89
    new-instance v4, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v5, "searchAttr is valid: "

    #@90
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v4

    #@94
    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v4

    #@98
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v4

    #@9c
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    goto :goto_43

    #@a0
    .line 1053
    .restart local v10       #needSendBody:I
    .restart local v14       #size:I
    :cond_a0
    if-nez v14, :cond_ac

    #@a2
    .line 1055
    const-string v3, "BluetoothPbapObexServer"

    #@a4
    const-string v4, "PhonebookSize is 0, return."

    #@a6
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 1057
    const/16 v10, 0xa0

    #@ab
    goto :goto_5a

    #@ac
    .line 1060
    :cond_ac
    move-object/from16 v0, p2

    #@ae
    iget-object v3, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->order:Ljava/lang/String;

    #@b0
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@b3
    move-result-object v11

    #@b4
    .line 1061
    .local v11, orderPara:Ljava/lang/String;
    if-eqz v11, :cond_d8

    #@b6
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@b9
    move-result v3

    #@ba
    if-lez v3, :cond_d8

    #@bc
    .line 1062
    new-instance v3, Ljava/lang/StringBuilder;

    #@be
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c1
    const-string v4, "### pullVcardListing() : orderPara("

    #@c3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v3

    #@c7
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v3

    #@cb
    const-string v4, ")"

    #@cd
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d0
    move-result-object v3

    #@d1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d4
    move-result-object v3

    #@d5
    invoke-direct {p0, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->BtUiLog(Ljava/lang/String;)V

    #@d8
    .line 1064
    :cond_d8
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@db
    move-result v3

    #@dc
    if-eqz v3, :cond_111

    #@de
    .line 1066
    const-string v11, "0"

    #@e0
    .line 1068
    const-string v3, "BluetoothPbapObexServer"

    #@e2
    const-string v4, "Order parameter is not set by PCE. Assume order by \'Indexed\' by default"

    #@e4
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e7
    .line 1085
    :goto_e7
    const-string v3, "0"

    #@e9
    invoke-virtual {v11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ec
    move-result v3

    #@ed
    if-eqz v3, :cond_16e

    #@ef
    .line 1086
    sget v3, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->ORDER_BY_INDEXED:I

    #@f1
    iput v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mOrderBy:I

    #@f3
    .line 1091
    :cond_f3
    :goto_f3
    move-object/from16 v0, p2

    #@f5
    iget v4, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@f7
    move-object/from16 v0, p2

    #@f9
    iget v6, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->maxListCount:I

    #@fb
    move-object/from16 v0, p2

    #@fd
    iget v7, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->listStartOffset:I

    #@ff
    move-object/from16 v0, p2

    #@101
    iget-object v8, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->searchValue:Ljava/lang/String;

    #@103
    move-object/from16 v0, p2

    #@105
    iget-object v9, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->searchAttr:Ljava/lang/String;

    #@107
    move-object v3, p0

    #@108
    move-object/from16 v5, p4

    #@10a
    invoke-direct/range {v3 .. v9}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->sendVcardListingXml(ILjavax/obex/Operation;IILjava/lang/String;Ljava/lang/String;)I

    #@10d
    move-result v13

    #@10e
    .local v13, sendResult:I
    move v10, v13

    #@10f
    .line 1094
    goto/16 :goto_5a

    #@111
    .line 1071
    .end local v13           #sendResult:I
    :cond_111
    const-string v3, "0"

    #@113
    invoke-virtual {v11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@116
    move-result v3

    #@117
    if-nez v3, :cond_154

    #@119
    const-string v3, "1"

    #@11b
    invoke-virtual {v11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11e
    move-result v3

    #@11f
    if-nez v3, :cond_154

    #@121
    .line 1073
    const-string v3, "BluetoothPbapObexServer"

    #@123
    new-instance v4, Ljava/lang/StringBuilder;

    #@125
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@128
    const-string v5, "Order parameter is not supported: "

    #@12a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12d
    move-result-object v4

    #@12e
    move-object/from16 v0, p2

    #@130
    iget-object v5, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->order:Ljava/lang/String;

    #@132
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@135
    move-result-object v4

    #@136
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v4

    #@13a
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@13d
    .line 1075
    const-string v3, "2"

    #@13f
    invoke-virtual {v11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@142
    move-result v3

    #@143
    if-eqz v3, :cond_150

    #@145
    .line 1077
    const-string v3, "BluetoothPbapObexServer"

    #@147
    const-string v4, "Do not support order by sound"

    #@149
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14c
    .line 1078
    const/16 v10, 0xd1

    #@14e
    goto/16 :goto_5a

    #@150
    .line 1080
    :cond_150
    const/16 v10, 0xcc

    #@152
    goto/16 :goto_5a

    #@154
    .line 1082
    :cond_154
    const-string v3, "BluetoothPbapObexServer"

    #@156
    new-instance v4, Ljava/lang/StringBuilder;

    #@158
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15b
    const-string v5, "Order parameter is valid: "

    #@15d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v4

    #@161
    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@164
    move-result-object v4

    #@165
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@168
    move-result-object v4

    #@169
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@16c
    goto/16 :goto_e7

    #@16e
    .line 1087
    :cond_16e
    const-string v3, "1"

    #@170
    invoke-virtual {v11, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@173
    move-result v3

    #@174
    if-eqz v3, :cond_f3

    #@176
    .line 1088
    sget v3, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->ORDER_BY_ALPHABETICAL:I

    #@178
    iput v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mOrderBy:I

    #@17a
    goto/16 :goto_f3
.end method

.method private final pushBytes(Ljavax/obex/Operation;Ljava/lang/String;)I
    .registers 9
    .parameter "op"
    .parameter "vcardString"

    #@0
    .prologue
    .line 911
    if-eqz p2, :cond_8

    #@2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    #@5
    move-result v3

    #@6
    if-nez v3, :cond_12

    #@8
    .line 913
    :cond_8
    const-string v3, "BluetoothPbapObexServer"

    #@a
    const-string v4, "vcardString is null!"

    #@c
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 914
    const/16 v2, 0xa0

    #@11
    .line 934
    :cond_11
    :goto_11
    return v2

    #@12
    .line 917
    :cond_12
    const/4 v1, 0x0

    #@13
    .line 918
    .local v1, outputStream:Ljava/io/OutputStream;
    const/16 v2, 0xa0

    #@15
    .line 920
    .local v2, pushResult:I
    :try_start_15
    invoke-interface {p1}, Ljavax/obex/Operation;->openOutputStream()Ljava/io/OutputStream;

    #@18
    move-result-object v1

    #@19
    .line 921
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v1, v3}, Ljava/io/OutputStream;->write([B)V

    #@20
    .line 923
    const-string v3, "BluetoothPbapObexServer"

    #@22
    const-string v4, "Send Data complete!"

    #@24
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_27} :catch_30

    #@27
    .line 930
    :goto_27
    invoke-static {v1, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->closeStream(Ljava/io/OutputStream;Ljavax/obex/Operation;)Z

    #@2a
    move-result v3

    #@2b
    if-nez v3, :cond_11

    #@2d
    .line 931
    const/16 v2, 0xd0

    #@2f
    goto :goto_11

    #@30
    .line 925
    :catch_30
    move-exception v0

    #@31
    .line 926
    .local v0, e:Ljava/io/IOException;
    const-string v3, "BluetoothPbapObexServer"

    #@33
    new-instance v4, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v5, "open/write outputstrem failed"

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@41
    move-result-object v5

    #@42
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 927
    const/16 v2, 0xd0

    #@4f
    goto :goto_27
.end method

.method private final pushHeader(Ljavax/obex/Operation;Ljavax/obex/HeaderSet;)I
    .registers 8
    .parameter "op"
    .parameter "reply"

    #@0
    .prologue
    .line 882
    const/4 v1, 0x0

    #@1
    .line 885
    .local v1, outputStream:Ljava/io/OutputStream;
    const-string v3, "BluetoothPbapObexServer"

    #@3
    const-string v4, "Push Header"

    #@5
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 888
    const-string v3, "BluetoothPbapObexServer"

    #@a
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 891
    const/16 v2, 0xa0

    #@13
    .line 893
    .local v2, pushResult:I
    :try_start_13
    invoke-interface {p1, p2}, Ljavax/obex/Operation;->sendHeaders(Ljavax/obex/HeaderSet;)V

    #@16
    .line 894
    invoke-interface {p1}, Ljavax/obex/Operation;->openOutputStream()Ljava/io/OutputStream;

    #@19
    move-result-object v1

    #@1a
    .line 895
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V
    :try_end_1d
    .catchall {:try_start_13 .. :try_end_1d} :catchall_39
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_1d} :catch_26

    #@1d
    .line 900
    invoke-static {v1, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->closeStream(Ljava/io/OutputStream;Ljavax/obex/Operation;)Z

    #@20
    move-result v3

    #@21
    if-nez v3, :cond_25

    #@23
    .line 901
    :goto_23
    const/16 v2, 0xd0

    #@25
    .line 904
    :cond_25
    return v2

    #@26
    .line 896
    :catch_26
    move-exception v0

    #@27
    .line 897
    .local v0, e:Ljava/io/IOException;
    :try_start_27
    const-string v3, "BluetoothPbapObexServer"

    #@29
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@2c
    move-result-object v4

    #@2d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_30
    .catchall {:try_start_27 .. :try_end_30} :catchall_39

    #@30
    .line 898
    const/16 v2, 0xd0

    #@32
    .line 900
    invoke-static {v1, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->closeStream(Ljava/io/OutputStream;Ljavax/obex/Operation;)Z

    #@35
    move-result v3

    #@36
    if-nez v3, :cond_25

    #@38
    goto :goto_23

    #@39
    .end local v0           #e:Ljava/io/IOException;
    :catchall_39
    move-exception v3

    #@3a
    invoke-static {v1, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->closeStream(Ljava/io/OutputStream;Ljavax/obex/Operation;)Z

    #@3d
    move-result v4

    #@3e
    if-nez v4, :cond_42

    #@40
    .line 901
    const/16 v2, 0xd0

    #@42
    .line 900
    :cond_42
    throw v3
.end method

.method private final sendVcardListingXml(ILjavax/obex/Operation;IILjava/lang/String;Ljava/lang/String;)I
    .registers 20
    .parameter "type"
    .parameter "op"
    .parameter "maxListCount"
    .parameter "listStartOffset"
    .parameter "searchValue"
    .parameter "searchAttr"

    #@0
    .prologue
    .line 695
    new-instance v5, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 696
    .local v5, result:Ljava/lang/StringBuilder;
    const/4 v8, 0x0

    #@6
    .line 697
    .local v8, itemsFound:I
    const-string v1, "<?xml version=\"1.0\"?>"

    #@8
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    .line 698
    const-string v1, "<!DOCTYPE vcard-listing SYSTEM \"vcard-listing.dtd\">"

    #@d
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    .line 699
    const-string v1, "<vCard-listing version=\"1.0\">"

    #@12
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 704
    const/4 v1, 0x1

    #@16
    if-eq p1, v1, :cond_1b

    #@18
    const/4 v1, 0x6

    #@19
    if-ne p1, v1, :cond_73

    #@1b
    .line 706
    :cond_1b
    const-string v1, "0"

    #@1d
    move-object/from16 v0, p6

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_58

    #@25
    .line 707
    const-string v6, "name"

    #@27
    move-object v1, p0

    #@28
    move/from16 v2, p3

    #@2a
    move/from16 v3, p4

    #@2c
    move-object/from16 v4, p5

    #@2e
    invoke-direct/range {v1 .. v6}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->createList(IILjava/lang/String;Ljava/lang/StringBuilder;Ljava/lang/String;)I

    #@31
    move-result v8

    #@32
    .line 734
    :cond_32
    :goto_32
    const-string v1, "</vCard-listing>"

    #@34
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    .line 737
    const-string v1, "BluetoothPbapObexServer"

    #@39
    new-instance v2, Ljava/lang/StringBuilder;

    #@3b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3e
    const-string v3, "itemsFound ="

    #@40
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v2

    #@44
    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@47
    move-result-object v2

    #@48
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 740
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-direct {p0, p2, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->pushBytes(Ljavax/obex/Operation;Ljava/lang/String;)I

    #@56
    move-result v1

    #@57
    :goto_57
    return v1

    #@58
    .line 709
    :cond_58
    const-string v1, "1"

    #@5a
    move-object/from16 v0, p6

    #@5c
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v1

    #@60
    if-eqz v1, :cond_70

    #@62
    .line 710
    const-string v6, "number"

    #@64
    move-object v1, p0

    #@65
    move/from16 v2, p3

    #@67
    move/from16 v3, p4

    #@69
    move-object/from16 v4, p5

    #@6b
    invoke-direct/range {v1 .. v6}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->createList(IILjava/lang/String;Ljava/lang/StringBuilder;Ljava/lang/String;)I

    #@6e
    move-result v8

    #@6f
    goto :goto_32

    #@70
    .line 714
    :cond_70
    const/16 v1, 0xcc

    #@72
    goto :goto_57

    #@73
    .line 719
    :cond_73
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@75
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->loadCallHistoryList(I)Ljava/util/ArrayList;

    #@78
    move-result-object v10

    #@79
    .line 720
    .local v10, nameList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@7c
    move-result v1

    #@7d
    move/from16 v0, p3

    #@7f
    if-lt v1, v0, :cond_c6

    #@81
    move/from16 v11, p3

    #@83
    .line 721
    .local v11, requestSize:I
    :goto_83
    move/from16 v12, p4

    #@85
    .line 722
    .local v12, startPoint:I
    add-int v7, v12, v11

    #@87
    .line 723
    .local v7, endPoint:I
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@8a
    move-result v1

    #@8b
    if-le v7, v1, :cond_91

    #@8d
    .line 724
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@90
    move-result v7

    #@91
    .line 727
    :cond_91
    const-string v1, "BluetoothPbapObexServer"

    #@93
    new-instance v2, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v3, "call log list, size="

    #@9a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v2

    #@9e
    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a1
    move-result-object v2

    #@a2
    const-string v3, " offset="

    #@a4
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a7
    move-result-object v2

    #@a8
    move/from16 v0, p4

    #@aa
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v2

    #@ae
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v2

    #@b2
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 730
    move v9, v12

    #@b6
    .local v9, j:I
    :goto_b6
    if-ge v9, v7, :cond_32

    #@b8
    .line 731
    add-int/lit8 v2, v9, 0x1

    #@ba
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@bd
    move-result-object v1

    #@be
    check-cast v1, Ljava/lang/String;

    #@c0
    invoke-direct {p0, v2, v1, v5}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->writeVCardEntry(ILjava/lang/String;Ljava/lang/StringBuilder;)V

    #@c3
    .line 730
    add-int/lit8 v9, v9, 0x1

    #@c5
    goto :goto_b6

    #@c6
    .line 720
    .end local v7           #endPoint:I
    .end local v9           #j:I
    .end local v11           #requestSize:I
    .end local v12           #startPoint:I
    :cond_c6
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    #@c9
    move-result v11

    #@ca
    goto :goto_83
.end method

.method private writeVCardEntry(ILjava/lang/String;Ljava/lang/StringBuilder;)V
    .registers 5
    .parameter "vcfIndex"
    .parameter "name"
    .parameter "result"

    #@0
    .prologue
    .line 1332
    const-string v0, "<card handle=\""

    #@2
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5
    .line 1333
    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8
    .line 1334
    const-string v0, ".vcf\" name=\""

    #@a
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 1335
    invoke-direct {p0, p2, p3}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V

    #@10
    .line 1336
    const-string v0, "\"/>"

    #@12
    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    .line 1337
    return-void
.end method

.method private xmlEncode(Ljava/lang/String;Ljava/lang/StringBuilder;)V
    .registers 6
    .parameter "name"
    .parameter "result"

    #@0
    .prologue
    .line 1301
    if-nez p1, :cond_3

    #@2
    .line 1329
    :cond_2
    return-void

    #@3
    .line 1305
    :cond_3
    new-instance v1, Ljava/text/StringCharacterIterator;

    #@5
    invoke-direct {v1, p1}, Ljava/text/StringCharacterIterator;-><init>(Ljava/lang/String;)V

    #@8
    .line 1306
    .local v1, iterator:Ljava/text/StringCharacterIterator;
    invoke-virtual {v1}, Ljava/text/StringCharacterIterator;->current()C

    #@b
    move-result v0

    #@c
    .line 1307
    .local v0, character:C
    :goto_c
    const v2, 0xffff

    #@f
    if-eq v0, v2, :cond_2

    #@11
    .line 1308
    const/16 v2, 0x3c

    #@13
    if-ne v0, v2, :cond_1f

    #@15
    .line 1309
    const-string v2, "&lt;"

    #@17
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    .line 1327
    :goto_1a
    invoke-virtual {v1}, Ljava/text/StringCharacterIterator;->next()C

    #@1d
    move-result v0

    #@1e
    goto :goto_c

    #@1f
    .line 1311
    :cond_1f
    const/16 v2, 0x3e

    #@21
    if-ne v0, v2, :cond_29

    #@23
    .line 1312
    const-string v2, "&gt;"

    #@25
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    goto :goto_1a

    #@29
    .line 1314
    :cond_29
    const/16 v2, 0x22

    #@2b
    if-ne v0, v2, :cond_33

    #@2d
    .line 1315
    const-string v2, "&quot;"

    #@2f
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    goto :goto_1a

    #@33
    .line 1317
    :cond_33
    const/16 v2, 0x27

    #@35
    if-ne v0, v2, :cond_3d

    #@37
    .line 1318
    const-string v2, "&#039;"

    #@39
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    goto :goto_1a

    #@3d
    .line 1320
    :cond_3d
    const/16 v2, 0x26

    #@3f
    if-ne v0, v2, :cond_47

    #@41
    .line 1321
    const-string v2, "&amp;"

    #@43
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    goto :goto_1a

    #@47
    .line 1325
    :cond_47
    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4a
    goto :goto_1a
.end method


# virtual methods
.method public onAbort(Ljavax/obex/HeaderSet;Ljavax/obex/HeaderSet;)I
    .registers 5
    .parameter "request"
    .parameter "reply"

    #@0
    .prologue
    .line 280
    const-string v0, "BluetoothPbapObexServer"

    #@2
    const-string v1, "onAbort(): enter."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 282
    const/4 v0, 0x1

    #@8
    sput-boolean v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->sIsAborted:Z

    #@a
    .line 283
    const/16 v0, 0xa0

    #@c
    return v0
.end method

.method public final onAuthenticationFailure([B)V
    .registers 2
    .parameter "userName"

    #@0
    .prologue
    .line 1268
    return-void
.end method

.method public onClose()V
    .registers 4

    #@0
    .prologue
    .line 348
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCallback:Landroid/os/Handler;

    #@2
    if-eqz v1, :cond_18

    #@4
    .line 349
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCallback:Landroid/os/Handler;

    #@6
    invoke-static {v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@9
    move-result-object v0

    #@a
    .line 350
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x1388

    #@c
    iput v1, v0, Landroid/os/Message;->what:I

    #@e
    .line 351
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@11
    .line 353
    const-string v1, "BluetoothPbapObexServer"

    #@13
    const-string v2, "onClose(): msg MSG_SERVERSESSION_CLOSE sent out."

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 356
    .end local v0           #msg:Landroid/os/Message;
    :cond_18
    return-void
.end method

.method public onConnect(Ljavax/obex/HeaderSet;Ljavax/obex/HeaderSet;)I
    .registers 15
    .parameter "request"
    .parameter "reply"

    #@0
    .prologue
    const/16 v8, 0xd0

    #@2
    const/16 v11, 0x10

    #@4
    const/16 v7, 0xc6

    #@6
    .line 205
    invoke-static {p1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->logHeader(Ljavax/obex/HeaderSet;)V

    #@9
    .line 208
    const/16 v6, 0x46

    #@b
    :try_start_b
    invoke-virtual {p1, v6}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@e
    move-result-object v6

    #@f
    check-cast v6, [B

    #@11
    move-object v0, v6

    #@12
    check-cast v0, [B

    #@14
    move-object v5, v0

    #@15
    .line 209
    .local v5, uuid:[B
    if-nez v5, :cond_19

    #@17
    move v6, v7

    #@18
    .line 254
    .end local v5           #uuid:[B
    :goto_18
    return v6

    #@19
    .line 213
    .restart local v5       #uuid:[B
    :cond_19
    const-string v6, "BluetoothPbapObexServer"

    #@1b
    new-instance v9, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v10, "onConnect(): uuid="

    #@22
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v9

    #@26
    invoke-static {v5}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@29
    move-result-object v10

    #@2a
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v9

    #@2e
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v9

    #@32
    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 216
    array-length v6, v5

    #@36
    if-eq v6, v11, :cond_41

    #@38
    .line 217
    const-string v6, "BluetoothPbapObexServer"

    #@3a
    const-string v9, "Wrong UUID length"

    #@3c
    invoke-static {v6, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    move v6, v7

    #@40
    .line 218
    goto :goto_18

    #@41
    .line 220
    :cond_41
    const/4 v2, 0x0

    #@42
    .local v2, i:I
    :goto_42
    if-ge v2, v11, :cond_58

    #@44
    .line 221
    aget-byte v6, v5, v2

    #@46
    sget-object v9, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->PBAP_TARGET:[B

    #@48
    aget-byte v9, v9, v2

    #@4a
    if-eq v6, v9, :cond_55

    #@4c
    .line 222
    const-string v6, "BluetoothPbapObexServer"

    #@4e
    const-string v9, "Wrong UUID"

    #@50
    invoke-static {v6, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    move v6, v7

    #@54
    .line 223
    goto :goto_18

    #@55
    .line 220
    :cond_55
    add-int/lit8 v2, v2, 0x1

    #@57
    goto :goto_42

    #@58
    .line 226
    :cond_58
    const/16 v6, 0x4a

    #@5a
    invoke-virtual {p2, v6, v5}, Ljavax/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_5d
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_5d} :catch_a4

    #@5d
    .line 233
    const/16 v6, 0x4a

    #@5f
    :try_start_5f
    invoke-virtual {p1, v6}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@62
    move-result-object v6

    #@63
    check-cast v6, [B

    #@65
    move-object v0, v6

    #@66
    check-cast v0, [B

    #@68
    move-object v4, v0

    #@69
    .line 234
    .local v4, remote:[B
    if-eqz v4, :cond_8c

    #@6b
    .line 236
    const-string v6, "BluetoothPbapObexServer"

    #@6d
    new-instance v7, Ljava/lang/StringBuilder;

    #@6f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@72
    const-string v9, "onConnect(): remote="

    #@74
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v7

    #@78
    invoke-static {v4}, Ljava/util/Arrays;->toString([B)Ljava/lang/String;

    #@7b
    move-result-object v9

    #@7c
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v7

    #@80
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@83
    move-result-object v7

    #@84
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@87
    .line 238
    const/16 v6, 0x46

    #@89
    invoke-virtual {p2, v6, v4}, Ljavax/obex/HeaderSet;->setHeader(ILjava/lang/Object;)V
    :try_end_8c
    .catch Ljava/io/IOException; {:try_start_5f .. :try_end_8c} :catch_b1

    #@8c
    .line 246
    :cond_8c
    const-string v6, "BluetoothPbapObexServer"

    #@8e
    const-string v7, "onConnect(): uuid is ok, will send out MSG_SESSION_ESTABLISHED msg."

    #@90
    invoke-static {v6, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 250
    iget-object v6, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCallback:Landroid/os/Handler;

    #@95
    invoke-static {v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@98
    move-result-object v3

    #@99
    .line 251
    .local v3, msg:Landroid/os/Message;
    const/16 v6, 0x1389

    #@9b
    iput v6, v3, Landroid/os/Message;->what:I

    #@9d
    .line 252
    invoke-virtual {v3}, Landroid/os/Message;->sendToTarget()V

    #@a0
    .line 254
    const/16 v6, 0xa0

    #@a2
    goto/16 :goto_18

    #@a4
    .line 227
    .end local v2           #i:I
    .end local v3           #msg:Landroid/os/Message;
    .end local v4           #remote:[B
    .end local v5           #uuid:[B
    :catch_a4
    move-exception v1

    #@a5
    .line 228
    .local v1, e:Ljava/io/IOException;
    const-string v6, "BluetoothPbapObexServer"

    #@a7
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@aa
    move-result-object v7

    #@ab
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    move v6, v8

    #@af
    .line 229
    goto/16 :goto_18

    #@b1
    .line 240
    .end local v1           #e:Ljava/io/IOException;
    .restart local v2       #i:I
    .restart local v5       #uuid:[B
    :catch_b1
    move-exception v1

    #@b2
    .line 241
    .restart local v1       #e:Ljava/io/IOException;
    const-string v6, "BluetoothPbapObexServer"

    #@b4
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@b7
    move-result-object v7

    #@b8
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@bb
    move v6, v8

    #@bc
    .line 242
    goto/16 :goto_18
.end method

.method public onDisconnect(Ljavax/obex/HeaderSet;Ljavax/obex/HeaderSet;)V
    .registers 6
    .parameter "req"
    .parameter "resp"

    #@0
    .prologue
    .line 260
    const-string v1, "BluetoothPbapObexServer"

    #@2
    const-string v2, "onDisconnect(): enter"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 263
    invoke-static {p1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->logHeader(Ljavax/obex/HeaderSet;)V

    #@a
    .line 266
    const/16 v1, 0xa0

    #@c
    iput v1, p2, Ljavax/obex/HeaderSet;->responseCode:I

    #@e
    .line 267
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCallback:Landroid/os/Handler;

    #@10
    if-eqz v1, :cond_26

    #@12
    .line 268
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCallback:Landroid/os/Handler;

    #@14
    invoke-static {v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@17
    move-result-object v0

    #@18
    .line 269
    .local v0, msg:Landroid/os/Message;
    const/16 v1, 0x138a

    #@1a
    iput v1, v0, Landroid/os/Message;->what:I

    #@1c
    .line 270
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    #@1f
    .line 272
    const-string v1, "BluetoothPbapObexServer"

    #@21
    const-string v2, "onDisconnect(): msg MSG_SESSION_DISCONNECTED sent out."

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 275
    .end local v0           #msg:Landroid/os/Message;
    :cond_26
    return-void
.end method

.method public onGet(Ljavax/obex/Operation;)I
    .registers 19
    .parameter "op"

    #@0
    .prologue
    .line 360
    const/4 v2, 0x0

    #@1
    sput-boolean v2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->sIsAborted:Z

    #@3
    .line 361
    const/4 v14, 0x0

    #@4
    .line 362
    .local v14, request:Ljavax/obex/HeaderSet;
    new-instance v10, Ljavax/obex/HeaderSet;

    #@6
    invoke-direct {v10}, Ljavax/obex/HeaderSet;-><init>()V

    #@9
    .line 363
    .local v10, reply:Ljavax/obex/HeaderSet;
    const-string v15, ""

    #@b
    .line 364
    .local v15, type:Ljava/lang/String;
    const-string v6, ""

    #@d
    .line 365
    .local v6, name:Ljava/lang/String;
    const/4 v3, 0x0

    #@e
    .line 366
    .local v3, appParam:[B
    new-instance v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;

    #@10
    move-object/from16 v0, p0

    #@12
    invoke-direct {v4, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;-><init>(Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;)V

    #@15
    .line 368
    .local v4, appParamValue:Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;
    :try_start_15
    invoke-interface/range {p1 .. p1}, Ljavax/obex/Operation;->getReceivedHeader()Ljavax/obex/HeaderSet;

    #@18
    move-result-object v14

    #@19
    .line 369
    const/16 v2, 0x42

    #@1b
    invoke-virtual {v14, v2}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@1e
    move-result-object v2

    #@1f
    move-object v0, v2

    #@20
    check-cast v0, Ljava/lang/String;

    #@22
    move-object v15, v0

    #@23
    .line 370
    const/4 v2, 0x1

    #@24
    invoke-virtual {v14, v2}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@27
    move-result-object v2

    #@28
    move-object v0, v2

    #@29
    check-cast v0, Ljava/lang/String;

    #@2b
    move-object v6, v0

    #@2c
    .line 371
    const/16 v2, 0x4c

    #@2e
    invoke-virtual {v14, v2}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@31
    move-result-object v2

    #@32
    check-cast v2, [B

    #@34
    move-object v0, v2

    #@35
    check-cast v0, [B

    #@37
    move-object v3, v0
    :try_end_38
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_38} :catch_62

    #@38
    .line 378
    invoke-static {v14}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->logHeader(Ljavax/obex/HeaderSet;)V

    #@3b
    .line 381
    const-string v2, "BluetoothPbapObexServer"

    #@3d
    new-instance v5, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v7, "OnGet type is "

    #@44
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v5

    #@48
    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v5

    #@4c
    const-string v7, "; name is "

    #@4e
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v5

    #@56
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v5

    #@5a
    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    .line 384
    if-nez v15, :cond_6d

    #@5f
    .line 385
    const/16 v2, 0xc6

    #@61
    .line 509
    :goto_61
    return v2

    #@62
    .line 372
    :catch_62
    move-exception v13

    #@63
    .line 373
    .local v13, e:Ljava/io/IOException;
    const-string v2, "BluetoothPbapObexServer"

    #@65
    const-string v5, "request headers error"

    #@67
    invoke-static {v2, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6a
    .line 374
    const/16 v2, 0xd0

    #@6c
    goto :goto_61

    #@6d
    .line 395
    .end local v13           #e:Ljava/io/IOException;
    :cond_6d
    const/16 v16, 0x1

    #@6f
    .line 396
    .local v16, validName:Z
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@72
    move-result v2

    #@73
    if-eqz v2, :cond_77

    #@75
    .line 397
    const/16 v16, 0x0

    #@77
    .line 400
    :cond_77
    const-string v2, "BluetoothPbapObexServer"

    #@79
    new-instance v5, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v7, "[BTUI] onGet(): name = "

    #@80
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v5

    #@84
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v5

    #@88
    const-string v7, ", validName = "

    #@8a
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v5

    #@8e
    move/from16 v0, v16

    #@90
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@93
    move-result-object v5

    #@94
    const-string v7, ", type = "

    #@96
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v5

    #@9a
    invoke-virtual {v5, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v5

    #@9e
    const-string v7, ", mCurrentPath = "

    #@a0
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v5

    #@a4
    move-object/from16 v0, p0

    #@a6
    iget-object v7, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@a8
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v5

    #@ac
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v5

    #@b0
    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    .line 401
    if-eqz v16, :cond_bf

    #@b5
    if-eqz v16, :cond_188

    #@b7
    const-string v2, "x-bt/vcard"

    #@b9
    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@bc
    move-result v2

    #@bd
    if-eqz v2, :cond_188

    #@bf
    .line 403
    :cond_bf
    const-string v2, "BluetoothPbapObexServer"

    #@c1
    new-instance v5, Ljava/lang/StringBuilder;

    #@c3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@c6
    const-string v7, "Guess what carkit actually want from current path ("

    #@c8
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cb
    move-result-object v5

    #@cc
    move-object/from16 v0, p0

    #@ce
    iget-object v7, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@d0
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v5

    #@d4
    const-string v7, ")"

    #@d6
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v5

    #@da
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@dd
    move-result-object v5

    #@de
    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e1
    .line 407
    move-object/from16 v0, p0

    #@e3
    iget-object v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@e5
    const-string v5, "/telecom/pb"

    #@e7
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ea
    move-result v2

    #@eb
    if-eqz v2, :cond_118

    #@ed
    .line 408
    const/4 v2, 0x1

    #@ee
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@f0
    .line 429
    :goto_f0
    const-string v2, "BluetoothPbapObexServer"

    #@f2
    new-instance v5, Ljava/lang/StringBuilder;

    #@f4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f7
    const-string v7, "onGet(): appParamValue.needTag="

    #@f9
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v5

    #@fd
    iget v7, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@ff
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@102
    move-result-object v5

    #@103
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@106
    move-result-object v5

    #@107
    invoke-static {v2, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10a
    .line 488
    :goto_10a
    if-eqz v3, :cond_2a7

    #@10c
    move-object/from16 v0, p0

    #@10e
    invoke-direct {v0, v3, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->parseApplicationParameter([BLcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;)Z

    #@111
    move-result v2

    #@112
    if-nez v2, :cond_2a7

    #@114
    .line 489
    const/16 v2, 0xc0

    #@116
    goto/16 :goto_61

    #@118
    .line 409
    :cond_118
    move-object/from16 v0, p0

    #@11a
    iget-object v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@11c
    const-string v5, "/telecom/ich"

    #@11e
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@121
    move-result v2

    #@122
    if-eqz v2, :cond_128

    #@124
    .line 410
    const/4 v2, 0x2

    #@125
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@127
    goto :goto_f0

    #@128
    .line 411
    :cond_128
    move-object/from16 v0, p0

    #@12a
    iget-object v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@12c
    const-string v5, "/telecom/och"

    #@12e
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@131
    move-result v2

    #@132
    if-eqz v2, :cond_138

    #@134
    .line 412
    const/4 v2, 0x3

    #@135
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@137
    goto :goto_f0

    #@138
    .line 413
    :cond_138
    move-object/from16 v0, p0

    #@13a
    iget-object v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@13c
    const-string v5, "/telecom/mch"

    #@13e
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@141
    move-result v2

    #@142
    if-eqz v2, :cond_14d

    #@144
    .line 414
    const/4 v2, 0x4

    #@145
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@147
    .line 415
    const/4 v2, 0x1

    #@148
    move-object/from16 v0, p0

    #@14a
    iput-boolean v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedNewMissedCallsNum:Z

    #@14c
    goto :goto_f0

    #@14d
    .line 416
    :cond_14d
    move-object/from16 v0, p0

    #@14f
    iget-object v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@151
    const-string v5, "/telecom/cch"

    #@153
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@156
    move-result v2

    #@157
    if-eqz v2, :cond_15d

    #@159
    .line 417
    const/4 v2, 0x5

    #@15a
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@15c
    goto :goto_f0

    #@15d
    .line 419
    :cond_15d
    move-object/from16 v0, p0

    #@15f
    iget-object v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@161
    const-string v5, "/SIM1/telecom/pb"

    #@163
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@166
    move-result v2

    #@167
    if-eqz v2, :cond_17d

    #@169
    const-string v2, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@16b
    invoke-static {v2}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@16e
    move-result v2

    #@16f
    if-eqz v2, :cond_17d

    #@171
    .line 421
    const/4 v2, 0x6

    #@172
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@174
    .line 422
    const-string v2, "BluetoothPbapObexServer"

    #@176
    const-string v5, "[BTUI] SIM phonebook request (if)"

    #@178
    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17b
    goto/16 :goto_f0

    #@17d
    .line 425
    :cond_17d
    const-string v2, "BluetoothPbapObexServer"

    #@17f
    const-string v5, "mCurrentpath is not valid path!!!"

    #@181
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@184
    .line 426
    const/16 v2, 0xc6

    #@186
    goto/16 :goto_61

    #@188
    .line 433
    :cond_188
    const-string v2, "pb"

    #@18a
    const/4 v5, 0x0

    #@18b
    const-string v7, "pb"

    #@18d
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@190
    move-result v7

    #@191
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@194
    move-result-object v2

    #@195
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@198
    move-result v2

    #@199
    if-eqz v2, :cond_1b6

    #@19b
    const-string v2, "SIM1"

    #@19d
    const/4 v5, 0x0

    #@19e
    const-string v7, "SIM1"

    #@1a0
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@1a3
    move-result v7

    #@1a4
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@1a7
    move-result-object v2

    #@1a8
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@1ab
    move-result v2

    #@1ac
    if-eqz v2, :cond_1b6

    #@1ae
    const-string v2, "x-bt/phonebook"

    #@1b0
    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1b3
    move-result v2

    #@1b4
    if-nez v2, :cond_1dd

    #@1b6
    :cond_1b6
    const-string v2, "pb"

    #@1b8
    const/4 v5, 0x0

    #@1b9
    const-string v7, "pb"

    #@1bb
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@1be
    move-result v7

    #@1bf
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@1c2
    move-result-object v2

    #@1c3
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@1c6
    move-result v2

    #@1c7
    if-eqz v2, :cond_1fc

    #@1c9
    move-object/from16 v0, p0

    #@1cb
    iget-object v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@1cd
    const-string v5, "/SIM1/telecom"

    #@1cf
    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d2
    move-result v2

    #@1d3
    if-eqz v2, :cond_1fc

    #@1d5
    const-string v2, "x-bt/vcard-listing"

    #@1d7
    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1da
    move-result v2

    #@1db
    if-eqz v2, :cond_1fc

    #@1dd
    .line 435
    :cond_1dd
    const-string v2, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@1df
    invoke-static {v2}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@1e2
    move-result v2

    #@1e3
    if-eqz v2, :cond_1f1

    #@1e5
    .line 436
    const/4 v2, 0x6

    #@1e6
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@1e8
    .line 437
    const-string v2, "BluetoothPbapObexServer"

    #@1ea
    const-string v5, "[BTUI] SIM phonebook request (else)"

    #@1ec
    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ef
    goto/16 :goto_10a

    #@1f1
    .line 439
    :cond_1f1
    const-string v2, "BluetoothPbapObexServer"

    #@1f3
    const-string v5, "[BTUI] Does not support SIM"

    #@1f5
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@1f8
    .line 440
    const/16 v2, 0xc6

    #@1fa
    goto/16 :goto_61

    #@1fc
    .line 456
    :cond_1fc
    const-string v2, "pb"

    #@1fe
    const/4 v5, 0x0

    #@1ff
    const-string v7, "pb"

    #@201
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@204
    move-result v7

    #@205
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@208
    move-result-object v2

    #@209
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@20c
    move-result v2

    #@20d
    if-eqz v2, :cond_21b

    #@20f
    .line 457
    const/4 v2, 0x1

    #@210
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@212
    .line 459
    const-string v2, "BluetoothPbapObexServer"

    #@214
    const-string v5, "download phonebook request"

    #@216
    invoke-static {v2, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@219
    goto/16 :goto_10a

    #@21b
    .line 461
    :cond_21b
    const-string v2, "ich"

    #@21d
    const/4 v5, 0x0

    #@21e
    const-string v7, "ich"

    #@220
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@223
    move-result v7

    #@224
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@227
    move-result-object v2

    #@228
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@22b
    move-result v2

    #@22c
    if-eqz v2, :cond_23a

    #@22e
    .line 462
    const/4 v2, 0x2

    #@22f
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@231
    .line 464
    const-string v2, "BluetoothPbapObexServer"

    #@233
    const-string v5, "download incoming calls request"

    #@235
    invoke-static {v2, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@238
    goto/16 :goto_10a

    #@23a
    .line 466
    :cond_23a
    const-string v2, "och"

    #@23c
    const/4 v5, 0x0

    #@23d
    const-string v7, "och"

    #@23f
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@242
    move-result v7

    #@243
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@246
    move-result-object v2

    #@247
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@24a
    move-result v2

    #@24b
    if-eqz v2, :cond_259

    #@24d
    .line 467
    const/4 v2, 0x3

    #@24e
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@250
    .line 469
    const-string v2, "BluetoothPbapObexServer"

    #@252
    const-string v5, "download outgoing calls request"

    #@254
    invoke-static {v2, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@257
    goto/16 :goto_10a

    #@259
    .line 471
    :cond_259
    const-string v2, "mch"

    #@25b
    const/4 v5, 0x0

    #@25c
    const-string v7, "mch"

    #@25e
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@261
    move-result v7

    #@262
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@265
    move-result-object v2

    #@266
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@269
    move-result v2

    #@26a
    if-eqz v2, :cond_27d

    #@26c
    .line 472
    const/4 v2, 0x4

    #@26d
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@26f
    .line 473
    const/4 v2, 0x1

    #@270
    move-object/from16 v0, p0

    #@272
    iput-boolean v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mNeedNewMissedCallsNum:Z

    #@274
    .line 475
    const-string v2, "BluetoothPbapObexServer"

    #@276
    const-string v5, "download missed calls request"

    #@278
    invoke-static {v2, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@27b
    goto/16 :goto_10a

    #@27d
    .line 477
    :cond_27d
    const-string v2, "cch"

    #@27f
    const/4 v5, 0x0

    #@280
    const-string v7, "cch"

    #@282
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    #@285
    move-result v7

    #@286
    invoke-virtual {v2, v5, v7}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    #@289
    move-result-object v2

    #@28a
    invoke-virtual {v6, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@28d
    move-result v2

    #@28e
    if-eqz v2, :cond_29c

    #@290
    .line 478
    const/4 v2, 0x5

    #@291
    iput v2, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@293
    .line 480
    const-string v2, "BluetoothPbapObexServer"

    #@295
    const-string v5, "download combined calls request"

    #@297
    invoke-static {v2, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@29a
    goto/16 :goto_10a

    #@29c
    .line 483
    :cond_29c
    const-string v2, "BluetoothPbapObexServer"

    #@29e
    const-string v5, "Input name doesn\'t contain valid info!!!"

    #@2a0
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2a3
    .line 484
    const/16 v2, 0xc6

    #@2a5
    goto/16 :goto_61

    #@2a7
    .line 493
    :cond_2a7
    move-object/from16 v0, p0

    #@2a9
    iget-object v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mVcardManager:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@2ab
    iget v5, v4, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;->needTag:I

    #@2ad
    invoke-virtual {v2, v5}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->setSelection(I)V

    #@2b0
    .line 497
    const-string v2, "x-bt/vcard-listing"

    #@2b2
    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b5
    move-result v2

    #@2b6
    if-eqz v2, :cond_2c2

    #@2b8
    .line 498
    move-object/from16 v0, p0

    #@2ba
    move-object/from16 v1, p1

    #@2bc
    invoke-direct {v0, v3, v4, v10, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->pullVcardListing([BLcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;Ljavax/obex/HeaderSet;Ljavax/obex/Operation;)I

    #@2bf
    move-result v2

    #@2c0
    goto/16 :goto_61

    #@2c2
    .line 501
    :cond_2c2
    const-string v2, "x-bt/vcard"

    #@2c4
    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c7
    move-result v2

    #@2c8
    if-eqz v2, :cond_2d8

    #@2ca
    .line 502
    move-object/from16 v0, p0

    #@2cc
    iget-object v7, v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@2ce
    move-object/from16 v2, p0

    #@2d0
    move-object/from16 v5, p1

    #@2d2
    invoke-direct/range {v2 .. v7}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->pullVcardEntry([BLcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;Ljavax/obex/Operation;Ljava/lang/String;Ljava/lang/String;)I

    #@2d5
    move-result v2

    #@2d6
    goto/16 :goto_61

    #@2d8
    .line 505
    :cond_2d8
    const-string v2, "x-bt/phonebook"

    #@2da
    invoke-virtual {v15, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2dd
    move-result v2

    #@2de
    if-eqz v2, :cond_2ed

    #@2e0
    move-object/from16 v7, p0

    #@2e2
    move-object v8, v3

    #@2e3
    move-object v9, v4

    #@2e4
    move-object/from16 v11, p1

    #@2e6
    move-object v12, v6

    #@2e7
    .line 506
    invoke-direct/range {v7 .. v12}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->pullPhonebook([BLcom/android/bluetooth/pbap/BluetoothPbapObexServer$AppParamValue;Ljavax/obex/HeaderSet;Ljavax/obex/Operation;Ljava/lang/String;)I

    #@2ea
    move-result v2

    #@2eb
    goto/16 :goto_61

    #@2ed
    .line 508
    :cond_2ed
    const-string v2, "BluetoothPbapObexServer"

    #@2ef
    const-string v5, "unknown type request!!!"

    #@2f1
    invoke-static {v2, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2f4
    .line 509
    const/16 v2, 0xc6

    #@2f6
    goto/16 :goto_61
.end method

.method public onPut(Ljavax/obex/Operation;)I
    .registers 4
    .parameter "op"

    #@0
    .prologue
    .line 289
    const-string v0, "BluetoothPbapObexServer"

    #@2
    const-string v1, "onPut(): not support PUT request."

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 291
    const/16 v0, 0xc0

    #@9
    return v0
.end method

.method public onSetPath(Ljavax/obex/HeaderSet;Ljavax/obex/HeaderSet;ZZ)I
    .registers 12
    .parameter "request"
    .parameter "reply"
    .parameter "backup"
    .parameter "create"

    #@0
    .prologue
    .line 298
    invoke-static {p1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->logHeader(Ljavax/obex/HeaderSet;)V

    #@3
    .line 301
    const-string v4, "BluetoothPbapObexServer"

    #@5
    new-instance v5, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v6, "before setPath, mCurrentPath ==  "

    #@c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v5

    #@10
    iget-object v6, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@12
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v5

    #@16
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v5

    #@1a
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 304
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@1f
    .line 305
    .local v1, current_path_tmp:Ljava/lang/String;
    const/4 v3, 0x0

    #@20
    .line 307
    .local v3, tmp_path:Ljava/lang/String;
    const/4 v4, 0x1

    #@21
    :try_start_21
    invoke-virtual {p1, v4}, Ljavax/obex/HeaderSet;->getHeader(I)Ljava/lang/Object;

    #@24
    move-result-object v4

    #@25
    move-object v0, v4

    #@26
    check-cast v0, Ljava/lang/String;

    #@28
    move-object v3, v0
    :try_end_29
    .catch Ljava/io/IOException; {:try_start_21 .. :try_end_29} :catch_80

    #@29
    .line 313
    const-string v4, "BluetoothPbapObexServer"

    #@2b
    new-instance v5, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v6, "backup="

    #@32
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v5

    #@36
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    const-string v6, " create="

    #@3c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@43
    move-result-object v5

    #@44
    const-string v6, " name="

    #@46
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v5

    #@4a
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@55
    .line 316
    if-eqz p3, :cond_8b

    #@57
    .line 317
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@5a
    move-result v4

    #@5b
    if-eqz v4, :cond_68

    #@5d
    .line 318
    const/4 v4, 0x0

    #@5e
    const-string v5, "/"

    #@60
    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@63
    move-result v5

    #@64
    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@67
    move-result-object v1

    #@68
    .line 329
    :cond_68
    :goto_68
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    #@6b
    move-result v4

    #@6c
    if-eqz v4, :cond_b2

    #@6e
    invoke-direct {p0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->isLegalPath(Ljava/lang/String;)Z

    #@71
    move-result v4

    #@72
    if-nez v4, :cond_b2

    #@74
    .line 330
    if-eqz p4, :cond_a8

    #@76
    .line 331
    const-string v4, "BluetoothPbapObexServer"

    #@78
    const-string v5, "path create is forbidden!"

    #@7a
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    .line 332
    const/16 v4, 0xc3

    #@7f
    .line 343
    :goto_7f
    return v4

    #@80
    .line 308
    :catch_80
    move-exception v2

    #@81
    .line 309
    .local v2, e:Ljava/io/IOException;
    const-string v4, "BluetoothPbapObexServer"

    #@83
    const-string v5, "Get name header fail"

    #@85
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@88
    .line 310
    const/16 v4, 0xd0

    #@8a
    goto :goto_7f

    #@8b
    .line 322
    .end local v2           #e:Ljava/io/IOException;
    :cond_8b
    if-nez v3, :cond_90

    #@8d
    .line 323
    const-string v1, ""

    #@8f
    goto :goto_68

    #@90
    .line 325
    :cond_90
    new-instance v4, Ljava/lang/StringBuilder;

    #@92
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@95
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v4

    #@99
    const-string v5, "/"

    #@9b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v4

    #@9f
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v4

    #@a3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v1

    #@a7
    goto :goto_68

    #@a8
    .line 334
    :cond_a8
    const-string v4, "BluetoothPbapObexServer"

    #@aa
    const-string v5, "path is not legal"

    #@ac
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@af
    .line 335
    const/16 v4, 0xc4

    #@b1
    goto :goto_7f

    #@b2
    .line 338
    :cond_b2
    iput-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@b4
    .line 340
    const-string v4, "BluetoothPbapObexServer"

    #@b6
    new-instance v5, Ljava/lang/StringBuilder;

    #@b8
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@bb
    const-string v6, "after setPath, mCurrentPath ==  "

    #@bd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v5

    #@c1
    iget-object v6, p0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->mCurrentPath:Ljava/lang/String;

    #@c3
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v5

    #@c7
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v5

    #@cb
    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    .line 343
    const/16 v4, 0xa0

    #@d0
    goto :goto_7f
.end method
