.class public Lcom/android/bluetooth/pbap/BluetoothPbapActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "BluetoothPbapActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/text/TextWatcher;


# static fields
.field private static final BLUETOOTH_OBEX_AUTHKEY_MAX_LENGTH:I = 0x10

.field private static final DIALOG_YES_NO_AUTH:I = 0x1

.field private static final DISMISS_TIMEOUT_DIALOG:I = 0x0

.field private static final DISMISS_TIMEOUT_DIALOG_VALUE:I = 0x7d0

.field private static final KEY_USER_TIMEOUT:Ljava/lang/String; = "user_timeout"

.field private static final TAG:Ljava/lang/String; = "BluetoothPbapActivity"

.field private static final V:Z = true


# instance fields
.field private mAlwaysAllowed:Landroid/widget/CheckBox;

.field private mAlwaysAllowedValue:Z

.field private mCurrentDialog:I

.field private mKeyView:Landroid/widget/EditText;

.field private mOkButton:Landroid/widget/Button;

.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mSessionKey:Ljava/lang/String;

.field private mTimeout:Z

.field private final mTimeoutHandler:Landroid/os/Handler;

.field private mView:Landroid/view/View;

.field private messageView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    #@3
    .line 84
    const-string v0, ""

    #@5
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mSessionKey:Ljava/lang/String;

    #@7
    .line 92
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeout:Z

    #@a
    .line 94
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mAlwaysAllowedValue:Z

    #@d
    .line 100
    new-instance v0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity$1;

    #@f
    invoke-direct {v0, p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity$1;-><init>(Lcom/android/bluetooth/pbap/BluetoothPbapActivity;)V

    #@12
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    #@14
    .line 296
    new-instance v0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity$2;

    #@16
    invoke-direct {v0, p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity$2;-><init>(Lcom/android/bluetooth/pbap/BluetoothPbapActivity;)V

    #@19
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeoutHandler:Landroid/os/Handler;

    #@1b
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/pbap/BluetoothPbapActivity;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 66
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->onTimeout()V

    #@3
    return-void
.end method

.method private createDisplayText(I)Ljava/lang/String;
    .registers 7
    .parameter "id"

    #@0
    .prologue
    .line 150
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getRemoteDeviceName()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 151
    .local v1, mRemoteName:Ljava/lang/String;
    packed-switch p1, :pswitch_data_18

    #@7
    .line 156
    const/4 v0, 0x0

    #@8
    :goto_8
    return-object v0

    #@9
    .line 153
    :pswitch_9
    const v2, 0x7f07011f

    #@c
    const/4 v3, 0x1

    #@d
    new-array v3, v3, [Ljava/lang/Object;

    #@f
    const/4 v4, 0x0

    #@10
    aput-object v1, v3, v4

    #@12
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 154
    .local v0, mMessage2:Ljava/lang/String;
    goto :goto_8

    #@17
    .line 151
    nop

    #@18
    :pswitch_data_18
    .packed-switch 0x1
        :pswitch_9
    .end packed-switch
.end method

.method private createView(I)Landroid/view/View;
    .registers 7
    .parameter "id"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 161
    packed-switch p1, :pswitch_data_4e

    #@4
    .line 173
    :goto_4
    return-object v0

    #@5
    .line 163
    :pswitch_5
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@8
    move-result-object v1

    #@9
    const/high16 v2, 0x7f03

    #@b
    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@e
    move-result-object v0

    #@f
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mView:Landroid/view/View;

    #@11
    .line 164
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mView:Landroid/view/View;

    #@13
    const/high16 v1, 0x7f0b

    #@15
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/widget/TextView;

    #@1b
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->messageView:Landroid/widget/TextView;

    #@1d
    .line 165
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->messageView:Landroid/widget/TextView;

    #@1f
    invoke-direct {p0, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->createDisplayText(I)Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@26
    .line 166
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mView:Landroid/view/View;

    #@28
    const v1, 0x7f0b0001

    #@2b
    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@2e
    move-result-object v0

    #@2f
    check-cast v0, Landroid/widget/EditText;

    #@31
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mKeyView:Landroid/widget/EditText;

    #@33
    .line 167
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mKeyView:Landroid/widget/EditText;

    #@35
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    #@38
    .line 168
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mKeyView:Landroid/widget/EditText;

    #@3a
    const/4 v1, 0x1

    #@3b
    new-array v1, v1, [Landroid/text/InputFilter;

    #@3d
    const/4 v2, 0x0

    #@3e
    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    #@40
    const/16 v4, 0x10

    #@42
    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    #@45
    aput-object v3, v1, v2

    #@47
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    #@4a
    .line 171
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mView:Landroid/view/View;

    #@4c
    goto :goto_4

    #@4d
    .line 161
    nop

    #@4e
    :pswitch_data_4e
    .packed-switch 0x1
        :pswitch_5
    .end packed-switch
.end method

.method private onNegative()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 190
    iget v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mCurrentDialog:I

    #@3
    const/4 v1, 0x1

    #@4
    if-ne v0, v1, :cond_10

    #@6
    .line 191
    const-string v0, "com.android.bluetooth.pbap.authcancelled"

    #@8
    invoke-direct {p0, v0, v2, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->sendIntentToReceiver(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@b
    .line 192
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mKeyView:Landroid/widget/EditText;

    #@d
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    #@10
    .line 194
    :cond_10
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->finish()V

    #@13
    .line 195
    return-void
.end method

.method private onPositive()V
    .registers 4

    #@0
    .prologue
    .line 178
    iget-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeout:Z

    #@2
    if-nez v0, :cond_17

    #@4
    .line 179
    iget v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mCurrentDialog:I

    #@6
    const/4 v1, 0x1

    #@7
    if-ne v0, v1, :cond_17

    #@9
    .line 180
    const-string v0, "com.android.bluetooth.pbap.authresponse"

    #@b
    const-string v1, "com.android.bluetooth.pbap.sessionkey"

    #@d
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mSessionKey:Ljava/lang/String;

    #@f
    invoke-direct {p0, v0, v1, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->sendIntentToReceiver(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    #@12
    .line 182
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mKeyView:Landroid/widget/EditText;

    #@14
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    #@17
    .line 185
    :cond_17
    const/4 v0, 0x0

    #@18
    iput-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeout:Z

    #@1a
    .line 186
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->finish()V

    #@1d
    .line 187
    return-void
.end method

.method private onTimeout()V
    .registers 8

    #@0
    .prologue
    const/16 v6, 0x8

    #@2
    const/4 v5, 0x0

    #@3
    const/4 v4, 0x1

    #@4
    .line 237
    iput-boolean v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeout:Z

    #@6
    .line 238
    iget v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mCurrentDialog:I

    #@8
    if-ne v0, v4, :cond_3c

    #@a
    .line 239
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->messageView:Landroid/widget/TextView;

    #@c
    const v1, 0x7f070122

    #@f
    new-array v2, v4, [Ljava/lang/Object;

    #@11
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getRemoteDeviceName()Ljava/lang/String;

    #@14
    move-result-object v3

    #@15
    aput-object v3, v2, v5

    #@17
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@1e
    .line 241
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mKeyView:Landroid/widget/EditText;

    #@20
    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setVisibility(I)V

    #@23
    .line 242
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mKeyView:Landroid/widget/EditText;

    #@25
    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    #@28
    .line 243
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mKeyView:Landroid/widget/EditText;

    #@2a
    invoke-virtual {v0, p0}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    #@2d
    .line 244
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mOkButton:Landroid/widget/Button;

    #@2f
    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    #@32
    .line 245
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@34
    const/4 v1, -0x2

    #@35
    invoke-virtual {v0, v1}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@38
    move-result-object v0

    #@39
    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    #@3c
    .line 248
    :cond_3c
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeoutHandler:Landroid/os/Handler;

    #@3e
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeoutHandler:Landroid/os/Handler;

    #@40
    invoke-virtual {v1, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@43
    move-result-object v1

    #@44
    const-wide/16 v2, 0x7d0

    #@46
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@49
    .line 250
    return-void
.end method

.method private sendIntentToReceiver(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "intentName"
    .parameter "extraName"
    .parameter "extraValue"

    #@0
    .prologue
    .line 199
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5
    .line 200
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.bluetooth"

    #@7
    const-class v2, Lcom/android/bluetooth/pbap/BluetoothPbapReceiver;

    #@9
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@10
    .line 202
    if-eqz p2, :cond_15

    #@12
    .line 203
    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@15
    .line 205
    :cond_15
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->sendBroadcast(Landroid/content/Intent;)V

    #@18
    .line 206
    return-void
.end method

.method private sendIntentToReceiver(Ljava/lang/String;Ljava/lang/String;Z)V
    .registers 7
    .parameter "intentName"
    .parameter "extraName"
    .parameter "extraValue"

    #@0
    .prologue
    .line 210
    new-instance v0, Landroid/content/Intent;

    #@2
    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5
    .line 211
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.android.bluetooth"

    #@7
    const-class v2, Lcom/android/bluetooth/pbap/BluetoothPbapReceiver;

    #@9
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@10
    .line 213
    if-eqz p2, :cond_15

    #@12
    .line 214
    invoke-virtual {v0, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@15
    .line 216
    :cond_15
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->sendBroadcast(Landroid/content/Intent;)V

    #@18
    .line 217
    return-void
.end method

.method private showPbapDialog(I)V
    .registers 5
    .parameter "id"

    #@0
    .prologue
    .line 128
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    #@2
    .line 129
    .local v0, p:Lcom/android/internal/app/AlertController$AlertParams;
    packed-switch p1, :pswitch_data_3e

    #@5
    .line 147
    :goto_5
    return-void

    #@6
    .line 134
    :pswitch_6
    const v1, 0x7f070120

    #@9
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->getString(I)Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    #@f
    .line 135
    const/4 v1, 0x1

    #@10
    invoke-direct {p0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->createView(I)Landroid/view/View;

    #@13
    move-result-object v1

    #@14
    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    #@16
    .line 136
    const v1, 0x104000a

    #@19
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->getString(I)Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonText:Ljava/lang/CharSequence;

    #@1f
    .line 137
    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mPositiveButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@21
    .line 138
    const/high16 v1, 0x104

    #@23
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->getString(I)Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    iput-object v1, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@29
    .line 139
    iput-object p0, v0, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@2b
    .line 140
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->setupAlert()V

    #@2e
    .line 141
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mAlert:Lcom/android/internal/app/AlertController;

    #@30
    const/4 v2, -0x1

    #@31
    invoke-virtual {v1, v2}, Lcom/android/internal/app/AlertController;->getButton(I)Landroid/widget/Button;

    #@34
    move-result-object v1

    #@35
    iput-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mOkButton:Landroid/widget/Button;

    #@37
    .line 142
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mOkButton:Landroid/widget/Button;

    #@39
    const/4 v2, 0x0

    #@3a
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    #@3d
    goto :goto_5

    #@3e
    .line 129
    :pswitch_data_3e
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .registers 4
    .parameter "s"

    #@0
    .prologue
    .line 287
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_d

    #@6
    .line 288
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mOkButton:Landroid/widget/Button;

    #@8
    const/4 v1, 0x1

    #@9
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    #@c
    .line 294
    :goto_c
    return-void

    #@d
    .line 291
    :cond_d
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mOkButton:Landroid/widget/Button;

    #@f
    const/4 v1, 0x0

    #@10
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    #@13
    goto :goto_c
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "after"

    #@0
    .prologue
    .line 281
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 5
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 220
    packed-switch p2, :pswitch_data_1e

    #@3
    .line 234
    :goto_3
    return-void

    #@4
    .line 222
    :pswitch_4
    iget v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mCurrentDialog:I

    #@6
    const/4 v1, 0x1

    #@7
    if-ne v0, v1, :cond_15

    #@9
    .line 223
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mKeyView:Landroid/widget/EditText;

    #@b
    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    #@e
    move-result-object v0

    #@f
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mSessionKey:Ljava/lang/String;

    #@15
    .line 225
    :cond_15
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->onPositive()V

    #@18
    goto :goto_3

    #@19
    .line 229
    :pswitch_19
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->onNegative()V

    #@1c
    goto :goto_3

    #@1d
    .line 220
    nop

    #@1e
    :pswitch_data_1e
    .packed-switch -0x2
        :pswitch_19
        :pswitch_4
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 7
    .parameter "savedInstanceState"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 112
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    #@4
    .line 113
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->getIntent()Landroid/content/Intent;

    #@7
    move-result-object v1

    #@8
    .line 114
    .local v1, i:Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 115
    .local v0, action:Ljava/lang/String;
    const-string v2, "com.android.bluetooth.pbap.authchall"

    #@e
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_26

    #@14
    .line 116
    invoke-direct {p0, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->showPbapDialog(I)V

    #@17
    .line 117
    iput v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mCurrentDialog:I

    #@19
    .line 123
    :goto_19
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    #@1b
    new-instance v3, Landroid/content/IntentFilter;

    #@1d
    const-string v4, "com.android.bluetooth.pbap.userconfirmtimeout"

    #@1f
    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@22
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@25
    .line 125
    return-void

    #@26
    .line 119
    :cond_26
    const-string v2, "BluetoothPbapActivity"

    #@28
    const-string v3, "Error: this activity may be started only with intent PBAP_ACCESS_REQUEST or PBAP_AUTH_CHALL "

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 121
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->finish()V

    #@30
    goto :goto_19
.end method

.method protected onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 272
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onDestroy()V

    #@3
    .line 273
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    #@5
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@8
    .line 274
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .registers 4
    .parameter "preference"
    .parameter "newValue"

    #@0
    .prologue
    .line 277
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .registers 5
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 254
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 255
    const-string v0, "user_timeout"

    #@5
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@8
    move-result v0

    #@9
    iput-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeout:Z

    #@b
    .line 257
    const-string v0, "BluetoothPbapActivity"

    #@d
    new-instance v1, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v2, "onRestoreInstanceState() mTimeout: "

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    iget-boolean v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeout:Z

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v1

    #@1e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v1

    #@22
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 259
    iget-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeout:Z

    #@27
    if-eqz v0, :cond_2c

    #@29
    .line 260
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->onTimeout()V

    #@2c
    .line 262
    :cond_2c
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .registers 4
    .parameter "outState"

    #@0
    .prologue
    .line 266
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    #@3
    .line 267
    const-string v0, "user_timeout"

    #@5
    iget-boolean v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;->mTimeout:Z

    #@7
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    #@a
    .line 268
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .registers 5
    .parameter "s"
    .parameter "start"
    .parameter "before"
    .parameter "count"

    #@0
    .prologue
    .line 284
    return-void
.end method
