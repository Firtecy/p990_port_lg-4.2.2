.class public Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;
.super Ljava/lang/Object;
.source "BluetoothPbapAuthenticator.java"

# interfaces
.implements Ljavax/obex/Authenticator;


# static fields
.field private static final TAG:Ljava/lang/String; = "BluetoothPbapAuthenticator"


# instance fields
.field private mAuthCancelled:Z

.field private mCallback:Landroid/os/Handler;

.field private mChallenged:Z

.field private mSessionKey:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 58
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mCallback:Landroid/os/Handler;

    #@6
    .line 59
    iput-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mChallenged:Z

    #@8
    .line 60
    iput-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mAuthCancelled:Z

    #@a
    .line 61
    const/4 v0, 0x0

    #@b
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mSessionKey:Ljava/lang/String;

    #@d
    .line 62
    return-void
.end method

.method private waitUserConfirmation()V
    .registers 5

    #@0
    .prologue
    .line 77
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mCallback:Landroid/os/Handler;

    #@2
    invoke-static {v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;)Landroid/os/Message;

    #@5
    move-result-object v1

    #@6
    .line 78
    .local v1, msg:Landroid/os/Message;
    const/16 v2, 0x138b

    #@8
    iput v2, v1, Landroid/os/Message;->what:I

    #@a
    .line 79
    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    #@d
    .line 80
    monitor-enter p0

    #@e
    .line 81
    :goto_e
    :try_start_e
    iget-boolean v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mChallenged:Z

    #@10
    if-nez v2, :cond_26

    #@12
    iget-boolean v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mAuthCancelled:Z
    :try_end_14
    .catchall {:try_start_e .. :try_end_14} :catchall_23

    #@14
    if-nez v2, :cond_26

    #@16
    .line 83
    :try_start_16
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_19
    .catchall {:try_start_16 .. :try_end_19} :catchall_23
    .catch Ljava/lang/InterruptedException; {:try_start_16 .. :try_end_19} :catch_1a

    #@19
    goto :goto_e

    #@1a
    .line 84
    :catch_1a
    move-exception v0

    #@1b
    .line 85
    .local v0, e:Ljava/lang/InterruptedException;
    :try_start_1b
    const-string v2, "BluetoothPbapAuthenticator"

    #@1d
    const-string v3, "Interrupted while waiting on isChalled"

    #@1f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_e

    #@23
    .line 88
    .end local v0           #e:Ljava/lang/InterruptedException;
    :catchall_23
    move-exception v2

    #@24
    monitor-exit p0
    :try_end_25
    .catchall {:try_start_1b .. :try_end_25} :catchall_23

    #@25
    throw v2

    #@26
    :cond_26
    :try_start_26
    monitor-exit p0
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_23

    #@27
    .line 89
    return-void
.end method


# virtual methods
.method public onAuthenticationChallenge(Ljava/lang/String;ZZ)Ljavax/obex/PasswordAuthentication;
    .registers 7
    .parameter "description"
    .parameter "isUserIdRequired"
    .parameter "isFullAccess"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 93
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->waitUserConfirmation()V

    #@4
    .line 94
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mSessionKey:Ljava/lang/String;

    #@6
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@9
    move-result-object v2

    #@a
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_1c

    #@10
    .line 95
    new-instance v0, Ljavax/obex/PasswordAuthentication;

    #@12
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mSessionKey:Ljava/lang/String;

    #@14
    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    #@17
    move-result-object v2

    #@18
    invoke-direct {v0, v1, v2}, Ljavax/obex/PasswordAuthentication;-><init>([B[B)V

    #@1b
    .line 98
    :goto_1b
    return-object v0

    #@1c
    :cond_1c
    move-object v0, v1

    #@1d
    goto :goto_1b
.end method

.method public onAuthenticationResponse([B)[B
    .registers 3
    .parameter "userName"

    #@0
    .prologue
    .line 103
    const/4 v0, 0x0

    #@1
    .line 104
    .local v0, b:[B
    return-object v0
.end method

.method public final declared-synchronized setCancelled(Z)V
    .registers 3
    .parameter "bool"

    #@0
    .prologue
    .line 69
    monitor-enter p0

    #@1
    :try_start_1
    iput-boolean p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mAuthCancelled:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    .line 70
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 69
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public final declared-synchronized setChallenged(Z)V
    .registers 3
    .parameter "bool"

    #@0
    .prologue
    .line 65
    monitor-enter p0

    #@1
    :try_start_1
    iput-boolean p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mChallenged:Z
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    .line 66
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 65
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method

.method public final declared-synchronized setSessionKey(Ljava/lang/String;)V
    .registers 3
    .parameter "string"

    #@0
    .prologue
    .line 73
    monitor-enter p0

    #@1
    :try_start_1
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->mSessionKey:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_1 .. :try_end_3} :catchall_5

    #@3
    .line 74
    monitor-exit p0

    #@4
    return-void

    #@5
    .line 73
    :catchall_5
    move-exception v0

    #@6
    monitor-exit p0

    #@7
    throw v0
.end method
