.class public Lcom/android/bluetooth/pbap/BluetoothPbapService;
.super Landroid/app/Service;
.source "BluetoothPbapService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;
    }
.end annotation


# static fields
.field private static final ACCESS_AUTHORITY_CLASS:Ljava/lang/String; = "com.android.settings.bluetooth.BluetoothPermissionRequest"

.field private static final ACCESS_AUTHORITY_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field public static final AUTH_CANCELLED_ACTION:Ljava/lang/String; = "com.android.bluetooth.pbap.authcancelled"

.field public static final AUTH_CHALL_ACTION:Ljava/lang/String; = "com.android.bluetooth.pbap.authchall"

.field public static final AUTH_RESPONSE_ACTION:Ljava/lang/String; = "com.android.bluetooth.pbap.authresponse"

.field private static final AUTH_TIMEOUT:I = 0x3

.field private static final BLUETOOTH_ADMIN_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH_ADMIN"

.field private static final BLUETOOTH_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH"

.field public static final DEBUG:Z = true

.field public static final EXTRA_SESSION_KEY:Ljava/lang/String; = "com.android.bluetooth.pbap.sessionkey"

.field public static final MSG_OBEX_AUTH_CHALL:I = 0x138b

.field public static final MSG_SERVERSESSION_CLOSE:I = 0x1388

.field public static final MSG_SESSION_DISCONNECTED:I = 0x138a

.field public static final MSG_SESSION_ESTABLISHED:I = 0x1389

.field private static final NOTIFICATION_ID_ACCESS:I = -0xf4241

.field private static final NOTIFICATION_ID_AUTH:I = -0xf4242

.field private static final START_LISTENER:I = 0x1

.field private static final TAG:Ljava/lang/String; = "BluetoothPbapService"

.field public static final THIS_PACKAGE_NAME:Ljava/lang/String; = "com.android.bluetooth"

.field public static final USER_CONFIRM_TIMEOUT_ACTION:Ljava/lang/String; = "com.android.bluetooth.pbap.userconfirmtimeout"

.field private static final USER_CONFIRM_TIMEOUT_VALUE:I = 0x7530

.field private static final USER_TIMEOUT:I = 0x2

.field public static final VERBOSE:Z = true

.field private static mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

.field private static sLocalPhoneName:Ljava/lang/String;

.field private static sLocalPhoneNum:Ljava/lang/String;

.field private static sRemoteDeviceName:Ljava/lang/String;


# instance fields
.field private isWaitingAuthorization:Z

.field private mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

.field private final mBinder:Landroid/bluetooth/IBluetoothPbap$Stub;

.field private mConnSocket:Landroid/bluetooth/BluetoothSocket;

.field private mHasStarted:Z

.field private volatile mInterrupted:Z

.field private mPbapServer:Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;

.field private mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

.field private mServerSession:Ljavax/obex/ServerSession;

.field private mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

.field private final mSessionStatusHandler:Landroid/os/Handler;

.field private mStartId:I

.field private mState:I

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 165
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneNum:Ljava/lang/String;

    #@3
    .line 167
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneName:Ljava/lang/String;

    #@5
    .line 169
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sRemoteDeviceName:Ljava/lang/String;

    #@7
    return-void
.end method

.method public constructor <init>()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 192
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@5
    .line 147
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    #@7
    .line 151
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

    #@9
    .line 153
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@b
    .line 157
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;

    #@d
    .line 159
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    #@f
    .line 161
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mConnSocket:Landroid/bluetooth/BluetoothSocket;

    #@11
    .line 163
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@13
    .line 171
    iput-boolean v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mHasStarted:Z

    #@15
    .line 177
    const/4 v0, -0x1

    #@16
    iput v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mStartId:I

    #@18
    .line 181
    iput-boolean v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->isWaitingAuthorization:Z

    #@1a
    .line 668
    new-instance v0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;

    #@1c
    invoke-direct {v0, p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;-><init>(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V

    #@1f
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mSessionStatusHandler:Landroid/os/Handler;

    #@21
    .line 809
    new-instance v0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;

    #@23
    invoke-direct {v0, p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;-><init>(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V

    #@26
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mBinder:Landroid/bluetooth/IBluetoothPbap$Stub;

    #@28
    .line 193
    iput v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I

    #@2a
    .line 195
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->getInstance(Landroid/content/Context;)Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@2d
    move-result-object v0

    #@2e
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@30
    .line 197
    return-void
.end method

.method static synthetic access$100(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothServerSocket;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    #@2
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothAdapter;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    return-object v0
.end method

.method static synthetic access$1100(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->startRfcommSocketListener()V

    #@3
    return-void
.end method

.method static synthetic access$1200(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->stopObexServerSession()V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/bluetooth/pbap/BluetoothPbapService;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->removePbapNotification(I)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->notifyAuthCancelled()V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/bluetooth/pbap/BluetoothPbapService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->createPbapNotification(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/android/bluetooth/pbap/BluetoothPbapService;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I

    #@2
    return v0
.end method

.method static synthetic access$1700(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Ljavax/obex/ServerSession;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;

    #@2
    return-object v0
.end method

.method static synthetic access$1702(Lcom/android/bluetooth/pbap/BluetoothPbapService;Ljavax/obex/ServerSession;)Ljavax/obex/ServerSession;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;

    #@2
    return-object p1
.end method

.method static synthetic access$1800(Lcom/android/bluetooth/pbap/BluetoothPbapService;ZZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->closeSocket(ZZ)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/bluetooth/pbap/BluetoothPbapService;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->setState(II)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->initSocket()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$300(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->closeService()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothSocket;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mConnSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/android/bluetooth/pbap/BluetoothPbapService;Landroid/bluetooth/BluetoothSocket;)Landroid/bluetooth/BluetoothSocket;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mConnSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/bluetooth/pbap/BluetoothPbapService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$600()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 73
    sget-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sRemoteDeviceName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Ljava/lang/String;)Ljava/lang/String;
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 73
    sput-object p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sRemoteDeviceName:Ljava/lang/String;

    #@2
    return-object p0
.end method

.method static synthetic access$700(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V
    .registers 1
    .parameter "x0"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 73
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->startObexServerSession()V

    #@3
    return-void
.end method

.method static synthetic access$802(Lcom/android/bluetooth/pbap/BluetoothPbapService;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->isWaitingAuthorization:Z

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/os/Handler;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 73
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mSessionStatusHandler:Landroid/os/Handler;

    #@2
    return-object v0
.end method

.method private final closeService()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, -0x1

    #@2
    .line 439
    const-string v1, "BluetoothPbapService"

    #@4
    const-string v2, "Pbap Service closeService in"

    #@6
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 443
    const/4 v1, 0x1

    #@a
    const/4 v2, 0x1

    #@b
    :try_start_b
    invoke-direct {p0, v1, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->closeSocket(ZZ)V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_e} :catch_4a

    #@e
    .line 448
    :goto_e
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

    #@10
    if-eqz v1, :cond_1f

    #@12
    .line 450
    :try_start_12
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

    #@14
    invoke-virtual {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->shutdown()V

    #@17
    .line 451
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

    #@19
    invoke-virtual {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->join()V

    #@1c
    .line 452
    const/4 v1, 0x0

    #@1d
    iput-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;
    :try_end_1f
    .catch Ljava/lang/InterruptedException; {:try_start_12 .. :try_end_1f} :catch_64

    #@1f
    .line 457
    :cond_1f
    :goto_1f
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;

    #@21
    if-eqz v1, :cond_2a

    #@23
    .line 458
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;

    #@25
    invoke-virtual {v1}, Ljavax/obex/ServerSession;->close()V

    #@28
    .line 459
    iput-object v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;

    #@2a
    .line 462
    :cond_2a
    const/4 v1, 0x0

    #@2b
    iput-boolean v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mHasStarted:Z

    #@2d
    .line 463
    iget v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mStartId:I

    #@2f
    if-eq v1, v4, :cond_42

    #@31
    iget v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mStartId:I

    #@33
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->stopSelfResult(I)Z

    #@36
    move-result v1

    #@37
    if-eqz v1, :cond_42

    #@39
    .line 465
    const-string v1, "BluetoothPbapService"

    #@3b
    const-string v2, "successfully stopped pbap service"

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 467
    iput v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mStartId:I

    #@42
    .line 470
    :cond_42
    const-string v1, "BluetoothPbapService"

    #@44
    const-string v2, "Pbap Service closeService out"

    #@46
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    .line 472
    return-void

    #@4a
    .line 444
    :catch_4a
    move-exception v0

    #@4b
    .line 445
    .local v0, ex:Ljava/io/IOException;
    const-string v1, "BluetoothPbapService"

    #@4d
    new-instance v2, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v3, "CloseSocket error: "

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v2

    #@5c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    goto :goto_e

    #@64
    .line 453
    .end local v0           #ex:Ljava/io/IOException;
    :catch_64
    move-exception v0

    #@65
    .line 454
    .local v0, ex:Ljava/lang/InterruptedException;
    const-string v1, "BluetoothPbapService"

    #@67
    new-instance v2, Ljava/lang/StringBuilder;

    #@69
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6c
    const-string v3, "mAcceptThread close error"

    #@6e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@71
    move-result-object v2

    #@72
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v2

    #@76
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@79
    move-result-object v2

    #@7a
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@7d
    goto :goto_1f
.end method

.method private final closeSocket(ZZ)V
    .registers 6
    .parameter "server"
    .parameter "accept"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    const/4 v1, 0x1

    #@2
    .line 419
    if-ne p1, v1, :cond_11

    #@4
    .line 421
    iput-boolean v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mInterrupted:Z

    #@6
    .line 423
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    #@8
    if-eqz v0, :cond_11

    #@a
    .line 424
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    #@c
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothServerSocket;->close()V

    #@f
    .line 425
    iput-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;

    #@11
    .line 429
    :cond_11
    if-ne p2, v1, :cond_1e

    #@13
    .line 430
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mConnSocket:Landroid/bluetooth/BluetoothSocket;

    #@15
    if-eqz v0, :cond_1e

    #@17
    .line 431
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mConnSocket:Landroid/bluetooth/BluetoothSocket;

    #@19
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V

    #@1c
    .line 432
    iput-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mConnSocket:Landroid/bluetooth/BluetoothSocket;

    #@1e
    .line 435
    :cond_1e
    return-void
.end method

.method private createPbapNotification(Ljava/lang/String;)V
    .registers 13
    .parameter "action"

    #@0
    .prologue
    const/4 v10, 0x1

    #@1
    const/4 v9, 0x0

    #@2
    .line 745
    const-string v5, "notification"

    #@4
    invoke-virtual {p0, v5}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v3

    #@8
    check-cast v3, Landroid/app/NotificationManager;

    #@a
    .line 749
    .local v3, nm:Landroid/app/NotificationManager;
    new-instance v0, Landroid/content/Intent;

    #@c
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@f
    .line 750
    .local v0, clickIntent:Landroid/content/Intent;
    const-class v5, Lcom/android/bluetooth/pbap/BluetoothPbapActivity;

    #@11
    invoke-virtual {v0, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    #@14
    .line 751
    const/high16 v5, 0x1000

    #@16
    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@19
    .line 752
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@1c
    .line 756
    new-instance v1, Landroid/content/Intent;

    #@1e
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@21
    .line 757
    .local v1, deleteIntent:Landroid/content/Intent;
    const-class v5, Lcom/android/bluetooth/pbap/BluetoothPbapReceiver;

    #@23
    invoke-virtual {v1, p0, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    #@26
    .line 759
    const/4 v4, 0x0

    #@27
    .line 760
    .local v4, notification:Landroid/app/Notification;
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getRemoteDeviceName()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    .line 762
    .local v2, name:Ljava/lang/String;
    const-string v5, "com.android.bluetooth.pbap.authchall"

    #@2d
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v5

    #@31
    if-eqz v5, :cond_7e

    #@33
    .line 763
    const-string v5, "com.android.bluetooth.pbap.authcancelled"

    #@35
    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@38
    .line 764
    new-instance v4, Landroid/app/Notification;

    #@3a
    .end local v4           #notification:Landroid/app/Notification;
    const v5, 0x1080080

    #@3d
    const v6, 0x7f070123

    #@40
    invoke-virtual {p0, v6}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getString(I)Ljava/lang/String;

    #@43
    move-result-object v6

    #@44
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@47
    move-result-wide v7

    #@48
    invoke-direct {v4, v5, v6, v7, v8}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@4b
    .line 766
    .restart local v4       #notification:Landroid/app/Notification;
    const v5, 0x7f070124

    #@4e
    invoke-virtual {p0, v5}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getString(I)Ljava/lang/String;

    #@51
    move-result-object v5

    #@52
    const v6, 0x7f070125

    #@55
    new-array v7, v10, [Ljava/lang/Object;

    #@57
    aput-object v2, v7, v9

    #@59
    invoke-virtual {p0, v6, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@5c
    move-result-object v6

    #@5d
    invoke-static {p0, v9, v0, v9}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@60
    move-result-object v7

    #@61
    invoke-virtual {v4, p0, v5, v6, v7}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@64
    .line 770
    iget v5, v4, Landroid/app/Notification;->flags:I

    #@66
    or-int/lit8 v5, v5, 0x10

    #@68
    iput v5, v4, Landroid/app/Notification;->flags:I

    #@6a
    .line 771
    iget v5, v4, Landroid/app/Notification;->flags:I

    #@6c
    or-int/lit8 v5, v5, 0x8

    #@6e
    iput v5, v4, Landroid/app/Notification;->flags:I

    #@70
    .line 772
    iput v10, v4, Landroid/app/Notification;->defaults:I

    #@72
    .line 773
    invoke-static {p0, v9, v1, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    #@75
    move-result-object v5

    #@76
    iput-object v5, v4, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    #@78
    .line 774
    const v5, -0xf4242

    #@7b
    invoke-virtual {v3, v5, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@7e
    .line 776
    :cond_7e
    return-void
.end method

.method public static getLocalPhoneName()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 795
    sget-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@2
    if-eqz v0, :cond_e

    #@4
    .line 796
    sget-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@6
    sget-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneName:Ljava/lang/String;

    #@8
    invoke-static {v0}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->getLocalPhoneName(Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneName:Ljava/lang/String;

    #@e
    .line 799
    :cond_e
    sget-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneName:Ljava/lang/String;

    #@10
    return-object v0
.end method

.method public static getLocalPhoneNum()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 786
    sget-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneNum:Ljava/lang/String;

    #@2
    if-nez v0, :cond_8

    #@4
    .line 787
    const-string v0, ""

    #@6
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneNum:Ljava/lang/String;

    #@8
    .line 790
    :cond_8
    sget-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneNum:Ljava/lang/String;

    #@a
    return-object v0
.end method

.method public static getRemoteDeviceName()Ljava/lang/String;
    .registers 1

    #@0
    .prologue
    .line 803
    sget-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sRemoteDeviceName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method private final initSocket()Z
    .registers 9

    #@0
    .prologue
    .line 362
    const-string v5, "BluetoothPbapService"

    #@2
    const-string v6, "Pbap Service initSocket"

    #@4
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 365
    const/4 v3, 0x1

    #@8
    .line 366
    .local v3, initSocketOK:Z
    const/16 v0, 0xa

    #@a
    .line 369
    .local v0, CREATE_RETRY_TIME:I
    const/4 v2, 0x0

    #@b
    .local v2, i:I
    :goto_b
    const/16 v5, 0xa

    #@d
    if-ge v2, v5, :cond_29

    #@f
    iget-boolean v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mInterrupted:Z

    #@11
    if-nez v5, :cond_29

    #@13
    .line 373
    :try_start_13
    iget-object v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@15
    const-string v6, "OBEX Phonebook Access Server"

    #@17
    sget-object v7, Landroid/bluetooth/BluetoothUuid;->PBAP_PSE:Landroid/os/ParcelUuid;

    #@19
    invoke-virtual {v7}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@1c
    move-result-object v7

    #@1d
    invoke-virtual {v5, v6, v7}, Landroid/bluetooth/BluetoothAdapter;->listenUsingEncryptedRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;

    #@20
    move-result-object v5

    #@21
    iput-object v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSocket:Landroid/bluetooth/BluetoothServerSocket;
    :try_end_23
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_23} :catch_33

    #@23
    .line 380
    :goto_23
    if-nez v3, :cond_29

    #@25
    .line 382
    iget-object v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@27
    if-nez v5, :cond_52

    #@29
    .line 407
    :cond_29
    :goto_29
    if-eqz v3, :cond_88

    #@2b
    .line 409
    const-string v5, "BluetoothPbapService"

    #@2d
    const-string v6, "Succeed to create listening socket "

    #@2f
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 415
    :goto_32
    return v3

    #@33
    .line 376
    :catch_33
    move-exception v1

    #@34
    .line 377
    .local v1, e:Ljava/io/IOException;
    const-string v5, "BluetoothPbapService"

    #@36
    new-instance v6, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v7, "Error create RfcommServerSocket "

    #@3d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v6

    #@41
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@44
    move-result-object v7

    #@45
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v6

    #@49
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4c
    move-result-object v6

    #@4d
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    .line 378
    const/4 v3, 0x0

    #@51
    goto :goto_23

    #@52
    .line 385
    .end local v1           #e:Ljava/io/IOException;
    :cond_52
    iget-object v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@54
    invoke-virtual {v5}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@57
    move-result v4

    #@58
    .line 386
    .local v4, state:I
    const/16 v5, 0xb

    #@5a
    if-eq v4, v5, :cond_68

    #@5c
    const/16 v5, 0xc

    #@5e
    if-eq v4, v5, :cond_68

    #@60
    .line 388
    const-string v5, "BluetoothPbapService"

    #@62
    const-string v6, "initServerSocket failed as BT is (being) turned off"

    #@64
    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@67
    goto :goto_29

    #@68
    .line 391
    :cond_68
    monitor-enter p0

    #@69
    .line 394
    :try_start_69
    const-string v5, "BluetoothPbapService"

    #@6b
    const-string v6, "wait 300 ms"

    #@6d
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 396
    const-wide/16 v5, 0x12c

    #@72
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_75
    .catchall {:try_start_69 .. :try_end_75} :catchall_85
    .catch Ljava/lang/InterruptedException; {:try_start_69 .. :try_end_75} :catch_79

    #@75
    .line 401
    :goto_75
    :try_start_75
    monitor-exit p0

    #@76
    .line 369
    add-int/lit8 v2, v2, 0x1

    #@78
    goto :goto_b

    #@79
    .line 397
    :catch_79
    move-exception v1

    #@7a
    .line 398
    .local v1, e:Ljava/lang/InterruptedException;
    const-string v5, "BluetoothPbapService"

    #@7c
    const-string v6, "socketAcceptThread thread was interrupted (3)"

    #@7e
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@81
    .line 399
    const/4 v5, 0x1

    #@82
    iput-boolean v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mInterrupted:Z

    #@84
    goto :goto_75

    #@85
    .line 401
    .end local v1           #e:Ljava/lang/InterruptedException;
    :catchall_85
    move-exception v5

    #@86
    monitor-exit p0
    :try_end_87
    .catchall {:try_start_75 .. :try_end_87} :catchall_85

    #@87
    throw v5

    #@88
    .line 413
    .end local v4           #state:I
    :cond_88
    const-string v5, "BluetoothPbapService"

    #@8a
    const-string v6, "Error to create listening socket after 10 try"

    #@8c
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    goto :goto_32
.end method

.method private notifyAuthCancelled()V
    .registers 4

    #@0
    .prologue
    .line 561
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@2
    monitor-enter v1

    #@3
    .line 562
    :try_start_3
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@5
    const/4 v2, 0x1

    #@6
    invoke-virtual {v0, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->setCancelled(Z)V

    #@9
    .line 563
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@b
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@e
    .line 564
    monitor-exit v1

    #@f
    .line 565
    return-void

    #@10
    .line 564
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method private notifyAuthKeyInput(Ljava/lang/String;)V
    .registers 5
    .parameter "key"

    #@0
    .prologue
    .line 551
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@2
    monitor-enter v1

    #@3
    .line 552
    if-eqz p1, :cond_a

    #@5
    .line 553
    :try_start_5
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@7
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->setSessionKey(Ljava/lang/String;)V

    #@a
    .line 555
    :cond_a
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@c
    const/4 v2, 0x1

    #@d
    invoke-virtual {v0, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->setChallenged(Z)V

    #@10
    .line 556
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@12
    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    #@15
    .line 557
    monitor-exit v1

    #@16
    .line 558
    return-void

    #@17
    .line 557
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_5 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method private parseIntent(Landroid/content/Intent;)V
    .registers 15
    .parameter "intent"

    #@0
    .prologue
    const/4 v12, 0x1

    #@1
    const/4 v11, 0x0

    #@2
    const/4 v10, 0x2

    #@3
    .line 246
    const-string v7, "action"

    #@5
    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    .line 247
    .local v0, action:Ljava/lang/String;
    if-nez v0, :cond_c

    #@b
    .line 316
    :cond_b
    :goto_b
    return-void

    #@c
    .line 251
    :cond_c
    const-string v7, "BluetoothPbapService"

    #@e
    new-instance v8, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v9, "action: "

    #@15
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v8

    #@19
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v8

    #@1d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v8

    #@21
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 254
    const-string v7, "android.bluetooth.adapter.extra.STATE"

    #@26
    const/high16 v8, -0x8000

    #@28
    invoke-virtual {p1, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@2b
    move-result v5

    #@2c
    .line 256
    .local v5, state:I
    const-string v7, "BluetoothPbapService"

    #@2e
    new-instance v8, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v9, "state: "

    #@35
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v8

    #@39
    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v8

    #@3d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v8

    #@41
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 259
    const/4 v2, 0x1

    #@45
    .line 260
    .local v2, removeTimeoutMsg:Z
    const-string v7, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@47
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4a
    move-result v7

    #@4b
    if-eqz v7, :cond_79

    #@4d
    .line 261
    const/16 v7, 0xd

    #@4f
    if-ne v5, v7, :cond_77

    #@51
    .line 263
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mSessionStatusHandler:Landroid/os/Handler;

    #@53
    invoke-virtual {v7, v10}, Landroid/os/Handler;->hasMessages(I)Z

    #@56
    move-result v7

    #@57
    if-eqz v7, :cond_6c

    #@59
    .line 264
    new-instance v6, Landroid/content/Intent;

    #@5b
    const-string v7, "android.bluetooth.device.action.CONNECTION_ACCESS_CANCEL"

    #@5d
    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@60
    .line 266
    .local v6, timeoutIntent:Landroid/content/Intent;
    const-string v7, "com.android.settings"

    #@62
    const-string v8, "com.android.settings.bluetooth.BluetoothPermissionRequest"

    #@64
    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@67
    .line 267
    const-string v7, "android.permission.BLUETOOTH_ADMIN"

    #@69
    invoke-virtual {p0, v6, v7}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@6c
    .line 270
    .end local v6           #timeoutIntent:Landroid/content/Intent;
    :cond_6c
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->closeService()V

    #@6f
    .line 313
    :goto_6f
    if-eqz v2, :cond_b

    #@71
    .line 314
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mSessionStatusHandler:Landroid/os/Handler;

    #@73
    invoke-virtual {v7, v10}, Landroid/os/Handler;->removeMessages(I)V

    #@76
    goto :goto_b

    #@77
    .line 272
    :cond_77
    const/4 v2, 0x0

    #@78
    goto :goto_6f

    #@79
    .line 274
    :cond_79
    const-string v7, "android.bluetooth.device.action.CONNECTION_ACCESS_REPLY"

    #@7b
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7e
    move-result v7

    #@7f
    if-eqz v7, :cond_e3

    #@81
    .line 275
    iget-boolean v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->isWaitingAuthorization:Z

    #@83
    if-eqz v7, :cond_b

    #@85
    .line 280
    iput-boolean v11, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->isWaitingAuthorization:Z

    #@87
    .line 282
    const-string v7, "android.bluetooth.device.extra.CONNECTION_ACCESS_RESULT"

    #@89
    invoke-virtual {p1, v7, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@8c
    move-result v7

    #@8d
    if-ne v7, v12, :cond_df

    #@8f
    .line 286
    const-string v7, "android.bluetooth.device.extra.ALWAYS_ALLOWED"

    #@91
    invoke-virtual {p1, v7, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@94
    move-result v7

    #@95
    if-eqz v7, :cond_b5

    #@97
    .line 287
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@99
    invoke-virtual {v7, v12}, Landroid/bluetooth/BluetoothDevice;->setTrust(Z)Z

    #@9c
    move-result v3

    #@9d
    .line 289
    .local v3, result:Z
    const-string v7, "BluetoothPbapService"

    #@9f
    new-instance v8, Ljava/lang/StringBuilder;

    #@a1
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a4
    const-string v9, "setTrust() result="

    #@a6
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a9
    move-result-object v8

    #@aa
    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v8

    #@ae
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v8

    #@b2
    invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 293
    .end local v3           #result:Z
    :cond_b5
    :try_start_b5
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mConnSocket:Landroid/bluetooth/BluetoothSocket;

    #@b7
    if-eqz v7, :cond_db

    #@b9
    .line 294
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->startObexServerSession()V
    :try_end_bc
    .catch Ljava/io/IOException; {:try_start_b5 .. :try_end_bc} :catch_bd

    #@bc
    goto :goto_6f

    #@bd
    .line 298
    :catch_bd
    move-exception v1

    #@be
    .line 299
    .local v1, ex:Ljava/io/IOException;
    const-string v7, "BluetoothPbapService"

    #@c0
    new-instance v8, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    const-string v9, "Caught the error: "

    #@c7
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v8

    #@cb
    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@ce
    move-result-object v9

    #@cf
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d2
    move-result-object v8

    #@d3
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d6
    move-result-object v8

    #@d7
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@da
    goto :goto_6f

    #@db
    .line 296
    .end local v1           #ex:Ljava/io/IOException;
    :cond_db
    :try_start_db
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->stopObexServerSession()V
    :try_end_de
    .catch Ljava/io/IOException; {:try_start_db .. :try_end_de} :catch_bd

    #@de
    goto :goto_6f

    #@df
    .line 302
    :cond_df
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->stopObexServerSession()V

    #@e2
    goto :goto_6f

    #@e3
    .line 304
    :cond_e3
    const-string v7, "com.android.bluetooth.pbap.authresponse"

    #@e5
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e8
    move-result v7

    #@e9
    if-eqz v7, :cond_f6

    #@eb
    .line 305
    const-string v7, "com.android.bluetooth.pbap.sessionkey"

    #@ed
    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@f0
    move-result-object v4

    #@f1
    .line 306
    .local v4, sessionkey:Ljava/lang/String;
    invoke-direct {p0, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->notifyAuthKeyInput(Ljava/lang/String;)V

    #@f4
    goto/16 :goto_6f

    #@f6
    .line 307
    .end local v4           #sessionkey:Ljava/lang/String;
    :cond_f6
    const-string v7, "com.android.bluetooth.pbap.authcancelled"

    #@f8
    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fb
    move-result v7

    #@fc
    if-eqz v7, :cond_103

    #@fe
    .line 308
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->notifyAuthCancelled()V

    #@101
    goto/16 :goto_6f

    #@103
    .line 310
    :cond_103
    const/4 v2, 0x0

    #@104
    goto/16 :goto_6f
.end method

.method private removePbapNotification(I)V
    .registers 4
    .parameter "id"

    #@0
    .prologue
    .line 779
    const-string v1, "notification"

    #@2
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Landroid/app/NotificationManager;

    #@8
    .line 781
    .local v0, nm:Landroid/app/NotificationManager;
    invoke-virtual {v0, p1}, Landroid/app/NotificationManager;->cancel(I)V

    #@b
    .line 782
    return-void
.end method

.method private setState(I)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 719
    const/4 v0, 0x1

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->setState(II)V

    #@4
    .line 720
    return-void
.end method

.method private declared-synchronized setState(II)V
    .registers 9
    .parameter "state"
    .parameter "result"

    #@0
    .prologue
    .line 723
    monitor-enter p0

    #@1
    :try_start_1
    iget v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I

    #@3
    if-eq p1, v3, :cond_64

    #@5
    .line 725
    const-string v3, "BluetoothPbapService"

    #@7
    new-instance v4, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v5, "Pbap state "

    #@e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v4

    #@12
    iget v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I

    #@14
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    const-string v5, " -> "

    #@1a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v4

    #@22
    const-string v5, ", result = "

    #@24
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v4

    #@28
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v4

    #@2c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 728
    iget v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I

    #@35
    .line 729
    .local v1, prevState:I
    iput p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I

    #@37
    .line 730
    new-instance v0, Landroid/content/Intent;

    #@39
    const-string v3, "android.bluetooth.pbap.intent.action.PBAP_STATE_CHANGED"

    #@3b
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3e
    .line 731
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "android.bluetooth.pbap.intent.PBAP_PREVIOUS_STATE"

    #@40
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@43
    .line 732
    const-string v3, "android.bluetooth.pbap.intent.PBAP_STATE"

    #@45
    iget v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I

    #@47
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@4a
    .line 733
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    #@4c
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@4e
    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@51
    .line 734
    const-string v3, "android.permission.BLUETOOTH"

    #@53
    invoke-virtual {p0, v0, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@56
    .line 735
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@59
    move-result-object v2

    #@5a
    .line 736
    .local v2, s:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v2, :cond_64

    #@5c
    .line 737
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;

    #@5e
    const/4 v4, 0x6

    #@5f
    iget v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I

    #@61
    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/android/bluetooth/btservice/AdapterService;->onProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V
    :try_end_64
    .catchall {:try_start_1 .. :try_end_64} :catchall_66

    #@64
    .line 741
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #prevState:I
    .end local v2           #s:Lcom/android/bluetooth/btservice/AdapterService;
    :cond_64
    monitor-exit p0

    #@65
    return-void

    #@66
    .line 723
    :catchall_66
    move-exception v3

    #@67
    monitor-exit p0

    #@68
    throw v3
.end method

.method private final startObexServerSession()V
    .registers 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 476
    const-string v2, "BluetoothPbapService"

    #@2
    const-string v3, "Pbap Service startObexServerSession"

    #@4
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 491
    const-string v2, "phone"

    #@9
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@f
    .line 492
    .local v0, tm:Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_2e

    #@11
    .line 493
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    sput-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneNum:Ljava/lang/String;

    #@17
    .line 494
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1AlphaTag()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    sput-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneName:Ljava/lang/String;

    #@1d
    .line 495
    sget-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneName:Ljava/lang/String;

    #@1f
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@22
    move-result v2

    #@23
    if-eqz v2, :cond_2e

    #@25
    .line 496
    const v2, 0x7f070128

    #@28
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getString(I)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    sput-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sLocalPhoneName:Ljava/lang/String;

    #@2e
    .line 500
    :cond_2e
    new-instance v2, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;

    #@30
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mSessionStatusHandler:Landroid/os/Handler;

    #@32
    invoke-direct {v2, v3, p0}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    #@35
    iput-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mPbapServer:Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;

    #@37
    .line 501
    monitor-enter p0

    #@38
    .line 502
    :try_start_38
    new-instance v2, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@3a
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mSessionStatusHandler:Landroid/os/Handler;

    #@3c
    invoke-direct {v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;-><init>(Landroid/os/Handler;)V

    #@3f
    iput-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@41
    .line 503
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@43
    const/4 v3, 0x0

    #@44
    invoke-virtual {v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->setChallenged(Z)V

    #@47
    .line 504
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@49
    const/4 v3, 0x0

    #@4a
    invoke-virtual {v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;->setCancelled(Z)V

    #@4d
    .line 505
    monitor-exit p0
    :try_end_4e
    .catchall {:try_start_38 .. :try_end_4e} :catchall_6c

    #@4e
    .line 506
    new-instance v1, Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;

    #@50
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mConnSocket:Landroid/bluetooth/BluetoothSocket;

    #@52
    invoke-direct {v1, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;-><init>(Landroid/bluetooth/BluetoothSocket;)V

    #@55
    .line 507
    .local v1, transport:Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;
    new-instance v2, Ljavax/obex/ServerSession;

    #@57
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mPbapServer:Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;

    #@59
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAuth:Lcom/android/bluetooth/pbap/BluetoothPbapAuthenticator;

    #@5b
    invoke-direct {v2, v1, v3, v4}, Ljavax/obex/ServerSession;-><init>(Ljavax/obex/ObexTransport;Ljavax/obex/ServerRequestHandler;Ljavax/obex/Authenticator;)V

    #@5e
    iput-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;

    #@60
    .line 508
    const/4 v2, 0x2

    #@61
    invoke-direct {p0, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->setState(I)V

    #@64
    .line 510
    const-string v2, "BluetoothPbapService"

    #@66
    const-string v3, "startObexServerSession() success!"

    #@68
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6b
    .line 512
    return-void

    #@6c
    .line 505
    .end local v1           #transport:Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;
    :catchall_6c
    move-exception v2

    #@6d
    :try_start_6d
    monitor-exit p0
    :try_end_6e
    .catchall {:try_start_6d .. :try_end_6e} :catchall_6c

    #@6e
    throw v2
.end method

.method private startRfcommSocketListener()V
    .registers 3

    #@0
    .prologue
    .line 350
    const-string v0, "BluetoothPbapService"

    #@2
    const-string v1, "Pbap Service startRfcommSocketListener"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 353
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

    #@9
    if-nez v0, :cond_1f

    #@b
    .line 354
    new-instance v0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

    #@d
    const/4 v1, 0x0

    #@e
    invoke-direct {v0, p0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;-><init>(Lcom/android/bluetooth/pbap/BluetoothPbapService;Lcom/android/bluetooth/pbap/BluetoothPbapService$1;)V

    #@11
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

    #@13
    .line 355
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

    #@15
    const-string v1, "BluetoothPbapAcceptThread"

    #@17
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->setName(Ljava/lang/String;)V

    #@1a
    .line 356
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

    #@1c
    invoke-virtual {v0}, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->start()V

    #@1f
    .line 358
    :cond_1f
    return-void
.end method

.method private stopObexServerSession()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 516
    const-string v1, "BluetoothPbapService"

    #@4
    const-string v2, "Pbap Service stopObexServerSession"

    #@6
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 529
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;

    #@b
    if-eqz v1, :cond_14

    #@d
    .line 530
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;

    #@f
    invoke-virtual {v1}, Ljavax/obex/ServerSession;->close()V

    #@12
    .line 531
    iput-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;

    #@14
    .line 534
    :cond_14
    iput-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAcceptThread:Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;

    #@16
    .line 537
    const/4 v1, 0x0

    #@17
    const/4 v2, 0x1

    #@18
    :try_start_18
    invoke-direct {p0, v1, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->closeSocket(ZZ)V

    #@1b
    .line 538
    const/4 v1, 0x0

    #@1c
    iput-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mConnSocket:Landroid/bluetooth/BluetoothSocket;
    :try_end_1e
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_1e} :catch_2d

    #@1e
    .line 544
    :goto_1e
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@20
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_29

    #@26
    .line 545
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->startRfcommSocketListener()V

    #@29
    .line 547
    :cond_29
    invoke-direct {p0, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->setState(I)V

    #@2c
    .line 548
    return-void

    #@2d
    .line 539
    :catch_2d
    move-exception v0

    #@2e
    .line 540
    .local v0, e:Ljava/io/IOException;
    const-string v1, "BluetoothPbapService"

    #@30
    new-instance v2, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v3, "closeSocket error: "

    #@37
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v2

    #@3b
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v2

    #@43
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v2

    #@47
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    goto :goto_1e
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 343
    const-string v0, "BluetoothPbapService"

    #@2
    const-string v1, "Pbap Service onBind"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 345
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mBinder:Landroid/bluetooth/IBluetoothPbap$Stub;

    #@9
    return-object v0
.end method

.method public onCreate()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 201
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@4
    .line 203
    const-string v1, "BluetoothPbapService"

    #@6
    const-string v2, "Pbap Service onCreate"

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 206
    const/4 v1, 0x0

    #@c
    iput-boolean v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mInterrupted:Z

    #@e
    .line 207
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@11
    move-result-object v1

    #@12
    iput-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@14
    .line 209
    iget-boolean v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mHasStarted:Z

    #@16
    if-nez v1, :cond_39

    #@18
    .line 210
    iput-boolean v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mHasStarted:Z

    #@1a
    .line 212
    const-string v1, "BluetoothPbapService"

    #@1c
    const-string v2, "Starting PBAP service"

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 214
    invoke-static {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->init(Landroid/content/Context;)V

    #@24
    .line 215
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@26
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    #@29
    move-result v0

    #@2a
    .line 216
    .local v0, state:I
    const/16 v1, 0xc

    #@2c
    if-ne v0, v1, :cond_39

    #@2e
    .line 217
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mSessionStatusHandler:Landroid/os/Handler;

    #@30
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mSessionStatusHandler:Landroid/os/Handler;

    #@32
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@39
    .line 221
    .end local v0           #state:I
    :cond_39
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 321
    const-string v0, "BluetoothPbapService"

    #@2
    const-string v1, "Pbap Service onDestroy"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 324
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@a
    .line 325
    const/4 v0, 0x0

    #@b
    const/4 v1, 0x2

    #@c
    invoke-direct {p0, v0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->setState(II)V

    #@f
    .line 334
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->closeService()V

    #@12
    .line 335
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mSessionStatusHandler:Landroid/os/Handler;

    #@14
    if-eqz v0, :cond_1c

    #@16
    .line 336
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mSessionStatusHandler:Landroid/os/Handler;

    #@18
    const/4 v1, 0x0

    #@19
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@1c
    .line 338
    :cond_1c
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 6
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    .line 227
    iput p3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mStartId:I

    #@2
    .line 228
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@4
    if-nez v0, :cond_12

    #@6
    .line 229
    const-string v0, "BluetoothPbapService"

    #@8
    const-string v1, "Stopping BluetoothPbapService: device does not have BT or device is not ready"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 232
    invoke-direct {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->closeService()V

    #@10
    .line 241
    :cond_10
    :goto_10
    const/4 v0, 0x2

    #@11
    return v0

    #@12
    .line 236
    :cond_12
    if-eqz p1, :cond_10

    #@14
    .line 237
    invoke-direct {p0, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->parseIntent(Landroid/content/Intent;)V

    #@17
    goto :goto_10
.end method
