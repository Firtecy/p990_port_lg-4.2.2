.class Lcom/android/bluetooth/pbap/BluetoothPbapService$2;
.super Landroid/bluetooth/IBluetoothPbap$Stub;
.source "BluetoothPbapService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/pbap/BluetoothPbapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 809
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@2
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothPbap$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 852
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v0

    #@5
    if-nez v0, :cond_f

    #@7
    .line 853
    const-string v0, "BluetoothPbapService"

    #@9
    const-string v1, "connect(): not allowed for non-active user"

    #@b
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 859
    :goto_e
    return v3

    #@f
    .line 857
    :cond_f
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@11
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@13
    const-string v2, "Need BLUETOOTH_ADMIN permission"

    #@15
    invoke-virtual {v0, v1, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    goto :goto_e
.end method

.method public disconnect()V
    .registers 6

    #@0
    .prologue
    .line 864
    const-string v1, "BluetoothPbapService"

    #@2
    const-string v2, "disconnect"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 867
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@a
    move-result v1

    #@b
    if-nez v1, :cond_15

    #@d
    .line 868
    const-string v1, "BluetoothPbapService"

    #@f
    const-string v2, "disconnect(): not allowed for non-active user"

    #@11
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 893
    :goto_14
    return-void

    #@15
    .line 872
    :cond_15
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@17
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@19
    const-string v3, "Need BLUETOOTH_ADMIN permission"

    #@1b
    invoke-virtual {v1, v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@1e
    .line 874
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@20
    monitor-enter v2

    #@21
    .line 875
    :try_start_21
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@23
    #getter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I
    invoke-static {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1600(Lcom/android/bluetooth/pbap/BluetoothPbapService;)I

    #@26
    move-result v1

    #@27
    packed-switch v1, :pswitch_data_76

    #@2a
    .line 892
    :goto_2a
    monitor-exit v2

    #@2b
    goto :goto_14

    #@2c
    :catchall_2c
    move-exception v1

    #@2d
    monitor-exit v2
    :try_end_2e
    .catchall {:try_start_21 .. :try_end_2e} :catchall_2c

    #@2e
    throw v1

    #@2f
    .line 877
    :pswitch_2f
    :try_start_2f
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@31
    #getter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;
    invoke-static {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1700(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Ljavax/obex/ServerSession;

    #@34
    move-result-object v1

    #@35
    if-eqz v1, :cond_46

    #@37
    .line 878
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@39
    #getter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;
    invoke-static {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1700(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Ljavax/obex/ServerSession;

    #@3c
    move-result-object v1

    #@3d
    invoke-virtual {v1}, Ljavax/obex/ServerSession;->close()V

    #@40
    .line 879
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@42
    const/4 v3, 0x0

    #@43
    #setter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mServerSession:Ljavax/obex/ServerSession;
    invoke-static {v1, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1702(Lcom/android/bluetooth/pbap/BluetoothPbapService;Ljavax/obex/ServerSession;)Ljavax/obex/ServerSession;
    :try_end_46
    .catchall {:try_start_2f .. :try_end_46} :catchall_2c

    #@46
    .line 882
    :cond_46
    :try_start_46
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@48
    const/4 v3, 0x0

    #@49
    const/4 v4, 0x1

    #@4a
    #calls: Lcom/android/bluetooth/pbap/BluetoothPbapService;->closeSocket(ZZ)V
    invoke-static {v1, v3, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1800(Lcom/android/bluetooth/pbap/BluetoothPbapService;ZZ)V

    #@4d
    .line 883
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@4f
    const/4 v3, 0x0

    #@50
    #setter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mConnSocket:Landroid/bluetooth/BluetoothSocket;
    invoke-static {v1, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$402(Lcom/android/bluetooth/pbap/BluetoothPbapService;Landroid/bluetooth/BluetoothSocket;)Landroid/bluetooth/BluetoothSocket;
    :try_end_53
    .catchall {:try_start_46 .. :try_end_53} :catchall_2c
    .catch Ljava/io/IOException; {:try_start_46 .. :try_end_53} :catch_5b

    #@53
    .line 887
    :goto_53
    :try_start_53
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@55
    const/4 v3, 0x0

    #@56
    const/4 v4, 0x2

    #@57
    #calls: Lcom/android/bluetooth/pbap/BluetoothPbapService;->setState(II)V
    invoke-static {v1, v3, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1900(Lcom/android/bluetooth/pbap/BluetoothPbapService;II)V

    #@5a
    goto :goto_2a

    #@5b
    .line 884
    :catch_5b
    move-exception v0

    #@5c
    .line 885
    .local v0, ex:Ljava/io/IOException;
    const-string v1, "BluetoothPbapService"

    #@5e
    new-instance v3, Ljava/lang/StringBuilder;

    #@60
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@63
    const-string v4, "Caught the error: "

    #@65
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v3

    #@6d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v3

    #@71
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_74
    .catchall {:try_start_53 .. :try_end_74} :catchall_2c

    #@74
    goto :goto_53

    #@75
    .line 875
    nop

    #@76
    :pswitch_data_76
    .packed-switch 0x2
        :pswitch_2f
    .end packed-switch
.end method

.method public getClient()Landroid/bluetooth/BluetoothDevice;
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 826
    const-string v1, "BluetoothPbapService"

    #@3
    new-instance v2, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v3, "getClient"

    #@a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v2

    #@e
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@10
    #getter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$500(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothDevice;

    #@13
    move-result-object v3

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 829
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@22
    move-result v1

    #@23
    if-nez v1, :cond_2d

    #@25
    .line 830
    const-string v1, "BluetoothPbapService"

    #@27
    const-string v2, "getClient(): not allowed for non-active user"

    #@29
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 838
    :cond_2c
    :goto_2c
    return-object v0

    #@2d
    .line 834
    :cond_2d
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@2f
    const-string v2, "android.permission.BLUETOOTH"

    #@31
    const-string v3, "Need BLUETOOTH permission"

    #@33
    invoke-virtual {v1, v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    .line 835
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@38
    #getter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I
    invoke-static {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1600(Lcom/android/bluetooth/pbap/BluetoothPbapService;)I

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_2c

    #@3e
    .line 838
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@40
    #getter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$500(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothDevice;

    #@43
    move-result-object v0

    #@44
    goto :goto_2c
.end method

.method public getState()I
    .registers 4

    #@0
    .prologue
    .line 812
    const-string v0, "BluetoothPbapService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "getState "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@f
    #getter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I
    invoke-static {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1600(Lcom/android/bluetooth/pbap/BluetoothPbapService;)I

    #@12
    move-result v2

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 815
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@21
    move-result v0

    #@22
    if-nez v0, :cond_2d

    #@24
    .line 816
    const-string v0, "BluetoothPbapService"

    #@26
    const-string v1, "getState(): not allowed for non-active user"

    #@28
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    .line 817
    const/4 v0, 0x0

    #@2c
    .line 821
    :goto_2c
    return v0

    #@2d
    .line 820
    :cond_2d
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@2f
    const-string v1, "android.permission.BLUETOOTH"

    #@31
    const-string v2, "Need BLUETOOTH permission"

    #@33
    invoke-virtual {v0, v1, v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@36
    .line 821
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@38
    #getter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I
    invoke-static {v0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1600(Lcom/android/bluetooth/pbap/BluetoothPbapService;)I

    #@3b
    move-result v0

    #@3c
    goto :goto_2c
.end method

.method public isConnected(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 842
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_f

    #@7
    .line 843
    const-string v1, "BluetoothPbapService"

    #@9
    const-string v2, "isConnected(): not allowed for non-active user"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 848
    :cond_e
    :goto_e
    return v0

    #@f
    .line 847
    :cond_f
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@11
    const-string v2, "android.permission.BLUETOOTH"

    #@13
    const-string v3, "Need BLUETOOTH permission"

    #@15
    invoke-virtual {v1, v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@18
    .line 848
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@1a
    #getter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mState:I
    invoke-static {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1600(Lcom/android/bluetooth/pbap/BluetoothPbapService;)I

    #@1d
    move-result v1

    #@1e
    const/4 v2, 0x2

    #@1f
    if-ne v1, v2, :cond_e

    #@21
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$2;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@23
    #getter for: Lcom/android/bluetooth/pbap/BluetoothPbapService;->mRemoteDevice:Landroid/bluetooth/BluetoothDevice;
    invoke-static {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$500(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothDevice;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v1

    #@2b
    if-eqz v1, :cond_e

    #@2d
    const/4 v0, 0x1

    #@2e
    goto :goto_e
.end method
