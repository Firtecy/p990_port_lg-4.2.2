.class public Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;
.super Ljava/lang/Object;
.source "BluetoothPbapRfcommTransport.java"

# interfaces
.implements Ljavax/obex/ObexTransport;


# instance fields
.field private mSocket:Landroid/bluetooth/BluetoothSocket;


# direct methods
.method public constructor <init>(Landroid/bluetooth/BluetoothSocket;)V
    .registers 3
    .parameter "rfs"

    #@0
    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 46
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@6
    .line 50
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@8
    .line 51
    return-void
.end method


# virtual methods
.method public close()V
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V

    #@5
    .line 55
    return-void
.end method

.method public connect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 74
    return-void
.end method

.method public create()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 77
    return-void
.end method

.method public disconnect()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 80
    return-void
.end method

.method public isConnected()Z
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 86
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method public listen()V
    .registers 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 83
    return-void
.end method

.method public openDataInputStream()Ljava/io/DataInputStream;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 58
    new-instance v0, Ljava/io/DataInputStream;

    #@2
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;->openInputStream()Ljava/io/InputStream;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    #@9
    return-object v0
.end method

.method public openDataOutputStream()Ljava/io/DataOutputStream;
    .registers 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 62
    new-instance v0, Ljava/io/DataOutputStream;

    #@2
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;->openOutputStream()Ljava/io/OutputStream;

    #@5
    move-result-object v1

    #@6
    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    #@9
    return-object v0
.end method

.method public openInputStream()Ljava/io/InputStream;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method

.method public openOutputStream()Ljava/io/OutputStream;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapRfcommTransport;->mSocket:Landroid/bluetooth/BluetoothSocket;

    #@2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    #@5
    move-result-object v0

    #@6
    return-object v0
.end method
