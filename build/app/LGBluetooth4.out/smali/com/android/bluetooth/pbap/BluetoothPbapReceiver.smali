.class public Lcom/android/bluetooth/pbap/BluetoothPbapReceiver;
.super Landroid/content/BroadcastReceiver;
.source "BluetoothPbapReceiver.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "BluetoothPbapReceiver"

.field private static final V:Z = true


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 41
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 50
    const-string v5, "BluetoothPbapReceiver"

    #@2
    const-string v6, "PbapReceiver onReceive "

    #@4
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 53
    new-instance v2, Landroid/content/Intent;

    #@9
    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    #@c
    .line 54
    .local v2, in:Landroid/content/Intent;
    invoke-virtual {v2, p2}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    #@f
    .line 55
    const-class v5, Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@11
    invoke-virtual {v2, p1, v5}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    #@14
    .line 56
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    .line 57
    .local v0, action:Ljava/lang/String;
    const-string v5, "action"

    #@1a
    invoke-virtual {v2, v5, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1d
    .line 59
    const-string v5, "BluetoothPbapReceiver"

    #@1f
    new-instance v6, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v7, "***********action = "

    #@26
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v6

    #@32
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 62
    const/4 v3, 0x1

    #@36
    .line 63
    .local v3, startService:Z
    const-string v5, "android.bluetooth.adapter.action.STATE_CHANGED"

    #@38
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3b
    move-result v5

    #@3c
    if-eqz v5, :cond_b2

    #@3e
    .line 64
    const-string v5, "android.bluetooth.adapter.extra.STATE"

    #@40
    const/high16 v6, -0x8000

    #@42
    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@45
    move-result v4

    #@46
    .line 65
    .local v4, state:I
    const-string v5, "android.bluetooth.adapter.extra.STATE"

    #@48
    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@4b
    .line 67
    const-string v5, "BluetoothPbapReceiver"

    #@4d
    new-instance v6, Ljava/lang/StringBuilder;

    #@4f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@52
    const-string v7, "***********state = "

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v6

    #@60
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 69
    const/16 v5, 0xb

    #@65
    if-eq v4, v5, :cond_6b

    #@67
    const/16 v5, 0xa

    #@69
    if-ne v4, v5, :cond_6c

    #@6b
    .line 73
    :cond_6b
    const/4 v3, 0x0

    #@6c
    .line 84
    .end local v4           #state:I
    :cond_6c
    :goto_6c
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@6f
    move-result-object v5

    #@70
    if-eqz v5, :cond_90

    #@72
    .line 85
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@75
    move-result-object v5

    #@76
    const/4 v6, 0x0

    #@77
    invoke-interface {v5, v6}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@7a
    move-result v5

    #@7b
    if-nez v5, :cond_88

    #@7d
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@80
    move-result-object v5

    #@81
    const/4 v6, 0x2

    #@82
    invoke-interface {v5, v6}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@85
    move-result v5

    #@86
    if-eqz v5, :cond_90

    #@88
    .line 88
    :cond_88
    const-string v5, "BluetoothPbapReceiver"

    #@8a
    const-string v6, "PBAP profile is blocked by LG MDM Server Policy"

    #@8c
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 89
    const/4 v3, 0x0

    #@90
    .line 94
    :cond_90
    if-eqz v3, :cond_b1

    #@92
    .line 96
    const-string v5, "BluetoothPbapReceiver"

    #@94
    new-instance v6, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v7, "***********Calling start service!!!! with action = "

    #@9b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v6

    #@9f
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@a2
    move-result-object v7

    #@a3
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a6
    move-result-object v6

    #@a7
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@aa
    move-result-object v6

    #@ab
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ae
    .line 98
    invoke-virtual {p1, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@b1
    .line 100
    :cond_b1
    return-void

    #@b2
    .line 77
    :cond_b2
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@b5
    move-result-object v1

    #@b6
    .line 78
    .local v1, adapter:Landroid/bluetooth/BluetoothAdapter;
    if-eqz v1, :cond_be

    #@b8
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@bb
    move-result v5

    #@bc
    if-nez v5, :cond_6c

    #@be
    .line 79
    :cond_be
    const/4 v3, 0x0

    #@bf
    goto :goto_6c
.end method
