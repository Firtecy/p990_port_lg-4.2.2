.class public Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;
.super Ljava/lang/Object;
.source "BluetoothPbapVcardManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    }
.end annotation


# static fields
.field static final CALLLOG_SORT_ORDER:Ljava/lang/String; = "_id DESC"

.field static final CONTACTS_ID_COLUMN_INDEX:I = 0x0

.field static final CONTACTS_NAME_COLUMN_INDEX:I = 0x1

.field static final CONTACTS_PROJECTION:[Ljava/lang/String; = null

.field static final PHONES_PROJECTION:[Ljava/lang/String; = null

.field private static final PHONE_NUMBER_COLUMN_INDEX:I = 0x3

.field static final SORT_ORDER_PHONE_NUMBER:Ljava/lang/String; = "data1 ASC"

.field private static final TAG:Ljava/lang/String; = "BluetoothPbapVcardManager"

.field private static final V:Z = true


# instance fields
.field private CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

.field private mResolver:Landroid/content/ContentResolver;


# direct methods
.method static constructor <clinit>()V
    .registers 6

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 88
    const/4 v0, 0x5

    #@4
    new-array v0, v0, [Ljava/lang/String;

    #@6
    const-string v1, "_id"

    #@8
    aput-object v1, v0, v3

    #@a
    const-string v1, "data2"

    #@c
    aput-object v1, v0, v4

    #@e
    const-string v1, "data3"

    #@10
    aput-object v1, v0, v5

    #@12
    const/4 v1, 0x3

    #@13
    const-string v2, "data1"

    #@15
    aput-object v2, v0, v1

    #@17
    const/4 v1, 0x4

    #@18
    const-string v2, "display_name"

    #@1a
    aput-object v2, v0, v1

    #@1c
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->PHONES_PROJECTION:[Ljava/lang/String;

    #@1e
    .line 100
    new-array v0, v5, [Ljava/lang/String;

    #@20
    const-string v1, "_id"

    #@22
    aput-object v1, v0, v3

    #@24
    const-string v1, "display_name"

    #@26
    aput-object v1, v0, v4

    #@28
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CONTACTS_PROJECTION:[Ljava/lang/String;

    #@2a
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 145
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@5
    .line 146
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@7
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@d
    .line 148
    invoke-static {p1}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->getInstance(Landroid/content/Context;)Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@13
    .line 150
    return-void
.end method

.method private final getOwnerPhoneNumberVcardFromProfile(Z[B)Ljava/lang/String;
    .registers 5
    .parameter "vcardType21"
    .parameter "filter"

    #@0
    .prologue
    .line 160
    if-eqz p1, :cond_14

    #@2
    .line 161
    const/high16 v0, -0x4000

    #@4
    .line 166
    .local v0, vcardType:I
    :goto_4
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->includePhotosInVcard()Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_d

    #@a
    .line 167
    const/high16 v1, 0x80

    #@c
    or-int/2addr v0, v1

    #@d
    .line 170
    :cond_d
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@f
    invoke-static {v1, v0, p2}, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->createProfileVCard(Landroid/content/Context;I[B)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    return-object v1

    #@14
    .line 163
    .end local v0           #vcardType:I
    :cond_14
    const v0, -0x3fffffff

    #@17
    .restart local v0       #vcardType:I
    goto :goto_4
.end method


# virtual methods
.method public final composeAndSendCallLogVcards(ILjavax/obex/Operation;IIZ)I
    .registers 25
    .parameter "type"
    .parameter "op"
    .parameter "startPoint"
    .parameter "endPoint"
    .parameter "vcardType21"

    #@0
    .prologue
    .line 380
    const/4 v2, 0x1

    #@1
    move/from16 v0, p3

    #@3
    if-lt v0, v2, :cond_b

    #@5
    move/from16 v0, p3

    #@7
    move/from16 v1, p4

    #@9
    if-le v0, v1, :cond_15

    #@b
    .line 381
    :cond_b
    const-string v2, "BluetoothPbapVcardManager"

    #@d
    const-string v6, "internal error: startPoint or endPoint is not correct."

    #@f
    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 382
    const/16 v2, 0xd0

    #@14
    .line 442
    :goto_14
    return v2

    #@15
    .line 384
    :cond_15
    invoke-static/range {p1 .. p1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->createSelectionPara(I)Ljava/lang/String;

    #@18
    move-result-object v5

    #@19
    .line 386
    .local v5, typeSelection:Ljava/lang/String;
    sget-object v3, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@1b
    .line 387
    .local v3, myUri:Landroid/net/Uri;
    const/4 v2, 0x1

    #@1c
    new-array v4, v2, [Ljava/lang/String;

    #@1e
    const/4 v2, 0x0

    #@1f
    const-string v6, "_id"

    #@21
    aput-object v6, v4, v2

    #@23
    .line 390
    .local v4, CALLLOG_PROJECTION:[Ljava/lang/String;
    const/4 v12, 0x0

    #@24
    .line 392
    .local v12, ID_COLUMN_INDEX:I
    const/4 v13, 0x0

    #@25
    .line 393
    .local v13, callsCursor:Landroid/database/Cursor;
    const-wide/16 v17, 0x0

    #@27
    .line 394
    .local v17, startPointId:J
    const-wide/16 v14, 0x0

    #@29
    .line 397
    .local v14, endPointId:J
    :try_start_29
    move-object/from16 v0, p0

    #@2b
    iget-object v2, v0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@2d
    const/4 v6, 0x0

    #@2e
    const-string v7, "_id DESC"

    #@30
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@33
    move-result-object v13

    #@34
    .line 399
    if-eqz v13, :cond_7a

    #@36
    .line 400
    add-int/lit8 v2, p3, -0x1

    #@38
    invoke-interface {v13, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@3b
    .line 401
    const/4 v2, 0x0

    #@3c
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    #@3f
    move-result-wide v17

    #@40
    .line 403
    const-string v2, "BluetoothPbapVcardManager"

    #@42
    new-instance v6, Ljava/lang/StringBuilder;

    #@44
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@47
    const-string v7, "Call Log query startPointId = "

    #@49
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v6

    #@4d
    move-wide/from16 v0, v17

    #@4f
    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    invoke-static {v2, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 405
    move/from16 v0, p3

    #@5c
    move/from16 v1, p4

    #@5e
    if-ne v0, v1, :cond_c4

    #@60
    .line 406
    move-wide/from16 v14, v17

    #@62
    .line 412
    :goto_62
    const-string v2, "BluetoothPbapVcardManager"

    #@64
    new-instance v6, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v7, "Call log query endPointId = "

    #@6b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v6

    #@6f
    invoke-virtual {v6, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v6

    #@77
    invoke-static {v2, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7a
    .catchall {:try_start_29 .. :try_end_7a} :catchall_cf

    #@7a
    .line 416
    :cond_7a
    if-eqz v13, :cond_7f

    #@7c
    .line 417
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@7f
    .line 422
    :cond_7f
    move/from16 v0, p3

    #@81
    move/from16 v1, p4

    #@83
    if-ne v0, v1, :cond_d6

    #@85
    .line 423
    new-instance v2, Ljava/lang/StringBuilder;

    #@87
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@8a
    const-string v6, "_id="

    #@8c
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v2

    #@90
    move-wide/from16 v0, v17

    #@92
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@95
    move-result-object v2

    #@96
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v16

    #@9a
    .line 432
    .local v16, recordSelection:Ljava/lang/String;
    :goto_9a
    if-nez v5, :cond_102

    #@9c
    .line 433
    move-object/from16 v8, v16

    #@9e
    .line 439
    .local v8, selection:Ljava/lang/String;
    :goto_9e
    const-string v2, "BluetoothPbapVcardManager"

    #@a0
    new-instance v6, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v7, "Call log query selection is: "

    #@a7
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v6

    #@ab
    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v6

    #@af
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v6

    #@b3
    invoke-static {v2, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 442
    const/4 v10, 0x0

    #@b7
    const/4 v11, 0x0

    #@b8
    move-object/from16 v6, p0

    #@ba
    move-object/from16 v7, p2

    #@bc
    move/from16 v9, p5

    #@be
    invoke-virtual/range {v6 .. v11}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->composeAndSendVCards(Ljavax/obex/Operation;Ljava/lang/String;ZLjava/lang/String;Z)I

    #@c1
    move-result v2

    #@c2
    goto/16 :goto_14

    #@c4
    .line 408
    .end local v8           #selection:Ljava/lang/String;
    .end local v16           #recordSelection:Ljava/lang/String;
    :cond_c4
    add-int/lit8 v2, p4, -0x1

    #@c6
    :try_start_c6
    invoke-interface {v13, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@c9
    .line 409
    const/4 v2, 0x0

    #@ca
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_cd
    .catchall {:try_start_c6 .. :try_end_cd} :catchall_cf

    #@cd
    move-result-wide v14

    #@ce
    goto :goto_62

    #@cf
    .line 416
    :catchall_cf
    move-exception v2

    #@d0
    if-eqz v13, :cond_d5

    #@d2
    .line 417
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    #@d5
    .line 416
    :cond_d5
    throw v2

    #@d6
    .line 427
    :cond_d6
    new-instance v2, Ljava/lang/StringBuilder;

    #@d8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@db
    const-string v6, "_id>="

    #@dd
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v2

    #@e1
    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v2

    #@e5
    const-string v6, " AND "

    #@e7
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v2

    #@eb
    const-string v6, "_id"

    #@ed
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v2

    #@f1
    const-string v6, "<="

    #@f3
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v2

    #@f7
    move-wide/from16 v0, v17

    #@f9
    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@fc
    move-result-object v2

    #@fd
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@100
    move-result-object v16

    #@101
    .restart local v16       #recordSelection:Ljava/lang/String;
    goto :goto_9a

    #@102
    .line 435
    :cond_102
    new-instance v2, Ljava/lang/StringBuilder;

    #@104
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@107
    const-string v6, "("

    #@109
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10c
    move-result-object v2

    #@10d
    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@110
    move-result-object v2

    #@111
    const-string v6, ") AND ("

    #@113
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@116
    move-result-object v2

    #@117
    move-object/from16 v0, v16

    #@119
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v2

    #@11d
    const-string v6, ")"

    #@11f
    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@122
    move-result-object v2

    #@123
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@126
    move-result-object v8

    #@127
    .restart local v8       #selection:Ljava/lang/String;
    goto/16 :goto_9e
.end method

.method public final composeAndSendPhonebookOneVcard(Ljavax/obex/Operation;IZLjava/lang/String;I)I
    .registers 18
    .parameter "op"
    .parameter "offset"
    .parameter "vcardType21"
    .parameter "ownerVCard"
    .parameter "orderByWhat"

    #@0
    .prologue
    .line 509
    const/4 v1, 0x1

    #@1
    if-ge p2, v1, :cond_d

    #@3
    .line 510
    const-string v1, "BluetoothPbapVcardManager"

    #@5
    const-string v3, "Internal error: offset is not correct."

    #@7
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 511
    const/16 v1, 0xd0

    #@c
    .line 565
    :goto_c
    return v1

    #@d
    .line 513
    :cond_d
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@f
    .line 514
    .local v2, myUri:Landroid/net/Uri;
    const/4 v9, 0x0

    #@10
    .line 515
    .local v9, contactCursor:Landroid/database/Cursor;
    const/4 v5, 0x0

    #@11
    .line 516
    .local v5, selection:Ljava/lang/String;
    const-wide/16 v10, 0x0

    #@13
    .line 517
    .local v10, contactId:J
    sget v1, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->ORDER_BY_INDEXED:I

    #@15
    move/from16 v0, p5

    #@17
    if-ne v0, v1, :cond_8c

    #@19
    .line 519
    :try_start_19
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@1b
    sget-object v3, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CONTACTS_PROJECTION:[Ljava/lang/String;

    #@1d
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@1f
    const/4 v5, 0x0

    #@20
    const-string v6, "_id"

    #@22
    .end local v5           #selection:Ljava/lang/String;
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@25
    move-result-object v9

    #@26
    .line 521
    if-eqz v9, :cond_4a

    #@28
    .line 522
    add-int/lit8 v1, p2, -0x1

    #@2a
    invoke-interface {v9, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@2d
    .line 523
    const/4 v1, 0x0

    #@2e
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    #@31
    move-result-wide v10

    #@32
    .line 525
    const-string v1, "BluetoothPbapVcardManager"

    #@34
    new-instance v3, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v4, "Query startPointId = "

    #@3b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@42
    move-result-object v3

    #@43
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v3

    #@47
    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4a
    .catchall {:try_start_19 .. :try_end_4a} :catchall_85

    #@4a
    .line 529
    :cond_4a
    if-eqz v9, :cond_4f

    #@4c
    .line 530
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@4f
    .line 559
    :cond_4f
    :goto_4f
    new-instance v1, Ljava/lang/StringBuilder;

    #@51
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@54
    const-string v3, "_id="

    #@56
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v1

    #@5a
    invoke-virtual {v1, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v1

    #@5e
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@61
    move-result-object v5

    #@62
    .line 562
    .restart local v5       #selection:Ljava/lang/String;
    const-string v1, "BluetoothPbapVcardManager"

    #@64
    new-instance v3, Ljava/lang/StringBuilder;

    #@66
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@69
    const-string v4, "Query selection is: "

    #@6b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v3

    #@6f
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v3

    #@73
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v3

    #@77
    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 565
    const/4 v8, 0x1

    #@7b
    move-object v3, p0

    #@7c
    move-object v4, p1

    #@7d
    move v6, p3

    #@7e
    move-object/from16 v7, p4

    #@80
    invoke-virtual/range {v3 .. v8}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->composeAndSendVCards(Ljavax/obex/Operation;Ljava/lang/String;ZLjava/lang/String;Z)I

    #@83
    move-result v1

    #@84
    goto :goto_c

    #@85
    .line 529
    .end local v5           #selection:Ljava/lang/String;
    :catchall_85
    move-exception v1

    #@86
    if-eqz v9, :cond_8b

    #@88
    .line 530
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@8b
    .line 529
    :cond_8b
    throw v1

    #@8c
    .line 533
    .restart local v5       #selection:Ljava/lang/String;
    :cond_8c
    sget v1, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->ORDER_BY_ALPHABETICAL:I

    #@8e
    move/from16 v0, p5

    #@90
    if-ne v0, v1, :cond_d0

    #@92
    .line 536
    :try_start_92
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@94
    sget-object v3, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CONTACTS_PROJECTION:[Ljava/lang/String;

    #@96
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@98
    const/4 v5, 0x0

    #@99
    const-string v6, "display_name COLLATE LOCALIZED ASC"

    #@9b
    .end local v5           #selection:Ljava/lang/String;
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@9e
    move-result-object v9

    #@9f
    .line 543
    if-eqz v9, :cond_c3

    #@a1
    .line 544
    add-int/lit8 v1, p2, -0x1

    #@a3
    invoke-interface {v9, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@a6
    .line 545
    const/4 v1, 0x0

    #@a7
    invoke-interface {v9, v1}, Landroid/database/Cursor;->getLong(I)J

    #@aa
    move-result-wide v10

    #@ab
    .line 547
    const-string v1, "BluetoothPbapVcardManager"

    #@ad
    new-instance v3, Ljava/lang/StringBuilder;

    #@af
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@b2
    const-string v4, "Query startPointId = "

    #@b4
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v3

    #@b8
    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v3

    #@bc
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v3

    #@c0
    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c3
    .catchall {:try_start_92 .. :try_end_c3} :catchall_c9

    #@c3
    .line 551
    :cond_c3
    if-eqz v9, :cond_4f

    #@c5
    .line 552
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@c8
    goto :goto_4f

    #@c9
    .line 551
    :catchall_c9
    move-exception v1

    #@ca
    if-eqz v9, :cond_cf

    #@cc
    .line 552
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    #@cf
    .line 551
    :cond_cf
    throw v1

    #@d0
    .line 556
    .restart local v5       #selection:Ljava/lang/String;
    :cond_d0
    const-string v1, "BluetoothPbapVcardManager"

    #@d2
    const-string v3, "Parameter orderByWhat is not supported!"

    #@d4
    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d7
    .line 557
    const/16 v1, 0xd0

    #@d9
    goto/16 :goto_c
.end method

.method public final composeAndSendPhonebookVcards(Ljavax/obex/Operation;IIZLjava/lang/String;)I
    .registers 21
    .parameter "op"
    .parameter "startPoint"
    .parameter "endPoint"
    .parameter "vcardType21"
    .parameter "ownerVCard"

    #@0
    .prologue
    .line 447
    const/4 v2, 0x1

    #@1
    move/from16 v0, p2

    #@3
    if-lt v0, v2, :cond_b

    #@5
    move/from16 v0, p2

    #@7
    move/from16 v1, p3

    #@9
    if-le v0, v1, :cond_15

    #@b
    .line 448
    :cond_b
    const-string v2, "BluetoothPbapVcardManager"

    #@d
    const-string v4, "internal error: startPoint or endPoint is not correct."

    #@f
    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 449
    const/16 v2, 0xd0

    #@14
    .line 504
    :goto_14
    return v2

    #@15
    .line 451
    :cond_15
    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@17
    .line 453
    .local v3, myUri:Landroid/net/Uri;
    const/4 v10, 0x0

    #@18
    .line 454
    .local v10, contactCursor:Landroid/database/Cursor;
    const-wide/16 v13, 0x0

    #@1a
    .line 455
    .local v13, startPointId:J
    const-wide/16 v11, 0x0

    #@1c
    .line 457
    .local v11, endPointId:J
    :try_start_1c
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@1e
    sget-object v4, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CONTACTS_PROJECTION:[Ljava/lang/String;

    #@20
    iget-object v5, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@22
    const/4 v6, 0x0

    #@23
    const-string v7, "_id"

    #@25
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@28
    move-result-object v10

    #@29
    .line 459
    if-eqz v10, :cond_6c

    #@2b
    .line 460
    add-int/lit8 v2, p2, -0x1

    #@2d
    invoke-interface {v10, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@30
    .line 461
    const/4 v2, 0x0

    #@31
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    #@34
    move-result-wide v13

    #@35
    .line 463
    const-string v2, "BluetoothPbapVcardManager"

    #@37
    new-instance v4, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v5, "Query startPointId = "

    #@3e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@45
    move-result-object v4

    #@46
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v4

    #@4a
    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 465
    move/from16 v0, p2

    #@4f
    move/from16 v1, p3

    #@51
    if-ne v0, v1, :cond_c4

    #@53
    .line 466
    move-wide v11, v13

    #@54
    .line 472
    :goto_54
    const-string v2, "BluetoothPbapVcardManager"

    #@56
    new-instance v4, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v5, "Query endPointId = "

    #@5d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v4

    #@61
    invoke-virtual {v4, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6c
    .catchall {:try_start_1c .. :try_end_6c} :catchall_cf

    #@6c
    .line 476
    :cond_6c
    if-eqz v10, :cond_71

    #@6e
    .line 477
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@71
    .line 483
    :cond_71
    const-string v2, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@73
    invoke-static {v2}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@76
    move-result v2

    #@77
    if-eqz v2, :cond_10c

    #@79
    .line 484
    move/from16 v0, p2

    #@7b
    move/from16 v1, p3

    #@7d
    if-ne v0, v1, :cond_d6

    #@7f
    .line 485
    new-instance v2, Ljava/lang/StringBuilder;

    #@81
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@84
    const-string v4, "_id="

    #@86
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v2

    #@8a
    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v2

    #@8e
    const-string v4, " AND "

    #@90
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v2

    #@94
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@96
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v2

    #@9a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9d
    move-result-object v6

    #@9e
    .line 501
    .local v6, selection:Ljava/lang/String;
    :goto_9e
    const-string v2, "BluetoothPbapVcardManager"

    #@a0
    new-instance v4, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v5, "Query selection is: "

    #@a7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v4

    #@ab
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v4

    #@af
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b2
    move-result-object v4

    #@b3
    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@b6
    .line 504
    const/4 v9, 0x1

    #@b7
    move-object v4, p0

    #@b8
    move-object/from16 v5, p1

    #@ba
    move/from16 v7, p4

    #@bc
    move-object/from16 v8, p5

    #@be
    invoke-virtual/range {v4 .. v9}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->composeAndSendVCards(Ljavax/obex/Operation;Ljava/lang/String;ZLjava/lang/String;Z)I

    #@c1
    move-result v2

    #@c2
    goto/16 :goto_14

    #@c4
    .line 468
    .end local v6           #selection:Ljava/lang/String;
    :cond_c4
    add-int/lit8 v2, p3, -0x1

    #@c6
    :try_start_c6
    invoke-interface {v10, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@c9
    .line 469
    const/4 v2, 0x0

    #@ca
    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J
    :try_end_cd
    .catchall {:try_start_c6 .. :try_end_cd} :catchall_cf

    #@cd
    move-result-wide v11

    #@ce
    goto :goto_54

    #@cf
    .line 476
    :catchall_cf
    move-exception v2

    #@d0
    if-eqz v10, :cond_d5

    #@d2
    .line 477
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    #@d5
    .line 476
    :cond_d5
    throw v2

    #@d6
    .line 487
    :cond_d6
    new-instance v2, Ljava/lang/StringBuilder;

    #@d8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@db
    const-string v4, "_id>="

    #@dd
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v2

    #@e1
    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v2

    #@e5
    const-string v4, " AND "

    #@e7
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v2

    #@eb
    const-string v4, "_id"

    #@ed
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v2

    #@f1
    const-string v4, "<="

    #@f3
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v2

    #@f7
    invoke-virtual {v2, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@fa
    move-result-object v2

    #@fb
    const-string v4, " AND "

    #@fd
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@100
    move-result-object v2

    #@101
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@103
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@106
    move-result-object v2

    #@107
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10a
    move-result-object v6

    #@10b
    .restart local v6       #selection:Ljava/lang/String;
    goto :goto_9e

    #@10c
    .line 491
    .end local v6           #selection:Ljava/lang/String;
    :cond_10c
    move/from16 v0, p2

    #@10e
    move/from16 v1, p3

    #@110
    if-ne v0, v1, :cond_127

    #@112
    .line 492
    new-instance v2, Ljava/lang/StringBuilder;

    #@114
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@117
    const-string v4, "_id="

    #@119
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11c
    move-result-object v2

    #@11d
    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@120
    move-result-object v2

    #@121
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@124
    move-result-object v6

    #@125
    .restart local v6       #selection:Ljava/lang/String;
    goto/16 :goto_9e

    #@127
    .line 494
    .end local v6           #selection:Ljava/lang/String;
    :cond_127
    new-instance v2, Ljava/lang/StringBuilder;

    #@129
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12c
    const-string v4, "_id>="

    #@12e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@131
    move-result-object v2

    #@132
    invoke-virtual {v2, v13, v14}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@135
    move-result-object v2

    #@136
    const-string v4, " AND "

    #@138
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v2

    #@13c
    const-string v4, "_id"

    #@13e
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@141
    move-result-object v2

    #@142
    const-string v4, "<="

    #@144
    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@147
    move-result-object v2

    #@148
    invoke-virtual {v2, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@14b
    move-result-object v2

    #@14c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14f
    move-result-object v6

    #@150
    .restart local v6       #selection:Ljava/lang/String;
    goto/16 :goto_9e
.end method

.method public final composeAndSendVCards(Ljavax/obex/Operation;Ljava/lang/String;ZLjava/lang/String;Z)I
    .registers 20
    .parameter "op"
    .parameter "selection"
    .parameter "vcardType21"
    .parameter "ownerVCard"
    .parameter "isContacts"

    #@0
    .prologue
    .line 570
    const-wide/16 v6, 0x0

    #@2
    .line 572
    .local v6, timestamp:J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@5
    move-result-wide v6

    #@6
    .line 575
    if-eqz p5, :cond_125

    #@8
    .line 578
    const/4 v4, 0x0

    #@9
    .line 580
    .local v4, composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    const/4 v1, 0x0

    #@a
    .line 584
    .local v1, buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    if-eqz p3, :cond_6f

    #@c
    .line 585
    const/high16 v9, -0x4000

    #@e
    .line 590
    .local v9, vcardType:I
    :goto_e
    :try_start_e
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->includePhotosInVcard()Z

    #@11
    move-result v10

    #@12
    if-nez v10, :cond_17

    #@14
    .line 591
    const/high16 v10, 0x80

    #@16
    or-int/2addr v9, v10

    #@17
    .line 600
    :cond_17
    const-string v3, "UTF-8"

    #@19
    .line 601
    .local v3, charset:Ljava/lang/String;
    iget-object v10, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@1b
    if-eqz v10, :cond_73

    #@1d
    iget-object v10, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@1f
    invoke-virtual {v10}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->isUseFilter()Z

    #@22
    move-result v10

    #@23
    if-eqz v10, :cond_73

    #@25
    .line 602
    const-string v10, "BluetoothPbapVcardManager"

    #@27
    const-string v11, "[BTUI] [PBAP] call VCardComposer() : DO use filter"

    #@29
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 603
    const/high16 v10, 0x8

    #@2e
    or-int/2addr v9, v10

    #@2f
    .line 604
    new-instance v5, Lcom/lge/bluetooth/LGBluetoothVCardComposer;

    #@31
    iget-object v10, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@33
    iget-object v11, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mManager:Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@35
    invoke-virtual {v11}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->getPbapFilter()[B

    #@38
    move-result-object v11

    #@39
    invoke-direct {v5, v10, v9, v3, v11}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;-><init>(Landroid/content/Context;ILjava/lang/String;[B)V

    #@3c
    .end local v4           #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .local v5, composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    move-object v4, v5

    #@3d
    .line 613
    .end local v5           #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .restart local v4       #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    :goto_3d
    new-instance v10, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$1;

    #@3f
    invoke-direct {v10, p0}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$1;-><init>(Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;)V

    #@42
    invoke-virtual {v4, v10}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->setPhoneNumberTranslationCallback(Lcom/android/vcard/VCardPhoneNumberTranslationCallback;)V

    #@45
    .line 626
    new-instance v2, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;

    #@47
    move-object/from16 v0, p4

    #@49
    invoke-direct {v2, p0, p1, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;-><init>(Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;Ljavax/obex/Operation;Ljava/lang/String;)V
    :try_end_4c
    .catchall {:try_start_e .. :try_end_4c} :catchall_119

    #@4c
    .line 627
    .end local v1           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .local v2, buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    :try_start_4c
    sget-object v10, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@4e
    const/4 v11, 0x0

    #@4f
    const-string v12, "_id"

    #@51
    move-object/from16 v0, p2

    #@53
    invoke-virtual {v4, v10, v0, v11, v12}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->init(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    #@56
    move-result v10

    #@57
    if-eqz v10, :cond_61

    #@59
    iget-object v10, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@5b
    invoke-virtual {v2, v10}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onInit(Landroid/content/Context;)Z
    :try_end_5e
    .catchall {:try_start_4c .. :try_end_5e} :catchall_1cd

    #@5e
    move-result v10

    #@5f
    if-nez v10, :cond_84

    #@61
    .line 629
    :cond_61
    const/16 v10, 0xd0

    #@63
    .line 655
    if-eqz v4, :cond_68

    #@65
    .line 656
    invoke-virtual {v4}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->terminate()V

    #@68
    .line 658
    :cond_68
    if-eqz v2, :cond_6d

    #@6a
    .line 659
    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onTerminate()V

    #@6d
    :cond_6d
    move-object v1, v2

    #@6e
    .line 709
    .end local v2           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .end local v3           #charset:Ljava/lang/String;
    .end local v4           #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .end local v9           #vcardType:I
    .end local p1
    .restart local v1       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    :goto_6e
    return v10

    #@6f
    .line 587
    .restart local v4       #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .restart local p1
    :cond_6f
    const v9, -0x3fffffff

    #@72
    .restart local v9       #vcardType:I
    goto :goto_e

    #@73
    .line 606
    .restart local v3       #charset:Ljava/lang/String;
    :cond_73
    :try_start_73
    const-string v10, "BluetoothPbapVcardManager"

    #@75
    const-string v11, "[BTUI] [PBAP] call VCardComposer() : NOT use filter"

    #@77
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 607
    new-instance v5, Lcom/lge/bluetooth/LGBluetoothVCardComposer;

    #@7c
    iget-object v10, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@7e
    const/4 v11, 0x0

    #@7f
    invoke-direct {v5, v10, v9, v3, v11}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;-><init>(Landroid/content/Context;ILjava/lang/String;[B)V
    :try_end_82
    .catchall {:try_start_73 .. :try_end_82} :catchall_119

    #@82
    .end local v4           #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .restart local v5       #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    move-object v4, v5

    #@83
    .end local v5           #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .restart local v4       #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    goto :goto_3d

    #@84
    .line 632
    .end local v1           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v2       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    :cond_84
    :try_start_84
    invoke-virtual {v4}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->isAfterLast()Z

    #@87
    move-result v10

    #@88
    if-nez v10, :cond_96

    #@8a
    .line 633
    sget-boolean v10, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->sIsAborted:Z

    #@8c
    if-eqz v10, :cond_c7

    #@8e
    .line 634
    check-cast p1, Ljavax/obex/ServerOperation;

    #@90
    .end local p1
    const/4 v10, 0x1

    #@91
    iput-boolean v10, p1, Ljavax/obex/ServerOperation;->isAborted:Z

    #@93
    .line 635
    const/4 v10, 0x0

    #@94
    sput-boolean v10, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->sIsAborted:Z
    :try_end_96
    .catchall {:try_start_84 .. :try_end_96} :catchall_1cd

    #@96
    .line 655
    :cond_96
    if-eqz v4, :cond_9b

    #@98
    .line 656
    invoke-virtual {v4}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->terminate()V

    #@9b
    .line 658
    :cond_9b
    if-eqz v2, :cond_a0

    #@9d
    .line 659
    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onTerminate()V

    #@a0
    :cond_a0
    move-object v1, v2

    #@a1
    .line 705
    .end local v2           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .end local v3           #charset:Ljava/lang/String;
    .end local v4           #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .end local v9           #vcardType:I
    .restart local v1       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    :goto_a1
    const-string v10, "BluetoothPbapVcardManager"

    #@a3
    new-instance v11, Ljava/lang/StringBuilder;

    #@a5
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@a8
    const-string v12, "Total vcard composing and sending out takes "

    #@aa
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v11

    #@ae
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@b1
    move-result-wide v12

    #@b2
    sub-long/2addr v12, v6

    #@b3
    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v11

    #@b7
    const-string v12, " ms"

    #@b9
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v11

    #@bd
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c0
    move-result-object v11

    #@c1
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c4
    .line 709
    const/16 v10, 0xa0

    #@c6
    goto :goto_6e

    #@c7
    .line 638
    .end local v1           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v2       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v3       #charset:Ljava/lang/String;
    .restart local v4       #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .restart local v9       #vcardType:I
    .restart local p1
    :cond_c7
    :try_start_c7
    invoke-virtual {v4}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->createOneEntry()Ljava/lang/String;

    #@ca
    move-result-object v8

    #@cb
    .line 639
    .local v8, vcard:Ljava/lang/String;
    if-nez v8, :cond_f8

    #@cd
    .line 640
    const-string v10, "BluetoothPbapVcardManager"

    #@cf
    new-instance v11, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v12, "Failed to read a contact. Error reason: "

    #@d6
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v11

    #@da
    invoke-virtual {v4}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->getErrorReason()Ljava/lang/String;

    #@dd
    move-result-object v12

    #@de
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e1
    move-result-object v11

    #@e2
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e5
    move-result-object v11

    #@e6
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e9
    .catchall {:try_start_c7 .. :try_end_e9} :catchall_1cd

    #@e9
    .line 642
    const/16 v10, 0xd0

    #@eb
    .line 655
    if-eqz v4, :cond_f0

    #@ed
    .line 656
    invoke-virtual {v4}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->terminate()V

    #@f0
    .line 658
    :cond_f0
    if-eqz v2, :cond_f5

    #@f2
    .line 659
    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onTerminate()V

    #@f5
    :cond_f5
    move-object v1, v2

    #@f6
    .line 642
    .end local v2           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v1       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    goto/16 :goto_6e

    #@f8
    .line 645
    .end local v1           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v2       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    :cond_f8
    :try_start_f8
    const-string v10, "BluetoothPbapVcardManager"

    #@fa
    const-string v11, "Vcard Entry:"

    #@fc
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@ff
    .line 646
    const-string v10, "BluetoothPbapVcardManager"

    #@101
    invoke-static {v10, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@104
    .line 649
    invoke-virtual {v2, v8}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onEntryCreated(Ljava/lang/String;)Z
    :try_end_107
    .catchall {:try_start_f8 .. :try_end_107} :catchall_1cd

    #@107
    move-result v10

    #@108
    if-nez v10, :cond_84

    #@10a
    .line 651
    const/16 v10, 0xd0

    #@10c
    .line 655
    if-eqz v4, :cond_111

    #@10e
    .line 656
    invoke-virtual {v4}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->terminate()V

    #@111
    .line 658
    :cond_111
    if-eqz v2, :cond_116

    #@113
    .line 659
    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onTerminate()V

    #@116
    :cond_116
    move-object v1, v2

    #@117
    .line 651
    .end local v2           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v1       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    goto/16 :goto_6e

    #@119
    .line 655
    .end local v3           #charset:Ljava/lang/String;
    .end local v8           #vcard:Ljava/lang/String;
    :catchall_119
    move-exception v10

    #@11a
    .end local p1
    :goto_11a
    if-eqz v4, :cond_11f

    #@11c
    .line 656
    invoke-virtual {v4}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->terminate()V

    #@11f
    .line 658
    :cond_11f
    if-eqz v1, :cond_124

    #@121
    .line 659
    invoke-virtual {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onTerminate()V

    #@124
    .line 655
    :cond_124
    throw v10

    #@125
    .line 663
    .end local v1           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .end local v4           #composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .end local v9           #vcardType:I
    .restart local p1
    :cond_125
    const/4 v4, 0x0

    #@126
    .line 664
    .local v4, composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    const/4 v1, 0x0

    #@127
    .line 667
    .restart local v1       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    :try_start_127
    new-instance v5, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;

    #@129
    iget-object v10, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@12b
    invoke-direct {v5, v10}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;-><init>(Landroid/content/Context;)V
    :try_end_12e
    .catchall {:try_start_127 .. :try_end_12e} :catchall_1ba

    #@12e
    .line 668
    .end local v4           #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    .local v5, composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    :try_start_12e
    new-instance v2, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;

    #@130
    move-object/from16 v0, p4

    #@132
    invoke-direct {v2, p0, p1, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;-><init>(Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;Ljavax/obex/Operation;Ljava/lang/String;)V
    :try_end_135
    .catchall {:try_start_12e .. :try_end_135} :catchall_1c6

    #@135
    .line 669
    .end local v1           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v2       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    :try_start_135
    sget-object v10, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@137
    const/4 v11, 0x0

    #@138
    const-string v12, "_id DESC"

    #@13a
    move-object/from16 v0, p2

    #@13c
    invoke-virtual {v5, v10, v0, v11, v12}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->init(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z

    #@13f
    move-result v10

    #@140
    if-eqz v10, :cond_14a

    #@142
    iget-object v10, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@144
    invoke-virtual {v2, v10}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onInit(Landroid/content/Context;)Z
    :try_end_147
    .catchall {:try_start_135 .. :try_end_147} :catchall_1c9

    #@147
    move-result v10

    #@148
    if-nez v10, :cond_168

    #@14a
    .line 672
    :cond_14a
    const/16 v10, 0xd0

    #@14c
    .line 695
    if-eqz v5, :cond_151

    #@14e
    .line 696
    invoke-virtual {v5}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->terminate()V

    #@151
    .line 698
    :cond_151
    if-eqz v2, :cond_156

    #@153
    .line 699
    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onTerminate()V

    #@156
    :cond_156
    move-object v1, v2

    #@157
    .line 672
    .end local v2           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v1       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    goto/16 :goto_6e

    #@159
    .line 688
    .end local v1           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v2       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v8       #vcard:Ljava/lang/String;
    :cond_159
    :try_start_159
    const-string v10, "BluetoothPbapVcardManager"

    #@15b
    const-string v11, "Vcard Entry:"

    #@15d
    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@160
    .line 689
    const-string v10, "BluetoothPbapVcardManager"

    #@162
    invoke-static {v10, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@165
    .line 692
    invoke-virtual {v2, v8}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onEntryCreated(Ljava/lang/String;)Z

    #@168
    .line 675
    .end local v8           #vcard:Ljava/lang/String;
    :cond_168
    invoke-virtual {v5}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->isAfterLast()Z

    #@16b
    move-result v10

    #@16c
    if-nez v10, :cond_17a

    #@16e
    .line 676
    sget-boolean v10, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->sIsAborted:Z

    #@170
    if-eqz v10, :cond_187

    #@172
    .line 677
    check-cast p1, Ljavax/obex/ServerOperation;

    #@174
    .end local p1
    const/4 v10, 0x1

    #@175
    iput-boolean v10, p1, Ljavax/obex/ServerOperation;->isAborted:Z

    #@177
    .line 678
    const/4 v10, 0x0

    #@178
    sput-boolean v10, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->sIsAborted:Z
    :try_end_17a
    .catchall {:try_start_159 .. :try_end_17a} :catchall_1c9

    #@17a
    .line 695
    :cond_17a
    if-eqz v5, :cond_17f

    #@17c
    .line 696
    invoke-virtual {v5}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->terminate()V

    #@17f
    .line 698
    :cond_17f
    if-eqz v2, :cond_184

    #@181
    .line 699
    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onTerminate()V

    #@184
    :cond_184
    move-object v1, v2

    #@185
    .line 701
    .end local v2           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v1       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    goto/16 :goto_a1

    #@187
    .line 681
    .end local v1           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v2       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local p1
    :cond_187
    :try_start_187
    move/from16 v0, p3

    #@189
    invoke-virtual {v5, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->createOneEntry(Z)Ljava/lang/String;

    #@18c
    move-result-object v8

    #@18d
    .line 682
    .restart local v8       #vcard:Ljava/lang/String;
    if-nez v8, :cond_159

    #@18f
    .line 683
    const-string v10, "BluetoothPbapVcardManager"

    #@191
    new-instance v11, Ljava/lang/StringBuilder;

    #@193
    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    #@196
    const-string v12, "Failed to read a contact. Error reason: "

    #@198
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19b
    move-result-object v11

    #@19c
    invoke-virtual {v5}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->getErrorReason()Ljava/lang/String;

    #@19f
    move-result-object v12

    #@1a0
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a3
    move-result-object v11

    #@1a4
    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a7
    move-result-object v11

    #@1a8
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1ab
    .catchall {:try_start_187 .. :try_end_1ab} :catchall_1c9

    #@1ab
    .line 685
    const/16 v10, 0xd0

    #@1ad
    .line 695
    if-eqz v5, :cond_1b2

    #@1af
    .line 696
    invoke-virtual {v5}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->terminate()V

    #@1b2
    .line 698
    :cond_1b2
    if-eqz v2, :cond_1b7

    #@1b4
    .line 699
    invoke-virtual {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onTerminate()V

    #@1b7
    :cond_1b7
    move-object v1, v2

    #@1b8
    .line 685
    .end local v2           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v1       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    goto/16 :goto_6e

    #@1ba
    .line 695
    .end local v5           #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    .end local v8           #vcard:Ljava/lang/String;
    .restart local v4       #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    :catchall_1ba
    move-exception v10

    #@1bb
    .end local p1
    :goto_1bb
    if-eqz v4, :cond_1c0

    #@1bd
    .line 696
    invoke-virtual {v4}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->terminate()V

    #@1c0
    .line 698
    :cond_1c0
    if-eqz v1, :cond_1c5

    #@1c2
    .line 699
    invoke-virtual {v1}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->onTerminate()V

    #@1c5
    .line 695
    :cond_1c5
    throw v10

    #@1c6
    .end local v4           #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    .restart local v5       #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    .restart local p1
    :catchall_1c6
    move-exception v10

    #@1c7
    move-object v4, v5

    #@1c8
    .end local v5           #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    .restart local v4       #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    goto :goto_1bb

    #@1c9
    .end local v1           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .end local v4           #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    .end local p1
    .restart local v2       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v5       #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    :catchall_1c9
    move-exception v10

    #@1ca
    move-object v1, v2

    #@1cb
    .end local v2           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v1       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    move-object v4, v5

    #@1cc
    .end local v5           #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    .restart local v4       #composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    goto :goto_1bb

    #@1cd
    .line 655
    .end local v1           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v2       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v3       #charset:Ljava/lang/String;
    .local v4, composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .restart local v9       #vcardType:I
    :catchall_1cd
    move-exception v10

    #@1ce
    move-object v1, v2

    #@1cf
    .end local v2           #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    .restart local v1       #buffer:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
    goto/16 :goto_11a
.end method

.method public final getCallHistorySize(I)I
    .registers 10
    .parameter "type"

    #@0
    .prologue
    .line 230
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@2
    .line 231
    .local v1, myUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->createSelectionPara(I)Ljava/lang/String;

    #@5
    move-result-object v3

    #@6
    .line 232
    .local v3, selection:Ljava/lang/String;
    const/4 v7, 0x0

    #@7
    .line 233
    .local v7, size:I
    const/4 v6, 0x0

    #@8
    .line 235
    .local v6, callCursor:Landroid/database/Cursor;
    :try_start_8
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@a
    const/4 v2, 0x0

    #@b
    const/4 v4, 0x0

    #@c
    const-string v5, "date DESC"

    #@e
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@11
    move-result-object v6

    #@12
    .line 237
    if-eqz v6, :cond_18

    #@14
    .line 238
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_17
    .catchall {:try_start_8 .. :try_end_17} :catchall_1e

    #@17
    move-result v7

    #@18
    .line 241
    :cond_18
    if-eqz v6, :cond_1d

    #@1a
    .line 242
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@1d
    .line 245
    :cond_1d
    return v7

    #@1e
    .line 241
    :catchall_1e
    move-exception v0

    #@1f
    if-eqz v6, :cond_24

    #@21
    .line 242
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@24
    .line 241
    :cond_24
    throw v0
.end method

.method public final getContactNamesByNumber(Ljava/lang/String;)Ljava/util/ArrayList;
    .registers 13
    .parameter "phoneNumber"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 340
    new-instance v10, Ljava/util/ArrayList;

    #@2
    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 342
    .local v10, nameList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v6, 0x0

    #@6
    .line 343
    .local v6, contactCursor:Landroid/database/Cursor;
    const/4 v1, 0x0

    #@7
    .line 345
    .local v1, uri:Landroid/net/Uri;
    if-eqz p1, :cond_7c

    #@9
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_7c

    #@f
    .line 346
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@11
    .line 353
    :goto_11
    :try_start_11
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@13
    sget-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CONTACTS_PROJECTION:[Ljava/lang/String;

    #@15
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@17
    const/4 v4, 0x0

    #@18
    const-string v5, "_id"

    #@1a
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1d
    move-result-object v6

    #@1e
    .line 356
    if-eqz v6, :cond_87

    #@20
    .line 357
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@23
    :goto_23
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    #@26
    move-result v0

    #@27
    if-nez v0, :cond_87

    #@29
    .line 359
    const/4 v0, 0x1

    #@2a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@2d
    move-result-object v9

    #@2e
    .line 360
    .local v9, name:Ljava/lang/String;
    const/4 v0, 0x0

    #@2f
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    #@32
    move-result-wide v7

    #@33
    .line 361
    .local v7, id:J
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@36
    move-result v0

    #@37
    if-eqz v0, :cond_42

    #@39
    .line 362
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@3b
    const v2, 0x104000e

    #@3e
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@41
    move-result-object v9

    #@42
    .line 365
    :cond_42
    const-string v0, "BluetoothPbapVcardManager"

    #@44
    new-instance v2, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v3, "got name "

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    const-string v3, " by number "

    #@55
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v2

    #@59
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    const-string v3, " @"

    #@5f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v2

    #@63
    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v2

    #@6b
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 367
    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@71
    .line 357
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_74
    .catchall {:try_start_11 .. :try_end_74} :catchall_75

    #@74
    goto :goto_23

    #@75
    .line 371
    .end local v7           #id:J
    .end local v9           #name:Ljava/lang/String;
    :catchall_75
    move-exception v0

    #@76
    if-eqz v6, :cond_7b

    #@78
    .line 372
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@7b
    .line 371
    :cond_7b
    throw v0

    #@7c
    .line 348
    :cond_7c
    sget-object v0, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@7e
    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    #@81
    move-result-object v2

    #@82
    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@85
    move-result-object v1

    #@86
    goto :goto_11

    #@87
    .line 371
    :cond_87
    if-eqz v6, :cond_8c

    #@89
    .line 372
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@8c
    .line 375
    :cond_8c
    return-object v10
.end method

.method public final getContactsSize()I
    .registers 9

    #@0
    .prologue
    .line 213
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@2
    .line 214
    .local v1, myUri:Landroid/net/Uri;
    const/4 v7, 0x0

    #@3
    .line 215
    .local v7, size:I
    const/4 v6, 0x0

    #@4
    .line 217
    .local v6, contactCursor:Landroid/database/Cursor;
    :try_start_4
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@6
    const/4 v2, 0x0

    #@7
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@9
    const/4 v4, 0x0

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@e
    move-result-object v6

    #@f
    .line 218
    if-eqz v6, :cond_17

    #@11
    .line 219
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_14
    .catchall {:try_start_4 .. :try_end_14} :catchall_1d

    #@14
    move-result v0

    #@15
    add-int/lit8 v7, v0, 0x1

    #@17
    .line 222
    :cond_17
    if-eqz v6, :cond_1c

    #@19
    .line 223
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@1c
    .line 226
    :cond_1c
    return v7

    #@1d
    .line 222
    :catchall_1d
    move-exception v0

    #@1e
    if-eqz v6, :cond_23

    #@20
    .line 223
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@23
    .line 222
    :cond_23
    throw v0
.end method

.method public final getOwnerPhoneNumberVcard(Z[B)Ljava/lang/String;
    .registers 8
    .parameter "vcardType21"
    .parameter "filter"

    #@0
    .prologue
    .line 184
    new-instance v0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;

    #@2
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@4
    invoke-direct {v0, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;-><init>(Landroid/content/Context;)V

    #@7
    .line 185
    .local v0, composer:Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getLocalPhoneName()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 186
    .local v1, name:Ljava/lang/String;
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getLocalPhoneNum()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    .line 187
    .local v2, number:Ljava/lang/String;
    const/4 v4, 0x2

    #@10
    invoke-virtual {v0, v4, v1, v2, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->composeVCardForPhoneOwnNumber(ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    .line 189
    .local v3, vcard:Ljava/lang/String;
    return-object v3
.end method

.method public final getPhonebookNameList(I)Ljava/util/ArrayList;
    .registers 12
    .parameter "orderByWhat"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 287
    new-instance v8, Ljava/util/ArrayList;

    #@2
    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 289
    .local v8, nameList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v9, 0x0

    #@6
    .line 290
    .local v9, ownerName:Ljava/lang/String;
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->useProfileForOwnerVcard()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 291
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@e
    invoke-static {v0}, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->getProfileName(Landroid/content/Context;)Ljava/lang/String;

    #@11
    move-result-object v9

    #@12
    .line 293
    :cond_12
    if-eqz v9, :cond_1a

    #@14
    invoke-virtual {v9}, Ljava/lang/String;->length()I

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_1e

    #@1a
    .line 294
    :cond_1a
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getLocalPhoneName()Ljava/lang/String;

    #@1d
    move-result-object v9

    #@1e
    .line 296
    :cond_1e
    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@21
    .line 299
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    #@23
    .line 300
    .local v1, myUri:Landroid/net/Uri;
    const/4 v6, 0x0

    #@24
    .line 302
    .local v6, contactCursor:Landroid/database/Cursor;
    :try_start_24
    sget v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->ORDER_BY_INDEXED:I

    #@26
    if-ne p1, v0, :cond_69

    #@28
    .line 304
    const-string v0, "BluetoothPbapVcardManager"

    #@2a
    const-string v2, "getPhonebookNameList, order by index"

    #@2c
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 306
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@31
    sget-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CONTACTS_PROJECTION:[Ljava/lang/String;

    #@33
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@35
    const/4 v4, 0x0

    #@36
    const-string v5, "_id"

    #@38
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@3b
    move-result-object v6

    #@3c
    .line 321
    :cond_3c
    :goto_3c
    if-eqz v6, :cond_82

    #@3e
    .line 322
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@41
    :goto_41
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    #@44
    move-result v0

    #@45
    if-nez v0, :cond_82

    #@47
    .line 324
    const/4 v0, 0x1

    #@48
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@4b
    move-result-object v7

    #@4c
    .line 325
    .local v7, name:Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@4f
    move-result v0

    #@50
    if-eqz v0, :cond_5b

    #@52
    .line 326
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@54
    const v2, 0x104000e

    #@57
    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5a
    move-result-object v7

    #@5b
    .line 328
    :cond_5b
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@5e
    .line 322
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_61
    .catchall {:try_start_24 .. :try_end_61} :catchall_62

    #@61
    goto :goto_41

    #@62
    .line 332
    .end local v7           #name:Ljava/lang/String;
    :catchall_62
    move-exception v0

    #@63
    if-eqz v6, :cond_68

    #@65
    .line 333
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@68
    .line 332
    :cond_68
    throw v0

    #@69
    .line 308
    :cond_69
    :try_start_69
    sget v0, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->ORDER_BY_ALPHABETICAL:I

    #@6b
    if-ne p1, v0, :cond_3c

    #@6d
    .line 310
    const-string v0, "BluetoothPbapVcardManager"

    #@6f
    const-string v2, "getPhonebookNameList, order by alpha"

    #@71
    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@74
    .line 313
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@76
    sget-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CONTACTS_PROJECTION:[Ljava/lang/String;

    #@78
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@7a
    const/4 v4, 0x0

    #@7b
    const-string v5, "display_name COLLATE LOCALIZED ASC"

    #@7d
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_80
    .catchall {:try_start_69 .. :try_end_80} :catchall_62

    #@80
    move-result-object v6

    #@81
    goto :goto_3c

    #@82
    .line 332
    :cond_82
    if-eqz v6, :cond_87

    #@84
    .line 333
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@87
    .line 336
    :cond_87
    return-object v8
.end method

.method public final getPhonebookSize(I)I
    .registers 6
    .parameter "type"

    #@0
    .prologue
    .line 193
    const-string v1, "BluetoothPbapVcardManager"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "[BTUI] getPhonebookSize : type = "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 195
    sparse-switch p1, :sswitch_data_48

    #@1b
    .line 203
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->getCallHistorySize(I)I

    #@1e
    move-result v0

    #@1f
    .line 207
    .local v0, size:I
    :goto_1f
    const-string v1, "BluetoothPbapVcardManager"

    #@21
    new-instance v2, Ljava/lang/StringBuilder;

    #@23
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@26
    const-string v3, "getPhonebookSize size = "

    #@28
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b
    move-result-object v2

    #@2c
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const-string v3, " type = "

    #@32
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 209
    return v0

    #@42
    .line 200
    .end local v0           #size:I
    :sswitch_42
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->getContactsSize()I

    #@45
    move-result v0

    #@46
    .line 201
    .restart local v0       #size:I
    goto :goto_1f

    #@47
    .line 195
    nop

    #@48
    :sswitch_data_48
    .sparse-switch
        0x1 -> :sswitch_42
        0x6 -> :sswitch_42
    .end sparse-switch
.end method

.method public final loadCallHistoryList(I)Ljava/util/ArrayList;
    .registers 13
    .parameter "type"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 249
    sget-object v1, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@4
    .line 250
    .local v1, myUri:Landroid/net/Uri;
    invoke-static {p1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->createSelectionPara(I)Ljava/lang/String;

    #@7
    move-result-object v3

    #@8
    .line 251
    .local v3, selection:Ljava/lang/String;
    const/4 v0, 0x2

    #@9
    new-array v2, v0, [Ljava/lang/String;

    #@b
    const-string v0, "number"

    #@d
    aput-object v0, v2, v4

    #@f
    const-string v0, "name"

    #@11
    aput-object v0, v2, v5

    #@13
    .line 254
    .local v2, projection:[Ljava/lang/String;
    const/4 v7, 0x0

    #@14
    .line 255
    .local v7, CALLS_NUMBER_COLUMN_INDEX:I
    const/4 v6, 0x1

    #@15
    .line 257
    .local v6, CALLS_NAME_COLUMN_INDEX:I
    const/4 v8, 0x0

    #@16
    .line 258
    .local v8, callCursor:Landroid/database/Cursor;
    new-instance v9, Ljava/util/ArrayList;

    #@18
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    #@1b
    .line 260
    .local v9, list:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/String;>;"
    :try_start_1b
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mResolver:Landroid/content/ContentResolver;

    #@1d
    const/4 v4, 0x0

    #@1e
    const-string v5, "_id DESC"

    #@20
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@23
    move-result-object v8

    #@24
    .line 262
    if-eqz v8, :cond_6e

    #@26
    .line 263
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@29
    :goto_29
    invoke-interface {v8}, Landroid/database/Cursor;->isAfterLast()Z

    #@2c
    move-result v0

    #@2d
    if-nez v0, :cond_6e

    #@2f
    .line 265
    const/4 v0, 0x1

    #@30
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@33
    move-result-object v10

    #@34
    .line 266
    .local v10, name:Ljava/lang/String;
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@37
    move-result v0

    #@38
    if-eqz v0, :cond_60

    #@3a
    .line 268
    const/4 v0, 0x0

    #@3b
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@3e
    move-result-object v10

    #@3f
    .line 269
    const-string v0, "-1"

    #@41
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@44
    move-result v0

    #@45
    if-nez v0, :cond_57

    #@47
    const-string v0, "-2"

    #@49
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4c
    move-result v0

    #@4d
    if-nez v0, :cond_57

    #@4f
    const-string v0, "-3"

    #@51
    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@54
    move-result v0

    #@55
    if-eqz v0, :cond_60

    #@57
    .line 272
    :cond_57
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->mContext:Landroid/content/Context;

    #@59
    const v4, 0x7f070009

    #@5c
    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@5f
    move-result-object v10

    #@60
    .line 275
    :cond_60
    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@63
    .line 264
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_66
    .catchall {:try_start_1b .. :try_end_66} :catchall_67

    #@66
    goto :goto_29

    #@67
    .line 279
    .end local v10           #name:Ljava/lang/String;
    :catchall_67
    move-exception v0

    #@68
    if-eqz v8, :cond_6d

    #@6a
    .line 280
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@6d
    .line 279
    :cond_6d
    throw v0

    #@6e
    :cond_6e
    if-eqz v8, :cond_73

    #@70
    .line 280
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@73
    .line 283
    :cond_73
    return-object v9
.end method

.method public setSelection(I)V
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 124
    const-string v0, "BluetoothPbapVcardManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[BTUI] setSelection : type = "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 125
    const-string v0, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@1a
    invoke-static {v0}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_32

    #@20
    .line 126
    sparse-switch p1, :sswitch_data_36

    #@23
    .line 134
    const-string v0, ""

    #@25
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@27
    .line 140
    :goto_27
    return-void

    #@28
    .line 128
    :sswitch_28
    const-string v0, "account_type!=\'com.android.contacts.sim\'"

    #@2a
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@2c
    goto :goto_27

    #@2d
    .line 131
    :sswitch_2d
    const-string v0, "account_type==\'com.android.contacts.sim\'"

    #@2f
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@31
    goto :goto_27

    #@32
    .line 138
    :cond_32
    const/4 v0, 0x0

    #@33
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;->CLAUSE_ONLY_VISIBLE:Ljava/lang/String;

    #@35
    goto :goto_27

    #@36
    .line 126
    :sswitch_data_36
    .sparse-switch
        0x1 -> :sswitch_28
        0x6 -> :sswitch_2d
    .end sparse-switch
.end method
