.class public Lcom/android/bluetooth/pbap/BluetoothPbapConfig;
.super Ljava/lang/Object;
.source "BluetoothPbapConfig.java"


# static fields
.field private static sIncludePhotosInVcard:Z

.field private static sUseProfileForOwnerVcard:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 27
    const/4 v0, 0x1

    #@1
    sput-boolean v0, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->sUseProfileForOwnerVcard:Z

    #@3
    .line 28
    const/4 v0, 0x0

    #@4
    sput-boolean v0, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->sIncludePhotosInVcard:Z

    #@6
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 26
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static includePhotosInVcard()Z
    .registers 1

    #@0
    .prologue
    .line 57
    sget-boolean v0, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->sIncludePhotosInVcard:Z

    #@2
    return v0
.end method

.method public static init(Landroid/content/Context;)V
    .registers 5
    .parameter "ctx"

    #@0
    .prologue
    .line 30
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v1

    #@4
    .line 31
    .local v1, r:Landroid/content/res/Resources;
    if-eqz v1, :cond_18

    #@6
    .line 33
    const v2, 0x7f060008

    #@9
    :try_start_9
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@c
    move-result v2

    #@d
    sput-boolean v2, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->sUseProfileForOwnerVcard:Z
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_f} :catch_19

    #@f
    .line 38
    :goto_f
    const v2, 0x7f060007

    #@12
    :try_start_12
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@15
    move-result v2

    #@16
    sput-boolean v2, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->sIncludePhotosInVcard:Z
    :try_end_18
    .catch Ljava/lang/Exception; {:try_start_12 .. :try_end_18} :catch_22

    #@18
    .line 43
    :cond_18
    :goto_18
    return-void

    #@19
    .line 34
    :catch_19
    move-exception v0

    #@1a
    .line 35
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "BluetoothPbapConfig"

    #@1c
    const-string v3, ""

    #@1e
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@21
    goto :goto_f

    #@22
    .line 39
    .end local v0           #e:Ljava/lang/Exception;
    :catch_22
    move-exception v0

    #@23
    .line 40
    .restart local v0       #e:Ljava/lang/Exception;
    const-string v2, "BluetoothPbapConfig"

    #@25
    const-string v3, ""

    #@27
    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2a
    goto :goto_18
.end method

.method public static useProfileForOwnerVcard()Z
    .registers 1

    #@0
    .prologue
    .line 49
    sget-boolean v0, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->sUseProfileForOwnerVcard:Z

    #@2
    return v0
.end method
