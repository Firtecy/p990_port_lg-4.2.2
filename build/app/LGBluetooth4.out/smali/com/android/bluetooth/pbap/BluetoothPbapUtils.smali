.class public Lcom/android/bluetooth/pbap/BluetoothPbapUtils;
.super Ljava/lang/Object;
.source "BluetoothPbapUtils.java"


# static fields
.field public static FILTER_NICKNAME:I = 0x0

.field public static FILTER_PHOTO:I = 0x0

.field public static FILTER_TEL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "FilterUtils"

.field private static final V:Z = true


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 55
    const/4 v0, 0x3

    #@1
    sput v0, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->FILTER_PHOTO:I

    #@3
    .line 56
    const/4 v0, 0x7

    #@4
    sput v0, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->FILTER_TEL:I

    #@6
    .line 57
    const/16 v0, 0x17

    #@8
    sput v0, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->FILTER_NICKNAME:I

    #@a
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static createFilteredVCardComposer(Landroid/content/Context;I[B)Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    .registers 9
    .parameter "ctx"
    .parameter "vcardType"
    .parameter "filter"

    #@0
    .prologue
    .line 117
    move v2, p1

    #@1
    .line 126
    .local v2, vType:I
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapConfig;->includePhotosInVcard()Z

    #@4
    move-result v3

    #@5
    if-eqz v3, :cond_32

    #@7
    invoke-static {p2}, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->hasFilter([B)Z

    #@a
    move-result v3

    #@b
    if-eqz v3, :cond_15

    #@d
    sget v3, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->FILTER_PHOTO:I

    #@f
    invoke-static {p2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->isFilterBitSet([BI)Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_32

    #@15
    :cond_15
    const/4 v0, 0x1

    #@16
    .line 128
    .local v0, includePhoto:Z
    :goto_16
    if-nez v0, :cond_22

    #@18
    .line 130
    const-string v3, "FilterUtils"

    #@1a
    const-string v4, "Excluding images from VCardComposer..."

    #@1c
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 132
    const/high16 v3, 0x80

    #@21
    or-int/2addr v2, v3

    #@22
    .line 137
    :cond_22
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->getInstance(Landroid/content/Context;)Lcom/lge/bluetooth/LGBluetoothPbapManager;

    #@25
    move-result-object v1

    #@26
    .line 138
    .local v1, manager:Lcom/lge/bluetooth/LGBluetoothPbapManager;
    new-instance v3, Lcom/lge/bluetooth/LGBluetoothVCardComposer;

    #@28
    const-string v4, "UTF-8"

    #@2a
    invoke-virtual {v1}, Lcom/lge/bluetooth/LGBluetoothPbapManager;->getPbapFilter()[B

    #@2d
    move-result-object v5

    #@2e
    invoke-direct {v3, p0, v2, v4, v5}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;-><init>(Landroid/content/Context;ILjava/lang/String;[B)V

    #@31
    return-object v3

    #@32
    .line 126
    .end local v0           #includePhoto:Z
    .end local v1           #manager:Lcom/lge/bluetooth/LGBluetoothPbapManager;
    :cond_32
    const/4 v0, 0x0

    #@33
    goto :goto_16
.end method

.method public static final createProfileVCard(Landroid/content/Context;I[B)Ljava/lang/String;
    .registers 13
    .parameter "ctx"
    .parameter "vcardType"
    .parameter "filter"

    #@0
    .prologue
    .line 169
    const/4 v0, 0x0

    #@1
    .line 171
    .local v0, composer:Lcom/lge/bluetooth/LGBluetoothVCardComposer;
    const/4 v8, 0x0

    #@2
    .line 173
    .local v8, vcard:Ljava/lang/String;
    :try_start_2
    invoke-static {p0, p1, p2}, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->createFilteredVCardComposer(Landroid/content/Context;I[B)Lcom/lge/bluetooth/LGBluetoothVCardComposer;

    #@5
    move-result-object v0

    #@6
    .line 174
    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    #@8
    const/4 v2, 0x0

    #@9
    const/4 v3, 0x0

    #@a
    const/4 v4, 0x0

    #@b
    const/4 v5, 0x0

    #@c
    sget-object v6, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    #@e
    sget-object v9, Landroid/provider/ContactsContract$RawContactsEntity;->CONTENT_URI:Landroid/net/Uri;

    #@10
    invoke-virtual {v9}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    #@13
    move-result-object v9

    #@14
    invoke-static {v6, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@17
    move-result-object v6

    #@18
    invoke-virtual/range {v0 .. v6}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->init(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_28

    #@1e
    .line 179
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->createOneEntry()Ljava/lang/String;
    :try_end_21
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_21} :catch_45

    #@21
    move-result-object v8

    #@22
    .line 188
    :goto_22
    if-eqz v0, :cond_27

    #@24
    .line 190
    :try_start_24
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->terminate()V
    :try_end_27
    .catch Ljava/lang/Throwable; {:try_start_24 .. :try_end_27} :catch_4e

    #@27
    .line 195
    :cond_27
    :goto_27
    return-object v8

    #@28
    .line 181
    :cond_28
    :try_start_28
    const-string v1, "FilterUtils"

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "Unable to create profile vcard. Error initializing composer: "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v0}, Lcom/lge/bluetooth/LGBluetoothVCardComposer;->getErrorReason()Ljava/lang/String;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v2

    #@41
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_44
    .catch Ljava/lang/Throwable; {:try_start_28 .. :try_end_44} :catch_45

    #@44
    goto :goto_22

    #@45
    .line 185
    :catch_45
    move-exception v7

    #@46
    .line 186
    .local v7, t:Ljava/lang/Throwable;
    const-string v1, "FilterUtils"

    #@48
    const-string v2, "Unable to create profile vcard."

    #@4a
    invoke-static {v1, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4d
    goto :goto_22

    #@4e
    .line 191
    .end local v7           #t:Ljava/lang/Throwable;
    :catch_4e
    move-exception v1

    #@4f
    goto :goto_27
.end method

.method public static createProfileVCardFile(Ljava/io/File;Landroid/content/Context;)Z
    .registers 11
    .parameter "file"
    .parameter "context"

    #@0
    .prologue
    .line 201
    const/4 v1, 0x0

    #@1
    .line 202
    .local v1, is:Ljava/io/FileInputStream;
    const/4 v2, 0x0

    #@2
    .line 203
    .local v2, os:Ljava/io/FileOutputStream;
    const/4 v4, 0x1

    #@3
    .line 205
    .local v4, success:Z
    :try_start_3
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v6

    #@7
    sget-object v7, Landroid/provider/ContactsContract$Profile;->CONTENT_VCARD_URI:Landroid/net/Uri;

    #@9
    const-string v8, "r"

    #@b
    invoke-virtual {v6, v7, v8}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    #@e
    move-result-object v0

    #@f
    .line 208
    .local v0, fd:Landroid/content/res/AssetFileDescriptor;
    if-nez v0, :cond_13

    #@11
    .line 210
    const/4 v6, 0x0

    #@12
    .line 221
    .end local v0           #fd:Landroid/content/res/AssetFileDescriptor;
    :goto_12
    return v6

    #@13
    .line 212
    .restart local v0       #fd:Landroid/content/res/AssetFileDescriptor;
    :cond_13
    invoke-virtual {v0}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    #@16
    move-result-object v1

    #@17
    .line 213
    new-instance v3, Ljava/io/FileOutputStream;

    #@19
    invoke-direct {v3, p0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1c
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_1c} :catch_2a

    #@1c
    .line 214
    .end local v2           #os:Ljava/io/FileOutputStream;
    .local v3, os:Ljava/io/FileOutputStream;
    const/16 v6, 0xc8

    #@1e
    :try_start_1e
    invoke-static {v1, v3, v6}, Lcom/android/bluetooth/Utils;->copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    :try_end_21
    .catch Ljava/lang/Throwable; {:try_start_1e .. :try_end_21} :catch_34

    #@21
    move-object v2, v3

    #@22
    .line 219
    .end local v0           #fd:Landroid/content/res/AssetFileDescriptor;
    .end local v3           #os:Ljava/io/FileOutputStream;
    .restart local v2       #os:Ljava/io/FileOutputStream;
    :goto_22
    invoke-static {v1}, Lcom/android/bluetooth/Utils;->safeCloseStream(Ljava/io/InputStream;)V

    #@25
    .line 220
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->safeCloseStream(Ljava/io/OutputStream;)V

    #@28
    move v6, v4

    #@29
    .line 221
    goto :goto_12

    #@2a
    .line 215
    :catch_2a
    move-exception v5

    #@2b
    .line 216
    .local v5, t:Ljava/lang/Throwable;
    :goto_2b
    const-string v6, "FilterUtils"

    #@2d
    const-string v7, "Unable to create default contact vcard file"

    #@2f
    invoke-static {v6, v7, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@32
    .line 217
    const/4 v4, 0x0

    #@33
    goto :goto_22

    #@34
    .line 215
    .end local v2           #os:Ljava/io/FileOutputStream;
    .end local v5           #t:Ljava/lang/Throwable;
    .restart local v0       #fd:Landroid/content/res/AssetFileDescriptor;
    .restart local v3       #os:Ljava/io/FileOutputStream;
    :catch_34
    move-exception v5

    #@35
    move-object v2, v3

    #@36
    .end local v3           #os:Ljava/io/FileOutputStream;
    .restart local v2       #os:Ljava/io/FileOutputStream;
    goto :goto_2b
.end method

.method public static getProfileName(Landroid/content/Context;)Ljava/lang/String;
    .registers 10
    .parameter "context"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v3, 0x0

    #@2
    .line 154
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v0

    #@6
    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    #@8
    const/4 v2, 0x1

    #@9
    new-array v2, v2, [Ljava/lang/String;

    #@b
    const-string v4, "display_name"

    #@d
    aput-object v4, v2, v8

    #@f
    move-object v4, v3

    #@10
    move-object v5, v3

    #@11
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@14
    move-result-object v6

    #@15
    .line 157
    .local v6, c:Landroid/database/Cursor;
    const/4 v7, 0x0

    #@16
    .line 158
    .local v7, ownerName:Ljava/lang/String;
    if-eqz v6, :cond_22

    #@18
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@1b
    move-result v0

    #@1c
    if-eqz v0, :cond_22

    #@1e
    .line 159
    invoke-interface {v6, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@21
    move-result-object v7

    #@22
    .line 161
    :cond_22
    if-eqz v6, :cond_27

    #@24
    .line 162
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@27
    .line 164
    :cond_27
    return-object v7
.end method

.method public static hasFilter([B)Z
    .registers 2
    .parameter "filter"

    #@0
    .prologue
    .line 60
    if-eqz p0, :cond_7

    #@2
    array-length v0, p0

    #@3
    if-lez v0, :cond_7

    #@5
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method public static isFilterBitSet([BI)Z
    .registers 8
    .parameter "filter"
    .parameter "filterBit"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 102
    invoke-static {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->hasFilter([B)Z

    #@5
    move-result v4

    #@6
    if-eqz v4, :cond_1b

    #@8
    .line 103
    div-int/lit8 v4, p1, 0x8

    #@a
    rsub-int/lit8 v1, v4, 0x7

    #@c
    .line 104
    .local v1, byteNumber:I
    rem-int/lit8 v0, p1, 0x8

    #@e
    .line 105
    .local v0, bitNumber:I
    array-length v4, p0

    #@f
    if-ge v1, v4, :cond_1b

    #@11
    .line 106
    aget-byte v4, p0, v1

    #@13
    shl-int v5, v2, v0

    #@15
    and-int/2addr v4, v5

    #@16
    if-lez v4, :cond_19

    #@18
    .line 109
    .end local v0           #bitNumber:I
    .end local v1           #byteNumber:I
    :goto_18
    return v2

    #@19
    .restart local v0       #bitNumber:I
    .restart local v1       #byteNumber:I
    :cond_19
    move v2, v3

    #@1a
    .line 106
    goto :goto_18

    #@1b
    .end local v0           #bitNumber:I
    .end local v1           #byteNumber:I
    :cond_1b
    move v2, v3

    #@1c
    .line 109
    goto :goto_18
.end method

.method public static isNameAndNumberOnly([B)Z
    .registers 5
    .parameter "filter"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 71
    invoke-static {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapUtils;->hasFilter([B)Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 72
    const-string v2, "FilterUtils"

    #@9
    const-string v3, "No filter set. isNameAndNumberOnly=false"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 98
    :cond_e
    :goto_e
    return v1

    #@f
    .line 77
    :cond_f
    const/4 v0, 0x0

    #@10
    .local v0, i:I
    :goto_10
    const/4 v2, 0x4

    #@11
    if-gt v0, v2, :cond_1a

    #@13
    .line 78
    aget-byte v2, p0, v0

    #@15
    if-nez v2, :cond_e

    #@17
    .line 77
    add-int/lit8 v0, v0, 0x1

    #@19
    goto :goto_10

    #@1a
    .line 84
    :cond_1a
    const/4 v2, 0x5

    #@1b
    aget-byte v2, p0, v2

    #@1d
    and-int/lit8 v2, v2, 0x7f

    #@1f
    if-gtz v2, :cond_e

    #@21
    .line 89
    const/4 v2, 0x6

    #@22
    aget-byte v2, p0, v2

    #@24
    if-nez v2, :cond_e

    #@26
    .line 94
    const/4 v2, 0x7

    #@27
    aget-byte v2, p0, v2

    #@29
    and-int/lit8 v2, v2, 0x78

    #@2b
    if-gtz v2, :cond_e

    #@2d
    .line 98
    const/4 v1, 0x1

    #@2e
    goto :goto_e
.end method

.method public static isProfileSet(Landroid/content/Context;)Z
    .registers 10
    .parameter "context"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 143
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v0

    #@7
    sget-object v1, Landroid/provider/ContactsContract$Profile;->CONTENT_VCARD_URI:Landroid/net/Uri;

    #@9
    new-array v2, v7, [Ljava/lang/String;

    #@b
    const-string v4, "_id"

    #@d
    aput-object v4, v2, v8

    #@f
    move-object v4, v3

    #@10
    move-object v5, v3

    #@11
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@14
    move-result-object v6

    #@15
    .line 146
    .local v6, c:Landroid/database/Cursor;
    if-eqz v6, :cond_23

    #@17
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    #@1a
    move-result v0

    #@1b
    if-lez v0, :cond_23

    #@1d
    .line 147
    .local v7, isSet:Z
    :goto_1d
    if-eqz v6, :cond_22

    #@1f
    .line 148
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@22
    .line 150
    :cond_22
    return v7

    #@23
    .end local v7           #isSet:Z
    :cond_23
    move v7, v8

    #@24
    .line 146
    goto :goto_1d
.end method
