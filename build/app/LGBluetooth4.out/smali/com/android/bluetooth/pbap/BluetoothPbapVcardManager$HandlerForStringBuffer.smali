.class public Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;
.super Ljava/lang/Object;
.source "BluetoothPbapVcardManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "HandlerForStringBuffer"
.end annotation


# instance fields
.field private operation:Ljavax/obex/Operation;

.field private outputStream:Ljava/io/OutputStream;

.field private phoneOwnVCard:Ljava/lang/String;

.field final synthetic this$0:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;Ljavax/obex/Operation;Ljava/lang/String;)V
    .registers 6
    .parameter
    .parameter "op"
    .parameter "ownerVCard"

    #@0
    .prologue
    .line 722
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager;

    #@2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 720
    const/4 v0, 0x0

    #@6
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->phoneOwnVCard:Ljava/lang/String;

    #@8
    .line 723
    iput-object p2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->operation:Ljavax/obex/Operation;

    #@a
    .line 724
    if-eqz p3, :cond_1c

    #@c
    .line 725
    iput-object p3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->phoneOwnVCard:Ljava/lang/String;

    #@e
    .line 727
    const-string v0, "BluetoothPbapVcardManager"

    #@10
    const-string v1, "phone own number vcard:"

    #@12
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 730
    const-string v0, "BluetoothPbapVcardManager"

    #@17
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->phoneOwnVCard:Ljava/lang/String;

    #@19
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 733
    :cond_1c
    return-void
.end method

.method private write(Ljava/lang/String;)Z
    .registers 6
    .parameter "vCard"

    #@0
    .prologue
    .line 737
    if-eqz p1, :cond_2a

    #@2
    .line 738
    :try_start_2
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->outputStream:Ljava/io/OutputStream;

    #@4
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_b} :catch_d

    #@b
    .line 739
    const/4 v1, 0x1

    #@c
    .line 744
    :goto_c
    return v1

    #@d
    .line 741
    :catch_d
    move-exception v0

    #@e
    .line 742
    .local v0, e:Ljava/io/IOException;
    const-string v1, "BluetoothPbapVcardManager"

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "write outputstrem failed"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 744
    .end local v0           #e:Ljava/io/IOException;
    :cond_2a
    const/4 v1, 0x0

    #@2b
    goto :goto_c
.end method


# virtual methods
.method public onEntryCreated(Ljava/lang/String;)Z
    .registers 3
    .parameter "vcard"

    #@0
    .prologue
    .line 761
    invoke-direct {p0, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->write(Ljava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public onInit(Landroid/content/Context;)Z
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 749
    :try_start_0
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->operation:Ljavax/obex/Operation;

    #@2
    invoke-interface {v1}, Ljavax/obex/Operation;->openOutputStream()Ljava/io/OutputStream;

    #@5
    move-result-object v1

    #@6
    iput-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->outputStream:Ljava/io/OutputStream;

    #@8
    .line 750
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->phoneOwnVCard:Ljava/lang/String;

    #@a
    if-eqz v1, :cond_13

    #@c
    .line 751
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->phoneOwnVCard:Ljava/lang/String;

    #@e
    invoke-direct {p0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->write(Ljava/lang/String;)Z
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_11} :catch_15

    #@11
    move-result v1

    #@12
    .line 757
    :goto_12
    return v1

    #@13
    .line 753
    :cond_13
    const/4 v1, 0x1

    #@14
    goto :goto_12

    #@15
    .line 754
    :catch_15
    move-exception v0

    #@16
    .line 755
    .local v0, e:Ljava/io/IOException;
    const-string v1, "BluetoothPbapVcardManager"

    #@18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "open outputstrem failed"

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v2

    #@2f
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 757
    const/4 v1, 0x0

    #@33
    goto :goto_12
.end method

.method public onTerminate()V
    .registers 3

    #@0
    .prologue
    .line 765
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->outputStream:Ljava/io/OutputStream;

    #@2
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapVcardManager$HandlerForStringBuffer;->operation:Ljavax/obex/Operation;

    #@4
    invoke-static {v0, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapObexServer;->closeStream(Ljava/io/OutputStream;Ljavax/obex/Operation;)Z

    #@7
    move-result v0

    #@8
    if-nez v0, :cond_12

    #@a
    .line 767
    const-string v0, "BluetoothPbapVcardManager"

    #@c
    const-string v1, "CloseStream failed!"

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 774
    :goto_11
    return-void

    #@12
    .line 771
    :cond_12
    const-string v0, "BluetoothPbapVcardManager"

    #@14
    const-string v1, "CloseStream ok!"

    #@16
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_11
.end method
