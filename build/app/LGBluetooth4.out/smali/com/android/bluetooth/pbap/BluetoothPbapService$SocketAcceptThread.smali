.class Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;
.super Ljava/lang/Thread;
.source "BluetoothPbapService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/pbap/BluetoothPbapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SocketAcceptThread"
.end annotation


# instance fields
.field private stopped:Z

.field final synthetic this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 573
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@2
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@5
    .line 575
    const/4 v0, 0x0

    #@6
    iput-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->stopped:Z

    #@8
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/pbap/BluetoothPbapService;Lcom/android/bluetooth/pbap/BluetoothPbapService$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 573
    invoke-direct {p0, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;-><init>(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V

    #@3
    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 579
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@3
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$100(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothServerSocket;

    #@6
    move-result-object v3

    #@7
    if-nez v3, :cond_88

    #@9
    .line 580
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@b
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$200(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Z

    #@e
    move-result v3

    #@f
    if-nez v3, :cond_88

    #@11
    .line 581
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@13
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$300(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V

    #@16
    .line 660
    :cond_16
    :goto_16
    return-void

    #@17
    .line 601
    :cond_17
    :try_start_17
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@19
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$500(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothDevice;

    #@1c
    move-result-object v3

    #@1d
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    #@20
    move-result-object v3

    #@21
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$602(Ljava/lang/String;)Ljava/lang/String;

    #@24
    .line 603
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$600()Ljava/lang/String;

    #@27
    move-result-object v3

    #@28
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2b
    move-result v3

    #@2c
    if-eqz v3, :cond_3a

    #@2e
    .line 604
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@30
    const v4, 0x7f070126

    #@33
    invoke-virtual {v3, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getString(I)Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$602(Ljava/lang/String;)Ljava/lang/String;

    #@3a
    .line 606
    :cond_3a
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@3c
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$500(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothDevice;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->getTrustState()Z

    #@43
    move-result v2

    #@44
    .line 608
    .local v2, trust:Z
    const-string v3, "BluetoothPbapService"

    #@46
    new-instance v4, Ljava/lang/StringBuilder;

    #@48
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@4b
    const-string v5, "GetTrustState() = "

    #@4d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v4

    #@51
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@54
    move-result-object v4

    #@55
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5c
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_5c} :catch_c9

    #@5c
    .line 611
    if-eqz v2, :cond_108

    #@5e
    .line 614
    :try_start_5e
    const-string v3, "BluetoothPbapService"

    #@60
    new-instance v4, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v5, "incoming connection accepted from: "

    #@67
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v4

    #@6b
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$600()Ljava/lang/String;

    #@6e
    move-result-object v5

    #@6f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v4

    #@73
    const-string v5, " automatically as trusted device"

    #@75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v4

    #@79
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v4

    #@7d
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 617
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@82
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$700(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V
    :try_end_85
    .catch Ljava/io/IOException; {:try_start_5e .. :try_end_85} :catch_e9

    #@85
    .line 647
    :goto_85
    const/4 v3, 0x1

    #@86
    :try_start_86
    iput-boolean v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->stopped:Z
    :try_end_88
    .catch Ljava/io/IOException; {:try_start_86 .. :try_end_88} :catch_c9

    #@88
    .line 586
    .end local v2           #trust:Z
    :cond_88
    :goto_88
    iget-boolean v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->stopped:Z

    #@8a
    if-nez v3, :cond_16

    #@8c
    .line 589
    :try_start_8c
    const-string v3, "BluetoothPbapService"

    #@8e
    const-string v4, "Accepting socket connection..."

    #@90
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 591
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@95
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@97
    invoke-static {v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$100(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothServerSocket;

    #@9a
    move-result-object v4

    #@9b
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothServerSocket;->accept()Landroid/bluetooth/BluetoothSocket;

    #@9e
    move-result-object v4

    #@9f
    invoke-static {v3, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$402(Lcom/android/bluetooth/pbap/BluetoothPbapService;Landroid/bluetooth/BluetoothSocket;)Landroid/bluetooth/BluetoothSocket;

    #@a2
    .line 593
    const-string v3, "BluetoothPbapService"

    #@a4
    const-string v4, "Accepted socket connection..."

    #@a6
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@a9
    .line 596
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@ab
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@ad
    invoke-static {v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$400(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothSocket;

    #@b0
    move-result-object v4

    #@b1
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothSocket;->getRemoteDevice()Landroid/bluetooth/BluetoothDevice;

    #@b4
    move-result-object v4

    #@b5
    invoke-static {v3, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$502(Lcom/android/bluetooth/pbap/BluetoothPbapService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@b8
    .line 597
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@ba
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$500(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothDevice;

    #@bd
    move-result-object v3

    #@be
    if-nez v3, :cond_17

    #@c0
    .line 598
    const-string v3, "BluetoothPbapService"

    #@c2
    const-string v4, "getRemoteDevice() = null"

    #@c4
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c7
    .catch Ljava/io/IOException; {:try_start_8c .. :try_end_c7} :catch_c9

    #@c7
    goto/16 :goto_16

    #@c9
    .line 648
    :catch_c9
    move-exception v0

    #@ca
    .line 649
    .local v0, ex:Ljava/io/IOException;
    iput-boolean v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->stopped:Z

    #@cc
    .line 656
    const-string v3, "BluetoothPbapService"

    #@ce
    new-instance v4, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v5, "Accept exception: "

    #@d5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v4

    #@d9
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@dc
    move-result-object v5

    #@dd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e0
    move-result-object v4

    #@e1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e4
    move-result-object v4

    #@e5
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    goto :goto_88

    #@e9
    .line 618
    .end local v0           #ex:Ljava/io/IOException;
    .restart local v2       #trust:Z
    :catch_e9
    move-exception v0

    #@ea
    .line 619
    .restart local v0       #ex:Ljava/io/IOException;
    :try_start_ea
    const-string v3, "BluetoothPbapService"

    #@ec
    new-instance v4, Ljava/lang/StringBuilder;

    #@ee
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f1
    const-string v5, "catch exception starting obex server session"

    #@f3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f6
    move-result-object v4

    #@f7
    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    #@fa
    move-result-object v5

    #@fb
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v4

    #@ff
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@102
    move-result-object v4

    #@103
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@106
    goto/16 :goto_85

    #@108
    .line 623
    .end local v0           #ex:Ljava/io/IOException;
    :cond_108
    new-instance v1, Landroid/content/Intent;

    #@10a
    const-string v3, "android.bluetooth.device.action.CONNECTION_ACCESS_REQUEST"

    #@10c
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@10f
    .line 625
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "com.android.settings"

    #@111
    const-string v4, "com.android.settings.bluetooth.BluetoothPermissionRequest"

    #@113
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@116
    .line 626
    const-string v3, "android.bluetooth.device.extra.ACCESS_REQUEST_TYPE"

    #@118
    const/4 v4, 0x2

    #@119
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@11c
    .line 628
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    #@11e
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@120
    invoke-static {v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$500(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothDevice;

    #@123
    move-result-object v4

    #@124
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@127
    .line 629
    const-string v3, "android.bluetooth.device.extra.PACKAGE_NAME"

    #@129
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@12b
    invoke-virtual {v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->getPackageName()Ljava/lang/String;

    #@12e
    move-result-object v4

    #@12f
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@132
    .line 630
    const-string v3, "android.bluetooth.device.extra.CLASS_NAME"

    #@134
    const-class v4, Lcom/android/bluetooth/pbap/BluetoothPbapReceiver;

    #@136
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@139
    move-result-object v4

    #@13a
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@13d
    .line 632
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@13f
    const-string v4, "android.permission.BLUETOOTH_ADMIN"

    #@141
    invoke-virtual {v3, v1, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@144
    .line 633
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@146
    const/4 v4, 0x1

    #@147
    invoke-static {v3, v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$802(Lcom/android/bluetooth/pbap/BluetoothPbapService;Z)Z

    #@14a
    .line 636
    const-string v3, "BluetoothPbapService"

    #@14c
    new-instance v4, Ljava/lang/StringBuilder;

    #@14e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@151
    const-string v5, "waiting for authorization for connection from: "

    #@153
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@156
    move-result-object v4

    #@157
    invoke-static {}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$600()Ljava/lang/String;

    #@15a
    move-result-object v5

    #@15b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15e
    move-result-object v4

    #@15f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@162
    move-result-object v4

    #@163
    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@166
    .line 644
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@168
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$900(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/os/Handler;

    #@16b
    move-result-object v3

    #@16c
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@16e
    invoke-static {v4}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$900(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/os/Handler;

    #@171
    move-result-object v4

    #@172
    const/4 v5, 0x2

    #@173
    invoke-virtual {v4, v5}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@176
    move-result-object v4

    #@177
    const-wide/16 v5, 0x7530

    #@179
    invoke-virtual {v3, v4, v5, v6}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_17c
    .catch Ljava/io/IOException; {:try_start_ea .. :try_end_17c} :catch_c9

    #@17c
    goto/16 :goto_85
.end method

.method shutdown()V
    .registers 2

    #@0
    .prologue
    .line 663
    const/4 v0, 0x1

    #@1
    iput-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->stopped:Z

    #@3
    .line 664
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapService$SocketAcceptThread;->interrupt()V

    #@6
    .line 665
    return-void
.end method
