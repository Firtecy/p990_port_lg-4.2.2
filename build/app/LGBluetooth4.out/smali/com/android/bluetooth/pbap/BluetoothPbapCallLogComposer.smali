.class public Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;
.super Ljava/lang/Object;
.source "BluetoothPbapCallLogComposer.java"


# static fields
.field private static final CALLER_NAME_COLUMN_INDEX:I = 0x3

.field private static final CALLER_NUMBERLABEL_COLUMN_INDEX:I = 0x5

.field private static final CALLER_NUMBERTYPE_COLUMN_INDEX:I = 0x4

.field private static final CALL_TYPE_COLUMN_INDEX:I = 0x2

.field private static final DATE_COLUMN_INDEX:I = 0x1

.field private static final FAILURE_REASON_FAILED_TO_GET_DATABASE_INFO:Ljava/lang/String; = "Failed to get database information"

.field private static final FAILURE_REASON_NOT_INITIALIZED:Ljava/lang/String; = "The vCard composer object is not correctly initialized"

.field private static final FAILURE_REASON_NO_ENTRY:Ljava/lang/String; = "There\'s no exportable in the database"

.field private static final FAILURE_REASON_UNSUPPORTED_URI:Ljava/lang/String; = "The Uri vCard composer received is not supported by the composer."

.field private static final NO_ERROR:Ljava/lang/String; = "No error"

.field private static final NUMBER_COLUMN_INDEX:I = 0x0

.field private static final TAG:Ljava/lang/String; = "CallLogComposer"

.field private static final VCARD_PROPERTY_CALLTYPE_INCOMING:Ljava/lang/String; = "RECEIVED"

.field private static final VCARD_PROPERTY_CALLTYPE_MISSED:Ljava/lang/String; = "MISSED"

.field private static final VCARD_PROPERTY_CALLTYPE_OUTGOING:Ljava/lang/String; = "DIALED"

.field private static final VCARD_PROPERTY_X_TIMESTAMP:Ljava/lang/String; = "X-IRMC-CALL-DATETIME"

.field private static final sCallLogProjection:[Ljava/lang/String;


# instance fields
.field private mContentResolver:Landroid/content/ContentResolver;

.field private final mContext:Landroid/content/Context;

.field private mCursor:Landroid/database/Cursor;

.field private mErrorReason:Ljava/lang/String;

.field private mTerminateIsCalled:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    .line 65
    const/4 v0, 0x6

    #@1
    new-array v0, v0, [Ljava/lang/String;

    #@3
    const/4 v1, 0x0

    #@4
    const-string v2, "number"

    #@6
    aput-object v2, v0, v1

    #@8
    const/4 v1, 0x1

    #@9
    const-string v2, "date"

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x2

    #@e
    const-string v2, "type"

    #@10
    aput-object v2, v0, v1

    #@12
    const/4 v1, 0x3

    #@13
    const-string v2, "name"

    #@15
    aput-object v2, v0, v1

    #@17
    const/4 v1, 0x4

    #@18
    const-string v2, "numbertype"

    #@1a
    aput-object v2, v0, v1

    #@1c
    const/4 v1, 0x5

    #@1d
    const-string v2, "numberlabel"

    #@1f
    aput-object v2, v0, v1

    #@21
    sput-object v0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->sCallLogProjection:[Ljava/lang/String;

    #@23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 88
    const-string v0, "No error"

    #@5
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mErrorReason:Ljava/lang/String;

    #@7
    .line 91
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mContext:Landroid/content/Context;

    #@9
    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@c
    move-result-object v0

    #@d
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mContentResolver:Landroid/content/ContentResolver;

    #@f
    .line 93
    return-void
.end method

.method private createOneCallLogEntryInternal(Z)Ljava/lang/String;
    .registers 13
    .parameter "vcardVer21"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 141
    if-eqz p1, :cond_b5

    #@4
    const/high16 v7, -0x4000

    #@6
    :goto_6
    const/high16 v9, 0x200

    #@8
    or-int v6, v7, v9

    #@a
    .line 144
    .local v6, vcardType:I
    new-instance v0, Lcom/android/vcard/VCardBuilder;

    #@c
    invoke-direct {v0, v6}, Lcom/android/vcard/VCardBuilder;-><init>(I)V

    #@f
    .line 145
    .local v0, builder:Lcom/android/vcard/VCardBuilder;
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@11
    const/4 v9, 0x3

    #@12
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    .line 146
    .local v2, name:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@19
    move-result v7

    #@1a
    if-eqz v7, :cond_1e

    #@1c
    .line 147
    const-string v2, ""

    #@1e
    .line 149
    :cond_1e
    const-string v7, "-1"

    #@20
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v7

    #@24
    if-nez v7, :cond_36

    #@26
    const-string v7, "-2"

    #@28
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v7

    #@2c
    if-nez v7, :cond_36

    #@2e
    const-string v7, "-3"

    #@30
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v7

    #@34
    if-eqz v7, :cond_38

    #@36
    .line 152
    :cond_36
    const-string v2, ""

    #@38
    .line 154
    :cond_38
    new-array v7, v3, [Ljava/lang/String;

    #@3a
    aput-object v2, v7, v8

    #@3c
    invoke-static {v7}, Lcom/android/vcard/VCardUtils;->containsOnlyPrintableAscii([Ljava/lang/String;)Z

    #@3f
    move-result v7

    #@40
    if-nez v7, :cond_ba

    #@42
    .line 155
    .local v3, needCharset:Z
    :goto_42
    const-string v7, "FN"

    #@44
    invoke-virtual {v0, v7, v2, v3, v8}, Lcom/android/vcard/VCardBuilder;->appendLine(Ljava/lang/String;Ljava/lang/String;ZZ)V

    #@47
    .line 156
    const-string v7, "N"

    #@49
    invoke-virtual {v0, v7, v2, v3, v8}, Lcom/android/vcard/VCardBuilder;->appendLine(Ljava/lang/String;Ljava/lang/String;ZZ)V

    #@4c
    .line 158
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@4e
    invoke-interface {v7, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@51
    move-result-object v4

    #@52
    .line 159
    .local v4, number:Ljava/lang/String;
    const-string v7, "-1"

    #@54
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@57
    move-result v7

    #@58
    if-nez v7, :cond_6a

    #@5a
    const-string v7, "-2"

    #@5c
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5f
    move-result v7

    #@60
    if-nez v7, :cond_6a

    #@62
    const-string v7, "-3"

    #@64
    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@67
    move-result v7

    #@68
    if-eqz v7, :cond_8b

    #@6a
    .line 162
    :cond_6a
    const-string v7, "CallLogComposer"

    #@6c
    new-instance v9, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v10, "[BTUI] createOneCallLogEntryInternal() : invalid number = "

    #@73
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v9

    #@77
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v9

    #@7b
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v9

    #@7f
    invoke-static {v7, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@82
    .line 163
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mContext:Landroid/content/Context;

    #@84
    const v9, 0x7f070009

    #@87
    invoke-virtual {v7, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@8a
    move-result-object v4

    #@8b
    .line 169
    :cond_8b
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@8d
    const/4 v9, 0x4

    #@8e
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getInt(I)I

    #@91
    move-result v5

    #@92
    .line 170
    .local v5, type:I
    if-nez v5, :cond_95

    #@94
    .line 171
    const/4 v5, 0x7

    #@95
    .line 174
    :cond_95
    iget-object v7, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@97
    const/4 v9, 0x5

    #@98
    invoke-interface {v7, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9b
    move-result-object v1

    #@9c
    .line 175
    .local v1, label:Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@9f
    move-result v7

    #@a0
    if-eqz v7, :cond_a6

    #@a2
    .line 176
    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@a5
    move-result-object v1

    #@a6
    .line 178
    :cond_a6
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a9
    move-result-object v7

    #@aa
    invoke-virtual {v0, v7, v1, v4, v8}, Lcom/android/vcard/VCardBuilder;->appendTelLine(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Z)V

    #@ad
    .line 179
    invoke-direct {p0, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->tryAppendCallHistoryTimeStampField(Lcom/android/vcard/VCardBuilder;)V

    #@b0
    .line 181
    invoke-virtual {v0}, Lcom/android/vcard/VCardBuilder;->toString()Ljava/lang/String;

    #@b3
    move-result-object v7

    #@b4
    return-object v7

    #@b5
    .line 141
    .end local v0           #builder:Lcom/android/vcard/VCardBuilder;
    .end local v1           #label:Ljava/lang/String;
    .end local v2           #name:Ljava/lang/String;
    .end local v3           #needCharset:Z
    .end local v4           #number:Ljava/lang/String;
    .end local v5           #type:I
    .end local v6           #vcardType:I
    :cond_b5
    const v7, -0x3fffffff

    #@b8
    goto/16 :goto_6

    #@ba
    .restart local v0       #builder:Lcom/android/vcard/VCardBuilder;
    .restart local v2       #name:Ljava/lang/String;
    .restart local v6       #vcardType:I
    :cond_ba
    move v3, v8

    #@bb
    .line 154
    goto :goto_42
.end method

.method private final toRfc2455Format(J)Ljava/lang/String;
    .registers 5
    .parameter "millSecs"

    #@0
    .prologue
    .line 231
    new-instance v0, Landroid/text/format/Time;

    #@2
    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    #@5
    .line 232
    .local v0, startDate:Landroid/text/format/Time;
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    #@8
    .line 233
    invoke-virtual {v0}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    return-object v1
.end method

.method private tryAppendCallHistoryTimeStampField(Lcom/android/vcard/VCardBuilder;)V
    .registers 9
    .parameter "builder"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    .line 249
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@3
    const/4 v5, 0x2

    #@4
    invoke-interface {v4, v5}, Landroid/database/Cursor;->getInt(I)I

    #@7
    move-result v0

    #@8
    .line 251
    .local v0, callLogType:I
    sparse-switch v0, :sswitch_data_3e

    #@b
    .line 271
    const-string v4, "CallLogComposer"

    #@d
    const-string v5, "Call log type not correct."

    #@f
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 279
    :goto_12
    return-void

    #@13
    .line 253
    :sswitch_13
    const-string v1, "RECEIVED"

    #@15
    .line 276
    .local v1, callLogTypeStr:Ljava/lang/String;
    :goto_15
    iget-object v4, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@17
    invoke-interface {v4, v6}, Landroid/database/Cursor;->getLong(I)J

    #@1a
    move-result-wide v2

    #@1b
    .line 277
    .local v2, dateAsLong:J
    const-string v4, "X-IRMC-CALL-DATETIME"

    #@1d
    new-array v5, v6, [Ljava/lang/String;

    #@1f
    const/4 v6, 0x0

    #@20
    aput-object v1, v5, v6

    #@22
    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    #@25
    move-result-object v5

    #@26
    invoke-direct {p0, v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->toRfc2455Format(J)Ljava/lang/String;

    #@29
    move-result-object v6

    #@2a
    invoke-virtual {p1, v4, v5, v6}, Lcom/android/vcard/VCardBuilder;->appendLine(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    #@2d
    goto :goto_12

    #@2e
    .line 258
    .end local v1           #callLogTypeStr:Ljava/lang/String;
    .end local v2           #dateAsLong:J
    :sswitch_2e
    const-string v1, "RECEIVED"

    #@30
    .line 259
    .restart local v1       #callLogTypeStr:Ljava/lang/String;
    const-string v4, "CallLogComposer"

    #@32
    const-string v5, "[BTUI] tryAppendCallHistoryTimeStampField() : add TIMESTAMP to rejected call history"

    #@34
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    goto :goto_15

    #@38
    .line 263
    .end local v1           #callLogTypeStr:Ljava/lang/String;
    :sswitch_38
    const-string v1, "DIALED"

    #@3a
    .line 264
    .restart local v1       #callLogTypeStr:Ljava/lang/String;
    goto :goto_15

    #@3b
    .line 267
    .end local v1           #callLogTypeStr:Ljava/lang/String;
    :sswitch_3b
    const-string v1, "MISSED"

    #@3d
    .line 268
    .restart local v1       #callLogTypeStr:Ljava/lang/String;
    goto :goto_15

    #@3e
    .line 251
    :sswitch_data_3e
    .sparse-switch
        0x1 -> :sswitch_13
        0x2 -> :sswitch_38
        0x3 -> :sswitch_3b
        0xa -> :sswitch_2e
    .end sparse-switch
.end method


# virtual methods
.method public composeVCardForPhoneOwnNumber(ILjava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;
    .registers 12
    .parameter "phonetype"
    .parameter "phoneName"
    .parameter "phoneNumber"
    .parameter "vcardVer21"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 189
    if-eqz p4, :cond_5b

    #@3
    const/high16 v4, -0x4000

    #@5
    :goto_5
    const/high16 v5, 0x200

    #@7
    or-int v3, v4, v5

    #@9
    .line 194
    .local v3, vcardType:I
    new-instance v0, Lcom/android/vcard/VCardBuilder;

    #@b
    const-string v4, "UTF-8"

    #@d
    invoke-direct {v0, v3, v4}, Lcom/android/vcard/VCardBuilder;-><init>(ILjava/lang/String;)V

    #@10
    .line 195
    .local v0, builder:Lcom/android/vcard/VCardBuilder;
    const/4 v2, 0x0

    #@11
    .line 196
    .local v2, needCharset:Z
    const/4 v4, 0x1

    #@12
    new-array v4, v4, [Ljava/lang/String;

    #@14
    aput-object p2, v4, v6

    #@16
    invoke-static {v4}, Lcom/android/vcard/VCardUtils;->containsOnlyPrintableAscii([Ljava/lang/String;)Z

    #@19
    move-result v4

    #@1a
    if-nez v4, :cond_1d

    #@1c
    .line 197
    const/4 v2, 0x1

    #@1d
    .line 199
    :cond_1d
    if-eqz p4, :cond_5f

    #@1f
    .line 200
    const-string v4, "FN"

    #@21
    invoke-virtual {v0, v4, p2, v2, v2}, Lcom/android/vcard/VCardBuilder;->appendLine(Ljava/lang/String;Ljava/lang/String;ZZ)V

    #@24
    .line 201
    const-string v4, "N"

    #@26
    invoke-virtual {v0, v4, p2, v2, v2}, Lcom/android/vcard/VCardBuilder;->appendLine(Ljava/lang/String;Ljava/lang/String;ZZ)V

    #@29
    .line 217
    :goto_29
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2c
    move-result v4

    #@2d
    if-nez v4, :cond_3a

    #@2f
    .line 218
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    .line 219
    .local v1, label:Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v0, v4, v1, p3, v6}, Lcom/android/vcard/VCardBuilder;->appendTelLine(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Z)V

    #@3a
    .line 222
    .end local v1           #label:Ljava/lang/String;
    :cond_3a
    const-string v4, "CallLogComposer"

    #@3c
    new-instance v5, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v6, "[BTUI] [PBAP] composeOwnerCard : builder = "

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    invoke-virtual {v0}, Lcom/android/vcard/VCardBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v6

    #@4b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v5

    #@4f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v5

    #@53
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    .line 223
    invoke-virtual {v0}, Lcom/android/vcard/VCardBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v4

    #@5a
    return-object v4

    #@5b
    .line 189
    .end local v0           #builder:Lcom/android/vcard/VCardBuilder;
    .end local v2           #needCharset:Z
    .end local v3           #vcardType:I
    :cond_5b
    const v4, -0x3fffffff

    #@5e
    goto :goto_5

    #@5f
    .line 203
    .restart local v0       #builder:Lcom/android/vcard/VCardBuilder;
    .restart local v2       #needCharset:Z
    .restart local v3       #vcardType:I
    :cond_5f
    const-string v4, "FN"

    #@61
    invoke-virtual {v0, v4, p2, v6, v6}, Lcom/android/vcard/VCardBuilder;->appendLine(Ljava/lang/String;Ljava/lang/String;ZZ)V

    #@64
    .line 204
    const-string v4, "N"

    #@66
    invoke-virtual {v0, v4, p2, v6, v6}, Lcom/android/vcard/VCardBuilder;->appendLine(Ljava/lang/String;Ljava/lang/String;ZZ)V

    #@69
    goto :goto_29
.end method

.method public createOneEntry(Z)Ljava/lang/String;
    .registers 4
    .parameter "vcardVer21"

    #@0
    .prologue
    .line 129
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@6
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_12

    #@c
    .line 130
    :cond_c
    const-string v0, "The vCard composer object is not correctly initialized"

    #@e
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mErrorReason:Ljava/lang/String;

    #@10
    .line 131
    const/4 v0, 0x0

    #@11
    .line 134
    :goto_11
    return-object v0

    #@12
    :cond_12
    :try_start_12
    invoke-direct {p0, p1}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->createOneCallLogEntryInternal(Z)Ljava/lang/String;
    :try_end_15
    .catchall {:try_start_12 .. :try_end_15} :catchall_1c

    #@15
    move-result-object v0

    #@16
    .line 136
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@18
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@1b
    goto :goto_11

    #@1c
    :catchall_1c
    move-exception v0

    #@1d
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@1f
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@22
    throw v0
.end method

.method public finalize()V
    .registers 2

    #@0
    .prologue
    .line 296
    iget-boolean v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mTerminateIsCalled:Z

    #@2
    if-nez v0, :cond_7

    #@4
    .line 297
    invoke-virtual {p0}, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->terminate()V

    #@7
    .line 299
    :cond_7
    return-void
.end method

.method public getCount()I
    .registers 2

    #@0
    .prologue
    .line 302
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 303
    const/4 v0, 0x0

    #@5
    .line 305
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@8
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public getErrorReason()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 316
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mErrorReason:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method public init(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Z
    .registers 14
    .parameter "contentUri"
    .parameter "selection"
    .parameter "selectionArgs"
    .parameter "sortOrder"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    .line 98
    sget-object v0, Landroid/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@4
    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_22

    #@a
    .line 99
    sget-object v2, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->sCallLogProjection:[Ljava/lang/String;

    #@c
    .line 105
    .local v2, projection:[Ljava/lang/String;
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mContentResolver:Landroid/content/ContentResolver;

    #@e
    move-object v1, p1

    #@f
    move-object v3, p2

    #@10
    move-object v4, p3

    #@11
    move-object v5, p4

    #@12
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@15
    move-result-object v0

    #@16
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@18
    .line 108
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@1a
    if-nez v0, :cond_28

    #@1c
    .line 109
    const-string v0, "Failed to get database information"

    #@1e
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mErrorReason:Ljava/lang/String;

    #@20
    move v0, v7

    #@21
    .line 125
    .end local v2           #projection:[Ljava/lang/String;
    :goto_21
    return v0

    #@22
    .line 101
    :cond_22
    const-string v0, "The Uri vCard composer received is not supported by the composer."

    #@24
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mErrorReason:Ljava/lang/String;

    #@26
    move v0, v7

    #@27
    .line 102
    goto :goto_21

    #@28
    .line 113
    .restart local v2       #projection:[Ljava/lang/String;
    :cond_28
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@2a
    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    #@2d
    move-result v0

    #@2e
    if-eqz v0, :cond_38

    #@30
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@32
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    #@35
    move-result v0

    #@36
    if-nez v0, :cond_6f

    #@38
    .line 115
    :cond_38
    :try_start_38
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@3a
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_3d
    .catchall {:try_start_38 .. :try_end_3d} :catchall_67
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_38 .. :try_end_3d} :catch_45

    #@3d
    .line 119
    const-string v0, "There\'s no exportable in the database"

    #@3f
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mErrorReason:Ljava/lang/String;

    #@41
    .line 120
    :goto_41
    iput-object v8, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@43
    move v0, v7

    #@44
    .line 122
    goto :goto_21

    #@45
    .line 116
    :catch_45
    move-exception v6

    #@46
    .line 117
    .local v6, e:Landroid/database/sqlite/SQLiteException;
    :try_start_46
    const-string v0, "CallLogComposer"

    #@48
    new-instance v1, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v3, "SQLiteException on Cursor#close(): "

    #@4f
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v1

    #@53
    invoke-virtual {v6}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    #@56
    move-result-object v3

    #@57
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v1

    #@5b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v1

    #@5f
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_62
    .catchall {:try_start_46 .. :try_end_62} :catchall_67

    #@62
    .line 119
    const-string v0, "There\'s no exportable in the database"

    #@64
    iput-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mErrorReason:Ljava/lang/String;

    #@66
    goto :goto_41

    #@67
    .end local v6           #e:Landroid/database/sqlite/SQLiteException;
    :catchall_67
    move-exception v0

    #@68
    const-string v1, "There\'s no exportable in the database"

    #@6a
    iput-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mErrorReason:Ljava/lang/String;

    #@6c
    .line 120
    iput-object v8, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@6e
    .line 119
    throw v0

    #@6f
    .line 125
    :cond_6f
    const/4 v0, 0x1

    #@70
    goto :goto_21
.end method

.method public isAfterLast()Z
    .registers 2

    #@0
    .prologue
    .line 309
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@2
    if-nez v0, :cond_6

    #@4
    .line 310
    const/4 v0, 0x0

    #@5
    .line 312
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@8
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public terminate()V
    .registers 5

    #@0
    .prologue
    .line 282
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@2
    if-eqz v1, :cond_c

    #@4
    .line 284
    :try_start_4
    iget-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@6
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_9
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_9} :catch_10

    #@9
    .line 288
    :goto_9
    const/4 v1, 0x0

    #@a
    iput-object v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mCursor:Landroid/database/Cursor;

    #@c
    .line 291
    :cond_c
    const/4 v1, 0x1

    #@d
    iput-boolean v1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapCallLogComposer;->mTerminateIsCalled:Z

    #@f
    .line 292
    return-void

    #@10
    .line 285
    :catch_10
    move-exception v0

    #@11
    .line 286
    .local v0, e:Landroid/database/sqlite/SQLiteException;
    const-string v1, "CallLogComposer"

    #@13
    new-instance v2, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v3, "SQLiteException on Cursor#close(): "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_9
.end method
