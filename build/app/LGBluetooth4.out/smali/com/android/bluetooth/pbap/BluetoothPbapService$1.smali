.class Lcom/android/bluetooth/pbap/BluetoothPbapService$1;
.super Landroid/os/Handler;
.source "BluetoothPbapService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/pbap/BluetoothPbapService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 668
    iput-object p1, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    .line 672
    const-string v2, "BluetoothPbapService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v4, "Handler(): got msg="

    #@9
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    iget v4, p1, Landroid/os/Message;->what:I

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 675
    iget v2, p1, Landroid/os/Message;->what:I

    #@1c
    sparse-switch v2, :sswitch_data_ae

    #@1f
    .line 715
    :goto_1f
    return-void

    #@20
    .line 677
    :sswitch_20
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@22
    invoke-static {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1000(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/bluetooth/BluetoothAdapter;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@29
    move-result v2

    #@2a
    if-eqz v2, :cond_32

    #@2c
    .line 678
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@2e
    invoke-static {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1100(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V

    #@31
    goto :goto_1f

    #@32
    .line 680
    :cond_32
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@34
    invoke-static {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$300(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V

    #@37
    goto :goto_1f

    #@38
    .line 684
    :sswitch_38
    new-instance v1, Landroid/content/Intent;

    #@3a
    const-string v2, "android.bluetooth.device.action.CONNECTION_ACCESS_CANCEL"

    #@3c
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3f
    .line 685
    .local v1, intent:Landroid/content/Intent;
    const-string v2, "com.android.settings"

    #@41
    const-string v3, "com.android.settings.bluetooth.BluetoothPermissionRequest"

    #@43
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@46
    .line 686
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@48
    invoke-virtual {v2, v1}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sendBroadcast(Landroid/content/Intent;)V

    #@4b
    .line 687
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@4d
    const/4 v3, 0x0

    #@4e
    invoke-static {v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$802(Lcom/android/bluetooth/pbap/BluetoothPbapService;Z)Z

    #@51
    .line 688
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@53
    invoke-static {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1200(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V

    #@56
    goto :goto_1f

    #@57
    .line 691
    .end local v1           #intent:Landroid/content/Intent;
    :sswitch_57
    new-instance v0, Landroid/content/Intent;

    #@59
    const-string v2, "com.android.bluetooth.pbap.userconfirmtimeout"

    #@5b
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@5e
    .line 692
    .local v0, i:Landroid/content/Intent;
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@60
    invoke-virtual {v2, v0}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->sendBroadcast(Landroid/content/Intent;)V

    #@63
    .line 693
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@65
    const v3, -0xf4242

    #@68
    invoke-static {v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1300(Lcom/android/bluetooth/pbap/BluetoothPbapService;I)V

    #@6b
    .line 694
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@6d
    invoke-static {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1400(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V

    #@70
    goto :goto_1f

    #@71
    .line 697
    .end local v0           #i:Landroid/content/Intent;
    :sswitch_71
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@73
    invoke-static {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1200(Lcom/android/bluetooth/pbap/BluetoothPbapService;)V

    #@76
    goto :goto_1f

    #@77
    .line 700
    :sswitch_77
    const-string v2, "BluetoothPbapService"

    #@79
    const-string v3, "[BTUI] ### PBAP Connected ###"

    #@7b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7e
    goto :goto_1f

    #@7f
    .line 703
    :sswitch_7f
    const-string v2, "BluetoothPbapService"

    #@81
    const-string v3, "[BTUI] ### PBAP Disconnected ###"

    #@83
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@86
    goto :goto_1f

    #@87
    .line 707
    :sswitch_87
    const-string v2, "BluetoothPbapService"

    #@89
    const-string v3, "[BTUI] MSG_OBEX_AUTH_CHALL : Obex Auth"

    #@8b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 708
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@90
    const-string v3, "com.android.bluetooth.pbap.authchall"

    #@92
    invoke-static {v2, v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$1500(Lcom/android/bluetooth/pbap/BluetoothPbapService;Ljava/lang/String;)V

    #@95
    .line 709
    iget-object v2, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@97
    invoke-static {v2}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$900(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/os/Handler;

    #@9a
    move-result-object v2

    #@9b
    iget-object v3, p0, Lcom/android/bluetooth/pbap/BluetoothPbapService$1;->this$0:Lcom/android/bluetooth/pbap/BluetoothPbapService;

    #@9d
    invoke-static {v3}, Lcom/android/bluetooth/pbap/BluetoothPbapService;->access$900(Lcom/android/bluetooth/pbap/BluetoothPbapService;)Landroid/os/Handler;

    #@a0
    move-result-object v3

    #@a1
    const/4 v4, 0x3

    #@a2
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@a5
    move-result-object v3

    #@a6
    const-wide/16 v4, 0x7530

    #@a8
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@ab
    goto/16 :goto_1f

    #@ad
    .line 675
    nop

    #@ae
    :sswitch_data_ae
    .sparse-switch
        0x1 -> :sswitch_20
        0x2 -> :sswitch_38
        0x3 -> :sswitch_57
        0x1388 -> :sswitch_71
        0x1389 -> :sswitch_77
        0x138a -> :sswitch_7f
        0x138b -> :sswitch_87
    .end sparse-switch
.end method
