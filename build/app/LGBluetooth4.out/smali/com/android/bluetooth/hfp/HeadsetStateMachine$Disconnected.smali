.class Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;
.super Lcom/android/internal/util/State;
.source "HeadsetStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hfp/HeadsetStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Disconnected"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 319
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 319
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@3
    return-void
.end method

.method private processConnectionEvent(ILandroid/bluetooth/BluetoothDevice;)V
    .registers 8
    .parameter "state"
    .parameter "device"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    const/4 v4, 0x2

    #@2
    .line 404
    packed-switch p1, :pswitch_data_148

    #@5
    .line 461
    :pswitch_5
    const-string v1, "HeadsetStateMachine"

    #@7
    new-instance v2, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v3, "Incorrect state: "

    #@e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v2

    #@12
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 464
    :cond_1d
    :goto_1d
    return-void

    #@1e
    .line 406
    :pswitch_1e
    const-string v1, "HeadsetStateMachine"

    #@20
    new-instance v2, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "Ignore HF DISCONNECTED event, device: "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v2

    #@33
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    goto :goto_1d

    #@37
    .line 409
    :pswitch_37
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@39
    invoke-virtual {v1, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->okToConnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@3c
    move-result v1

    #@3d
    if-eqz v1, :cond_64

    #@3f
    .line 410
    const-string v1, "HeadsetStateMachine"

    #@41
    const-string v2, "Incoming Hf accepted"

    #@43
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 411
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@48
    const/4 v2, 0x1

    #@49
    invoke-static {v1, p2, v2, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@4c
    .line 413
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@4e
    monitor-enter v2

    #@4f
    .line 414
    :try_start_4f
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@51
    invoke-static {v1, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1102(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@54
    .line 415
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@56
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@58
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;

    #@5b
    move-result-object v3

    #@5c
    invoke-static {v1, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V

    #@5f
    .line 416
    monitor-exit v2

    #@60
    goto :goto_1d

    #@61
    :catchall_61
    move-exception v1

    #@62
    monitor-exit v2
    :try_end_63
    .catchall {:try_start_4f .. :try_end_63} :catchall_61

    #@63
    throw v1

    #@64
    .line 418
    :cond_64
    const-string v1, "HeadsetStateMachine"

    #@66
    new-instance v2, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v3, "Incoming Hf rejected. priority="

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@73
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetService;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3, p2}, Lcom/android/bluetooth/hfp/HeadsetService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@7a
    move-result v3

    #@7b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v2

    #@7f
    const-string v3, " bondState="

    #@81
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v2

    #@85
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    #@88
    move-result v3

    #@89
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v2

    #@8d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@90
    move-result-object v2

    #@91
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@94
    .line 421
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@96
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@98
    invoke-static {v2, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)[B

    #@9b
    move-result-object v2

    #@9c
    invoke-static {v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;[B)Z

    #@9f
    .line 423
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@a2
    move-result-object v0

    #@a3
    .line 424
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_1d

    #@a5
    .line 425
    invoke-virtual {v0, p2, v4}, Lcom/android/bluetooth/btservice/AdapterService;->connectOtherProfile(Landroid/bluetooth/BluetoothDevice;I)V

    #@a8
    goto/16 :goto_1d

    #@aa
    .line 431
    .end local v0           #adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    :pswitch_aa
    const-string v1, "HeadsetStateMachine"

    #@ac
    const-string v2, "HFP Connected from Disconnected state"

    #@ae
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@b1
    .line 432
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@b3
    invoke-virtual {v1, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->okToConnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b6
    move-result v1

    #@b7
    if-eqz v1, :cond_e8

    #@b9
    .line 433
    const-string v1, "HeadsetStateMachine"

    #@bb
    const-string v2, "Incoming Hf accepted"

    #@bd
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c0
    .line 434
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@c2
    invoke-static {v1, p2, v4, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@c5
    .line 436
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@c7
    monitor-enter v2

    #@c8
    .line 437
    :try_start_c8
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@ca
    invoke-static {v1, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$902(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@cd
    .line 439
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@cf
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@d2
    .line 441
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@d4
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@d6
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;

    #@d9
    move-result-object v3

    #@da
    invoke-static {v1, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V

    #@dd
    .line 442
    monitor-exit v2
    :try_end_de
    .catchall {:try_start_c8 .. :try_end_de} :catchall_e5

    #@de
    .line 443
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@e0
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@e3
    goto/16 :goto_1d

    #@e5
    .line 442
    :catchall_e5
    move-exception v1

    #@e6
    :try_start_e6
    monitor-exit v2
    :try_end_e7
    .catchall {:try_start_e6 .. :try_end_e7} :catchall_e5

    #@e7
    throw v1

    #@e8
    .line 446
    :cond_e8
    const-string v1, "HeadsetStateMachine"

    #@ea
    new-instance v2, Ljava/lang/StringBuilder;

    #@ec
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ef
    const-string v3, "Incoming Hf rejected. priority="

    #@f1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f4
    move-result-object v2

    #@f5
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@f7
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetService;

    #@fa
    move-result-object v3

    #@fb
    invoke-virtual {v3, p2}, Lcom/android/bluetooth/hfp/HeadsetService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@fe
    move-result v3

    #@ff
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@102
    move-result-object v2

    #@103
    const-string v3, " bondState="

    #@105
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@108
    move-result-object v2

    #@109
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    #@10c
    move-result v3

    #@10d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@110
    move-result-object v2

    #@111
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@114
    move-result-object v2

    #@115
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@118
    .line 448
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@11a
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@11c
    invoke-static {v2, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)[B

    #@11f
    move-result-object v2

    #@120
    invoke-static {v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;[B)Z

    #@123
    .line 450
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@126
    move-result-object v0

    #@127
    .line 451
    .restart local v0       #adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_1d

    #@129
    .line 452
    invoke-virtual {v0, p2, v4}, Lcom/android/bluetooth/btservice/AdapterService;->connectOtherProfile(Landroid/bluetooth/BluetoothDevice;I)V

    #@12c
    goto/16 :goto_1d

    #@12e
    .line 458
    .end local v0           #adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    :pswitch_12e
    const-string v1, "HeadsetStateMachine"

    #@130
    new-instance v2, Ljava/lang/StringBuilder;

    #@132
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@135
    const-string v3, "Ignore HF DISCONNECTING event, device: "

    #@137
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v2

    #@13b
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v2

    #@13f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@142
    move-result-object v2

    #@143
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@146
    goto/16 :goto_1d

    #@148
    .line 404
    :pswitch_data_148
    .packed-switch 0x0
        :pswitch_1e
        :pswitch_37
        :pswitch_aa
        :pswitch_5
        :pswitch_12e
    .end packed-switch
.end method


# virtual methods
.method public enter()V
    .registers 4

    #@0
    .prologue
    .line 322
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Enter Disconnected: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@f
    invoke-static {v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/os/Message;

    #@12
    move-result-object v2

    #@13
    iget v2, v2, Landroid/os/Message;->what:I

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V

    #@20
    .line 323
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@22
    invoke-static {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/AtPhonebook;

    #@25
    move-result-object v0

    #@26
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/AtPhonebook;->resetAtState()V

    #@29
    .line 324
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2b
    invoke-static {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@2e
    move-result-object v0

    #@2f
    const/4 v1, 0x0

    #@30
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->listenForPhoneState(Z)V

    #@33
    .line 326
    const-string v0, "HeadsetStateMachine"

    #@35
    const-string v1, "[BTUI] change sampling rate to 8000 when device is disconnected"

    #@37
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3a
    .line 327
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@3c
    invoke-static {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/media/AudioManager;

    #@3f
    move-result-object v0

    #@40
    const-string v1, "bt_samplerate=8000"

    #@42
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@45
    .line 329
    return-void
.end method

.method public exit()V
    .registers 4

    #@0
    .prologue
    .line 399
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Exit Disconnected: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@f
    invoke-static {v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/os/Message;

    #@12
    move-result-object v2

    #@13
    iget v2, v2, Landroid/os/Message;->what:I

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V

    #@20
    .line 400
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 10
    .parameter "message"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 333
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@4
    new-instance v6, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v7, "Disconnected process message: "

    #@b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v6

    #@f
    iget v7, p1, Landroid/os/Message;->what:I

    #@11
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v6

    #@19
    invoke-static {v3, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V

    #@1c
    .line 334
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@1e
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@21
    move-result-object v3

    #@22
    if-nez v3, :cond_34

    #@24
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@26
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@29
    move-result-object v3

    #@2a
    if-nez v3, :cond_34

    #@2c
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2e
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@31
    move-result-object v3

    #@32
    if-eqz v3, :cond_3c

    #@34
    .line 335
    :cond_34
    const-string v3, "HeadsetStateMachine"

    #@36
    const-string v4, "ERROR: current, target, or mIncomingDevice not null in Disconnected"

    #@38
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 394
    :goto_3b
    return v5

    #@3c
    .line 339
    :cond_3c
    const/4 v2, 0x1

    #@3d
    .line 340
    .local v2, retValue:Z
    iget v3, p1, Landroid/os/Message;->what:I

    #@3f
    sparse-switch v3, :sswitch_data_f6

    #@42
    goto :goto_3b

    #@43
    .line 342
    :sswitch_43
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@45
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@47
    .line 343
    .local v0, device:Landroid/bluetooth/BluetoothDevice;
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@49
    invoke-static {v3, v0, v4, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@4c
    .line 346
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@4e
    iget-object v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@50
    invoke-static {v6, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)[B

    #@53
    move-result-object v6

    #@54
    invoke-static {v3, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;[B)Z

    #@57
    move-result v3

    #@58
    if-nez v3, :cond_61

    #@5a
    .line 347
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@5c
    invoke-static {v3, v0, v5, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@5f
    .end local v0           #device:Landroid/bluetooth/BluetoothDevice;
    :cond_5f
    :goto_5f
    :sswitch_5f
    move v5, v2

    #@60
    .line 394
    goto :goto_3b

    #@61
    .line 352
    .restart local v0       #device:Landroid/bluetooth/BluetoothDevice;
    :cond_61
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@63
    monitor-enter v4

    #@64
    .line 353
    :try_start_64
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@66
    invoke-static {v3, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1002(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@69
    .line 354
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@6b
    iget-object v5, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@6d
    invoke-static {v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;

    #@70
    move-result-object v5

    #@71
    invoke-static {v3, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V

    #@74
    .line 355
    monitor-exit v4
    :try_end_75
    .catchall {:try_start_64 .. :try_end_75} :catchall_7f

    #@75
    .line 358
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@77
    const/16 v4, 0xc9

    #@79
    const-wide/16 v5, 0x7530

    #@7b
    invoke-virtual {v3, v4, v5, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessageDelayed(IJ)V

    #@7e
    goto :goto_5f

    #@7f
    .line 355
    :catchall_7f
    move-exception v3

    #@80
    :try_start_80
    monitor-exit v4
    :try_end_81
    .catchall {:try_start_80 .. :try_end_81} :catchall_7f

    #@81
    throw v3

    #@82
    .line 364
    .end local v0           #device:Landroid/bluetooth/BluetoothDevice;
    :sswitch_82
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@84
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@86
    check-cast v3, Landroid/content/Intent;

    #@88
    invoke-static {v4, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/content/Intent;)V

    #@8b
    goto :goto_5f

    #@8c
    .line 367
    :sswitch_8c
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@8e
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@90
    check-cast v3, Ljava/lang/Boolean;

    #@92
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    #@95
    move-result v3

    #@96
    invoke-static {v4, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Z)V

    #@99
    goto :goto_5f

    #@9a
    .line 370
    :sswitch_9a
    iget-object v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9c
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9e
    check-cast v3, Lcom/android/bluetooth/hfp/HeadsetCallState;

    #@a0
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@a2
    if-ne v7, v4, :cond_a8

    #@a4
    :goto_a4
    invoke-static {v6, v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetCallState;Z)V

    #@a7
    goto :goto_5f

    #@a8
    :cond_a8
    move v4, v5

    #@a9
    goto :goto_a4

    #@aa
    .line 374
    :sswitch_aa
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@ac
    check-cast v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@ae
    .line 376
    .local v1, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@b0
    new-instance v4, Ljava/lang/StringBuilder;

    #@b2
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@b5
    const-string v5, "event type: "

    #@b7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v4

    #@bb
    iget v5, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->type:I

    #@bd
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v4

    #@c1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c4
    move-result-object v4

    #@c5
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V

    #@c8
    .line 378
    iget v3, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->type:I

    #@ca
    packed-switch v3, :pswitch_data_110

    #@cd
    .line 387
    const-string v3, "HeadsetStateMachine"

    #@cf
    new-instance v4, Ljava/lang/StringBuilder;

    #@d1
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d4
    const-string v5, "Unexpected stack event: "

    #@d6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v4

    #@da
    iget v5, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->type:I

    #@dc
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@df
    move-result-object v4

    #@e0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e3
    move-result-object v4

    #@e4
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e7
    goto/16 :goto_5f

    #@e9
    .line 381
    :pswitch_e9
    iget-object v3, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->device:Landroid/bluetooth/BluetoothDevice;

    #@eb
    if-eqz v3, :cond_5f

    #@ed
    .line 382
    iget v3, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@ef
    iget-object v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->device:Landroid/bluetooth/BluetoothDevice;

    #@f1
    invoke-direct {p0, v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;->processConnectionEvent(ILandroid/bluetooth/BluetoothDevice;)V

    #@f4
    goto/16 :goto_5f

    #@f6
    .line 340
    :sswitch_data_f6
    .sparse-switch
        0x1 -> :sswitch_43
        0x2 -> :sswitch_5f
        0x9 -> :sswitch_9a
        0xa -> :sswitch_82
        0xc -> :sswitch_8c
        0x65 -> :sswitch_aa
    .end sparse-switch

    #@110
    .line 378
    :pswitch_data_110
    .packed-switch 0x1
        :pswitch_e9
    .end packed-switch
.end method
