.class final Lcom/android/bluetooth/hfp/HeadsetStateMachine;
.super Lcom/android/internal/util/StateMachine;
.source "HeadsetStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;,
        Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;,
        Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;,
        Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;,
        Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;
    }
.end annotation


# static fields
.field private static final BTUI_SUPPORT_MEMORY_DIAL:Z = true

.field static final CALL_STATE_CHANGED:I = 0x9

.field static final CONNECT:I = 0x1

.field static final CONNECT_AUDIO:I = 0x3

.field private static final CONNECT_TIMEOUT:I = 0xc9

.field private static final DBG:Z = true

.field static final DEVICE_STATE_CHANGED:I = 0xb

.field private static final DIALING_OUT_TIMEOUT:I = 0x66

.field private static final DIALING_OUT_TIMEOUT_VALUE:I = 0x2710

.field static final DISCONNECT:I = 0x2

.field static final DISCONNECT_AUDIO:I = 0x4

.field private static final EVENT_TYPE_ANSWER_CALL:I = 0x4

.field private static final EVENT_TYPE_AT_CHLD:I = 0xa

.field private static final EVENT_TYPE_AT_CIND:I = 0xc

.field private static final EVENT_TYPE_AT_CLCC:I = 0xe

.field private static final EVENT_TYPE_AT_COPS:I = 0xd

.field private static final EVENT_TYPE_AUDIO_STATE_CHANGED:I = 0x2

.field private static final EVENT_TYPE_BRSF:I = 0x12

.field private static final EVENT_TYPE_CONNECTION_STATE_CHANGED:I = 0x1

.field private static final EVENT_TYPE_DIAL_CALL:I = 0x7

.field private static final EVENT_TYPE_HANGUP_CALL:I = 0x5

.field private static final EVENT_TYPE_KEY_PRESSED:I = 0x10

.field private static final EVENT_TYPE_NOICE_REDUCTION:I = 0x9

.field private static final EVENT_TYPE_NONE:I = 0x0

.field private static final EVENT_TYPE_SEND_DTMF:I = 0x8

.field private static final EVENT_TYPE_SUBSCRIBER_NUMBER_REQUEST:I = 0xb

.field private static final EVENT_TYPE_UNKNOWN_AT:I = 0xf

.field private static final EVENT_TYPE_VOLUME_CHANGED:I = 0x6

.field private static final EVENT_TYPE_VR_STATE_CHANGED:I = 0x3

.field private static final EVENT_TYPE_WBS:I = 0x11

.field private static final HEADSET_NAME:Ljava/lang/String; = "bt_headset_name"

.field private static final HEADSET_NREC:Ljava/lang/String; = "bt_headset_nrec"

.field private static final HEADSET_SAMPLERATE:Ljava/lang/String; = "bt_samplerate"

.field private static final HEADSET_UUIDS:[Landroid/os/ParcelUuid; = null

.field private static final HEADSET_WBS:Ljava/lang/String; = "bt_headset_wbs"

.field static final INTENT_BATTERY_CHANGED:I = 0xa

.field static final INTENT_SCO_VOLUME_CHANGED:I = 0x7

#the value of this static final field might be set in the static constructor
.field public static final LG_BTUI_SIG:Z = false

.field static final ROAM_CHANGED:I = 0xc

.field private static final SCHEME_TEL:Ljava/lang/String; = "tel"

.field static final SEND_CCLC_RESPONSE:I = 0xd

.field static final SET_MIC_VOLUME:I = 0x8

.field private static final STACK_EVENT:I = 0x65

.field private static final START_VR_TIMEOUT:I = 0x67

.field private static final START_VR_TIMEOUT_VALUE:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "HeadsetStateMachine"

.field static final VIRTUAL_CALL_START:I = 0xe

.field static final VIRTUAL_CALL_STOP:I = 0xf

.field static final VOICE_RECOGNITION_START:I = 0x5

.field static final VOICE_RECOGNITION_STOP:I = 0x6

.field private static mDtmfPlay:Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;

.field private static sRefCount:I

.field private static sVoiceCommandIntent:Landroid/content/Intent;


# instance fields
.field private final BTA_AG_FEAT_VREC:I

.field private final BTA_HF_FEAT_VREC:I

.field private VR_SUPPORT_AG:Z

.field private VR_SUPPORT_HF:Z

.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mAudioManager:Landroid/media/AudioManager;

.field private mAudioOn:Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

.field private mAudioState:I

.field private mConnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;

.field private mConnection:Landroid/content/ServiceConnection;

.field private mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

.field private mDialingOut:Z

.field private mDisconnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;

.field private mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

.field private mIsA2dpSuspended:Z

.field private mIsPeerDeviceInBlackList:Z

.field private mNativeAvailable:Z

.field private mPending:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;

.field private mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

.field private mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

.field private mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

.field private mPowerManager:Landroid/os/PowerManager;

.field private mService:Lcom/android/bluetooth/hfp/HeadsetService;

.field private mStartVoiceRecognitionWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mTargetDevice:Landroid/bluetooth/BluetoothDevice;

.field private mVirtualCallStarted:Z

.field private mVoiceRecognitionStarted:Z

.field private mWaitingForVoiceRecognition:Z


# direct methods
.method static constructor <clinit>()V
    .registers 3

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 112
    const-string v0, "persist.service.btui.sig"

    #@3
    const-string v1, ""

    #@5
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@8
    move-result-object v0

    #@9
    const-string v1, "1"

    #@b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    sput-boolean v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->LG_BTUI_SIG:Z

    #@11
    .line 127
    sput v2, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sRefCount:I

    #@13
    .line 161
    const/4 v0, 0x2

    #@14
    new-array v0, v0, [Landroid/os/ParcelUuid;

    #@16
    sget-object v1, Landroid/bluetooth/BluetoothUuid;->HSP:Landroid/os/ParcelUuid;

    #@18
    aput-object v1, v0, v2

    #@1a
    const/4 v1, 0x1

    #@1b
    sget-object v2, Landroid/bluetooth/BluetoothUuid;->Handsfree:Landroid/os/ParcelUuid;

    #@1d
    aput-object v2, v0, v1

    #@1f
    sput-object v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->HEADSET_UUIDS:[Landroid/os/ParcelUuid;

    #@21
    .line 217
    invoke-static {}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->classInitNative()V

    #@24
    .line 218
    return-void
.end method

.method private constructor <init>(Lcom/android/bluetooth/hfp/HeadsetService;)V
    .registers 7
    .parameter "context"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    const/4 v2, 0x0

    #@3
    .line 221
    const-string v0, "HeadsetStateMachine"

    #@5
    invoke-direct {p0, v0}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    #@8
    .line 116
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsA2dpSuspended:Z

    #@a
    .line 120
    const/4 v0, 0x4

    #@b
    iput v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BTA_AG_FEAT_VREC:I

    #@d
    .line 121
    const/16 v0, 0x8

    #@f
    iput v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BTA_HF_FEAT_VREC:I

    #@11
    .line 122
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->VR_SUPPORT_AG:Z

    #@13
    .line 123
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->VR_SUPPORT_HF:Z

    #@15
    .line 173
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVirtualCallStarted:Z

    #@17
    .line 174
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@19
    .line 175
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@1b
    .line 178
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDialingOut:Z

    #@1d
    .line 212
    iput-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@1f
    .line 213
    iput-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@21
    .line 214
    iput-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

    #@23
    .line 1192
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;

    #@25
    invoke-direct {v0, p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@28
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mConnection:Landroid/content/ServiceConnection;

    #@2a
    .line 1904
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsPeerDeviceInBlackList:Z

    #@2c
    .line 222
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@2e
    .line 223
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@30
    .line 224
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@32
    .line 226
    const-string v0, "power"

    #@34
    invoke-virtual {p1, v0}, Lcom/android/bluetooth/hfp/HeadsetService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@37
    move-result-object v0

    #@38
    check-cast v0, Landroid/os/PowerManager;

    #@3a
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPowerManager:Landroid/os/PowerManager;

    #@3c
    .line 227
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPowerManager:Landroid/os/PowerManager;

    #@3e
    const-string v1, "HeadsetStateMachine:VoiceRecognition"

    #@40
    invoke-virtual {v0, v4, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@43
    move-result-object v0

    #@44
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mStartVoiceRecognitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@46
    .line 229
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mStartVoiceRecognitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@48
    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    #@4b
    .line 231
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDialingOut:Z

    #@4d
    .line 232
    const-string v0, "audio"

    #@4f
    invoke-virtual {p1, v0}, Lcom/android/bluetooth/hfp/HeadsetService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@52
    move-result-object v0

    #@53
    check-cast v0, Landroid/media/AudioManager;

    #@55
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@57
    .line 233
    new-instance v0, Lcom/android/bluetooth/hfp/AtPhonebook;

    #@59
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@5b
    invoke-direct {v0, v1, p0}, Lcom/android/bluetooth/hfp/AtPhonebook;-><init>(Landroid/content/Context;Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@5e
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@60
    .line 234
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@62
    invoke-direct {v0, p1, p0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;-><init>(Landroid/content/Context;Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@65
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@67
    .line 235
    const/16 v0, 0xa

    #@69
    iput v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioState:I

    #@6b
    .line 236
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@6e
    move-result-object v0

    #@6f
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@71
    .line 237
    new-instance v0, Landroid/content/Intent;

    #@73
    const-class v1, Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@75
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@78
    move-result-object v1

    #@79
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7c
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mConnection:Landroid/content/ServiceConnection;

    #@7e
    invoke-virtual {p1, v0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    #@81
    move-result v0

    #@82
    if-nez v0, :cond_8b

    #@84
    .line 239
    const-string v0, "HeadsetStateMachine"

    #@86
    const-string v1, "Could not bind to Bluetooth Headset Phone Service"

    #@88
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8b
    .line 242
    :cond_8b
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->initializeNative()V

    #@8e
    .line 243
    iput-boolean v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mNativeAvailable:Z

    #@90
    .line 245
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;

    #@92
    invoke-direct {v0, p0, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@95
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDisconnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;

    #@97
    .line 246
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;

    #@99
    invoke-direct {v0, p0, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@9c
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPending:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;

    #@9e
    .line 247
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;

    #@a0
    invoke-direct {v0, p0, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@a3
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mConnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;

    #@a5
    .line 248
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

    #@a7
    invoke-direct {v0, p0, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@aa
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioOn:Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

    #@ac
    .line 250
    sget-object v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sVoiceCommandIntent:Landroid/content/Intent;

    #@ae
    if-nez v0, :cond_da

    #@b0
    .line 252
    const-string v0, "KR"

    #@b2
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->getCountry()Ljava/lang/String;

    #@b5
    move-result-object v1

    #@b6
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b9
    move-result v0

    #@ba
    if-eqz v0, :cond_fb

    #@bc
    .line 253
    const-string v0, "HeadsetStateMachine"

    #@be
    const-string v1, "[KR] Qvoice"

    #@c0
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    .line 254
    new-instance v0, Landroid/content/Intent;

    #@c5
    const-string v1, "android.intent.action.VOICE_COMMAND"

    #@c7
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@ca
    sput-object v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sVoiceCommandIntent:Landroid/content/Intent;

    #@cc
    .line 263
    :goto_cc
    sget-object v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sVoiceCommandIntent:Landroid/content/Intent;

    #@ce
    const/high16 v1, 0x1000

    #@d0
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@d3
    .line 265
    sget-object v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sVoiceCommandIntent:Landroid/content/Intent;

    #@d5
    const/high16 v1, 0x4

    #@d7
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@da
    .line 269
    :cond_da
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDisconnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;

    #@dc
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@df
    .line 270
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPending:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;

    #@e1
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@e4
    .line 271
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mConnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;

    #@e6
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@e9
    .line 272
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioOn:Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

    #@eb
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@ee
    .line 274
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDisconnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;

    #@f0
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    #@f3
    .line 276
    new-instance v0, Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;

    #@f5
    invoke-direct {v0, p1}, Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;-><init>(Landroid/content/Context;)V

    #@f8
    sput-object v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDtmfPlay:Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;

    #@fa
    .line 278
    return-void

    #@fb
    .line 256
    :cond_fb
    const-string v0, "HeadsetStateMachine"

    #@fd
    const-string v1, "[Not KR] Voice Dialer"

    #@ff
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@102
    .line 257
    new-instance v0, Landroid/content/Intent;

    #@104
    const-string v1, "android.intent.action.MIC_VOICE_COMMAND"

    #@106
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@109
    sput-object v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sVoiceCommandIntent:Landroid/content/Intent;

    #@10b
    goto :goto_cc
.end method

.method private BtUiLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 2681
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    if-eqz v0, :cond_11

    #@4
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@6
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_11

    #@c
    .line 2682
    const-string v0, "HeadsetStateMachine"

    #@e
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 2684
    :cond_11
    return-void
.end method

.method static synthetic access$1000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$1002(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$1100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$1102(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$1200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;II)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)[B
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->connectHfpNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPending:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;

    #@2
    return-object v0
.end method

.method static synthetic access$1600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/content/Intent;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processIntentBatteryChanged(Landroid/content/Intent;)V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processRoamChanged(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetCallState;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processCallState(Lcom/android/bluetooth/hfp/HeadsetCallState;Z)V

    #@3
    return-void
.end method

.method static synthetic access$2000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetService;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@2
    return-object v0
.end method

.method static synthetic access$2300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->disconnectHfpNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->checkIsCurrentInBlackList()V

    #@3
    return-void
.end method

.method static synthetic access$2500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mConnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;

    #@2
    return-object v0
.end method

.method static synthetic access$2600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$2700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->configAudioParameters()V

    #@3
    return-void
.end method

.method static synthetic access$2800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$2900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$3000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I[B)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->onConnectionStateChanged(I[B)V

    #@3
    return-void
.end method

.method static synthetic access$3100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$3200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->removeMessages(I)V

    #@3
    return-void
.end method

.method static synthetic access$3300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->removeMessages(I)V

    #@3
    return-void
.end method

.method static synthetic access$3400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processBrsf(II)V

    #@3
    return-void
.end method

.method static synthetic access$3502(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsPeerDeviceInBlackList:Z

    #@2
    return p1
.end method

.method static synthetic access$3600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDisconnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;

    #@2
    return-object v0
.end method

.method static synthetic access$3700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$3900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$4000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$4600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$4800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->connectAudioNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$4900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processLocalVrEvent(I)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$5000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetDeviceState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processDeviceStateChanged(Lcom/android/bluetooth/hfp/HeadsetDeviceState;)V

    #@3
    return-void
.end method

.method static synthetic access$5100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetClccResponse;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processSendClccResponse(Lcom/android/bluetooth/hfp/HeadsetClccResponse;)V

    #@3
    return-void
.end method

.method static synthetic access$5200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDialingOut:Z

    #@2
    return v0
.end method

.method static synthetic access$5202(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDialingOut:Z

    #@2
    return p1
.end method

.method static synthetic access$5300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@2
    return v0
.end method

.method static synthetic access$5302(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    iput-boolean p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@2
    return p1
.end method

.method static synthetic access$5400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processVrEvent(I)V

    #@3
    return-void
.end method

.method static synthetic access$5500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processAnswerCall()V

    #@3
    return-void
.end method

.method static synthetic access$5600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processHangupCall()V

    #@3
    return-void
.end method

.method static synthetic access$5700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processVolumeEvent(II)V

    #@3
    return-void
.end method

.method static synthetic access$5800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processDialCall(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$5900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processSendDtmf(I)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/AtPhonebook;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@2
    return-object v0
.end method

.method static synthetic access$6000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processNoiceReductionEvent(I)V

    #@3
    return-void
.end method

.method static synthetic access$6100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processWBSEvent(I)V

    #@3
    return-void
.end method

.method static synthetic access$6200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processAtChld(I)V

    #@3
    return-void
.end method

.method static synthetic access$6300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processSubscriberNumberRequest()V

    #@3
    return-void
.end method

.method static synthetic access$6400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processAtCind()V

    #@3
    return-void
.end method

.method static synthetic access$6500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processAtCops()V

    #@3
    return-void
.end method

.method static synthetic access$6600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processAtClcc()V

    #@3
    return-void
.end method

.method static synthetic access$6700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processUnknownAt(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$6800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processKeyPressed()V

    #@3
    return-void
.end method

.method static synthetic access$6900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetPhoneState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@2
    return-object v0
.end method

.method static synthetic access$7000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioState:I

    #@2
    return v0
.end method

.method static synthetic access$7002(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    iput p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioState:I

    #@2
    return p1
.end method

.method static synthetic access$7100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->broadcastAudioState(Landroid/bluetooth/BluetoothDevice;II)V

    #@3
    return-void
.end method

.method static synthetic access$7200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioOn:Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

    #@2
    return-object v0
.end method

.method static synthetic access$7300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$7400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/IBluetoothHeadsetPhone;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@2
    return-object v0
.end method

.method static synthetic access$7402(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/IBluetoothHeadsetPhone;)Landroid/bluetooth/IBluetoothHeadsetPhone;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@2
    return-object p1
.end method

.method static synthetic access$7500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$7600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method static synthetic access$7700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->disconnectAudioNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$7800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$7900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/media/AudioManager;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@2
    return-object v0
.end method

.method static synthetic access$8000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;II)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 99
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->setVolumeNative(II)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object v0
.end method

.method static synthetic access$902(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 99
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object p1
.end method

.method private broadcastAudioState(Landroid/bluetooth/BluetoothDevice;II)V
    .registers 7
    .parameter "device"
    .parameter "newState"
    .parameter "prevState"

    #@0
    .prologue
    .line 1470
    const/16 v1, 0xc

    #@2
    if-ne p3, v1, :cond_7

    #@4
    .line 1473
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->terminateScoUsingVirtualVoiceCall()Z

    #@7
    .line 1475
    :cond_7
    new-instance v0, Landroid/content/Intent;

    #@9
    const-string v1, "android.bluetooth.headset.profile.action.AUDIO_STATE_CHANGED"

    #@b
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@e
    .line 1476
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    #@10
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@13
    .line 1477
    const-string v1, "android.bluetooth.profile.extra.STATE"

    #@15
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@18
    .line 1478
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    #@1a
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@1d
    .line 1479
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@1f
    const-string v2, "android.permission.BLUETOOTH"

    #@21
    invoke-virtual {v1, v0, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@24
    .line 1480
    new-instance v1, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v2, "Audio state "

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v1

    #@33
    const-string v2, ": "

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, "->"

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@4e
    .line 1481
    return-void
.end method

.method private broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;II)V
    .registers 10
    .parameter "device"
    .parameter "newState"
    .parameter "prevState"

    #@0
    .prologue
    const/4 v3, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    .line 1441
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Connection state "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ": "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "->"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@2c
    .line 1442
    if-ne p3, v3, :cond_31

    #@2e
    .line 1444
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->terminateScoUsingVirtualVoiceCall()Z

    #@31
    .line 1450
    :cond_31
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@33
    invoke-virtual {v1, p1, v5, p2, p3}, Lcom/android/bluetooth/hfp/HeadsetService;->notifyProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V

    #@36
    .line 1452
    new-instance v0, Landroid/content/Intent;

    #@38
    const-string v1, "android.bluetooth.headset.profile.action.CONNECTION_STATE_CHANGED"

    #@3a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3d
    .line 1453
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    #@3f
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@42
    .line 1454
    const-string v1, "android.bluetooth.profile.extra.STATE"

    #@44
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@47
    .line 1455
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    #@49
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@4c
    .line 1456
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@4e
    const-string v2, "android.permission.BLUETOOTH"

    #@50
    invoke-virtual {v1, v0, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@53
    .line 1459
    if-ne p2, v3, :cond_67

    #@55
    .line 1460
    invoke-static {}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getDefaultChecker()Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@58
    move-result-object v1

    #@59
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@5b
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    #@62
    move-result-object v4

    #@63
    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->setConnected(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Z

    #@66
    .line 1467
    :cond_66
    :goto_66
    return-void

    #@67
    .line 1462
    :cond_67
    if-nez p2, :cond_66

    #@69
    .line 1463
    invoke-static {}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getDefaultChecker()Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@6c
    move-result-object v1

    #@6d
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@6f
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@72
    move-result-object v3

    #@73
    invoke-virtual {v1, v2, v5, v3}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->setDisconnected(Landroid/content/Context;ILjava/lang/String;)Z

    #@76
    goto :goto_66
.end method

.method private broadcastVendorSpecificEventIntent(Ljava/lang/String;II[Ljava/lang/Object;Landroid/bluetooth/BluetoothDevice;)V
    .registers 9
    .parameter "command"
    .parameter "companyId"
    .parameter "commandType"
    .parameter "arguments"
    .parameter "device"

    #@0
    .prologue
    .line 1491
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "broadcastVendorSpecificEventIntent("

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    const-string v2, ")"

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@1c
    .line 1492
    new-instance v0, Landroid/content/Intent;

    #@1e
    const-string v1, "android.bluetooth.headset.action.VENDOR_SPECIFIC_HEADSET_EVENT"

    #@20
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@23
    .line 1494
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_CMD"

    #@25
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@28
    .line 1495
    const-string v1, "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_CMD_TYPE"

    #@2a
    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@2d
    .line 1498
    const-string v1, "android.bluetooth.headset.extra.VENDOR_SPECIFIC_HEADSET_EVENT_ARGS"

    #@2f
    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    #@32
    .line 1499
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    #@34
    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@37
    .line 1501
    new-instance v1, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    const-string v2, "android.bluetooth.headset.intent.category.companyid."

    #@3e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v1

    #@42
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@45
    move-result-object v2

    #@46
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v1

    #@4a
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v1

    #@4e
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@51
    .line 1504
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@53
    const-string v2, "android.permission.BLUETOOTH"

    #@55
    invoke-virtual {v1, v0, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@58
    .line 1505
    return-void
.end method

.method private callstate_to_callsetup(I)I
    .registers 4
    .parameter "call_state"

    #@0
    .prologue
    .line 2052
    const/4 v0, 0x0

    #@1
    .line 2053
    .local v0, call_setup:I
    const/4 v1, 0x4

    #@2
    if-ne p1, v1, :cond_5

    #@4
    .line 2054
    const/4 v0, 0x1

    #@5
    .line 2056
    :cond_5
    const/4 v1, 0x2

    #@6
    if-ne p1, v1, :cond_9

    #@8
    .line 2057
    const/4 v0, 0x2

    #@9
    .line 2059
    :cond_9
    const/4 v1, 0x3

    #@a
    if-ne p1, v1, :cond_d

    #@c
    .line 2060
    const/4 v0, 0x3

    #@d
    .line 2063
    :cond_d
    return v0
.end method

.method private checkIsCurrentInBlackList()V
    .registers 7

    #@0
    .prologue
    const/16 v5, 0x9

    #@2
    const/4 v3, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    .line 1907
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsPeerDeviceInBlackList:Z

    #@6
    .line 1908
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@8
    if-nez v4, :cond_31

    #@a
    .line 1909
    const-string v2, "HeadsetStateMachine"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "checkIsCurrentInBlackList, mCurrentDevice : "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, ", isBlackList? "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    iget-boolean v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsPeerDeviceInBlackList:Z

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 1925
    :goto_30
    return-void

    #@31
    .line 1912
    :cond_31
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@33
    invoke-virtual {v4}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@36
    move-result-object v1

    #@37
    .line 1913
    .local v1, deviceName:Ljava/lang/String;
    if-nez v1, :cond_60

    #@39
    .line 1914
    const-string v2, "HeadsetStateMachine"

    #@3b
    new-instance v3, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v4, "checkIsCurrentInBlackList, mCurrentDevice : "

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    const-string v4, ", isBlackList? "

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    iget-boolean v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsPeerDeviceInBlackList:Z

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    goto :goto_30

    #@60
    .line 1917
    :cond_60
    const-string v4, "94:44:44"

    #@62
    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@65
    move-result v4

    #@66
    if-eqz v4, :cond_9e

    #@68
    .line 1918
    invoke-virtual {v1, v5}, Ljava/lang/String;->charAt(I)C

    #@6b
    move-result v0

    #@6c
    .line 1919
    .local v0, c:C
    const/16 v4, 0x35

    #@6e
    if-eq v0, v4, :cond_74

    #@70
    const/16 v4, 0x31

    #@72
    if-ne v0, v4, :cond_75

    #@74
    :cond_74
    move v2, v3

    #@75
    :cond_75
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsPeerDeviceInBlackList:Z

    #@77
    .line 1924
    .end local v0           #c:C
    :cond_77
    :goto_77
    const-string v2, "HeadsetStateMachine"

    #@79
    new-instance v3, Ljava/lang/StringBuilder;

    #@7b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7e
    const-string v4, "checkIsCurrentInBlackList, mCurrentDevice : "

    #@80
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v3

    #@84
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@86
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v3

    #@8a
    const-string v4, ", isBlackList? "

    #@8c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8f
    move-result-object v3

    #@90
    iget-boolean v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsPeerDeviceInBlackList:Z

    #@92
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@95
    move-result-object v3

    #@96
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v3

    #@9a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9d
    goto :goto_30

    #@9e
    .line 1920
    :cond_9e
    const-string v4, "00:1E:B2"

    #@a0
    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@a3
    move-result v4

    #@a4
    if-eqz v4, :cond_77

    #@a6
    .line 1921
    invoke-virtual {v1, v5}, Ljava/lang/String;->charAt(I)C

    #@a9
    move-result v0

    #@aa
    .line 1922
    .restart local v0       #c:C
    const/16 v4, 0x37

    #@ac
    if-ne v0, v4, :cond_b1

    #@ae
    :goto_ae
    iput-boolean v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsPeerDeviceInBlackList:Z

    #@b0
    goto :goto_77

    #@b1
    :cond_b1
    move v3, v2

    #@b2
    goto :goto_ae
.end method

.method private native cindResponseNative(IIIIIII)Z
.end method

.method private static native classInitNative()V
.end method

.method private native clccResponseNative(IIIIZLjava/lang/String;I)Z
.end method

.method private native cleanupNative()V
.end method

.method private configAudioParameters()V
    .registers 4

    #@0
    .prologue
    .line 1510
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "bt_headset_name="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentDeviceName()Ljava/lang/String;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ";"

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, "bt_headset_nrec"

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    const-string v2, "=on"

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@2e
    .line 1512
    return-void
.end method

.method private native configureWBSNative([BI)Z
.end method

.method private native connectAudioNative([B)Z
.end method

.method private native connectHfpNative([B)Z
.end method

.method private native copsResponseNative(Ljava/lang/String;)Z
.end method

.method private native disconnectAudioNative([B)Z
.end method

.method private native disconnectHfpNative([B)Z
.end method

.method private declared-synchronized expectVoiceRecognition()V
    .registers 4

    #@0
    .prologue
    .line 1411
    monitor-enter p0

    #@1
    const/4 v0, 0x1

    #@2
    :try_start_2
    iput-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@4
    .line 1412
    const/16 v0, 0x67

    #@6
    const-wide/16 v1, 0x1388

    #@8
    invoke-virtual {p0, v0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessageDelayed(IJ)V

    #@b
    .line 1413
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mStartVoiceRecognitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@d
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@10
    move-result v0

    #@11
    if-nez v0, :cond_1a

    #@13
    .line 1414
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mStartVoiceRecognitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@15
    const-wide/16 v1, 0x1388

    #@17
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager$WakeLock;->acquire(J)V
    :try_end_1a
    .catchall {:try_start_2 .. :try_end_1a} :catchall_1c

    #@1a
    .line 1416
    :cond_1a
    monitor-exit p0

    #@1b
    return-void

    #@1c
    .line 1411
    :catchall_1c
    move-exception v0

    #@1d
    monitor-exit p0

    #@1e
    throw v0
.end method

.method private static findChar(CLjava/lang/String;I)I
    .registers 7
    .parameter "ch"
    .parameter "input"
    .parameter "fromIndex"

    #@0
    .prologue
    const/16 v3, 0x22

    #@2
    .line 2155
    move v1, p2

    #@3
    .local v1, i:I
    :goto_3
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@6
    move-result v2

    #@7
    if-ge v1, v2, :cond_24

    #@9
    .line 2156
    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    #@c
    move-result v0

    #@d
    .line 2157
    .local v0, c:C
    if-ne v0, v3, :cond_1d

    #@f
    .line 2158
    add-int/lit8 v2, v1, 0x1

    #@11
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->indexOf(II)I

    #@14
    move-result v1

    #@15
    .line 2159
    const/4 v2, -0x1

    #@16
    if-ne v1, v2, :cond_21

    #@18
    .line 2160
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@1b
    move-result v2

    #@1c
    .line 2166
    .end local v0           #c:C
    :goto_1c
    return v2

    #@1d
    .line 2162
    .restart local v0       #c:C
    :cond_1d
    if-ne v0, p0, :cond_21

    #@1f
    move v2, v1

    #@20
    .line 2163
    goto :goto_1c

    #@21
    .line 2155
    :cond_21
    add-int/lit8 v1, v1, 0x1

    #@23
    goto :goto_3

    #@24
    .line 2166
    .end local v0           #c:C
    :cond_24
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@27
    move-result v2

    #@28
    goto :goto_1c
.end method

.method private static generateArgs(Ljava/lang/String;)[Ljava/lang/Object;
    .registers 7
    .parameter "input"

    #@0
    .prologue
    .line 2175
    const/4 v2, 0x0

    #@1
    .line 2177
    .local v2, i:I
    new-instance v4, Ljava/util/ArrayList;

    #@3
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    #@6
    .line 2178
    .local v4, out:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :goto_6
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@9
    move-result v5

    #@a
    if-gt v2, v5, :cond_26

    #@c
    .line 2179
    const/16 v5, 0x2c

    #@e
    invoke-static {v5, p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->findChar(CLjava/lang/String;I)I

    #@11
    move-result v3

    #@12
    .line 2181
    .local v3, j:I
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 2183
    .local v0, arg:Ljava/lang/String;
    :try_start_16
    new-instance v5, Ljava/lang/Integer;

    #@18
    invoke-direct {v5, v0}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    #@1b
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1e
    .catch Ljava/lang/NumberFormatException; {:try_start_16 .. :try_end_1e} :catch_21

    #@1e
    .line 2188
    :goto_1e
    add-int/lit8 v2, v3, 0x1

    #@20
    .line 2189
    goto :goto_6

    #@21
    .line 2184
    :catch_21
    move-exception v1

    #@22
    .line 2185
    .local v1, e:Ljava/lang/NumberFormatException;
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@25
    goto :goto_1e

    #@26
    .line 2190
    .end local v0           #arg:Ljava/lang/String;
    .end local v1           #e:Ljava/lang/NumberFormatException;
    .end local v3           #j:I
    :cond_26
    invoke-virtual {v4}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    #@29
    move-result-object v5

    #@2a
    return-object v5
.end method

.method private getAtCommandType(Ljava/lang/String;)I
    .registers 6
    .parameter "atCommand"

    #@0
    .prologue
    const/4 v3, 0x5

    #@1
    .line 1540
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@3
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@6
    const/4 v1, -0x1

    #@7
    .line 1541
    .local v1, commandType:I
    const/4 v0, 0x0

    #@8
    .line 1542
    .local v0, atString:Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@b
    move-result-object p1

    #@c
    .line 1543
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@f
    move-result v2

    #@10
    if-le v2, v3, :cond_24

    #@12
    .line 1545
    invoke-virtual {p1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    .line 1546
    const-string v2, "?"

    #@18
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1b
    move-result v2

    #@1c
    if-eqz v2, :cond_25

    #@1e
    .line 1547
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@20
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@23
    const/4 v1, 0x0

    #@24
    .line 1556
    :cond_24
    :goto_24
    return v1

    #@25
    .line 1548
    :cond_25
    const-string v2, "=?"

    #@27
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_34

    #@2d
    .line 1549
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@2f
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@32
    const/4 v1, 0x2

    #@33
    goto :goto_24

    #@34
    .line 1550
    :cond_34
    const-string v2, "="

    #@36
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@39
    move-result v2

    #@3a
    if-eqz v2, :cond_43

    #@3c
    .line 1551
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@3e
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@41
    const/4 v1, 0x1

    #@42
    goto :goto_24

    #@43
    .line 1553
    :cond_43
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@45
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@48
    const/4 v1, -0x1

    #@49
    goto :goto_24
.end method

.method private getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 2524
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method private getCurrentDeviceName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 2512
    const-string v0, "<unknown>"

    #@2
    .line 2513
    .local v0, defaultName:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@4
    if-nez v2, :cond_7

    #@6
    .line 2520
    .end local v0           #defaultName:Ljava/lang/String;
    :cond_6
    :goto_6
    return-object v0

    #@7
    .line 2516
    .restart local v0       #defaultName:Ljava/lang/String;
    :cond_7
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@9
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    #@c
    move-result-object v1

    #@d
    .line 2517
    .local v1, deviceName:Ljava/lang/String;
    if-eqz v1, :cond_6

    #@f
    move-object v0, v1

    #@10
    .line 2520
    goto :goto_6
.end method

.method private getDevice([B)Landroid/bluetooth/BluetoothDevice;
    .registers 4
    .parameter "address"

    #@0
    .prologue
    .line 2528
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method private native initializeNative()V
.end method

.method private isInCall()Z
    .registers 3

    #@0
    .prologue
    .line 2532
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@2
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getNumActiveCall()I

    #@5
    move-result v0

    #@6
    if-gtz v0, :cond_19

    #@8
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@a
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getNumHeldCall()I

    #@d
    move-result v0

    #@e
    if-gtz v0, :cond_19

    #@10
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@12
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getCallState()I

    #@15
    move-result v0

    #@16
    const/4 v1, 0x6

    #@17
    if-eq v0, v1, :cond_1b

    #@19
    :cond_19
    const/4 v0, 0x1

    #@1a
    :goto_1a
    return v0

    #@1b
    :cond_1b
    const/4 v0, 0x0

    #@1c
    goto :goto_1a
.end method

.method private isInCallNeedSCO()Z
    .registers 3

    #@0
    .prologue
    .line 2539
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@2
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getNumActiveCall()I

    #@5
    move-result v0

    #@6
    if-gtz v0, :cond_22

    #@8
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@a
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getNumHeldCall()I

    #@d
    move-result v0

    #@e
    if-gtz v0, :cond_22

    #@10
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@12
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getCallState()I

    #@15
    move-result v0

    #@16
    const/4 v1, 0x6

    #@17
    if-eq v0, v1, :cond_24

    #@19
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@1b
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getCallState()I

    #@1e
    move-result v0

    #@1f
    const/4 v1, 0x4

    #@20
    if-eq v0, v1, :cond_24

    #@22
    :cond_22
    const/4 v0, 0x1

    #@23
    :goto_23
    return v0

    #@24
    :cond_24
    const/4 v0, 0x0

    #@25
    goto :goto_23
.end method

.method private isLTEVTSupported()Z
    .registers 9

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 1749
    iget-object v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@4
    invoke-virtual {v6}, Lcom/android/bluetooth/hfp/HeadsetService;->getApplicationContext()Landroid/content/Context;

    #@7
    move-result-object v6

    #@8
    invoke-virtual {v6}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@b
    move-result-object v3

    #@c
    .line 1752
    .local v3, pm:Landroid/content/pm/PackageManager;
    :try_start_c
    const-string v6, "com.lge.vt"

    #@e
    const/4 v7, 0x0

    #@f
    invoke-virtual {v3, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_12
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_c .. :try_end_12} :catch_28
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_12} :catch_14

    #@12
    move-result-object v1

    #@13
    .line 1767
    :goto_13
    return v4

    #@14
    .line 1756
    :catch_14
    move-exception v0

    #@15
    .line 1757
    .local v0, e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@18
    .line 1760
    .end local v0           #e:Ljava/lang/Exception;
    :goto_18
    :try_start_18
    const-string v6, "com.lge.ltecall"

    #@1a
    const/4 v7, 0x0

    #@1b
    invoke-virtual {v3, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_18 .. :try_end_1e} :catch_26
    .catch Ljava/lang/Exception; {:try_start_18 .. :try_end_1e} :catch_20

    #@1e
    move-result-object v2

    #@1f
    .line 1761
    .local v2, packageInfo2:Landroid/content/pm/PackageInfo;
    goto :goto_13

    #@20
    .line 1764
    .end local v2           #packageInfo2:Landroid/content/pm/PackageInfo;
    :catch_20
    move-exception v0

    #@21
    .line 1765
    .restart local v0       #e:Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@24
    .end local v0           #e:Ljava/lang/Exception;
    :goto_24
    move v4, v5

    #@25
    .line 1767
    goto :goto_13

    #@26
    .line 1762
    :catch_26
    move-exception v4

    #@27
    goto :goto_24

    #@28
    .line 1754
    :catch_28
    move-exception v6

    #@29
    goto :goto_18
.end method

.method private isVirtualCallInProgress()Z
    .registers 2

    #@0
    .prologue
    .line 1561
    iget-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVirtualCallStarted:Z

    #@2
    return v0
.end method

.method private log(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 2575
    const-string v0, "HeadsetStateMachine"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 2577
    return-void
.end method

.method static make(Lcom/android/bluetooth/hfp/HeadsetService;)Lcom/android/bluetooth/hfp/HeadsetStateMachine;
    .registers 4
    .parameter "context"

    #@0
    .prologue
    .line 281
    const-string v1, "HeadsetStateMachine"

    #@2
    const-string v2, "make"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 282
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9
    invoke-direct {v0, p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;-><init>(Lcom/android/bluetooth/hfp/HeadsetService;)V

    #@c
    .line 283
    .local v0, hssm:Lcom/android/bluetooth/hfp/HeadsetStateMachine;
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->start()V

    #@f
    .line 284
    return-object v0
.end method

.method private memoryDial(I)Ljava/lang/String;
    .registers 14
    .parameter "index"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v7, 0x0

    #@2
    .line 1811
    const-string v0, "KR"

    #@4
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->getCountry()Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    if-eqz v0, :cond_2c

    #@e
    .line 1812
    const/16 v0, 0x3e7

    #@10
    if-gt p1, v0, :cond_14

    #@12
    if-ge p1, v3, :cond_4a

    #@14
    .line 1813
    :cond_14
    new-instance v0, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v2, "[BTUI] [MemDial] index error = "

    #@1b
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@2a
    move-object v11, v7

    #@2b
    .line 1865
    :cond_2b
    :goto_2b
    return-object v11

    #@2c
    .line 1819
    :cond_2c
    const/16 v0, 0x63

    #@2e
    if-gt p1, v0, :cond_32

    #@30
    if-ge p1, v3, :cond_4a

    #@32
    .line 1820
    :cond_32
    new-instance v0, Ljava/lang/StringBuilder;

    #@34
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@37
    const-string v2, "[BTUI] [MemDial] index error = "

    #@39
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v0

    #@3d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44
    move-result-object v0

    #@45
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@48
    move-object v11, v7

    #@49
    .line 1821
    goto :goto_2b

    #@4a
    .line 1825
    :cond_4a
    sget-object v0, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    #@4c
    const-string v2, "speed_dials"

    #@4e
    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@51
    move-result-object v1

    #@52
    .line 1828
    .local v1, uri:Landroid/net/Uri;
    int-to-long v9, p1

    #@53
    .line 1831
    .local v9, indexForQuery:J
    new-instance v0, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v2, "[BTUI] [MemDial] rank = "

    #@5a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v0

    #@5e
    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@61
    move-result-object v0

    #@62
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@65
    move-result-object v0

    #@66
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@69
    .line 1833
    const/4 v6, 0x0

    #@6a
    .line 1835
    .local v6, cursor:Landroid/database/Cursor;
    :try_start_6a
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@6c
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetService;->getContentResolver()Landroid/content/ContentResolver;

    #@6f
    move-result-object v0

    #@70
    const/4 v2, 0x1

    #@71
    new-array v2, v2, [Ljava/lang/String;

    #@73
    const/4 v3, 0x0

    #@74
    const-string v4, "phone"

    #@76
    aput-object v4, v2, v3

    #@78
    new-instance v3, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v4, "rank = "

    #@7f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v3

    #@83
    invoke-virtual {v3, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@86
    move-result-object v3

    #@87
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8a
    move-result-object v3

    #@8b
    const/4 v4, 0x0

    #@8c
    const/4 v5, 0x0

    #@8d
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@90
    move-result-object v6

    #@91
    .line 1837
    if-eqz v6, :cond_ce

    #@93
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    #@96
    move-result v0

    #@97
    if-eqz v0, :cond_ce

    #@99
    .line 1838
    const/4 v0, 0x0

    #@9a
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@9d
    move-result-object v11

    #@9e
    .line 1839
    .local v11, number:Ljava/lang/String;
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    #@a1
    move-result v0

    #@a2
    if-lez v0, :cond_c1

    #@a4
    .line 1840
    new-instance v0, Ljava/lang/StringBuilder;

    #@a6
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@a9
    const-string v2, "[BTUI] [MemDial] OK... Outgoing call => "

    #@ab
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v0

    #@af
    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v0

    #@b3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b6
    move-result-object v0

    #@b7
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V
    :try_end_ba
    .catchall {:try_start_6a .. :try_end_ba} :catchall_100
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6a .. :try_end_ba} :catch_db

    #@ba
    .line 1864
    if-eqz v6, :cond_2b

    #@bc
    .line 1865
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@bf
    goto/16 :goto_2b

    #@c1
    .line 1843
    :cond_c1
    :try_start_c1
    const-string v0, "[BTUI] [MemDial] length error..."

    #@c3
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V
    :try_end_c6
    .catchall {:try_start_c1 .. :try_end_c6} :catchall_100
    .catch Ljava/lang/IllegalArgumentException; {:try_start_c1 .. :try_end_c6} :catch_db

    #@c6
    .line 1864
    if-eqz v6, :cond_cb

    #@c8
    .line 1865
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@cb
    :cond_cb
    move-object v11, v7

    #@cc
    goto/16 :goto_2b

    #@ce
    .line 1847
    .end local v11           #number:Ljava/lang/String;
    :cond_ce
    :try_start_ce
    const-string v0, "[BTUI] [MemDial] not exist..."

    #@d0
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V
    :try_end_d3
    .catchall {:try_start_ce .. :try_end_d3} :catchall_100
    .catch Ljava/lang/IllegalArgumentException; {:try_start_ce .. :try_end_d3} :catch_db

    #@d3
    .line 1864
    if-eqz v6, :cond_d8

    #@d5
    .line 1865
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@d8
    :cond_d8
    move-object v11, v7

    #@d9
    goto/16 :goto_2b

    #@db
    .line 1850
    :catch_db
    move-exception v8

    #@dc
    .line 1851
    .local v8, iae:Ljava/lang/IllegalArgumentException;
    :try_start_dc
    const-string v0, "[BTUI] [MemDial] IllegalArgumentException handled..."

    #@de
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@e1
    .line 1853
    sget-boolean v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->LG_BTUI_SIG:Z

    #@e3
    if-eqz v0, :cond_f8

    #@e5
    .line 1855
    const/4 v7, 0x0

    #@e6
    .line 1856
    .local v7, dialNumber:Ljava/lang/String;
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@e8
    if-eqz v0, :cond_f0

    #@ea
    .line 1857
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@ec
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/AtPhonebook;->getLastDialledNumber()Ljava/lang/String;
    :try_end_ef
    .catchall {:try_start_dc .. :try_end_ef} :catchall_100

    #@ef
    move-result-object v7

    #@f0
    .line 1864
    :cond_f0
    if-eqz v6, :cond_f5

    #@f2
    .line 1865
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@f5
    :cond_f5
    move-object v11, v7

    #@f6
    goto/16 :goto_2b

    #@f8
    .line 1864
    .end local v7           #dialNumber:Ljava/lang/String;
    :cond_f8
    if-eqz v6, :cond_fd

    #@fa
    .line 1865
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@fd
    :cond_fd
    move-object v11, v7

    #@fe
    goto/16 :goto_2b

    #@100
    .line 1864
    .end local v8           #iae:Ljava/lang/IllegalArgumentException;
    :catchall_100
    move-exception v0

    #@101
    if-eqz v6, :cond_106

    #@103
    .line 1865
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@106
    :cond_106
    throw v0
.end method

.method private native notifyDeviceStatusNative(IIII)Z
.end method

.method private onAnswerCall()V
    .registers 4

    #@0
    .prologue
    .line 2307
    const-string v1, "[BTUI] [r] ATA"

    #@2
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 2308
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@7
    const/4 v1, 0x4

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@c
    .line 2309
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    const/16 v1, 0x65

    #@e
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@11
    .line 2310
    return-void
.end method

.method private onAtChld(I)V
    .registers 5
    .parameter "chld"

    #@0
    .prologue
    .line 2352
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "[BTUI] [r] AT+CHLD="

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@16
    .line 2353
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@18
    const/16 v1, 0xa

    #@1a
    const/4 v2, 0x0

    #@1b
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@1e
    .line 2354
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iput p1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@20
    .line 2355
    const/16 v1, 0x65

    #@22
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@25
    .line 2356
    return-void
.end method

.method private onAtCind()V
    .registers 4

    #@0
    .prologue
    .line 2365
    const-string v1, "[BTUI] [r] AT+CIND?"

    #@2
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 2366
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@7
    const/16 v1, 0xc

    #@9
    const/4 v2, 0x0

    #@a
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@d
    .line 2367
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    const/16 v1, 0x65

    #@f
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@12
    .line 2368
    return-void
.end method

.method private onAtClcc()V
    .registers 4

    #@0
    .prologue
    .line 2377
    const-string v1, "[BTUI] [r] AT+CLCC"

    #@2
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 2378
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@7
    const/16 v1, 0xe

    #@9
    const/4 v2, 0x0

    #@a
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@d
    .line 2379
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    const/16 v1, 0x65

    #@f
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@12
    .line 2380
    return-void
.end method

.method private onAtCnum()V
    .registers 4

    #@0
    .prologue
    .line 2359
    const-string v1, "[BTUI] [r] AT+CNUM"

    #@2
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 2360
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@7
    const/16 v1, 0xb

    #@9
    const/4 v2, 0x0

    #@a
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@d
    .line 2361
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    const/16 v1, 0x65

    #@f
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@12
    .line 2362
    return-void
.end method

.method private onAtCops()V
    .registers 4

    #@0
    .prologue
    .line 2371
    const-string v1, "[BTUI] [r] AT+COPS?"

    #@2
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 2372
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@7
    const/16 v1, 0xd

    #@9
    const/4 v2, 0x0

    #@a
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@d
    .line 2373
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    const/16 v1, 0x65

    #@f
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@12
    .line 2374
    return-void
.end method

.method private onAudioStateChanged(I[B)V
    .registers 6
    .parameter "state"
    .parameter "address"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    .line 2282
    if-ne p1, v2, :cond_1c

    #@3
    .line 2283
    const-string v1, "[BTUI] ### [SCO_CONNECTED]"

    #@5
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@8
    .line 2287
    :cond_8
    :goto_8
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@a
    const/4 v1, 0x0

    #@b
    invoke-direct {v0, p0, v2, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@e
    .line 2288
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iput p1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@10
    .line 2289
    invoke-direct {p0, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@13
    move-result-object v1

    #@14
    iput-object v1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->device:Landroid/bluetooth/BluetoothDevice;

    #@16
    .line 2290
    const/16 v1, 0x65

    #@18
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@1b
    .line 2291
    return-void

    #@1c
    .line 2284
    .end local v0           #event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    :cond_1c
    if-nez p1, :cond_8

    #@1e
    .line 2285
    const-string v1, "[BTUI] ### [SCO_DISCONNECTED]"

    #@20
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@23
    goto :goto_8
.end method

.method private onBrsf(II)V
    .registers 6
    .parameter "local_feature"
    .parameter "peer_feature"

    #@0
    .prologue
    .line 2404
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@2
    const/16 v1, 0x12

    #@4
    const/4 v2, 0x0

    #@5
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@8
    .line 2405
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iput p1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@a
    .line 2406
    iput p2, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt2:I

    #@c
    .line 2407
    const/16 v1, 0x65

    #@e
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@11
    .line 2408
    return-void
.end method

.method private onConnectionStateChanged(I[B)V
    .registers 6
    .parameter "state"
    .parameter "address"

    #@0
    .prologue
    .line 2275
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@2
    const/4 v1, 0x1

    #@3
    const/4 v2, 0x0

    #@4
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@7
    .line 2276
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iput p1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@9
    .line 2277
    invoke-direct {p0, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@c
    move-result-object v1

    #@d
    iput-object v1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->device:Landroid/bluetooth/BluetoothDevice;

    #@f
    .line 2278
    const/16 v1, 0x65

    #@11
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@14
    .line 2279
    return-void
.end method

.method private onDialCall(Ljava/lang/String;)V
    .registers 5
    .parameter "number"

    #@0
    .prologue
    .line 2331
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "[BTUI] [r] ATD="

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@16
    .line 2332
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@18
    const/4 v1, 0x7

    #@19
    const/4 v2, 0x0

    #@1a
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@1d
    .line 2333
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iput-object p1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueString:Ljava/lang/String;

    #@1f
    .line 2334
    const/16 v1, 0x65

    #@21
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@24
    .line 2335
    return-void
.end method

.method private onHangupCall()V
    .registers 4

    #@0
    .prologue
    .line 2313
    const-string v1, "[BTUI] [r] AT+CHUP"

    #@2
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 2314
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@7
    const/4 v1, 0x5

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@c
    .line 2315
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    const/16 v1, 0x65

    #@e
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@11
    .line 2316
    return-void
.end method

.method private onKeyPressed()V
    .registers 4

    #@0
    .prologue
    .line 2389
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@2
    const/16 v1, 0x10

    #@4
    const/4 v2, 0x0

    #@5
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@8
    .line 2390
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    const/16 v1, 0x65

    #@a
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@d
    .line 2391
    return-void
.end method

.method private onNoiceReductionEnable(Z)V
    .registers 5
    .parameter "enable"

    #@0
    .prologue
    .line 2345
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "[BTUI] [r] AT+NREC="

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v2

    #@b
    if-eqz p1, :cond_2d

    #@d
    const-string v1, "1"

    #@f
    :goto_f
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@1a
    .line 2346
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@1c
    const/16 v1, 0x9

    #@1e
    const/4 v2, 0x0

    #@1f
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@22
    .line 2347
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    if-eqz p1, :cond_30

    #@24
    const/4 v1, 0x1

    #@25
    :goto_25
    iput v1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@27
    .line 2348
    const/16 v1, 0x65

    #@29
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@2c
    .line 2349
    return-void

    #@2d
    .line 2345
    .end local v0           #event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    :cond_2d
    const-string v1, "0"

    #@2f
    goto :goto_f

    #@30
    .line 2347
    .restart local v0       #event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    :cond_30
    const/4 v1, 0x0

    #@31
    goto :goto_25
.end method

.method private onSendDtmf(I)V
    .registers 5
    .parameter "dtmf"

    #@0
    .prologue
    .line 2338
    new-instance v1, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v2, "[BTUI] [r] AT+VTS="

    #@7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v1

    #@f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@16
    .line 2339
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@18
    const/16 v1, 0x8

    #@1a
    const/4 v2, 0x0

    #@1b
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@1e
    .line 2340
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iput p1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@20
    .line 2341
    const/16 v1, 0x65

    #@22
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@25
    .line 2342
    return-void
.end method

.method private onUnknownAt(Ljava/lang/String;)V
    .registers 5
    .parameter "atString"

    #@0
    .prologue
    .line 2383
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@2
    const/16 v1, 0xf

    #@4
    const/4 v2, 0x0

    #@5
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@8
    .line 2384
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iput-object p1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueString:Ljava/lang/String;

    #@a
    .line 2385
    const/16 v1, 0x65

    #@c
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@f
    .line 2386
    return-void
.end method

.method private onVolumeChanged(II)V
    .registers 6
    .parameter "type"
    .parameter "volume"

    #@0
    .prologue
    .line 2319
    if-nez p1, :cond_29

    #@2
    .line 2320
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[BTUI] [r] AT+VGS="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@18
    .line 2324
    :cond_18
    :goto_18
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@1a
    const/4 v1, 0x6

    #@1b
    const/4 v2, 0x0

    #@1c
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@1f
    .line 2325
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iput p1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@21
    .line 2326
    iput p2, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt2:I

    #@23
    .line 2327
    const/16 v1, 0x65

    #@25
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@28
    .line 2328
    return-void

    #@29
    .line 2321
    .end local v0           #event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    :cond_29
    const/4 v1, 0x1

    #@2a
    if-ne p1, v1, :cond_18

    #@2c
    .line 2322
    new-instance v1, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v2, "[BTUI] [r] AT+VGM="

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v1

    #@3b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v1

    #@3f
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@42
    goto :goto_18
.end method

.method private onVrStateChanged(I)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2294
    new-instance v3, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v4, "[BTUI] [r] AT+BVRA="

    #@8
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v3

    #@c
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v3

    #@10
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v3

    #@14
    invoke-direct {p0, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@17
    .line 2297
    new-instance v0, Landroid/content/Intent;

    #@19
    const-string v3, "com.lge.bluetooth.CLOSE_AVR_ACTIVITY"

    #@1b
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1e
    .line 2298
    .local v0, csloseAVRIntent:Landroid/content/Intent;
    const-string v3, "com.lge.bluetooth.IS_AVR_STARTED"

    #@20
    if-ne p1, v2, :cond_3b

    #@22
    :goto_22
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@25
    .line 2299
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@27
    const-string v3, "android.permission.BLUETOOTH"

    #@29
    invoke-virtual {v2, v0, v3}, Lcom/android/bluetooth/hfp/HeadsetService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@2c
    .line 2301
    new-instance v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@2e
    const/4 v2, 0x3

    #@2f
    const/4 v3, 0x0

    #@30
    invoke-direct {v1, p0, v2, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@33
    .line 2302
    .local v1, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iput p1, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@35
    .line 2303
    const/16 v2, 0x65

    #@37
    invoke-virtual {p0, v2, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@3a
    .line 2304
    return-void

    #@3b
    .line 2298
    .end local v1           #event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    :cond_3b
    const/4 v2, 0x0

    #@3c
    goto :goto_22
.end method

.method private onWBS(I)V
    .registers 5
    .parameter "codec"

    #@0
    .prologue
    .line 2398
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@2
    const/16 v1, 0x11

    #@4
    const/4 v2, 0x0

    #@5
    invoke-direct {v0, p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;ILcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V

    #@8
    .line 2399
    .local v0, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iput p1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@a
    .line 2400
    const/16 v1, 0x65

    #@c
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@f
    .line 2401
    return-void
.end method

.method private parseUnknownAt(Ljava/lang/String;)Ljava/lang/String;
    .registers 9
    .parameter "atString"

    #@0
    .prologue
    const/16 v6, 0x22

    #@2
    .line 1516
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@7
    move-result v5

    #@8
    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    #@b
    .line 1517
    .local v0, atCommand:Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    #@c
    .line 1519
    .local v4, result:Ljava/lang/String;
    const/4 v2, 0x0

    #@d
    .local v2, i:I
    :goto_d
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@10
    move-result v5

    #@11
    if-ge v2, v5, :cond_30

    #@13
    .line 1520
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    #@16
    move-result v1

    #@17
    .line 1521
    .local v1, c:C
    if-ne v1, v6, :cond_42

    #@19
    .line 1522
    add-int/lit8 v5, v2, 0x1

    #@1b
    invoke-virtual {p1, v6, v5}, Ljava/lang/String;->indexOf(II)I

    #@1e
    move-result v3

    #@1f
    .line 1523
    .local v3, j:I
    const/4 v5, -0x1

    #@20
    if-ne v3, v5, :cond_35

    #@22
    .line 1524
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@25
    move-result v5

    #@26
    invoke-virtual {p1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@29
    move-result-object v5

    #@2a
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    .line 1525
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@30
    .line 1534
    .end local v1           #c:C
    .end local v3           #j:I
    :cond_30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v4

    #@34
    .line 1535
    return-object v4

    #@35
    .line 1528
    .restart local v1       #c:C
    .restart local v3       #j:I
    :cond_35
    add-int/lit8 v5, v3, 0x1

    #@37
    invoke-virtual {p1, v2, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    .line 1529
    move v2, v3

    #@3f
    .line 1519
    .end local v3           #j:I
    :cond_3f
    :goto_3f
    add-int/lit8 v2, v2, 0x1

    #@41
    goto :goto_d

    #@42
    .line 1530
    :cond_42
    const/16 v5, 0x20

    #@44
    if-eq v1, v5, :cond_3f

    #@46
    .line 1531
    invoke-static {v1}, Ljava/lang/Character;->toUpperCase(C)C

    #@49
    move-result v5

    #@4a
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@4d
    goto :goto_3f
.end method

.method private native phoneStateChangeNative(IIILjava/lang/String;I)Z
.end method

.method private processAnswerCall()V
    .registers 4

    #@0
    .prologue
    .line 1620
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@2
    if-eqz v1, :cond_1a

    #@4
    .line 1622
    :try_start_4
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@6
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothHeadsetPhone;->answerCall()Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_a

    #@9
    .line 1629
    :goto_9
    return-void

    #@a
    .line 1623
    :catch_a
    move-exception v0

    #@b
    .line 1624
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "HeadsetStateMachine"

    #@d
    new-instance v2, Ljava/lang/Throwable;

    #@f
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@12
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@15
    move-result-object v2

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@19
    goto :goto_9

    #@1a
    .line 1627
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_1a
    const-string v1, "HeadsetStateMachine"

    #@1c
    const-string v2, "Handsfree phone proxy null for answering call"

    #@1e
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    goto :goto_9
.end method

.method private processAtChld(I)V
    .registers 6
    .parameter "chld"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1982
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@3
    if-eqz v1, :cond_2c

    #@5
    .line 1984
    :try_start_5
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@7
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadsetPhone;->processChld(I)Z

    #@a
    move-result v1

    #@b
    if-eqz v1, :cond_13

    #@d
    .line 1985
    const/4 v1, 0x1

    #@e
    const/4 v2, 0x0

    #@f
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@12
    .line 1997
    :goto_12
    return-void

    #@13
    .line 1987
    :cond_13
    const/4 v1, 0x0

    #@14
    const/4 v2, 0x0

    #@15
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_18} :catch_19

    #@18
    goto :goto_12

    #@19
    .line 1989
    :catch_19
    move-exception v0

    #@1a
    .line 1990
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "HeadsetStateMachine"

    #@1c
    new-instance v2, Ljava/lang/Throwable;

    #@1e
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@21
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1991
    invoke-virtual {p0, v3, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@2b
    goto :goto_12

    #@2c
    .line 1994
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2c
    const-string v1, "HeadsetStateMachine"

    #@2e
    const-string v2, "Handsfree phone proxy null for At+Chld"

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 1995
    invoke-virtual {p0, v3, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@36
    goto :goto_12
.end method

.method private processAtCind()V
    .registers 9

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2024
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isVirtualCallInProgress()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_b0

    #@8
    .line 2025
    const/4 v2, 0x1

    #@9
    .line 2026
    .local v2, call:I
    const/4 v3, 0x0

    #@a
    .line 2034
    .local v3, call_setup:I
    :goto_a
    const-string v5, "HeadsetStateMachine"

    #@c
    new-instance v0, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v6, "[BTUI] +CIND="

    #@13
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v6

    #@17
    add-int v0, v2, v3

    #@19
    if-lez v0, :cond_be

    #@1b
    move v0, v1

    #@1c
    :goto_1c
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v0

    #@20
    const-string v6, ","

    #@22
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v0

    #@26
    iget-object v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@28
    invoke-virtual {v6}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getCallState()I

    #@2b
    move-result v6

    #@2c
    invoke-direct {p0, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->callstate_to_callsetup(I)I

    #@2f
    move-result v6

    #@30
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    const-string v6, ","

    #@36
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v0

    #@3a
    iget-object v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@3c
    invoke-virtual {v6}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getService()I

    #@3f
    move-result v6

    #@40
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@43
    move-result-object v0

    #@44
    const-string v6, ","

    #@46
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v0

    #@4a
    iget-object v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@4c
    invoke-virtual {v6}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getSignal()I

    #@4f
    move-result v6

    #@50
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v0

    #@54
    const-string v6, ","

    #@56
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v0

    #@5a
    iget-object v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@5c
    invoke-virtual {v6}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getRoam()I

    #@5f
    move-result v6

    #@60
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@63
    move-result-object v0

    #@64
    const-string v6, ","

    #@66
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v0

    #@6a
    iget-object v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@6c
    invoke-virtual {v6}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getBatteryCharge()I

    #@6f
    move-result v6

    #@70
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@73
    move-result-object v0

    #@74
    const-string v6, ","

    #@76
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v0

    #@7a
    if-nez v3, :cond_c1

    #@7c
    :goto_7c
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v0

    #@80
    const-string v1, " (call, callsetup, service, signal, roam, battchg, callheld)"

    #@82
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85
    move-result-object v0

    #@86
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@89
    move-result-object v0

    #@8a
    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8d
    .line 2044
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@8f
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getService()I

    #@92
    move-result v1

    #@93
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@95
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getCallState()I

    #@98
    move-result v4

    #@99
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@9b
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getSignal()I

    #@9e
    move-result v5

    #@9f
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@a1
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getRoam()I

    #@a4
    move-result v6

    #@a5
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@a7
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getBatteryCharge()I

    #@aa
    move-result v7

    #@ab
    move-object v0, p0

    #@ac
    invoke-direct/range {v0 .. v7}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->cindResponseNative(IIIIIII)Z

    #@af
    .line 2048
    return-void

    #@b0
    .line 2029
    .end local v2           #call:I
    .end local v3           #call_setup:I
    :cond_b0
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@b2
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getNumActiveCall()I

    #@b5
    move-result v2

    #@b6
    .line 2030
    .restart local v2       #call:I
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@b8
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getNumHeldCall()I

    #@bb
    move-result v3

    #@bc
    .restart local v3       #call_setup:I
    goto/16 :goto_a

    #@be
    :cond_be
    move v0, v4

    #@bf
    .line 2034
    goto/16 :goto_1c

    #@c1
    :cond_c1
    if-nez v2, :cond_c5

    #@c3
    const/4 v4, 0x2

    #@c4
    goto :goto_7c

    #@c5
    :cond_c5
    move v4, v1

    #@c6
    goto :goto_7c
.end method

.method private processAtClcc()V
    .registers 20

    #@0
    .prologue
    .line 2086
    move-object/from16 v0, p0

    #@2
    iget-object v1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@4
    if-eqz v1, :cond_69

    #@6
    .line 2088
    :try_start_6
    invoke-direct/range {p0 .. p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isVirtualCallInProgress()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_32

    #@c
    .line 2089
    const-string v7, ""
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_e} :catch_4b

    #@e
    .line 2090
    .local v7, phoneNumber:Ljava/lang/String;
    const/16 v8, 0x81

    #@10
    .line 2092
    .local v8, type:I
    :try_start_10
    move-object/from16 v0, p0

    #@12
    iget-object v1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@14
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothHeadsetPhone;->getSubscriberNumber()Ljava/lang/String;

    #@17
    move-result-object v7

    #@18
    .line 2093
    invoke-static {v7}, Landroid/telephony/PhoneNumberUtils;->toaFromString(Ljava/lang/String;)I
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_10 .. :try_end_1b} :catch_27

    #@1b
    move-result v8

    #@1c
    .line 2099
    :goto_1c
    const/4 v2, 0x1

    #@1d
    const/4 v3, 0x0

    #@1e
    const/4 v4, 0x0

    #@1f
    const/4 v5, 0x0

    #@20
    const/4 v6, 0x0

    #@21
    move-object/from16 v1, p0

    #@23
    :try_start_23
    invoke-direct/range {v1 .. v8}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->clccResponseNative(IIIIZLjava/lang/String;I)Z

    #@26
    .line 2112
    .end local v7           #phoneNumber:Ljava/lang/String;
    .end local v8           #type:I
    :cond_26
    :goto_26
    return-void

    #@27
    .line 2094
    .restart local v7       #phoneNumber:Ljava/lang/String;
    .restart local v8       #type:I
    :catch_27
    move-exception v18

    #@28
    .line 2095
    .local v18, ee:Landroid/os/RemoteException;
    const-string v1, "HeadsetStateMachine"

    #@2a
    const-string v2, "Unable to retrieve phone numberusing IBluetoothHeadsetPhone proxy"

    #@2c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 2097
    const-string v7, ""

    #@31
    goto :goto_1c

    #@32
    .line 2101
    .end local v7           #phoneNumber:Ljava/lang/String;
    .end local v8           #type:I
    .end local v18           #ee:Landroid/os/RemoteException;
    :cond_32
    move-object/from16 v0, p0

    #@34
    iget-object v1, v0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@36
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothHeadsetPhone;->listCurrentCalls()Z

    #@39
    move-result v1

    #@3a
    if-nez v1, :cond_26

    #@3c
    .line 2102
    const/4 v10, 0x0

    #@3d
    const/4 v11, 0x0

    #@3e
    const/4 v12, 0x0

    #@3f
    const/4 v13, 0x0

    #@40
    const/4 v14, 0x0

    #@41
    const-string v15, ""

    #@43
    const/16 v16, 0x0

    #@45
    move-object/from16 v9, p0

    #@47
    invoke-direct/range {v9 .. v16}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->clccResponseNative(IIIIZLjava/lang/String;I)Z
    :try_end_4a
    .catch Landroid/os/RemoteException; {:try_start_23 .. :try_end_4a} :catch_4b

    #@4a
    goto :goto_26

    #@4b
    .line 2104
    :catch_4b
    move-exception v17

    #@4c
    .line 2105
    .local v17, e:Landroid/os/RemoteException;
    const-string v1, "HeadsetStateMachine"

    #@4e
    new-instance v2, Ljava/lang/Throwable;

    #@50
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@53
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@56
    move-result-object v2

    #@57
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5a
    .line 2106
    const/4 v10, 0x0

    #@5b
    const/4 v11, 0x0

    #@5c
    const/4 v12, 0x0

    #@5d
    const/4 v13, 0x0

    #@5e
    const/4 v14, 0x0

    #@5f
    const-string v15, ""

    #@61
    const/16 v16, 0x0

    #@63
    move-object/from16 v9, p0

    #@65
    invoke-direct/range {v9 .. v16}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->clccResponseNative(IIIIZLjava/lang/String;I)Z

    #@68
    goto :goto_26

    #@69
    .line 2109
    .end local v17           #e:Landroid/os/RemoteException;
    :cond_69
    const-string v1, "HeadsetStateMachine"

    #@6b
    const-string v2, "Handsfree phone proxy null for At+CLCC"

    #@6d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 2110
    const/4 v10, 0x0

    #@71
    const/4 v11, 0x0

    #@72
    const/4 v12, 0x0

    #@73
    const/4 v13, 0x0

    #@74
    const/4 v14, 0x0

    #@75
    const-string v15, ""

    #@77
    const/16 v16, 0x0

    #@79
    move-object/from16 v9, p0

    #@7b
    invoke-direct/range {v9 .. v16}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->clccResponseNative(IIIIZLjava/lang/String;I)Z

    #@7e
    goto :goto_26
.end method

.method private processAtCops()V
    .registers 5

    #@0
    .prologue
    .line 2068
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@2
    if-eqz v2, :cond_27

    #@4
    .line 2070
    :try_start_4
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@6
    invoke-interface {v2}, Landroid/bluetooth/IBluetoothHeadsetPhone;->getNetworkOperator()Ljava/lang/String;

    #@9
    move-result-object v1

    #@a
    .line 2071
    .local v1, operatorName:Ljava/lang/String;
    if-nez v1, :cond_e

    #@c
    .line 2072
    const-string v1, ""

    #@e
    .line 2074
    :cond_e
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->copsResponseNative(Ljava/lang/String;)Z
    :try_end_11
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_11} :catch_12

    #@11
    .line 2083
    .end local v1           #operatorName:Ljava/lang/String;
    :goto_11
    return-void

    #@12
    .line 2075
    :catch_12
    move-exception v0

    #@13
    .line 2076
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "HeadsetStateMachine"

    #@15
    new-instance v3, Ljava/lang/Throwable;

    #@17
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@1a
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21
    .line 2077
    const-string v2, ""

    #@23
    invoke-direct {p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->copsResponseNative(Ljava/lang/String;)Z

    #@26
    goto :goto_11

    #@27
    .line 2080
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_27
    const-string v2, "HeadsetStateMachine"

    #@29
    const-string v3, "Handsfree phone proxy null for At+COPS"

    #@2b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 2081
    const-string v2, ""

    #@30
    invoke-direct {p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->copsResponseNative(Ljava/lang/String;)Z

    #@33
    goto :goto_11
.end method

.method private processAtCpbr(Ljava/lang/String;ILandroid/bluetooth/BluetoothDevice;)V
    .registers 7
    .parameter "atString"
    .parameter "type"
    .parameter "mCurrentDevice"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2139
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "processAtCpbr - atString = "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@17
    .line 2140
    const-string v0, "[BTUI] [r] AT+CPBR"

    #@19
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@1c
    .line 2141
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@1e
    if-eqz v0, :cond_26

    #@20
    .line 2142
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@22
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/bluetooth/hfp/AtPhonebook;->handleCpbrCommand(Ljava/lang/String;ILandroid/bluetooth/BluetoothDevice;)V

    #@25
    .line 2148
    :goto_25
    return-void

    #@26
    .line 2145
    :cond_26
    const-string v0, "HeadsetStateMachine"

    #@28
    const-string v1, "Phonebook handle null for At+CPBR"

    #@2a
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 2146
    invoke-virtual {p0, v2, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@30
    goto :goto_25
.end method

.method private processAtCpbs(Ljava/lang/String;I)V
    .registers 6
    .parameter "atString"
    .parameter "type"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2127
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "processAtCpbs - atString = "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@17
    .line 2128
    const-string v0, "[BTUI] [r] AT+CPBS"

    #@19
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@1c
    .line 2129
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@1e
    if-eqz v0, :cond_26

    #@20
    .line 2130
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@22
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hfp/AtPhonebook;->handleCpbsCommand(Ljava/lang/String;I)V

    #@25
    .line 2136
    :goto_25
    return-void

    #@26
    .line 2133
    :cond_26
    const-string v0, "HeadsetStateMachine"

    #@28
    const-string v1, "Phonebook handle null for At+CPBS"

    #@2a
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 2134
    invoke-virtual {p0, v2, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@30
    goto :goto_25
.end method

.method private processAtCscs(Ljava/lang/String;I)V
    .registers 6
    .parameter "atString"
    .parameter "type"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2115
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v1, "processAtCscs - atString = "

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v0

    #@c
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v0

    #@10
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v0

    #@14
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@17
    .line 2116
    const-string v0, "[BTUI] [r] AT+CSCS"

    #@19
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@1c
    .line 2117
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@1e
    if-eqz v0, :cond_26

    #@20
    .line 2118
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@22
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hfp/AtPhonebook;->handleCscsCommand(Ljava/lang/String;I)V

    #@25
    .line 2124
    :goto_25
    return-void

    #@26
    .line 2121
    :cond_26
    const-string v0, "HeadsetStateMachine"

    #@28
    const-string v1, "Phonebook handle null for At+CSCS"

    #@2a
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 2122
    invoke-virtual {p0, v2, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@30
    goto :goto_25
.end method

.method private processAtXevent(Ljava/lang/String;)V
    .registers 10
    .parameter "atString"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 2194
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v1, "processAtXevent - atString = "

    #@9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v0

    #@d
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@18
    .line 2195
    const-string v0, "="

    #@1a
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@1d
    move-result v0

    #@1e
    if-eqz v0, :cond_3f

    #@20
    const-string v0, "=?"

    #@22
    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@25
    move-result v0

    #@26
    if-nez v0, :cond_3f

    #@28
    .line 2196
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2b
    move-result-object v0

    #@2c
    invoke-static {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->generateArgs(Ljava/lang/String;)[Ljava/lang/Object;

    #@2f
    move-result-object v4

    #@30
    .line 2197
    .local v4, args:[Ljava/lang/Object;
    const-string v1, "+XEVENT"

    #@32
    const/16 v2, 0x55

    #@34
    const/4 v3, 0x2

    #@35
    iget-object v5, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@37
    move-object v0, p0

    #@38
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->broadcastVendorSpecificEventIntent(Ljava/lang/String;II[Ljava/lang/Object;Landroid/bluetooth/BluetoothDevice;)V

    #@3b
    .line 2202
    invoke-virtual {p0, v7, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@3e
    .line 2208
    .end local v4           #args:[Ljava/lang/Object;
    :goto_3e
    return-void

    #@3f
    .line 2205
    :cond_3f
    const-string v0, "HeadsetStateMachine"

    #@41
    const-string v1, "processAtXevent: command type error"

    #@43
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 2206
    invoke-virtual {p0, v6, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@49
    goto :goto_3e
.end method

.method private processBrsf(II)V
    .registers 10
    .parameter "local_feature"
    .parameter "peer_feature"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 2451
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "local_feature:"

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, ", peer_feature:"

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-direct {p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@22
    .line 2452
    const-string v2, "HeadsetStateMachine"

    #@24
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "local_feature:"

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    const-string v4, ", peer_feature:"

    #@35
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v3

    #@39
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 2454
    const-string v1, "bt_headset_vgs"

    #@46
    .line 2455
    .local v1, HEADSET_VGS:Ljava/lang/String;
    const/16 v0, 0x10

    #@48
    .line 2457
    .local v0, BRSF_HF_REMOTE_VOL_CONTROL:I
    and-int/lit8 v2, p2, 0x10

    #@4a
    if-nez v2, :cond_78

    #@4c
    .line 2458
    const-string v2, "HeadsetStateMachine"

    #@4e
    const-string v3, "[BTUI] Remote volume control : not supported (bt_headset_vgs=off)"

    #@50
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 2459
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@55
    const-string v3, "bt_headset_vgs=off"

    #@57
    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@5a
    .line 2467
    :goto_5a
    and-int/lit8 v2, p1, 0x4

    #@5c
    const/4 v3, 0x4

    #@5d
    if-ne v2, v3, :cond_87

    #@5f
    .line 2468
    const-string v2, "HeadsetStateMachine"

    #@61
    const-string v3, "AG VR is Supported!"

    #@63
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 2469
    iput-boolean v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->VR_SUPPORT_AG:Z

    #@68
    .line 2474
    :goto_68
    and-int/lit8 v2, p2, 0x8

    #@6a
    const/16 v3, 0x8

    #@6c
    if-ne v2, v3, :cond_91

    #@6e
    .line 2475
    const-string v2, "HeadsetStateMachine"

    #@70
    const-string v3, "HF VR is Supported!"

    #@72
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 2476
    iput-boolean v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->VR_SUPPORT_HF:Z

    #@77
    .line 2482
    :goto_77
    return-void

    #@78
    .line 2461
    :cond_78
    const-string v2, "HeadsetStateMachine"

    #@7a
    const-string v3, "[BTUI] Remote volume control : supported (bt_headset_vgs=on)"

    #@7c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7f
    .line 2462
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@81
    const-string v3, "bt_headset_vgs=on"

    #@83
    invoke-virtual {v2, v3}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@86
    goto :goto_5a

    #@87
    .line 2471
    :cond_87
    const-string v2, "HeadsetStateMachine"

    #@89
    const-string v3, "AG VR is not supported!"

    #@8b
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8e
    .line 2472
    iput-boolean v5, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->VR_SUPPORT_AG:Z

    #@90
    goto :goto_68

    #@91
    .line 2478
    :cond_91
    const-string v2, "HeadsetStateMachine"

    #@93
    const-string v3, "HF VR is not supported!"

    #@95
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@98
    .line 2479
    iput-boolean v5, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->VR_SUPPORT_HF:Z

    #@9a
    goto :goto_77
.end method

.method private processCallState(Lcom/android/bluetooth/hfp/HeadsetCallState;)V
    .registers 3
    .parameter "callState"

    #@0
    .prologue
    .line 1901
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processCallState(Lcom/android/bluetooth/hfp/HeadsetCallState;Z)V

    #@4
    .line 1902
    return-void
.end method

.method private processCallState(Lcom/android/bluetooth/hfp/HeadsetCallState;Z)V
    .registers 9
    .parameter "callState"
    .parameter "isVirtualCall"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 1929
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@4
    iget v1, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mNumActive:I

    #@6
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->setNumActiveCall(I)V

    #@9
    .line 1930
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@b
    iget v1, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mNumHeld:I

    #@d
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->setNumHeldCall(I)V

    #@10
    .line 1931
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@12
    iget v1, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mCallState:I

    #@14
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->setCallState(I)V

    #@17
    .line 1934
    iget-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsA2dpSuspended:Z

    #@19
    if-eqz v0, :cond_b4

    #@1b
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isInCall()Z

    #@1e
    move-result v0

    #@1f
    if-nez v0, :cond_b4

    #@21
    .line 1935
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsA2dpSuspended:Z

    #@23
    .line 1936
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@25
    const-string v1, "A2dpSuspended=false"

    #@27
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@2a
    .line 1937
    const-string v0, "HeadsetStateMachine"

    #@2c
    const-string v1, "[BTUI] A2DP was suspended, since it is not in-call now, release A2DP suspend!"

    #@2e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@31
    .line 1951
    :cond_31
    :goto_31
    iget-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDialingOut:Z

    #@33
    if-eqz v0, :cond_44

    #@35
    iget v0, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mCallState:I

    #@37
    const/4 v1, 0x2

    #@38
    if-ne v0, v1, :cond_44

    #@3a
    .line 1953
    invoke-virtual {p0, v3, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@3d
    .line 1954
    const/16 v0, 0x66

    #@3f
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->removeMessages(I)V

    #@42
    .line 1955
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDialingOut:Z

    #@44
    .line 1957
    :cond_44
    new-instance v0, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v1, "mNumActive: "

    #@4b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v0

    #@4f
    iget v1, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mNumActive:I

    #@51
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@54
    move-result-object v0

    #@55
    const-string v1, " mNumHeld: "

    #@57
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v0

    #@5b
    iget v1, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mNumHeld:I

    #@5d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v0

    #@61
    const-string v1, " mCallState: "

    #@63
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v0

    #@67
    iget v1, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mCallState:I

    #@69
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v0

    #@6d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@70
    move-result-object v0

    #@71
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@74
    .line 1959
    new-instance v0, Ljava/lang/StringBuilder;

    #@76
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@79
    const-string v1, "mNumber: "

    #@7b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v0

    #@7f
    iget-object v1, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mNumber:Ljava/lang/String;

    #@81
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v0

    #@85
    const-string v1, " mType: "

    #@87
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v0

    #@8b
    iget v1, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mType:I

    #@8d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v0

    #@91
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v0

    #@95
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@98
    .line 1960
    if-nez p2, :cond_9d

    #@9a
    .line 1963
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->terminateScoUsingVirtualVoiceCall()Z

    #@9d
    .line 1965
    :cond_9d
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@a0
    move-result-object v0

    #@a1
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDisconnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;

    #@a3
    if-eq v0, v1, :cond_b3

    #@a5
    .line 1966
    iget v1, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mNumActive:I

    #@a7
    iget v2, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mNumHeld:I

    #@a9
    iget v3, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mCallState:I

    #@ab
    iget-object v4, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mNumber:Ljava/lang/String;

    #@ad
    iget v5, p1, Lcom/android/bluetooth/hfp/HeadsetCallState;->mType:I

    #@af
    move-object v0, p0

    #@b0
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->phoneStateChangeNative(IIILjava/lang/String;I)Z

    #@b3
    .line 1969
    :cond_b3
    return-void

    #@b4
    .line 1939
    :cond_b4
    iget-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsPeerDeviceInBlackList:Z

    #@b6
    if-eqz v0, :cond_31

    #@b8
    sget v0, Lcom/android/bluetooth/a2dp/A2dpService;->mPlayingStatus:I

    #@ba
    const/16 v1, 0xa

    #@bc
    if-ne v0, v1, :cond_31

    #@be
    iget-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsA2dpSuspended:Z

    #@c0
    if-nez v0, :cond_31

    #@c2
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isInCall()Z

    #@c5
    move-result v0

    #@c6
    if-eqz v0, :cond_31

    #@c8
    .line 1941
    iput-boolean v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsA2dpSuspended:Z

    #@ca
    .line 1942
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@cc
    const-string v1, "A2dpSuspended=true"

    #@ce
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@d1
    .line 1943
    const-string v0, "HeadsetStateMachine"

    #@d3
    const-string v1, "[BTUI] since it is in-call now, suspend A2DP!"

    #@d5
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@d8
    .line 1945
    const-wide/16 v0, 0x64

    #@da
    :try_start_da
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_dd
    .catch Ljava/lang/InterruptedException; {:try_start_da .. :try_end_dd} :catch_df

    #@dd
    goto/16 :goto_31

    #@df
    .line 1946
    :catch_df
    move-exception v0

    #@e0
    goto/16 :goto_31
.end method

.method private processCodecSelectionEvent(I)V
    .registers 4
    .parameter "codec"

    #@0
    .prologue
    .line 2432
    const/4 v0, 0x1

    #@1
    if-ne p1, v0, :cond_b

    #@3
    .line 2433
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@5
    const-string v1, "bt_headset_nrec=on"

    #@7
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@a
    .line 2437
    :goto_a
    return-void

    #@b
    .line 2435
    :cond_b
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@d
    const-string v1, "bt_headset_nrecoff"

    #@f
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@12
    goto :goto_a
.end method

.method private processDeviceStateChanged(Lcom/android/bluetooth/hfp/HeadsetDeviceState;)V
    .registers 6
    .parameter "deviceState"

    #@0
    .prologue
    .line 2502
    iget v0, p1, Lcom/android/bluetooth/hfp/HeadsetDeviceState;->mService:I

    #@2
    iget v1, p1, Lcom/android/bluetooth/hfp/HeadsetDeviceState;->mRoam:I

    #@4
    iget v2, p1, Lcom/android/bluetooth/hfp/HeadsetDeviceState;->mSignal:I

    #@6
    iget v3, p1, Lcom/android/bluetooth/hfp/HeadsetDeviceState;->mBatteryCharge:I

    #@8
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->notifyDeviceStatusNative(IIII)Z

    #@b
    .line 2504
    return-void
.end method

.method private processDialCall(Ljava/lang/String;)V
    .registers 11
    .parameter "number"

    #@0
    .prologue
    const/16 v8, 0x3b

    #@2
    const/4 v7, 0x1

    #@3
    const/4 v6, 0x0

    #@4
    .line 1651
    if-eqz p1, :cond_c

    #@6
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@9
    move-result v4

    #@a
    if-nez v4, :cond_1d

    #@c
    .line 1652
    :cond_c
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@e
    invoke-virtual {v4}, Lcom/android/bluetooth/hfp/AtPhonebook;->getLastDialledNumber()Ljava/lang/String;

    #@11
    move-result-object v0

    #@12
    .line 1653
    .local v0, dialNumber:Ljava/lang/String;
    if-nez v0, :cond_b4

    #@14
    .line 1655
    const-string v4, "processDialCall, last dial number null"

    #@16
    invoke-direct {p0, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@19
    .line 1657
    invoke-virtual {p0, v6, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@1c
    .line 1743
    .end local v0           #dialNumber:Ljava/lang/String;
    :goto_1c
    return-void

    #@1d
    .line 1660
    :cond_1d
    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    #@20
    move-result v4

    #@21
    const/16 v5, 0x3e

    #@23
    if-ne v4, v5, :cond_9a

    #@25
    .line 1663
    const-string v4, ">9999"

    #@27
    invoke-virtual {p1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@2a
    move-result v4

    #@2b
    if-eqz v4, :cond_31

    #@2d
    .line 1664
    invoke-virtual {p0, v6, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@30
    goto :goto_1c

    #@31
    .line 1669
    :cond_31
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@34
    move-result v4

    #@35
    if-ne v4, v7, :cond_40

    #@37
    .line 1670
    const-string v4, "[BTUI] [MemDial] No number specified in ATD>nnn command"

    #@39
    invoke-direct {p0, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@3c
    .line 1671
    invoke-virtual {p0, v6, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@3f
    goto :goto_1c

    #@40
    .line 1675
    :cond_40
    new-instance v4, Ljava/lang/StringBuilder;

    #@42
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@45
    const-string v5, "[BTUI] [r] [MemDial] ATD"

    #@47
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v4

    #@4f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    invoke-direct {p0, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@56
    .line 1676
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@59
    move-result v4

    #@5a
    add-int/lit8 v4, v4, -0x1

    #@5c
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@5f
    move-result v4

    #@60
    if-ne v4, v8, :cond_7a

    #@62
    .line 1677
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@65
    move-result v4

    #@66
    add-int/lit8 v4, v4, -0x1

    #@68
    invoke-virtual {p1, v7, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@6b
    move-result-object p1

    #@6c
    .line 1683
    :goto_6c
    :try_start_6c
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_6f
    .catch Ljava/lang/NumberFormatException; {:try_start_6c .. :try_end_6f} :catch_7f

    #@6f
    move-result v2

    #@70
    .line 1690
    .local v2, index:I
    invoke-direct {p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->memoryDial(I)Ljava/lang/String;

    #@73
    move-result-object v0

    #@74
    .line 1691
    .restart local v0       #dialNumber:Ljava/lang/String;
    if-nez v0, :cond_b4

    #@76
    .line 1692
    invoke-virtual {p0, v6, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@79
    goto :goto_1c

    #@7a
    .line 1679
    .end local v0           #dialNumber:Ljava/lang/String;
    .end local v2           #index:I
    :cond_7a
    invoke-virtual {p1, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@7d
    move-result-object p1

    #@7e
    goto :goto_6c

    #@7f
    .line 1684
    :catch_7f
    move-exception v1

    #@80
    .line 1685
    .local v1, e:Ljava/lang/NumberFormatException;
    new-instance v4, Ljava/lang/StringBuilder;

    #@82
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@85
    const-string v5, "[BTUI] [MemDial] invalid integer : "

    #@87
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8a
    move-result-object v4

    #@8b
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v4

    #@8f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v4

    #@93
    invoke-direct {p0, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->BtUiLog(Ljava/lang/String;)V

    #@96
    .line 1686
    invoke-virtual {p0, v6, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@99
    goto :goto_1c

    #@9a
    .line 1715
    .end local v1           #e:Ljava/lang/NumberFormatException;
    :cond_9a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@9d
    move-result v4

    #@9e
    add-int/lit8 v4, v4, -0x1

    #@a0
    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    #@a3
    move-result v4

    #@a4
    if-ne v4, v8, :cond_b0

    #@a6
    .line 1716
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    #@a9
    move-result v4

    #@aa
    add-int/lit8 v4, v4, -0x1

    #@ac
    invoke-virtual {p1, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@af
    move-result-object p1

    #@b0
    .line 1719
    :cond_b0
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->convertPreDial(Ljava/lang/String;)Ljava/lang/String;

    #@b3
    move-result-object v0

    #@b4
    .line 1722
    .restart local v0       #dialNumber:Ljava/lang/String;
    :cond_b4
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->terminateScoUsingVirtualVoiceCall()Z

    #@b7
    .line 1724
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@b9
    sget-boolean v4, Lcom/android/bluetooth/hfp/AtPhonebook;->isVideoCall:Z

    #@bb
    if-nez v4, :cond_e5

    #@bd
    .line 1726
    new-instance v3, Landroid/content/Intent;

    #@bf
    const-string v4, "android.intent.action.CALL_PRIVILEGED"

    #@c1
    const-string v5, "tel"

    #@c3
    const/4 v6, 0x0

    #@c4
    invoke-static {v5, v0, v6}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    #@c7
    move-result-object v5

    #@c8
    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@cb
    .line 1728
    .local v3, intent:Landroid/content/Intent;
    const/high16 v4, 0x1000

    #@cd
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@d0
    .line 1730
    const/high16 v4, 0x4

    #@d2
    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@d5
    .line 1732
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@d7
    invoke-virtual {v4, v3}, Lcom/android/bluetooth/hfp/HeadsetService;->startActivity(Landroid/content/Intent;)V

    #@da
    .line 1741
    .end local v3           #intent:Landroid/content/Intent;
    :goto_da
    iput-boolean v7, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDialingOut:Z

    #@dc
    .line 1742
    const/16 v4, 0x66

    #@de
    const-wide/16 v5, 0x2710

    #@e0
    invoke-virtual {p0, v4, v5, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessageDelayed(IJ)V

    #@e3
    goto/16 :goto_1c

    #@e5
    .line 1735
    :cond_e5
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->videodial(Ljava/lang/String;)V

    #@e8
    goto :goto_da
.end method

.method private processHangupCall()V
    .registers 4

    #@0
    .prologue
    .line 1634
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isVirtualCallInProgress()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_a

    #@6
    .line 1635
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->terminateScoUsingVirtualVoiceCall()Z

    #@9
    .line 1647
    :goto_9
    return-void

    #@a
    .line 1637
    :cond_a
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@c
    if-eqz v1, :cond_24

    #@e
    .line 1639
    :try_start_e
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@10
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothHeadsetPhone;->hangupCall()Z
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_13} :catch_14

    #@13
    goto :goto_9

    #@14
    .line 1640
    :catch_14
    move-exception v0

    #@15
    .line 1641
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "HeadsetStateMachine"

    #@17
    new-instance v2, Ljava/lang/Throwable;

    #@19
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@1c
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_9

    #@24
    .line 1644
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_24
    const-string v1, "HeadsetStateMachine"

    #@26
    const-string v2, "Handsfree phone proxy null for hanging up call"

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_9
.end method

.method private processIntentBatteryChanged(Landroid/content/Intent;)V
    .registers 7
    .parameter "intent"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 2486
    const-string v2, "level"

    #@3
    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@6
    move-result v0

    #@7
    .line 2487
    .local v0, batteryLevel:I
    const-string v2, "scale"

    #@9
    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@c
    move-result v1

    #@d
    .line 2488
    .local v1, scale:I
    if-eq v0, v3, :cond_13

    #@f
    if-eq v1, v3, :cond_13

    #@11
    if-nez v1, :cond_36

    #@13
    .line 2489
    :cond_13
    const-string v2, "HeadsetStateMachine"

    #@15
    new-instance v3, Ljava/lang/StringBuilder;

    #@17
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1a
    const-string v4, "Bad Battery Changed intent: "

    #@1c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@23
    move-result-object v3

    #@24
    const-string v4, ","

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v3

    #@2e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v3

    #@32
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 2494
    :goto_35
    return-void

    #@36
    .line 2492
    :cond_36
    mul-int/lit8 v2, v0, 0x5

    #@38
    div-int v0, v2, v1

    #@3a
    .line 2493
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@3c
    invoke-virtual {v2, v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->setBatteryCharge(I)V

    #@3f
    goto :goto_35
.end method

.method private processKeyPressed()V
    .registers 7

    #@0
    .prologue
    .line 2229
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@2
    invoke-virtual {v3}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getCallState()I

    #@5
    move-result v3

    #@6
    const/4 v4, 0x4

    #@7
    if-ne v3, v4, :cond_2b

    #@9
    .line 2230
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@b
    if-eqz v3, :cond_23

    #@d
    .line 2232
    :try_start_d
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@f
    invoke-interface {v3}, Landroid/bluetooth/IBluetoothHeadsetPhone;->answerCall()Z
    :try_end_12
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_12} :catch_13

    #@12
    .line 2272
    :goto_12
    return-void

    #@13
    .line 2233
    :catch_13
    move-exception v1

    #@14
    .line 2234
    .local v1, e:Landroid/os/RemoteException;
    const-string v3, "HeadsetStateMachine"

    #@16
    new-instance v4, Ljava/lang/Throwable;

    #@18
    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    #@1b
    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@1e
    move-result-object v4

    #@1f
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_12

    #@23
    .line 2237
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_23
    const-string v3, "HeadsetStateMachine"

    #@25
    const-string v4, "Handsfree phone proxy null for answering call"

    #@27
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    goto :goto_12

    #@2b
    .line 2239
    :cond_2b
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@2d
    invoke-virtual {v3}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->getNumActiveCall()I

    #@30
    move-result v3

    #@31
    if-lez v3, :cond_65

    #@33
    .line 2240
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isAudioOn()Z

    #@36
    move-result v3

    #@37
    if-nez v3, :cond_43

    #@39
    .line 2242
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@3b
    invoke-direct {p0, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@3e
    move-result-object v3

    #@3f
    invoke-direct {p0, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->connectAudioNative([B)Z

    #@42
    goto :goto_12

    #@43
    .line 2246
    :cond_43
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@45
    if-eqz v3, :cond_5d

    #@47
    .line 2248
    :try_start_47
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@49
    invoke-interface {v3}, Landroid/bluetooth/IBluetoothHeadsetPhone;->hangupCall()Z
    :try_end_4c
    .catch Landroid/os/RemoteException; {:try_start_47 .. :try_end_4c} :catch_4d

    #@4c
    goto :goto_12

    #@4d
    .line 2249
    :catch_4d
    move-exception v1

    #@4e
    .line 2250
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v3, "HeadsetStateMachine"

    #@50
    new-instance v4, Ljava/lang/Throwable;

    #@52
    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    #@55
    invoke-static {v4}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@58
    move-result-object v4

    #@59
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    goto :goto_12

    #@5d
    .line 2253
    .end local v1           #e:Landroid/os/RemoteException;
    :cond_5d
    const-string v3, "HeadsetStateMachine"

    #@5f
    const-string v4, "Handsfree phone proxy null for hangup call"

    #@61
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    goto :goto_12

    #@65
    .line 2257
    :cond_65
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@67
    invoke-virtual {v3}, Lcom/android/bluetooth/hfp/AtPhonebook;->getLastDialledNumber()Ljava/lang/String;

    #@6a
    move-result-object v0

    #@6b
    .line 2258
    .local v0, dialNumber:Ljava/lang/String;
    if-nez v0, :cond_73

    #@6d
    .line 2260
    const-string v3, "processKeyPressed, last dial number null"

    #@6f
    invoke-direct {p0, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@72
    goto :goto_12

    #@73
    .line 2264
    :cond_73
    new-instance v2, Landroid/content/Intent;

    #@75
    const-string v3, "android.intent.action.CALL_PRIVILEGED"

    #@77
    const-string v4, "tel"

    #@79
    const/4 v5, 0x0

    #@7a
    invoke-static {v4, v0, v5}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    #@7d
    move-result-object v4

    #@7e
    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@81
    .line 2266
    .local v2, intent:Landroid/content/Intent;
    const/high16 v3, 0x1000

    #@83
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@86
    .line 2268
    const/high16 v3, 0x4

    #@88
    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@8b
    .line 2270
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@8d
    invoke-virtual {v3, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->startActivity(Landroid/content/Intent;)V

    #@90
    goto :goto_12
.end method

.method private processLocalVrEvent(I)V
    .registers 7
    .parameter "state"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1337
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isBtVrSupported()Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_10

    #@8
    .line 1338
    const-string v1, "HeadsetStateMachine"

    #@a
    const-string v2, "[BTUI] processLocalVrEvent() : AG or HF does not support BT VR."

    #@c
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 1408
    :cond_f
    :goto_f
    return-void

    #@10
    .line 1343
    :cond_10
    if-ne p1, v3, :cond_98

    #@12
    .line 1345
    const/4 v0, 0x1

    #@13
    .line 1346
    .local v0, needAudio:Z
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@15
    if-nez v1, :cond_1d

    #@17
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isInCall()Z

    #@1a
    move-result v1

    #@1b
    if-eqz v1, :cond_46

    #@1d
    .line 1348
    :cond_1d
    const-string v1, "HeadsetStateMachine"

    #@1f
    new-instance v2, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v3, "Voice recognition started when call is active. isInCall:"

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isInCall()Z

    #@2d
    move-result v3

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    const-string v3, " mVoiceRecognitionStarted: "

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    iget-boolean v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@3a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v2

    #@3e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v2

    #@42
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    goto :goto_f

    #@46
    .line 1352
    :cond_46
    iput-boolean v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@48
    .line 1354
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@4a
    if-eqz v1, :cond_8c

    #@4c
    .line 1356
    const-string v1, "HeadsetStateMachine"

    #@4e
    const-string v2, "Voice recognition started successfully"

    #@50
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    .line 1357
    iput-boolean v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@55
    .line 1358
    invoke-virtual {p0, v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@58
    .line 1359
    const/16 v1, 0x67

    #@5a
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->removeMessages(I)V

    #@5d
    .line 1367
    :goto_5d
    if-eqz v0, :cond_7e

    #@5f
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isAudioOn()Z

    #@62
    move-result v1

    #@63
    if-nez v1, :cond_7e

    #@65
    .line 1369
    const-string v1, "HeadsetStateMachine"

    #@67
    const-string v2, "Initiating audio connection for Voice Recognition"

    #@69
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 1378
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@6e
    const-string v2, "A2dpSuspended=true"

    #@70
    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@73
    .line 1379
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@75
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@78
    move-result-object v1

    #@79
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->connectAudioNative([B)Z

    #@7c
    .line 1382
    iput-boolean v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIsA2dpSuspended:Z

    #@7e
    .line 1386
    :cond_7e
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mStartVoiceRecognitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@80
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@83
    move-result v1

    #@84
    if-eqz v1, :cond_f

    #@86
    .line 1387
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mStartVoiceRecognitionWakeLock:Landroid/os/PowerManager$WakeLock;

    #@88
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@8b
    goto :goto_f

    #@8c
    .line 1363
    :cond_8c
    const-string v1, "HeadsetStateMachine"

    #@8e
    const-string v2, "Voice recognition started locally"

    #@90
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@93
    .line 1364
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->startVoiceRecognitionNative()Z

    #@96
    move-result v0

    #@97
    goto :goto_5d

    #@98
    .line 1392
    .end local v0           #needAudio:Z
    :cond_98
    const-string v1, "HeadsetStateMachine"

    #@9a
    new-instance v2, Ljava/lang/StringBuilder;

    #@9c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9f
    const-string v3, "Voice Recognition stopped. mVoiceRecognitionStarted: "

    #@a1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v2

    #@a5
    iget-boolean v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@a7
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v2

    #@ab
    const-string v3, " mWaitingForVoiceRecognition: "

    #@ad
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v2

    #@b1
    iget-boolean v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@b3
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v2

    #@b7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v2

    #@bb
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@be
    .line 1394
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@c0
    if-nez v1, :cond_c6

    #@c2
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@c4
    if-eqz v1, :cond_f

    #@c6
    .line 1396
    :cond_c6
    iput-boolean v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@c8
    .line 1397
    iput-boolean v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@ca
    .line 1401
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->stopVoiceRecognitionNative()Z

    #@cd
    move-result v1

    #@ce
    if-eqz v1, :cond_f

    #@d0
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isInCallNeedSCO()Z

    #@d3
    move-result v1

    #@d4
    if-nez v1, :cond_f

    #@d6
    .line 1403
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@d8
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@db
    move-result-object v1

    #@dc
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->disconnectAudioNative([B)Z

    #@df
    .line 1404
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@e1
    const-string v2, "A2dpSuspended=false"

    #@e3
    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@e6
    goto/16 :goto_f
.end method

.method private processNoiceReductionEvent(I)V
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 1974
    const/4 v0, 0x1

    #@1
    if-ne p1, v0, :cond_b

    #@3
    .line 1975
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@5
    const-string v1, "bt_headset_nrec=on"

    #@7
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@a
    .line 1979
    :goto_a
    return-void

    #@b
    .line 1977
    :cond_b
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@d
    const-string v1, "bt_headset_nrec=off"

    #@f
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@12
    goto :goto_a
.end method

.method private processRoamChanged(Z)V
    .registers 4
    .parameter "roam"

    #@0
    .prologue
    .line 2497
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@2
    if-eqz p1, :cond_9

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->setRoam(I)V

    #@8
    .line 2499
    return-void

    #@9
    .line 2497
    :cond_9
    const/4 v0, 0x0

    #@a
    goto :goto_5
.end method

.method private processSendClccResponse(Lcom/android/bluetooth/hfp/HeadsetClccResponse;)V
    .registers 10
    .parameter "clcc"

    #@0
    .prologue
    .line 2507
    iget v1, p1, Lcom/android/bluetooth/hfp/HeadsetClccResponse;->mIndex:I

    #@2
    iget v2, p1, Lcom/android/bluetooth/hfp/HeadsetClccResponse;->mDirection:I

    #@4
    iget v3, p1, Lcom/android/bluetooth/hfp/HeadsetClccResponse;->mStatus:I

    #@6
    iget v4, p1, Lcom/android/bluetooth/hfp/HeadsetClccResponse;->mMode:I

    #@8
    iget-boolean v5, p1, Lcom/android/bluetooth/hfp/HeadsetClccResponse;->mMpty:Z

    #@a
    iget-object v6, p1, Lcom/android/bluetooth/hfp/HeadsetClccResponse;->mNumber:Ljava/lang/String;

    #@c
    iget v7, p1, Lcom/android/bluetooth/hfp/HeadsetClccResponse;->mType:I

    #@e
    move-object v0, p0

    #@f
    invoke-direct/range {v0 .. v7}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->clccResponseNative(IIIIZLjava/lang/String;I)Z

    #@12
    .line 2509
    return-void
.end method

.method private processSendDtmf(I)V
    .registers 5
    .parameter "dtmf"

    #@0
    .prologue
    .line 1884
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@2
    if-eqz v1, :cond_24

    #@4
    .line 1886
    :try_start_4
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@6
    invoke-interface {v1, p1}, Landroid/bluetooth/IBluetoothHeadsetPhone;->sendDtmf(I)Z
    :try_end_9
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_9} :catch_14

    #@9
    .line 1894
    :goto_9
    sget-object v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDtmfPlay:Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;

    #@b
    if-eqz v1, :cond_13

    #@d
    .line 1895
    sget-object v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDtmfPlay:Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;

    #@f
    int-to-char v2, p1

    #@10
    invoke-virtual {v1, v2}, Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;->playDTMF(C)V

    #@13
    .line 1898
    :cond_13
    return-void

    #@14
    .line 1887
    :catch_14
    move-exception v0

    #@15
    .line 1888
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "HeadsetStateMachine"

    #@17
    new-instance v2, Ljava/lang/Throwable;

    #@19
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@1c
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    goto :goto_9

    #@24
    .line 1891
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_24
    const-string v1, "HeadsetStateMachine"

    #@26
    const-string v2, "Handsfree phone proxy null for sending DTMF"

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_9
.end method

.method private processSubscriberNumberRequest()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 2000
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@3
    if-eqz v2, :cond_50

    #@5
    .line 2002
    :try_start_5
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@7
    invoke-interface {v2}, Landroid/bluetooth/IBluetoothHeadsetPhone;->getSubscriberNumber()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    .line 2003
    .local v1, number:Ljava/lang/String;
    if-eqz v1, :cond_3c

    #@d
    .line 2004
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "+CNUM: ,\""

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    const-string v3, "\","

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->toaFromString(Ljava/lang/String;)I

    #@25
    move-result v3

    #@26
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@29
    move-result-object v2

    #@2a
    const-string v3, ",,4"

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v2

    #@34
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseStringNative(Ljava/lang/String;)Z

    #@37
    .line 2006
    const/4 v2, 0x1

    #@38
    const/4 v3, 0x0

    #@39
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z
    :try_end_3c
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_3c} :catch_3d

    #@3c
    .line 2015
    .end local v1           #number:Ljava/lang/String;
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 2008
    :catch_3d
    move-exception v0

    #@3e
    .line 2009
    .local v0, e:Landroid/os/RemoteException;
    const-string v2, "HeadsetStateMachine"

    #@40
    new-instance v3, Ljava/lang/Throwable;

    #@42
    invoke-direct {v3}, Ljava/lang/Throwable;-><init>()V

    #@45
    invoke-static {v3}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 2010
    invoke-virtual {p0, v4, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@4f
    goto :goto_3c

    #@50
    .line 2013
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_50
    const-string v2, "HeadsetStateMachine"

    #@52
    const-string v3, "Handsfree phone proxy null for At+CNUM"

    #@54
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    goto :goto_3c
.end method

.method private processUnknownAt(Ljava/lang/String;)V
    .registers 8
    .parameter "atString"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, 0x5

    #@2
    .line 2212
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "processUnknownAt - atString = "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-direct {p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@18
    .line 2213
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->parseUnknownAt(Ljava/lang/String;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 2214
    .local v0, atCommand:Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getAtCommandType(Ljava/lang/String;)I

    #@1f
    move-result v1

    #@20
    .line 2215
    .local v1, commandType:I
    const-string v2, "+CSCS"

    #@22
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@25
    move-result v2

    #@26
    if-eqz v2, :cond_30

    #@28
    .line 2216
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@2b
    move-result-object v2

    #@2c
    invoke-direct {p0, v2, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processAtCscs(Ljava/lang/String;I)V

    #@2f
    .line 2226
    :goto_2f
    return-void

    #@30
    .line 2217
    :cond_30
    const-string v2, "+CPBS"

    #@32
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@35
    move-result v2

    #@36
    if-eqz v2, :cond_40

    #@38
    .line 2218
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-direct {p0, v2, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processAtCpbs(Ljava/lang/String;I)V

    #@3f
    goto :goto_2f

    #@40
    .line 2219
    :cond_40
    const-string v2, "+CPBR"

    #@42
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@45
    move-result v2

    #@46
    if-eqz v2, :cond_52

    #@48
    .line 2220
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@4b
    move-result-object v2

    #@4c
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@4e
    invoke-direct {p0, v2, v1, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processAtCpbr(Ljava/lang/String;ILandroid/bluetooth/BluetoothDevice;)V

    #@51
    goto :goto_2f

    #@52
    .line 2221
    :cond_52
    const-string v2, "+XEVENT"

    #@54
    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@57
    move-result v2

    #@58
    if-eqz v2, :cond_63

    #@5a
    .line 2222
    const/4 v2, 0x7

    #@5b
    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@5e
    move-result-object v2

    #@5f
    invoke-direct {p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processAtXevent(Ljava/lang/String;)V

    #@62
    goto :goto_2f

    #@63
    .line 2224
    :cond_63
    invoke-virtual {p0, v5, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@66
    goto :goto_2f
.end method

.method private processVolumeEvent(II)V
    .registers 7
    .parameter "volumeType"
    .parameter "volume"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 1872
    if-nez p1, :cond_19

    #@3
    .line 1873
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@5
    invoke-virtual {v1, p2}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->setSpeakerVolume(I)V

    #@8
    .line 1874
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@b
    move-result-object v1

    #@c
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioOn:Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

    #@e
    if-ne v1, v2, :cond_17

    #@10
    .line 1875
    .local v0, flag:I
    :goto_10
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@12
    const/4 v2, 0x6

    #@13
    invoke-virtual {v1, v2, p2, v0}, Landroid/media/AudioManager;->setStreamVolume(III)V

    #@16
    .line 1881
    .end local v0           #flag:I
    :goto_16
    return-void

    #@17
    .line 1874
    :cond_17
    const/4 v0, 0x0

    #@18
    goto :goto_10

    #@19
    .line 1876
    :cond_19
    if-ne p1, v0, :cond_21

    #@1b
    .line 1877
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@1d
    invoke-virtual {v1, p2}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->setMicVolume(I)V

    #@20
    goto :goto_16

    #@21
    .line 1879
    :cond_21
    const-string v1, "HeadsetStateMachine"

    #@23
    new-instance v2, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v3, "Bad voluem type: "

    #@2a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v2

    #@2e
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35
    move-result-object v2

    #@36
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_16
.end method

.method private processVrEvent(I)V
    .registers 8
    .parameter "state"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1283
    const-string v1, "HeadsetStateMachine"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "processVrEvent: state="

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, " mVoiceRecognitionStarted: "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    iget-boolean v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " mWaitingforVoiceRecognition: "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    iget-boolean v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, " isInCall: "

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isInCall()Z

    #@34
    move-result v3

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 1287
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isBtVrSupported()Z

    #@43
    move-result v1

    #@44
    if-nez v1, :cond_4e

    #@46
    .line 1288
    const-string v1, "HeadsetStateMachine"

    #@48
    const-string v2, "[BTUI] processVrEvent() : AG or HF does not support BT VR."

    #@4a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 1332
    :cond_4d
    :goto_4d
    return-void

    #@4e
    .line 1293
    :cond_4e
    if-ne p1, v5, :cond_70

    #@50
    .line 1294
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@52
    if-nez v1, :cond_4d

    #@54
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isVirtualCallInProgress()Z

    #@57
    move-result v1

    #@58
    if-nez v1, :cond_4d

    #@5a
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isInCall()Z

    #@5d
    move-result v1

    #@5e
    if-nez v1, :cond_4d

    #@60
    .line 1299
    :try_start_60
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@62
    sget-object v2, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sVoiceCommandIntent:Landroid/content/Intent;

    #@64
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->startActivity(Landroid/content/Intent;)V
    :try_end_67
    .catch Landroid/content/ActivityNotFoundException; {:try_start_60 .. :try_end_67} :catch_6b

    #@67
    .line 1304
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->expectVoiceRecognition()V

    #@6a
    goto :goto_4d

    #@6b
    .line 1300
    :catch_6b
    move-exception v0

    #@6c
    .line 1301
    .local v0, e:Landroid/content/ActivityNotFoundException;
    invoke-virtual {p0, v4, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@6f
    goto :goto_4d

    #@70
    .line 1306
    .end local v0           #e:Landroid/content/ActivityNotFoundException;
    :cond_70
    if-nez p1, :cond_a6

    #@72
    .line 1307
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@74
    if-nez v1, :cond_7a

    #@76
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@78
    if-eqz v1, :cond_98

    #@7a
    .line 1309
    :cond_7a
    invoke-virtual {p0, v5, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@7d
    .line 1310
    iput-boolean v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@7f
    .line 1311
    iput-boolean v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mWaitingForVoiceRecognition:Z

    #@81
    .line 1312
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isInCall()Z

    #@84
    move-result v1

    #@85
    if-nez v1, :cond_4d

    #@87
    .line 1313
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@89
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@8c
    move-result-object v1

    #@8d
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->disconnectAudioNative([B)Z

    #@90
    .line 1314
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@92
    const-string v2, "A2dpSuspended=false"

    #@94
    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@97
    goto :goto_4d

    #@98
    .line 1322
    :cond_98
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isInCall()Z

    #@9b
    move-result v1

    #@9c
    if-eqz v1, :cond_a2

    #@9e
    .line 1323
    invoke-virtual {p0, v4, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@a1
    goto :goto_4d

    #@a2
    .line 1325
    :cond_a2
    invoke-virtual {p0, v5, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@a5
    goto :goto_4d

    #@a6
    .line 1330
    :cond_a6
    const-string v1, "HeadsetStateMachine"

    #@a8
    new-instance v2, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    const-string v3, "Bad Voice Recognition state: "

    #@af
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v2

    #@b3
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v2

    #@b7
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ba
    move-result-object v2

    #@bb
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@be
    goto :goto_4d
.end method

.method private processWBSEvent(I)V
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 2423
    const/4 v0, 0x2

    #@1
    if-ne p1, v0, :cond_b

    #@3
    .line 2424
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@5
    const-string v1, "bt_samplerate=16000"

    #@7
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@a
    .line 2429
    :goto_a
    return-void

    #@b
    .line 2426
    :cond_b
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioManager:Landroid/media/AudioManager;

    #@d
    const-string v1, "bt_samplerate=8000"

    #@f
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setParameters(Ljava/lang/String;)V

    #@12
    goto :goto_a
.end method

.method private native setVolumeNative(II)Z
.end method

.method private native startVoiceRecognitionNative()Z
.end method

.method private native stopVoiceRecognitionNative()Z
.end method

.method private videodial(Ljava/lang/String;)V
    .registers 10
    .parameter "number"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 1775
    const-string v4, "videodial"

    #@5
    invoke-direct {p0, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@8
    .line 1778
    const/4 v2, 0x0

    #@9
    .line 1780
    .local v2, intent:Landroid/content/Intent;
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isLTEVTSupported()Z

    #@c
    move-result v4

    #@d
    if-eqz v4, :cond_33

    #@f
    .line 1781
    new-instance v2, Landroid/content/Intent;

    #@11
    .end local v2           #intent:Landroid/content/Intent;
    const-string v4, "com.lge.ims.action.VT_REQUEST"

    #@13
    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@16
    .line 1783
    .restart local v2       #intent:Landroid/content/Intent;
    new-array v0, v6, [Ljava/lang/String;

    #@18
    .line 1784
    .local v0, displayName:[Ljava/lang/String;
    new-array v3, v6, [Ljava/lang/String;

    #@1a
    .line 1786
    .local v3, phoneNumbers:[Ljava/lang/String;
    aput-object v7, v0, v5

    #@1c
    .line 1787
    aput-object p1, v3, v5

    #@1e
    .line 1789
    const-string v4, "com.lge.ims.extra.VT_DISPLAY_NAME_LIST"

    #@20
    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@23
    .line 1790
    const-string v4, "com.lge.ims.extra.VT_PHONE_NUMBER_LIST"

    #@25
    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    #@28
    .line 1796
    .end local v0           #displayName:[Ljava/lang/String;
    .end local v3           #phoneNumbers:[Ljava/lang/String;
    :goto_28
    const/high16 v4, 0x1000

    #@2a
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@2d
    .line 1799
    :try_start_2d
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@2f
    invoke-virtual {v4, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->startActivity(Landroid/content/Intent;)V
    :try_end_32
    .catch Ljava/lang/Exception; {:try_start_2d .. :try_end_32} :catch_41

    #@32
    .line 1803
    :goto_32
    return-void

    #@33
    .line 1793
    :cond_33
    new-instance v2, Landroid/content/Intent;

    #@35
    .end local v2           #intent:Landroid/content/Intent;
    const-string v4, "com.lge.ims.action.VT_REQUEST"

    #@37
    const-string v5, "tel"

    #@39
    invoke-static {v5, p1, v7}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    #@3c
    move-result-object v5

    #@3d
    invoke-direct {v2, v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@40
    .restart local v2       #intent:Landroid/content/Intent;
    goto :goto_28

    #@41
    .line 1800
    :catch_41
    move-exception v1

    #@42
    .line 1801
    .local v1, e:Ljava/lang/Exception;
    const-string v4, "HeadsetStateMachine"

    #@44
    const-string v5, "Cannot start activity for VT"

    #@46
    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@49
    goto :goto_32
.end method


# virtual methods
.method native atResponseCodeNative(II)Z
.end method

.method native atResponseStringNative(Ljava/lang/String;)Z
.end method

.method public cleanup()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 293
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@3
    if-eqz v1, :cond_1a

    #@5
    .line 295
    const-string v1, "HeadsetStateMachine"

    #@7
    const-string v2, "Unbinding service..."

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 297
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mConnection:Landroid/content/ServiceConnection;

    #@e
    monitor-enter v2

    #@f
    .line 299
    const/4 v1, 0x0

    #@10
    :try_start_10
    iput-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneProxy:Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@12
    .line 300
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@14
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mConnection:Landroid/content/ServiceConnection;

    #@16
    invoke-virtual {v1, v3}, Lcom/android/bluetooth/hfp/HeadsetService;->unbindService(Landroid/content/ServiceConnection;)V
    :try_end_19
    .catchall {:try_start_10 .. :try_end_19} :catchall_44
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_19} :catch_3b

    #@19
    .line 304
    :goto_19
    :try_start_19
    monitor-exit v2
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_44

    #@1a
    .line 306
    :cond_1a
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@1c
    if-eqz v1, :cond_28

    #@1e
    .line 307
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@20
    invoke-virtual {v1, v4}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->listenForPhoneState(Z)V

    #@23
    .line 308
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhoneState:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@25
    invoke-virtual {v1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->cleanup()V

    #@28
    .line 310
    :cond_28
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@2a
    if-eqz v1, :cond_31

    #@2c
    .line 311
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@2e
    invoke-virtual {v1}, Lcom/android/bluetooth/hfp/AtPhonebook;->cleanup()V

    #@31
    .line 313
    :cond_31
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mNativeAvailable:Z

    #@33
    if-eqz v1, :cond_3a

    #@35
    .line 314
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->cleanupNative()V

    #@38
    .line 315
    iput-boolean v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mNativeAvailable:Z

    #@3a
    .line 317
    :cond_3a
    return-void

    #@3b
    .line 301
    :catch_3b
    move-exception v0

    #@3c
    .line 302
    .local v0, re:Ljava/lang/Exception;
    :try_start_3c
    const-string v1, "HeadsetStateMachine"

    #@3e
    const-string v3, "Error unbinding from IBluetoothHeadsetPhone"

    #@40
    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@43
    goto :goto_19

    #@44
    .line 304
    .end local v0           #re:Ljava/lang/Exception;
    :catchall_44
    move-exception v1

    #@45
    monitor-exit v2
    :try_end_46
    .catchall {:try_start_3c .. :try_end_46} :catchall_44

    #@46
    throw v1
.end method

.method public doQuit()V
    .registers 1

    #@0
    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->quitNow()V

    #@3
    .line 290
    return-void
.end method

.method getAudioState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 1274
    monitor-enter p0

    #@1
    .line 1275
    :try_start_1
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@3
    if-eqz v0, :cond_d

    #@5
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@7
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@a
    move-result v0

    #@b
    if-nez v0, :cond_11

    #@d
    .line 1276
    :cond_d
    const/16 v0, 0xa

    #@f
    monitor-exit p0

    #@10
    .line 1279
    :goto_10
    return v0

    #@11
    .line 1278
    :cond_11
    monitor-exit p0
    :try_end_12
    .catchall {:try_start_1 .. :try_end_12} :catchall_15

    #@12
    .line 1279
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioState:I

    #@14
    goto :goto_10

    #@15
    .line 1278
    :catchall_15
    move-exception v0

    #@16
    :try_start_16
    monitor-exit p0
    :try_end_17
    .catchall {:try_start_16 .. :try_end_17} :catchall_15

    #@17
    throw v0
.end method

.method getConnectedDevices()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1243
    new-instance v0, Ljava/util/ArrayList;

    #@2
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1244
    .local v0, devices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    monitor-enter p0

    #@6
    .line 1245
    :try_start_6
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isConnected()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_11

    #@c
    .line 1246
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@e
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@11
    .line 1248
    :cond_11
    monitor-exit p0

    #@12
    .line 1249
    return-object v0

    #@13
    .line 1248
    :catchall_13
    move-exception v1

    #@14
    monitor-exit p0
    :try_end_15
    .catchall {:try_start_6 .. :try_end_15} :catchall_13

    #@15
    throw v1
.end method

.method getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 7
    .parameter "device"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 1211
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@5
    move-result-object v3

    #@6
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mDisconnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;

    #@8
    if-ne v3, v4, :cond_b

    #@a
    .line 1237
    :goto_a
    return v1

    #@b
    .line 1215
    :cond_b
    monitor-enter p0

    #@c
    .line 1216
    :try_start_c
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@f
    move-result-object v0

    #@10
    .line 1217
    .local v0, currentState:Lcom/android/internal/util/IState;
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPending:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;

    #@12
    if-ne v0, v3, :cond_46

    #@14
    .line 1218
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@16
    if-eqz v3, :cond_23

    #@18
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@1a
    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_23

    #@20
    .line 1219
    monitor-exit p0

    #@21
    move v1, v2

    #@22
    goto :goto_a

    #@23
    .line 1221
    :cond_23
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@25
    if-eqz v3, :cond_35

    #@27
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@29
    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@2c
    move-result v3

    #@2d
    if-eqz v3, :cond_35

    #@2f
    .line 1222
    const/4 v1, 0x3

    #@30
    monitor-exit p0

    #@31
    goto :goto_a

    #@32
    .line 1239
    .end local v0           #currentState:Lcom/android/internal/util/IState;
    :catchall_32
    move-exception v1

    #@33
    monitor-exit p0
    :try_end_34
    .catchall {:try_start_c .. :try_end_34} :catchall_32

    #@34
    throw v1

    #@35
    .line 1224
    .restart local v0       #currentState:Lcom/android/internal/util/IState;
    :cond_35
    :try_start_35
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

    #@37
    if-eqz v3, :cond_44

    #@39
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mIncomingDevice:Landroid/bluetooth/BluetoothDevice;

    #@3b
    invoke-virtual {v3, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_44

    #@41
    .line 1225
    monitor-exit p0

    #@42
    move v1, v2

    #@43
    goto :goto_a

    #@44
    .line 1227
    :cond_44
    monitor-exit p0

    #@45
    goto :goto_a

    #@46
    .line 1230
    :cond_46
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mConnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;

    #@48
    if-eq v0, v2, :cond_4e

    #@4a
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioOn:Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

    #@4c
    if-ne v0, v2, :cond_5b

    #@4e
    .line 1231
    :cond_4e
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@50
    invoke-virtual {v2, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@53
    move-result v2

    #@54
    if-eqz v2, :cond_59

    #@56
    .line 1232
    const/4 v1, 0x2

    #@57
    monitor-exit p0

    #@58
    goto :goto_a

    #@59
    .line 1234
    :cond_59
    monitor-exit p0

    #@5a
    goto :goto_a

    #@5b
    .line 1236
    :cond_5b
    const-string v2, "HeadsetStateMachine"

    #@5d
    new-instance v3, Ljava/lang/StringBuilder;

    #@5f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@62
    const-string v4, "Bad currentState: "

    #@64
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@67
    move-result-object v3

    #@68
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v3

    #@6c
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object v3

    #@70
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@73
    .line 1237
    monitor-exit p0
    :try_end_74
    .catchall {:try_start_35 .. :try_end_74} :catchall_32

    #@74
    goto :goto_a
.end method

.method getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 10
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 1419
    new-instance v3, Ljava/util/ArrayList;

    #@2
    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 1420
    .local v3, deviceList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    iget-object v7, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@7
    invoke-virtual {v7}, Landroid/bluetooth/BluetoothAdapter;->getBondedDevices()Ljava/util/Set;

    #@a
    move-result-object v0

    #@b
    .line 1422
    .local v0, bondedDevices:Ljava/util/Set;,"Ljava/util/Set<Landroid/bluetooth/BluetoothDevice;>;"
    monitor-enter p0

    #@c
    .line 1423
    :try_start_c
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@f
    move-result-object v6

    #@10
    .local v6, i$:Ljava/util/Iterator;
    :cond_10
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    #@13
    move-result v7

    #@14
    if-eqz v7, :cond_3a

    #@16
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@19
    move-result-object v2

    #@1a
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@1c
    .line 1424
    .local v2, device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getUuids()[Landroid/os/ParcelUuid;

    #@1f
    move-result-object v4

    #@20
    .line 1425
    .local v4, featureUuids:[Landroid/os/ParcelUuid;
    sget-object v7, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->HEADSET_UUIDS:[Landroid/os/ParcelUuid;

    #@22
    invoke-static {v4, v7}, Landroid/bluetooth/BluetoothUuid;->containsAnyUuid([Landroid/os/ParcelUuid;[Landroid/os/ParcelUuid;)Z

    #@25
    move-result v7

    #@26
    if-eqz v7, :cond_10

    #@28
    .line 1428
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@2b
    move-result v1

    #@2c
    .line 1429
    .local v1, connectionState:I
    const/4 v5, 0x0

    #@2d
    .local v5, i:I
    :goto_2d
    array-length v7, p1

    #@2e
    if-ge v5, v7, :cond_10

    #@30
    .line 1430
    aget v7, p1, v5

    #@32
    if-ne v1, v7, :cond_37

    #@34
    .line 1431
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@37
    .line 1429
    :cond_37
    add-int/lit8 v5, v5, 0x1

    #@39
    goto :goto_2d

    #@3a
    .line 1435
    .end local v1           #connectionState:I
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #featureUuids:[Landroid/os/ParcelUuid;
    .end local v5           #i:I
    :cond_3a
    monitor-exit p0

    #@3b
    .line 1436
    return-object v3

    #@3c
    .line 1435
    .end local v6           #i$:Ljava/util/Iterator;
    :catchall_3c
    move-exception v7

    #@3d
    monitor-exit p0
    :try_end_3e
    .catchall {:try_start_c .. :try_end_3e} :catchall_3c

    #@3e
    throw v7
.end method

.method public handleAccessPermissionResult(Landroid/content/Intent;)V
    .registers 8
    .parameter "intent"

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 2580
    const-string v2, "handleAccessPermissionResult"

    #@4
    invoke-direct {p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@7
    .line 2581
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@9
    if-eqz v2, :cond_55

    #@b
    .line 2582
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@d
    invoke-virtual {v2}, Lcom/android/bluetooth/hfp/AtPhonebook;->getCheckingAccessPermission()Z

    #@10
    move-result v2

    #@11
    if-nez v2, :cond_14

    #@13
    .line 2615
    :goto_13
    return-void

    #@14
    .line 2585
    :cond_14
    const/4 v1, 0x0

    #@15
    .line 2586
    .local v1, atCommandResult:I
    const/4 v0, 0x0

    #@16
    .line 2591
    .local v0, atCommandErrorCode:I
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    const-string v3, "android.bluetooth.device.action.CONNECTION_ACCESS_REPLY"

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1f
    move-result v2

    #@20
    if-eqz v2, :cond_3e

    #@22
    .line 2592
    const-string v2, "android.bluetooth.device.extra.CONNECTION_ACCESS_RESULT"

    #@24
    const/4 v3, 0x2

    #@25
    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@28
    move-result v2

    #@29
    if-ne v2, v5, :cond_3e

    #@2b
    .line 2595
    const-string v2, "android.bluetooth.device.extra.ALWAYS_ALLOWED"

    #@2d
    invoke-virtual {p1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@30
    move-result v2

    #@31
    if-eqz v2, :cond_38

    #@33
    .line 2596
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@35
    invoke-virtual {v2, v5}, Landroid/bluetooth/BluetoothDevice;->setTrust(Z)Z

    #@38
    .line 2598
    :cond_38
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@3a
    invoke-virtual {v2}, Lcom/android/bluetooth/hfp/AtPhonebook;->processCpbrCommand()I

    #@3d
    move-result v1

    #@3e
    .line 2601
    :cond_3e
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@40
    const/4 v3, -0x1

    #@41
    invoke-virtual {v2, v3}, Lcom/android/bluetooth/hfp/AtPhonebook;->setCpbrIndex(I)V

    #@44
    .line 2602
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mPhonebook:Lcom/android/bluetooth/hfp/AtPhonebook;

    #@46
    invoke-virtual {v2, v4}, Lcom/android/bluetooth/hfp/AtPhonebook;->setCheckingAccessPermission(Z)V

    #@49
    .line 2604
    if-ltz v1, :cond_4f

    #@4b
    .line 2605
    invoke-virtual {p0, v1, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@4e
    goto :goto_13

    #@4f
    .line 2608
    :cond_4f
    const-string v2, "handleAccessPermissionResult - RESULT_NONE"

    #@51
    invoke-direct {p0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@54
    goto :goto_13

    #@55
    .line 2612
    .end local v0           #atCommandErrorCode:I
    .end local v1           #atCommandResult:I
    :cond_55
    const-string v2, "HeadsetStateMachine"

    #@57
    const-string v3, "Phonebook handle null"

    #@59
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    .line 2613
    invoke-virtual {p0, v4, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@5f
    goto :goto_13
.end method

.method declared-synchronized initiateScoUsingVirtualVoiceCall()Z
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 1574
    monitor-enter p0

    #@3
    :try_start_3
    const-string v1, "initiateScoUsingVirtualVoiceCall: Received"

    #@5
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@8
    .line 1577
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isInCall()Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_12

    #@e
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVoiceRecognitionStarted:Z

    #@10
    if-eqz v1, :cond_1b

    #@12
    .line 1578
    :cond_12
    const-string v1, "HeadsetStateMachine"

    #@14
    const-string v2, "initiateScoUsingVirtualVoiceCall: Call in progress."

    #@16
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_53

    #@19
    .line 1594
    :goto_19
    monitor-exit p0

    #@1a
    return v0

    #@1b
    .line 1583
    :cond_1b
    :try_start_1b
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetCallState;

    #@1d
    const/4 v1, 0x0

    #@1e
    const/4 v2, 0x0

    #@1f
    const/4 v3, 0x2

    #@20
    const-string v4, ""

    #@22
    const/4 v5, 0x0

    #@23
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/hfp/HeadsetCallState;-><init>(IIILjava/lang/String;I)V

    #@26
    const/4 v1, 0x1

    #@27
    invoke-direct {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processCallState(Lcom/android/bluetooth/hfp/HeadsetCallState;Z)V

    #@2a
    .line 1585
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetCallState;

    #@2c
    const/4 v1, 0x0

    #@2d
    const/4 v2, 0x0

    #@2e
    const/4 v3, 0x3

    #@2f
    const-string v4, ""

    #@31
    const/4 v5, 0x0

    #@32
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/hfp/HeadsetCallState;-><init>(IIILjava/lang/String;I)V

    #@35
    const/4 v1, 0x1

    #@36
    invoke-direct {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processCallState(Lcom/android/bluetooth/hfp/HeadsetCallState;Z)V

    #@39
    .line 1587
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetCallState;

    #@3b
    const/4 v1, 0x1

    #@3c
    const/4 v2, 0x0

    #@3d
    const/4 v3, 0x6

    #@3e
    const-string v4, ""

    #@40
    const/4 v5, 0x0

    #@41
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/hfp/HeadsetCallState;-><init>(IIILjava/lang/String;I)V

    #@44
    const/4 v1, 0x1

    #@45
    invoke-direct {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processCallState(Lcom/android/bluetooth/hfp/HeadsetCallState;Z)V

    #@48
    .line 1589
    const/4 v0, 0x1

    #@49
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->setVirtualCallInProgress(Z)V

    #@4c
    .line 1592
    const-string v0, "initiateScoUsingVirtualVoiceCall: Done"

    #@4e
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V
    :try_end_51
    .catchall {:try_start_1b .. :try_end_51} :catchall_53

    #@51
    move v0, v6

    #@52
    .line 1594
    goto :goto_19

    #@53
    .line 1574
    :catchall_53
    move-exception v0

    #@54
    monitor-exit p0

    #@55
    throw v0
.end method

.method isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 1257
    monitor-enter p0

    #@1
    .line 1264
    :try_start_1
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@4
    move-result-object v0

    #@5
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioOn:Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

    #@7
    if-ne v0, v1, :cond_1a

    #@9
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mCurrentDevice:Landroid/bluetooth/BluetoothDevice;

    #@b
    invoke-virtual {v0, p1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v0

    #@f
    if-eqz v0, :cond_1a

    #@11
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioState:I

    #@13
    const/16 v1, 0xa

    #@15
    if-eq v0, v1, :cond_1a

    #@17
    .line 1267
    const/4 v0, 0x1

    #@18
    monitor-exit p0

    #@19
    .line 1270
    :goto_19
    return v0

    #@1a
    .line 1269
    :cond_1a
    monitor-exit p0

    #@1b
    .line 1270
    const/4 v0, 0x0

    #@1c
    goto :goto_19

    #@1d
    .line 1269
    :catchall_1d
    move-exception v0

    #@1e
    monitor-exit p0
    :try_end_1f
    .catchall {:try_start_1 .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method isAudioOn()Z
    .registers 3

    #@0
    .prologue
    .line 1253
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@3
    move-result-object v0

    #@4
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioOn:Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

    #@6
    if-ne v0, v1, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method public isBtVrSupported()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2441
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->VR_SUPPORT_AG:Z

    #@3
    if-ne v1, v0, :cond_a

    #@5
    iget-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->VR_SUPPORT_HF:Z

    #@7
    if-ne v1, v0, :cond_a

    #@9
    .line 2445
    :goto_9
    return v0

    #@a
    .line 2444
    :cond_a
    const-string v0, "HeadsetStateMachine"

    #@c
    const-string v1, "[BTUI] isBtVrSupported() : AG or HF does not support BT VR."

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 2445
    const/4 v0, 0x0

    #@12
    goto :goto_9
.end method

.method isConnected()Z
    .registers 3

    #@0
    .prologue
    .line 2547
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getCurrentState()Lcom/android/internal/util/IState;

    #@3
    move-result-object v0

    #@4
    .line 2548
    .local v0, currentState:Lcom/android/internal/util/IState;
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mConnected:Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;

    #@6
    if-eq v0, v1, :cond_c

    #@8
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mAudioOn:Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

    #@a
    if-ne v0, v1, :cond_e

    #@c
    :cond_c
    const/4 v1, 0x1

    #@d
    :goto_d
    return v1

    #@e
    :cond_e
    const/4 v1, 0x0

    #@f
    goto :goto_d
.end method

.method okToConnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 7
    .parameter "device"

    #@0
    .prologue
    .line 2552
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 2553
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@6
    invoke-virtual {v3, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@9
    move-result v1

    #@a
    .line 2554
    .local v1, priority:I
    const/4 v2, 0x0

    #@b
    .line 2556
    .local v2, ret:Z
    if-eqz v0, :cond_18

    #@d
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->isQuietModeEnabled()Z

    #@10
    move-result v3

    #@11
    const/4 v4, 0x1

    #@12
    if-ne v3, v4, :cond_1a

    #@14
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@16
    if-nez v3, :cond_1a

    #@18
    .line 2559
    :cond_18
    const/4 v2, 0x0

    #@19
    .line 2569
    :cond_19
    :goto_19
    return v2

    #@1a
    .line 2564
    :cond_1a
    if-gtz v1, :cond_27

    #@1c
    const/4 v3, -0x1

    #@1d
    if-ne v3, v1, :cond_19

    #@1f
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    #@22
    move-result v3

    #@23
    const/16 v4, 0xa

    #@25
    if-eq v3, v4, :cond_19

    #@27
    .line 2567
    :cond_27
    const/4 v2, 0x1

    #@28
    goto :goto_19
.end method

.method setVirtualCallInProgress(Z)V
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 1565
    iput-boolean p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->mVirtualCallStarted:Z

    #@2
    .line 1566
    return-void
.end method

.method declared-synchronized terminateScoUsingVirtualVoiceCall()Z
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v0, 0x0

    #@2
    .line 1599
    monitor-enter p0

    #@3
    :try_start_3
    const-string v1, "terminateScoUsingVirtualVoiceCall: Received"

    #@5
    invoke-direct {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V

    #@8
    .line 1602
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isVirtualCallInProgress()Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_17

    #@e
    .line 1603
    const-string v1, "HeadsetStateMachine"

    #@10
    const-string v2, "terminateScoUsingVirtualVoiceCall:No present call to terminate"

    #@12
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_15
    .catchall {:try_start_3 .. :try_end_15} :catchall_31

    #@15
    .line 1616
    :goto_15
    monitor-exit p0

    #@16
    return v0

    #@17
    .line 1609
    :cond_17
    :try_start_17
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetCallState;

    #@19
    const/4 v1, 0x0

    #@1a
    const/4 v2, 0x0

    #@1b
    const/4 v3, 0x6

    #@1c
    const-string v4, ""

    #@1e
    const/4 v5, 0x0

    #@1f
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/hfp/HeadsetCallState;-><init>(IIILjava/lang/String;I)V

    #@22
    const/4 v1, 0x1

    #@23
    invoke-direct {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->processCallState(Lcom/android/bluetooth/hfp/HeadsetCallState;Z)V

    #@26
    .line 1611
    const/4 v0, 0x0

    #@27
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->setVirtualCallInProgress(Z)V

    #@2a
    .line 1614
    const-string v0, "terminateScoUsingVirtualVoiceCall: Done"

    #@2c
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->log(Ljava/lang/String;)V
    :try_end_2f
    .catchall {:try_start_17 .. :try_end_2f} :catchall_31

    #@2f
    move v0, v6

    #@30
    .line 1616
    goto :goto_15

    #@31
    .line 1599
    :catchall_31
    move-exception v0

    #@32
    monitor-exit p0

    #@33
    throw v0
.end method
