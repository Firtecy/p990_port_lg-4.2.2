.class Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;
.super Lcom/android/internal/util/State;
.source "HeadsetStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hfp/HeadsetStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Connected"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 700
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 700
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;-><init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@3
    return-void
.end method

.method private processAudioEvent(ILandroid/bluetooth/BluetoothDevice;)V
    .registers 7
    .parameter "state"
    .parameter "device"

    #@0
    .prologue
    const/16 v3, 0xc

    #@2
    const/16 v2, 0xb

    #@4
    .line 931
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@6
    invoke-static {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@9
    move-result-object v0

    #@a
    invoke-virtual {v0, p2}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v0

    #@e
    if-nez v0, :cond_29

    #@10
    .line 932
    const-string v0, "HeadsetStateMachine"

    #@12
    new-instance v1, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v2, "Audio changed on disconnected device: "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 955
    :goto_28
    return-void

    #@29
    .line 936
    :cond_29
    packed-switch p1, :pswitch_data_7c

    #@2c
    .line 952
    const-string v0, "HeadsetStateMachine"

    #@2e
    new-instance v1, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v2, "Audio State Device: "

    #@35
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v1

    #@3d
    const-string v2, " bad state: "

    #@3f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v1

    #@43
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@46
    move-result-object v1

    #@47
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v1

    #@4b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    goto :goto_28

    #@4f
    .line 939
    :pswitch_4f
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@51
    invoke-static {v0, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$7002(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)I

    #@54
    .line 940
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@56
    invoke-static {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/media/AudioManager;

    #@59
    move-result-object v0

    #@5a
    const/4 v1, 0x1

    #@5b
    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setBluetoothScoOn(Z)V

    #@5e
    .line 941
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@60
    invoke-static {v0, p2, v3, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$7100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@63
    .line 943
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@65
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@67
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$7200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$AudioOn;

    #@6a
    move-result-object v1

    #@6b
    invoke-static {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$7300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V

    #@6e
    goto :goto_28

    #@6f
    .line 946
    :pswitch_6f
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@71
    invoke-static {v0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$7002(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)I

    #@74
    .line 947
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@76
    const/16 v1, 0xa

    #@78
    invoke-static {v0, p2, v2, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$7100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@7b
    goto :goto_28

    #@7c
    .line 936
    :pswitch_data_7c
    .packed-switch 0x1
        :pswitch_6f
        :pswitch_4f
    .end packed-switch
.end method

.method private processConnectionEvent(ILandroid/bluetooth/BluetoothDevice;)V
    .registers 7
    .parameter "state"
    .parameter "device"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 904
    packed-switch p1, :pswitch_data_7c

    #@4
    .line 924
    :pswitch_4
    const-string v0, "HeadsetStateMachine"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "Connection State Device: "

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, " bad state: "

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 927
    :goto_26
    return-void

    #@27
    .line 906
    :pswitch_27
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@29
    invoke-static {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@2c
    move-result-object v0

    #@2d
    invoke-virtual {v0, p2}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_5e

    #@33
    .line 907
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@35
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@37
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@3a
    move-result-object v1

    #@3b
    const/4 v2, 0x2

    #@3c
    invoke-static {v0, v1, v3, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@3f
    .line 909
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@41
    monitor-enter v1

    #@42
    .line 910
    :try_start_42
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@44
    const/4 v2, 0x0

    #@45
    invoke-static {v0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$902(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@48
    .line 912
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@4a
    const/4 v2, 0x0

    #@4b
    invoke-static {v0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$3502(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Z)Z

    #@4e
    .line 914
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@50
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@52
    invoke-static {v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$3600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$Disconnected;

    #@55
    move-result-object v2

    #@56
    invoke-static {v0, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$6900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V

    #@59
    .line 915
    monitor-exit v1

    #@5a
    goto :goto_26

    #@5b
    :catchall_5b
    move-exception v0

    #@5c
    monitor-exit v1
    :try_end_5d
    .catchall {:try_start_42 .. :try_end_5d} :catchall_5b

    #@5d
    throw v0

    #@5e
    .line 917
    :cond_5e
    const-string v0, "HeadsetStateMachine"

    #@60
    new-instance v1, Ljava/lang/StringBuilder;

    #@62
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@65
    const-string v2, "Disconnected from unknown device: "

    #@67
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v1

    #@6b
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v1

    #@6f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@72
    move-result-object v1

    #@73
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@76
    goto :goto_26

    #@77
    .line 921
    :pswitch_77
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->processSlcConnected()V

    #@7a
    goto :goto_26

    #@7b
    .line 904
    nop

    #@7c
    :pswitch_data_7c
    .packed-switch 0x0
        :pswitch_27
        :pswitch_4
        :pswitch_4
        :pswitch_77
    .end packed-switch
.end method

.method private processSlcConnected()V
    .registers 4

    #@0
    .prologue
    .line 958
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$7400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@5
    move-result-object v1

    #@6
    if-eqz v1, :cond_2c

    #@8
    .line 964
    :try_start_8
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@a
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@d
    move-result-object v1

    #@e
    const/4 v2, 0x1

    #@f
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->listenForPhoneState(Z)V

    #@12
    .line 965
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@14
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$7400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@17
    move-result-object v1

    #@18
    invoke-interface {v1}, Landroid/bluetooth/IBluetoothHeadsetPhone;->queryPhoneState()Z
    :try_end_1b
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_1b} :catch_1c

    #@1b
    .line 973
    :goto_1b
    return-void

    #@1c
    .line 966
    :catch_1c
    move-exception v0

    #@1d
    .line 967
    .local v0, e:Landroid/os/RemoteException;
    const-string v1, "HeadsetStateMachine"

    #@1f
    new-instance v2, Ljava/lang/Throwable;

    #@21
    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    #@24
    invoke-static {v2}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    #@27
    move-result-object v2

    #@28
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_1b

    #@2c
    .line 970
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_2c
    const-string v1, "HeadsetStateMachine"

    #@2e
    const-string v2, "Handsfree phone proxy null for query phone state"

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    goto :goto_1b
.end method


# virtual methods
.method public enter()V
    .registers 4

    #@0
    .prologue
    .line 703
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Enter Connected: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@f
    invoke-static {v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$4500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/os/Message;

    #@12
    move-result-object v2

    #@13
    iget v2, v2, Landroid/os/Message;->what:I

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v1

    #@1d
    invoke-static {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V

    #@20
    .line 704
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 11
    .parameter "message"

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v4, 0x1

    #@2
    const/4 v5, 0x0

    #@3
    .line 708
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@5
    new-instance v6, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v7, "Connected process message: "

    #@c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v6

    #@10
    iget v7, p1, Landroid/os/Message;->what:I

    #@12
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v6

    #@16
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@19
    move-result-object v6

    #@1a
    invoke-static {v3, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V

    #@1d
    .line 710
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@1f
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@22
    move-result-object v3

    #@23
    if-nez v3, :cond_2d

    #@25
    .line 711
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@27
    const-string v4, "ERROR: mCurrentDevice is null in Connected"

    #@29
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V

    #@2c
    .line 899
    :goto_2c
    return v5

    #@2d
    .line 716
    :cond_2d
    const/4 v2, 0x1

    #@2e
    .line 717
    .local v2, retValue:Z
    iget v3, p1, Landroid/os/Message;->what:I

    #@30
    sparse-switch v3, :sswitch_data_2ba

    #@33
    goto :goto_2c

    #@34
    .line 720
    :sswitch_34
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@36
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@38
    .line 721
    .local v0, device:Landroid/bluetooth/BluetoothDevice;
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@3a
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@3d
    move-result-object v3

    #@3e
    invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@41
    move-result v3

    #@42
    if-eqz v3, :cond_46

    #@44
    .end local v0           #device:Landroid/bluetooth/BluetoothDevice;
    :cond_44
    :goto_44
    move v5, v2

    #@45
    .line 899
    goto :goto_2c

    #@46
    .line 725
    .restart local v0       #device:Landroid/bluetooth/BluetoothDevice;
    :cond_46
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@48
    invoke-static {v3, v0, v4, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@4b
    .line 727
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@4d
    iget-object v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@4f
    iget-object v7, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@51
    invoke-static {v7}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@54
    move-result-object v7

    #@55
    invoke-static {v6, v7}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)[B

    #@58
    move-result-object v6

    #@59
    invoke-static {v3, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;[B)Z

    #@5c
    move-result v3

    #@5d
    if-nez v3, :cond_65

    #@5f
    .line 728
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@61
    invoke-static {v3, v0, v5, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@64
    goto :goto_44

    #@65
    .line 733
    :cond_65
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@67
    monitor-enter v4

    #@68
    .line 734
    :try_start_68
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@6a
    invoke-static {v3, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1002(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@6d
    .line 735
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@6f
    iget-object v5, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@71
    invoke-static {v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;

    #@74
    move-result-object v5

    #@75
    invoke-static {v3, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$4600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V

    #@78
    .line 736
    monitor-exit v4

    #@79
    goto :goto_44

    #@7a
    :catchall_7a
    move-exception v3

    #@7b
    monitor-exit v4
    :try_end_7c
    .catchall {:try_start_68 .. :try_end_7c} :catchall_7a

    #@7c
    throw v3

    #@7d
    .line 741
    .end local v0           #device:Landroid/bluetooth/BluetoothDevice;
    :sswitch_7d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@7f
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@81
    .line 742
    .restart local v0       #device:Landroid/bluetooth/BluetoothDevice;
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@83
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@86
    move-result-object v3

    #@87
    invoke-virtual {v3, v0}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@8a
    move-result v3

    #@8b
    if-eqz v3, :cond_44

    #@8d
    .line 745
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@8f
    const/4 v4, 0x3

    #@90
    invoke-static {v3, v0, v4, v8}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@93
    .line 747
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@95
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@97
    invoke-static {v4, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)[B

    #@9a
    move-result-object v4

    #@9b
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$2300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;[B)Z

    #@9e
    move-result v3

    #@9f
    if-nez v3, :cond_a7

    #@a1
    .line 748
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@a3
    invoke-static {v3, v0, v8, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@a6
    goto :goto_44

    #@a7
    .line 752
    :cond_a7
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@a9
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@ab
    invoke-static {v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Lcom/android/bluetooth/hfp/HeadsetStateMachine$Pending;

    #@ae
    move-result-object v4

    #@af
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$4700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/internal/util/IState;)V

    #@b2
    goto :goto_44

    #@b3
    .line 758
    .end local v0           #device:Landroid/bluetooth/BluetoothDevice;
    :sswitch_b3
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@b5
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@b7
    iget-object v5, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@b9
    invoke-static {v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Landroid/bluetooth/BluetoothDevice;

    #@bc
    move-result-object v5

    #@bd
    invoke-static {v4, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/BluetoothDevice;)[B

    #@c0
    move-result-object v4

    #@c1
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$4800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;[B)Z

    #@c4
    goto :goto_44

    #@c5
    .line 761
    :sswitch_c5
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@c7
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$4900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V

    #@ca
    goto/16 :goto_44

    #@cc
    .line 764
    :sswitch_cc
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@ce
    invoke-static {v3, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$4900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V

    #@d1
    goto/16 :goto_44

    #@d3
    .line 767
    :sswitch_d3
    iget-object v6, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@d5
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@d7
    check-cast v3, Lcom/android/bluetooth/hfp/HeadsetCallState;

    #@d9
    iget v7, p1, Landroid/os/Message;->arg1:I

    #@db
    if-ne v7, v4, :cond_e2

    #@dd
    :goto_dd
    invoke-static {v6, v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetCallState;Z)V

    #@e0
    goto/16 :goto_44

    #@e2
    :cond_e2
    move v4, v5

    #@e3
    goto :goto_dd

    #@e4
    .line 770
    :sswitch_e4
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@e6
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e8
    check-cast v3, Landroid/content/Intent;

    #@ea
    invoke-static {v4, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/content/Intent;)V

    #@ed
    goto/16 :goto_44

    #@ef
    .line 773
    :sswitch_ef
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@f1
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f3
    check-cast v3, Ljava/lang/Boolean;

    #@f5
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    #@f8
    move-result v3

    #@f9
    invoke-static {v4, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$1800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Z)V

    #@fc
    goto/16 :goto_44

    #@fe
    .line 776
    :sswitch_fe
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@100
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@102
    check-cast v3, Lcom/android/bluetooth/hfp/HeadsetDeviceState;

    #@104
    invoke-static {v4, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetDeviceState;)V

    #@107
    goto/16 :goto_44

    #@109
    .line 779
    :sswitch_109
    iget-object v4, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@10b
    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@10d
    check-cast v3, Lcom/android/bluetooth/hfp/HeadsetClccResponse;

    #@10f
    invoke-static {v4, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Lcom/android/bluetooth/hfp/HeadsetClccResponse;)V

    #@112
    goto/16 :goto_44

    #@114
    .line 782
    :sswitch_114
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@116
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Z

    #@119
    move-result v3

    #@11a
    if-eqz v3, :cond_44

    #@11c
    .line 783
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@11e
    invoke-static {v3, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5202(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Z)Z

    #@121
    .line 784
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@123
    invoke-virtual {v3, v5, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@126
    goto/16 :goto_44

    #@128
    .line 788
    :sswitch_128
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@12a
    invoke-virtual {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->initiateScoUsingVirtualVoiceCall()Z

    #@12d
    goto/16 :goto_44

    #@12f
    .line 791
    :sswitch_12f
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@131
    invoke-virtual {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->terminateScoUsingVirtualVoiceCall()Z

    #@134
    goto/16 :goto_44

    #@136
    .line 794
    :sswitch_136
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@138
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)Z

    #@13b
    move-result v3

    #@13c
    if-eqz v3, :cond_44

    #@13e
    .line 795
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@140
    invoke-static {v3, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5302(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Z)Z

    #@143
    .line 796
    const-string v3, "HeadsetStateMachine"

    #@145
    const-string v4, "Timeout waiting for voice recognition to start"

    #@147
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@14a
    .line 797
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@14c
    invoke-virtual {v3, v5, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@14f
    goto/16 :goto_44

    #@151
    .line 801
    :sswitch_151
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@153
    check-cast v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;

    #@155
    .line 803
    .local v1, event:Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@157
    new-instance v4, Ljava/lang/StringBuilder;

    #@159
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15c
    const-string v5, "event type: "

    #@15e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@161
    move-result-object v4

    #@162
    iget v5, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->type:I

    #@164
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@167
    move-result-object v4

    #@168
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16b
    move-result-object v4

    #@16c
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V

    #@16f
    .line 805
    iget v3, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->type:I

    #@171
    packed-switch v3, :pswitch_data_2f8

    #@174
    .line 892
    const-string v3, "HeadsetStateMachine"

    #@176
    new-instance v4, Ljava/lang/StringBuilder;

    #@178
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@17b
    const-string v5, "Unknown stack event: "

    #@17d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@180
    move-result-object v4

    #@181
    iget v5, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->type:I

    #@183
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@186
    move-result-object v4

    #@187
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18a
    move-result-object v4

    #@18b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18e
    goto/16 :goto_44

    #@190
    .line 807
    :pswitch_190
    const-string v3, "HeadsetStateMachine"

    #@192
    const-string v4, "EVENT_TYPE_CONNECTION_STATE_CHANGED"

    #@194
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@197
    .line 809
    iget-object v3, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->device:Landroid/bluetooth/BluetoothDevice;

    #@199
    if-eqz v3, :cond_44

    #@19b
    .line 810
    iget v3, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@19d
    iget-object v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->device:Landroid/bluetooth/BluetoothDevice;

    #@19f
    invoke-direct {p0, v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->processConnectionEvent(ILandroid/bluetooth/BluetoothDevice;)V

    #@1a2
    goto/16 :goto_44

    #@1a4
    .line 815
    :pswitch_1a4
    const-string v3, "HeadsetStateMachine"

    #@1a6
    const-string v4, "EVENT_TYPE_AUDIO_STATE_CHANGED"

    #@1a8
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1ab
    .line 817
    iget-object v3, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->device:Landroid/bluetooth/BluetoothDevice;

    #@1ad
    if-eqz v3, :cond_44

    #@1af
    .line 818
    iget v3, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@1b1
    iget-object v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->device:Landroid/bluetooth/BluetoothDevice;

    #@1b3
    invoke-direct {p0, v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->processAudioEvent(ILandroid/bluetooth/BluetoothDevice;)V

    #@1b6
    goto/16 :goto_44

    #@1b8
    .line 823
    :pswitch_1b8
    const-string v3, "HeadsetStateMachine"

    #@1ba
    const-string v4, "EVENT_TYPE_VR_STATE_CHANGED"

    #@1bc
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1bf
    .line 824
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@1c1
    iget v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@1c3
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V

    #@1c6
    goto/16 :goto_44

    #@1c8
    .line 827
    :pswitch_1c8
    const-string v3, "HeadsetStateMachine"

    #@1ca
    const-string v4, "EVENT_TYPE_ANSWER_CALL"

    #@1cc
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1cf
    .line 829
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@1d1
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@1d4
    goto/16 :goto_44

    #@1d6
    .line 832
    :pswitch_1d6
    const-string v3, "HeadsetStateMachine"

    #@1d8
    const-string v4, "EVENT_TYPE_HANGUP_CALL"

    #@1da
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1dd
    .line 834
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@1df
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@1e2
    goto/16 :goto_44

    #@1e4
    .line 837
    :pswitch_1e4
    const-string v3, "HeadsetStateMachine"

    #@1e6
    const-string v4, "EVENT_TYPE_VOLUME_CHANGED"

    #@1e8
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1eb
    .line 838
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@1ed
    iget v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@1ef
    iget v5, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt2:I

    #@1f1
    invoke-static {v3, v4, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;II)V

    #@1f4
    goto/16 :goto_44

    #@1f6
    .line 841
    :pswitch_1f6
    const-string v3, "HeadsetStateMachine"

    #@1f8
    const-string v4, "EVENT_TYPE_DIAL_CALL"

    #@1fa
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1fd
    .line 842
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@1ff
    iget-object v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueString:Ljava/lang/String;

    #@201
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V

    #@204
    goto/16 :goto_44

    #@206
    .line 845
    :pswitch_206
    const-string v3, "HeadsetStateMachine"

    #@208
    const-string v4, "EVENT_TYPE_SEND_DTMF"

    #@20a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20d
    .line 846
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@20f
    iget v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@211
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$5900(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V

    #@214
    goto/16 :goto_44

    #@216
    .line 849
    :pswitch_216
    const-string v3, "HeadsetStateMachine"

    #@218
    const-string v4, "EVENT_TYPE_NOICE_REDUCTION"

    #@21a
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@21d
    .line 850
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@21f
    iget v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@221
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$6000(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V

    #@224
    goto/16 :goto_44

    #@226
    .line 854
    :pswitch_226
    const-string v3, "HeadsetStateMachine"

    #@228
    new-instance v4, Ljava/lang/StringBuilder;

    #@22a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@22d
    const-string v5, "EVENT_TYPE_WBS codec is "

    #@22f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@232
    move-result-object v4

    #@233
    iget v5, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@235
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@238
    move-result-object v4

    #@239
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23c
    move-result-object v4

    #@23d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@240
    .line 855
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@242
    iget v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@244
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$6100(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V

    #@247
    goto/16 :goto_44

    #@249
    .line 859
    :pswitch_249
    const-string v3, "HeadsetStateMachine"

    #@24b
    const-string v4, "EVENT_TYPE_AT_CHLD"

    #@24d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@250
    .line 860
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@252
    iget v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@254
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$6200(Lcom/android/bluetooth/hfp/HeadsetStateMachine;I)V

    #@257
    goto/16 :goto_44

    #@259
    .line 863
    :pswitch_259
    const-string v3, "HeadsetStateMachine"

    #@25b
    const-string v4, "EVENT_TYPE_SUBSCRIBER_NUMBER_REQUEST"

    #@25d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@260
    .line 864
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@262
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$6300(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@265
    goto/16 :goto_44

    #@267
    .line 867
    :pswitch_267
    const-string v3, "HeadsetStateMachine"

    #@269
    const-string v4, "EVENT_TYPE_AT_CIND"

    #@26b
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26e
    .line 868
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@270
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$6400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@273
    goto/16 :goto_44

    #@275
    .line 871
    :pswitch_275
    const-string v3, "HeadsetStateMachine"

    #@277
    const-string v4, "EVENT_TYPE_AT_COPS"

    #@279
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27c
    .line 872
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@27e
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$6500(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@281
    goto/16 :goto_44

    #@283
    .line 875
    :pswitch_283
    const-string v3, "HeadsetStateMachine"

    #@285
    const-string v4, "EVENT_TYPE_AT_CLCC"

    #@287
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28a
    .line 876
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@28c
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$6600(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@28f
    goto/16 :goto_44

    #@291
    .line 879
    :pswitch_291
    const-string v3, "HeadsetStateMachine"

    #@293
    const-string v4, "EVENT_TYPE_UNKNOWN_AT"

    #@295
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@298
    .line 880
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@29a
    iget-object v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueString:Ljava/lang/String;

    #@29c
    invoke-static {v3, v4}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$6700(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Ljava/lang/String;)V

    #@29f
    goto/16 :goto_44

    #@2a1
    .line 883
    :pswitch_2a1
    const-string v3, "HeadsetStateMachine"

    #@2a3
    const-string v4, "EVENT_TYPE_KEY_PRESSED"

    #@2a5
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2a8
    .line 884
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2aa
    invoke-static {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$6800(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V

    #@2ad
    goto/16 :goto_44

    #@2af
    .line 888
    :pswitch_2af
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$Connected;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2b1
    iget v4, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt:I

    #@2b3
    iget v5, v1, Lcom/android/bluetooth/hfp/HeadsetStateMachine$StackEvent;->valueInt2:I

    #@2b5
    invoke-static {v3, v4, v5}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$3400(Lcom/android/bluetooth/hfp/HeadsetStateMachine;II)V

    #@2b8
    goto/16 :goto_44

    #@2ba
    .line 717
    :sswitch_data_2ba
    .sparse-switch
        0x1 -> :sswitch_34
        0x2 -> :sswitch_7d
        0x3 -> :sswitch_b3
        0x5 -> :sswitch_c5
        0x6 -> :sswitch_cc
        0x9 -> :sswitch_d3
        0xa -> :sswitch_e4
        0xb -> :sswitch_fe
        0xc -> :sswitch_ef
        0xd -> :sswitch_109
        0xe -> :sswitch_128
        0xf -> :sswitch_12f
        0x65 -> :sswitch_151
        0x66 -> :sswitch_114
        0x67 -> :sswitch_136
    .end sparse-switch

    #@2f8
    .line 805
    :pswitch_data_2f8
    .packed-switch 0x1
        :pswitch_190
        :pswitch_1a4
        :pswitch_1b8
        :pswitch_1c8
        :pswitch_1d6
        :pswitch_1e4
        :pswitch_1f6
        :pswitch_206
        :pswitch_216
        :pswitch_249
        :pswitch_259
        :pswitch_267
        :pswitch_275
        :pswitch_283
        :pswitch_291
        :pswitch_2a1
        :pswitch_226
        :pswitch_2af
    .end packed-switch
.end method
