.class Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;
.super Landroid/bluetooth/IBluetoothHeadset$Stub;
.source "HeadsetService.java"

# interfaces
.implements Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hfp/HeadsetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BluetoothHeadsetBinder"
.end annotation


# instance fields
.field private mService:Lcom/android/bluetooth/hfp/HeadsetService;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/hfp/HeadsetService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 120
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothHeadset$Stub;-><init>()V

    #@3
    .line 121
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@5
    .line 122
    return-void
.end method

.method private getService()Lcom/android/bluetooth/hfp/HeadsetService;
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 129
    const-string v1, "HeadsetService"

    #@3
    const-string v2, "[BTUI] BluetoothHeadsetBinder - getService()"

    #@5
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8
    .line 130
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@b
    move-result v1

    #@c
    if-nez v1, :cond_16

    #@e
    .line 131
    const-string v1, "HeadsetService"

    #@10
    const-string v2, "Headset call not allowed for non-active user"

    #@12
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 139
    :goto_15
    return-object v0

    #@16
    .line 135
    :cond_16
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@18
    if-eqz v1, :cond_25

    #@1a
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@1c
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetService;->access$100(Lcom/android/bluetooth/hfp/HeadsetService;)Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_25

    #@22
    .line 136
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@24
    goto :goto_15

    #@25
    .line 138
    :cond_25
    const-string v1, "HeadsetService"

    #@27
    new-instance v2, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v3, "[BTUI] getService() - mService is null or not available. mService : "

    #@2e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v2

    #@32
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@34
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v2

    #@38
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v2

    #@3c
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_15
.end method


# virtual methods
.method public acceptIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 239
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 240
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 241
    const/4 v1, 0x0

    #@7
    .line 243
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->acceptIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public clccResponse(IIIIZLjava/lang/String;I)V
    .registers 16
    .parameter "index"
    .parameter "direction"
    .parameter "status"
    .parameter "mode"
    .parameter "mpty"
    .parameter "number"
    .parameter "type"

    #@0
    .prologue
    .line 316
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 317
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_7

    #@6
    .line 321
    :goto_6
    return-void

    #@7
    :cond_7
    move v1, p1

    #@8
    move v2, p2

    #@9
    move v3, p3

    #@a
    move v4, p4

    #@b
    move v5, p5

    #@c
    move-object v6, p6

    #@d
    move v7, p7

    #@e
    .line 320
    #calls: Lcom/android/bluetooth/hfp/HeadsetService;->clccResponse(IIIIZLjava/lang/String;I)V
    invoke-static/range {v0 .. v7}, Lcom/android/bluetooth/hfp/HeadsetService;->access$500(Lcom/android/bluetooth/hfp/HeadsetService;IIIIZLjava/lang/String;I)V

    #@11
    goto :goto_6
.end method

.method public cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 124
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->mService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    .line 125
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 143
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 144
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 145
    const/4 v1, 0x0

    #@7
    .line 147
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public connectAudio()Z
    .registers 3

    #@0
    .prologue
    .line 263
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 264
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 265
    const/4 v1, 0x0

    #@7
    .line 267
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetService;->connectAudio()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 151
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 152
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 153
    const/4 v1, 0x0

    #@7
    .line 155
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public disconnectAudio()Z
    .registers 3

    #@0
    .prologue
    .line 271
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 272
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 273
    const/4 v1, 0x0

    #@7
    .line 275
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetService;->disconnectAudio()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getAudioState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 255
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 256
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_9

    #@6
    .line 257
    const/16 v1, 0xa

    #@8
    .line 259
    :goto_8
    return v1

    #@9
    :cond_9
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->getAudioState(Landroid/bluetooth/BluetoothDevice;)I

    #@c
    move-result v1

    #@d
    goto :goto_8
.end method

.method public getBatteryUsageHint(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 231
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 232
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 233
    const/4 v1, 0x0

    #@7
    .line 235
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->getBatteryUsageHint(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 159
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 160
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_d

    #@6
    .line 161
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 163
    :goto_c
    return-object v1

    #@d
    :cond_d
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetService;->getConnectedDevices()Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 175
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 176
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 177
    const/4 v1, 0x0

    #@7
    .line 179
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 5
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 167
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 168
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_d

    #@6
    .line 169
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 171
    :goto_c
    return-object v1

    #@d
    :cond_d
    #calls: Lcom/android/bluetooth/hfp/HeadsetService;->getDevicesMatchingConnectionStates([I)Ljava/util/List;
    invoke-static {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->access$200(Lcom/android/bluetooth/hfp/HeadsetService;[I)Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method

.method public getPriority(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 191
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 192
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 193
    const/4 v1, -0x1

    #@7
    .line 195
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 223
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 224
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 225
    const/4 v1, 0x0

    #@7
    .line 227
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public isAudioOn()Z
    .registers 3

    #@0
    .prologue
    .line 215
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 216
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 217
    const/4 v1, 0x0

    #@7
    .line 219
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetService;->isAudioOn()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public phoneStateChanged(IIILjava/lang/String;I)V
    .registers 12
    .parameter "numActive"
    .parameter "numHeld"
    .parameter "callState"
    .parameter "number"
    .parameter "type"

    #@0
    .prologue
    .line 296
    const-string v1, "HeadsetService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "[BTUI] phoneStateChanged() - numActive : "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, ", numHeld : "

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ", callState : "

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    const-string v3, ", number : "

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, ", type : "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 299
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@43
    move-result-object v0

    #@44
    .line 300
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_47

    #@46
    .line 304
    :goto_46
    return-void

    #@47
    :cond_47
    move v1, p1

    #@48
    move v2, p2

    #@49
    move v3, p3

    #@4a
    move-object v4, p4

    #@4b
    move v5, p5

    #@4c
    .line 303
    invoke-static/range {v0 .. v5}, Lcom/android/bluetooth/hfp/HeadsetService;->access$300(Lcom/android/bluetooth/hfp/HeadsetService;IIILjava/lang/String;I)V

    #@4f
    goto :goto_46
.end method

.method public rejectIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 247
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 248
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 249
    const/4 v1, 0x0

    #@7
    .line 251
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->rejectIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public roamChanged(Z)V
    .registers 3
    .parameter "roam"

    #@0
    .prologue
    .line 307
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 308
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_7

    #@6
    .line 312
    :goto_6
    return-void

    #@7
    .line 311
    :cond_7
    #calls: Lcom/android/bluetooth/hfp/HeadsetService;->roamChanged(Z)V
    invoke-static {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->access$400(Lcom/android/bluetooth/hfp/HeadsetService;Z)V

    #@a
    goto :goto_6
.end method

.method public setPriority(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 5
    .parameter "device"
    .parameter "priority"

    #@0
    .prologue
    .line 183
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 184
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 185
    const/4 v1, 0x0

    #@7
    .line 187
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hfp/HeadsetService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public startScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 279
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 280
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 281
    const/4 v1, 0x0

    #@7
    .line 283
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->startScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public startVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 199
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 200
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 201
    const/4 v1, 0x0

    #@7
    .line 203
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->startVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public stopScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 287
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 288
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 289
    const/4 v1, 0x0

    #@7
    .line 291
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->stopScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 207
    invoke-direct {p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;->getService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v0

    #@4
    .line 208
    .local v0, service:Lcom/android/bluetooth/hfp/HeadsetService;
    if-nez v0, :cond_8

    #@6
    .line 209
    const/4 v1, 0x0

    #@7
    .line 211
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method
