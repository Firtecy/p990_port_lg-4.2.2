.class Lcom/android/bluetooth/hfp/HeadsetService$1;
.super Landroid/content/BroadcastReceiver;
.source "HeadsetService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hfp/HeadsetService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/hfp/HeadsetService;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/hfp/HeadsetService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 94
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetService$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 97
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 98
    .local v0, action:Ljava/lang/String;
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    #@6
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9
    move-result v2

    #@a
    if-eqz v2, :cond_18

    #@c
    .line 99
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetService;

    #@e
    invoke-static {v2}, Lcom/android/bluetooth/hfp/HeadsetService;->access$000(Lcom/android/bluetooth/hfp/HeadsetService;)Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@11
    move-result-object v2

    #@12
    const/16 v3, 0xa

    #@14
    invoke-virtual {v2, v3, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@17
    .line 111
    :cond_17
    :goto_17
    return-void

    #@18
    .line 100
    :cond_18
    const-string v2, "android.media.VOLUME_CHANGED_ACTION"

    #@1a
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v2

    #@1e
    if-eqz v2, :cond_35

    #@20
    .line 101
    const-string v2, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    #@22
    const/4 v3, -0x1

    #@23
    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@26
    move-result v1

    #@27
    .line 102
    .local v1, streamType:I
    const/4 v2, 0x6

    #@28
    if-ne v1, v2, :cond_17

    #@2a
    .line 103
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetService;

    #@2c
    invoke-static {v2}, Lcom/android/bluetooth/hfp/HeadsetService;->access$000(Lcom/android/bluetooth/hfp/HeadsetService;)Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2f
    move-result-object v2

    #@30
    const/4 v3, 0x7

    #@31
    invoke-virtual {v2, v3, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@34
    goto :goto_17

    #@35
    .line 107
    .end local v1           #streamType:I
    :cond_35
    const-string v2, "android.bluetooth.device.action.CONNECTION_ACCESS_REPLY"

    #@37
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a
    move-result v2

    #@3b
    if-eqz v2, :cond_17

    #@3d
    .line 108
    const-string v2, "HeadsetService"

    #@3f
    const-string v3, "HeadsetService -  Received BluetoothDevice.ACTION_CONNECTION_ACCESS_REPLY"

    #@41
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 109
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetService;

    #@46
    invoke-static {v2}, Lcom/android/bluetooth/hfp/HeadsetService;->access$000(Lcom/android/bluetooth/hfp/HeadsetService;)Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@49
    move-result-object v2

    #@4a
    invoke-virtual {v2, p2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->handleAccessPermissionResult(Landroid/content/Intent;)V

    #@4d
    goto :goto_17
.end method
