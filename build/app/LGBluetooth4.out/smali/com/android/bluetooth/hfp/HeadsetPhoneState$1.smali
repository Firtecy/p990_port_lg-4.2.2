.class Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;
.super Landroid/telephony/PhoneStateListener;
.source "HeadsetPhoneState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hfp/HeadsetPhoneState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 178
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@2
    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    #@5
    return-void
.end method

.method private cdmaDbmEcioToSignal(Landroid/telephony/SignalStrength;)I
    .registers 19
    .parameter "signalStrength"

    #@0
    .prologue
    .line 247
    const/4 v11, 0x0

    #@1
    .line 248
    .local v11, levelDbm:I
    const/4 v12, 0x0

    #@2
    .line 249
    .local v12, levelEcio:I
    const/4 v3, 0x0

    #@3
    .line 250
    .local v3, cdmaIconLevel:I
    const/4 v5, 0x0

    #@4
    .line 251
    .local v5, evdoIconLevel:I
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    #@7
    move-result v1

    #@8
    .line 252
    .local v1, cdmaDbm:I
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/SignalStrength;->getCdmaEcio()I

    #@b
    move-result v2

    #@c
    .line 255
    .local v2, cdmaEcio:I
    const-string v15, "LGBT_CNDTL_FUNCTION_HFP_CDMA_RSSI_MODIFY"

    #@e
    invoke-static {v15}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@11
    move-result v15

    #@12
    if-eqz v15, :cond_2f

    #@14
    .line 256
    const/16 v7, -0x54

    #@16
    .line 257
    .local v7, level1:I
    const/16 v8, -0x5e

    #@18
    .line 258
    .local v8, level2:I
    const/16 v9, -0x65

    #@1a
    .line 259
    .local v9, level3:I
    const/16 v10, -0x6e

    #@1c
    .line 260
    .local v10, level4:I
    if-le v1, v7, :cond_21

    #@1e
    .line 261
    const/4 v11, 0x4

    #@1f
    :goto_1f
    move v15, v11

    #@20
    .line 337
    .end local v7           #level1:I
    .end local v8           #level2:I
    .end local v9           #level3:I
    .end local v10           #level4:I
    :goto_20
    return v15

    #@21
    .line 262
    .restart local v7       #level1:I
    .restart local v8       #level2:I
    .restart local v9       #level3:I
    .restart local v10       #level4:I
    :cond_21
    if-le v1, v8, :cond_25

    #@23
    .line 263
    const/4 v11, 0x3

    #@24
    goto :goto_1f

    #@25
    .line 264
    :cond_25
    if-le v1, v9, :cond_29

    #@27
    .line 265
    const/4 v11, 0x2

    #@28
    goto :goto_1f

    #@29
    .line 266
    :cond_29
    if-le v1, v10, :cond_2d

    #@2b
    .line 267
    const/4 v11, 0x1

    #@2c
    goto :goto_1f

    #@2d
    .line 269
    :cond_2d
    const/4 v11, 0x0

    #@2e
    goto :goto_1f

    #@2f
    .line 274
    .end local v7           #level1:I
    .end local v8           #level2:I
    .end local v9           #level3:I
    .end local v10           #level4:I
    :cond_2f
    const/16 v15, -0x4b

    #@31
    if-lt v1, v15, :cond_84

    #@33
    .line 275
    const/4 v11, 0x4

    #@34
    .line 287
    :goto_34
    const/16 v15, -0x5a

    #@36
    if-lt v2, v15, :cond_98

    #@38
    .line 288
    const/4 v12, 0x4

    #@39
    .line 299
    :goto_39
    if-ge v11, v12, :cond_ac

    #@3b
    move v3, v11

    #@3c
    .line 301
    :goto_3c
    move-object/from16 v0, p0

    #@3e
    iget-object v15, v0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@40
    invoke-static {v15}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$000(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)Landroid/telephony/ServiceState;

    #@43
    move-result-object v15

    #@44
    if-eqz v15, :cond_80

    #@46
    move-object/from16 v0, p0

    #@48
    iget-object v15, v0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@4a
    invoke-static {v15}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$000(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)Landroid/telephony/ServiceState;

    #@4d
    move-result-object v15

    #@4e
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@51
    move-result v15

    #@52
    const/16 v16, 0x7

    #@54
    move/from16 v0, v16

    #@56
    if-eq v15, v0, :cond_6a

    #@58
    move-object/from16 v0, p0

    #@5a
    iget-object v15, v0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@5c
    invoke-static {v15}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$000(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)Landroid/telephony/ServiceState;

    #@5f
    move-result-object v15

    #@60
    invoke-virtual {v15}, Landroid/telephony/ServiceState;->getRadioTechnology()I

    #@63
    move-result v15

    #@64
    const/16 v16, 0x8

    #@66
    move/from16 v0, v16

    #@68
    if-ne v15, v0, :cond_80

    #@6a
    .line 304
    :cond_6a
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/SignalStrength;->getEvdoEcio()I

    #@6d
    move-result v4

    #@6e
    .line 305
    .local v4, evdoEcio:I
    invoke-virtual/range {p1 .. p1}, Landroid/telephony/SignalStrength;->getEvdoSnr()I

    #@71
    move-result v6

    #@72
    .line 306
    .local v6, evdoSnr:I
    const/4 v13, 0x0

    #@73
    .line 307
    .local v13, levelEvdoEcio:I
    const/4 v14, 0x0

    #@74
    .line 310
    .local v14, levelEvdoSnr:I
    const/16 v15, -0x28a

    #@76
    if-lt v4, v15, :cond_ae

    #@78
    .line 311
    const/4 v13, 0x4

    #@79
    .line 322
    :goto_79
    const/4 v15, 0x7

    #@7a
    if-le v6, v15, :cond_c2

    #@7c
    .line 323
    const/4 v14, 0x4

    #@7d
    .line 334
    :goto_7d
    if-ge v13, v14, :cond_d3

    #@7f
    move v5, v13

    #@80
    .line 337
    .end local v4           #evdoEcio:I
    .end local v6           #evdoSnr:I
    .end local v13           #levelEvdoEcio:I
    .end local v14           #levelEvdoSnr:I
    :cond_80
    :goto_80
    if-le v3, v5, :cond_d5

    #@82
    move v15, v3

    #@83
    goto :goto_20

    #@84
    .line 276
    :cond_84
    const/16 v15, -0x55

    #@86
    if-lt v1, v15, :cond_8a

    #@88
    .line 277
    const/4 v11, 0x3

    #@89
    goto :goto_34

    #@8a
    .line 278
    :cond_8a
    const/16 v15, -0x5f

    #@8c
    if-lt v1, v15, :cond_90

    #@8e
    .line 279
    const/4 v11, 0x2

    #@8f
    goto :goto_34

    #@90
    .line 280
    :cond_90
    const/16 v15, -0x64

    #@92
    if-lt v1, v15, :cond_96

    #@94
    .line 281
    const/4 v11, 0x1

    #@95
    goto :goto_34

    #@96
    .line 283
    :cond_96
    const/4 v11, 0x0

    #@97
    goto :goto_34

    #@98
    .line 289
    :cond_98
    const/16 v15, -0x6e

    #@9a
    if-lt v2, v15, :cond_9e

    #@9c
    .line 290
    const/4 v12, 0x3

    #@9d
    goto :goto_39

    #@9e
    .line 291
    :cond_9e
    const/16 v15, -0x82

    #@a0
    if-lt v2, v15, :cond_a4

    #@a2
    .line 292
    const/4 v12, 0x2

    #@a3
    goto :goto_39

    #@a4
    .line 293
    :cond_a4
    const/16 v15, -0x96

    #@a6
    if-lt v2, v15, :cond_aa

    #@a8
    .line 294
    const/4 v12, 0x1

    #@a9
    goto :goto_39

    #@aa
    .line 296
    :cond_aa
    const/4 v12, 0x0

    #@ab
    goto :goto_39

    #@ac
    :cond_ac
    move v3, v12

    #@ad
    .line 299
    goto :goto_3c

    #@ae
    .line 312
    .restart local v4       #evdoEcio:I
    .restart local v6       #evdoSnr:I
    .restart local v13       #levelEvdoEcio:I
    .restart local v14       #levelEvdoSnr:I
    :cond_ae
    const/16 v15, -0x2ee

    #@b0
    if-lt v4, v15, :cond_b4

    #@b2
    .line 313
    const/4 v13, 0x3

    #@b3
    goto :goto_79

    #@b4
    .line 314
    :cond_b4
    const/16 v15, -0x384

    #@b6
    if-lt v4, v15, :cond_ba

    #@b8
    .line 315
    const/4 v13, 0x2

    #@b9
    goto :goto_79

    #@ba
    .line 316
    :cond_ba
    const/16 v15, -0x41a

    #@bc
    if-lt v4, v15, :cond_c0

    #@be
    .line 317
    const/4 v13, 0x1

    #@bf
    goto :goto_79

    #@c0
    .line 319
    :cond_c0
    const/4 v13, 0x0

    #@c1
    goto :goto_79

    #@c2
    .line 324
    :cond_c2
    const/4 v15, 0x5

    #@c3
    if-le v6, v15, :cond_c7

    #@c5
    .line 325
    const/4 v14, 0x3

    #@c6
    goto :goto_7d

    #@c7
    .line 326
    :cond_c7
    const/4 v15, 0x3

    #@c8
    if-le v6, v15, :cond_cc

    #@ca
    .line 327
    const/4 v14, 0x2

    #@cb
    goto :goto_7d

    #@cc
    .line 328
    :cond_cc
    const/4 v15, 0x1

    #@cd
    if-le v6, v15, :cond_d1

    #@cf
    .line 329
    const/4 v14, 0x1

    #@d0
    goto :goto_7d

    #@d1
    .line 331
    :cond_d1
    const/4 v14, 0x0

    #@d2
    goto :goto_7d

    #@d3
    :cond_d3
    move v5, v14

    #@d4
    .line 334
    goto :goto_80

    #@d5
    .end local v4           #evdoEcio:I
    .end local v6           #evdoSnr:I
    .end local v13           #levelEvdoEcio:I
    .end local v14           #levelEvdoSnr:I
    :cond_d5
    move v15, v5

    #@d6
    .line 337
    goto/16 :goto_20
.end method

.method private gsmAsuToSignal(Landroid/telephony/SignalStrength;)I
    .registers 7
    .parameter "signalStrength"

    #@0
    .prologue
    const/4 v1, 0x4

    #@1
    const/4 v2, 0x2

    #@2
    const/4 v3, 0x1

    #@3
    .line 223
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    #@6
    move-result v0

    #@7
    .line 224
    .local v0, asu:I
    const/16 v4, 0x10

    #@9
    if-lt v0, v4, :cond_d

    #@b
    .line 225
    const/4 v1, 0x5

    #@c
    .line 235
    :cond_c
    :goto_c
    return v1

    #@d
    .line 226
    :cond_d
    const/16 v4, 0x8

    #@f
    if-ge v0, v4, :cond_c

    #@11
    .line 228
    if-lt v0, v1, :cond_15

    #@13
    .line 229
    const/4 v1, 0x3

    #@14
    goto :goto_c

    #@15
    .line 230
    :cond_15
    if-lt v0, v2, :cond_19

    #@17
    move v1, v2

    #@18
    .line 231
    goto :goto_c

    #@19
    .line 232
    :cond_19
    if-lt v0, v3, :cond_1d

    #@1b
    move v1, v3

    #@1c
    .line 233
    goto :goto_c

    #@1d
    .line 235
    :cond_1d
    const/4 v1, 0x0

    #@1e
    goto :goto_c
.end method


# virtual methods
.method public onServiceStateChanged(Landroid/telephony/ServiceState;)V
    .registers 5
    .parameter "serviceState"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 181
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@3
    invoke-static {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$002(Lcom/android/bluetooth/hfp/HeadsetPhoneState;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;

    #@6
    .line 182
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@8
    invoke-virtual {p1}, Landroid/telephony/ServiceState;->getState()I

    #@b
    move-result v0

    #@c
    if-nez v0, :cond_25

    #@e
    const/4 v0, 0x1

    #@f
    :goto_f
    invoke-static {v2, v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$102(Lcom/android/bluetooth/hfp/HeadsetPhoneState;I)I

    #@12
    .line 186
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@14
    invoke-static {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$100(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)I

    #@17
    move-result v0

    #@18
    if-nez v0, :cond_1f

    #@1a
    .line 187
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@1c
    invoke-static {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$202(Lcom/android/bluetooth/hfp/HeadsetPhoneState;I)I

    #@1f
    .line 190
    :cond_1f
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@21
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->sendDeviceStateChanged()V

    #@24
    .line 191
    return-void

    #@25
    :cond_25
    move v0, v1

    #@26
    .line 182
    goto :goto_f
.end method

.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .registers 5
    .parameter "signalStrength"

    #@0
    .prologue
    .line 195
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@2
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$200(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)I

    #@5
    move-result v0

    #@6
    .line 197
    .local v0, prevSignal:I
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@8
    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getLevel()I

    #@b
    move-result v2

    #@c
    invoke-static {v1, v2}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$202(Lcom/android/bluetooth/hfp/HeadsetPhoneState;I)I

    #@f
    .line 212
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@11
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$200(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)I

    #@14
    move-result v1

    #@15
    if-eq v0, v1, :cond_25

    #@17
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@19
    invoke-static {v1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->access$100(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)I

    #@1c
    move-result v1

    #@1d
    const/4 v2, 0x1

    #@1e
    if-ne v1, v2, :cond_25

    #@20
    .line 215
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetPhoneState;

    #@22
    invoke-virtual {v1}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->sendDeviceStateChanged()V

    #@25
    .line 217
    :cond_25
    return-void
.end method
