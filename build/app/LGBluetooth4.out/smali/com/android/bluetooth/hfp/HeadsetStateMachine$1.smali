.class Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;
.super Ljava/lang/Object;
.source "HeadsetStateMachine.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hfp/HeadsetStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 1192
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .registers 5
    .parameter "className"
    .parameter "service"

    #@0
    .prologue
    .line 1195
    const-string v0, "HeadsetStateMachine"

    #@2
    const-string v1, "Proxy object connected"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1197
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9
    invoke-static {p2}, Landroid/bluetooth/IBluetoothHeadsetPhone$Stub;->asInterface(Landroid/os/IBinder;)Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@c
    move-result-object v1

    #@d
    invoke-static {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$7402(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/IBluetoothHeadsetPhone;)Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@10
    .line 1198
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .registers 4
    .parameter "className"

    #@0
    .prologue
    .line 1202
    const-string v0, "HeadsetStateMachine"

    #@2
    const-string v1, "Proxy object disconnected"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 1204
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetStateMachine$1;->this$0:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9
    const/4 v1, 0x0

    #@a
    invoke-static {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->access$7402(Lcom/android/bluetooth/hfp/HeadsetStateMachine;Landroid/bluetooth/IBluetoothHeadsetPhone;)Landroid/bluetooth/IBluetoothHeadsetPhone;

    #@d
    .line 1205
    return-void
.end method
