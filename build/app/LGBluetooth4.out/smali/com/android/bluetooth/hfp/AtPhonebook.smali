.class public Lcom/android/bluetooth/hfp/AtPhonebook;
.super Ljava/lang/Object;
.source "AtPhonebook.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/hfp/AtPhonebook$1;,
        Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    }
.end annotation


# static fields
.field private static final ACCESS_AUTHORITY_CLASS:Ljava/lang/String; = "com.android.settings.bluetooth.BluetoothPermissionRequest"

.field private static final ACCESS_AUTHORITY_PACKAGE:Ljava/lang/String; = "com.android.settings"

.field private static final BLUETOOTH_ADMIN_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH_ADMIN"

.field private static final CALLS_PROJECTION:[Ljava/lang/String; = null

.field private static final DBG:Z = true

.field private static final INCOMING_CALL_WHERE:Ljava/lang/String; = "type=1"

.field private static final MAX_PHONEBOOK_SIZE:I = 0x4000

.field private static final MISSED_CALL_WHERE:Ljava/lang/String; = "type=3"

.field private static final OUTGOING_CALL_WHERE:Ljava/lang/String; = "type=2"

.field private static final PHONES_PROJECTION:[Ljava/lang/String; = null

.field private static final TAG:Ljava/lang/String; = "BluetoothAtPhonebook"

.field private static final VISIBLE_PHONEBOOK_WHERE:Ljava/lang/String; = "in_visible_group=1"

.field public static isVideoCall:Z


# instance fields
.field final TYPE_READ:I

.field final TYPE_SET:I

.field final TYPE_TEST:I

.field final TYPE_UNKNOWN:I

.field private mCharacterSet:Ljava/lang/String;

.field private mCheckingAccessPermission:Z

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mCpbrIndex1:I

.field private mCpbrIndex2:I

.field private mCurrentPhonebook:Ljava/lang/String;

.field private final mPhonebooks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;",
            ">;"
        }
    .end annotation
.end field

.field private mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v3, 0x1

    #@2
    const/4 v2, 0x0

    #@3
    .line 63
    new-array v0, v4, [Ljava/lang/String;

    #@5
    const-string v1, "_id"

    #@7
    aput-object v1, v0, v2

    #@9
    const-string v1, "number"

    #@b
    aput-object v1, v0, v3

    #@d
    sput-object v0, Lcom/android/bluetooth/hfp/AtPhonebook;->CALLS_PROJECTION:[Ljava/lang/String;

    #@f
    .line 70
    const/4 v0, 0x4

    #@10
    new-array v0, v0, [Ljava/lang/String;

    #@12
    const-string v1, "_id"

    #@14
    aput-object v1, v0, v2

    #@16
    const-string v1, "display_name"

    #@18
    aput-object v1, v0, v3

    #@1a
    const-string v1, "data1"

    #@1c
    aput-object v1, v0, v4

    #@1e
    const/4 v1, 0x3

    #@1f
    const-string v2, "data2"

    #@21
    aput-object v2, v0, v1

    #@23
    sput-object v0, Lcom/android/bluetooth/hfp/AtPhonebook;->PHONES_PROJECTION:[Ljava/lang/String;

    #@25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 9
    .parameter "context"
    .parameter "headsetState"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v4, -0x1

    #@2
    const/4 v3, 0x0

    #@3
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@6
    .line 94
    const-string v0, "UTF-8"

    #@8
    iput-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCharacterSet:Ljava/lang/String;

    #@a
    .line 106
    new-instance v0, Ljava/util/HashMap;

    #@c
    const/4 v1, 0x5

    #@d
    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    #@10
    iput-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mPhonebooks:Ljava/util/HashMap;

    #@12
    .line 118
    iput v4, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->TYPE_UNKNOWN:I

    #@14
    .line 119
    iput v5, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->TYPE_READ:I

    #@16
    .line 120
    const/4 v0, 0x1

    #@17
    iput v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->TYPE_SET:I

    #@19
    .line 121
    const/4 v0, 0x2

    #@1a
    iput v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->TYPE_TEST:I

    #@1c
    .line 124
    iput-object p1, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mContext:Landroid/content/Context;

    #@1e
    .line 125
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@21
    move-result-object v0

    #@22
    iput-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mContentResolver:Landroid/content/ContentResolver;

    #@24
    .line 126
    iput-object p2, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@26
    .line 127
    iget-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mPhonebooks:Ljava/util/HashMap;

    #@28
    const-string v1, "DC"

    #@2a
    new-instance v2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@2c
    invoke-direct {v2, p0, v3}, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;-><init>(Lcom/android/bluetooth/hfp/AtPhonebook;Lcom/android/bluetooth/hfp/AtPhonebook$1;)V

    #@2f
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@32
    .line 128
    iget-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mPhonebooks:Ljava/util/HashMap;

    #@34
    const-string v1, "RC"

    #@36
    new-instance v2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@38
    invoke-direct {v2, p0, v3}, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;-><init>(Lcom/android/bluetooth/hfp/AtPhonebook;Lcom/android/bluetooth/hfp/AtPhonebook$1;)V

    #@3b
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3e
    .line 129
    iget-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mPhonebooks:Ljava/util/HashMap;

    #@40
    const-string v1, "MC"

    #@42
    new-instance v2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@44
    invoke-direct {v2, p0, v3}, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;-><init>(Lcom/android/bluetooth/hfp/AtPhonebook;Lcom/android/bluetooth/hfp/AtPhonebook$1;)V

    #@47
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@4a
    .line 130
    iget-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mPhonebooks:Ljava/util/HashMap;

    #@4c
    const-string v1, "ME"

    #@4e
    new-instance v2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@50
    invoke-direct {v2, p0, v3}, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;-><init>(Lcom/android/bluetooth/hfp/AtPhonebook;Lcom/android/bluetooth/hfp/AtPhonebook$1;)V

    #@53
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@56
    .line 132
    const-string v0, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@58
    invoke-static {v0}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_6a

    #@5e
    .line 133
    iget-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mPhonebooks:Ljava/util/HashMap;

    #@60
    const-string v1, "SM"

    #@62
    new-instance v2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@64
    invoke-direct {v2, p0, v3}, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;-><init>(Lcom/android/bluetooth/hfp/AtPhonebook;Lcom/android/bluetooth/hfp/AtPhonebook$1;)V

    #@67
    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@6a
    .line 137
    :cond_6a
    const-string v0, "ME"

    #@6c
    iput-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCurrentPhonebook:Ljava/lang/String;

    #@6e
    .line 139
    iput v4, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex2:I

    #@70
    iput v4, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@72
    .line 140
    iput-boolean v5, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCheckingAccessPermission:Z

    #@74
    .line 141
    return-void
.end method

.method private checkAccessPermission(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "remoteDevice"

    #@0
    .prologue
    .line 761
    const-string v2, "checkAccessPermission"

    #@2
    invoke-static {v2}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@5
    .line 762
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getTrustState()Z

    #@8
    move-result v1

    #@9
    .line 764
    .local v1, trust:Z
    if-eqz v1, :cond_d

    #@b
    .line 765
    const/4 v2, 0x1

    #@c
    .line 777
    :goto_c
    return v2

    #@d
    .line 768
    :cond_d
    const-string v2, "checkAccessPermission - ACTION_CONNECTION_ACCESS_REQUEST"

    #@f
    invoke-static {v2}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@12
    .line 769
    new-instance v0, Landroid/content/Intent;

    #@14
    const-string v2, "android.bluetooth.device.action.CONNECTION_ACCESS_REQUEST"

    #@16
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@19
    .line 770
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "com.android.settings"

    #@1b
    const-string v3, "com.android.settings.bluetooth.BluetoothPermissionRequest"

    #@1d
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@20
    .line 771
    const-string v2, "android.bluetooth.device.extra.ACCESS_REQUEST_TYPE"

    #@22
    const/4 v3, 0x2

    #@23
    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@26
    .line 773
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    #@28
    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@2b
    .line 776
    iget-object v2, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mContext:Landroid/content/Context;

    #@2d
    const-string v3, "android.permission.BLUETOOTH_ADMIN"

    #@2f
    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@32
    .line 777
    const/4 v2, 0x0

    #@33
    goto :goto_c
.end method

.method private declared-synchronized getMaxPhoneBookSize(I)I
    .registers 4
    .parameter "currSize"

    #@0
    .prologue
    const/16 v0, 0x64

    #@2
    .line 587
    monitor-enter p0

    #@3
    if-ge p1, v0, :cond_e

    #@5
    .line 588
    .local v0, maxSize:I
    :goto_5
    :try_start_5
    div-int/lit8 v1, v0, 0x2

    #@7
    add-int/2addr v0, v1

    #@8
    .line 589
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/AtPhonebook;->roundUpToPowerOfTwo(I)I
    :try_end_b
    .catchall {:try_start_5 .. :try_end_b} :catchall_10

    #@b
    move-result v1

    #@c
    monitor-exit p0

    #@d
    return v1

    #@e
    .end local v0           #maxSize:I
    :cond_e
    move v0, p1

    #@f
    .line 587
    goto :goto_5

    #@10
    .restart local v0       #maxSize:I
    :catchall_10
    move-exception v1

    #@11
    monitor-exit p0

    #@12
    throw v1
.end method

.method private static getPhoneType(I)Ljava/lang/String;
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 781
    packed-switch p0, :pswitch_data_12

    #@3
    .line 794
    const-string v0, "O"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 783
    :pswitch_6
    const-string v0, "H"

    #@8
    goto :goto_5

    #@9
    .line 785
    :pswitch_9
    const-string v0, "M"

    #@b
    goto :goto_5

    #@c
    .line 787
    :pswitch_c
    const-string v0, "W"

    #@e
    goto :goto_5

    #@f
    .line 790
    :pswitch_f
    const-string v0, "F"

    #@11
    goto :goto_5

    #@12
    .line 781
    :pswitch_data_12
    .packed-switch 0x1
        :pswitch_6
        :pswitch_9
        :pswitch_c
        :pswitch_f
        :pswitch_f
    .end packed-switch
.end method

.method private declared-synchronized getPhonebookResult(Ljava/lang/String;Z)Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    .registers 6
    .parameter "pb"
    .parameter "force"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 484
    monitor-enter p0

    #@2
    if-nez p1, :cond_7

    #@4
    move-object v0, v1

    #@5
    .line 497
    :cond_5
    :goto_5
    monitor-exit p0

    #@6
    return-object v0

    #@7
    .line 487
    :cond_7
    :try_start_7
    iget-object v2, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mPhonebooks:Ljava/util/HashMap;

    #@9
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@f
    .line 488
    .local v0, pbr:Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    if-nez v0, :cond_17

    #@11
    .line 489
    new-instance v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@13
    .end local v0           #pbr:Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    const/4 v2, 0x0

    #@14
    invoke-direct {v0, p0, v2}, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;-><init>(Lcom/android/bluetooth/hfp/AtPhonebook;Lcom/android/bluetooth/hfp/AtPhonebook$1;)V

    #@17
    .line 491
    .restart local v0       #pbr:Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    :cond_17
    if-nez p2, :cond_1d

    #@19
    iget-object v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@1b
    if-nez v2, :cond_5

    #@1d
    .line 492
    :cond_1d
    invoke-direct {p0, p1, v0}, Lcom/android/bluetooth/hfp/AtPhonebook;->queryPhonebook(Ljava/lang/String;Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;)Z
    :try_end_20
    .catchall {:try_start_7 .. :try_end_20} :catchall_25

    #@20
    move-result v2

    #@21
    if-nez v2, :cond_5

    #@23
    move-object v0, v1

    #@24
    .line 493
    goto :goto_5

    #@25
    .line 484
    .end local v0           #pbr:Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    :catchall_25
    move-exception v1

    #@26
    monitor-exit p0

    #@27
    throw v1
.end method

.method private static log(Ljava/lang/String;)V
    .registers 2
    .parameter "msg"

    #@0
    .prologue
    .line 799
    const-string v0, "BluetoothAtPhonebook"

    #@2
    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 800
    return-void
.end method

.method private declared-synchronized queryPhonebook(Ljava/lang/String;Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;)Z
    .registers 12
    .parameter "pb"
    .parameter "pbr"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 502
    monitor-enter p0

    #@2
    const/4 v7, 0x1

    #@3
    .line 504
    .local v7, ancillaryPhonebook:Z
    const/4 v6, 0x0

    #@4
    .line 507
    .local v6, SIMPhonebook:Z
    if-eqz p1, :cond_4d

    #@6
    .line 508
    :try_start_6
    const-string v0, "BluetoothAtPhonebook"

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v2, "[BTUI] queryPhonebook : "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 521
    const-string v0, "ME"

    #@20
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v0

    #@24
    if-eqz v0, :cond_56

    #@26
    .line 522
    const/4 v7, 0x0

    #@27
    .line 523
    const-string v3, "in_visible_group=1"

    #@29
    .line 542
    .local v3, where:Ljava/lang/String;
    :goto_29
    iget-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@2b
    if-eqz v0, :cond_35

    #@2d
    .line 543
    iget-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@2f
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    #@32
    .line 544
    const/4 v0, 0x0

    #@33
    iput-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@35
    .line 547
    :cond_35
    if-eqz v7, :cond_cf

    #@37
    .line 548
    iget-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mContentResolver:Landroid/content/ContentResolver;

    #@39
    sget-object v1, Lcom/lge/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@3b
    sget-object v2, Lcom/android/bluetooth/hfp/AtPhonebook;->CALLS_PROJECTION:[Ljava/lang/String;

    #@3d
    const/4 v4, 0x0

    #@3e
    const-string v5, "date DESC LIMIT 16384"

    #@40
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@43
    move-result-object v0

    #@44
    iput-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@46
    .line 551
    iget-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;
    :try_end_48
    .catchall {:try_start_6 .. :try_end_48} :catchall_104

    #@48
    if-nez v0, :cond_8e

    #@4a
    move v0, v8

    #@4b
    .line 570
    .end local v3           #where:Ljava/lang/String;
    :goto_4b
    monitor-exit p0

    #@4c
    return v0

    #@4d
    .line 510
    :cond_4d
    :try_start_4d
    const-string v0, "BluetoothAtPhonebook"

    #@4f
    const-string v1, "[BTUI] queryPhonebook is null"

    #@51
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    move v0, v8

    #@55
    .line 511
    goto :goto_4b

    #@56
    .line 524
    :cond_56
    const-string v0, "DC"

    #@58
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@5b
    move-result v0

    #@5c
    if-eqz v0, :cond_61

    #@5e
    .line 525
    const-string v3, "type=2"

    #@60
    .restart local v3       #where:Ljava/lang/String;
    goto :goto_29

    #@61
    .line 526
    .end local v3           #where:Ljava/lang/String;
    :cond_61
    const-string v0, "RC"

    #@63
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@66
    move-result v0

    #@67
    if-eqz v0, :cond_6c

    #@69
    .line 527
    const-string v3, "type=1"

    #@6b
    .restart local v3       #where:Ljava/lang/String;
    goto :goto_29

    #@6c
    .line 528
    .end local v3           #where:Ljava/lang/String;
    :cond_6c
    const-string v0, "MC"

    #@6e
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v0

    #@72
    if-eqz v0, :cond_77

    #@74
    .line 529
    const-string v3, "type=3"

    #@76
    .restart local v3       #where:Ljava/lang/String;
    goto :goto_29

    #@77
    .line 531
    .end local v3           #where:Ljava/lang/String;
    :cond_77
    const-string v0, "SM"

    #@79
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@7c
    move-result v0

    #@7d
    if-eqz v0, :cond_8c

    #@7f
    const-string v0, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@81
    invoke-static {v0}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@84
    move-result v0

    #@85
    if-eqz v0, :cond_8c

    #@87
    .line 532
    const/4 v7, 0x0

    #@88
    .line 533
    const/4 v6, 0x1

    #@89
    .line 535
    const-string v3, "account_type==\'com.android.contacts.sim\'"

    #@8b
    .restart local v3       #where:Ljava/lang/String;
    goto :goto_29

    #@8c
    .end local v3           #where:Ljava/lang/String;
    :cond_8c
    move v0, v8

    #@8d
    .line 539
    goto :goto_4b

    #@8e
    .line 555
    .restart local v3       #where:Ljava/lang/String;
    :cond_8e
    iget-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@90
    const-string v1, "number"

    #@92
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@95
    move-result v0

    #@96
    iput v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->numberColumn:I

    #@98
    .line 556
    const/4 v0, -0x1

    #@99
    iput v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->typeColumn:I

    #@9b
    .line 557
    const/4 v0, -0x1

    #@9c
    iput v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->nameColumn:I

    #@9e
    .line 569
    :goto_9e
    const-string v0, "BluetoothAtPhonebook"

    #@a0
    new-instance v1, Ljava/lang/StringBuilder;

    #@a2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a5
    const-string v2, "Refreshed phonebook "

    #@a7
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v1

    #@ab
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ae
    move-result-object v1

    #@af
    const-string v2, " with "

    #@b1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v1

    #@b5
    iget-object v2, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@b7
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    #@ba
    move-result v2

    #@bb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@be
    move-result-object v1

    #@bf
    const-string v2, " results"

    #@c1
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v1

    #@c5
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c8
    move-result-object v1

    #@c9
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@cc
    .line 570
    const/4 v0, 0x1

    #@cd
    goto/16 :goto_4b

    #@cf
    .line 559
    :cond_cf
    iget-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mContentResolver:Landroid/content/ContentResolver;

    #@d1
    sget-object v1, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    #@d3
    sget-object v2, Lcom/android/bluetooth/hfp/AtPhonebook;->PHONES_PROJECTION:[Ljava/lang/String;

    #@d5
    const/4 v4, 0x0

    #@d6
    const-string v5, "data1 LIMIT 16384"

    #@d8
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@db
    move-result-object v0

    #@dc
    iput-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@de
    .line 561
    iget-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@e0
    if-nez v0, :cond_e5

    #@e2
    move v0, v8

    #@e3
    .line 562
    goto/16 :goto_4b

    #@e5
    .line 565
    :cond_e5
    iget-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@e7
    const-string v1, "data1"

    #@e9
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@ec
    move-result v0

    #@ed
    iput v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->numberColumn:I

    #@ef
    .line 566
    iget-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@f1
    const-string v1, "data2"

    #@f3
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@f6
    move-result v0

    #@f7
    iput v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->typeColumn:I

    #@f9
    .line 567
    iget-object v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@fb
    const-string v1, "display_name"

    #@fd
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    #@100
    move-result v0

    #@101
    iput v0, p2, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->nameColumn:I
    :try_end_103
    .catchall {:try_start_4d .. :try_end_103} :catchall_104

    #@103
    goto :goto_9e

    #@104
    .line 502
    .end local v3           #where:Ljava/lang/String;
    :catchall_104
    move-exception v0

    #@105
    monitor-exit p0

    #@106
    throw v0
.end method

.method private roundUpToPowerOfTwo(I)I
    .registers 3
    .parameter "x"

    #@0
    .prologue
    .line 593
    shr-int/lit8 v0, p1, 0x1

    #@2
    or-int/2addr p1, v0

    #@3
    .line 594
    shr-int/lit8 v0, p1, 0x2

    #@5
    or-int/2addr p1, v0

    #@6
    .line 595
    shr-int/lit8 v0, p1, 0x4

    #@8
    or-int/2addr p1, v0

    #@9
    .line 596
    shr-int/lit8 v0, p1, 0x8

    #@b
    or-int/2addr p1, v0

    #@c
    .line 597
    shr-int/lit8 v0, p1, 0x10

    #@e
    or-int/2addr p1, v0

    #@f
    .line 598
    add-int/lit8 v0, p1, 0x1

    #@11
    return v0
.end method


# virtual methods
.method public cleanup()V
    .registers 2

    #@0
    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mPhonebooks:Ljava/util/HashMap;

    #@2
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@5
    .line 145
    return-void
.end method

.method public getCheckingAccessPermission()Z
    .registers 2

    #@0
    .prologue
    .line 214
    iget-boolean v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCheckingAccessPermission:Z

    #@2
    return v0
.end method

.method public getLastDialledNumber()Ljava/lang/String;
    .registers 12

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v4, 0x0

    #@2
    const/4 v10, 0x1

    #@3
    .line 154
    const/4 v0, 0x2

    #@4
    new-array v2, v0, [Ljava/lang/String;

    #@6
    const-string v0, "number"

    #@8
    aput-object v0, v2, v1

    #@a
    const-string v0, "type"

    #@c
    aput-object v0, v2, v10

    #@e
    .line 164
    .local v2, projection:[Ljava/lang/String;
    iget-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mContentResolver:Landroid/content/ContentResolver;

    #@10
    sget-object v1, Lcom/lge/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@12
    const-string v3, "type=2 OR type=6502 OR type=6 OR type=12 OR type=1111 OR type=1112 OR type=10002"

    #@14
    const-string v5, "date DESC LIMIT 1"

    #@16
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@19
    move-result-object v8

    #@1a
    .line 174
    .local v8, cursor:Landroid/database/Cursor;
    if-nez v8, :cond_1d

    #@1c
    .line 197
    :goto_1c
    return-object v4

    #@1d
    .line 180
    :cond_1d
    :try_start_1d
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I
    :try_end_20
    .catchall {:try_start_1d .. :try_end_20} :catchall_4d

    #@20
    move-result v0

    #@21
    if-ge v0, v10, :cond_27

    #@23
    .line 197
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@26
    goto :goto_1c

    #@27
    .line 183
    :cond_27
    :try_start_27
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    #@2a
    .line 185
    const-string v0, "type"

    #@2c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@2f
    move-result v7

    #@30
    .line 186
    .local v7, columnType:I
    invoke-interface {v8, v7}, Landroid/database/Cursor;->getInt(I)I

    #@33
    move-result v0

    #@34
    const/4 v1, 0x6

    #@35
    if-ne v0, v1, :cond_49

    #@37
    .line 187
    const/4 v0, 0x1

    #@38
    sput-boolean v0, Lcom/android/bluetooth/hfp/AtPhonebook;->isVideoCall:Z

    #@3a
    .line 193
    :goto_3a
    const-string v0, "number"

    #@3c
    invoke-interface {v8, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    #@3f
    move-result v6

    #@40
    .line 194
    .local v6, column:I
    invoke-interface {v8, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_43
    .catchall {:try_start_27 .. :try_end_43} :catchall_4d

    #@43
    move-result-object v9

    #@44
    .line 197
    .local v9, number:Ljava/lang/String;
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@47
    move-object v4, v9

    #@48
    goto :goto_1c

    #@49
    .line 190
    .end local v6           #column:I
    .end local v9           #number:Ljava/lang/String;
    :cond_49
    const/4 v0, 0x0

    #@4a
    :try_start_4a
    sput-boolean v0, Lcom/android/bluetooth/hfp/AtPhonebook;->isVideoCall:Z
    :try_end_4c
    .catchall {:try_start_4a .. :try_end_4c} :catchall_4d

    #@4c
    goto :goto_3a

    #@4d
    .line 197
    .end local v7           #columnType:I
    :catchall_4d
    move-exception v0

    #@4e
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@51
    throw v0
.end method

.method public handleCpbrCommand(Ljava/lang/String;ILandroid/bluetooth/BluetoothDevice;)V
    .registers 19
    .parameter "atString"
    .parameter "type"
    .parameter "remoteDevice"

    #@0
    .prologue
    .line 363
    new-instance v12, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v13, "handleCpbrCommand - atString = "

    #@7
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v12

    #@b
    move-object/from16 v0, p1

    #@d
    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v12

    #@11
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v12

    #@15
    invoke-static {v12}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@18
    .line 364
    const/4 v4, 0x0

    #@19
    .line 365
    .local v4, atCommandResult:I
    const/4 v2, -0x1

    #@1a
    .line 366
    .local v2, atCommandErrorCode:I
    const/4 v3, 0x0

    #@1b
    .line 367
    .local v3, atCommandResponse:Ljava/lang/String;
    packed-switch p2, :pswitch_data_182

    #@1e
    .line 472
    const-string v12, "handleCpbrCommand - invalid chars"

    #@20
    invoke-static {v12}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@23
    .line 473
    const/16 v2, 0x19

    #@25
    .line 474
    iget-object v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@27
    invoke-virtual {v12, v4, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@2a
    .line 476
    :cond_2a
    :goto_2a
    return-void

    #@2b
    .line 374
    :pswitch_2b
    const-string v12, "handleCpbrCommand - test command"

    #@2d
    invoke-static {v12}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@30
    .line 380
    const-string v12, "SM"

    #@32
    iget-object v13, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCurrentPhonebook:Ljava/lang/String;

    #@34
    invoke-virtual {v12, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@37
    move-result v12

    #@38
    if-eqz v12, :cond_6c

    #@3a
    const-string v12, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@3c
    invoke-static {v12}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@3f
    move-result v12

    #@40
    if-nez v12, :cond_6c

    #@42
    .line 382
    const/4 v11, 0x0

    #@43
    .line 403
    .local v11, size:I
    :goto_43
    if-nez v11, :cond_a1

    #@45
    .line 404
    new-instance v12, Ljava/lang/StringBuilder;

    #@47
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@4a
    const-string v13, "+CPBR: (0-"

    #@4c
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v12

    #@50
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@53
    move-result-object v12

    #@54
    const-string v13, "),30,30"

    #@56
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@59
    move-result-object v12

    #@5a
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5d
    move-result-object v3

    #@5e
    .line 412
    :goto_5e
    const/4 v4, 0x1

    #@5f
    .line 413
    if-eqz v3, :cond_66

    #@61
    .line 414
    iget-object v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@63
    invoke-virtual {v12, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseStringNative(Ljava/lang/String;)Z

    #@66
    .line 416
    :cond_66
    iget-object v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@68
    invoke-virtual {v12, v4, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@6b
    goto :goto_2a

    #@6c
    .line 384
    .end local v11           #size:I
    :cond_6c
    iget-object v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCurrentPhonebook:Ljava/lang/String;

    #@6e
    const/4 v13, 0x1

    #@6f
    invoke-direct {p0, v12, v13}, Lcom/android/bluetooth/hfp/AtPhonebook;->getPhonebookResult(Ljava/lang/String;Z)Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@72
    move-result-object v10

    #@73
    .line 385
    .local v10, pbr:Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    if-nez v10, :cond_7c

    #@75
    .line 386
    const/4 v2, 0x3

    #@76
    .line 387
    iget-object v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@78
    invoke-virtual {v12, v4, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@7b
    goto :goto_2a

    #@7c
    .line 390
    :cond_7c
    iget-object v12, v10, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@7e
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    #@81
    move-result v11

    #@82
    .line 391
    .restart local v11       #size:I
    new-instance v12, Ljava/lang/StringBuilder;

    #@84
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@87
    const-string v13, "handleCpbrCommand - size = "

    #@89
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8c
    move-result-object v12

    #@8d
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@90
    move-result-object v12

    #@91
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@94
    move-result-object v12

    #@95
    invoke-static {v12}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@98
    .line 392
    iget-object v12, v10, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@9a
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    #@9d
    .line 393
    const/4 v12, 0x0

    #@9e
    iput-object v12, v10, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@a0
    goto :goto_43

    #@a1
    .line 405
    .end local v10           #pbr:Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    :cond_a1
    const/4 v12, 0x1

    #@a2
    if-ne v11, v12, :cond_c0

    #@a4
    .line 406
    add-int/lit8 v11, v11, 0x1

    #@a6
    .line 407
    new-instance v12, Ljava/lang/StringBuilder;

    #@a8
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@ab
    const-string v13, "+CPBR: (1-"

    #@ad
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v12

    #@b1
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b4
    move-result-object v12

    #@b5
    const-string v13, "),30,30"

    #@b7
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v12

    #@bb
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@be
    move-result-object v3

    #@bf
    goto :goto_5e

    #@c0
    .line 409
    :cond_c0
    new-instance v12, Ljava/lang/StringBuilder;

    #@c2
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@c5
    const-string v13, "+CPBR: (1-"

    #@c7
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v12

    #@cb
    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ce
    move-result-object v12

    #@cf
    const-string v13, "),30,30"

    #@d1
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d4
    move-result-object v12

    #@d5
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d8
    move-result-object v3

    #@d9
    goto :goto_5e

    #@da
    .line 423
    .end local v11           #size:I
    :pswitch_da
    const-string v12, "handleCpbrCommand - set/read command"

    #@dc
    invoke-static {v12}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@df
    .line 424
    iget v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@e1
    const/4 v13, -0x1

    #@e2
    if-eq v12, v13, :cond_ec

    #@e4
    .line 426
    const/4 v2, 0x3

    #@e5
    .line 427
    iget-object v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@e7
    invoke-virtual {v12, v4, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@ea
    goto/16 :goto_2a

    #@ec
    .line 433
    :cond_ec
    const-string v12, "="

    #@ee
    move-object/from16 v0, p1

    #@f0
    invoke-virtual {v0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@f3
    move-result-object v12

    #@f4
    array-length v12, v12

    #@f5
    const/4 v13, 0x2

    #@f6
    if-ge v12, v13, :cond_ff

    #@f8
    .line 434
    iget-object v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@fa
    invoke-virtual {v12, v4, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@fd
    goto/16 :goto_2a

    #@ff
    .line 437
    :cond_ff
    const-string v12, "="

    #@101
    move-object/from16 v0, p1

    #@103
    invoke-virtual {v0, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@106
    move-result-object v12

    #@107
    const/4 v13, 0x1

    #@108
    aget-object v1, v12, v13

    #@10a
    .line 438
    .local v1, atCommand:Ljava/lang/String;
    const-string v12, ","

    #@10c
    invoke-virtual {v1, v12}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@10f
    move-result-object v9

    #@110
    .line 439
    .local v9, indices:[Ljava/lang/String;
    const/4 v6, 0x0

    #@111
    .local v6, i:I
    :goto_111
    array-length v12, v9

    #@112
    if-ge v6, v12, :cond_127

    #@114
    .line 441
    aget-object v12, v9, v6

    #@116
    const/16 v13, 0x3b

    #@118
    const/16 v14, 0x20

    #@11a
    invoke-virtual {v12, v13, v14}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@11d
    move-result-object v12

    #@11e
    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@121
    move-result-object v12

    #@122
    aput-object v12, v9, v6

    #@124
    .line 439
    add-int/lit8 v6, v6, 0x1

    #@126
    goto :goto_111

    #@127
    .line 444
    :cond_127
    const/4 v12, 0x0

    #@128
    :try_start_128
    aget-object v12, v9, v12

    #@12a
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@12d
    move-result v7

    #@12e
    .line 445
    .local v7, index1:I
    array-length v12, v9
    :try_end_12f
    .catch Ljava/lang/Exception; {:try_start_128 .. :try_end_12f} :catch_15d

    #@12f
    const/4 v13, 0x1

    #@130
    if-ne v12, v13, :cond_155

    #@132
    .line 446
    move v8, v7

    #@133
    .line 457
    .local v8, index2:I
    :goto_133
    iput v7, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@135
    .line 458
    iput v8, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex2:I

    #@137
    .line 459
    const/4 v12, 0x1

    #@138
    iput-boolean v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCheckingAccessPermission:Z

    #@13a
    .line 461
    move-object/from16 v0, p3

    #@13c
    invoke-direct {p0, v0}, Lcom/android/bluetooth/hfp/AtPhonebook;->checkAccessPermission(Landroid/bluetooth/BluetoothDevice;)Z

    #@13f
    move-result v12

    #@140
    if-eqz v12, :cond_2a

    #@142
    .line 462
    const/4 v12, 0x0

    #@143
    iput-boolean v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCheckingAccessPermission:Z

    #@145
    .line 463
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/AtPhonebook;->processCpbrCommand()I

    #@148
    move-result v4

    #@149
    .line 464
    const/4 v12, -0x1

    #@14a
    iput v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex2:I

    #@14c
    iput v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@14e
    .line 465
    iget-object v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@150
    invoke-virtual {v12, v4, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@153
    goto/16 :goto_2a

    #@155
    .line 448
    .end local v8           #index2:I
    :cond_155
    const/4 v12, 0x1

    #@156
    :try_start_156
    aget-object v12, v9, v12

    #@158
    invoke-static {v12}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_15b
    .catch Ljava/lang/Exception; {:try_start_156 .. :try_end_15b} :catch_15d

    #@15b
    move-result v8

    #@15c
    .restart local v8       #index2:I
    goto :goto_133

    #@15d
    .line 451
    .end local v7           #index1:I
    .end local v8           #index2:I
    :catch_15d
    move-exception v5

    #@15e
    .line 452
    .local v5, e:Ljava/lang/Exception;
    new-instance v12, Ljava/lang/StringBuilder;

    #@160
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@163
    const-string v13, "handleCpbrCommand - exception - invalid chars: "

    #@165
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@168
    move-result-object v12

    #@169
    invoke-virtual {v5}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@16c
    move-result-object v13

    #@16d
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@170
    move-result-object v12

    #@171
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@174
    move-result-object v12

    #@175
    invoke-static {v12}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@178
    .line 453
    const/16 v2, 0x19

    #@17a
    .line 454
    iget-object v12, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@17c
    invoke-virtual {v12, v4, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@17f
    goto/16 :goto_2a

    #@181
    .line 367
    nop

    #@182
    :pswitch_data_182
    .packed-switch 0x0
        :pswitch_da
        :pswitch_da
        :pswitch_2b
    .end packed-switch
.end method

.method public handleCpbsCommand(Ljava/lang/String;I)V
    .registers 14
    .parameter "atString"
    .parameter "type"

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x1

    #@2
    .line 273
    new-instance v7, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v8, "handleCpbsCommand - atString = "

    #@9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v7

    #@d
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v7

    #@11
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v7

    #@15
    invoke-static {v7}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@18
    .line 274
    const/4 v3, 0x0

    #@19
    .line 275
    .local v3, atCommandResult:I
    const/4 v1, -0x1

    #@1a
    .line 276
    .local v1, atCommandErrorCode:I
    const/4 v2, 0x0

    #@1b
    .line 277
    .local v2, atCommandResponse:Ljava/lang/String;
    packed-switch p2, :pswitch_data_146

    #@1e
    .line 353
    const-string v7, "handleCpbsCommand - invalid chars"

    #@20
    invoke-static {v7}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@23
    .line 354
    const/16 v1, 0x19

    #@25
    .line 356
    :goto_25
    if-eqz v2, :cond_2c

    #@27
    .line 357
    iget-object v7, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@29
    invoke-virtual {v7, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseStringNative(Ljava/lang/String;)Z

    #@2c
    .line 359
    :cond_2c
    iget-object v7, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2e
    invoke-virtual {v7, v3, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@31
    .line 360
    return-void

    #@32
    .line 279
    :pswitch_32
    const-string v7, "handleCpbsCommand - read command"

    #@34
    invoke-static {v7}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@37
    .line 282
    const-string v7, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@39
    invoke-static {v7}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@3c
    move-result v7

    #@3d
    if-nez v7, :cond_6e

    #@3f
    .line 283
    const-string v7, "SM"

    #@41
    iget-object v8, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCurrentPhonebook:Ljava/lang/String;

    #@43
    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v7

    #@47
    if-eqz v7, :cond_6e

    #@49
    .line 284
    new-instance v7, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v8, "+CPBS: \"SM\",0,"

    #@50
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v7

    #@54
    invoke-direct {p0, v10}, Lcom/android/bluetooth/hfp/AtPhonebook;->getMaxPhoneBookSize(I)I

    #@57
    move-result v8

    #@58
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v7

    #@5c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v2

    #@60
    .line 285
    const/4 v3, 0x1

    #@61
    .line 286
    if-eqz v2, :cond_68

    #@63
    .line 287
    iget-object v7, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@65
    invoke-virtual {v7, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseStringNative(Ljava/lang/String;)Z

    #@68
    .line 289
    :cond_68
    iget-object v7, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@6a
    invoke-virtual {v7, v3, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@6d
    goto :goto_25

    #@6e
    .line 294
    :cond_6e
    iget-object v7, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCurrentPhonebook:Ljava/lang/String;

    #@70
    invoke-direct {p0, v7, v9}, Lcom/android/bluetooth/hfp/AtPhonebook;->getPhonebookResult(Ljava/lang/String;Z)Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@73
    move-result-object v5

    #@74
    .line 295
    .local v5, pbr:Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    if-nez v5, :cond_7d

    #@76
    .line 296
    const/4 v1, 0x4

    #@77
    .line 297
    iget-object v7, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@79
    invoke-virtual {v7, v3, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@7c
    goto :goto_25

    #@7d
    .line 300
    :cond_7d
    iget-object v7, v5, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@7f
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    #@82
    move-result v6

    #@83
    .line 301
    .local v6, size:I
    new-instance v7, Ljava/lang/StringBuilder;

    #@85
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@88
    const-string v8, "+CPBS: \""

    #@8a
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v7

    #@8e
    iget-object v8, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCurrentPhonebook:Ljava/lang/String;

    #@90
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v7

    #@94
    const-string v8, "\","

    #@96
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@99
    move-result-object v7

    #@9a
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v7

    #@9e
    const-string v8, ","

    #@a0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v7

    #@a4
    invoke-direct {p0, v6}, Lcom/android/bluetooth/hfp/AtPhonebook;->getMaxPhoneBookSize(I)I

    #@a7
    move-result v8

    #@a8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ab
    move-result-object v7

    #@ac
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@af
    move-result-object v2

    #@b0
    .line 302
    iget-object v7, v5, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@b2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    #@b5
    .line 303
    const/4 v7, 0x0

    #@b6
    iput-object v7, v5, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@b8
    .line 304
    const/4 v3, 0x1

    #@b9
    .line 305
    goto/16 :goto_25

    #@bb
    .line 307
    .end local v5           #pbr:Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    .end local v6           #size:I
    :pswitch_bb
    const-string v7, "handleCpbsCommand - test command"

    #@bd
    invoke-static {v7}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@c0
    .line 312
    const-string v7, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@c2
    invoke-static {v7}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@c5
    move-result v7

    #@c6
    if-eqz v7, :cond_cd

    #@c8
    .line 313
    const-string v2, "+CPBS: (\"ME\",\"SM\",\"DC\",\"RC\",\"MC\")"

    #@ca
    .line 318
    :goto_ca
    const/4 v3, 0x1

    #@cb
    .line 319
    goto/16 :goto_25

    #@cd
    .line 315
    :cond_cd
    const-string v2, "+CPBS: (\"ME\",\"DC\",\"RC\",\"MC\")"

    #@cf
    goto :goto_ca

    #@d0
    .line 321
    :pswitch_d0
    const-string v7, "handleCpbsCommand - set command"

    #@d2
    invoke-static {v7}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@d5
    .line 322
    const-string v7, "="

    #@d7
    invoke-virtual {p1, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@da
    move-result-object v0

    #@db
    .line 324
    .local v0, args:[Ljava/lang/String;
    array-length v7, v0

    #@dc
    const/4 v8, 0x2

    #@dd
    if-lt v7, v8, :cond_e5

    #@df
    aget-object v7, v0, v9

    #@e1
    instance-of v7, v7, Ljava/lang/String;

    #@e3
    if-nez v7, :cond_ec

    #@e5
    .line 325
    :cond_e5
    iget-object v7, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@e7
    invoke-virtual {v7, v3, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@ea
    goto/16 :goto_25

    #@ec
    .line 328
    :cond_ec
    aget-object v7, v0, v9

    #@ee
    invoke-virtual {v7}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@f1
    move-result-object v4

    #@f2
    .line 329
    .local v4, pb:Ljava/lang/String;
    :goto_f2
    const-string v7, "\""

    #@f4
    invoke-virtual {v4, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    #@f7
    move-result v7

    #@f8
    if-eqz v7, :cond_105

    #@fa
    .line 330
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@fd
    move-result v7

    #@fe
    add-int/lit8 v7, v7, -0x1

    #@100
    invoke-virtual {v4, v10, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@103
    move-result-object v4

    #@104
    goto :goto_f2

    #@105
    .line 332
    :cond_105
    :goto_105
    const-string v7, "\""

    #@107
    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@10a
    move-result v7

    #@10b
    if-eqz v7, :cond_116

    #@10d
    .line 333
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    #@110
    move-result v7

    #@111
    invoke-virtual {v4, v9, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@114
    move-result-object v4

    #@115
    goto :goto_105

    #@116
    .line 336
    :cond_116
    invoke-direct {p0, v4, v10}, Lcom/android/bluetooth/hfp/AtPhonebook;->getPhonebookResult(Ljava/lang/String;Z)Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@119
    move-result-object v7

    #@11a
    if-nez v7, :cond_140

    #@11c
    .line 342
    new-instance v7, Ljava/lang/StringBuilder;

    #@11e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@121
    const-string v8, "Dont know phonebook: \'"

    #@123
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v7

    #@127
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12a
    move-result-object v7

    #@12b
    const-string v8, "\'"

    #@12d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@130
    move-result-object v7

    #@131
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v7

    #@135
    invoke-static {v7}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@138
    .line 344
    const/4 v1, 0x3

    #@139
    .line 345
    iget-object v7, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@13b
    invoke-virtual {v7, v3, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@13e
    goto/16 :goto_25

    #@140
    .line 348
    :cond_140
    iput-object v4, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCurrentPhonebook:Ljava/lang/String;

    #@142
    .line 349
    const/4 v3, 0x1

    #@143
    .line 350
    goto/16 :goto_25

    #@145
    .line 277
    nop

    #@146
    :pswitch_data_146
    .packed-switch 0x0
        :pswitch_32
        :pswitch_d0
        :pswitch_bb
    .end packed-switch
.end method

.method public handleCscsCommand(Ljava/lang/String;I)V
    .registers 11
    .parameter "atString"
    .parameter "type"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 227
    new-instance v5, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v6, "handleCscsCommand - atString = "

    #@8
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v5

    #@c
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v5

    #@10
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v5

    #@14
    invoke-static {v5}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@17
    .line 229
    const/4 v3, 0x0

    #@18
    .line 230
    .local v3, atCommandResult:I
    const/4 v1, -0x1

    #@19
    .line 231
    .local v1, atCommandErrorCode:I
    const/4 v2, 0x0

    #@1a
    .line 232
    .local v2, atCommandResponse:Ljava/lang/String;
    packed-switch p2, :pswitch_data_b0

    #@1d
    .line 262
    const-string v5, "handleCscsCommand - Invalid chars"

    #@1f
    invoke-static {v5}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@22
    .line 263
    const/16 v1, 0x19

    #@24
    .line 265
    :goto_24
    if-eqz v2, :cond_2b

    #@26
    .line 266
    iget-object v5, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@28
    invoke-virtual {v5, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseStringNative(Ljava/lang/String;)Z

    #@2b
    .line 268
    :cond_2b
    iget-object v5, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2d
    invoke-virtual {v5, v3, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@30
    .line 269
    return-void

    #@31
    .line 234
    :pswitch_31
    const-string v5, "handleCscsCommand - Read Command"

    #@33
    invoke-static {v5}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@36
    .line 235
    new-instance v5, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v6, "+CSCS: \""

    #@3d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v5

    #@41
    iget-object v6, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCharacterSet:Ljava/lang/String;

    #@43
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v5

    #@47
    const-string v6, "\""

    #@49
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v5

    #@4d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    .line 236
    const/4 v3, 0x1

    #@52
    .line 237
    goto :goto_24

    #@53
    .line 239
    :pswitch_53
    const-string v5, "handleCscsCommand - Test Command"

    #@55
    invoke-static {v5}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@58
    .line 240
    const-string v2, "+CSCS: (\"UTF-8\",\"IRA\",\"GSM\")"

    #@5a
    .line 241
    const/4 v3, 0x1

    #@5b
    .line 242
    goto :goto_24

    #@5c
    .line 244
    :pswitch_5c
    const-string v5, "handleCscsCommand - Set Command"

    #@5e
    invoke-static {v5}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@61
    .line 245
    const-string v5, "="

    #@63
    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@66
    move-result-object v0

    #@67
    .line 246
    .local v0, args:[Ljava/lang/String;
    array-length v5, v0

    #@68
    const/4 v6, 0x2

    #@69
    if-lt v5, v6, :cond_71

    #@6b
    aget-object v5, v0, v7

    #@6d
    instance-of v5, v5, Ljava/lang/String;

    #@6f
    if-nez v5, :cond_77

    #@71
    .line 247
    :cond_71
    iget-object v5, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@73
    invoke-virtual {v5, v3, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseCodeNative(II)Z

    #@76
    goto :goto_24

    #@77
    .line 250
    :cond_77
    const-string v5, "="

    #@79
    invoke-virtual {p1, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@7c
    move-result-object v5

    #@7d
    aget-object v4, v5, v7

    #@7f
    .line 251
    .local v4, characterSet:Ljava/lang/String;
    const-string v5, "\""

    #@81
    const-string v6, ""

    #@83
    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@86
    move-result-object v4

    #@87
    .line 252
    const-string v5, "GSM"

    #@89
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8c
    move-result v5

    #@8d
    if-nez v5, :cond_a7

    #@8f
    const-string v5, "IRA"

    #@91
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@94
    move-result v5

    #@95
    if-nez v5, :cond_a7

    #@97
    const-string v5, "UTF-8"

    #@99
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9c
    move-result v5

    #@9d
    if-nez v5, :cond_a7

    #@9f
    const-string v5, "UTF8"

    #@a1
    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@a4
    move-result v5

    #@a5
    if-eqz v5, :cond_ac

    #@a7
    .line 254
    :cond_a7
    iput-object v4, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCharacterSet:Ljava/lang/String;

    #@a9
    .line 255
    const/4 v3, 0x1

    #@aa
    goto/16 :goto_24

    #@ac
    .line 257
    :cond_ac
    const/4 v1, 0x4

    #@ad
    .line 259
    goto/16 :goto_24

    #@af
    .line 232
    nop

    #@b0
    :pswitch_data_b0
    .packed-switch 0x0
        :pswitch_31
        :pswitch_5c
        :pswitch_53
    .end packed-switch
.end method

.method processCpbrCommand()I
    .registers 23

    #@0
    .prologue
    .line 604
    const-string v1, "processCpbrCommand"

    #@2
    invoke-static {v1}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@5
    .line 605
    const/4 v9, 0x0

    #@6
    .line 606
    .local v9, atCommandResult:I
    const/4 v7, -0x1

    #@7
    .line 607
    .local v7, atCommandErrorCode:I
    const/4 v8, 0x0

    #@8
    .line 608
    .local v8, atCommandResponse:Ljava/lang/String;
    new-instance v20, Ljava/lang/StringBuilder;

    #@a
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    .line 616
    .local v20, response:Ljava/lang/StringBuilder;
    const-string v1, "SM"

    #@f
    move-object/from16 v0, p0

    #@11
    iget-object v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCurrentPhonebook:Ljava/lang/String;

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_24

    #@19
    const-string v1, "LGBT_COMMON_SUPPORT_SIM_PHONEBOOK"

    #@1b
    invoke-static {v1}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@1e
    move-result v1

    #@1f
    if-nez v1, :cond_24

    #@21
    .line 618
    const/4 v9, 0x1

    #@22
    move v10, v9

    #@23
    .line 754
    .end local v9           #atCommandResult:I
    .local v10, atCommandResult:I
    :goto_23
    return v10

    #@24
    .line 623
    .end local v10           #atCommandResult:I
    .restart local v9       #atCommandResult:I
    :cond_24
    move-object/from16 v0, p0

    #@26
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCurrentPhonebook:Ljava/lang/String;

    #@28
    const/4 v2, 0x1

    #@29
    move-object/from16 v0, p0

    #@2b
    invoke-direct {v0, v1, v2}, Lcom/android/bluetooth/hfp/AtPhonebook;->getPhonebookResult(Ljava/lang/String;Z)Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;

    #@2e
    move-result-object v17

    #@2f
    .line 624
    .local v17, pbr:Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;
    if-nez v17, :cond_34

    #@31
    .line 625
    const/4 v7, 0x3

    #@32
    move v10, v9

    #@33
    .line 626
    .end local v9           #atCommandResult:I
    .restart local v10       #atCommandResult:I
    goto :goto_23

    #@34
    .line 633
    .end local v10           #atCommandResult:I
    .restart local v9       #atCommandResult:I
    :cond_34
    move-object/from16 v0, v17

    #@36
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@38
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    #@3b
    move-result v1

    #@3c
    if-eqz v1, :cond_6a

    #@3e
    move-object/from16 v0, p0

    #@40
    iget v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@42
    if-lez v1, :cond_6a

    #@44
    move-object/from16 v0, p0

    #@46
    iget v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex2:I

    #@48
    move-object/from16 v0, p0

    #@4a
    iget v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@4c
    if-lt v1, v2, :cond_6a

    #@4e
    move-object/from16 v0, p0

    #@50
    iget v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex2:I

    #@52
    move-object/from16 v0, v17

    #@54
    iget-object v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@56
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    #@59
    move-result v2

    #@5a
    if-gt v1, v2, :cond_6a

    #@5c
    move-object/from16 v0, p0

    #@5e
    iget v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@60
    move-object/from16 v0, v17

    #@62
    iget-object v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@64
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    #@67
    move-result v2

    #@68
    if-le v1, v2, :cond_6d

    #@6a
    .line 635
    :cond_6a
    const/4 v9, 0x1

    #@6b
    move v10, v9

    #@6c
    .line 636
    .end local v9           #atCommandResult:I
    .restart local v10       #atCommandResult:I
    goto :goto_23

    #@6d
    .line 640
    .end local v10           #atCommandResult:I
    .restart local v9       #atCommandResult:I
    :cond_6d
    const/4 v9, 0x1

    #@6e
    .line 641
    const/4 v12, -0x1

    #@6f
    .line 642
    .local v12, errorDetected:I
    move-object/from16 v0, v17

    #@71
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@73
    move-object/from16 v0, p0

    #@75
    iget v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@77
    add-int/lit8 v2, v2, -0x1

    #@79
    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    #@7c
    .line 643
    new-instance v1, Ljava/lang/StringBuilder;

    #@7e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@81
    const-string v2, "mCpbrIndex1 = "

    #@83
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@86
    move-result-object v1

    #@87
    move-object/from16 v0, p0

    #@89
    iget v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@8b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v1

    #@8f
    const-string v2, " and mCpbrIndex2 = "

    #@91
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v1

    #@95
    move-object/from16 v0, p0

    #@97
    iget v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex2:I

    #@99
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v1

    #@9d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a0
    move-result-object v1

    #@a1
    invoke-static {v1}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@a4
    .line 644
    move-object/from16 v0, p0

    #@a6
    iget v13, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@a8
    .local v13, index:I
    :goto_a8
    move-object/from16 v0, p0

    #@aa
    iget v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex2:I

    #@ac
    if-gt v13, v1, :cond_21b

    #@ae
    .line 645
    move-object/from16 v0, v17

    #@b0
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@b2
    move-object/from16 v0, v17

    #@b4
    iget v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->numberColumn:I

    #@b6
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@b9
    move-result-object v16

    #@ba
    .line 646
    .local v16, number:Ljava/lang/String;
    const/4 v14, 0x0

    #@bb
    .line 647
    .local v14, name:Ljava/lang/String;
    const/16 v21, -0x1

    #@bd
    .line 649
    .local v21, type:I
    const-string v1, "-1"

    #@bf
    move-object/from16 v0, v16

    #@c1
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c4
    move-result v1

    #@c5
    if-nez v1, :cond_db

    #@c7
    const-string v1, "-2"

    #@c9
    move-object/from16 v0, v16

    #@cb
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ce
    move-result v1

    #@cf
    if-nez v1, :cond_db

    #@d1
    const-string v1, "-3"

    #@d3
    move-object/from16 v0, v16

    #@d5
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d8
    move-result v1

    #@d9
    if-eqz v1, :cond_232

    #@db
    .line 651
    :cond_db
    const-string v16, ""

    #@dd
    .line 652
    move-object/from16 v0, p0

    #@df
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mContext:Landroid/content/Context;

    #@e1
    const v2, 0x7f070009

    #@e4
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@e7
    move-result-object v14

    #@e8
    .line 689
    :cond_e8
    :goto_e8
    if-nez v14, :cond_ec

    #@ea
    .line 690
    const-string v14, ""

    #@ec
    .line 692
    :cond_ec
    invoke-virtual {v14}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@ef
    move-result-object v14

    #@f0
    .line 693
    invoke-virtual {v14}, Ljava/lang/String;->length()I

    #@f3
    move-result v1

    #@f4
    const/16 v2, 0x1c

    #@f6
    if-le v1, v2, :cond_ff

    #@f8
    .line 694
    const/4 v1, 0x0

    #@f9
    const/16 v2, 0x1c

    #@fb
    invoke-virtual {v14, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@fe
    move-result-object v14

    #@ff
    .line 697
    :cond_ff
    move-object/from16 v0, v17

    #@101
    iget v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->typeColumn:I

    #@103
    const/4 v2, -0x1

    #@104
    if-eq v1, v2, :cond_12d

    #@106
    .line 698
    move-object/from16 v0, v17

    #@108
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@10a
    move-object/from16 v0, v17

    #@10c
    iget v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->typeColumn:I

    #@10e
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    #@111
    move-result v21

    #@112
    .line 699
    new-instance v1, Ljava/lang/StringBuilder;

    #@114
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@117
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11a
    move-result-object v1

    #@11b
    const-string v2, "/"

    #@11d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v1

    #@121
    invoke-static/range {v21 .. v21}, Lcom/android/bluetooth/hfp/AtPhonebook;->getPhoneType(I)Ljava/lang/String;

    #@124
    move-result-object v2

    #@125
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v1

    #@129
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12c
    move-result-object v14

    #@12d
    .line 702
    :cond_12d
    if-nez v16, :cond_131

    #@12f
    .line 703
    const-string v16, ""

    #@131
    .line 705
    :cond_131
    invoke-static/range {v16 .. v16}, Landroid/telephony/PhoneNumberUtils;->toaFromString(Ljava/lang/String;)I

    #@134
    move-result v19

    #@135
    .line 707
    .local v19, regionType:I
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    #@138
    move-result-object v16

    #@139
    .line 709
    if-nez v16, :cond_13d

    #@13b
    .line 710
    const-string v16, ""

    #@13d
    .line 714
    :cond_13d
    invoke-static/range {v16 .. v16}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    #@140
    move-result-object v16

    #@141
    .line 717
    const/16 v1, 0x2c

    #@143
    const/16 v2, 0x70

    #@145
    move-object/from16 v0, v16

    #@147
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@14a
    move-result-object v16

    #@14b
    .line 718
    const/16 v1, 0x3b

    #@14d
    const/16 v2, 0x77

    #@14f
    move-object/from16 v0, v16

    #@151
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    #@154
    move-result-object v16

    #@155
    .line 721
    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    #@158
    move-result v1

    #@159
    const/16 v2, 0x1e

    #@15b
    if-le v1, v2, :cond_166

    #@15d
    .line 722
    const/4 v1, 0x0

    #@15e
    const/16 v2, 0x1e

    #@160
    move-object/from16 v0, v16

    #@162
    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@165
    move-result-object v16

    #@166
    .line 724
    :cond_166
    const-string v1, "-1"

    #@168
    move-object/from16 v0, v16

    #@16a
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16d
    move-result v1

    #@16e
    if-eqz v1, :cond_17d

    #@170
    .line 726
    const-string v16, ""

    #@172
    .line 727
    move-object/from16 v0, p0

    #@174
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mContext:Landroid/content/Context;

    #@176
    const v2, 0x7f070009

    #@179
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@17c
    move-result-object v14

    #@17d
    .line 732
    :cond_17d
    const-string v1, ""

    #@17f
    invoke-virtual {v14, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@182
    move-result v1

    #@183
    if-nez v1, :cond_1a2

    #@185
    move-object/from16 v0, p0

    #@187
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCharacterSet:Ljava/lang/String;

    #@189
    const-string v2, "GSM"

    #@18b
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18e
    move-result v1

    #@18f
    if-eqz v1, :cond_1a2

    #@191
    .line 733
    invoke-static {v14}, Lcom/android/internal/telephony/GsmAlphabet;->stringToGsm8BitPacked(Ljava/lang/String;)[B

    #@194
    move-result-object v15

    #@195
    .line 734
    .local v15, nameByte:[B
    if-nez v15, :cond_29d

    #@197
    .line 735
    move-object/from16 v0, p0

    #@199
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mContext:Landroid/content/Context;

    #@19b
    const v2, 0x7f070009

    #@19e
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@1a1
    move-result-object v14

    #@1a2
    .line 741
    .end local v15           #nameByte:[B
    :cond_1a2
    :goto_1a2
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1a7
    const-string v2, "+CPBR: "

    #@1a9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ac
    move-result-object v1

    #@1ad
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1b0
    move-result-object v1

    #@1b1
    const-string v2, ",\""

    #@1b3
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b6
    move-result-object v1

    #@1b7
    move-object/from16 v0, v16

    #@1b9
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v1

    #@1bd
    const-string v2, "\","

    #@1bf
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c2
    move-result-object v1

    #@1c3
    move/from16 v0, v19

    #@1c5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v1

    #@1c9
    const-string v2, ",\""

    #@1cb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ce
    move-result-object v1

    #@1cf
    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d2
    move-result-object v1

    #@1d3
    const-string v2, "\""

    #@1d5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d8
    move-result-object v1

    #@1d9
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1dc
    move-result-object v18

    #@1dd
    .line 742
    .local v18, record:Ljava/lang/String;
    new-instance v1, Ljava/lang/StringBuilder;

    #@1df
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1e2
    move-object/from16 v0, v18

    #@1e4
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e7
    move-result-object v1

    #@1e8
    const-string v2, "\r\n\r\n"

    #@1ea
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1ed
    move-result-object v1

    #@1ee
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f1
    move-result-object v18

    #@1f2
    .line 743
    move-object/from16 v8, v18

    #@1f4
    .line 744
    new-instance v1, Ljava/lang/StringBuilder;

    #@1f6
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1f9
    const-string v2, "processCpbrCommand - atCommandResponse = "

    #@1fb
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1fe
    move-result-object v1

    #@1ff
    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@202
    move-result-object v1

    #@203
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@206
    move-result-object v1

    #@207
    invoke-static {v1}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@20a
    .line 745
    move-object/from16 v0, p0

    #@20c
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@20e
    invoke-virtual {v1, v8}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->atResponseStringNative(Ljava/lang/String;)Z

    #@211
    .line 746
    move-object/from16 v0, v17

    #@213
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@215
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    #@218
    move-result v1

    #@219
    if-nez v1, :cond_2a4

    #@21b
    .line 750
    .end local v14           #name:Ljava/lang/String;
    .end local v16           #number:Ljava/lang/String;
    .end local v18           #record:Ljava/lang/String;
    .end local v19           #regionType:I
    .end local v21           #type:I
    :cond_21b
    if-eqz v17, :cond_22f

    #@21d
    move-object/from16 v0, v17

    #@21f
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@221
    if-eqz v1, :cond_22f

    #@223
    .line 751
    move-object/from16 v0, v17

    #@225
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@227
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    #@22a
    .line 752
    const/4 v1, 0x0

    #@22b
    move-object/from16 v0, v17

    #@22d
    iput-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@22f
    :cond_22f
    move v10, v9

    #@230
    .line 754
    .end local v9           #atCommandResult:I
    .restart local v10       #atCommandResult:I
    goto/16 :goto_23

    #@232
    .line 655
    .end local v10           #atCommandResult:I
    .restart local v9       #atCommandResult:I
    .restart local v14       #name:Ljava/lang/String;
    .restart local v16       #number:Ljava/lang/String;
    .restart local v21       #type:I
    :cond_232
    move-object/from16 v0, v17

    #@234
    iget v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->nameColumn:I

    #@236
    const/4 v2, -0x1

    #@237
    if-ne v1, v2, :cond_28f

    #@239
    .line 659
    move-object/from16 v0, p0

    #@23b
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook;->mContentResolver:Landroid/content/ContentResolver;

    #@23d
    sget-object v2, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    #@23f
    move-object/from16 v0, v16

    #@241
    invoke-static {v2, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    #@244
    move-result-object v2

    #@245
    const/4 v3, 0x2

    #@246
    new-array v3, v3, [Ljava/lang/String;

    #@248
    const/4 v4, 0x0

    #@249
    const-string v5, "display_name"

    #@24b
    aput-object v5, v3, v4

    #@24d
    const/4 v4, 0x1

    #@24e
    const-string v5, "type"

    #@250
    aput-object v5, v3, v4

    #@252
    const/4 v4, 0x0

    #@253
    const/4 v5, 0x0

    #@254
    const/4 v6, 0x0

    #@255
    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@258
    move-result-object v11

    #@259
    .line 663
    .local v11, c:Landroid/database/Cursor;
    if-eqz v11, :cond_26e

    #@25b
    .line 666
    :try_start_25b
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    #@25e
    move-result v1

    #@25f
    if-eqz v1, :cond_26b

    #@261
    .line 667
    const/4 v1, 0x0

    #@262
    invoke-interface {v11, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@265
    move-result-object v14

    #@266
    .line 668
    const/4 v1, 0x1

    #@267
    invoke-interface {v11, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_26a
    .catchall {:try_start_25b .. :try_end_26a} :catchall_28a

    #@26a
    move-result v21

    #@26b
    .line 671
    :cond_26b
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@26e
    .line 682
    :cond_26e
    if-nez v14, :cond_e8

    #@270
    .line 683
    new-instance v1, Ljava/lang/StringBuilder;

    #@272
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@275
    const-string v2, "Caller ID lookup failed for "

    #@277
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27a
    move-result-object v1

    #@27b
    move-object/from16 v0, v16

    #@27d
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@280
    move-result-object v1

    #@281
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@284
    move-result-object v1

    #@285
    invoke-static {v1}, Lcom/android/bluetooth/hfp/AtPhonebook;->log(Ljava/lang/String;)V

    #@288
    goto/16 :goto_e8

    #@28a
    .line 671
    :catchall_28a
    move-exception v1

    #@28b
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    #@28e
    throw v1

    #@28f
    .line 687
    .end local v11           #c:Landroid/database/Cursor;
    :cond_28f
    move-object/from16 v0, v17

    #@291
    iget-object v1, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->cursor:Landroid/database/Cursor;

    #@293
    move-object/from16 v0, v17

    #@295
    iget v2, v0, Lcom/android/bluetooth/hfp/AtPhonebook$PhonebookResult;->nameColumn:I

    #@297
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@29a
    move-result-object v14

    #@29b
    goto/16 :goto_e8

    #@29d
    .line 737
    .restart local v15       #nameByte:[B
    .restart local v19       #regionType:I
    :cond_29d
    new-instance v14, Ljava/lang/String;

    #@29f
    .end local v14           #name:Ljava/lang/String;
    invoke-direct {v14, v15}, Ljava/lang/String;-><init>([B)V

    #@2a2
    .restart local v14       #name:Ljava/lang/String;
    goto/16 :goto_1a2

    #@2a4
    .line 644
    .end local v15           #nameByte:[B
    .restart local v18       #record:Ljava/lang/String;
    :cond_2a4
    add-int/lit8 v13, v13, 0x1

    #@2a6
    goto/16 :goto_a8
.end method

.method declared-synchronized resetAtState()V
    .registers 2

    #@0
    .prologue
    .line 574
    monitor-enter p0

    #@1
    :try_start_1
    const-string v0, "UTF-8"

    #@3
    iput-object v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCharacterSet:Ljava/lang/String;

    #@5
    .line 575
    const/4 v0, -0x1

    #@6
    iput v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex2:I

    #@8
    iput v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@a
    .line 576
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCheckingAccessPermission:Z
    :try_end_d
    .catchall {:try_start_1 .. :try_end_d} :catchall_f

    #@d
    .line 577
    monitor-exit p0

    #@e
    return-void

    #@f
    .line 574
    :catchall_f
    move-exception v0

    #@10
    monitor-exit p0

    #@11
    throw v0
.end method

.method public setCheckingAccessPermission(Z)V
    .registers 2
    .parameter "checkAccessPermission"

    #@0
    .prologue
    .line 218
    iput-boolean p1, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCheckingAccessPermission:Z

    #@2
    .line 219
    return-void
.end method

.method public setCpbrIndex(I)V
    .registers 2
    .parameter "cpbrIndex"

    #@0
    .prologue
    .line 222
    iput p1, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex2:I

    #@2
    iput p1, p0, Lcom/android/bluetooth/hfp/AtPhonebook;->mCpbrIndex1:I

    #@4
    .line 223
    return-void
.end method
