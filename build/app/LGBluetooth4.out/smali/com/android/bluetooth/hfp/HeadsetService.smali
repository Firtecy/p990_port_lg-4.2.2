.class public Lcom/android/bluetooth/hfp/HeadsetService;
.super Lcom/android/bluetooth/btservice/ProfileService;
.source "HeadsetService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final MODIFY_PHONE_STATE:Ljava/lang/String; = "android.permission.MODIFY_PHONE_STATE"

.field private static final TAG:Ljava/lang/String; = "HeadsetService"

.field private static sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;


# instance fields
.field private final mHeadsetReceiver:Landroid/content/BroadcastReceiver;

.field private mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 46
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/ProfileService;-><init>()V

    #@3
    .line 94
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetService$1;

    #@5
    invoke-direct {v0, p0}, Lcom/android/bluetooth/hfp/HeadsetService$1;-><init>(Lcom/android/bluetooth/hfp/HeadsetService;)V

    #@8
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mHeadsetReceiver:Landroid/content/BroadcastReceiver;

    #@a
    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/hfp/HeadsetService;)Lcom/android/bluetooth/hfp/HeadsetStateMachine;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/bluetooth/hfp/HeadsetService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetService;->isAvailable()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/bluetooth/hfp/HeadsetService;[I)Ljava/util/List;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/bluetooth/hfp/HeadsetService;IIILjava/lang/String;I)V
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p5}, Lcom/android/bluetooth/hfp/HeadsetService;->phoneStateChanged(IIILjava/lang/String;I)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/bluetooth/hfp/HeadsetService;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->roamChanged(Z)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/bluetooth/hfp/HeadsetService;IIIIZLjava/lang/String;I)V
    .registers 8
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"
    .parameter "x5"
    .parameter "x6"
    .parameter "x7"

    #@0
    .prologue
    .line 46
    invoke-direct/range {p0 .. p7}, Lcom/android/bluetooth/hfp/HeadsetService;->clccResponse(IIIIZLjava/lang/String;I)V

    #@3
    return-void
.end method

.method private clccResponse(IIIIZLjava/lang/String;I)V
    .registers 18
    .parameter "index"
    .parameter "direction"
    .parameter "status"
    .parameter "mode"
    .parameter "mpty"
    .parameter "number"
    .parameter "type"

    #@0
    .prologue
    .line 552
    const-string v0, "android.permission.MODIFY_PHONE_STATE"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 553
    iget-object v8, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@8
    const/16 v9, 0xd

    #@a
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetClccResponse;

    #@c
    move v1, p1

    #@d
    move v2, p2

    #@e
    move v3, p3

    #@f
    move v4, p4

    #@10
    move v5, p5

    #@11
    move-object/from16 v6, p6

    #@13
    move/from16 v7, p7

    #@15
    invoke-direct/range {v0 .. v7}, Lcom/android/bluetooth/hfp/HeadsetClccResponse;-><init>(IIIIZLjava/lang/String;I)V

    #@18
    invoke-virtual {v8, v9, v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@1b
    .line 555
    return-void
.end method

.method private static declared-synchronized clearHeadsetService()V
    .registers 2

    #@0
    .prologue
    .line 360
    const-class v0, Lcom/android/bluetooth/hfp/HeadsetService;

    #@2
    monitor-enter v0

    #@3
    const/4 v1, 0x0

    #@4
    :try_start_4
    sput-object v1, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;
    :try_end_6
    .catchall {:try_start_4 .. :try_end_6} :catchall_8

    #@6
    .line 361
    monitor-exit v0

    #@7
    return-void

    #@8
    .line 360
    :catchall_8
    move-exception v1

    #@9
    monitor-exit v0

    #@a
    throw v1
.end method

.method private getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 4
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 400
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 401
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method public static declared-synchronized getHeadsetService()Lcom/android/bluetooth/hfp/HeadsetService;
    .registers 4

    #@0
    .prologue
    .line 326
    const-class v1, Lcom/android/bluetooth/hfp/HeadsetService;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@5
    if-eqz v0, :cond_2d

    #@7
    sget-object v0, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetService;->isAvailable()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_2d

    #@f
    .line 328
    const-string v0, "HeadsetService"

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "getHeadsetService(): returning "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    sget-object v3, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 330
    sget-object v0, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;
    :try_end_2b
    .catchall {:try_start_3 .. :try_end_2b} :catchall_4a

    #@2b
    .line 339
    :goto_2b
    monitor-exit v1

    #@2c
    return-object v0

    #@2d
    .line 333
    :cond_2d
    :try_start_2d
    sget-object v0, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@2f
    if-nez v0, :cond_3a

    #@31
    .line 334
    const-string v0, "HeadsetService"

    #@33
    const-string v2, "getHeadsetService(): service is NULL"

    #@35
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 339
    :cond_38
    :goto_38
    const/4 v0, 0x0

    #@39
    goto :goto_2b

    #@3a
    .line 335
    :cond_3a
    sget-object v0, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@3c
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetService;->isAvailable()Z

    #@3f
    move-result v0

    #@40
    if-nez v0, :cond_38

    #@42
    .line 336
    const-string v0, "HeadsetService"

    #@44
    const-string v2, "getHeadsetService(): service is not available"

    #@46
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_49
    .catchall {:try_start_2d .. :try_end_49} :catchall_4a

    #@49
    goto :goto_38

    #@4a
    .line 326
    :catchall_4a
    move-exception v0

    #@4b
    monitor-exit v1

    #@4c
    throw v0
.end method

.method private phoneStateChanged(IIILjava/lang/String;I)V
    .registers 13
    .parameter "numActive"
    .parameter "numHeld"
    .parameter "callState"
    .parameter "number"
    .parameter "type"

    #@0
    .prologue
    .line 538
    const-string v0, "android.permission.MODIFY_PHONE_STATE"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 539
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@8
    const/16 v1, 0x9

    #@a
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->obtainMessage(I)Landroid/os/Message;

    #@d
    move-result-object v6

    #@e
    .line 540
    .local v6, msg:Landroid/os/Message;
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetCallState;

    #@10
    move v1, p1

    #@11
    move v2, p2

    #@12
    move v3, p3

    #@13
    move-object v4, p4

    #@14
    move v5, p5

    #@15
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/hfp/HeadsetCallState;-><init>(IIILjava/lang/String;I)V

    #@18
    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a
    .line 541
    const/4 v0, 0x0

    #@1b
    iput v0, v6, Landroid/os/Message;->arg1:I

    #@1d
    .line 542
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@1f
    invoke-virtual {v0, v6}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(Landroid/os/Message;)V

    #@22
    .line 543
    return-void
.end method

.method private roamChanged(Z)V
    .registers 5
    .parameter "roam"

    #@0
    .prologue
    .line 546
    const-string v0, "android.permission.MODIFY_PHONE_STATE"

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@6
    .line 547
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@8
    const/16 v1, 0xc

    #@a
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@d
    move-result-object v2

    #@e
    invoke-virtual {v0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@11
    .line 548
    return-void
.end method

.method private static declared-synchronized setHeadsetService(Lcom/android/bluetooth/hfp/HeadsetService;)V
    .registers 5
    .parameter "instance"

    #@0
    .prologue
    .line 343
    const-class v1, Lcom/android/bluetooth/hfp/HeadsetService;

    #@2
    monitor-enter v1

    #@3
    if-eqz p0, :cond_29

    #@5
    :try_start_5
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetService;->isAvailable()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_29

    #@b
    .line 345
    const-string v0, "HeadsetService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "setHeadsetService(): set to: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    sget-object v3, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 347
    sput-object p0, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;
    :try_end_27
    .catchall {:try_start_5 .. :try_end_27} :catchall_35

    #@27
    .line 357
    :cond_27
    :goto_27
    monitor-exit v1

    #@28
    return-void

    #@29
    .line 350
    :cond_29
    :try_start_29
    sget-object v0, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@2b
    if-nez v0, :cond_38

    #@2d
    .line 351
    const-string v0, "HeadsetService"

    #@2f
    const-string v2, "setHeadsetService(): service not available"

    #@31
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_34
    .catchall {:try_start_29 .. :try_end_34} :catchall_35

    #@34
    goto :goto_27

    #@35
    .line 343
    :catchall_35
    move-exception v0

    #@36
    monitor-exit v1

    #@37
    throw v0

    #@38
    .line 352
    :cond_38
    :try_start_38
    sget-object v0, Lcom/android/bluetooth/hfp/HeadsetService;->sHeadsetService:Lcom/android/bluetooth/hfp/HeadsetService;

    #@3a
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetService;->isAvailable()Z

    #@3d
    move-result v0

    #@3e
    if-nez v0, :cond_27

    #@40
    .line 353
    const-string v0, "HeadsetService"

    #@42
    const-string v2, "setHeadsetService(): service is cleaning up"

    #@44
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_47
    .catchall {:try_start_38 .. :try_end_47} :catchall_35

    #@47
    goto :goto_27
.end method


# virtual methods
.method acceptIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 481
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method protected cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 88
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@6
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->cleanup()V

    #@9
    .line 90
    :cond_9
    invoke-static {}, Lcom/android/bluetooth/hfp/HeadsetService;->clearHeadsetService()V

    #@c
    .line 91
    const/4 v0, 0x1

    #@d
    return v0
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 7
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v2, 0x1

    #@2
    .line 364
    const-string v3, "android.permission.BLUETOOTH_ADMIN"

    #@4
    const-string v4, "Need BLUETOOTH ADMIN permission"

    #@6
    invoke-virtual {p0, v3, v4}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 367
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@c
    move-result v3

    #@d
    if-nez v3, :cond_10

    #@f
    .line 378
    :cond_f
    :goto_f
    return v1

    #@10
    .line 371
    :cond_10
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@12
    invoke-virtual {v3, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@15
    move-result v0

    #@16
    .line 372
    .local v0, connectionState:I
    const/4 v3, 0x2

    #@17
    if-eq v0, v3, :cond_f

    #@19
    if-eq v0, v2, :cond_f

    #@1b
    .line 377
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@1d
    invoke-virtual {v1, v2, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@20
    move v1, v2

    #@21
    .line 378
    goto :goto_f
.end method

.method connectAudio()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 495
    const-string v1, "android.permission.BLUETOOTH"

    #@3
    const-string v2, "Need BLUETOOTH permission"

    #@5
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 496
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@a
    invoke-virtual {v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isConnected()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_11

    #@10
    .line 503
    :cond_10
    :goto_10
    return v0

    #@11
    .line 499
    :cond_11
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@13
    invoke-virtual {v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isAudioOn()Z

    #@16
    move-result v1

    #@17
    if-nez v1, :cond_10

    #@19
    .line 502
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@1b
    const/4 v1, 0x3

    #@1c
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(I)V

    #@1f
    .line 503
    const/4 v0, 0x1

    #@20
    goto :goto_10
.end method

.method disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 7
    .parameter "device"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v1, 0x1

    #@2
    .line 382
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@4
    const-string v3, "Need BLUETOOTH ADMIN permission"

    #@6
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 384
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@b
    invoke-virtual {v2, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@e
    move-result v0

    #@f
    .line 385
    .local v0, connectionState:I
    if-eq v0, v4, :cond_15

    #@11
    if-eq v0, v1, :cond_15

    #@13
    .line 387
    const/4 v1, 0x0

    #@14
    .line 391
    :goto_14
    return v1

    #@15
    .line 390
    :cond_15
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@17
    invoke-virtual {v2, v4, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@1a
    goto :goto_14
.end method

.method disconnectAudio()Z
    .registers 3

    #@0
    .prologue
    .line 508
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 509
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isAudioOn()Z

    #@c
    move-result v0

    #@d
    if-nez v0, :cond_11

    #@f
    .line 510
    const/4 v0, 0x0

    #@10
    .line 513
    :goto_10
    return v0

    #@11
    .line 512
    :cond_11
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@13
    const/4 v1, 0x4

    #@14
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(I)V

    #@17
    .line 513
    const/4 v0, 0x1

    #@18
    goto :goto_10
.end method

.method getAudioState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 490
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getAudioState(Landroid/bluetooth/BluetoothDevice;)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method getBatteryUsageHint(Landroid/bluetooth/BluetoothDevice;)I
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 476
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 395
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 396
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getConnectedDevices()Ljava/util/List;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 405
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 406
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method protected getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 55
    const-string v0, "HeadsetService"

    #@2
    return-object v0
.end method

.method public getPriority(Landroid/bluetooth/BluetoothDevice;)I
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 422
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v2, "Need BLUETOOTH_ADMIN permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 424
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetService;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-static {v2}, Landroid/provider/Settings$Global;->getBluetoothHeadsetPriorityKey(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    const/4 v3, -0x1

    #@14
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@17
    move-result v0

    #@18
    .line 427
    .local v0, priority:I
    return v0
.end method

.method public initBinder()Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;
    .registers 2

    #@0
    .prologue
    .line 59
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;

    #@2
    invoke-direct {v0, p0}, Lcom/android/bluetooth/hfp/HeadsetService$BluetoothHeadsetBinder;-><init>(Lcom/android/bluetooth/hfp/HeadsetService;)V

    #@5
    return-object v0
.end method

.method isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 470
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 471
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isAudioConnected(Landroid/bluetooth/BluetoothDevice;)Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method isAudioOn()Z
    .registers 3

    #@0
    .prologue
    .line 465
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 466
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isAudioOn()Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method rejectIncomingConnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 486
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public sendBattRssiSvcRoamLevel(Ljava/lang/String;I)V
    .registers 10
    .parameter "type"
    .parameter "value"

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/16 v4, 0xb

    #@4
    const/4 v3, 0x3

    #@5
    .line 559
    const-string v0, "HeadsetService"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "[BTUI] sendBattRssiSvcRoamLevel = "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v1

    #@16
    const-string v2, "("

    #@18
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v1

    #@1c
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v2, ")"

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 560
    const-string v0, "batt_level"

    #@2f
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v0

    #@33
    if-eqz v0, :cond_40

    #@35
    .line 561
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@37
    new-instance v1, Lcom/android/bluetooth/hfp/HeadsetDeviceState;

    #@39
    invoke-direct {v1, v6, v5, v3, p2}, Lcom/android/bluetooth/hfp/HeadsetDeviceState;-><init>(IIII)V

    #@3c
    invoke-virtual {v0, v4, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@3f
    .line 573
    :cond_3f
    :goto_3f
    return-void

    #@40
    .line 563
    :cond_40
    const-string v0, "rssi_level"

    #@42
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v0

    #@46
    if-eqz v0, :cond_53

    #@48
    .line 564
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@4a
    new-instance v1, Lcom/android/bluetooth/hfp/HeadsetDeviceState;

    #@4c
    invoke-direct {v1, v6, v5, p2, v3}, Lcom/android/bluetooth/hfp/HeadsetDeviceState;-><init>(IIII)V

    #@4f
    invoke-virtual {v0, v4, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@52
    goto :goto_3f

    #@53
    .line 566
    :cond_53
    const-string v0, "svc_level"

    #@55
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@58
    move-result v0

    #@59
    if-eqz v0, :cond_66

    #@5b
    .line 567
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@5d
    new-instance v1, Lcom/android/bluetooth/hfp/HeadsetDeviceState;

    #@5f
    invoke-direct {v1, p2, v5, v3, v3}, Lcom/android/bluetooth/hfp/HeadsetDeviceState;-><init>(IIII)V

    #@62
    invoke-virtual {v0, v4, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@65
    goto :goto_3f

    #@66
    .line 569
    :cond_66
    const-string v0, "roam_level"

    #@68
    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@6b
    move-result v0

    #@6c
    if-eqz v0, :cond_3f

    #@6e
    .line 570
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@70
    new-instance v1, Lcom/android/bluetooth/hfp/HeadsetDeviceState;

    #@72
    invoke-direct {v1, v6, p2, v3, v3}, Lcom/android/bluetooth/hfp/HeadsetDeviceState;-><init>(IIII)V

    #@75
    invoke-virtual {v0, v4, v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@78
    goto :goto_3f
.end method

.method public sendScoOnOff(Z)V
    .registers 5
    .parameter "onoff"

    #@0
    .prologue
    .line 576
    const-string v1, "HeadsetService"

    #@2
    new-instance v0, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[BTUI] sendScoOnOff = "

    #@9
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    if-eqz p1, :cond_22

    #@f
    const-string v0, "on"

    #@11
    :goto_11
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v0

    #@19
    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 577
    if-eqz p1, :cond_25

    #@1e
    .line 578
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetService;->connectAudio()Z

    #@21
    .line 582
    :goto_21
    return-void

    #@22
    .line 576
    :cond_22
    const-string v0, "off"

    #@24
    goto :goto_11

    #@25
    .line 580
    :cond_25
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetService;->disconnectAudio()Z

    #@28
    goto :goto_21
.end method

.method public setPriority(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 6
    .parameter "device"
    .parameter "priority"

    #@0
    .prologue
    .line 410
    const-string v0, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v1, "Need BLUETOOTH_ADMIN permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 412
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetService;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-static {v1}, Landroid/provider/Settings$Global;->getBluetoothHeadsetPriorityKey(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@16
    .line 416
    const-string v0, "HeadsetService"

    #@18
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v2, "Saved priority "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, " = "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 418
    const/4 v0, 0x1

    #@39
    return v0
.end method

.method protected start()Z
    .registers 5

    #@0
    .prologue
    .line 63
    invoke-static {p0}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->make(Lcom/android/bluetooth/hfp/HeadsetService;)Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@3
    move-result-object v2

    #@4
    iput-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@6
    .line 64
    new-instance v1, Landroid/content/IntentFilter;

    #@8
    const-string v2, "android.intent.action.BATTERY_CHANGED"

    #@a
    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@d
    .line 65
    .local v1, filter:Landroid/content/IntentFilter;
    const-string v2, "android.media.VOLUME_CHANGED_ACTION"

    #@f
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@12
    .line 66
    const-string v2, "android.bluetooth.device.action.CONNECTION_ACCESS_REPLY"

    #@14
    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@17
    .line 68
    :try_start_17
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mHeadsetReceiver:Landroid/content/BroadcastReceiver;

    #@19
    invoke-virtual {p0, v2, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_1c
    .catch Ljava/lang/Exception; {:try_start_17 .. :try_end_1c} :catch_21

    #@1c
    .line 72
    :goto_1c
    invoke-static {p0}, Lcom/android/bluetooth/hfp/HeadsetService;->setHeadsetService(Lcom/android/bluetooth/hfp/HeadsetService;)V

    #@1f
    .line 73
    const/4 v2, 0x1

    #@20
    return v2

    #@21
    .line 69
    :catch_21
    move-exception v0

    #@22
    .line 70
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "HeadsetService"

    #@24
    const-string v3, "Unable to register headset receiver"

    #@26
    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@29
    goto :goto_1c
.end method

.method startScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 517
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@3
    invoke-virtual {v2, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@6
    move-result v0

    #@7
    .line 518
    .local v0, connectionState:I
    const/4 v2, 0x2

    #@8
    if-eq v0, v2, :cond_e

    #@a
    if-eq v0, v1, :cond_e

    #@c
    .line 520
    const/4 v1, 0x0

    #@d
    .line 523
    :goto_d
    return v1

    #@e
    .line 522
    :cond_e
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@10
    const/16 v3, 0xe

    #@12
    invoke-virtual {v2, v3, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@15
    goto :goto_d
.end method

.method startVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 7
    .parameter "device"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 431
    const-string v3, "android.permission.BLUETOOTH"

    #@4
    const-string v4, "Need BLUETOOTH permission"

    #@6
    invoke-virtual {p0, v3, v4}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 432
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@b
    invoke-virtual {v3, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@e
    move-result v0

    #@f
    .line 433
    .local v0, connectionState:I
    const/4 v3, 0x2

    #@10
    if-eq v0, v3, :cond_15

    #@12
    if-eq v0, v2, :cond_15

    #@14
    .line 446
    :goto_14
    return v1

    #@15
    .line 439
    :cond_15
    iget-object v3, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@17
    invoke-virtual {v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->isBtVrSupported()Z

    #@1a
    move-result v3

    #@1b
    if-nez v3, :cond_25

    #@1d
    .line 440
    const-string v2, "HeadsetService"

    #@1f
    const-string v3, "[BTUI] startVoiceRecognition() : AG or HF does not support BT VR."

    #@21
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_14

    #@25
    .line 445
    :cond_25
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@27
    const/4 v3, 0x5

    #@28
    invoke-virtual {v1, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(I)V

    #@2b
    move v1, v2

    #@2c
    .line 446
    goto :goto_14
.end method

.method protected stop()Z
    .registers 4

    #@0
    .prologue
    .line 78
    :try_start_0
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mHeadsetReceiver:Landroid/content/BroadcastReceiver;

    #@2
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_5} :catch_c

    #@5
    .line 82
    :goto_5
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@7
    invoke-virtual {v1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->doQuit()V

    #@a
    .line 83
    const/4 v1, 0x1

    #@b
    return v1

    #@c
    .line 79
    :catch_c
    move-exception v0

    #@d
    .line 80
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "HeadsetService"

    #@f
    const-string v2, "Unable to unregister headset receiver"

    #@11
    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14
    goto :goto_5
.end method

.method stopScoUsingVirtualVoiceCall(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 527
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@3
    invoke-virtual {v2, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@6
    move-result v0

    #@7
    .line 528
    .local v0, connectionState:I
    const/4 v2, 0x2

    #@8
    if-eq v0, v2, :cond_e

    #@a
    if-eq v0, v1, :cond_e

    #@c
    .line 530
    const/4 v1, 0x0

    #@d
    .line 533
    :goto_d
    return v1

    #@e
    .line 532
    :cond_e
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@10
    const/16 v3, 0xf

    #@12
    invoke-virtual {v2, v3, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@15
    goto :goto_d
.end method

.method stopVoiceRecognition(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 450
    const-string v2, "android.permission.BLUETOOTH"

    #@3
    const-string v3, "Need BLUETOOTH permission"

    #@5
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/hfp/HeadsetService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 454
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@a
    invoke-virtual {v2, p1}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@d
    move-result v0

    #@e
    .line 455
    .local v0, connectionState:I
    const/4 v2, 0x2

    #@f
    if-eq v0, v2, :cond_15

    #@11
    if-eq v0, v1, :cond_15

    #@13
    .line 457
    const/4 v1, 0x0

    #@14
    .line 461
    :goto_14
    return v1

    #@15
    .line 459
    :cond_15
    iget-object v2, p0, Lcom/android/bluetooth/hfp/HeadsetService;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@17
    const/4 v3, 0x6

    #@18
    invoke-virtual {v2, v3}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(I)V

    #@1b
    goto :goto_14
.end method
