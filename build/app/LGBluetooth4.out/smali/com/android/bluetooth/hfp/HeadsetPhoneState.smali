.class Lcom/android/bluetooth/hfp/HeadsetPhoneState;
.super Ljava/lang/Object;
.source "HeadsetPhoneState.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "HeadsetPhoneState"


# instance fields
.field private mBatteryCharge:I

.field private mCallState:I

.field private mListening:Z

.field private mMicVolume:I

.field private mNumActive:I

.field private mNumHeld:I

.field private mPhoneStateListener:Landroid/telephony/PhoneStateListener;

.field private mRoam:I

.field private mService:I

.field private mServiceState:Landroid/telephony/ServiceState;

.field private mSignal:I

.field private mSpeakerVolume:I

.field private mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

.field private mTelephonyManager:Landroid/telephony/TelephonyManager;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/android/bluetooth/hfp/HeadsetStateMachine;)V
    .registers 5
    .parameter "context"
    .parameter "stateMachine"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 68
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 42
    iput v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mService:I

    #@6
    .line 45
    iput v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mNumActive:I

    #@8
    .line 48
    const/4 v0, 0x6

    #@9
    iput v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mCallState:I

    #@b
    .line 51
    iput v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mNumHeld:I

    #@d
    .line 54
    iput v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mSignal:I

    #@f
    .line 57
    iput v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mRoam:I

    #@11
    .line 60
    iput v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mBatteryCharge:I

    #@13
    .line 62
    iput v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mSpeakerVolume:I

    #@15
    .line 64
    iput v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mMicVolume:I

    #@17
    .line 66
    iput-boolean v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mListening:Z

    #@19
    .line 178
    new-instance v0, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;

    #@1b
    invoke-direct {v0, p0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState$1;-><init>(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)V

    #@1e
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@20
    .line 69
    iput-object p2, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@22
    .line 70
    const-string v0, "phone"

    #@24
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@27
    move-result-object v0

    #@28
    check-cast v0, Landroid/telephony/TelephonyManager;

    #@2a
    iput-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@2c
    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)Landroid/telephony/ServiceState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mServiceState:Landroid/telephony/ServiceState;

    #@2
    return-object v0
.end method

.method static synthetic access$002(Lcom/android/bluetooth/hfp/HeadsetPhoneState;Landroid/telephony/ServiceState;)Landroid/telephony/ServiceState;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput-object p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mServiceState:Landroid/telephony/ServiceState;

    #@2
    return-object p1
.end method

.method static synthetic access$100(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mService:I

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/android/bluetooth/hfp/HeadsetPhoneState;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mService:I

    #@2
    return p1
.end method

.method static synthetic access$200(Lcom/android/bluetooth/hfp/HeadsetPhoneState;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 34
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mSignal:I

    #@2
    return v0
.end method

.method static synthetic access$202(Lcom/android/bluetooth/hfp/HeadsetPhoneState;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 34
    iput p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mSignal:I

    #@2
    return p1
.end method


# virtual methods
.method public cleanup()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 74
    const/4 v0, 0x0

    #@2
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->listenForPhoneState(Z)V

    #@5
    .line 75
    iput-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@7
    .line 76
    iput-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@9
    .line 77
    return-void
.end method

.method getBatteryCharge()I
    .registers 2

    #@0
    .prologue
    .line 143
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mBatteryCharge:I

    #@2
    return v0
.end method

.method getCallState()I
    .registers 2

    #@0
    .prologue
    .line 108
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mCallState:I

    #@2
    return v0
.end method

.method getMicVolume()I
    .registers 2

    #@0
    .prologue
    .line 159
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mMicVolume:I

    #@2
    return v0
.end method

.method getNumActiveCall()I
    .registers 2

    #@0
    .prologue
    .line 100
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mNumActive:I

    #@2
    return v0
.end method

.method getNumHeldCall()I
    .registers 2

    #@0
    .prologue
    .line 116
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mNumHeld:I

    #@2
    return v0
.end method

.method getRoam()I
    .registers 2

    #@0
    .prologue
    .line 128
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mRoam:I

    #@2
    return v0
.end method

.method getService()I
    .registers 2

    #@0
    .prologue
    .line 96
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mService:I

    #@2
    return v0
.end method

.method getSignal()I
    .registers 2

    #@0
    .prologue
    .line 124
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mSignal:I

    #@2
    return v0
.end method

.method getSpeakerVolume()I
    .registers 2

    #@0
    .prologue
    .line 151
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mSpeakerVolume:I

    #@2
    return v0
.end method

.method isInCall()Z
    .registers 3

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 163
    iget v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mNumActive:I

    #@3
    if-lt v1, v0, :cond_6

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method listenForPhoneState(Z)V
    .registers 5
    .parameter "start"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 80
    if-eqz p1, :cond_14

    #@3
    .line 81
    iget-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mListening:Z

    #@5
    if-nez v0, :cond_13

    #@7
    .line 82
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@9
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@b
    const/16 v2, 0x101

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@10
    .line 85
    const/4 v0, 0x1

    #@11
    iput-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mListening:Z

    #@13
    .line 93
    :cond_13
    :goto_13
    return-void

    #@14
    .line 88
    :cond_14
    iget-boolean v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mListening:Z

    #@16
    if-eqz v0, :cond_13

    #@18
    .line 89
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mTelephonyManager:Landroid/telephony/TelephonyManager;

    #@1a
    iget-object v1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mPhoneStateListener:Landroid/telephony/PhoneStateListener;

    #@1c
    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    #@1f
    .line 90
    iput-boolean v2, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mListening:Z

    #@21
    goto :goto_13
.end method

.method sendDeviceStateChanged()V
    .registers 8

    #@0
    .prologue
    .line 168
    const-string v1, "HeadsetPhoneState"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "sendDeviceStateChanged. mService="

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    iget v3, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mService:I

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, " mSignal="

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    iget v3, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mSignal:I

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, " mRoam="

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    iget v3, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mRoam:I

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v2

    #@2b
    const-string v3, " mBatteryCharge="

    #@2d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v2

    #@31
    iget v3, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mBatteryCharge:I

    #@33
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v2

    #@3b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    .line 171
    iget-object v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mStateMachine:Lcom/android/bluetooth/hfp/HeadsetStateMachine;

    #@40
    .line 172
    .local v0, sm:Lcom/android/bluetooth/hfp/HeadsetStateMachine;
    if-eqz v0, :cond_54

    #@42
    .line 173
    const/16 v1, 0xb

    #@44
    new-instance v2, Lcom/android/bluetooth/hfp/HeadsetDeviceState;

    #@46
    iget v3, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mService:I

    #@48
    iget v4, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mRoam:I

    #@4a
    iget v5, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mSignal:I

    #@4c
    iget v6, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mBatteryCharge:I

    #@4e
    invoke-direct {v2, v3, v4, v5, v6}, Lcom/android/bluetooth/hfp/HeadsetDeviceState;-><init>(IIII)V

    #@51
    invoke-virtual {v0, v1, v2}, Lcom/android/bluetooth/hfp/HeadsetStateMachine;->sendMessage(ILjava/lang/Object;)V

    #@54
    .line 176
    :cond_54
    return-void
.end method

.method setBatteryCharge(I)V
    .registers 3
    .parameter "batteryLevel"

    #@0
    .prologue
    .line 136
    iget v0, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mBatteryCharge:I

    #@2
    if-eq v0, p1, :cond_9

    #@4
    .line 137
    iput p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mBatteryCharge:I

    #@6
    .line 138
    invoke-virtual {p0}, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->sendDeviceStateChanged()V

    #@9
    .line 140
    :cond_9
    return-void
.end method

.method setCallState(I)V
    .registers 2
    .parameter "callState"

    #@0
    .prologue
    .line 112
    iput p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mCallState:I

    #@2
    .line 113
    return-void
.end method

.method setMicVolume(I)V
    .registers 2
    .parameter "volume"

    #@0
    .prologue
    .line 155
    iput p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mMicVolume:I

    #@2
    .line 156
    return-void
.end method

.method setNumActiveCall(I)V
    .registers 2
    .parameter "numActive"

    #@0
    .prologue
    .line 104
    iput p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mNumActive:I

    #@2
    .line 105
    return-void
.end method

.method setNumHeldCall(I)V
    .registers 2
    .parameter "numHeldCall"

    #@0
    .prologue
    .line 120
    iput p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mNumHeld:I

    #@2
    .line 121
    return-void
.end method

.method setRoam(I)V
    .registers 2
    .parameter "roam"

    #@0
    .prologue
    .line 132
    iput p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mRoam:I

    #@2
    .line 133
    return-void
.end method

.method setSpeakerVolume(I)V
    .registers 2
    .parameter "volume"

    #@0
    .prologue
    .line 147
    iput p1, p0, Lcom/android/bluetooth/hfp/HeadsetPhoneState;->mSpeakerVolume:I

    #@2
    .line 148
    return-void
.end method
