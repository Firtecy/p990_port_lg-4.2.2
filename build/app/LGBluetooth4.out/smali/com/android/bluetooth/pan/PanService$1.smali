.class Lcom/android/bluetooth/pan/PanService$1;
.super Landroid/os/Handler;
.source "PanService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/pan/PanService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/pan/PanService;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/pan/PanService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 130
    iput-object p1, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    .line 133
    iget v0, p1, Landroid/os/Message;->what:I

    #@2
    sparse-switch v0, :sswitch_data_a0

    #@5
    .line 174
    :cond_5
    :goto_5
    return-void

    #@6
    .line 136
    :sswitch_6
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@a
    .line 137
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@c
    invoke-static {v1}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@f
    move-result-object v2

    #@10
    const/4 v3, 0x2

    #@11
    const/4 v4, 0x1

    #@12
    invoke-static {v0, v2, v3, v4}, Lcom/android/bluetooth/pan/PanService;->access$000(Lcom/android/bluetooth/pan/PanService;[BII)Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_5

    #@18
    .line 138
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@1a
    const/4 v2, 0x0

    #@1b
    const/4 v3, 0x1

    #@1c
    const/4 v4, 0x2

    #@1d
    const/4 v5, 0x1

    #@1e
    invoke-virtual/range {v0 .. v5}, Lcom/android/bluetooth/pan/PanService;->handlePanDeviceStateChange(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;III)V

    #@21
    .line 140
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@23
    const/4 v2, 0x0

    #@24
    const/4 v3, 0x0

    #@25
    const/4 v4, 0x2

    #@26
    const/4 v5, 0x1

    #@27
    invoke-virtual/range {v0 .. v5}, Lcom/android/bluetooth/pan/PanService;->handlePanDeviceStateChange(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;III)V

    #@2a
    goto :goto_5

    #@2b
    .line 148
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    :sswitch_2b
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2d
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@2f
    .line 149
    .restart local v1       #device:Landroid/bluetooth/BluetoothDevice;
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@31
    invoke-static {v1}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@34
    move-result-object v2

    #@35
    invoke-static {v0, v2}, Lcom/android/bluetooth/pan/PanService;->access$100(Lcom/android/bluetooth/pan/PanService;[B)Z

    #@38
    move-result v0

    #@39
    if-nez v0, :cond_5

    #@3b
    .line 150
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@3d
    iget-object v2, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@3f
    invoke-static {v2}, Lcom/android/bluetooth/pan/PanService;->access$200(Lcom/android/bluetooth/pan/PanService;)Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    const/4 v3, 0x3

    #@44
    const/4 v4, 0x2

    #@45
    const/4 v5, 0x1

    #@46
    invoke-virtual/range {v0 .. v5}, Lcom/android/bluetooth/pan/PanService;->handlePanDeviceStateChange(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;III)V

    #@49
    .line 152
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@4b
    iget-object v2, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@4d
    invoke-static {v2}, Lcom/android/bluetooth/pan/PanService;->access$200(Lcom/android/bluetooth/pan/PanService;)Ljava/lang/String;

    #@50
    move-result-object v2

    #@51
    const/4 v3, 0x0

    #@52
    const/4 v4, 0x2

    #@53
    const/4 v5, 0x1

    #@54
    invoke-virtual/range {v0 .. v5}, Lcom/android/bluetooth/pan/PanService;->handlePanDeviceStateChange(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;III)V

    #@57
    goto :goto_5

    #@58
    .line 160
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    :sswitch_58
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5a
    check-cast v6, Lcom/android/bluetooth/pan/PanService$ConnectState;

    #@5c
    .line 161
    .local v6, cs:Lcom/android/bluetooth/pan/PanService$ConnectState;
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@5e
    iget-object v2, v6, Lcom/android/bluetooth/pan/PanService$ConnectState;->addr:[B

    #@60
    invoke-static {v0, v2}, Lcom/android/bluetooth/pan/PanService;->access$300(Lcom/android/bluetooth/pan/PanService;[B)Landroid/bluetooth/BluetoothDevice;

    #@63
    move-result-object v1

    #@64
    .line 164
    .restart local v1       #device:Landroid/bluetooth/BluetoothDevice;
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@66
    new-instance v2, Ljava/lang/StringBuilder;

    #@68
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@6b
    const-string v3, "MESSAGE_CONNECT_STATE_CHANGED: "

    #@6d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@70
    move-result-object v2

    #@71
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@74
    move-result-object v2

    #@75
    const-string v3, " state: "

    #@77
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v2

    #@7b
    iget v3, v6, Lcom/android/bluetooth/pan/PanService$ConnectState;->state:I

    #@7d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@80
    move-result-object v2

    #@81
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v2

    #@85
    invoke-static {v0, v2}, Lcom/android/bluetooth/pan/PanService;->access$400(Lcom/android/bluetooth/pan/PanService;Ljava/lang/String;)V

    #@88
    .line 166
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@8a
    iget-object v2, p0, Lcom/android/bluetooth/pan/PanService$1;->this$0:Lcom/android/bluetooth/pan/PanService;

    #@8c
    invoke-static {v2}, Lcom/android/bluetooth/pan/PanService;->access$200(Lcom/android/bluetooth/pan/PanService;)Ljava/lang/String;

    #@8f
    move-result-object v2

    #@90
    iget v3, v6, Lcom/android/bluetooth/pan/PanService$ConnectState;->state:I

    #@92
    invoke-static {v3}, Lcom/android/bluetooth/pan/PanService;->access$500(I)I

    #@95
    move-result v3

    #@96
    iget v4, v6, Lcom/android/bluetooth/pan/PanService$ConnectState;->local_role:I

    #@98
    iget v5, v6, Lcom/android/bluetooth/pan/PanService$ConnectState;->remote_role:I

    #@9a
    invoke-virtual/range {v0 .. v5}, Lcom/android/bluetooth/pan/PanService;->handlePanDeviceStateChange(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;III)V

    #@9d
    goto/16 :goto_5

    #@9f
    .line 133
    nop

    #@a0
    :sswitch_data_a0
    .sparse-switch
        0x1 -> :sswitch_6
        0x2 -> :sswitch_2b
        0xb -> :sswitch_58
    .end sparse-switch
.end method
