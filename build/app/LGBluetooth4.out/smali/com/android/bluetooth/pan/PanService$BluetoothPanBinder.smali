.class Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;
.super Landroid/bluetooth/IBluetoothPan$Stub;
.source "PanService.java"

# interfaces
.implements Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/pan/PanService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BluetoothPanBinder"
.end annotation


# instance fields
.field private mService:Lcom/android/bluetooth/pan/PanService;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/pan/PanService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 182
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothPan$Stub;-><init>()V

    #@3
    .line 183
    iput-object p1, p0, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->mService:Lcom/android/bluetooth/pan/PanService;

    #@5
    .line 184
    return-void
.end method

.method private getService()Lcom/android/bluetooth/pan/PanService;
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 190
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_f

    #@7
    .line 191
    const-string v1, "PanService"

    #@9
    const-string v2, "Pan call not allowed for non-active user"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 198
    :cond_e
    :goto_e
    return-object v0

    #@f
    .line 195
    :cond_f
    iget-object v1, p0, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->mService:Lcom/android/bluetooth/pan/PanService;

    #@11
    if-eqz v1, :cond_e

    #@13
    iget-object v1, p0, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->mService:Lcom/android/bluetooth/pan/PanService;

    #@15
    invoke-static {v1}, Lcom/android/bluetooth/pan/PanService;->access$600(Lcom/android/bluetooth/pan/PanService;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_e

    #@1b
    .line 196
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->mService:Lcom/android/bluetooth/pan/PanService;

    #@1d
    goto :goto_e
.end method

.method private isPanNapOn()Z
    .registers 3

    #@0
    .prologue
    .line 222
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->getService()Lcom/android/bluetooth/pan/PanService;

    #@3
    move-result-object v0

    #@4
    .line 223
    .local v0, service:Lcom/android/bluetooth/pan/PanService;
    if-nez v0, :cond_8

    #@6
    .line 224
    const/4 v1, 0x0

    #@7
    .line 226
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/pan/PanService;->isPanNapOn()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method private isPanUOn()Z
    .registers 4

    #@0
    .prologue
    .line 230
    const-string v1, "PanService"

    #@2
    const-string v2, "isTetheringOn call getPanLocalRoleNative"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 232
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->getService()Lcom/android/bluetooth/pan/PanService;

    #@a
    move-result-object v0

    #@b
    .line 233
    .local v0, service:Lcom/android/bluetooth/pan/PanService;
    invoke-virtual {v0}, Lcom/android/bluetooth/pan/PanService;->isPanUOn()Z

    #@e
    move-result v1

    #@f
    return v1
.end method


# virtual methods
.method public cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 186
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->mService:Lcom/android/bluetooth/pan/PanService;

    #@3
    .line 187
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 201
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->getService()Lcom/android/bluetooth/pan/PanService;

    #@3
    move-result-object v0

    #@4
    .line 202
    .local v0, service:Lcom/android/bluetooth/pan/PanService;
    if-nez v0, :cond_8

    #@6
    .line 203
    const/4 v1, 0x0

    #@7
    .line 205
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/pan/PanService;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 208
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->getService()Lcom/android/bluetooth/pan/PanService;

    #@3
    move-result-object v0

    #@4
    .line 209
    .local v0, service:Lcom/android/bluetooth/pan/PanService;
    if-nez v0, :cond_8

    #@6
    .line 210
    const/4 v1, 0x0

    #@7
    .line 212
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/pan/PanService;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 253
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->getService()Lcom/android/bluetooth/pan/PanService;

    #@3
    move-result-object v0

    #@4
    .line 254
    .local v0, service:Lcom/android/bluetooth/pan/PanService;
    if-nez v0, :cond_d

    #@6
    .line 255
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 257
    :goto_c
    return-object v1

    #@d
    :cond_d
    invoke-virtual {v0}, Lcom/android/bluetooth/pan/PanService;->getConnectedDevices()Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 215
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->getService()Lcom/android/bluetooth/pan/PanService;

    #@3
    move-result-object v0

    #@4
    .line 216
    .local v0, service:Lcom/android/bluetooth/pan/PanService;
    if-nez v0, :cond_8

    #@6
    .line 217
    const/4 v1, 0x0

    #@7
    .line 219
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/pan/PanService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 5
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 261
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->getService()Lcom/android/bluetooth/pan/PanService;

    #@3
    move-result-object v0

    #@4
    .line 262
    .local v0, service:Lcom/android/bluetooth/pan/PanService;
    if-nez v0, :cond_d

    #@6
    .line 263
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 265
    :goto_c
    return-object v1

    #@d
    :cond_d
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/pan/PanService;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method

.method public isTetheringOn()Z
    .registers 3

    #@0
    .prologue
    .line 237
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->getService()Lcom/android/bluetooth/pan/PanService;

    #@3
    move-result-object v0

    #@4
    .line 238
    .local v0, service:Lcom/android/bluetooth/pan/PanService;
    if-nez v0, :cond_8

    #@6
    .line 239
    const/4 v1, 0x0

    #@7
    .line 241
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/pan/PanService;->isTetheringOn()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public setBluetoothTethering(Z)V
    .registers 6
    .parameter "value"

    #@0
    .prologue
    .line 244
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;->getService()Lcom/android/bluetooth/pan/PanService;

    #@3
    move-result-object v0

    #@4
    .line 245
    .local v0, service:Lcom/android/bluetooth/pan/PanService;
    if-nez v0, :cond_7

    #@6
    .line 250
    :goto_6
    return-void

    #@7
    .line 248
    :cond_7
    const-string v1, "PanService"

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "setBluetoothTethering: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    const-string v3, ", mTetherOn: "

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-static {v0}, Lcom/android/bluetooth/pan/PanService;->access$700(Lcom/android/bluetooth/pan/PanService;)Z

    #@21
    move-result v3

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 249
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/pan/PanService;->setBluetoothTethering(Z)V

    #@30
    goto :goto_6
.end method
