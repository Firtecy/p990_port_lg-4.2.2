.class public Lcom/android/bluetooth/pan/PanService;
.super Lcom/android/bluetooth/btservice/ProfileService;
.source "PanService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;,
        Lcom/android/bluetooth/pan/PanService$ConnectState;,
        Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;
    }
.end annotation


# static fields
.field private static final BLUETOOTH_IFACE_ADDR_START:Ljava/lang/String; = "192.168.44.1"

.field private static final BLUETOOTH_MAX_PAN_CONNECTIONS:I = 0x5

.field private static final BLUETOOTH_PREFIX_LENGTH:I = 0x18

.field private static final CONN_STATE_CONNECTED:I = 0x0

.field private static final CONN_STATE_CONNECTING:I = 0x1

.field private static final CONN_STATE_DISCONNECTED:I = 0x2

.field private static final CONN_STATE_DISCONNECTING:I = 0x3

.field private static final DBG:Z = true

.field private static final MESSAGE_CONNECT:I = 0x1

.field private static final MESSAGE_CONNECT_STATE_CHANGED:I = 0xb

.field private static final MESSAGE_DISCONNECT:I = 0x2

.field private static final TAG:Ljava/lang/String; = "PanService"


# instance fields
.field private mBluetoothIfaceAddresses:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mMaxPanDevices:I

.field private mNativeAvailable:Z

.field private mPanDevices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            "Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mPanIfName:Ljava/lang/String;

.field private mTetherOn:Z


# direct methods
.method static constructor <clinit>()V
    .registers 0

    #@0
    .prologue
    .line 81
    invoke-static {}, Lcom/android/bluetooth/pan/PanService;->classInitNative()V

    #@3
    .line 82
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 60
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/ProfileService;-><init>()V

    #@3
    .line 77
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/android/bluetooth/pan/PanService;->mTetherOn:Z

    #@6
    .line 130
    new-instance v0, Lcom/android/bluetooth/pan/PanService$1;

    #@8
    invoke-direct {v0, p0}, Lcom/android/bluetooth/pan/PanService$1;-><init>(Lcom/android/bluetooth/pan/PanService;)V

    #@b
    iput-object v0, p0, Lcom/android/bluetooth/pan/PanService;->mHandler:Landroid/os/Handler;

    #@d
    .line 627
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/pan/PanService;[BII)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/pan/PanService;->connectPanNative([BII)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Lcom/android/bluetooth/pan/PanService;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/android/bluetooth/pan/PanService;->disconnectPanNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/bluetooth/pan/PanService;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService;->mPanIfName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/bluetooth/pan/PanService;[B)Landroid/bluetooth/BluetoothDevice;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/pan/PanService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/bluetooth/pan/PanService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/pan/PanService;->log(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$500(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-static {p0}, Lcom/android/bluetooth/pan/PanService;->convertHalState(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$600(Lcom/android/bluetooth/pan/PanService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/android/bluetooth/pan/PanService;->isAvailable()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/android/bluetooth/pan/PanService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/android/bluetooth/pan/PanService;->mTetherOn:Z

    #@2
    return v0
.end method

.method private static native classInitNative()V
.end method

.method private native cleanupNative()V
.end method

.method private native connectPanNative([BII)Z
.end method

.method private static convertHalState(I)I
    .registers 5
    .parameter "halState"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 400
    packed-switch p0, :pswitch_data_24

    #@4
    .line 410
    const-string v1, "PanService"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "bad pan connection state: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 411
    :goto_1c
    :pswitch_1c
    return v0

    #@1d
    .line 402
    :pswitch_1d
    const/4 v0, 0x2

    #@1e
    goto :goto_1c

    #@1f
    .line 404
    :pswitch_1f
    const/4 v0, 0x1

    #@20
    goto :goto_1c

    #@21
    .line 408
    :pswitch_21
    const/4 v0, 0x3

    #@22
    goto :goto_1c

    #@23
    .line 400
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1f
        :pswitch_1c
        :pswitch_21
    .end packed-switch
.end method

.method private createNewTetheringAddressLocked()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x2

    #@1
    .line 588
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService;->getConnectedPanDevices()Ljava/util/List;

    #@4
    move-result-object v3

    #@5
    invoke-interface {v3}, Ljava/util/List;->size()I

    #@8
    move-result v3

    #@9
    iget v4, p0, Lcom/android/bluetooth/pan/PanService;->mMaxPanDevices:I

    #@b
    if-ne v3, v4, :cond_16

    #@d
    .line 590
    const-string v3, "PanService"

    #@f
    const-string v4, "Max PAN device connections reached"

    #@11
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@14
    .line 592
    const/4 v1, 0x0

    #@15
    .line 605
    :goto_15
    return-object v1

    #@16
    .line 594
    :cond_16
    const-string v1, "192.168.44.1"

    #@18
    .line 596
    .local v1, address:Ljava/lang/String;
    :goto_18
    iget-object v3, p0, Lcom/android/bluetooth/pan/PanService;->mBluetoothIfaceAddresses:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_3d

    #@20
    .line 597
    const-string v3, "\\."

    #@22
    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@25
    move-result-object v0

    #@26
    .line 598
    .local v0, addr:[Ljava/lang/String;
    aget-object v3, v0, v5

    #@28
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    #@2b
    move-result v3

    #@2c
    add-int/lit8 v3, v3, 0x1

    #@2e
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v2

    #@32
    .line 599
    .local v2, newIp:Ljava/lang/Integer;
    aget-object v3, v0, v5

    #@34
    invoke-virtual {v2}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    .line 600
    goto :goto_18

    #@3d
    .line 604
    .end local v0           #addr:[Ljava/lang/String;
    .end local v2           #newIp:Ljava/lang/Integer;
    :cond_3d
    iget-object v3, p0, Lcom/android/bluetooth/pan/PanService;->mBluetoothIfaceAddresses:Ljava/util/ArrayList;

    #@3f
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@42
    goto :goto_15
.end method

.method private native disconnectPanNative([B)Z
.end method

.method private native enablePanNative(I)Z
.end method

.method private enableTethering(Ljava/lang/String;)Ljava/lang/String;
    .registers 22
    .parameter "iface"

    #@0
    .prologue
    .line 525
    const-string v17, "PanService"

    #@2
    new-instance v18, Ljava/lang/StringBuilder;

    #@4
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v19, "updateTetherState:"

    #@9
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v18

    #@d
    move-object/from16 v0, v18

    #@f
    move-object/from16 v1, p1

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v18

    #@15
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v18

    #@19
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 528
    const-string v17, "network_management"

    #@1e
    invoke-static/range {v17 .. v17}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@21
    move-result-object v5

    #@22
    .line 529
    .local v5, b:Landroid/os/IBinder;
    invoke-static {v5}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@25
    move-result-object v16

    #@26
    .line 530
    .local v16, service:Landroid/os/INetworkManagementService;
    const-string v17, "connectivity"

    #@28
    move-object/from16 v0, p0

    #@2a
    move-object/from16 v1, v17

    #@2c
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/pan/PanService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@2f
    move-result-object v7

    #@30
    check-cast v7, Landroid/net/ConnectivityManager;

    #@32
    .line 532
    .local v7, cm:Landroid/net/ConnectivityManager;
    invoke-virtual {v7}, Landroid/net/ConnectivityManager;->getTetherableBluetoothRegexs()[Ljava/lang/String;

    #@35
    move-result-object v6

    #@36
    .line 535
    .local v6, bluetoothRegexs:[Ljava/lang/String;
    const/16 v17, 0x0

    #@38
    move/from16 v0, v17

    #@3a
    new-array v9, v0, [Ljava/lang/String;

    #@3c
    .line 537
    .local v9, currentIfaces:[Ljava/lang/String;
    :try_start_3c
    invoke-interface/range {v16 .. v16}, Landroid/os/INetworkManagementService;->listInterfaces()[Ljava/lang/String;
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_3c .. :try_end_3f} :catch_55

    #@3f
    move-result-object v9

    #@40
    .line 543
    const/4 v11, 0x0

    #@41
    .line 544
    .local v11, found:Z
    move-object v4, v9

    #@42
    .local v4, arr$:[Ljava/lang/String;
    array-length v14, v4

    #@43
    .local v14, len$:I
    const/4 v12, 0x0

    #@44
    .local v12, i$:I
    :goto_44
    if-ge v12, v14, :cond_51

    #@46
    aget-object v8, v4, v12

    #@48
    .line 545
    .local v8, currIface:Ljava/lang/String;
    move-object/from16 v0, p1

    #@4a
    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v17

    #@4e
    if-eqz v17, :cond_72

    #@50
    .line 546
    const/4 v11, 0x1

    #@51
    .line 551
    .end local v8           #currIface:Ljava/lang/String;
    :cond_51
    if-nez v11, :cond_75

    #@53
    .line 552
    const/4 v3, 0x0

    #@54
    .line 584
    .end local v4           #arr$:[Ljava/lang/String;
    .end local v11           #found:Z
    .end local v12           #i$:I
    .end local v14           #len$:I
    :cond_54
    :goto_54
    return-object v3

    #@55
    .line 538
    :catch_55
    move-exception v10

    #@56
    .line 539
    .local v10, e:Ljava/lang/Exception;
    const-string v17, "PanService"

    #@58
    new-instance v18, Ljava/lang/StringBuilder;

    #@5a
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@5d
    const-string v19, "Error listing Interfaces :"

    #@5f
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v18

    #@63
    move-object/from16 v0, v18

    #@65
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v18

    #@69
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v18

    #@6d
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@70
    .line 540
    const/4 v3, 0x0

    #@71
    goto :goto_54

    #@72
    .line 544
    .end local v10           #e:Ljava/lang/Exception;
    .restart local v4       #arr$:[Ljava/lang/String;
    .restart local v8       #currIface:Ljava/lang/String;
    .restart local v11       #found:Z
    .restart local v12       #i$:I
    .restart local v14       #len$:I
    :cond_72
    add-int/lit8 v12, v12, 0x1

    #@74
    goto :goto_44

    #@75
    .line 555
    .end local v8           #currIface:Ljava/lang/String;
    :cond_75
    invoke-direct/range {p0 .. p0}, Lcom/android/bluetooth/pan/PanService;->createNewTetheringAddressLocked()Ljava/lang/String;

    #@78
    move-result-object v3

    #@79
    .line 556
    .local v3, address:Ljava/lang/String;
    if-nez v3, :cond_7d

    #@7b
    .line 557
    const/4 v3, 0x0

    #@7c
    goto :goto_54

    #@7d
    .line 560
    :cond_7d
    const/4 v13, 0x0

    #@7e
    .line 562
    .local v13, ifcg:Landroid/net/InterfaceConfiguration;
    :try_start_7e
    move-object/from16 v0, v16

    #@80
    move-object/from16 v1, p1

    #@82
    invoke-interface {v0, v1}, Landroid/os/INetworkManagementService;->getInterfaceConfig(Ljava/lang/String;)Landroid/net/InterfaceConfiguration;

    #@85
    move-result-object v13

    #@86
    .line 563
    if-eqz v13, :cond_54

    #@88
    .line 564
    const/4 v2, 0x0

    #@89
    .line 565
    .local v2, addr:Ljava/net/InetAddress;
    invoke-virtual {v13}, Landroid/net/InterfaceConfiguration;->getLinkAddress()Landroid/net/LinkAddress;

    #@8c
    move-result-object v15

    #@8d
    .line 566
    .local v15, linkAddr:Landroid/net/LinkAddress;
    if-eqz v15, :cond_b1

    #@8f
    invoke-virtual {v15}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    #@92
    move-result-object v2

    #@93
    if-eqz v2, :cond_b1

    #@95
    const-string v17, "0.0.0.0"

    #@97
    invoke-static/range {v17 .. v17}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@9a
    move-result-object v17

    #@9b
    move-object/from16 v0, v17

    #@9d
    invoke-virtual {v2, v0}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@a0
    move-result v17

    #@a1
    if-nez v17, :cond_b1

    #@a3
    const-string v17, "::0"

    #@a5
    invoke-static/range {v17 .. v17}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@a8
    move-result-object v17

    #@a9
    move-object/from16 v0, v17

    #@ab
    invoke-virtual {v2, v0}, Ljava/net/InetAddress;->equals(Ljava/lang/Object;)Z

    #@ae
    move-result v17

    #@af
    if-eqz v17, :cond_b5

    #@b1
    .line 569
    :cond_b1
    invoke-static {v3}, Landroid/net/NetworkUtils;->numericToInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    #@b4
    move-result-object v2

    #@b5
    .line 571
    :cond_b5
    invoke-virtual {v13}, Landroid/net/InterfaceConfiguration;->setInterfaceUp()V

    #@b8
    .line 572
    new-instance v17, Landroid/net/LinkAddress;

    #@ba
    const/16 v18, 0x18

    #@bc
    move-object/from16 v0, v17

    #@be
    move/from16 v1, v18

    #@c0
    invoke-direct {v0, v2, v1}, Landroid/net/LinkAddress;-><init>(Ljava/net/InetAddress;I)V

    #@c3
    move-object/from16 v0, v17

    #@c5
    invoke-virtual {v13, v0}, Landroid/net/InterfaceConfiguration;->setLinkAddress(Landroid/net/LinkAddress;)V

    #@c8
    .line 573
    const-string v17, "running"

    #@ca
    move-object/from16 v0, v17

    #@cc
    invoke-virtual {v13, v0}, Landroid/net/InterfaceConfiguration;->clearFlag(Ljava/lang/String;)V

    #@cf
    .line 575
    move-object/from16 v0, v16

    #@d1
    move-object/from16 v1, p1

    #@d3
    invoke-interface {v0, v1, v13}, Landroid/os/INetworkManagementService;->setInterfaceConfig(Ljava/lang/String;Landroid/net/InterfaceConfiguration;)V

    #@d6
    .line 576
    move-object/from16 v0, p1

    #@d8
    invoke-virtual {v7, v0}, Landroid/net/ConnectivityManager;->tether(Ljava/lang/String;)I

    #@db
    move-result v17

    #@dc
    if-eqz v17, :cond_54

    #@de
    .line 577
    const-string v17, "PanService"

    #@e0
    new-instance v18, Ljava/lang/StringBuilder;

    #@e2
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@e5
    const-string v19, "Error tethering "

    #@e7
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v18

    #@eb
    move-object/from16 v0, v18

    #@ed
    move-object/from16 v1, p1

    #@ef
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v18

    #@f3
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f6
    move-result-object v18

    #@f7
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_fa
    .catch Ljava/lang/Exception; {:try_start_7e .. :try_end_fa} :catch_fc

    #@fa
    goto/16 :goto_54

    #@fc
    .line 580
    .end local v2           #addr:Ljava/net/InetAddress;
    .end local v15           #linkAddr:Landroid/net/LinkAddress;
    :catch_fc
    move-exception v10

    #@fd
    .line 581
    .restart local v10       #e:Ljava/lang/Exception;
    const-string v17, "PanService"

    #@ff
    new-instance v18, Ljava/lang/StringBuilder;

    #@101
    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    #@104
    const-string v19, "Error configuring interface "

    #@106
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v18

    #@10a
    move-object/from16 v0, v18

    #@10c
    move-object/from16 v1, p1

    #@10e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@111
    move-result-object v18

    #@112
    const-string v19, ", :"

    #@114
    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v18

    #@118
    move-object/from16 v0, v18

    #@11a
    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v18

    #@11e
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@121
    move-result-object v18

    #@122
    invoke-static/range {v17 .. v18}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@125
    .line 582
    const/4 v3, 0x0

    #@126
    goto/16 :goto_54
.end method

.method private getConnectedPanDevices()Ljava/util/List;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 609
    new-instance v1, Ljava/util/ArrayList;

    #@2
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@5
    .line 611
    .local v1, devices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    iget-object v3, p0, Lcom/android/bluetooth/pan/PanService;->mPanDevices:Ljava/util/HashMap;

    #@7
    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@a
    move-result-object v3

    #@b
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@e
    move-result-object v2

    #@f
    .local v2, i$:Ljava/util/Iterator;
    :cond_f
    :goto_f
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@12
    move-result v3

    #@13
    if-eqz v3, :cond_26

    #@15
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@1b
    .line 612
    .local v0, device:Landroid/bluetooth/BluetoothDevice;
    invoke-direct {p0, v0}, Lcom/android/bluetooth/pan/PanService;->getPanDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@1e
    move-result v3

    #@1f
    const/4 v4, 0x2

    #@20
    if-ne v3, v4, :cond_f

    #@22
    .line 613
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@25
    goto :goto_f

    #@26
    .line 616
    .end local v0           #device:Landroid/bluetooth/BluetoothDevice;
    :cond_26
    return-object v1
.end method

.method private getPanDeviceConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 620
    iget-object v1, p0, Lcom/android/bluetooth/pan/PanService;->mPanDevices:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;

    #@8
    .line 621
    .local v0, panDevice:Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;
    if-nez v0, :cond_c

    #@a
    .line 622
    const/4 v1, 0x0

    #@b
    .line 624
    :goto_b
    return v1

    #@c
    :cond_c
    invoke-static {v0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;->access$800(Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;)I

    #@f
    move-result v1

    #@10
    goto :goto_b
.end method

.method private native getPanLocalRoleNative()I
.end method

.method private native initializeNative()V
.end method

.method private onConnectStateChanged([BIIII)V
    .registers 13
    .parameter "address"
    .parameter "state"
    .parameter "error"
    .parameter "local_role"
    .parameter "remote_role"

    #@0
    .prologue
    .line 384
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "onConnectStateChanged: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, ", local role:"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, ", remote_role: "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/pan/PanService;->log(Ljava/lang/String;)V

    #@2a
    .line 386
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService;->mHandler:Landroid/os/Handler;

    #@2c
    const/16 v1, 0xb

    #@2e
    invoke-virtual {v0, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@31
    move-result-object v6

    #@32
    .line 387
    .local v6, msg:Landroid/os/Message;
    new-instance v0, Lcom/android/bluetooth/pan/PanService$ConnectState;

    #@34
    move-object v1, p1

    #@35
    move v2, p2

    #@36
    move v3, p3

    #@37
    move v4, p4

    #@38
    move v5, p5

    #@39
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/pan/PanService$ConnectState;-><init>([BIIII)V

    #@3c
    iput-object v0, v6, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3e
    .line 388
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService;->mHandler:Landroid/os/Handler;

    #@40
    invoke-virtual {v0, v6}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@43
    .line 389
    return-void
.end method

.method private onControlStateChanged(IIILjava/lang/String;)V
    .registers 7
    .parameter "local_role"
    .parameter "state"
    .parameter "error"
    .parameter "ifname"

    #@0
    .prologue
    .line 392
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "onControlStateChanged: "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e
    move-result-object v0

    #@f
    const-string v1, ", error: "

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v0

    #@15
    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    const-string v1, ", ifname: "

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/pan/PanService;->log(Ljava/lang/String;)V

    #@2a
    .line 394
    if-nez p3, :cond_2e

    #@2c
    .line 395
    iput-object p4, p0, Lcom/android/bluetooth/pan/PanService;->mPanIfName:Ljava/lang/String;

    #@2e
    .line 397
    :cond_2e
    return-void
.end method


# virtual methods
.method protected cleanup()Z
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 112
    iget-boolean v0, p0, Lcom/android/bluetooth/pan/PanService;->mNativeAvailable:Z

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 113
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService;->cleanupNative()V

    #@9
    .line 114
    iput-boolean v3, p0, Lcom/android/bluetooth/pan/PanService;->mNativeAvailable:Z

    #@b
    .line 116
    :cond_b
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService;->mPanDevices:Ljava/util/HashMap;

    #@d
    if-eqz v0, :cond_30

    #@f
    .line 117
    invoke-virtual {p0}, Lcom/android/bluetooth/pan/PanService;->getConnectedDevices()Ljava/util/List;

    #@12
    move-result-object v6

    #@13
    .line 118
    .local v6, DevList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@16
    move-result-object v7

    #@17
    .local v7, i$:Ljava/util/Iterator;
    :goto_17
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_2b

    #@1d
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@20
    move-result-object v1

    #@21
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@23
    .line 119
    .local v1, dev:Landroid/bluetooth/BluetoothDevice;
    iget-object v2, p0, Lcom/android/bluetooth/pan/PanService;->mPanIfName:Ljava/lang/String;

    #@25
    const/4 v4, 0x2

    #@26
    move-object v0, p0

    #@27
    invoke-virtual/range {v0 .. v5}, Lcom/android/bluetooth/pan/PanService;->handlePanDeviceStateChange(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;III)V

    #@2a
    goto :goto_17

    #@2b
    .line 122
    .end local v1           #dev:Landroid/bluetooth/BluetoothDevice;
    :cond_2b
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService;->mPanDevices:Ljava/util/HashMap;

    #@2d
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@30
    .line 124
    .end local v6           #DevList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    .end local v7           #i$:Ljava/util/Iterator;
    :cond_30
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService;->mBluetoothIfaceAddresses:Ljava/util/ArrayList;

    #@32
    if-eqz v0, :cond_39

    #@34
    .line 125
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService;->mBluetoothIfaceAddresses:Ljava/util/ArrayList;

    #@36
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@39
    .line 127
    :cond_39
    return v5
.end method

.method connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 270
    const-string v2, "android.permission.BLUETOOTH"

    #@3
    const-string v3, "Need BLUETOOTH permission"

    #@5
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/pan/PanService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 271
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/pan/PanService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_28

    #@e
    .line 272
    const-string v1, "PanService"

    #@10
    new-instance v2, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v3, "Pan Device not disconnected: "

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 273
    const/4 v1, 0x0

    #@27
    .line 277
    :goto_27
    return v1

    #@28
    .line 275
    :cond_28
    iget-object v2, p0, Lcom/android/bluetooth/pan/PanService;->mHandler:Landroid/os/Handler;

    #@2a
    invoke-virtual {v2, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@2d
    move-result-object v0

    #@2e
    .line 276
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/bluetooth/pan/PanService;->mHandler:Landroid/os/Handler;

    #@30
    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@33
    goto :goto_27
.end method

.method disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 281
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/pan/PanService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 282
    iget-object v1, p0, Lcom/android/bluetooth/pan/PanService;->mHandler:Landroid/os/Handler;

    #@9
    const/4 v2, 0x2

    #@a
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 283
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/bluetooth/pan/PanService;->mHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 284
    const/4 v1, 0x1

    #@14
    return v1
.end method

.method getConnectedDevices()Ljava/util/List;
    .registers 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 346
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/pan/PanService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 347
    const/4 v1, 0x1

    #@8
    new-array v1, v1, [I

    #@a
    const/4 v2, 0x0

    #@b
    const/4 v3, 0x2

    #@c
    aput v3, v1, v2

    #@e
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/pan/PanService;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@11
    move-result-object v0

    #@12
    .line 349
    .local v0, devices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    return-object v0
.end method

.method getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 288
    iget-object v1, p0, Lcom/android/bluetooth/pan/PanService;->mPanDevices:Ljava/util/HashMap;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    check-cast v0, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;

    #@8
    .line 289
    .local v0, panDevice:Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;
    if-nez v0, :cond_c

    #@a
    .line 290
    const/4 v1, 0x0

    #@b
    .line 292
    :goto_b
    return v1

    #@c
    :cond_c
    invoke-static {v0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;->access$800(Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;)I

    #@f
    move-result v1

    #@10
    goto :goto_b
.end method

.method getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 12
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 353
    const-string v8, "android.permission.BLUETOOTH"

    #@2
    const-string v9, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v8, v9}, Lcom/android/bluetooth/pan/PanService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 354
    new-instance v6, Ljava/util/ArrayList;

    #@9
    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    #@c
    .line 356
    .local v6, panDevices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    iget-object v8, p0, Lcom/android/bluetooth/pan/PanService;->mPanDevices:Ljava/util/HashMap;

    #@e
    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@11
    move-result-object v8

    #@12
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v2

    #@16
    :cond_16
    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v8

    #@1a
    if-eqz v8, :cond_36

    #@1c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@22
    .line 357
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/pan/PanService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@25
    move-result v5

    #@26
    .line 358
    .local v5, panDeviceState:I
    move-object v0, p1

    #@27
    .local v0, arr$:[I
    array-length v4, v0

    #@28
    .local v4, len$:I
    const/4 v3, 0x0

    #@29
    .local v3, i$:I
    :goto_29
    if-ge v3, v4, :cond_16

    #@2b
    aget v7, v0, v3

    #@2d
    .line 359
    .local v7, state:I
    if-ne v7, v5, :cond_33

    #@2f
    .line 360
    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@32
    goto :goto_16

    #@33
    .line 358
    :cond_33
    add-int/lit8 v3, v3, 0x1

    #@35
    goto :goto_29

    #@36
    .line 365
    .end local v0           #arr$:[I
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v3           #i$:I
    .end local v4           #len$:I
    .end local v5           #panDeviceState:I
    .end local v7           #state:I
    :cond_36
    return-object v6
.end method

.method protected getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 85
    const-string v0, "PanService"

    #@2
    return-object v0
.end method

.method handlePanDeviceStateChange(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;III)V
    .registers 18
    .parameter "device"
    .parameter "iface"
    .parameter "state"
    .parameter "local_role"
    .parameter "remote_role"

    #@0
    .prologue
    .line 418
    const-string v2, "PanService"

    #@2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v5, "handlePanDeviceStateChange: device: "

    #@9
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v3

    #@11
    const-string v5, ", iface: "

    #@13
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    const-string v5, ", state: "

    #@1d
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    const-string v5, ", local_role:"

    #@27
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    move/from16 v0, p4

    #@2d
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v3

    #@31
    const-string v5, ", remote_role:"

    #@33
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v3

    #@37
    move/from16 v0, p5

    #@39
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v3

    #@3d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@44
    .line 422
    const/4 v4, 0x0

    #@45
    .line 423
    .local v4, ifaceAddr:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/bluetooth/pan/PanService;->mPanDevices:Ljava/util/HashMap;

    #@47
    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@4a
    move-result-object v1

    #@4b
    check-cast v1, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;

    #@4d
    .line 424
    .local v1, panDevice:Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;
    if-nez v1, :cond_75

    #@4f
    .line 425
    const/4 v10, 0x0

    #@50
    .line 430
    .local v10, prevState:I
    :goto_50
    const-string v2, "PanService"

    #@52
    new-instance v3, Ljava/lang/StringBuilder;

    #@54
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@57
    const-string v5, "handlePanDeviceStateChange preState: "

    #@59
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v3

    #@5d
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    const-string v5, " state: "

    #@63
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@66
    move-result-object v3

    #@67
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v3

    #@6b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6e
    move-result-object v3

    #@6f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@72
    .line 431
    if-ne v10, p3, :cond_7e

    #@74
    .line 520
    :cond_74
    :goto_74
    return-void

    #@75
    .line 427
    .end local v10           #prevState:I
    :cond_75
    invoke-static {v1}, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;->access$800(Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;)I

    #@78
    move-result v10

    #@79
    .line 428
    .restart local v10       #prevState:I
    invoke-static {v1}, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;->access$900(Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;)Ljava/lang/String;

    #@7c
    move-result-object v4

    #@7d
    goto :goto_50

    #@7e
    .line 434
    :cond_7e
    const/4 v2, 0x2

    #@7f
    move/from16 v0, p5

    #@81
    if-ne v0, v2, :cond_136

    #@83
    .line 435
    const/4 v2, 0x2

    #@84
    if-ne p3, v2, :cond_12a

    #@86
    .line 436
    iget-boolean v2, p0, Lcom/android/bluetooth/pan/PanService;->mTetherOn:Z

    #@88
    if-eqz v2, :cond_8f

    #@8a
    const/4 v2, 0x2

    #@8b
    move/from16 v0, p4

    #@8d
    if-ne v0, v2, :cond_9e

    #@8f
    .line 437
    :cond_8f
    const-string v2, "PanService"

    #@91
    const-string v3, "handlePanDeviceStateChange BT tethering is off/Local role is PANU drop the connection"

    #@93
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@96
    .line 439
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@99
    move-result-object v2

    #@9a
    invoke-direct {p0, v2}, Lcom/android/bluetooth/pan/PanService;->disconnectPanNative([B)Z

    #@9d
    goto :goto_74

    #@9e
    .line 442
    :cond_9e
    const-string v2, "PanService"

    #@a0
    const-string v3, "handlePanDeviceStateChange LOCAL_NAP_ROLE:REMOTE_PANU_ROLE"

    #@a2
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    .line 443
    invoke-direct {p0, p2}, Lcom/android/bluetooth/pan/PanService;->enableTethering(Ljava/lang/String;)Ljava/lang/String;

    #@a8
    move-result-object v4

    #@a9
    .line 444
    if-nez v4, :cond_b2

    #@ab
    .line 445
    const-string v2, "PanService"

    #@ad
    const-string v3, "Error seting up tether interface"

    #@af
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b2
    .line 488
    :cond_b2
    :goto_b2
    if-nez v1, :cond_1c6

    #@b4
    .line 489
    new-instance v1, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;

    #@b6
    .end local v1           #panDevice:Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;
    move-object v2, p0

    #@b7
    move v3, p3

    #@b8
    move-object v5, p2

    #@b9
    move/from16 v6, p4

    #@bb
    invoke-direct/range {v1 .. v6}, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;-><init>(Lcom/android/bluetooth/pan/PanService;ILjava/lang/String;Ljava/lang/String;I)V

    #@be
    .line 490
    .restart local v1       #panDevice:Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;
    iget-object v2, p0, Lcom/android/bluetooth/pan/PanService;->mPanDevices:Ljava/util/HashMap;

    #@c0
    invoke-virtual {v2, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@c3
    .line 501
    :goto_c3
    const-string v2, "PanService"

    #@c5
    new-instance v3, Ljava/lang/StringBuilder;

    #@c7
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@ca
    const-string v5, "Pan Device state : device: "

    #@cc
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cf
    move-result-object v3

    #@d0
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v3

    #@d4
    const-string v5, " State:"

    #@d6
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d9
    move-result-object v3

    #@da
    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v3

    #@de
    const-string v5, "->"

    #@e0
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v3

    #@e4
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e7
    move-result-object v3

    #@e8
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@eb
    move-result-object v3

    #@ec
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ef
    .line 503
    const/4 v2, 0x5

    #@f0
    invoke-virtual {p0, p1, v2, p3, v10}, Lcom/android/bluetooth/pan/PanService;->notifyProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V

    #@f3
    .line 504
    new-instance v9, Landroid/content/Intent;

    #@f5
    const-string v2, "android.bluetooth.pan.profile.action.CONNECTION_STATE_CHANGED"

    #@f7
    invoke-direct {v9, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@fa
    .line 505
    .local v9, intent:Landroid/content/Intent;
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    #@fc
    invoke-virtual {v9, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@ff
    .line 506
    const-string v2, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    #@101
    invoke-virtual {v9, v2, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@104
    .line 507
    const-string v2, "android.bluetooth.profile.extra.STATE"

    #@106
    invoke-virtual {v9, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@109
    .line 508
    const-string v2, "android.bluetooth.pan.extra.LOCAL_ROLE"

    #@10b
    move/from16 v0, p4

    #@10d
    invoke-virtual {v9, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@110
    .line 509
    const-string v2, "android.permission.BLUETOOTH"

    #@112
    invoke-virtual {p0, v9, v2}, Lcom/android/bluetooth/pan/PanService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@115
    .line 512
    const/4 v2, 0x2

    #@116
    if-ne p3, v2, :cond_1d6

    #@118
    .line 513
    invoke-static {}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getDefaultChecker()Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@11b
    move-result-object v2

    #@11c
    const/4 v3, 0x3

    #@11d
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@120
    move-result-object v5

    #@121
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    #@124
    move-result-object v6

    #@125
    invoke-virtual {v2, p0, v3, v5, v6}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->setConnected(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Z

    #@128
    goto/16 :goto_74

    #@12a
    .line 448
    .end local v9           #intent:Landroid/content/Intent;
    :cond_12a
    if-nez p3, :cond_b2

    #@12c
    .line 449
    if-eqz v4, :cond_b2

    #@12e
    .line 450
    iget-object v2, p0, Lcom/android/bluetooth/pan/PanService;->mBluetoothIfaceAddresses:Ljava/util/ArrayList;

    #@130
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@133
    .line 451
    const/4 v4, 0x0

    #@134
    goto/16 :goto_b2

    #@136
    .line 456
    :cond_136
    const-string v2, "PanService"

    #@138
    const-string v3, "handlePanDeviceStateChange LOCAL_PANU_ROLE:REMOTE_NAP_ROLE"

    #@13a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@13d
    .line 457
    const/4 v2, 0x2

    #@13e
    if-ne p3, v2, :cond_178

    #@140
    .line 459
    const-string v2, "PanService"

    #@142
    const-string v3, "handlePanDeviceStateChange: panu STATE_CONNECTED, startReverseTether"

    #@144
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@147
    .line 461
    const-string v2, "network_management"

    #@149
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@14c
    move-result-object v7

    #@14d
    .line 462
    .local v7, b:Landroid/os/IBinder;
    invoke-static {v7}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@150
    move-result-object v11

    #@151
    .line 463
    .local v11, service:Landroid/os/INetworkManagementService;
    const-string v2, "PanService"

    #@153
    const-string v3, "call INetworkManagementService.startReverseTethering()"

    #@155
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@158
    .line 465
    :try_start_158
    invoke-interface {v11, p2}, Landroid/os/INetworkManagementService;->startReverseTethering(Ljava/lang/String;)V
    :try_end_15b
    .catch Ljava/lang/Exception; {:try_start_158 .. :try_end_15b} :catch_15d

    #@15b
    goto/16 :goto_b2

    #@15d
    .line 466
    :catch_15d
    move-exception v8

    #@15e
    .line 467
    .local v8, e:Ljava/lang/Exception;
    const-string v2, "PanService"

    #@160
    new-instance v3, Ljava/lang/StringBuilder;

    #@162
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@165
    const-string v5, "Cannot start reverse tethering: "

    #@167
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16a
    move-result-object v3

    #@16b
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16e
    move-result-object v3

    #@16f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@172
    move-result-object v3

    #@173
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@176
    goto/16 :goto_74

    #@178
    .line 470
    .end local v7           #b:Landroid/os/IBinder;
    .end local v8           #e:Ljava/lang/Exception;
    .end local v11           #service:Landroid/os/INetworkManagementService;
    :cond_178
    if-nez p3, :cond_b2

    #@17a
    const/4 v2, 0x2

    #@17b
    if-eq v10, v2, :cond_180

    #@17d
    const/4 v2, 0x3

    #@17e
    if-ne v10, v2, :cond_b2

    #@180
    .line 474
    :cond_180
    const-string v2, "PanService"

    #@182
    new-instance v3, Ljava/lang/StringBuilder;

    #@184
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@187
    const-string v5, "handlePanDeviceStateChange: stopReverseTether, panDevice.mIface: "

    #@189
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18c
    move-result-object v3

    #@18d
    invoke-static {v1}, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;->access$1000(Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;)Ljava/lang/String;

    #@190
    move-result-object v5

    #@191
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@194
    move-result-object v3

    #@195
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@198
    move-result-object v3

    #@199
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@19c
    .line 477
    const-string v2, "network_management"

    #@19e
    invoke-static {v2}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@1a1
    move-result-object v7

    #@1a2
    .line 478
    .restart local v7       #b:Landroid/os/IBinder;
    invoke-static {v7}, Landroid/os/INetworkManagementService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/INetworkManagementService;

    #@1a5
    move-result-object v11

    #@1a6
    .line 480
    .restart local v11       #service:Landroid/os/INetworkManagementService;
    :try_start_1a6
    invoke-interface {v11}, Landroid/os/INetworkManagementService;->stopReverseTethering()V
    :try_end_1a9
    .catch Ljava/lang/Exception; {:try_start_1a6 .. :try_end_1a9} :catch_1ab

    #@1a9
    goto/16 :goto_b2

    #@1ab
    .line 481
    :catch_1ab
    move-exception v8

    #@1ac
    .line 482
    .restart local v8       #e:Ljava/lang/Exception;
    const-string v2, "PanService"

    #@1ae
    new-instance v3, Ljava/lang/StringBuilder;

    #@1b0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@1b3
    const-string v5, "Cannot stop reverse tethering: "

    #@1b5
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b8
    move-result-object v3

    #@1b9
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1bc
    move-result-object v3

    #@1bd
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c0
    move-result-object v3

    #@1c1
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c4
    goto/16 :goto_74

    #@1c6
    .line 492
    .end local v7           #b:Landroid/os/IBinder;
    .end local v8           #e:Ljava/lang/Exception;
    .end local v11           #service:Landroid/os/INetworkManagementService;
    :cond_1c6
    invoke-static {v1, p3}, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;->access$802(Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;I)I

    #@1c9
    .line 493
    invoke-static {v1, v4}, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;->access$902(Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;Ljava/lang/String;)Ljava/lang/String;

    #@1cc
    .line 494
    move/from16 v0, p4

    #@1ce
    invoke-static {v1, v0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;->access$1102(Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;I)I

    #@1d1
    .line 495
    invoke-static {v1, p2}, Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;->access$1002(Lcom/android/bluetooth/pan/PanService$BluetoothPanDevice;Ljava/lang/String;)Ljava/lang/String;

    #@1d4
    goto/16 :goto_c3

    #@1d6
    .line 515
    .restart local v9       #intent:Landroid/content/Intent;
    :cond_1d6
    if-nez p3, :cond_74

    #@1d8
    .line 516
    invoke-static {}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getDefaultChecker()Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@1db
    move-result-object v2

    #@1dc
    const/4 v3, 0x3

    #@1dd
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@1e0
    move-result-object v5

    #@1e1
    invoke-virtual {v2, p0, v3, v5}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->setDisconnected(Landroid/content/Context;ILjava/lang/String;)Z

    #@1e4
    goto/16 :goto_74
.end method

.method public initBinder()Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;
    .registers 2

    #@0
    .prologue
    .line 89
    new-instance v0, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;

    #@2
    invoke-direct {v0, p0}, Lcom/android/bluetooth/pan/PanService$BluetoothPanBinder;-><init>(Lcom/android/bluetooth/pan/PanService;)V

    #@5
    return-object v0
.end method

.method isPanNapOn()Z
    .registers 3

    #@0
    .prologue
    .line 297
    const-string v0, "PanService"

    #@2
    const-string v1, "isTetheringOn call getPanLocalRoleNative"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 299
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService;->getPanLocalRoleNative()I

    #@a
    move-result v0

    #@b
    and-int/lit8 v0, v0, 0x1

    #@d
    if-eqz v0, :cond_11

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method isPanUOn()Z
    .registers 3

    #@0
    .prologue
    .line 303
    const-string v0, "PanService"

    #@2
    const-string v1, "isTetheringOn call getPanLocalRoleNative"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 305
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService;->getPanLocalRoleNative()I

    #@a
    move-result v0

    #@b
    and-int/lit8 v0, v0, 0x2

    #@d
    if-eqz v0, :cond_11

    #@f
    const/4 v0, 0x1

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    const/4 v0, 0x0

    #@12
    goto :goto_10
.end method

.method isTetheringOn()Z
    .registers 2

    #@0
    .prologue
    .line 309
    iget-boolean v0, p0, Lcom/android/bluetooth/pan/PanService;->mTetherOn:Z

    #@2
    return v0
.end method

.method setBluetoothTethering(Z)V
    .registers 9
    .parameter "value"

    #@0
    .prologue
    .line 314
    const-string v4, "PanService"

    #@2
    new-instance v5, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v6, "setBluetoothTethering: "

    #@9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v5

    #@d
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v5

    #@11
    const-string v6, ", mTetherOn: "

    #@13
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v5

    #@17
    iget-boolean v6, p0, Lcom/android/bluetooth/pan/PanService;->mTetherOn:Z

    #@19
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v5

    #@1d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v5

    #@21
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 316
    const-string v4, "android.permission.BLUETOOTH_ADMIN"

    #@26
    const-string v5, "Need BLUETOOTH_ADMIN permission"

    #@28
    invoke-virtual {p0, v4, v5}, Lcom/android/bluetooth/pan/PanService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@2b
    .line 319
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2e
    move-result-object v4

    #@2f
    if-eqz v4, :cond_41

    #@31
    if-eqz p1, :cond_41

    #@33
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@36
    move-result-object v4

    #@37
    const/4 v5, 0x0

    #@38
    const-string v6, "LGMDMBluetoothTetheringUIAdapter"

    #@3a
    invoke-interface {v4, v5, v6}, Lcom/lge/cappuccino/IMdm;->checkDisabledSystemService(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@3d
    move-result v4

    #@3e
    if-eqz v4, :cond_41

    #@40
    .line 343
    :cond_40
    return-void

    #@41
    .line 328
    :cond_41
    move v2, p1

    #@42
    .line 329
    .local v2, filtered_value:Z
    if-eqz p1, :cond_53

    #@44
    invoke-virtual {p0}, Lcom/android/bluetooth/pan/PanService;->getContentResolver()Landroid/content/ContentResolver;

    #@47
    move-result-object v4

    #@48
    const-string v5, "tethering_blocked"

    #@4a
    const/4 v6, 0x0

    #@4b
    invoke-static {v4, v5, v6}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@4e
    move-result v4

    #@4f
    const/4 v5, 0x1

    #@50
    if-ne v4, v5, :cond_53

    #@52
    .line 332
    const/4 v2, 0x0

    #@53
    .line 335
    :cond_53
    iget-boolean v4, p0, Lcom/android/bluetooth/pan/PanService;->mTetherOn:Z

    #@55
    if-eq v4, v2, :cond_40

    #@57
    .line 337
    iput-boolean v2, p0, Lcom/android/bluetooth/pan/PanService;->mTetherOn:Z

    #@59
    .line 338
    invoke-virtual {p0}, Lcom/android/bluetooth/pan/PanService;->getConnectedDevices()Ljava/util/List;

    #@5c
    move-result-object v0

    #@5d
    .line 339
    .local v0, DevList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    #@60
    move-result-object v3

    #@61
    .local v3, i$:Ljava/util/Iterator;
    :goto_61
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@64
    move-result v4

    #@65
    if-eqz v4, :cond_40

    #@67
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@6a
    move-result-object v1

    #@6b
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@6d
    .line 340
    .local v1, dev:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/pan/PanService;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@70
    goto :goto_61
.end method

.method protected start()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 93
    new-instance v1, Ljava/util/HashMap;

    #@3
    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    #@6
    iput-object v1, p0, Lcom/android/bluetooth/pan/PanService;->mPanDevices:Ljava/util/HashMap;

    #@8
    .line 94
    new-instance v1, Ljava/util/ArrayList;

    #@a
    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    #@d
    iput-object v1, p0, Lcom/android/bluetooth/pan/PanService;->mBluetoothIfaceAddresses:Ljava/util/ArrayList;

    #@f
    .line 96
    :try_start_f
    invoke-virtual {p0}, Lcom/android/bluetooth/pan/PanService;->getResources()Landroid/content/res/Resources;

    #@12
    move-result-object v1

    #@13
    const v2, 0x10e000a

    #@16
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    #@19
    move-result v1

    #@1a
    iput v1, p0, Lcom/android/bluetooth/pan/PanService;->mMaxPanDevices:I
    :try_end_1c
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_f .. :try_end_1c} :catch_22

    #@1c
    .line 101
    :goto_1c
    invoke-direct {p0}, Lcom/android/bluetooth/pan/PanService;->initializeNative()V

    #@1f
    .line 102
    iput-boolean v3, p0, Lcom/android/bluetooth/pan/PanService;->mNativeAvailable:Z

    #@21
    .line 103
    return v3

    #@22
    .line 98
    :catch_22
    move-exception v0

    #@23
    .line 99
    .local v0, e:Landroid/content/res/Resources$NotFoundException;
    const/4 v1, 0x5

    #@24
    iput v1, p0, Lcom/android/bluetooth/pan/PanService;->mMaxPanDevices:I

    #@26
    goto :goto_1c
.end method

.method protected stop()Z
    .registers 3

    #@0
    .prologue
    .line 107
    iget-object v0, p0, Lcom/android/bluetooth/pan/PanService;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    #@6
    .line 108
    const/4 v0, 0x1

    #@7
    return v0
.end method
