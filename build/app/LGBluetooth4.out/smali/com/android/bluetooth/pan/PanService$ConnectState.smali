.class public Lcom/android/bluetooth/pan/PanService$ConnectState;
.super Ljava/lang/Object;
.source "PanService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/pan/PanService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "ConnectState"
.end annotation


# instance fields
.field addr:[B

.field error:I

.field local_role:I

.field remote_role:I

.field state:I


# direct methods
.method public constructor <init>([BIIII)V
    .registers 6
    .parameter "address"
    .parameter "state"
    .parameter "error"
    .parameter "local_role"
    .parameter "remote_role"

    #@0
    .prologue
    .line 369
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 370
    iput-object p1, p0, Lcom/android/bluetooth/pan/PanService$ConnectState;->addr:[B

    #@5
    .line 371
    iput p2, p0, Lcom/android/bluetooth/pan/PanService$ConnectState;->state:I

    #@7
    .line 372
    iput p3, p0, Lcom/android/bluetooth/pan/PanService$ConnectState;->error:I

    #@9
    .line 373
    iput p4, p0, Lcom/android/bluetooth/pan/PanService$ConnectState;->local_role:I

    #@b
    .line 374
    iput p5, p0, Lcom/android/bluetooth/pan/PanService$ConnectState;->remote_role:I

    #@d
    .line 375
    return-void
.end method
