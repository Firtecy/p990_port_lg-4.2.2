.class Lcom/android/bluetooth/hid/HidService$1;
.super Landroid/os/Handler;
.source "HidService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hid/HidService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/hid/HidService;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/hid/HidService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 154
    iput-object p1, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 16
    .parameter "msg"

    #@0
    .prologue
    .line 158
    iget v11, p1, Landroid/os/Message;->what:I

    #@2
    packed-switch v11, :pswitch_data_1f2

    #@5
    .line 303
    :cond_5
    :goto_5
    :pswitch_5
    return-void

    #@6
    .line 161
    :pswitch_6
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@8
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@a
    .line 167
    .local v2, device:Landroid/bluetooth/BluetoothDevice;
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@c
    const/4 v12, 0x1

    #@d
    invoke-static {v11, v2, v12}, Lcom/android/bluetooth/hid/HidService;->access$000(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V

    #@10
    .line 169
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@12
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@15
    move-result-object v12

    #@16
    invoke-static {v11, v12}, Lcom/android/bluetooth/hid/HidService;->access$100(Lcom/android/bluetooth/hid/HidService;[B)Z

    #@19
    move-result v11

    #@1a
    if-nez v11, :cond_29

    #@1c
    .line 170
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@1e
    const/4 v12, 0x3

    #@1f
    invoke-static {v11, v2, v12}, Lcom/android/bluetooth/hid/HidService;->access$000(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V

    #@22
    .line 171
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@24
    const/4 v12, 0x0

    #@25
    invoke-static {v11, v2, v12}, Lcom/android/bluetooth/hid/HidService;->access$000(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V

    #@28
    goto :goto_5

    #@29
    .line 174
    :cond_29
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@2b
    invoke-static {v11, v2}, Lcom/android/bluetooth/hid/HidService;->access$202(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@2e
    goto :goto_5

    #@2f
    .line 179
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    :pswitch_2f
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@31
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@33
    .line 180
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@35
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@38
    move-result-object v12

    #@39
    invoke-static {v11, v12}, Lcom/android/bluetooth/hid/HidService;->access$300(Lcom/android/bluetooth/hid/HidService;[B)Z

    #@3c
    move-result v11

    #@3d
    if-nez v11, :cond_5

    #@3f
    .line 181
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@41
    const/4 v12, 0x3

    #@42
    invoke-static {v11, v2, v12}, Lcom/android/bluetooth/hid/HidService;->access$000(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V

    #@45
    .line 182
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@47
    const/4 v12, 0x0

    #@48
    invoke-static {v11, v2, v12}, Lcom/android/bluetooth/hid/HidService;->access$000(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V

    #@4b
    goto :goto_5

    #@4c
    .line 189
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    :pswitch_4c
    iget-object v12, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@4e
    iget-object v11, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@50
    check-cast v11, [B

    #@52
    check-cast v11, [B

    #@54
    invoke-static {v12, v11}, Lcom/android/bluetooth/hid/HidService;->access$400(Lcom/android/bluetooth/hid/HidService;[B)Landroid/bluetooth/BluetoothDevice;

    #@57
    move-result-object v2

    #@58
    .line 190
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@5a
    .line 191
    .local v3, halState:I
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@5c
    invoke-static {v11}, Lcom/android/bluetooth/hid/HidService;->access$500(Lcom/android/bluetooth/hid/HidService;)Ljava/util/Map;

    #@5f
    move-result-object v11

    #@60
    invoke-interface {v11, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@63
    move-result-object v5

    #@64
    check-cast v5, Ljava/lang/Integer;

    #@66
    .line 192
    .local v5, prevStateInteger:Ljava/lang/Integer;
    if-nez v5, :cond_b6

    #@68
    const/4 v4, 0x0

    #@69
    .line 195
    .local v4, prevState:I
    :goto_69
    const-string v11, "HidService"

    #@6b
    new-instance v12, Ljava/lang/StringBuilder;

    #@6d
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@70
    const-string v13, "MESSAGE_CONNECT_STATE_CHANGED newState:"

    #@72
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@75
    move-result-object v12

    #@76
    invoke-static {v3}, Lcom/android/bluetooth/hid/HidService;->access$600(I)I

    #@79
    move-result v13

    #@7a
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v12

    #@7e
    const-string v13, ", prevState:"

    #@80
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@83
    move-result-object v12

    #@84
    invoke-virtual {v12, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@87
    move-result-object v12

    #@88
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8b
    move-result-object v12

    #@8c
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8f
    .line 198
    if-nez v3, :cond_bb

    #@91
    if-nez v4, :cond_bb

    #@93
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@95
    invoke-static {v11, v2}, Lcom/android/bluetooth/hid/HidService;->access$700(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;)Z

    #@98
    move-result v11

    #@99
    if-nez v11, :cond_bb

    #@9b
    .line 202
    const-string v11, "HidService"

    #@9d
    const-string v12, "Incoming HID connection rejected"

    #@9f
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a2
    .line 204
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@a4
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@a7
    move-result-object v12

    #@a8
    invoke-static {v11, v12}, Lcom/android/bluetooth/hid/HidService;->access$300(Lcom/android/bluetooth/hid/HidService;[B)Z

    #@ab
    .line 216
    :goto_ab
    const/4 v11, 0x1

    #@ac
    if-eq v3, v11, :cond_da

    #@ae
    .line 217
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@b0
    const/4 v12, 0x0

    #@b1
    invoke-static {v11, v12}, Lcom/android/bluetooth/hid/HidService;->access$202(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@b4
    goto/16 :goto_5

    #@b6
    .line 192
    .end local v4           #prevState:I
    :cond_b6
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@b9
    move-result v4

    #@ba
    goto :goto_69

    #@bb
    .line 207
    .restart local v4       #prevState:I
    :cond_bb
    const/4 v11, 0x3

    #@bc
    if-ne v3, v11, :cond_d0

    #@be
    if-nez v4, :cond_d0

    #@c0
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@c2
    invoke-static {v11, v2}, Lcom/android/bluetooth/hid/HidService;->access$700(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;)Z

    #@c5
    move-result v11

    #@c6
    if-nez v11, :cond_d0

    #@c8
    .line 210
    const-string v11, "HidService"

    #@ca
    const-string v12, "[BTUI] skip toast"

    #@cc
    invoke-static {v11, v12}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@cf
    goto :goto_ab

    #@d0
    .line 214
    :cond_d0
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@d2
    invoke-static {v3}, Lcom/android/bluetooth/hid/HidService;->access$600(I)I

    #@d5
    move-result v12

    #@d6
    invoke-static {v11, v2, v12}, Lcom/android/bluetooth/hid/HidService;->access$000(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V

    #@d9
    goto :goto_ab

    #@da
    .line 222
    :cond_da
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@dc
    invoke-static {v11, v2}, Lcom/android/bluetooth/hid/HidService;->access$202(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;

    #@df
    goto/16 :goto_5

    #@e1
    .line 228
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v3           #halState:I
    .end local v4           #prevState:I
    .end local v5           #prevStateInteger:Ljava/lang/Integer;
    :pswitch_e1
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@e3
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@e5
    .line 229
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@e7
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@ea
    move-result-object v12

    #@eb
    invoke-static {v11, v12}, Lcom/android/bluetooth/hid/HidService;->access$800(Lcom/android/bluetooth/hid/HidService;[B)Z

    #@ee
    move-result v11

    #@ef
    if-nez v11, :cond_5

    #@f1
    .line 230
    const-string v11, "HidService"

    #@f3
    const-string v12, "Error: get protocol mode native returns false"

    #@f5
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@f8
    goto/16 :goto_5

    #@fa
    .line 237
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    :pswitch_fa
    iget-object v12, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@fc
    iget-object v11, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@fe
    check-cast v11, [B

    #@100
    check-cast v11, [B

    #@102
    invoke-static {v12, v11}, Lcom/android/bluetooth/hid/HidService;->access$900(Lcom/android/bluetooth/hid/HidService;[B)Landroid/bluetooth/BluetoothDevice;

    #@105
    move-result-object v2

    #@106
    .line 238
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    iget v6, p1, Landroid/os/Message;->arg1:I

    #@108
    .line 239
    .local v6, protocolMode:I
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@10a
    invoke-static {v11, v2, v6}, Lcom/android/bluetooth/hid/HidService;->access$1000(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V

    #@10d
    goto/16 :goto_5

    #@10f
    .line 244
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #protocolMode:I
    :pswitch_10f
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@111
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@113
    .line 245
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@115
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@118
    move-result-object v12

    #@119
    invoke-static {v11, v12}, Lcom/android/bluetooth/hid/HidService;->access$1100(Lcom/android/bluetooth/hid/HidService;[B)Z

    #@11c
    move-result v11

    #@11d
    if-nez v11, :cond_5

    #@11f
    .line 246
    const-string v11, "HidService"

    #@121
    const-string v12, "Error: virtual unplug native returns false"

    #@123
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@126
    goto/16 :goto_5

    #@128
    .line 252
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    :pswitch_128
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@12a
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@12c
    .line 253
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    iget v11, p1, Landroid/os/Message;->arg1:I

    #@12e
    int-to-byte v6, v11

    #@12f
    .line 254
    .local v6, protocolMode:B
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@131
    new-instance v12, Ljava/lang/StringBuilder;

    #@133
    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    #@136
    const-string v13, "sending set protocol mode("

    #@138
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13b
    move-result-object v12

    #@13c
    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v12

    #@140
    const-string v13, ")"

    #@142
    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@145
    move-result-object v12

    #@146
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@149
    move-result-object v12

    #@14a
    invoke-static {v11, v12}, Lcom/android/bluetooth/hid/HidService;->access$1200(Lcom/android/bluetooth/hid/HidService;Ljava/lang/String;)V

    #@14d
    .line 255
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@14f
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@152
    move-result-object v12

    #@153
    invoke-static {v11, v12, v6}, Lcom/android/bluetooth/hid/HidService;->access$1300(Lcom/android/bluetooth/hid/HidService;[BB)Z

    #@156
    move-result v11

    #@157
    if-nez v11, :cond_5

    #@159
    .line 256
    const-string v11, "HidService"

    #@15b
    const-string v12, "Error: set protocol mode native returns false"

    #@15d
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@160
    goto/16 :goto_5

    #@162
    .line 262
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v6           #protocolMode:B
    :pswitch_162
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@164
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@166
    .line 263
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@169
    move-result-object v1

    #@16a
    .line 264
    .local v1, data:Landroid/os/Bundle;
    const-string v11, "android.bluetooth.BluetoothInputDevice.extra.REPORT_TYPE"

    #@16c
    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@16f
    move-result v9

    #@170
    .line 265
    .local v9, reportType:B
    const-string v11, "android.bluetooth.BluetoothInputDevice.extra.REPORT_ID"

    #@172
    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@175
    move-result v8

    #@176
    .line 266
    .local v8, reportId:B
    const-string v11, "android.bluetooth.BluetoothInputDevice.extra.REPORT_BUFFER_SIZE"

    #@178
    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    #@17b
    move-result v0

    #@17c
    .line 267
    .local v0, bufferSize:I
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@17e
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@181
    move-result-object v12

    #@182
    invoke-static {v11, v12, v9, v8, v0}, Lcom/android/bluetooth/hid/HidService;->access$1400(Lcom/android/bluetooth/hid/HidService;[BBBI)Z

    #@185
    move-result v11

    #@186
    if-nez v11, :cond_5

    #@188
    .line 268
    const-string v11, "HidService"

    #@18a
    const-string v12, "Error: get report native returns false"

    #@18c
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@18f
    goto/16 :goto_5

    #@191
    .line 274
    .end local v0           #bufferSize:I
    .end local v1           #data:Landroid/os/Bundle;
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v8           #reportId:B
    .end local v9           #reportType:B
    :pswitch_191
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@193
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@195
    .line 275
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@198
    move-result-object v1

    #@199
    .line 276
    .restart local v1       #data:Landroid/os/Bundle;
    const-string v11, "android.bluetooth.BluetoothInputDevice.extra.REPORT_TYPE"

    #@19b
    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getByte(Ljava/lang/String;)B

    #@19e
    move-result v9

    #@19f
    .line 277
    .restart local v9       #reportType:B
    const-string v11, "android.bluetooth.BluetoothInputDevice.extra.REPORT"

    #@1a1
    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1a4
    move-result-object v7

    #@1a5
    .line 278
    .local v7, report:Ljava/lang/String;
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@1a7
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@1aa
    move-result-object v12

    #@1ab
    invoke-static {v11, v12, v9, v7}, Lcom/android/bluetooth/hid/HidService;->access$1500(Lcom/android/bluetooth/hid/HidService;[BBLjava/lang/String;)Z

    #@1ae
    move-result v11

    #@1af
    if-nez v11, :cond_5

    #@1b1
    .line 279
    const-string v11, "HidService"

    #@1b3
    const-string v12, "Error: set report native returns false"

    #@1b5
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b8
    goto/16 :goto_5

    #@1ba
    .line 285
    .end local v1           #data:Landroid/os/Bundle;
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v7           #report:Ljava/lang/String;
    .end local v9           #reportType:B
    :pswitch_1ba
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1bc
    check-cast v2, Landroid/bluetooth/BluetoothDevice;

    #@1be
    .line 286
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@1c1
    move-result-object v1

    #@1c2
    .line 287
    .restart local v1       #data:Landroid/os/Bundle;
    const-string v11, "android.bluetooth.BluetoothInputDevice.extra.REPORT"

    #@1c4
    invoke-virtual {v1, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    #@1c7
    move-result-object v7

    #@1c8
    .line 288
    .restart local v7       #report:Ljava/lang/String;
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@1ca
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@1cd
    move-result-object v12

    #@1ce
    invoke-static {v11, v12, v7}, Lcom/android/bluetooth/hid/HidService;->access$1600(Lcom/android/bluetooth/hid/HidService;[BLjava/lang/String;)Z

    #@1d1
    move-result v11

    #@1d2
    if-nez v11, :cond_5

    #@1d4
    .line 289
    const-string v11, "HidService"

    #@1d6
    const-string v12, "Error: send data native returns false"

    #@1d8
    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1db
    goto/16 :goto_5

    #@1dd
    .line 295
    .end local v1           #data:Landroid/os/Bundle;
    .end local v2           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v7           #report:Ljava/lang/String;
    :pswitch_1dd
    iget-object v12, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@1df
    iget-object v11, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1e1
    check-cast v11, [B

    #@1e3
    check-cast v11, [B

    #@1e5
    invoke-static {v12, v11}, Lcom/android/bluetooth/hid/HidService;->access$1700(Lcom/android/bluetooth/hid/HidService;[B)Landroid/bluetooth/BluetoothDevice;

    #@1e8
    move-result-object v2

    #@1e9
    .line 296
    .restart local v2       #device:Landroid/bluetooth/BluetoothDevice;
    iget v10, p1, Landroid/os/Message;->arg1:I

    #@1eb
    .line 297
    .local v10, status:I
    iget-object v11, p0, Lcom/android/bluetooth/hid/HidService$1;->this$0:Lcom/android/bluetooth/hid/HidService;

    #@1ed
    invoke-static {v11, v2, v10}, Lcom/android/bluetooth/hid/HidService;->access$1800(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V

    #@1f0
    goto/16 :goto_5

    #@1f2
    .line 158
    :pswitch_data_1f2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_2f
        :pswitch_4c
        :pswitch_e1
        :pswitch_10f
        :pswitch_fa
        :pswitch_128
        :pswitch_162
        :pswitch_5
        :pswitch_191
        :pswitch_1ba
        :pswitch_1dd
    .end packed-switch
.end method
