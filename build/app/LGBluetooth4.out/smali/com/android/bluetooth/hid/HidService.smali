.class public Lcom/android/bluetooth/hid/HidService;
.super Lcom/android/bluetooth/btservice/ProfileService;
.source "HidService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;
    }
.end annotation


# static fields
.field private static final CONN_STATE_CONNECTED:I = 0x0

.field private static final CONN_STATE_CONNECTING:I = 0x1

.field private static final CONN_STATE_DISCONNECTED:I = 0x2

.field private static final CONN_STATE_DISCONNECTING:I = 0x3

.field private static final DBG:Z = true

.field private static final MESSAGE_CONNECT:I = 0x1

.field private static final MESSAGE_CONNECT_STATE_CHANGED:I = 0x3

.field private static final MESSAGE_DISCONNECT:I = 0x2

.field private static final MESSAGE_GET_PROTOCOL_MODE:I = 0x4

.field private static final MESSAGE_GET_REPORT:I = 0x8

.field private static final MESSAGE_ON_GET_PROTOCOL_MODE:I = 0x6

.field private static final MESSAGE_ON_GET_REPORT:I = 0x9

.field private static final MESSAGE_ON_VIRTUAL_UNPLUG:I = 0xc

.field private static final MESSAGE_SEND_DATA:I = 0xb

.field private static final MESSAGE_SET_PROTOCOL_MODE:I = 0x7

.field private static final MESSAGE_SET_REPORT:I = 0xa

.field private static final MESSAGE_VIRTUAL_UNPLUG:I = 0x5

.field private static final TAG:Ljava/lang/String; = "HidService"

.field private static sHidService:Lcom/android/bluetooth/hid/HidService;


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mInputDevices:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mNativeAvailable:Z

.field private mTargetDevice:Landroid/bluetooth/BluetoothDevice;


# direct methods
.method static constructor <clinit>()V
    .registers 0

    #@0
    .prologue
    .line 76
    invoke-static {}, Lcom/android/bluetooth/hid/HidService;->classInitNative()V

    #@3
    .line 77
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 53
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/ProfileService;-><init>()V

    #@3
    .line 60
    const/4 v0, 0x0

    #@4
    iput-object v0, p0, Lcom/android/bluetooth/hid/HidService;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@6
    .line 154
    new-instance v0, Lcom/android/bluetooth/hid/HidService$1;

    #@8
    invoke-direct {v0, p0}, Lcom/android/bluetooth/hid/HidService$1;-><init>(Lcom/android/bluetooth/hid/HidService;)V

    #@b
    iput-object v0, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@d
    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hid/HidService;->broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;I)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/bluetooth/hid/HidService;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hid/HidService;->connectHidNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1000(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hid/HidService;->broadcastProtocolMode(Landroid/bluetooth/BluetoothDevice;I)V

    #@3
    return-void
.end method

.method static synthetic access$1100(Lcom/android/bluetooth/hid/HidService;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hid/HidService;->virtualUnPlugNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1200(Lcom/android/bluetooth/hid/HidService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->log(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/bluetooth/hid/HidService;[BB)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hid/HidService;->setProtocolModeNative([BB)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1400(Lcom/android/bluetooth/hid/HidService;[BBBI)Z
    .registers 6
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/bluetooth/hid/HidService;->getReportNative([BBBI)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1500(Lcom/android/bluetooth/hid/HidService;[BBLjava/lang/String;)Z
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/hid/HidService;->setReportNative([BBLjava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1600(Lcom/android/bluetooth/hid/HidService;[BLjava/lang/String;)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hid/HidService;->sendDataNative([BLjava/lang/String;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1700(Lcom/android/bluetooth/hid/HidService;[B)Landroid/bluetooth/BluetoothDevice;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1800(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/hid/HidService;->broadcastVirtualUnplugStatus(Landroid/bluetooth/BluetoothDevice;I)V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/bluetooth/hid/HidService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/android/bluetooth/hid/HidService;->isAvailable()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$202(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;)Landroid/bluetooth/BluetoothDevice;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    iput-object p1, p0, Lcom/android/bluetooth/hid/HidService;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@2
    return-object p1
.end method

.method static synthetic access$300(Lcom/android/bluetooth/hid/HidService;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hid/HidService;->disconnectHidNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Lcom/android/bluetooth/hid/HidService;[B)Landroid/bluetooth/BluetoothDevice;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/bluetooth/hid/HidService;)Ljava/util/Map;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    iget-object v0, p0, Lcom/android/bluetooth/hid/HidService;->mInputDevices:Ljava/util/Map;

    #@2
    return-object v0
.end method

.method static synthetic access$600(I)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 53
    invoke-static {p0}, Lcom/android/bluetooth/hid/HidService;->convertHalState(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$700(Lcom/android/bluetooth/hid/HidService;Landroid/bluetooth/BluetoothDevice;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hid/HidService;->okToConnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$800(Lcom/android/bluetooth/hid/HidService;[B)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getProtocolModeNative([B)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$900(Lcom/android/bluetooth/hid/HidService;[B)Landroid/bluetooth/BluetoothDevice;
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private broadcastConnectionState(Landroid/bluetooth/BluetoothDevice;I)V
    .registers 10
    .parameter "device"
    .parameter "newState"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    .line 621
    iget-object v3, p0, Lcom/android/bluetooth/hid/HidService;->mInputDevices:Ljava/util/Map;

    #@3
    invoke-interface {v3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    check-cast v2, Ljava/lang/Integer;

    #@9
    .line 622
    .local v2, prevStateInteger:Ljava/lang/Integer;
    if-nez v2, :cond_27

    #@b
    const/4 v1, 0x0

    #@c
    .line 624
    .local v1, prevState:I
    :goto_c
    if-ne v1, p2, :cond_2c

    #@e
    .line 625
    const-string v3, "HidService"

    #@10
    new-instance v4, Ljava/lang/StringBuilder;

    #@12
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15
    const-string v5, "no state change: "

    #@17
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v4

    #@1f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v4

    #@23
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 651
    :cond_26
    :goto_26
    return-void

    #@27
    .line 622
    .end local v1           #prevState:I
    :cond_27
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    #@2a
    move-result v1

    #@2b
    goto :goto_c

    #@2c
    .line 628
    .restart local v1       #prevState:I
    :cond_2c
    iget-object v3, p0, Lcom/android/bluetooth/hid/HidService;->mInputDevices:Ljava/util/Map;

    #@2e
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@31
    move-result-object v4

    #@32
    invoke-interface {v3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@35
    .line 633
    new-instance v3, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v4, "Connection state "

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@43
    move-result-object v3

    #@44
    const-string v4, ": "

    #@46
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v3

    #@4e
    const-string v4, "->"

    #@50
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v3

    #@5c
    invoke-virtual {p0, v3}, Lcom/android/bluetooth/hid/HidService;->log(Ljava/lang/String;)V

    #@5f
    .line 634
    const/4 v3, 0x4

    #@60
    invoke-virtual {p0, p1, v3, p2, v1}, Lcom/android/bluetooth/hid/HidService;->notifyProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V

    #@63
    .line 636
    new-instance v0, Landroid/content/Intent;

    #@65
    const-string v3, "android.bluetooth.input.profile.action.CONNECTION_STATE_CHANGED"

    #@67
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@6a
    .line 637
    .local v0, intent:Landroid/content/Intent;
    const-string v3, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    #@6c
    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@6f
    .line 638
    const-string v3, "android.bluetooth.profile.extra.STATE"

    #@71
    invoke-virtual {v0, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@74
    .line 639
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    #@76
    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@79
    .line 640
    const/high16 v3, 0x800

    #@7b
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@7e
    .line 641
    const-string v3, "android.permission.BLUETOOTH"

    #@80
    invoke-virtual {p0, v0, v3}, Lcom/android/bluetooth/hid/HidService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@83
    .line 643
    if-ne p2, v6, :cond_95

    #@85
    .line 644
    invoke-static {}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getDefaultChecker()Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@88
    move-result-object v3

    #@89
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@8c
    move-result-object v4

    #@8d
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAliasName()Ljava/lang/String;

    #@90
    move-result-object v5

    #@91
    invoke-virtual {v3, p0, v6, v4, v5}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->setConnected(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Z

    #@94
    goto :goto_26

    #@95
    .line 646
    :cond_95
    if-nez p2, :cond_26

    #@97
    .line 647
    invoke-static {}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getDefaultChecker()Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@9a
    move-result-object v3

    #@9b
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@9e
    move-result-object v4

    #@9f
    invoke-virtual {v3, p0, v6, v4}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->setDisconnected(Landroid/content/Context;ILjava/lang/String;)Z

    #@a2
    goto :goto_26
.end method

.method private broadcastProtocolMode(Landroid/bluetooth/BluetoothDevice;I)V
    .registers 6
    .parameter "device"
    .parameter "protocolMode"

    #@0
    .prologue
    .line 654
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.bluetooth.input.profile.action.PROTOCOL_MODE_CHANGED"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 655
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@c
    .line 656
    const-string v1, "android.bluetooth.BluetoothInputDevice.extra.PROTOCOL_MODE"

    #@e
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@11
    .line 657
    const/high16 v1, 0x800

    #@13
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@16
    .line 658
    const-string v1, "android.permission.BLUETOOTH"

    #@18
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hid/HidService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@1b
    .line 660
    new-instance v1, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v2, "Protocol Mode ("

    #@22
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v1

    #@2a
    const-string v2, "): "

    #@2c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/hid/HidService;->log(Ljava/lang/String;)V

    #@3b
    .line 662
    return-void
.end method

.method private broadcastVirtualUnplugStatus(Landroid/bluetooth/BluetoothDevice;I)V
    .registers 5
    .parameter "device"
    .parameter "status"

    #@0
    .prologue
    .line 665
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.bluetooth.input.profile.action.VIRTUAL_UNPLUG_STATUS"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 666
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    #@9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@c
    .line 667
    const-string v1, "android.bluetooth.BluetoothInputDevice.extra.VIRTUAL_UNPLUG_STATUS"

    #@e
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@11
    .line 668
    const/high16 v1, 0x800

    #@13
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@16
    .line 669
    const-string v1, "android.permission.BLUETOOTH"

    #@18
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hid/HidService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@1b
    .line 670
    return-void
.end method

.method private static native classInitNative()V
.end method

.method private native cleanupNative()V
.end method

.method private static declared-synchronized clearHidService()V
    .registers 2

    #@0
    .prologue
    .line 150
    const-class v0, Lcom/android/bluetooth/hid/HidService;

    #@2
    monitor-enter v0

    #@3
    const/4 v1, 0x0

    #@4
    :try_start_4
    sput-object v1, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;
    :try_end_6
    .catchall {:try_start_4 .. :try_end_6} :catchall_8

    #@6
    .line 151
    monitor-exit v0

    #@7
    return-void

    #@8
    .line 150
    :catchall_8
    move-exception v1

    #@9
    monitor-exit v0

    #@a
    throw v1
.end method

.method private native connectHidNative([B)Z
.end method

.method private static convertHalState(I)I
    .registers 5
    .parameter "halState"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 686
    packed-switch p0, :pswitch_data_24

    #@4
    .line 696
    const-string v1, "HidService"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "bad hid connection state: "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 697
    :goto_1c
    :pswitch_1c
    return v0

    #@1d
    .line 688
    :pswitch_1d
    const/4 v0, 0x2

    #@1e
    goto :goto_1c

    #@1f
    .line 690
    :pswitch_1f
    const/4 v0, 0x1

    #@20
    goto :goto_1c

    #@21
    .line 694
    :pswitch_21
    const/4 v0, 0x3

    #@22
    goto :goto_1c

    #@23
    .line 686
    nop

    #@24
    :pswitch_data_24
    .packed-switch 0x0
        :pswitch_1d
        :pswitch_1f
        :pswitch_1c
        :pswitch_21
    .end packed-switch
.end method

.method private native disconnectHidNative([B)Z
.end method

.method public static declared-synchronized getHidService()Lcom/android/bluetooth/hid/HidService;
    .registers 4

    #@0
    .prologue
    .line 116
    const-class v1, Lcom/android/bluetooth/hid/HidService;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;

    #@5
    if-eqz v0, :cond_2d

    #@7
    sget-object v0, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/hid/HidService;->isAvailable()Z

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_2d

    #@f
    .line 118
    const-string v0, "HidService"

    #@11
    new-instance v2, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v3, "getHidService(): returning "

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    sget-object v3, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v2

    #@22
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v2

    #@26
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 120
    sget-object v0, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;
    :try_end_2b
    .catchall {:try_start_3 .. :try_end_2b} :catchall_4a

    #@2b
    .line 129
    :goto_2b
    monitor-exit v1

    #@2c
    return-object v0

    #@2d
    .line 123
    :cond_2d
    :try_start_2d
    sget-object v0, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;

    #@2f
    if-nez v0, :cond_3a

    #@31
    .line 124
    const-string v0, "HidService"

    #@33
    const-string v2, "getHidService(): service is NULL"

    #@35
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 129
    :cond_38
    :goto_38
    const/4 v0, 0x0

    #@39
    goto :goto_2b

    #@3a
    .line 125
    :cond_3a
    sget-object v0, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;

    #@3c
    invoke-virtual {v0}, Lcom/android/bluetooth/hid/HidService;->isAvailable()Z

    #@3f
    move-result v0

    #@40
    if-nez v0, :cond_38

    #@42
    .line 126
    const-string v0, "HidService"

    #@44
    const-string v2, "getHidService(): service is not available"

    #@46
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_49
    .catchall {:try_start_2d .. :try_end_49} :catchall_4a

    #@49
    goto :goto_38

    #@4a
    .line 116
    :catchall_4a
    move-exception v0

    #@4b
    monitor-exit v1

    #@4c
    throw v0
.end method

.method private native getProtocolModeNative([B)Z
.end method

.method private native getReportNative([BBBI)Z
.end method

.method private native initializeNative()V
.end method

.method private okToConnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 673
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 676
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_1e

    #@6
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->isQuietModeEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_10

    #@c
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mTargetDevice:Landroid/bluetooth/BluetoothDevice;

    #@e
    if-eqz v1, :cond_1e

    #@10
    :cond_10
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_1e

    #@16
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    #@19
    move-result v1

    #@1a
    const/16 v2, 0xa

    #@1c
    if-ne v1, v2, :cond_20

    #@1e
    .line 680
    :cond_1e
    const/4 v1, 0x0

    #@1f
    .line 683
    :goto_1f
    return v1

    #@20
    :cond_20
    const/4 v1, 0x1

    #@21
    goto :goto_1f
.end method

.method private onConnectStateChanged([BI)V
    .registers 6
    .parameter "address"
    .parameter "state"

    #@0
    .prologue
    .line 613
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v2, 0x3

    #@3
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 614
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 615
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@b
    .line 616
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@d
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@10
    .line 617
    return-void
.end method

.method private onGetProtocolMode([BI)V
    .registers 6
    .parameter "address"
    .parameter "mode"

    #@0
    .prologue
    .line 599
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v2, 0x6

    #@3
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 600
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 601
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@b
    .line 602
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@d
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@10
    .line 603
    return-void
.end method

.method private onVirtualUnplug([BI)V
    .registers 6
    .parameter "address"
    .parameter "status"

    #@0
    .prologue
    .line 606
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@2
    const/16 v2, 0xc

    #@4
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v0

    #@8
    .line 607
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    .line 608
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@c
    .line 609
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@e
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@11
    .line 610
    return-void
.end method

.method private native sendDataNative([BLjava/lang/String;)Z
.end method

.method private static declared-synchronized setHidService(Lcom/android/bluetooth/hid/HidService;)V
    .registers 5
    .parameter "instance"

    #@0
    .prologue
    .line 133
    const-class v1, Lcom/android/bluetooth/hid/HidService;

    #@2
    monitor-enter v1

    #@3
    if-eqz p0, :cond_29

    #@5
    :try_start_5
    invoke-virtual {p0}, Lcom/android/bluetooth/hid/HidService;->isAvailable()Z

    #@8
    move-result v0

    #@9
    if-eqz v0, :cond_29

    #@b
    .line 135
    const-string v0, "HidService"

    #@d
    new-instance v2, Ljava/lang/StringBuilder;

    #@f
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@12
    const-string v3, "setHidService(): set to: "

    #@14
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v2

    #@18
    sget-object v3, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;

    #@1a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 137
    sput-object p0, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;
    :try_end_27
    .catchall {:try_start_5 .. :try_end_27} :catchall_35

    #@27
    .line 147
    :cond_27
    :goto_27
    monitor-exit v1

    #@28
    return-void

    #@29
    .line 140
    :cond_29
    :try_start_29
    sget-object v0, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;

    #@2b
    if-nez v0, :cond_38

    #@2d
    .line 141
    const-string v0, "HidService"

    #@2f
    const-string v2, "setHidService(): service not available"

    #@31
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_34
    .catchall {:try_start_29 .. :try_end_34} :catchall_35

    #@34
    goto :goto_27

    #@35
    .line 133
    :catchall_35
    move-exception v0

    #@36
    monitor-exit v1

    #@37
    throw v0

    #@38
    .line 142
    :cond_38
    :try_start_38
    sget-object v0, Lcom/android/bluetooth/hid/HidService;->sHidService:Lcom/android/bluetooth/hid/HidService;

    #@3a
    invoke-virtual {v0}, Lcom/android/bluetooth/hid/HidService;->isAvailable()Z

    #@3d
    move-result v0

    #@3e
    if-nez v0, :cond_27

    #@40
    .line 143
    const-string v0, "HidService"

    #@42
    const-string v2, "setHidService(): service is cleaning up"

    #@44
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_47
    .catchall {:try_start_38 .. :try_end_47} :catchall_35

    #@47
    goto :goto_27
.end method

.method private native setProtocolModeNative([BB)Z
.end method

.method private native setReportNative([BBLjava/lang/String;)Z
.end method

.method private native virtualUnPlugNative([B)Z
.end method


# virtual methods
.method protected cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/android/bluetooth/hid/HidService;->mNativeAvailable:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 104
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService;->cleanupNative()V

    #@7
    .line 105
    const/4 v0, 0x0

    #@8
    iput-boolean v0, p0, Lcom/android/bluetooth/hid/HidService;->mNativeAvailable:Z

    #@a
    .line 108
    :cond_a
    iget-object v0, p0, Lcom/android/bluetooth/hid/HidService;->mInputDevices:Ljava/util/Map;

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 109
    iget-object v0, p0, Lcom/android/bluetooth/hid/HidService;->mInputDevices:Ljava/util/Map;

    #@10
    invoke-interface {v0}, Ljava/util/Map;->clear()V

    #@13
    .line 111
    :cond_13
    invoke-static {}, Lcom/android/bluetooth/hid/HidService;->clearHidService()V

    #@16
    .line 112
    const/4 v0, 0x1

    #@17
    return v0
.end method

.method connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 7
    .parameter "device"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 437
    const-string v3, "android.permission.BLUETOOTH"

    #@4
    const-string v4, "Need BLUETOOTH permission"

    #@6
    invoke-virtual {p0, v3, v4}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@9
    .line 438
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@c
    move-result v3

    #@d
    if-eqz v3, :cond_28

    #@f
    .line 439
    const-string v2, "HidService"

    #@11
    new-instance v3, Ljava/lang/StringBuilder;

    #@13
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@16
    const-string v4, "Hid Device not disconnected: "

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 449
    :goto_27
    return v1

    #@28
    .line 442
    :cond_28
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@2b
    move-result v3

    #@2c
    if-nez v3, :cond_47

    #@2e
    .line 443
    const-string v2, "HidService"

    #@30
    new-instance v3, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v4, "Hid Device PRIORITY_OFF: "

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v3

    #@3f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v3

    #@43
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_27

    #@47
    .line 447
    :cond_47
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@49
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@4c
    move-result-object v0

    #@4d
    .line 448
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@4f
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@52
    move v1, v2

    #@53
    .line 449
    goto :goto_27
.end method

.method disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 453
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 454
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@9
    const/4 v2, 0x2

    #@a
    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@d
    move-result-object v0

    #@e
    .line 455
    .local v0, msg:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@10
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@13
    .line 456
    const/4 v1, 0x1

    #@14
    return v1
.end method

.method getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 3
    .parameter "device"

    #@0
    .prologue
    .line 460
    iget-object v0, p0, Lcom/android/bluetooth/hid/HidService;->mInputDevices:Ljava/util/Map;

    #@2
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@5
    move-result-object v0

    #@6
    if-nez v0, :cond_a

    #@8
    .line 461
    const/4 v0, 0x0

    #@9
    .line 463
    :goto_9
    return v0

    #@a
    :cond_a
    iget-object v0, p0, Lcom/android/bluetooth/hid/HidService;->mInputDevices:Ljava/util/Map;

    #@c
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@f
    move-result-object v0

    #@10
    check-cast v0, Ljava/lang/Integer;

    #@12
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@15
    move-result v0

    #@16
    goto :goto_9
.end method

.method getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 12
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 467
    const-string v8, "android.permission.BLUETOOTH"

    #@2
    const-string v9, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v8, v9}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 468
    new-instance v5, Ljava/util/ArrayList;

    #@9
    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    #@c
    .line 470
    .local v5, inputDevices:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    iget-object v8, p0, Lcom/android/bluetooth/hid/HidService;->mInputDevices:Ljava/util/Map;

    #@e
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    #@11
    move-result-object v8

    #@12
    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@15
    move-result-object v2

    #@16
    :cond_16
    :goto_16
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@19
    move-result v8

    #@1a
    if-eqz v8, :cond_36

    #@1c
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1f
    move-result-object v1

    #@20
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@22
    .line 471
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/hid/HidService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@25
    move-result v4

    #@26
    .line 472
    .local v4, inputDeviceState:I
    move-object v0, p1

    #@27
    .local v0, arr$:[I
    array-length v6, v0

    #@28
    .local v6, len$:I
    const/4 v3, 0x0

    #@29
    .local v3, i$:I
    :goto_29
    if-ge v3, v6, :cond_16

    #@2b
    aget v7, v0, v3

    #@2d
    .line 473
    .local v7, state:I
    if-ne v7, v4, :cond_33

    #@2f
    .line 474
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    #@32
    goto :goto_16

    #@33
    .line 472
    :cond_33
    add-int/lit8 v3, v3, 0x1

    #@35
    goto :goto_29

    #@36
    .line 479
    .end local v0           #arr$:[I
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v3           #i$:I
    .end local v4           #inputDeviceState:I
    .end local v6           #len$:I
    .end local v7           #state:I
    :cond_36
    return-object v5
.end method

.method public getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 80
    const-string v0, "HidService"

    #@2
    return-object v0
.end method

.method public getPriority(Landroid/bluetooth/BluetoothDevice;)I
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 495
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v2, "Need BLUETOOTH_ADMIN permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 497
    invoke-virtual {p0}, Lcom/android/bluetooth/hid/HidService;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v1

    #@b
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@e
    move-result-object v2

    #@f
    invoke-static {v2}, Landroid/provider/Settings$Global;->getBluetoothInputDevicePriorityKey(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v2

    #@13
    const/4 v3, -0x1

    #@14
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@17
    move-result v0

    #@18
    .line 500
    .local v0, priority:I
    return v0
.end method

.method getProtocolMode(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 505
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v3, "Need BLUETOOTH_ADMIN permission"

    #@4
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 507
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@a
    move-result v1

    #@b
    .line 508
    .local v1, state:I
    const/4 v2, 0x2

    #@c
    if-eq v1, v2, :cond_10

    #@e
    .line 509
    const/4 v2, 0x0

    #@f
    .line 513
    :goto_f
    return v2

    #@10
    .line 511
    :cond_10
    iget-object v2, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@12
    const/4 v3, 0x4

    #@13
    invoke-virtual {v2, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@16
    move-result-object v0

    #@17
    .line 512
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@19
    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1c
    .line 513
    const/4 v2, 0x1

    #@1d
    goto :goto_f
.end method

.method getReport(Landroid/bluetooth/BluetoothDevice;BBI)Z
    .registers 10
    .parameter "device"
    .parameter "reportType"
    .parameter "reportId"
    .parameter "bufferSize"

    #@0
    .prologue
    .line 545
    const-string v3, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v4, "Need BLUETOOTH_ADMIN permission"

    #@4
    invoke-virtual {p0, v3, v4}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 547
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@a
    move-result v2

    #@b
    .line 548
    .local v2, state:I
    const/4 v3, 0x2

    #@c
    if-eq v2, v3, :cond_10

    #@e
    .line 549
    const/4 v3, 0x0

    #@f
    .line 559
    :goto_f
    return v3

    #@10
    .line 551
    :cond_10
    iget-object v3, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@12
    const/16 v4, 0x8

    #@14
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@17
    move-result-object v1

    #@18
    .line 552
    .local v1, msg:Landroid/os/Message;
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a
    .line 553
    new-instance v0, Landroid/os/Bundle;

    #@1c
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@1f
    .line 554
    .local v0, data:Landroid/os/Bundle;
    const-string v3, "android.bluetooth.BluetoothInputDevice.extra.REPORT_TYPE"

    #@21
    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@24
    .line 555
    const-string v3, "android.bluetooth.BluetoothInputDevice.extra.REPORT_ID"

    #@26
    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@29
    .line 556
    const-string v3, "android.bluetooth.BluetoothInputDevice.extra.REPORT_BUFFER_SIZE"

    #@2b
    invoke-virtual {v0, v3, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@2e
    .line 557
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@31
    .line 558
    iget-object v3, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@33
    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@36
    .line 559
    const/4 v3, 0x1

    #@37
    goto :goto_f
.end method

.method public initBinder()Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;
    .registers 2

    #@0
    .prologue
    .line 84
    new-instance v0, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;

    #@2
    invoke-direct {v0, p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;-><init>(Lcom/android/bluetooth/hid/HidService;)V

    #@5
    return-object v0
.end method

.method sendData(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z
    .registers 6
    .parameter "device"
    .parameter "report"

    #@0
    .prologue
    .line 581
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v2, "Need BLUETOOTH_ADMIN permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 583
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@a
    move-result v0

    #@b
    .line 584
    .local v0, state:I
    const/4 v1, 0x2

    #@c
    if-eq v0, v1, :cond_10

    #@e
    .line 585
    const/4 v1, 0x0

    #@f
    .line 588
    :goto_f
    return v1

    #@10
    :cond_10
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@13
    move-result-object v1

    #@14
    invoke-direct {p0, v1, p2}, Lcom/android/bluetooth/hid/HidService;->sendDataNative([BLjava/lang/String;)Z

    #@17
    move-result v1

    #@18
    goto :goto_f
.end method

.method public setPriority(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 6
    .parameter "device"
    .parameter "priority"

    #@0
    .prologue
    .line 483
    const-string v0, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v1, "Need BLUETOOTH_ADMIN permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 485
    invoke-virtual {p0}, Lcom/android/bluetooth/hid/HidService;->getContentResolver()Landroid/content/ContentResolver;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@e
    move-result-object v1

    #@f
    invoke-static {v1}, Landroid/provider/Settings$Global;->getBluetoothInputDevicePriorityKey(Ljava/lang/String;)Ljava/lang/String;

    #@12
    move-result-object v1

    #@13
    invoke-static {v0, v1, p2}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    #@16
    .line 489
    const-string v0, "HidService"

    #@18
    new-instance v1, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v2, "Saved priority "

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    const-string v2, " = "

    #@29
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v1

    #@35
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 491
    const/4 v0, 0x1

    #@39
    return v0
.end method

.method setProtocolMode(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 7
    .parameter "device"
    .parameter "protocolMode"

    #@0
    .prologue
    .line 531
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v3, "Need BLUETOOTH_ADMIN permission"

    #@4
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 533
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@a
    move-result v1

    #@b
    .line 534
    .local v1, state:I
    const/4 v2, 0x2

    #@c
    if-eq v1, v2, :cond_10

    #@e
    .line 535
    const/4 v2, 0x0

    #@f
    .line 541
    :goto_f
    return v2

    #@10
    .line 537
    :cond_10
    iget-object v2, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@12
    const/4 v3, 0x7

    #@13
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@16
    move-result-object v0

    #@17
    .line 538
    .local v0, msg:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@19
    .line 539
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@1b
    .line 540
    iget-object v2, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@1d
    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@20
    .line 541
    const/4 v2, 0x1

    #@21
    goto :goto_f
.end method

.method setReport(Landroid/bluetooth/BluetoothDevice;BLjava/lang/String;)Z
    .registers 9
    .parameter "device"
    .parameter "reportType"
    .parameter "report"

    #@0
    .prologue
    .line 563
    const-string v3, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v4, "Need BLUETOOTH_ADMIN permission"

    #@4
    invoke-virtual {p0, v3, v4}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 565
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@a
    move-result v2

    #@b
    .line 566
    .local v2, state:I
    const/4 v3, 0x2

    #@c
    if-eq v2, v3, :cond_10

    #@e
    .line 567
    const/4 v3, 0x0

    #@f
    .line 576
    :goto_f
    return v3

    #@10
    .line 569
    :cond_10
    iget-object v3, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@12
    const/16 v4, 0xa

    #@14
    invoke-virtual {v3, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@17
    move-result-object v1

    #@18
    .line 570
    .local v1, msg:Landroid/os/Message;
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@1a
    .line 571
    new-instance v0, Landroid/os/Bundle;

    #@1c
    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    #@1f
    .line 572
    .local v0, data:Landroid/os/Bundle;
    const-string v3, "android.bluetooth.BluetoothInputDevice.extra.REPORT_TYPE"

    #@21
    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putByte(Ljava/lang/String;B)V

    #@24
    .line 573
    const-string v3, "android.bluetooth.BluetoothInputDevice.extra.REPORT"

    #@26
    invoke-virtual {v0, v3, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 574
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@2c
    .line 575
    iget-object v3, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@2e
    invoke-virtual {v3, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@31
    .line 576
    const/4 v3, 0x1

    #@32
    goto :goto_f
.end method

.method protected start()Z
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 88
    new-instance v0, Ljava/util/HashMap;

    #@3
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@6
    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/android/bluetooth/hid/HidService;->mInputDevices:Ljava/util/Map;

    #@c
    .line 89
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService;->initializeNative()V

    #@f
    .line 90
    iput-boolean v1, p0, Lcom/android/bluetooth/hid/HidService;->mNativeAvailable:Z

    #@11
    .line 91
    invoke-static {p0}, Lcom/android/bluetooth/hid/HidService;->setHidService(Lcom/android/bluetooth/hid/HidService;)V

    #@14
    .line 92
    return v1
.end method

.method protected stop()Z
    .registers 2

    #@0
    .prologue
    .line 97
    const-string v0, "Stopping Bluetooth HidService"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hid/HidService;->log(Ljava/lang/String;)V

    #@5
    .line 99
    const/4 v0, 0x1

    #@6
    return v0
.end method

.method virtualUnplug(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 519
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v3, "Need BLUETOOTH_ADMIN permission"

    #@4
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/hid/HidService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 521
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/hid/HidService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@a
    move-result v1

    #@b
    .line 522
    .local v1, state:I
    const/4 v2, 0x2

    #@c
    if-eq v1, v2, :cond_10

    #@e
    .line 523
    const/4 v2, 0x0

    #@f
    .line 527
    :goto_f
    return v2

    #@10
    .line 525
    :cond_10
    iget-object v2, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@12
    const/4 v3, 0x5

    #@13
    invoke-virtual {v2, v3, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@16
    move-result-object v0

    #@17
    .line 526
    .local v0, msg:Landroid/os/Message;
    iget-object v2, p0, Lcom/android/bluetooth/hid/HidService;->mHandler:Landroid/os/Handler;

    #@19
    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@1c
    .line 527
    const/4 v2, 0x1

    #@1d
    goto :goto_f
.end method
