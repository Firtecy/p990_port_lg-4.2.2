.class Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;
.super Landroid/bluetooth/IBluetoothInputDevice$Stub;
.source "HidService.java"

# interfaces
.implements Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/hid/HidService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BluetoothInputDeviceBinder"
.end annotation


# instance fields
.field private mService:Lcom/android/bluetooth/hid/HidService;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/hid/HidService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 311
    invoke-direct {p0}, Landroid/bluetooth/IBluetoothInputDevice$Stub;-><init>()V

    #@3
    .line 312
    iput-object p1, p0, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->mService:Lcom/android/bluetooth/hid/HidService;

    #@5
    .line 313
    return-void
.end method

.method private getService()Lcom/android/bluetooth/hid/HidService;
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 321
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_f

    #@7
    .line 322
    const-string v1, "HidService"

    #@9
    const-string v2, "InputDevice call not allowed for non-active user"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 329
    :cond_e
    :goto_e
    return-object v0

    #@f
    .line 326
    :cond_f
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->mService:Lcom/android/bluetooth/hid/HidService;

    #@11
    if-eqz v1, :cond_e

    #@13
    iget-object v1, p0, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->mService:Lcom/android/bluetooth/hid/HidService;

    #@15
    invoke-static {v1}, Lcom/android/bluetooth/hid/HidService;->access$1900(Lcom/android/bluetooth/hid/HidService;)Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_e

    #@1b
    .line 327
    iget-object v0, p0, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->mService:Lcom/android/bluetooth/hid/HidService;

    #@1d
    goto :goto_e
.end method


# virtual methods
.method public cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 316
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->mService:Lcom/android/bluetooth/hid/HidService;

    #@3
    .line 317
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public connect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 333
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 334
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 335
    const/4 v1, 0x0

    #@7
    .line 337
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hid/HidService;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public disconnect(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 341
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 342
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 343
    const/4 v1, 0x0

    #@7
    .line 345
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hid/HidService;->disconnect(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getConnectedDevices()Ljava/util/List;
    .registers 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 357
    const/4 v0, 0x1

    #@1
    new-array v0, v0, [I

    #@3
    const/4 v1, 0x0

    #@4
    const/4 v2, 0x2

    #@5
    aput v2, v0, v1

    #@7
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@a
    move-result-object v0

    #@b
    return-object v0
.end method

.method public getConnectionState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 349
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 350
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 351
    const/4 v1, 0x0

    #@7
    .line 353
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hid/HidService;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getDevicesMatchingConnectionStates([I)Ljava/util/List;
    .registers 5
    .parameter "states"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation

    #@0
    .prologue
    .line 362
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 363
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_d

    #@6
    .line 364
    new-instance v1, Ljava/util/ArrayList;

    #@8
    const/4 v2, 0x0

    #@9
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@c
    .line 366
    :goto_c
    return-object v1

    #@d
    :cond_d
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hid/HidService;->getDevicesMatchingConnectionStates([I)Ljava/util/List;

    #@10
    move-result-object v1

    #@11
    goto :goto_c
.end method

.method public getPriority(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 378
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 379
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 380
    const/4 v1, -0x1

    #@7
    .line 382
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hid/HidService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getProtocolMode(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 387
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 388
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 389
    const/4 v1, 0x0

    #@7
    .line 391
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hid/HidService;->getProtocolMode(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getReport(Landroid/bluetooth/BluetoothDevice;BBI)Z
    .registers 7
    .parameter "device"
    .parameter "reportType"
    .parameter "reportId"
    .parameter "bufferSize"

    #@0
    .prologue
    .line 411
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 412
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 413
    const/4 v1, 0x0

    #@7
    .line 415
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/bluetooth/hid/HidService;->getReport(Landroid/bluetooth/BluetoothDevice;BBI)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public sendData(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z
    .registers 5
    .parameter "device"
    .parameter "report"

    #@0
    .prologue
    .line 427
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 428
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 429
    const/4 v1, 0x0

    #@7
    .line 431
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hid/HidService;->sendData(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public setPriority(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 5
    .parameter "device"
    .parameter "priority"

    #@0
    .prologue
    .line 370
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 371
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 372
    const/4 v1, 0x0

    #@7
    .line 374
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hid/HidService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public setProtocolMode(Landroid/bluetooth/BluetoothDevice;I)Z
    .registers 5
    .parameter "device"
    .parameter "protocolMode"

    #@0
    .prologue
    .line 403
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 404
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 405
    const/4 v1, 0x0

    #@7
    .line 407
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hid/HidService;->setProtocolMode(Landroid/bluetooth/BluetoothDevice;I)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public setReport(Landroid/bluetooth/BluetoothDevice;BLjava/lang/String;)Z
    .registers 6
    .parameter "device"
    .parameter "reportType"
    .parameter "report"

    #@0
    .prologue
    .line 419
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 420
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 421
    const/4 v1, 0x0

    #@7
    .line 423
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/bluetooth/hid/HidService;->setReport(Landroid/bluetooth/BluetoothDevice;BLjava/lang/String;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public virtualUnplug(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 395
    invoke-direct {p0}, Lcom/android/bluetooth/hid/HidService$BluetoothInputDeviceBinder;->getService()Lcom/android/bluetooth/hid/HidService;

    #@3
    move-result-object v0

    #@4
    .line 396
    .local v0, service:Lcom/android/bluetooth/hid/HidService;
    if-nez v0, :cond_8

    #@6
    .line 397
    const/4 v1, 0x0

    #@7
    .line 399
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hid/HidService;->virtualUnplug(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method
