.class public final Lcom/android/bluetooth/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# static fields
.field static final BD_ADDR_LEN:I = 0x6

.field static final BD_UUID_LEN:I = 0x10

.field private static final TAG:Ljava/lang/String; = "BluetoothUtils"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 38
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static byteArrayToInt([B)I
    .registers 2
    .parameter "valueBuf"

    #@0
    .prologue
    .line 74
    const/4 v0, 0x0

    #@1
    invoke-static {p0, v0}, Lcom/android/bluetooth/Utils;->byteArrayToInt([BI)I

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public static byteArrayToInt([BI)I
    .registers 4
    .parameter "valueBuf"
    .parameter "offset"

    #@0
    .prologue
    .line 84
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    .line 85
    .local v0, converter:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@b
    .line 86
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->getInt(I)I

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public static byteArrayToShort([B)S
    .registers 3
    .parameter "valueBuf"

    #@0
    .prologue
    .line 78
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@3
    move-result-object v0

    #@4
    .line 79
    .local v0, converter:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@7
    move-result-object v1

    #@8
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@b
    .line 80
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getShort()S

    #@e
    move-result v1

    #@f
    return v1
.end method

.method public static byteArrayToUuid([B)[Landroid/os/ParcelUuid;
    .registers 12
    .parameter "val"

    #@0
    .prologue
    .line 128
    array-length v5, p0

    #@1
    div-int/lit8 v2, v5, 0x10

    #@3
    .line 129
    .local v2, numUuids:I
    new-array v4, v2, [Landroid/os/ParcelUuid;

    #@5
    .line 131
    .local v4, puuids:[Landroid/os/ParcelUuid;
    const/4 v3, 0x0

    #@6
    .line 133
    .local v3, offset:I
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    #@9
    move-result-object v0

    #@a
    .line 134
    .local v0, converter:Ljava/nio/ByteBuffer;
    sget-object v5, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    #@c
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@f
    .line 136
    const/4 v1, 0x0

    #@10
    .local v1, i:I
    :goto_10
    if-ge v1, v2, :cond_2d

    #@12
    .line 137
    new-instance v5, Landroid/os/ParcelUuid;

    #@14
    new-instance v6, Ljava/util/UUID;

    #@16
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->getLong(I)J

    #@19
    move-result-wide v7

    #@1a
    add-int/lit8 v9, v3, 0x8

    #@1c
    invoke-virtual {v0, v9}, Ljava/nio/ByteBuffer;->getLong(I)J

    #@1f
    move-result-wide v9

    #@20
    invoke-direct {v6, v7, v8, v9, v10}, Ljava/util/UUID;-><init>(JJ)V

    #@23
    invoke-direct {v5, v6}, Landroid/os/ParcelUuid;-><init>(Ljava/util/UUID;)V

    #@26
    aput-object v5, v4, v1

    #@28
    .line 139
    add-int/lit8 v3, v3, 0x10

    #@2a
    .line 136
    add-int/lit8 v1, v1, 0x1

    #@2c
    goto :goto_10

    #@2d
    .line 141
    :cond_2d
    return-object v4
.end method

.method public static checkCaller()Z
    .registers 9

    #@0
    .prologue
    .line 188
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    #@3
    move-result v0

    #@4
    .line 189
    .local v0, callingUser:I
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    #@7
    move-result-wide v3

    #@8
    .line 193
    .local v3, ident:J
    :try_start_8
    invoke-static {}, Landroid/app/ActivityManager;->getCurrentUser()I
    :try_end_b
    .catchall {:try_start_8 .. :try_end_b} :catchall_30
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_b} :catch_15

    #@b
    move-result v2

    #@c
    .line 194
    .local v2, foregroundUser:I
    if-ne v2, v0, :cond_13

    #@e
    const/4 v5, 0x1

    #@f
    .line 199
    .end local v2           #foregroundUser:I
    .local v5, ok:Z
    :goto_f
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@12
    .line 201
    return v5

    #@13
    .line 194
    .end local v5           #ok:Z
    .restart local v2       #foregroundUser:I
    :cond_13
    const/4 v5, 0x0

    #@14
    goto :goto_f

    #@15
    .line 195
    .end local v2           #foregroundUser:I
    :catch_15
    move-exception v1

    #@16
    .line 196
    .local v1, ex:Ljava/lang/Exception;
    :try_start_16
    const-string v6, "BluetoothUtils"

    #@18
    new-instance v7, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v8, "checkIfCallerIsSelfOrForegroundUser: Exception ex="

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v7

    #@27
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v7

    #@2b
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2e
    .catchall {:try_start_16 .. :try_end_2e} :catchall_30

    #@2e
    .line 197
    const/4 v5, 0x0

    #@2f
    .restart local v5       #ok:Z
    goto :goto_f

    #@30
    .line 199
    .end local v1           #ex:Ljava/lang/Exception;
    .end local v5           #ok:Z
    :catchall_30
    move-exception v6

    #@31
    invoke-static {v3, v4}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    #@34
    throw v6
.end method

.method public static copyStream(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .registers 6
    .parameter "is"
    .parameter "os"
    .parameter "bufferSize"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    #@0
    .prologue
    .line 155
    if-eqz p0, :cond_12

    #@2
    if-eqz p1, :cond_12

    #@4
    .line 156
    new-array v0, p2, [B

    #@6
    .line 157
    .local v0, buffer:[B
    const/4 v1, 0x0

    #@7
    .line 158
    .local v1, bytesRead:I
    :goto_7
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    #@a
    move-result v1

    #@b
    if-ltz v1, :cond_12

    #@d
    .line 159
    const/4 v2, 0x0

    #@e
    invoke-virtual {p1, v0, v2, v1}, Ljava/io/OutputStream;->write([BII)V

    #@11
    goto :goto_7

    #@12
    .line 162
    .end local v0           #buffer:[B
    .end local v1           #bytesRead:I
    :cond_12
    return-void
.end method

.method public static debugGetAdapterStateString(I)Ljava/lang/String;
    .registers 2
    .parameter "state"

    #@0
    .prologue
    .line 145
    packed-switch p0, :pswitch_data_12

    #@3
    .line 150
    const-string v0, "UNKNOWN"

    #@5
    :goto_5
    return-object v0

    #@6
    .line 146
    :pswitch_6
    const-string v0, "STATE_OFF"

    #@8
    goto :goto_5

    #@9
    .line 147
    :pswitch_9
    const-string v0, "STATE_ON"

    #@b
    goto :goto_5

    #@c
    .line 148
    :pswitch_c
    const-string v0, "STATE_TURNING_ON"

    #@e
    goto :goto_5

    #@f
    .line 149
    :pswitch_f
    const-string v0, "STATE_TURNING_OFF"

    #@11
    goto :goto_5

    #@12
    .line 145
    :pswitch_data_12
    .packed-switch 0xa
        :pswitch_6
        :pswitch_c
        :pswitch_9
        :pswitch_f
    .end packed-switch
.end method

.method public static getAddressStringFromByte([B)Ljava/lang/String;
    .registers 9
    .parameter "address"

    #@0
    .prologue
    const/4 v7, 0x4

    #@1
    const/4 v6, 0x3

    #@2
    const/4 v5, 0x2

    #@3
    const/4 v4, 0x1

    #@4
    const/4 v3, 0x0

    #@5
    .line 44
    if-eqz p0, :cond_b

    #@7
    array-length v0, p0

    #@8
    const/4 v1, 0x6

    #@9
    if-eq v0, v1, :cond_d

    #@b
    .line 45
    :cond_b
    const/4 v0, 0x0

    #@c
    .line 48
    :goto_c
    return-object v0

    #@d
    :cond_d
    const-string v0, "%02X:%02X:%02X:%02X:%02X:%02X"

    #@f
    const/4 v1, 0x6

    #@10
    new-array v1, v1, [Ljava/lang/Object;

    #@12
    aget-byte v2, p0, v3

    #@14
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@17
    move-result-object v2

    #@18
    aput-object v2, v1, v3

    #@1a
    aget-byte v2, p0, v4

    #@1c
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@1f
    move-result-object v2

    #@20
    aput-object v2, v1, v4

    #@22
    aget-byte v2, p0, v5

    #@24
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@27
    move-result-object v2

    #@28
    aput-object v2, v1, v5

    #@2a
    aget-byte v2, p0, v6

    #@2c
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@2f
    move-result-object v2

    #@30
    aput-object v2, v1, v6

    #@32
    aget-byte v2, p0, v7

    #@34
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@37
    move-result-object v2

    #@38
    aput-object v2, v1, v7

    #@3a
    const/4 v2, 0x5

    #@3b
    const/4 v3, 0x5

    #@3c
    aget-byte v3, p0, v3

    #@3e
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    #@41
    move-result-object v3

    #@42
    aput-object v3, v1, v2

    #@44
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    #@47
    move-result-object v0

    #@48
    goto :goto_c
.end method

.method public static getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B
    .registers 2
    .parameter "device"

    #@0
    .prologue
    .line 54
    invoke-virtual {p0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    invoke-static {v0}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method public static getBytesFromAddress(Ljava/lang/String;)[B
    .registers 6
    .parameter "address"

    #@0
    .prologue
    .line 59
    const/4 v1, 0x0

    #@1
    .line 60
    .local v1, j:I
    const/4 v3, 0x6

    #@2
    new-array v2, v3, [B

    #@4
    .line 62
    .local v2, output:[B
    const/4 v0, 0x0

    #@5
    .local v0, i:I
    :goto_5
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@8
    move-result v3

    #@9
    if-ge v0, v3, :cond_29

    #@b
    .line 63
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    #@e
    move-result v3

    #@f
    const/16 v4, 0x3a

    #@11
    if-eq v3, v4, :cond_26

    #@13
    .line 64
    add-int/lit8 v3, v0, 0x2

    #@15
    invoke-virtual {p0, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@18
    move-result-object v3

    #@19
    const/16 v4, 0x10

    #@1b
    invoke-static {v3, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    #@1e
    move-result v3

    #@1f
    int-to-byte v3, v3

    #@20
    aput-byte v3, v2, v1

    #@22
    .line 65
    add-int/lit8 v1, v1, 0x1

    #@24
    .line 66
    add-int/lit8 v0, v0, 0x1

    #@26
    .line 62
    :cond_26
    add-int/lit8 v0, v0, 0x1

    #@28
    goto :goto_5

    #@29
    .line 70
    :cond_29
    return-object v2
.end method

.method public static intToByteArray(I)[B
    .registers 3
    .parameter "value"

    #@0
    .prologue
    .line 90
    const/4 v1, 0x4

    #@1
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@4
    move-result-object v0

    #@5
    .line 91
    .local v0, converter:Ljava/nio/ByteBuffer;
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    #@8
    move-result-object v1

    #@9
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@c
    .line 92
    invoke-virtual {v0, p0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    #@f
    .line 93
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@12
    move-result-object v1

    #@13
    return-object v1
.end method

.method public static safeCloseStream(Ljava/io/InputStream;)V
    .registers 4
    .parameter "is"

    #@0
    .prologue
    .line 165
    if-eqz p0, :cond_5

    #@2
    .line 167
    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_5} :catch_6

    #@5
    .line 172
    :cond_5
    :goto_5
    return-void

    #@6
    .line 168
    :catch_6
    move-exception v0

    #@7
    .line 169
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "BluetoothUtils"

    #@9
    const-string v2, "Error closing stream"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method

.method public static safeCloseStream(Ljava/io/OutputStream;)V
    .registers 4
    .parameter "os"

    #@0
    .prologue
    .line 175
    if-eqz p0, :cond_5

    #@2
    .line 177
    :try_start_2
    invoke-virtual {p0}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_5} :catch_6

    #@5
    .line 182
    :cond_5
    :goto_5
    return-void

    #@6
    .line 178
    :catch_6
    move-exception v0

    #@7
    .line 179
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "BluetoothUtils"

    #@9
    const-string v2, "Error closing stream"

    #@b
    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@e
    goto :goto_5
.end method

.method public static uuidToByteArray(Landroid/os/ParcelUuid;)[B
    .registers 9
    .parameter "pUuid"

    #@0
    .prologue
    .line 97
    const/16 v1, 0x10

    #@2
    .line 98
    .local v1, length:I
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@5
    move-result-object v0

    #@6
    .line 99
    .local v0, converter:Ljava/nio/ByteBuffer;
    sget-object v7, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    #@8
    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@b
    .line 102
    invoke-virtual {p0}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@e
    move-result-object v6

    #@f
    .line 103
    .local v6, uuid:Ljava/util/UUID;
    invoke-virtual {v6}, Ljava/util/UUID;->getMostSignificantBits()J

    #@12
    move-result-wide v4

    #@13
    .line 104
    .local v4, msb:J
    invoke-virtual {v6}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@16
    move-result-wide v2

    #@17
    .line 105
    .local v2, lsb:J
    invoke-virtual {v0, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    #@1a
    .line 106
    const/16 v7, 0x8

    #@1c
    invoke-virtual {v0, v7, v2, v3}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    #@1f
    .line 107
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@22
    move-result-object v7

    #@23
    return-object v7
.end method

.method public static uuidsToByteArray([Landroid/os/ParcelUuid;)[B
    .registers 10
    .parameter "uuids"

    #@0
    .prologue
    .line 111
    array-length v8, p0

    #@1
    mul-int/lit8 v2, v8, 0x10

    #@3
    .line 112
    .local v2, length:I
    invoke-static {v2}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    #@6
    move-result-object v0

    #@7
    .line 113
    .local v0, converter:Ljava/nio/ByteBuffer;
    sget-object v8, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    #@9
    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    #@c
    .line 117
    const/4 v1, 0x0

    #@d
    .local v1, i:I
    :goto_d
    array-length v8, p0

    #@e
    if-ge v1, v8, :cond_2d

    #@10
    .line 118
    aget-object v8, p0, v1

    #@12
    invoke-virtual {v8}, Landroid/os/ParcelUuid;->getUuid()Ljava/util/UUID;

    #@15
    move-result-object v7

    #@16
    .line 119
    .local v7, uuid:Ljava/util/UUID;
    invoke-virtual {v7}, Ljava/util/UUID;->getMostSignificantBits()J

    #@19
    move-result-wide v5

    #@1a
    .line 120
    .local v5, msb:J
    invoke-virtual {v7}, Ljava/util/UUID;->getLeastSignificantBits()J

    #@1d
    move-result-wide v3

    #@1e
    .line 121
    .local v3, lsb:J
    mul-int/lit8 v8, v1, 0x10

    #@20
    invoke-virtual {v0, v8, v5, v6}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    #@23
    .line 122
    mul-int/lit8 v8, v1, 0x10

    #@25
    add-int/lit8 v8, v8, 0x8

    #@27
    invoke-virtual {v0, v8, v3, v4}, Ljava/nio/ByteBuffer;->putLong(IJ)Ljava/nio/ByteBuffer;

    #@2a
    .line 117
    add-int/lit8 v1, v1, 0x1

    #@2c
    goto :goto_d

    #@2d
    .line 124
    .end local v3           #lsb:J
    .end local v5           #msb:J
    .end local v7           #uuid:Ljava/util/UUID;
    :cond_2d
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    #@30
    move-result-object v8

    #@31
    return-object v8
.end method
