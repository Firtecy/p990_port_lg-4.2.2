.class public final Lcom/android/bluetooth/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ack_record:I = 0x7f0b002e

.field public static final ack_text:I = 0x7f0b002d

.field public static final address_text:I = 0x7f0b0027

.field public static final appIcon:I = 0x7f0b0021

.field public static final bluetooth_opp_remember_choice:I = 0x7f0b000b

.field public static final browse:I = 0x7f0b000d

.field public static final cancel:I = 0x7f0b0013

.field public static final complete_date:I = 0x7f0b0005

.field public static final complete_text:I = 0x7f0b0006

.field public static final content:I = 0x7f0b000a

.field public static final defaultContactName:I = 0x7f0b000c

.field public static final deleteAll_record:I = 0x7f0b002f

.field public static final delete_record:I = 0x7f0b002a

.field public static final delete_text:I = 0x7f0b0029

.field public static final description:I = 0x7f0b0023

.field public static final dup_action:I = 0x7f0b0010

.field public static final dup_action_add_as_new:I = 0x7f0b0014

.field public static final dup_action_replace:I = 0x7f0b0016

.field public static final dup_icon:I = 0x7f0b000e

.field public static final dup_menu_add_all:I = 0x7f0b0032

.field public static final dup_menu_ignore_all:I = 0x7f0b0034

.field public static final dup_menu_update_all:I = 0x7f0b0033

.field public static final dup_title:I = 0x7f0b000f

.field public static final empty:I = 0x7f0b0008

.field public static final existingFileName:I = 0x7f0b0017

.field public static final insert_record:I = 0x7f0b0028

.field public static final insert_text:I = 0x7f0b0025

.field public static final line1_view:I = 0x7f0b0018

.field public static final line2_view:I = 0x7f0b0019

.field public static final line3_view:I = 0x7f0b001a

.field public static final line4_view:I = 0x7f0b001b

.field public static final line5_view:I = 0x7f0b001c

.field public static final list:I = 0x7f0b0007

.field public static final media_text:I = 0x7f0b0026

.field public static final menu:I = 0x7f0b0015

.field public static final message:I = 0x7f0b0000

.field public static final no_duplicates_view:I = 0x7f0b001f

.field public static final no_transfers_view:I = 0x7f0b0020

.field public static final notify_server:I = 0x7f0b0031

.field public static final progress_bar:I = 0x7f0b0024

.field public static final progress_info:I = 0x7f0b0009

.field public static final progress_percent:I = 0x7f0b001d

.field public static final progress_text:I = 0x7f0b0022

.field public static final progress_transfer:I = 0x7f0b001e

.field public static final prompt:I = 0x7f0b0011

.field public static final save:I = 0x7f0b0012

.field public static final start_server:I = 0x7f0b0030

.field public static final targetdevice:I = 0x7f0b0004

.field public static final text:I = 0x7f0b0001

.field public static final transfer_icon:I = 0x7f0b0002

.field public static final transfer_menu_clear:I = 0x7f0b0037

.field public static final transfer_menu_clear_all:I = 0x7f0b0036

.field public static final transfer_menu_open:I = 0x7f0b0035

.field public static final transfer_title:I = 0x7f0b0003

.field public static final update_record:I = 0x7f0b002c

.field public static final update_text:I = 0x7f0b002b


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 96
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
