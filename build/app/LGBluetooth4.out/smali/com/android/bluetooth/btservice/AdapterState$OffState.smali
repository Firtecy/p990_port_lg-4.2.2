.class Lcom/android/bluetooth/btservice/AdapterState$OffState;
.super Lcom/android/internal/util/State;
.source "AdapterState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/btservice/AdapterState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OffState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/btservice/AdapterState;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/btservice/AdapterState;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 154
    iput-object p1, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/bluetooth/btservice/AdapterState$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 154
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/AdapterState$OffState;-><init>(Lcom/android/bluetooth/btservice/AdapterState;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 3

    #@0
    .prologue
    .line 157
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2
    const-string v1, "Entering OffState"

    #@4
    invoke-static {v0, v1}, Lcom/android/bluetooth/btservice/AdapterState;->access$300(Lcom/android/bluetooth/btservice/AdapterState;Ljava/lang/String;)V

    #@7
    .line 158
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 163
    iget v2, p1, Landroid/os/Message;->what:I

    #@3
    sparse-switch v2, :sswitch_data_de

    #@6
    .line 219
    const-string v1, "BluetoothAdapterState"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "ERROR: UNEXPECTED MESSAGE: CURRENT_STATE=OFF, MESSAGE = "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    iget v3, p1, Landroid/os/Message;->what:I

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 221
    const/4 v1, 0x0

    #@21
    .line 223
    :goto_21
    return v1

    #@22
    .line 166
    :sswitch_22
    const-string v2, "BluetoothAdapterState"

    #@24
    const-string v3, "CURRENT_STATE=OFF, MESSAGE = USER_TURN_ON"

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 168
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2b
    const/16 v3, 0xb

    #@2d
    invoke-static {v2, v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@30
    .line 169
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@32
    invoke-static {v2}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@35
    move-result-object v2

    #@36
    invoke-virtual {v2, v1}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOn(Z)V

    #@39
    .line 170
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@3b
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@3d
    invoke-static {v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@40
    move-result-object v3

    #@41
    invoke-static {v2, v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$600(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@44
    .line 171
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@46
    const/16 v3, 0x64

    #@48
    const-wide/16 v4, 0x1388

    #@4a
    invoke-virtual {v2, v3, v4, v5}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessageDelayed(IJ)V

    #@4d
    .line 172
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4f
    invoke-static {v2}, Lcom/android/bluetooth/btservice/AdapterState;->access$700(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterService;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v2}, Lcom/android/bluetooth/btservice/AdapterService;->processStart()V

    #@56
    goto :goto_21

    #@57
    .line 177
    :sswitch_57
    const-string v2, "BluetoothAdapterState"

    #@59
    const-string v3, "CURRENT_STATE=OFF, MESSAGE = USER_TURN_ON_RADIO"

    #@5b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 179
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@60
    invoke-static {v2}, Lcom/android/bluetooth/btservice/AdapterState;->access$800(Lcom/android/bluetooth/btservice/AdapterState;)Z

    #@63
    move-result v2

    #@64
    if-nez v2, :cond_82

    #@66
    .line 181
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@68
    invoke-static {v2}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v2, v1}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOnRadio(Z)V

    #@6f
    .line 182
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@71
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@73
    invoke-static {v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@76
    move-result-object v3

    #@77
    invoke-static {v2, v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$900(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@7a
    .line 183
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@7c
    const/16 v3, 0xc9

    #@7e
    invoke-virtual {v2, v3}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(I)V

    #@81
    goto :goto_21

    #@82
    .line 185
    :cond_82
    const-string v2, "BluetoothAdapterState"

    #@84
    const-string v3, "Radio already turned ON"

    #@86
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    goto :goto_21

    #@8a
    .line 192
    :sswitch_8a
    const-string v2, "BluetoothAdapterState"

    #@8c
    const-string v3, "CURRENT_STATE=OFF, MESSAGE = USER_TURN_OFF"

    #@8e
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    goto :goto_21

    #@92
    .line 199
    :sswitch_92
    const-string v2, "BluetoothAdapterState"

    #@94
    new-instance v3, Ljava/lang/StringBuilder;

    #@96
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@99
    const-string v4, "CURRENT_STATE=OFF, MESSAGE = USER_TURN_OFF_RADIO, requestId= "

    #@9b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v3

    #@9f
    iget v4, p1, Landroid/os/Message;->arg1:I

    #@a1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v3

    #@a5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a8
    move-result-object v3

    #@a9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 202
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@ae
    invoke-static {v2}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@b1
    move-result-object v2

    #@b2
    invoke-virtual {v2, v1}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOffRadio(Z)V

    #@b5
    .line 203
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@b7
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@b9
    invoke-static {v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@bc
    move-result-object v3

    #@bd
    invoke-static {v2, v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$1000(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@c0
    .line 205
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@c2
    const/16 v3, 0xcd

    #@c4
    invoke-virtual {v2, v3}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@c7
    move-result-object v0

    #@c8
    .line 207
    .local v0, m:Landroid/os/Message;
    if-eqz v0, :cond_d5

    #@ca
    .line 208
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@cc
    iput v2, v0, Landroid/os/Message;->arg1:I

    #@ce
    .line 209
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterState$OffState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@d0
    invoke-virtual {v2, v0}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V

    #@d3
    goto/16 :goto_21

    #@d5
    .line 211
    :cond_d5
    const-string v2, "BluetoothAdapterState"

    #@d7
    const-string v3, "processMessage(): m is null."

    #@d9
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@dc
    goto/16 :goto_21

    #@de
    .line 163
    :sswitch_data_de
    .sparse-switch
        0x1 -> :sswitch_22
        0x14 -> :sswitch_8a
        0xca -> :sswitch_57
        0xcb -> :sswitch_92
    .end sparse-switch
.end method
