.class Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;
.super Lcom/android/internal/util/State;
.source "BondStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/btservice/BondStateMachine;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PendingCommandState"
.end annotation


# instance fields
.field private final mDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/android/bluetooth/btservice/BondStateMachine;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/btservice/BondStateMachine;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 152
    iput-object p1, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    .line 153
    new-instance v0, Ljava/util/ArrayList;

    #@7
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@a
    iput-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->mDevices:Ljava/util/ArrayList;

    #@c
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/btservice/BondStateMachine;Lcom/android/bluetooth/btservice/BondStateMachine$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 152
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;-><init>(Lcom/android/bluetooth/btservice/BondStateMachine;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 4

    #@0
    .prologue
    .line 158
    iget-object v1, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@2
    const-string v2, "Entering PendingCommandState State"

    #@4
    invoke-static {v1, v2}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$200(Lcom/android/bluetooth/btservice/BondStateMachine;Ljava/lang/String;)V

    #@7
    .line 159
    iget-object v1, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@9
    invoke-static {v1}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$800(Lcom/android/bluetooth/btservice/BondStateMachine;)Landroid/os/Message;

    #@c
    move-result-object v1

    #@d
    iget-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@f
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@11
    .line 160
    .local v0, dev:Landroid/bluetooth/BluetoothDevice;
    return-void
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 10
    .parameter "msg"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    .line 165
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@4
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@6
    .line 166
    .local v0, dev:Landroid/bluetooth/BluetoothDevice;
    const/4 v3, 0x0

    #@7
    .line 167
    .local v3, result:Z
    iget-object v6, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->mDevices:Ljava/util/ArrayList;

    #@9
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@c
    move-result v6

    #@d
    if-eqz v6, :cond_1f

    #@f
    iget v6, p1, Landroid/os/Message;->what:I

    #@11
    const/4 v7, 0x2

    #@12
    if-eq v6, v7, :cond_1f

    #@14
    iget v6, p1, Landroid/os/Message;->what:I

    #@16
    const/4 v7, 0x4

    #@17
    if-eq v6, v7, :cond_1f

    #@19
    .line 169
    iget-object v5, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@1b
    invoke-static {v5, p1}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$900(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/os/Message;)V

    #@1e
    .line 222
    :cond_1e
    :goto_1e
    return v4

    #@1f
    .line 173
    :cond_1f
    iget v6, p1, Landroid/os/Message;->what:I

    #@21
    packed-switch v6, :pswitch_data_ac

    #@24
    .line 215
    const-string v4, "BluetoothBondStateMachine"

    #@26
    new-instance v6, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v7, "Received unhandled event:"

    #@2d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v6

    #@31
    iget v7, p1, Landroid/os/Message;->what:I

    #@33
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v6

    #@37
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v6

    #@3b
    invoke-static {v4, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@3e
    move v4, v5

    #@3f
    .line 216
    goto :goto_1e

    #@40
    .line 175
    :pswitch_40
    iget-object v6, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@42
    invoke-static {v6, v0, v5}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$300(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;Z)Z

    #@45
    move-result v3

    #@46
    .line 218
    :cond_46
    :goto_46
    if-eqz v3, :cond_1e

    #@48
    .line 219
    iget-object v5, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->mDevices:Ljava/util/ArrayList;

    #@4a
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@4d
    goto :goto_1e

    #@4e
    .line 178
    :pswitch_4e
    iget-object v6, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@50
    invoke-static {v6, v0, v5}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$400(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;Z)Z

    #@53
    move-result v3

    #@54
    .line 179
    goto :goto_46

    #@55
    .line 181
    :pswitch_55
    iget-object v5, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@57
    invoke-static {v5, v0}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$1000(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;)Z

    #@5a
    move-result v3

    #@5b
    .line 182
    goto :goto_46

    #@5c
    .line 184
    :pswitch_5c
    iget v1, p1, Landroid/os/Message;->arg1:I

    #@5e
    .line 185
    .local v1, newState:I
    iget-object v6, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@60
    iget v7, p1, Landroid/os/Message;->arg2:I

    #@62
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$1100(Lcom/android/bluetooth/btservice/BondStateMachine;I)I

    #@65
    move-result v2

    #@66
    .line 186
    .local v2, reason:I
    iget-object v6, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@68
    invoke-static {v6, v0, v1, v2}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$500(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;II)V

    #@6b
    .line 187
    const/16 v6, 0xb

    #@6d
    if-eq v1, v6, :cond_a2

    #@6f
    .line 190
    iget-object v6, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->mDevices:Ljava/util/ArrayList;

    #@71
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@74
    move-result v6

    #@75
    if-nez v6, :cond_96

    #@77
    move v3, v4

    #@78
    .line 191
    :goto_78
    iget-object v5, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->mDevices:Ljava/util/ArrayList;

    #@7a
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    #@7d
    move-result v5

    #@7e
    if-eqz v5, :cond_8c

    #@80
    .line 196
    const/4 v3, 0x0

    #@81
    .line 197
    iget-object v5, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@83
    iget-object v6, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@85
    invoke-static {v6}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$1200(Lcom/android/bluetooth/btservice/BondStateMachine;)Lcom/android/bluetooth/btservice/BondStateMachine$StableState;

    #@88
    move-result-object v6

    #@89
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$1300(Lcom/android/bluetooth/btservice/BondStateMachine;Lcom/android/internal/util/IState;)V

    #@8c
    .line 199
    :cond_8c
    const/16 v5, 0xa

    #@8e
    if-ne v1, v5, :cond_98

    #@90
    .line 202
    iget-object v5, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@92
    invoke-static {v5, v0}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$1400(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;)V

    #@95
    goto :goto_46

    #@96
    :cond_96
    move v3, v5

    #@97
    .line 190
    goto :goto_78

    #@98
    .line 204
    :cond_98
    const/16 v5, 0xc

    #@9a
    if-ne v1, v5, :cond_46

    #@9c
    .line 207
    iget-object v5, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@9e
    invoke-static {v5, v0}, Lcom/android/bluetooth/btservice/BondStateMachine;->access$1500(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;)V

    #@a1
    goto :goto_46

    #@a2
    .line 210
    :cond_a2
    iget-object v5, p0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;->mDevices:Ljava/util/ArrayList;

    #@a4
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@a7
    move-result v5

    #@a8
    if-nez v5, :cond_46

    #@aa
    .line 211
    const/4 v3, 0x1

    #@ab
    goto :goto_46

    #@ac
    .line 173
    :pswitch_data_ac
    .packed-switch 0x1
        :pswitch_40
        :pswitch_55
        :pswitch_4e
        :pswitch_5c
    .end packed-switch
.end method
