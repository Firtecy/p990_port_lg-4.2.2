.class Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;
.super Landroid/database/ContentObserver;
.source "AdapterService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/btservice/AdapterService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SettingsDBUpdateObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/btservice/AdapterService;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/btservice/AdapterService;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 159
    iput-object p1, p0, Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    .line 160
    new-instance v0, Landroid/os/Handler;

    #@4
    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    #@7
    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    #@a
    .line 161
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .registers 6
    .parameter "selfChange"

    #@0
    .prologue
    .line 165
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    invoke-virtual {v1}, Lcom/android/bluetooth/btservice/AdapterService;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v1

    #@6
    const-string v2, "lg_device_name"

    #@8
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@b
    move-result-object v0

    #@c
    .line 166
    .local v0, device_name:Ljava/lang/String;
    const-string v1, "BluetoothAdapterService"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "[BTUI] lg device name changed : "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 167
    if-eqz v0, :cond_3f

    #@26
    .line 168
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@28
    invoke-static {v1}, Lcom/android/bluetooth/btservice/AdapterService;->access$000(Lcom/android/bluetooth/btservice/AdapterService;)Lcom/android/bluetooth/btservice/AdapterProperties;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Lcom/android/bluetooth/btservice/AdapterProperties;->getName()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@33
    move-result v1

    #@34
    if-nez v1, :cond_3f

    #@36
    .line 169
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@38
    invoke-static {v1}, Lcom/android/bluetooth/btservice/AdapterService;->access$000(Lcom/android/bluetooth/btservice/AdapterService;)Lcom/android/bluetooth/btservice/AdapterProperties;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->setNameWithoutDBUpdate(Ljava/lang/String;)Z

    #@3f
    .line 172
    :cond_3f
    return-void
.end method

.method public register(Landroid/content/ContentResolver;)V
    .registers 4
    .parameter "contentResolver"

    #@0
    .prologue
    .line 174
    const-string v0, "BluetoothAdapterService"

    #@2
    const-string v1, "[BTUI] register observer for lg device name DB"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 175
    const-string v0, "lg_device_name"

    #@9
    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    #@c
    move-result-object v0

    #@d
    const/4 v1, 0x0

    #@e
    invoke-virtual {p1, v0, v1, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    #@11
    .line 176
    return-void
.end method

.method public unregister(Landroid/content/ContentResolver;)V
    .registers 4
    .parameter "contentResolver"

    #@0
    .prologue
    .line 179
    const-string v0, "BluetoothAdapterService"

    #@2
    const-string v1, "[BTUI] unregister observer for lg device name DB"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 180
    invoke-virtual {p1, p0}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    #@a
    .line 181
    return-void
.end method
