.class Lcom/android/bluetooth/btservice/AdapterState$OnState;
.super Lcom/android/internal/util/State;
.source "AdapterState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/btservice/AdapterState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnState"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/btservice/AdapterState;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/btservice/AdapterState;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 230
    iput-object p1, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/bluetooth/btservice/AdapterState$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 230
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/AdapterState$OnState;-><init>(Lcom/android/bluetooth/btservice/AdapterState;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 4

    #@0
    .prologue
    .line 233
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2
    const-string v2, "Entering On State"

    #@4
    #calls: Lcom/android/bluetooth/btservice/AdapterState;->infoLog(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/android/bluetooth/btservice/AdapterState;->access$300(Lcom/android/bluetooth/btservice/AdapterState;Ljava/lang/String;)V

    #@7
    .line 236
    const-string v1, "LGBT_COMMON_SCENARIO_DEVICE_NAME_SYNC"

    #@9
    invoke-static {v1}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_3c

    #@f
    .line 237
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@11
    invoke-static {v1}, Lcom/android/bluetooth/btservice/AdapterState;->access$700(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterService;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Lcom/android/bluetooth/btservice/AdapterService;->getContentResolver()Landroid/content/ContentResolver;

    #@18
    move-result-object v1

    #@19
    const-string v2, "lg_device_name"

    #@1b
    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v0

    #@1f
    .line 238
    .local v0, device_name:Ljava/lang/String;
    if-nez v0, :cond_23

    #@21
    .line 239
    const-string v0, "Optimus"

    #@23
    .line 242
    :cond_23
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@25
    invoke-static {v1}, Lcom/android/bluetooth/btservice/AdapterState;->access$1100(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterProperties;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Lcom/android/bluetooth/btservice/AdapterProperties;->getName()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@30
    move-result v1

    #@31
    if-nez v1, :cond_3c

    #@33
    .line 243
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@35
    invoke-static {v1}, Lcom/android/bluetooth/btservice/AdapterState;->access$1100(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterProperties;

    #@38
    move-result-object v1

    #@39
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->setNameWithoutDBUpdate(Ljava/lang/String;)Z

    #@3c
    .line 254
    .end local v0           #device_name:Ljava/lang/String;
    :cond_3c
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@3e
    invoke-static {v1}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@41
    move-result-object v1

    #@42
    invoke-virtual {v1}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->isTurningOnRadio()Z

    #@45
    move-result v1

    #@46
    if-eqz v1, :cond_53

    #@48
    .line 255
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4a
    invoke-static {v1}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@4d
    move-result-object v1

    #@4e
    const/4 v2, 0x0

    #@4f
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOnRadio(Z)V

    #@52
    .line 260
    :goto_52
    return-void

    #@53
    .line 257
    :cond_53
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@55
    invoke-static {v1}, Lcom/android/bluetooth/btservice/AdapterState;->access$700(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterService;

    #@58
    move-result-object v1

    #@59
    invoke-virtual {v1}, Lcom/android/bluetooth/btservice/AdapterService;->autoConnect()V

    #@5c
    goto :goto_52
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 8
    .parameter "msg"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 265
    iget v3, p1, Landroid/os/Message;->what:I

    #@3
    sparse-switch v3, :sswitch_data_ea

    #@6
    .line 329
    const-string v2, "BluetoothAdapterState"

    #@8
    new-instance v3, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v4, "ERROR: UNEXPECTED MESSAGE: CURRENT_STATE=ON, MESSAGE = "

    #@f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    iget v4, p1, Landroid/os/Message;->what:I

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 331
    const/4 v2, 0x0

    #@21
    .line 333
    :goto_21
    return v2

    #@22
    .line 268
    :sswitch_22
    const-string v3, "BluetoothAdapterState"

    #@24
    const-string v4, "CURRENT_STATE=ON, MESSAGE = USER_TURN_OFF"

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 270
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2b
    const/16 v4, 0xd

    #@2d
    #calls: Lcom/android/bluetooth/btservice/AdapterState;->notifyAdapterStateChange(I)V
    invoke-static {v3, v4}, Lcom/android/bluetooth/btservice/AdapterState;->access$400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@30
    .line 271
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@32
    invoke-static {v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@35
    move-result-object v3

    #@36
    invoke-virtual {v3, v2}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOff(Z)V

    #@39
    .line 272
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@3b
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@3d
    invoke-static {v4}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@40
    move-result-object v4

    #@41
    invoke-static {v3, v4}, Lcom/android/bluetooth/btservice/AdapterState;->access$1200(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@44
    .line 276
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@46
    const/16 v4, 0x69

    #@48
    invoke-virtual {v3, v4}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@4b
    move-result-object v0

    #@4c
    .line 277
    .local v0, m:Landroid/os/Message;
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4e
    const-wide/16 v4, 0x7d0

    #@50
    invoke-virtual {v3, v0, v4, v5}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessageDelayed(Landroid/os/Message;J)V

    #@53
    .line 278
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@55
    invoke-static {v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$1100(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterProperties;

    #@58
    move-result-object v3

    #@59
    invoke-virtual {v3}, Lcom/android/bluetooth/btservice/AdapterProperties;->onBluetoothDisable()V

    #@5c
    goto :goto_21

    #@5d
    .line 284
    .end local v0           #m:Landroid/os/Message;
    :sswitch_5d
    const-string v3, "BluetoothAdapterState"

    #@5f
    const-string v4, "CURRENT_STATE=ON, MESSAGE = USER_TURN_OFF_RADIO"

    #@61
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 286
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@66
    invoke-static {v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$800(Lcom/android/bluetooth/btservice/AdapterState;)Z

    #@69
    move-result v3

    #@6a
    if-eqz v3, :cond_9c

    #@6c
    .line 287
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@6e
    invoke-static {v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@71
    move-result-object v3

    #@72
    invoke-virtual {v3, v2}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOffRadio(Z)V

    #@75
    .line 288
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@77
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@79
    invoke-static {v4}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@7c
    move-result-object v4

    #@7d
    invoke-static {v3, v4}, Lcom/android/bluetooth/btservice/AdapterState;->access$1300(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@80
    .line 290
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@82
    const/16 v4, 0xcd

    #@84
    invoke-virtual {v3, v4}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@87
    move-result-object v1

    #@88
    .line 292
    .local v1, m1:Landroid/os/Message;
    if-eqz v1, :cond_94

    #@8a
    .line 293
    iget v3, p1, Landroid/os/Message;->arg1:I

    #@8c
    iput v3, v1, Landroid/os/Message;->arg1:I

    #@8e
    .line 294
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@90
    invoke-virtual {v3, v1}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V

    #@93
    goto :goto_21

    #@94
    .line 296
    :cond_94
    const-string v3, "BluetoothAdapterState"

    #@96
    const-string v4, "processMessage(): m1 is null."

    #@98
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9b
    goto :goto_21

    #@9c
    .line 300
    .end local v1           #m1:Landroid/os/Message;
    :cond_9c
    const-string v3, "BluetoothAdapterState"

    #@9e
    const-string v4, "Radio already turned OFF"

    #@a0
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    goto/16 :goto_21

    #@a5
    .line 307
    :sswitch_a5
    const-string v3, "BluetoothAdapterState"

    #@a7
    const-string v4, "CURRENT_STATE=ON, MESSAGE = USER_TURN_ON"

    #@a9
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ac
    .line 309
    const-string v3, "BluetoothAdapterState"

    #@ae
    const-string v4, "Bluetooth already ON, ignoring USER_TURN_ON"

    #@b0
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b3
    goto/16 :goto_21

    #@b5
    .line 314
    :sswitch_b5
    const-string v3, "BluetoothAdapterState"

    #@b7
    const-string v4, "CURRENT_STATE=ON, MESSAGE = USER_TURN_ON_RADIO"

    #@b9
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@bc
    .line 316
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@be
    invoke-static {v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$800(Lcom/android/bluetooth/btservice/AdapterState;)Z

    #@c1
    move-result v3

    #@c2
    if-nez v3, :cond_e1

    #@c4
    .line 318
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@c6
    invoke-static {v3}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@c9
    move-result-object v3

    #@ca
    invoke-virtual {v3, v2}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOnRadio(Z)V

    #@cd
    .line 319
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@cf
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@d1
    invoke-static {v4}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@d4
    move-result-object v4

    #@d5
    invoke-static {v3, v4}, Lcom/android/bluetooth/btservice/AdapterState;->access$1400(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@d8
    .line 320
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterState$OnState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@da
    const/16 v4, 0xc9

    #@dc
    invoke-virtual {v3, v4}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(I)V

    #@df
    goto/16 :goto_21

    #@e1
    .line 322
    :cond_e1
    const-string v3, "BluetoothAdapterState"

    #@e3
    const-string v4, "Radio already turned ON"

    #@e5
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e8
    goto/16 :goto_21

    #@ea
    .line 265
    :sswitch_data_ea
    .sparse-switch
        0x1 -> :sswitch_a5
        0x14 -> :sswitch_22
        0xca -> :sswitch_b5
        0xcb -> :sswitch_5d
    .end sparse-switch
.end method
