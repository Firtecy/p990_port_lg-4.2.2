.class final Lcom/android/bluetooth/btservice/BondStateMachine;
.super Lcom/android/internal/util/StateMachine;
.source "BondStateMachine.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/btservice/BondStateMachine$1;,
        Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;,
        Lcom/android/bluetooth/btservice/BondStateMachine$StableState;
    }
.end annotation


# static fields
.field static final BONDING_STATE_CHANGE:I = 0x4

.field static final BOND_STATE_BONDED:I = 0x2

.field static final BOND_STATE_BONDING:I = 0x1

.field static final BOND_STATE_NONE:I = 0x0

.field static final CANCEL_BOND:I = 0x2

.field static final CREATE_BOND:I = 0x1

.field private static final DBG:Z = true

.field static final REMOVE_BOND:I = 0x3

.field private static final TAG:Ljava/lang/String; = "BluetoothBondStateMachine"


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

.field private mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

.field private mPendingCommandState:Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;

.field private mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

.field private mStableState:Lcom/android/bluetooth/btservice/BondStateMachine$StableState;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/btservice/AdapterService;Lcom/android/bluetooth/btservice/AdapterProperties;Lcom/android/bluetooth/btservice/RemoteDevices;)V
    .registers 6
    .parameter "service"
    .parameter "prop"
    .parameter "remoteDevices"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 75
    const-string v0, "BondStateMachine:"

    #@3
    invoke-direct {p0, v0}, Lcom/android/internal/util/StateMachine;-><init>(Ljava/lang/String;)V

    #@6
    .line 70
    new-instance v0, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;

    #@8
    invoke-direct {v0, p0, v1}, Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;-><init>(Lcom/android/bluetooth/btservice/BondStateMachine;Lcom/android/bluetooth/btservice/BondStateMachine$1;)V

    #@b
    iput-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mPendingCommandState:Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;

    #@d
    .line 71
    new-instance v0, Lcom/android/bluetooth/btservice/BondStateMachine$StableState;

    #@f
    invoke-direct {v0, p0, v1}, Lcom/android/bluetooth/btservice/BondStateMachine$StableState;-><init>(Lcom/android/bluetooth/btservice/BondStateMachine;Lcom/android/bluetooth/btservice/BondStateMachine$1;)V

    #@12
    iput-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mStableState:Lcom/android/bluetooth/btservice/BondStateMachine$StableState;

    #@14
    .line 76
    iget-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mStableState:Lcom/android/bluetooth/btservice/BondStateMachine$StableState;

    #@16
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/BondStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@19
    .line 77
    iget-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mPendingCommandState:Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;

    #@1b
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/BondStateMachine;->addState(Lcom/android/internal/util/State;)V

    #@1e
    .line 78
    iput-object p3, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@20
    .line 79
    iput-object p1, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@22
    .line 80
    iput-object p2, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@24
    .line 81
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@27
    move-result-object v0

    #@28
    iput-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2a
    .line 82
    iget-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mStableState:Lcom/android/bluetooth/btservice/BondStateMachine$StableState;

    #@2c
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/BondStateMachine;->setInitialState(Lcom/android/internal/util/State;)V

    #@2f
    .line 83
    return-void
.end method

.method static synthetic access$1000(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;)Z
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/BondStateMachine;->cancelBond(Landroid/bluetooth/BluetoothDevice;)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(Lcom/android/bluetooth/btservice/BondStateMachine;I)I
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/BondStateMachine;->getUnbondReasonFromHALCode(I)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1200(Lcom/android/bluetooth/btservice/BondStateMachine;)Lcom/android/bluetooth/btservice/BondStateMachine$StableState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mStableState:Lcom/android/bluetooth/btservice/BondStateMachine$StableState;

    #@2
    return-object v0
.end method

.method static synthetic access$1300(Lcom/android/bluetooth/btservice/BondStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/BondStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/BondStateMachine;->clearProfilePriorty(Landroid/bluetooth/BluetoothDevice;)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/BondStateMachine;->setProfilePriorty(Landroid/bluetooth/BluetoothDevice;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/bluetooth/btservice/BondStateMachine;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/BondStateMachine;->infoLog(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;Z)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/btservice/BondStateMachine;->createBond(Landroid/bluetooth/BluetoothDevice;Z)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$400(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;Z)Z
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/btservice/BondStateMachine;->removeBond(Landroid/bluetooth/BluetoothDevice;Z)Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$500(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/bluetooth/BluetoothDevice;II)V
    .registers 4
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"

    #@0
    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/android/bluetooth/btservice/BondStateMachine;->sendIntent(Landroid/bluetooth/BluetoothDevice;II)V

    #@3
    return-void
.end method

.method static synthetic access$600(Lcom/android/bluetooth/btservice/BondStateMachine;)Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    iget-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mPendingCommandState:Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;

    #@2
    return-object v0
.end method

.method static synthetic access$700(Lcom/android/bluetooth/btservice/BondStateMachine;Lcom/android/internal/util/IState;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/BondStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/bluetooth/btservice/BondStateMachine;)Landroid/os/Message;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/BondStateMachine;->getCurrentMessage()Landroid/os/Message;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/bluetooth/btservice/BondStateMachine;Landroid/os/Message;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/BondStateMachine;->deferMessage(Landroid/os/Message;)V

    #@3
    return-void
.end method

.method private cancelBond(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "dev"

    #@0
    .prologue
    .line 227
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    #@3
    move-result v1

    #@4
    const/16 v2, 0xb

    #@6
    if-ne v1, v2, :cond_1f

    #@8
    .line 228
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-static {v1}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@f
    move-result-object v0

    #@10
    .line 229
    .local v0, addr:[B
    iget-object v1, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/btservice/AdapterService;->cancelBondNative([B)Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_21

    #@18
    .line 230
    const-string v1, "BluetoothBondStateMachine"

    #@1a
    const-string v2, "Unexpected error while cancelling bond:"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 235
    .end local v0           #addr:[B
    :cond_1f
    const/4 v1, 0x0

    #@20
    :goto_20
    return v1

    #@21
    .line 232
    .restart local v0       #addr:[B
    :cond_21
    const/4 v1, 0x1

    #@22
    goto :goto_20
.end method

.method private clearProfilePriorty(Landroid/bluetooth/BluetoothDevice;)V
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v3, -0x1

    #@1
    .line 373
    invoke-static {}, Lcom/android/bluetooth/hid/HidService;->getHidService()Lcom/android/bluetooth/hid/HidService;

    #@4
    move-result-object v2

    #@5
    .line 374
    .local v2, hidService:Lcom/android/bluetooth/hid/HidService;
    invoke-static {}, Lcom/android/bluetooth/a2dp/A2dpService;->getA2dpService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@8
    move-result-object v0

    #@9
    .line 375
    .local v0, a2dpService:Lcom/android/bluetooth/a2dp/A2dpService;
    invoke-static {}, Lcom/android/bluetooth/hfp/HeadsetService;->getHeadsetService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@c
    move-result-object v1

    #@d
    .line 377
    .local v1, headsetService:Lcom/android/bluetooth/hfp/HeadsetService;
    if-eqz v2, :cond_12

    #@f
    .line 378
    invoke-virtual {v2, p1, v3}, Lcom/android/bluetooth/hid/HidService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@12
    .line 380
    :cond_12
    if-eqz v0, :cond_17

    #@14
    .line 381
    invoke-virtual {v0, p1, v3}, Lcom/android/bluetooth/a2dp/A2dpService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@17
    .line 383
    :cond_17
    if-eqz v1, :cond_1c

    #@19
    .line 384
    invoke-virtual {v1, p1, v3}, Lcom/android/bluetooth/hfp/HeadsetService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@1c
    .line 386
    :cond_1c
    return-void
.end method

.method private createBond(Landroid/bluetooth/BluetoothDevice;Z)Z
    .registers 10
    .parameter "dev"
    .parameter "transition"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/16 v6, 0x9

    #@3
    const/16 v5, 0xa

    #@5
    const/4 v1, 0x0

    #@6
    .line 255
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    #@9
    move-result v3

    #@a
    if-ne v3, v5, :cond_48

    #@c
    .line 256
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "Bond address is:"

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/BondStateMachine;->infoLog(Ljava/lang/String;)V

    #@22
    .line 257
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v3}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@29
    move-result-object v0

    #@2a
    .line 259
    .local v0, addr:[B
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@2d
    move-result-object v3

    #@2e
    if-eqz v3, :cond_62

    #@30
    .line 260
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@37
    move-result-object v4

    #@38
    invoke-interface {v3, v4}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@3b
    move-result v3

    #@3c
    if-eqz v3, :cond_49

    #@3e
    .line 261
    const-string v2, "BluetoothBondStateMachine"

    #@40
    const-string v3, "createBond is blocked by LG MDM Server Policy(audio only"

    #@42
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 262
    invoke-direct {p0, p1, v5, v6}, Lcom/android/bluetooth/btservice/BondStateMachine;->sendIntent(Landroid/bluetooth/BluetoothDevice;II)V

    #@48
    .line 283
    .end local v0           #addr:[B
    :cond_48
    :goto_48
    return v1

    #@49
    .line 265
    .restart local v0       #addr:[B
    :cond_49
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4c
    move-result-object v3

    #@4d
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@50
    move-result-object v4

    #@51
    invoke-interface {v3, v4, v2}, Lcom/lge/cappuccino/IMdm;->checkBluetoothPairing(Ljava/lang/String;Z)Z

    #@54
    move-result v3

    #@55
    if-eqz v3, :cond_62

    #@57
    .line 267
    const-string v2, "BluetoothBondStateMachine"

    #@59
    const-string v3, "createBond is blocked by LG MDM Server Policy(pairing)"

    #@5b
    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5e
    .line 268
    invoke-direct {p0, p1, v5, v6}, Lcom/android/bluetooth/btservice/BondStateMachine;->sendIntent(Landroid/bluetooth/BluetoothDevice;II)V

    #@61
    goto :goto_48

    #@62
    .line 274
    :cond_62
    iget-object v3, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@64
    invoke-virtual {v3, v0}, Lcom/android/bluetooth/btservice/AdapterService;->createBondNative([B)Z

    #@67
    move-result v3

    #@68
    if-nez v3, :cond_6e

    #@6a
    .line 275
    invoke-direct {p0, p1, v5, v6}, Lcom/android/bluetooth/btservice/BondStateMachine;->sendIntent(Landroid/bluetooth/BluetoothDevice;II)V

    #@6d
    goto :goto_48

    #@6e
    .line 278
    :cond_6e
    if-eqz p2, :cond_75

    #@70
    .line 279
    iget-object v1, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mPendingCommandState:Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;

    #@72
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/btservice/BondStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@75
    :cond_75
    move v1, v2

    #@76
    .line 281
    goto :goto_48
.end method

.method private errorLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 393
    const-string v0, "BluetoothBondStateMachine"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 394
    return-void
.end method

.method private getUnbondReasonFromHALCode(I)I
    .registers 4
    .parameter "reason"

    #@0
    .prologue
    const/16 v0, 0x9

    #@2
    .line 397
    if-nez p1, :cond_6

    #@4
    .line 398
    const/4 v0, 0x0

    #@5
    .line 417
    :cond_5
    :goto_5
    return v0

    #@6
    .line 399
    :cond_6
    const/16 v1, 0xa

    #@8
    if-ne p1, v1, :cond_c

    #@a
    .line 400
    const/4 v0, 0x4

    #@b
    goto :goto_5

    #@c
    .line 401
    :cond_c
    if-ne p1, v0, :cond_10

    #@e
    .line 402
    const/4 v0, 0x1

    #@f
    goto :goto_5

    #@10
    .line 406
    :cond_10
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isBRCMSolution()Z

    #@13
    move-result v1

    #@14
    if-eqz v1, :cond_5

    #@16
    .line 407
    const/16 v1, 0xb

    #@18
    if-ne p1, v1, :cond_1c

    #@1a
    .line 408
    const/4 v0, 0x2

    #@1b
    goto :goto_5

    #@1c
    .line 409
    :cond_1c
    const/16 v1, 0xc

    #@1e
    if-ne p1, v1, :cond_5

    #@20
    .line 410
    const/4 v0, 0x6

    #@21
    goto :goto_5
.end method

.method private infoLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 389
    const-string v0, "BluetoothBondStateMachine"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 390
    return-void
.end method

.method public static make(Lcom/android/bluetooth/btservice/AdapterService;Lcom/android/bluetooth/btservice/AdapterProperties;Lcom/android/bluetooth/btservice/RemoteDevices;)Lcom/android/bluetooth/btservice/BondStateMachine;
    .registers 6
    .parameter "service"
    .parameter "prop"
    .parameter "remoteDevices"

    #@0
    .prologue
    .line 87
    const-string v1, "BluetoothBondStateMachine"

    #@2
    const-string v2, "make"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 88
    new-instance v0, Lcom/android/bluetooth/btservice/BondStateMachine;

    #@9
    invoke-direct {v0, p0, p1, p2}, Lcom/android/bluetooth/btservice/BondStateMachine;-><init>(Lcom/android/bluetooth/btservice/AdapterService;Lcom/android/bluetooth/btservice/AdapterProperties;Lcom/android/bluetooth/btservice/RemoteDevices;)V

    #@c
    .line 89
    .local v0, bsm:Lcom/android/bluetooth/btservice/BondStateMachine;
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/BondStateMachine;->start()V

    #@f
    .line 90
    return-object v0
.end method

.method private removeBond(Landroid/bluetooth/BluetoothDevice;Z)Z
    .registers 6
    .parameter "dev"
    .parameter "transition"

    #@0
    .prologue
    .line 239
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    #@3
    move-result v1

    #@4
    const/16 v2, 0xc

    #@6
    if-ne v1, v2, :cond_1f

    #@8
    .line 240
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    invoke-static {v1}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@f
    move-result-object v0

    #@10
    .line 241
    .local v0, addr:[B
    iget-object v1, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/btservice/AdapterService;->removeBondNative([B)Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_21

    #@18
    .line 242
    const-string v1, "BluetoothBondStateMachine"

    #@1a
    const-string v2, "Unexpected error while removing bond:"

    #@1c
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 251
    .end local v0           #addr:[B
    :cond_1f
    const/4 v1, 0x0

    #@20
    :goto_20
    return v1

    #@21
    .line 244
    .restart local v0       #addr:[B
    :cond_21
    if-eqz p2, :cond_28

    #@23
    .line 245
    iget-object v1, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mPendingCommandState:Lcom/android/bluetooth/btservice/BondStateMachine$PendingCommandState;

    #@25
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/btservice/BondStateMachine;->transitionTo(Lcom/android/internal/util/IState;)V

    #@28
    .line 247
    :cond_28
    const/4 v1, 0x1

    #@29
    goto :goto_20
.end method

.method private sendIntent(Landroid/bluetooth/BluetoothDevice;II)V
    .registers 11
    .parameter "device"
    .parameter "newState"
    .parameter "reason"

    #@0
    .prologue
    const/16 v6, 0xa

    #@2
    .line 287
    iget-object v3, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@4
    invoke-virtual {v3, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@7
    move-result-object v0

    #@8
    .line 288
    .local v0, devProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    const/16 v2, 0xa

    #@a
    .line 289
    .local v2, oldState:I
    if-eqz v0, :cond_10

    #@c
    .line 290
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getBondState()I

    #@f
    move-result v2

    #@10
    .line 292
    :cond_10
    if-ne v2, p2, :cond_13

    #@12
    .line 321
    :goto_12
    return-void

    #@13
    .line 295
    :cond_13
    iget-object v3, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@15
    invoke-virtual {v3, p1, p2}, Lcom/android/bluetooth/btservice/AdapterProperties;->onBondStateChanged(Landroid/bluetooth/BluetoothDevice;I)V

    #@18
    .line 298
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1b
    move-result-object v3

    #@1c
    if-eqz v3, :cond_4d

    #@1e
    if-eq p2, v6, :cond_4d

    #@20
    .line 299
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@27
    move-result-object v4

    #@28
    invoke-interface {v3, v4}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@2b
    move-result v3

    #@2c
    if-eqz v3, :cond_36

    #@2e
    .line 300
    const-string v3, "BluetoothBondStateMachine"

    #@30
    const-string v4, "sendIntent is blocked by LG MDM Server Policy"

    #@32
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_12

    #@36
    .line 302
    :cond_36
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@3d
    move-result-object v4

    #@3e
    const/4 v5, 0x0

    #@3f
    invoke-interface {v3, v4, v5}, Lcom/lge/cappuccino/IMdm;->checkBluetoothPairing(Ljava/lang/String;Z)Z

    #@42
    move-result v3

    #@43
    if-eqz v3, :cond_4d

    #@45
    .line 304
    const-string v3, "BluetoothBondStateMachine"

    #@47
    const-string v4, "sendIntent is blocked by LG MDM Server Policy"

    #@49
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_12

    #@4d
    .line 310
    :cond_4d
    new-instance v1, Landroid/content/Intent;

    #@4f
    const-string v3, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    #@51
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@54
    .line 311
    .local v1, intent:Landroid/content/Intent;
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    #@56
    invoke-virtual {v1, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@59
    .line 312
    const-string v3, "android.bluetooth.device.extra.BOND_STATE"

    #@5b
    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@5e
    .line 313
    const-string v3, "android.bluetooth.device.extra.PREVIOUS_BOND_STATE"

    #@60
    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@63
    .line 314
    if-ne p2, v6, :cond_6a

    #@65
    .line 315
    const-string v3, "android.bluetooth.device.extra.REASON"

    #@67
    invoke-virtual {v1, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@6a
    .line 317
    :cond_6a
    iget-object v3, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@6c
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@6e
    const-string v5, "android.permission.BLUETOOTH"

    #@70
    invoke-virtual {v3, v1, v4, v5}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    #@73
    .line 319
    new-instance v3, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v4, "Bond State Change Intent:"

    #@7a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v3

    #@7e
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v3

    #@82
    const-string v4, " OldState: "

    #@84
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87
    move-result-object v3

    #@88
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@8b
    move-result-object v3

    #@8c
    const-string v4, " NewState: "

    #@8e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91
    move-result-object v3

    #@92
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@95
    move-result-object v3

    #@96
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@99
    move-result-object v3

    #@9a
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/BondStateMachine;->infoLog(Ljava/lang/String;)V

    #@9d
    goto/16 :goto_12
.end method

.method private setProfilePriorty(Landroid/bluetooth/BluetoothDevice;)V
    .registers 8
    .parameter "device"

    #@0
    .prologue
    const/16 v5, 0x64

    #@2
    const/4 v4, -0x1

    #@3
    .line 352
    invoke-static {}, Lcom/android/bluetooth/hid/HidService;->getHidService()Lcom/android/bluetooth/hid/HidService;

    #@6
    move-result-object v2

    #@7
    .line 353
    .local v2, hidService:Lcom/android/bluetooth/hid/HidService;
    invoke-static {}, Lcom/android/bluetooth/a2dp/A2dpService;->getA2dpService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@a
    move-result-object v0

    #@b
    .line 354
    .local v0, a2dpService:Lcom/android/bluetooth/a2dp/A2dpService;
    invoke-static {}, Lcom/android/bluetooth/hfp/HeadsetService;->getHeadsetService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@e
    move-result-object v1

    #@f
    .line 356
    .local v1, headsetService:Lcom/android/bluetooth/hfp/HeadsetService;
    if-eqz v2, :cond_1a

    #@11
    invoke-virtual {v2, p1}, Lcom/android/bluetooth/hid/HidService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@14
    move-result v3

    #@15
    if-ne v3, v4, :cond_1a

    #@17
    .line 358
    invoke-virtual {v2, p1, v5}, Lcom/android/bluetooth/hid/HidService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@1a
    .line 361
    :cond_1a
    if-eqz v0, :cond_25

    #@1c
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@1f
    move-result v3

    #@20
    if-ne v3, v4, :cond_25

    #@22
    .line 363
    invoke-virtual {v0, p1, v5}, Lcom/android/bluetooth/a2dp/A2dpService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@25
    .line 366
    :cond_25
    if-eqz v1, :cond_30

    #@27
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@2a
    move-result v3

    #@2b
    if-ne v3, v4, :cond_30

    #@2d
    .line 368
    invoke-virtual {v1, p1, v5}, Lcom/android/bluetooth/hfp/HeadsetService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@30
    .line 370
    :cond_30
    return-void
.end method


# virtual methods
.method bondStateChangeCallback(I[BI)V
    .registers 8
    .parameter "status"
    .parameter "address"
    .parameter "newState"

    #@0
    .prologue
    .line 324
    iget-object v2, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    invoke-virtual {v2, p2}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@5
    move-result-object v0

    #@6
    .line 326
    .local v0, device:Landroid/bluetooth/BluetoothDevice;
    if-nez v0, :cond_28

    #@8
    .line 327
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "No record of the device:"

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-direct {p0, v2}, Lcom/android/bluetooth/btservice/BondStateMachine;->infoLog(Ljava/lang/String;)V

    #@1e
    .line 330
    iget-object v2, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@20
    invoke-static {p2}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@27
    move-result-object v0

    #@28
    .line 333
    :cond_28
    new-instance v2, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v3, "bondStateChangeCallback: Status: "

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v2

    #@37
    const-string v3, " Address: "

    #@39
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v2

    #@3d
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v2

    #@41
    const-string v3, " newState: "

    #@43
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v2

    #@47
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v2

    #@4b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v2

    #@4f
    invoke-direct {p0, v2}, Lcom/android/bluetooth/btservice/BondStateMachine;->infoLog(Ljava/lang/String;)V

    #@52
    .line 336
    const/4 v2, 0x4

    #@53
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/btservice/BondStateMachine;->obtainMessage(I)Landroid/os/Message;

    #@56
    move-result-object v1

    #@57
    .line 337
    .local v1, msg:Landroid/os/Message;
    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@59
    .line 339
    const/4 v2, 0x2

    #@5a
    if-ne p3, v2, :cond_66

    #@5c
    .line 340
    const/16 v2, 0xc

    #@5e
    iput v2, v1, Landroid/os/Message;->arg1:I

    #@60
    .line 346
    :goto_60
    iput p1, v1, Landroid/os/Message;->arg2:I

    #@62
    .line 348
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/btservice/BondStateMachine;->sendMessage(Landroid/os/Message;)V

    #@65
    .line 349
    return-void

    #@66
    .line 341
    :cond_66
    const/4 v2, 0x1

    #@67
    if-ne p3, v2, :cond_6e

    #@69
    .line 342
    const/16 v2, 0xb

    #@6b
    iput v2, v1, Landroid/os/Message;->arg1:I

    #@6d
    goto :goto_60

    #@6e
    .line 344
    :cond_6e
    const/16 v2, 0xa

    #@70
    iput v2, v1, Landroid/os/Message;->arg1:I

    #@72
    goto :goto_60
.end method

.method public cleanup()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 98
    iput-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    .line 99
    iput-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@5
    .line 100
    iput-object v0, p0, Lcom/android/bluetooth/btservice/BondStateMachine;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@7
    .line 101
    return-void
.end method

.method public doQuit()V
    .registers 1

    #@0
    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/BondStateMachine;->quitNow()V

    #@3
    .line 95
    return-void
.end method
