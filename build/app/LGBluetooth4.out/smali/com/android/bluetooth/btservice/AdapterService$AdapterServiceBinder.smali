.class Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;
.super Landroid/bluetooth/IBluetooth$Stub;
.source "AdapterService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/btservice/AdapterService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AdapterServiceBinder"
.end annotation


# instance fields
.field private mService:Lcom/android/bluetooth/btservice/AdapterService;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/btservice/AdapterService;)V
    .registers 2
    .parameter "svc"

    #@0
    .prologue
    .line 861
    invoke-direct {p0}, Landroid/bluetooth/IBluetooth$Stub;-><init>()V

    #@3
    .line 862
    iput-object p1, p0, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@5
    .line 863
    return-void
.end method


# virtual methods
.method public cancelBondProcess(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1161
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1162
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "cancelBondProcess(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1170
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1166
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1167
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1170
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->cancelBondProcess(Landroid/bluetooth/BluetoothDevice;)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public cancelDiscovery()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1092
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1093
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "cancelDiscovery(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1101
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1097
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1098
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1101
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->cancelDiscovery()Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 865
    const/4 v0, 0x0

    #@1
    iput-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    .line 866
    const/4 v0, 0x1

    #@4
    return v0
.end method

.method public connectSocket(Landroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;II)Landroid/os/ParcelFileDescriptor;
    .registers 12
    .parameter "device"
    .parameter "type"
    .parameter "uuid"
    .parameter "port"
    .parameter "flag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1354
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1355
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "connectSocket(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1363
    :cond_e
    :goto_e
    return-object v1

    #@f
    .line 1359
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1360
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    move-object v1, p1

    #@16
    move v2, p2

    #@17
    move-object v3, p3

    #@18
    move v4, p4

    #@19
    move v5, p5

    #@1a
    .line 1363
    invoke-virtual/range {v0 .. v5}, Lcom/android/bluetooth/btservice/AdapterService;->connectSocket(Landroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;II)Landroid/os/ParcelFileDescriptor;

    #@1d
    move-result-object v1

    #@1e
    goto :goto_e
.end method

.method public createBond(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1148
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1149
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "createBond(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1157
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1153
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1154
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1157
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->createBond(Landroid/bluetooth/BluetoothDevice;)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public createSocketChannel(ILjava/lang/String;Landroid/os/ParcelUuid;II)Landroid/os/ParcelFileDescriptor;
    .registers 12
    .parameter "type"
    .parameter "serviceName"
    .parameter "uuid"
    .parameter "port"
    .parameter "flag"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1368
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1369
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "createSocketChannel(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1377
    :cond_e
    :goto_e
    return-object v1

    #@f
    .line 1373
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1374
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    move v1, p1

    #@16
    move-object v2, p2

    #@17
    move-object v3, p3

    #@18
    move v4, p4

    #@19
    move v5, p5

    #@1a
    .line 1377
    invoke-virtual/range {v0 .. v5}, Lcom/android/bluetooth/btservice/AdapterService;->createSocketChannel(ILjava/lang/String;Landroid/os/ParcelUuid;II)Landroid/os/ParcelFileDescriptor;

    #@1d
    move-result-object v1

    #@1e
    goto :goto_e
.end method

.method public disable()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 941
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0x3e8

    #@7
    if-eq v2, v3, :cond_17

    #@9
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_17

    #@f
    .line 943
    const-string v2, "BluetoothAdapterService"

    #@11
    const-string v3, "disable(): not allowed for non-active user and non system user"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 951
    :cond_16
    :goto_16
    return v1

    #@17
    .line 947
    :cond_17
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@1a
    move-result-object v0

    #@1b
    .line 948
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_16

    #@1d
    .line 951
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->disable()Z

    #@20
    move-result v1

    #@21
    goto :goto_16
.end method

.method public disableRadio()Z
    .registers 3

    #@0
    .prologue
    .line 964
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 965
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_8

    #@6
    .line 966
    const/4 v1, 0x0

    #@7
    .line 968
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->disableRadio()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public enable()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 913
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0x3e8

    #@7
    if-eq v2, v3, :cond_17

    #@9
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_17

    #@f
    .line 915
    const-string v2, "BluetoothAdapterService"

    #@11
    const-string v3, "enable(): not allowed for non-active user and non system user"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 923
    :cond_16
    :goto_16
    return v1

    #@17
    .line 919
    :cond_17
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@1a
    move-result-object v0

    #@1b
    .line 920
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_16

    #@1d
    .line 923
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->enable()Z

    #@20
    move-result v1

    #@21
    goto :goto_16
.end method

.method public enableNoAutoConnect()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 927
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0x3e8

    #@7
    if-eq v2, v3, :cond_17

    #@9
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_17

    #@f
    .line 929
    const-string v2, "BluetoothAdapterService"

    #@11
    const-string v3, "enableNoAuto(): not allowed for non-active user and non system user"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 937
    :cond_16
    :goto_16
    return v1

    #@17
    .line 933
    :cond_17
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@1a
    move-result-object v0

    #@1b
    .line 934
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_16

    #@1d
    .line 937
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->enableNoAutoConnect()Z

    #@20
    move-result v1

    #@21
    goto :goto_16
.end method

.method public enableRadio()Z
    .registers 3

    #@0
    .prologue
    .line 956
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 957
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_8

    #@6
    .line 958
    const/4 v1, 0x0

    #@7
    .line 960
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->enableRadio()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public fetchRemoteUuids(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1292
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1293
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "fetchRemoteUuids(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1301
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1297
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1298
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1301
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->fetchRemoteUuids(Landroid/bluetooth/BluetoothDevice;)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public getAdapterConnectionState()I
    .registers 3

    #@0
    .prologue
    .line 1127
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 1128
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_8

    #@6
    .line 1129
    const/4 v1, 0x0

    #@7
    .line 1131
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterConnectionState()I

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getAddress()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 973
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0x3e8

    #@7
    if-eq v2, v3, :cond_17

    #@9
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_17

    #@f
    .line 975
    const-string v2, "BluetoothAdapterService"

    #@11
    const-string v3, "getAddress(): not allowed for non-active user and non system user"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 983
    :cond_16
    :goto_16
    return-object v1

    #@17
    .line 979
    :cond_17
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@1a
    move-result-object v0

    #@1b
    .line 980
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_16

    #@1d
    .line 983
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->getAddress()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    goto :goto_16
.end method

.method public getBondState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 1188
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 1189
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_9

    #@6
    .line 1190
    const/16 v1, 0xa

    #@8
    .line 1192
    :goto_8
    return v1

    #@9
    :cond_9
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->getBondState(Landroid/bluetooth/BluetoothDevice;)I

    #@c
    move-result v1

    #@d
    goto :goto_8
.end method

.method public getBondedDevices()[Landroid/bluetooth/BluetoothDevice;
    .registers 3

    #@0
    .prologue
    .line 1118
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 1119
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_a

    #@6
    .line 1120
    const/4 v1, 0x0

    #@7
    new-array v1, v1, [Landroid/bluetooth/BluetoothDevice;

    #@9
    .line 1122
    :goto_9
    return-object v1

    #@a
    :cond_a
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->getBondedDevices()[Landroid/bluetooth/BluetoothDevice;

    #@d
    move-result-object v1

    #@e
    goto :goto_9
.end method

.method public getConnectedDevices()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 885
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 886
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_8

    #@6
    .line 887
    const/4 v1, 0x0

    #@7
    .line 889
    :goto_7
    return-object v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->getConnectedDevices()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    goto :goto_7
.end method

.method public getDiscoverableTimeout()I
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1053
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1054
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "getDiscoverableTimeout(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1062
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1058
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1059
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1062
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->getDiscoverableTimeout()I

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public getJustWorks(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 1218
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 1219
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_8

    #@6
    .line 1220
    const/4 v1, 0x0

    #@7
    .line 1222
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->getJustWorks(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getName()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1000
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    #@4
    move-result v2

    #@5
    const/16 v3, 0x3e8

    #@7
    if-eq v2, v3, :cond_17

    #@9
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@c
    move-result v2

    #@d
    if-nez v2, :cond_17

    #@f
    .line 1002
    const-string v2, "BluetoothAdapterService"

    #@11
    const-string v3, "getName(): not allowed for non-active user and non system user"

    #@13
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@16
    .line 1010
    :cond_16
    :goto_16
    return-object v1

    #@17
    .line 1006
    :cond_17
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@1a
    move-result-object v0

    #@1b
    .line 1007
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_16

    #@1d
    .line 1010
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->getName()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    goto :goto_16
.end method

.method public getProfileConnectionState(I)I
    .registers 6
    .parameter "profile"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1135
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1136
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "getProfileConnectionState: not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1144
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1140
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1141
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1144
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->getProfileConnectionState(I)I

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public getRemoteAlias(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1240
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1241
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "getRemoteAlias(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1249
    :cond_e
    :goto_e
    return-object v1

    #@f
    .line 1245
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1246
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1249
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->getRemoteAlias(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    goto :goto_e
.end method

.method public getRemoteClass(Landroid/bluetooth/BluetoothDevice;)I
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1266
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1267
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "getRemoteClass(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1275
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1271
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1272
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1275
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->getRemoteClass(Landroid/bluetooth/BluetoothDevice;)I

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public getRemoteName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1227
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1228
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "getRemoteName(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1236
    :cond_e
    :goto_e
    return-object v1

    #@f
    .line 1232
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1233
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1236
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->getRemoteName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    goto :goto_e
.end method

.method public getRemoteUuids(Landroid/bluetooth/BluetoothDevice;)[Landroid/os/ParcelUuid;
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 1279
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_11

    #@7
    .line 1280
    const-string v1, "BluetoothAdapterService"

    #@9
    const-string v2, "getRemoteUuids(): not allowed for non-active user"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1281
    new-array v1, v3, [Landroid/os/ParcelUuid;

    #@10
    .line 1288
    :goto_10
    return-object v1

    #@11
    .line 1284
    :cond_11
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@14
    move-result-object v0

    #@15
    .line 1285
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_1a

    #@17
    .line 1286
    new-array v1, v3, [Landroid/os/ParcelUuid;

    #@19
    goto :goto_10

    #@1a
    .line 1288
    :cond_1a
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->getRemoteUuids(Landroid/bluetooth/BluetoothDevice;)[Landroid/os/ParcelUuid;

    #@1d
    move-result-object v1

    #@1e
    goto :goto_10
.end method

.method public getScanMode()I
    .registers 5

    #@0
    .prologue
    const/16 v1, 0x14

    #@2
    .line 1027
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@5
    move-result v2

    #@6
    if-nez v2, :cond_10

    #@8
    .line 1028
    const-string v2, "BluetoothAdapterService"

    #@a
    const-string v3, "getScanMode(): not allowed for non-active user"

    #@c
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@f
    .line 1036
    :cond_f
    :goto_f
    return v1

    #@10
    .line 1032
    :cond_10
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@13
    move-result-object v0

    #@14
    .line 1033
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_f

    #@16
    .line 1036
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->getScanMode()I

    #@19
    move-result v1

    #@1a
    goto :goto_f
.end method

.method public getService()Lcom/android/bluetooth/btservice/AdapterService;
    .registers 2

    #@0
    .prologue
    .line 870
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    if-eqz v0, :cond_f

    #@4
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@6
    invoke-static {v0}, Lcom/android/bluetooth/btservice/AdapterService;->access$500(Lcom/android/bluetooth/btservice/AdapterService;)Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_f

    #@c
    .line 871
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@e
    .line 873
    :goto_e
    return-object v0

    #@f
    :cond_f
    const/4 v0, 0x0

    #@10
    goto :goto_e
.end method

.method public getState()I
    .registers 3

    #@0
    .prologue
    .line 905
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 906
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_9

    #@6
    .line 907
    const/16 v1, 0xa

    #@8
    .line 909
    :goto_8
    return v1

    #@9
    :cond_9
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->getState()I

    #@c
    move-result v1

    #@d
    goto :goto_8
.end method

.method public getTrustState(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 1198
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 1199
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_8

    #@6
    .line 1200
    const/4 v1, 0x0

    #@7
    .line 1202
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->getTrustState(Landroid/bluetooth/BluetoothDevice;)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public getUuids()[Landroid/os/ParcelUuid;
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 987
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v1

    #@5
    if-nez v1, :cond_11

    #@7
    .line 988
    const-string v1, "BluetoothAdapterService"

    #@9
    const-string v2, "getUuids(): not allowed for non-active user"

    #@b
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 989
    new-array v1, v3, [Landroid/os/ParcelUuid;

    #@10
    .line 996
    :goto_10
    return-object v1

    #@11
    .line 992
    :cond_11
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@14
    move-result-object v0

    #@15
    .line 993
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_1a

    #@17
    .line 994
    new-array v1, v3, [Landroid/os/ParcelUuid;

    #@19
    goto :goto_10

    #@1a
    .line 996
    :cond_1a
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->getUuids()[Landroid/os/ParcelUuid;

    #@1d
    move-result-object v1

    #@1e
    goto :goto_10
.end method

.method public isDiscovering()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1104
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1105
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "isDiscovering(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1113
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1109
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1110
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1113
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->isDiscovering()Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public isEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 877
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 878
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_8

    #@6
    .line 879
    const/4 v1, 0x0

    #@7
    .line 881
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->isEnabled()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public isRadioEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 895
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 896
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_8

    #@6
    .line 897
    const/4 v1, 0x0

    #@7
    .line 899
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->isRadioEnabled()Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public registerCallback(Landroid/bluetooth/IBluetoothCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 1381
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 1382
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_7

    #@6
    .line 1386
    :goto_6
    return-void

    #@7
    .line 1385
    :cond_7
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->registerCallback(Landroid/bluetooth/IBluetoothCallback;)V

    #@a
    goto :goto_6
.end method

.method public removeBond(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1174
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1175
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "removeBond(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1183
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1179
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1180
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1183
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->removeBond(Landroid/bluetooth/BluetoothDevice;)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V
    .registers 6
    .parameter "device"
    .parameter "profile"
    .parameter "state"
    .parameter "prevState"

    #@0
    .prologue
    .line 1345
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 1346
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_7

    #@6
    .line 1350
    :goto_6
    return-void

    #@7
    .line 1349
    :cond_7
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/bluetooth/btservice/AdapterService;->sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V

    #@a
    goto :goto_6
.end method

.method public setDiscoverableTimeout(I)Z
    .registers 6
    .parameter "timeout"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1066
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1067
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "setDiscoverableTimeout(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1075
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1071
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1072
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1075
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->setDiscoverableTimeout(I)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public setName(Ljava/lang/String;)Z
    .registers 6
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1014
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1015
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "setName(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1023
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1019
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1020
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1023
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->setName(Ljava/lang/String;)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public setPairingConfirmation(Landroid/bluetooth/BluetoothDevice;Z)Z
    .registers 7
    .parameter "device"
    .parameter "accept"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1331
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1332
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "setPairingConfirmation(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1340
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1336
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1337
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1340
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/btservice/AdapterService;->setPairingConfirmation(Landroid/bluetooth/BluetoothDevice;Z)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public setPasskey(Landroid/bluetooth/BluetoothDevice;ZI[B)Z
    .registers 9
    .parameter "device"
    .parameter "accept"
    .parameter "len"
    .parameter "passkey"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1318
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1319
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "setPasskey(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1327
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1323
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1324
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1327
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/bluetooth/btservice/AdapterService;->setPasskey(Landroid/bluetooth/BluetoothDevice;ZI[B)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public setPin(Landroid/bluetooth/BluetoothDevice;ZI[B)Z
    .registers 9
    .parameter "device"
    .parameter "accept"
    .parameter "len"
    .parameter "pinCode"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1305
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1306
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "setPin(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1314
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1310
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1311
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1314
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/bluetooth/btservice/AdapterService;->setPin(Landroid/bluetooth/BluetoothDevice;ZI[B)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public setRemoteAlias(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z
    .registers 7
    .parameter "device"
    .parameter "name"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1253
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1254
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "setRemoteAlias(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1262
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1258
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1259
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1262
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/btservice/AdapterService;->setRemoteAlias(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public setScanMode(II)Z
    .registers 7
    .parameter "mode"
    .parameter "duration"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1040
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1041
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "setScanMode(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1049
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1045
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1046
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1049
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/btservice/AdapterService;->setScanMode(II)Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public setTrustState(Landroid/bluetooth/BluetoothDevice;Z)Z
    .registers 5
    .parameter "device"
    .parameter "truststate"

    #@0
    .prologue
    .line 1207
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 1208
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_8

    #@6
    .line 1209
    const/4 v1, 0x0

    #@7
    .line 1211
    :goto_7
    return v1

    #@8
    :cond_8
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/btservice/AdapterService;->setTrustState(Landroid/bluetooth/BluetoothDevice;Z)Z

    #@b
    move-result v1

    #@c
    goto :goto_7
.end method

.method public startDiscovery()Z
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 1079
    invoke-static {}, Lcom/android/bluetooth/Utils;->checkCaller()Z

    #@4
    move-result v2

    #@5
    if-nez v2, :cond_f

    #@7
    .line 1080
    const-string v2, "BluetoothAdapterService"

    #@9
    const-string v3, "startDiscovery(): not allowed for non-active user"

    #@b
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 1088
    :cond_e
    :goto_e
    return v1

    #@f
    .line 1084
    :cond_f
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    .line 1085
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_e

    #@15
    .line 1088
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->startDiscovery()Z

    #@18
    move-result v1

    #@19
    goto :goto_e
.end method

.method public unregisterCallback(Landroid/bluetooth/IBluetoothCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 1389
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->getService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 1390
    .local v0, service:Lcom/android/bluetooth/btservice/AdapterService;
    if-nez v0, :cond_7

    #@6
    .line 1394
    :goto_6
    return-void

    #@7
    .line 1393
    :cond_7
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->unregisterCallback(Landroid/bluetooth/IBluetoothCallback;)V

    #@a
    goto :goto_6
.end method
