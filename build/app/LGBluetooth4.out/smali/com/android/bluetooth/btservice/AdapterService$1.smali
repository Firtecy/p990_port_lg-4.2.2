.class Lcom/android/bluetooth/btservice/AdapterService$1;
.super Landroid/os/Handler;
.source "AdapterService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/btservice/AdapterService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/btservice/AdapterService;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/btservice/AdapterService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 661
    iput-object p1, p0, Lcom/android/bluetooth/btservice/AdapterService$1;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .registers 9
    .parameter "msg"

    #@0
    .prologue
    .line 665
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService$1;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Message: "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p1, Landroid/os/Message;->what:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->access$100(Lcom/android/bluetooth/btservice/AdapterService;Ljava/lang/String;)V

    #@1a
    .line 668
    iget v0, p1, Landroid/os/Message;->what:I

    #@1c
    sparse-switch v0, :sswitch_data_68

    #@1f
    .line 693
    :goto_1f
    return-void

    #@20
    .line 671
    :sswitch_20
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService$1;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@22
    const-string v1, "MESSAGE_PROFILE_SERVICE_STATE_CHANGED"

    #@24
    invoke-static {v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->access$100(Lcom/android/bluetooth/btservice/AdapterService;Ljava/lang/String;)V

    #@27
    .line 673
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService$1;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@29
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@2b
    check-cast v0, Ljava/lang/String;

    #@2d
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@2f
    invoke-static {v1, v0, v2}, Lcom/android/bluetooth/btservice/AdapterService;->access$200(Lcom/android/bluetooth/btservice/AdapterService;Ljava/lang/String;I)V

    #@32
    goto :goto_1f

    #@33
    .line 678
    :sswitch_33
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService$1;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@35
    const-string v1, "MESSAGE_PROFILE_CONNECTION_STATE_CHANGED"

    #@37
    invoke-static {v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->access$100(Lcom/android/bluetooth/btservice/AdapterService;Ljava/lang/String;)V

    #@3a
    .line 680
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService$1;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@3c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@3e
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@40
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@42
    iget v3, p1, Landroid/os/Message;->arg2:I

    #@44
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    #@47
    move-result-object v4

    #@48
    const-string v5, "prevState"

    #@4a
    const/high16 v6, -0x8000

    #@4c
    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    #@4f
    move-result v4

    #@50
    invoke-static {v1, v0, v2, v3, v4}, Lcom/android/bluetooth/btservice/AdapterService;->access$300(Lcom/android/bluetooth/btservice/AdapterService;Landroid/bluetooth/BluetoothDevice;III)V

    #@53
    goto :goto_1f

    #@54
    .line 685
    :sswitch_54
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService$1;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@56
    const-string v1, "MESSAGE_CONNECT_OTHER_PROFILES"

    #@58
    invoke-static {v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->access$100(Lcom/android/bluetooth/btservice/AdapterService;Ljava/lang/String;)V

    #@5b
    .line 687
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService$1;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@5d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@5f
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@61
    iget v2, p1, Landroid/os/Message;->arg1:I

    #@63
    invoke-static {v1, v0, v2}, Lcom/android/bluetooth/btservice/AdapterService;->access$400(Lcom/android/bluetooth/btservice/AdapterService;Landroid/bluetooth/BluetoothDevice;I)V

    #@66
    goto :goto_1f

    #@67
    .line 668
    nop

    #@68
    :sswitch_data_68
    .sparse-switch
        0x1 -> :sswitch_20
        0x14 -> :sswitch_33
        0x1e -> :sswitch_54
    .end sparse-switch
.end method
