.class public Lcom/android/bluetooth/btservice/AdapterService;
.super Landroid/app/Service;
.source "AdapterService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;,
        Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;
    }
.end annotation


# static fields
.field private static final A2DP:Ljava/lang/String; = "com.android.bluetooth.a2dp.A2dpService"

.field public static final ACTION_LOAD_ADAPTER_PROPERTIES:Ljava/lang/String; = "com.android.bluetooth.btservice.action.LOAD_ADAPTER_PROPERTIES"

.field public static final ACTION_SERVICE_STATE_CHANGED:Ljava/lang/String; = "com.android.bluetooth.btservice.action.STATE_CHANGED"

.field private static final ADAPTER_SERVICE_TYPE:I = 0x1

.field static final BLUETOOTH_ADMIN_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH_ADMIN"

.field static final BLUETOOTH_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH"

.field private static final CONNECT_OTHER_PROFILES_TIMEOUT:I = 0x1770

.field private static final DBG:Z = true

.field public static final EXTRA_ACTION:Ljava/lang/String; = "action"

.field private static final FTP:Ljava/lang/String; = "com.broadcom.bt.service.ftp.FTPService"

.field private static final GATT:Ljava/lang/String; = "com.broadcom.bt.service.gatt.GattService"

.field private static final HDP:Ljava/lang/String; = "com.android.bluetooth.hdp.HealthService"

.field private static final HID:Ljava/lang/String; = "com.android.bluetooth.hid.HidService"

.field private static final HSP_HFP:Ljava/lang/String; = "com.android.bluetooth.hfp.HeadsetService"

.field private static final MAP:Ljava/lang/String; = "com.broadcom.bt.service.map.MapService"

.field private static final MESSAGE_CONNECT_OTHER_PROFILES:I = 0x1e

.field private static final MESSAGE_PROFILE_CONNECTION_STATE_CHANGED:I = 0x14

.field private static final MESSAGE_PROFILE_SERVICE_STATE_CHANGED:I = 0x1

.field private static final PAN:Ljava/lang/String; = "com.android.bluetooth.pan.PanService"

.field public static final PROFILE_CONN_CONNECTED:I = 0x1

.field public static final PROFILE_CONN_REJECTED:I = 0x2

.field private static final SAP:Ljava/lang/String; = "com.broadcom.bt.service.sap.SapService"

.field private static final TAG:Ljava/lang/String; = "BluetoothAdapterService"

.field private static final TRACE_REF:Z = true

.field private static mServiceAdapter:Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;

.field private static sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

.field private static sRefCount:I


# instance fields
.field private mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

.field private mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

.field private mBinder:Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;

.field private final mBluetoothReceiver:Landroid/content/BroadcastReceiver;

.field private mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

.field private mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/bluetooth/IBluetoothCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mCleaningUp:Z

.field private mCurrentRequestId:I

.field private mFd:I

.field private final mHandler:Landroid/os/Handler;

.field private mJniCallbacks:Lcom/android/bluetooth/btservice/JniCallbacks;

.field private mNativeAvailable:Z

.field private mProfileServicesState:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mProfilesStarted:Z

.field private mQuietmode:Z

.field private mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

.field private mServiceChanged:Z

.field mSettingsDBUpdateObserver:Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 122
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/bluetooth/btservice/AdapterService;->sRefCount:I

    #@3
    .line 153
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->classInitNative()V

    #@6
    .line 154
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    .line 245
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 236
    new-instance v0, Ljava/util/HashMap;

    #@5
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@a
    .line 239
    const/4 v0, 0x0

    #@b
    iput-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mQuietmode:Z

    #@d
    .line 241
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mSettingsDBUpdateObserver:Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;

    #@10
    .line 661
    new-instance v0, Lcom/android/bluetooth/btservice/AdapterService$1;

    #@12
    invoke-direct {v0, p0}, Lcom/android/bluetooth/btservice/AdapterService$1;-><init>(Lcom/android/bluetooth/btservice/AdapterService;)V

    #@15
    iput-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mHandler:Landroid/os/Handler;

    #@17
    .line 697
    new-instance v0, Lcom/android/bluetooth/btservice/AdapterService$2;

    #@19
    invoke-direct {v0, p0}, Lcom/android/bluetooth/btservice/AdapterService$2;-><init>(Lcom/android/bluetooth/btservice/AdapterService;)V

    #@1c
    iput-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    #@1e
    .line 247
    const-class v1, Lcom/android/bluetooth/btservice/AdapterService;

    #@20
    monitor-enter v1

    #@21
    .line 248
    :try_start_21
    sget v0, Lcom/android/bluetooth/btservice/AdapterService;->sRefCount:I

    #@23
    add-int/lit8 v0, v0, 0x1

    #@25
    sput v0, Lcom/android/bluetooth/btservice/AdapterService;->sRefCount:I

    #@27
    .line 249
    const-string v0, "BluetoothAdapterService"

    #@29
    new-instance v2, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "REFCOUNT: CREATED. INSTANCE_COUNT"

    #@30
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v2

    #@34
    sget v3, Lcom/android/bluetooth/btservice/AdapterService;->sRefCount:I

    #@36
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v2

    #@3a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v2

    #@3e
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 250
    monitor-exit v1

    #@42
    .line 252
    return-void

    #@43
    .line 250
    :catchall_43
    move-exception v0

    #@44
    monitor-exit v1
    :try_end_45
    .catchall {:try_start_21 .. :try_end_45} :catchall_43

    #@45
    throw v0
.end method

.method static synthetic access$000(Lcom/android/bluetooth/btservice/AdapterService;)Lcom/android/bluetooth/btservice/AdapterProperties;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 117
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/bluetooth/btservice/AdapterService;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 117
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$200(Lcom/android/bluetooth/btservice/AdapterService;Ljava/lang/String;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/btservice/AdapterService;->processProfileServiceStateChanged(Ljava/lang/String;I)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/bluetooth/btservice/AdapterService;Landroid/bluetooth/BluetoothDevice;III)V
    .registers 5
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter "x3"
    .parameter "x4"

    #@0
    .prologue
    .line 117
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/android/bluetooth/btservice/AdapterService;->processProfileStateChanged(Landroid/bluetooth/BluetoothDevice;III)V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/bluetooth/btservice/AdapterService;Landroid/bluetooth/BluetoothDevice;I)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 117
    invoke-direct {p0, p1, p2}, Lcom/android/bluetooth/btservice/AdapterService;->processConnectOtherProfiles(Landroid/bluetooth/BluetoothDevice;I)V

    #@3
    return-void
.end method

.method static synthetic access$500(Lcom/android/bluetooth/btservice/AdapterService;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 117
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/AdapterService;->isAvailable()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method private addDummyPairedListNative()Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private adjustOtherHeadsetPriorities(Lcom/android/bluetooth/hfp/HeadsetService;Landroid/bluetooth/BluetoothDevice;)V
    .registers 9
    .parameter "hsService"
    .parameter "connectedDevice"

    #@0
    .prologue
    .line 1764
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->getBondedDevices()[Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    .local v0, arr$:[Landroid/bluetooth/BluetoothDevice;
    array-length v3, v0

    #@5
    .local v3, len$:I
    const/4 v2, 0x0

    #@6
    .local v2, i$:I
    :goto_6
    if-ge v2, v3, :cond_20

    #@8
    aget-object v1, v0, v2

    #@a
    .line 1765
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p1, v1}, Lcom/android/bluetooth/hfp/HeadsetService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@d
    move-result v4

    #@e
    const/16 v5, 0x3e8

    #@10
    if-lt v4, v5, :cond_1d

    #@12
    invoke-virtual {v1, p2}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v4

    #@16
    if-nez v4, :cond_1d

    #@18
    .line 1767
    const/16 v4, 0x64

    #@1a
    invoke-virtual {p1, v1, v4}, Lcom/android/bluetooth/hfp/HeadsetService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@1d
    .line 1764
    :cond_1d
    add-int/lit8 v2, v2, 0x1

    #@1f
    goto :goto_6

    #@20
    .line 1770
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    :cond_20
    return-void
.end method

.method private adjustOtherSinkPriorities(Lcom/android/bluetooth/a2dp/A2dpService;Landroid/bluetooth/BluetoothDevice;)V
    .registers 9
    .parameter "a2dpService"
    .parameter "connectedDevice"

    #@0
    .prologue
    .line 1774
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->getBondedDevices()[Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    .local v0, arr$:[Landroid/bluetooth/BluetoothDevice;
    array-length v3, v0

    #@5
    .local v3, len$:I
    const/4 v2, 0x0

    #@6
    .local v2, i$:I
    :goto_6
    if-ge v2, v3, :cond_20

    #@8
    aget-object v1, v0, v2

    #@a
    .line 1775
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {p1, v1}, Lcom/android/bluetooth/a2dp/A2dpService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@d
    move-result v4

    #@e
    const/16 v5, 0x3e8

    #@10
    if-lt v4, v5, :cond_1d

    #@12
    invoke-virtual {v1, p2}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    #@15
    move-result v4

    #@16
    if-nez v4, :cond_1d

    #@18
    .line 1777
    const/16 v4, 0x64

    #@1a
    invoke-virtual {p1, v1, v4}, Lcom/android/bluetooth/a2dp/A2dpService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@1d
    .line 1774
    :cond_1d
    add-int/lit8 v2, v2, 0x1

    #@1f
    goto :goto_6

    #@20
    .line 1780
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    :cond_20
    return-void
.end method

.method private authorizeServiceNative([BIZZ)Z
	.registers 6
	const/4 v0, 0x1
	return v0
.end method

.method private autoConnectA2dp()V
    .registers 10

    #@0
    .prologue
    .line 1708
    invoke-static {}, Lcom/android/bluetooth/a2dp/A2dpService;->getA2dpService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@3
    move-result-object v0

    #@4
    .line 1709
    .local v0, a2dpSservice:Lcom/android/bluetooth/a2dp/A2dpService;
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->getBondedDevices()[Landroid/bluetooth/BluetoothDevice;

    #@7
    move-result-object v2

    #@8
    .line 1710
    .local v2, bondedDevices:[Landroid/bluetooth/BluetoothDevice;
    if-eqz v2, :cond_c

    #@a
    if-nez v0, :cond_d

    #@c
    .line 1719
    :cond_c
    return-void

    #@d
    .line 1713
    :cond_d
    move-object v1, v2

    #@e
    .local v1, arr$:[Landroid/bluetooth/BluetoothDevice;
    array-length v5, v1

    #@f
    .local v5, len$:I
    const/4 v4, 0x0

    #@10
    .local v4, i$:I
    :goto_10
    if-ge v4, v5, :cond_c

    #@12
    aget-object v3, v1, v4

    #@14
    .line 1714
    .local v3, device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0, v3}, Lcom/android/bluetooth/a2dp/A2dpService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@17
    move-result v6

    #@18
    const/16 v7, 0x3e8

    #@1a
    if-ne v6, v7, :cond_3b

    #@1c
    .line 1715
    const-string v6, "BluetoothAdapterService"

    #@1e
    new-instance v7, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v8, "Auto Connecting A2DP Profile with device "

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v3}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    #@2c
    move-result-object v8

    #@2d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v7

    #@31
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v7

    #@35
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 1716
    invoke-virtual {v0, v3}, Lcom/android/bluetooth/a2dp/A2dpService;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@3b
    .line 1713
    :cond_3b
    add-int/lit8 v4, v4, 0x1

    #@3d
    goto :goto_10
.end method

.method private autoConnectHeadset()V
    .registers 10

    #@0
    .prologue
    .line 1693
    invoke-static {}, Lcom/android/bluetooth/hfp/HeadsetService;->getHeadsetService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@3
    move-result-object v3

    #@4
    .line 1695
    .local v3, hsService:Lcom/android/bluetooth/hfp/HeadsetService;
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->getBondedDevices()[Landroid/bluetooth/BluetoothDevice;

    #@7
    move-result-object v1

    #@8
    .line 1696
    .local v1, bondedDevices:[Landroid/bluetooth/BluetoothDevice;
    if-eqz v1, :cond_c

    #@a
    if-nez v3, :cond_d

    #@c
    .line 1705
    :cond_c
    return-void

    #@d
    .line 1699
    :cond_d
    move-object v0, v1

    #@e
    .local v0, arr$:[Landroid/bluetooth/BluetoothDevice;
    array-length v5, v0

    #@f
    .local v5, len$:I
    const/4 v4, 0x0

    #@10
    .local v4, i$:I
    :goto_10
    if-ge v4, v5, :cond_c

    #@12
    aget-object v2, v0, v4

    #@14
    .line 1700
    .local v2, device:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v3, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@17
    move-result v6

    #@18
    const/16 v7, 0x3e8

    #@1a
    if-ne v6, v7, :cond_3b

    #@1c
    .line 1701
    const-string v6, "BluetoothAdapterService"

    #@1e
    new-instance v7, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v8, "Auto Connecting Headset Profile with device "

    #@25
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v7

    #@29
    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    #@2c
    move-result-object v8

    #@2d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v7

    #@31
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v7

    #@35
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 1702
    invoke-virtual {v3, v2}, Lcom/android/bluetooth/hfp/HeadsetService;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@3b
    .line 1699
    :cond_3b
    add-int/lit8 v4, v4, 0x1

    #@3d
    goto :goto_10
.end method

.method private cancelDiscoveryNative()Z
	.registers 2
	const/4 v0, 0x1
	return v0
.end method

.method private static classInitNative()V
	.registers 0
	return-void
.end method

.method private cleanupNative()V
	.registers 1
	return-void
.end method

.method private static declared-synchronized clearAdapterService()V
    .registers 2

    #@0
    .prologue
    .line 225
    const-class v0, Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    monitor-enter v0

    #@3
    const/4 v1, 0x0

    #@4
    :try_start_4
    sput-object v1, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;
    :try_end_6
    .catchall {:try_start_4 .. :try_end_6} :catchall_8

    #@6
    .line 226
    monitor-exit v0

    #@7
    return-void

    #@8
    .line 225
    :catchall_8
    move-exception v1

    #@9
    monitor-exit v0

    #@a
    throw v1
.end method

.method private connectSocketNative([BI[BII)I
	.registers 7
	const/4 v0, 0x1
	return v0
.end method

.method static convertScanModeFromHal(I)I
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 2056
    packed-switch p0, :pswitch_data_e

    #@3
    .line 2067
    const/4 v0, -0x1

    #@4
    :goto_4
    return v0

    #@5
    .line 2058
    :pswitch_5
    const/16 v0, 0x14

    #@7
    goto :goto_4

    #@8
    .line 2060
    :pswitch_8
    const/16 v0, 0x15

    #@a
    goto :goto_4

    #@b
    .line 2062
    :pswitch_b
    const/16 v0, 0x17

    #@d
    goto :goto_4

    #@e
    .line 2056
    :pswitch_data_e
    .packed-switch 0x0
        :pswitch_5
        :pswitch_8
        :pswitch_b
    .end packed-switch
.end method

.method private static convertScanModeToHal(I)I
    .registers 2
    .parameter "mode"

    #@0
    .prologue
    .line 2041
    packed-switch p0, :pswitch_data_c

    #@3
    .line 2052
    :pswitch_3
    const/4 v0, -0x1

    #@4
    :goto_4
    return v0

    #@5
    .line 2043
    :pswitch_5
    const/4 v0, 0x0

    #@6
    goto :goto_4

    #@7
    .line 2045
    :pswitch_7
    const/4 v0, 0x1

    #@8
    goto :goto_4

    #@9
    .line 2047
    :pswitch_9
    const/4 v0, 0x2

    #@a
    goto :goto_4

    #@b
    .line 2041
    nop

    #@c
    :pswitch_data_c
    .packed-switch 0x14
        :pswitch_5
        :pswitch_7
        :pswitch_3
        :pswitch_9
    .end packed-switch
.end method

.method private createSocketChannelNative(ILjava/lang/String;[BII)I
	.registers 6
	const/4 v0, 0x1
	return v0
.end method

.method private debugLog(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 2081
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "BluetoothAdapterService("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@e
    move-result v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, ")"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 2082
    return-void
.end method

.method private errorLog(Ljava/lang/String;)V
    .registers 4
    .parameter "msg"

    #@0
    .prologue
    .line 2085
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "BluetoothAdapterService("

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    #@e
    move-result v1

    #@f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v0

    #@13
    const-string v1, ")"

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v0

    #@1d
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 2086
    return-void
.end method

.method public static declared-synchronized getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;
    .registers 4

    #@0
    .prologue
    .line 191
    const-class v1, Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@5
    if-eqz v0, :cond_2b

    #@7
    sget-object v0, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@9
    iget-boolean v0, v0, Lcom/android/bluetooth/btservice/AdapterService;->mCleaningUp:Z

    #@b
    if-nez v0, :cond_2b

    #@d
    .line 193
    const-string v0, "BluetoothAdapterService"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "getAdapterService(): returning "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    sget-object v3, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@27
    .line 195
    sget-object v0, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;
    :try_end_29
    .catchall {:try_start_3 .. :try_end_29} :catchall_46

    #@29
    .line 204
    :goto_29
    monitor-exit v1

    #@2a
    return-object v0

    #@2b
    .line 198
    :cond_2b
    :try_start_2b
    sget-object v0, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@2d
    if-nez v0, :cond_38

    #@2f
    .line 199
    const-string v0, "BluetoothAdapterService"

    #@31
    const-string v2, "getAdapterService(): service not available"

    #@33
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 204
    :cond_36
    :goto_36
    const/4 v0, 0x0

    #@37
    goto :goto_29

    #@38
    .line 200
    :cond_38
    sget-object v0, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@3a
    iget-boolean v0, v0, Lcom/android/bluetooth/btservice/AdapterService;->mCleaningUp:Z

    #@3c
    if-eqz v0, :cond_36

    #@3e
    .line 201
    const-string v0, "BluetoothAdapterService"

    #@40
    const-string v2, "getAdapterService(): service is cleaning up"

    #@42
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_45
    .catchall {:try_start_2b .. :try_end_45} :catchall_46

    #@45
    goto :goto_36

    #@46
    .line 191
    :catchall_46
    move-exception v0

    #@47
    monitor-exit v1

    #@48
    throw v0
.end method

.method private initNative()Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 839
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCleaningUp:Z

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x1

    #@5
    :goto_5
    return v0

    #@6
    :cond_6
    const/4 v0, 0x0

    #@7
    goto :goto_5
.end method

.method private pinReplyNative([BZI[B)Z
	.registers 6
	const/4 v0, 0x1
	return v0
.end method

.method private processConnectOtherProfiles(Landroid/bluetooth/BluetoothDevice;I)V
    .registers 10
    .parameter "device"
    .parameter "firstProfileStatus"

    #@0
    .prologue
    const/16 v6, 0x64

    #@2
    .line 1732
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->getState()I

    #@5
    move-result v4

    #@6
    const/16 v5, 0xc

    #@8
    if-eq v4, v5, :cond_b

    #@a
    .line 1760
    :cond_a
    :goto_a
    return-void

    #@b
    .line 1735
    :cond_b
    invoke-static {}, Lcom/android/bluetooth/hfp/HeadsetService;->getHeadsetService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@e
    move-result-object v3

    #@f
    .line 1736
    .local v3, hsService:Lcom/android/bluetooth/hfp/HeadsetService;
    invoke-static {}, Lcom/android/bluetooth/a2dp/A2dpService;->getA2dpService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@12
    move-result-object v1

    #@13
    .line 1738
    .local v1, a2dpService:Lcom/android/bluetooth/a2dp/A2dpService;
    if-eqz v3, :cond_a

    #@15
    if-eqz v1, :cond_a

    #@17
    .line 1741
    invoke-virtual {v1}, Lcom/android/bluetooth/a2dp/A2dpService;->getConnectedDevices()Ljava/util/List;

    #@1a
    move-result-object v0

    #@1b
    .line 1742
    .local v0, a2dpConnDevList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-virtual {v3}, Lcom/android/bluetooth/hfp/HeadsetService;->getConnectedDevices()Ljava/util/List;

    #@1e
    move-result-object v2

    #@1f
    .line 1748
    .local v2, hfConnDevList:Ljava/util/List;,"Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    #@22
    move-result v4

    #@23
    if-eqz v4, :cond_2e

    #@25
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@28
    move-result v4

    #@29
    if-eqz v4, :cond_2e

    #@2b
    const/4 v4, 0x1

    #@2c
    if-eq v4, p2, :cond_a

    #@2e
    .line 1752
    :cond_2e
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    #@31
    move-result v4

    #@32
    if-eqz v4, :cond_3e

    #@34
    invoke-virtual {v3, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@37
    move-result v4

    #@38
    if-lt v4, v6, :cond_3e

    #@3a
    .line 1754
    invoke-virtual {v3, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@3d
    goto :goto_a

    #@3e
    .line 1756
    :cond_3e
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    #@41
    move-result v4

    #@42
    if-eqz v4, :cond_a

    #@44
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@47
    move-result v4

    #@48
    if-lt v4, v6, :cond_a

    #@4a
    .line 1758
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->connect(Landroid/bluetooth/BluetoothDevice;)Z

    #@4d
    goto :goto_a
.end method

.method private processProfileServiceStateChanged(Ljava/lang/String;I)V
    .registers 15
    .parameter "serviceName"
    .parameter "state"

    #@0
    .prologue
    const/4 v11, 0x2

    #@1
    const/4 v10, 0x1

    #@2
    .line 298
    const/4 v0, 0x0

    #@3
    .line 302
    .local v0, doUpdate:Z
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@5
    monitor-enter v8

    #@6
    .line 303
    :try_start_6
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@8
    invoke-virtual {v7, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@b
    move-result-object v6

    #@c
    check-cast v6, Ljava/lang/Integer;

    #@e
    .line 304
    .local v6, prevState:Ljava/lang/Integer;
    if-eqz v6, :cond_20

    #@10
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@13
    move-result v7

    #@14
    if-eq v7, p2, :cond_20

    #@16
    .line 305
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@18
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v9

    #@1c
    invoke-virtual {v7, p1, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1f
    .line 306
    const/4 v0, 0x1

    #@20
    .line 308
    :cond_20
    monitor-exit v8
    :try_end_21
    .catchall {:try_start_6 .. :try_end_21} :catchall_50

    #@21
    .line 310
    const-string v7, "BluetoothAdapterService"

    #@23
    new-instance v8, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v9, "onProfileServiceStateChange: serviceName="

    #@2a
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v8

    #@2e
    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v8

    #@32
    const-string v9, ", state = "

    #@34
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v8

    #@38
    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v8

    #@3c
    const-string v9, ", doUpdate = "

    #@3e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v8

    #@42
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@45
    move-result-object v8

    #@46
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v8

    #@4a
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 313
    if-nez v0, :cond_53

    #@4f
    .line 417
    :cond_4f
    :goto_4f
    return-void

    #@50
    .line 308
    .end local v6           #prevState:Ljava/lang/Integer;
    :catchall_50
    move-exception v7

    #@51
    :try_start_51
    monitor-exit v8
    :try_end_52
    .catchall {:try_start_51 .. :try_end_52} :catchall_50

    #@52
    throw v7

    #@53
    .line 317
    .restart local v6       #prevState:Ljava/lang/Integer;
    :cond_53
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@55
    monitor-enter v8

    #@56
    .line 318
    :try_start_56
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@58
    invoke-virtual {v7}, Lcom/android/bluetooth/btservice/AdapterState;->isTurningOff()Z

    #@5b
    move-result v3

    #@5c
    .line 319
    .local v3, isTurningOff:Z
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@5e
    invoke-virtual {v7}, Lcom/android/bluetooth/btservice/AdapterState;->isTurningOn()Z

    #@61
    move-result v4

    #@62
    .line 320
    .local v4, isTurningOn:Z
    monitor-exit v8
    :try_end_63
    .catchall {:try_start_56 .. :try_end_63} :catchall_af

    #@63
    .line 322
    if-eqz v3, :cond_cb

    #@65
    .line 326
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@67
    monitor-enter v8

    #@68
    .line 327
    :try_start_68
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@6a
    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@6d
    move-result-object v7

    #@6e
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@71
    move-result-object v2

    #@72
    .line 328
    .local v2, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :cond_72
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@75
    move-result v7

    #@76
    if-eqz v7, :cond_b2

    #@78
    .line 329
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@7b
    move-result-object v1

    #@7c
    check-cast v1, Ljava/util/Map$Entry;

    #@7e
    .line 330
    .local v1, entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/16 v9, 0xa

    #@80
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@83
    move-result-object v7

    #@84
    check-cast v7, Ljava/lang/Integer;

    #@86
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@89
    move-result v7

    #@8a
    if-eq v9, v7, :cond_72

    #@8c
    .line 331
    const-string v9, "BluetoothAdapterService"

    #@8e
    new-instance v7, Ljava/lang/StringBuilder;

    #@90
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@93
    const-string v10, "Profile still running: "

    #@95
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@98
    move-result-object v10

    #@99
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@9c
    move-result-object v7

    #@9d
    check-cast v7, Ljava/lang/String;

    #@9f
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a2
    move-result-object v7

    #@a3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a6
    move-result-object v7

    #@a7
    invoke-static {v9, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@aa
    .line 332
    monitor-exit v8

    #@ab
    goto :goto_4f

    #@ac
    .line 335
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v2           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :catchall_ac
    move-exception v7

    #@ad
    monitor-exit v8
    :try_end_ae
    .catchall {:try_start_68 .. :try_end_ae} :catchall_ac

    #@ae
    throw v7

    #@af
    .line 320
    .end local v3           #isTurningOff:Z
    .end local v4           #isTurningOn:Z
    :catchall_af
    move-exception v7

    #@b0
    :try_start_b0
    monitor-exit v8
    :try_end_b1
    .catchall {:try_start_b0 .. :try_end_b1} :catchall_af

    #@b1
    throw v7

    #@b2
    .line 335
    .restart local v2       #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .restart local v3       #isTurningOff:Z
    .restart local v4       #isTurningOn:Z
    :cond_b2
    :try_start_b2
    monitor-exit v8
    :try_end_b3
    .catchall {:try_start_b2 .. :try_end_b3} :catchall_ac

    #@b3
    .line 337
    const-string v7, "BluetoothAdapterService"

    #@b5
    const-string v8, "All profile services stopped..."

    #@b7
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ba
    .line 340
    const/4 v7, 0x0

    #@bb
    iput-boolean v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfilesStarted:Z

    #@bd
    .line 341
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@bf
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@c1
    const/16 v9, 0x19

    #@c3
    invoke-virtual {v8, v9}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@c6
    move-result-object v8

    #@c7
    invoke-virtual {v7, v8}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V

    #@ca
    goto :goto_4f

    #@cb
    .line 342
    .end local v2           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    :cond_cb
    if-eqz v4, :cond_4f

    #@cd
    .line 346
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@cf
    monitor-enter v8

    #@d0
    .line 347
    :try_start_d0
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@d2
    invoke-virtual {v7}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    #@d5
    move-result-object v7

    #@d6
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@d9
    move-result-object v2

    #@da
    .line 349
    .restart local v2       #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    const/4 v5, 0x1

    #@db
    .line 351
    .local v5, needToReturn:Z
    :cond_db
    :goto_db
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    #@de
    move-result v7

    #@df
    if-eqz v7, :cond_1fa

    #@e1
    .line 352
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@e4
    move-result-object v1

    #@e5
    check-cast v1, Ljava/util/Map$Entry;

    #@e7
    .line 353
    .restart local v1       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    const/16 v9, 0xc

    #@e9
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    #@ec
    move-result-object v7

    #@ed
    check-cast v7, Ljava/lang/Integer;

    #@ef
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    #@f2
    move-result v7

    #@f3
    if-eq v9, v7, :cond_db

    #@f5
    .line 355
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@f8
    move-result-object v7

    #@f9
    if-eqz v7, :cond_126

    #@fb
    .line 356
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@fe
    move-result-object v7

    #@ff
    const/4 v9, 0x1

    #@100
    invoke-interface {v7, v9}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@103
    move-result v7

    #@104
    if-eqz v7, :cond_126

    #@106
    .line 358
    const-string v7, "com.android.bluetooth.hfp.HeadsetService"

    #@108
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@10b
    move-result-object v9

    #@10c
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10f
    move-result v7

    #@110
    if-nez v7, :cond_11e

    #@112
    const-string v7, "com.android.bluetooth.a2dp.A2dpService"

    #@114
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@117
    move-result-object v9

    #@118
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11b
    move-result v7

    #@11c
    if-eqz v7, :cond_126

    #@11e
    .line 359
    :cond_11e
    const-string v7, "BluetoothAdapterService"

    #@120
    const-string v9, "MDM: Audio Profiles do not need state check"

    #@122
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@125
    .line 360
    const/4 v5, 0x0

    #@126
    .line 364
    :cond_126
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@129
    move-result-object v7

    #@12a
    if-eqz v7, :cond_186

    #@12c
    .line 365
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@12f
    move-result-object v7

    #@130
    const/4 v9, 0x0

    #@131
    invoke-interface {v7, v9}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@134
    move-result v7

    #@135
    if-nez v7, :cond_142

    #@137
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@13a
    move-result-object v7

    #@13b
    const/4 v9, 0x2

    #@13c
    invoke-interface {v7, v9}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@13f
    move-result v7

    #@140
    if-eqz v7, :cond_186

    #@142
    .line 368
    :cond_142
    const-string v7, "com.broadcom.bt.service.ftp.FTPService"

    #@144
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@147
    move-result-object v9

    #@148
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14b
    move-result v7

    #@14c
    if-nez v7, :cond_17e

    #@14e
    const-string v7, "com.broadcom.bt.service.map.MapService"

    #@150
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@153
    move-result-object v9

    #@154
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@157
    move-result v7

    #@158
    if-nez v7, :cond_17e

    #@15a
    const-string v7, "com.broadcom.bt.service.gatt.GattService"

    #@15c
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@15f
    move-result-object v9

    #@160
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@163
    move-result v7

    #@164
    if-nez v7, :cond_17e

    #@166
    const-string v7, "com.android.bluetooth.hdp.HealthService"

    #@168
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@16b
    move-result-object v9

    #@16c
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@16f
    move-result v7

    #@170
    if-nez v7, :cond_17e

    #@172
    const-string v7, "com.broadcom.bt.service.sap.SapService"

    #@174
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@177
    move-result-object v9

    #@178
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@17b
    move-result v7

    #@17c
    if-eqz v7, :cond_186

    #@17e
    .line 370
    :cond_17e
    const-string v7, "BluetoothAdapterService"

    #@180
    const-string v9, "MDM: File transfer profiles do not need state check"

    #@182
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@185
    .line 371
    const/4 v5, 0x0

    #@186
    .line 375
    :cond_186
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@189
    move-result-object v7

    #@18a
    if-eqz v7, :cond_1ab

    #@18c
    .line 376
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@18f
    move-result-object v7

    #@190
    const/4 v9, 0x4

    #@191
    invoke-interface {v7, v9}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@194
    move-result v7

    #@195
    if-eqz v7, :cond_1ab

    #@197
    .line 378
    const-string v7, "com.android.bluetooth.pan.PanService"

    #@199
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@19c
    move-result-object v9

    #@19d
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1a0
    move-result v7

    #@1a1
    if-eqz v7, :cond_1ab

    #@1a3
    .line 379
    const-string v7, "BluetoothAdapterService"

    #@1a5
    const-string v9, "MDM: Network profiles do not need state check"

    #@1a7
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1aa
    .line 380
    const/4 v5, 0x0

    #@1ab
    .line 384
    :cond_1ab
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1ae
    move-result-object v7

    #@1af
    if-eqz v7, :cond_1d1

    #@1b1
    .line 385
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1b4
    move-result-object v7

    #@1b5
    const/16 v9, 0x8

    #@1b7
    invoke-interface {v7, v9}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@1ba
    move-result v7

    #@1bb
    if-eqz v7, :cond_1d1

    #@1bd
    .line 387
    const-string v7, "com.android.bluetooth.hid.HidService"

    #@1bf
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@1c2
    move-result-object v9

    #@1c3
    invoke-virtual {v7, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1c6
    move-result v7

    #@1c7
    if-eqz v7, :cond_1d1

    #@1c9
    .line 388
    const-string v7, "BluetoothAdapterService"

    #@1cb
    const-string v9, "MDM: HID profile does not need state check"

    #@1cd
    invoke-static {v7, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d0
    .line 389
    const/4 v5, 0x0

    #@1d1
    .line 396
    :cond_1d1
    if-eqz v5, :cond_1f7

    #@1d3
    .line 397
    const-string v9, "BluetoothAdapterService"

    #@1d5
    new-instance v7, Ljava/lang/StringBuilder;

    #@1d7
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1da
    const-string v10, "Profile still not running:"

    #@1dc
    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v10

    #@1e0
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    #@1e3
    move-result-object v7

    #@1e4
    check-cast v7, Ljava/lang/String;

    #@1e6
    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e9
    move-result-object v7

    #@1ea
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ed
    move-result-object v7

    #@1ee
    invoke-static {v9, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f1
    .line 398
    monitor-exit v8

    #@1f2
    goto/16 :goto_4f

    #@1f4
    .line 409
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .end local v2           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .end local v5           #needToReturn:Z
    :catchall_1f4
    move-exception v7

    #@1f5
    monitor-exit v8
    :try_end_1f6
    .catchall {:try_start_d0 .. :try_end_1f6} :catchall_1f4

    #@1f6
    throw v7

    #@1f7
    .line 401
    .restart local v1       #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    .restart local v2       #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;>;"
    .restart local v5       #needToReturn:Z
    :cond_1f7
    const/4 v5, 0x1

    #@1f8
    goto/16 :goto_db

    #@1fa
    .line 409
    .end local v1           #entry:Ljava/util/Map$Entry;,"Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Integer;>;"
    :cond_1fa
    :try_start_1fa
    monitor-exit v8
    :try_end_1fb
    .catchall {:try_start_1fa .. :try_end_1fb} :catchall_1f4

    #@1fb
    .line 411
    const-string v7, "BluetoothAdapterService"

    #@1fd
    const-string v8, "All profile services started."

    #@1ff
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@202
    .line 413
    iput-boolean v10, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfilesStarted:Z

    #@204
    .line 415
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@206
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@208
    invoke-virtual {v8, v11}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@20b
    move-result-object v8

    #@20c
    invoke-virtual {v7, v8}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V

    #@20f
    goto/16 :goto_4f
.end method

.method private processProfileStateChanged(Landroid/bluetooth/BluetoothDevice;III)V
    .registers 12
    .parameter "device"
    .parameter "profileId"
    .parameter "newState"
    .parameter "prevState"

    #@0
    .prologue
    const/4 v6, 0x2

    #@1
    const/4 v5, 0x1

    #@2
    .line 267
    const/4 v2, 0x3

    #@3
    if-le p2, v2, :cond_2d

    #@5
    .line 268
    const-string v2, "BluetoothAdapterService"

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v4, "[BTUI] processProfileStateChanged : profileId("

    #@e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v3

    #@12
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@15
    move-result-object v3

    #@16
    const-string v4, ") state("

    #@18
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v3

    #@20
    const-string v4, ") [4:HID, 5:PAN, 6:PBAP, 7:FTP, 8:MAP]"

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 272
    :cond_2d
    if-eq p2, v6, :cond_31

    #@2f
    if-ne p2, v5, :cond_3e

    #@31
    :cond_31
    if-ne p3, v6, :cond_3e

    #@33
    .line 275
    const-string v2, "Profile connected. Schedule missing profile connection if any"

    #@35
    invoke-direct {p0, v2}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@38
    .line 277
    invoke-virtual {p0, p1, v5}, Lcom/android/bluetooth/btservice/AdapterService;->connectOtherProfile(Landroid/bluetooth/BluetoothDevice;I)V

    #@3b
    .line 278
    invoke-virtual {p0, p1, p2}, Lcom/android/bluetooth/btservice/AdapterService;->setProfileAutoConnectionPriority(Landroid/bluetooth/BluetoothDevice;I)V

    #@3e
    .line 280
    :cond_3e
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBinder:Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;

    #@40
    .line 281
    .local v0, binder:Landroid/bluetooth/IBluetooth$Stub;
    if-eqz v0, :cond_45

    #@42
    .line 283
    :try_start_42
    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/bluetooth/IBluetooth$Stub;->sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V
    :try_end_45
    .catch Landroid/os/RemoteException; {:try_start_42 .. :try_end_45} :catch_46

    #@45
    .line 288
    :cond_45
    :goto_45
    return-void

    #@46
    .line 284
    :catch_46
    move-exception v1

    #@47
    .line 285
    .local v1, re:Landroid/os/RemoteException;
    const-string v2, "BluetoothAdapterService"

    #@49
    const-string v3, ""

    #@4b
    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4e
    goto :goto_45
.end method

.method private static declared-synchronized setAdapterService(Lcom/android/bluetooth/btservice/AdapterService;)V
    .registers 5
    .parameter "instance"

    #@0
    .prologue
    .line 208
    const-class v1, Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    monitor-enter v1

    #@3
    if-eqz p0, :cond_27

    #@5
    :try_start_5
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCleaningUp:Z

    #@7
    if-nez v0, :cond_27

    #@9
    .line 210
    const-string v0, "BluetoothAdapterService"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "setAdapterService(): set to: "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    sget-object v3, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@18
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1b
    move-result-object v2

    #@1c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 212
    sput-object p0, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;
    :try_end_25
    .catchall {:try_start_5 .. :try_end_25} :catchall_33

    #@25
    .line 222
    :cond_25
    :goto_25
    monitor-exit v1

    #@26
    return-void

    #@27
    .line 215
    :cond_27
    :try_start_27
    sget-object v0, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@29
    if-nez v0, :cond_36

    #@2b
    .line 216
    const-string v0, "BluetoothAdapterService"

    #@2d
    const-string v2, "setAdapterService(): service not available"

    #@2f
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_32
    .catchall {:try_start_27 .. :try_end_32} :catchall_33

    #@32
    goto :goto_25

    #@33
    .line 208
    :catchall_33
    move-exception v0

    #@34
    monitor-exit v1

    #@35
    throw v0

    #@36
    .line 217
    :cond_36
    :try_start_36
    sget-object v0, Lcom/android/bluetooth/btservice/AdapterService;->sAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@38
    iget-boolean v0, v0, Lcom/android/bluetooth/btservice/AdapterService;->mCleaningUp:Z

    #@3a
    if-eqz v0, :cond_25

    #@3c
    .line 218
    const-string v0, "BluetoothAdapterService"

    #@3e
    const-string v2, "setAdapterService(): service is cleaning up"

    #@40
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_43
    .catchall {:try_start_36 .. :try_end_43} :catchall_33

    #@43
    goto :goto_25
.end method

.method private setProfileServiceState([Ljava/lang/Class;I)V
    .registers 16
    .parameter "services"
    .parameter "state"

    #@0
    .prologue
    const/16 v12, 0xc

    #@2
    const/4 v11, 0x1

    #@3
    const/16 v10, 0xa

    #@5
    .line 737
    if-eq p2, v12, :cond_11

    #@7
    if-eq p2, v10, :cond_11

    #@9
    .line 738
    const-string v7, "BluetoothAdapterService"

    #@b
    const-string v8, "setProfileServiceState(): invalid state...Leaving..."

    #@d
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 836
    :cond_10
    return-void

    #@11
    .line 742
    :cond_11
    const/16 v0, 0xa

    #@13
    .line 743
    .local v0, expectedCurrentState:I
    const/16 v4, 0xb

    #@15
    .line 745
    .local v4, pendingState:I
    const/4 v3, 0x0

    #@16
    .line 748
    .local v3, mIsMdmActivated:Z
    if-ne p2, v10, :cond_1c

    #@18
    .line 749
    const/16 v0, 0xc

    #@1a
    .line 750
    const/16 v4, 0xd

    #@1c
    .line 753
    :cond_1c
    const/4 v1, 0x0

    #@1d
    .local v1, i:I
    :goto_1d
    array-length v7, p1

    #@1e
    if-ge v1, v7, :cond_10

    #@20
    .line 754
    aget-object v7, p1, v1

    #@22
    invoke-virtual {v7}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@25
    move-result-object v5

    #@26
    .line 755
    .local v5, serviceName:Ljava/lang/String;
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@28
    invoke-virtual {v7, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    move-result-object v6

    #@2c
    check-cast v6, Ljava/lang/Integer;

    #@2e
    .line 758
    .local v6, serviceState:Ljava/lang/Integer;
    if-ne v12, p2, :cond_e7

    #@30
    .line 759
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@33
    move-result-object v7

    #@34
    if-eqz v7, :cond_58

    #@36
    .line 760
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@39
    move-result-object v7

    #@3a
    invoke-interface {v7, v11}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@3d
    move-result v7

    #@3e
    if-eqz v7, :cond_58

    #@40
    .line 762
    const-string v7, "com.android.bluetooth.hfp.HeadsetService"

    #@42
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@45
    move-result v7

    #@46
    if-nez v7, :cond_50

    #@48
    const-string v7, "com.android.bluetooth.a2dp.A2dpService"

    #@4a
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@4d
    move-result v7

    #@4e
    if-eqz v7, :cond_58

    #@50
    .line 763
    :cond_50
    const-string v7, "BluetoothAdapterService"

    #@52
    const-string v8, "Audio Profiles are blocked by LG MDM Server Policy"

    #@54
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    .line 764
    const/4 v3, 0x1

    #@58
    .line 768
    :cond_58
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@5b
    move-result-object v7

    #@5c
    if-eqz v7, :cond_a4

    #@5e
    .line 769
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@61
    move-result-object v7

    #@62
    const/4 v8, 0x0

    #@63
    invoke-interface {v7, v8}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@66
    move-result v7

    #@67
    if-nez v7, :cond_74

    #@69
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@6c
    move-result-object v7

    #@6d
    const/4 v8, 0x2

    #@6e
    invoke-interface {v7, v8}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@71
    move-result v7

    #@72
    if-eqz v7, :cond_a4

    #@74
    .line 772
    :cond_74
    const-string v7, "com.broadcom.bt.service.ftp.FTPService"

    #@76
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@79
    move-result v7

    #@7a
    if-nez v7, :cond_9c

    #@7c
    const-string v7, "com.broadcom.bt.service.map.MapService"

    #@7e
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@81
    move-result v7

    #@82
    if-nez v7, :cond_9c

    #@84
    const-string v7, "com.broadcom.bt.service.gatt.GattService"

    #@86
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@89
    move-result v7

    #@8a
    if-nez v7, :cond_9c

    #@8c
    const-string v7, "com.android.bluetooth.hdp.HealthService"

    #@8e
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@91
    move-result v7

    #@92
    if-nez v7, :cond_9c

    #@94
    const-string v7, "com.broadcom.bt.service.sap.SapService"

    #@96
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@99
    move-result v7

    #@9a
    if-eqz v7, :cond_a4

    #@9c
    .line 774
    :cond_9c
    const-string v7, "BluetoothAdapterService"

    #@9e
    const-string v8, "File transfer profiles are blocked by LG MDM Server Policy"

    #@a0
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a3
    .line 775
    const/4 v3, 0x1

    #@a4
    .line 779
    :cond_a4
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a7
    move-result-object v7

    #@a8
    if-eqz v7, :cond_c5

    #@aa
    .line 780
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@ad
    move-result-object v7

    #@ae
    const/4 v8, 0x4

    #@af
    invoke-interface {v7, v8}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@b2
    move-result v7

    #@b3
    if-eqz v7, :cond_c5

    #@b5
    .line 782
    const-string v7, "com.android.bluetooth.pan.PanService"

    #@b7
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ba
    move-result v7

    #@bb
    if-eqz v7, :cond_c5

    #@bd
    .line 783
    const-string v7, "BluetoothAdapterService"

    #@bf
    const-string v8, "Network profiles are blocked by LG MDM Server Policy"

    #@c1
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c4
    .line 784
    const/4 v3, 0x1

    #@c5
    .line 788
    :cond_c5
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@c8
    move-result-object v7

    #@c9
    if-eqz v7, :cond_e7

    #@cb
    .line 789
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@ce
    move-result-object v7

    #@cf
    const/16 v8, 0x8

    #@d1
    invoke-interface {v7, v8}, Lcom/lge/cappuccino/IMdm;->checkBluetoothProfileStatus(I)Z

    #@d4
    move-result v7

    #@d5
    if-eqz v7, :cond_e7

    #@d7
    .line 791
    const-string v7, "com.android.bluetooth.hid.HidService"

    #@d9
    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@dc
    move-result v7

    #@dd
    if-eqz v7, :cond_e7

    #@df
    .line 792
    const-string v7, "BluetoothAdapterService"

    #@e1
    const-string v8, "HID profile is blocked by LG MDM Server Policy"

    #@e3
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@e6
    .line 793
    const/4 v3, 0x1

    #@e7
    .line 800
    :cond_e7
    if-eqz v6, :cond_164

    #@e9
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@ec
    move-result v7

    #@ed
    if-eq v7, v0, :cond_164

    #@ef
    .line 801
    const-string v8, "BluetoothAdapterService"

    #@f1
    new-instance v7, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v9, "Unable to "

    #@f8
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v9

    #@fc
    if-ne p2, v10, :cond_15b

    #@fe
    const-string v7, "start"

    #@100
    :goto_100
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v7

    #@104
    const-string v9, " service "

    #@106
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@109
    move-result-object v7

    #@10a
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10d
    move-result-object v7

    #@10e
    const-string v9, ". Invalid state: "

    #@110
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@113
    move-result-object v7

    #@114
    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@117
    move-result-object v7

    #@118
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11b
    move-result-object v7

    #@11c
    invoke-static {v8, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@11f
    .line 804
    const-string v8, "BluetoothAdapterService"

    #@121
    new-instance v7, Ljava/lang/StringBuilder;

    #@123
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@126
    const-string v9, "[BTUI] Unable to "

    #@128
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12b
    move-result-object v9

    #@12c
    if-ne p2, v10, :cond_15e

    #@12e
    const-string v7, "stop"

    #@130
    :goto_130
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@133
    move-result-object v7

    #@134
    const-string v9, " service "

    #@136
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v7

    #@13a
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13d
    move-result-object v7

    #@13e
    const-string v9, ". Already service "

    #@140
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@143
    move-result-object v9

    #@144
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    #@147
    move-result v7

    #@148
    if-ne v7, v10, :cond_161

    #@14a
    const-string v7, "stopped"

    #@14c
    :goto_14c
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v7

    #@150
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@153
    move-result-object v7

    #@154
    invoke-static {v8, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@157
    .line 753
    :goto_157
    add-int/lit8 v1, v1, 0x1

    #@159
    goto/16 :goto_1d

    #@15b
    .line 801
    :cond_15b
    const-string v7, "stop"

    #@15d
    goto :goto_100

    #@15e
    .line 804
    :cond_15e
    const-string v7, "start"

    #@160
    goto :goto_130

    #@161
    :cond_161
    const-string v7, "started"

    #@163
    goto :goto_14c

    #@164
    .line 811
    :cond_164
    const-string v8, "BluetoothAdapterService"

    #@166
    new-instance v9, Ljava/lang/StringBuilder;

    #@168
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@16b
    if-ne p2, v10, :cond_1a9

    #@16d
    const-string v7, "Stopping"

    #@16f
    :goto_16f
    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@172
    move-result-object v7

    #@173
    const-string v9, " service "

    #@175
    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@178
    move-result-object v7

    #@179
    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17c
    move-result-object v7

    #@17d
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@180
    move-result-object v7

    #@181
    invoke-static {v8, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@184
    .line 816
    if-nez v3, :cond_1a7

    #@186
    .line 817
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@188
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18b
    move-result-object v8

    #@18c
    invoke-virtual {v7, v5, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@18f
    .line 818
    new-instance v2, Landroid/content/Intent;

    #@191
    aget-object v7, p1, v1

    #@193
    invoke-direct {v2, p0, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@196
    .line 819
    .local v2, intent:Landroid/content/Intent;
    const-string v7, "action"

    #@198
    const-string v8, "com.android.bluetooth.btservice.action.STATE_CHANGED"

    #@19a
    invoke-virtual {v2, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@19d
    .line 820
    const-string v7, "android.bluetooth.adapter.extra.STATE"

    #@19f
    invoke-virtual {v2, v7, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1a2
    .line 821
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/btservice/AdapterService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    #@1a5
    .line 823
    iput-boolean v11, p0, Lcom/android/bluetooth/btservice/AdapterService;->mServiceChanged:Z

    #@1a7
    .line 826
    .end local v2           #intent:Landroid/content/Intent;
    :cond_1a7
    const/4 v3, 0x0

    #@1a8
    goto :goto_157

    #@1a9
    .line 811
    :cond_1a9
    const-string v7, "Starting"

    #@1ab
    goto :goto_16f
.end method

.method private sspDebugConfigureNative(Z)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method private sspReplyNative([BIZI)Z
	.registers 6
	const/4 v0, 0x1
	return v0
.end method

.method private startDiscoveryNative()Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method


# virtual methods
.method public addDummyPairedList()Z
    .registers 3

    #@0
    .prologue
    .line 1969
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1970
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/AdapterService;->addDummyPairedListNative()Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method public authorizeService(Landroid/bluetooth/BluetoothDevice;Landroid/os/ParcelUuid;ZZ)Z
    .registers 9
    .parameter "device"
    .parameter "ServiceUuid"
    .parameter "authorize"
    .parameter "autoReply"

    #@0
    .prologue
    .line 1950
    const-string v2, "android.permission.BLUETOOTH"

    #@2
    const-string v3, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1952
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@a
    move-result-object v2

    #@b
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@e
    move-result-object v0

    #@f
    .line 1955
    .local v0, addr:[B
    invoke-static {p2}, Lcom/broadcom/bt/service/Utils;->getServiceIdFromProfileUUID(Landroid/os/ParcelUuid;)I

    #@12
    move-result v1

    #@13
    .line 1958
    .local v1, serviceId:I
    invoke-direct {p0, v0, v1, p3, p4}, Lcom/android/bluetooth/btservice/AdapterService;->authorizeServiceNative([BIZZ)Z

    #@16
    move-result v2

    #@17
    return v2
.end method

.method public autoConnect()V
    .registers 3

    #@0
    .prologue
    .line 1669
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->getState()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0xc

    #@6
    if-eq v0, v1, :cond_e

    #@8
    .line 1670
    const-string v0, "BT is not ON. Exiting autoConnect"

    #@a
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->errorLog(Ljava/lang/String;)V

    #@d
    .line 1690
    :goto_d
    return-void

    #@e
    .line 1673
    :cond_e
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->isQuietModeEnabled()Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_21

    #@14
    .line 1675
    const-string v0, "Initiate auto connection on BT on..."

    #@16
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@19
    .line 1682
    const-string v0, "BluetoothAdapterService"

    #@1b
    const-string v1, "[BTUI] autoConnect() : not allowed"

    #@1d
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    goto :goto_d

    #@21
    .line 1687
    :cond_21
    const-string v0, "BT is in Quiet mode. Not initiating  auto connections"

    #@23
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@26
    goto :goto_d
.end method

.method cancelBondNative([B)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method cancelBondProcess(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 1802
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1803
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@a
    move-result-object v1

    #@b
    invoke-static {v1}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@e
    move-result-object v0

    #@f
    .line 1804
    .local v0, addr:[B
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->cancelBondNative([B)Z

    #@12
    move-result v1

    #@13
    return v1
.end method

.method cancelDiscovery()Z
    .registers 3

    #@0
    .prologue
    .line 1612
    const-string v0, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v1, "Need BLUETOOTH ADMIN permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1615
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/AdapterService;->cancelDiscoveryNative()Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method cleanup()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 585
    const-string v0, "cleanup()"

    #@3
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@6
    .line 587
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCleaningUp:Z

    #@8
    if-eqz v0, :cond_12

    #@a
    .line 588
    const-string v0, "BluetoothAdapterService"

    #@c
    const-string v1, "*************service already starting to cleanup... Ignoring cleanup request........."

    #@e
    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 654
    :cond_11
    :goto_11
    return-void

    #@12
    .line 592
    :cond_12
    const/4 v0, 0x1

    #@13
    iput-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCleaningUp:Z

    #@15
    .line 594
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@17
    if-eqz v0, :cond_23

    #@19
    .line 595
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@1b
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterState;->doQuit()V

    #@1e
    .line 596
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@20
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterState;->cleanup()V

    #@23
    .line 599
    :cond_23
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@25
    if-eqz v0, :cond_31

    #@27
    .line 600
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@29
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/BondStateMachine;->doQuit()V

    #@2c
    .line 601
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@2e
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/BondStateMachine;->cleanup()V

    #@31
    .line 604
    :cond_31
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@33
    if-eqz v0, :cond_3a

    #@35
    .line 605
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@37
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->cleanup()V

    #@3a
    .line 608
    :cond_3a
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mNativeAvailable:Z

    #@3c
    if-eqz v0, :cond_52

    #@3e
    .line 609
    const-string v0, "BluetoothAdapterService"

    #@40
    const-string v1, "Cleaning up adapter native...."

    #@42
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@45
    .line 610
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/AdapterService;->cleanupNative()V

    #@48
    .line 611
    const-string v0, "BluetoothAdapterService"

    #@4a
    const-string v1, "Done cleaning up adapter native...."

    #@4c
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 612
    const/4 v0, 0x0

    #@50
    iput-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mNativeAvailable:Z

    #@52
    .line 615
    :cond_52
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@54
    if-eqz v0, :cond_5b

    #@56
    .line 616
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@58
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->cleanup()V

    #@5b
    .line 619
    :cond_5b
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mJniCallbacks:Lcom/android/bluetooth/btservice/JniCallbacks;

    #@5d
    if-eqz v0, :cond_64

    #@5f
    .line 620
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mJniCallbacks:Lcom/android/bluetooth/btservice/JniCallbacks;

    #@61
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/JniCallbacks;->cleanup()V

    #@64
    .line 623
    :cond_64
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@66
    if-eqz v0, :cond_6d

    #@68
    .line 624
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@6a
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@6d
    .line 627
    :cond_6d
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->clearAdapterService()V

    #@70
    .line 629
    sget-object v0, Lcom/android/bluetooth/btservice/AdapterService;->mServiceAdapter:Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;

    #@72
    if-eqz v0, :cond_7b

    #@74
    .line 630
    sget-object v0, Lcom/android/bluetooth/btservice/AdapterService;->mServiceAdapter:Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;

    #@76
    invoke-virtual {v0}, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->cleanup()V

    #@79
    .line 631
    sput-object v2, Lcom/android/bluetooth/btservice/AdapterService;->mServiceAdapter:Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;

    #@7b
    .line 635
    :cond_7b
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBinder:Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;

    #@7d
    if-eqz v0, :cond_86

    #@7f
    .line 636
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBinder:Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;

    #@81
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;->cleanup()Z

    #@84
    .line 637
    iput-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBinder:Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;

    #@86
    .line 640
    :cond_86
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@88
    if-eqz v0, :cond_8f

    #@8a
    .line 641
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@8c
    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    #@8f
    .line 645
    :cond_8f
    const-string v0, "cleanup() done"

    #@91
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@94
    .line 648
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mFd:I

    #@96
    const/16 v1, 0x1f4

    #@98
    if-le v0, v1, :cond_11

    #@9a
    .line 650
    const-string v0, "FD is over 500."

    #@9c
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->errorLog(Ljava/lang/String;)V

    #@9f
    .line 651
    invoke-static {}, Landroid/os/Process;->myPid()I

    #@a2
    move-result v0

    #@a3
    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    #@a6
    goto/16 :goto_11
.end method

.method public connectOtherProfile(Landroid/bluetooth/BluetoothDevice;I)V
    .registers 7
    .parameter "device"
    .parameter "firstProfileStatus"

    #@0
    .prologue
    const/16 v2, 0x1e

    #@2
    .line 1722
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mHandler:Landroid/os/Handler;

    #@4
    invoke-virtual {v1, v2}, Landroid/os/Handler;->hasMessages(I)Z

    #@7
    move-result v1

    #@8
    if-nez v1, :cond_21

    #@a
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->isQuietModeEnabled()Z

    #@d
    move-result v1

    #@e
    if-nez v1, :cond_21

    #@10
    .line 1724
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mHandler:Landroid/os/Handler;

    #@12
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@15
    move-result-object v0

    #@16
    .line 1725
    .local v0, m:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@18
    .line 1726
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@1a
    .line 1727
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mHandler:Landroid/os/Handler;

    #@1c
    const-wide/16 v2, 0x1770

    #@1e
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@21
    .line 1729
    .end local v0           #m:Landroid/os/Message;
    :cond_21
    return-void
.end method

.method connectSocket(Landroid/bluetooth/BluetoothDevice;ILandroid/os/ParcelUuid;II)Landroid/os/ParcelFileDescriptor;
    .registers 13
    .parameter "device"
    .parameter "type"
    .parameter "uuid"
    .parameter "port"
    .parameter "flag"

    #@0
    .prologue
    .line 1993
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1994
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@a
    move-result-object v0

    #@b
    invoke-static {v0}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@e
    move-result-object v1

    #@f
    invoke-static {p3}, Lcom/android/bluetooth/Utils;->uuidToByteArray(Landroid/os/ParcelUuid;)[B

    #@12
    move-result-object v3

    #@13
    move-object v0, p0

    #@14
    move v2, p2

    #@15
    move v4, p4

    #@16
    move v5, p5

    #@17
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/btservice/AdapterService;->connectSocketNative([BI[BII)I

    #@1a
    move-result v6

    #@1b
    .line 1998
    .local v6, fd:I
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mFd:I

    #@1d
    if-le v6, v0, :cond_43

    #@1f
    .line 2000
    new-instance v0, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v1, "connectSocket FD="

    #@26
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v0

    #@2a
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v0

    #@2e
    const-string v1, " mFd="

    #@30
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v0

    #@34
    iget v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mFd:I

    #@36
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v0

    #@3a
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v0

    #@3e
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@41
    .line 2001
    iput v6, p0, Lcom/android/bluetooth/btservice/AdapterService;->mFd:I

    #@43
    .line 2004
    :cond_43
    if-gez v6, :cond_4c

    #@45
    .line 2005
    const-string v0, "Failed to connect socket"

    #@47
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->errorLog(Ljava/lang/String;)V

    #@4a
    .line 2006
    const/4 v0, 0x0

    #@4b
    .line 2008
    :goto_4b
    return-object v0

    #@4c
    :cond_4c
    invoke-static {v6}, Landroid/os/ParcelFileDescriptor;->adoptFd(I)Landroid/os/ParcelFileDescriptor;

    #@4f
    move-result-object v0

    #@50
    goto :goto_4b
.end method

.method createBond(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 7
    .parameter "device"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 1648
    const-string v3, "android.permission.BLUETOOTH_ADMIN"

    #@3
    const-string v4, "Need BLUETOOTH ADMIN permission"

    #@5
    invoke-virtual {p0, v3, v4}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 1650
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@a
    invoke-virtual {v3, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@d
    move-result-object v0

    #@e
    .line 1651
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-eqz v0, :cond_1a

    #@10
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getBondState()I

    #@13
    move-result v3

    #@14
    const/16 v4, 0xa

    #@16
    if-eq v3, v4, :cond_1a

    #@18
    .line 1652
    const/4 v2, 0x0

    #@19
    .line 1658
    :goto_19
    return v2

    #@1a
    .line 1655
    :cond_1a
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@1c
    invoke-virtual {v3, v2}, Lcom/android/bluetooth/btservice/BondStateMachine;->obtainMessage(I)Landroid/os/Message;

    #@1f
    move-result-object v1

    #@20
    .line 1656
    .local v1, msg:Landroid/os/Message;
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@22
    .line 1657
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@24
    invoke-virtual {v3, v1}, Lcom/android/bluetooth/btservice/BondStateMachine;->sendMessage(Landroid/os/Message;)V

    #@27
    goto :goto_19
.end method

.method createBondNative([B)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method createSocketChannel(ILjava/lang/String;Landroid/os/ParcelUuid;II)Landroid/os/ParcelFileDescriptor;
    .registers 13
    .parameter "type"
    .parameter "serviceName"
    .parameter "uuid"
    .parameter "port"
    .parameter "flag"

    #@0
    .prologue
    .line 2013
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 2014
    invoke-static {p3}, Lcom/android/bluetooth/Utils;->uuidToByteArray(Landroid/os/ParcelUuid;)[B

    #@a
    move-result-object v3

    #@b
    move-object v0, p0

    #@c
    move v1, p1

    #@d
    move-object v2, p2

    #@e
    move v4, p4

    #@f
    move v5, p5

    #@10
    invoke-direct/range {v0 .. v5}, Lcom/android/bluetooth/btservice/AdapterService;->createSocketChannelNative(ILjava/lang/String;[BII)I

    #@13
    move-result v6

    #@14
    .line 2018
    .local v6, fd:I
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mFd:I

    #@16
    if-le v6, v0, :cond_3c

    #@18
    .line 2020
    new-instance v0, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v1, "createSocketChannel FD="

    #@1f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v0

    #@23
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v0

    #@27
    const-string v1, " mFd="

    #@29
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v0

    #@2d
    iget v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mFd:I

    #@2f
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@32
    move-result-object v0

    #@33
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@3a
    .line 2021
    iput v6, p0, Lcom/android/bluetooth/btservice/AdapterService;->mFd:I

    #@3c
    .line 2025
    :cond_3c
    if-gez v6, :cond_45

    #@3e
    .line 2026
    const-string v0, "Failed to create socket channel"

    #@40
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->errorLog(Ljava/lang/String;)V

    #@43
    .line 2027
    const/4 v0, 0x0

    #@44
    .line 2029
    :goto_44
    return-object v0

    #@45
    :cond_45
    invoke-static {v6}, Landroid/os/ParcelFileDescriptor;->adoptFd(I)Landroid/os/ParcelFileDescriptor;

    #@48
    move-result-object v0

    #@49
    goto :goto_44
.end method

.method disable()Z
    .registers 4

    #@0
    .prologue
    .line 1516
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1520
    const-string v1, "disable() called..."

    #@9
    invoke-direct {p0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@c
    .line 1522
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@e
    const/16 v2, 0x14

    #@10
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    .line 1524
    .local v0, m:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@16
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V

    #@19
    .line 1525
    const/4 v1, 0x1

    #@1a
    return v1
.end method

.method disableNative()Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method disableRadio()Z
    .registers 4

    #@0
    .prologue
    .line 1442
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1445
    const-string v1, "disableRadio() called..."

    #@9
    invoke-direct {p0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@c
    .line 1447
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@e
    const/16 v2, 0xcb

    #@10
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    .line 1449
    .local v0, m:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@16
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V

    #@19
    .line 1450
    const/4 v1, 0x1

    #@1a
    return v1
.end method

.method disableRadioNative()Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method enable()Z
    .registers 2

    #@0
    .prologue
    .line 1425
    const/4 v0, 0x0

    #@1
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->enable(Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method public declared-synchronized enable(Z)Z
    .registers 10
    .parameter "quietMode"

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 1459
    monitor-enter p0

    #@3
    :try_start_3
    const-string v5, "android.permission.BLUETOOTH_ADMIN"

    #@5
    const-string v6, "Need BLUETOOTH ADMIN permission"

    #@7
    invoke-virtual {p0, v5, v6}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@a
    .line 1462
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@d
    move-result-object v5

    #@e
    if-eqz v5, :cond_26

    #@10
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@13
    move-result-object v5

    #@14
    const/4 v6, 0x0

    #@15
    const-string v7, "LGMDMBluetoothAdapter"

    #@17
    invoke-interface {v5, v6, v7}, Lcom/lge/cappuccino/IMdm;->checkDisabledSystemService(Landroid/content/ComponentName;Ljava/lang/String;)Z

    #@1a
    move-result v5

    #@1b
    if-eqz v5, :cond_26

    #@1d
    .line 1465
    const-string v4, "BluetoothAdapterService"

    #@1f
    const-string v5, "handleEnable is blocked by LG MDM Server Policy"

    #@21
    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_24
    .catchall {:try_start_3 .. :try_end_24} :catchall_72

    #@24
    .line 1512
    :goto_24
    monitor-exit p0

    #@25
    return v3

    #@26
    .line 1470
    :cond_26
    :try_start_26
    sget-boolean v5, Lcom/lge/config/ThreelmMdmConfig;->THREELM_MDM:Z

    #@28
    if-eqz v5, :cond_4a

    #@2a
    .line 1471
    const-string v5, "DeviceManager3LM"

    #@2c
    invoke-static {v5}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@2f
    move-result-object v5

    #@30
    invoke-static {v5}, Landroid/os/IDeviceManager3LM$Stub;->asInterface(Landroid/os/IBinder;)Landroid/os/IDeviceManager3LM;
    :try_end_33
    .catchall {:try_start_26 .. :try_end_33} :catchall_72

    #@33
    move-result-object v1

    #@34
    .line 1476
    .local v1, dm:Landroid/os/IDeviceManager3LM;
    if-eqz v1, :cond_4a

    #@36
    :try_start_36
    invoke-interface {v1}, Landroid/os/IDeviceManager3LM;->getBluetoothEnabled()Z

    #@39
    move-result v5

    #@3a
    if-nez v5, :cond_4a

    #@3c
    .line 1477
    invoke-interface {v1}, Landroid/os/IDeviceManager3LM;->getNotificationText()Ljava/lang/String;

    #@3f
    move-result-object v0

    #@40
    .line 1478
    .local v0, customText:Ljava/lang/String;
    new-instance v5, Lcom/android/bluetooth/btservice/AdapterService$3;

    #@42
    invoke-direct {v5, p0, v0}, Lcom/android/bluetooth/btservice/AdapterService$3;-><init>(Lcom/android/bluetooth/btservice/AdapterService;Ljava/lang/String;)V

    #@45
    invoke-virtual {v5}, Lcom/android/bluetooth/btservice/AdapterService$3;->start()V
    :try_end_48
    .catchall {:try_start_36 .. :try_end_48} :catchall_72
    .catch Landroid/os/RemoteException; {:try_start_36 .. :try_end_48} :catch_49

    #@48
    goto :goto_24

    #@49
    .line 1500
    .end local v0           #customText:Ljava/lang/String;
    :catch_49
    move-exception v3

    #@4a
    .line 1506
    .end local v1           #dm:Landroid/os/IDeviceManager3LM;
    :cond_4a
    :try_start_4a
    new-instance v3, Ljava/lang/StringBuilder;

    #@4c
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4f
    const-string v5, "Enable called with quiet mode status =  "

    #@51
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@54
    move-result-object v3

    #@55
    iget-boolean v5, p0, Lcom/android/bluetooth/btservice/AdapterService;->mQuietmode:Z

    #@57
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5a
    move-result-object v3

    #@5b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5e
    move-result-object v3

    #@5f
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@62
    .line 1508
    iput-boolean p1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mQuietmode:Z

    #@64
    .line 1509
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@66
    const/4 v5, 0x1

    #@67
    invoke-virtual {v3, v5}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@6a
    move-result-object v2

    #@6b
    .line 1511
    .local v2, m:Landroid/os/Message;
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@6d
    invoke-virtual {v3, v2}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V
    :try_end_70
    .catchall {:try_start_4a .. :try_end_70} :catchall_72

    #@70
    move v3, v4

    #@71
    .line 1512
    goto :goto_24

    #@72
    .line 1459
    .end local v2           #m:Landroid/os/Message;
    :catchall_72
    move-exception v3

    #@73
    monitor-exit p0

    #@74
    throw v3
.end method

.method enableNative()Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method public enableNoAutoConnect()Z
    .registers 2

    #@0
    .prologue
    .line 1455
    const/4 v0, 0x1

    #@1
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->enable(Z)Z

    #@4
    move-result v0

    #@5
    return v0
.end method

.method enableRadio()Z
    .registers 4

    #@0
    .prologue
    .line 1430
    const-string v1, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v2, "Need BLUETOOTH ADMIN permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1433
    const-string v1, "enableRadio() called..."

    #@9
    invoke-direct {p0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@c
    .line 1435
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@e
    const/16 v2, 0xca

    #@10
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@13
    move-result-object v0

    #@14
    .line 1437
    .local v0, m:Landroid/os/Message;
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@16
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V

    #@19
    .line 1438
    const/4 v1, 0x1

    #@1a
    return v1
.end method

.method enableRadioNative()Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method fetchRemoteUuids(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 1876
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1877
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->fetchUuids(Landroid/bluetooth/BluetoothDevice;)V

    #@c
    .line 1878
    const/4 v0, 0x1

    #@d
    return v0
.end method

.method protected finalize()V
    .registers 5

    #@0
    .prologue
    .line 2183
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->cleanup()V

    #@3
    .line 2185
    const-class v1, Lcom/android/bluetooth/btservice/AdapterService;

    #@5
    monitor-enter v1

    #@6
    .line 2186
    :try_start_6
    sget v0, Lcom/android/bluetooth/btservice/AdapterService;->sRefCount:I

    #@8
    add-int/lit8 v0, v0, -0x1

    #@a
    sput v0, Lcom/android/bluetooth/btservice/AdapterService;->sRefCount:I

    #@c
    .line 2187
    const-string v0, "BluetoothAdapterService"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "REFCOUNT: FINALIZED. INSTANCE_COUNT= "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    sget v3, Lcom/android/bluetooth/btservice/AdapterService;->sRefCount:I

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v2

    #@23
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 2188
    monitor-exit v1

    #@27
    .line 2190
    return-void

    #@28
    .line 2188
    :catchall_28
    move-exception v0

    #@29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_6 .. :try_end_2a} :catchall_28

    #@2a
    throw v0
.end method

.method getAdapterConnectionState()I
    .registers 3

    #@0
    .prologue
    .line 1631
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1633
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->getConnectionState()I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method getAdapterPropertiesNative()Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method getAdapterPropertyNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method getAddress()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 1529
    const-string v2, "android.permission.BLUETOOTH"

    #@2
    const-string v3, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1531
    const/4 v0, 0x0

    #@8
    .line 1532
    .local v0, addrString:Ljava/lang/String;
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@a
    invoke-virtual {v2}, Lcom/android/bluetooth/btservice/AdapterProperties;->getAddress()[B

    #@d
    move-result-object v1

    #@e
    .line 1533
    .local v1, address:[B
    invoke-static {v1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@11
    move-result-object v2

    #@12
    return-object v2
.end method

.method getBondState(Landroid/bluetooth/BluetoothDevice;)I
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 1820
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1821
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v0

    #@d
    .line 1822
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v0, :cond_12

    #@f
    .line 1823
    const/16 v1, 0xa

    #@11
    .line 1825
    :goto_11
    return v1

    #@12
    :cond_12
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getBondState()I

    #@15
    move-result v1

    #@16
    goto :goto_11
.end method

.method getBondedDevices()[Landroid/bluetooth/BluetoothDevice;
    .registers 3

    #@0
    .prologue
    .line 1625
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1626
    const-string v0, "Get Bonded Devices being called"

    #@9
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@c
    .line 1627
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@e
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->getBondedDevices()[Landroid/bluetooth/BluetoothDevice;

    #@11
    move-result-object v0

    #@12
    return-object v0
.end method

.method getConnectedDevices()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    .line 2072
    invoke-static {}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getDefaultChecker()Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@3
    move-result-object v2

    #@4
    .line 2073
    .local v2, profileConnectionChecker:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;
    invoke-virtual {v2}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getConnectedDeviceAmount()I

    #@7
    move-result v0

    #@8
    .line 2074
    .local v0, amount:I
    invoke-virtual {v2}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->getConnectedDeviceName()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    .line 2075
    .local v1, deviceName:Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/lge/bluetooth/LGBluetoothProfileConnectionManager;->parseConnectedDeviceName(ILjava/lang/String;)Ljava/lang/String;

    #@f
    move-result-object v3

    #@10
    return-object v3
.end method

.method getDevicePropertyNative([BI)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method getDiscoverableTimeout()I
    .registers 3

    #@0
    .prologue
    .line 1588
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1590
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->getDiscoverableTimeout()I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public getJustWorks(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 1938
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1939
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v0

    #@d
    .line 1940
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v0, :cond_11

    #@f
    .line 1941
    const/4 v1, 0x0

    #@10
    .line 1943
    :goto_10
    return v1

    #@11
    :cond_11
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getJustWorks()Z

    #@14
    move-result v1

    #@15
    goto :goto_10
.end method

.method getName()Ljava/lang/String;
    .registers 4

    #@0
    .prologue
    .line 1543
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1547
    :try_start_7
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@9
    invoke-virtual {v1}, Lcom/android/bluetooth/btservice/AdapterProperties;->getName()Ljava/lang/String;
    :try_end_c
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_c} :catch_e

    #@c
    move-result-object v1

    #@d
    .line 1551
    :goto_d
    return-object v1

    #@e
    .line 1548
    :catch_e
    move-exception v0

    #@f
    .line 1549
    .local v0, t:Ljava/lang/Throwable;
    const-string v1, "BluetoothAdapterService"

    #@11
    const-string v2, "Unexpected exception while calling getName()"

    #@13
    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@16
    .line 1551
    const/4 v1, 0x0

    #@17
    goto :goto_d
.end method

.method public getProfileClass(Ljava/lang/String;)[Ljava/lang/Class;
    .registers 5
    .parameter "profile_name"

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    .line 2105
    new-array v0, v2, [Ljava/lang/Class;

    #@3
    .line 2106
    .local v0, SUPPORTED_PROFILES:[Ljava/lang/Class;
    new-instance v1, Ljava/util/ArrayList;

    #@5
    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    #@8
    .line 2107
    .local v1, profile:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Class;>;"
    const-string v2, "PAN"

    #@a
    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_19

    #@10
    .line 2109
    const-class v2, Lcom/android/bluetooth/pan/PanService;

    #@12
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@15
    .line 2132
    :cond_15
    :goto_15
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@18
    .line 2133
    return-object v0

    #@19
    .line 2111
    :cond_19
    const-string v2, "SAP"

    #@1b
    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_2d

    #@21
    .line 2112
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isBRCMSolution()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_15

    #@27
    .line 2113
    const-class v2, Lcom/broadcom/bt/service/sap/SapService;

    #@29
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@2c
    goto :goto_15

    #@2d
    .line 2118
    :cond_2d
    const-string v2, "MAP"

    #@2f
    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_41

    #@35
    .line 2119
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isBRCMSolution()Z

    #@38
    move-result v2

    #@39
    if-eqz v2, :cond_15

    #@3b
    .line 2120
    const-class v2, Lcom/broadcom/bt/service/map/MapService;

    #@3d
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@40
    goto :goto_15

    #@41
    .line 2125
    :cond_41
    const-string v2, "FTP"

    #@43
    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@46
    move-result v2

    #@47
    if-eqz v2, :cond_15

    #@49
    .line 2126
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isBRCMSolution()Z

    #@4c
    move-result v2

    #@4d
    if-eqz v2, :cond_15

    #@4f
    .line 2127
    const-class v2, Lcom/broadcom/bt/service/ftp/FTPService;

    #@51
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@54
    goto :goto_15
.end method

.method public getProfileConnectionState(I)I
    .registers 4
    .parameter "profile"

    #@0
    .prologue
    .line 1642
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1644
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@9
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterProperties;->getProfileConnectionState(I)I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method getRemoteAlias(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 1838
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1839
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v0

    #@d
    .line 1840
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v0, :cond_11

    #@f
    .line 1841
    const/4 v1, 0x0

    #@10
    .line 1843
    :goto_10
    return-object v1

    #@11
    :cond_11
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getAlias()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    goto :goto_10
.end method

.method getRemoteClass(Landroid/bluetooth/BluetoothDevice;)I
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 1857
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1858
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v0

    #@d
    .line 1859
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v0, :cond_11

    #@f
    .line 1860
    const/4 v1, 0x0

    #@10
    .line 1863
    :goto_10
    return v1

    #@11
    :cond_11
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getBluetoothClass()I

    #@14
    move-result v1

    #@15
    goto :goto_10
.end method

.method getRemoteName(Landroid/bluetooth/BluetoothDevice;)Ljava/lang/String;
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 1829
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1830
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v0

    #@d
    .line 1831
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v0, :cond_11

    #@f
    .line 1832
    const/4 v1, 0x0

    #@10
    .line 1834
    :goto_10
    return-object v1

    #@11
    :cond_11
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getName()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    goto :goto_10
.end method

.method getRemoteServicesNative([B)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method getRemoteUuids(Landroid/bluetooth/BluetoothDevice;)[Landroid/os/ParcelUuid;
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 1867
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1868
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v0

    #@d
    .line 1869
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v0, :cond_11

    #@f
    .line 1870
    const/4 v1, 0x0

    #@10
    .line 1872
    :goto_10
    return-object v1

    #@11
    :cond_11
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getUuids()[Landroid/os/ParcelUuid;

    #@14
    move-result-object v1

    #@15
    goto :goto_10
.end method

.method getScanMode()I
    .registers 3

    #@0
    .prologue
    .line 1562
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1564
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->getScanMode()I

    #@c
    move-result v0

    #@d
    return v0
.end method

.method getState()I
    .registers 3

    #@0
    .prologue
    .line 1413
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1415
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@9
    if-nez v0, :cond_e

    #@b
    .line 1416
    const/16 v0, 0xa

    #@d
    .line 1420
    :goto_d
    return v0

    #@e
    .line 1419
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v1, "getState(): mAdapterProperties: "

    #@15
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v0

    #@19
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@1b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v0

    #@1f
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v0

    #@23
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@26
    .line 1420
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@28
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->getState()I

    #@2b
    move-result v0

    #@2c
    goto :goto_d
.end method

.method public getTrustState(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 5
    .parameter "device"

    #@0
    .prologue
    .line 1918
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1919
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v0

    #@d
    .line 1920
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v0, :cond_11

    #@f
    .line 1921
    const/4 v1, 0x0

    #@10
    .line 1923
    :goto_10
    return v1

    #@11
    :cond_11
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getTrustState()Z

    #@14
    move-result v1

    #@15
    goto :goto_10
.end method

.method getUuids()[Landroid/os/ParcelUuid;
    .registers 3

    #@0
    .prologue
    .line 1537
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1539
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->getUuids()[Landroid/os/ParcelUuid;

    #@c
    move-result-object v0

    #@d
    return-object v0
.end method

.method isDiscovering()Z
    .registers 3

    #@0
    .prologue
    .line 1619
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1621
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->isDiscovering()Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method isEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 1400
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1402
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->getState()I

    #@c
    move-result v0

    #@d
    const/16 v1, 0xc

    #@f
    if-ne v0, v1, :cond_13

    #@11
    const/4 v0, 0x1

    #@12
    :goto_12
    return v0

    #@13
    :cond_13
    const/4 v0, 0x0

    #@14
    goto :goto_12
.end method

.method public isQuietModeEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 1663
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    const-string v1, "Quiet mode Enabled = "

    #@7
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    move-result-object v0

    #@b
    iget-boolean v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mQuietmode:Z

    #@d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v0

    #@11
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v0

    #@15
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@18
    .line 1665
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mQuietmode:Z

    #@1a
    return v0
.end method

.method isRadioEnabled()Z
    .registers 3

    #@0
    .prologue
    .line 1407
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1408
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@9
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterState;->isRadioOn()Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 458
    const-string v0, "onBind"

    #@2
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@5
    .line 460
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBinder:Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;

    #@7
    return-object v0
.end method

.method public onCreate()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    .line 421
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@4
    .line 423
    const-string v2, "onCreate"

    #@6
    invoke-direct {p0, v2}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@9
    .line 426
    invoke-static {p0}, Lcom/lge/bluetooth/LGBluetoothServiceManager;->getInstance(Landroid/content/Context;)Lcom/lge/bluetooth/LGBluetoothServiceManager;

    #@c
    move-result-object v1

    #@d
    .line 427
    .local v1, manager:Lcom/lge/bluetooth/LGBluetoothServiceManager;
    if-eqz v1, :cond_15

    #@f
    .line 428
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothServiceManager;->loadBluetoothAddress()V

    #@12
    .line 429
    invoke-virtual {v1}, Lcom/lge/bluetooth/LGBluetoothServiceManager;->terminateServiceManager()V

    #@15
    .line 432
    :cond_15
    new-instance v2, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;

    #@17
    invoke-direct {v2, p0}, Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;-><init>(Lcom/android/bluetooth/btservice/AdapterService;)V

    #@1a
    iput-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBinder:Lcom/android/bluetooth/btservice/AdapterService$AdapterServiceBinder;

    #@1c
    .line 433
    new-instance v2, Lcom/android/bluetooth/btservice/AdapterProperties;

    #@1e
    invoke-direct {v2, p0}, Lcom/android/bluetooth/btservice/AdapterProperties;-><init>(Lcom/android/bluetooth/btservice/AdapterService;)V

    #@21
    iput-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@23
    .line 434
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@25
    invoke-static {p0, v2}, Lcom/android/bluetooth/btservice/AdapterState;->make(Lcom/android/bluetooth/btservice/AdapterService;Lcom/android/bluetooth/btservice/AdapterProperties;)Lcom/android/bluetooth/btservice/AdapterState;

    #@28
    move-result-object v2

    #@29
    iput-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@2b
    .line 435
    new-instance v2, Lcom/android/bluetooth/btservice/JniCallbacks;

    #@2d
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@2f
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@31
    invoke-direct {v2, v3, v4}, Lcom/android/bluetooth/btservice/JniCallbacks;-><init>(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/bluetooth/btservice/AdapterProperties;)V

    #@34
    iput-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mJniCallbacks:Lcom/android/bluetooth/btservice/JniCallbacks;

    #@36
    .line 436
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/AdapterService;->initNative()Z

    #@39
    .line 437
    iput-boolean v5, p0, Lcom/android/bluetooth/btservice/AdapterService;->mNativeAvailable:Z

    #@3b
    .line 438
    new-instance v2, Landroid/os/RemoteCallbackList;

    #@3d
    invoke-direct {v2}, Landroid/os/RemoteCallbackList;-><init>()V

    #@40
    iput-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@42
    .line 440
    const/4 v2, 0x2

    #@43
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterPropertyNative(I)Z

    #@46
    .line 441
    invoke-virtual {p0, v5}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterPropertyNative(I)Z

    #@49
    .line 444
    new-instance v0, Landroid/content/IntentFilter;

    #@4b
    const-string v2, "android.bluetooth.device.action.CONNECTION_ACCESS_REPLY"

    #@4d
    invoke-direct {v0, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@50
    .line 445
    .local v0, filter:Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    #@52
    invoke-virtual {p0, v2, v0}, Lcom/android/bluetooth/btservice/AdapterService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@55
    .line 448
    const-string v2, "LGBT_COMMON_SCENARIO_DEVICE_NAME_SYNC"

    #@57
    invoke-static {v2}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@5a
    move-result v2

    #@5b
    if-eqz v2, :cond_6d

    #@5d
    .line 449
    new-instance v2, Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;

    #@5f
    invoke-direct {v2, p0}, Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;-><init>(Lcom/android/bluetooth/btservice/AdapterService;)V

    #@62
    iput-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mSettingsDBUpdateObserver:Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;

    #@64
    .line 450
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mSettingsDBUpdateObserver:Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;

    #@66
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->getContentResolver()Landroid/content/ContentResolver;

    #@69
    move-result-object v3

    #@6a
    invoke-virtual {v2, v3}, Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;->register(Landroid/content/ContentResolver;)V

    #@6d
    .line 453
    :cond_6d
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 471
    const-string v0, "****onDestroy()********"

    #@2
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@5
    .line 473
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBluetoothReceiver:Landroid/content/BroadcastReceiver;

    #@7
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@a
    .line 476
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mSettingsDBUpdateObserver:Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;

    #@c
    if-eqz v0, :cond_17

    #@e
    .line 477
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mSettingsDBUpdateObserver:Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;

    #@10
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->getContentResolver()Landroid/content/ContentResolver;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/btservice/AdapterService$SettingsDBUpdateObserver;->unregister(Landroid/content/ContentResolver;)V

    #@17
    .line 480
    :cond_17
    return-void
.end method

.method public onProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V
    .registers 9
    .parameter "device"
    .parameter "profileId"
    .parameter "newState"
    .parameter "prevState"

    #@0
    .prologue
    .line 255
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mHandler:Landroid/os/Handler;

    #@2
    const/16 v3, 0x14

    #@4
    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@7
    move-result-object v1

    #@8
    .line 256
    .local v1, m:Landroid/os/Message;
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@a
    .line 257
    iput p2, v1, Landroid/os/Message;->arg1:I

    #@c
    .line 258
    iput p3, v1, Landroid/os/Message;->arg2:I

    #@e
    .line 259
    new-instance v0, Landroid/os/Bundle;

    #@10
    const/4 v2, 0x1

    #@11
    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(I)V

    #@14
    .line 260
    .local v0, b:Landroid/os/Bundle;
    const-string v2, "prevState"

    #@16
    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    #@19
    .line 261
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    #@1c
    .line 262
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mHandler:Landroid/os/Handler;

    #@1e
    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@21
    .line 263
    return-void
.end method

.method public onProfileServiceStateChanged(Ljava/lang/String;I)V
    .registers 6
    .parameter "serviceName"
    .parameter "state"

    #@0
    .prologue
    .line 291
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mHandler:Landroid/os/Handler;

    #@2
    const/4 v2, 0x1

    #@3
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@6
    move-result-object v0

    #@7
    .line 292
    .local v0, m:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@9
    .line 293
    iput p2, v0, Landroid/os/Message;->arg1:I

    #@b
    .line 294
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mHandler:Landroid/os/Handler;

    #@d
    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    #@10
    .line 295
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 464
    const-string v0, "onUnbind, calling cleanup"

    #@2
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@5
    .line 466
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->cleanup()V

    #@8
    .line 467
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    #@b
    move-result v0

    #@c
    return v0
.end method

.method processStart()V
    .registers 10

    #@0
    .prologue
    const/16 v8, 0xa

    #@2
    const/4 v7, 0x2

    #@3
    .line 484
    const-string v4, "processStart()"

    #@5
    invoke-direct {p0, v4}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@8
    .line 487
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isBRCMSolution()Z

    #@b
    move-result v4

    #@c
    if-eqz v4, :cond_47

    #@e
    .line 489
    invoke-static {}, Lcom/broadcom/bt/service/ProfileConfig;->getSupportedProfiles()[Ljava/lang/Class;

    #@11
    move-result-object v3

    #@12
    .line 490
    .local v3, supportedProfileServices:[Ljava/lang/Class;
    const/4 v0, 0x0

    #@13
    .local v0, i:I
    :goto_13
    array-length v4, v3

    #@14
    if-ge v0, v4, :cond_61

    #@16
    .line 491
    aget-object v4, v3, v0

    #@18
    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@1b
    move-result-object v2

    #@1c
    .line 492
    .local v2, profileName:Ljava/lang/String;
    invoke-static {v2}, Lcom/broadcom/bt/service/ProfileConfig;->isProfileConfiguredEnabled(Ljava/lang/String;)Z

    #@1f
    move-result v4

    #@20
    if-eqz v4, :cond_2e

    #@22
    .line 493
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@24
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v4, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    .line 490
    :goto_2b
    add-int/lit8 v0, v0, 0x1

    #@2d
    goto :goto_13

    #@2e
    .line 495
    :cond_2e
    const-string v4, "BluetoothAdapterService"

    #@30
    new-instance v5, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    const-string v6, "processStart(): profile not enabled: "

    #@37
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v5

    #@3b
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v5

    #@3f
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v5

    #@43
    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    goto :goto_2b

    #@47
    .line 500
    .end local v0           #i:I
    .end local v2           #profileName:Ljava/lang/String;
    .end local v3           #supportedProfileServices:[Ljava/lang/Class;
    :cond_47
    invoke-static {}, Lcom/android/bluetooth/btservice/Config;->getSupportedProfiles()[Ljava/lang/Class;

    #@4a
    move-result-object v3

    #@4b
    .line 502
    .restart local v3       #supportedProfileServices:[Ljava/lang/Class;
    const/4 v0, 0x0

    #@4c
    .restart local v0       #i:I
    :goto_4c
    array-length v4, v3

    #@4d
    if-ge v0, v4, :cond_61

    #@4f
    .line 503
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfileServicesState:Ljava/util/HashMap;

    #@51
    aget-object v5, v3, v0

    #@53
    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@56
    move-result-object v5

    #@57
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@5a
    move-result-object v6

    #@5b
    invoke-virtual {v4, v5, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@5e
    .line 502
    add-int/lit8 v0, v0, 0x1

    #@60
    goto :goto_4c

    #@61
    .line 507
    :cond_61
    new-instance v4, Lcom/android/bluetooth/btservice/RemoteDevices;

    #@63
    invoke-direct {v4, p0}, Lcom/android/bluetooth/btservice/RemoteDevices;-><init>(Lcom/android/bluetooth/btservice/AdapterService;)V

    #@66
    iput-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@68
    .line 508
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@6a
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@6c
    invoke-virtual {v4, v5}, Lcom/android/bluetooth/btservice/AdapterProperties;->init(Lcom/android/bluetooth/btservice/RemoteDevices;)V

    #@6f
    .line 511
    const-string v4, "processStart(): Make Bond State Machine"

    #@71
    invoke-direct {p0, v4}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@74
    .line 513
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@76
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@78
    invoke-static {p0, v4, v5}, Lcom/android/bluetooth/btservice/BondStateMachine;->make(Lcom/android/bluetooth/btservice/AdapterService;Lcom/android/bluetooth/btservice/AdapterProperties;Lcom/android/bluetooth/btservice/RemoteDevices;)Lcom/android/bluetooth/btservice/BondStateMachine;

    #@7b
    move-result-object v4

    #@7c
    iput-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@7e
    .line 515
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mJniCallbacks:Lcom/android/bluetooth/btservice/JniCallbacks;

    #@80
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@82
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@84
    invoke-virtual {v4, v5, v6}, Lcom/android/bluetooth/btservice/JniCallbacks;->init(Lcom/android/bluetooth/btservice/BondStateMachine;Lcom/android/bluetooth/btservice/RemoteDevices;)V

    #@87
    .line 518
    invoke-static {p0}, Lcom/android/bluetooth/btservice/AdapterService;->setAdapterService(Lcom/android/bluetooth/btservice/AdapterService;)V

    #@8a
    .line 520
    new-instance v4, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;

    #@8c
    invoke-direct {v4, p0}, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;-><init>(Landroid/content/Context;)V

    #@8f
    sput-object v4, Lcom/android/bluetooth/btservice/AdapterService;->mServiceAdapter:Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;

    #@91
    .line 524
    iget-boolean v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfilesStarted:Z

    #@93
    if-nez v4, :cond_c1

    #@95
    array-length v4, v3

    #@96
    if-lez v4, :cond_c1

    #@98
    .line 526
    const/16 v4, 0xc

    #@9a
    invoke-direct {p0, v3, v4}, Lcom/android/bluetooth/btservice/AdapterService;->setProfileServiceState([Ljava/lang/Class;I)V

    #@9d
    .line 528
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a0
    move-result-object v4

    #@a1
    if-eqz v4, :cond_c0

    #@a3
    .line 529
    invoke-static {}, Lcom/lge/mdm/LGMDMSystemServer;->getLGMDMDevicePolicyManager()Lcom/lge/mdm/manager/LGMDMDevicePolicyManager;

    #@a6
    move-result-object v1

    #@a7
    .line 530
    .local v1, mdm:Lcom/lge/mdm/manager/LGMDMDevicePolicyManager;
    const/4 v4, 0x0

    #@a8
    invoke-virtual {v1, v4}, Lcom/lge/mdm/manager/LGMDMDevicePolicyManager;->getAllowBluetoothProfiles(Landroid/content/ComponentName;)I

    #@ab
    move-result v4

    #@ac
    if-nez v4, :cond_c0

    #@ae
    .line 532
    const-string v4, "BluetoothAdapterService"

    #@b0
    const-string v5, "MDM restricted all Bluetooth profiles, Bluetooth enable routine is started."

    #@b2
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b5
    .line 533
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@b7
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@b9
    invoke-virtual {v5, v7}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@bc
    move-result-object v5

    #@bd
    invoke-virtual {v4, v5}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V

    #@c0
    .line 543
    .end local v1           #mdm:Lcom/lge/mdm/manager/LGMDMDevicePolicyManager;
    :cond_c0
    :goto_c0
    return-void

    #@c1
    .line 539
    :cond_c1
    const-string v4, "processStart(): Profile Services alreay started"

    #@c3
    invoke-direct {p0, v4}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@c6
    .line 541
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@c8
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@ca
    invoke-virtual {v5, v7}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@cd
    move-result-object v5

    #@ce
    invoke-virtual {v4, v5}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V

    #@d1
    goto :goto_c0
.end method

.method registerCallback(Landroid/bluetooth/IBluetoothCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 2033
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@5
    .line 2034
    return-void
.end method

.method removeBond(Landroid/bluetooth/BluetoothDevice;)Z
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 1808
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v3, "Need BLUETOOTH ADMIN permission"

    #@4
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1809
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v2, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v0

    #@d
    .line 1810
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-eqz v0, :cond_17

    #@f
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getBondState()I

    #@12
    move-result v2

    #@13
    const/16 v3, 0xc

    #@15
    if-eq v2, v3, :cond_19

    #@17
    .line 1811
    :cond_17
    const/4 v2, 0x0

    #@18
    .line 1816
    :goto_18
    return v2

    #@19
    .line 1813
    :cond_19
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@1b
    const/4 v3, 0x3

    #@1c
    invoke-virtual {v2, v3}, Lcom/android/bluetooth/btservice/BondStateMachine;->obtainMessage(I)Landroid/os/Message;

    #@1f
    move-result-object v1

    #@20
    .line 1814
    .local v1, msg:Landroid/os/Message;
    iput-object p1, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@22
    .line 1815
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@24
    invoke-virtual {v2, v1}, Lcom/android/bluetooth/btservice/BondStateMachine;->sendMessage(Landroid/os/Message;)V

    #@27
    .line 1816
    const/4 v2, 0x1

    #@28
    goto :goto_18
.end method

.method removeBondNative([B)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method public sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V
    .registers 7
    .parameter "device"
    .parameter "profile"
    .parameter "state"
    .parameter "prevState"

    #@0
    .prologue
    .line 1983
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterService;->getState()I

    #@3
    move-result v0

    #@4
    const/16 v1, 0xa

    #@6
    if-ne v0, v1, :cond_9

    #@8
    .line 1989
    :goto_8
    return-void

    #@9
    .line 1987
    :cond_9
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@b
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/bluetooth/btservice/AdapterProperties;->sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V

    #@e
    goto :goto_8
.end method

.method setAdapterPropertyNative(I)Z
	.registers 3
	const/4 v0, 0x1
	return v0
.end method

.method setAdapterPropertyNative(I[B)Z
	.registers 4
	const/4 v0, 0x1
	return v0
.end method

.method setDevicePropertyNative([BI[B)Z
	.registers 5
	const/4 v0, 0x1
	return v0
.end method

.method setDiscoverableTimeout(I)Z
    .registers 4
    .parameter "timeout"

    #@0
    .prologue
    .line 1594
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1597
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@a
    move-result-object v0

    #@b
    if-eqz v0, :cond_15

    #@d
    .line 1598
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@10
    move-result-object v0

    #@11
    invoke-interface {v0, p1}, Lcom/lge/cappuccino/IMdm;->getBluetoothVisibleDuration(I)I

    #@14
    move-result p1

    #@15
    .line 1601
    :cond_15
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@17
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterProperties;->setDiscoverableTimeout(I)Z

    #@1a
    move-result v0

    #@1b
    return v0
.end method

.method setName(Ljava/lang/String;)Z
    .registers 4
    .parameter "name"

    #@0
    .prologue
    .line 1555
    const-string v0, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v1, "Need BLUETOOTH ADMIN permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1558
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@9
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterProperties;->setName(Ljava/lang/String;)Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method setPairingConfirmation(Landroid/bluetooth/BluetoothDevice;Z)Z
    .registers 8
    .parameter "device"
    .parameter "accept"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1905
    const-string v3, "android.permission.BLUETOOTH"

    #@3
    const-string v4, "Need BLUETOOTH permission"

    #@5
    invoke-virtual {p0, v3, v4}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 1906
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@a
    invoke-virtual {v3, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@d
    move-result-object v1

    #@e
    .line 1907
    .local v1, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-eqz v1, :cond_18

    #@10
    invoke-virtual {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getBondState()I

    #@13
    move-result v3

    #@14
    const/16 v4, 0xb

    #@16
    if-eq v3, v4, :cond_19

    #@18
    .line 1912
    :cond_18
    :goto_18
    return v2

    #@19
    .line 1911
    :cond_19
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@1c
    move-result-object v3

    #@1d
    invoke-static {v3}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@20
    move-result-object v0

    #@21
    .line 1912
    .local v0, addr:[B
    invoke-direct {p0, v0, v2, p2, v2}, Lcom/android/bluetooth/btservice/AdapterService;->sspReplyNative([BIZI)Z

    #@24
    move-result v2

    #@25
    goto :goto_18
.end method

.method setPasskey(Landroid/bluetooth/BluetoothDevice;ZI[B)Z
    .registers 9
    .parameter "device"
    .parameter "accept"
    .parameter "len"
    .parameter "passkey"

    #@0
    .prologue
    .line 1893
    const-string v2, "android.permission.BLUETOOTH"

    #@2
    const-string v3, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1894
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v2, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v1

    #@d
    .line 1895
    .local v1, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-eqz v1, :cond_17

    #@f
    invoke-virtual {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getBondState()I

    #@12
    move-result v2

    #@13
    const/16 v3, 0xb

    #@15
    if-eq v2, v3, :cond_19

    #@17
    .line 1896
    :cond_17
    const/4 v2, 0x0

    #@18
    .line 1900
    :goto_18
    return v2

    #@19
    .line 1899
    :cond_19
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@20
    move-result-object v0

    #@21
    .line 1900
    .local v0, addr:[B
    const/4 v2, 0x1

    #@22
    invoke-static {p4}, Lcom/android/bluetooth/Utils;->byteArrayToInt([B)I

    #@25
    move-result v3

    #@26
    invoke-direct {p0, v0, v2, p2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->sspReplyNative([BIZI)Z

    #@29
    move-result v2

    #@2a
    goto :goto_18
.end method

.method setPin(Landroid/bluetooth/BluetoothDevice;ZI[B)Z
    .registers 9
    .parameter "device"
    .parameter "accept"
    .parameter "len"
    .parameter "pinCode"

    #@0
    .prologue
    .line 1882
    const-string v2, "android.permission.BLUETOOTH"

    #@2
    const-string v3, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1883
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v2, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v1

    #@d
    .line 1884
    .local v1, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-eqz v1, :cond_17

    #@f
    invoke-virtual {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->getBondState()I

    #@12
    move-result v2

    #@13
    const/16 v3, 0xb

    #@15
    if-eq v2, v3, :cond_19

    #@17
    .line 1885
    :cond_17
    const/4 v2, 0x0

    #@18
    .line 1889
    :goto_18
    return v2

    #@19
    .line 1888
    :cond_19
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@20
    move-result-object v0

    #@21
    .line 1889
    .local v0, addr:[B
    invoke-direct {p0, v0, p2, p3, p4}, Lcom/android/bluetooth/btservice/AdapterService;->pinReplyNative([BZI[B)Z

    #@24
    move-result v2

    #@25
    goto :goto_18
.end method

.method setProfileAutoConnectionPriority(Landroid/bluetooth/BluetoothDevice;I)V
    .registers 7
    .parameter "device"
    .parameter "profileId"

    #@0
    .prologue
    const/16 v3, 0x3e8

    #@2
    .line 1783
    const/4 v2, 0x1

    #@3
    if-ne p2, v2, :cond_18

    #@5
    .line 1784
    invoke-static {}, Lcom/android/bluetooth/hfp/HeadsetService;->getHeadsetService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@8
    move-result-object v1

    #@9
    .line 1785
    .local v1, hsService:Lcom/android/bluetooth/hfp/HeadsetService;
    if-eqz v1, :cond_17

    #@b
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@e
    move-result v2

    #@f
    if-eq v3, v2, :cond_17

    #@11
    .line 1787
    invoke-direct {p0, v1, p1}, Lcom/android/bluetooth/btservice/AdapterService;->adjustOtherHeadsetPriorities(Lcom/android/bluetooth/hfp/HeadsetService;Landroid/bluetooth/BluetoothDevice;)V

    #@14
    .line 1788
    invoke-virtual {v1, p1, v3}, Lcom/android/bluetooth/hfp/HeadsetService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@17
    .line 1799
    .end local v1           #hsService:Lcom/android/bluetooth/hfp/HeadsetService;
    :cond_17
    :goto_17
    return-void

    #@18
    .line 1791
    :cond_18
    const/4 v2, 0x2

    #@19
    if-ne p2, v2, :cond_17

    #@1b
    .line 1792
    invoke-static {}, Lcom/android/bluetooth/a2dp/A2dpService;->getA2dpService()Lcom/android/bluetooth/a2dp/A2dpService;

    #@1e
    move-result-object v0

    #@1f
    .line 1793
    .local v0, a2dpService:Lcom/android/bluetooth/a2dp/A2dpService;
    if-eqz v0, :cond_17

    #@21
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/a2dp/A2dpService;->getPriority(Landroid/bluetooth/BluetoothDevice;)I

    #@24
    move-result v2

    #@25
    if-eq v3, v2, :cond_17

    #@27
    .line 1795
    invoke-direct {p0, v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->adjustOtherSinkPriorities(Lcom/android/bluetooth/a2dp/A2dpService;Landroid/bluetooth/BluetoothDevice;)V

    #@2a
    .line 1796
    invoke-virtual {v0, p1, v3}, Lcom/android/bluetooth/a2dp/A2dpService;->setPriority(Landroid/bluetooth/BluetoothDevice;I)Z

    #@2d
    goto :goto_17
.end method

.method public setProfileOnOff(Ljava/lang/String;Z)Z
    .registers 8
    .parameter "profile_name"
    .parameter "onoff"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2091
    const-string v3, "BluetoothAdapterService"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v4, "[BTUI] setProfileOnOff = "

    #@a
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    const-string v4, " ("

    #@14
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v4

    #@18
    if-eqz p2, :cond_44

    #@1a
    const-string v1, "enable"

    #@1c
    :goto_1c
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v1

    #@20
    const-string v4, ")"

    #@22
    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v1

    #@26
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 2092
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->getProfileClass(Ljava/lang/String;)[Ljava/lang/Class;

    #@30
    move-result-object v0

    #@31
    .line 2093
    .local v0, profile:[Ljava/lang/Class;
    if-eqz v0, :cond_4a

    #@33
    .line 2094
    iput-boolean v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mServiceChanged:Z

    #@35
    .line 2095
    if-eqz p2, :cond_47

    #@37
    const/16 v1, 0xc

    #@39
    :goto_39
    invoke-direct {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->setProfileServiceState([Ljava/lang/Class;I)V

    #@3c
    .line 2096
    iget-boolean v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mServiceChanged:Z

    #@3e
    if-eqz v1, :cond_4a

    #@40
    .line 2097
    iput-boolean v2, p0, Lcom/android/bluetooth/btservice/AdapterService;->mServiceChanged:Z

    #@42
    .line 2098
    const/4 v1, 0x1

    #@43
    .line 2101
    :goto_43
    return v1

    #@44
    .line 2091
    .end local v0           #profile:[Ljava/lang/Class;
    :cond_44
    const-string v1, "disable"

    #@46
    goto :goto_1c

    #@47
    .line 2095
    .restart local v0       #profile:[Ljava/lang/Class;
    :cond_47
    const/16 v1, 0xa

    #@49
    goto :goto_39

    #@4a
    :cond_4a
    move v1, v2

    #@4b
    .line 2101
    goto :goto_43
.end method

.method setRemoteAlias(Landroid/bluetooth/BluetoothDevice;Ljava/lang/String;)Z
    .registers 6
    .parameter "device"
    .parameter "name"

    #@0
    .prologue
    .line 1847
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1848
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v0

    #@d
    .line 1849
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v0, :cond_11

    #@f
    .line 1850
    const/4 v1, 0x0

    #@10
    .line 1853
    :goto_10
    return v1

    #@11
    .line 1852
    :cond_11
    invoke-virtual {v0, p2}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->setAlias(Ljava/lang/String;)V

    #@14
    .line 1853
    const/4 v1, 0x1

    #@15
    goto :goto_10
.end method

.method setScanMode(II)Z
    .registers 6
    .parameter "mode"
    .parameter "duration"

    #@0
    .prologue
    .line 1568
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1570
    invoke-virtual {p0, p2}, Lcom/android/bluetooth/btservice/AdapterService;->setDiscoverableTimeout(I)Z

    #@a
    .line 1572
    invoke-static {p1}, Lcom/android/bluetooth/btservice/AdapterService;->convertScanModeToHal(I)I

    #@d
    move-result v0

    #@e
    .line 1574
    .local v0, newMode:I
    const/4 v1, 0x2

    #@f
    if-ne v0, v1, :cond_35

    #@11
    .line 1575
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@14
    move-result-object v1

    #@15
    if-eqz v1, :cond_35

    #@17
    .line 1576
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1a
    move-result-object v1

    #@1b
    const/4 v2, 0x0

    #@1c
    invoke-interface {v1, v2}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@1f
    move-result v1

    #@20
    if-nez v1, :cond_2c

    #@22
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@25
    move-result-object v1

    #@26
    invoke-interface {v1}, Lcom/lge/cappuccino/IMdm;->checkBluetoothVisible()Z

    #@29
    move-result v1

    #@2a
    if-eqz v1, :cond_35

    #@2c
    .line 1578
    :cond_2c
    const-string v1, "BluetoothAdapterService"

    #@2e
    const-string v2, "setScanMode is blocked by LG MDM Server Policy"

    #@30
    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 1579
    const/4 v1, 0x0

    #@34
    .line 1584
    :goto_34
    return v1

    #@35
    :cond_35
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@37
    invoke-virtual {v1, v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->setScanMode(I)Z

    #@3a
    move-result v1

    #@3b
    goto :goto_34
.end method

.method public setTrustState(Landroid/bluetooth/BluetoothDevice;Z)Z
    .registers 6
    .parameter "device"
    .parameter "truststate"

    #@0
    .prologue
    .line 1927
    const-string v1, "android.permission.BLUETOOTH"

    #@2
    const-string v2, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1928
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v1, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v0

    #@d
    .line 1929
    .local v0, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v0, :cond_11

    #@f
    .line 1930
    const/4 v1, 0x0

    #@10
    .line 1932
    :goto_10
    return v1

    #@11
    :cond_11
    invoke-virtual {v0, p2}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->setTrustState(Z)Z

    #@14
    move-result v1

    #@15
    goto :goto_10
.end method

.method public sspDebugConfigure(Z)Z
    .registers 4
    .parameter "enable"

    #@0
    .prologue
    .line 1962
    const-string v0, "android.permission.BLUETOOTH"

    #@2
    const-string v1, "Need BLUETOOTH permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1963
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->sspDebugConfigureNative(Z)Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method startBluetoothDisable()V
    .registers 4

    #@0
    .prologue
    .line 546
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@2
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@4
    const/16 v2, 0x15

    #@6
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/btservice/AdapterState;->obtainMessage(I)Landroid/os/Message;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v0, v1}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessage(Landroid/os/Message;)V

    #@d
    .line 547
    return-void
.end method

.method startDiscovery()Z
    .registers 3

    #@0
    .prologue
    .line 1605
    const-string v0, "android.permission.BLUETOOTH_ADMIN"

    #@2
    const-string v1, "Need BLUETOOTH ADMIN permission"

    #@4
    invoke-virtual {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@7
    .line 1608
    invoke-direct {p0}, Lcom/android/bluetooth/btservice/AdapterService;->startDiscoveryNative()Z

    #@a
    move-result v0

    #@b
    return v0
.end method

.method stopProfileServices()Z
    .registers 3

    #@0
    .prologue
    .line 551
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isBRCMSolution()Z

    #@3
    move-result v1

    #@4
    if-eqz v1, :cond_18

    #@6
    .line 553
    invoke-static {}, Lcom/broadcom/bt/service/ProfileConfig;->getSupportedProfiles()[Ljava/lang/Class;

    #@9
    move-result-object v0

    #@a
    .line 557
    .local v0, supportedProfileServices:[Ljava/lang/Class;
    :goto_a
    iget-boolean v1, p0, Lcom/android/bluetooth/btservice/AdapterService;->mProfilesStarted:Z

    #@c
    if-eqz v1, :cond_1d

    #@e
    array-length v1, v0

    #@f
    if-lez v1, :cond_1d

    #@11
    .line 558
    const/16 v1, 0xa

    #@13
    invoke-direct {p0, v0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->setProfileServiceState([Ljava/lang/Class;I)V

    #@16
    .line 559
    const/4 v1, 0x1

    #@17
    .line 564
    :goto_17
    return v1

    #@18
    .line 555
    .end local v0           #supportedProfileServices:[Ljava/lang/Class;
    :cond_18
    invoke-static {}, Lcom/android/bluetooth/btservice/Config;->getSupportedProfiles()[Ljava/lang/Class;

    #@1b
    move-result-object v0

    #@1c
    .restart local v0       #supportedProfileServices:[Ljava/lang/Class;
    goto :goto_a

    #@1d
    .line 562
    :cond_1d
    const-string v1, "stopProfileServices(): No profiles services to stop or already stopped."

    #@1f
    invoke-direct {p0, v1}, Lcom/android/bluetooth/btservice/AdapterService;->debugLog(Ljava/lang/String;)V

    #@22
    .line 564
    const/4 v1, 0x0

    #@23
    goto :goto_17
.end method

.method unregisterCallback(Landroid/bluetooth/IBluetoothCallback;)V
    .registers 3
    .parameter "cb"

    #@0
    .prologue
    .line 2037
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@2
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@5
    .line 2038
    return-void
.end method

.method updateAdapterState(II)V
    .registers 9
    .parameter "prevState"
    .parameter "newState"

    #@0
    .prologue
    .line 569
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@2
    if-eqz v3, :cond_58

    #@4
    .line 570
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@6
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    #@9
    move-result v2

    #@a
    .line 571
    .local v2, n:I
    const-string v3, "BluetoothAdapterService"

    #@c
    new-instance v4, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v5, "Broadcasting updateAdapterState() to "

    #@13
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v4

    #@17
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v4

    #@1b
    const-string v5, " receivers."

    #@1d
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 572
    const/4 v1, 0x0

    #@29
    .local v1, i:I
    :goto_29
    if-ge v1, v2, :cond_53

    #@2b
    .line 574
    :try_start_2b
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@2d
    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@30
    move-result-object v3

    #@31
    check-cast v3, Landroid/bluetooth/IBluetoothCallback;

    #@33
    invoke-interface {v3, p1, p2}, Landroid/bluetooth/IBluetoothCallback;->onBluetoothStateChange(II)V
    :try_end_36
    .catch Landroid/os/RemoteException; {:try_start_2b .. :try_end_36} :catch_39

    #@36
    .line 572
    :goto_36
    add-int/lit8 v1, v1, 0x1

    #@38
    goto :goto_29

    #@39
    .line 575
    :catch_39
    move-exception v0

    #@3a
    .line 576
    .local v0, e:Landroid/os/RemoteException;
    const-string v3, "BluetoothAdapterService"

    #@3c
    new-instance v4, Ljava/lang/StringBuilder;

    #@3e
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@41
    const-string v5, "Unable to call onBluetoothStateChange() on callback #"

    #@43
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v4

    #@47
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v4

    #@4b
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4e
    move-result-object v4

    #@4f
    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@52
    goto :goto_36

    #@53
    .line 579
    .end local v0           #e:Landroid/os/RemoteException;
    :cond_53
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService;->mCallbacks:Landroid/os/RemoteCallbackList;

    #@55
    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@58
    .line 581
    .end local v1           #i:I
    .end local v2           #n:I
    :cond_58
    return-void
.end method
