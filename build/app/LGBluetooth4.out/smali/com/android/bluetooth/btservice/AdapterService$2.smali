.class Lcom/android/bluetooth/btservice/AdapterService$2;
.super Landroid/content/BroadcastReceiver;
.source "AdapterService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/btservice/AdapterService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/btservice/AdapterService;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/btservice/AdapterService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 697
    iput-object p1, p0, Lcom/android/bluetooth/btservice/AdapterService$2;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 13
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    const/4 v6, 0x1

    #@2
    .line 700
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 701
    .local v0, action:Ljava/lang/String;
    const-string v8, "android.bluetooth.device.action.CONNECTION_ACCESS_REPLY"

    #@8
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v8

    #@c
    if-eqz v8, :cond_37

    #@e
    .line 703
    const-string v8, "android.bluetooth.device.extra.DEVICE"

    #@10
    invoke-virtual {p2, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@13
    move-result-object v4

    #@14
    check-cast v4, Landroid/bluetooth/BluetoothDevice;

    #@16
    .line 705
    .local v4, device:Landroid/bluetooth/BluetoothDevice;
    const-string v8, "android.bluetooth.device.extra.UUID"

    #@18
    invoke-virtual {p2, v8}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@1b
    move-result-object v5

    #@1c
    check-cast v5, Landroid/os/ParcelUuid;

    #@1e
    .line 707
    .local v5, serviceUuid:Landroid/os/ParcelUuid;
    const-string v8, "android.bluetooth.device.extra.CONNECTION_ACCESS_RESULT"

    #@20
    const/4 v9, 0x2

    #@21
    invoke-virtual {p2, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@24
    move-result v1

    #@25
    .line 710
    .local v1, authorize:I
    if-ne v1, v6, :cond_38

    #@27
    move v2, v6

    #@28
    .line 711
    .local v2, authorize_result:Z
    :goto_28
    const-string v8, "android.bluetooth.device.extra.ALWAYS_ALLOWED"

    #@2a
    invoke-virtual {p2, v8, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    #@2d
    move-result v3

    #@2e
    .line 714
    .local v3, autoReply:Z
    if-nez v4, :cond_3a

    #@30
    .line 715
    const-string v6, "BluetoothAdapterService"

    #@32
    const-string v7, "[BTUI] AUTHORIZE_SERVICE : device is null"

    #@34
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@37
    .line 731
    .end local v1           #authorize:I
    .end local v2           #authorize_result:Z
    .end local v3           #autoReply:Z
    .end local v4           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v5           #serviceUuid:Landroid/os/ParcelUuid;
    :cond_37
    :goto_37
    return-void

    #@38
    .restart local v1       #authorize:I
    .restart local v4       #device:Landroid/bluetooth/BluetoothDevice;
    .restart local v5       #serviceUuid:Landroid/os/ParcelUuid;
    :cond_38
    move v2, v7

    #@39
    .line 710
    goto :goto_28

    #@3a
    .line 720
    .restart local v2       #authorize_result:Z
    .restart local v3       #autoReply:Z
    :cond_3a
    if-nez v5, :cond_44

    #@3c
    .line 721
    const-string v6, "BluetoothAdapterService"

    #@3e
    const-string v7, "[BTUI] AUTHORIZE_SERVICE : serviceUuid is null"

    #@40
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    goto :goto_37

    #@44
    .line 726
    :cond_44
    if-eqz v3, :cond_49

    #@46
    .line 727
    invoke-virtual {v4, v6}, Landroid/bluetooth/BluetoothDevice;->setTrust(Z)Z

    #@49
    .line 729
    :cond_49
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterService$2;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@4b
    invoke-virtual {v6, v4, v5, v2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->authorizeService(Landroid/bluetooth/BluetoothDevice;Landroid/os/ParcelUuid;ZZ)Z

    #@4e
    goto :goto_37
.end method
