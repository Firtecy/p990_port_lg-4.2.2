.class final Lcom/android/bluetooth/btservice/RemoteDevices;
.super Ljava/lang/Object;
.source "RemoteDevices.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    }
.end annotation


# static fields
.field private static final DBG:Z = true

.field private static final MESSAGE_UUID_INTENT:I = 0x1

.field private static final TAG:Ljava/lang/String; = "BluetoothRemoteDevices"

.field private static final UUID_INTENT_DELAY:I = 0x1770

.field private static mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private static mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

.field private static mSdpTracker:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mDevices:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            "Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mObject:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/btservice/AdapterService;)V
    .registers 3
    .parameter "service"

    #@0
    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 57
    new-instance v0, Ljava/lang/Object;

    #@5
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    #@8
    iput-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;

    #@a
    .line 647
    new-instance v0, Lcom/android/bluetooth/btservice/RemoteDevices$1;

    #@c
    invoke-direct {v0, p0}, Lcom/android/bluetooth/btservice/RemoteDevices$1;-><init>(Lcom/android/bluetooth/btservice/RemoteDevices;)V

    #@f
    iput-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mHandler:Landroid/os/Handler;

    #@11
    .line 65
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@14
    move-result-object v0

    #@15
    sput-object v0, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@17
    .line 66
    sput-object p1, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@19
    .line 67
    new-instance v0, Ljava/util/ArrayList;

    #@1b
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@1e
    sput-object v0, Lcom/android/bluetooth/btservice/RemoteDevices;->mSdpTracker:Ljava/util/ArrayList;

    #@20
    .line 68
    new-instance v0, Ljava/util/HashMap;

    #@22
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@25
    iput-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mDevices:Ljava/util/HashMap;

    #@27
    .line 69
    return-void
.end method

.method static synthetic access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 48
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;

    #@2
    return-object v0
.end method

.method static synthetic access$1200(Lcom/android/bluetooth/btservice/RemoteDevices;Landroid/bluetooth/BluetoothDevice;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->sendUuidIntent(Landroid/bluetooth/BluetoothDevice;)V

    #@3
    return-void
.end method

.method static synthetic access$200()Lcom/android/bluetooth/btservice/AdapterService;
    .registers 1

    #@0
    .prologue
    .line 48
    sget-object v0, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/bluetooth/btservice/RemoteDevices;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method private debugLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 669
    const-string v0, "BluetoothRemoteDevices"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 671
    return-void
.end method

.method private errorLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 664
    const-string v0, "BluetoothRemoteDevices"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 665
    return-void
.end method

.method private infoLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 675
    const-string v0, "BluetoothRemoteDevices"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 677
    return-void
.end method

.method private sendDisplayPinIntent([BI)V
    .registers 6
    .parameter "address"
    .parameter "pin"

    #@0
    .prologue
    .line 285
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "android.bluetooth.device.action.PAIRING_REQUEST"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 286
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    #@9
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@10
    .line 287
    const-string v1, "android.bluetooth.device.extra.PAIRING_KEY"

    #@12
    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@15
    .line 288
    const-string v1, "android.bluetooth.device.extra.PAIRING_VARIANT"

    #@17
    const/4 v2, 0x5

    #@18
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1b
    .line 290
    sget-object v1, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@1d
    sget-object v2, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@1f
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@21
    invoke-virtual {v1, v0, v2}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@24
    .line 291
    return-void
.end method

.method private sendUuidIntent(Landroid/bluetooth/BluetoothDevice;)V
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 274
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@3
    move-result-object v1

    #@4
    .line 275
    .local v1, prop:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    new-instance v0, Landroid/content/Intent;

    #@6
    const-string v2, "android.bluetooth.device.action.UUID"

    #@8
    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b
    .line 276
    .local v0, intent:Landroid/content/Intent;
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    #@d
    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@10
    .line 277
    const-string v3, "android.bluetooth.device.extra.UUID"

    #@12
    if-nez v1, :cond_25

    #@14
    const/4 v2, 0x0

    #@15
    :goto_15
    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Landroid/os/Parcelable;)Landroid/content/Intent;

    #@18
    .line 278
    sget-object v2, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@1a
    const-string v3, "android.permission.BLUETOOTH_ADMIN"

    #@1c
    invoke-virtual {v2, v0, v3}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@1f
    .line 281
    sget-object v2, Lcom/android/bluetooth/btservice/RemoteDevices;->mSdpTracker:Ljava/util/ArrayList;

    #@21
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@24
    .line 282
    return-void

    #@25
    .line 277
    :cond_25
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mUuids:[Landroid/os/ParcelUuid;
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$400(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)[Landroid/os/ParcelUuid;

    #@28
    move-result-object v2

    #@29
    goto :goto_15
.end method

.method private warnLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 680
    const-string v0, "BluetoothRemoteDevices"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 681
    return-void
.end method


# virtual methods
.method public Clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 82
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    #@2
    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    #@5
    throw v0
.end method

.method aclStateChangeCallback(I[BI)V
    .registers 9
    .parameter "status"
    .parameter "address"
    .parameter "newState"

    #@0
    .prologue
    const/high16 v4, 0x800

    #@2
    .line 601
    invoke-virtual {p0, p2}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@5
    move-result-object v0

    #@6
    .line 603
    .local v0, device:Landroid/bluetooth/BluetoothDevice;
    if-nez v0, :cond_3a

    #@8
    .line 604
    const-string v2, "aclStateChangeCallback: Device is NULL"

    #@a
    invoke-direct {p0, v2}, Lcom/android/bluetooth/btservice/RemoteDevices;->errorLog(Ljava/lang/String;)V

    #@d
    .line 606
    const/4 v1, 0x0

    #@e
    .line 607
    .local v1, intent:Landroid/content/Intent;
    if-nez p3, :cond_2b

    #@10
    .line 608
    new-instance v1, Landroid/content/Intent;

    #@12
    .end local v1           #intent:Landroid/content/Intent;
    const-string v2, "com.lge.bluetooth.device.action.ACL_CONNECTED"

    #@14
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@17
    .line 609
    .restart local v1       #intent:Landroid/content/Intent;
    const-string v2, "BluetoothRemoteDevices"

    #@19
    const-string v3, "[BTUI] aclStateChangeCallback: Connected, but device is null, sendBroadcast"

    #@1b
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 614
    :goto_1e
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@21
    .line 615
    sget-object v2, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@23
    sget-object v3, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@25
    const-string v3, "android.permission.BLUETOOTH"

    #@27
    invoke-virtual {v2, v1, v3}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@2a
    .line 631
    :goto_2a
    return-void

    #@2b
    .line 611
    :cond_2b
    new-instance v1, Landroid/content/Intent;

    #@2d
    .end local v1           #intent:Landroid/content/Intent;
    const-string v2, "com.lge.bluetooth.device.action.ACL_DISCONNECTED"

    #@2f
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@32
    .line 612
    .restart local v1       #intent:Landroid/content/Intent;
    const-string v2, "BluetoothRemoteDevices"

    #@34
    const-string v3, "[BTUI] aclStateChangeCallback: DisConnected, but device is null, sendBroadcast"

    #@36
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@39
    goto :goto_1e

    #@3a
    .line 620
    .end local v1           #intent:Landroid/content/Intent;
    :cond_3a
    const/4 v1, 0x0

    #@3b
    .line 621
    .restart local v1       #intent:Landroid/content/Intent;
    if-nez p3, :cond_6c

    #@3d
    .line 622
    new-instance v1, Landroid/content/Intent;

    #@3f
    .end local v1           #intent:Landroid/content/Intent;
    const-string v2, "android.bluetooth.device.action.ACL_CONNECTED"

    #@41
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@44
    .line 623
    .restart local v1       #intent:Landroid/content/Intent;
    new-instance v2, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v3, "aclStateChangeCallback: State:Connected to Device:"

    #@4b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v2

    #@4f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v2

    #@57
    invoke-direct {p0, v2}, Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V

    #@5a
    .line 628
    :goto_5a
    const-string v2, "android.bluetooth.device.extra.DEVICE"

    #@5c
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@5f
    .line 629
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@62
    .line 630
    sget-object v2, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@64
    sget-object v3, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@66
    const-string v3, "android.permission.BLUETOOTH"

    #@68
    invoke-virtual {v2, v1, v3}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@6b
    goto :goto_2a

    #@6c
    .line 625
    :cond_6c
    new-instance v1, Landroid/content/Intent;

    #@6e
    .end local v1           #intent:Landroid/content/Intent;
    const-string v2, "android.bluetooth.device.action.ACL_DISCONNECTED"

    #@70
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@73
    .line 626
    .restart local v1       #intent:Landroid/content/Intent;
    new-instance v2, Ljava/lang/StringBuilder;

    #@75
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@78
    const-string v3, "aclStateChangeCallback: State:DisConnected to Device:"

    #@7a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v2

    #@7e
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v2

    #@82
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v2

    #@86
    invoke-direct {p0, v2}, Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V

    #@89
    goto :goto_5a
.end method

.method addDeviceProperties([B)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    .registers 7
    .parameter "address"

    #@0
    .prologue
    .line 101
    iget-object v3, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mDevices:Ljava/util/HashMap;

    #@2
    monitor-enter v3

    #@3
    .line 102
    :try_start_3
    new-instance v1, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@5
    invoke-direct {v1, p0}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;-><init>(Lcom/android/bluetooth/btservice/RemoteDevices;)V

    #@8
    .line 103
    .local v1, prop:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    sget-object v2, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@a
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@d
    move-result-object v4

    #@e
    invoke-virtual {v2, v4}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@11
    move-result-object v0

    #@12
    .line 105
    .local v0, device:Landroid/bluetooth/BluetoothDevice;
    #setter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAddress:[B
    invoke-static {v1, p1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$002(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;[B)[B

    #@15
    .line 106
    iget-object v2, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mDevices:Ljava/util/HashMap;

    #@17
    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@1a
    .line 107
    monitor-exit v3

    #@1b
    return-object v1

    #@1c
    .line 108
    .end local v0           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v1           #prop:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    :catchall_1c
    move-exception v2

    #@1d
    monitor-exit v3
    :try_end_1e
    .catchall {:try_start_3 .. :try_end_1e} :catchall_1c

    #@1e
    throw v2
.end method

.method authorizeRequestCallback([BI)V
    .registers 10
    .parameter "address"
    .parameter "serviceId"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    const/4 v5, 0x1

    #@2
    .line 545
    invoke-static {p2}, Lcom/broadcom/bt/service/Utils;->getProfileUUIDFromServiceId(I)Landroid/os/ParcelUuid;

    #@5
    move-result-object v0

    #@6
    .line 548
    .local v0, ServiceUUID:Landroid/os/ParcelUuid;
    if-nez v0, :cond_e

    #@8
    .line 549
    const-string v3, "authorizeRequestCallback(): ServiceUUID is null."

    #@a
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/RemoteDevices;->errorLog(Ljava/lang/String;)V

    #@d
    .line 597
    :goto_d
    return-void

    #@e
    .line 553
    :cond_e
    sget-object v3, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@10
    invoke-virtual {v3}, Lcom/android/bluetooth/btservice/AdapterService;->isQuietModeEnabled()Z

    #@13
    move-result v3

    #@14
    if-eqz v3, :cond_21

    #@16
    .line 556
    sget-object v3, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@18
    sget-object v4, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@1a
    invoke-virtual {v4, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@1d
    move-result-object v4

    #@1e
    invoke-virtual {v3, v4, v0, v5, v6}, Lcom/android/bluetooth/btservice/AdapterService;->authorizeService(Landroid/bluetooth/BluetoothDevice;Landroid/os/ParcelUuid;ZZ)Z

    #@21
    .line 561
    :cond_21
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@24
    move-result-object v1

    #@25
    .line 562
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    if-nez v1, :cond_50

    #@27
    .line 563
    new-instance v3, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v4, "Device is not known for:"

    #@2e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v3

    #@32
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v3

    #@3a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v3

    #@3e
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/RemoteDevices;->warnLog(Ljava/lang/String;)V

    #@41
    .line 564
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->addDeviceProperties([B)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@44
    .line 565
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@47
    move-result-object v1

    #@48
    .line 567
    if-nez v1, :cond_50

    #@4a
    .line 568
    const-string v3, "authorizeRequestCallback(): device is null."

    #@4c
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/RemoteDevices;->errorLog(Ljava/lang/String;)V

    #@4f
    goto :goto_d

    #@50
    .line 573
    :cond_50
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothDevice;->getTrustState()Z

    #@53
    move-result v3

    #@54
    if-ne v3, v5, :cond_7c

    #@56
    .line 574
    new-instance v3, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v4, "trusted device :"

    #@5d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v3

    #@61
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v3

    #@69
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6c
    move-result-object v3

    #@6d
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/RemoteDevices;->warnLog(Ljava/lang/String;)V

    #@70
    .line 575
    sget-object v3, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@72
    sget-object v4, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@74
    invoke-virtual {v4, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@77
    move-result-object v4

    #@78
    invoke-virtual {v3, v4, v0, v5, v6}, Lcom/android/bluetooth/btservice/AdapterService;->authorizeService(Landroid/bluetooth/BluetoothDevice;Landroid/os/ParcelUuid;ZZ)Z

    #@7b
    goto :goto_d

    #@7c
    .line 588
    :cond_7c
    new-instance v2, Landroid/content/Intent;

    #@7e
    const-string v3, "android.bluetooth.device.action.CONNECTION_ACCESS_REQUEST"

    #@80
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@83
    .line 589
    .local v2, intent:Landroid/content/Intent;
    const-string v3, "com.android.settings"

    #@85
    const-string v4, "com.android.settings.bluetooth.BluetoothPermissionRequest"

    #@87
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@8a
    .line 591
    const-string v3, "android.bluetooth.device.extra.ACCESS_REQUEST_TYPE"

    #@8c
    invoke-static {v0}, Lcom/lge/bluetooth/LGBluetoothDevice;->getRequestServiceType(Landroid/os/ParcelUuid;)I

    #@8f
    move-result v4

    #@90
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@93
    .line 593
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    #@95
    sget-object v4, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@97
    invoke-virtual {v4, p1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@9a
    move-result-object v4

    #@9b
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@9e
    .line 594
    new-instance v3, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v4, "[BTUI] ("

    #@a5
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v3

    #@a9
    invoke-static {v0}, Lcom/lge/bluetooth/LGBluetoothDevice;->getRequestServiceType(Landroid/os/ParcelUuid;)I

    #@ac
    move-result v4

    #@ad
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v3

    #@b1
    const-string v4, ") = "

    #@b3
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v3

    #@b7
    invoke-virtual {v0}, Landroid/os/ParcelUuid;->toString()Ljava/lang/String;

    #@ba
    move-result-object v4

    #@bb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v3

    #@bf
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v3

    #@c3
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V

    #@c6
    .line 596
    sget-object v3, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@c8
    sget-object v4, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@ca
    const-string v4, "android.permission.BLUETOOTH_ADMIN"

    #@cc
    invoke-virtual {v3, v2, v4}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@cf
    goto/16 :goto_d
.end method

.method cleanup()V
    .registers 2

    #@0
    .prologue
    .line 73
    sget-object v0, Lcom/android/bluetooth/btservice/RemoteDevices;->mSdpTracker:Ljava/util/ArrayList;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 74
    sget-object v0, Lcom/android/bluetooth/btservice/RemoteDevices;->mSdpTracker:Ljava/util/ArrayList;

    #@6
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@9
    .line 76
    :cond_9
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mDevices:Ljava/util/HashMap;

    #@b
    if-eqz v0, :cond_12

    #@d
    .line 77
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mDevices:Ljava/util/HashMap;

    #@f
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@12
    .line 79
    :cond_12
    return-void
.end method

.method deviceFoundCallback([B)V
    .registers 8
    .parameter "address"

    #@0
    .prologue
    .line 404
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    .line 405
    .local v0, device:Landroid/bluetooth/BluetoothDevice;
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "deviceFoundCallback: Remote Address is:"

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v3

    #@17
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V

    #@1a
    .line 406
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@1d
    move-result-object v1

    #@1e
    .line 407
    .local v1, deviceProp:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v1, :cond_37

    #@20
    .line 408
    new-instance v3, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v4, "Device Properties is null for Device:"

    #@27
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v3

    #@2b
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v3

    #@33
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/RemoteDevices;->errorLog(Ljava/lang/String;)V

    #@36
    .line 431
    :goto_36
    return-void

    #@37
    .line 413
    :cond_37
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3a
    move-result-object v3

    #@3b
    if-eqz v3, :cond_61

    #@3d
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@40
    move-result-object v3

    #@41
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@44
    move-result-object v4

    #@45
    invoke-interface {v3, v4}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@48
    move-result v3

    #@49
    if-nez v3, :cond_59

    #@4b
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4e
    move-result-object v3

    #@4f
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@52
    move-result-object v4

    #@53
    invoke-interface {v3, v4}, Lcom/lge/cappuccino/IMdm;->checkBluetoothDevice(Ljava/lang/String;)Z

    #@56
    move-result v3

    #@57
    if-eqz v3, :cond_61

    #@59
    .line 418
    :cond_59
    const-string v3, "BluetoothRemoteDevices"

    #@5b
    const-string v4, "deviceFoundCallback  is blocked by LG MDM Server Policy"

    #@5d
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@60
    goto :goto_36

    #@61
    .line 423
    :cond_61
    new-instance v2, Landroid/content/Intent;

    #@63
    const-string v3, "android.bluetooth.device.action.FOUND"

    #@65
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@68
    .line 424
    .local v2, intent:Landroid/content/Intent;
    const-string v3, "android.bluetooth.device.extra.DEVICE"

    #@6a
    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@6d
    .line 425
    const-string v3, "android.bluetooth.device.extra.CLASS"

    #@6f
    new-instance v4, Landroid/bluetooth/BluetoothClass;

    #@71
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mBluetoothClass:I
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$700(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)I

    #@74
    move-result v5

    #@75
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@78
    move-result-object v5

    #@79
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@7c
    move-result v5

    #@7d
    invoke-direct {v4, v5}, Landroid/bluetooth/BluetoothClass;-><init>(I)V

    #@80
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@83
    .line 427
    const-string v3, "android.bluetooth.device.extra.RSSI"

    #@85
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mRssi:S
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$900(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)S

    #@88
    move-result v4

    #@89
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;S)Landroid/content/Intent;

    #@8c
    .line 428
    const-string v3, "android.bluetooth.device.extra.NAME"

    #@8e
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mName:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$500(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Ljava/lang/String;

    #@91
    move-result-object v4

    #@92
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@95
    .line 430
    sget-object v3, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@97
    sget-object v4, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@99
    const-string v4, "android.permission.BLUETOOTH"

    #@9b
    invoke-virtual {v3, v2, v4}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@9e
    goto :goto_36
.end method

.method devicePropertyChangedCallback([B[I[[B)V
    .registers 17
    .parameter "address"
    .parameter "types"
    .parameter "values"

    #@0
    .prologue
    .line 297
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@3
    move-result-object v0

    #@4
    .line 299
    .local v0, bdDevice:Landroid/bluetooth/BluetoothDevice;
    if-nez v0, :cond_16

    #@6
    .line 300
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->addDeviceProperties([B)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@9
    move-result-object v1

    #@a
    .line 301
    .local v1, device:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@d
    move-result-object v0

    #@e
    .line 306
    :goto_e
    if-nez v1, :cond_1b

    #@10
    .line 307
    const-string v8, "devicePropertyChangedCallback(): device is null."

    #@12
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/RemoteDevices;->errorLog(Ljava/lang/String;)V

    #@15
    .line 399
    :cond_15
    :goto_15
    return-void

    #@16
    .line 303
    .end local v1           #device:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    :cond_16
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@19
    move-result-object v1

    #@1a
    .restart local v1       #device:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    goto :goto_e

    #@1b
    .line 312
    :cond_1b
    const/4 v3, 0x0

    #@1c
    .local v3, j:I
    :goto_1c
    array-length v8, p2

    #@1d
    if-ge v3, v8, :cond_15

    #@1f
    .line 313
    aget v6, p2, v3

    #@21
    .line 314
    .local v6, type:I
    aget-object v7, p3, v3

    #@23
    .line 315
    .local v7, val:[B
    array-length v8, v7

    #@24
    if-gtz v8, :cond_49

    #@26
    .line 316
    new-instance v8, Ljava/lang/StringBuilder;

    #@28
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@2b
    const-string v9, "devicePropertyChangedCallback: bdDevice: "

    #@2d
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v8

    #@31
    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v8

    #@35
    const-string v9, ", value is empty for type: "

    #@37
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v8

    #@3b
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v8

    #@3f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v8

    #@43
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/RemoteDevices;->errorLog(Ljava/lang/String;)V

    #@46
    .line 312
    :goto_46
    add-int/lit8 v3, v3, 0x1

    #@48
    goto :goto_1c

    #@49
    .line 319
    :cond_49
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@4c
    move-result-object v8

    #@4d
    if-eqz v8, :cond_65

    #@4f
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@52
    move-result-object v8

    #@53
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@56
    move-result-object v9

    #@57
    invoke-interface {v8, v9}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@5a
    move-result v8

    #@5b
    if-eqz v8, :cond_65

    #@5d
    .line 322
    const-string v8, "BluetoothRemoteDevices"

    #@5f
    const-string v9, "devicePropertyChangedCallback  is blocked by LG MDM Server Policy"

    #@61
    invoke-static {v8, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    goto :goto_15

    #@65
    .line 326
    :cond_65
    iget-object v9, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;

    #@67
    monitor-enter v9

    #@68
    .line 327
    packed-switch v6, :pswitch_data_1ac

    #@6b
    .line 396
    :goto_6b
    :pswitch_6b
    :try_start_6b
    monitor-exit v9

    #@6c
    goto :goto_46

    #@6d
    :catchall_6d
    move-exception v8

    #@6e
    monitor-exit v9
    :try_end_6f
    .catchall {:try_start_6b .. :try_end_6f} :catchall_6d

    #@6f
    throw v8

    #@70
    .line 329
    :pswitch_70
    :try_start_70
    new-instance v8, Ljava/lang/String;

    #@72
    invoke-direct {v8, v7}, Ljava/lang/String;-><init>([B)V

    #@75
    #setter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mName:Ljava/lang/String;
    invoke-static {v1, v8}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$502(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;Ljava/lang/String;)Ljava/lang/String;

    #@78
    .line 330
    new-instance v2, Landroid/content/Intent;

    #@7a
    const-string v8, "android.bluetooth.device.action.NAME_CHANGED"

    #@7c
    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7f
    .line 331
    .local v2, intent:Landroid/content/Intent;
    const-string v8, "android.bluetooth.device.extra.DEVICE"

    #@81
    invoke-virtual {v2, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@84
    .line 332
    const-string v8, "android.bluetooth.device.extra.NAME"

    #@86
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mName:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$500(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Ljava/lang/String;

    #@89
    move-result-object v10

    #@8a
    invoke-virtual {v2, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@8d
    .line 333
    const/high16 v8, 0x800

    #@8f
    invoke-virtual {v2, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@92
    .line 334
    sget-object v8, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@94
    sget-object v10, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@96
    const-string v10, "android.permission.BLUETOOTH"

    #@98
    invoke-virtual {v8, v2, v10}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@9b
    .line 335
    new-instance v8, Ljava/lang/StringBuilder;

    #@9d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@a0
    const-string v10, "Remote Device name is: "

    #@a2
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a5
    move-result-object v8

    #@a6
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mName:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$500(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Ljava/lang/String;

    #@a9
    move-result-object v10

    #@aa
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ad
    move-result-object v8

    #@ae
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b1
    move-result-object v8

    #@b2
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V

    #@b5
    goto :goto_6b

    #@b6
    .line 338
    .end local v2           #intent:Landroid/content/Intent;
    :pswitch_b6
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAlias:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$600(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Ljava/lang/String;

    #@b9
    move-result-object v8

    #@ba
    if-eqz v8, :cond_c7

    #@bc
    .line 339
    const/4 v8, 0x0

    #@bd
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAlias:Ljava/lang/String;
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$600(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Ljava/lang/String;

    #@c0
    move-result-object v10

    #@c1
    const/4 v11, 0x0

    #@c2
    array-length v12, v7

    #@c3
    invoke-static {v7, v8, v10, v11, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@c6
    goto :goto_6b

    #@c7
    .line 342
    :cond_c7
    new-instance v8, Ljava/lang/String;

    #@c9
    invoke-direct {v8, v7}, Ljava/lang/String;-><init>([B)V

    #@cc
    #setter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAlias:Ljava/lang/String;
    invoke-static {v1, v8}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$602(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;Ljava/lang/String;)Ljava/lang/String;

    #@cf
    goto :goto_6b

    #@d0
    .line 346
    :pswitch_d0
    #setter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAddress:[B
    invoke-static {v1, v7}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$002(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;[B)[B

    #@d3
    .line 347
    new-instance v8, Ljava/lang/StringBuilder;

    #@d5
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d8
    const-string v10, "Remote Address is:"

    #@da
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@dd
    move-result-object v8

    #@de
    invoke-static {v7}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@e1
    move-result-object v10

    #@e2
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e5
    move-result-object v8

    #@e6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e9
    move-result-object v8

    #@ea
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V

    #@ed
    goto/16 :goto_6b

    #@ef
    .line 350
    :pswitch_ef
    invoke-static {v7}, Lcom/android/bluetooth/Utils;->byteArrayToInt([B)I

    #@f2
    move-result v8

    #@f3
    #setter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mBluetoothClass:I
    invoke-static {v1, v8}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$702(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;I)I

    #@f6
    .line 351
    new-instance v2, Landroid/content/Intent;

    #@f8
    const-string v8, "android.bluetooth.device.action.CLASS_CHANGED"

    #@fa
    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@fd
    .line 352
    .restart local v2       #intent:Landroid/content/Intent;
    const-string v8, "android.bluetooth.device.extra.DEVICE"

    #@ff
    invoke-virtual {v2, v8, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@102
    .line 353
    const-string v8, "android.bluetooth.device.extra.CLASS"

    #@104
    new-instance v10, Landroid/bluetooth/BluetoothClass;

    #@106
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mBluetoothClass:I
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$700(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)I

    #@109
    move-result v11

    #@10a
    invoke-direct {v10, v11}, Landroid/bluetooth/BluetoothClass;-><init>(I)V

    #@10d
    invoke-virtual {v2, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@110
    .line 355
    const/high16 v8, 0x800

    #@112
    invoke-virtual {v2, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@115
    .line 356
    sget-object v8, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@117
    sget-object v10, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@119
    const-string v10, "android.permission.BLUETOOTH"

    #@11b
    invoke-virtual {v8, v2, v10}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@11e
    .line 357
    new-instance v8, Ljava/lang/StringBuilder;

    #@120
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@123
    const-string v10, "Remote class is:"

    #@125
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@128
    move-result-object v8

    #@129
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mBluetoothClass:I
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$700(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)I

    #@12c
    move-result v10

    #@12d
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@130
    move-result-object v8

    #@131
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@134
    move-result-object v8

    #@135
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V

    #@138
    goto/16 :goto_6b

    #@13a
    .line 360
    .end local v2           #intent:Landroid/content/Intent;
    :pswitch_13a
    array-length v8, v7

    #@13b
    div-int/lit8 v4, v8, 0x10

    #@13d
    .line 361
    .local v4, numUuids:I
    invoke-static {v7}, Lcom/android/bluetooth/Utils;->byteArrayToUuid([B)[Landroid/os/ParcelUuid;

    #@140
    move-result-object v8

    #@141
    #setter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mUuids:[Landroid/os/ParcelUuid;
    invoke-static {v1, v8}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$402(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;[Landroid/os/ParcelUuid;)[Landroid/os/ParcelUuid;

    #@144
    .line 362
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->sendUuidIntent(Landroid/bluetooth/BluetoothDevice;)V

    #@147
    goto/16 :goto_6b

    #@149
    .line 365
    .end local v4           #numUuids:I
    :pswitch_149
    invoke-static {v7}, Lcom/android/bluetooth/Utils;->byteArrayToInt([B)I

    #@14c
    move-result v8

    #@14d
    #setter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceType:I
    invoke-static {v1, v8}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$802(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;I)I

    #@150
    goto/16 :goto_6b

    #@152
    .line 368
    :pswitch_152
    invoke-static {v7}, Lcom/android/bluetooth/Utils;->byteArrayToShort([B)S

    #@155
    move-result v8

    #@156
    #setter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mRssi:S
    invoke-static {v1, v8}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$902(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;S)S

    #@159
    goto/16 :goto_6b

    #@15b
    .line 377
    :pswitch_15b
    invoke-static {v7}, Lcom/android/bluetooth/Utils;->byteArrayToInt([B)I

    #@15e
    move-result v5

    #@15f
    .line 378
    .local v5, temp:I
    if-eqz v5, :cond_181

    #@161
    const/4 v8, 0x1

    #@162
    :goto_162
    #setter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceTrust:Z
    invoke-static {v1, v8}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$1002(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;Z)Z

    #@165
    .line 379
    new-instance v8, Ljava/lang/StringBuilder;

    #@167
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@16a
    const-string v10, "devicePropertyChangedCallback: mDeviceTrust: "

    #@16c
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v8

    #@170
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceTrust:Z
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$1000(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Z

    #@173
    move-result v10

    #@174
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@177
    move-result-object v8

    #@178
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17b
    move-result-object v8

    #@17c
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/RemoteDevices;->errorLog(Ljava/lang/String;)V

    #@17f
    goto/16 :goto_6b

    #@181
    .line 378
    :cond_181
    const/4 v8, 0x0

    #@182
    goto :goto_162

    #@183
    .line 387
    .end local v5           #temp:I
    :pswitch_183
    invoke-static {v7}, Lcom/android/bluetooth/Utils;->byteArrayToInt([B)I

    #@186
    move-result v5

    #@187
    .line 388
    .restart local v5       #temp:I
    if-eqz v5, :cond_1a9

    #@189
    const/4 v8, 0x1

    #@18a
    :goto_18a
    #setter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mJustWorks:Z
    invoke-static {v1, v8}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$1102(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;Z)Z

    #@18d
    .line 389
    new-instance v8, Ljava/lang/StringBuilder;

    #@18f
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@192
    const-string v10, "devicePropertyChangedCallback: mJustWorks: "

    #@194
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@197
    move-result-object v8

    #@198
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mJustWorks:Z
    invoke-static {v1}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->access$1100(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Z

    #@19b
    move-result v10

    #@19c
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19f
    move-result-object v8

    #@1a0
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a3
    move-result-object v8

    #@1a4
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/RemoteDevices;->errorLog(Ljava/lang/String;)V
    :try_end_1a7
    .catchall {:try_start_70 .. :try_end_1a7} :catchall_6d

    #@1a7
    goto/16 :goto_6b

    #@1a9
    .line 388
    :cond_1a9
    const/4 v8, 0x0

    #@1aa
    goto :goto_18a

    #@1ab
    .line 327
    nop

    #@1ac
    :pswitch_data_1ac
    .packed-switch 0x1
        :pswitch_70
        :pswitch_d0
        :pswitch_13a
        :pswitch_ef
        :pswitch_149
        :pswitch_6b
        :pswitch_6b
        :pswitch_6b
        :pswitch_6b
        :pswitch_b6
        :pswitch_152
        :pswitch_15b
        :pswitch_183
    .end packed-switch
.end method

.method fetchUuids(Landroid/bluetooth/BluetoothDevice;)V
    .registers 6
    .parameter "device"

    #@0
    .prologue
    .line 634
    sget-object v1, Lcom/android/bluetooth/btservice/RemoteDevices;->mSdpTracker:Ljava/util/ArrayList;

    #@2
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@5
    move-result v1

    #@6
    if-eqz v1, :cond_9

    #@8
    .line 645
    :goto_8
    return-void

    #@9
    .line 637
    :cond_9
    sget-object v1, Lcom/android/bluetooth/btservice/RemoteDevices;->mSdpTracker:Ljava/util/ArrayList;

    #@b
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@e
    .line 639
    iget-object v1, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mHandler:Landroid/os/Handler;

    #@10
    const/4 v2, 0x1

    #@11
    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    #@14
    move-result-object v0

    #@15
    .line 640
    .local v0, message:Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    #@17
    .line 641
    iget-object v1, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mHandler:Landroid/os/Handler;

    #@19
    const-wide/16 v2, 0x1770

    #@1b
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@1e
    .line 644
    sget-object v1, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@20
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    invoke-static {v2}, Lcom/android/bluetooth/Utils;->getBytesFromAddress(Ljava/lang/String;)[B

    #@27
    move-result-object v2

    #@28
    invoke-virtual {v1, v2}, Lcom/android/bluetooth/btservice/AdapterService;->getRemoteServicesNative([B)Z

    #@2b
    goto :goto_8
.end method

.method getDevice([B)Landroid/bluetooth/BluetoothDevice;
    .registers 6
    .parameter "address"

    #@0
    .prologue
    .line 92
    iget-object v2, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mDevices:Ljava/util/HashMap;

    #@2
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@5
    move-result-object v2

    #@6
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@9
    move-result-object v1

    #@a
    .local v1, i$:Ljava/util/Iterator;
    :cond_a
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    #@d
    move-result v2

    #@e
    if-eqz v2, :cond_25

    #@10
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@13
    move-result-object v0

    #@14
    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    #@16
    .line 93
    .local v0, dev:Landroid/bluetooth/BluetoothDevice;
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    #@19
    move-result-object v2

    #@1a
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v2

    #@22
    if-eqz v2, :cond_a

    #@24
    .line 97
    .end local v0           #dev:Landroid/bluetooth/BluetoothDevice;
    :goto_24
    return-object v0

    #@25
    :cond_25
    const/4 v0, 0x0

    #@26
    goto :goto_24
.end method

.method getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    .registers 4
    .parameter "device"

    #@0
    .prologue
    .line 86
    iget-object v1, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mDevices:Ljava/util/HashMap;

    #@2
    monitor-enter v1

    #@3
    .line 87
    :try_start_3
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices;->mDevices:Ljava/util/HashMap;

    #@5
    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@8
    move-result-object v0

    #@9
    check-cast v0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@b
    monitor-exit v1

    #@c
    return-object v0

    #@d
    .line 88
    :catchall_d
    move-exception v0

    #@e
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_3 .. :try_end_f} :catchall_d

    #@f
    throw v0
.end method

.method pinRequestCallback([B[BI)V
    .registers 13
    .parameter "address"
    .parameter "name"
    .parameter "cod"

    #@0
    .prologue
    .line 436
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@3
    move-result-object v5

    #@4
    if-eqz v5, :cond_33

    #@6
    .line 437
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9
    move-result-object v5

    #@a
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@d
    move-result-object v6

    #@e
    invoke-interface {v5, v6}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@11
    move-result v5

    #@12
    if-eqz v5, :cond_1c

    #@14
    .line 439
    const-string v5, "BluetoothRemoteDevices"

    #@16
    const-string v6, "pinRequestCallback  is blocked by LG MDM Server Policy"

    #@18
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 477
    :goto_1b
    return-void

    #@1c
    .line 441
    :cond_1c
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@1f
    move-result-object v5

    #@20
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@23
    move-result-object v6

    #@24
    const/4 v7, 0x1

    #@25
    invoke-interface {v5, v6, v7}, Lcom/lge/cappuccino/IMdm;->checkBluetoothPairing(Ljava/lang/String;Z)Z

    #@28
    move-result v5

    #@29
    if-eqz v5, :cond_33

    #@2b
    .line 443
    const-string v5, "BluetoothRemoteDevices"

    #@2d
    const-string v6, "pinRequestCallback  is blocked by LG MDM Server Policy"

    #@2f
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_1b

    #@33
    .line 448
    :cond_33
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@36
    move-result-object v0

    #@37
    .line 449
    .local v0, bdDevice:Landroid/bluetooth/BluetoothDevice;
    if-nez v0, :cond_3c

    #@39
    .line 450
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->addDeviceProperties([B)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@3c
    .line 452
    :cond_3c
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getBluetoothClass()Landroid/bluetooth/BluetoothClass;

    #@3f
    move-result-object v1

    #@40
    .line 453
    .local v1, btClass:Landroid/bluetooth/BluetoothClass;
    invoke-virtual {v1}, Landroid/bluetooth/BluetoothClass;->getDeviceClass()I

    #@43
    move-result v2

    #@44
    .line 454
    .local v2, btDeviceClass:I
    const/16 v5, 0x540

    #@46
    if-eq v2, v5, :cond_4c

    #@48
    const/16 v5, 0x5c0

    #@4a
    if-ne v2, v5, :cond_5f

    #@4c
    .line 466
    :cond_4c
    invoke-static {}, Ljava/lang/Math;->random()D

    #@4f
    move-result-wide v5

    #@50
    const-wide v7, 0x412e848000000000L

    #@55
    mul-double/2addr v5, v7

    #@56
    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    #@59
    move-result-wide v5

    #@5a
    double-to-int v4, v5

    #@5b
    .line 467
    .local v4, pin:I
    invoke-direct {p0, p1, v4}, Lcom/android/bluetooth/btservice/RemoteDevices;->sendDisplayPinIntent([BI)V

    #@5e
    goto :goto_1b

    #@5f
    .line 470
    .end local v4           #pin:I
    :cond_5f
    new-instance v5, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v6, "pinRequestCallback: "

    #@66
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v5

    #@6e
    const-string v6, " name:"

    #@70
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v5

    #@74
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@77
    move-result-object v5

    #@78
    const-string v6, " cod:"

    #@7a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v5

    #@7e
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@81
    move-result-object v5

    #@82
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v5

    #@86
    invoke-direct {p0, v5}, Lcom/android/bluetooth/btservice/RemoteDevices;->infoLog(Ljava/lang/String;)V

    #@89
    .line 472
    new-instance v3, Landroid/content/Intent;

    #@8b
    const-string v5, "android.bluetooth.device.action.PAIRING_REQUEST"

    #@8d
    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@90
    .line 473
    .local v3, intent:Landroid/content/Intent;
    const-string v5, "android.bluetooth.device.extra.DEVICE"

    #@92
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@95
    move-result-object v6

    #@96
    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@99
    .line 474
    const-string v5, "android.bluetooth.device.extra.PAIRING_VARIANT"

    #@9b
    const/4 v6, 0x0

    #@9c
    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@9f
    .line 476
    sget-object v5, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@a1
    sget-object v6, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@a3
    const-string v6, "android.permission.BLUETOOTH_ADMIN"

    #@a5
    invoke-virtual {v5, v3, v6}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@a8
    goto/16 :goto_1b
.end method

.method sspRequestCallback([B[BIII)V
    .registers 14
    .parameter "address"
    .parameter "name"
    .parameter "cod"
    .parameter "pairingVariant"
    .parameter "passkey"

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    .line 483
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@4
    move-result-object v0

    #@5
    .line 484
    .local v0, bdDevice:Landroid/bluetooth/BluetoothDevice;
    if-nez v0, :cond_a

    #@7
    .line 485
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->addDeviceProperties([B)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@a
    .line 489
    :cond_a
    new-instance v5, Ljava/lang/StringBuilder;

    #@c
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@f
    const-string v6, "sspRequestCallback: "

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    const-string v6, " name: "

    #@1b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v5

    #@1f
    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v5

    #@23
    const-string v6, " cod: "

    #@25
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v5

    #@29
    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    const-string v6, " pairingVariant "

    #@2f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v5

    #@33
    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v5

    #@3b
    invoke-direct {p0, v5}, Lcom/android/bluetooth/btservice/RemoteDevices;->infoLog(Ljava/lang/String;)V

    #@3e
    .line 498
    const/4 v2, 0x0

    #@3f
    .line 499
    .local v2, displayPasskey:Z
    if-nez p4, :cond_86

    #@41
    .line 500
    const/4 v4, 0x2

    #@42
    .line 501
    .local v4, variant:I
    const/4 v2, 0x1

    #@43
    .line 513
    :goto_43
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@46
    move-result-object v1

    #@47
    .line 514
    .local v1, device:Landroid/bluetooth/BluetoothDevice;
    if-nez v1, :cond_6a

    #@49
    .line 515
    new-instance v5, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v6, "Device is not known for:"

    #@50
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v5

    #@54
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v5

    #@5c
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v5

    #@60
    invoke-direct {p0, v5}, Lcom/android/bluetooth/btservice/RemoteDevices;->warnLog(Ljava/lang/String;)V

    #@63
    .line 516
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->addDeviceProperties([B)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@66
    .line 517
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDevice([B)Landroid/bluetooth/BluetoothDevice;

    #@69
    move-result-object v1

    #@6a
    .line 520
    :cond_6a
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@6d
    move-result-object v5

    #@6e
    if-eqz v5, :cond_b1

    #@70
    .line 521
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@73
    move-result-object v5

    #@74
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@77
    move-result-object v6

    #@78
    invoke-interface {v5, v6}, Lcom/lge/cappuccino/IMdm;->checkBluetoothAudioOnly(Ljava/lang/String;)Z

    #@7b
    move-result v5

    #@7c
    if-eqz v5, :cond_9b

    #@7e
    .line 523
    const-string v5, "BluetoothRemoteDevices"

    #@80
    const-string v6, "sspRequestCallback  is blocked by LG MDM Server Policy"

    #@82
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    .line 539
    .end local v1           #device:Landroid/bluetooth/BluetoothDevice;
    .end local v4           #variant:I
    :goto_85
    return-void

    #@86
    .line 502
    :cond_86
    const/4 v5, 0x2

    #@87
    if-ne p4, v5, :cond_8b

    #@89
    .line 503
    const/4 v4, 0x3

    #@8a
    .restart local v4       #variant:I
    goto :goto_43

    #@8b
    .line 504
    .end local v4           #variant:I
    :cond_8b
    if-ne p4, v7, :cond_8f

    #@8d
    .line 505
    const/4 v4, 0x1

    #@8e
    .restart local v4       #variant:I
    goto :goto_43

    #@8f
    .line 506
    .end local v4           #variant:I
    :cond_8f
    const/4 v5, 0x3

    #@90
    if-ne p4, v5, :cond_95

    #@92
    .line 507
    const/4 v4, 0x4

    #@93
    .line 508
    .restart local v4       #variant:I
    const/4 v2, 0x1

    #@94
    goto :goto_43

    #@95
    .line 510
    .end local v4           #variant:I
    :cond_95
    const-string v5, "SSP Pairing variant not present"

    #@97
    invoke-direct {p0, v5}, Lcom/android/bluetooth/btservice/RemoteDevices;->errorLog(Ljava/lang/String;)V

    #@9a
    goto :goto_85

    #@9b
    .line 525
    .restart local v1       #device:Landroid/bluetooth/BluetoothDevice;
    .restart local v4       #variant:I
    :cond_9b
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9e
    move-result-object v5

    #@9f
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@a2
    move-result-object v6

    #@a3
    invoke-interface {v5, v6, v7}, Lcom/lge/cappuccino/IMdm;->checkBluetoothPairing(Ljava/lang/String;Z)Z

    #@a6
    move-result v5

    #@a7
    if-eqz v5, :cond_b1

    #@a9
    .line 527
    const-string v5, "BluetoothRemoteDevices"

    #@ab
    const-string v6, "sspRequestCallback  is blocked by LG MDM Server Policy"

    #@ad
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@b0
    goto :goto_85

    #@b1
    .line 532
    :cond_b1
    new-instance v3, Landroid/content/Intent;

    #@b3
    const-string v5, "android.bluetooth.device.action.PAIRING_REQUEST"

    #@b5
    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@b8
    .line 533
    .local v3, intent:Landroid/content/Intent;
    const-string v5, "android.bluetooth.device.extra.DEVICE"

    #@ba
    invoke-virtual {v3, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@bd
    .line 534
    if-eqz v2, :cond_c4

    #@bf
    .line 535
    const-string v5, "android.bluetooth.device.extra.PAIRING_KEY"

    #@c1
    invoke-virtual {v3, v5, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c4
    .line 537
    :cond_c4
    const-string v5, "android.bluetooth.device.extra.PAIRING_VARIANT"

    #@c6
    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@c9
    .line 538
    sget-object v5, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@cb
    sget-object v6, Lcom/android/bluetooth/btservice/RemoteDevices;->mAdapterService:Lcom/android/bluetooth/btservice/AdapterService;

    #@cd
    const-string v6, "android.permission.BLUETOOTH_ADMIN"

    #@cf
    invoke-virtual {v5, v3, v6}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@d2
    goto :goto_85
.end method
