.class Lcom/android/bluetooth/btservice/AdapterProperties;
.super Ljava/lang/Object;
.source "AdapterProperties.java"


# static fields
.field private static final BD_ADDR_LEN:I = 0x6

.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "BluetoothAdapterProperties"


# instance fields
.field private mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private mAddress:[B

.field private mBluetoothClass:I

.field private mBluetoothDisabling:Z

.field private mBondedDevices:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/bluetooth/BluetoothDevice;",
            ">;"
        }
    .end annotation
.end field

.field private mConnectionState:I

.field private mDiscoverableTimeout:I

.field private mDiscovering:Z

.field private mName:Ljava/lang/String;

.field private mObject:Ljava/lang/Object;

.field private mProfileConnectionState:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field

.field private mProfilesConnected:I

.field private mProfilesConnecting:I

.field private mProfilesDisconnecting:I

.field private mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

.field private mScanMode:I

.field private mService:Lcom/android/bluetooth/btservice/AdapterService;

.field private mState:I

.field private mUuids:[Landroid/os/ParcelUuid;


# direct methods
.method public constructor <init>(Lcom/android/bluetooth/btservice/AdapterService;)V
    .registers 4
    .parameter "service"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@4
    .line 51
    new-instance v0, Ljava/util/ArrayList;

    #@6
    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBondedDevices:Ljava/util/ArrayList;

    #@b
    .line 59
    iput v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mConnectionState:I

    #@d
    .line 60
    const/16 v0, 0xa

    #@f
    iput v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mState:I

    #@11
    .line 70
    new-instance v0, Ljava/lang/Object;

    #@13
    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    #@16
    iput-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@18
    .line 567
    iput-boolean v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBluetoothDisabling:Z

    #@1a
    .line 73
    iput-object p1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@1c
    .line 74
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@1f
    move-result-object v0

    #@20
    iput-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@22
    .line 75
    return-void
.end method

.method private convertToAdapterState(I)I
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 365
    packed-switch p1, :pswitch_data_14

    #@3
    .line 377
    const-string v0, "BluetoothAdapterProperties"

    #@5
    const-string v1, "Error in convertToAdapterState"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 378
    const/4 v0, -0x1

    #@b
    :goto_b
    return v0

    #@c
    .line 367
    :pswitch_c
    const/4 v0, 0x0

    #@d
    goto :goto_b

    #@e
    .line 369
    :pswitch_e
    const/4 v0, 0x3

    #@f
    goto :goto_b

    #@10
    .line 371
    :pswitch_10
    const/4 v0, 0x2

    #@11
    goto :goto_b

    #@12
    .line 373
    :pswitch_12
    const/4 v0, 0x1

    #@13
    goto :goto_b

    #@14
    .line 365
    :pswitch_data_14
    .packed-switch 0x0
        :pswitch_c
        :pswitch_12
        :pswitch_10
        :pswitch_e
    .end packed-switch
.end method

.method private debugLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 605
    const-string v0, "BluetoothAdapterProperties"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 607
    return-void
.end method

.method private errorLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 610
    const-string v0, "BluetoothAdapterProperties"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 611
    return-void
.end method

.method private infoLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 599
    const-string v0, "BluetoothAdapterProperties"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 601
    return-void
.end method

.method private updateCountersAndCheckForConnectionStateChange(II)Z
    .registers 8
    .parameter "state"
    .parameter "prevState"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v0, 0x1

    #@2
    .line 382
    const-string v2, "BluetoothAdapterProperties"

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "[BTUI] ###1_updateCountersAndCheckForConnectionStateChange: prevState( "

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v3

    #@13
    const-string v4, " )"

    #@15
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v3

    #@19
    const-string v4, " -> state( "

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    const-string v4, " )"

    #@25
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 383
    packed-switch p2, :pswitch_data_d2

    #@33
    .line 406
    :cond_33
    :goto_33
    const-string v2, "BluetoothAdapterProperties"

    #@35
    new-instance v3, Ljava/lang/StringBuilder;

    #@37
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3a
    const-string v4, "[BTUI] ###2_updateCountersAndCheckForConnectionStateChange: mProfilesConnecting( "

    #@3c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v3

    #@40
    iget v4, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnecting:I

    #@42
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@45
    move-result-object v3

    #@46
    const-string v4, " ), "

    #@48
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v3

    #@4c
    const-string v4, "mProfilesConnected( "

    #@4e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v3

    #@52
    iget v4, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnected:I

    #@54
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@57
    move-result-object v3

    #@58
    const-string v4, " ), "

    #@5a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v3

    #@5e
    const-string v4, "mProfilesDisconnecting( "

    #@60
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    iget v4, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesDisconnecting:I

    #@66
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@69
    move-result-object v3

    #@6a
    const-string v4, " )"

    #@6c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6f
    move-result-object v3

    #@70
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@73
    move-result-object v3

    #@74
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    .line 409
    packed-switch p1, :pswitch_data_dc

    #@7a
    .line 426
    :cond_7a
    :goto_7a
    return v0

    #@7b
    .line 385
    :pswitch_7b
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnecting:I

    #@7d
    if-lez v2, :cond_33

    #@7f
    .line 386
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnecting:I

    #@81
    add-int/lit8 v2, v2, -0x1

    #@83
    iput v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnecting:I

    #@85
    goto :goto_33

    #@86
    .line 391
    :pswitch_86
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnected:I

    #@88
    if-lez v2, :cond_33

    #@8a
    .line 392
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnected:I

    #@8c
    add-int/lit8 v2, v2, -0x1

    #@8e
    iput v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnected:I

    #@90
    goto :goto_33

    #@91
    .line 397
    :pswitch_91
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesDisconnecting:I

    #@93
    if-lez v2, :cond_33

    #@95
    .line 398
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesDisconnecting:I

    #@97
    add-int/lit8 v2, v2, -0x1

    #@99
    iput v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesDisconnecting:I

    #@9b
    goto :goto_33

    #@9c
    .line 411
    :pswitch_9c
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnecting:I

    #@9e
    add-int/lit8 v2, v2, 0x1

    #@a0
    iput v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnecting:I

    #@a2
    .line 412
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnected:I

    #@a4
    if-nez v2, :cond_aa

    #@a6
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnecting:I

    #@a8
    if-eq v2, v0, :cond_7a

    #@aa
    :cond_aa
    move v0, v1

    #@ab
    goto :goto_7a

    #@ac
    .line 415
    :pswitch_ac
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnected:I

    #@ae
    add-int/lit8 v2, v2, 0x1

    #@b0
    iput v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnected:I

    #@b2
    .line 416
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnected:I

    #@b4
    if-eq v2, v0, :cond_7a

    #@b6
    move v0, v1

    #@b7
    goto :goto_7a

    #@b8
    .line 419
    :pswitch_b8
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesDisconnecting:I

    #@ba
    add-int/lit8 v2, v2, 0x1

    #@bc
    iput v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesDisconnecting:I

    #@be
    .line 420
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnected:I

    #@c0
    if-nez v2, :cond_c6

    #@c2
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesDisconnecting:I

    #@c4
    if-eq v2, v0, :cond_7a

    #@c6
    :cond_c6
    move v0, v1

    #@c7
    goto :goto_7a

    #@c8
    .line 423
    :pswitch_c8
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnected:I

    #@ca
    if-nez v2, :cond_d0

    #@cc
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfilesConnecting:I

    #@ce
    if-eqz v2, :cond_7a

    #@d0
    :cond_d0
    move v0, v1

    #@d1
    goto :goto_7a

    #@d2
    .line 383
    :pswitch_data_d2
    .packed-switch 0x1
        :pswitch_7b
        :pswitch_86
        :pswitch_91
    .end packed-switch

    #@dc
    .line 409
    :pswitch_data_dc
    .packed-switch 0x0
        :pswitch_c8
        :pswitch_9c
        :pswitch_ac
        :pswitch_b8
    .end packed-switch
.end method

.method private updateProfileConnectionState(III)V
    .registers 14
    .parameter "profile"
    .parameter "newState"
    .parameter "oldState"

    #@0
    .prologue
    const/4 v8, 0x2

    #@1
    const/4 v7, 0x1

    #@2
    .line 436
    const/4 v2, 0x1

    #@3
    .line 437
    .local v2, numDev:I
    move v1, p2

    #@4
    .line 438
    .local v1, newHashState:I
    const/4 v4, 0x1

    #@5
    .line 450
    .local v4, update:Z
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfileConnectionState:Ljava/util/HashMap;

    #@7
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@a
    move-result-object v6

    #@b
    invoke-virtual {v5, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@e
    move-result-object v3

    #@f
    check-cast v3, Landroid/util/Pair;

    #@11
    .line 451
    .local v3, stateNumDev:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz v3, :cond_27

    #@13
    .line 452
    iget-object v5, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@15
    check-cast v5, Ljava/lang/Integer;

    #@17
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@1a
    move-result v0

    #@1b
    .line 453
    .local v0, currHashState:I
    iget-object v5, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    #@1d
    check-cast v5, Ljava/lang/Integer;

    #@1f
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    #@22
    move-result v2

    #@23
    .line 455
    if-ne p2, v0, :cond_40

    #@25
    .line 456
    add-int/lit8 v2, v2, 0x1

    #@27
    .line 475
    .end local v0           #currHashState:I
    :cond_27
    :goto_27
    if-eqz v4, :cond_3f

    #@29
    .line 476
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfileConnectionState:Ljava/util/HashMap;

    #@2b
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2e
    move-result-object v6

    #@2f
    new-instance v7, Landroid/util/Pair;

    #@31
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@34
    move-result-object v8

    #@35
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@38
    move-result-object v9

    #@39
    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    #@3c
    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@3f
    .line 479
    :cond_3f
    return-void

    #@40
    .line 457
    .restart local v0       #currHashState:I
    :cond_40
    if-eq p2, v8, :cond_46

    #@42
    if-ne p2, v7, :cond_48

    #@44
    if-eq v0, v8, :cond_48

    #@46
    .line 460
    :cond_46
    const/4 v2, 0x1

    #@47
    goto :goto_27

    #@48
    .line 461
    :cond_48
    if-ne v2, v7, :cond_4e

    #@4a
    if-ne p3, v0, :cond_4e

    #@4c
    .line 462
    const/4 v4, 0x1

    #@4d
    goto :goto_27

    #@4e
    .line 463
    :cond_4e
    if-le v2, v7, :cond_5a

    #@50
    if-ne p3, v0, :cond_5a

    #@52
    .line 464
    add-int/lit8 v2, v2, -0x1

    #@54
    .line 466
    if-eq v0, v8, :cond_58

    #@56
    if-ne v0, v7, :cond_27

    #@58
    .line 468
    :cond_58
    move v1, v0

    #@59
    goto :goto_27

    #@5a
    .line 471
    :cond_5a
    const/4 v4, 0x0

    #@5b
    goto :goto_27
.end method

.method private validateProfileConnectionState(I)Z
    .registers 4
    .parameter "state"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 357
    if-eqz p1, :cond_b

    #@3
    if-eq p1, v0, :cond_b

    #@5
    const/4 v1, 0x2

    #@6
    if-eq p1, v1, :cond_b

    #@8
    const/4 v1, 0x3

    #@9
    if-ne p1, v1, :cond_c

    #@b
    :cond_b
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method


# virtual methods
.method public Clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 98
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    #@2
    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    #@5
    throw v0
.end method

.method adapterPropertyChangedCallback([I[[B)V
    .registers 15
    .parameter "types"
    .parameter "values"

    #@0
    .prologue
    .line 485
    const/4 v1, 0x0

    #@1
    .local v1, i:I
    :goto_1
    array-length v8, p1

    #@2
    if-ge v1, v8, :cond_162

    #@4
    .line 486
    aget-object v7, p2, v1

    #@6
    .line 487
    .local v7, val:[B
    aget v6, p1, v1

    #@8
    .line 488
    .local v6, type:I
    new-instance v8, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v9, "adapterPropertyChangedCallback with type:"

    #@f
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v8

    #@13
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@16
    move-result-object v8

    #@17
    const-string v9, " len:"

    #@19
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v8

    #@1d
    array-length v9, v7

    #@1e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@21
    move-result-object v8

    #@22
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v8

    #@26
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/AdapterProperties;->infoLog(Ljava/lang/String;)V

    #@29
    .line 489
    iget-object v9, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2b
    monitor-enter v9

    #@2c
    .line 490
    packed-switch v6, :pswitch_data_164

    #@2f
    .line 538
    :pswitch_2f
    :try_start_2f
    new-instance v8, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v10, "Property change not handled in Java land:"

    #@36
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v8

    #@3a
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v8

    #@3e
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@41
    move-result-object v8

    #@42
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/AdapterProperties;->errorLog(Ljava/lang/String;)V

    #@45
    .line 540
    :cond_45
    :goto_45
    monitor-exit v9

    #@46
    .line 485
    add-int/lit8 v1, v1, 0x1

    #@48
    goto :goto_1

    #@49
    .line 492
    :pswitch_49
    new-instance v8, Ljava/lang/String;

    #@4b
    invoke-direct {v8, v7}, Ljava/lang/String;-><init>([B)V

    #@4e
    iput-object v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mName:Ljava/lang/String;

    #@50
    .line 493
    new-instance v2, Landroid/content/Intent;

    #@52
    const-string v8, "android.bluetooth.adapter.action.LOCAL_NAME_CHANGED"

    #@54
    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@57
    .line 494
    .local v2, intent:Landroid/content/Intent;
    const-string v8, "android.bluetooth.adapter.extra.LOCAL_NAME"

    #@59
    iget-object v10, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mName:Ljava/lang/String;

    #@5b
    invoke-virtual {v2, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@5e
    .line 495
    const/high16 v8, 0x800

    #@60
    invoke-virtual {v2, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@63
    .line 496
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@65
    iget-object v10, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@67
    const-string v10, "android.permission.BLUETOOTH"

    #@69
    invoke-virtual {v8, v2, v10}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@6c
    .line 497
    new-instance v8, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v10, "Name is: "

    #@73
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v8

    #@77
    iget-object v10, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mName:Ljava/lang/String;

    #@79
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v8

    #@7d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v8

    #@81
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V

    #@84
    goto :goto_45

    #@85
    .line 540
    .end local v2           #intent:Landroid/content/Intent;
    :catchall_85
    move-exception v8

    #@86
    monitor-exit v9
    :try_end_87
    .catchall {:try_start_2f .. :try_end_87} :catchall_85

    #@87
    throw v8

    #@88
    .line 500
    :pswitch_88
    :try_start_88
    iput-object v7, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mAddress:[B

    #@8a
    .line 501
    new-instance v8, Ljava/lang/StringBuilder;

    #@8c
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@8f
    const-string v10, "Address is:"

    #@91
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@94
    move-result-object v8

    #@95
    iget-object v10, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mAddress:[B

    #@97
    invoke-static {v10}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@9a
    move-result-object v10

    #@9b
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v8

    #@9f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v8

    #@a3
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V

    #@a6
    goto :goto_45

    #@a7
    .line 504
    :pswitch_a7
    const/4 v8, 0x0

    #@a8
    invoke-static {v7, v8}, Lcom/android/bluetooth/Utils;->byteArrayToInt([BI)I

    #@ab
    move-result v8

    #@ac
    iput v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBluetoothClass:I

    #@ae
    .line 505
    new-instance v8, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v10, "BT Class:"

    #@b5
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v8

    #@b9
    iget v10, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBluetoothClass:I

    #@bb
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@be
    move-result-object v8

    #@bf
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@c2
    move-result-object v8

    #@c3
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V

    #@c6
    goto/16 :goto_45

    #@c8
    .line 508
    :pswitch_c8
    const/4 v8, 0x0

    #@c9
    invoke-static {v7, v8}, Lcom/android/bluetooth/Utils;->byteArrayToInt([BI)I

    #@cc
    move-result v4

    #@cd
    .line 509
    .local v4, mode:I
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@cf
    invoke-static {v4}, Lcom/android/bluetooth/btservice/AdapterService;->convertScanModeFromHal(I)I

    #@d2
    move-result v8

    #@d3
    iput v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mScanMode:I

    #@d5
    .line 510
    new-instance v2, Landroid/content/Intent;

    #@d7
    const-string v8, "android.bluetooth.adapter.action.SCAN_MODE_CHANGED"

    #@d9
    invoke-direct {v2, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@dc
    .line 511
    .restart local v2       #intent:Landroid/content/Intent;
    const-string v8, "android.bluetooth.adapter.extra.SCAN_MODE"

    #@de
    iget v10, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mScanMode:I

    #@e0
    invoke-virtual {v2, v8, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@e3
    .line 512
    const/high16 v8, 0x800

    #@e5
    invoke-virtual {v2, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@e8
    .line 513
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@ea
    iget-object v10, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@ec
    const-string v10, "android.permission.BLUETOOTH"

    #@ee
    invoke-virtual {v8, v2, v10}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@f1
    .line 514
    new-instance v8, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v10, "Scan Mode:"

    #@f8
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v8

    #@fc
    iget v10, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mScanMode:I

    #@fe
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@101
    move-result-object v8

    #@102
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v8

    #@106
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V

    #@109
    .line 515
    iget-boolean v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBluetoothDisabling:Z

    #@10b
    if-eqz v8, :cond_45

    #@10d
    .line 516
    const/4 v8, 0x0

    #@10e
    iput-boolean v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBluetoothDisabling:Z

    #@110
    .line 517
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@112
    invoke-virtual {v8}, Lcom/android/bluetooth/btservice/AdapterService;->startBluetoothDisable()V

    #@115
    goto/16 :goto_45

    #@117
    .line 521
    .end local v2           #intent:Landroid/content/Intent;
    .end local v4           #mode:I
    :pswitch_117
    invoke-static {v7}, Lcom/android/bluetooth/Utils;->byteArrayToUuid([B)[Landroid/os/ParcelUuid;

    #@11a
    move-result-object v8

    #@11b
    iput-object v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mUuids:[Landroid/os/ParcelUuid;

    #@11d
    goto/16 :goto_45

    #@11f
    .line 524
    :pswitch_11f
    array-length v8, v7

    #@120
    div-int/lit8 v5, v8, 0x6

    #@122
    .line 525
    .local v5, number:I
    const/4 v8, 0x6

    #@123
    new-array v0, v8, [B

    #@125
    .line 526
    .local v0, addrByte:[B
    const/4 v3, 0x0

    #@126
    .local v3, j:I
    :goto_126
    if-ge v3, v5, :cond_45

    #@128
    .line 527
    mul-int/lit8 v8, v3, 0x6

    #@12a
    const/4 v10, 0x0

    #@12b
    const/4 v11, 0x6

    #@12c
    invoke-static {v7, v8, v0, v10, v11}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@12f
    .line 528
    iget-object v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@131
    invoke-static {v0}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@134
    move-result-object v10

    #@135
    invoke-virtual {v8, v10}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@138
    move-result-object v8

    #@139
    const/16 v10, 0xc

    #@13b
    invoke-virtual {p0, v8, v10}, Lcom/android/bluetooth/btservice/AdapterProperties;->onBondStateChanged(Landroid/bluetooth/BluetoothDevice;I)V

    #@13e
    .line 526
    add-int/lit8 v3, v3, 0x1

    #@140
    goto :goto_126

    #@141
    .line 534
    .end local v0           #addrByte:[B
    .end local v3           #j:I
    .end local v5           #number:I
    :pswitch_141
    const/4 v8, 0x0

    #@142
    invoke-static {v7, v8}, Lcom/android/bluetooth/Utils;->byteArrayToInt([BI)I

    #@145
    move-result v8

    #@146
    iput v8, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mDiscoverableTimeout:I

    #@148
    .line 535
    new-instance v8, Ljava/lang/StringBuilder;

    #@14a
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@14d
    const-string v10, "Discoverable Timeout:"

    #@14f
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@152
    move-result-object v8

    #@153
    iget v10, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mDiscoverableTimeout:I

    #@155
    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@158
    move-result-object v8

    #@159
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15c
    move-result-object v8

    #@15d
    invoke-direct {p0, v8}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V
    :try_end_160
    .catchall {:try_start_88 .. :try_end_160} :catchall_85

    #@160
    goto/16 :goto_45

    #@162
    .line 542
    .end local v6           #type:I
    .end local v7           #val:[B
    :cond_162
    return-void

    #@163
    .line 490
    nop

    #@164
    :pswitch_data_164
    .packed-switch 0x1
        :pswitch_49
        :pswitch_88
        :pswitch_117
        :pswitch_a7
        :pswitch_2f
        :pswitch_2f
        :pswitch_c8
        :pswitch_11f
        :pswitch_141
    .end packed-switch
.end method

.method public cleanup()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 86
    iput-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@3
    .line 87
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfileConnectionState:Ljava/util/HashMap;

    #@5
    if-eqz v0, :cond_e

    #@7
    .line 88
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfileConnectionState:Ljava/util/HashMap;

    #@9
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@c
    .line 89
    iput-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfileConnectionState:Ljava/util/HashMap;

    #@e
    .line 91
    :cond_e
    iput-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@10
    .line 92
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBondedDevices:Ljava/util/ArrayList;

    #@12
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    #@15
    move-result v0

    #@16
    if-nez v0, :cond_1d

    #@18
    .line 93
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBondedDevices:Ljava/util/ArrayList;

    #@1a
    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    #@1d
    .line 95
    :cond_1d
    return-void
.end method

.method discoveryStateChangeCallback(I)V
    .registers 6
    .parameter "state"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 582
    new-instance v1, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    const-string v2, "Callback:discoveryStateChangeCallback with state:"

    #@8
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b
    move-result-object v1

    #@c
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13
    move-result-object v1

    #@14
    invoke-direct {p0, v1}, Lcom/android/bluetooth/btservice/AdapterProperties;->infoLog(Ljava/lang/String;)V

    #@17
    .line 583
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@19
    monitor-enter v2

    #@1a
    .line 585
    if-nez p1, :cond_31

    #@1c
    .line 586
    const/4 v1, 0x0

    #@1d
    :try_start_1d
    iput-boolean v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mDiscovering:Z

    #@1f
    .line 587
    new-instance v0, Landroid/content/Intent;

    #@21
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_FINISHED"

    #@23
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@26
    .line 588
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@28
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@2a
    const-string v3, "android.permission.BLUETOOTH"

    #@2c
    invoke-virtual {v1, v0, v3}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@2f
    .line 594
    .end local v0           #intent:Landroid/content/Intent;
    :cond_2f
    :goto_2f
    monitor-exit v2

    #@30
    .line 595
    return-void

    #@31
    .line 589
    :cond_31
    if-ne p1, v3, :cond_2f

    #@33
    .line 590
    const/4 v1, 0x1

    #@34
    iput-boolean v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mDiscovering:Z

    #@36
    .line 591
    new-instance v0, Landroid/content/Intent;

    #@38
    const-string v1, "android.bluetooth.adapter.action.DISCOVERY_STARTED"

    #@3a
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3d
    .line 592
    .restart local v0       #intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@3f
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@41
    const-string v3, "android.permission.BLUETOOTH"

    #@43
    invoke-virtual {v1, v0, v3}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@46
    goto :goto_2f

    #@47
    .line 594
    .end local v0           #intent:Landroid/content/Intent;
    :catchall_47
    move-exception v1

    #@48
    monitor-exit v2
    :try_end_49
    .catchall {:try_start_1d .. :try_end_49} :catchall_47

    #@49
    throw v1
.end method

.method getAddress()[B
    .registers 3

    #@0
    .prologue
    .line 194
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 195
    :try_start_3
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mAddress:[B

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 196
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method getBluetoothClass()I
    .registers 3

    #@0
    .prologue
    .line 143
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 144
    :try_start_3
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBluetoothClass:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 145
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method getBondedDevices()[Landroid/bluetooth/BluetoothDevice;
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 241
    new-array v1, v3, [Landroid/bluetooth/BluetoothDevice;

    #@3
    .line 242
    .local v1, bondedDeviceList:[Landroid/bluetooth/BluetoothDevice;
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@5
    monitor-enter v4

    #@6
    .line 243
    :try_start_6
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBondedDevices:Ljava/util/ArrayList;

    #@8
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    #@b
    move-result v3

    #@c
    if-eqz v3, :cond_13

    #@e
    .line 244
    const/4 v3, 0x0

    #@f
    new-array v3, v3, [Landroid/bluetooth/BluetoothDevice;

    #@11
    monitor-exit v4
    :try_end_12
    .catchall {:try_start_6 .. :try_end_12} :catchall_42

    #@12
    .line 252
    :goto_12
    return-object v3

    #@13
    .line 247
    :cond_13
    :try_start_13
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBondedDevices:Ljava/util/ArrayList;

    #@15
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@18
    move-result-object v3

    #@19
    move-object v0, v3

    #@1a
    check-cast v0, [Landroid/bluetooth/BluetoothDevice;

    #@1c
    move-object v1, v0

    #@1d
    .line 248
    new-instance v3, Ljava/lang/StringBuilder;

    #@1f
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@22
    const-string v5, "getBondedDevices: length="

    #@24
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v3

    #@28
    array-length v5, v1

    #@29
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v3

    #@31
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V
    :try_end_34
    .catchall {:try_start_13 .. :try_end_34} :catchall_42
    .catch Ljava/lang/ArrayStoreException; {:try_start_13 .. :try_end_34} :catch_37

    #@34
    .line 249
    :try_start_34
    monitor-exit v4

    #@35
    move-object v3, v1

    #@36
    goto :goto_12

    #@37
    .line 250
    :catch_37
    move-exception v2

    #@38
    .line 251
    .local v2, ee:Ljava/lang/ArrayStoreException;
    const-string v3, "Error retrieving bonded device array"

    #@3a
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/AdapterProperties;->errorLog(Ljava/lang/String;)V

    #@3d
    .line 252
    const/4 v3, 0x0

    #@3e
    new-array v3, v3, [Landroid/bluetooth/BluetoothDevice;

    #@40
    monitor-exit v4

    #@41
    goto :goto_12

    #@42
    .line 254
    .end local v2           #ee:Ljava/lang/ArrayStoreException;
    :catchall_42
    move-exception v3

    #@43
    monitor-exit v4
    :try_end_44
    .catchall {:try_start_34 .. :try_end_44} :catchall_42

    #@44
    throw v3
.end method

.method getConnectionState()I
    .registers 3

    #@0
    .prologue
    .line 212
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 213
    :try_start_3
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mConnectionState:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 214
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method getDiscoverableTimeout()I
    .registers 3

    #@0
    .prologue
    .line 292
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 293
    :try_start_3
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mDiscoverableTimeout:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 294
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method getName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 105
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 106
    :try_start_3
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mName:Ljava/lang/String;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 107
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method getProfileConnectionState(I)I
    .registers 6
    .parameter "profile"

    #@0
    .prologue
    .line 306
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v2

    #@3
    .line 307
    :try_start_3
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfileConnectionState:Ljava/util/HashMap;

    #@5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@8
    move-result-object v3

    #@9
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@c
    move-result-object v0

    #@d
    check-cast v0, Landroid/util/Pair;

    #@f
    .line 308
    .local v0, p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_1b

    #@11
    .line 309
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    #@13
    check-cast v1, Ljava/lang/Integer;

    #@15
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@18
    move-result v1

    #@19
    monitor-exit v2

    #@1a
    .line 311
    :goto_1a
    return v1

    #@1b
    :cond_1b
    const/4 v1, 0x0

    #@1c
    monitor-exit v2

    #@1d
    goto :goto_1a

    #@1e
    .line 312
    .end local v0           #p:Landroid/util/Pair;,"Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :catchall_1e
    move-exception v1

    #@1f
    monitor-exit v2
    :try_end_20
    .catchall {:try_start_3 .. :try_end_20} :catchall_1e

    #@20
    throw v1
.end method

.method getScanMode()I
    .registers 3

    #@0
    .prologue
    .line 152
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 153
    :try_start_3
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mScanMode:I

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 154
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method getState()I
    .registers 4

    #@0
    .prologue
    .line 231
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 232
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "State = "

    #@a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mState:I

    #@10
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v0

    #@14
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v0

    #@18
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V

    #@1b
    .line 233
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mState:I

    #@1d
    monitor-exit v1

    #@1e
    return v0

    #@1f
    .line 234
    :catchall_1f
    move-exception v0

    #@20
    monitor-exit v1
    :try_end_21
    .catchall {:try_start_3 .. :try_end_21} :catchall_1f

    #@21
    throw v0
.end method

.method getUuids()[Landroid/os/ParcelUuid;
    .registers 3

    #@0
    .prologue
    .line 173
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 174
    :try_start_3
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mUuids:[Landroid/os/ParcelUuid;

    #@5
    monitor-exit v1

    #@6
    return-object v0

    #@7
    .line 175
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method public init(Lcom/android/bluetooth/btservice/RemoteDevices;)V
    .registers 3
    .parameter "remoteDevices"

    #@0
    .prologue
    .line 77
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfileConnectionState:Ljava/util/HashMap;

    #@2
    if-nez v0, :cond_e

    #@4
    .line 78
    new-instance v0, Ljava/util/HashMap;

    #@6
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@9
    iput-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfileConnectionState:Ljava/util/HashMap;

    #@b
    .line 82
    :goto_b
    iput-object p1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@d
    .line 83
    return-void

    #@e
    .line 80
    :cond_e
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mProfileConnectionState:Ljava/util/HashMap;

    #@10
    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    #@13
    goto :goto_b
.end method

.method isDiscovering()Z
    .registers 3

    #@0
    .prologue
    .line 316
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 317
    :try_start_3
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mDiscovering:Z

    #@5
    monitor-exit v1

    #@6
    return v0

    #@7
    .line 318
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method onBluetoothDisable()V
    .registers 3

    #@0
    .prologue
    .line 575
    const-string v0, "onBluetoothDisable()"

    #@2
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V

    #@5
    .line 576
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBluetoothDisabling:Z

    #@8
    .line 577
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterProperties;->getState()I

    #@b
    move-result v0

    #@c
    const/16 v1, 0xd

    #@e
    if-ne v0, v1, :cond_14

    #@10
    .line 578
    const/4 v0, 0x0

    #@11
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->setScanMode(I)Z

    #@14
    .line 580
    :cond_14
    return-void
.end method

.method onBluetoothReady()V
    .registers 4

    #@0
    .prologue
    .line 545
    const-string v0, "BluetoothAdapterProperties"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "ScanMode =  "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mScanMode:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 546
    const-string v0, "BluetoothAdapterProperties"

    #@1c
    new-instance v1, Ljava/lang/StringBuilder;

    #@1e
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@21
    const-string v2, "State =  "

    #@23
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v1

    #@27
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterProperties;->getState()I

    #@2a
    move-result v2

    #@2b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    .line 550
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@38
    monitor-enter v1

    #@39
    .line 551
    :try_start_39
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterProperties;->getState()I

    #@3c
    move-result v0

    #@3d
    const/16 v2, 0xb

    #@3f
    if-ne v0, v2, :cond_54

    #@41
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mScanMode:I

    #@43
    const/16 v2, 0x14

    #@45
    if-ne v0, v2, :cond_54

    #@47
    .line 556
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mDiscoverableTimeout:I

    #@49
    if-eqz v0, :cond_56

    #@4b
    .line 557
    const/4 v0, 0x1

    #@4c
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->setScanMode(I)Z

    #@4f
    .line 562
    :goto_4f
    iget v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mDiscoverableTimeout:I

    #@51
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->setDiscoverableTimeout(I)Z

    #@54
    .line 564
    :cond_54
    monitor-exit v1

    #@55
    .line 565
    return-void

    #@56
    .line 559
    :cond_56
    const/4 v0, 0x2

    #@57
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->setScanMode(I)Z

    #@5a
    goto :goto_4f

    #@5b
    .line 564
    :catchall_5b
    move-exception v0

    #@5c
    monitor-exit v1
    :try_end_5d
    .catchall {:try_start_39 .. :try_end_5d} :catchall_5b

    #@5d
    throw v0
.end method

.method onBondStateChanged(Landroid/bluetooth/BluetoothDevice;I)V
    .registers 8
    .parameter "device"
    .parameter "state"

    #@0
    .prologue
    .line 260
    if-nez p1, :cond_3

    #@2
    .line 289
    :cond_2
    :goto_2
    return-void

    #@3
    .line 264
    :cond_3
    :try_start_3
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getByteAddress(Landroid/bluetooth/BluetoothDevice;)[B

    #@6
    move-result-object v0

    #@7
    .line 265
    .local v0, addrByte:[B
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    invoke-virtual {v3, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->getDeviceProperties(Landroid/bluetooth/BluetoothDevice;)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@c
    move-result-object v2

    #@d
    .line 266
    .local v2, prop:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    if-nez v2, :cond_15

    #@f
    .line 267
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@11
    invoke-virtual {v3, v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->addDeviceProperties([B)Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;

    #@14
    move-result-object v2

    #@15
    .line 269
    :cond_15
    invoke-virtual {v2, p2}, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->setBondState(I)V

    #@18
    .line 271
    const/16 v3, 0xc

    #@1a
    if-ne p2, v3, :cond_49

    #@1c
    .line 273
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBondedDevices:Ljava/util/ArrayList;

    #@1e
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    #@21
    move-result v3

    #@22
    if-nez v3, :cond_2

    #@24
    .line 274
    new-instance v3, Ljava/lang/StringBuilder;

    #@26
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@29
    const-string v4, "Adding bonded device:"

    #@2b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object v3

    #@37
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V

    #@3a
    .line 275
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBondedDevices:Ljava/util/ArrayList;

    #@3c
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3f
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3f} :catch_40

    #@3f
    goto :goto_2

    #@40
    .line 286
    .end local v0           #addrByte:[B
    .end local v2           #prop:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    :catch_40
    move-exception v1

    #@41
    .line 287
    .local v1, ee:Ljava/lang/Exception;
    const-string v3, "BluetoothAdapterProperties"

    #@43
    const-string v4, "Exception in onBondStateChanged : "

    #@45
    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@48
    goto :goto_2

    #@49
    .line 277
    .end local v1           #ee:Ljava/lang/Exception;
    .restart local v0       #addrByte:[B
    .restart local v2       #prop:Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
    :cond_49
    const/16 v3, 0xa

    #@4b
    if-ne p2, v3, :cond_2

    #@4d
    .line 279
    :try_start_4d
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mBondedDevices:Ljava/util/ArrayList;

    #@4f
    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    #@52
    move-result v3

    #@53
    if-eqz v3, :cond_6c

    #@55
    .line 280
    new-instance v3, Ljava/lang/StringBuilder;

    #@57
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@5a
    const-string v4, "Removing bonded device:"

    #@5c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v3

    #@60
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@63
    move-result-object v3

    #@64
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@67
    move-result-object v3

    #@68
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V

    #@6b
    goto :goto_2

    #@6c
    .line 282
    :cond_6c
    new-instance v3, Ljava/lang/StringBuilder;

    #@6e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@71
    const-string v4, "Failed to remove device: "

    #@73
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v3

    #@77
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v3

    #@7b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7e
    move-result-object v3

    #@7f
    invoke-direct {p0, v3}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V
    :try_end_82
    .catch Ljava/lang/Exception; {:try_start_4d .. :try_end_82} :catch_40

    #@82
    goto :goto_2
.end method

.method sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V
    .registers 10
    .parameter "device"
    .parameter "profile"
    .parameter "state"
    .parameter "prevState"

    #@0
    .prologue
    .line 322
    const-string v1, "BluetoothAdapterProperties"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "[BTUI] sendConnectionStateChange :  "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, "("

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {p1}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, "):  prevState( "

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    const-string v3, " )"

    #@2b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v2

    #@2f
    const-string v3, " -> state( "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    const-string v3, " )  [ 0:DISCONNECTED, 1:CONNECTING, 2:CONNECTED, 3:DISCONNECTING ]"

    #@3b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 323
    const-string v1, "BluetoothAdapterProperties"

    #@48
    new-instance v2, Ljava/lang/StringBuilder;

    #@4a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@4d
    const-string v3, "[BTUI] profile( "

    #@4f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v2

    #@53
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v2

    #@57
    const-string v3, " )  [ 1:HEADSET, 2:A2DP, 3:HDP, 4:HID, 5:PAN, 6:PBAP, 7:FTP, 8:MAP ]"

    #@59
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v2

    #@5d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@60
    move-result-object v2

    #@61
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 324
    invoke-direct {p0, p3}, Lcom/android/bluetooth/btservice/AdapterProperties;->validateProfileConnectionState(I)Z

    #@67
    move-result v1

    #@68
    if-eqz v1, :cond_70

    #@6a
    invoke-direct {p0, p4}, Lcom/android/bluetooth/btservice/AdapterProperties;->validateProfileConnectionState(I)Z

    #@6d
    move-result v1

    #@6e
    if-nez v1, :cond_91

    #@70
    .line 330
    :cond_70
    new-instance v1, Ljava/lang/StringBuilder;

    #@72
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@75
    const-string v2, "Error in sendConnectionStateChange: prevState "

    #@77
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7a
    move-result-object v1

    #@7b
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@7e
    move-result-object v1

    #@7f
    const-string v2, " state "

    #@81
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@84
    move-result-object v1

    #@85
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@88
    move-result-object v1

    #@89
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8c
    move-result-object v1

    #@8d
    invoke-direct {p0, v1}, Lcom/android/bluetooth/btservice/AdapterProperties;->errorLog(Ljava/lang/String;)V

    #@90
    .line 354
    :goto_90
    return-void

    #@91
    .line 335
    :cond_91
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@93
    monitor-enter v2

    #@94
    .line 336
    :try_start_94
    invoke-direct {p0, p2, p3, p4}, Lcom/android/bluetooth/btservice/AdapterProperties;->updateProfileConnectionState(III)V

    #@97
    .line 338
    invoke-direct {p0, p3, p4}, Lcom/android/bluetooth/btservice/AdapterProperties;->updateCountersAndCheckForConnectionStateChange(II)Z

    #@9a
    move-result v1

    #@9b
    if-eqz v1, :cond_fa

    #@9d
    .line 339
    invoke-virtual {p0, p3}, Lcom/android/bluetooth/btservice/AdapterProperties;->setConnectionState(I)V

    #@a0
    .line 341
    new-instance v0, Landroid/content/Intent;

    #@a2
    const-string v1, "android.bluetooth.adapter.action.CONNECTION_STATE_CHANGED"

    #@a4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@a7
    .line 342
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "android.bluetooth.device.extra.DEVICE"

    #@a9
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    #@ac
    .line 343
    const-string v1, "android.bluetooth.adapter.extra.CONNECTION_STATE"

    #@ae
    invoke-direct {p0, p3}, Lcom/android/bluetooth/btservice/AdapterProperties;->convertToAdapterState(I)I

    #@b1
    move-result v3

    #@b2
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@b5
    .line 345
    const-string v1, "android.bluetooth.adapter.extra.PREVIOUS_CONNECTION_STATE"

    #@b7
    invoke-direct {p0, p4}, Lcom/android/bluetooth/btservice/AdapterProperties;->convertToAdapterState(I)I

    #@ba
    move-result v3

    #@bb
    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@be
    .line 347
    const/high16 v1, 0x800

    #@c0
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@c3
    .line 348
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@c5
    sget-object v3, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@c7
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@c9
    const-string v4, "android.permission.BLUETOOTH"

    #@cb
    invoke-virtual {v1, v0, v3, v4}, Lcom/android/bluetooth/btservice/AdapterService;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;)V

    #@ce
    .line 350
    const-string v1, "BluetoothAdapterProperties"

    #@d0
    new-instance v3, Ljava/lang/StringBuilder;

    #@d2
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d5
    const-string v4, "CONNECTION_STATE_CHANGE: "

    #@d7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@da
    move-result-object v3

    #@db
    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@de
    move-result-object v3

    #@df
    const-string v4, ": "

    #@e1
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e4
    move-result-object v3

    #@e5
    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e8
    move-result-object v3

    #@e9
    const-string v4, " -> "

    #@eb
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ee
    move-result-object v3

    #@ef
    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@f2
    move-result-object v3

    #@f3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@f6
    move-result-object v3

    #@f7
    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@fa
    .line 353
    .end local v0           #intent:Landroid/content/Intent;
    :cond_fa
    monitor-exit v2

    #@fb
    goto :goto_90

    #@fc
    :catchall_fc
    move-exception v1

    #@fd
    monitor-exit v2
    :try_end_fe
    .catchall {:try_start_94 .. :try_end_fe} :catchall_fc

    #@fe
    throw v1
.end method

.method setConnectionState(I)V
    .registers 4
    .parameter "mConnectionState"

    #@0
    .prologue
    .line 203
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 204
    :try_start_3
    iput p1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mConnectionState:I

    #@5
    .line 205
    monitor-exit v1

    #@6
    .line 206
    return-void

    #@7
    .line 205
    :catchall_7
    move-exception v0

    #@8
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_3 .. :try_end_9} :catchall_7

    #@9
    throw v0
.end method

.method setDiscoverableTimeout(I)Z
    .registers 6
    .parameter "timeout"

    #@0
    .prologue
    .line 298
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 299
    :try_start_3
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@5
    const/16 v2, 0x9

    #@7
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->intToByteArray(I)[B

    #@a
    move-result-object v3

    #@b
    invoke-virtual {v0, v2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->setAdapterPropertyNative(I[B)Z

    #@e
    move-result v0

    #@f
    monitor-exit v1

    #@10
    return v0

    #@11
    .line 302
    :catchall_11
    move-exception v0

    #@12
    monitor-exit v1
    :try_end_13
    .catchall {:try_start_3 .. :try_end_13} :catchall_11

    #@13
    throw v0
.end method

.method setName(Ljava/lang/String;)Z
    .registers 7
    .parameter "name"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 115
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@3
    monitor-enter v1

    #@4
    .line 117
    :try_start_4
    const-string v2, "LGBT_COMMON_SCENARIO_DEVICE_NAME_SYNC"

    #@6
    invoke-static {v2}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_12

    #@c
    .line 118
    invoke-virtual {p0, p1}, Lcom/android/bluetooth/btservice/AdapterProperties;->setNameWithoutDBUpdate(Ljava/lang/String;)Z

    #@f
    move-result v0

    #@10
    monitor-exit v1

    #@11
    .line 126
    :goto_11
    return v0

    #@12
    .line 120
    :cond_12
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@14
    const/4 v3, 0x1

    #@15
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@18
    move-result-object v4

    #@19
    invoke-virtual {v2, v3, v4}, Lcom/android/bluetooth/btservice/AdapterService;->setAdapterPropertyNative(I[B)Z

    #@1c
    move-result v2

    #@1d
    if-eqz v2, :cond_41

    #@1f
    .line 121
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@21
    invoke-virtual {v2}, Lcom/android/bluetooth/btservice/AdapterService;->getContentResolver()Landroid/content/ContentResolver;

    #@24
    move-result-object v2

    #@25
    const-string v3, "lg_device_name"

    #@27
    invoke-static {v2, v3}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e
    move-result v2

    #@2f
    if-nez v2, :cond_3c

    #@31
    .line 122
    iget-object v2, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@33
    invoke-virtual {v2}, Lcom/android/bluetooth/btservice/AdapterService;->getContentResolver()Landroid/content/ContentResolver;

    #@36
    move-result-object v2

    #@37
    const-string v3, "lg_device_name"

    #@39
    invoke-static {v2, v3, p1}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@3c
    .line 124
    :cond_3c
    monitor-exit v1

    #@3d
    goto :goto_11

    #@3e
    .line 128
    :catchall_3e
    move-exception v0

    #@3f
    monitor-exit v1
    :try_end_40
    .catchall {:try_start_4 .. :try_end_40} :catchall_3e

    #@40
    throw v0

    #@41
    .line 126
    :cond_41
    const/4 v0, 0x0

    #@42
    :try_start_42
    monitor-exit v1
    :try_end_43
    .catchall {:try_start_42 .. :try_end_43} :catchall_3e

    #@43
    goto :goto_11
.end method

.method setNameWithoutDBUpdate(Ljava/lang/String;)Z
    .registers 6
    .parameter "name"

    #@0
    .prologue
    .line 132
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 134
    :try_start_3
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@5
    const/4 v2, 0x1

    #@6
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v0, v2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->setAdapterPropertyNative(I[B)Z

    #@d
    move-result v0

    #@e
    monitor-exit v1

    #@f
    return v0

    #@10
    .line 136
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method setScanMode(I)Z
    .registers 6
    .parameter "scanMode"

    #@0
    .prologue
    .line 163
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 164
    :try_start_3
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@5
    const/4 v2, 0x7

    #@6
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->intToByteArray(I)[B

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v0, v2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->setAdapterPropertyNative(I[B)Z

    #@d
    move-result v0

    #@e
    monitor-exit v1

    #@f
    return v0

    #@10
    .line 166
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method

.method setState(I)V
    .registers 5
    .parameter "mState"

    #@0
    .prologue
    .line 221
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 222
    :try_start_3
    new-instance v0, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "Setting state to "

    #@a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v0

    #@e
    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11
    move-result-object v0

    #@12
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    invoke-direct {p0, v0}, Lcom/android/bluetooth/btservice/AdapterProperties;->debugLog(Ljava/lang/String;)V

    #@19
    .line 223
    iput p1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mState:I

    #@1b
    .line 224
    monitor-exit v1

    #@1c
    .line 225
    return-void

    #@1d
    .line 224
    :catchall_1d
    move-exception v0

    #@1e
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_3 .. :try_end_1f} :catchall_1d

    #@1f
    throw v0
.end method

.method setUuids([Landroid/os/ParcelUuid;)Z
    .registers 6
    .parameter "uuids"

    #@0
    .prologue
    .line 184
    iget-object v1, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mObject:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 185
    :try_start_3
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterProperties;->mService:Lcom/android/bluetooth/btservice/AdapterService;

    #@5
    const/4 v2, 0x3

    #@6
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->uuidsToByteArray([Landroid/os/ParcelUuid;)[B

    #@9
    move-result-object v3

    #@a
    invoke-virtual {v0, v2, v3}, Lcom/android/bluetooth/btservice/AdapterService;->setAdapterPropertyNative(I[B)Z

    #@d
    move-result v0

    #@e
    monitor-exit v1

    #@f
    return v0

    #@10
    .line 187
    :catchall_10
    move-exception v0

    #@11
    monitor-exit v1
    :try_end_12
    .catchall {:try_start_3 .. :try_end_12} :catchall_10

    #@12
    throw v0
.end method
