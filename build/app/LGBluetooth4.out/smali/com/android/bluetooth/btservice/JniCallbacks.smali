.class final Lcom/android/bluetooth/btservice/JniCallbacks;
.super Ljava/lang/Object;
.source "JniCallbacks.java"


# instance fields
.field private mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

.field private mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

.field private mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

.field private mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/bluetooth/btservice/AdapterProperties;)V
    .registers 3
    .parameter "adapterStateMachine"
    .parameter "adapterProperties"

    #@0
    .prologue
    .line 27
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 28
    iput-object p1, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@5
    .line 29
    iput-object p2, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@7
    .line 30
    return-void
.end method


# virtual methods
.method public Clone()Ljava/lang/Object;
    .registers 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    #@0
    .prologue
    .line 45
    new-instance v0, Ljava/lang/CloneNotSupportedException;

    #@2
    invoke-direct {v0}, Ljava/lang/CloneNotSupportedException;-><init>()V

    #@5
    throw v0
.end method

.method aclStateChangeCallback(I[BI)V
    .registers 5
    .parameter "status"
    .parameter "address"
    .parameter "newState"

    #@0
    .prologue
    .line 70
    iget-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/bluetooth/btservice/RemoteDevices;->aclStateChangeCallback(I[BI)V

    #@5
    .line 71
    return-void
.end method

.method adapterPropertyChangedCallback([I[[B)V
    .registers 4
    .parameter "types"
    .parameter "val"

    #@0
    .prologue
    .line 82
    iget-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/btservice/AdapterProperties;->adapterPropertyChangedCallback([I[[B)V

    #@5
    .line 83
    return-void
.end method

.method authorizeRequestCallback([BI)V
    .registers 4
    .parameter "address"
    .parameter "serviceId"

    #@0
    .prologue
    .line 87
    iget-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/btservice/RemoteDevices;->authorizeRequestCallback([BI)V

    #@5
    .line 88
    return-void
.end method

.method bondStateChangeCallback(I[BI)V
    .registers 5
    .parameter "status"
    .parameter "address"
    .parameter "newState"

    #@0
    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/bluetooth/btservice/BondStateMachine;->bondStateChangeCallback(I[BI)V

    #@5
    .line 67
    return-void
.end method

.method cleanup()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 38
    iput-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@3
    .line 39
    iput-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@5
    .line 40
    iput-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@7
    .line 41
    iput-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@9
    .line 42
    return-void
.end method

.method deviceFoundCallback([B)V
    .registers 3
    .parameter "address"

    #@0
    .prologue
    .line 58
    iget-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/RemoteDevices;->deviceFoundCallback([B)V

    #@5
    .line 59
    return-void
.end method

.method devicePropertyChangedCallback([B[I[[B)V
    .registers 5
    .parameter "address"
    .parameter "types"
    .parameter "val"

    #@0
    .prologue
    .line 54
    iget-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/bluetooth/btservice/RemoteDevices;->devicePropertyChangedCallback([B[I[[B)V

    #@5
    .line 55
    return-void
.end method

.method discoveryStateChangeCallback(I)V
    .registers 3
    .parameter "state"

    #@0
    .prologue
    .line 78
    iget-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mAdapterProperties:Lcom/android/bluetooth/btservice/AdapterProperties;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterProperties;->discoveryStateChangeCallback(I)V

    #@5
    .line 79
    return-void
.end method

.method init(Lcom/android/bluetooth/btservice/BondStateMachine;Lcom/android/bluetooth/btservice/RemoteDevices;)V
    .registers 3
    .parameter "bondStateMachine"
    .parameter "remoteDevices"

    #@0
    .prologue
    .line 33
    iput-object p2, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    .line 34
    iput-object p1, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mBondStateMachine:Lcom/android/bluetooth/btservice/BondStateMachine;

    #@4
    .line 35
    return-void
.end method

.method pinRequestCallback([B[BI)V
    .registers 5
    .parameter "address"
    .parameter "name"
    .parameter "cod"

    #@0
    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/bluetooth/btservice/RemoteDevices;->pinRequestCallback([B[BI)V

    #@5
    .line 63
    return-void
.end method

.method sspRequestCallback([B[BIII)V
    .registers 12
    .parameter "address"
    .parameter "name"
    .parameter "cod"
    .parameter "pairingVariant"
    .parameter "passkey"

    #@0
    .prologue
    .line 50
    iget-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mRemoteDevices:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    move-object v1, p1

    #@3
    move-object v2, p2

    #@4
    move v3, p3

    #@5
    move v4, p4

    #@6
    move v5, p5

    #@7
    invoke-virtual/range {v0 .. v5}, Lcom/android/bluetooth/btservice/RemoteDevices;->sspRequestCallback([B[BIII)V

    #@a
    .line 52
    return-void
.end method

.method stateChangeCallback(I)V
    .registers 3
    .parameter "status"

    #@0
    .prologue
    .line 74
    iget-object v0, p0, Lcom/android/bluetooth/btservice/JniCallbacks;->mAdapterStateMachine:Lcom/android/bluetooth/btservice/AdapterState;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterState;->stateChangeCallback(I)V

    #@5
    .line 75
    return-void
.end method
