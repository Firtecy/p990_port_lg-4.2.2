.class Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;
.super Lcom/android/internal/util/State;
.source "AdapterState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/btservice/AdapterState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PendingCommandState"
.end annotation


# instance fields
.field private mIsTurningOff:Z

.field private mIsTurningOffRadio:Z

.field private mIsTurningOn:Z

.field private mIsTurningOnRadio:Z

.field private mRequestId:I

.field final synthetic this$0:Lcom/android/bluetooth/btservice/AdapterState;


# direct methods
.method private constructor <init>(Lcom/android/bluetooth/btservice/AdapterState;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 337
    iput-object p1, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2
    invoke-direct {p0}, Lcom/android/internal/util/State;-><init>()V

    #@5
    return-void
.end method

.method synthetic constructor <init>(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/bluetooth/btservice/AdapterState$1;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 337
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;-><init>(Lcom/android/bluetooth/btservice/AdapterState;)V

    #@3
    return-void
.end method


# virtual methods
.method public enter()V
    .registers 4

    #@0
    .prologue
    .line 348
    iget-object v0, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "Entering PendingCommandState State: isTurningOn()="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->isTurningOn()Z

    #@10
    move-result v2

    #@11
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    const-string v2, ", isTurningOff()="

    #@17
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->isTurningOff()Z

    #@1e
    move-result v2

    #@1f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@22
    move-result-object v1

    #@23
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v1

    #@27
    invoke-static {v0, v1}, Lcom/android/bluetooth/btservice/AdapterState;->access$300(Lcom/android/bluetooth/btservice/AdapterState;Ljava/lang/String;)V

    #@2a
    .line 349
    return-void
.end method

.method public isTurningOff()Z
    .registers 2

    #@0
    .prologue
    .line 364
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->mIsTurningOff:Z

    #@2
    return v0
.end method

.method public isTurningOffRadio()Z
    .registers 2

    #@0
    .prologue
    .line 381
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->mIsTurningOffRadio:Z

    #@2
    return v0
.end method

.method public isTurningOn()Z
    .registers 2

    #@0
    .prologue
    .line 356
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->mIsTurningOn:Z

    #@2
    return v0
.end method

.method public isTurningOnRadio()Z
    .registers 2

    #@0
    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->mIsTurningOnRadio:Z

    #@2
    return v0
.end method

.method public processMessage(Landroid/os/Message;)Z
    .registers 16
    .parameter "msg"

    #@0
    .prologue
    const-wide/16 v12, 0x2710

    #@2
    const/16 v11, 0x67

    #@4
    const/16 v9, 0x65

    #@6
    const/16 v10, 0xa

    #@8
    const/4 v5, 0x0

    #@9
    .line 387
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->isTurningOn()Z

    #@c
    move-result v2

    #@d
    .line 388
    .local v2, isTurningOn:Z
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->isTurningOff()Z

    #@10
    move-result v0

    #@11
    .line 390
    .local v0, isTurningOff:Z
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->isTurningOnRadio()Z

    #@14
    move-result v3

    #@15
    .line 391
    .local v3, isTurningOnRadio:Z
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->isTurningOffRadio()Z

    #@18
    move-result v1

    #@19
    .line 394
    .local v1, isTurningOffRadio:Z
    iget v6, p1, Landroid/os/Message;->what:I

    #@1b
    sparse-switch v6, :sswitch_data_596

    #@1e
    .line 661
    const-string v6, "BluetoothAdapterState"

    #@20
    new-instance v7, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v8, "ERROR: UNEXPECTED MESSAGE: CURRENT_STATE=PENDING, MESSAGE = "

    #@27
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v7

    #@2b
    iget v8, p1, Landroid/os/Message;->what:I

    #@2d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@30
    move-result-object v7

    #@31
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@34
    move-result-object v7

    #@35
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@38
    .line 665
    :goto_38
    return v5

    #@39
    .line 397
    :sswitch_39
    const-string v5, "BluetoothAdapterState"

    #@3b
    new-instance v6, Ljava/lang/StringBuilder;

    #@3d
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@40
    const-string v7, "CURRENT_STATE=PENDING, MESSAGE = USER_TURN_ON, isTurningOn="

    #@42
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v6

    #@46
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@49
    move-result-object v6

    #@4a
    const-string v7, ", isTurningOff="

    #@4c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v6

    #@50
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@53
    move-result-object v6

    #@54
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v6

    #@58
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 400
    if-eqz v2, :cond_66

    #@5d
    .line 401
    const-string v5, "BluetoothAdapterState"

    #@5f
    const-string v6, "CURRENT_STATE=PENDING: Alreadying turning on bluetooth... Ignoring USER_TURN_ON..."

    #@61
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@64
    .line 665
    :cond_64
    :goto_64
    const/4 v5, 0x1

    #@65
    goto :goto_38

    #@66
    .line 403
    :cond_66
    const-string v5, "BluetoothAdapterState"

    #@68
    const-string v6, "CURRENT_STATE=PENDING: Deferring request USER_TURN_ON"

    #@6a
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 404
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@6f
    invoke-static {v5, p1}, Lcom/android/bluetooth/btservice/AdapterState;->access$1500(Lcom/android/bluetooth/btservice/AdapterState;Landroid/os/Message;)V

    #@72
    goto :goto_64

    #@73
    .line 409
    :sswitch_73
    const-string v5, "BluetoothAdapterState"

    #@75
    new-instance v6, Ljava/lang/StringBuilder;

    #@77
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@7a
    const-string v7, "CURRENT_STATE=PENDING, MESSAGE = USER_TURN_ON, isTurningOn="

    #@7c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7f
    move-result-object v6

    #@80
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@83
    move-result-object v6

    #@84
    const-string v7, ", isTurningOff="

    #@86
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v6

    #@8a
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@8d
    move-result-object v6

    #@8e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@91
    move-result-object v6

    #@92
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 412
    if-eqz v0, :cond_9f

    #@97
    .line 413
    const-string v5, "BluetoothAdapterState"

    #@99
    const-string v6, "CURRENT_STATE=PENDING: Alreadying turning off bluetooth... Ignoring USER_TURN_OFF..."

    #@9b
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9e
    goto :goto_64

    #@9f
    .line 415
    :cond_9f
    const-string v5, "BluetoothAdapterState"

    #@a1
    const-string v6, "CURRENT_STATE=PENDING: Deferring request USER_TURN_OFF"

    #@a3
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 416
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@a8
    invoke-static {v5, p1}, Lcom/android/bluetooth/btservice/AdapterState;->access$1600(Lcom/android/bluetooth/btservice/AdapterState;Landroid/os/Message;)V

    #@ab
    goto :goto_64

    #@ac
    .line 423
    :sswitch_ac
    const-string v5, "BluetoothAdapterState"

    #@ae
    new-instance v6, Ljava/lang/StringBuilder;

    #@b0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@b3
    const-string v7, "CURRENT_STATE=PENDING, MESSAGE = USER_TURN_ON_RADIO, isTurningOn="

    #@b5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b8
    move-result-object v6

    #@b9
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@bc
    move-result-object v6

    #@bd
    const-string v7, ", isTurningOff="

    #@bf
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c2
    move-result-object v6

    #@c3
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@c6
    move-result-object v6

    #@c7
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ca
    move-result-object v6

    #@cb
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    .line 426
    if-eqz v3, :cond_d8

    #@d0
    .line 427
    const-string v5, "BluetoothAdapterState"

    #@d2
    const-string v6, "CURRENT_STATE=PENDING: Alreadying turning on Radio... Ignoring USER_TURN_ON_RADIO..."

    #@d4
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@d7
    goto :goto_64

    #@d8
    .line 429
    :cond_d8
    const-string v5, "BluetoothAdapterState"

    #@da
    const-string v6, "CURRENT_STATE=PENDING: Deferring request USER_TURN_ON_RADIO"

    #@dc
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 430
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@e1
    invoke-static {v5, p1}, Lcom/android/bluetooth/btservice/AdapterState;->access$1700(Lcom/android/bluetooth/btservice/AdapterState;Landroid/os/Message;)V

    #@e4
    goto :goto_64

    #@e5
    .line 435
    :sswitch_e5
    const-string v5, "BluetoothAdapterState"

    #@e7
    new-instance v6, Ljava/lang/StringBuilder;

    #@e9
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@ec
    const-string v7, "CURRENT_STATE=PENDING, MESSAGE = USER_TURN_OFF_RADIO, isTurningOn="

    #@ee
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f1
    move-result-object v6

    #@f2
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v6

    #@f6
    const-string v7, ", isTurningOff="

    #@f8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v6

    #@fc
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v6

    #@100
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v6

    #@104
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@107
    .line 438
    if-eqz v1, :cond_112

    #@109
    .line 439
    const-string v5, "BluetoothAdapterState"

    #@10b
    const-string v6, "CURRENT_STATE=PENDING: Alreadying turning off Radio... Ignoring USER_TURN_OFF_RADIO..."

    #@10d
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@110
    goto/16 :goto_64

    #@112
    .line 441
    :cond_112
    const-string v5, "BluetoothAdapterState"

    #@114
    const-string v6, "CURRENT_STATE=PENDING: Deferring request USER_TURN_OFF_RADIO"

    #@116
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@119
    .line 442
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@11b
    invoke-static {v5, p1}, Lcom/android/bluetooth/btservice/AdapterState;->access$1800(Lcom/android/bluetooth/btservice/AdapterState;Landroid/os/Message;)V

    #@11e
    goto/16 :goto_64

    #@120
    .line 448
    :sswitch_120
    const-string v5, "BluetoothAdapterState"

    #@122
    new-instance v6, Ljava/lang/StringBuilder;

    #@124
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@127
    const-string v7, "CURRENT_STATE=PENDING, MESSAGE = STARTED, isTurningOn="

    #@129
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12c
    move-result-object v6

    #@12d
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@130
    move-result-object v6

    #@131
    const-string v7, ", isTurningOff="

    #@133
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@136
    move-result-object v6

    #@137
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13a
    move-result-object v6

    #@13b
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@13e
    move-result-object v6

    #@13f
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@142
    .line 451
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@144
    const/16 v6, 0x64

    #@146
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$1900(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@149
    .line 454
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@14b
    invoke-static {v5}, Lcom/android/bluetooth/btservice/AdapterState;->access$700(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterService;

    #@14e
    move-result-object v5

    #@14f
    invoke-virtual {v5}, Lcom/android/bluetooth/btservice/AdapterService;->enableNative()Z

    #@152
    move-result v4

    #@153
    .line 455
    .local v4, ret:Z
    if-nez v4, :cond_16e

    #@155
    .line 456
    const-string v5, "BluetoothAdapterState"

    #@157
    const-string v6, "Error while turning Bluetooth On"

    #@159
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@15c
    .line 457
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@15e
    invoke-static {v5, v10}, Lcom/android/bluetooth/btservice/AdapterState;->access$400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@161
    .line 458
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@163
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@165
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2000(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OffState;

    #@168
    move-result-object v6

    #@169
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2100(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@16c
    goto/16 :goto_64

    #@16e
    .line 460
    :cond_16e
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@170
    invoke-virtual {v5, v9, v12, v13}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessageDelayed(IJ)V

    #@173
    goto/16 :goto_64

    #@175
    .line 468
    .end local v4           #ret:Z
    :sswitch_175
    const-string v6, "BluetoothAdapterState"

    #@177
    new-instance v7, Ljava/lang/StringBuilder;

    #@179
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@17c
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = BEGIN_ENABLE_RADIO, isTurningOnRadio="

    #@17e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@181
    move-result-object v7

    #@182
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@185
    move-result-object v7

    #@186
    const-string v8, ", isTurningOffRadio="

    #@188
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18b
    move-result-object v7

    #@18c
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18f
    move-result-object v7

    #@190
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@193
    move-result-object v7

    #@194
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@197
    .line 472
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@199
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$700(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterService;

    #@19c
    move-result-object v6

    #@19d
    invoke-virtual {v6}, Lcom/android/bluetooth/btservice/AdapterService;->enableRadioNative()Z

    #@1a0
    move-result v4

    #@1a1
    .line 473
    .restart local v4       #ret:Z
    if-nez v4, :cond_1d8

    #@1a3
    .line 474
    const-string v6, "BluetoothAdapterState"

    #@1a5
    const-string v7, "Error while turning Radio On"

    #@1a7
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1aa
    .line 475
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@1ac
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$1100(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterProperties;

    #@1af
    move-result-object v6

    #@1b0
    invoke-virtual {v6}, Lcom/android/bluetooth/btservice/AdapterProperties;->getState()I

    #@1b3
    move-result v6

    #@1b4
    if-ne v10, v6, :cond_1cc

    #@1b6
    .line 476
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@1b8
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@1ba
    invoke-static {v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$2000(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OffState;

    #@1bd
    move-result-object v7

    #@1be
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$2200(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@1c1
    .line 480
    :goto_1c1
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@1c3
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@1c6
    move-result-object v6

    #@1c7
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOnRadio(Z)V

    #@1ca
    goto/16 :goto_64

    #@1cc
    .line 478
    :cond_1cc
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@1ce
    iget-object v7, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@1d0
    invoke-static {v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$2300(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OnState;

    #@1d3
    move-result-object v7

    #@1d4
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$2400(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@1d7
    goto :goto_1c1

    #@1d8
    .line 482
    :cond_1d8
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@1da
    invoke-virtual {v5, v9, v12, v13}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessageDelayed(IJ)V

    #@1dd
    goto/16 :goto_64

    #@1df
    .line 490
    .end local v4           #ret:Z
    :sswitch_1df
    const-string v6, "BluetoothAdapterState"

    #@1e1
    new-instance v7, Ljava/lang/StringBuilder;

    #@1e3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@1e6
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = ENABLE_READY, isTurningOn="

    #@1e8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v7

    #@1ec
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1ef
    move-result-object v7

    #@1f0
    const-string v8, ", isTurningOff="

    #@1f2
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f5
    move-result-object v7

    #@1f6
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@1f9
    move-result-object v7

    #@1fa
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1fd
    move-result-object v7

    #@1fe
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@201
    .line 492
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@203
    invoke-static {v6, v9}, Lcom/android/bluetooth/btservice/AdapterState;->access$2500(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@206
    .line 493
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@208
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$1100(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterProperties;

    #@20b
    move-result-object v6

    #@20c
    invoke-virtual {v6}, Lcom/android/bluetooth/btservice/AdapterProperties;->onBluetoothReady()V

    #@20f
    .line 494
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@211
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@214
    move-result-object v6

    #@215
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOn(Z)V

    #@218
    .line 495
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@21a
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@21c
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2300(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OnState;

    #@21f
    move-result-object v6

    #@220
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2600(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@223
    .line 496
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@225
    const/16 v6, 0xc

    #@227
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@22a
    goto/16 :goto_64

    #@22c
    .line 501
    :sswitch_22c
    const-string v6, "BluetoothAdapterState"

    #@22e
    new-instance v7, Ljava/lang/StringBuilder;

    #@230
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@233
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = ENABLED_RADIO, isTurningOnRadio="

    #@235
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@238
    move-result-object v7

    #@239
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@23c
    move-result-object v7

    #@23d
    const-string v8, ", isTurningOffRadio="

    #@23f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@242
    move-result-object v7

    #@243
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@246
    move-result-object v7

    #@247
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24a
    move-result-object v7

    #@24b
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24e
    .line 505
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@250
    invoke-static {v6, v9}, Lcom/android/bluetooth/btservice/AdapterState;->access$2700(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@253
    .line 506
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@255
    const/4 v7, 0x1

    #@256
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$802(Lcom/android/bluetooth/btservice/AdapterState;Z)Z

    #@259
    .line 507
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@25b
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$1100(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterProperties;

    #@25e
    move-result-object v6

    #@25f
    invoke-virtual {v6}, Lcom/android/bluetooth/btservice/AdapterProperties;->getState()I

    #@262
    move-result v6

    #@263
    if-ne v10, v6, :cond_282

    #@265
    .line 508
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@267
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@26a
    move-result-object v6

    #@26b
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOnRadio(Z)V

    #@26e
    .line 509
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@270
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@272
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2000(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OffState;

    #@275
    move-result-object v6

    #@276
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2800(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@279
    .line 510
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@27b
    const/16 v6, 0xe

    #@27d
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2900(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@280
    goto/16 :goto_64

    #@282
    .line 513
    :cond_282
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@284
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@286
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2300(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OnState;

    #@289
    move-result-object v6

    #@28a
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$3000(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@28d
    .line 516
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@28f
    const/16 v6, 0xe

    #@291
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2900(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@294
    goto/16 :goto_64

    #@296
    .line 522
    :sswitch_296
    const-string v6, "BluetoothAdapterState"

    #@298
    const-string v7, "Timeout will setting scan mode..Continuing with disable..."

    #@29a
    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@29d
    .line 526
    :sswitch_29d
    const-string v6, "BluetoothAdapterState"

    #@29f
    new-instance v7, Ljava/lang/StringBuilder;

    #@2a1
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2a4
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = BEGIN_DISABLE, isTurningOn="

    #@2a6
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a9
    move-result-object v7

    #@2aa
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2ad
    move-result-object v7

    #@2ae
    const-string v8, ", isTurningOff="

    #@2b0
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2b3
    move-result-object v7

    #@2b4
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2b7
    move-result-object v7

    #@2b8
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2bb
    move-result-object v7

    #@2bc
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2bf
    .line 528
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2c1
    const/16 v7, 0x69

    #@2c3
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$3100(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@2c6
    .line 529
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2c8
    invoke-virtual {v6, v11, v12, v13}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessageDelayed(IJ)V

    #@2cb
    .line 530
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2cd
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$700(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterService;

    #@2d0
    move-result-object v6

    #@2d1
    invoke-virtual {v6}, Lcom/android/bluetooth/btservice/AdapterService;->disableNative()Z

    #@2d4
    move-result v4

    #@2d5
    .line 531
    .restart local v4       #ret:Z
    if-nez v4, :cond_64

    #@2d7
    .line 532
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2d9
    invoke-static {v6, v11}, Lcom/android/bluetooth/btservice/AdapterState;->access$3200(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@2dc
    .line 533
    const-string v6, "BluetoothAdapterState"

    #@2de
    const-string v7, "Error while turning Bluetooth Off"

    #@2e0
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e3
    .line 535
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2e5
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@2e8
    move-result-object v6

    #@2e9
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOff(Z)V

    #@2ec
    .line 536
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@2ee
    const/16 v6, 0xc

    #@2f0
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@2f3
    goto/16 :goto_64

    #@2f5
    .line 543
    .end local v4           #ret:Z
    :sswitch_2f5
    const-string v6, "BluetoothAdapterState"

    #@2f7
    new-instance v7, Ljava/lang/StringBuilder;

    #@2f9
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2fc
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = BEGIN_DISABLE_RADIO"

    #@2fe
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@301
    move-result-object v7

    #@302
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@305
    move-result-object v7

    #@306
    const-string v8, ", isTurningOffRadio="

    #@308
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30b
    move-result-object v7

    #@30c
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@30f
    move-result-object v7

    #@310
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@313
    move-result-object v7

    #@314
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@317
    .line 546
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@319
    invoke-virtual {v6, v11, v12, v13}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessageDelayed(IJ)V

    #@31c
    .line 547
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@31e
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$700(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterService;

    #@321
    move-result-object v6

    #@322
    invoke-virtual {v6}, Lcom/android/bluetooth/btservice/AdapterService;->disableRadioNative()Z

    #@325
    move-result v4

    #@326
    .line 548
    .restart local v4       #ret:Z
    if-nez v4, :cond_64

    #@328
    .line 549
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@32a
    invoke-static {v6, v11}, Lcom/android/bluetooth/btservice/AdapterState;->access$3300(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@32d
    .line 550
    const-string v6, "BluetoothAdapterState"

    #@32f
    const-string v7, "Error while turning Radio Off"

    #@331
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@334
    .line 551
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@336
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@339
    move-result-object v6

    #@33a
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOffRadio(Z)V

    #@33d
    goto/16 :goto_64

    #@33f
    .line 559
    .end local v4           #ret:Z
    :sswitch_33f
    const-string v6, "BluetoothAdapterState"

    #@341
    new-instance v7, Ljava/lang/StringBuilder;

    #@343
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@346
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = DISABLED, isTurningOn="

    #@348
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34b
    move-result-object v7

    #@34c
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@34f
    move-result-object v7

    #@350
    const-string v8, ", isTurningOff="

    #@352
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@355
    move-result-object v7

    #@356
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@359
    move-result-object v7

    #@35a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@35d
    move-result-object v7

    #@35e
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@361
    .line 562
    if-eqz v2, :cond_393

    #@363
    .line 563
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@365
    invoke-static {v6, v9}, Lcom/android/bluetooth/btservice/AdapterState;->access$3400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@368
    .line 564
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@36a
    const-string v7, "Error enabling Bluetooth - hardware init failed"

    #@36c
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$3500(Lcom/android/bluetooth/btservice/AdapterState;Ljava/lang/String;)V

    #@36f
    .line 565
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@371
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@374
    move-result-object v6

    #@375
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOn(Z)V

    #@378
    .line 566
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@37a
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@37c
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2000(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OffState;

    #@37f
    move-result-object v6

    #@380
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$3600(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@383
    .line 567
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@385
    invoke-static {v5}, Lcom/android/bluetooth/btservice/AdapterState;->access$700(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterService;

    #@388
    move-result-object v5

    #@389
    invoke-virtual {v5}, Lcom/android/bluetooth/btservice/AdapterService;->stopProfileServices()Z

    #@38c
    .line 568
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@38e
    invoke-static {v5, v10}, Lcom/android/bluetooth/btservice/AdapterState;->access$400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@391
    goto/16 :goto_64

    #@393
    .line 572
    :cond_393
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@395
    invoke-static {v6, v11}, Lcom/android/bluetooth/btservice/AdapterState;->access$3700(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@398
    .line 573
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@39a
    const/16 v7, 0x68

    #@39c
    const-wide/16 v8, 0x1388

    #@39e
    invoke-virtual {v6, v7, v8, v9}, Lcom/android/bluetooth/btservice/AdapterState;->sendMessageDelayed(IJ)V

    #@3a1
    .line 574
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@3a3
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$700(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterService;

    #@3a6
    move-result-object v6

    #@3a7
    invoke-virtual {v6}, Lcom/android/bluetooth/btservice/AdapterService;->stopProfileServices()Z

    #@3aa
    move-result v6

    #@3ab
    if-eqz v6, :cond_3b6

    #@3ad
    .line 575
    const-string v5, "BluetoothAdapterState"

    #@3af
    const-string v6, "Stopping profile services that were post enabled"

    #@3b1
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b4
    goto/16 :goto_64

    #@3b6
    .line 581
    :cond_3b6
    :sswitch_3b6
    const-string v6, "BluetoothAdapterState"

    #@3b8
    new-instance v7, Ljava/lang/StringBuilder;

    #@3ba
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3bd
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = STOPPED, isTurningOn="

    #@3bf
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c2
    move-result-object v7

    #@3c3
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3c6
    move-result-object v7

    #@3c7
    const-string v8, ", isTurningOff="

    #@3c9
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3cc
    move-result-object v7

    #@3cd
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@3d0
    move-result-object v7

    #@3d1
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d4
    move-result-object v7

    #@3d5
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3d8
    .line 583
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@3da
    const/16 v7, 0x68

    #@3dc
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$3800(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@3df
    .line 584
    invoke-virtual {p0, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOff(Z)V

    #@3e2
    .line 585
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@3e4
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@3e6
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2000(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OffState;

    #@3e9
    move-result-object v6

    #@3ea
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$3900(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@3ed
    .line 586
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@3ef
    invoke-static {v5, v10}, Lcom/android/bluetooth/btservice/AdapterState;->access$400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@3f2
    goto/16 :goto_64

    #@3f4
    .line 591
    :sswitch_3f4
    const-string v6, "BluetoothAdapterState"

    #@3f6
    new-instance v7, Ljava/lang/StringBuilder;

    #@3f8
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3fb
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = STOPPED, isTurningOnRadio="

    #@3fd
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@400
    move-result-object v7

    #@401
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@404
    move-result-object v7

    #@405
    const-string v8, ", isTurningOffRadio="

    #@407
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40a
    move-result-object v7

    #@40b
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->isTurningOffRadio()Z

    #@40e
    move-result v8

    #@40f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@412
    move-result-object v7

    #@413
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@416
    move-result-object v7

    #@417
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41a
    .line 595
    if-eqz v3, :cond_431

    #@41c
    .line 596
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@41e
    invoke-static {v6, v9}, Lcom/android/bluetooth/btservice/AdapterState;->access$4000(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@421
    .line 597
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@423
    const-string v7, "Error enabling radio - hardware init failed"

    #@425
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$3500(Lcom/android/bluetooth/btservice/AdapterState;Ljava/lang/String;)V

    #@428
    .line 598
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@42a
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@42d
    move-result-object v6

    #@42e
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOnRadio(Z)V

    #@431
    .line 601
    :cond_431
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@433
    invoke-static {v6, v11}, Lcom/android/bluetooth/btservice/AdapterState;->access$4100(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@436
    .line 602
    invoke-virtual {p0, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOffRadio(Z)V

    #@439
    .line 603
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@43b
    invoke-static {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState;->access$802(Lcom/android/bluetooth/btservice/AdapterState;Z)Z

    #@43e
    .line 604
    const/16 v5, 0xc

    #@440
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@442
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$1100(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterProperties;

    #@445
    move-result-object v6

    #@446
    invoke-virtual {v6}, Lcom/android/bluetooth/btservice/AdapterProperties;->getState()I

    #@449
    move-result v6

    #@44a
    if-ne v5, v6, :cond_460

    #@44c
    .line 605
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@44e
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@450
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2300(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OnState;

    #@453
    move-result-object v6

    #@454
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$4200(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@457
    .line 606
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@459
    const/16 v6, 0xf

    #@45b
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2900(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@45e
    goto/16 :goto_64

    #@460
    .line 608
    :cond_460
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@462
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@464
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2000(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OffState;

    #@467
    move-result-object v6

    #@468
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$4300(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@46b
    .line 609
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@46d
    const/16 v6, 0xf

    #@46f
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2900(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@472
    goto/16 :goto_64

    #@474
    .line 617
    :sswitch_474
    const-string v6, "BluetoothAdapterState"

    #@476
    new-instance v7, Ljava/lang/StringBuilder;

    #@478
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@47b
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = START_TIMEOUT, isTurningOn="

    #@47d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@480
    move-result-object v7

    #@481
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@484
    move-result-object v7

    #@485
    const-string v8, ", isTurningOff="

    #@487
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48a
    move-result-object v7

    #@48b
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@48e
    move-result-object v7

    #@48f
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@492
    move-result-object v7

    #@493
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@496
    .line 619
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@498
    const-string v7, "Error enabling Bluetooth"

    #@49a
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$3500(Lcom/android/bluetooth/btservice/AdapterState;Ljava/lang/String;)V

    #@49d
    .line 620
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@49f
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@4a2
    move-result-object v6

    #@4a3
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOn(Z)V

    #@4a6
    .line 621
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4a8
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4aa
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2000(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OffState;

    #@4ad
    move-result-object v6

    #@4ae
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$4400(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@4b1
    .line 622
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4b3
    invoke-static {v5, v10}, Lcom/android/bluetooth/btservice/AdapterState;->access$400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@4b6
    goto/16 :goto_64

    #@4b8
    .line 626
    :sswitch_4b8
    const-string v6, "BluetoothAdapterState"

    #@4ba
    new-instance v7, Ljava/lang/StringBuilder;

    #@4bc
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4bf
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = ENABLE_TIMEOUT, isTurningOn="

    #@4c1
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4c4
    move-result-object v7

    #@4c5
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4c8
    move-result-object v7

    #@4c9
    const-string v8, ", isTurningOff="

    #@4cb
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4ce
    move-result-object v7

    #@4cf
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4d2
    move-result-object v7

    #@4d3
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d6
    move-result-object v7

    #@4d7
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4da
    .line 628
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4dc
    const-string v7, "Error enabling Bluetooth"

    #@4de
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$3500(Lcom/android/bluetooth/btservice/AdapterState;Ljava/lang/String;)V

    #@4e1
    .line 629
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4e3
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@4e6
    move-result-object v6

    #@4e7
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOn(Z)V

    #@4ea
    .line 631
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4ec
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@4ef
    move-result-object v6

    #@4f0
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOnRadio(Z)V

    #@4f3
    .line 632
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4f5
    invoke-static {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState;->access$802(Lcom/android/bluetooth/btservice/AdapterState;Z)Z

    #@4f8
    .line 634
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4fa
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@4fc
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2000(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OffState;

    #@4ff
    move-result-object v6

    #@500
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$4500(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@503
    .line 635
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@505
    invoke-static {v5, v10}, Lcom/android/bluetooth/btservice/AdapterState;->access$400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@508
    goto/16 :goto_64

    #@50a
    .line 639
    :sswitch_50a
    const-string v6, "BluetoothAdapterState"

    #@50c
    new-instance v7, Ljava/lang/StringBuilder;

    #@50e
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@511
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = STOP_TIMEOUT, isTurningOn="

    #@513
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@516
    move-result-object v7

    #@517
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@51a
    move-result-object v7

    #@51b
    const-string v8, ", isTurningOff="

    #@51d
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@520
    move-result-object v7

    #@521
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@524
    move-result-object v7

    #@525
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@528
    move-result-object v7

    #@529
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52c
    .line 641
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@52e
    const-string v7, "Error stopping Bluetooth profiles"

    #@530
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$3500(Lcom/android/bluetooth/btservice/AdapterState;Ljava/lang/String;)V

    #@533
    .line 642
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@535
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@538
    move-result-object v6

    #@539
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOff(Z)V

    #@53c
    .line 643
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@53e
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@540
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2000(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OffState;

    #@543
    move-result-object v6

    #@544
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$4600(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@547
    .line 645
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@549
    invoke-static {v5, v10}, Lcom/android/bluetooth/btservice/AdapterState;->access$400(Lcom/android/bluetooth/btservice/AdapterState;I)V

    #@54c
    goto/16 :goto_64

    #@54e
    .line 650
    :sswitch_54e
    const-string v6, "BluetoothAdapterState"

    #@550
    new-instance v7, Ljava/lang/StringBuilder;

    #@552
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@555
    const-string v8, "CURRENT_STATE=PENDING, MESSAGE = DISABLE_TIMEOUT, isTurningOn="

    #@557
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@55a
    move-result-object v7

    #@55b
    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@55e
    move-result-object v7

    #@55f
    const-string v8, ", isTurningOff="

    #@561
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@564
    move-result-object v7

    #@565
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@568
    move-result-object v7

    #@569
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56c
    move-result-object v7

    #@56d
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@570
    .line 652
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@572
    const-string v7, "Error disabling Bluetooth"

    #@574
    invoke-static {v6, v7}, Lcom/android/bluetooth/btservice/AdapterState;->access$3500(Lcom/android/bluetooth/btservice/AdapterState;Ljava/lang/String;)V

    #@577
    .line 653
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@579
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@57c
    move-result-object v6

    #@57d
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOff(Z)V

    #@580
    .line 655
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@582
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$500(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;

    #@585
    move-result-object v6

    #@586
    invoke-virtual {v6, v5}, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->setTurningOffRadio(Z)V

    #@589
    .line 657
    iget-object v5, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@58b
    iget-object v6, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->this$0:Lcom/android/bluetooth/btservice/AdapterState;

    #@58d
    invoke-static {v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$2300(Lcom/android/bluetooth/btservice/AdapterState;)Lcom/android/bluetooth/btservice/AdapterState$OnState;

    #@590
    move-result-object v6

    #@591
    invoke-static {v5, v6}, Lcom/android/bluetooth/btservice/AdapterState;->access$4700(Lcom/android/bluetooth/btservice/AdapterState;Lcom/android/internal/util/IState;)V

    #@594
    goto/16 :goto_64

    #@596
    .line 394
    :sswitch_data_596
    .sparse-switch
        0x1 -> :sswitch_39
        0x2 -> :sswitch_120
        0x3 -> :sswitch_1df
        0x14 -> :sswitch_73
        0x15 -> :sswitch_29d
        0x18 -> :sswitch_33f
        0x19 -> :sswitch_3b6
        0x64 -> :sswitch_474
        0x65 -> :sswitch_4b8
        0x67 -> :sswitch_54e
        0x68 -> :sswitch_50a
        0x69 -> :sswitch_296
        0xc8 -> :sswitch_22c
        0xc9 -> :sswitch_175
        0xca -> :sswitch_ac
        0xcb -> :sswitch_e5
        0xcc -> :sswitch_3f4
        0xcd -> :sswitch_2f5
    .end sparse-switch
.end method

.method public setTurningOff(Z)V
    .registers 2
    .parameter "isTurningOff"

    #@0
    .prologue
    .line 360
    iput-boolean p1, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->mIsTurningOff:Z

    #@2
    .line 361
    return-void
.end method

.method public setTurningOffRadio(Z)V
    .registers 2
    .parameter "isTurningOffRadio"

    #@0
    .prologue
    .line 377
    iput-boolean p1, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->mIsTurningOffRadio:Z

    #@2
    .line 378
    return-void
.end method

.method public setTurningOn(Z)V
    .registers 2
    .parameter "isTurningOn"

    #@0
    .prologue
    .line 352
    iput-boolean p1, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->mIsTurningOn:Z

    #@2
    .line 353
    return-void
.end method

.method public setTurningOnRadio(Z)V
    .registers 2
    .parameter "isTurningOnRadio"

    #@0
    .prologue
    .line 369
    iput-boolean p1, p0, Lcom/android/bluetooth/btservice/AdapterState$PendingCommandState;->mIsTurningOnRadio:Z

    #@2
    .line 370
    return-void
.end method
