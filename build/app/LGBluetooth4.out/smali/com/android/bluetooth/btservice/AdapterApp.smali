.class public Lcom/android/bluetooth/btservice/AdapterApp;
.super Landroid/app/Application;
.source "AdapterApp.java"


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "BluetoothAdapterApp"

.field private static sRefCount:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 37
    const/4 v0, 0x0

    #@1
    sput v0, Lcom/android/bluetooth/btservice/AdapterApp;->sRefCount:I

    #@3
    .line 41
    const-string v0, "BluetoothAdapterApp"

    #@5
    const-string v1, "Loading JNI Library"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 45
    const-string v0, "bluetooth_jni2"

    #@c
    #invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    #@f
    .line 47
    return-void
.end method

.method public constructor <init>()V
    .registers 5

    #@0
    .prologue
    .line 50
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    #@3
    .line 52
    const-class v1, Lcom/android/bluetooth/btservice/AdapterApp;

    #@5
    monitor-enter v1

    #@6
    .line 53
    :try_start_6
    sget v0, Lcom/android/bluetooth/btservice/AdapterApp;->sRefCount:I

    #@8
    add-int/lit8 v0, v0, 0x1

    #@a
    sput v0, Lcom/android/bluetooth/btservice/AdapterApp;->sRefCount:I

    #@c
    .line 54
    const-string v0, "BluetoothAdapterApp"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "REFCOUNT: Constructed "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    const-string v3, " Instance Count = "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    sget v3, Lcom/android/bluetooth/btservice/AdapterApp;->sRefCount:I

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 55
    monitor-exit v1

    #@31
    .line 57
    return-void

    #@32
    .line 55
    :catchall_32
    move-exception v0

    #@33
    monitor-exit v1
    :try_end_34
    .catchall {:try_start_6 .. :try_end_34} :catchall_32

    #@34
    throw v0
.end method


# virtual methods
.method protected finalize()V
    .registers 5

    #@0
    .prologue
    .line 77
    const-class v1, Lcom/android/bluetooth/btservice/AdapterApp;

    #@2
    monitor-enter v1

    #@3
    .line 78
    :try_start_3
    sget v0, Lcom/android/bluetooth/btservice/AdapterApp;->sRefCount:I

    #@5
    add-int/lit8 v0, v0, -0x1

    #@7
    sput v0, Lcom/android/bluetooth/btservice/AdapterApp;->sRefCount:I

    #@9
    .line 79
    const-string v0, "BluetoothAdapterApp"

    #@b
    new-instance v2, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v3, "REFCOUNT: Finalized: "

    #@12
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v2

    #@16
    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    const-string v3, ", Instance Count = "

    #@1c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1f
    move-result-object v2

    #@20
    sget v3, Lcom/android/bluetooth/btservice/AdapterApp;->sRefCount:I

    #@22
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@25
    move-result-object v2

    #@26
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v2

    #@2a
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 80
    monitor-exit v1

    #@2e
    .line 82
    return-void

    #@2f
    .line 80
    :catchall_2f
    move-exception v0

    #@30
    monitor-exit v1
    :try_end_31
    .catchall {:try_start_3 .. :try_end_31} :catchall_2f

    #@31
    throw v0
.end method

.method public onCreate()V
    .registers 3

    #@0
    .prologue
    .line 61
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    #@3
    .line 63
    const-string v0, "BluetoothAdapterApp"

    #@5
    const-string v1, "onCreate"

    #@7
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 66
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isBRCMSolution()Z

    #@d
    move-result v0

    #@e
    if-eqz v0, :cond_14

    #@10
    .line 67
    invoke-static {p0}, Lcom/broadcom/bt/service/ProfileConfig;->init(Landroid/content/Context;)V

    #@13
    .line 72
    :goto_13
    return-void

    #@14
    .line 69
    :cond_14
    invoke-static {p0}, Lcom/android/bluetooth/btservice/Config;->init(Landroid/content/Context;)V

    #@17
    goto :goto_13
.end method
