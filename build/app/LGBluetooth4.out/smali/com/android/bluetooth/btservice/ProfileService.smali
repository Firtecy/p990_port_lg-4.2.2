.class public abstract Lcom/android/bluetooth/btservice/ProfileService;
.super Landroid/app/Service;
.source "ProfileService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;
    }
.end annotation


# static fields
.field public static final BLUETOOTH_ADMIN_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH_ADMIN"

.field public static final BLUETOOTH_PERM:Ljava/lang/String; = "android.permission.BLUETOOTH"

.field private static final DBG:Z = true

.field private static final PROFILE_SERVICE_MODE:I = 0x2

.field private static sReferenceCount:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected mAdapter:Landroid/bluetooth/BluetoothAdapter;

.field protected mBinder:Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

.field private mCleaningUp:Z

.field protected mContext:Landroid/content/Context;

.field protected mName:Ljava/lang/String;

.field protected mStartError:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 37
    new-instance v0, Ljava/util/HashMap;

    #@2
    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    #@5
    sput-object v0, Lcom/android/bluetooth/btservice/ProfileService;->sReferenceCount:Ljava/util/HashMap;

    #@7
    return-void
.end method

.method protected constructor <init>()V
    .registers 5

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 76
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@4
    .line 52
    iput-boolean v1, p0, Lcom/android/bluetooth/btservice/ProfileService;->mStartError:Z

    #@6
    .line 53
    iput-boolean v1, p0, Lcom/android/bluetooth/btservice/ProfileService;->mCleaningUp:Z

    #@8
    .line 77
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/ProfileService;->getName()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    iput-object v1, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@e
    .line 79
    sget-object v2, Lcom/android/bluetooth/btservice/ProfileService;->sReferenceCount:Ljava/util/HashMap;

    #@10
    monitor-enter v2

    #@11
    .line 80
    :try_start_11
    sget-object v1, Lcom/android/bluetooth/btservice/ProfileService;->sReferenceCount:Ljava/util/HashMap;

    #@13
    iget-object v3, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@15
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v0

    #@19
    check-cast v0, Ljava/lang/Integer;

    #@1b
    .line 81
    .local v0, refCount:Ljava/lang/Integer;
    if-nez v0, :cond_41

    #@1d
    .line 82
    const/4 v1, 0x1

    #@1e
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v0

    #@22
    .line 86
    :goto_22
    sget-object v1, Lcom/android/bluetooth/btservice/ProfileService;->sReferenceCount:Ljava/util/HashMap;

    #@24
    iget-object v3, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@26
    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v3, "REFCOUNT: CREATED. INSTANCE_COUNT="

    #@30
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v1

    #@34
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v1

    #@38
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v1

    #@3c
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@3f
    .line 90
    monitor-exit v2

    #@40
    .line 92
    return-void

    #@41
    .line 84
    :cond_41
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@44
    move-result v1

    #@45
    add-int/lit8 v1, v1, 0x1

    #@47
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4a
    move-result-object v0

    #@4b
    goto :goto_22

    #@4c
    .line 90
    .end local v0           #refCount:Ljava/lang/Integer;
    :catchall_4c
    move-exception v1

    #@4d
    monitor-exit v2
    :try_end_4e
    .catchall {:try_start_11 .. :try_end_4e} :catchall_4c

    #@4e
    throw v1
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 57
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@4
    .line 52
    iput-boolean v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mStartError:Z

    #@6
    .line 53
    iput-boolean v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mCleaningUp:Z

    #@8
    .line 58
    iput-object p1, p0, Lcom/android/bluetooth/btservice/ProfileService;->mContext:Landroid/content/Context;

    #@a
    .line 59
    return-void
.end method

.method private doStart(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    if-nez v0, :cond_c

    #@4
    .line 194
    iget-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@6
    const-string v1, "Error starting profile. BluetoothAdapter is null"

    #@8
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 206
    :goto_b
    return-void

    #@c
    .line 197
    :cond_c
    const-string v0, "start()"

    #@e
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@11
    .line 199
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/ProfileService;->start()Z

    #@14
    move-result v0

    #@15
    if-nez v0, :cond_24

    #@17
    const/4 v0, 0x1

    #@18
    :goto_18
    iput-boolean v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mStartError:Z

    #@1a
    .line 200
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mStartError:Z

    #@1c
    if-nez v0, :cond_26

    #@1e
    .line 201
    const/16 v0, 0xc

    #@20
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/ProfileService;->notifyProfileServiceStateChanged(I)V

    #@23
    goto :goto_b

    #@24
    .line 199
    :cond_24
    const/4 v0, 0x0

    #@25
    goto :goto_18

    #@26
    .line 203
    :cond_26
    iget-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@28
    const-string v1, "Error starting profile. BluetoothAdapter is null"

    #@2a
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_b
.end method

.method private doStop(Landroid/content/Intent;)V
    .registers 4
    .parameter "intent"

    #@0
    .prologue
    .line 209
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/ProfileService;->stop()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_14

    #@6
    .line 211
    const-string v0, "stop()"

    #@8
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@b
    .line 213
    const/16 v0, 0xa

    #@d
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/ProfileService;->notifyProfileServiceStateChanged(I)V

    #@10
    .line 214
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/ProfileService;->stopSelf()V

    #@13
    .line 218
    :goto_13
    return-void

    #@14
    .line 216
    :cond_14
    iget-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@16
    const-string v1, "Unable to stop profile"

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    goto :goto_13
.end method


# virtual methods
.method protected cleanup()Z
    .registers 2

    #@0
    .prologue
    .line 73
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected finalize()V
    .registers 5

    #@0
    .prologue
    .line 96
    sget-object v2, Lcom/android/bluetooth/btservice/ProfileService;->sReferenceCount:Ljava/util/HashMap;

    #@2
    monitor-enter v2

    #@3
    .line 97
    :try_start_3
    sget-object v1, Lcom/android/bluetooth/btservice/ProfileService;->sReferenceCount:Ljava/util/HashMap;

    #@5
    iget-object v3, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@7
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@a
    move-result-object v0

    #@b
    check-cast v0, Ljava/lang/Integer;

    #@d
    .line 98
    .local v0, refCount:Ljava/lang/Integer;
    if-eqz v0, :cond_38

    #@f
    .line 99
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    #@12
    move-result v1

    #@13
    add-int/lit8 v1, v1, -0x1

    #@15
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@18
    move-result-object v0

    #@19
    .line 103
    :goto_19
    sget-object v1, Lcom/android/bluetooth/btservice/ProfileService;->sReferenceCount:Ljava/util/HashMap;

    #@1b
    iget-object v3, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@1d
    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@20
    .line 104
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    const-string v3, "REFCOUNT: FINALIZED. INSTANCE_COUNT="

    #@27
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v1

    #@2f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@32
    move-result-object v1

    #@33
    invoke-virtual {p0, v1}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@36
    .line 105
    monitor-exit v2

    #@37
    .line 107
    return-void

    #@38
    .line 101
    :cond_38
    const/4 v1, 0x0

    #@39
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@3c
    move-result-object v0

    #@3d
    goto :goto_19

    #@3e
    .line 105
    .end local v0           #refCount:Ljava/lang/Integer;
    :catchall_3e
    move-exception v1

    #@3f
    monitor-exit v2
    :try_end_40
    .catchall {:try_start_3 .. :try_end_40} :catchall_3e

    #@40
    throw v1
.end method

.method protected getDevice([B)Landroid/bluetooth/BluetoothDevice;
    .registers 4
    .parameter "address"

    #@0
    .prologue
    .line 237
    iget-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@2
    invoke-static {p1}, Lcom/android/bluetooth/Utils;->getAddressStringFromByte([B)Ljava/lang/String;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method protected getName()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 62
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@3
    move-result-object v0

    #@4
    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    return-object v0
.end method

.method protected abstract initBinder()Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;
.end method

.method protected isAvailable()Z
    .registers 2

    #@0
    .prologue
    .line 66
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mStartError:Z

    #@2
    if-nez v0, :cond_a

    #@4
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mCleaningUp:Z

    #@6
    if-nez v0, :cond_a

    #@8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method protected log(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 241
    iget-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 242
    return-void
.end method

.method public notifyProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V
    .registers 6
    .parameter "device"
    .parameter "profileId"
    .parameter "newState"
    .parameter "prevState"

    #@0
    .prologue
    .line 230
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 231
    .local v0, svc:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_9

    #@6
    .line 232
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/bluetooth/btservice/AdapterService;->onProfileConnectionStateChanged(Landroid/bluetooth/BluetoothDevice;III)V

    #@9
    .line 234
    :cond_9
    return-void
.end method

.method protected notifyProfileServiceStateChanged(I)V
    .registers 4
    .parameter "state"

    #@0
    .prologue
    .line 222
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 223
    .local v0, sAdapter:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_11

    #@6
    .line 224
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@9
    move-result-object v1

    #@a
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v0, v1, p1}, Lcom/android/bluetooth/btservice/AdapterService;->onProfileServiceStateChanged(Ljava/lang/String;I)V

    #@11
    .line 226
    :cond_11
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 155
    const-string v0, "onBind"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@5
    .line 157
    iget-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mBinder:Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

    #@7
    return-object v0
.end method

.method public onCreate()V
    .registers 2

    #@0
    .prologue
    .line 112
    const-string v0, "onCreate"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@5
    .line 114
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@8
    .line 115
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    #@b
    move-result-object v0

    #@c
    iput-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@e
    .line 116
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/ProfileService;->initBinder()Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

    #@11
    move-result-object v0

    #@12
    iput-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mBinder:Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

    #@14
    .line 117
    return-void
.end method

.method public onDestroy()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 170
    const-string v0, "Destroying service."

    #@3
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@6
    .line 172
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mCleaningUp:Z

    #@8
    if-eqz v0, :cond_15

    #@a
    .line 174
    const-string v0, "Cleanup already started... Skipping cleanup()..."

    #@c
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@f
    .line 187
    :cond_f
    :goto_f
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@12
    .line 188
    iput-object v1, p0, Lcom/android/bluetooth/btservice/ProfileService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@14
    .line 189
    return-void

    #@15
    .line 178
    :cond_15
    const-string v0, "cleanup()"

    #@17
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@1a
    .line 180
    const/4 v0, 0x1

    #@1b
    iput-boolean v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mCleaningUp:Z

    #@1d
    .line 181
    invoke-virtual {p0}, Lcom/android/bluetooth/btservice/ProfileService;->cleanup()Z

    #@20
    .line 182
    iget-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mBinder:Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

    #@22
    if-eqz v0, :cond_f

    #@24
    .line 183
    iget-object v0, p0, Lcom/android/bluetooth/btservice/ProfileService;->mBinder:Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

    #@26
    invoke-interface {v0}, Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;->cleanup()Z

    #@29
    .line 184
    iput-object v1, p0, Lcom/android/bluetooth/btservice/ProfileService;->mBinder:Lcom/android/bluetooth/btservice/ProfileService$IProfileServiceBinder;

    #@2b
    goto :goto_f
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 9
    .parameter "intent"
    .parameter "flags"
    .parameter "startId"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    .line 121
    const-string v2, "onStartCommand()"

    #@3
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@6
    .line 123
    iget-boolean v2, p0, Lcom/android/bluetooth/btservice/ProfileService;->mStartError:Z

    #@8
    if-nez v2, :cond_e

    #@a
    iget-object v2, p0, Lcom/android/bluetooth/btservice/ProfileService;->mAdapter:Landroid/bluetooth/BluetoothAdapter;

    #@c
    if-nez v2, :cond_19

    #@e
    .line 124
    :cond_e
    iget-object v2, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@10
    const-string v3, "Stopping profile service: device does not have BT"

    #@12
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@15
    .line 125
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/ProfileService;->doStop(Landroid/content/Intent;)V

    #@18
    .line 150
    :cond_18
    :goto_18
    return v4

    #@19
    .line 129
    :cond_19
    const-string v2, "android.permission.BLUETOOTH_ADMIN"

    #@1b
    invoke-virtual {p0, v2}, Lcom/android/bluetooth/btservice/ProfileService;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@1e
    move-result v2

    #@1f
    if-eqz v2, :cond_29

    #@21
    .line 130
    iget-object v2, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@23
    const-string v3, "Permission denied!"

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    goto :goto_18

    #@29
    .line 134
    :cond_29
    if-nez p1, :cond_33

    #@2b
    .line 135
    iget-object v2, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@2d
    const-string v3, "Restarting profile service..."

    #@2f
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    goto :goto_18

    #@33
    .line 138
    :cond_33
    const-string v2, "action"

    #@35
    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@38
    move-result-object v0

    #@39
    .line 139
    .local v0, action:Ljava/lang/String;
    const-string v2, "com.android.bluetooth.btservice.action.STATE_CHANGED"

    #@3b
    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e
    move-result v2

    #@3f
    if-eqz v2, :cond_18

    #@41
    .line 140
    const-string v2, "android.bluetooth.adapter.extra.STATE"

    #@43
    const/high16 v3, -0x8000

    #@45
    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@48
    move-result v1

    #@49
    .line 141
    .local v1, state:I
    const/16 v2, 0xa

    #@4b
    if-ne v1, v2, :cond_58

    #@4d
    .line 142
    iget-object v2, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@4f
    const-string v3, "Received stop request...Stopping profile..."

    #@51
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    .line 143
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/ProfileService;->doStop(Landroid/content/Intent;)V

    #@57
    goto :goto_18

    #@58
    .line 144
    :cond_58
    const/16 v2, 0xc

    #@5a
    if-ne v1, v2, :cond_18

    #@5c
    .line 145
    iget-object v2, p0, Lcom/android/bluetooth/btservice/ProfileService;->mName:Ljava/lang/String;

    #@5e
    const-string v3, "Received start request. Starting profile..."

    #@60
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 146
    invoke-direct {p0, p1}, Lcom/android/bluetooth/btservice/ProfileService;->doStart(Landroid/content/Intent;)V

    #@66
    goto :goto_18
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 3
    .parameter "intent"

    #@0
    .prologue
    .line 162
    const-string v0, "onUnbind"

    #@2
    invoke-virtual {p0, v0}, Lcom/android/bluetooth/btservice/ProfileService;->log(Ljava/lang/String;)V

    #@5
    .line 164
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    #@8
    move-result v0

    #@9
    return v0
.end method

.method protected abstract start()Z
.end method

.method protected abstract stop()Z
.end method
