.class Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;
.super Ljava/lang/Object;
.source "RemoteDevices.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/btservice/RemoteDevices;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceProperties"
.end annotation


# instance fields
.field private mAddress:[B

.field private mAlias:Ljava/lang/String;

.field private mBluetoothClass:I

.field private mBondState:I

.field private mDeviceTrust:Z

.field private mDeviceType:I

.field private mJustWorks:Z

.field private mName:Ljava/lang/String;

.field private mRssi:S

.field private mUuids:[Landroid/os/ParcelUuid;

.field final synthetic this$0:Lcom/android/bluetooth/btservice/RemoteDevices;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/btservice/RemoteDevices;)V
    .registers 3
    .parameter

    #@0
    .prologue
    .line 127
    iput-object p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@5
    .line 128
    const/16 v0, 0xa

    #@7
    iput v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mBondState:I

    #@9
    .line 129
    return-void
.end method

.method static synthetic access$002(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;[B)[B
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAddress:[B

    #@2
    return-object p1
.end method

.method static synthetic access$1000(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceTrust:Z

    #@2
    return v0
.end method

.method static synthetic access$1002(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceTrust:Z

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mJustWorks:Z

    #@2
    return v0
.end method

.method static synthetic access$1102(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mJustWorks:Z

    #@2
    return p1
.end method

.method static synthetic access$400(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)[Landroid/os/ParcelUuid;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mUuids:[Landroid/os/ParcelUuid;

    #@2
    return-object v0
.end method

.method static synthetic access$402(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;[Landroid/os/ParcelUuid;)[Landroid/os/ParcelUuid;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mUuids:[Landroid/os/ParcelUuid;

    #@2
    return-object p1
.end method

.method static synthetic access$500(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mName:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mName:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$600(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAlias:Ljava/lang/String;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-object p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAlias:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$700(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mBluetoothClass:I

    #@2
    return v0
.end method

.method static synthetic access$702(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mBluetoothClass:I

    #@2
    return p1
.end method

.method static synthetic access$802(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceType:I

    #@2
    return p1
.end method

.method static synthetic access$900(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;)S
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 111
    iget-short v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mRssi:S

    #@2
    return v0
.end method

.method static synthetic access$902(Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;S)S
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 111
    iput-short p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mRssi:S

    #@2
    return p1
.end method


# virtual methods
.method getAddress()[B
    .registers 3

    #@0
    .prologue
    .line 162
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 163
    :try_start_7
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAddress:[B

    #@9
    monitor-exit v1

    #@a
    return-object v0

    #@b
    .line 164
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method getAlias()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 190
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 191
    :try_start_7
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAlias:Ljava/lang/String;

    #@9
    monitor-exit v1

    #@a
    return-object v0

    #@b
    .line 192
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method getBluetoothClass()I
    .registers 3

    #@0
    .prologue
    .line 144
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 145
    :try_start_7
    iget v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mBluetoothClass:I

    #@9
    monitor-exit v1

    #@a
    return v0

    #@b
    .line 146
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method getBondState()I
    .registers 3

    #@0
    .prologue
    .line 234
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 235
    :try_start_7
    iget v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mBondState:I

    #@9
    monitor-exit v1

    #@a
    return v0

    #@b
    .line 236
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method getDeviceType()I
    .registers 3

    #@0
    .prologue
    .line 181
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 182
    :try_start_7
    iget v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceType:I

    #@9
    monitor-exit v1

    #@a
    return v0

    #@b
    .line 183
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method getJustWorks()Z
    .registers 5

    #@0
    .prologue
    .line 264
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 265
    :try_start_7
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "getJustWorks: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget-boolean v3, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mJustWorks:Z

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    #calls: Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V
    invoke-static {v0, v2}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$300(Lcom/android/bluetooth/btservice/RemoteDevices;Ljava/lang/String;)V

    #@21
    .line 266
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mJustWorks:Z

    #@23
    monitor-exit v1

    #@24
    return v0

    #@25
    .line 267
    :catchall_25
    move-exception v0

    #@26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_7 .. :try_end_27} :catchall_25

    #@27
    throw v0
.end method

.method getName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 135
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 136
    :try_start_7
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mName:Ljava/lang/String;

    #@9
    monitor-exit v1

    #@a
    return-object v0

    #@b
    .line 137
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method getRssi()S
    .registers 3

    #@0
    .prologue
    .line 171
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 172
    :try_start_7
    iget-short v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mRssi:S

    #@9
    monitor-exit v1

    #@a
    return v0

    #@b
    .line 173
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method getTrustState()Z
    .registers 5

    #@0
    .prologue
    .line 241
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 242
    :try_start_7
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@9
    new-instance v2, Ljava/lang/StringBuilder;

    #@b
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@e
    const-string v3, "getTrustState: "

    #@10
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@13
    move-result-object v2

    #@14
    iget-boolean v3, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceTrust:Z

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    #calls: Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V
    invoke-static {v0, v2}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$300(Lcom/android/bluetooth/btservice/RemoteDevices;Ljava/lang/String;)V

    #@21
    .line 243
    iget-boolean v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceTrust:Z

    #@23
    monitor-exit v1

    #@24
    return v0

    #@25
    .line 244
    :catchall_25
    move-exception v0

    #@26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_7 .. :try_end_27} :catchall_25

    #@27
    throw v0
.end method

.method getUuids()[Landroid/os/ParcelUuid;
    .registers 3

    #@0
    .prologue
    .line 153
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 154
    :try_start_7
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mUuids:[Landroid/os/ParcelUuid;

    #@9
    monitor-exit v1

    #@a
    return-object v0

    #@b
    .line 155
    :catchall_b
    move-exception v0

    #@c
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_7 .. :try_end_d} :catchall_b

    #@d
    throw v0
.end method

.method setAlias(Ljava/lang/String;)V
    .registers 7
    .parameter "alias"

    #@0
    .prologue
    .line 199
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isBRCMSolution()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_23

    #@6
    .line 200
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@8
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@b
    move-result-object v1

    #@c
    monitor-enter v1

    #@d
    .line 201
    :try_start_d
    iput-object p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAlias:Ljava/lang/String;

    #@f
    .line 202
    invoke-static {}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$200()Lcom/android/bluetooth/btservice/AdapterService;

    #@12
    move-result-object v0

    #@13
    iget-object v2, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAddress:[B

    #@15
    const/16 v3, 0xa

    #@17
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@1a
    move-result-object v4

    #@1b
    invoke-virtual {v0, v2, v3, v4}, Lcom/android/bluetooth/btservice/AdapterService;->setDevicePropertyNative([BI[B)Z

    #@1e
    .line 204
    monitor-exit v1

    #@1f
    .line 211
    :goto_1f
    return-void

    #@20
    .line 204
    :catchall_20
    move-exception v0

    #@21
    monitor-exit v1
    :try_end_22
    .catchall {:try_start_d .. :try_end_22} :catchall_20

    #@22
    throw v0

    #@23
    .line 206
    :cond_23
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@25
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@28
    move-result-object v1

    #@29
    monitor-enter v1

    #@2a
    .line 207
    :try_start_2a
    invoke-static {}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$200()Lcom/android/bluetooth/btservice/AdapterService;

    #@2d
    move-result-object v0

    #@2e
    iget-object v2, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAddress:[B

    #@30
    const/16 v3, 0xa

    #@32
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    #@35
    move-result-object v4

    #@36
    invoke-virtual {v0, v2, v3, v4}, Lcom/android/bluetooth/btservice/AdapterService;->setDevicePropertyNative([BI[B)Z

    #@39
    .line 209
    monitor-exit v1

    #@3a
    goto :goto_1f

    #@3b
    :catchall_3b
    move-exception v0

    #@3c
    monitor-exit v1
    :try_end_3d
    .catchall {:try_start_2a .. :try_end_3d} :catchall_3b

    #@3d
    throw v0
.end method

.method setBondState(I)V
    .registers 4
    .parameter "mBondState"

    #@0
    .prologue
    .line 217
    iget-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@2
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@5
    move-result-object v1

    #@6
    monitor-enter v1

    #@7
    .line 218
    :try_start_7
    iput p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mBondState:I

    #@9
    .line 219
    const/16 v0, 0xa

    #@b
    if-ne p1, v0, :cond_10

    #@d
    .line 225
    const/4 v0, 0x0

    #@e
    iput-object v0, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mUuids:[Landroid/os/ParcelUuid;

    #@10
    .line 227
    :cond_10
    monitor-exit v1

    #@11
    .line 228
    return-void

    #@12
    .line 227
    :catchall_12
    move-exception v0

    #@13
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_7 .. :try_end_14} :catchall_12

    #@14
    throw v0
.end method

.method setTrustState(Z)Z
    .registers 9
    .parameter "mTrustState"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 248
    iget-object v2, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@3
    #getter for: Lcom/android/bluetooth/btservice/RemoteDevices;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$100(Lcom/android/bluetooth/btservice/RemoteDevices;)Ljava/lang/Object;

    #@6
    move-result-object v2

    #@7
    monitor-enter v2

    #@8
    .line 249
    if-eqz p1, :cond_38

    #@a
    move v0, v1

    #@b
    .line 250
    .local v0, mTrustSave:I
    :goto_b
    :try_start_b
    iput-boolean p1, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceTrust:Z

    #@d
    .line 251
    invoke-static {}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$200()Lcom/android/bluetooth/btservice/AdapterService;

    #@10
    move-result-object v3

    #@11
    iget-object v4, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mAddress:[B

    #@13
    const/16 v5, 0xc

    #@15
    invoke-static {v0}, Lcom/android/bluetooth/Utils;->intToByteArray(I)[B

    #@18
    move-result-object v6

    #@19
    invoke-virtual {v3, v4, v5, v6}, Lcom/android/bluetooth/btservice/AdapterService;->setDevicePropertyNative([BI[B)Z

    #@1c
    .line 256
    monitor-exit v2
    :try_end_1d
    .catchall {:try_start_b .. :try_end_1d} :catchall_3a

    #@1d
    .line 257
    iget-object v2, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->this$0:Lcom/android/bluetooth/btservice/RemoteDevices;

    #@1f
    new-instance v3, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v4, "setTrustState: "

    #@26
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    iget-boolean v4, p0, Lcom/android/bluetooth/btservice/RemoteDevices$DeviceProperties;->mDeviceTrust:Z

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    #calls: Lcom/android/bluetooth/btservice/RemoteDevices;->debugLog(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/android/bluetooth/btservice/RemoteDevices;->access$300(Lcom/android/bluetooth/btservice/RemoteDevices;Ljava/lang/String;)V

    #@37
    .line 258
    return v1

    #@38
    .line 249
    .end local v0           #mTrustSave:I
    :cond_38
    const/4 v0, 0x0

    #@39
    goto :goto_b

    #@3a
    .line 256
    .restart local v0       #mTrustSave:I
    :catchall_3a
    move-exception v1

    #@3b
    :try_start_3b
    monitor-exit v2
    :try_end_3c
    .catchall {:try_start_3b .. :try_end_3c} :catchall_3a

    #@3c
    throw v1
.end method
