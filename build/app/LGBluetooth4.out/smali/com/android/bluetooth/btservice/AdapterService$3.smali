.class Lcom/android/bluetooth/btservice/AdapterService$3;
.super Ljava/lang/Thread;
.source "AdapterService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/bluetooth/btservice/AdapterService;->enable(Z)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/bluetooth/btservice/AdapterService;

.field final synthetic val$customText:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/android/bluetooth/btservice/AdapterService;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter

    #@0
    .prologue
    .line 1478
    iput-object p1, p0, Lcom/android/bluetooth/btservice/AdapterService$3;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@2
    iput-object p2, p0, Lcom/android/bluetooth/btservice/AdapterService$3;->val$customText:Ljava/lang/String;

    #@4
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    #@7
    return-void
.end method


# virtual methods
.method public run()V
    .registers 9

    #@0
    .prologue
    const v7, 0x104002e

    #@3
    .line 1480
    const v0, 0x1080078

    #@6
    .line 1481
    .local v0, icon:I
    new-instance v2, Landroid/app/Notification;

    #@8
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService$3;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@a
    invoke-virtual {v3}, Lcom/android/bluetooth/btservice/AdapterService;->getResources()Landroid/content/res/Resources;

    #@d
    move-result-object v3

    #@e
    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@11
    move-result-object v3

    #@12
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    #@15
    move-result-wide v4

    #@16
    invoke-direct {v2, v0, v3, v4, v5}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    #@19
    .line 1486
    .local v2, notification:Landroid/app/Notification;
    iget v3, v2, Landroid/app/Notification;->flags:I

    #@1b
    or-int/lit8 v3, v3, 0x10

    #@1d
    iput v3, v2, Landroid/app/Notification;->flags:I

    #@1f
    .line 1487
    iget-object v4, p0, Lcom/android/bluetooth/btservice/AdapterService$3;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@21
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService$3;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@23
    invoke-virtual {v3}, Lcom/android/bluetooth/btservice/AdapterService;->getResources()Landroid/content/res/Resources;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@2a
    move-result-object v5

    #@2b
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService$3;->val$customText:Ljava/lang/String;

    #@2d
    if-eqz v3, :cond_45

    #@2f
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService$3;->val$customText:Ljava/lang/String;

    #@31
    :goto_31
    const/4 v6, 0x0

    #@32
    invoke-virtual {v2, v4, v5, v3, v6}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    #@35
    .line 1491
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService$3;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@37
    const-string v4, "notification"

    #@39
    invoke-virtual {v3, v4}, Lcom/android/bluetooth/btservice/AdapterService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@3c
    move-result-object v1

    #@3d
    check-cast v1, Landroid/app/NotificationManager;

    #@3f
    .line 1493
    .local v1, nm:Landroid/app/NotificationManager;
    if-eqz v1, :cond_44

    #@41
    .line 1494
    invoke-virtual {v1, v7, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    #@44
    .line 1496
    :cond_44
    return-void

    #@45
    .line 1487
    .end local v1           #nm:Landroid/app/NotificationManager;
    :cond_45
    iget-object v3, p0, Lcom/android/bluetooth/btservice/AdapterService$3;->this$0:Lcom/android/bluetooth/btservice/AdapterService;

    #@47
    invoke-virtual {v3}, Lcom/android/bluetooth/btservice/AdapterService;->getResources()Landroid/content/res/Resources;

    #@4a
    move-result-object v3

    #@4b
    const v6, 0x1040026

    #@4e
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@51
    move-result-object v3

    #@52
    goto :goto_31
.end method
