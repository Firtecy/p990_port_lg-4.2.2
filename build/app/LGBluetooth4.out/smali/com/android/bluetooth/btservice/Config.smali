.class public Lcom/android/bluetooth/btservice/Config;
.super Ljava/lang/Object;
.source "Config.java"


# static fields
.field private static final PROFILE_SERVICES:[Ljava/lang/Class; = null

.field private static final PROFILE_SERVICES_FLAG:[I = null

.field private static SUPPORTED_PROFILES:[Ljava/lang/Class; = null

.field private static final TAG:Ljava/lang/String; = "AdapterServiceConfig"


# direct methods
.method static constructor <clinit>()V
    .registers 5

    #@0
    .prologue
    const/4 v4, 0x5

    #@1
    const/4 v3, 0x0

    #@2
    .line 40
    new-array v0, v4, [Ljava/lang/Class;

    #@4
    const-class v1, Lcom/android/bluetooth/hfp/HeadsetService;

    #@6
    aput-object v1, v0, v3

    #@8
    const/4 v1, 0x1

    #@9
    const-class v2, Lcom/android/bluetooth/a2dp/A2dpService;

    #@b
    aput-object v2, v0, v1

    #@d
    const/4 v1, 0x2

    #@e
    const-class v2, Lcom/android/bluetooth/hid/HidService;

    #@10
    aput-object v2, v0, v1

    #@12
    const/4 v1, 0x3

    #@13
    const-class v2, Lcom/android/bluetooth/hdp/HealthService;

    #@15
    aput-object v2, v0, v1

    #@17
    const/4 v1, 0x4

    #@18
    const-class v2, Lcom/android/bluetooth/pan/PanService;

    #@1a
    aput-object v2, v0, v1

    #@1c
    sput-object v0, Lcom/android/bluetooth/btservice/Config;->PROFILE_SERVICES:[Ljava/lang/Class;

    #@1e
    .line 50
    new-array v0, v4, [I

    #@20
    fill-array-data v0, :array_2a

    #@23
    sput-object v0, Lcom/android/bluetooth/btservice/Config;->PROFILE_SERVICES_FLAG:[I

    #@25
    .line 58
    new-array v0, v3, [Ljava/lang/Class;

    #@27
    sput-object v0, Lcom/android/bluetooth/btservice/Config;->SUPPORTED_PROFILES:[Ljava/lang/Class;

    #@29
    return-void

    #@2a
    .line 50
    :array_2a
    .array-data 0x4
        0x2t 0x0t 0x6t 0x7ft
        0x0t 0x0t 0x6t 0x7ft
        0x3t 0x0t 0x6t 0x7ft
        0x1t 0x0t 0x6t 0x7ft
        0x5t 0x0t 0x6t 0x7ft
    .end array-data
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 32
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static getSupportedProfiles()[Ljava/lang/Class;
    .registers 1

    #@0
    .prologue
    .line 82
    sget-object v0, Lcom/android/bluetooth/btservice/Config;->SUPPORTED_PROFILES:[Ljava/lang/Class;

    #@2
    return-object v0
.end method

.method static init(Landroid/content/Context;)V
    .registers 9
    .parameter "ctx"

    #@0
    .prologue
    .line 61
    if-nez p0, :cond_3

    #@2
    .line 79
    :cond_2
    :goto_2
    return-void

    #@3
    .line 64
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@6
    move-result-object v2

    #@7
    .line 65
    .local v2, resources:Landroid/content/res/Resources;
    if-eqz v2, :cond_2

    #@9
    .line 68
    new-instance v1, Ljava/util/ArrayList;

    #@b
    sget-object v5, Lcom/android/bluetooth/btservice/Config;->PROFILE_SERVICES:[Ljava/lang/Class;

    #@d
    array-length v5, v5

    #@e
    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    #@11
    .line 69
    .local v1, profiles:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Ljava/lang/Class;>;"
    const/4 v0, 0x0

    #@12
    .local v0, i:I
    :goto_12
    sget-object v5, Lcom/android/bluetooth/btservice/Config;->PROFILE_SERVICES_FLAG:[I

    #@14
    array-length v5, v5

    #@15
    if-ge v0, v5, :cond_4b

    #@17
    .line 70
    sget-object v5, Lcom/android/bluetooth/btservice/Config;->PROFILE_SERVICES_FLAG:[I

    #@19
    aget v5, v5, v0

    #@1b
    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@1e
    move-result v3

    #@1f
    .line 71
    .local v3, supported:Z
    if-eqz v3, :cond_48

    #@21
    .line 72
    const-string v5, "AdapterServiceConfig"

    #@23
    new-instance v6, Ljava/lang/StringBuilder;

    #@25
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@28
    const-string v7, "Adding "

    #@2a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v6

    #@2e
    sget-object v7, Lcom/android/bluetooth/btservice/Config;->PROFILE_SERVICES:[Ljava/lang/Class;

    #@30
    aget-object v7, v7, v0

    #@32
    invoke-virtual {v7}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    #@35
    move-result-object v7

    #@36
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v6

    #@3a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v6

    #@3e
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 73
    sget-object v5, Lcom/android/bluetooth/btservice/Config;->PROFILE_SERVICES:[Ljava/lang/Class;

    #@43
    aget-object v5, v5, v0

    #@45
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    #@48
    .line 69
    :cond_48
    add-int/lit8 v0, v0, 0x1

    #@4a
    goto :goto_12

    #@4b
    .line 76
    .end local v3           #supported:Z
    :cond_4b
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    #@4e
    move-result v4

    #@4f
    .line 77
    .local v4, totalProfiles:I
    new-array v5, v4, [Ljava/lang/Class;

    #@51
    sput-object v5, Lcom/android/bluetooth/btservice/Config;->SUPPORTED_PROFILES:[Ljava/lang/Class;

    #@53
    .line 78
    sget-object v5, Lcom/android/bluetooth/btservice/Config;->SUPPORTED_PROFILES:[Ljava/lang/Class;

    #@55
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    #@58
    goto :goto_2
.end method
