.class public final Lcom/android/bluetooth/Manifest$permission;
.super Ljava/lang/Object;
.source "Manifest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/Manifest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "permission"
.end annotation


# static fields
.field public static final ACCESS_BLUETOOTH_SHARE:Ljava/lang/String; = "android.permission.ACCESS_BLUETOOTH_SHARE"

.field public static final ACCESS_FM_RECEIVER:Ljava/lang/String; = "android.permission.ACCESS_FM_RECEIVER"

.field public static final BLUETOOTH_MAP:Ljava/lang/String; = "broadcom.permission.BLUETOOTH_MAP"

.field public static final HANDOVER_STATUS:Ljava/lang/String; = "com.android.permission.HANDOVER_STATUS"

.field public static final WHITELIST_BLUETOOTH_DEVICE:Ljava/lang/String; = "com.android.permission.WHITELIST_BLUETOOTH_DEVICE"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 11
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
