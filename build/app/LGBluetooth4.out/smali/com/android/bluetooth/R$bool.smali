.class public final Lcom/android/bluetooth/R$bool;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/bluetooth/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "bool"
.end annotation


# static fields
.field public static final mse_datasource_supported_mms:I = 0x7f060021

.field public static final mse_datasource_supported_sms:I = 0x7f060020

.field public static final pbap_include_photos_in_vcard:I = 0x7f060007

.field public static final pbap_use_profile_for_owner_vcard:I = 0x7f060008

.field public static final profile_configurable_a2dp:I = 0x7f06000d

.field public static final profile_configurable_ftp_server:I = 0x7f060016

.field public static final profile_configurable_gatt:I = 0x7f060019

.field public static final profile_configurable_hdp:I = 0x7f060011

.field public static final profile_configurable_hid:I = 0x7f06000f

.field public static final profile_configurable_hs_hfp:I = 0x7f06000b

.field public static final profile_configurable_mse:I = 0x7f06001f

.field public static final profile_configurable_pan:I = 0x7f060013

.field public static final profile_configurable_sap:I = 0x7f06001c

.field public static final profile_default_start_a2dp:I = 0x7f06000c

.field public static final profile_default_start_ftp_server:I = 0x7f060015

.field public static final profile_default_start_gatt:I = 0x7f060018

.field public static final profile_default_start_hdp:I = 0x7f060010

.field public static final profile_default_start_hid:I = 0x7f06000e

.field public static final profile_default_start_hs_hfp:I = 0x7f06000a

.field public static final profile_default_start_mse:I = 0x7f06001e

.field public static final profile_default_start_pan:I = 0x7f060012

.field public static final profile_default_start_sap:I = 0x7f06001b

.field public static final profile_supported_a2dp:I = 0x7f060000

.field public static final profile_supported_ftp_server:I = 0x7f060014

.field public static final profile_supported_gatt:I = 0x7f060017

.field public static final profile_supported_hdp:I = 0x7f060001

.field public static final profile_supported_hid:I = 0x7f060003

.field public static final profile_supported_hs_hfp:I = 0x7f060002

.field public static final profile_supported_mse:I = 0x7f06001d

.field public static final profile_supported_opp:I = 0x7f060004

.field public static final profile_supported_pan:I = 0x7f060005

.field public static final profile_supported_pbap:I = 0x7f060006

.field public static final profile_supported_sap:I = 0x7f06001a

.field public static final supports_advanced_settings:I = 0x7f060009


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 22
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method
