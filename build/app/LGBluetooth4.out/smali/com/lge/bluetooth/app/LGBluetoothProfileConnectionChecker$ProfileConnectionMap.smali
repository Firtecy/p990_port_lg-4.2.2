.class Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;
.super Ljava/util/HashMap;
.source "LGBluetoothProfileConnectionChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ProfileConnectionMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/util/HashMap",
        "<TT;TV;>;"
    }
.end annotation


# instance fields
.field mProfileName:Ljava/lang/String;

.field final synthetic this$0:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;


# direct methods
.method public constructor <init>(Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter "profileName"

    #@0
    .prologue
    .line 62
    .local p0, this:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;,"Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap<TT;TV;>;"
    iput-object p1, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->this$0:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@2
    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    #@5
    .line 63
    iput-object p2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->mProfileName:Ljava/lang/String;

    #@7
    .line 64
    return-void
.end method


# virtual methods
.method public getDeviceName()Ljava/lang/Object;
    .registers 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    #@0
    .prologue
    .line 77
    .local p0, this:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;,"Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap<TT;TV;>;"
    invoke-virtual {p0}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->keySet()Ljava/util/Set;

    #@3
    move-result-object v2

    #@4
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@7
    move-result-object v0

    #@8
    .line 78
    .local v0, i:Ljava/util/Iterator;,"Ljava/util/Iterator<TT;>;"
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@b
    move-result v2

    #@c
    if-eqz v2, :cond_35

    #@e
    .line 79
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@11
    move-result-object v1

    #@12
    .line 80
    .local v1, t:Ljava/lang/Object;,"TT;"
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->this$0:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@14
    new-instance v3, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    iget-object v4, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->mProfileName:Ljava/lang/String;

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    const-string v4, " getDeviceName() - address : "

    #@21
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v3

    #@25
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v3

    #@2d
    invoke-virtual {v2, v3}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->debug(Ljava/lang/String;)V

    #@30
    .line 81
    invoke-virtual {p0, v1}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@33
    move-result-object v2

    #@34
    .line 84
    .end local v1           #t:Ljava/lang/Object;,"TT;"
    :goto_34
    return-object v2

    #@35
    .line 83
    :cond_35
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->this$0:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@37
    new-instance v3, Ljava/lang/StringBuilder;

    #@39
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3c
    iget-object v4, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->mProfileName:Ljava/lang/String;

    #@3e
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@41
    move-result-object v3

    #@42
    const-string v4, " no device on hashMap!!"

    #@44
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    invoke-virtual {v2, v3}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->error(Ljava/lang/String;)V

    #@4f
    .line 84
    const/4 v2, 0x0

    #@50
    goto :goto_34
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TV;)TV;"
        }
    .end annotation

    #@0
    .prologue
    .line 67
    .local p0, this:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;,"Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap<TT;TV;>;"
    .local p1, t:Ljava/lang/Object;,"TT;"
    .local p2, v:Ljava/lang/Object;,"TV;"
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->this$0:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->mProfileName:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, " connected, address : "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ", name : "

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v0, v1}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->debug(Ljava/lang/String;)V

    #@28
    .line 68
    invoke-super {p0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@2b
    move-result-object v0

    #@2c
    return-object v0
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .registers 5
    .parameter "v"
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    #@0
    .prologue
    .line 72
    .local p0, this:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;,"Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap<TT;TV;>;"
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->this$0:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->mProfileName:Ljava/lang/String;

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    const-string v2, " is disconnected : "

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v0, v1}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->debug(Ljava/lang/String;)V

    #@1e
    .line 73
    invoke-super {p0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@21
    move-result-object v0

    #@22
    return-object v0
.end method
