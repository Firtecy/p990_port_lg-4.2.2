.class public Lcom/lge/bluetooth/app/LGSmartShareOppEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "LGSmartShareOppEventReceiver.java"


# static fields
.field private static final DBG:Z = true

.field public static final SEND_VIA_SMARTSHARE:Ljava/lang/String; = "com.lge.smartshare.connect_wizard.SEND_VIA_SMARTSHARE"

.field private static final TAG:Ljava/lang/String; = "LGSmartShareOppEventReceiver"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 13
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 11
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 22
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@4
    move-result-object v0

    #@5
    .line 23
    .local v0, action:Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@8
    move-result-object v2

    #@9
    .line 24
    .local v2, mimeType:Ljava/lang/String;
    const-string v4, "android.intent.extra.STREAM"

    #@b
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    #@e
    move-result-object v3

    #@f
    .line 25
    .local v3, uriList:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v4, "android.bluetooth.device.extra.DEVICE"

    #@11
    invoke-virtual {p2, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@14
    move-result-object v1

    #@15
    check-cast v1, Landroid/bluetooth/BluetoothDevice;

    #@17
    .line 27
    .local v1, mOppDev:Landroid/bluetooth/BluetoothDevice;
    const-string v4, "LGSmartShareOppEventReceiver"

    #@19
    new-instance v5, Ljava/lang/StringBuilder;

    #@1b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@1e
    const-string v6, "[BTUI] onReceive: action = "

    #@20
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@23
    move-result-object v5

    #@24
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27
    move-result-object v5

    #@28
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2b
    move-result-object v5

    #@2c
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2f
    .line 28
    if-eqz v3, :cond_39

    #@31
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@34
    move-result v4

    #@35
    if-eqz v4, :cond_39

    #@37
    if-nez v2, :cond_41

    #@39
    .line 30
    :cond_39
    const-string v4, "LGSmartShareOppEventReceiver"

    #@3b
    const-string v5, "[BTUI] Unable to process send multiple request. uriList or mimeType invalid"

    #@3d
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@40
    .line 48
    :cond_40
    :goto_40
    return-void

    #@41
    .line 36
    :cond_41
    const-string v4, "LGSmartShareOppEventReceiver"

    #@43
    new-instance v5, Ljava/lang/StringBuilder;

    #@45
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v6, "[BTUI] onReceive: mimeType = "

    #@4a
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v5

    #@4e
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v5

    #@52
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@55
    move-result-object v5

    #@56
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    .line 37
    const-string v4, "LGSmartShareOppEventReceiver"

    #@5b
    new-instance v5, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v6, "[BTUI] onReceive: uriList = "

    #@62
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v5

    #@66
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v5

    #@6a
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v5

    #@6e
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 40
    const-string v4, "com.lge.smartshare.connect_wizard.SEND_VIA_SMARTSHARE"

    #@73
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@76
    move-result v4

    #@77
    if-eqz v4, :cond_40

    #@79
    .line 41
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    #@7c
    move-result v4

    #@7d
    const/4 v5, 0x1

    #@7e
    if-ne v4, v5, :cond_99

    #@80
    .line 42
    invoke-static {p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@83
    move-result-object v5

    #@84
    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@87
    move-result-object v4

    #@88
    check-cast v4, Landroid/net/Uri;

    #@8a
    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@8d
    move-result-object v4

    #@8e
    invoke-virtual {v5, v2, v4, v7}, Lcom/android/bluetooth/opp/BluetoothOppManager;->saveSendingFileInfo(Ljava/lang/String;Ljava/lang/String;Z)V

    #@91
    .line 46
    :goto_91
    invoke-static {p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@94
    move-result-object v4

    #@95
    invoke-virtual {v4, v1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->startTransfer(Landroid/bluetooth/BluetoothDevice;)V

    #@98
    goto :goto_40

    #@99
    .line 44
    :cond_99
    invoke-static {p1}, Lcom/android/bluetooth/opp/BluetoothOppManager;->getInstance(Landroid/content/Context;)Lcom/android/bluetooth/opp/BluetoothOppManager;

    #@9c
    move-result-object v4

    #@9d
    invoke-virtual {v4, v2, v3, v7}, Lcom/android/bluetooth/opp/BluetoothOppManager;->saveSendingFileInfo(Ljava/lang/String;Ljava/util/ArrayList;Z)V

    #@a0
    goto :goto_91
.end method
