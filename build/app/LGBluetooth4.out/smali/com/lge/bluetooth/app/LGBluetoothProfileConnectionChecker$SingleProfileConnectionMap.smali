.class Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;
.super Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;
.source "LGBluetoothProfileConnectionChecker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SingleProfileConnectionMap"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap",
        "<TT;TV;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;


# direct methods
.method public constructor <init>(Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter "profileName"

    #@0
    .prologue
    .line 89
    .local p0, this:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;,"Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap<TT;TV;>;"
    iput-object p1, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;->this$0:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@2
    .line 90
    invoke-direct {p0, p1, p2}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;-><init>(Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;Ljava/lang/String;)V

    #@5
    .line 91
    return-void
.end method


# virtual methods
.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .registers 6
    .parameter
    .parameter
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TV;)TV;"
        }
    .end annotation

    #@0
    .prologue
    .line 94
    .local p0, this:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;,"Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap<TT;TV;>;"
    .local p1, t:Ljava/lang/Object;,"TT;"
    .local p2, v:Ljava/lang/Object;,"TV;"
    invoke-virtual {p0}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;->size()I

    #@3
    move-result v0

    #@4
    if-lez v0, :cond_32

    #@6
    .line 95
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;->this$0:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@8
    new-instance v1, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->mProfileName:Ljava/lang/String;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, " is already connected!! check why "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->mProfileName:Ljava/lang/String;

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v2, " is connected with "

    #@21
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v1

    #@29
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2c
    move-result-object v1

    #@2d
    invoke-virtual {v0, v1}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->error(Ljava/lang/String;)V

    #@30
    .line 96
    const/4 v0, 0x0

    #@31
    .line 98
    :goto_31
    return-object v0

    #@32
    :cond_32
    invoke-super {p0, p1, p2}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@35
    move-result-object v0

    #@36
    goto :goto_31
.end method
