.class public final Lcom/lge/bluetooth/app/LGBluetoothOppUtils;
.super Ljava/lang/Object;
.source "LGBluetoothOppUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "LGBluetoothOppUtils"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method static getFileNamefromFilePath(Ljava/lang/String;)Ljava/lang/String;
    .registers 5
    .parameter "filePath"

    #@0
    .prologue
    .line 132
    if-nez p0, :cond_b

    #@2
    .line 133
    const-string v2, "LGBluetoothOppUtils"

    #@4
    const-string v3, "[BTUI] FilePath is null!!"

    #@6
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 134
    const/4 v0, 0x0

    #@a
    .line 138
    :goto_a
    return-object v0

    #@b
    .line 136
    :cond_b
    const-string v2, "/"

    #@d
    invoke-virtual {p0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@10
    move-result v2

    #@11
    add-int/lit8 v1, v2, 0x1

    #@13
    .line 137
    .local v1, iFileName:I
    invoke-virtual {p0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@16
    move-result-object v0

    #@17
    .line 138
    .local v0, fileName:Ljava/lang/String;
    goto :goto_a
.end method

.method static getFilePathfromUri(Landroid/net/Uri;)Ljava/lang/String;
    .registers 6
    .parameter "uri"

    #@0
    .prologue
    .line 116
    new-instance v2, Ljava/lang/String;

    #@2
    const-string v3, "file:///"

    #@4
    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    #@7
    .line 118
    .local v2, startStr:Ljava/lang/String;
    if-nez p0, :cond_12

    #@9
    .line 119
    const-string v3, "LGBluetoothOppUtils"

    #@b
    const-string v4, "[BTUI] Uri is null!!"

    #@d
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@10
    .line 120
    const/4 v0, 0x0

    #@11
    .line 125
    :goto_11
    return-object v0

    #@12
    .line 123
    :cond_12
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@15
    move-result-object v3

    #@16
    invoke-virtual {v3, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    #@19
    move-result v3

    #@1a
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    #@1d
    move-result v4

    #@1e
    add-int v1, v3, v4

    #@20
    .line 124
    .local v1, iFilePath:I
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@23
    move-result-object v3

    #@24
    invoke-virtual {v3, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .line 125
    .local v0, filePath:Ljava/lang/String;
    goto :goto_11
.end method

.method static isDrmFile(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Boolean;
    .registers 10
    .parameter "filePath"
    .parameter "cContext"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 142
    if-nez p0, :cond_c

    #@3
    .line 143
    const-string v4, "LGBluetoothOppUtils"

    #@5
    const-string v5, "[BTUI] FilePath is null!!"

    #@7
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@a
    .line 144
    const/4 v4, 0x0

    #@b
    .line 185
    :goto_b
    return-object v4

    #@c
    .line 148
    :cond_c
    invoke-static {p0}, Lcom/lge/lgdrm/DrmManager;->isDRM(Ljava/lang/String;)I

    #@f
    move-result v4

    #@10
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@13
    move-result-object v1

    #@14
    .line 149
    .local v1, drmTypeTemp:Ljava/lang/Integer;
    const/4 v0, 0x0

    #@15
    .line 150
    .local v0, drmType:I
    if-nez v1, :cond_23

    #@17
    .line 151
    const-string v4, "LGBluetoothOppUtils"

    #@19
    const-string v5, "[BTUI] drmTypeTemp is null"

    #@1b
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 152
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@21
    move-result-object v4

    #@22
    goto :goto_b

    #@23
    .line 154
    :cond_23
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    #@26
    move-result v0

    #@27
    .line 161
    const/16 v4, 0x10

    #@29
    if-lt v0, v4, :cond_5a

    #@2b
    const/16 v4, 0x3000

    #@2d
    if-gt v0, v4, :cond_5a

    #@2f
    .line 163
    :try_start_2f
    const-string v4, "LGBluetoothOppUtils"

    #@31
    new-instance v5, Ljava/lang/StringBuilder;

    #@33
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@36
    const-string v6, "[BTUI] "

    #@38
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b
    move-result-object v5

    #@3c
    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3f
    move-result-object v5

    #@40
    const-string v6, "is a DRM file"

    #@42
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@45
    move-result-object v5

    #@46
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@49
    move-result-object v5

    #@4a
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@4d
    .line 164
    invoke-static {p0, p1}, Lcom/lge/lgdrm/DrmManager;->createContentSession(Ljava/lang/String;Landroid/content/Context;)Lcom/lge/lgdrm/DrmContentSession;

    #@50
    move-result-object v3

    #@51
    .line 166
    .local v3, mDrmContentSession:Lcom/lge/lgdrm/DrmContentSession;
    if-nez v3, :cond_5f

    #@53
    .line 167
    const-string v4, "LGBluetoothOppUtils"

    #@55
    const-string v5, "[BTUI] isDrmFile() : mDrmContentSession is null"

    #@57
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5a
    .catch Ljava/lang/NullPointerException; {:try_start_2f .. :try_end_5a} :catch_6c
    .catch Lcom/lge/lgdrm/DrmException; {:try_start_2f .. :try_end_5a} :catch_86

    #@5a
    .line 185
    .end local v3           #mDrmContentSession:Lcom/lge/lgdrm/DrmContentSession;
    :cond_5a
    :goto_5a
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    #@5d
    move-result-object v4

    #@5e
    goto :goto_b

    #@5f
    .line 169
    .restart local v3       #mDrmContentSession:Lcom/lge/lgdrm/DrmContentSession;
    :cond_5f
    const/4 v4, 0x1

    #@60
    :try_start_60
    invoke-virtual {v3, v4}, Lcom/lge/lgdrm/DrmContentSession;->isActionSupported(I)Z

    #@63
    move-result v4

    #@64
    if-nez v4, :cond_5a

    #@66
    .line 170
    const/4 v4, 0x1

    #@67
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_6a
    .catch Ljava/lang/NullPointerException; {:try_start_60 .. :try_end_6a} :catch_6c
    .catch Lcom/lge/lgdrm/DrmException; {:try_start_60 .. :try_end_6a} :catch_86

    #@6a
    move-result-object v4

    #@6b
    goto :goto_b

    #@6c
    .line 179
    .end local v3           #mDrmContentSession:Lcom/lge/lgdrm/DrmContentSession;
    :catch_6c
    move-exception v2

    #@6d
    .line 180
    .local v2, e:Ljava/lang/NullPointerException;
    const-string v4, "LGBluetoothOppUtils"

    #@6f
    new-instance v5, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v6, "[BTUI] Exception occurred : "

    #@76
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v5

    #@7a
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@7d
    move-result-object v5

    #@7e
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@81
    move-result-object v5

    #@82
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@85
    goto :goto_5a

    #@86
    .line 181
    .end local v2           #e:Ljava/lang/NullPointerException;
    :catch_86
    move-exception v2

    #@87
    .line 182
    .local v2, e:Lcom/lge/lgdrm/DrmException;
    const-string v4, "LGBluetoothOppUtils"

    #@89
    new-instance v5, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v6, "[BTUI] Exception occurred : "

    #@90
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v5

    #@94
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v5

    #@98
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v5

    #@9c
    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    goto :goto_5a
.end method

.method public static isDrmFileCheck(Landroid/content/Intent;ZLandroid/content/Context;)Z
    .registers 14
    .parameter "intent"
    .parameter "isMultiSend"
    .parameter "mContext"

    #@0
    .prologue
    const/4 v9, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 62
    const-string v10, "LGBT_COMMON_SCENARIO_BLOCK_SENDING_DRM_FILES"

    #@4
    invoke-static {v10}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isServiceSupported(Ljava/lang/String;)Z

    #@7
    move-result v10

    #@8
    if-nez v10, :cond_12

    #@a
    .line 63
    const-string v9, "LGBluetoothOppUtils"

    #@c
    const-string v10, "[BTUI] Do not check DRM FILE"

    #@e
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 104
    :cond_11
    :goto_11
    return v8

    #@12
    .line 66
    :cond_12
    const/4 v1, 0x0

    #@13
    .line 67
    .local v1, filePath:Ljava/lang/String;
    const/4 v0, 0x0

    #@14
    .line 69
    .local v0, fileName:Ljava/lang/String;
    if-nez p1, :cond_44

    #@16
    .line 70
    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@19
    move-result-object v5

    #@1a
    .line 71
    .local v5, type:Ljava/lang/String;
    const-string v10, "android.intent.extra.STREAM"

    #@1c
    invoke-virtual {p0, v10}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    #@1f
    move-result-object v4

    #@20
    check-cast v4, Landroid/net/Uri;

    #@22
    .line 72
    .local v4, stream:Landroid/net/Uri;
    if-eqz v4, :cond_11

    #@24
    if-eqz v5, :cond_11

    #@26
    .line 73
    invoke-static {v4}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->getFilePathfromUri(Landroid/net/Uri;)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    .line 74
    invoke-static {v1}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->getFileNamefromFilePath(Ljava/lang/String;)Ljava/lang/String;

    #@2d
    move-result-object v0

    #@2e
    .line 75
    invoke-static {v1, p2}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->isDrmFile(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Boolean;

    #@31
    move-result-object v10

    #@32
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    #@35
    move-result v10

    #@36
    if-eqz v10, :cond_11

    #@38
    .line 76
    const-string v8, "LGBluetoothOppUtils"

    #@3a
    const-string v10, "[BTUI]This file is a DRM file"

    #@3c
    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    .line 77
    invoke-static {v0, p2}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->startDrmDialog(Ljava/lang/String;Landroid/content/Context;)V

    #@42
    move v8, v9

    #@43
    .line 78
    goto :goto_11

    #@44
    .line 82
    .end local v4           #stream:Landroid/net/Uri;
    .end local v5           #type:Ljava/lang/String;
    :cond_44
    new-instance v7, Ljava/util/ArrayList;

    #@46
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    #@49
    .line 83
    .local v7, uris:Ljava/util/ArrayList;,"Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {p0}, Landroid/content/Intent;->getType()Ljava/lang/String;

    #@4c
    move-result-object v3

    #@4d
    .line 84
    .local v3, mimeType:Ljava/lang/String;
    const-string v10, "android.intent.extra.STREAM"

    #@4f
    invoke-virtual {p0, v10}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    #@52
    move-result-object v7

    #@53
    .line 86
    if-nez v7, :cond_5d

    #@55
    .line 87
    const-string v9, "LGBluetoothOppUtils"

    #@57
    const-string v10, "[BTUI]isDrmFileCheck() uris is null"

    #@59
    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5c
    goto :goto_11

    #@5d
    .line 92
    :cond_5d
    const/4 v2, 0x0

    #@5e
    .local v2, k:I
    :goto_5e
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    #@61
    move-result v10

    #@62
    if-ge v2, v10, :cond_11

    #@64
    .line 93
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    #@67
    move-result-object v6

    #@68
    check-cast v6, Landroid/net/Uri;

    #@6a
    .line 94
    .local v6, uri:Landroid/net/Uri;
    invoke-static {v6}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->getFilePathfromUri(Landroid/net/Uri;)Ljava/lang/String;

    #@6d
    move-result-object v1

    #@6e
    .line 95
    invoke-static {v1}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->getFileNamefromFilePath(Ljava/lang/String;)Ljava/lang/String;

    #@71
    move-result-object v0

    #@72
    .line 96
    invoke-static {v1, p2}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->isDrmFile(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/Boolean;

    #@75
    move-result-object v10

    #@76
    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    #@79
    move-result v10

    #@7a
    if-eqz v10, :cond_88

    #@7c
    .line 97
    const-string v8, "LGBluetoothOppUtils"

    #@7e
    const-string v10, "[BTUI] Not able to send the files because one or more DRM files are included!"

    #@80
    invoke-static {v8, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@83
    .line 98
    invoke-static {v0, p2}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->startDrmDialog(Ljava/lang/String;Landroid/content/Context;)V

    #@86
    move v8, v9

    #@87
    .line 99
    goto :goto_11

    #@88
    .line 92
    :cond_88
    add-int/lit8 v2, v2, 0x1

    #@8a
    goto :goto_5e
.end method

.method public static makeHtmlCodeToString(Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "uri"

    #@0
    .prologue
    .line 32
    const-string v2, "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"/></head><body>\n"

    #@2
    .line 33
    .local v2, header:Ljava/lang/String;
    const-string v1, "</p></body></html>"

    #@4
    .line 34
    .local v1, footer:Ljava/lang/String;
    const-string v0, ""

    #@6
    .line 36
    .local v0, body:Ljava/lang/String;
    const-string v6, "\n"

    #@8
    invoke-virtual {p0, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    #@b
    move-result-object v5

    #@c
    .line 37
    .local v5, uriList:[Ljava/lang/String;
    const/4 v3, 0x0

    #@d
    .local v3, i:I
    :goto_d
    array-length v6, v5

    #@e
    if-ge v3, v6, :cond_62

    #@10
    .line 38
    const-string v6, "LGBluetoothOppUtils"

    #@12
    new-instance v7, Ljava/lang/StringBuilder;

    #@14
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@17
    const-string v8, "URI = "

    #@19
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v7

    #@1d
    aget-object v8, v5, v3

    #@1f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v7

    #@23
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v7

    #@27
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 39
    aget-object v6, v5, v3

    #@2c
    invoke-static {v6}, Lcom/lge/bluetooth/app/LGBluetoothOppUtils;->parseByHttp(Ljava/lang/String;)Ljava/lang/String;

    #@2f
    move-result-object v4

    #@30
    .line 40
    .local v4, str:Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    #@32
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@35
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v6

    #@3d
    const-string v7, "\n"

    #@3f
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v6

    #@43
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v0

    #@47
    .line 41
    const-string v6, "LGBluetoothOppUtils"

    #@49
    new-instance v7, Ljava/lang/StringBuilder;

    #@4b
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@4e
    const-string v8, "body : "

    #@50
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v7

    #@54
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v7

    #@58
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5b
    move-result-object v7

    #@5c
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5f
    .line 37
    add-int/lit8 v3, v3, 0x1

    #@61
    goto :goto_d

    #@62
    .line 44
    .end local v4           #str:Ljava/lang/String;
    :cond_62
    new-instance v6, Ljava/lang/StringBuilder;

    #@64
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@67
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6a
    move-result-object v6

    #@6b
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6e
    move-result-object v6

    #@6f
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v6

    #@73
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v6

    #@77
    return-object v6
.end method

.method static parseByHttp(Ljava/lang/String;)Ljava/lang/String;
    .registers 6
    .parameter "s"

    #@0
    .prologue
    .line 48
    invoke-virtual {p0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    #@3
    move-result-object v1

    #@4
    .line 49
    .local v1, result:Ljava/lang/String;
    const-string v2, "http"

    #@6
    invoke-virtual {v1, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    #@9
    move-result v0

    #@a
    .line 50
    .local v0, offsetURL:I
    if-nez v0, :cond_38

    #@c
    .line 51
    new-instance v2, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v3, "<a href=\""

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@1a
    move-result-object v3

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    const-string v3, "\">"

    #@21
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v2

    #@25
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@28
    move-result-object v3

    #@29
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    const-string v3, "</a>"

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v2

    #@33
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@36
    move-result-object p0

    #@37
    .line 55
    .end local p0
    :cond_37
    :goto_37
    return-object p0

    #@38
    .line 52
    .restart local p0
    :cond_38
    if-lez v0, :cond_37

    #@3a
    .line 53
    new-instance v2, Ljava/lang/StringBuilder;

    #@3c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@3f
    const/4 v3, 0x0

    #@40
    add-int/lit8 v4, v0, -0x1

    #@42
    invoke-virtual {p0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v2

    #@4a
    const-string v3, "<a href=\""

    #@4c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v2

    #@50
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@53
    move-result-object v3

    #@54
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v2

    #@58
    const-string v3, "\">"

    #@5a
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v2

    #@5e
    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    #@61
    move-result-object v3

    #@62
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    const-string v3, "</a>"

    #@68
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6b
    move-result-object v2

    #@6c
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6f
    move-result-object p0

    #@70
    goto :goto_37
.end method

.method static startDrmDialog(Ljava/lang/String;Landroid/content/Context;)V
    .registers 4
    .parameter "fileName"
    .parameter "alertContext"

    #@0
    .prologue
    .line 108
    new-instance v0, Landroid/content/Intent;

    #@2
    const-class v1, Lcom/lge/bluetooth/app/LGBluetoothOppDrmDialog;

    #@4
    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    #@7
    .line 109
    .local v0, drmIntent:Landroid/content/Intent;
    const/high16 v1, 0x1000

    #@9
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@c
    .line 110
    const-string v1, "fileName"

    #@e
    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@11
    .line 111
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@14
    .line 112
    return-void
.end method
