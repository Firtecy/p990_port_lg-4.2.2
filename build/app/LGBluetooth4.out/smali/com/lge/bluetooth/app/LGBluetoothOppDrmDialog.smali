.class public Lcom/lge/bluetooth/app/LGBluetoothOppDrmDialog;
.super Lcom/android/internal/app/AlertActivity;
.source "LGBluetoothOppDrmDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 21
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    #@3
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 3
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/lge/bluetooth/app/LGBluetoothOppDrmDialog;->dismiss()V

    #@3
    .line 39
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 9
    .parameter "saveedInstanceState"

    #@0
    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 25
    iget-object v3, p0, Lcom/lge/bluetooth/app/LGBluetoothOppDrmDialog;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    #@5
    .line 26
    .local v3, p:Lcom/android/internal/app/AlertController$AlertParams;
    invoke-virtual {p0}, Lcom/lge/bluetooth/app/LGBluetoothOppDrmDialog;->getIntent()Landroid/content/Intent;

    #@8
    move-result-object v0

    #@9
    .line 27
    .local v0, intent:Landroid/content/Intent;
    const v2, 0x7f070070

    #@c
    .line 28
    .local v2, messageResId:I
    const/4 v4, 0x1

    #@d
    new-array v4, v4, [Ljava/lang/Object;

    #@f
    const/4 v5, 0x0

    #@10
    const-string v6, "fileName"

    #@12
    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v6

    #@16
    aput-object v6, v4, v5

    #@18
    invoke-virtual {p0, v2, v4}, Lcom/lge/bluetooth/app/LGBluetoothOppDrmDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    .line 29
    .local v1, message:Ljava/lang/String;
    iput-object v1, v3, Lcom/android/internal/app/AlertController$AlertParams;->mMessage:Ljava/lang/CharSequence;

    #@1e
    .line 31
    const v4, 0x7f070038

    #@21
    invoke-virtual {p0, v4}, Lcom/lge/bluetooth/app/LGBluetoothOppDrmDialog;->getString(I)Ljava/lang/String;

    #@24
    move-result-object v4

    #@25
    iput-object v4, v3, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonText:Ljava/lang/CharSequence;

    #@27
    .line 32
    iput-object p0, v3, Lcom/android/internal/app/AlertController$AlertParams;->mNegativeButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@29
    .line 33
    invoke-virtual {p0}, Lcom/lge/bluetooth/app/LGBluetoothOppDrmDialog;->setupAlert()V

    #@2c
    .line 34
    return-void
.end method
