.class public Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;
.super Ljava/lang/Object;
.source "LGBluetoothWifiCoexChecker.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# static fields
.field public static final BLOCKING_TIME:I = 0x2710

.field public static final BT_WIFI_COEX_CHECK:I = 0x1

.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String; = "LGBluetoothWifiCoexChecker"

.field public static final UNBLOCK_COEX:I = 0x2


# instance fields
.field private mBlocked:Z

.field mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 28
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 22
    const/4 v0, 0x0

    #@4
    iput-boolean v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mBlocked:Z

    #@6
    .line 71
    new-instance v0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker$1;

    #@8
    invoke-direct {v0, p0}, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker$1;-><init>(Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;)V

    #@b
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mHandler:Landroid/os/Handler;

    #@d
    .line 29
    iput-object p1, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mContext:Landroid/content/Context;

    #@f
    .line 30
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mContext:Landroid/content/Context;

    #@11
    const-string v1, "wifip2p"

    #@13
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@16
    move-result-object v0

    #@17
    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    #@19
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@1b
    .line 31
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@1d
    if-eqz v0, :cond_30

    #@1f
    .line 32
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@21
    iget-object v1, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mContext:Landroid/content/Context;

    #@23
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mContext:Landroid/content/Context;

    #@25
    invoke-virtual {v2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    #@28
    move-result-object v2

    #@29
    const/4 v3, 0x0

    #@2a
    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@2d
    move-result-object v0

    #@2e
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@30
    .line 34
    :cond_30
    return-void
.end method

.method static synthetic access$000(Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 16
    invoke-direct {p0}, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->isWifiConnected()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$100(Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 16
    invoke-direct {p0}, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->queryIsWifiDirectConnected()V

    #@3
    return-void
.end method

.method static synthetic access$202(Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 16
    iput-boolean p1, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mBlocked:Z

    #@2
    return p1
.end method

.method private isWifiConnected()Z
    .registers 6

    #@0
    .prologue
    .line 102
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mContext:Landroid/content/Context;

    #@2
    const-string v3, "connectivity"

    #@4
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/net/ConnectivityManager;

    #@a
    .line 103
    .local v0, cm:Landroid/net/ConnectivityManager;
    const/4 v1, 0x0

    #@b
    .line 105
    .local v1, wifiInfo:Landroid/net/NetworkInfo;
    if-eqz v0, :cond_19

    #@d
    .line 106
    const/4 v2, 0x1

    #@e
    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    #@11
    move-result-object v1

    #@12
    .line 107
    if-eqz v1, :cond_19

    #@14
    .line 108
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    #@17
    move-result v2

    #@18
    .line 114
    :goto_18
    return v2

    #@19
    .line 112
    :cond_19
    const-string v2, "LGBluetoothWifiCoexChecker"

    #@1b
    new-instance v3, Ljava/lang/StringBuilder;

    #@1d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@20
    const-string v4, "[BTUI] isWifiConnected false, cm : "

    #@22
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v3

    #@2a
    const-string v4, ", wifiInfo : "

    #@2c
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v3

    #@30
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v3

    #@38
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    .line 114
    const/4 v2, 0x0

    #@3c
    goto :goto_18
.end method

.method private queryIsWifiDirectConnected()V
    .registers 4

    #@0
    .prologue
    .line 44
    const-string v0, "LGBluetoothWifiCoexChecker"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "queryIsWifiDirectConnected, mWifiP2pManager : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    const-string v2, ", mChannel : "

    #@15
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v1

    #@19
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@1b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@22
    move-result-object v1

    #@23
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 46
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@28
    if-eqz v0, :cond_35

    #@2a
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@2c
    if-eqz v0, :cond_35

    #@2e
    .line 47
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@30
    iget-object v1, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@32
    invoke-virtual {v0, v1, p0}, Landroid/net/wifi/p2p/WifiP2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    #@35
    .line 49
    :cond_35
    return-void
.end method


# virtual methods
.method public checkBtWifiCoex()V
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 92
    const-string v0, "LGBluetoothWifiCoexChecker"

    #@3
    new-instance v1, Ljava/lang/StringBuilder;

    #@5
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@8
    const-string v2, "[BTUI] checkBtWifiCoex, mBlocked : "

    #@a
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    move-result-object v1

    #@e
    iget-boolean v2, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mBlocked:Z

    #@10
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 94
    iget-boolean v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mBlocked:Z

    #@1d
    if-nez v0, :cond_2e

    #@1f
    .line 95
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mHandler:Landroid/os/Handler;

    #@21
    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@24
    .line 96
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mHandler:Landroid/os/Handler;

    #@26
    const/4 v1, 0x2

    #@27
    const-wide/16 v2, 0x2710

    #@29
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    #@2c
    .line 97
    iput-boolean v4, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mBlocked:Z

    #@2e
    .line 99
    :cond_2e
    return-void
.end method

.method public cleanup()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 37
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mContext:Landroid/content/Context;

    #@3
    .line 38
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mWifiP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    #@5
    .line 39
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    #@7
    .line 40
    return-void
.end method

.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .registers 9
    .parameter "peers"

    #@0
    .prologue
    .line 52
    move-object v3, p1

    #@1
    .line 53
    .local v3, peerList:Landroid/net/wifi/p2p/WifiP2pDeviceList;
    const/4 v1, 0x0

    #@2
    .line 56
    .local v1, isConnected:Z
    const-string v4, "LGBluetoothWifiCoexChecker"

    #@4
    const-string v5, "onPeersAvailable"

    #@6
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 59
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    #@c
    move-result-object v4

    #@d
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    #@10
    move-result-object v0

    #@11
    .local v0, i$:Ljava/util/Iterator;
    :cond_11
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    #@14
    move-result v4

    #@15
    if-eqz v4, :cond_22

    #@17
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@1a
    move-result-object v2

    #@1b
    check-cast v2, Landroid/net/wifi/p2p/WifiP2pDevice;

    #@1d
    .line 60
    .local v2, peer:Landroid/net/wifi/p2p/WifiP2pDevice;
    iget v4, v2, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    #@1f
    if-nez v4, :cond_11

    #@21
    .line 61
    const/4 v1, 0x1

    #@22
    .line 66
    .end local v2           #peer:Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_22
    if-eqz v1, :cond_37

    #@24
    .line 67
    iget-object v4, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mContext:Landroid/content/Context;

    #@26
    iget-object v5, p0, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->mContext:Landroid/content/Context;

    #@28
    const v6, 0x7f070107

    #@2b
    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@2e
    move-result-object v5

    #@2f
    const/4 v6, 0x1

    #@30
    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@33
    move-result-object v4

    #@34
    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    #@37
    .line 69
    :cond_37
    return-void
.end method
