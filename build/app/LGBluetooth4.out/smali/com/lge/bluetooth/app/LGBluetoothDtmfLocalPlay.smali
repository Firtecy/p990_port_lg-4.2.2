.class public Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;
.super Ljava/lang/Object;
.source "LGBluetoothDtmfLocalPlay.java"


# static fields
.field private static final DBG:Z = true

.field private static final DTMF_DUR_MS:I = 0x64

.field private static final TAG:Ljava/lang/String; = "LGBluetoothDtmfLocalPlay"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mToneGenerator:Landroid/media/ToneGenerator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 6
    .parameter "context"

    #@0
    .prologue
    .line 30
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 31
    iput-object p1, p0, Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;->mContext:Landroid/content/Context;

    #@5
    .line 38
    iget-object v1, p0, Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;->mToneGenerator:Landroid/media/ToneGenerator;

    #@7
    if-nez v1, :cond_14

    #@9
    .line 40
    :try_start_9
    new-instance v1, Landroid/media/ToneGenerator;

    #@b
    const/16 v2, 0x8

    #@d
    const/16 v3, 0x64

    #@f
    invoke-direct {v1, v2, v3}, Landroid/media/ToneGenerator;-><init>(II)V

    #@12
    iput-object v1, p0, Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;->mToneGenerator:Landroid/media/ToneGenerator;
    :try_end_14
    .catch Ljava/lang/RuntimeException; {:try_start_9 .. :try_end_14} :catch_15

    #@14
    .line 46
    :cond_14
    :goto_14
    return-void

    #@15
    .line 41
    :catch_15
    move-exception v0

    #@16
    .line 42
    .local v0, e:Ljava/lang/RuntimeException;
    const-string v1, "LGBluetoothDtmfLocalPlay"

    #@18
    new-instance v2, Ljava/lang/StringBuilder;

    #@1a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@1d
    const-string v3, "[BTUI] RuntimeException while creating tone generator: "

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v2

    #@27
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v2

    #@2b
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 43
    const/4 v1, 0x0

    #@2f
    iput-object v1, p0, Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;->mToneGenerator:Landroid/media/ToneGenerator;

    #@31
    goto :goto_14
.end method


# virtual methods
.method public playDTMF(C)V
    .registers 6
    .parameter "c"

    #@0
    .prologue
    .line 49
    const-string v1, "LGBluetoothDtmfLocalPlay"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "[BTUI] playDTMF = "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v2

    #@15
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 50
    iget-object v1, p0, Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;->mToneGenerator:Landroid/media/ToneGenerator;

    #@1a
    if-nez v1, :cond_35

    #@1c
    .line 51
    const-string v1, "LGBluetoothDtmfLocalPlay"

    #@1e
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "[BTUI] mToneGeneratoris null: "

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 74
    :goto_34
    return-void

    #@35
    .line 55
    :cond_35
    const/4 v0, -0x1

    #@36
    .line 56
    .local v0, tone:I
    sparse-switch p1, :sswitch_data_6e

    #@39
    .line 64
    const/16 v1, 0x30

    #@3b
    if-lt p1, v1, :cond_45

    #@3d
    const/16 v1, 0x39

    #@3f
    if-gt p1, v1, :cond_45

    #@41
    .line 65
    add-int/lit8 v1, p1, 0x0

    #@43
    add-int/lit8 v0, v1, -0x30

    #@45
    .line 69
    :cond_45
    :goto_45
    if-ltz v0, :cond_55

    #@47
    .line 70
    iget-object v1, p0, Lcom/lge/bluetooth/app/LGBluetoothDtmfLocalPlay;->mToneGenerator:Landroid/media/ToneGenerator;

    #@49
    const/16 v2, 0x64

    #@4b
    invoke-virtual {v1, v0, v2}, Landroid/media/ToneGenerator;->startTone(II)Z

    #@4e
    goto :goto_34

    #@4f
    .line 58
    :sswitch_4f
    const/16 v0, 0xb

    #@51
    .line 59
    goto :goto_45

    #@52
    .line 61
    :sswitch_52
    const/16 v0, 0xb

    #@54
    .line 62
    goto :goto_45

    #@55
    .line 72
    :cond_55
    const-string v1, "LGBluetoothDtmfLocalPlay"

    #@57
    new-instance v2, Ljava/lang/StringBuilder;

    #@59
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@5c
    const-string v3, "[BTUI] Invalid TONE: "

    #@5e
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@61
    move-result-object v2

    #@62
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    #@65
    move-result-object v2

    #@66
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v2

    #@6a
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    goto :goto_34

    #@6e
    .line 56
    :sswitch_data_6e
    .sparse-switch
        0x23 -> :sswitch_4f
        0x2a -> :sswitch_52
    .end sparse-switch
.end method
