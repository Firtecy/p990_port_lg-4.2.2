.class Lcom/lge/bluetooth/app/LGBluetoothService$1;
.super Ljava/lang/Thread;
.source "LGBluetoothService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/lge/bluetooth/app/LGBluetoothService;->onStartCommand(Landroid/content/Intent;II)I
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/bluetooth/app/LGBluetoothService;


# direct methods
.method constructor <init>(Lcom/lge/bluetooth/app/LGBluetoothService;Ljava/lang/String;)V
    .registers 3
    .parameter
    .parameter "x0"

    #@0
    .prologue
    .line 62
    iput-object p1, p0, Lcom/lge/bluetooth/app/LGBluetoothService$1;->this$0:Lcom/lge/bluetooth/app/LGBluetoothService;

    #@2
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 8

    #@0
    .prologue
    .line 65
    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    #@3
    move-result-object v5

    #@4
    invoke-virtual {v5}, Ljava/lang/Thread;->isInterrupted()Z

    #@7
    move-result v5

    #@8
    if-nez v5, :cond_40

    #@a
    .line 66
    iget-object v5, p0, Lcom/lge/bluetooth/app/LGBluetoothService$1;->this$0:Lcom/lge/bluetooth/app/LGBluetoothService;

    #@c
    iget v6, v5, Lcom/lge/bluetooth/app/LGBluetoothService;->mCurNum:I

    #@e
    add-int/lit8 v6, v6, 0x1

    #@10
    iput v6, v5, Lcom/lge/bluetooth/app/LGBluetoothService;->mCurNum:I

    #@12
    .line 67
    iget-object v5, p0, Lcom/lge/bluetooth/app/LGBluetoothService$1;->this$0:Lcom/lge/bluetooth/app/LGBluetoothService;

    #@14
    iget-object v5, v5, Lcom/lge/bluetooth/app/LGBluetoothService;->mCallbackList:Landroid/os/RemoteCallbackList;

    #@16
    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_19
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_19} :catch_3f

    #@19
    move-result v1

    #@1a
    .line 68
    .local v1, count:I
    const/4 v4, 0x0

    #@1b
    .local v4, i:I
    :goto_1b
    if-ge v4, v1, :cond_41

    #@1d
    .line 70
    :try_start_1d
    iget-object v5, p0, Lcom/lge/bluetooth/app/LGBluetoothService$1;->this$0:Lcom/lge/bluetooth/app/LGBluetoothService;

    #@1f
    iget-object v5, v5, Lcom/lge/bluetooth/app/LGBluetoothService;->mCallbackList:Landroid/os/RemoteCallbackList;

    #@21
    invoke-virtual {v5, v4}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    #@24
    move-result-object v0

    #@25
    check-cast v0, Lcom/lge/bluetooth/ILGBluetoothServiceCallback;

    #@27
    .line 72
    .local v0, callback:Lcom/lge/bluetooth/ILGBluetoothServiceCallback;
    if-eqz v0, :cond_37

    #@29
    .line 73
    const-string v5, "LGBluetoothService"

    #@2b
    const-string v6, "Service call onBluetoothCallbackTest()"

    #@2d
    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@30
    .line 74
    iget-object v5, p0, Lcom/lge/bluetooth/app/LGBluetoothService$1;->this$0:Lcom/lge/bluetooth/app/LGBluetoothService;

    #@32
    iget v5, v5, Lcom/lge/bluetooth/app/LGBluetoothService;->mCurNum:I

    #@34
    invoke-interface {v0, v5}, Lcom/lge/bluetooth/ILGBluetoothServiceCallback;->onBluetoothCallbackTest(I)V
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_1d .. :try_end_37} :catch_3a
    .catch Ljava/lang/InterruptedException; {:try_start_1d .. :try_end_37} :catch_3f

    #@37
    .line 68
    .end local v0           #callback:Lcom/lge/bluetooth/ILGBluetoothServiceCallback;
    :cond_37
    :goto_37
    add-int/lit8 v4, v4, 0x1

    #@39
    goto :goto_1b

    #@3a
    .line 77
    :catch_3a
    move-exception v2

    #@3b
    .line 78
    .local v2, e:Landroid/os/RemoteException;
    :try_start_3b
    invoke-virtual {v2}, Landroid/os/RemoteException;->printStackTrace()V

    #@3e
    goto :goto_37

    #@3f
    .line 85
    .end local v1           #count:I
    .end local v2           #e:Landroid/os/RemoteException;
    .end local v4           #i:I
    :catch_3f
    move-exception v3

    #@40
    .line 88
    :cond_40
    return-void

    #@41
    .line 81
    .restart local v1       #count:I
    .restart local v4       #i:I
    :cond_41
    iget-object v5, p0, Lcom/lge/bluetooth/app/LGBluetoothService$1;->this$0:Lcom/lge/bluetooth/app/LGBluetoothService;

    #@43
    iget-object v5, v5, Lcom/lge/bluetooth/app/LGBluetoothService;->mCallbackList:Landroid/os/RemoteCallbackList;

    #@45
    invoke-virtual {v5}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    #@48
    .line 82
    const-wide/16 v5, 0x3e8

    #@4a
    invoke-static {v5, v6}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4d
    .catch Ljava/lang/InterruptedException; {:try_start_3b .. :try_end_4d} :catch_3f

    #@4d
    goto :goto_0
.end method
