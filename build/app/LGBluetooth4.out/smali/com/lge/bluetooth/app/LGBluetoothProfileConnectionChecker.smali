.class public final Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;
.super Ljava/lang/Object;
.source "LGBluetoothProfileConnectionChecker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;,
        Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;
    }
.end annotation


# static fields
.field public static final DEBUG:Z = true

.field public static final PROFILE_A2DP:I = 0x0

.field public static final PROFILE_BLE:I = 0x4

.field public static final PROFILE_HFP:I = 0x1

.field public static final PROFILE_HID:I = 0x2

.field private static final PROFILE_MAX:I = 0x5

.field public static final PROFILE_PAN:I = 0x3

.field public static final TAG:Ljava/lang/String; = "LGBluetoothProfileConnectionChecker"

.field private static sChecker:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;


# instance fields
.field private mA2DPConnection:Ljava/util/HashMap;

.field private mBLEConnection:Ljava/util/HashMap;

.field private mConnectedDeviceAddress:Ljava/lang/String;

.field private mConnectedDeviceName:Ljava/lang/String;

.field private mHFPConnection:Ljava/util/HashMap;

.field private mHIDPConnection:Ljava/util/HashMap;

.field public mMaps:[Ljava/util/HashMap;

.field private mPANConnection:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 31
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->sChecker:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@3
    return-void
.end method

.method private constructor <init>()V
    .registers 4

    #@0
    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 15
    new-instance v0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;

    #@5
    const-string v1, "BLE Profile"

    #@7
    invoke-direct {v0, p0, v1}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;-><init>(Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;Ljava/lang/String;)V

    #@a
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mBLEConnection:Ljava/util/HashMap;

    #@c
    .line 16
    new-instance v0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;

    #@e
    const-string v1, "HID Profile"

    #@10
    invoke-direct {v0, p0, v1}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;-><init>(Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;Ljava/lang/String;)V

    #@13
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mHIDPConnection:Ljava/util/HashMap;

    #@15
    .line 17
    new-instance v0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;

    #@17
    const-string v1, "PAN Profile"

    #@19
    invoke-direct {v0, p0, v1}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$ProfileConnectionMap;-><init>(Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;Ljava/lang/String;)V

    #@1c
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mPANConnection:Ljava/util/HashMap;

    #@1e
    .line 18
    new-instance v0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;

    #@20
    const-string v1, "A2DP Profile"

    #@22
    invoke-direct {v0, p0, v1}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;-><init>(Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;Ljava/lang/String;)V

    #@25
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mA2DPConnection:Ljava/util/HashMap;

    #@27
    .line 19
    new-instance v0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;

    #@29
    const-string v1, "HFP Profile"

    #@2b
    invoke-direct {v0, p0, v1}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker$SingleProfileConnectionMap;-><init>(Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;Ljava/lang/String;)V

    #@2e
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mHFPConnection:Ljava/util/HashMap;

    #@30
    .line 29
    const/4 v0, 0x5

    #@31
    new-array v0, v0, [Ljava/util/HashMap;

    #@33
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mMaps:[Ljava/util/HashMap;

    #@35
    .line 42
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mMaps:[Ljava/util/HashMap;

    #@37
    const/4 v1, 0x0

    #@38
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mA2DPConnection:Ljava/util/HashMap;

    #@3a
    aput-object v2, v0, v1

    #@3c
    .line 43
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mMaps:[Ljava/util/HashMap;

    #@3e
    const/4 v1, 0x1

    #@3f
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mHFPConnection:Ljava/util/HashMap;

    #@41
    aput-object v2, v0, v1

    #@43
    .line 44
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mMaps:[Ljava/util/HashMap;

    #@45
    const/4 v1, 0x2

    #@46
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mHIDPConnection:Ljava/util/HashMap;

    #@48
    aput-object v2, v0, v1

    #@4a
    .line 45
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mMaps:[Ljava/util/HashMap;

    #@4c
    const/4 v1, 0x3

    #@4d
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mPANConnection:Ljava/util/HashMap;

    #@4f
    aput-object v2, v0, v1

    #@51
    .line 46
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mMaps:[Ljava/util/HashMap;

    #@53
    const/4 v1, 0x4

    #@54
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mBLEConnection:Ljava/util/HashMap;

    #@56
    aput-object v2, v0, v1

    #@58
    .line 47
    return-void
.end method

.method private dropConnectedDeviceInfo()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 143
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mConnectedDeviceAddress:Ljava/lang/String;

    #@3
    .line 144
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mConnectedDeviceName:Ljava/lang/String;

    #@5
    .line 145
    return-void
.end method

.method public static declared-synchronized getDefaultChecker()Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;
    .registers 3

    #@0
    .prologue
    .line 34
    const-class v1, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@2
    monitor-enter v1

    #@3
    :try_start_3
    sget-object v0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->sChecker:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@5
    if-nez v0, :cond_15

    #@7
    .line 35
    const-string v0, "LGBluetoothProfileConnectionChecker"

    #@9
    const-string v2, "new LGBluetoothProfileConnectionChecker() singletone"

    #@b
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 36
    new-instance v0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@10
    invoke-direct {v0}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;-><init>()V

    #@13
    sput-object v0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->sChecker:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;

    #@15
    .line 38
    :cond_15
    sget-object v0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->sChecker:Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;
    :try_end_17
    .catchall {:try_start_3 .. :try_end_17} :catchall_19

    #@17
    monitor-exit v1

    #@18
    return-object v0

    #@19
    .line 34
    :catchall_19
    move-exception v0

    #@1a
    monitor-exit v1

    #@1b
    throw v0
.end method


# virtual methods
.method debug(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 51
    const-string v0, "LGBluetoothProfileConnectionChecker"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 53
    return-void
.end method

.method error(Ljava/lang/String;)V
    .registers 3
    .parameter "s"

    #@0
    .prologue
    .line 56
    const-string v0, "LGBluetoothProfileConnectionChecker"

    #@2
    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5
    .line 57
    return-void
.end method

.method public getConnectedDeviceAmount()I
    .registers 11

    #@0
    .prologue
    const/4 v8, 0x3

    #@1
    const/4 v9, 0x1

    #@2
    .line 148
    const/4 v1, 0x0

    #@3
    .line 149
    .local v1, count:I
    const/4 v6, 0x0

    #@4
    .line 150
    .local v6, s:Ljava/lang/String;
    invoke-direct {p0}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->dropConnectedDeviceInfo()V

    #@7
    .line 152
    const-string v7, "getConnectedDeviceAmount() start"

    #@9
    invoke-virtual {p0, v7}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->debug(Ljava/lang/String;)V

    #@c
    .line 153
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mMaps:[Ljava/util/HashMap;

    #@e
    .local v0, arr$:[Ljava/util/HashMap;
    array-length v5, v0

    #@f
    .local v5, len$:I
    const/4 v4, 0x0

    #@10
    .local v4, i$:I
    :goto_10
    if-ge v4, v5, :cond_73

    #@12
    aget-object v2, v0, v4

    #@14
    .line 154
    .local v2, hm:Ljava/util/HashMap;
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@17
    move-result v7

    #@18
    if-ne v7, v9, :cond_63

    #@1a
    .line 155
    if-nez v6, :cond_41

    #@1c
    .line 156
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@1f
    move-result-object v7

    #@20
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@23
    move-result-object v3

    #@24
    .line 157
    .local v3, i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@27
    move-result v7

    #@28
    if-eqz v7, :cond_3e

    #@2a
    .line 158
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@2d
    move-result-object v6

    #@2e
    .end local v6           #s:Ljava/lang/String;
    check-cast v6, Ljava/lang/String;

    #@30
    .line 159
    .restart local v6       #s:Ljava/lang/String;
    if-eqz v6, :cond_3e

    #@32
    .line 160
    add-int/lit8 v1, v1, 0x1

    #@34
    .line 161
    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    #@37
    move-result-object v7

    #@38
    check-cast v7, Ljava/lang/String;

    #@3a
    iput-object v7, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mConnectedDeviceName:Ljava/lang/String;

    #@3c
    .line 162
    iput-object v6, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mConnectedDeviceAddress:Ljava/lang/String;

    #@3e
    .line 153
    .end local v3           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_3e
    add-int/lit8 v4, v4, 0x1

    #@40
    goto :goto_10

    #@41
    .line 166
    :cond_41
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    #@44
    move-result-object v7

    #@45
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    #@48
    move-result-object v3

    #@49
    .line 167
    .restart local v3       #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    #@4c
    move-result v7

    #@4d
    if-eqz v7, :cond_3e

    #@4f
    .line 168
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    #@52
    move-result-object v7

    #@53
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@56
    move-result v7

    #@57
    if-nez v7, :cond_3e

    #@59
    .line 169
    invoke-direct {p0}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->dropConnectedDeviceInfo()V

    #@5c
    .line 170
    const-string v7, "multiple devices are connected to each profile"

    #@5e
    invoke-virtual {p0, v7}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->debug(Ljava/lang/String;)V

    #@61
    move v7, v8

    #@62
    .line 189
    .end local v2           #hm:Ljava/util/HashMap;
    .end local v3           #i:Ljava/util/Iterator;,"Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_62
    return v7

    #@63
    .line 175
    .restart local v2       #hm:Ljava/util/HashMap;
    :cond_63
    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    #@66
    move-result v7

    #@67
    if-le v7, v9, :cond_3e

    #@69
    .line 176
    invoke-direct {p0}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->dropConnectedDeviceInfo()V

    #@6c
    .line 177
    const-string v7, "mutiple devices are connected to same profile"

    #@6e
    invoke-virtual {p0, v7}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->debug(Ljava/lang/String;)V

    #@71
    move v7, v8

    #@72
    .line 178
    goto :goto_62

    #@73
    .line 182
    .end local v2           #hm:Ljava/util/HashMap;
    :cond_73
    if-nez v1, :cond_7f

    #@75
    .line 183
    invoke-direct {p0}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->dropConnectedDeviceInfo()V

    #@78
    .line 184
    const-string v7, "no device connected"

    #@7a
    invoke-virtual {p0, v7}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->debug(Ljava/lang/String;)V

    #@7d
    move v7, v9

    #@7e
    .line 185
    goto :goto_62

    #@7f
    .line 188
    :cond_7f
    const-string v7, "a device is connected"

    #@81
    invoke-virtual {p0, v7}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->debug(Ljava/lang/String;)V

    #@84
    .line 189
    const/4 v7, 0x2

    #@85
    goto :goto_62
.end method

.method public getConnectedDeviceName()Ljava/lang/String;
    .registers 3

    #@0
    .prologue
    .line 193
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mConnectedDeviceName:Ljava/lang/String;

    #@2
    if-eqz v0, :cond_c

    #@4
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mConnectedDeviceName:Ljava/lang/String;

    #@6
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_27

    #@c
    .line 194
    :cond_c
    new-instance v0, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v1, "getConnectedDeviceName : "

    #@13
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v0

    #@17
    iget-object v1, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mConnectedDeviceAddress:Ljava/lang/String;

    #@19
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v0

    #@1d
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {p0, v0}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->debug(Ljava/lang/String;)V

    #@24
    .line 195
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mConnectedDeviceAddress:Ljava/lang/String;

    #@26
    .line 198
    :goto_26
    return-object v0

    #@27
    .line 197
    :cond_27
    new-instance v0, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v1, "getConnectedDeviceName : "

    #@2e
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v0

    #@32
    iget-object v1, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mConnectedDeviceName:Ljava/lang/String;

    #@34
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v0

    #@38
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v0

    #@3c
    invoke-virtual {p0, v0}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->debug(Ljava/lang/String;)V

    #@3f
    .line 198
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mConnectedDeviceName:Ljava/lang/String;

    #@41
    goto :goto_26
.end method

.method public setConnected(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Z
    .registers 9
    .parameter "context"
    .parameter "profileNum"
    .parameter "address"
    .parameter "name"

    #@0
    .prologue
    const/16 v3, 0x32

    #@2
    const/4 v1, 0x0

    #@3
    .line 103
    const/4 v2, 0x5

    #@4
    if-le p2, v2, :cond_23

    #@6
    .line 104
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "Profile number "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    const-string v3, " is not supported!!"

    #@17
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {p0, v2}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->error(Ljava/lang/String;)V

    #@22
    .line 118
    :goto_22
    return v1

    #@23
    .line 108
    :cond_23
    if-eqz p4, :cond_2f

    #@25
    .line 109
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    #@28
    move-result v2

    #@29
    if-le v2, v3, :cond_2f

    #@2b
    .line 110
    invoke-virtual {p4, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    #@2e
    move-result-object p4

    #@2f
    .line 113
    :cond_2f
    iget-object v1, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mMaps:[Ljava/util/HashMap;

    #@31
    aget-object v0, v1, p2

    #@33
    .line 114
    .local v0, hm:Ljava/util/HashMap;
    monitor-enter v0

    #@34
    .line 115
    :try_start_34
    invoke-virtual {v0, p3, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    #@37
    .line 116
    monitor-exit v0
    :try_end_38
    .catchall {:try_start_34 .. :try_end_38} :catchall_44

    #@38
    .line 117
    new-instance v1, Landroid/content/Intent;

    #@3a
    const-string v2, "com.lge.bluetooth.action.UPDATE_CONNECT_DEV_NAME"

    #@3c
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3f
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@42
    .line 118
    const/4 v1, 0x1

    #@43
    goto :goto_22

    #@44
    .line 116
    :catchall_44
    move-exception v1

    #@45
    :try_start_45
    monitor-exit v0
    :try_end_46
    .catchall {:try_start_45 .. :try_end_46} :catchall_44

    #@46
    throw v1
.end method

.method public setDisconnected(Landroid/content/Context;ILjava/lang/String;)Z
    .registers 8
    .parameter "context"
    .parameter "profileNum"
    .parameter "address"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 122
    const/4 v2, 0x5

    #@2
    if-le p2, v2, :cond_21

    #@4
    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "Profile number "

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    const-string v3, " is not supported!!"

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {p0, v2}, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->error(Ljava/lang/String;)V

    #@20
    .line 133
    :goto_20
    return v1

    #@21
    .line 126
    :cond_21
    iget-object v2, p0, Lcom/lge/bluetooth/app/LGBluetoothProfileConnectionChecker;->mMaps:[Ljava/util/HashMap;

    #@23
    aget-object v0, v2, p2

    #@25
    .line 127
    .local v0, hm:Ljava/util/HashMap;
    monitor-enter v0

    #@26
    .line 128
    :try_start_26
    invoke-virtual {v0, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    #@29
    move-result-object v2

    #@2a
    if-nez v2, :cond_31

    #@2c
    .line 129
    monitor-exit v0

    #@2d
    goto :goto_20

    #@2e
    .line 131
    :catchall_2e
    move-exception v1

    #@2f
    monitor-exit v0
    :try_end_30
    .catchall {:try_start_26 .. :try_end_30} :catchall_2e

    #@30
    throw v1

    #@31
    :cond_31
    :try_start_31
    monitor-exit v0
    :try_end_32
    .catchall {:try_start_31 .. :try_end_32} :catchall_2e

    #@32
    .line 132
    new-instance v1, Landroid/content/Intent;

    #@34
    const-string v2, "com.lge.bluetooth.action.UPDATE_CONNECT_DEV_NAME"

    #@36
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@39
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@3c
    .line 133
    const/4 v1, 0x1

    #@3d
    goto :goto_20
.end method
