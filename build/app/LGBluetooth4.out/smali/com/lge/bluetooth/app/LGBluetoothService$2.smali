.class Lcom/lge/bluetooth/app/LGBluetoothService$2;
.super Lcom/lge/bluetooth/ILGBluetoothService$Stub;
.source "LGBluetoothService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/bluetooth/app/LGBluetoothService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/bluetooth/app/LGBluetoothService;


# direct methods
.method constructor <init>(Lcom/lge/bluetooth/app/LGBluetoothService;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 114
    iput-object p1, p0, Lcom/lge/bluetooth/app/LGBluetoothService$2;->this$0:Lcom/lge/bluetooth/app/LGBluetoothService;

    #@2
    invoke-direct {p0}, Lcom/lge/bluetooth/ILGBluetoothService$Stub;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public addDummyPairedList()V
    .registers 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 163
    const-string v1, "LGBluetoothService"

    #@2
    const-string v2, "[BTUI] addDummyPairedList"

    #@4
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 164
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@a
    move-result-object v0

    #@b
    .line 165
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_10

    #@d
    .line 166
    invoke-virtual {v0}, Lcom/android/bluetooth/btservice/AdapterService;->addDummyPairedList()Z

    #@10
    .line 168
    :cond_10
    return-void
.end method

.method public getProfileConnectionState(I)I
    .registers 4
    .parameter "profile"

    #@0
    .prologue
    .line 201
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 202
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_b

    #@6
    .line 203
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->getProfileConnectionState(I)I

    #@9
    move-result v1

    #@a
    .line 205
    :goto_a
    return v1

    #@b
    :cond_b
    const/4 v1, 0x0

    #@c
    goto :goto_a
.end method

.method public registerServerCallback(Lcom/lge/bluetooth/ILGBluetoothServiceCallback;)Z
    .registers 3
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 172
    if-eqz p1, :cond_b

    #@2
    .line 173
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService$2;->this$0:Lcom/lge/bluetooth/app/LGBluetoothService;

    #@4
    iget-object v0, v0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCallbackList:Landroid/os/RemoteCallbackList;

    #@6
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    #@9
    move-result v0

    #@a
    .line 175
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method

.method public sendBattRssiSvcRoamLevel(Ljava/lang/String;I)V
    .registers 7
    .parameter "type"
    .parameter "value"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 143
    const-string v1, "LGBluetoothService"

    #@2
    new-instance v2, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "[BTUI] sendBattRssiSvcRoamLevel = "

    #@9
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v2

    #@d
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    const-string v3, "("

    #@13
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v2

    #@1b
    const-string v3, ")"

    #@1d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v2

    #@21
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 145
    invoke-static {}, Lcom/android/bluetooth/hfp/HeadsetService;->getHeadsetService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@2b
    move-result-object v0

    #@2c
    .line 146
    .local v0, headsetService:Lcom/android/bluetooth/hfp/HeadsetService;
    if-eqz v0, :cond_31

    #@2e
    .line 147
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/hfp/HeadsetService;->sendBattRssiSvcRoamLevel(Ljava/lang/String;I)V

    #@31
    .line 149
    :cond_31
    return-void
.end method

.method public sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V
    .registers 6
    .parameter "device"
    .parameter "profile"
    .parameter "state"
    .parameter "prevState"

    #@0
    .prologue
    .line 192
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@3
    move-result-object v0

    #@4
    .line 193
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_9

    #@6
    .line 194
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/android/bluetooth/btservice/AdapterService;->sendConnectionStateChange(Landroid/bluetooth/BluetoothDevice;III)V

    #@9
    .line 198
    :cond_9
    return-void
.end method

.method public sendScoOnOff(Z)V
    .registers 6
    .parameter "onoff"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 153
    const-string v2, "LGBluetoothService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "[BTUI] sendScoOnOff = "

    #@9
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    if-eqz p1, :cond_26

    #@f
    const-string v1, "on"

    #@11
    :goto_11
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 155
    invoke-static {}, Lcom/android/bluetooth/hfp/HeadsetService;->getHeadsetService()Lcom/android/bluetooth/hfp/HeadsetService;

    #@1f
    move-result-object v0

    #@20
    .line 156
    .local v0, headsetService:Lcom/android/bluetooth/hfp/HeadsetService;
    if-eqz v0, :cond_25

    #@22
    .line 157
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/hfp/HeadsetService;->sendScoOnOff(Z)V

    #@25
    .line 159
    :cond_25
    return-void

    #@26
    .line 153
    .end local v0           #headsetService:Lcom/android/bluetooth/hfp/HeadsetService;
    :cond_26
    const-string v1, "off"

    #@28
    goto :goto_11
.end method

.method public setProfileOnOff(Ljava/lang/String;Z)Z
    .registers 7
    .parameter "profile_name"
    .parameter "onoff"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 118
    const-string v2, "LGBluetoothService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "[BTUI] setProfileOnOff = "

    #@9
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v3, " ("

    #@13
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    if-eqz p2, :cond_37

    #@19
    const-string v1, "enable"

    #@1b
    :goto_1b
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v1

    #@1f
    const-string v3, ")"

    #@21
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@28
    move-result-object v1

    #@29
    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2c
    .line 120
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@2f
    move-result-object v0

    #@30
    .line 121
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_3a

    #@32
    .line 122
    invoke-virtual {v0, p1, p2}, Lcom/android/bluetooth/btservice/AdapterService;->setProfileOnOff(Ljava/lang/String;Z)Z

    #@35
    move-result v1

    #@36
    .line 124
    :goto_36
    return v1

    #@37
    .line 118
    .end local v0           #adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    :cond_37
    const-string v1, "disable"

    #@39
    goto :goto_1b

    #@3a
    .line 124
    .restart local v0       #adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    :cond_3a
    const/4 v1, 0x0

    #@3b
    goto :goto_36
.end method

.method public setSspDebugMode(Z)V
    .registers 6
    .parameter "onoff"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 130
    const-string v2, "LGBluetoothService"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v3, "[BTUI] setSspDebugMode = "

    #@9
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v3

    #@d
    if-eqz p1, :cond_2c

    #@f
    const-string v1, "on"

    #@11
    :goto_11
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v1

    #@15
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v1

    #@19
    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 131
    invoke-static {}, Lcom/lge/bluetooth/LGBluetoothFeatureConfig;->isBRCMSolution()Z

    #@1f
    move-result v1

    #@20
    if-eqz v1, :cond_2b

    #@22
    .line 132
    invoke-static {}, Lcom/android/bluetooth/btservice/AdapterService;->getAdapterService()Lcom/android/bluetooth/btservice/AdapterService;

    #@25
    move-result-object v0

    #@26
    .line 133
    .local v0, adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    if-eqz v0, :cond_2b

    #@28
    .line 134
    invoke-virtual {v0, p1}, Lcom/android/bluetooth/btservice/AdapterService;->sspDebugConfigure(Z)Z

    #@2b
    .line 139
    .end local v0           #adapterService:Lcom/android/bluetooth/btservice/AdapterService;
    :cond_2b
    return-void

    #@2c
    .line 130
    :cond_2c
    const-string v1, "off"

    #@2e
    goto :goto_11
.end method

.method public unregisterCountCallback(Lcom/lge/bluetooth/ILGBluetoothServiceCallback;)Z
    .registers 3
    .parameter "callback"
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    #@0
    .prologue
    .line 181
    if-eqz p1, :cond_b

    #@2
    .line 182
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService$2;->this$0:Lcom/lge/bluetooth/app/LGBluetoothService;

    #@4
    iget-object v0, v0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCallbackList:Landroid/os/RemoteCallbackList;

    #@6
    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    #@9
    move-result v0

    #@a
    .line 184
    :goto_a
    return v0

    #@b
    :cond_b
    const/4 v0, 0x0

    #@c
    goto :goto_a
.end method
