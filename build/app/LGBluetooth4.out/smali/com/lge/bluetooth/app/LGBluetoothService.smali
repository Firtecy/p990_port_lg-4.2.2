.class public Lcom/lge/bluetooth/app/LGBluetoothService;
.super Landroid/app/Service;
.source "LGBluetoothService.java"


# static fields
#the value of this static final field might be set in the static constructor
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "LGBluetoothService"

.field private static isServiceStarted:Z

.field private static mContext:Landroid/content/Context;


# instance fields
.field mBinder:Lcom/lge/bluetooth/ILGBluetoothService$Stub;

.field final mCallbackList:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/lge/bluetooth/ILGBluetoothServiceCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mCleaningUp:Z

.field public mCountThread:Ljava/lang/Thread;

.field public mCurNum:I


# direct methods
.method static constructor <clinit>()V
    .registers 2

    #@0
    .prologue
    .line 36
    const-string v0, "persist.service.btui.debug"

    #@2
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    const-string v1, "1"

    #@8
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@b
    move-result v0

    #@c
    sput-boolean v0, Lcom/lge/bluetooth/app/LGBluetoothService;->DEBUG:Z

    #@e
    return-void
.end method

.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    #@3
    .line 41
    const/4 v0, 0x0

    #@4
    iput v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCurNum:I

    #@6
    .line 42
    const/4 v0, 0x0

    #@7
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCountThread:Ljava/lang/Thread;

    #@9
    .line 110
    new-instance v0, Landroid/os/RemoteCallbackList;

    #@b
    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    #@e
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCallbackList:Landroid/os/RemoteCallbackList;

    #@10
    .line 113
    new-instance v0, Lcom/lge/bluetooth/app/LGBluetoothService$2;

    #@12
    invoke-direct {v0, p0}, Lcom/lge/bluetooth/app/LGBluetoothService$2;-><init>(Lcom/lge/bluetooth/app/LGBluetoothService;)V

    #@15
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mBinder:Lcom/lge/bluetooth/ILGBluetoothService$Stub;

    #@17
    return-void
.end method

.method private BtUiLog(Ljava/lang/String;)V
    .registers 3
    .parameter "msg"

    #@0
    .prologue
    .line 243
    sget-boolean v0, Lcom/lge/bluetooth/app/LGBluetoothService;->DEBUG:Z

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 244
    const-string v0, "LGBluetoothService"

    #@6
    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@9
    .line 246
    :cond_9
    return-void
.end method


# virtual methods
.method cleanup()V
    .registers 2

    #@0
    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCleaningUp:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 230
    const-string v0, "[BTUI] *************service already starting to cleanup... Ignoring cleanup request........."

    #@6
    invoke-direct {p0, v0}, Lcom/lge/bluetooth/app/LGBluetoothService;->BtUiLog(Ljava/lang/String;)V

    #@9
    .line 240
    :goto_9
    return-void

    #@a
    .line 234
    :cond_a
    const/4 v0, 0x1

    #@b
    iput-boolean v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCleaningUp:Z

    #@d
    .line 236
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mBinder:Lcom/lge/bluetooth/ILGBluetoothService$Stub;

    #@f
    if-eqz v0, :cond_14

    #@11
    .line 237
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mBinder:Lcom/lge/bluetooth/ILGBluetoothService$Stub;

    #@14
    .line 239
    :cond_14
    const-string v0, "[BTUI] cleanup() done"

    #@16
    invoke-direct {p0, v0}, Lcom/lge/bluetooth/app/LGBluetoothService;->BtUiLog(Ljava/lang/String;)V

    #@19
    goto :goto_9
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .registers 3
    .parameter "arg0"

    #@0
    .prologue
    .line 212
    const-string v0, "[BTUI] onBind()"

    #@2
    invoke-direct {p0, v0}, Lcom/lge/bluetooth/app/LGBluetoothService;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 213
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mBinder:Lcom/lge/bluetooth/ILGBluetoothService$Stub;

    #@7
    return-object v0
.end method

.method public onCreate()V
    .registers 2

    #@0
    .prologue
    .line 50
    const-string v0, "[BTUI] onCreate()"

    #@2
    invoke-direct {p0, v0}, Lcom/lge/bluetooth/app/LGBluetoothService;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 51
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    #@8
    .line 52
    sput-object p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mContext:Landroid/content/Context;

    #@a
    .line 53
    const/4 v0, 0x1

    #@b
    sput-boolean v0, Lcom/lge/bluetooth/app/LGBluetoothService;->isServiceStarted:Z

    #@d
    .line 54
    return-void
.end method

.method public onDestroy()V
    .registers 2

    #@0
    .prologue
    .line 97
    const-string v0, "[BTUI] onDestroy()"

    #@2
    invoke-direct {p0, v0}, Lcom/lge/bluetooth/app/LGBluetoothService;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 98
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    #@8
    .line 100
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCountThread:Ljava/lang/Thread;

    #@a
    if-eqz v0, :cond_17

    #@c
    .line 101
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCountThread:Ljava/lang/Thread;

    #@e
    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    #@11
    .line 102
    const/4 v0, 0x0

    #@12
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCountThread:Ljava/lang/Thread;

    #@14
    .line 103
    const/4 v0, 0x0

    #@15
    iput v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCurNum:I

    #@17
    .line 105
    :cond_17
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .registers 3
    .parameter "arg0"

    #@0
    .prologue
    .line 225
    const-string v0, "[BTUI] onRebind()"

    #@2
    invoke-direct {p0, v0}, Lcom/lge/bluetooth/app/LGBluetoothService;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 226
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .registers 6
    .parameter "aIntent"
    .parameter "aFlags"
    .parameter "aStartId"

    #@0
    .prologue
    .line 58
    const-string v0, "[BTUI] onStartCommand()"

    #@2
    invoke-direct {p0, v0}, Lcom/lge/bluetooth/app/LGBluetoothService;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 59
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    #@8
    .line 61
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCountThread:Ljava/lang/Thread;

    #@a
    if-nez v0, :cond_1a

    #@c
    .line 62
    new-instance v0, Lcom/lge/bluetooth/app/LGBluetoothService$1;

    #@e
    const-string v1, "LG Bluetooth Service Thread"

    #@10
    invoke-direct {v0, p0, v1}, Lcom/lge/bluetooth/app/LGBluetoothService$1;-><init>(Lcom/lge/bluetooth/app/LGBluetoothService;Ljava/lang/String;)V

    #@13
    iput-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCountThread:Ljava/lang/Thread;

    #@15
    .line 90
    iget-object v0, p0, Lcom/lge/bluetooth/app/LGBluetoothService;->mCountThread:Ljava/lang/Thread;

    #@17
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    #@1a
    .line 92
    :cond_1a
    const/4 v0, 0x1

    #@1b
    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .registers 3
    .parameter "arg0"

    #@0
    .prologue
    .line 218
    const-string v0, "[BTUI] onUnbind()"

    #@2
    invoke-direct {p0, v0}, Lcom/lge/bluetooth/app/LGBluetoothService;->BtUiLog(Ljava/lang/String;)V

    #@5
    .line 219
    invoke-virtual {p0}, Lcom/lge/bluetooth/app/LGBluetoothService;->cleanup()V

    #@8
    .line 220
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    #@b
    move-result v0

    #@c
    return v0
.end method
