.class public Lcom/lge/bluetooth/opp/BluetoothStorageUtility;
.super Ljava/lang/Object;
.source "BluetoothStorageUtility.java"


# static fields
.field private static final D:Z = true

.field private static final EXTERNAL_STORAGE:Z = true

.field private static final INTERNAL_STORAGE:Z = false

.field private static final TAG:Ljava/lang/String; = "BluetoothOppStorageUtility"

.field private static mCurrentBtFile:Ljava/io/File;

.field private static mExternalStatus:I

.field private static mInternalStatus:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFileLength:J


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 40
    const/4 v0, 0x0

    #@1
    sput-object v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mCurrentBtFile:Ljava/io/File;

    #@3
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;J)V
    .registers 4
    .parameter "context"
    .parameter "length"

    #@0
    .prologue
    .line 47
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 48
    iput-object p1, p0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mContext:Landroid/content/Context;

    #@5
    .line 49
    iput-wide p2, p0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mFileLength:J

    #@7
    .line 50
    return-void
.end method

.method private checkStorage(Z)I
    .registers 25
    .parameter "type"

    #@0
    .prologue
    .line 65
    move-object/from16 v0, p0

    #@2
    iget-object v0, v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mContext:Landroid/content/Context;

    #@4
    move-object/from16 v19, v0

    #@6
    const-string v20, "storage"

    #@8
    invoke-virtual/range {v19 .. v20}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v15

    #@c
    check-cast v15, Landroid/os/storage/StorageManager;

    #@e
    .line 66
    .local v15, storageManager:Landroid/os/storage/StorageManager;
    invoke-virtual {v15}, Landroid/os/storage/StorageManager;->getVolumeList()[Landroid/os/storage/StorageVolume;

    #@11
    move-result-object v16

    #@12
    .line 68
    .local v16, storageVolumes:[Landroid/os/storage/StorageVolume;
    const/4 v12, 0x0

    #@13
    .line 69
    .local v12, pathOfRoot:Ljava/lang/String;
    const/4 v14, 0x0

    #@14
    .line 72
    .local v14, state:Ljava/lang/String;
    if-nez v16, :cond_20

    #@16
    .line 74
    const-string v19, "BluetoothOppStorageUtility"

    #@18
    const-string v20, "Receive File aborted - no storage"

    #@1a
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    .line 76
    const/16 v19, 0x1ed

    #@1f
    .line 134
    :goto_1f
    return v19

    #@20
    .line 80
    :cond_20
    const/4 v9, 0x0

    #@21
    .local v9, i:I
    :goto_21
    move-object/from16 v0, v16

    #@23
    array-length v0, v0

    #@24
    move/from16 v19, v0

    #@26
    move/from16 v0, v19

    #@28
    if-ge v9, v0, :cond_40

    #@2a
    .line 81
    aget-object v19, v16, v9

    #@2c
    invoke-virtual/range {v19 .. v19}, Landroid/os/storage/StorageVolume;->isRemovable()Z

    #@2f
    move-result v19

    #@30
    move/from16 v0, v19

    #@32
    move/from16 v1, p1

    #@34
    if-ne v0, v1, :cond_af

    #@36
    .line 82
    aget-object v19, v16, v9

    #@38
    invoke-virtual/range {v19 .. v19}, Landroid/os/storage/StorageVolume;->getPath()Ljava/lang/String;

    #@3b
    move-result-object v12

    #@3c
    .line 83
    invoke-virtual {v15, v12}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    #@3f
    move-result-object v14

    #@40
    .line 88
    :cond_40
    const/4 v8, 0x0

    #@41
    .line 90
    .local v8, btFile:Ljava/io/File;
    const-string v19, "BluetoothOppStorageUtility"

    #@43
    new-instance v20, Ljava/lang/StringBuilder;

    #@45
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@48
    const-string v21, "state : "

    #@4a
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4d
    move-result-object v20

    #@4e
    move-object/from16 v0, v20

    #@50
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@53
    move-result-object v20

    #@54
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@57
    move-result-object v20

    #@58
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5b
    .line 91
    if-eqz v14, :cond_b3

    #@5d
    const-string v19, "mounted"

    #@5f
    move-object/from16 v0, v19

    #@61
    invoke-virtual {v14, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@64
    move-result v19

    #@65
    if-eqz v19, :cond_b3

    #@67
    .line 92
    new-instance v8, Ljava/io/File;

    #@69
    .end local v8           #btFile:Ljava/io/File;
    new-instance v19, Ljava/lang/StringBuilder;

    #@6b
    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    #@6e
    move-object/from16 v0, v19

    #@70
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@73
    move-result-object v19

    #@74
    const-string v20, "/Bluetooth"

    #@76
    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v19

    #@7a
    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7d
    move-result-object v19

    #@7e
    move-object/from16 v0, v19

    #@80
    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@83
    .line 93
    .restart local v8       #btFile:Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->isDirectory()Z

    #@86
    move-result v19

    #@87
    if-nez v19, :cond_be

    #@89
    invoke-virtual {v8}, Ljava/io/File;->mkdir()Z

    #@8c
    move-result v19

    #@8d
    if-nez v19, :cond_be

    #@8f
    .line 94
    const-string v19, "BluetoothOppStorageUtility"

    #@91
    new-instance v20, Ljava/lang/StringBuilder;

    #@93
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@96
    const-string v21, "Receive File aborted - can\'t create base directory "

    #@98
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v20

    #@9c
    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@9f
    move-result-object v21

    #@a0
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v20

    #@a4
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v20

    #@a8
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ab
    .line 95
    const/16 v19, 0x1ec

    #@ad
    goto/16 :goto_1f

    #@af
    .line 80
    .end local v8           #btFile:Ljava/io/File;
    :cond_af
    add-int/lit8 v9, v9, 0x1

    #@b1
    goto/16 :goto_21

    #@b3
    .line 98
    .restart local v8       #btFile:Ljava/io/File;
    :cond_b3
    const-string v19, "BluetoothOppStorageUtility"

    #@b5
    const-string v20, "Receive File aborted - no storage"

    #@b7
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@ba
    .line 99
    const/16 v19, 0x1ed

    #@bc
    goto/16 :goto_1f

    #@be
    .line 102
    :cond_be
    new-instance v13, Landroid/os/StatFs;

    #@c0
    invoke-virtual {v8}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@c3
    move-result-object v19

    #@c4
    move-object/from16 v0, v19

    #@c6
    invoke-direct {v13, v0}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    #@c9
    .line 103
    .local v13, stat:Landroid/os/StatFs;
    const-wide/32 v10, 0x100000

    #@cc
    .line 104
    .local v10, margin:J
    invoke-virtual {v13}, Landroid/os/StatFs;->getBlockSize()I

    #@cf
    move-result v19

    #@d0
    move/from16 v0, v19

    #@d2
    int-to-long v6, v0

    #@d3
    .line 105
    .local v6, blockSize:J
    invoke-virtual {v13}, Landroid/os/StatFs;->getAvailableBlocks()I

    #@d6
    move-result v19

    #@d7
    move/from16 v0, v19

    #@d9
    int-to-long v2, v0

    #@da
    .line 106
    .local v2, availableBlocks:J
    mul-long v19, v2, v6

    #@dc
    const-wide/32 v21, 0x100000

    #@df
    sub-long v4, v19, v21

    #@e1
    .line 108
    .local v4, availableSize:J
    const-wide/16 v19, 0x0

    #@e3
    cmp-long v19, v4, v19

    #@e5
    if-gez v19, :cond_e9

    #@e7
    const-wide/16 v4, 0x0

    #@e9
    .line 110
    :cond_e9
    invoke-virtual {v13}, Landroid/os/StatFs;->getBlockCount()I

    #@ec
    move-result v19

    #@ed
    move/from16 v0, v19

    #@ef
    int-to-long v0, v0

    #@f0
    move-wide/from16 v17, v0

    #@f2
    .line 112
    .local v17, totalBlocks:J
    const-string v19, "BluetoothOppStorageUtility"

    #@f4
    const-string v20, "====== Bluetooth Storage Info. ======"

    #@f6
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@f9
    .line 113
    const-string v19, "BluetoothOppStorageUtility"

    #@fb
    new-instance v20, Ljava/lang/StringBuilder;

    #@fd
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@100
    const-string v21, "   Root Path :  "

    #@102
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@105
    move-result-object v20

    #@106
    move-object/from16 v0, v20

    #@108
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10b
    move-result-object v20

    #@10c
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10f
    move-result-object v20

    #@110
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@113
    .line 114
    const-string v19, "BluetoothOppStorageUtility"

    #@115
    new-instance v20, Ljava/lang/StringBuilder;

    #@117
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@11a
    const-string v21, "   Block Size :  "

    #@11c
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11f
    move-result-object v20

    #@120
    move-object/from16 v0, v20

    #@122
    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@125
    move-result-object v20

    #@126
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@129
    move-result-object v20

    #@12a
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12d
    .line 115
    const-string v19, "BluetoothOppStorageUtility"

    #@12f
    new-instance v20, Ljava/lang/StringBuilder;

    #@131
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@134
    const-string v21, "   Available Blocks :  "

    #@136
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@139
    move-result-object v20

    #@13a
    move-object/from16 v0, v20

    #@13c
    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v20

    #@140
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@143
    move-result-object v20

    #@144
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@147
    .line 116
    const-string v19, "BluetoothOppStorageUtility"

    #@149
    new-instance v20, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    const-string v21, "   Total Size :  "

    #@150
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v20

    #@154
    mul-long v21, v17, v6

    #@156
    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@159
    move-result-object v20

    #@15a
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15d
    move-result-object v20

    #@15e
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@161
    .line 117
    const-string v19, "BluetoothOppStorageUtility"

    #@163
    new-instance v20, Ljava/lang/StringBuilder;

    #@165
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@168
    const-string v21, "   Avail Full Size :  "

    #@16a
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v20

    #@16e
    mul-long v21, v2, v6

    #@170
    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@173
    move-result-object v20

    #@174
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@177
    move-result-object v20

    #@178
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@17b
    .line 118
    const-string v19, "BluetoothOppStorageUtility"

    #@17d
    new-instance v20, Ljava/lang/StringBuilder;

    #@17f
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@182
    const-string v21, "   Avail Margin Size :  "

    #@184
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v20

    #@188
    move-object/from16 v0, v20

    #@18a
    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v20

    #@18e
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@191
    move-result-object v20

    #@192
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@195
    .line 119
    const-string v19, "BluetoothOppStorageUtility"

    #@197
    new-instance v20, Ljava/lang/StringBuilder;

    #@199
    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    #@19c
    const-string v21, "   * File Size :  "

    #@19e
    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v20

    #@1a2
    move-object/from16 v0, p0

    #@1a4
    iget-wide v0, v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mFileLength:J

    #@1a6
    move-wide/from16 v21, v0

    #@1a8
    invoke-virtual/range {v20 .. v22}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1ab
    move-result-object v20

    #@1ac
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1af
    move-result-object v20

    #@1b0
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1b3
    .line 120
    const-string v19, "BluetoothOppStorageUtility"

    #@1b5
    const-string v20, "==========================="

    #@1b7
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1ba
    .line 122
    move-object/from16 v0, p0

    #@1bc
    iget-wide v0, v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mFileLength:J

    #@1be
    move-wide/from16 v19, v0

    #@1c0
    cmp-long v19, v4, v19

    #@1c2
    if-gez v19, :cond_1cf

    #@1c4
    .line 123
    const-string v19, "BluetoothOppStorageUtility"

    #@1c6
    const-string v20, "Receive File aborted - not enough free space"

    #@1c8
    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1cb
    .line 124
    const/16 v19, 0x1ee

    #@1cd
    goto/16 :goto_1f

    #@1cf
    .line 132
    :cond_1cf
    sput-object v8, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mCurrentBtFile:Ljava/io/File;

    #@1d1
    .line 134
    const/16 v19, 0xc8

    #@1d3
    goto/16 :goto_1f
.end method

.method public static getBtFile()Ljava/io/File;
    .registers 3

    #@0
    .prologue
    .line 140
    const-string v0, "BluetoothOppStorageUtility"

    #@2
    const-string v1, "getBtFile()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 141
    sget-object v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mCurrentBtFile:Ljava/io/File;

    #@9
    if-nez v0, :cond_40

    #@b
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    const-string v1, "mounted"

    #@11
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14
    move-result v0

    #@15
    if-eqz v0, :cond_40

    #@17
    .line 142
    const-string v0, "BluetoothOppStorageUtility"

    #@19
    const-string v1, "If mCurrentBtFile is null, it is selected the default storage"

    #@1b
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 143
    new-instance v0, Ljava/io/File;

    #@20
    new-instance v1, Ljava/lang/StringBuilder;

    #@22
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@25
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30
    move-result-object v1

    #@31
    const-string v2, "/Bluetooth"

    #@33
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v1

    #@37
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3a
    move-result-object v1

    #@3b
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@3e
    sput-object v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mCurrentBtFile:Ljava/io/File;

    #@40
    .line 145
    :cond_40
    sget-object v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mCurrentBtFile:Ljava/io/File;

    #@42
    return-object v0
.end method

.method public static getBtStoragePath()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 149
    const-string v0, "BluetoothOppStorageUtility"

    #@2
    const-string v1, "getBtStoragePath()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 150
    invoke-static {}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->getBtFile()Ljava/io/File;

    #@a
    move-result-object v0

    #@b
    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    return-object v0
.end method

.method public static isExternalStorageFull()Z
    .registers 3

    #@0
    .prologue
    .line 162
    const-string v0, "BluetoothOppStorageUtility"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "isExternalStorageFull(): mInternalStatus : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget v2, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mExternalStatus:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 163
    sget v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mExternalStatus:I

    #@1c
    const/16 v1, 0x1ee

    #@1e
    if-ne v0, v1, :cond_22

    #@20
    .line 164
    const/4 v0, 0x1

    #@21
    .line 166
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method public static isInternalStorageFull()Z
    .registers 3

    #@0
    .prologue
    .line 154
    const-string v0, "BluetoothOppStorageUtility"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "isInternalStorageFull(): mInternalStatus : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget v2, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mInternalStatus:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 155
    sget v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mInternalStatus:I

    #@1c
    const/16 v1, 0x1ee

    #@1e
    if-ne v0, v1, :cond_22

    #@20
    .line 156
    const/4 v0, 0x1

    #@21
    .line 158
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method public static isNoExternalStorage()Z
    .registers 3

    #@0
    .prologue
    .line 170
    const-string v0, "BluetoothOppStorageUtility"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "isNoExternalStorage(): mInternalStatus : "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    sget v2, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mExternalStatus:I

    #@f
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@12
    move-result-object v1

    #@13
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v1

    #@17
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 171
    sget v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mExternalStatus:I

    #@1c
    const/16 v1, 0x1ed

    #@1e
    if-ne v0, v1, :cond_22

    #@20
    .line 172
    const/4 v0, 0x1

    #@21
    .line 174
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method


# virtual methods
.method public checkExternalStorage()I
    .registers 3

    #@0
    .prologue
    .line 59
    const-string v0, "BluetoothOppStorageUtility"

    #@2
    const-string v1, "Check the external storage"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 60
    const/4 v0, 0x1

    #@8
    invoke-direct {p0, v0}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->checkStorage(Z)I

    #@b
    move-result v0

    #@c
    sput v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mExternalStatus:I

    #@e
    .line 61
    sget v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mExternalStatus:I

    #@10
    return v0
.end method

.method public checkInternalStorage()I
    .registers 3

    #@0
    .prologue
    .line 53
    const-string v0, "BluetoothOppStorageUtility"

    #@2
    const-string v1, "Check the internal storage"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 54
    const/4 v0, 0x0

    #@8
    invoke-direct {p0, v0}, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->checkStorage(Z)I

    #@b
    move-result v0

    #@c
    sput v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mInternalStatus:I

    #@e
    .line 55
    sget v0, Lcom/lge/bluetooth/opp/BluetoothStorageUtility;->mInternalStatus:I

    #@10
    return v0
.end method
