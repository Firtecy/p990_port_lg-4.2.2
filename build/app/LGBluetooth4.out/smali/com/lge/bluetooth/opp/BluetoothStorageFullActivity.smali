.class public Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;
.super Lcom/android/internal/app/AlertActivity;
.source "BluetoothStorageFullActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field private static final D:Z = true

.field private static final TAG:Ljava/lang/String; = "BluetoothOppStorageFullActivity"


# instance fields
.field private mReceiver:Landroid/content/BroadcastReceiver;

.field private mUpdateValues:Landroid/content/ContentValues;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .registers 2

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Lcom/android/internal/app/AlertActivity;-><init>()V

    #@3
    .line 50
    new-instance v0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity$1;

    #@5
    invoke-direct {v0, p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity$1;-><init>(Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;)V

    #@8
    iput-object v0, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    #@a
    return-void
.end method

.method private createView()Landroid/view/View;
    .registers 6

    #@0
    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    #@3
    move-result-object v2

    #@4
    const v3, 0x7f030004

    #@7
    const/4 v4, 0x0

    #@8
    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    #@b
    move-result-object v1

    #@c
    .line 100
    .local v1, view:Landroid/view/View;
    if-eqz v1, :cond_2c

    #@e
    .line 101
    const v2, 0x7f0b000a

    #@11
    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@14
    move-result-object v0

    #@15
    check-cast v0, Landroid/widget/TextView;

    #@17
    .line 102
    .local v0, contentView:Landroid/widget/TextView;
    if-eqz v0, :cond_24

    #@19
    .line 103
    const v2, 0x7f07010e

    #@1c
    invoke-virtual {p0, v2}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->getString(I)Ljava/lang/String;

    #@1f
    move-result-object v2

    #@20
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@23
    .line 120
    .end local v0           #contentView:Landroid/widget/TextView;
    :goto_23
    return-object v1

    #@24
    .line 105
    .restart local v0       #contentView:Landroid/widget/TextView;
    :cond_24
    const-string v2, "BluetoothOppStorageFullActivity"

    #@26
    const-string v3, "[createView] contentView is null"

    #@28
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b
    goto :goto_23

    #@2c
    .line 108
    .end local v0           #contentView:Landroid/widget/TextView;
    :cond_2c
    const-string v2, "BluetoothOppStorageFullActivity"

    #@2e
    const-string v3, "[createView] view is null"

    #@30
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    goto :goto_23
.end method

.method private showDialog()V
    .registers 6

    #@0
    .prologue
    .line 72
    iget-object v1, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mAlertParams:Lcom/android/internal/app/AlertController$AlertParams;

    #@2
    .line 75
    .local v1, p:Lcom/android/internal/app/AlertController$AlertParams;
    const v2, 0x2020239

    #@5
    :try_start_5
    iput v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mIconId:I

    #@7
    .line 76
    const v2, 0x7f070065

    #@a
    invoke-virtual {p0, v2}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->getString(I)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mTitle:Ljava/lang/CharSequence;

    #@10
    .line 77
    invoke-direct {p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->createView()Landroid/view/View;

    #@13
    move-result-object v2

    #@14
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mView:Landroid/view/View;

    #@16
    .line 80
    const v2, 0x7f070038

    #@19
    invoke-virtual {p0, v2}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->getString(I)Ljava/lang/String;

    #@1c
    move-result-object v2

    #@1d
    iput-object v2, v1, Lcom/android/internal/app/AlertController$AlertParams;->mNeutralButtonText:Ljava/lang/CharSequence;

    #@1f
    .line 81
    iput-object p0, v1, Lcom/android/internal/app/AlertController$AlertParams;->mNeutralButtonListener:Landroid/content/DialogInterface$OnClickListener;

    #@21
    .line 89
    invoke-virtual {p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->setupAlert()V
    :try_end_24
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_24} :catch_25

    #@24
    .line 95
    :goto_24
    return-void

    #@25
    .line 90
    :catch_25
    move-exception v0

    #@26
    .line 91
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "BluetoothOppStorageFullActivity"

    #@28
    new-instance v3, Ljava/lang/StringBuilder;

    #@2a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2d
    const-string v4, "showDialog Exception: "

    #@2f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@32
    move-result-object v3

    #@33
    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    #@36
    move-result-object v4

    #@37
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3a
    move-result-object v3

    #@3b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3e
    move-result-object v3

    #@3f
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    .line 92
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    #@45
    goto :goto_24
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .registers 7
    .parameter "dialog"
    .parameter "which"

    #@0
    .prologue
    const/4 v2, 0x3

    #@1
    const/4 v3, 0x0

    #@2
    .line 124
    packed-switch p2, :pswitch_data_76

    #@5
    .line 151
    :goto_5
    return-void

    #@6
    .line 126
    :pswitch_6
    const-string v0, "BluetoothOppStorageFullActivity"

    #@8
    const-string v1, "BUTTON_POSITIVE"

    #@a
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d
    .line 128
    new-instance v0, Landroid/content/ContentValues;

    #@f
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@12
    iput-object v0, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@14
    .line 129
    iget-object v0, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@16
    const-string v1, "confirm"

    #@18
    const/4 v2, 0x1

    #@19
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@20
    .line 130
    invoke-virtual {p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@23
    move-result-object v0

    #@24
    iget-object v1, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUri:Landroid/net/Uri;

    #@26
    iget-object v2, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@28
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@2b
    goto :goto_5

    #@2c
    .line 134
    :pswitch_2c
    const-string v0, "BluetoothOppStorageFullActivity"

    #@2e
    const-string v1, "BUTTON_NEGATIVE"

    #@30
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@33
    .line 136
    new-instance v0, Landroid/content/ContentValues;

    #@35
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@38
    iput-object v0, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@3a
    .line 137
    iget-object v0, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@3c
    const-string v1, "confirm"

    #@3e
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@41
    move-result-object v2

    #@42
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@45
    .line 138
    invoke-virtual {p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@48
    move-result-object v0

    #@49
    iget-object v1, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUri:Landroid/net/Uri;

    #@4b
    iget-object v2, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@4d
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@50
    goto :goto_5

    #@51
    .line 142
    :pswitch_51
    const-string v0, "BluetoothOppStorageFullActivity"

    #@53
    const-string v1, "BUTTON_NEUTRAL"

    #@55
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@58
    .line 144
    new-instance v0, Landroid/content/ContentValues;

    #@5a
    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    #@5d
    iput-object v0, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@5f
    .line 145
    iget-object v0, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@61
    const-string v1, "confirm"

    #@63
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@66
    move-result-object v2

    #@67
    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@6a
    .line 146
    invoke-virtual {p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@6d
    move-result-object v0

    #@6e
    iget-object v1, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUri:Landroid/net/Uri;

    #@70
    iget-object v2, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@72
    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@75
    goto :goto_5

    #@76
    .line 124
    :pswitch_data_76
    .packed-switch -0x3
        :pswitch_51
        :pswitch_2c
        :pswitch_6
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .registers 6
    .parameter "savedInstanceState"

    #@0
    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/android/internal/app/AlertActivity;->onCreate(Landroid/os/Bundle;)V

    #@3
    .line 64
    invoke-virtual {p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->getIntent()Landroid/content/Intent;

    #@6
    move-result-object v0

    #@7
    .line 65
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    #@a
    move-result-object v1

    #@b
    iput-object v1, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUri:Landroid/net/Uri;

    #@d
    .line 67
    invoke-direct {p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->showDialog()V

    #@10
    .line 68
    iget-object v1, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    #@12
    new-instance v2, Landroid/content/IntentFilter;

    #@14
    const-string v3, "android.btopp.intent.action.USER_CONFIRMATION_TIMEOUT"

    #@16
    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@19
    invoke-virtual {p0, v1, v2}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1c
    .line 69
    return-void
.end method

.method protected onDestroy()V
    .registers 3

    #@0
    .prologue
    .line 175
    const-string v0, "BluetoothOppStorageFullActivity"

    #@2
    const-string v1, "onDestroy()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 176
    invoke-super {p0}, Lcom/android/internal/app/AlertActivity;->onDestroy()V

    #@a
    .line 177
    iget-object v0, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mReceiver:Landroid/content/BroadcastReceiver;

    #@c
    invoke-virtual {p0, v0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    #@f
    .line 178
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 8
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    const/4 v0, 0x0

    #@2
    .line 155
    const/4 v1, 0x4

    #@3
    if-ne p1, v1, :cond_3c

    #@5
    .line 156
    const-string v1, "BluetoothOppStorageFullActivity"

    #@7
    const-string v2, "onKeyDown() called; Key: back key"

    #@9
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@c
    .line 157
    new-instance v1, Landroid/content/ContentValues;

    #@e
    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    #@11
    iput-object v1, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@13
    .line 159
    iget-object v1, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@15
    const-string v2, "confirm"

    #@17
    const/4 v3, 0x3

    #@18
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1b
    move-result-object v3

    #@1c
    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@1f
    .line 164
    invoke-virtual {p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->getContentResolver()Landroid/content/ContentResolver;

    #@22
    move-result-object v1

    #@23
    iget-object v2, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUri:Landroid/net/Uri;

    #@25
    iget-object v3, p0, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->mUpdateValues:Landroid/content/ContentValues;

    #@27
    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    #@2a
    .line 166
    const v1, 0x7f070040

    #@2d
    invoke-virtual {p0, v1}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->getString(I)Ljava/lang/String;

    #@30
    move-result-object v1

    #@31
    invoke-static {p0, v1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@34
    move-result-object v0

    #@35
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    #@38
    .line 167
    invoke-virtual {p0}, Lcom/lge/bluetooth/opp/BluetoothStorageFullActivity;->finish()V

    #@3b
    .line 168
    const/4 v0, 0x1

    #@3c
    .line 170
    :cond_3c
    return v0
.end method
