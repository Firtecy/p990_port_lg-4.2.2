.class Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter$1;
.super Landroid/content/BroadcastReceiver;
.source "LGBluetoothServiceAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;


# direct methods
.method constructor <init>(Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 44
    iput-object p1, p0, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter$1;->this$0:Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;

    #@2
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .registers 7
    .parameter "context"
    .parameter "intent"

    #@0
    .prologue
    .line 47
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    .line 49
    .local v0, action:Ljava/lang/String;
    const-string v1, "LGBluetoothServiceAdapter"

    #@6
    new-instance v2, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v3, "onReceive : "

    #@d
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v2

    #@11
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@14
    move-result-object v2

    #@15
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1c
    .line 51
    const-string v1, "com.lge.bluetooth.action.check_bt_wifi_coex"

    #@1e
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@21
    move-result v1

    #@22
    if-eqz v1, :cond_2d

    #@24
    .line 52
    iget-object v1, p0, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter$1;->this$0:Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;

    #@26
    invoke-static {v1}, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->access$000(Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;)Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;

    #@29
    move-result-object v1

    #@2a
    invoke-virtual {v1}, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->checkBtWifiCoex()V

    #@2d
    .line 54
    :cond_2d
    return-void
.end method
