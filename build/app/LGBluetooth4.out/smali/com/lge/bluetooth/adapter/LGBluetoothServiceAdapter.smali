.class public Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;
.super Ljava/lang/Object;
.source "LGBluetoothServiceAdapter.java"


# static fields
.field private static final DBG:Z = true

.field private static final TAG:Ljava/lang/String; = "LGBluetoothServiceAdapter"

.field private static mAdapterContext:Landroid/content/Context;


# instance fields
.field private mBtWifiCoexChecker:Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;

.field private final mReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 5
    .parameter "adapterService"

    #@0
    .prologue
    .line 25
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    .line 44
    new-instance v1, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter$1;

    #@5
    invoke-direct {v1, p0}, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter$1;-><init>(Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;)V

    #@8
    iput-object v1, p0, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mReceiver:Landroid/content/BroadcastReceiver;

    #@a
    .line 26
    sput-object p1, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mAdapterContext:Landroid/content/Context;

    #@c
    .line 27
    new-instance v1, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;

    #@e
    sget-object v2, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mAdapterContext:Landroid/content/Context;

    #@10
    invoke-direct {v1, v2}, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;-><init>(Landroid/content/Context;)V

    #@13
    iput-object v1, p0, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mBtWifiCoexChecker:Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;

    #@15
    .line 28
    new-instance v0, Landroid/content/IntentFilter;

    #@17
    const-string v1, "com.lge.bluetooth.action.check_bt_wifi_coex"

    #@19
    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@1c
    .line 29
    .local v0, filter:Landroid/content/IntentFilter;
    sget-object v1, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mAdapterContext:Landroid/content/Context;

    #@1e
    iget-object v2, p0, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mReceiver:Landroid/content/BroadcastReceiver;

    #@20
    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@23
    .line 30
    return-void
.end method

.method static synthetic access$000(Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;)Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 16
    iget-object v0, p0, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mBtWifiCoexChecker:Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;

    #@2
    return-object v0
.end method


# virtual methods
.method public cleanup()V
    .registers 5

    #@0
    .prologue
    .line 32
    iget-object v1, p0, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mBtWifiCoexChecker:Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;

    #@2
    if-eqz v1, :cond_13

    #@4
    .line 33
    const-string v1, "LGBluetoothServiceAdapter"

    #@6
    const-string v2, " mBtWifiCoexChecker isn\'t null"

    #@8
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@b
    .line 34
    iget-object v1, p0, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mBtWifiCoexChecker:Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;

    #@d
    invoke-virtual {v1}, Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;->cleanup()V

    #@10
    .line 35
    const/4 v1, 0x0

    #@11
    iput-object v1, p0, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mBtWifiCoexChecker:Lcom/lge/bluetooth/app/LGBluetoothWifiCoexChecker;

    #@13
    .line 38
    :cond_13
    :try_start_13
    sget-object v1, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mAdapterContext:Landroid/content/Context;

    #@15
    iget-object v2, p0, Lcom/lge/bluetooth/adapter/LGBluetoothServiceAdapter;->mReceiver:Landroid/content/BroadcastReceiver;

    #@17
    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1a
    .catch Ljava/lang/Exception; {:try_start_13 .. :try_end_1a} :catch_1b

    #@1a
    .line 42
    :goto_1a
    return-void

    #@1b
    .line 39
    :catch_1b
    move-exception v0

    #@1c
    .line 40
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "LGBluetoothServiceAdapter"

    #@1e
    new-instance v2, Ljava/lang/StringBuilder;

    #@20
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@23
    const-string v3, "unregisterReceiver() error :"

    #@25
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@28
    move-result-object v2

    #@29
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v2

    #@2d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v2

    #@31
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    goto :goto_1a
.end method
