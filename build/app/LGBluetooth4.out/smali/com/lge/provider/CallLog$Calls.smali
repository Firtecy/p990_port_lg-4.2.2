.class public Lcom/lge/provider/CallLog$Calls;
.super Landroid/provider/CallLog$Calls;
.source "CallLog.java"

# interfaces
.implements Lcom/lge/provider/CallLog$CallTypeColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/provider/CallLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Calls"
.end annotation


# static fields
.field public static final CACHED_CDNIP:Ljava/lang/String; = "cdnipnumber"

.field public static final CACHED_CNAP:Ljava/lang/String; = "cnapname"

.field public static final CBS_NUMBER:Ljava/lang/String; = "-5"

.field public static final CONTACTS_ID:Ljava/lang/String; = "contacts_id"

.field public static final CONTENT_LAST_INCOMING_CALLS_URI:Landroid/net/Uri; = null

.field public static final CONTENT_LAST_OUTGOING_CALLS_URI:Landroid/net/Uri; = null

.field public static final CONTENT_LOOKUP_URI:Landroid/net/Uri; = null

.field public static final CONTENT_SYNC:Landroid/net/Uri; = null

.field public static final DURATION_VIDEO:Ljava/lang/String; = "duration_video"

.field public static final GROUPCALL_INFO:Ljava/lang/String; = "groupcall_info"

.field public static final GROUPCALL_NUMBER:Ljava/lang/String; = "groupcall_number"

.field public static final ICC_ID:Ljava/lang/String; = "iccid"

.field public static final INCOMING_VT_TYPE:I = 0x5

.field public static final MESSAGE_ID:Ljava/lang/String; = "message_id"

.field public static final MESSAGE_SENSITIVITY_VVM:Ljava/lang/String; = "vvm_sensitivity"

.field public static final MESSAGE_URGENT_VVM:Ljava/lang/String; = "vvm_urgent"

.field public static final MESSAGE_VVM_TYPE:Ljava/lang/String; = "vvm_type"

.field public static final MISSED_VT_TYPE:I = 0x7

.field public static final MODIFIED_TIME:Ljava/lang/String; = "modified_time"

.field public static final OUTGOING_VT_TYPE:I = 0x6

.field public static final PREFIX:Ljava/lang/String; = "prefix"

.field public static final QUICK_MESSAGE:Ljava/lang/String; = "quick_messsage"

.field public static final REJECTED_TYPE:I = 0xa

.field public static final REJECT_VT_TYPE:I = 0x8

.field public static final VIDEOMESSAGE_VT_TYPE:I = 0x9


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 2553
    const-string v0, "content://call_log/calls/out"

    #@2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@5
    move-result-object v0

    #@6
    sput-object v0, Lcom/lge/provider/CallLog$Calls;->CONTENT_LAST_OUTGOING_CALLS_URI:Landroid/net/Uri;

    #@8
    .line 2559
    const-string v0, "content://call_log/calls/in"

    #@a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@d
    move-result-object v0

    #@e
    sput-object v0, Lcom/lge/provider/CallLog$Calls;->CONTENT_LAST_INCOMING_CALLS_URI:Landroid/net/Uri;

    #@10
    .line 2565
    const-string v0, "content://call_log/calls/lookup"

    #@12
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@15
    move-result-object v0

    #@16
    sput-object v0, Lcom/lge/provider/CallLog$Calls;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    #@18
    .line 2571
    const-string v0, "content://call_log/sync"

    #@1a
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1d
    move-result-object v0

    #@1e
    sput-object v0, Lcom/lge/provider/CallLog$Calls;->CONTENT_SYNC:Landroid/net/Uri;

    #@20
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 2547
    invoke-direct {p0}, Landroid/provider/CallLog$Calls;-><init>()V

    #@3
    return-void
.end method

.method public static addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;IIJI)Landroid/net/Uri;
    .registers 24
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"

    #@0
    .prologue
    .line 2628
    const-string v0, "CallLog"

    #@2
    const-string v1, "addCall_1"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2629
    const/4 v3, 0x0

    #@8
    const/4 v9, 0x0

    #@9
    const/4 v10, 0x0

    #@a
    const/4 v11, 0x0

    #@b
    const/4 v12, 0x0

    #@c
    const/4 v13, 0x0

    #@d
    const/4 v14, 0x0

    #@e
    const/4 v15, 0x0

    #@f
    move-object/from16 v0, p0

    #@11
    move-object/from16 v1, p1

    #@13
    move-object/from16 v2, p2

    #@15
    move/from16 v4, p3

    #@17
    move/from16 v5, p4

    #@19
    move-wide/from16 v6, p5

    #@1b
    move/from16 v8, p7

    #@1d
    invoke-static/range {v0 .. v15}, Lcom/lge/provider/CallLog$Calls;->addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIIZZZZLjava/lang/String;)Landroid/net/Uri;

    #@20
    move-result-object v0

    #@21
    return-object v0
.end method

.method public static addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;IIJII)Landroid/net/Uri;
    .registers 25
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"
    .parameter "subscription"

    #@0
    .prologue
    .line 2642
    const-string v0, "CallLog"

    #@2
    const-string v1, "addCall_2"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2643
    const/4 v3, 0x0

    #@8
    const/4 v9, 0x0

    #@9
    const/4 v11, 0x0

    #@a
    const/4 v12, 0x0

    #@b
    const/4 v13, 0x0

    #@c
    const/4 v14, 0x0

    #@d
    const/4 v15, 0x0

    #@e
    move-object/from16 v0, p0

    #@10
    move-object/from16 v1, p1

    #@12
    move-object/from16 v2, p2

    #@14
    move/from16 v4, p3

    #@16
    move/from16 v5, p4

    #@18
    move-wide/from16 v6, p5

    #@1a
    move/from16 v8, p7

    #@1c
    move/from16 v10, p8

    #@1e
    invoke-static/range {v0 .. v15}, Lcom/lge/provider/CallLog$Calls;->addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIIZZZZLjava/lang/String;)Landroid/net/Uri;

    #@21
    move-result-object v0

    #@22
    return-object v0
.end method

.method public static addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;IIJILjava/lang/String;ZZZZ)Landroid/net/Uri;
    .registers 29
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"
    .parameter "iccid"
    .parameter "showP1Number"
    .parameter "showEmptyNumber"
    .parameter "hideCnap"
    .parameter "setCallEntry"

    #@0
    .prologue
    .line 2658
    const-string v0, "CallLog"

    #@2
    const-string v1, "addCall_3"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2659
    const/4 v3, 0x0

    #@8
    const/4 v9, 0x0

    #@9
    const/4 v10, 0x0

    #@a
    const/4 v15, 0x0

    #@b
    move-object/from16 v0, p0

    #@d
    move-object/from16 v1, p1

    #@f
    move-object/from16 v2, p2

    #@11
    move/from16 v4, p3

    #@13
    move/from16 v5, p4

    #@15
    move-wide/from16 v6, p5

    #@17
    move/from16 v8, p7

    #@19
    move/from16 v11, p9

    #@1b
    move/from16 v12, p10

    #@1d
    move/from16 v13, p11

    #@1f
    move/from16 v14, p12

    #@21
    invoke-static/range {v0 .. v15}, Lcom/lge/provider/CallLog$Calls;->addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIIZZZZLjava/lang/String;)Landroid/net/Uri;

    #@24
    move-result-object v0

    #@25
    return-object v0
.end method

.method public static addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIIZZZZ)Landroid/net/Uri;
    .registers 31
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "prefix"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"
    .parameter "durationVideo"
    .parameter "subscription"
    .parameter "showP1Number"
    .parameter "showEmptyNumber"
    .parameter "hideCnap"
    .parameter "setCallEntry"

    #@0
    .prologue
    .line 2725
    const-string v0, "CallLog"

    #@2
    const-string v1, "addCall_6"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2726
    const/4 v15, 0x0

    #@8
    move-object/from16 v0, p0

    #@a
    move-object/from16 v1, p1

    #@c
    move-object/from16 v2, p2

    #@e
    move-object/from16 v3, p3

    #@10
    move/from16 v4, p4

    #@12
    move/from16 v5, p5

    #@14
    move-wide/from16 v6, p6

    #@16
    move/from16 v8, p8

    #@18
    move/from16 v9, p9

    #@1a
    move/from16 v10, p10

    #@1c
    move/from16 v11, p11

    #@1e
    move/from16 v12, p12

    #@20
    move/from16 v13, p13

    #@22
    move/from16 v14, p14

    #@24
    invoke-static/range {v0 .. v15}, Lcom/lge/provider/CallLog$Calls;->addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIIZZZZLjava/lang/String;)Landroid/net/Uri;

    #@27
    move-result-object v0

    #@28
    return-object v0
.end method

.method public static addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIIZZZZLjava/lang/String;)Landroid/net/Uri;
    .registers 34
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "prefix"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"
    .parameter "durationVideo"
    .parameter "subscription"
    .parameter "showP1Number"
    .parameter "showEmptyNumber"
    .parameter "hideCnap"
    .parameter "setCallEntry"
    .parameter "quickMessage"

    #@0
    .prologue
    .line 2756
    const-string v3, "CallLog"

    #@2
    const-string v4, "addCall_7"

    #@4
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2759
    if-eqz p0, :cond_4f8

    #@9
    .line 2760
    const-string v4, "CallLog"

    #@b
    new-instance v3, Ljava/lang/StringBuilder;

    #@d
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@10
    const-string v5, "ci.phoneNumber length : "

    #@12
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@15
    move-result-object v5

    #@16
    move-object/from16 v0, p0

    #@18
    iget-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@1a
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1d
    move-result v3

    #@1e
    if-eqz v3, :cond_4dc

    #@20
    const-string v3, "Empty"

    #@22
    :goto_22
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@25
    move-result-object v3

    #@26
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29
    move-result-object v3

    #@2a
    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    .line 2761
    const-string v4, "CallLog"

    #@2f
    new-instance v3, Ljava/lang/StringBuilder;

    #@31
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@34
    const-string v5, "ci.cdnipNumber length : "

    #@36
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v5

    #@3a
    move-object/from16 v0, p0

    #@3c
    iget-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->cdnipNumber:Ljava/lang/String;

    #@3e
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@41
    move-result v3

    #@42
    if-eqz v3, :cond_4ea

    #@44
    const-string v3, "Empty"

    #@46
    :goto_46
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@49
    move-result-object v3

    #@4a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d
    move-result-object v3

    #@4e
    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@51
    .line 2762
    const-string v3, "CallLog"

    #@53
    new-instance v4, Ljava/lang/StringBuilder;

    #@55
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@58
    const-string v5, "ci.cnapName : "

    #@5a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5d
    move-result-object v4

    #@5e
    move-object/from16 v0, p0

    #@60
    iget-object v5, v0, Lcom/android/internal/telephony/CallerInfo;->cnapName:Ljava/lang/String;

    #@62
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v4

    #@66
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@69
    move-result-object v4

    #@6a
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6d
    .line 2763
    const-string v3, "CallLog"

    #@6f
    new-instance v4, Ljava/lang/StringBuilder;

    #@71
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@74
    const-string v5, "ci.name : "

    #@76
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@79
    move-result-object v4

    #@7a
    move-object/from16 v0, p0

    #@7c
    iget-object v5, v0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@7e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v4

    #@86
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 2764
    const-string v3, "CallLog"

    #@8b
    new-instance v4, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v5, "ci.person_id : "

    #@92
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v4

    #@96
    move-object/from16 v0, p0

    #@98
    iget-wide v5, v0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@9a
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v4

    #@9e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a1
    move-result-object v4

    #@a2
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    .line 2765
    const-string v3, "CallLog"

    #@a7
    new-instance v4, Ljava/lang/StringBuilder;

    #@a9
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ac
    const-string v5, "ci.lookupKey : "

    #@ae
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v4

    #@b2
    move-object/from16 v0, p0

    #@b4
    iget-object v5, v0, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@b6
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b9
    move-result-object v4

    #@ba
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bd
    move-result-object v4

    #@be
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c1
    .line 2771
    :goto_c1
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c4
    move-result v3

    #@c5
    if-nez v3, :cond_501

    #@c7
    .line 2772
    const-string v3, "CallLog"

    #@c9
    new-instance v4, Ljava/lang/StringBuilder;

    #@cb
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ce
    const-string v5, "number length : "

    #@d0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d3
    move-result-object v4

    #@d4
    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->length()I

    #@d7
    move-result v5

    #@d8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@db
    move-result-object v4

    #@dc
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@df
    move-result-object v4

    #@e0
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@e3
    .line 2778
    :goto_e3
    const-string v3, "CallLog"

    #@e5
    new-instance v4, Ljava/lang/StringBuilder;

    #@e7
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ea
    const-string v5, "start : "

    #@ec
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ef
    move-result-object v4

    #@f0
    move-wide/from16 v0, p6

    #@f2
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@f5
    move-result-object v4

    #@f6
    const-string v5, " duration : "

    #@f8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v4

    #@fc
    move/from16 v0, p8

    #@fe
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@101
    move-result-object v4

    #@102
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@105
    move-result-object v4

    #@106
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@109
    .line 2781
    const-string v3, "CallLog"

    #@10b
    new-instance v4, Ljava/lang/StringBuilder;

    #@10d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@110
    const-string v5, "subscription : "

    #@112
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@115
    move-result-object v4

    #@116
    move/from16 v0, p10

    #@118
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@11b
    move-result-object v4

    #@11c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@11f
    move-result-object v4

    #@120
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@123
    .line 2784
    const-string v3, "CallLog"

    #@125
    new-instance v4, Ljava/lang/StringBuilder;

    #@127
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@12a
    const-string v5, "presentation : "

    #@12c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12f
    move-result-object v4

    #@130
    move/from16 v0, p4

    #@132
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@135
    move-result-object v4

    #@136
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@139
    move-result-object v4

    #@13a
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@13d
    .line 2785
    const-string v3, "CallLog"

    #@13f
    new-instance v4, Ljava/lang/StringBuilder;

    #@141
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@144
    const-string v5, "callType : "

    #@146
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@149
    move-result-object v4

    #@14a
    move/from16 v0, p5

    #@14c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14f
    move-result-object v4

    #@150
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@153
    move-result-object v4

    #@154
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@157
    .line 2786
    const-string v3, "CallLog"

    #@159
    new-instance v4, Ljava/lang/StringBuilder;

    #@15b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@15e
    const-string v5, "showP1Number : "

    #@160
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@163
    move-result-object v4

    #@164
    move/from16 v0, p11

    #@166
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@169
    move-result-object v4

    #@16a
    const-string v5, " showEmptyNumber : "

    #@16c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16f
    move-result-object v4

    #@170
    move/from16 v0, p12

    #@172
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@175
    move-result-object v4

    #@176
    const-string v5, " hideCnap : "

    #@178
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@17b
    move-result-object v4

    #@17c
    move/from16 v0, p13

    #@17e
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@181
    move-result-object v4

    #@182
    const-string v5, " setCallEntry : "

    #@184
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@187
    move-result-object v4

    #@188
    move/from16 v0, p14

    #@18a
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@18d
    move-result-object v4

    #@18e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@191
    move-result-object v4

    #@192
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@195
    .line 2788
    const-string v3, "CallLog"

    #@197
    new-instance v4, Ljava/lang/StringBuilder;

    #@199
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@19c
    const-string v5, "quickMessage : "

    #@19e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a1
    move-result-object v4

    #@1a2
    move-object/from16 v0, p15

    #@1a4
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a7
    move-result-object v4

    #@1a8
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1ab
    move-result-object v4

    #@1ac
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1af
    .line 2790
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@1b2
    move-result-object v2

    #@1b3
    .line 2796
    .local v2, resolver:Landroid/content/ContentResolver;
    if-eqz p0, :cond_1d1

    #@1b5
    move-object/from16 v0, p0

    #@1b7
    iget-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@1b9
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1bc
    move-result v3

    #@1bd
    if-nez v3, :cond_1d1

    #@1bf
    .line 2797
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1c2
    move-result v3

    #@1c3
    if-eqz v3, :cond_1d1

    #@1c5
    .line 2798
    move-object/from16 v0, p0

    #@1c7
    iget-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@1c9
    const-string v4, "-"

    #@1cb
    const-string v5, ""

    #@1cd
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@1d0
    move-result-object p2

    #@1d1
    .line 2802
    :cond_1d1
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1d4
    move-result v3

    #@1d5
    if-nez v3, :cond_50a

    #@1d7
    const/4 v3, 0x2

    #@1d8
    move/from16 v0, p5

    #@1da
    if-ne v0, v3, :cond_50a

    #@1dc
    .line 2843
    :cond_1dc
    :goto_1dc
    const/16 v3, 0x337

    #@1de
    move/from16 v0, p5

    #@1e0
    if-ne v0, v3, :cond_1f3

    #@1e2
    .line 2844
    const-string p2, "-5"

    #@1e4
    .line 2845
    if-eqz p0, :cond_1ec

    #@1e6
    .line 2846
    const-string v3, ""

    #@1e8
    move-object/from16 v0, p0

    #@1ea
    iput-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@1ec
    .line 2848
    :cond_1ec
    const-string v3, "CallLog"

    #@1ee
    const-string v4, "Changing the number to CBS cell broadcast"

    #@1f0
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1f3
    .line 2852
    :cond_1f3
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderOperator()Ljava/lang/String;

    #@1f6
    move-result-object v3

    #@1f7
    const-string v4, "SKT"

    #@1f9
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1fc
    move-result v3

    #@1fd
    if-eqz v3, :cond_220

    #@1ff
    const/16 v3, 0xda

    #@201
    move/from16 v0, p5

    #@203
    if-ne v0, v3, :cond_220

    #@205
    .line 2853
    invoke-static {}, Landroid/telephony/TelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@208
    move-result-object v3

    #@209
    invoke-virtual {v3}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    #@20c
    move-result-object v3

    #@20d
    invoke-static {v3}, Lcom/lge/provider/CallLog;->access$000(Ljava/lang/String;)Ljava/lang/String;

    #@210
    move-result-object p2

    #@211
    .line 2854
    if-eqz p0, :cond_219

    #@213
    .line 2855
    const-string v3, ""

    #@215
    move-object/from16 v0, p0

    #@217
    iput-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@219
    .line 2857
    :cond_219
    const-string v3, "CallLog"

    #@21b
    const-string v4, "Changing the number to MissedPhone Service Number"

    #@21d
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@220
    .line 2860
    :cond_220
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderOperator()Ljava/lang/String;

    #@223
    move-result-object v3

    #@224
    const-string v4, "SKT"

    #@226
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@229
    move-result v3

    #@22a
    if-eqz v3, :cond_241

    #@22c
    const/16 v3, 0xdb

    #@22e
    move/from16 v0, p5

    #@230
    if-ne v0, v3, :cond_241

    #@232
    .line 2861
    if-eqz p0, :cond_23a

    #@234
    .line 2862
    const-string v3, ""

    #@236
    move-object/from16 v0, p0

    #@238
    iput-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@23a
    .line 2864
    :cond_23a
    const-string v3, "CallLog"

    #@23c
    const-string v4, "Changing the number to MissedPhone Service Number"

    #@23e
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@241
    .line 2866
    :cond_241
    new-instance v15, Landroid/content/ContentValues;

    #@243
    const/16 v3, 0x10

    #@245
    invoke-direct {v15, v3}, Landroid/content/ContentValues;-><init>(I)V

    #@248
    .line 2869
    .local v15, values:Landroid/content/ContentValues;
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@24b
    move-result v3

    #@24c
    if-nez v3, :cond_294

    #@24e
    const-string v3, "-1"

    #@250
    move-object/from16 v0, p2

    #@252
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@255
    move-result v3

    #@256
    if-nez v3, :cond_294

    #@258
    const-string v3, "-2"

    #@25a
    move-object/from16 v0, p2

    #@25c
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25f
    move-result v3

    #@260
    if-nez v3, :cond_294

    #@262
    const-string v3, "-3"

    #@264
    move-object/from16 v0, p2

    #@266
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@269
    move-result v3

    #@26a
    if-nez v3, :cond_294

    #@26c
    const-string v3, "-4"

    #@26e
    move-object/from16 v0, p2

    #@270
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@273
    move-result v3

    #@274
    if-nez v3, :cond_294

    #@276
    const-string v3, "-5"

    #@278
    move-object/from16 v0, p2

    #@27a
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@27d
    move-result v3

    #@27e
    if-nez v3, :cond_294

    #@280
    const-string v3, "@"

    #@282
    move-object/from16 v0, p2

    #@284
    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@287
    move-result v3

    #@288
    if-nez v3, :cond_294

    #@28a
    .line 2876
    const-string v3, "-"

    #@28c
    const-string v4, ""

    #@28e
    move-object/from16 v0, p2

    #@290
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@293
    move-result-object p2

    #@294
    .line 2883
    :cond_294
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderCountry()Ljava/lang/String;

    #@297
    move-result-object v3

    #@298
    const-string v4, "KR"

    #@29a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@29d
    move-result v3

    #@29e
    if-eqz v3, :cond_2ff

    #@2a0
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderOperator()Ljava/lang/String;

    #@2a3
    move-result-object v3

    #@2a4
    const-string v4, "LGU"

    #@2a6
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a9
    move-result v3

    #@2aa
    if-eqz v3, :cond_2ff

    #@2ac
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@2af
    move-result v3

    #@2b0
    if-nez v3, :cond_2ff

    #@2b2
    const-string v3, "-1"

    #@2b4
    move-object/from16 v0, p2

    #@2b6
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b9
    move-result v3

    #@2ba
    if-nez v3, :cond_2ff

    #@2bc
    const-string v3, "-2"

    #@2be
    move-object/from16 v0, p2

    #@2c0
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2c3
    move-result v3

    #@2c4
    if-nez v3, :cond_2ff

    #@2c6
    const-string v3, "-3"

    #@2c8
    move-object/from16 v0, p2

    #@2ca
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2cd
    move-result v3

    #@2ce
    if-nez v3, :cond_2ff

    #@2d0
    const-string v3, "-4"

    #@2d2
    move-object/from16 v0, p2

    #@2d4
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2d7
    move-result v3

    #@2d8
    if-nez v3, :cond_2ff

    #@2da
    const-string v3, "-5"

    #@2dc
    move-object/from16 v0, p2

    #@2de
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2e1
    move-result v3

    #@2e2
    if-nez v3, :cond_2ff

    #@2e4
    invoke-static/range {p5 .. p5}, Lcom/lge/provider/CallLog$CallTypesFunc;->isIncommingDualType(I)Z

    #@2e7
    move-result v3

    #@2e8
    if-eqz v3, :cond_2ff

    #@2ea
    .line 2892
    new-instance v3, Ljava/lang/StringBuilder;

    #@2ec
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2ef
    move-object/from16 v0, p2

    #@2f1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f4
    move-result-object v3

    #@2f5
    const-string v4, "#"

    #@2f7
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2fa
    move-result-object v3

    #@2fb
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2fe
    move-result-object p2

    #@2ff
    .line 2898
    :cond_2ff
    const-string v3, "number"

    #@301
    move-object/from16 v0, p2

    #@303
    invoke-virtual {v15, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@306
    .line 2899
    const-string v3, "type"

    #@308
    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@30b
    move-result-object v4

    #@30c
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@30f
    .line 2900
    const-string v3, "date"

    #@311
    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@314
    move-result-object v4

    #@315
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@318
    .line 2901
    const-string v3, "duration"

    #@31a
    move/from16 v0, p8

    #@31c
    int-to-long v4, v0

    #@31d
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@320
    move-result-object v4

    #@321
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@324
    .line 2902
    const-string v3, "duration_video"

    #@326
    move/from16 v0, p9

    #@328
    int-to-long v4, v0

    #@329
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@32c
    move-result-object v4

    #@32d
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@330
    .line 2903
    const-string v3, "new"

    #@332
    const/4 v4, 0x1

    #@333
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@336
    move-result-object v4

    #@337
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@33a
    .line 2904
    const-string v3, "prefix"

    #@33c
    move-object/from16 v0, p3

    #@33e
    invoke-virtual {v15, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@341
    .line 2905
    const-string v3, "modified_time"

    #@343
    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@346
    move-result-object v4

    #@347
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@34a
    .line 2906
    const-string v3, "quick_messsage"

    #@34c
    move-object/from16 v0, p15

    #@34e
    invoke-virtual {v15, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@351
    .line 2908
    invoke-static/range {p5 .. p5}, Lcom/lge/provider/CallLog$CallTypesFunc;->isMissedType(I)Z

    #@354
    move-result v3

    #@355
    if-eqz v3, :cond_361

    #@357
    .line 2909
    const-string v3, "is_read"

    #@359
    const/4 v4, 0x0

    #@35a
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@35d
    move-result-object v4

    #@35e
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@361
    .line 2912
    :cond_361
    if-eqz p0, :cond_40d

    #@363
    .line 2913
    const-string v3, "name"

    #@365
    move-object/from16 v0, p0

    #@367
    iget-object v4, v0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@369
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@36c
    .line 2914
    const-string v3, "numbertype"

    #@36e
    move-object/from16 v0, p0

    #@370
    iget v4, v0, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@372
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@375
    move-result-object v4

    #@376
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@379
    .line 2915
    const-string v3, "numberlabel"

    #@37b
    move-object/from16 v0, p0

    #@37d
    iget-object v4, v0, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@37f
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@382
    .line 2917
    move-object/from16 v0, p0

    #@384
    iget-boolean v3, v0, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    #@386
    if-eqz v3, :cond_3d2

    #@388
    .line 2918
    const-string v3, "contacts_id"

    #@38a
    move-object/from16 v0, p0

    #@38c
    iget-wide v4, v0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@38e
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@391
    move-result-object v4

    #@392
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@395
    .line 2919
    const-string v3, "lookup_uri"

    #@397
    move-object/from16 v0, p0

    #@399
    iget-wide v4, v0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@39b
    move-object/from16 v0, p0

    #@39d
    iget-object v6, v0, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@39f
    invoke-static {v4, v5, v6}, Lcom/lge/provider/CallLog;->access$100(JLjava/lang/String;)Landroid/net/Uri;

    #@3a2
    move-result-object v4

    #@3a3
    invoke-static {v4}, Lcom/lge/provider/CallLog;->uriToString(Landroid/net/Uri;)Ljava/lang/String;

    #@3a6
    move-result-object v4

    #@3a7
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3aa
    .line 2920
    const-string v3, "CallLog"

    #@3ac
    new-instance v4, Ljava/lang/StringBuilder;

    #@3ae
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@3b1
    const-string v5, "Lookup Uri : "

    #@3b3
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3b6
    move-result-object v4

    #@3b7
    move-object/from16 v0, p0

    #@3b9
    iget-wide v5, v0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@3bb
    move-object/from16 v0, p0

    #@3bd
    iget-object v7, v0, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@3bf
    invoke-static {v5, v6, v7}, Lcom/lge/provider/CallLog;->access$100(JLjava/lang/String;)Landroid/net/Uri;

    #@3c2
    move-result-object v5

    #@3c3
    invoke-static {v5}, Lcom/lge/provider/CallLog;->uriToString(Landroid/net/Uri;)Ljava/lang/String;

    #@3c6
    move-result-object v5

    #@3c7
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3ca
    move-result-object v4

    #@3cb
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3ce
    move-result-object v4

    #@3cf
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3d2
    .line 2924
    :cond_3d2
    const/4 v3, 0x1

    #@3d3
    move/from16 v0, p13

    #@3d5
    if-eq v0, v3, :cond_3ef

    #@3d7
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderCountry()Ljava/lang/String;

    #@3da
    move-result-object v3

    #@3db
    const-string v4, "KR"

    #@3dd
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3e0
    move-result v3

    #@3e1
    if-eqz v3, :cond_580

    #@3e3
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderOperator()Ljava/lang/String;

    #@3e6
    move-result-object v3

    #@3e7
    const-string v4, "LGT"

    #@3e9
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ec
    move-result v3

    #@3ed
    if-eqz v3, :cond_580

    #@3ef
    .line 2926
    :cond_3ef
    sget v3, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_RESTRICTED:I

    #@3f1
    move/from16 v0, p4

    #@3f3
    if-eq v0, v3, :cond_404

    #@3f5
    sget v3, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_UNKNOWN:I

    #@3f7
    move/from16 v0, p4

    #@3f9
    if-eq v0, v3, :cond_404

    #@3fb
    .line 2927
    const-string v3, "cnapname"

    #@3fd
    move-object/from16 v0, p0

    #@3ff
    iget-object v4, v0, Lcom/android/internal/telephony/CallerInfo;->cnapName:Ljava/lang/String;

    #@401
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@404
    .line 2933
    :cond_404
    :goto_404
    const-string v3, "cdnipnumber"

    #@406
    move-object/from16 v0, p0

    #@408
    iget-object v4, v0, Lcom/android/internal/telephony/CallerInfo;->cdnipNumber:Ljava/lang/String;

    #@40a
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@40d
    .line 2937
    :cond_40d
    invoke-static/range {p10 .. p10}, Lcom/lge/provider/CallLog;->access$200(I)Ljava/lang/String;

    #@410
    move-result-object v10

    #@411
    .line 2938
    .local v10, iccId:Ljava/lang/String;
    const-string v3, "iccid"

    #@413
    invoke-virtual {v15, v3, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@416
    .line 2941
    if-eqz p0, :cond_4b3

    #@418
    move-object/from16 v0, p0

    #@41a
    iget-wide v3, v0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@41c
    const-wide/16 v5, 0x0

    #@41e
    cmp-long v3, v3, v5

    #@420
    if-lez v3, :cond_4b3

    #@422
    .line 2950
    move-object/from16 v0, p0

    #@424
    iget-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->normalizedNumber:Ljava/lang/String;

    #@426
    if-eqz v3, :cond_58b

    #@428
    .line 2951
    move-object/from16 v0, p0

    #@42a
    iget-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->normalizedNumber:Ljava/lang/String;

    #@42c
    const-string v4, "-"

    #@42e
    const-string v5, ""

    #@430
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@433
    move-result-object v13

    #@434
    .line 2952
    .local v13, removedhyphenNumber:Ljava/lang/String;
    const-string v3, " "

    #@436
    const-string v4, ""

    #@438
    invoke-virtual {v13, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@43b
    move-result-object v11

    #@43c
    .line 2953
    .local v11, normalizedPhoneNumber:Ljava/lang/String;
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    #@43e
    const/4 v4, 0x1

    #@43f
    new-array v4, v4, [Ljava/lang/String;

    #@441
    const/4 v5, 0x0

    #@442
    const-string v6, "_id"

    #@444
    aput-object v6, v4, v5

    #@446
    const-string v5, "contact_id =? AND replace(replace(data4 , \'-\', \'\'), \' \', \'\')  =?"

    #@448
    const/4 v6, 0x2

    #@449
    new-array v6, v6, [Ljava/lang/String;

    #@44b
    const/4 v7, 0x0

    #@44c
    move-object/from16 v0, p0

    #@44e
    iget-wide v0, v0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@450
    move-wide/from16 v16, v0

    #@452
    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@455
    move-result-object v16

    #@456
    aput-object v16, v6, v7

    #@458
    const/4 v7, 0x1

    #@459
    aput-object v11, v6, v7

    #@45b
    const/4 v7, 0x0

    #@45c
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@45f
    move-result-object v8

    #@460
    .line 2976
    .end local v11           #normalizedPhoneNumber:Ljava/lang/String;
    .end local v13           #removedhyphenNumber:Ljava/lang/String;
    .local v8, cursor:Landroid/database/Cursor;
    :goto_460
    if-eqz v8, :cond_4b3

    #@462
    .line 2978
    :try_start_462
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    #@465
    move-result v3

    #@466
    if-lez v3, :cond_5c6

    #@468
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@46b
    move-result v3

    #@46c
    if-eqz v3, :cond_5c6

    #@46e
    .line 2979
    const-string v3, "CallLog"

    #@470
    new-instance v4, Ljava/lang/StringBuilder;

    #@472
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@475
    const-string v5, "Update for Favorite : "

    #@477
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47a
    move-result-object v4

    #@47b
    const/4 v5, 0x0

    #@47c
    invoke-interface {v8, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@47f
    move-result-object v5

    #@480
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@483
    move-result-object v4

    #@484
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@487
    move-result-object v4

    #@488
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@48b
    .line 2980
    sget-object v3, Landroid/provider/ContactsContract$DataUsageFeedback;->FEEDBACK_URI:Landroid/net/Uri;

    #@48d
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@490
    move-result-object v3

    #@491
    const/4 v4, 0x0

    #@492
    invoke-interface {v8, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@495
    move-result-object v4

    #@496
    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@499
    move-result-object v3

    #@49a
    const-string v4, "type"

    #@49c
    const-string v5, "call"

    #@49e
    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@4a1
    move-result-object v3

    #@4a2
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@4a5
    move-result-object v9

    #@4a6
    .line 2985
    .local v9, feedbackUri:Landroid/net/Uri;
    new-instance v3, Landroid/content/ContentValues;

    #@4a8
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@4ab
    const/4 v4, 0x0

    #@4ac
    const/4 v5, 0x0

    #@4ad
    invoke-virtual {v2, v9, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_4b0
    .catchall {:try_start_462 .. :try_end_4b0} :catchall_5cf

    #@4b0
    .line 2990
    .end local v9           #feedbackUri:Landroid/net/Uri;
    :goto_4b0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@4b3
    .line 2995
    .end local v8           #cursor:Landroid/database/Cursor;
    :cond_4b3
    sget-object v3, Lcom/lge/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@4b5
    invoke-virtual {v2, v3, v15}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@4b8
    move-result-object v14

    #@4b9
    .line 2996
    .local v14, result:Landroid/net/Uri;
    move-object/from16 v0, p1

    #@4bb
    move/from16 v1, p14

    #@4bd
    invoke-static {v0, v1}, Lcom/lge/provider/CallLog$Calls;->removeExpiredLogEntries(Landroid/content/Context;Z)V

    #@4c0
    .line 2998
    const-string v4, "CallLog"

    #@4c2
    new-instance v3, Ljava/lang/StringBuilder;

    #@4c4
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@4c7
    const-string v5, "Result : "

    #@4c9
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4cc
    move-result-object v5

    #@4cd
    if-eqz v14, :cond_5d4

    #@4cf
    const/4 v3, 0x1

    #@4d0
    :goto_4d0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4d3
    move-result-object v3

    #@4d4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4d7
    move-result-object v3

    #@4d8
    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4db
    .line 2999
    return-object v14

    #@4dc
    .line 2760
    .end local v2           #resolver:Landroid/content/ContentResolver;
    .end local v10           #iccId:Ljava/lang/String;
    .end local v14           #result:Landroid/net/Uri;
    .end local v15           #values:Landroid/content/ContentValues;
    :cond_4dc
    move-object/from16 v0, p0

    #@4de
    iget-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@4e0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@4e3
    move-result v3

    #@4e4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4e7
    move-result-object v3

    #@4e8
    goto/16 :goto_22

    #@4ea
    .line 2761
    :cond_4ea
    move-object/from16 v0, p0

    #@4ec
    iget-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->cdnipNumber:Ljava/lang/String;

    #@4ee
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@4f1
    move-result v3

    #@4f2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@4f5
    move-result-object v3

    #@4f6
    goto/16 :goto_46

    #@4f8
    .line 2767
    :cond_4f8
    const-string v3, "CallLog"

    #@4fa
    const-string v4, "ci is null."

    #@4fc
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4ff
    goto/16 :goto_c1

    #@501
    .line 2774
    :cond_501
    const-string v3, "CallLog"

    #@503
    const-string v4, "number is empty."

    #@505
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@508
    goto/16 :goto_e3

    #@50a
    .line 2805
    .restart local v2       #resolver:Landroid/content/ContentResolver;
    :cond_50a
    sget v3, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_RESTRICTED:I

    #@50c
    move/from16 v0, p4

    #@50e
    if-ne v0, v3, :cond_542

    #@510
    .line 2806
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@513
    move-result v3

    #@514
    if-nez v3, :cond_518

    #@516
    if-nez p11, :cond_530

    #@518
    :cond_518
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderCountry()Ljava/lang/String;

    #@51b
    move-result-object v3

    #@51c
    const-string v4, "KR"

    #@51e
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@521
    move-result v3

    #@522
    if-eqz v3, :cond_536

    #@524
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderOperator()Ljava/lang/String;

    #@527
    move-result-object v3

    #@528
    const-string v4, "KT"

    #@52a
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@52d
    move-result v3

    #@52e
    if-eqz v3, :cond_536

    #@530
    :cond_530
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@533
    move-result v3

    #@534
    if-eqz v3, :cond_1dc

    #@536
    .line 2813
    :cond_536
    const-string p2, "-2"

    #@538
    .line 2814
    if-eqz p0, :cond_1dc

    #@53a
    .line 2815
    const-string v3, ""

    #@53c
    move-object/from16 v0, p0

    #@53e
    iput-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@540
    goto/16 :goto_1dc

    #@542
    .line 2818
    :cond_542
    sget v3, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_PAYPHONE:I

    #@544
    move/from16 v0, p4

    #@546
    if-ne v0, v3, :cond_554

    #@548
    .line 2819
    const-string p2, "-3"

    #@54a
    .line 2820
    if-eqz p0, :cond_1dc

    #@54c
    .line 2821
    const-string v3, ""

    #@54e
    move-object/from16 v0, p0

    #@550
    iput-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@552
    goto/16 :goto_1dc

    #@554
    .line 2823
    :cond_554
    sget v3, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_UNKNOWN:I

    #@556
    move/from16 v0, p4

    #@558
    if-ne v0, v3, :cond_566

    #@55a
    .line 2824
    const-string p2, "-1"

    #@55c
    .line 2825
    if-eqz p0, :cond_1dc

    #@55e
    .line 2826
    const-string v3, ""

    #@560
    move-object/from16 v0, p0

    #@562
    iput-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@564
    goto/16 :goto_1dc

    #@566
    .line 2829
    :cond_566
    invoke-static/range {p2 .. p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@569
    move-result v3

    #@56a
    if-eqz v3, :cond_1dc

    #@56c
    .line 2830
    const/4 v3, 0x1

    #@56d
    move/from16 v0, p12

    #@56f
    if-ne v0, v3, :cond_57d

    #@571
    .line 2831
    const-string p2, "-4"

    #@573
    .line 2835
    :goto_573
    if-eqz p0, :cond_1dc

    #@575
    .line 2836
    const-string v3, ""

    #@577
    move-object/from16 v0, p0

    #@579
    iput-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@57b
    goto/16 :goto_1dc

    #@57d
    .line 2833
    :cond_57d
    const-string p2, "-1"

    #@57f
    goto :goto_573

    #@580
    .line 2930
    .restart local v15       #values:Landroid/content/ContentValues;
    :cond_580
    const-string v3, "cnapname"

    #@582
    move-object/from16 v0, p0

    #@584
    iget-object v4, v0, Lcom/android/internal/telephony/CallerInfo;->cnapName:Ljava/lang/String;

    #@586
    invoke-virtual {v15, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@589
    goto/16 :goto_404

    #@58b
    .line 2963
    .restart local v10       #iccId:Ljava/lang/String;
    :cond_58b
    move-object/from16 v0, p0

    #@58d
    iget-object v3, v0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@58f
    if-eqz v3, :cond_5c3

    #@591
    move-object/from16 v0, p0

    #@593
    iget-object v12, v0, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@595
    .line 2964
    .local v12, phoneNumber:Ljava/lang/String;
    :goto_595
    const-string v3, "-"

    #@597
    const-string v4, ""

    #@599
    invoke-virtual {v12, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@59c
    move-result-object v12

    #@59d
    .line 2965
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    #@59f
    const/4 v4, 0x1

    #@5a0
    new-array v4, v4, [Ljava/lang/String;

    #@5a2
    const/4 v5, 0x0

    #@5a3
    const-string v6, "_id"

    #@5a5
    aput-object v6, v4, v5

    #@5a7
    const-string v5, "contact_id =? AND replace(replace(data1 , \'-\', \'\'), \' \', \'\')  =?"

    #@5a9
    const/4 v6, 0x2

    #@5aa
    new-array v6, v6, [Ljava/lang/String;

    #@5ac
    const/4 v7, 0x0

    #@5ad
    move-object/from16 v0, p0

    #@5af
    iget-wide v0, v0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@5b1
    move-wide/from16 v16, v0

    #@5b3
    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@5b6
    move-result-object v16

    #@5b7
    aput-object v16, v6, v7

    #@5b9
    const/4 v7, 0x1

    #@5ba
    aput-object v12, v6, v7

    #@5bc
    const/4 v7, 0x0

    #@5bd
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@5c0
    move-result-object v8

    #@5c1
    .restart local v8       #cursor:Landroid/database/Cursor;
    goto/16 :goto_460

    #@5c3
    .end local v8           #cursor:Landroid/database/Cursor;
    .end local v12           #phoneNumber:Ljava/lang/String;
    :cond_5c3
    move-object/from16 v12, p2

    #@5c5
    .line 2963
    goto :goto_595

    #@5c6
    .line 2987
    .restart local v8       #cursor:Landroid/database/Cursor;
    :cond_5c6
    :try_start_5c6
    const-string v3, "CallLog"

    #@5c8
    const-string v4, "Not Updated  Favorite!"

    #@5ca
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5cd
    .catchall {:try_start_5c6 .. :try_end_5cd} :catchall_5cf

    #@5cd
    goto/16 :goto_4b0

    #@5cf
    .line 2990
    :catchall_5cf
    move-exception v3

    #@5d0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@5d3
    throw v3

    #@5d4
    .line 2998
    .end local v8           #cursor:Landroid/database/Cursor;
    .restart local v14       #result:Landroid/net/Uri;
    :cond_5d4
    const/4 v3, 0x0

    #@5d5
    goto/16 :goto_4d0
.end method

.method public static addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIZZZZ)Landroid/net/Uri;
    .registers 30
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "prefix"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"
    .parameter "durationVideo"
    .parameter "showP1Number"
    .parameter "showEmptyNumber"
    .parameter "hideCnap"
    .parameter "setCallEntry"

    #@0
    .prologue
    .line 2691
    const-string v0, "CallLog"

    #@2
    const-string v1, "addCall_5"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2692
    const/4 v10, 0x0

    #@8
    const/4 v15, 0x0

    #@9
    move-object/from16 v0, p0

    #@b
    move-object/from16 v1, p1

    #@d
    move-object/from16 v2, p2

    #@f
    move-object/from16 v3, p3

    #@11
    move/from16 v4, p4

    #@13
    move/from16 v5, p5

    #@15
    move-wide/from16 v6, p6

    #@17
    move/from16 v8, p8

    #@19
    move/from16 v9, p9

    #@1b
    move/from16 v11, p10

    #@1d
    move/from16 v12, p11

    #@1f
    move/from16 v13, p12

    #@21
    move/from16 v14, p13

    #@23
    invoke-static/range {v0 .. v15}, Lcom/lge/provider/CallLog$Calls;->addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIIZZZZLjava/lang/String;)Landroid/net/Uri;

    #@26
    move-result-object v0

    #@27
    return-object v0
.end method

.method public static addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIZZZZLjava/lang/String;)Landroid/net/Uri;
    .registers 31
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "prefix"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"
    .parameter "durationVideo"
    .parameter "showP1Number"
    .parameter "showEmptyNumber"
    .parameter "hideCnap"
    .parameter "setCallEntry"
    .parameter "quickMessage"

    #@0
    .prologue
    .line 2708
    const-string v0, "CallLog"

    #@2
    const-string v1, "addCall_5 + (quickMessage)"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2709
    const/4 v10, 0x0

    #@8
    move-object/from16 v0, p0

    #@a
    move-object/from16 v1, p1

    #@c
    move-object/from16 v2, p2

    #@e
    move-object/from16 v3, p3

    #@10
    move/from16 v4, p4

    #@12
    move/from16 v5, p5

    #@14
    move-wide/from16 v6, p6

    #@16
    move/from16 v8, p8

    #@18
    move/from16 v9, p9

    #@1a
    move/from16 v11, p10

    #@1c
    move/from16 v12, p11

    #@1e
    move/from16 v13, p12

    #@20
    move/from16 v14, p13

    #@22
    move-object/from16 v15, p14

    #@24
    invoke-static/range {v0 .. v15}, Lcom/lge/provider/CallLog$Calls;->addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIIZZZZLjava/lang/String;)Landroid/net/Uri;

    #@27
    move-result-object v0

    #@28
    return-object v0
.end method

.method public static addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIZZZZ)Landroid/net/Uri;
    .registers 29
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "prefix"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"
    .parameter "showP1Number"
    .parameter "showEmptyNumber"
    .parameter "hideCnap"
    .parameter "setCallEntry"

    #@0
    .prologue
    .line 2674
    const-string v0, "CallLog"

    #@2
    const-string v1, "addCall_4"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 2675
    const/4 v9, 0x0

    #@8
    const/4 v10, 0x0

    #@9
    const/4 v15, 0x0

    #@a
    move-object/from16 v0, p0

    #@c
    move-object/from16 v1, p1

    #@e
    move-object/from16 v2, p2

    #@10
    move-object/from16 v3, p3

    #@12
    move/from16 v4, p4

    #@14
    move/from16 v5, p5

    #@16
    move-wide/from16 v6, p6

    #@18
    move/from16 v8, p8

    #@1a
    move/from16 v11, p9

    #@1c
    move/from16 v12, p10

    #@1e
    move/from16 v13, p11

    #@20
    move/from16 v14, p12

    #@22
    invoke-static/range {v0 .. v15}, Lcom/lge/provider/CallLog$Calls;->addCall(Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IIJIIIZZZZLjava/lang/String;)Landroid/net/Uri;

    #@25
    move-result-object v0

    #@26
    return-object v0
.end method

.method public static addCall([Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;[Ljava/lang/String;[IIJI[I)Landroid/net/Uri;
    .registers 21
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"
    .parameter "status"

    #@0
    .prologue
    .line 3013
    const-string v0, "CallLog"

    #@2
    const-string v1, "addCall_8"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 3014
    const/4 v9, 0x0

    #@8
    const/4 v10, 0x0

    #@9
    const/4 v11, 0x0

    #@a
    move-object v0, p0

    #@b
    move-object v1, p1

    #@c
    move-object v2, p2

    #@d
    move-object v3, p3

    #@e
    move/from16 v4, p4

    #@10
    move-wide/from16 v5, p5

    #@12
    move/from16 v7, p7

    #@14
    move-object/from16 v8, p8

    #@16
    invoke-static/range {v0 .. v11}, Lcom/lge/provider/CallLog$Calls;->addCall([Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;[Ljava/lang/String;[IIJI[IZZLjava/lang/String;)Landroid/net/Uri;

    #@19
    move-result-object v0

    #@1a
    return-object v0
.end method

.method public static addCall([Lcom/android/internal/telephony/CallerInfo;Landroid/content/Context;[Ljava/lang/String;[IIJI[IZZLjava/lang/String;)Landroid/net/Uri;
    .registers 34
    .parameter "ci"
    .parameter "context"
    .parameter "number"
    .parameter "presentation"
    .parameter "callType"
    .parameter "start"
    .parameter "duration"
    .parameter "status"
    .parameter "showP1Number"
    .parameter "setCallEntry"
    .parameter "quickMessage"

    #@0
    .prologue
    .line 3040
    const/4 v3, 0x0

    #@1
    aget-object v3, p0, v3

    #@3
    if-eqz v3, :cond_1dd

    #@5
    .line 3041
    const-string v4, "CallLog"

    #@7
    new-instance v3, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v5, "ci.phoneNumber length : "

    #@e
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v5

    #@12
    const/4 v3, 0x0

    #@13
    aget-object v3, p0, v3

    #@15
    iget-object v3, v3, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@17
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1a
    move-result v3

    #@1b
    if-eqz v3, :cond_1bf

    #@1d
    const-string v3, "Empty"

    #@1f
    :goto_1f
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 3042
    const-string v4, "CallLog"

    #@2c
    new-instance v3, Ljava/lang/StringBuilder;

    #@2e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@31
    const-string v5, "ci.cdnipNumber length : "

    #@33
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@36
    move-result-object v5

    #@37
    const/4 v3, 0x0

    #@38
    aget-object v3, p0, v3

    #@3a
    iget-object v3, v3, Lcom/android/internal/telephony/CallerInfo;->cdnipNumber:Ljava/lang/String;

    #@3c
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@3f
    move-result v3

    #@40
    if-eqz v3, :cond_1ce

    #@42
    const-string v3, "Empty"

    #@44
    :goto_44
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v3

    #@48
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4b
    move-result-object v3

    #@4c
    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 3043
    const-string v3, "CallLog"

    #@51
    new-instance v4, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v5, "ci.cnapName : "

    #@58
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v4

    #@5c
    const/4 v5, 0x0

    #@5d
    aget-object v5, p0, v5

    #@5f
    iget-object v5, v5, Lcom/android/internal/telephony/CallerInfo;->cnapName:Ljava/lang/String;

    #@61
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@64
    move-result-object v4

    #@65
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v4

    #@69
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 3044
    const-string v3, "CallLog"

    #@6e
    new-instance v4, Ljava/lang/StringBuilder;

    #@70
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@73
    const-string v5, "ci.name : "

    #@75
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@78
    move-result-object v4

    #@79
    const/4 v5, 0x0

    #@7a
    aget-object v5, p0, v5

    #@7c
    iget-object v5, v5, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@7e
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@81
    move-result-object v4

    #@82
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@85
    move-result-object v4

    #@86
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@89
    .line 3045
    const-string v3, "CallLog"

    #@8b
    new-instance v4, Ljava/lang/StringBuilder;

    #@8d
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@90
    const-string v5, "ci.person_id : "

    #@92
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@95
    move-result-object v4

    #@96
    const/4 v5, 0x0

    #@97
    aget-object v5, p0, v5

    #@99
    iget-wide v5, v5, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@9b
    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@9e
    move-result-object v4

    #@9f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a2
    move-result-object v4

    #@a3
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a6
    .line 3046
    const-string v3, "CallLog"

    #@a8
    new-instance v4, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    const-string v5, "ci.lookupKey : "

    #@af
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v4

    #@b3
    const/4 v5, 0x0

    #@b4
    aget-object v5, p0, v5

    #@b6
    iget-object v5, v5, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@b8
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v4

    #@bc
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v4

    #@c0
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    .line 3052
    :goto_c3
    const/4 v3, 0x0

    #@c4
    aget-object v3, p2, v3

    #@c6
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@c9
    move-result v3

    #@ca
    if-nez v3, :cond_1e6

    #@cc
    .line 3053
    const-string v3, "CallLog"

    #@ce
    new-instance v4, Ljava/lang/StringBuilder;

    #@d0
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@d3
    const-string v5, "number length : "

    #@d5
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d8
    move-result-object v4

    #@d9
    const/4 v5, 0x0

    #@da
    aget-object v5, p2, v5

    #@dc
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@df
    move-result v5

    #@e0
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@e3
    move-result-object v4

    #@e4
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@e7
    move-result-object v4

    #@e8
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@eb
    .line 3059
    :goto_eb
    const-string v3, "CallLog"

    #@ed
    new-instance v4, Ljava/lang/StringBuilder;

    #@ef
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@f2
    const-string v5, "start : "

    #@f4
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f7
    move-result-object v4

    #@f8
    move-wide/from16 v0, p5

    #@fa
    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@fd
    move-result-object v4

    #@fe
    const-string v5, " duration : "

    #@100
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@103
    move-result-object v4

    #@104
    move/from16 v0, p7

    #@106
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@109
    move-result-object v4

    #@10a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@10d
    move-result-object v4

    #@10e
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@111
    .line 3062
    const-string v3, "CallLog"

    #@113
    new-instance v4, Ljava/lang/StringBuilder;

    #@115
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@118
    const-string v5, "status : "

    #@11a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11d
    move-result-object v4

    #@11e
    const/4 v5, 0x0

    #@11f
    aget v5, p8, v5

    #@121
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@124
    move-result-object v4

    #@125
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@128
    move-result-object v4

    #@129
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@12c
    .line 3065
    const-string v3, "CallLog"

    #@12e
    new-instance v4, Ljava/lang/StringBuilder;

    #@130
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@133
    const-string v5, "presentation : "

    #@135
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@138
    move-result-object v4

    #@139
    const/4 v5, 0x0

    #@13a
    aget v5, p3, v5

    #@13c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13f
    move-result-object v4

    #@140
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@143
    move-result-object v4

    #@144
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@147
    .line 3066
    const-string v3, "CallLog"

    #@149
    new-instance v4, Ljava/lang/StringBuilder;

    #@14b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@14e
    const-string v5, "callType : "

    #@150
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@153
    move-result-object v4

    #@154
    move/from16 v0, p4

    #@156
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@159
    move-result-object v4

    #@15a
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@15d
    move-result-object v4

    #@15e
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@161
    .line 3067
    const-string v3, "CallLog"

    #@163
    new-instance v4, Ljava/lang/StringBuilder;

    #@165
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@168
    const-string v5, "showP1Number : "

    #@16a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16d
    move-result-object v4

    #@16e
    move/from16 v0, p9

    #@170
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@173
    move-result-object v4

    #@174
    const-string v5, " setCallEntry : "

    #@176
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@179
    move-result-object v4

    #@17a
    move/from16 v0, p10

    #@17c
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@17f
    move-result-object v4

    #@180
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@183
    move-result-object v4

    #@184
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@187
    .line 3070
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@18a
    move-result-object v2

    #@18b
    .line 3071
    .local v2, resolver:Landroid/content/ContentResolver;
    const-string v14, ""

    #@18d
    .line 3072
    .local v14, numberString:Ljava/lang/String;
    const-string v11, ""

    #@18f
    .line 3073
    .local v11, groupCallInfo:Ljava/lang/String;
    const/4 v9, 0x0

    #@190
    .line 3074
    .local v9, entryNumber:I
    const-string v15, ""

    #@192
    .line 3076
    .local v15, oneNumber:Ljava/lang/String;
    move-object/from16 v0, p2

    #@194
    array-length v9, v0

    #@195
    .line 3080
    const/4 v12, 0x0

    #@196
    .local v12, i:I
    :goto_196
    if-ge v12, v9, :cond_246

    #@198
    .line 3081
    aget-object v3, p0, v12

    #@19a
    if-eqz v3, :cond_1ef

    #@19c
    aget-object v3, p0, v12

    #@19e
    iget-object v3, v3, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@1a0
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1a3
    move-result v3

    #@1a4
    if-nez v3, :cond_1ef

    #@1a6
    .line 3082
    aget-object v3, p2, v12

    #@1a8
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1ab
    move-result v3

    #@1ac
    if-eqz v3, :cond_1bc

    #@1ae
    .line 3083
    aget-object v3, p0, v12

    #@1b0
    iget-object v3, v3, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@1b2
    const-string v4, "-"

    #@1b4
    const-string v5, ""

    #@1b6
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@1b9
    move-result-object v3

    #@1ba
    aput-object v3, p2, v12

    #@1bc
    .line 3080
    :cond_1bc
    :goto_1bc
    add-int/lit8 v12, v12, 0x1

    #@1be
    goto :goto_196

    #@1bf
    .line 3041
    .end local v2           #resolver:Landroid/content/ContentResolver;
    .end local v9           #entryNumber:I
    .end local v11           #groupCallInfo:Ljava/lang/String;
    .end local v12           #i:I
    .end local v14           #numberString:Ljava/lang/String;
    .end local v15           #oneNumber:Ljava/lang/String;
    :cond_1bf
    const/4 v3, 0x0

    #@1c0
    aget-object v3, p0, v3

    #@1c2
    iget-object v3, v3, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@1c4
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@1c7
    move-result v3

    #@1c8
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1cb
    move-result-object v3

    #@1cc
    goto/16 :goto_1f

    #@1ce
    .line 3042
    :cond_1ce
    const/4 v3, 0x0

    #@1cf
    aget-object v3, p0, v3

    #@1d1
    iget-object v3, v3, Lcom/android/internal/telephony/CallerInfo;->cdnipNumber:Ljava/lang/String;

    #@1d3
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    #@1d6
    move-result v3

    #@1d7
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1da
    move-result-object v3

    #@1db
    goto/16 :goto_44

    #@1dd
    .line 3048
    :cond_1dd
    const-string v3, "CallLog"

    #@1df
    const-string v4, "ci is null."

    #@1e1
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1e4
    goto/16 :goto_c3

    #@1e6
    .line 3055
    :cond_1e6
    const-string v3, "CallLog"

    #@1e8
    const-string v4, "number is empty."

    #@1ea
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1ed
    goto/16 :goto_eb

    #@1ef
    .line 3086
    .restart local v2       #resolver:Landroid/content/ContentResolver;
    .restart local v9       #entryNumber:I
    .restart local v11       #groupCallInfo:Ljava/lang/String;
    .restart local v12       #i:I
    .restart local v14       #numberString:Ljava/lang/String;
    .restart local v15       #oneNumber:Ljava/lang/String;
    :cond_1ef
    aget v3, p3, v12

    #@1f1
    sget v4, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_RESTRICTED:I

    #@1f3
    if-ne v3, v4, :cond_204

    #@1f5
    .line 3087
    const-string v3, "-2"

    #@1f7
    aput-object v3, p2, v12

    #@1f9
    .line 3088
    aget-object v3, p0, v12

    #@1fb
    if-eqz v3, :cond_1bc

    #@1fd
    .line 3089
    aget-object v3, p0, v12

    #@1ff
    const-string v4, ""

    #@201
    iput-object v4, v3, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@203
    goto :goto_1bc

    #@204
    .line 3091
    :cond_204
    aget v3, p3, v12

    #@206
    sget v4, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_PAYPHONE:I

    #@208
    if-ne v3, v4, :cond_219

    #@20a
    .line 3092
    const-string v3, "-3"

    #@20c
    aput-object v3, p2, v12

    #@20e
    .line 3093
    aget-object v3, p0, v12

    #@210
    if-eqz v3, :cond_1bc

    #@212
    .line 3094
    aget-object v3, p0, v12

    #@214
    const-string v4, ""

    #@216
    iput-object v4, v3, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@218
    goto :goto_1bc

    #@219
    .line 3096
    :cond_219
    aget v3, p3, v12

    #@21b
    sget v4, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_UNKNOWN:I

    #@21d
    if-ne v3, v4, :cond_22e

    #@21f
    .line 3097
    const-string v3, "-1"

    #@221
    aput-object v3, p2, v12

    #@223
    .line 3098
    aget-object v3, p0, v12

    #@225
    if-eqz v3, :cond_1bc

    #@227
    .line 3099
    aget-object v3, p0, v12

    #@229
    const-string v4, ""

    #@22b
    iput-object v4, v3, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@22d
    goto :goto_1bc

    #@22e
    .line 3102
    :cond_22e
    aget-object v3, p2, v12

    #@230
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@233
    move-result v3

    #@234
    if-eqz v3, :cond_1bc

    #@236
    .line 3103
    const-string v3, "-4"

    #@238
    aput-object v3, p2, v12

    #@23a
    .line 3104
    aget-object v3, p0, v12

    #@23c
    if-eqz v3, :cond_1bc

    #@23e
    .line 3105
    aget-object v3, p0, v12

    #@240
    const-string v4, ""

    #@242
    iput-object v4, v3, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@244
    goto/16 :goto_1bc

    #@246
    .line 3112
    :cond_246
    const/4 v12, 0x0

    #@247
    :goto_247
    if-ge v12, v9, :cond_29f

    #@249
    .line 3113
    new-instance v3, Ljava/lang/StringBuilder;

    #@24b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@24e
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@251
    move-result-object v3

    #@252
    aget-object v4, p2, v12

    #@254
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@257
    move-result-object v3

    #@258
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25b
    move-result-object v14

    #@25c
    .line 3114
    new-instance v3, Ljava/lang/StringBuilder;

    #@25e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@261
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@264
    move-result-object v3

    #@265
    aget v4, p8, v12

    #@267
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26a
    move-result-object v3

    #@26b
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26e
    move-result-object v11

    #@26f
    .line 3115
    const/4 v3, 0x0

    #@270
    aget-object v15, p2, v3

    #@272
    .line 3117
    add-int/lit8 v3, v9, -0x1

    #@274
    if-eq v12, v3, :cond_29c

    #@276
    .line 3118
    new-instance v3, Ljava/lang/StringBuilder;

    #@278
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@27b
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@27e
    move-result-object v3

    #@27f
    const-string v4, "|"

    #@281
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@284
    move-result-object v3

    #@285
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@288
    move-result-object v14

    #@289
    .line 3119
    new-instance v3, Ljava/lang/StringBuilder;

    #@28b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@28e
    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@291
    move-result-object v3

    #@292
    const-string v4, "|"

    #@294
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@297
    move-result-object v3

    #@298
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@29b
    move-result-object v11

    #@29c
    .line 3112
    :cond_29c
    add-int/lit8 v12, v12, 0x1

    #@29e
    goto :goto_247

    #@29f
    .line 3123
    :cond_29f
    new-instance v19, Landroid/content/ContentValues;

    #@2a1
    const/16 v3, 0xb

    #@2a3
    move-object/from16 v0, v19

    #@2a5
    invoke-direct {v0, v3}, Landroid/content/ContentValues;-><init>(I)V

    #@2a8
    .line 3125
    .local v19, values:Landroid/content/ContentValues;
    const-string v3, "number"

    #@2aa
    move-object/from16 v0, v19

    #@2ac
    invoke-virtual {v0, v3, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2af
    .line 3126
    const-string v3, "type"

    #@2b1
    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2b4
    move-result-object v4

    #@2b5
    move-object/from16 v0, v19

    #@2b7
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2ba
    .line 3127
    const-string v3, "date"

    #@2bc
    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2bf
    move-result-object v4

    #@2c0
    move-object/from16 v0, v19

    #@2c2
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@2c5
    .line 3128
    const-string v3, "duration"

    #@2c7
    move/from16 v0, p7

    #@2c9
    int-to-long v4, v0

    #@2ca
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2cd
    move-result-object v4

    #@2ce
    move-object/from16 v0, v19

    #@2d0
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@2d3
    .line 3129
    const-string v3, "new"

    #@2d5
    const/4 v4, 0x1

    #@2d6
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@2d9
    move-result-object v4

    #@2da
    move-object/from16 v0, v19

    #@2dc
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@2df
    .line 3130
    const-string v3, "groupcall_info"

    #@2e1
    move-object/from16 v0, v19

    #@2e3
    invoke-virtual {v0, v3, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2e6
    .line 3131
    const-string v3, "groupcall_number"

    #@2e8
    move-object/from16 v0, v19

    #@2ea
    invoke-virtual {v0, v3, v14}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@2ed
    .line 3132
    const-string v3, "modified_time"

    #@2ef
    invoke-static/range {p5 .. p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@2f2
    move-result-object v4

    #@2f3
    move-object/from16 v0, v19

    #@2f5
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@2f8
    .line 3133
    const-string v3, "quick_messsage"

    #@2fa
    move-object/from16 v0, v19

    #@2fc
    move-object/from16 v1, p11

    #@2fe
    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@301
    .line 3135
    invoke-static/range {p4 .. p4}, Lcom/lge/provider/CallLog$CallTypesFunc;->isMissedType(I)Z

    #@304
    move-result v3

    #@305
    if-eqz v3, :cond_313

    #@307
    .line 3136
    const-string v3, "is_read"

    #@309
    const/4 v4, 0x0

    #@30a
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@30d
    move-result-object v4

    #@30e
    move-object/from16 v0, v19

    #@310
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@313
    .line 3139
    :cond_313
    if-eqz p0, :cond_3d5

    #@315
    .line 3141
    const-string v3, "name"

    #@317
    const/4 v4, 0x0

    #@318
    aget-object v4, p0, v4

    #@31a
    iget-object v4, v4, Lcom/android/internal/telephony/CallerInfo;->name:Ljava/lang/String;

    #@31c
    move-object/from16 v0, v19

    #@31e
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@321
    .line 3142
    const-string v3, "numbertype"

    #@323
    const/4 v4, 0x0

    #@324
    aget-object v4, p0, v4

    #@326
    iget v4, v4, Lcom/android/internal/telephony/CallerInfo;->numberType:I

    #@328
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@32b
    move-result-object v4

    #@32c
    move-object/from16 v0, v19

    #@32e
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    #@331
    .line 3143
    const-string v3, "numberlabel"

    #@333
    const/4 v4, 0x0

    #@334
    aget-object v4, p0, v4

    #@336
    iget-object v4, v4, Lcom/android/internal/telephony/CallerInfo;->numberLabel:Ljava/lang/String;

    #@338
    move-object/from16 v0, v19

    #@33a
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@33d
    .line 3145
    const/4 v3, 0x0

    #@33e
    aget-object v3, p0, v3

    #@340
    iget-boolean v3, v3, Lcom/android/internal/telephony/CallerInfo;->contactExists:Z

    #@342
    if-eqz v3, :cond_397

    #@344
    .line 3146
    const-string v3, "contacts_id"

    #@346
    const/4 v4, 0x0

    #@347
    aget-object v4, p0, v4

    #@349
    iget-wide v4, v4, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@34b
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    #@34e
    move-result-object v4

    #@34f
    move-object/from16 v0, v19

    #@351
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    #@354
    .line 3147
    const-string v3, "lookup_uri"

    #@356
    const/4 v4, 0x0

    #@357
    aget-object v4, p0, v4

    #@359
    iget-wide v4, v4, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@35b
    const/4 v6, 0x0

    #@35c
    aget-object v6, p0, v6

    #@35e
    iget-object v6, v6, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@360
    invoke-static {v4, v5, v6}, Lcom/lge/provider/CallLog;->access$100(JLjava/lang/String;)Landroid/net/Uri;

    #@363
    move-result-object v4

    #@364
    invoke-static {v4}, Lcom/lge/provider/CallLog;->uriToString(Landroid/net/Uri;)Ljava/lang/String;

    #@367
    move-result-object v4

    #@368
    move-object/from16 v0, v19

    #@36a
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@36d
    .line 3148
    const-string v3, "CallLog"

    #@36f
    new-instance v4, Ljava/lang/StringBuilder;

    #@371
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@374
    const-string v5, "Lookup Uri : "

    #@376
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@379
    move-result-object v4

    #@37a
    const/4 v5, 0x0

    #@37b
    aget-object v5, p0, v5

    #@37d
    iget-wide v5, v5, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@37f
    const/4 v7, 0x0

    #@380
    aget-object v7, p0, v7

    #@382
    iget-object v7, v7, Lcom/android/internal/telephony/CallerInfo;->lookupKey:Ljava/lang/String;

    #@384
    invoke-static {v5, v6, v7}, Lcom/lge/provider/CallLog;->access$100(JLjava/lang/String;)Landroid/net/Uri;

    #@387
    move-result-object v5

    #@388
    invoke-static {v5}, Lcom/lge/provider/CallLog;->uriToString(Landroid/net/Uri;)Ljava/lang/String;

    #@38b
    move-result-object v5

    #@38c
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38f
    move-result-object v4

    #@390
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@393
    move-result-object v4

    #@394
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@397
    .line 3152
    :cond_397
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderCountry()Ljava/lang/String;

    #@39a
    move-result-object v3

    #@39b
    const-string v4, "KR"

    #@39d
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3a0
    move-result v3

    #@3a1
    if-eqz v3, :cond_4a6

    #@3a3
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderOperator()Ljava/lang/String;

    #@3a6
    move-result-object v3

    #@3a7
    const-string v4, "LGT"

    #@3a9
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@3ac
    move-result v3

    #@3ad
    if-eqz v3, :cond_4a6

    #@3af
    .line 3153
    const/4 v3, 0x0

    #@3b0
    aget v3, p3, v3

    #@3b2
    sget v4, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_RESTRICTED:I

    #@3b4
    if-eq v3, v4, :cond_3c9

    #@3b6
    const/4 v3, 0x0

    #@3b7
    aget v3, p3, v3

    #@3b9
    sget v4, Lcom/android/internal/telephony/PhoneConstants;->PRESENTATION_UNKNOWN:I

    #@3bb
    if-eq v3, v4, :cond_3c9

    #@3bd
    .line 3154
    const-string v3, "cnapname"

    #@3bf
    const/4 v4, 0x0

    #@3c0
    aget-object v4, p0, v4

    #@3c2
    iget-object v4, v4, Lcom/android/internal/telephony/CallerInfo;->cnapName:Ljava/lang/String;

    #@3c4
    move-object/from16 v0, v19

    #@3c6
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3c9
    .line 3160
    :cond_3c9
    :goto_3c9
    const-string v3, "cdnipnumber"

    #@3cb
    const/4 v4, 0x0

    #@3cc
    aget-object v4, p0, v4

    #@3ce
    iget-object v4, v4, Lcom/android/internal/telephony/CallerInfo;->cdnipNumber:Ljava/lang/String;

    #@3d0
    move-object/from16 v0, v19

    #@3d2
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@3d5
    .line 3163
    :cond_3d5
    if-eqz p0, :cond_47b

    #@3d7
    const/4 v3, 0x0

    #@3d8
    aget-object v3, p0, v3

    #@3da
    iget-wide v3, v3, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@3dc
    const-wide/16 v5, 0x0

    #@3de
    cmp-long v3, v3, v5

    #@3e0
    if-lez v3, :cond_47b

    #@3e2
    .line 3172
    const/4 v3, 0x0

    #@3e3
    aget-object v3, p0, v3

    #@3e5
    iget-object v3, v3, Lcom/android/internal/telephony/CallerInfo;->normalizedNumber:Ljava/lang/String;

    #@3e7
    if-eqz v3, :cond_4b4

    #@3e9
    .line 3173
    const/4 v3, 0x0

    #@3ea
    aget-object v3, p0, v3

    #@3ec
    iget-object v3, v3, Lcom/android/internal/telephony/CallerInfo;->normalizedNumber:Ljava/lang/String;

    #@3ee
    const-string v4, "-"

    #@3f0
    const-string v5, ""

    #@3f2
    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@3f5
    move-result-object v17

    #@3f6
    .line 3174
    .local v17, removedhyphenNumber:Ljava/lang/String;
    const-string v3, " "

    #@3f8
    const-string v4, ""

    #@3fa
    move-object/from16 v0, v17

    #@3fc
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@3ff
    move-result-object v13

    #@400
    .line 3175
    .local v13, normalizedPhoneNumber:Ljava/lang/String;
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    #@402
    const/4 v4, 0x1

    #@403
    new-array v4, v4, [Ljava/lang/String;

    #@405
    const/4 v5, 0x0

    #@406
    const-string v6, "_id"

    #@408
    aput-object v6, v4, v5

    #@40a
    const-string v5, "contact_id =? AND replace(replace(data4 , \'-\', \'\'), \' \', \'\')  =?"

    #@40c
    const/4 v6, 0x2

    #@40d
    new-array v6, v6, [Ljava/lang/String;

    #@40f
    const/4 v7, 0x0

    #@410
    const/16 v20, 0x0

    #@412
    aget-object v20, p0, v20

    #@414
    move-object/from16 v0, v20

    #@416
    iget-wide v0, v0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@418
    move-wide/from16 v20, v0

    #@41a
    invoke-static/range {v20 .. v21}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@41d
    move-result-object v20

    #@41e
    aput-object v20, v6, v7

    #@420
    const/4 v7, 0x1

    #@421
    aput-object v13, v6, v7

    #@423
    const/4 v7, 0x0

    #@424
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@427
    move-result-object v8

    #@428
    .line 3199
    .end local v13           #normalizedPhoneNumber:Ljava/lang/String;
    .end local v17           #removedhyphenNumber:Ljava/lang/String;
    .local v8, cursor:Landroid/database/Cursor;
    :goto_428
    if-eqz v8, :cond_47b

    #@42a
    .line 3201
    :try_start_42a
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    #@42d
    move-result v3

    #@42e
    if-lez v3, :cond_4f9

    #@430
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    #@433
    move-result v3

    #@434
    if-eqz v3, :cond_4f9

    #@436
    .line 3202
    const-string v3, "CallLog"

    #@438
    new-instance v4, Ljava/lang/StringBuilder;

    #@43a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@43d
    const-string v5, "Update for Favorite : "

    #@43f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@442
    move-result-object v4

    #@443
    const/4 v5, 0x0

    #@444
    invoke-interface {v8, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@447
    move-result-object v5

    #@448
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44b
    move-result-object v4

    #@44c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@44f
    move-result-object v4

    #@450
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@453
    .line 3203
    sget-object v3, Landroid/provider/ContactsContract$DataUsageFeedback;->FEEDBACK_URI:Landroid/net/Uri;

    #@455
    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    #@458
    move-result-object v3

    #@459
    const/4 v4, 0x0

    #@45a
    invoke-interface {v8, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@45d
    move-result-object v4

    #@45e
    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@461
    move-result-object v3

    #@462
    const-string v4, "type"

    #@464
    const-string v5, "call"

    #@466
    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    #@469
    move-result-object v3

    #@46a
    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    #@46d
    move-result-object v10

    #@46e
    .line 3208
    .local v10, feedbackUri:Landroid/net/Uri;
    new-instance v3, Landroid/content/ContentValues;

    #@470
    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    #@473
    const/4 v4, 0x0

    #@474
    const/4 v5, 0x0

    #@475
    invoke-virtual {v2, v10, v3, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_478
    .catchall {:try_start_42a .. :try_end_478} :catchall_502

    #@478
    .line 3213
    .end local v10           #feedbackUri:Landroid/net/Uri;
    :goto_478
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@47b
    .line 3218
    .end local v8           #cursor:Landroid/database/Cursor;
    :cond_47b
    sget-object v3, Lcom/lge/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@47d
    move-object/from16 v0, v19

    #@47f
    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    #@482
    move-result-object v18

    #@483
    .line 3220
    .local v18, result:Landroid/net/Uri;
    move-object/from16 v0, p1

    #@485
    move/from16 v1, p10

    #@487
    invoke-static {v0, v1}, Lcom/lge/provider/CallLog$Calls;->removeExpiredLogEntries(Landroid/content/Context;Z)V

    #@48a
    .line 3221
    const-string v4, "CallLog"

    #@48c
    new-instance v3, Ljava/lang/StringBuilder;

    #@48e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@491
    const-string v5, "Result : "

    #@493
    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@496
    move-result-object v5

    #@497
    if-eqz v18, :cond_507

    #@499
    const/4 v3, 0x1

    #@49a
    :goto_49a
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@49d
    move-result-object v3

    #@49e
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a1
    move-result-object v3

    #@4a2
    invoke-static {v4, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4a5
    .line 3222
    return-object v18

    #@4a6
    .line 3157
    .end local v18           #result:Landroid/net/Uri;
    :cond_4a6
    const-string v3, "cnapname"

    #@4a8
    const/4 v4, 0x0

    #@4a9
    aget-object v4, p0, v4

    #@4ab
    iget-object v4, v4, Lcom/android/internal/telephony/CallerInfo;->cnapName:Ljava/lang/String;

    #@4ad
    move-object/from16 v0, v19

    #@4af
    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    #@4b2
    goto/16 :goto_3c9

    #@4b4
    .line 3185
    :cond_4b4
    const/4 v3, 0x0

    #@4b5
    aget-object v3, p0, v3

    #@4b7
    iget-object v3, v3, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@4b9
    if-eqz v3, :cond_4f6

    #@4bb
    const/4 v3, 0x0

    #@4bc
    aget-object v3, p0, v3

    #@4be
    iget-object v0, v3, Lcom/android/internal/telephony/CallerInfo;->phoneNumber:Ljava/lang/String;

    #@4c0
    move-object/from16 v16, v0

    #@4c2
    .line 3187
    .local v16, phoneNumber:Ljava/lang/String;
    :goto_4c2
    const-string v3, "-"

    #@4c4
    const-string v4, ""

    #@4c6
    move-object/from16 v0, v16

    #@4c8
    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    #@4cb
    move-result-object v16

    #@4cc
    .line 3188
    sget-object v3, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->CONTENT_URI:Landroid/net/Uri;

    #@4ce
    const/4 v4, 0x1

    #@4cf
    new-array v4, v4, [Ljava/lang/String;

    #@4d1
    const/4 v5, 0x0

    #@4d2
    const-string v6, "_id"

    #@4d4
    aput-object v6, v4, v5

    #@4d6
    const-string v5, "contact_id =? AND replace(replace(data1 , \'-\', \'\'), \' \', \'\')  =?"

    #@4d8
    const/4 v6, 0x2

    #@4d9
    new-array v6, v6, [Ljava/lang/String;

    #@4db
    const/4 v7, 0x0

    #@4dc
    const/16 v20, 0x0

    #@4de
    aget-object v20, p0, v20

    #@4e0
    move-object/from16 v0, v20

    #@4e2
    iget-wide v0, v0, Lcom/android/internal/telephony/CallerInfo;->person_id:J

    #@4e4
    move-wide/from16 v20, v0

    #@4e6
    invoke-static/range {v20 .. v21}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@4e9
    move-result-object v20

    #@4ea
    aput-object v20, v6, v7

    #@4ec
    const/4 v7, 0x1

    #@4ed
    aput-object v16, v6, v7

    #@4ef
    const/4 v7, 0x0

    #@4f0
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@4f3
    move-result-object v8

    #@4f4
    .restart local v8       #cursor:Landroid/database/Cursor;
    goto/16 :goto_428

    #@4f6
    .end local v8           #cursor:Landroid/database/Cursor;
    .end local v16           #phoneNumber:Ljava/lang/String;
    :cond_4f6
    move-object/from16 v16, v15

    #@4f8
    .line 3185
    goto :goto_4c2

    #@4f9
    .line 3210
    .restart local v8       #cursor:Landroid/database/Cursor;
    :cond_4f9
    :try_start_4f9
    const-string v3, "CallLog"

    #@4fb
    const-string v4, "Not Updated  Favorite!"

    #@4fd
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_500
    .catchall {:try_start_4f9 .. :try_end_500} :catchall_502

    #@500
    goto/16 :goto_478

    #@502
    .line 3213
    :catchall_502
    move-exception v3

    #@503
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    #@506
    throw v3

    #@507
    .line 3221
    .end local v8           #cursor:Landroid/database/Cursor;
    .restart local v18       #result:Landroid/net/Uri;
    :cond_507
    const/4 v3, 0x0

    #@508
    goto :goto_49a
.end method

.method private static getBuilderCountry()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3275
    const-string v0, ""

    #@2
    .line 3277
    .local v0, CUNTURY:Ljava/lang/String;
    :try_start_2
    const-string v1, "ro.build.target_country"

    #@4
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_7} :catch_9

    #@7
    move-result-object v0

    #@8
    .line 3280
    :goto_8
    return-object v0

    #@9
    .line 3278
    :catch_9
    move-exception v1

    #@a
    goto :goto_8
.end method

.method private static getBuilderOperator()Ljava/lang/String;
    .registers 2

    #@0
    .prologue
    .line 3284
    const-string v0, ""

    #@2
    .line 3286
    .local v0, OPERATOR:Ljava/lang/String;
    :try_start_2
    const-string v1, "ro.build.target_operator"

    #@4
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;
    :try_end_7
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_7} :catch_9

    #@7
    move-result-object v0

    #@8
    .line 3289
    :goto_8
    return-object v0

    #@9
    .line 3287
    :catch_9
    move-exception v1

    #@a
    goto :goto_8
.end method

.method public static getLastOutgoingCall(Landroid/content/Context;)Ljava/lang/String;
    .registers 8
    .parameter "context"

    #@0
    .prologue
    .line 3233
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@3
    move-result-object v0

    #@4
    .line 3234
    .local v0, resolver:Landroid/content/ContentResolver;
    const/4 v6, 0x0

    #@5
    .line 3236
    .local v6, c:Landroid/database/Cursor;
    :try_start_5
    sget-object v1, Lcom/lge/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@7
    const/4 v2, 0x1

    #@8
    new-array v2, v2, [Ljava/lang/String;

    #@a
    const/4 v3, 0x0

    #@b
    const-string v4, "number"

    #@d
    aput-object v4, v2, v3

    #@f
    const-string v3, "type=2 or type=12 or type=1111 or type=1112 or type=6502 or type=10002"

    #@11
    const/4 v4, 0x0

    #@12
    const-string v5, "date DESC LIMIT 1"

    #@14
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@17
    move-result-object v6

    #@18
    .line 3248
    if-eqz v6, :cond_20

    #@1a
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@1d
    move-result v1

    #@1e
    if-nez v1, :cond_28

    #@20
    .line 3249
    :cond_20
    const-string v1, ""
    :try_end_22
    .catchall {:try_start_5 .. :try_end_22} :catchall_33

    #@22
    .line 3253
    if-eqz v6, :cond_27

    #@24
    .line 3254
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@27
    :cond_27
    :goto_27
    return-object v1

    #@28
    .line 3251
    :cond_28
    const/4 v1, 0x0

    #@29
    :try_start_29
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_2c
    .catchall {:try_start_29 .. :try_end_2c} :catchall_33

    #@2c
    move-result-object v1

    #@2d
    .line 3253
    if-eqz v6, :cond_27

    #@2f
    .line 3254
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@32
    goto :goto_27

    #@33
    .line 3253
    :catchall_33
    move-exception v1

    #@34
    if-eqz v6, :cond_39

    #@36
    .line 3254
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@39
    :cond_39
    throw v1
.end method

.method private static removeExpiredLogEntries(Landroid/content/Context;Z)V
    .registers 6
    .parameter "context"
    .parameter "setCallEntry"

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 3260
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@4
    move-result-object v0

    #@5
    .line 3262
    .local v0, resolver:Landroid/content/ContentResolver;
    const/4 v1, 0x1

    #@6
    if-eq p1, v1, :cond_20

    #@8
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderCountry()Ljava/lang/String;

    #@b
    move-result-object v1

    #@c
    const-string v2, "KR"

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_28

    #@14
    invoke-static {}, Lcom/lge/provider/CallLog$Calls;->getBuilderOperator()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    const-string v2, "SKT"

    #@1a
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1d
    move-result v1

    #@1e
    if-eqz v1, :cond_28

    #@20
    .line 3264
    :cond_20
    sget-object v1, Lcom/lge/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@22
    const-string v2, "_id IN (SELECT _id FROM calls ORDER BY date DESC LIMIT -1 OFFSET 1000)"

    #@24
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@27
    .line 3272
    :goto_27
    return-void

    #@28
    .line 3268
    :cond_28
    sget-object v1, Lcom/lge/provider/CallLog$Calls;->CONTENT_URI:Landroid/net/Uri;

    #@2a
    const-string v2, "_id IN (SELECT _id FROM calls ORDER BY date DESC LIMIT -1 OFFSET 500)"

    #@2c
    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    #@2f
    goto :goto_27
.end method
