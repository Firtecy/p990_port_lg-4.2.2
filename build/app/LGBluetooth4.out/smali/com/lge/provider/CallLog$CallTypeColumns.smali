.class public interface abstract Lcom/lge/provider/CallLog$CallTypeColumns;
.super Ljava/lang/Object;
.source "CallLog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/provider/CallLog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CallTypeColumns"
.end annotation


# static fields
.field public static final CALLTYPE_CBS_RECEIVE:I = 0x337

.field public static final CALLTYPE_CHAT_SEND:I = 0x38f

.field public static final CALLTYPE_EMAIL_BASE:I = 0x1f40

.field public static final CALLTYPE_IM_CONNECT:I = 0x32b

.field public static final CALLTYPE_IM_CONNECTFAIL:I = 0x32c

.field public static final CALLTYPE_INCOMING_VT_TYPE_DUALNUM:I = 0x270

.field public static final CALLTYPE_INTERNET_BASE:I = 0x1b58

.field public static final CALLTYPE_MISSEDPHONE_CALLTO_CUSTOMERCENTER:I = 0xdb

.field public static final CALLTYPE_MISSEDPHONE_CALLTO_OWNER:I = 0xda

.field public static final CALLTYPE_MISSED_VT_TYPE_DUALNUM:I = 0x272

.field public static final CALLTYPE_MISS_BASE:I = 0xfa0

.field public static final CALLTYPE_MISS_CALLEDBY:I = 0xfa1

.field public static final CALLTYPE_MISS_CALLEDBY_DUALNUMBER:I = 0x1195

.field public static final CALLTYPE_MISS_CALLEDBY_DUALNUMBER_NONUM:I = 0x1196

.field public static final CALLTYPE_MISS_CALLEDBY_DUALNUMBER_RES:I = 0x1197

.field public static final CALLTYPE_MISS_CALLEDBY_DUALNUMBER_UNA:I = 0x1198

.field public static final CALLTYPE_MISS_CALLEDBY_NONUM:I = 0xfa4

.field public static final CALLTYPE_MISS_CALLEDBY_NONUM_PLUS:I = 0x1008

.field public static final CALLTYPE_MISS_CALLEDBY_PLUS:I = 0x1005

.field public static final CALLTYPE_MISS_CALLEDBY_RES:I = 0xfa2

.field public static final CALLTYPE_MISS_CALLEDBY_RES_PLUS:I = 0x1006

.field public static final CALLTYPE_MISS_CALLEDBY_UNA:I = 0xfa3

.field public static final CALLTYPE_MISS_CALLEDBY_UNA_PLUS:I = 0x1007

.field public static final CALLTYPE_MISS_CALLFORWARDING:I = 0x51f

.field public static final CALLTYPE_MISS_CALLWAITING:I = 0xfa5

.field public static final CALLTYPE_MISS_CALLWAITING_DUALNUMBER:I = 0x1199

.field public static final CALLTYPE_MISS_CALLWAITING_DUALNUMBER_NONUM:I = 0x119a

.field public static final CALLTYPE_MISS_CALLWAITING_DUALNUMBER_RES:I = 0x119b

.field public static final CALLTYPE_MISS_CALLWAITING_DUALNUMBER_UNA:I = 0x119c

.field public static final CALLTYPE_MISS_CALLWAITING_NONUM:I = 0xfa8

.field public static final CALLTYPE_MISS_CALLWAITING_NONUM_PLUS:I = 0x100c

.field public static final CALLTYPE_MISS_CALLWAITING_PLUS:I = 0x1009

.field public static final CALLTYPE_MISS_CALLWAITING_RES:I = 0xfa6

.field public static final CALLTYPE_MISS_CALLWAITING_RES_PLUS:I = 0x100a

.field public static final CALLTYPE_MISS_CALLWAITING_UNA:I = 0xfa7

.field public static final CALLTYPE_MISS_CALLWAITING_UNA_PLUS:I = 0x100b

.field public static final CALLTYPE_MISS_CISSCALLEDBY:I = 0x1069

.field public static final CALLTYPE_MISS_CISSCALLEDBY_NONUM:I = 0x106c

.field public static final CALLTYPE_MISS_CISSCALLEDBY_NONUM_PLUS:I = 0x1134

.field public static final CALLTYPE_MISS_CISSCALLEDBY_PLUS:I = 0x1131

.field public static final CALLTYPE_MISS_CISSCALLEDBY_RES:I = 0x106a

.field public static final CALLTYPE_MISS_CISSCALLEDBY_RES_PLUS:I = 0x1132

.field public static final CALLTYPE_MISS_CISSCALLEDBY_UNA:I = 0x106b

.field public static final CALLTYPE_MISS_CISSCALLEDBY_UNA_PLUS:I = 0x1133

.field public static final CALLTYPE_MISS_DATA:I = 0x3f3

.field public static final CALLTYPE_MISS_MMCISSCALLEDBY:I = 0x10cd

.field public static final CALLTYPE_MISS_MMCISSCALLEDBY_NONUM:I = 0x10d0

.field public static final CALLTYPE_MISS_MMCISSCALLEDBY_RES:I = 0x10ce

.field public static final CALLTYPE_MISS_MMCISSCALLEDBY_UNA:I = 0x10cf

.field public static final CALLTYPE_MMS_CANCEL:I = 0x336

.field public static final CALLTYPE_MMS_RECEIVE:I = 0x334

.field public static final CALLTYPE_MMS_RECEIVE_DUALNUM:I = 0x335

.field public static final CALLTYPE_MMS_SENDTO:I = 0x333

.field public static final CALLTYPE_MMS_SEND_DUALNUM:I = 0x26f

.field public static final CALLTYPE_MPCS_RCS_BASE:I = 0x2328

.field public static final CALLTYPE_MPCS_RCS_VIDEO_CALL_INCOMING:I = 0x232c

.field public static final CALLTYPE_MPCS_RCS_VIDEO_CALL_MISSED:I = 0x232e

.field public static final CALLTYPE_MPCS_RCS_VIDEO_CALL_OUTGOING:I = 0x232d

.field public static final CALLTYPE_MPCS_RCS_VOIP_INCOMING:I = 0x2329

.field public static final CALLTYPE_MPCS_RCS_VOIP_MISSED:I = 0x232b

.field public static final CALLTYPE_MPCS_RCS_VOIP_OUTGOING:I = 0x232a

.field public static final CALLTYPE_NONE_CALLMEMO:I = 0x16

.field public static final CALLTYPE_NONE_PALDB:I = 0xa

.field public static final CALLTYPE_OUTGOING_VT_TYPE_DUALNUM:I = 0x271

.field public static final CALLTYPE_RECEIVE_EMAIL:I = 0x1f42

.field public static final CALLTYPE_RECE_CALLEDBY:I = 0xd

.field public static final CALLTYPE_RECE_CALLEDBY_DUALNUMBER:I = 0x263

.field public static final CALLTYPE_RECE_CALLEDBY_DUALNUMBER_NONUM:I = 0x264

.field public static final CALLTYPE_RECE_CALLEDBY_DUALNUMBER_RES:I = 0x265

.field public static final CALLTYPE_RECE_CALLEDBY_DUALNUMBER_UNA:I = 0x266

.field public static final CALLTYPE_RECE_CALLEDBY_NONUM:I = 0x10

.field public static final CALLTYPE_RECE_CALLEDBY_NONUM_PLUS:I = 0x72

.field public static final CALLTYPE_RECE_CALLEDBY_PLUS:I = 0x6f

.field public static final CALLTYPE_RECE_CALLEDBY_RES:I = 0xe

.field public static final CALLTYPE_RECE_CALLEDBY_RES_PLUS:I = 0x70

.field public static final CALLTYPE_RECE_CALLEDBY_UNA:I = 0xf

.field public static final CALLTYPE_RECE_CALLEDBY_UNA_PLUS:I = 0x71

.field public static final CALLTYPE_RECE_CALLFORWARDING:I = 0x520

.field public static final CALLTYPE_RECE_CALLREJECT:I = 0x15

.field public static final CALLTYPE_RECE_CALLREJECT_DUALNUMBER:I = 0x26b

.field public static final CALLTYPE_RECE_CALLREJECT_DUALNUMBER_RES:I = 0x26c

.field public static final CALLTYPE_RECE_CALLREJECT_PLUS:I = 0x77

.field public static final CALLTYPE_RECE_CALLWAITING:I = 0x11

.field public static final CALLTYPE_RECE_CALLWAITING_DUALNUMBER:I = 0x267

.field public static final CALLTYPE_RECE_CALLWAITING_DUALNUMBER_NONUM:I = 0x268

.field public static final CALLTYPE_RECE_CALLWAITING_DUALNUMBER_RES:I = 0x269

.field public static final CALLTYPE_RECE_CALLWAITING_DUALNUMBER_UNA:I = 0x26a

.field public static final CALLTYPE_RECE_CALLWAITING_NONUM:I = 0x14

.field public static final CALLTYPE_RECE_CALLWAITING_NONUM_PLUS:I = 0x76

.field public static final CALLTYPE_RECE_CALLWAITING_PLUS:I = 0x73

.field public static final CALLTYPE_RECE_CALLWAITING_RES:I = 0x12

.field public static final CALLTYPE_RECE_CALLWAITING_RES_PLUS:I = 0x74

.field public static final CALLTYPE_RECE_CALLWAITING_UNA:I = 0x13

.field public static final CALLTYPE_RECE_CALLWAITING_UNA_PLUS:I = 0x75

.field public static final CALLTYPE_RECE_CISSCALLEDBY:I = 0x137

.field public static final CALLTYPE_RECE_CISSCALLEDBY_NONUM:I = 0x13a

.field public static final CALLTYPE_RECE_CISSCALLEDBY_NONUM_PLUS:I = 0x202

.field public static final CALLTYPE_RECE_CISSCALLEDBY_PLUS:I = 0x1ff

.field public static final CALLTYPE_RECE_CISSCALLEDBY_RES:I = 0x138

.field public static final CALLTYPE_RECE_CISSCALLEDBY_RES_PLUS:I = 0x200

.field public static final CALLTYPE_RECE_CISSCALLEDBY_UNA:I = 0x139

.field public static final CALLTYPE_RECE_CISSCALLEDBY_UNA_PLUS:I = 0x201

.field public static final CALLTYPE_RECE_CISSCALLREJECT:I = 0x13f

.field public static final CALLTYPE_RECE_CISSCALLREJECT_PLUS:I = 0x207

.field public static final CALLTYPE_RECE_CISSCALLWAITING:I = 0x13b

.field public static final CALLTYPE_RECE_CISSCALLWAITING_NONUM:I = 0x13e

.field public static final CALLTYPE_RECE_CISSCALLWAITING_NONUM_PLUS:I = 0x206

.field public static final CALLTYPE_RECE_CISSCALLWAITING_PLUS:I = 0x203

.field public static final CALLTYPE_RECE_CISSCALLWAITING_RES:I = 0x13c

.field public static final CALLTYPE_RECE_CISSCALLWAITING_RES_PLUS:I = 0x204

.field public static final CALLTYPE_RECE_CISSCALLWAITING_UNA:I = 0x13d

.field public static final CALLTYPE_RECE_CISSCALLWAITING_UNA_PLUS:I = 0x205

.field public static final CALLTYPE_RECE_DATA:I = 0x3f4

.field public static final CALLTYPE_RECE_MMCISSCALLEDBY:I = 0x19b

.field public static final CALLTYPE_RECE_MMCISSCALLEDBY_NONUM:I = 0x19e

.field public static final CALLTYPE_RECE_MMCISSCALLEDBY_RES:I = 0x19c

.field public static final CALLTYPE_RECE_MMCISSCALLEDBY_UNA:I = 0x19d

.field public static final CALLTYPE_RECE_MMCISSCALLREJECT:I = 0x1a3

.field public static final CALLTYPE_RECE_MMCISSCALLWAITING:I = 0x19f

.field public static final CALLTYPE_RECE_MMCISSCALLWAITING_NONUM:I = 0x1a2

.field public static final CALLTYPE_RECE_MMCISSCALLWAITING_RES:I = 0x1a0

.field public static final CALLTYPE_RECE_MMCISSCALLWAITING_UNA:I = 0x1a1

.field public static final CALLTYPE_RECE_NCWCALLREJECT:I = 0xd3

.field public static final CALLTYPE_RECE_NCWCALLREJECT_NONUM:I = 0xd4

.field public static final CALLTYPE_REJECT_CALLFORWARDING:I = 0x521

.field public static final CALLTYPE_REJECT_VT_TYPE_DUALNUM:I = 0x273

.field public static final CALLTYPE_ROAMING_FIELD_SEND_CALLTO:I = 0x458

.field public static final CALLTYPE_ROAMING_INCOMING_VT_TYPE:I = 0x4c8

.field public static final CALLTYPE_ROAMING_MISSED_VT_TYPE:I = 0x4ca

.field public static final CALLTYPE_ROAMING_MISS_CALLEDBY:I = 0x11f9

.field public static final CALLTYPE_ROAMING_MISS_CALLEDBY_NONUM:I = 0x11fc

.field public static final CALLTYPE_ROAMING_MISS_CALLEDBY_RES:I = 0x11fa

.field public static final CALLTYPE_ROAMING_MISS_CALLEDBY_UNA:I = 0x11fb

.field public static final CALLTYPE_ROAMING_MISS_CALLWAITING:I = 0x11fd

.field public static final CALLTYPE_ROAMING_MISS_CALLWAITING_NONUM:I = 0x1200

.field public static final CALLTYPE_ROAMING_MISS_CALLWAITING_RES:I = 0x11fe

.field public static final CALLTYPE_ROAMING_MISS_CALLWAITING_UNA:I = 0x11ff

.field public static final CALLTYPE_ROAMING_MMS_RECEIVE:I = 0x4c7

.field public static final CALLTYPE_ROAMING_MMS_SENDTO:I = 0x4c6

.field public static final CALLTYPE_ROAMING_OUTGOING_VT_TYPE:I = 0x4c9

.field public static final CALLTYPE_ROAMING_RECE_CALLEDBY:I = 0x4bb

.field public static final CALLTYPE_ROAMING_RECE_CALLEDBY_NONUM:I = 0x4be

.field public static final CALLTYPE_ROAMING_RECE_CALLEDBY_RES:I = 0x4bc

.field public static final CALLTYPE_ROAMING_RECE_CALLEDBY_UNA:I = 0x4bd

.field public static final CALLTYPE_ROAMING_RECE_CALLREJECT:I = 0x4c3

.field public static final CALLTYPE_ROAMING_RECE_CALLWAITING:I = 0x4bf

.field public static final CALLTYPE_ROAMING_RECE_CALLWAITING_NONUM:I = 0x4c2

.field public static final CALLTYPE_ROAMING_RECE_CALLWAITING_RES:I = 0x4c0

.field public static final CALLTYPE_ROAMING_RECE_CALLWAITING_UNA:I = 0x4c1

.field public static final CALLTYPE_ROAMING_REJECT_VT_TYPE:I = 0x4cb

.field public static final CALLTYPE_ROAMING_SEND_CALLTO:I = 0x457

.field public static final CALLTYPE_ROAMING_SMS_RECEIVE:I = 0x4c5

.field public static final CALLTYPE_ROAMING_SMS_SENDTO:I = 0x4c4

.field public static final CALLTYPE_SEND_CALLMEFREE:I = 0xd6

.field public static final CALLTYPE_SEND_CALLTO:I = 0xb

.field public static final CALLTYPE_SEND_CALLTO_DUALNUMBER:I = 0x26d

.field public static final CALLTYPE_SEND_CONFERENCE:I = 0xc

.field public static final CALLTYPE_SEND_DATA:I = 0x3f5

.field public static final CALLTYPE_SEND_EMAIL:I = 0x1f41

.field public static final CALLTYPE_SEND_GROUPCALL:I = 0x2c7

.field public static final CALLTYPE_SEND_GROUPCALL_INVITE:I = 0x2c8

.field public static final CALLTYPE_SEND_INTERNET:I = 0x1b59

.field public static final CALLTYPE_SEND_MESSAGECALL:I = 0xd5

.field public static final CALLTYPE_SEND_MOBILESECURITY:I = 0xd7

.field public static final CALLTYPE_SEND_VIDEO_GROUPCALL:I = 0x2c9

.field public static final CALLTYPE_SEND_VT_MESSAGECALL:I = 0xd9

.field public static final CALLTYPE_SEND_WORDDIAL:I = 0xd8

.field public static final CALLTYPE_SMS_CANCEL:I = 0x332

.field public static final CALLTYPE_SMS_RECEIVE:I = 0x32e

.field public static final CALLTYPE_SMS_RECEIVE_DUALNUM:I = 0x32f

.field public static final CALLTYPE_SMS_SENDTO:I = 0x32d

.field public static final CALLTYPE_SMS_SEND_DUALNUM:I = 0x26e

.field public static final CALLTYPE_SMS_VIDEO:I = 0x331

.field public static final CALLTYPE_SMS_VOICE:I = 0x330

.field public static final CALLTYPE_VOIP_BASE:I = 0x1770

.field public static final CALLTYPE_VOIP_CALLWAITING:I = 0x1774

.field public static final CALLTYPE_VOIP_MISSED:I = 0x1773

.field public static final CALLTYPE_VOIP_RECEIVE:I = 0x1772

.field public static final CALLTYPE_VOIP_REJECT:I = 0x1775

.field public static final CALLTYPE_VOIP_SENDTO:I = 0x1771

.field public static final CALLTYPE_VOLTE_BASE:I = 0x1964

.field public static final CALLTYPE_VOLTE_CALLWAITING:I = 0x1969

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_BASE:I = 0x2710

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_CALLWAITING:I = 0x2715

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_CHAT_SEND:I = 0x271f

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISSED:I = 0x2713

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY:I = 0x272d

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_DUALNUMBER:I = 0x2735

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_DUALNUMBER_NONUM:I = 0x2738

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_DUALNUMBER_RES:I = 0x2736

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_DUALNUMBER_UNA:I = 0x2737

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_NONUM:I = 0x2730

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_NONUM_PLUS:I = 0x2734

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_PLUS:I = 0x2731

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_RES:I = 0x272e

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_RES_PLUS:I = 0x2732

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_UNA:I = 0x272f

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLEDBY_UNA_PLUS:I = 0x2733

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLFORWARDING:I = 0x2755

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING:I = 0x2739

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_DUALNUMBER:I = 0x2741

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_DUALNUMBER_NONUM:I = 0x2744

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_DUALNUMBER_RES:I = 0x2742

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_DUALNUMBER_UNA:I = 0x2743

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_NONUM:I = 0x273c

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_NONUM_PLUS:I = 0x2740

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_PLUS:I = 0x273d

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_RES:I = 0x273a

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_RES_PLUS:I = 0x273e

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_UNA:I = 0x273b

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_MISS_CALLWAITING_UNA_PLUS:I = 0x273f

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECEIVE:I = 0x2711

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY:I = 0x2721

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_DUALNUMBER:I = 0x2729

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_DUALNUMBER_NONUM:I = 0x272c

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_DUALNUMBER_RES:I = 0x272a

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_DUALNUMBER_UNA:I = 0x272b

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_NONUM:I = 0x2724

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_NONUM_PLUS:I = 0x2728

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_PLUS:I = 0x2725

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_RES:I = 0x2722

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_RES_PLUS:I = 0x2726

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_UNA:I = 0x2723

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLEDBY_UNA_PLUS:I = 0x2727

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLFORWARDING:I = 0x2756

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLREJECT:I = 0x2745

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLREJECT_DUALNUMBER:I = 0x2747

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLREJECT_DUALNUMBER_RES:I = 0x2748

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLREJECT_PLUS:I = 0x2746

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING:I = 0x2749

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_DUALNUMBER:I = 0x2751

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_DUALNUMBER_NONUM:I = 0x2754

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_DUALNUMBER_RES:I = 0x2752

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_DUALNUMBER_UNA:I = 0x2753

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_NONUM:I = 0x274c

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_NONUM_PLUS:I = 0x2750

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_PLUS:I = 0x274d

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_RES:I = 0x274a

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_RES_PLUS:I = 0x274e

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_UNA:I = 0x274b

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_RECE_CALLWAITING_UNA_PLUS:I = 0x274f

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_REJECT:I = 0x2714

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_REJECT_CALLFORWARDING:I = 0x2757

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SENDTO:I = 0x2712

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SEND_CALLMEFREE:I = 0x271a

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SEND_CALLTO:I = 0x2716

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SEND_CALLTO_DUALNUMBER:I = 0x2720

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SEND_CONFERENCE:I = 0x2719

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SEND_GROUPCALL:I = 0x2717

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SEND_GROUPCALL_INVITE:I = 0x2718

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SEND_INTERNET:I = 0x271d

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SEND_MESSAGECALL:I = 0x271b

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SEND_MOBILESECURITY:I = 0x271c

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_SEND_WORDDIAL:I = 0x271e

.field public static final CALLTYPE_VOLTE_CALL_MESSAGE_WAITING_CALLFORWARDING:I = 0x2758

.field public static final CALLTYPE_VOLTE_CHAT_SEND:I = 0x1973

.field public static final CALLTYPE_VOLTE_MISSED:I = 0x1967

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY:I = 0x1981

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_DUALNUMBER:I = 0x1989

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_DUALNUMBER_NONUM:I = 0x198c

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_DUALNUMBER_RES:I = 0x198a

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_DUALNUMBER_UNA:I = 0x198b

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_NONUM:I = 0x1984

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_NONUM_PLUS:I = 0x1988

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_PLUS:I = 0x1985

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_RES:I = 0x1982

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_RES_PLUS:I = 0x1986

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_UNA:I = 0x1983

.field public static final CALLTYPE_VOLTE_MISS_CALLEDBY_UNA_PLUS:I = 0x1987

.field public static final CALLTYPE_VOLTE_MISS_CALLFORWARDING:I = 0x19a9

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING:I = 0x198d

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_DUALNUMBER:I = 0x1995

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_DUALNUMBER_NONUM:I = 0x1998

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_DUALNUMBER_RES:I = 0x1996

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_DUALNUMBER_UNA:I = 0x1997

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_NONUM:I = 0x1990

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_NONUM_PLUS:I = 0x1994

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_PLUS:I = 0x1991

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_RES:I = 0x198e

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_RES_PLUS:I = 0x1992

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_UNA:I = 0x198f

.field public static final CALLTYPE_VOLTE_MISS_CALLWAITING_UNA_PLUS:I = 0x1993

.field public static final CALLTYPE_VOLTE_RECEIVE:I = 0x1965

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY:I = 0x1975

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_DUALNUMBER:I = 0x197d

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_DUALNUMBER_NONUM:I = 0x1980

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_DUALNUMBER_RES:I = 0x197e

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_DUALNUMBER_UNA:I = 0x197f

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_NONUM:I = 0x1978

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_NONUM_PLUS:I = 0x197c

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_PLUS:I = 0x1979

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_RES:I = 0x1976

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_RES_PLUS:I = 0x197a

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_UNA:I = 0x1977

.field public static final CALLTYPE_VOLTE_RECE_CALLEDBY_UNA_PLUS:I = 0x197b

.field public static final CALLTYPE_VOLTE_RECE_CALLFORWARDING:I = 0x19aa

.field public static final CALLTYPE_VOLTE_RECE_CALLREJECT:I = 0x1999

.field public static final CALLTYPE_VOLTE_RECE_CALLREJECT_DUALNUMBER:I = 0x199b

.field public static final CALLTYPE_VOLTE_RECE_CALLREJECT_DUALNUMBER_RES:I = 0x199c

.field public static final CALLTYPE_VOLTE_RECE_CALLREJECT_PLUS:I = 0x199a

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING:I = 0x199d

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_DUALNUMBER:I = 0x19a5

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_DUALNUMBER_NONUM:I = 0x19a8

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_DUALNUMBER_RES:I = 0x19a6

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_DUALNUMBER_UNA:I = 0x19a7

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_NONUM:I = 0x19a0

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_NONUM_PLUS:I = 0x19a4

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_PLUS:I = 0x19a1

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_RES:I = 0x199e

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_RES_PLUS:I = 0x19a2

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_UNA:I = 0x199f

.field public static final CALLTYPE_VOLTE_RECE_CALLWAITING_UNA_PLUS:I = 0x19a3

.field public static final CALLTYPE_VOLTE_REJECT:I = 0x1968

.field public static final CALLTYPE_VOLTE_REJECT_CALLFORWARDING:I = 0x19ab

.field public static final CALLTYPE_VOLTE_SENDTO:I = 0x1966

.field public static final CALLTYPE_VOLTE_SEND_CALLMEFREE:I = 0x196e

.field public static final CALLTYPE_VOLTE_SEND_CALLTO:I = 0x196a

.field public static final CALLTYPE_VOLTE_SEND_CALLTO_DUALNUMBER:I = 0x1974

.field public static final CALLTYPE_VOLTE_SEND_CONFERENCE:I = 0x196d

.field public static final CALLTYPE_VOLTE_SEND_GROUPCALL:I = 0x196b

.field public static final CALLTYPE_VOLTE_SEND_GROUPCALL_INVITE:I = 0x196c

.field public static final CALLTYPE_VOLTE_SEND_INTERNET:I = 0x1971

.field public static final CALLTYPE_VOLTE_SEND_MESSAGECALL:I = 0x196f

.field public static final CALLTYPE_VOLTE_SEND_MOBILESECURITY:I = 0x1970

.field public static final CALLTYPE_VOLTE_SEND_WORDDIAL:I = 0x1972

.field public static final CALLTYPE_VOLTE_WAITING_CALLFORWARDING:I = 0x19ac

.field public static final CALLTYPE_WAITING_CALLFORWARDING:I = 0x522
