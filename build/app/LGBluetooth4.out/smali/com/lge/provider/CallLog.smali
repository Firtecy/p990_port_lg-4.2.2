.class public Lcom/lge/provider/CallLog;
.super Landroid/provider/CallLog;
.source "CallLog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/lge/provider/CallLog$Calls;,
        Lcom/lge/provider/CallLog$CallTypesFunc;,
        Lcom/lge/provider/CallLog$CallTypeColumns;
    }
.end annotation


# static fields
.field static final TAG:Ljava/lang/String; = "CallLog"


# direct methods
.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 42
    invoke-direct {p0}, Landroid/provider/CallLog;-><init>()V

    #@3
    .line 2547
    return-void
.end method

.method static synthetic access$000(Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    invoke-static {p0}, Lcom/lge/provider/CallLog;->formattedNumberWithCountryIso(Ljava/lang/String;)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$100(JLjava/lang/String;)Landroid/net/Uri;
    .registers 4
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    invoke-static {p0, p1, p2}, Lcom/lge/provider/CallLog;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$200(I)Ljava/lang/String;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    invoke-static {p0}, Lcom/lge/provider/CallLog;->getIccId(I)Ljava/lang/String;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method private static formattedNumberWithCountryIso(Ljava/lang/String;)Ljava/lang/String;
    .registers 10
    .parameter "number"

    #@0
    .prologue
    .line 3324
    const/4 v2, 0x0

    #@1
    .line 3326
    .local v2, formattednumber:Ljava/lang/String;
    if-eqz p0, :cond_73

    #@3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    #@6
    move-result v6

    #@7
    const/4 v7, 0x2

    #@8
    if-lt v6, v7, :cond_73

    #@a
    const/4 v6, 0x0

    #@b
    invoke-virtual {p0, v6}, Ljava/lang/String;->charAt(I)C

    #@e
    move-result v6

    #@f
    const/16 v7, 0x2b

    #@11
    if-ne v6, v7, :cond_73

    #@13
    .line 3327
    invoke-static {}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->getInstance()Lcom/android/i18n/phonenumbers/PhoneNumberUtil;

    #@16
    move-result-object v5

    #@17
    .line 3328
    .local v5, util:Lcom/android/i18n/phonenumbers/PhoneNumberUtil;
    const/4 v0, 0x0

    #@18
    .line 3329
    .local v0, defaultCountryIso:Ljava/lang/String;
    const/4 v3, 0x0

    #@19
    .line 3332
    .local v3, normalizedNumber:Ljava/lang/String;
    :try_start_19
    invoke-virtual {v5, p0, v0}, Lcom/android/i18n/phonenumbers/PhoneNumberUtil;->parse(Ljava/lang/String;Ljava/lang/String;)Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;

    #@1c
    move-result-object v4

    #@1d
    .line 3333
    .local v4, pn:Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    invoke-virtual {v4}, Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;->getNationalNumber()J

    #@20
    move-result-wide v6

    #@21
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    .line 3335
    const-string v6, "purple"

    #@27
    new-instance v7, Ljava/lang/StringBuilder;

    #@29
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@2c
    const-string v8, "NationalCode : "

    #@2e
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@31
    move-result-object v7

    #@32
    invoke-virtual {v4}, Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;->getCountryCode()I

    #@35
    move-result v8

    #@36
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@39
    move-result-object v7

    #@3a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v7

    #@3e
    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_41
    .catch Ljava/lang/Exception; {:try_start_19 .. :try_end_41} :catch_58

    #@41
    .line 3336
    if-eqz v3, :cond_44

    #@43
    .line 3337
    move-object v2, v3

    #@44
    .line 3343
    .end local v4           #pn:Lcom/android/i18n/phonenumbers/Phonenumber$PhoneNumber;
    :cond_44
    :goto_44
    new-instance v6, Ljava/lang/StringBuilder;

    #@46
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@49
    const-string v7, "0"

    #@4b
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v6

    #@4f
    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@52
    move-result-object v6

    #@53
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@56
    move-result-object v6

    #@57
    .line 3346
    .end local v0           #defaultCountryIso:Ljava/lang/String;
    .end local v3           #normalizedNumber:Ljava/lang/String;
    .end local v5           #util:Lcom/android/i18n/phonenumbers/PhoneNumberUtil;
    :goto_57
    return-object v6

    #@58
    .line 3339
    .restart local v0       #defaultCountryIso:Ljava/lang/String;
    .restart local v3       #normalizedNumber:Ljava/lang/String;
    .restart local v5       #util:Lcom/android/i18n/phonenumbers/PhoneNumberUtil;
    :catch_58
    move-exception v1

    #@59
    .line 3340
    .local v1, e:Ljava/lang/Exception;
    const-string v6, "purple"

    #@5b
    new-instance v7, Ljava/lang/StringBuilder;

    #@5d
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@60
    const-string v8, "normalizedNumber Exception :"

    #@62
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@65
    move-result-object v7

    #@66
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v7

    #@6a
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6d
    move-result-object v7

    #@6e
    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 3341
    move-object v2, p0

    #@72
    goto :goto_44

    #@73
    .line 3345
    .end local v0           #defaultCountryIso:Ljava/lang/String;
    .end local v1           #e:Ljava/lang/Exception;
    .end local v3           #normalizedNumber:Ljava/lang/String;
    .end local v5           #util:Lcom/android/i18n/phonenumbers/PhoneNumberUtil;
    :cond_73
    move-object v2, p0

    #@74
    move-object v6, v2

    #@75
    .line 3346
    goto :goto_57
.end method

.method private static getIccId(I)Ljava/lang/String;
    .registers 2
    .parameter "subscription"

    #@0
    .prologue
    .line 3295
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->isMultiSimEnabled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_b

    #@6
    .line 3296
    invoke-static {p0}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getLine1Number(I)Ljava/lang/String;

    #@9
    move-result-object v0

    #@a
    .line 3298
    :goto_a
    return-object v0

    #@b
    :cond_b
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getLine1Number()Ljava/lang/String;

    #@e
    move-result-object v0

    #@f
    goto :goto_a
.end method

.method private static getLookupUri(JLjava/lang/String;)Landroid/net/Uri;
    .registers 6
    .parameter "contactId"
    .parameter "lookupKey"

    #@0
    .prologue
    .line 3314
    new-instance v0, Ljava/lang/StringBuilder;

    #@2
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@5
    .line 3315
    .local v0, sb:Ljava/lang/StringBuilder;
    const-string v2, "content://com.android.contacts/contacts/lookup/"

    #@7
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a
    .line 3316
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d
    .line 3317
    const-string v2, "/"

    #@f
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    .line 3318
    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@15
    .line 3319
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@18
    move-result-object v2

    #@19
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@1c
    move-result-object v1

    #@1d
    .line 3321
    .local v1, uri:Landroid/net/Uri;
    return-object v1
.end method

.method public static uriToString(Landroid/net/Uri;)Ljava/lang/String;
    .registers 2
    .parameter "uri"

    #@0
    .prologue
    .line 3351
    if-nez p0, :cond_4

    #@2
    const/4 v0, 0x0

    #@3
    :goto_3
    return-object v0

    #@4
    :cond_4
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    #@7
    move-result-object v0

    #@8
    goto :goto_3
.end method
