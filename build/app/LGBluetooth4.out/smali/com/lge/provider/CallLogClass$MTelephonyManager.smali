.class public Lcom/lge/provider/CallLogClass$MTelephonyManager;
.super Ljava/lang/Object;
.source "CallLogClass.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/lge/provider/CallLogClass;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MTelephonyManager"
.end annotation


# static fields
.field private static final CLASS_NAME_MSIM_TELEPHONY:Ljava/lang/String; = "android.telephony.MSimTelephonyManager"

.field private static final CLASS_NAME_TELEPHONY:Ljava/lang/String; = "android.telephony.TelephonyManager"

.field private static mSimTManager:Landroid/telephony/MSimTelephonyManager;

.field private static mTManger:Landroid/telephony/TelephonyManager;

.field private static mtkGeminiSupport:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 55
    sput-object v0, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@3
    .line 56
    sput-object v0, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@5
    .line 57
    const/4 v0, 0x0

    #@6
    sput-boolean v0, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mtkGeminiSupport:Z

    #@8
    return-void
.end method

.method public constructor <init>()V
    .registers 1

    #@0
    .prologue
    .line 49
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@3
    return-void
.end method

.method public static getDefault()Landroid/telephony/TelephonyManager;
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 61
    :try_start_1
    const-string v2, "getDefault"

    #@3
    .line 62
    .local v2, methodName:Ljava/lang/String;
    const-string v3, "android.telephony.MSimTelephonyManager"

    #@5
    const/4 v5, 0x0

    #@6
    invoke-static {v3, v2, v5}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@9
    move-result-object v1

    #@a
    .line 63
    .local v1, method:Ljava/lang/reflect/Method;
    const/4 v3, 0x0

    #@b
    const/4 v5, 0x0

    #@c
    new-array v5, v5, [Ljava/lang/Object;

    #@e
    invoke-virtual {v1, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@11
    move-result-object v3

    #@12
    check-cast v3, Landroid/telephony/TelephonyManager;
    :try_end_14
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_14} :catch_15

    #@14
    .line 67
    .end local v1           #method:Ljava/lang/reflect/Method;
    :goto_14
    return-object v3

    #@15
    .line 64
    :catch_15
    move-exception v0

    #@16
    .line 65
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "LGCallLogClass"

    #@18
    const-string v5, "it does not exist TelephonyManager.getDefault"

    #@1a
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1d
    move-object v3, v4

    #@1e
    .line 67
    goto :goto_14
.end method

.method public static getDefaultMSim()Landroid/telephony/MSimTelephonyManager;
    .registers 6

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 72
    :try_start_1
    const-string v2, "getDefault"

    #@3
    .line 73
    .local v2, methodName:Ljava/lang/String;
    const-string v3, "android.telephony.MSimTelephonyManager"

    #@5
    const/4 v5, 0x0

    #@6
    invoke-static {v3, v2, v5}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@9
    move-result-object v1

    #@a
    .line 74
    .local v1, method:Ljava/lang/reflect/Method;
    const-string v3, "LGCallLogClass"

    #@c
    const-string v5, "it exist MSimTelephonyManager.getDefault#########"

    #@e
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 75
    const/4 v3, 0x0

    #@12
    const/4 v5, 0x0

    #@13
    new-array v5, v5, [Ljava/lang/Object;

    #@15
    invoke-virtual {v1, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@18
    move-result-object v3

    #@19
    check-cast v3, Landroid/telephony/MSimTelephonyManager;
    :try_end_1b
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1b} :catch_1c

    #@1b
    .line 79
    .end local v1           #method:Ljava/lang/reflect/Method;
    :goto_1b
    return-object v3

    #@1c
    .line 76
    :catch_1c
    move-exception v0

    #@1d
    .line 77
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "LGCallLogClass"

    #@1f
    const-string v5, "it does not exist MSimTelephonyManager.getDefault"

    #@21
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    move-object v3, v4

    #@25
    .line 79
    goto :goto_1b
.end method

.method public static getDefaultSubscription()I
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 109
    sget-boolean v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mtkGeminiSupport:Z

    #@3
    if-eqz v5, :cond_a

    #@5
    .line 110
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefaultSubscriptionGemini()I

    #@8
    move-result v4

    #@9
    .line 128
    .local v2, methodName:Ljava/lang/String;
    :cond_9
    :goto_9
    return v4

    #@a
    .line 113
    .end local v2           #methodName:Ljava/lang/String;
    :cond_a
    const-string v2, "getDefaultSubscription"

    #@c
    .line 116
    .restart local v2       #methodName:Ljava/lang/String;
    :try_start_c
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefaultMSim()Landroid/telephony/MSimTelephonyManager;

    #@f
    move-result-object v5

    #@10
    sput-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@12
    .line 117
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@14
    if-eqz v5, :cond_9

    #@16
    .line 121
    const-string v5, "android.telephony.MSimTelephonyManager"

    #@18
    const/4 v6, 0x0

    #@19
    invoke-static {v5, v2, v6}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@1c
    move-result-object v1

    #@1d
    .line 122
    .local v1, method:Ljava/lang/reflect/Method;
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@1f
    const/4 v6, 0x0

    #@20
    new-array v6, v6, [Ljava/lang/Object;

    #@22
    invoke-virtual {v1, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@25
    move-result-object v3

    #@26
    check-cast v3, Ljava/lang/Integer;

    #@28
    .line 123
    .local v3, ret:Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_2b
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_2b} :catch_2d

    #@2b
    move-result v4

    #@2c
    goto :goto_9

    #@2d
    .line 124
    .end local v1           #method:Ljava/lang/reflect/Method;
    .end local v3           #ret:Ljava/lang/Integer;
    :catch_2d
    move-exception v0

    #@2e
    .line 126
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "LGCallLogClass"

    #@30
    const-string v6, "it does not exist TelephonyManager.getDefaultSubscription"

    #@32
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    goto :goto_9
.end method

.method public static getDefaultSubscriptionGemini()I
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 258
    const-string v2, "getDefaultSubscription"

    #@3
    .line 261
    .local v2, methodName:Ljava/lang/String;
    :try_start_3
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6
    move-result-object v5

    #@7
    sput-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@9
    .line 262
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@b
    if-nez v5, :cond_e

    #@d
    .line 273
    :goto_d
    return v4

    #@e
    .line 266
    :cond_e
    const-string v5, "android.telephony.TelephonyManager"

    #@10
    const/4 v6, 0x0

    #@11
    invoke-static {v5, v2, v6}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@14
    move-result-object v1

    #@15
    .line 267
    .local v1, method:Ljava/lang/reflect/Method;
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@17
    const/4 v6, 0x0

    #@18
    new-array v6, v6, [Ljava/lang/Object;

    #@1a
    invoke-virtual {v1, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Ljava/lang/Integer;

    #@20
    .line 268
    .local v3, ret:Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_23} :catch_25

    #@23
    move-result v4

    #@24
    goto :goto_d

    #@25
    .line 269
    .end local v1           #method:Ljava/lang/reflect/Method;
    .end local v3           #ret:Ljava/lang/Integer;
    :catch_25
    move-exception v0

    #@26
    .line 271
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "LGCallLogClass"

    #@28
    const-string v6, "it does not exist TelephonyManager.getDefaultSubscription"

    #@2a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_d
.end method

.method public static getLine1Number()Ljava/lang/String;
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 221
    const-string v2, "getSimSerialNumber"

    #@3
    .line 223
    .local v2, methodName:Ljava/lang/String;
    :try_start_3
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefaultMSim()Landroid/telephony/MSimTelephonyManager;

    #@6
    move-result-object v3

    #@7
    sput-object v3, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@9
    .line 224
    sget-object v3, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@b
    if-nez v3, :cond_f

    #@d
    move-object v3, v4

    #@e
    .line 234
    :goto_e
    return-object v3

    #@f
    .line 228
    :cond_f
    const-string v3, "android.telephony.TelephonyManager"

    #@11
    const/4 v5, 0x0

    #@12
    invoke-static {v3, v2, v5}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@15
    move-result-object v1

    #@16
    .line 229
    .local v1, method:Ljava/lang/reflect/Method;
    sget-object v3, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@18
    const/4 v5, 0x0

    #@19
    new-array v5, v5, [Ljava/lang/Object;

    #@1b
    invoke-virtual {v1, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@1e
    move-result-object v3

    #@1f
    check-cast v3, Ljava/lang/String;
    :try_end_21
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_21} :catch_22

    #@21
    goto :goto_e

    #@22
    .line 230
    .end local v1           #method:Ljava/lang/reflect/Method;
    :catch_22
    move-exception v0

    #@23
    .line 232
    .local v0, e:Ljava/lang/Exception;
    const-string v3, "LGCallLogClass"

    #@25
    new-instance v5, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v6, "it does not exist TelephonyManager.getLine1Number : "

    #@2c
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v5

    #@30
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v5

    #@34
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@37
    move-result-object v5

    #@38
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3b
    move-object v3, v4

    #@3c
    .line 234
    goto :goto_e
.end method

.method public static getLine1Number(I)Ljava/lang/String;
    .registers 11
    .parameter "subscription"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 187
    sget-boolean v6, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mtkGeminiSupport:Z

    #@3
    if-eqz v6, :cond_a

    #@5
    .line 188
    invoke-static {p0}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getLine1NumberGemini(I)Ljava/lang/String;

    #@8
    move-result-object v6

    #@9
    .line 216
    :goto_9
    return-object v6

    #@a
    .line 191
    :cond_a
    invoke-static {p0}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getSimSerialNumber(I)Ljava/lang/String;

    #@d
    move-result-object v2

    #@e
    .line 194
    .local v2, iccid:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@11
    move-result v6

    #@12
    if-eqz v6, :cond_5f

    #@14
    .line 195
    const-string v4, "getLine1Number"

    #@16
    .line 197
    .local v4, methodName:Ljava/lang/String;
    :try_start_16
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefaultMSim()Landroid/telephony/MSimTelephonyManager;

    #@19
    move-result-object v6

    #@1a
    sput-object v6, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@1c
    .line 198
    sget-object v6, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@1e
    if-nez v6, :cond_22

    #@20
    move-object v6, v7

    #@21
    .line 199
    goto :goto_9

    #@22
    .line 202
    :cond_22
    const/4 v6, 0x1

    #@23
    new-array v5, v6, [Ljava/lang/Class;

    #@25
    const/4 v6, 0x0

    #@26
    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@28
    aput-object v8, v5, v6

    #@2a
    .line 205
    .local v5, parameterTypes:[Ljava/lang/Class;
    const/4 v6, 0x1

    #@2b
    new-array v0, v6, [Ljava/lang/Object;

    #@2d
    const/4 v6, 0x0

    #@2e
    new-instance v8, Ljava/lang/Integer;

    #@30
    invoke-direct {v8, p0}, Ljava/lang/Integer;-><init>(I)V

    #@33
    aput-object v8, v0, v6

    #@35
    .line 208
    .local v0, arguments:[Ljava/lang/Object;
    const-string v6, "android.telephony.MSimTelephonyManager"

    #@37
    invoke-static {v6, v4, v5}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@3a
    move-result-object v3

    #@3b
    .line 209
    .local v3, method:Ljava/lang/reflect/Method;
    sget-object v6, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@3d
    invoke-virtual {v3, v6, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@40
    move-result-object v6

    #@41
    check-cast v6, Ljava/lang/String;
    :try_end_43
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_43} :catch_44

    #@43
    goto :goto_9

    #@44
    .line 210
    .end local v0           #arguments:[Ljava/lang/Object;
    .end local v3           #method:Ljava/lang/reflect/Method;
    .end local v5           #parameterTypes:[Ljava/lang/Class;
    :catch_44
    move-exception v1

    #@45
    .line 212
    .local v1, e:Ljava/lang/Exception;
    const-string v6, "LGCallLogClass"

    #@47
    new-instance v8, Ljava/lang/StringBuilder;

    #@49
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@4c
    const-string v9, "it does not exist MSimTelephonyManager.getLine1Number : "

    #@4e
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@51
    move-result-object v8

    #@52
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@55
    move-result-object v8

    #@56
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@59
    move-result-object v8

    #@5a
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@5d
    move-object v6, v7

    #@5e
    .line 214
    goto :goto_9

    #@5f
    .end local v1           #e:Ljava/lang/Exception;
    .end local v4           #methodName:Ljava/lang/String;
    :cond_5f
    move-object v6, v2

    #@60
    .line 216
    goto :goto_9
.end method

.method public static getLine1NumberGemini(I)Ljava/lang/String;
    .registers 11
    .parameter "subscription"

    #@0
    .prologue
    const/4 v7, 0x0

    #@1
    .line 324
    invoke-static {p0}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getSimSerialNumberGemini(I)Ljava/lang/String;

    #@4
    move-result-object v2

    #@5
    .line 327
    .local v2, iccid:Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@8
    move-result v6

    #@9
    if-eqz v6, :cond_56

    #@b
    .line 328
    const-string v4, "getLine1NumberGemini"

    #@d
    .line 330
    .local v4, methodName:Ljava/lang/String;
    :try_start_d
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@10
    move-result-object v6

    #@11
    sput-object v6, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@13
    .line 331
    sget-object v6, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@15
    if-nez v6, :cond_19

    #@17
    move-object v6, v7

    #@18
    .line 349
    .end local v4           #methodName:Ljava/lang/String;
    :goto_18
    return-object v6

    #@19
    .line 335
    .restart local v4       #methodName:Ljava/lang/String;
    :cond_19
    const/4 v6, 0x1

    #@1a
    new-array v5, v6, [Ljava/lang/Class;

    #@1c
    const/4 v6, 0x0

    #@1d
    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@1f
    aput-object v8, v5, v6

    #@21
    .line 338
    .local v5, parameterTypes:[Ljava/lang/Class;
    const/4 v6, 0x1

    #@22
    new-array v0, v6, [Ljava/lang/Object;

    #@24
    const/4 v6, 0x0

    #@25
    new-instance v8, Ljava/lang/Integer;

    #@27
    invoke-direct {v8, p0}, Ljava/lang/Integer;-><init>(I)V

    #@2a
    aput-object v8, v0, v6

    #@2c
    .line 341
    .local v0, arguments:[Ljava/lang/Object;
    const-string v6, "android.telephony.TelephonyManager"

    #@2e
    invoke-static {v6, v4, v5}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@31
    move-result-object v3

    #@32
    .line 342
    .local v3, method:Ljava/lang/reflect/Method;
    sget-object v6, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@34
    invoke-virtual {v3, v6, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@37
    move-result-object v6

    #@38
    check-cast v6, Ljava/lang/String;
    :try_end_3a
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_3a} :catch_3b

    #@3a
    goto :goto_18

    #@3b
    .line 343
    .end local v0           #arguments:[Ljava/lang/Object;
    .end local v3           #method:Ljava/lang/reflect/Method;
    .end local v5           #parameterTypes:[Ljava/lang/Class;
    :catch_3b
    move-exception v1

    #@3c
    .line 345
    .local v1, e:Ljava/lang/Exception;
    const-string v6, "LGCallLogClass"

    #@3e
    new-instance v8, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v9, "it does not exist MSimTelephonyManager.getLine1Number : "

    #@45
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v8

    #@49
    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4c
    move-result-object v8

    #@4d
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50
    move-result-object v8

    #@51
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@54
    move-object v6, v7

    #@55
    .line 347
    goto :goto_18

    #@56
    .end local v1           #e:Ljava/lang/Exception;
    .end local v4           #methodName:Ljava/lang/String;
    :cond_56
    move-object v6, v2

    #@57
    .line 349
    goto :goto_18
.end method

.method public static getPhoneCount()I
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x1

    #@1
    .line 356
    const-string v2, "getPhoneCount"

    #@3
    .line 359
    .local v2, methodName:Ljava/lang/String;
    :try_start_3
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6
    move-result-object v5

    #@7
    sput-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@9
    .line 360
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@b
    if-nez v5, :cond_e

    #@d
    .line 371
    :goto_d
    return v4

    #@e
    .line 364
    :cond_e
    const-string v5, "android.telephony.TelephonyManager"

    #@10
    const/4 v6, 0x0

    #@11
    invoke-static {v5, v2, v6}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@14
    move-result-object v1

    #@15
    .line 365
    .local v1, method:Ljava/lang/reflect/Method;
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@17
    const/4 v6, 0x0

    #@18
    new-array v6, v6, [Ljava/lang/Object;

    #@1a
    invoke-virtual {v1, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Ljava/lang/Integer;

    #@20
    .line 366
    .local v3, ret:Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_23} :catch_25

    #@23
    move-result v4

    #@24
    goto :goto_d

    #@25
    .line 367
    .end local v1           #method:Ljava/lang/reflect/Method;
    .end local v3           #ret:Ljava/lang/Integer;
    :catch_25
    move-exception v0

    #@26
    .line 369
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "LGCallLogClass"

    #@28
    const-string v6, "it does not exist TelephonyManager.getPhoneCount"

    #@2a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_d
.end method

.method public static getSimSerialNumber(I)Ljava/lang/String;
    .registers 10
    .parameter "subscription"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 159
    sget-boolean v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mtkGeminiSupport:Z

    #@3
    if-eqz v5, :cond_a

    #@5
    .line 160
    invoke-static {p0}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getSimSerialNumberGemini(I)Ljava/lang/String;

    #@8
    move-result-object v5

    #@9
    .line 182
    :goto_9
    return-object v5

    #@a
    .line 163
    :cond_a
    const-string v3, "getSimSerialNumber"

    #@c
    .line 165
    .local v3, methodName:Ljava/lang/String;
    :try_start_c
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefaultMSim()Landroid/telephony/MSimTelephonyManager;

    #@f
    move-result-object v5

    #@10
    sput-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@12
    .line 166
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@14
    if-nez v5, :cond_18

    #@16
    move-object v5, v6

    #@17
    .line 167
    goto :goto_9

    #@18
    .line 170
    :cond_18
    const/4 v5, 0x1

    #@19
    new-array v4, v5, [Ljava/lang/Class;

    #@1b
    const/4 v5, 0x0

    #@1c
    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@1e
    aput-object v7, v4, v5

    #@20
    .line 173
    .local v4, parameterTypes:[Ljava/lang/Class;
    const/4 v5, 0x1

    #@21
    new-array v0, v5, [Ljava/lang/Object;

    #@23
    const/4 v5, 0x0

    #@24
    new-instance v7, Ljava/lang/Integer;

    #@26
    invoke-direct {v7, p0}, Ljava/lang/Integer;-><init>(I)V

    #@29
    aput-object v7, v0, v5

    #@2b
    .line 176
    .local v0, arguments:[Ljava/lang/Object;
    const-string v5, "android.telephony.MSimTelephonyManager"

    #@2d
    invoke-static {v5, v3, v4}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@30
    move-result-object v2

    #@31
    .line 177
    .local v2, method:Ljava/lang/reflect/Method;
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@33
    invoke-virtual {v2, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    move-result-object v5

    #@37
    check-cast v5, Ljava/lang/String;
    :try_end_39
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_39} :catch_3a

    #@39
    goto :goto_9

    #@3a
    .line 178
    .end local v0           #arguments:[Ljava/lang/Object;
    .end local v2           #method:Ljava/lang/reflect/Method;
    .end local v4           #parameterTypes:[Ljava/lang/Class;
    :catch_3a
    move-exception v1

    #@3b
    .line 180
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "LGCallLogClass"

    #@3d
    new-instance v7, Ljava/lang/StringBuilder;

    #@3f
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@42
    const-string v8, "it does not exist MSimTelephonyManager.getSimSerialNumber : "

    #@44
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@47
    move-result-object v7

    #@48
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4f
    move-result-object v7

    #@50
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@53
    move-object v5, v6

    #@54
    .line 182
    goto :goto_9
.end method

.method public static getSimSerialNumberGemini(I)Ljava/lang/String;
    .registers 10
    .parameter "subscription"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 300
    const-string v3, "getSimSerialNumberGemini"

    #@3
    .line 302
    .local v3, methodName:Ljava/lang/String;
    :try_start_3
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6
    move-result-object v5

    #@7
    sput-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@9
    .line 303
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@b
    if-nez v5, :cond_f

    #@d
    move-object v5, v6

    #@e
    .line 319
    :goto_e
    return-object v5

    #@f
    .line 307
    :cond_f
    const/4 v5, 0x1

    #@10
    new-array v4, v5, [Ljava/lang/Class;

    #@12
    const/4 v5, 0x0

    #@13
    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@15
    aput-object v7, v4, v5

    #@17
    .line 310
    .local v4, parameterTypes:[Ljava/lang/Class;
    const/4 v5, 0x1

    #@18
    new-array v0, v5, [Ljava/lang/Object;

    #@1a
    const/4 v5, 0x0

    #@1b
    new-instance v7, Ljava/lang/Integer;

    #@1d
    invoke-direct {v7, p0}, Ljava/lang/Integer;-><init>(I)V

    #@20
    aput-object v7, v0, v5

    #@22
    .line 313
    .local v0, arguments:[Ljava/lang/Object;
    const-string v5, "android.telephony.TelephonyManager"

    #@24
    invoke-static {v5, v3, v4}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@27
    move-result-object v2

    #@28
    .line 314
    .local v2, method:Ljava/lang/reflect/Method;
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@2a
    invoke-virtual {v2, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    move-result-object v5

    #@2e
    check-cast v5, Ljava/lang/String;
    :try_end_30
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_30} :catch_31

    #@30
    goto :goto_e

    #@31
    .line 315
    .end local v0           #arguments:[Ljava/lang/Object;
    .end local v2           #method:Ljava/lang/reflect/Method;
    .end local v4           #parameterTypes:[Ljava/lang/Class;
    :catch_31
    move-exception v1

    #@32
    .line 317
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "LGCallLogClass"

    #@34
    new-instance v7, Ljava/lang/StringBuilder;

    #@36
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@39
    const-string v8, "it does not exist MSimTelephonyManager.getSimSerialNumber : "

    #@3b
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v7

    #@3f
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@46
    move-result-object v7

    #@47
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4a
    move-object v5, v6

    #@4b
    .line 319
    goto :goto_e
.end method

.method public static hasIccCard(I)Z
    .registers 10
    .parameter "subscription"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 132
    sget-boolean v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mtkGeminiSupport:Z

    #@3
    if-eqz v5, :cond_a

    #@5
    .line 133
    invoke-static {p0}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->hasIccCardGemini(I)Z

    #@8
    move-result v5

    #@9
    .line 155
    :goto_9
    return v5

    #@a
    .line 136
    :cond_a
    const-string v3, "hasIccCard"

    #@c
    .line 138
    .local v3, methodName:Ljava/lang/String;
    :try_start_c
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefaultMSim()Landroid/telephony/MSimTelephonyManager;

    #@f
    move-result-object v5

    #@10
    sput-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@12
    .line 139
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@14
    if-nez v5, :cond_18

    #@16
    move v5, v6

    #@17
    .line 140
    goto :goto_9

    #@18
    .line 143
    :cond_18
    const/4 v5, 0x1

    #@19
    new-array v4, v5, [Ljava/lang/Class;

    #@1b
    const/4 v5, 0x0

    #@1c
    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@1e
    aput-object v7, v4, v5

    #@20
    .line 146
    .local v4, parameterTypes:[Ljava/lang/Class;
    const/4 v5, 0x1

    #@21
    new-array v0, v5, [Ljava/lang/Object;

    #@23
    const/4 v5, 0x0

    #@24
    new-instance v7, Ljava/lang/Integer;

    #@26
    invoke-direct {v7, p0}, Ljava/lang/Integer;-><init>(I)V

    #@29
    aput-object v7, v0, v5

    #@2b
    .line 149
    .local v0, arguments:[Ljava/lang/Object;
    const-string v5, "android.telephony.MSimTelephonyManager"

    #@2d
    invoke-static {v5, v3, v4}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@30
    move-result-object v2

    #@31
    .line 150
    .local v2, method:Ljava/lang/reflect/Method;
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@33
    invoke-virtual {v2, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@36
    move-result-object v5

    #@37
    check-cast v5, Ljava/lang/Boolean;

    #@39
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_3c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_3c} :catch_3e

    #@3c
    move-result v5

    #@3d
    goto :goto_9

    #@3e
    .line 151
    .end local v0           #arguments:[Ljava/lang/Object;
    .end local v2           #method:Ljava/lang/reflect/Method;
    .end local v4           #parameterTypes:[Ljava/lang/Class;
    :catch_3e
    move-exception v1

    #@3f
    .line 153
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "LGCallLogClass"

    #@41
    new-instance v7, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v8, "it does not exist MSimTelephonyManager.getLine1Number : "

    #@48
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v7

    #@4c
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4f
    move-result-object v7

    #@50
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@53
    move-result-object v7

    #@54
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@57
    move v5, v6

    #@58
    .line 155
    goto :goto_9
.end method

.method public static hasIccCardGemini(I)Z
    .registers 10
    .parameter "subscription"

    #@0
    .prologue
    const/4 v6, 0x0

    #@1
    .line 277
    const-string v3, "hasIccCardGemini"

    #@3
    .line 279
    .local v3, methodName:Ljava/lang/String;
    :try_start_3
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6
    move-result-object v5

    #@7
    sput-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@9
    .line 280
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@b
    if-nez v5, :cond_f

    #@d
    move v5, v6

    #@e
    .line 296
    :goto_e
    return v5

    #@f
    .line 284
    :cond_f
    const/4 v5, 0x1

    #@10
    new-array v4, v5, [Ljava/lang/Class;

    #@12
    const/4 v5, 0x0

    #@13
    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    #@15
    aput-object v7, v4, v5

    #@17
    .line 287
    .local v4, parameterTypes:[Ljava/lang/Class;
    const/4 v5, 0x1

    #@18
    new-array v0, v5, [Ljava/lang/Object;

    #@1a
    const/4 v5, 0x0

    #@1b
    new-instance v7, Ljava/lang/Integer;

    #@1d
    invoke-direct {v7, p0}, Ljava/lang/Integer;-><init>(I)V

    #@20
    aput-object v7, v0, v5

    #@22
    .line 290
    .local v0, arguments:[Ljava/lang/Object;
    const-string v5, "android.telephony.TelephonyManager"

    #@24
    invoke-static {v5, v3, v4}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@27
    move-result-object v2

    #@28
    .line 291
    .local v2, method:Ljava/lang/reflect/Method;
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@2a
    invoke-virtual {v2, v5, v0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@2d
    move-result-object v5

    #@2e
    check-cast v5, Ljava/lang/Boolean;

    #@30
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_33
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_33} :catch_35

    #@33
    move-result v5

    #@34
    goto :goto_e

    #@35
    .line 292
    .end local v0           #arguments:[Ljava/lang/Object;
    .end local v2           #method:Ljava/lang/reflect/Method;
    .end local v4           #parameterTypes:[Ljava/lang/Class;
    :catch_35
    move-exception v1

    #@36
    .line 294
    .local v1, e:Ljava/lang/Exception;
    const-string v5, "LGCallLogClass"

    #@38
    new-instance v7, Ljava/lang/StringBuilder;

    #@3a
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@3d
    const-string v8, "it does not exist MSimTelephonyManager.getLine1Number : "

    #@3f
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v7

    #@43
    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@46
    move-result-object v7

    #@47
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@4a
    move-result-object v7

    #@4b
    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4e
    move v5, v6

    #@4f
    .line 296
    goto :goto_e
.end method

.method public static isMultiSimEnabled()Z
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 84
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->isMultiSimEnabledGemini()Z

    #@4
    move-result v5

    #@5
    sput-boolean v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mtkGeminiSupport:Z

    #@7
    .line 85
    sget-boolean v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mtkGeminiSupport:Z

    #@9
    if-eqz v5, :cond_14

    #@b
    .line 86
    const-string v4, "LGCallLogClass"

    #@d
    const-string v5, "it is Gemini Dual SIM"

    #@f
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 87
    const/4 v4, 0x1

    #@13
    .line 105
    .local v2, methodName:Ljava/lang/String;
    :cond_13
    :goto_13
    return v4

    #@14
    .line 90
    .end local v2           #methodName:Ljava/lang/String;
    :cond_14
    const-string v2, "isMultiSimEnabled"

    #@16
    .line 92
    .restart local v2       #methodName:Ljava/lang/String;
    :try_start_16
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefaultMSim()Landroid/telephony/MSimTelephonyManager;

    #@19
    move-result-object v5

    #@1a
    sput-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@1c
    .line 93
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@1e
    if-eqz v5, :cond_13

    #@20
    .line 97
    const-string v5, "android.telephony.MSimTelephonyManager"

    #@22
    const/4 v6, 0x0

    #@23
    invoke-static {v5, v2, v6}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@26
    move-result-object v1

    #@27
    .line 98
    .local v1, method:Ljava/lang/reflect/Method;
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mSimTManager:Landroid/telephony/MSimTelephonyManager;

    #@29
    const/4 v6, 0x0

    #@2a
    new-array v6, v6, [Ljava/lang/Object;

    #@2c
    invoke-virtual {v1, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@2f
    move-result-object v3

    #@30
    check-cast v3, Ljava/lang/Boolean;

    #@32
    .line 99
    .local v3, ret:Ljava/lang/Boolean;
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_35
    .catch Ljava/lang/Exception; {:try_start_16 .. :try_end_35} :catch_37

    #@35
    move-result v4

    #@36
    goto :goto_13

    #@37
    .line 101
    .end local v1           #method:Ljava/lang/reflect/Method;
    .end local v3           #ret:Ljava/lang/Boolean;
    :catch_37
    move-exception v0

    #@38
    .line 103
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "LGCallLogClass"

    #@3a
    const-string v6, "it does not support multi sim"

    #@3c
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3f
    goto :goto_13
.end method

.method public static isMultiSimEnabledGemini()Z
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 239
    const-string v2, "isMultiSimEnabled"

    #@3
    .line 241
    .local v2, methodName:Ljava/lang/String;
    :try_start_3
    invoke-static {}, Lcom/lge/provider/CallLogClass$MTelephonyManager;->getDefault()Landroid/telephony/TelephonyManager;

    #@6
    move-result-object v5

    #@7
    sput-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@9
    .line 242
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@b
    if-nez v5, :cond_e

    #@d
    .line 254
    :goto_d
    return v4

    #@e
    .line 246
    :cond_e
    const-string v5, "android.telephony.TelephonyManager"

    #@10
    const/4 v6, 0x0

    #@11
    invoke-static {v5, v2, v6}, Lcom/lge/provider/CallLogClass;->getMethod(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    #@14
    move-result-object v1

    #@15
    .line 247
    .local v1, method:Ljava/lang/reflect/Method;
    sget-object v5, Lcom/lge/provider/CallLogClass$MTelephonyManager;->mTManger:Landroid/telephony/TelephonyManager;

    #@17
    const/4 v6, 0x0

    #@18
    new-array v6, v6, [Ljava/lang/Object;

    #@1a
    invoke-virtual {v1, v5, v6}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    #@1d
    move-result-object v3

    #@1e
    check-cast v3, Ljava/lang/Boolean;

    #@20
    .line 248
    .local v3, ret:Ljava/lang/Boolean;
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_23
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_23} :catch_25

    #@23
    move-result v4

    #@24
    goto :goto_d

    #@25
    .line 250
    .end local v1           #method:Ljava/lang/reflect/Method;
    .end local v3           #ret:Ljava/lang/Boolean;
    :catch_25
    move-exception v0

    #@26
    .line 252
    .local v0, e:Ljava/lang/Exception;
    const-string v5, "LGCallLogClass"

    #@28
    const-string v6, "it does not support Gemini Dual SIM"

    #@2a
    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2d
    goto :goto_d
.end method
