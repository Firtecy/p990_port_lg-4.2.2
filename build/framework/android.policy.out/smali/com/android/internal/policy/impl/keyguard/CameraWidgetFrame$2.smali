.class Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$2;
.super Ljava/lang/Object;
.source "CameraWidgetFrame.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 75
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$2;->this$0:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 6

    #@0
    .prologue
    .line 78
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$2;->this$0:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@2
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->access$100(Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;)Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_9

    #@8
    .line 84
    :goto_8
    return-void

    #@9
    .line 80
    :cond_9
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$2;->this$0:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@b
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->getWorkerHandler()Landroid/os/Handler;

    #@e
    move-result-object v1

    #@f
    if-eqz v1, :cond_56

    #@11
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$2;->this$0:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@13
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->getWorkerHandler()Landroid/os/Handler;

    #@16
    move-result-object v0

    #@17
    .line 81
    .local v0, worker:Landroid/os/Handler;
    :goto_17
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$2;->this$0:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@19
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1c
    move-result-wide v2

    #@1d
    invoke-static {v1, v2, v3}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->access$302(Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;J)J

    #@20
    .line 82
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->access$400()Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_46

    #@26
    invoke-static {}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->access$500()Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    new-instance v2, Ljava/lang/StringBuilder;

    #@2c
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2f
    const-string v3, "Launching camera at "

    #@31
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v2

    #@35
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$2;->this$0:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@37
    invoke-static {v3}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->access$300(Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;)J

    #@3a
    move-result-wide v3

    #@3b
    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v2

    #@3f
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@42
    move-result-object v2

    #@43
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@46
    .line 83
    :cond_46
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$2;->this$0:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@48
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->access$700(Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;)Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;

    #@4b
    move-result-object v1

    #@4c
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$2;->this$0:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@4e
    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->access$600(Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;)Ljava/lang/Runnable;

    #@51
    move-result-object v2

    #@52
    invoke-virtual {v1, v0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardActivityLauncher;->launchCamera(Landroid/os/Handler;Ljava/lang/Runnable;)V

    #@55
    goto :goto_8

    #@56
    .line 80
    .end local v0           #worker:Landroid/os/Handler;
    :cond_56
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame$2;->this$0:Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;

    #@58
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;->access$200(Lcom/android/internal/policy/impl/keyguard/CameraWidgetFrame;)Landroid/os/Handler;

    #@5b
    move-result-object v0

    #@5c
    goto :goto_17
.end method
