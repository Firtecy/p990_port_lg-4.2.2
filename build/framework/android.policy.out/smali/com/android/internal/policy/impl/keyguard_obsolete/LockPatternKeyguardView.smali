.class public Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;
.super Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;
.source "LockPatternKeyguardView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$5;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$FastBitmapDrawable;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;
    }
.end annotation


# static fields
.field static final ACTION_EMERGENCY_DIAL:Ljava/lang/String; = "com.android.phone.EmergencyDialer.DIAL"

.field private static final DEBUG:Z = false

.field static final DEBUG_CONFIGURATION:Z = false

.field private static final EMERGENCY_CALL_TIMEOUT:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "LockPatternKeyguardView"

.field private static final TRANSPORT_USERACTIVITY_TIMEOUT:I = 0x2710

.field static isBooting:Z

.field private static sIsFirstAppearanceAfterBoot:Z


# instance fields
.field private final BIOMETRIC_AREA_EMERGENCY_DIALER_TIMEOUT:I

.field private mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

.field private final mBiometricUnlockStartupLock:Ljava/lang/Object;

.field private mConfiguration:Landroid/content/res/Configuration;

.field private mEnableFallback:Z

.field private mForgotPattern:Z

.field private mHasDialog:Z

.field mInfoCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

.field private mIsVerifyUnlockOnly:Z

.field mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

.field private final mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mLockScreen:Landroid/view/View;

.field private mMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

.field private mPluggedIn:Z

.field private mRecreateRunnable:Ljava/lang/Runnable;

.field private mRequiresSim:Z

.field private mSavedState:Landroid/os/Parcelable;

.field private mScreenOn:Z

.field private mShowLockBeforeUnlock:Z

.field private mSuppressBiometricUnlock:Z

.field private mTransportControlView:Lcom/android/internal/widget/TransportControlView;

.field private mUnlockScreen:Landroid/view/View;

.field private mUnlockScreenMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

.field private final mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

.field private mWidgetCallback:Lcom/android/internal/widget/LockScreenWidgetCallback;

.field private final mWindowController:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardWindowController;

.field private mWindowFocused:Z


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 98
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->isBooting:Z

    #@3
    .line 116
    sput-boolean v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->sIsFirstAppearanceAfterBoot:Z

    #@5
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardWindowController;)V
    .registers 9
    .parameter "context"
    .parameter "callback"
    .parameter "updateMonitor"
    .parameter "lockPatternUtils"
    .parameter "controller"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 438
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;-><init>(Landroid/content/Context;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;)V

    #@4
    .line 93
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mWindowFocused:Z

    #@6
    .line 94
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mEnableFallback:Z

    #@8
    .line 96
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    #@a
    .line 101
    new-instance v0, Ljava/lang/Object;

    #@c
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@f
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlockStartupLock:Ljava/lang/Object;

    #@11
    .line 104
    const/16 v0, 0x3e8

    #@13
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->BIOMETRIC_AREA_EMERGENCY_DIALER_TIMEOUT:I

    #@15
    .line 112
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mHasDialog:Z

    #@17
    .line 172
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->LockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@19
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@1b
    .line 178
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Unknown:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@1d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@1f
    .line 186
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    #@21
    .line 198
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$1;

    #@23
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)V

    #@26
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    #@28
    .line 218
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$2;

    #@2a
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$2;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)V

    #@2d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mWidgetCallback:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@2f
    .line 272
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$3;

    #@31
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$3;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)V

    #@34
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@36
    .line 690
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$4;

    #@38
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$4;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)V

    #@3b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@3d
    .line 440
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@40
    move-result-object v0

    #@41
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@44
    move-result-object v0

    #@45
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    #@47
    .line 441
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mEnableFallback:Z

    #@49
    .line 442
    const-string v0, "keyguard.no_require_sim"

    #@4b
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@4e
    move-result-object v0

    #@4f
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@52
    move-result v0

    #@53
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mRequiresSim:Z

    #@55
    .line 443
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@57
    .line 444
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@59
    .line 445
    iput-object p5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mWindowController:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardWindowController;

    #@5b
    .line 446
    sget-boolean v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->sIsFirstAppearanceAfterBoot:Z

    #@5d
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    #@5f
    .line 447
    sput-boolean v2, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->sIsFirstAppearanceAfterBoot:Z

    #@61
    .line 448
    const-string v0, "power"

    #@63
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@66
    move-result-object v0

    #@67
    check-cast v0, Landroid/os/PowerManager;

    #@69
    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    #@6c
    move-result v0

    #@6d
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mScreenOn:Z

    #@6f
    .line 449
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@71
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@73
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;)V

    #@76
    .line 455
    const/4 v0, 0x1

    #@77
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->setFocusableInTouchMode(Z)V

    #@7a
    .line 456
    const/high16 v0, 0x4

    #@7c
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->setDescendantFocusability(I)V

    #@7f
    .line 458
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getInitialMode()Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@82
    move-result-object v0

    #@83
    invoke-direct {p0, v0, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->updateScreen(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;Z)V

    #@86
    .line 459
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->maybeEnableFallback(Landroid/content/Context;)V

    #@89
    .line 460
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@2
    return-object v0
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getUnlockMode()Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->isSecure()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$1202(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    #@2
    return p1
.end method

.method static synthetic access$1300(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@2
    return-object v0
.end method

.method static synthetic access$1402(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Landroid/os/Parcelable;)Landroid/os/Parcelable;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mSavedState:Landroid/os/Parcelable;

    #@2
    return-object p1
.end method

.method static synthetic access$1500(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;II)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->showAlmostAtWipeDialog(II)V

    #@3
    return-void
.end method

.method static synthetic access$1600(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->showWipeDialog(I)V

    #@3
    return-void
.end method

.method static synthetic access$1700(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->showAlmostAtAccountLoginDialog()V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->showTimeoutDialog()V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Landroid/view/View;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;Z)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->updateScreen(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;Z)V

    #@3
    return-void
.end method

.method static synthetic access$2100(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mPluggedIn:Z

    #@2
    return v0
.end method

.method static synthetic access$2102(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mPluggedIn:Z

    #@2
    return p1
.end method

.method static synthetic access$2200(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getInitialMode()Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@3
    move-result-object v0

    #@4
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->restoreWidgetState()V

    #@3
    return-void
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    return-object v0
.end method

.method static synthetic access$502(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mForgotPattern:Z

    #@2
    return p1
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    #@2
    return v0
.end method

.method static synthetic access$602(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    #@2
    return p1
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->stuckOnLockScreenBecauseSimMissing()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$800(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Lcom/android/internal/widget/LockPatternUtils;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2
    return-object v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mEnableFallback:Z

    #@2
    return v0
.end method

.method static synthetic access$902(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 71
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mEnableFallback:Z

    #@2
    return p1
.end method

.method private getInitialMode()Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;
    .registers 3

    #@0
    .prologue
    .line 1042
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@5
    move-result-object v0

    #@6
    .line 1043
    .local v0, simState:Lcom/android/internal/telephony/IccCardConstants$State;
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->stuckOnLockScreenBecauseSimMissing()Z

    #@9
    move-result v1

    #@a
    if-nez v1, :cond_18

    #@c
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@e
    if-ne v0, v1, :cond_1b

    #@10
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@12
    invoke-virtual {v1}, Lcom/android/internal/widget/LockPatternUtils;->isPukUnlockScreenEnable()Z

    #@15
    move-result v1

    #@16
    if-nez v1, :cond_1b

    #@18
    .line 1046
    :cond_18
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->LockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@1a
    .line 1051
    :goto_1a
    return-object v1

    #@1b
    .line 1048
    :cond_1b
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->isSecure()Z

    #@1e
    move-result v1

    #@1f
    if-eqz v1, :cond_25

    #@21
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    #@23
    if-eqz v1, :cond_28

    #@25
    .line 1049
    :cond_25
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->LockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@27
    goto :goto_1a

    #@28
    .line 1051
    :cond_28
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@2a
    goto :goto_1a
.end method

.method private getUnlockMode()Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    .registers 7

    #@0
    .prologue
    .line 1060
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@5
    move-result-object v2

    #@6
    .line 1062
    .local v2, simState:Lcom/android/internal/telephony/IccCardConstants$State;
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@8
    if-ne v2, v3, :cond_d

    #@a
    .line 1063
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->SimPin:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@c
    .line 1092
    .local v0, currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    :goto_c
    return-object v0

    #@d
    .line 1064
    .end local v0           #currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    :cond_d
    sget-object v3, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@f
    if-ne v2, v3, :cond_14

    #@11
    .line 1065
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@13
    .restart local v0       #currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    goto :goto_c

    #@14
    .line 1067
    .end local v0           #currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    :cond_14
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@16
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality()I

    #@19
    move-result v1

    #@1a
    .line 1068
    .local v1, mode:I
    sparse-switch v1, :sswitch_data_56

    #@1d
    .line 1089
    new-instance v3, Ljava/lang/IllegalStateException;

    #@1f
    new-instance v4, Ljava/lang/StringBuilder;

    #@21
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@24
    const-string v5, "Unknown unlock mode:"

    #@26
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@29
    move-result-object v4

    #@2a
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2d
    move-result-object v4

    #@2e
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31
    move-result-object v4

    #@32
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@35
    throw v3

    #@36
    .line 1073
    :sswitch_36
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Password:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@38
    .line 1074
    .restart local v0       #currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    goto :goto_c

    #@39
    .line 1077
    .end local v0           #currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    :sswitch_39
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@3b
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->isLockPatternEnabled()Z

    #@3e
    move-result v3

    #@3f
    if-eqz v3, :cond_53

    #@41
    .line 1079
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mForgotPattern:Z

    #@43
    if-nez v3, :cond_4d

    #@45
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@47
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->isPermanentlyLocked()Z

    #@4a
    move-result v3

    #@4b
    if-eqz v3, :cond_50

    #@4d
    .line 1080
    :cond_4d
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Account:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@4f
    .restart local v0       #currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    goto :goto_c

    #@50
    .line 1082
    .end local v0           #currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    :cond_50
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@52
    .restart local v0       #currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    goto :goto_c

    #@53
    .line 1085
    .end local v0           #currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    :cond_53
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Unknown:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@55
    .line 1087
    .restart local v0       #currentMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    goto :goto_c

    #@56
    .line 1068
    :sswitch_data_56
    .sparse-switch
        0x0 -> :sswitch_39
        0x10000 -> :sswitch_39
        0x20000 -> :sswitch_36
        0x40000 -> :sswitch_36
        0x50000 -> :sswitch_36
        0x60000 -> :sswitch_36
    .end sparse-switch
.end method

.method private initializeBiometricUnlockView(Landroid/view/View;)V
    .registers 9
    .parameter "view"

    #@0
    .prologue
    .line 994
    const/4 v1, 0x0

    #@1
    .line 996
    .local v1, restartBiometricUnlock:Z
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@3
    if-eqz v2, :cond_b

    #@5
    .line 997
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@7
    invoke-interface {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->stop()Z

    #@a
    move-result v1

    #@b
    .line 1005
    :cond_b
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mScreenOn:Z

    #@d
    if-nez v2, :cond_1e

    #@f
    .line 1006
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@11
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getPhoneState()I

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_1b

    #@17
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mHasDialog:Z

    #@19
    if-eqz v2, :cond_59

    #@1b
    :cond_1b
    const/4 v2, 0x1

    #@1c
    :goto_1c
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    #@1e
    .line 1013
    :cond_1e
    const/4 v2, 0x0

    #@1f
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@21
    .line 1014
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->useBiometricUnlock()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_4f

    #@27
    .line 1018
    const v2, 0x10202bc

    #@2a
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@2d
    move-result-object v0

    #@2e
    .line 1019
    .local v0, biometricUnlockView:Landroid/view/View;
    if-eqz v0, :cond_5b

    #@30
    .line 1020
    new-instance v2, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;

    #@32
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@34
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@36
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@38
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@3a
    invoke-direct {v2, v3, v4, v5, v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/FaceUnlock;-><init>(Landroid/content/Context;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;)V

    #@3d
    iput-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@3f
    .line 1022
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@41
    invoke-interface {v2, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->initializeView(Landroid/view/View;)V

    #@44
    .line 1026
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mScreenOn:Z

    #@46
    if-nez v2, :cond_4f

    #@48
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@4a
    const-wide/16 v3, 0x0

    #@4c
    invoke-interface {v2, v3, v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->show(J)V

    #@4f
    .line 1032
    .end local v0           #biometricUnlockView:Landroid/view/View;
    :cond_4f
    :goto_4f
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@51
    if-eqz v2, :cond_58

    #@53
    if-eqz v1, :cond_58

    #@55
    .line 1033
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->maybeStartBiometricUnlock()V

    #@58
    .line 1035
    :cond_58
    return-void

    #@59
    .line 1006
    :cond_59
    const/4 v2, 0x0

    #@5a
    goto :goto_1c

    #@5b
    .line 1028
    .restart local v0       #biometricUnlockView:Landroid/view/View;
    :cond_5b
    const-string v2, "LockPatternKeyguardView"

    #@5d
    const-string v3, "Couldn\'t find biometric unlock view"

    #@5f
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@62
    goto :goto_4f
.end method

.method private initializeTransportControlView(Landroid/view/View;)V
    .registers 4
    .parameter "view"

    #@0
    .prologue
    .line 967
    const v0, 0x10202f8

    #@3
    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    #@6
    move-result-object v0

    #@7
    check-cast v0, Lcom/android/internal/widget/TransportControlView;

    #@9
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mTransportControlView:Lcom/android/internal/widget/TransportControlView;

    #@b
    .line 968
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mTransportControlView:Lcom/android/internal/widget/TransportControlView;

    #@d
    if-nez v0, :cond_10

    #@f
    .line 975
    :goto_f
    return-void

    #@10
    .line 971
    :cond_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@12
    const/4 v1, 0x1

    #@13
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->reportClockVisible(Z)V

    #@16
    .line 972
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mTransportControlView:Lcom/android/internal/widget/TransportControlView;

    #@18
    const/16 v1, 0x8

    #@1a
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/TransportControlView;->setVisibility(I)V

    #@1d
    .line 973
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mTransportControlView:Lcom/android/internal/widget/TransportControlView;

    #@1f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mWidgetCallback:Lcom/android/internal/widget/LockScreenWidgetCallback;

    #@21
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/TransportControlView;->setCallback(Lcom/android/internal/widget/LockScreenWidgetCallback;)V

    #@24
    goto :goto_f
.end method

.method private isSecure()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 802
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getUnlockMode()Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@5
    move-result-object v1

    #@6
    .line 803
    .local v1, unlockMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    const/4 v0, 0x0

    #@7
    .line 804
    .local v0, secure:Z
    sget-object v4, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$5;->$SwitchMap$com$android$internal$policy$impl$keyguard_obsolete$LockPatternKeyguardView$UnlockMode:[I

    #@9
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->ordinal()I

    #@c
    move-result v5

    #@d
    aget v4, v4, v5

    #@f
    packed-switch v4, :pswitch_data_58

    #@12
    .line 824
    new-instance v2, Ljava/lang/IllegalStateException;

    #@14
    new-instance v3, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v4, "unknown unlock mode "

    #@1b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v3

    #@27
    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@2a
    throw v2

    #@2b
    .line 806
    :pswitch_2b
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@2d
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->isLockPatternEnabled()Z

    #@30
    move-result v0

    #@31
    .line 826
    :goto_31
    :pswitch_31
    return v0

    #@32
    .line 809
    :pswitch_32
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@34
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@37
    move-result-object v4

    #@38
    sget-object v5, Lcom/android/internal/telephony/IccCardConstants$State;->PIN_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@3a
    if-ne v4, v5, :cond_3e

    #@3c
    move v0, v2

    #@3d
    .line 810
    :goto_3d
    goto :goto_31

    #@3e
    :cond_3e
    move v0, v3

    #@3f
    .line 809
    goto :goto_3d

    #@40
    .line 812
    :pswitch_40
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@42
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@45
    move-result-object v4

    #@46
    sget-object v5, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@48
    if-ne v4, v5, :cond_4c

    #@4a
    move v0, v2

    #@4b
    .line 813
    :goto_4b
    goto :goto_31

    #@4c
    :cond_4c
    move v0, v3

    #@4d
    .line 812
    goto :goto_4b

    #@4e
    .line 815
    :pswitch_4e
    const/4 v0, 0x1

    #@4f
    .line 816
    goto :goto_31

    #@50
    .line 818
    :pswitch_50
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@52
    invoke-virtual {v2}, Lcom/android/internal/widget/LockPatternUtils;->isLockPasswordEnabled()Z

    #@55
    move-result v0

    #@56
    .line 819
    goto :goto_31

    #@57
    .line 804
    nop

    #@58
    :pswitch_data_58
    .packed-switch 0x1
        :pswitch_2b
        :pswitch_32
        :pswitch_40
        :pswitch_4e
        :pswitch_50
        :pswitch_31
    .end packed-switch
.end method

.method private maybeEnableFallback(Landroid/content/Context;)V
    .registers 5
    .parameter "context"

    #@0
    .prologue
    .line 516
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;

    #@2
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    #@5
    move-result-object v1

    #@6
    const/4 v2, 0x0

    #@7
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;Landroid/accounts/AccountManager;Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$1;)V

    #@a
    .line 517
    .local v0, accountAnalyzer:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$AccountAnalyzer;->start()V

    #@d
    .line 518
    return-void
.end method

.method private maybeStartBiometricUnlock()V
    .registers 4

    #@0
    .prologue
    .line 1210
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@2
    if-eqz v1, :cond_29

    #@4
    .line 1211
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@6
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getFailedAttempts()I

    #@9
    move-result v1

    #@a
    const/4 v2, 0x5

    #@b
    if-lt v1, v2, :cond_2a

    #@d
    const/4 v0, 0x1

    #@e
    .line 1213
    .local v0, backupIsTimedOut:Z
    :goto_e
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    #@10
    if-nez v1, :cond_2c

    #@12
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@14
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getPhoneState()I

    #@17
    move-result v1

    #@18
    if-nez v1, :cond_2c

    #@1a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@1c
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getMaxBiometricUnlockAttemptsReached()Z

    #@1f
    move-result v1

    #@20
    if-nez v1, :cond_2c

    #@22
    if-nez v0, :cond_2c

    #@24
    .line 1217
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@26
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->start()Z

    #@29
    .line 1222
    .end local v0           #backupIsTimedOut:Z
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 1211
    :cond_2a
    const/4 v0, 0x0

    #@2b
    goto :goto_e

    #@2c
    .line 1219
    .restart local v0       #backupIsTimedOut:Z
    :cond_2c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@2e
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->hide()V

    #@31
    goto :goto_29
.end method

.method private recreateLockScreen()V
    .registers 3

    #@0
    .prologue
    .line 641
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 642
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@6
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@8
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->onPause()V

    #@b
    .line 643
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@d
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@f
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->cleanUp()V

    #@12
    .line 644
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@14
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->removeView(Landroid/view/View;)V

    #@17
    .line 647
    :cond_17
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->createLockScreen()Landroid/view/View;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@1d
    .line 648
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@1f
    const/4 v1, 0x4

    #@20
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@23
    .line 649
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@25
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->addView(Landroid/view/View;)V

    #@28
    .line 650
    return-void
.end method

.method private recreateUnlockScreen(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;)V
    .registers 4
    .parameter "unlockMode"

    #@0
    .prologue
    .line 653
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@2
    if-eqz v0, :cond_17

    #@4
    .line 654
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@6
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@8
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->onPause()V

    #@b
    .line 655
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@d
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@f
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->cleanUp()V

    #@12
    .line 656
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@14
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->removeView(Landroid/view/View;)V

    #@17
    .line 659
    :cond_17
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->createUnlockScreenFor(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;)Landroid/view/View;

    #@1a
    move-result-object v0

    #@1b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@1d
    .line 660
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@1f
    const/4 v1, 0x4

    #@20
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    #@23
    .line 661
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@25
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->addView(Landroid/view/View;)V

    #@28
    .line 662
    return-void
.end method

.method private restoreWidgetState()V
    .registers 3

    #@0
    .prologue
    .line 589
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mTransportControlView:Lcom/android/internal/widget/TransportControlView;

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 591
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mSavedState:Landroid/os/Parcelable;

    #@6
    if-eqz v0, :cond_f

    #@8
    .line 592
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mTransportControlView:Lcom/android/internal/widget/TransportControlView;

    #@a
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mSavedState:Landroid/os/Parcelable;

    #@c
    invoke-virtual {v0, v1}, Lcom/android/internal/widget/TransportControlView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    #@f
    .line 595
    :cond_f
    return-void
.end method

.method private saveWidgetState()V
    .registers 2

    #@0
    .prologue
    .line 582
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mTransportControlView:Lcom/android/internal/widget/TransportControlView;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 584
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mTransportControlView:Lcom/android/internal/widget/TransportControlView;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/widget/TransportControlView;->onSaveInstanceState()Landroid/os/Parcelable;

    #@9
    move-result-object v0

    #@a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mSavedState:Landroid/os/Parcelable;

    #@c
    .line 586
    :cond_c
    return-void
.end method

.method private showAlmostAtAccountLoginDialog()V
    .registers 9

    #@0
    .prologue
    .line 1124
    const/16 v2, 0x1e

    #@2
    .line 1125
    .local v2, timeoutInSeconds:I
    const/16 v0, 0xf

    #@4
    .line 1127
    .local v0, count:I
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@6
    const v4, 0x1040331

    #@9
    const/4 v5, 0x3

    #@a
    new-array v5, v5, [Ljava/lang/Object;

    #@c
    const/4 v6, 0x0

    #@d
    const/16 v7, 0xf

    #@f
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@12
    move-result-object v7

    #@13
    aput-object v7, v5, v6

    #@15
    const/4 v6, 0x1

    #@16
    const/4 v7, 0x5

    #@17
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@1a
    move-result-object v7

    #@1b
    aput-object v7, v5, v6

    #@1d
    const/4 v6, 0x2

    #@1e
    const/16 v7, 0x1e

    #@20
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@23
    move-result-object v7

    #@24
    aput-object v7, v5, v6

    #@26
    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@29
    move-result-object v1

    #@2a
    .line 1129
    .local v1, message:Ljava/lang/String;
    const/4 v3, 0x0

    #@2b
    invoke-direct {p0, v3, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@2e
    .line 1130
    return-void
.end method

.method private showAlmostAtWipeDialog(II)V
    .registers 10
    .parameter "attempts"
    .parameter "remaining"

    #@0
    .prologue
    .line 1133
    const/16 v1, 0x1e

    #@2
    .line 1134
    .local v1, timeoutInSeconds:I
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@4
    const v3, 0x1040332

    #@7
    const/4 v4, 0x2

    #@8
    new-array v4, v4, [Ljava/lang/Object;

    #@a
    const/4 v5, 0x0

    #@b
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@e
    move-result-object v6

    #@f
    aput-object v6, v4, v5

    #@11
    const/4 v5, 0x1

    #@12
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@15
    move-result-object v6

    #@16
    aput-object v6, v4, v5

    #@18
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@1b
    move-result-object v0

    #@1c
    .line 1136
    .local v0, message:Ljava/lang/String;
    const/4 v2, 0x0

    #@1d
    invoke-direct {p0, v2, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@20
    .line 1137
    return-void
.end method

.method private showDialog(Ljava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "title"
    .parameter "message"

    #@0
    .prologue
    .line 1096
    const/4 v1, 0x1

    #@1
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mHasDialog:Z

    #@3
    .line 1097
    new-instance v1, Landroid/app/AlertDialog$Builder;

    #@5
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@7
    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    #@a
    invoke-virtual {v1, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@d
    move-result-object v1

    #@e
    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    #@11
    move-result-object v1

    #@12
    const v2, 0x104000a

    #@15
    const/4 v3, 0x0

    #@16
    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@19
    move-result-object v1

    #@1a
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@1d
    move-result-object v0

    #@1e
    .line 1102
    .local v0, dialog:Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@21
    move-result-object v1

    #@22
    const/16 v2, 0x7d9

    #@24
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@27
    .line 1103
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    #@2a
    .line 1104
    return-void
.end method

.method private showTimeoutDialog()V
    .registers 8

    #@0
    .prologue
    .line 1107
    const/16 v2, 0x1e

    #@2
    .line 1108
    .local v2, timeoutInSeconds:I
    const v1, 0x104032e

    #@5
    .line 1109
    .local v1, messageId:I
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getUnlockMode()Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@8
    move-result-object v3

    #@9
    sget-object v4, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Password:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@b
    if-ne v3, v4, :cond_1a

    #@d
    .line 1110
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@f
    invoke-virtual {v3}, Lcom/android/internal/widget/LockPatternUtils;->getKeyguardStoredPasswordQuality()I

    #@12
    move-result v3

    #@13
    const/high16 v4, 0x2

    #@15
    if-ne v3, v4, :cond_3c

    #@17
    .line 1112
    const v1, 0x1040330

    #@1a
    .line 1117
    :cond_1a
    :goto_1a
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@1c
    const/4 v4, 0x2

    #@1d
    new-array v4, v4, [Ljava/lang/Object;

    #@1f
    const/4 v5, 0x0

    #@20
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@22
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getFailedAttempts()I

    #@25
    move-result v6

    #@26
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@29
    move-result-object v6

    #@2a
    aput-object v6, v4, v5

    #@2c
    const/4 v5, 0x1

    #@2d
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@30
    move-result-object v6

    #@31
    aput-object v6, v4, v5

    #@33
    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@36
    move-result-object v0

    #@37
    .line 1120
    .local v0, message:Ljava/lang/String;
    const/4 v3, 0x0

    #@38
    invoke-direct {p0, v3, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@3b
    .line 1121
    return-void

    #@3c
    .line 1114
    .end local v0           #message:Ljava/lang/String;
    :cond_3c
    const v1, 0x104032f

    #@3f
    goto :goto_1a
.end method

.method private showWipeDialog(I)V
    .registers 8
    .parameter "attempts"

    #@0
    .prologue
    .line 1140
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@2
    const v2, 0x1040333

    #@5
    const/4 v3, 0x1

    #@6
    new-array v3, v3, [Ljava/lang/Object;

    #@8
    const/4 v4, 0x0

    #@9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@c
    move-result-object v5

    #@d
    aput-object v5, v3, v4

    #@f
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@12
    move-result-object v0

    #@13
    .line 1143
    .local v0, message:Ljava/lang/String;
    const/4 v1, 0x0

    #@14
    invoke-direct {p0, v1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->showDialog(Ljava/lang/String;Ljava/lang/String;)V

    #@17
    .line 1144
    return-void
.end method

.method private stuckOnLockScreenBecauseSimMissing()Z
    .registers 3

    #@0
    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mRequiresSim:Z

    #@2
    if-eqz v0, :cond_22

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->isDeviceProvisioned()Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_22

    #@c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@e
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@11
    move-result-object v0

    #@12
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->ABSENT:Lcom/android/internal/telephony/IccCardConstants$State;

    #@14
    if-eq v0, v1, :cond_20

    #@16
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@18
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@1b
    move-result-object v0

    #@1c
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PERM_DISABLED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@1e
    if-ne v0, v1, :cond_22

    #@20
    :cond_20
    const/4 v0, 0x1

    #@21
    :goto_21
    return v0

    #@22
    :cond_22
    const/4 v0, 0x0

    #@23
    goto :goto_21
.end method

.method private updateScreen(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;Z)V
    .registers 9
    .parameter "mode"
    .parameter "force"

    #@0
    .prologue
    .line 834
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@2
    .line 837
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->LockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@4
    if-eq p1, v3, :cond_a

    #@6
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    #@8
    if-eqz v3, :cond_13

    #@a
    .line 838
    :cond_a
    if-nez p2, :cond_10

    #@c
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@e
    if-nez v3, :cond_13

    #@10
    .line 839
    :cond_10
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->recreateLockScreen()V

    #@13
    .line 844
    :cond_13
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getUnlockMode()Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@16
    move-result-object v1

    #@17
    .line 845
    .local v1, unlockMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@19
    if-ne p1, v3, :cond_2c

    #@1b
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Unknown:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@1d
    if-eq v1, v3, :cond_2c

    #@1f
    .line 846
    if-nez p2, :cond_29

    #@21
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@23
    if-eqz v3, :cond_29

    #@25
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@27
    if-eq v1, v3, :cond_2c

    #@29
    .line 847
    :cond_29
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->recreateUnlockScreen(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;)V

    #@2c
    .line 852
    :cond_2c
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->LockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@2e
    if-ne p1, v3, :cond_97

    #@30
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@32
    .line 853
    .local v0, goneScreen:Landroid/view/View;
    :goto_32
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->LockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@34
    if-ne p1, v3, :cond_9a

    #@36
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@38
    .line 857
    .local v2, visibleScreen:Landroid/view/View;
    :goto_38
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mWindowController:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardWindowController;

    #@3a
    move-object v3, v2

    #@3b
    check-cast v3, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@3d
    invoke-interface {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->needsInput()Z

    #@40
    move-result v3

    #@41
    invoke-interface {v4, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardWindowController;->setNeedsInput(Z)V

    #@44
    .line 864
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mScreenOn:Z

    #@46
    if-eqz v3, :cond_62

    #@48
    .line 865
    if-eqz v0, :cond_56

    #@4a
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    #@4d
    move-result v3

    #@4e
    if-nez v3, :cond_56

    #@50
    move-object v3, v0

    #@51
    .line 866
    check-cast v3, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@53
    invoke-interface {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->onPause()V

    #@56
    .line 868
    :cond_56
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    #@59
    move-result v3

    #@5a
    if-eqz v3, :cond_62

    #@5c
    move-object v3, v2

    #@5d
    .line 869
    check-cast v3, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@5f
    invoke-interface {v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->onResume()V

    #@62
    .line 873
    :cond_62
    if-eqz v0, :cond_69

    #@64
    .line 874
    const/16 v3, 0x8

    #@66
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    #@69
    .line 876
    :cond_69
    const/4 v3, 0x0

    #@6a
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    #@6d
    .line 877
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->requestLayout()V

    #@70
    .line 879
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    #@73
    move-result v3

    #@74
    if-nez v3, :cond_9d

    #@76
    .line 880
    new-instance v3, Ljava/lang/IllegalStateException;

    #@78
    new-instance v4, Ljava/lang/StringBuilder;

    #@7a
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@7d
    const-string v5, "keyguard screen must be able to take focus when shown "

    #@7f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@82
    move-result-object v4

    #@83
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    #@86
    move-result-object v5

    #@87
    invoke-virtual {v5}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    #@8a
    move-result-object v5

    #@8b
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8e
    move-result-object v4

    #@8f
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@92
    move-result-object v4

    #@93
    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    #@96
    throw v3

    #@97
    .line 852
    .end local v0           #goneScreen:Landroid/view/View;
    .end local v2           #visibleScreen:Landroid/view/View;
    :cond_97
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@99
    goto :goto_32

    #@9a
    .line 853
    .restart local v0       #goneScreen:Landroid/view/View;
    :cond_9a
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@9c
    goto :goto_38

    #@9d
    .line 883
    .restart local v2       #visibleScreen:Landroid/view/View;
    :cond_9d
    return-void
.end method

.method private useBiometricUnlock()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x1

    #@1
    const/4 v3, 0x0

    #@2
    .line 983
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getUnlockMode()Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@5
    move-result-object v1

    #@6
    .line 984
    .local v1, unlockMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@8
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getFailedAttempts()I

    #@b
    move-result v4

    #@c
    const/4 v5, 0x5

    #@d
    if-lt v4, v5, :cond_33

    #@f
    move v0, v2

    #@10
    .line 986
    .local v0, backupIsTimedOut:Z
    :goto_10
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@12
    invoke-virtual {v4}, Lcom/android/internal/widget/LockPatternUtils;->usingBiometricWeak()Z

    #@15
    move-result v4

    #@16
    if-eqz v4, :cond_35

    #@18
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@1a
    invoke-virtual {v4}, Lcom/android/internal/widget/LockPatternUtils;->isBiometricWeakInstalled()Z

    #@1d
    move-result v4

    #@1e
    if-eqz v4, :cond_35

    #@20
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@22
    invoke-virtual {v4}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getMaxBiometricUnlockAttemptsReached()Z

    #@25
    move-result v4

    #@26
    if-nez v4, :cond_35

    #@28
    if-nez v0, :cond_35

    #@2a
    sget-object v4, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@2c
    if-eq v1, v4, :cond_32

    #@2e
    sget-object v4, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Password:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@30
    if-ne v1, v4, :cond_35

    #@32
    :cond_32
    :goto_32
    return v2

    #@33
    .end local v0           #backupIsTimedOut:Z
    :cond_33
    move v0, v3

    #@34
    .line 984
    goto :goto_10

    #@35
    .restart local v0       #backupIsTimedOut:Z
    :cond_35
    move v2, v3

    #@36
    .line 986
    goto :goto_32
.end method


# virtual methods
.method public cleanUp()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 783
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@3
    if-eqz v0, :cond_1a

    #@5
    .line 784
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@7
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@9
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->onPause()V

    #@c
    .line 785
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@e
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@10
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->cleanUp()V

    #@13
    .line 786
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@15
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->removeView(Landroid/view/View;)V

    #@18
    .line 787
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@1a
    .line 789
    :cond_1a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@1c
    if-eqz v0, :cond_33

    #@1e
    .line 790
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@20
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@22
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->onPause()V

    #@25
    .line 791
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@27
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@29
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->cleanUp()V

    #@2c
    .line 792
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@2e
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->removeView(Landroid/view/View;)V

    #@31
    .line 793
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@33
    .line 795
    :cond_33
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@35
    invoke-virtual {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    #@38
    .line 796
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@3a
    if-eqz v0, :cond_41

    #@3c
    .line 797
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@3e
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->cleanUp()V

    #@41
    .line 799
    :cond_41
    return-void
.end method

.method createLockScreen()Landroid/view/View;
    .registers 7

    #@0
    .prologue
    .line 886
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@4
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    #@6
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@8
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@a
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@c
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;)V

    #@f
    .line 892
    .local v0, lockView:Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->initializeTransportControlView(Landroid/view/View;)V

    #@12
    .line 893
    return-object v0
.end method

.method createUnlockScreenFor(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;)Landroid/view/View;
    .registers 12
    .parameter "unlockMode"

    #@0
    .prologue
    .line 897
    const/4 v1, 0x0

    #@1
    .line 902
    .local v1, unlockView:Landroid/view/View;
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@3
    if-ne p1, v3, :cond_2a

    #@5
    .line 903
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;

    #@7
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@9
    .end local v1           #unlockView:Landroid/view/View;
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    #@b
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@d
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@f
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@11
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@13
    invoke-virtual {v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getFailedAttempts()I

    #@16
    move-result v6

    #@17
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;I)V

    #@1a
    .line 910
    .local v0, view:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mEnableFallback:Z

    #@1c
    invoke-virtual {v0, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;->setEnableFallback(Z)V

    #@1f
    .line 911
    move-object v1, v0

    #@20
    .line 959
    .end local v0           #view:Lcom/android/internal/policy/impl/keyguard_obsolete/PatternUnlockScreen;
    .restart local v1       #unlockView:Landroid/view/View;
    :goto_20
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->initializeTransportControlView(Landroid/view/View;)V

    #@23
    .line 960
    invoke-direct {p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->initializeBiometricUnlockView(Landroid/view/View;)V

    #@26
    .line 962
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@28
    move-object v3, v1

    #@29
    .line 963
    :goto_29
    return-object v3

    #@2a
    .line 912
    :cond_2a
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->SimPuk:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@2c
    if-ne p1, v3, :cond_3e

    #@2e
    .line 913
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/SimPukUnlockScreen;

    #@30
    .end local v1           #unlockView:Landroid/view/View;
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@32
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    #@34
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@36
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@38
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@3a
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimPukUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;Lcom/android/internal/widget/LockPatternUtils;)V

    #@3d
    .restart local v1       #unlockView:Landroid/view/View;
    goto :goto_20

    #@3e
    .line 919
    :cond_3e
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->SimPin:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@40
    if-ne p1, v3, :cond_52

    #@42
    .line 920
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;

    #@44
    .end local v1           #unlockView:Landroid/view/View;
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@46
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    #@48
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@4a
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@4c
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@4e
    invoke-direct/range {v1 .. v6}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;Lcom/android/internal/widget/LockPatternUtils;)V

    #@51
    .restart local v1       #unlockView:Landroid/view/View;
    goto :goto_20

    #@52
    .line 926
    :cond_52
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Account:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@54
    if-ne p1, v3, :cond_76

    #@56
    .line 928
    :try_start_56
    new-instance v2, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;

    #@58
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@5a
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    #@5c
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@5e
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@60
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@62
    invoke-direct/range {v2 .. v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/AccountUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;Lcom/android/internal/widget/LockPatternUtils;)V
    :try_end_65
    .catch Ljava/lang/IllegalStateException; {:try_start_56 .. :try_end_65} :catch_67

    #@65
    .end local v1           #unlockView:Landroid/view/View;
    .local v2, unlockView:Landroid/view/View;
    move-object v1, v2

    #@66
    .line 948
    .end local v2           #unlockView:Landroid/view/View;
    .restart local v1       #unlockView:Landroid/view/View;
    goto :goto_20

    #@67
    .line 934
    :catch_67
    move-exception v9

    #@68
    .line 935
    .local v9, e:Ljava/lang/IllegalStateException;
    const-string v3, "LockPatternKeyguardView"

    #@6a
    const-string v4, "Couldn\'t instantiate AccountUnlockScreen (IAccountsService isn\'t available)"

    #@6c
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@6f
    .line 947
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@71
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->createUnlockScreenFor(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;)Landroid/view/View;

    #@74
    move-result-object v3

    #@75
    goto :goto_29

    #@76
    .line 949
    .end local v9           #e:Ljava/lang/IllegalStateException;
    :cond_76
    sget-object v3, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Password:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@78
    if-ne p1, v3, :cond_8b

    #@7a
    .line 950
    new-instance v1, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;

    #@7c
    .end local v1           #unlockView:Landroid/view/View;
    iget-object v4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@7e
    iget-object v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    #@80
    iget-object v6, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@82
    iget-object v7, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@84
    iget-object v8, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mKeyguardScreenCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@86
    move-object v3, v1

    #@87
    invoke-direct/range {v3 .. v8}, Lcom/android/internal/policy/impl/keyguard_obsolete/PasswordUnlockScreen;-><init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;)V

    #@8a
    .restart local v1       #unlockView:Landroid/view/View;
    goto :goto_20

    #@8b
    .line 957
    :cond_8b
    new-instance v3, Ljava/lang/IllegalArgumentException;

    #@8d
    new-instance v4, Ljava/lang/StringBuilder;

    #@8f
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@92
    const-string v5, "unknown unlock mode "

    #@94
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@97
    move-result-object v4

    #@98
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@9b
    move-result-object v4

    #@9c
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9f
    move-result-object v4

    #@a0
    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    #@a3
    throw v3
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .registers 2
    .parameter "canvas"

    #@0
    .prologue
    .line 529
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->dispatchDraw(Landroid/graphics/Canvas;)V

    #@3
    .line 530
    return-void
.end method

.method protected dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .registers 4
    .parameter "event"

    #@0
    .prologue
    .line 744
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityManager;->getInstance(Landroid/content/Context;)Landroid/view/accessibility/AccessibilityManager;

    #@5
    move-result-object v0

    #@6
    .line 745
    .local v0, accessibilityManager:Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    #@9
    move-result v1

    #@a
    if-eqz v1, :cond_19

    #@c
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    #@f
    move-result v1

    #@10
    if-eqz v1, :cond_19

    #@12
    .line 746
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getCallback()Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;

    #@15
    move-result-object v1

    #@16
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;->pokeWakelock()V

    #@19
    .line 748
    :cond_19
    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    #@1c
    move-result v1

    #@1d
    return v1
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 4
    .parameter "newConfig"

    #@0
    .prologue
    .line 680
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v0

    #@4
    .line 681
    .local v0, resources:Landroid/content/res/Resources;
    const v1, 0x1110026

    #@7
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@a
    move-result v1

    #@b
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mShowLockBeforeUnlock:Z

    #@d
    .line 682
    iput-object p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mConfiguration:Landroid/content/res/Configuration;

    #@f
    .line 684
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->saveWidgetState()V

    #@12
    .line 685
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    #@14
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@17
    .line 687
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    #@19
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->post(Ljava/lang/Runnable;)Z

    #@1c
    .line 688
    return-void
.end method

.method protected onDetachedFromWindow()V
    .registers 3

    #@0
    .prologue
    .line 666
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitorCallback;

    #@4
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    #@7
    .line 668
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    #@9
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    #@c
    .line 670
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@e
    if-eqz v0, :cond_15

    #@10
    .line 673
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@12
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->stop()Z

    #@15
    .line 676
    :cond_15
    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewBase;->onDetachedFromWindow()V

    #@18
    .line 677
    return-void
.end method

.method public onScreenTurnedOff()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 543
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mScreenOn:Z

    #@3
    .line 544
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mForgotPattern:Z

    #@5
    .line 547
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@7
    if-eqz v0, :cond_10

    #@9
    .line 548
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@b
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@d
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->onPause()V

    #@10
    .line 550
    :cond_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@12
    if-eqz v0, :cond_1b

    #@14
    .line 551
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@16
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@18
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->onPause()V

    #@1b
    .line 554
    :cond_1b
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->saveWidgetState()V

    #@1e
    .line 556
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@20
    if-eqz v0, :cond_27

    #@22
    .line 558
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@24
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->stop()Z

    #@27
    .line 560
    :cond_27
    return-void
.end method

.method public onScreenTurnedOn()V
    .registers 4

    #@0
    .prologue
    .line 565
    const/4 v0, 0x0

    #@1
    .line 567
    .local v0, startBiometricUnlock:Z
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlockStartupLock:Ljava/lang/Object;

    #@3
    monitor-enter v2

    #@4
    .line 568
    const/4 v1, 0x1

    #@5
    :try_start_5
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mScreenOn:Z

    #@7
    .line 569
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mWindowFocused:Z

    #@9
    .line 570
    monitor-exit v2
    :try_end_a
    .catchall {:try_start_5 .. :try_end_a} :catchall_1a

    #@a
    .line 572
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->show()V

    #@d
    .line 574
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->restoreWidgetState()V

    #@10
    .line 576
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@12
    if-eqz v1, :cond_19

    #@14
    if-eqz v0, :cond_19

    #@16
    .line 577
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->maybeStartBiometricUnlock()V

    #@19
    .line 579
    :cond_19
    return-void

    #@1a
    .line 570
    :catchall_1a
    move-exception v1

    #@1b
    :try_start_1b
    monitor-exit v2
    :try_end_1c
    .catchall {:try_start_1b .. :try_end_1c} :catchall_1a

    #@1c
    throw v1
.end method

.method public onWindowFocusChanged(Z)V
    .registers 5
    .parameter "hasWindowFocus"

    #@0
    .prologue
    .line 605
    const/4 v0, 0x0

    #@1
    .line 607
    .local v0, startBiometricUnlock:Z
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlockStartupLock:Ljava/lang/Object;

    #@3
    monitor-enter v2

    #@4
    .line 608
    :try_start_4
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mScreenOn:Z

    #@6
    if-eqz v1, :cond_d

    #@8
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mWindowFocused:Z

    #@a
    if-nez v1, :cond_d

    #@c
    move v0, p1

    #@d
    .line 609
    :cond_d
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mWindowFocused:Z

    #@f
    .line 610
    monitor-exit v2
    :try_end_10
    .catchall {:try_start_4 .. :try_end_10} :catchall_24

    #@10
    .line 611
    if-nez p1, :cond_27

    #@12
    .line 612
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@14
    if-eqz v1, :cond_23

    #@16
    .line 613
    const/4 v1, 0x1

    #@17
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    #@19
    .line 614
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@1b
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->stop()Z

    #@1e
    .line 615
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@20
    invoke-interface {v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->hide()V

    #@23
    .line 623
    :cond_23
    :goto_23
    return-void

    #@24
    .line 610
    :catchall_24
    move-exception v1

    #@25
    :try_start_25
    monitor-exit v2
    :try_end_26
    .catchall {:try_start_25 .. :try_end_26} :catchall_24

    #@26
    throw v1

    #@27
    .line 618
    :cond_27
    const/4 v1, 0x0

    #@28
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mHasDialog:Z

    #@2a
    .line 619
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@2c
    if-eqz v1, :cond_23

    #@2e
    if-eqz v0, :cond_23

    #@30
    .line 620
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->maybeStartBiometricUnlock()V

    #@33
    goto :goto_23
.end method

.method public reset()V
    .registers 2

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 534
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    #@3
    .line 535
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mForgotPattern:Z

    #@5
    .line 537
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mRecreateRunnable:Ljava/lang/Runnable;

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 538
    return-void
.end method

.method public show()V
    .registers 2

    #@0
    .prologue
    .line 628
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@2
    if-eqz v0, :cond_b

    #@4
    .line 629
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mLockScreen:Landroid/view/View;

    #@6
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@8
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->onResume()V

    #@b
    .line 631
    :cond_b
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@d
    if-eqz v0, :cond_16

    #@f
    .line 632
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreen:Landroid/view/View;

    #@11
    check-cast v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;

    #@13
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;->onResume()V

    #@16
    .line 635
    :cond_16
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@18
    if-eqz v0, :cond_23

    #@1a
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mSuppressBiometricUnlock:Z

    #@1c
    if-eqz v0, :cond_23

    #@1e
    .line 636
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mBiometricUnlock:Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;

    #@20
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/BiometricSensorUnlock;->hide()V

    #@23
    .line 638
    :cond_23
    return-void
.end method

.method public verifyUnlock()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 767
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->isSecure()Z

    #@5
    move-result v0

    #@6
    if-nez v0, :cond_10

    #@8
    .line 769
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getCallback()Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;

    #@b
    move-result-object v0

    #@c
    invoke-interface {v0, v3}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;->keyguardDone(Z)V

    #@f
    .line 779
    :goto_f
    return-void

    #@10
    .line 770
    :cond_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@12
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Pattern:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@14
    if-eq v0, v1, :cond_24

    #@16
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUnlockScreenMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@18
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;->Password:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$UnlockMode;

    #@1a
    if-eq v0, v1, :cond_24

    #@1c
    .line 773
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getCallback()Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;

    #@1f
    move-result-object v0

    #@20
    invoke-interface {v0, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;->keyguardDone(Z)V

    #@23
    goto :goto_f

    #@24
    .line 776
    :cond_24
    iput-boolean v3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mIsVerifyUnlockOnly:Z

    #@26
    .line 777
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@28
    invoke-direct {p0, v0, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->updateScreen(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;Z)V

    #@2b
    goto :goto_f
.end method

.method public wakeWhenReadyTq(I)V
    .registers 4
    .parameter "keyCode"

    #@0
    .prologue
    .line 754
    const/16 v0, 0x52

    #@2
    if-ne p1, v0, :cond_28

    #@4
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->isSecure()Z

    #@7
    move-result v0

    #@8
    if-eqz v0, :cond_28

    #@a
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mMode:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@c
    sget-object v1, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->LockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@e
    if-ne v0, v1, :cond_28

    #@10
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@12
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->getSimState()Lcom/android/internal/telephony/IccCardConstants$State;

    #@15
    move-result-object v0

    #@16
    sget-object v1, Lcom/android/internal/telephony/IccCardConstants$State;->PUK_REQUIRED:Lcom/android/internal/telephony/IccCardConstants$State;

    #@18
    if-eq v0, v1, :cond_28

    #@1a
    .line 757
    sget-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;->UnlockScreen:Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;

    #@1c
    const/4 v1, 0x0

    #@1d
    invoke-direct {p0, v0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->updateScreen(Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView$Mode;Z)V

    #@20
    .line 758
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getCallback()Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;

    #@23
    move-result-object v0

    #@24
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;->pokeWakelock()V

    #@27
    .line 763
    :goto_27
    return-void

    #@28
    .line 761
    :cond_28
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/LockPatternKeyguardView;->getCallback()Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;

    #@2b
    move-result-object v0

    #@2c
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardViewCallback;->pokeWakelock()V

    #@2f
    goto :goto_27
.end method
