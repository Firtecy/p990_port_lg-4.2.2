.class final Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;
.super Landroid/view/InputEventReceiver;
.source "PhoneWindowManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PointerLocationInputEventReceiver"
.end annotation


# instance fields
.field private final mView:Lcom/android/internal/widget/PointerLocationView;


# direct methods
.method public constructor <init>(Landroid/view/InputChannel;Landroid/os/Looper;Lcom/android/internal/widget/PointerLocationView;)V
    .registers 4
    .parameter "inputChannel"
    .parameter "looper"
    .parameter "view"

    #@0
    .prologue
    .line 556
    invoke-direct {p0, p1, p2}, Landroid/view/InputEventReceiver;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@3
    .line 557
    iput-object p3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;->mView:Lcom/android/internal/widget/PointerLocationView;

    #@5
    .line 558
    return-void
.end method


# virtual methods
.method public onInputEvent(Landroid/view/InputEvent;)V
    .registers 6
    .parameter "event"

    #@0
    .prologue
    .line 562
    const/4 v1, 0x0

    #@1
    .line 564
    .local v1, handled:Z
    :try_start_1
    instance-of v3, p1, Landroid/view/MotionEvent;

    #@3
    if-eqz v3, :cond_17

    #@5
    invoke-virtual {p1}, Landroid/view/InputEvent;->getSource()I

    #@8
    move-result v3

    #@9
    and-int/lit8 v3, v3, 0x2

    #@b
    if-eqz v3, :cond_17

    #@d
    .line 566
    move-object v0, p1

    #@e
    check-cast v0, Landroid/view/MotionEvent;

    #@10
    move-object v2, v0

    #@11
    .line 567
    .local v2, motionEvent:Landroid/view/MotionEvent;
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;->mView:Lcom/android/internal/widget/PointerLocationView;

    #@13
    invoke-virtual {v3, v2}, Lcom/android/internal/widget/PointerLocationView;->addPointerEvent(Landroid/view/MotionEvent;)V
    :try_end_16
    .catchall {:try_start_1 .. :try_end_16} :catchall_1b

    #@16
    .line 568
    const/4 v1, 0x1

    #@17
    .line 571
    .end local v2           #motionEvent:Landroid/view/MotionEvent;
    :cond_17
    invoke-virtual {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@1a
    .line 573
    return-void

    #@1b
    .line 571
    :catchall_1b
    move-exception v3

    #@1c
    invoke-virtual {p0, p1, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;->finishInputEvent(Landroid/view/InputEvent;Z)V

    #@1f
    throw v3
.end method
