.class Lcom/android/internal/policy/impl/GlobalActions$8;
.super Lcom/android/internal/policy/impl/GlobalActions$SinglePressAction;
.source "GlobalActions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/internal/policy/impl/GlobalActions;->addUsersToMenu(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/GlobalActions;

.field final synthetic val$user:Landroid/content/pm/UserInfo;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/GlobalActions;ILandroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;Landroid/content/pm/UserInfo;)V
    .registers 6
    .parameter
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"
    .parameter

    #@0
    .prologue
    .line 481
    iput-object p1, p0, Lcom/android/internal/policy/impl/GlobalActions$8;->this$0:Lcom/android/internal/policy/impl/GlobalActions;

    #@2
    iput-object p5, p0, Lcom/android/internal/policy/impl/GlobalActions$8;->val$user:Landroid/content/pm/UserInfo;

    #@4
    invoke-direct {p0, p2, p3, p4}, Lcom/android/internal/policy/impl/GlobalActions$SinglePressAction;-><init>(ILandroid/graphics/drawable/Drawable;Ljava/lang/CharSequence;)V

    #@7
    return-void
.end method


# virtual methods
.method public onPress()V
    .registers 5

    #@0
    .prologue
    .line 484
    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@3
    move-result-object v1

    #@4
    iget-object v2, p0, Lcom/android/internal/policy/impl/GlobalActions$8;->val$user:Landroid/content/pm/UserInfo;

    #@6
    iget v2, v2, Landroid/content/pm/UserInfo;->id:I

    #@8
    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->switchUser(I)Z
    :try_end_b
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_b} :catch_c

    #@b
    .line 488
    :goto_b
    return-void

    #@c
    .line 485
    :catch_c
    move-exception v0

    #@d
    .line 486
    .local v0, re:Landroid/os/RemoteException;
    const-string v1, "GlobalActions"

    #@f
    new-instance v2, Ljava/lang/StringBuilder;

    #@11
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@14
    const-string v3, "Couldn\'t switch user "

    #@16
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@19
    move-result-object v2

    #@1a
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v2

    #@1e
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@21
    move-result-object v2

    #@22
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    goto :goto_b
.end method

.method public showBeforeProvisioning()Z
    .registers 2

    #@0
    .prologue
    .line 495
    const/4 v0, 0x0

    #@1
    return v0
.end method

.method public showDuringKeyguard()Z
    .registers 2

    #@0
    .prologue
    .line 491
    const/4 v0, 0x1

    #@1
    return v0
.end method
