.class Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;
.super Landroid/widget/TextView;
.source "KeyguardMessageArea.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$Helper;
    }
.end annotation


# static fields
.field static final BATTERY_LOW_ICON:I = 0x0

.field static final CHARGING_ICON:I = 0x0

.field protected static final FADE_DURATION:I = 0x2ee

.field static final SECURITY_MESSAGE_DURATION:I = 0x1388


# instance fields
.field protected mBatteryCharged:Z

.field protected mBatteryIsLow:Z

.field mBatteryLevel:I

.field mCharging:Z

.field mClearMessageRunnable:Ljava/lang/Runnable;

.field private mHandler:Landroid/os/Handler;

.field private mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

.field mMessage:Ljava/lang/CharSequence;

.field private mSeparator:Ljava/lang/CharSequence;

.field mShowingBatteryInfo:Z

.field mShowingBouncer:Z

.field mShowingMessage:Z

.field mTimeout:J

.field mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .registers 3
    .parameter "context"

    #@0
    .prologue
    .line 158
    const/4 v0, 0x0

    #@1
    invoke-direct {p0, p1, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 159
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .registers 5
    .parameter "context"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 162
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    #@4
    .line 53
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mShowingBatteryInfo:Z

    #@6
    .line 56
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mShowingBouncer:Z

    #@8
    .line 59
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mCharging:Z

    #@a
    .line 62
    const/16 v0, 0x64

    #@c
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mBatteryLevel:I

    #@e
    .line 67
    const-wide/16 v0, 0x1388

    #@10
    iput-wide v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mTimeout:J

    #@12
    .line 77
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$1;

    #@14
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$1;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;)V

    #@17
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mClearMessageRunnable:Ljava/lang/Runnable;

    #@19
    .line 142
    new-instance v0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$2;

    #@1b
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$2;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;)V

    #@1e
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@20
    .line 165
    const/4 v0, 0x1

    #@21
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->setSelected(Z)V

    #@24
    .line 168
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->getContext()Landroid/content/Context;

    #@27
    move-result-object v0

    #@28
    invoke-static {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2b
    move-result-object v0

    #@2c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2e
    .line 169
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@30
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mInfoCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;

    #@32
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->registerCallback(Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitorCallback;)V

    #@35
    .line 170
    new-instance v0, Landroid/os/Handler;

    #@37
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@3a
    move-result-object v1

    #@3b
    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    #@3e
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mHandler:Landroid/os/Handler;

    #@40
    .line 172
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->getResources()Landroid/content/res/Resources;

    #@43
    move-result-object v0

    #@44
    const v1, 0x104056b

    #@47
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@4a
    move-result-object v0

    #@4b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mSeparator:Ljava/lang/CharSequence;

    #@4d
    .line 173
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_NEXTI_LOCKSCREEN:Z

    #@4f
    if-eqz v0, :cond_5a

    #@51
    .line 175
    const-string v0, "/system/fonts/Roboto-Regular.ttf"

    #@53
    invoke-static {v0}, Landroid/graphics/Typeface;->createFromFile(Ljava/lang/String;)Landroid/graphics/Typeface;

    #@56
    move-result-object v0

    #@57
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->setTypeface(Landroid/graphics/Typeface;)V

    #@5a
    .line 178
    :cond_5a
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->update()V

    #@5d
    .line 179
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;IZ)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->hideMessage(IZ)V

    #@3
    return-void
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 45
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->showMessage(I)V

    #@3
    return-void
.end method

.method private varargs concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .registers 7
    .parameter "args"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    #@3
    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    #@6
    .line 208
    .local v0, b:Ljava/lang/StringBuilder;
    aget-object v3, p1, v4

    #@8
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@b
    move-result v3

    #@c
    if-nez v3, :cond_13

    #@e
    .line 209
    aget-object v3, p1, v4

    #@10
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@13
    .line 211
    :cond_13
    const/4 v1, 0x1

    #@14
    .local v1, i:I
    :goto_14
    array-length v3, p1

    #@15
    if-ge v1, v3, :cond_30

    #@17
    .line 212
    aget-object v2, p1, v1

    #@19
    .line 213
    .local v2, text:Ljava/lang/CharSequence;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    #@1c
    move-result v3

    #@1d
    if-nez v3, :cond_2d

    #@1f
    .line 214
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    #@22
    move-result v3

    #@23
    if-lez v3, :cond_2a

    #@25
    .line 215
    iget-object v3, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mSeparator:Ljava/lang/CharSequence;

    #@27
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@2a
    .line 217
    :cond_2a
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    #@2d
    .line 211
    :cond_2d
    add-int/lit8 v1, v1, 0x1

    #@2f
    goto :goto_14

    #@30
    .line 220
    .end local v2           #text:Ljava/lang/CharSequence;
    :cond_30
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@33
    move-result-object v3

    #@34
    return-object v3
.end method

.method private getChargeInfo(Llibcore/util/MutableInt;)Ljava/lang/CharSequence;
    .registers 8
    .parameter "icon"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 237
    const/4 v0, 0x0

    #@2
    .line 238
    .local v0, string:Ljava/lang/CharSequence;
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mShowingBatteryInfo:Z

    #@4
    if-eqz v1, :cond_2a

    #@6
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mShowingMessage:Z

    #@8
    if-nez v1, :cond_2a

    #@a
    .line 240
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mCharging:Z

    #@c
    if-eqz v1, :cond_2f

    #@e
    .line 242
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->getContext()Landroid/content/Context;

    #@11
    move-result-object v2

    #@12
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mBatteryCharged:Z

    #@14
    if-eqz v1, :cond_2b

    #@16
    const v1, 0x1040317

    #@19
    :goto_19
    const/4 v3, 0x1

    #@1a
    new-array v3, v3, [Ljava/lang/Object;

    #@1c
    iget v4, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mBatteryLevel:I

    #@1e
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    #@21
    move-result-object v4

    #@22
    aput-object v4, v3, v5

    #@24
    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    #@27
    move-result-object v0

    #@28
    .line 245
    iput v5, p1, Llibcore/util/MutableInt;->value:I

    #@2a
    .line 253
    :cond_2a
    :goto_2a
    return-object v0

    #@2b
    .line 242
    :cond_2b
    const v1, 0x1040316

    #@2e
    goto :goto_19

    #@2f
    .line 246
    :cond_2f
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mBatteryIsLow:Z

    #@31
    if-eqz v1, :cond_2a

    #@33
    .line 248
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->getContext()Landroid/content/Context;

    #@36
    move-result-object v1

    #@37
    const v2, 0x1040319

    #@3a
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@3d
    move-result-object v0

    #@3e
    .line 250
    iput v5, p1, Llibcore/util/MutableInt;->value:I

    #@40
    goto :goto_2a
.end method

.method private hideMessage(IZ)V
    .registers 8
    .parameter "duration"
    .parameter "thenUpdate"

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 257
    if-lez p1, :cond_21

    #@3
    .line 258
    const-string v1, "alpha"

    #@5
    const/4 v2, 0x1

    #@6
    new-array v2, v2, [F

    #@8
    const/4 v3, 0x0

    #@9
    aput v4, v2, v3

    #@b
    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@e
    move-result-object v0

    #@f
    .line 259
    .local v0, anim:Landroid/animation/Animator;
    int-to-long v1, p1

    #@10
    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@13
    .line 260
    if-eqz p2, :cond_1d

    #@15
    .line 261
    new-instance v1, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$3;

    #@17
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea$3;-><init>(Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;)V

    #@1a
    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    #@1d
    .line 268
    :cond_1d
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@20
    .line 275
    .end local v0           #anim:Landroid/animation/Animator;
    :cond_20
    :goto_20
    return-void

    #@21
    .line 270
    :cond_21
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->setAlpha(F)V

    #@24
    .line 271
    if-eqz p2, :cond_20

    #@26
    .line 272
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->update()V

    #@29
    goto :goto_20
.end method

.method private showMessage(I)V
    .registers 7
    .parameter "duration"

    #@0
    .prologue
    const/high16 v4, 0x3f80

    #@2
    .line 278
    if-lez p1, :cond_18

    #@4
    .line 279
    const-string v1, "alpha"

    #@6
    const/4 v2, 0x1

    #@7
    new-array v2, v2, [F

    #@9
    const/4 v3, 0x0

    #@a
    aput v4, v2, v3

    #@c
    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    #@f
    move-result-object v0

    #@10
    .line 280
    .local v0, anim:Landroid/animation/Animator;
    int-to-long v1, p1

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    #@14
    .line 281
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    #@17
    .line 285
    .end local v0           #anim:Landroid/animation/Animator;
    :goto_17
    return-void

    #@18
    .line 283
    :cond_18
    invoke-virtual {p0, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->setAlpha(F)V

    #@1b
    goto :goto_17
.end method


# virtual methods
.method getCurrentMessage()Ljava/lang/CharSequence;
    .registers 2

    #@0
    .prologue
    .line 224
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mShowingMessage:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mMessage:Ljava/lang/CharSequence;

    #@6
    :goto_6
    return-object v0

    #@7
    :cond_7
    const/4 v0, 0x0

    #@8
    goto :goto_6
.end method

.method getOwnerInfo()Ljava/lang/String;
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v3, -0x2

    #@2
    .line 228
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->getContext()Landroid/content/Context;

    #@5
    move-result-object v2

    #@6
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@9
    move-result-object v1

    #@a
    .line 229
    .local v1, res:Landroid/content/ContentResolver;
    const-string v2, "lock_screen_owner_info_enabled"

    #@c
    invoke-static {v1, v2, v0, v3}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@f
    move-result v2

    #@10
    if-eqz v2, :cond_1f

    #@12
    .line 231
    .local v0, ownerInfoEnabled:Z
    :goto_12
    if-eqz v0, :cond_21

    #@14
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mShowingMessage:Z

    #@16
    if-nez v2, :cond_21

    #@18
    const-string v2, "lock_screen_owner_info"

    #@1a
    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@1d
    move-result-object v2

    #@1e
    :goto_1e
    return-object v2

    #@1f
    .line 229
    .end local v0           #ownerInfoEnabled:Z
    :cond_1f
    const/4 v0, 0x0

    #@20
    goto :goto_12

    #@21
    .line 231
    .restart local v0       #ownerInfoEnabled:Z
    :cond_21
    const/4 v2, 0x0

    #@22
    goto :goto_1e
.end method

.method public securityMessageChanged()V
    .registers 5

    #@0
    .prologue
    .line 182
    const/high16 v0, 0x3f80

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->setAlpha(F)V

    #@5
    .line 183
    const/4 v0, 0x1

    #@6
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mShowingMessage:Z

    #@8
    .line 184
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->update()V

    #@b
    .line 185
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mHandler:Landroid/os/Handler;

    #@d
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mClearMessageRunnable:Ljava/lang/Runnable;

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@12
    .line 186
    iget-wide v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mTimeout:J

    #@14
    const-wide/16 v2, 0x0

    #@16
    cmp-long v0, v0, v2

    #@18
    if-lez v0, :cond_23

    #@1a
    .line 187
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mHandler:Landroid/os/Handler;

    #@1c
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mClearMessageRunnable:Ljava/lang/Runnable;

    #@1e
    iget-wide v2, p0, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->mTimeout:J

    #@20
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@23
    .line 189
    :cond_23
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->getText()Ljava/lang/CharSequence;

    #@26
    move-result-object v0

    #@27
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->announceForAccessibility(Ljava/lang/CharSequence;)V

    #@2a
    .line 190
    return-void
.end method

.method update()V
    .registers 7

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 200
    new-instance v0, Llibcore/util/MutableInt;

    #@3
    invoke-direct {v0, v5}, Llibcore/util/MutableInt;-><init>(I)V

    #@6
    .line 201
    .local v0, icon:Llibcore/util/MutableInt;
    const/4 v2, 0x3

    #@7
    new-array v2, v2, [Ljava/lang/CharSequence;

    #@9
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->getChargeInfo(Llibcore/util/MutableInt;)Ljava/lang/CharSequence;

    #@c
    move-result-object v3

    #@d
    aput-object v3, v2, v5

    #@f
    const/4 v3, 0x1

    #@10
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->getOwnerInfo()Ljava/lang/String;

    #@13
    move-result-object v4

    #@14
    aput-object v4, v2, v3

    #@16
    const/4 v3, 0x2

    #@17
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->getCurrentMessage()Ljava/lang/CharSequence;

    #@1a
    move-result-object v4

    #@1b
    aput-object v4, v2, v3

    #@1d
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@20
    move-result-object v1

    #@21
    .line 202
    .local v1, status:Ljava/lang/CharSequence;
    iget v2, v0, Llibcore/util/MutableInt;->value:I

    #@23
    invoke-virtual {p0, v2, v5, v5, v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    #@26
    .line 203
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardMessageArea;->setText(Ljava/lang/CharSequence;)V

    #@29
    .line 204
    return-void
.end method
