.class public Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;
.super Landroid/widget/LinearLayout;
.source "SimUnlockScreen.java"

# interfaces
.implements Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreen;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;,
        Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$CheckSimPin;
    }
.end annotation


# static fields
.field private static final DIGITS:[C = null

.field private static final DIGIT_PRESS_WAKE_MILLIS:I = 0x1388


# instance fields
.field private mBackSpaceButton:Landroid/view/View;

.field private final mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

.field private mCreationOrientation:I

.field private mEnteredDigits:I

.field private final mEnteredPin:[I

.field private mHeaderText:Landroid/widget/TextView;

.field private mKeyboardHidden:I

.field private mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mOkButton:Landroid/widget/TextView;

.field private mPinText:Landroid/widget/TextView;

.field private mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

.field private final mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;


# direct methods
.method static constructor <clinit>()V
    .registers 1

    #@0
    .prologue
    .line 69
    const/16 v0, 0xa

    #@2
    new-array v0, v0, [C

    #@4
    fill-array-data v0, :array_a

    #@7
    sput-object v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->DIGITS:[C

    #@9
    return-void

    #@a
    :array_a
    .array-data 0x2
        0x30t 0x0t
        0x31t 0x0t
        0x32t 0x0t
        0x33t 0x0t
        0x34t 0x0t
        0x35t 0x0t
        0x36t 0x0t
        0x37t 0x0t
        0x38t 0x0t
        0x39t 0x0t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Configuration;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;Lcom/android/internal/widget/LockPatternUtils;)V
    .registers 14
    .parameter "context"
    .parameter "configuration"
    .parameter "updateMonitor"
    .parameter "callback"
    .parameter "lockpatternutils"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v7, 0x1

    #@3
    .line 74
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    #@6
    .line 56
    const/16 v0, 0x8

    #@8
    new-array v0, v0, [I

    #@a
    fill-array-data v0, :array_8c

    #@d
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredPin:[I

    #@f
    .line 57
    iput v5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@11
    .line 59
    iput-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@13
    .line 75
    iput-object p3, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@15
    .line 76
    iput-object p4, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@17
    .line 78
    iget v0, p2, Landroid/content/res/Configuration;->orientation:I

    #@19
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mCreationOrientation:I

    #@1b
    .line 79
    iget v0, p2, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@1d
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mKeyboardHidden:I

    #@1f
    .line 80
    iput-object p5, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    #@21
    .line 82
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    #@24
    move-result-object v6

    #@25
    .line 83
    .local v6, inflater:Landroid/view/LayoutInflater;
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mKeyboardHidden:I

    #@27
    if-ne v0, v7, :cond_7f

    #@29
    .line 84
    const v0, 0x1090066

    #@2c
    invoke-virtual {v6, v0, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@2f
    .line 90
    :goto_2f
    const v0, 0x10202fb

    #@32
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@35
    move-result-object v0

    #@36
    check-cast v0, Landroid/widget/TextView;

    #@38
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mHeaderText:Landroid/widget/TextView;

    #@3a
    .line 91
    const v0, 0x10202fd

    #@3d
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@40
    move-result-object v0

    #@41
    check-cast v0, Landroid/widget/TextView;

    #@43
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mPinText:Landroid/widget/TextView;

    #@45
    .line 92
    const v0, 0x10202fe

    #@48
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@4b
    move-result-object v0

    #@4c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mBackSpaceButton:Landroid/view/View;

    #@4e
    .line 93
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mBackSpaceButton:Landroid/view/View;

    #@50
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@53
    .line 95
    const v0, 0x10202b3

    #@56
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->findViewById(I)Landroid/view/View;

    #@59
    move-result-object v0

    #@5a
    check-cast v0, Landroid/widget/TextView;

    #@5c
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mOkButton:Landroid/widget/TextView;

    #@5e
    .line 97
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mHeaderText:Landroid/widget/TextView;

    #@60
    const v1, 0x10402fc

    #@63
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    #@66
    .line 98
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mPinText:Landroid/widget/TextView;

    #@68
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setFocusable(Z)V

    #@6b
    .line 100
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mOkButton:Landroid/widget/TextView;

    #@6d
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    #@70
    .line 102
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@72
    move-object v1, p0

    #@73
    move-object v2, p3

    #@74
    move-object v3, p5

    #@75
    move-object v4, p4

    #@76
    invoke-direct/range {v0 .. v5}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;-><init>(Landroid/view/View;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;Lcom/android/internal/widget/LockPatternUtils;Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;Z)V

    #@79
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@7b
    .line 105
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->setFocusableInTouchMode(Z)V

    #@7e
    .line 106
    return-void

    #@7f
    .line 86
    :cond_7f
    const v0, 0x1090067

    #@82
    invoke-virtual {v6, v0, p0, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    #@85
    .line 87
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;

    #@87
    invoke-direct {v0, p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$TouchInput;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;)V

    #@8a
    goto :goto_2f

    #@8b
    .line 56
    nop

    #@8c
    :array_8c
    .array-data 0x4
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
        0x0t 0x0t 0x0t 0x0t
    .end array-data
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Landroid/app/ProgressDialog;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@2
    return-object v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@2
    return-object v0
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@2
    return-object v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mHeaderText:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;)Landroid/widget/TextView;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 42
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mPinText:Landroid/widget/TextView;

    #@2
    return-object v0
.end method

.method static synthetic access$602(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    iput p1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@2
    return p1
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;I)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->reportDigit(I)V

    #@3
    return-void
.end method

.method private checkPin()V
    .registers 3

    #@0
    .prologue
    .line 206
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@2
    const/4 v1, 0x4

    #@3
    if-ge v0, v1, :cond_1d

    #@5
    .line 208
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mHeaderText:Landroid/widget/TextView;

    #@7
    const v1, 0x104009e

    #@a
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    #@d
    .line 209
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mPinText:Landroid/widget/TextView;

    #@f
    const-string v1, ""

    #@11
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@14
    .line 210
    const/4 v0, 0x0

    #@15
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@17
    .line 211
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@19
    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@1c
    .line 238
    :goto_1c
    return-void

    #@1d
    .line 214
    :cond_1d
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->getSimUnlockProgressDialog()Landroid/app/Dialog;

    #@20
    move-result-object v0

    #@21
    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    #@24
    .line 216
    new-instance v0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;

    #@26
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mPinText:Landroid/widget/TextView;

    #@28
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    #@2b
    move-result-object v1

    #@2c
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@2f
    move-result-object v1

    #@30
    invoke-direct {v0, p0, v1}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;-><init>(Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;Ljava/lang/String;)V

    #@33
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen$1;->start()V

    #@36
    goto :goto_1c
.end method

.method private getSimUnlockProgressDialog()Landroid/app/Dialog;
    .registers 4

    #@0
    .prologue
    .line 191
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@2
    if-nez v0, :cond_32

    #@4
    .line 192
    new-instance v0, Landroid/app/ProgressDialog;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mContext:Landroid/content/Context;

    #@8
    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@d
    .line 193
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@f
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mContext:Landroid/content/Context;

    #@11
    const v2, 0x104032d

    #@14
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    #@1b
    .line 195
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@1d
    const/4 v1, 0x1

    #@1e
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    #@21
    .line 196
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@23
    const/4 v1, 0x0

    #@24
    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    #@27
    .line 197
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@29
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getWindow()Landroid/view/Window;

    #@2c
    move-result-object v0

    #@2d
    const/16 v1, 0x7d9

    #@2f
    invoke-virtual {v0, v1}, Landroid/view/Window;->setType(I)V

    #@32
    .line 200
    :cond_32
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@34
    return-object v0
.end method

.method private reportDigit(I)V
    .registers 5
    .parameter "digit"

    #@0
    .prologue
    .line 269
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@2
    if-nez v0, :cond_b

    #@4
    .line 270
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mPinText:Landroid/widget/TextView;

    #@6
    const-string v1, ""

    #@8
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@b
    .line 272
    :cond_b
    iget v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@d
    const/16 v1, 0x8

    #@f
    if-ne v0, v1, :cond_12

    #@11
    .line 277
    :goto_11
    return-void

    #@12
    .line 275
    :cond_12
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mPinText:Landroid/widget/TextView;

    #@14
    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->append(Ljava/lang/CharSequence;)V

    #@1b
    .line 276
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredPin:[I

    #@1d
    iget v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@1f
    add-int/lit8 v2, v1, 0x1

    #@21
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@23
    aput p1, v0, v1

    #@25
    goto :goto_11
.end method


# virtual methods
.method public cleanUp()V
    .registers 2

    #@0
    .prologue
    .line 134
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@2
    if-eqz v0, :cond_c

    #@4
    .line 135
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@6
    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    #@9
    .line 136
    const/4 v0, 0x0

    #@a
    iput-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mSimUnlockProgressDialog:Landroid/app/ProgressDialog;

    #@c
    .line 138
    :cond_c
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mUpdateMonitor:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;

    #@e
    invoke-virtual {v0, p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardUpdateMonitor;->removeCallback(Ljava/lang/Object;)V

    #@11
    .line 139
    return-void
.end method

.method public needsInput()Z
    .registers 2

    #@0
    .prologue
    .line 110
    const/4 v0, 0x1

    #@1
    return v0
.end method

.method protected onAttachedToWindow()V
    .registers 1

    #@0
    .prologue
    .line 290
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    #@3
    .line 291
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->updateConfiguration()V

    #@6
    .line 292
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .registers 5
    .parameter "v"

    #@0
    .prologue
    .line 177
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mBackSpaceButton:Landroid/view/View;

    #@2
    if-ne p1, v2, :cond_21

    #@4
    .line 178
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mPinText:Landroid/widget/TextView;

    #@6
    invoke-virtual {v2}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    #@9
    move-result-object v0

    #@a
    .line 179
    .local v0, digits:Landroid/text/Editable;
    invoke-interface {v0}, Landroid/text/Editable;->length()I

    #@d
    move-result v1

    #@e
    .line 180
    .local v1, len:I
    if-lez v1, :cond_1b

    #@10
    .line 181
    add-int/lit8 v2, v1, -0x1

    #@12
    invoke-interface {v0, v2, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    #@15
    .line 182
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@17
    add-int/lit8 v2, v2, -0x1

    #@19
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@1b
    .line 184
    :cond_1b
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@1d
    invoke-interface {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->pokeWakelock()V

    #@20
    .line 188
    .end local v0           #digits:Landroid/text/Editable;
    .end local v1           #len:I
    :cond_20
    :goto_20
    return-void

    #@21
    .line 185
    :cond_21
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mOkButton:Landroid/widget/TextView;

    #@23
    if-ne p1, v2, :cond_20

    #@25
    .line 186
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->checkPin()V

    #@28
    goto :goto_20
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .registers 2
    .parameter "newConfig"

    #@0
    .prologue
    .line 297
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    #@3
    .line 298
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->updateConfiguration()V

    #@6
    .line 299
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .registers 6
    .parameter "keyCode"
    .parameter "event"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 242
    const/4 v2, 0x4

    #@2
    if-ne p1, v2, :cond_a

    #@4
    .line 243
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@6
    invoke-interface {v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->goToLockScreen()V

    #@9
    .line 265
    :cond_9
    :goto_9
    return v1

    #@a
    .line 247
    :cond_a
    sget-object v2, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->DIGITS:[C

    #@c
    invoke-virtual {p2, v2}, Landroid/view/KeyEvent;->getMatch([C)C

    #@f
    move-result v0

    #@10
    .line 248
    .local v0, match:C
    if-eqz v0, :cond_18

    #@12
    .line 249
    add-int/lit8 v2, v0, -0x30

    #@14
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->reportDigit(I)V

    #@17
    goto :goto_9

    #@18
    .line 252
    :cond_18
    const/16 v2, 0x43

    #@1a
    if-ne p1, v2, :cond_2c

    #@1c
    .line 253
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@1e
    if-lez v2, :cond_9

    #@20
    .line 254
    iget-object v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mPinText:Landroid/widget/TextView;

    #@22
    invoke-virtual {v2, p1, p2}, Landroid/widget/TextView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    #@25
    .line 255
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@27
    add-int/lit8 v2, v2, -0x1

    #@29
    iput v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@2b
    goto :goto_9

    #@2c
    .line 260
    :cond_2c
    const/16 v2, 0x42

    #@2e
    if-ne p1, v2, :cond_34

    #@30
    .line 261
    invoke-direct {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->checkPin()V

    #@33
    goto :goto_9

    #@34
    .line 265
    :cond_34
    const/4 v1, 0x0

    #@35
    goto :goto_9
.end method

.method public onPause()V
    .registers 2

    #@0
    .prologue
    .line 115
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->onPause()V

    #@5
    .line 116
    return-void
.end method

.method public onResume()V
    .registers 3

    #@0
    .prologue
    .line 121
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mHeaderText:Landroid/widget/TextView;

    #@2
    const v1, 0x10402fc

    #@5
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    #@8
    .line 125
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mPinText:Landroid/widget/TextView;

    #@a
    const-string v1, ""

    #@c
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    #@f
    .line 126
    const/4 v0, 0x0

    #@10
    iput v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mEnteredDigits:I

    #@12
    .line 128
    iget-object v0, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mKeyguardStatusViewManager:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;

    #@14
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardStatusViewManager;->onResume()V

    #@17
    .line 129
    return-void
.end method

.method updateConfiguration()V
    .registers 4

    #@0
    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->getResources()Landroid/content/res/Resources;

    #@3
    move-result-object v1

    #@4
    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    #@7
    move-result-object v0

    #@8
    .line 281
    .local v0, newConfig:Landroid/content/res/Configuration;
    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    #@a
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mCreationOrientation:I

    #@c
    if-eq v1, v2, :cond_14

    #@e
    .line 282
    iget-object v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mCallback:Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;

    #@10
    invoke-interface {v1, v0}, Lcom/android/internal/policy/impl/keyguard_obsolete/KeyguardScreenCallback;->recreateMe(Landroid/content/res/Configuration;)V

    #@13
    .line 286
    :cond_13
    :goto_13
    return-void

    #@14
    .line 283
    :cond_14
    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@16
    iget v2, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mKeyboardHidden:I

    #@18
    if-eq v1, v2, :cond_13

    #@1a
    .line 284
    iget v1, v0, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@1c
    iput v1, p0, Lcom/android/internal/policy/impl/keyguard_obsolete/SimUnlockScreen;->mKeyboardHidden:I

    #@1e
    goto :goto_13
.end method
