.class Lcom/android/internal/policy/impl/PhoneWindowManager$41;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/policy/impl/PhoneWindowManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;


# direct methods
.method constructor <init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 2
    .parameter

    #@0
    .prologue
    .line 8174
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$41;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@5
    return-void
.end method


# virtual methods
.method public run()V
    .registers 15

    #@0
    .prologue
    .line 8177
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$41;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2
    const/4 v3, 0x0

    #@3
    iput-boolean v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenuLongPressing:Z

    #@5
    .line 8179
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$41;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@7
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenulongToSearch:Z

    #@9
    if-eqz v0, :cond_23

    #@b
    .line 8180
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$41;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@d
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRecentAppsDialog:Lcom/android/internal/policy/impl/RecentApplicationsDialog;

    #@f
    if-eqz v0, :cond_24

    #@11
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$41;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@13
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRecentAppsDialog:Lcom/android/internal/policy/impl/RecentApplicationsDialog;

    #@15
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/RecentApplicationsDialog;->isShowing()Z

    #@18
    move-result v0

    #@19
    const/4 v3, 0x1

    #@1a
    if-ne v0, v3, :cond_24

    #@1c
    .line 8181
    const-string v0, "WindowManager"

    #@1e
    const-string v3, "Skip menu long key -> search key injection in the recent apps dialog."

    #@20
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@23
    .line 8203
    :cond_23
    :goto_23
    return-void

    #@24
    .line 8184
    :cond_24
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$41;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@26
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGlobalActions:Lcom/android/internal/policy/impl/GlobalActions;

    #@28
    if-eqz v0, :cond_3d

    #@2a
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$41;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@2c
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGlobalActions:Lcom/android/internal/policy/impl/GlobalActions;

    #@2e
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/GlobalActions;->isShowing()Z

    #@31
    move-result v0

    #@32
    const/4 v3, 0x1

    #@33
    if-ne v0, v3, :cond_3d

    #@35
    .line 8185
    const-string v0, "WindowManager"

    #@37
    const-string v3, "Skip menu long key -> search key injection in the global actions dialog."

    #@39
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    goto :goto_23

    #@3d
    .line 8188
    :cond_3d
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$41;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@3f
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isKeyguardLocked()Z

    #@42
    move-result v0

    #@43
    if-eqz v0, :cond_4d

    #@45
    .line 8189
    const-string v0, "WindowManager"

    #@47
    const-string v3, "Skip menu long key when keyguard is locked"

    #@49
    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    goto :goto_23

    #@4d
    .line 8193
    :cond_4d
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager$41;->this$0:Lcom/android/internal/policy/impl/PhoneWindowManager;

    #@4f
    const/4 v3, 0x0

    #@50
    const/4 v4, 0x0

    #@51
    const/4 v5, 0x0

    #@52
    invoke-virtual {v0, v3, v4, v5}, Lcom/android/internal/policy/impl/PhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    #@55
    .line 8195
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@58
    move-result-wide v1

    #@59
    .line 8196
    .local v1, now:J
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@5c
    move-result-object v13

    #@5d
    new-instance v0, Landroid/view/KeyEvent;

    #@5f
    const/4 v5, 0x0

    #@60
    const/16 v6, 0x54

    #@62
    const/4 v7, 0x0

    #@63
    const/4 v8, 0x0

    #@64
    const/4 v9, -0x1

    #@65
    const/4 v10, 0x0

    #@66
    const/4 v11, 0x0

    #@67
    const/16 v12, 0x101

    #@69
    move-wide v3, v1

    #@6a
    invoke-direct/range {v0 .. v12}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@6d
    const/4 v3, 0x0

    #@6e
    invoke-virtual {v13, v0, v3}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@71
    .line 8199
    invoke-static {}, Landroid/hardware/input/InputManager;->getInstance()Landroid/hardware/input/InputManager;

    #@74
    move-result-object v13

    #@75
    new-instance v0, Landroid/view/KeyEvent;

    #@77
    const/4 v5, 0x1

    #@78
    const/16 v6, 0x54

    #@7a
    const/4 v7, 0x0

    #@7b
    const/4 v8, 0x0

    #@7c
    const/4 v9, -0x1

    #@7d
    const/4 v10, 0x0

    #@7e
    const/4 v11, 0x0

    #@7f
    const/16 v12, 0x101

    #@81
    move-wide v3, v1

    #@82
    invoke-direct/range {v0 .. v12}, Landroid/view/KeyEvent;-><init>(JJIIIIIIII)V

    #@85
    const/4 v3, 0x0

    #@86
    invoke-virtual {v13, v0, v3}, Landroid/hardware/input/InputManager;->injectInputEvent(Landroid/view/InputEvent;I)Z

    #@89
    goto :goto_23
.end method
