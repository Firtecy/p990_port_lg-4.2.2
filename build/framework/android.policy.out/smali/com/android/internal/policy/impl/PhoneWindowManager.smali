.class public Lcom/android/internal/policy/impl/PhoneWindowManager;
.super Ljava/lang/Object;
.source "PhoneWindowManager.java"

# interfaces
.implements Landroid/view/WindowManagerPolicy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;,
        Lcom/android/internal/policy/impl/PhoneWindowManager$HideNavInputEventReceiver;,
        Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;,
        Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;,
        Lcom/android/internal/policy/impl/PhoneWindowManager$SettingsObserver;,
        Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;,
        Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;,
        Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;
    }
.end annotation


# static fields
.field private static final ACTION_EXCESS_SPC_FAIL_EVENT:Ljava/lang/String; = "com.lge.intent.action.EXCESS_SPC_FAIL_EVENT"

.field public static final ACTION_QUICK_BUTTON_SETTING:Ljava/lang/String; = "com.lge.phone.action.QUICK_BUTTON_SETTING"

.field private static final ACTION_SLIDE_ASIDE_HOME_PRESSED:Ljava/lang/String; = "com.lge.slideaside.HOME_PRESSED"

.field private static final ACTION_TOGGLE_QUICKCLIP:Ljava/lang/String; = "com.lge.QuickClip.action.TOGGLE_QUICKCLIP"

.field static final APPLICATION_MEDIA_OVERLAY_SUBLAYER:I = -0x1

.field static final APPLICATION_MEDIA_SUBLAYER:I = -0x2

.field static final APPLICATION_PANEL_SUBLAYER:I = 0x1

.field static final APPLICATION_SUB_PANEL_SUBLAYER:I = 0x2

.field private static final BIT_HEADSET:I = 0x1

.field private static final BIT_HEADSET_NO_MIC:I = 0x2

.field static final DEBUG:Z = false

.field static final DEBUG_EASY_ACCESS:Z = true

.field static final DEBUG_INPUT:Z = true

.field static final DEBUG_LAYOUT:Z = false

.field static final DEBUG_STARTING_WINDOW:Z = false

.field private static final DEVICE_NAME:Ljava/lang/String; = null

.field private static final DISMISS_KEYGUARD_CONTINUE:I = 0x2

.field private static final DISMISS_KEYGUARD_NONE:I = 0x0

.field private static final DISMISS_KEYGUARD_START:I = 0x1

.field private static final EASY_ACCESS_DEBOUNCE_DELAY_MILLIS:J = 0x384L

.field private static final EASY_ACCESS_INITIATE_INTERVAL_MILLIS:J = 0x12cL

.field static final ENABLE_CAR_DOCK_HOME_CAPTURE:Z = true

.field static final ENABLE_DESK_DOCK_HOME_CAPTURE:Z = false

.field public static final EXTRA_QUICK_BUTTON_SETTING_VALUE:Ljava/lang/String; = "com.lge.phone.extra.QUICK_BUTTON_SETTING_VALUE"

.field private static final INTENT_TOUCH_PALM_SWIPE_DOWN:Ljava/lang/String; = "com.lge.android.intent.action.TOUCH_PALM_SWIPE_DOWN"

.field private static final INTENT_TOUCH_PALM_SWIPE_UP:Ljava/lang/String; = "com.lge.android.intent.action.TOUCH_PALM_SWIPE_UP"

.field private static final KEYGUARD_SCREENSHOT_CHORD_DELAY_MULTIPLIER:F = 2.5f

.field static final LONG_PRESS_HOME_NOTHING:I = 0x0

.field static final LONG_PRESS_HOME_RECENT_DIALOG:I = 0x1

.field static final LONG_PRESS_HOME_RECENT_SYSTEM_UI:I = 0x2

.field static final LONG_PRESS_POWER_GLOBAL_ACTIONS:I = 0x1

.field static final LONG_PRESS_POWER_NOTHING:I = 0x0

.field static final LONG_PRESS_POWER_SHUT_OFF:I = 0x2

.field static final LONG_PRESS_POWER_SHUT_OFF_NO_CONFIRM:I = 0x3

.field private static final MSG_DISABLE_HIDE_DISPLAY:I = 0x66

.field private static final MSG_DISABLE_POINTER_LOCATION:I = 0x2

.field private static final MSG_DISABLE_SLIDE_ASIDE:I = 0x6a

.field private static final MSG_DISABLE_TAKE_SCREENSHOT:I = 0x68

.field private static final MSG_DISPATCH_MEDIA_KEY_REPEAT_WITH_WAKE_LOCK:I = 0x4

.field private static final MSG_DISPATCH_MEDIA_KEY_WITH_WAKE_LOCK:I = 0x3

.field private static final MSG_ENABLE_HIDE_DISPLAY:I = 0x65

.field private static final MSG_ENABLE_POINTER_LOCATION:I = 0x1

.field private static final MSG_ENABLE_SLIDE_ASIDE:I = 0x69

.field private static final MSG_ENABLE_TAKE_SCREENSHOT:I = 0x67

.field private static final MVP_REPOST_TIME:I = 0x64

.field private static final MVP_SYS_CLOSING_TIMEOUT:I = 0x258

#the value of this static final field might be set in the static constructor
.field static final NOTUSER_DEBUG:Z = false

.field private static final POLICE_112APP_CHORD_DEBOUNCE_DELAY_MILLIS:I = 0x3e8

.field private static final POLICE_112APP_RUN_DELAY_MILLIS:I = 0xbb8

.field static final PRINT_ANIM:Z = false

.field private static final QUICKCLIP_CHORD_DEBOUNCE_DELAY_MILLIS:J = 0x96L

.field static final RECENT_APPS_BEHAVIOR_DISMISS:I = 0x2

.field static final RECENT_APPS_BEHAVIOR_DISMISS_AND_SWITCH:I = 0x3

.field static final RECENT_APPS_BEHAVIOR_EXIT_TOUCH_MODE_AND_SHOW:I = 0x1

.field static final RECENT_APPS_BEHAVIOR_SHOW_OR_DISMISS:I = 0x0

.field private static final SCREENSHOT_CHORD_DEBOUNCE_DELAY_MILLIS:J = 0x96L

.field static final SHOW_PROCESSES_ON_ALT_MENU:Z = false

.field static final SHOW_STARTING_ANIMATIONS:Z = true

.field static SPC_ERR_FREEZE:Z = false

.field private static final SUPPORTED_HEADSETS:I = 0x3

.field private static final SW_HEADPHONE_INSERT:I = 0x2

.field private static final SW_HEADPHONE_INSERT_BIT:I = 0x4

.field private static final SW_HEADSET_INSERT:I = 0x6

.field private static final SW_JACK_BITS:I = 0x94

.field private static final SW_JACK_PHYSICAL_INSERT:I = 0x7

.field private static final SW_JACK_PHYSICAL_INSERT_BIT:I = 0x80

.field private static final SW_MICROPHONE_INSERT:I = 0x4

.field private static final SW_MICROPHONE_INSERT_BIT:I = 0x10

.field public static final SYSTEM_DIALOG_REASON_ASSIST:Ljava/lang/String; = "assist"

.field public static final SYSTEM_DIALOG_REASON_GLOBAL_ACTIONS:Ljava/lang/String; = "globalactions"

.field public static final SYSTEM_DIALOG_REASON_HOME_KEY:Ljava/lang/String; = "homekey"

.field public static final SYSTEM_DIALOG_REASON_KEY:Ljava/lang/String; = "reason"

.field public static final SYSTEM_DIALOG_REASON_RECENT_APPS:Ljava/lang/String; = "recentapps"

.field static final SYSTEM_UI_CHANGING_LAYOUT:I = 0xe

.field static final TAG:Ljava/lang/String; = "WindowManager"

.field private static final TOUCH_PALM_COVER:Ljava/lang/String; = "com.lge.android.intent.action.TOUCH_PALM_COVER"

.field private static final TOUCH_PALM_SWIPE_DOWN:Ljava/lang/String; = "com.lge.android.intent.action.TOUCH_PALM_SWIPE_DOWN"

.field private static final TOUCH_PALM_SWIPE_UP:Ljava/lang/String; = "com.lge.android.intent.action.TOUCH_PALM_SWIPE_UP"

.field private static final VOLUME_KEY_LONG_PRESS_DELAY_MILLIS:J = 0x3e8L

.field private static final VOL_LONG_CANCEL_BY_112APP:I = 0x6

.field private static final VOL_LONG_CANCEL_BY_INCALL:I = 0x4

.field private static final VOL_LONG_CANCEL_BY_KEYUP:I = 0x1

.field private static final VOL_LONG_CANCEL_BY_MUSIC:I = 0x2

.field private static final VOL_LONG_CANCEL_BY_RINGING:I = 0x3

.field private static final VOL_LONG_CANCEL_BY_SCREENSHOT:I = 0x5

.field private static final VOL_LONG_NO_CANCEL:I

.field private static final WINDOW_TYPES_WHERE_HOME_DOESNT_WORK:[I

.field static final isOverlapPolicy:Z

.field static final localLOGV:Z

.field private static mHeadsetJackState:I

.field static final mTmpContentFrame:Landroid/graphics/Rect;

.field static final mTmpDisplayFrame:Landroid/graphics/Rect;

.field static final mTmpNavigationFrame:Landroid/graphics/Rect;

.field static final mTmpParentFrame:Landroid/graphics/Rect;

.field static final mTmpVisibleFrame:Landroid/graphics/Rect;

.field static sApplicationLaunchKeyCategories:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private CAPP_TILT:Z

.field private CAPP_TOUCH_PALM_COVER:Z

.field private CAPP_TOUCH_PALM_SWIPE:Z

.field private CAPP_TOUCH_SLIDEASIDE:Z

.field INSECURE_CAMERA_INTENT:Landroid/content/Intent;

.field SECURE_CAMERA_INTENT:Landroid/content/Intent;

.field private isReversed:Z

.field isSplitWindowLoaded:Z

.field m112AppAvailable:Z

.field private final m112AppChordLongPress:Ljava/lang/Runnable;

.field m112AppReceiver:Landroid/content/BroadcastReceiver;

.field private m112AppStartedByReservedKey:Z

.field private mAATIsRunning:Z

.field mAccelerometerDefault:Z

.field mAllowAllRotations:I

.field mAllowLockscreenWhenOn:Z

.field final mAllowSystemUiDelay:Ljava/lang/Runnable;

.field mAssistKeyLongPressed:Z

.field mAutocallReceiver:Landroid/content/BroadcastReceiver;

.field mAutocallTest:Z

.field mBlockingLcdoff:Z

.field mBootMsgDialog:Landroid/app/ProgressDialog;

.field mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mCameraCoverOpened:Z

.field mCanHideNavigationBar:Z

.field private final mCancel_112AppChordLongPress:Ljava/lang/Runnable;

.field mCarDockEnablesAccelerometer:Z

.field mCarDockIntent:Landroid/content/Intent;

.field mCarDockRotation:I

.field mConsumeSearchKeyUp:Z

.field mContentBottom:I

.field mContentLeft:I

.field mContentRight:I

.field mContentTop:I

.field mContext:Landroid/content/Context;

.field mCurBottom:I

.field private mCurHeadsetState:I

.field mCurLeft:I

.field mCurRight:I

.field mCurTop:I

.field mCurrentAppOrientation:I

.field mDeskDockEnablesAccelerometer:Z

.field mDeskDockIntent:Landroid/content/Intent;

.field mDeskDockRotation:I

.field mDismissKeyguard:I

.field mDisplay:Landroid/view/Display;

.field mDockBottom:I

.field mDockLayer:I

.field mDockLeft:I

.field mDockMode:I

.field mDockReceiver:Landroid/content/BroadcastReceiver;

.field mDockRight:I

.field mDockTop:I

.field mDreamReceiver:Landroid/content/BroadcastReceiver;

.field mDreamingLockscreen:Z

.field private mEasyAccessEnabled:Z

.field private final mEasyAccessEntry:Ljava/lang/Runnable;

.field mEnableShiftMenuBugReports:Z

.field mEndcallBehavior:I

.field private final mFallbackActions:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/view/KeyCharacterMap$FallbackAction;",
            ">;"
        }
    .end annotation
.end field

.field mFocusedApp:Landroid/view/IApplicationToken;

.field mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

.field mForceClearedSystemUiFlags:I

.field mForceStatusBar:Z

.field mForceStatusBarFromKeyguard:Z

.field mForcingShowNavBar:Z

.field mForcingShowNavBarLayer:I

.field mGlobalActionReceiver:Landroid/content/BroadcastReceiver;

.field mGlobalActions:Lcom/android/internal/policy/impl/GlobalActions;

.field mGoHomeDialog:Landroid/app/AlertDialog;

.field mGoHomeDialog2:Landroid/app/AlertDialog;

.field private mHDMIObserver:Landroid/os/UEventObserver;

.field mHWKeyControlMode:Z

.field mHandler:Landroid/os/Handler;

.field mHasNavigationBar:Z

.field mHasSoftInput:Z

.field mHasSystemNavBar:Z

.field mHaveBuiltInKeyboard:Z

.field mHavePendingMediaKeyRepeatWithWakeLock:Z

.field mHdmiPlugged:Z

.field mHdmiRotation:I

.field mHdmiRotationLock:Z

.field mHeadless:Z

.field private mHeadsetIntent:Z

.field private final mHeadsetLock:Ljava/lang/Object;

.field private mHeadsetName:Ljava/lang/String;

.field private mHideDisplayMode:I

.field mHideLockScreen:Z

.field mHideNavFakeWindow:Landroid/view/WindowManagerPolicy$FakeWindow;

.field final mHideNavInputEventReceiverFactory:Landroid/view/InputEventReceiver$Factory;

.field mHomeIntent:Landroid/content/Intent;

.field private mHomeKeyintent:Landroid/content/Intent;

.field mHomeLongPressed:Z

.field mHomePressed:Z

.field private mHomeUpKeyConsumedByEasyAccess:Z

.field private mHomeUpKeyTime:[J

.field mHotKeyCustomizing:Z

.field mIncallPowerBehavior:I

.field mInitialContentBottom:I

.field mInitialCurBottom:I

.field private final mIntentHandler:Landroid/os/Handler;

.field private mIsExpanded:Z

.field private mIsTargetTmoUs:Z

.field private mKeepHidingNavInputChannel:Landroid/view/InputChannel;

.field private mKeepHidingNavInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

.field mKeyboardTapVibePattern:[J

.field mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

.field mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

.field mLandscapeRotation:I

.field mLanguageSwitchKeyPressed:Z

.field mLastFocusNeedsMenu:Z

.field mLastInputMethodTargetWindow:Landroid/view/WindowManagerPolicy$WindowState;

.field mLastInputMethodWindow:Landroid/view/WindowManagerPolicy$WindowState;

.field mLastSystemUiFlags:I

.field final mLgeGestureLock:Ljava/lang/Object;

.field mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

.field mLgeInputEventMonitorChannel:Landroid/view/InputChannel;

.field mLidControlsSleep:Z

.field mLidKeyboardAccessibility:I

.field mLidNavigationAccessibility:I

.field mLidOpenRotation:I

.field mLidState:I

.field final mLock:Ljava/lang/Object;

.field mLockScreenTimeout:I

.field mLockScreenTimerActive:Z

.field private mLongPressOnHomeBehavior:I

.field mLongPressOnPowerBehavior:I

.field mLongPressVibePattern:[J

.field mMenuLongPress:Ljava/lang/Runnable;

.field mMenuLongPressing:Z

.field mMenulongEnabled:Z

.field mMenulongToSearch:Z

.field mMultiuserReceiver:Landroid/content/BroadcastReceiver;

.field mNaviReceiver:Landroid/content/BroadcastReceiver;

.field mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

.field mNavigationBarCanMove:Z

.field mNavigationBarHeightForRotation:[I

.field mNavigationBarOnBottom:Z

.field mNavigationBarWidthForRotation:[I

.field private mOperator:Ljava/lang/String;

.field mOrientationListener:Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;

.field mOrientationSensorEnabled:Z

.field mPalmGestureReceiver:Landroid/content/BroadcastReceiver;

.field mPendingPowerKeyUpCanceled:Z

.field mPointerLocationInputChannel:Landroid/view/InputChannel;

.field mPointerLocationInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;

.field mPointerLocationMode:I

.field mPointerLocationView:Lcom/android/internal/widget/PointerLocationView;

.field mPortraitRotation:I

.field private final mPower8SecLongPress:Ljava/lang/Runnable;

.field mPower8SecLongPressHandled:Z

.field volatile mPowerKeyHandled:Z

.field mPowerKeyPress:Ljava/lang/Runnable;

.field mPowerKeyScreenOn:Z

.field private mPowerKeyTime:J

.field private mPowerKeyTriggered:Z

.field private final mPowerLongPress:Ljava/lang/Runnable;

.field mPowerManager:Landroid/os/PowerManager;

.field private mPrevHeadsetState:I

.field private mProximityNear:Z

.field private mProximitySensor:Landroid/hardware/Sensor;

.field private mProximitySensorEnabled:Z

.field private final mProximitySensorListener:Landroid/hardware/SensorEventListener;

.field mQucikBTNAnswerSettingReceiver:Landroid/content/BroadcastReceiver;

.field private mQuickBTNAnswserMode:I

.field private final mQuickClipChordLongPress:Ljava/lang/Runnable;

.field mQuickClipHotKeyDisable:Z

.field mQuickClipLongPress:Ljava/lang/Runnable;

.field private mQuickclipPressed:Z

.field mRearSideKeyEnable:Z

.field mRecentAppsDialog:Lcom/android/internal/policy/impl/RecentApplicationsDialog;

.field mRecentAppsDialogHeldModifiers:I

.field mResettingSystemUiFlags:I

.field mRestartAction:Lcom/android/internal/policy/impl/RestartAction;

.field mRestrictedScreenHeight:I

.field mRestrictedScreenLeft:I

.field mRestrictedScreenTop:I

.field mRestrictedScreenWidth:I

.field mSafeMode:Z

.field mSafeModeDisabledVibePattern:[J

.field mSafeModeEnabledVibePattern:[J

.field mSafetyCareIntent:Landroid/content/Intent;

.field mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

.field mScreenOnEarly:Z

.field mScreenOnFully:Z

.field private mScreenshotChordEnabled:Z

.field private final mScreenshotChordLongPress:Ljava/lang/Runnable;

.field mScreenshotConnection:Landroid/content/ServiceConnection;

.field final mScreenshotLock:Ljava/lang/Object;

.field final mScreenshotTimeout:Ljava/lang/Runnable;

.field mSearchKeyShortcutPending:Z

.field mSearchManager:Landroid/app/SearchManager;

.field mSeascapeRotation:I

.field private mSensorManager:Landroid/hardware/SensorManager;

.field final mServiceAquireLock:Ljava/lang/Object;

.field mSettingsObserver:Lcom/android/internal/policy/impl/PhoneWindowManager$SettingsObserver;

.field mShortcutManager:Lcom/android/internal/policy/impl/ShortcutManager;

.field mShowingDream:Z

.field mShowingLockscreen:Z

.field private mSkipUdub:Z

.field private mSlideAsideMode:I

.field mSmartCoverReceiver:Landroid/content/BroadcastReceiver;

.field mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

.field mStableBottom:I

.field mStableFullscreenBottom:I

.field mStableFullscreenLeft:I

.field mStableFullscreenRight:I

.field mStableFullscreenTop:I

.field mStableLeft:I

.field mStableRight:I

.field mStableTop:I

.field mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

.field mStatusBarHeight:I

.field mStatusBarLayer:I

.field mStatusBarReceiver:Landroid/content/BroadcastReceiver;

.field mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

.field private mSwitchValues:I

.field mSystemBarType:I

.field mSystemBooted:Z

.field mSystemBottom:I

.field mSystemLeft:I

.field mSystemReady:Z

.field mSystemRight:I

.field mSystemTop:I

.field private mTakeScreenshotMode:I

.field mToastIsRinging:Ljava/lang/Runnable;

.field mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

.field mTopIsFullscreen:Z

.field mTouchCrackMode:I

.field private mTouchCrackModeObserver:Landroid/os/UEventObserver;

.field mTouchCrackModeReceiver:Landroid/content/BroadcastReceiver;

.field private mTranslucentImeMode:Z

.field mTransparentSystemBar:Z

.field private mUDUBLongPressedStartTime:J

.field mUiMode:I

.field mUnrestrictedScreenHeight:I

.field mUnrestrictedScreenLeft:I

.field mUnrestrictedScreenTop:I

.field mUnrestrictedScreenWidth:I

.field mUpsideDownRotation:I

.field mUserRotation:I

.field mUserRotationMode:I

.field private mViewCoverClosed:Z

.field private mViewCoverEnabled:Z

.field private mViewCoverOpened:Z

.field mVirtualKeyVibePattern:[J

.field private mVolumeDownKeyConsumedByLongPress:Z

.field private mVolumeDownKeyConsumedByQuickClipChord:Z

.field private mVolumeDownKeyConsumedByScreenshotChord:Z

.field private mVolumeDownKeyTime:J

.field private mVolumeDownKeyTriggered:Z

.field private mVolumeDownLongKeyCancelReason:I

.field private final mVolumeDownLongPress:Ljava/lang/Runnable;

.field private mVolumeKeyLongPressEnabled:Z

.field private mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mVolumeUpKeyConsumedBy112App:Z

.field private mVolumeUpKeyConsumedByLongPress:Z

.field private mVolumeUpKeyConsumedByQuickClipChord:Z

.field private mVolumeUpKeyTime:J

.field private mVolumeUpKeyTriggered:Z

.field private mVolumeUpLongKeyCancelReason:I

.field private final mVolumeUpLongPress:Ljava/lang/Runnable;

.field mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

.field private mWinDismissingKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

.field mWindowManager:Landroid/view/IWindowManager;

.field mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

.field nExptToast:Landroid/widget/Toast;

.field sShortKeyClass:Ljava/lang/String;

.field sShortKeyPkg:Ljava/lang/String;

.field private sizeOffset:I

.field spcErrReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .registers 4

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 244
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    #@3
    const-string v2, "user"

    #@5
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@8
    move-result v0

    #@9
    if-nez v0, :cond_83

    #@b
    const/4 v0, 0x1

    #@c
    :goto_c
    sput-boolean v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->NOTUSER_DEBUG:Z

    #@e
    .line 312
    sput v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetJackState:I

    #@10
    .line 323
    const-string v0, "ro.product.device"

    #@12
    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@15
    move-result-object v0

    #@16
    sput-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->DEVICE_NAME:Ljava/lang/String;

    #@18
    .line 353
    new-instance v0, Landroid/util/SparseArray;

    #@1a
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@1d
    sput-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sApplicationLaunchKeyCategories:Landroid/util/SparseArray;

    #@1f
    .line 354
    sget-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sApplicationLaunchKeyCategories:Landroid/util/SparseArray;

    #@21
    const/16 v2, 0x40

    #@23
    const-string v3, "android.intent.category.APP_BROWSER"

    #@25
    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@28
    .line 356
    sget-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sApplicationLaunchKeyCategories:Landroid/util/SparseArray;

    #@2a
    const/16 v2, 0x41

    #@2c
    const-string v3, "android.intent.category.APP_EMAIL"

    #@2e
    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@31
    .line 358
    sget-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sApplicationLaunchKeyCategories:Landroid/util/SparseArray;

    #@33
    const/16 v2, 0xcf

    #@35
    const-string v3, "android.intent.category.APP_CONTACTS"

    #@37
    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@3a
    .line 360
    sget-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sApplicationLaunchKeyCategories:Landroid/util/SparseArray;

    #@3c
    const/16 v2, 0xd0

    #@3e
    const-string v3, "android.intent.category.APP_CALENDAR"

    #@40
    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@43
    .line 362
    sget-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sApplicationLaunchKeyCategories:Landroid/util/SparseArray;

    #@45
    const/16 v2, 0xd1

    #@47
    const-string v3, "android.intent.category.APP_MUSIC"

    #@49
    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@4c
    .line 364
    sget-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sApplicationLaunchKeyCategories:Landroid/util/SparseArray;

    #@4e
    const/16 v2, 0xd2

    #@50
    const-string v3, "android.intent.category.APP_CALCULATOR"

    #@52
    invoke-virtual {v0, v2, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    #@55
    .line 707
    new-instance v0, Landroid/graphics/Rect;

    #@57
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@5a
    sput-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpParentFrame:Landroid/graphics/Rect;

    #@5c
    .line 708
    new-instance v0, Landroid/graphics/Rect;

    #@5e
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@61
    sput-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpDisplayFrame:Landroid/graphics/Rect;

    #@63
    .line 709
    new-instance v0, Landroid/graphics/Rect;

    #@65
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@68
    sput-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpContentFrame:Landroid/graphics/Rect;

    #@6a
    .line 710
    new-instance v0, Landroid/graphics/Rect;

    #@6c
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@6f
    sput-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpVisibleFrame:Landroid/graphics/Rect;

    #@71
    .line 711
    new-instance v0, Landroid/graphics/Rect;

    #@73
    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    #@76
    sput-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@78
    .line 3214
    const/4 v0, 0x2

    #@79
    new-array v0, v0, [I

    #@7b
    fill-array-data v0, :array_86

    #@7e
    sput-object v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->WINDOW_TYPES_WHERE_HOME_DOESNT_WORK:[I

    #@80
    .line 8379
    sput-boolean v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->SPC_ERR_FREEZE:Z

    #@82
    return-void

    #@83
    :cond_83
    move v0, v1

    #@84
    .line 244
    goto :goto_c

    #@85
    .line 3214
    nop

    #@86
    :array_86
    .array-data 0x4
        0xd3t 0x7t 0x0t 0x0t
        0xdat 0x7t 0x0t 0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x4

    #@1
    const/4 v5, 0x1

    #@2
    const/4 v4, -0x1

    #@3
    const/4 v3, 0x0

    #@4
    const/4 v2, 0x0

    #@5
    .line 233
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    #@8
    .line 249
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_SLIDEASIDE:Z

    #@a
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->CAPP_TOUCH_SLIDEASIDE:Z

    #@c
    .line 253
    new-instance v0, Landroid/content/Intent;

    #@e
    const-string v1, "com.lge.slideaside.HOME_PRESSED"

    #@10
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@13
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeKeyintent:Landroid/content/Intent;

    #@15
    .line 260
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_PALM_SWIPE:Z

    #@17
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->CAPP_TOUCH_PALM_SWIPE:Z

    #@19
    .line 264
    sget-boolean v0, Lcom/lge/config/ConfigBuildFlags;->CAPP_TOUCH_PALM_COVER:Z

    #@1b
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->CAPP_TOUCH_PALM_COVER:Z

    #@1d
    .line 268
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->CAPP_TILT:Z

    #@1f
    .line 325
    const-string v0, "Headset"

    #@21
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetName:Ljava/lang/String;

    #@23
    .line 326
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPrevHeadsetState:I

    #@25
    .line 327
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurHeadsetState:I

    #@27
    .line 328
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetIntent:Z

    #@29
    .line 329
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSwitchValues:I

    #@2b
    .line 330
    new-instance v0, Ljava/lang/Object;

    #@2d
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@30
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetLock:Ljava/lang/Object;

    #@32
    .line 373
    new-instance v0, Ljava/lang/Object;

    #@34
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@37
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@39
    .line 380
    new-instance v0, Ljava/lang/Object;

    #@3b
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@3e
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mServiceAquireLock:Ljava/lang/Object;

    #@40
    .line 403
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEnableShiftMenuBugReports:Z

    #@42
    .line 407
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@44
    .line 410
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@46
    .line 411
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasNavigationBar:Z

    #@48
    .line 412
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCanHideNavigationBar:Z

    #@4a
    .line 413
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarCanMove:Z

    #@4c
    .line 414
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarOnBottom:Z

    #@4e
    .line 415
    new-array v0, v6, [I

    #@50
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarHeightForRotation:[I

    #@52
    .line 416
    new-array v0, v6, [I

    #@54
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarWidthForRotation:[I

    #@56
    .line 418
    iput v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemBarType:I

    #@58
    .line 419
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTransparentSystemBar:Z

    #@5a
    .line 422
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@5c
    .line 426
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestartAction:Lcom/android/internal/policy/impl/RestartAction;

    #@5e
    .line 427
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPower8SecLongPressHandled:Z

    #@60
    .line 431
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyScreenOn:Z

    #@62
    .line 433
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@64
    .line 434
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodTargetWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@66
    .line 445
    iput v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidState:I

    #@68
    .line 454
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@6a
    .line 463
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotationMode:I

    #@6c
    .line 464
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotation:I

    #@6e
    .line 467
    iput v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAllowAllRotations:I

    #@70
    .line 473
    iput v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnPowerBehavior:I

    #@72
    .line 474
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnEarly:Z

    #@74
    .line 475
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnFully:Z

    #@76
    .line 476
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationSensorEnabled:Z

    #@78
    .line 477
    iput v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurrentAppOrientation:I

    #@7a
    .line 478
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSoftInput:Z

    #@7c
    .line 481
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenulongEnabled:Z

    #@7e
    .line 482
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenulongToSearch:Z

    #@80
    .line 483
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->nExptToast:Landroid/widget/Toast;

    #@82
    .line 486
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sShortKeyPkg:Ljava/lang/String;

    #@84
    .line 487
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sShortKeyClass:Ljava/lang/String;

    #@86
    .line 488
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHotKeyCustomizing:Z

    #@88
    .line 489
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@8a
    .line 491
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppAvailable:Z

    #@8c
    .line 492
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationMode:I

    #@8e
    .line 494
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRearSideKeyEnable:Z

    #@90
    .line 495
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBlockingLcdoff:Z

    #@92
    .line 497
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAutocallTest:Z

    #@94
    .line 500
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@96
    .line 503
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTouchCrackMode:I

    #@98
    .line 505
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideDisplayMode:I

    #@9a
    .line 506
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTakeScreenshotMode:I

    #@9c
    .line 507
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSlideAsideMode:I

    #@9e
    .line 509
    new-instance v0, Ljava/lang/Object;

    #@a0
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@a3
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeGestureLock:Ljava/lang/Object;

    #@a5
    .line 518
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverOpened:Z

    #@a7
    .line 519
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverClosed:Z

    #@a9
    .line 520
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverEnabled:Z

    #@ab
    .line 521
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCameraCoverOpened:Z

    #@ad
    .line 527
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOperator:Ljava/lang/String;

    #@af
    .line 529
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTranslucentImeMode:Z

    #@b1
    .line 532
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->isSplitWindowLoaded:Z

    #@b3
    .line 535
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIsExpanded:Z

    #@b5
    .line 540
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickBTNAnswserMode:I

    #@b7
    .line 545
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorEnabled:Z

    #@b9
    .line 546
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximityNear:Z

    #@bb
    .line 549
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAATIsRunning:Z

    #@bd
    .line 698
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mResettingSystemUiFlags:I

    #@bf
    .line 700
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceClearedSystemUiFlags:I

    #@c1
    .line 703
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastFocusNeedsMenu:Z

    #@c3
    .line 705
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideNavFakeWindow:Landroid/view/WindowManagerPolicy$FakeWindow;

    #@c5
    .line 725
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDismissKeyguard:I

    #@c7
    .line 757
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@c9
    .line 758
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSeascapeRotation:I

    #@cb
    .line 759
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@cd
    .line 760
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUpsideDownRotation:I

    #@cf
    .line 763
    iput v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@d1
    .line 784
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEasyAccessEnabled:Z

    #@d3
    .line 785
    const/4 v0, 0x3

    #@d4
    new-array v0, v0, [J

    #@d6
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@d8
    .line 792
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIsTargetTmoUs:Z

    #@da
    .line 793
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSkipUdub:Z

    #@dc
    .line 802
    new-instance v0, Landroid/util/SparseArray;

    #@de
    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    #@e1
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFallbackActions:Landroid/util/SparseArray;

    #@e3
    .line 831
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownLongKeyCancelReason:I

    #@e5
    .line 832
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpLongKeyCancelReason:I

    #@e7
    .line 836
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@e9
    .line 839
    new-instance v0, Landroid/content/Intent;

    #@eb
    const-string v1, "android.media.action.STILL_IMAGE_CAMERA_SECURE"

    #@ed
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@f0
    const/high16 v1, 0x80

    #@f2
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@f5
    move-result-object v0

    #@f6
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->SECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@f8
    .line 842
    new-instance v0, Landroid/content/Intent;

    #@fa
    const-string v1, "android.media.action.STILL_IMAGE_CAMERA"

    #@fc
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@ff
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->INSECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@101
    .line 922
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$1;

    #@103
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$1;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@106
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHDMIObserver:Landroid/os/UEventObserver;

    #@108
    .line 1307
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$2;

    #@10a
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$2;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@10d
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerLongPress:Ljava/lang/Runnable;

    #@10f
    .line 1360
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$3;

    #@111
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$3;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@114
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotChordLongPress:Ljava/lang/Runnable;

    #@116
    .line 1367
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$4;

    #@118
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$4;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@11b
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEasyAccessEntry:Ljava/lang/Runnable;

    #@11d
    .line 1373
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$5;

    #@11f
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$5;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@122
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPower8SecLongPress:Ljava/lang/Runnable;

    #@124
    .line 1386
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$6;

    #@126
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$6;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@129
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpLongPress:Ljava/lang/Runnable;

    #@12b
    .line 1408
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$7;

    #@12d
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$7;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@130
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownLongPress:Ljava/lang/Runnable;

    #@132
    .line 1452
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$8;

    #@134
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$8;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@137
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipChordLongPress:Ljava/lang/Runnable;

    #@139
    .line 1493
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickclipPressed:Z

    #@13b
    .line 1518
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$9;

    #@13d
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$9;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@140
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipLongPress:Ljava/lang/Runnable;

    #@142
    .line 1539
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$10;

    #@144
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$10;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@147
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mToastIsRinging:Ljava/lang/Runnable;

    #@149
    .line 1631
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedBy112App:Z

    #@14b
    .line 1632
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppStartedByReservedKey:Z

    #@14d
    .line 1665
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$11;

    #@14f
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$11;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@152
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppChordLongPress:Ljava/lang/Runnable;

    #@154
    .line 1687
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$12;

    #@156
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$12;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@159
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCancel_112AppChordLongPress:Ljava/lang/Runnable;

    #@15b
    .line 1729
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$13;

    #@15d
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$13;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@160
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppReceiver:Landroid/content/BroadcastReceiver;

    #@162
    .line 1848
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$14;

    #@164
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$14;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@167
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGlobalActionReceiver:Landroid/content/BroadcastReceiver;

    #@169
    .line 4239
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$17;

    #@16b
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$17;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@16e
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAllowSystemUiDelay:Ljava/lang/Runnable;

    #@170
    .line 4352
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputChannel:Landroid/view/InputChannel;

    #@172
    .line 4353
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@174
    .line 4456
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$19;

    #@176
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$19;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@179
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideNavInputEventReceiverFactory:Landroid/view/InputEventReceiver$Factory;

    #@17b
    .line 5063
    iput v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sizeOffset:I

    #@17d
    .line 5064
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->isReversed:Z

    #@17f
    .line 5930
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$23;

    #@181
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$23;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@184
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIntentHandler:Landroid/os/Handler;

    #@186
    .line 6001
    new-instance v0, Ljava/lang/Object;

    #@188
    invoke-direct/range {v0 .. v0}, Ljava/lang/Object;-><init>()V

    #@18b
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotLock:Ljava/lang/Object;

    #@18d
    .line 6002
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotConnection:Landroid/content/ServiceConnection;

    #@18f
    .line 6004
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$24;

    #@191
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$24;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@194
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotTimeout:Ljava/lang/Runnable;

    #@196
    .line 6909
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$26;

    #@198
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$26;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@19b
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockReceiver:Landroid/content/BroadcastReceiver;

    #@19d
    .line 6920
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$27;

    #@19f
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$27;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1a2
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDreamReceiver:Landroid/content/BroadcastReceiver;

    #@1a4
    .line 6935
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$28;

    #@1a6
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$28;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1a9
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMultiuserReceiver:Landroid/content/BroadcastReceiver;

    #@1ab
    .line 6957
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$29;

    #@1ad
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$29;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1b0
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPalmGestureReceiver:Landroid/content/BroadcastReceiver;

    #@1b2
    .line 6989
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$30;

    #@1b4
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$30;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1b7
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAutocallReceiver:Landroid/content/BroadcastReceiver;

    #@1b9
    .line 7001
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$31;

    #@1bb
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$31;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1be
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTouchCrackModeReceiver:Landroid/content/BroadcastReceiver;

    #@1c0
    .line 7016
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$32;

    #@1c2
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$32;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1c5
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTouchCrackModeObserver:Landroid/os/UEventObserver;

    #@1c7
    .line 7034
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$33;

    #@1c9
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$33;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1cc
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQucikBTNAnswerSettingReceiver:Landroid/content/BroadcastReceiver;

    #@1ce
    .line 7501
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBootMsgDialog:Landroid/app/ProgressDialog;

    #@1d0
    .line 7606
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@1d2
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1d5
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@1d7
    .line 8174
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$41;

    #@1d9
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$41;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1dc
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenuLongPress:Ljava/lang/Runnable;

    #@1de
    .line 8381
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$45;

    #@1e0
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$45;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1e3
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->spcErrReceiver:Landroid/content/BroadcastReceiver;

    #@1e5
    .line 8453
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$46;

    #@1e7
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$46;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1ea
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarReceiver:Landroid/content/BroadcastReceiver;

    #@1ec
    .line 8466
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$47;

    #@1ee
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$47;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1f1
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSmartCoverReceiver:Landroid/content/BroadcastReceiver;

    #@1f3
    .line 8511
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$48;

    #@1f5
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$48;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1f8
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorListener:Landroid/hardware/SensorEventListener;

    #@1fa
    .line 8533
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$49;

    #@1fc
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$49;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@1ff
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyPress:Ljava/lang/Runnable;

    #@201
    .line 8544
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$50;

    #@203
    invoke-direct {v0, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$50;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@206
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNaviReceiver:Landroid/content/BroadcastReceiver;

    #@208
    return-void
.end method

.method static synthetic access$000(Lcom/android/internal/policy/impl/PhoneWindowManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSlideAsideMode:I

    #@2
    return v0
.end method

.method static synthetic access$002(Lcom/android/internal/policy/impl/PhoneWindowManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSlideAsideMode:I

    #@2
    return p1
.end method

.method static synthetic access$100(Lcom/android/internal/policy/impl/PhoneWindowManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTakeScreenshotMode:I

    #@2
    return v0
.end method

.method static synthetic access$1000(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$102(Lcom/android/internal/policy/impl/PhoneWindowManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTakeScreenshotMode:I

    #@2
    return p1
.end method

.method static synthetic access$1100(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverClosed:Z

    #@2
    return v0
.end method

.method static synthetic access$1102(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverClosed:Z

    #@2
    return p1
.end method

.method static synthetic access$1200(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->takeScreenshot()V

    #@3
    return-void
.end method

.method static synthetic access$1300(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->launchEasyAccess()V

    #@3
    return-void
.end method

.method static synthetic access$1400(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setProximitySensorEnabled(Z)V

    #@3
    return-void
.end method

.method static synthetic access$1500(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximityNear:Z

    #@2
    return v0
.end method

.method static synthetic access$1502(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximityNear:Z

    #@2
    return p1
.end method

.method static synthetic access$1600(Lcom/android/internal/policy/impl/PhoneWindowManager;)Landroid/os/PowerManager$WakeLock;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2
    return-object v0
.end method

.method static synthetic access$1700(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->launchVolumeUpLongPressAction()V

    #@3
    return-void
.end method

.method static synthetic access$1800(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->launchVolumeDownLongPressAction()V

    #@3
    return-void
.end method

.method static synthetic access$1900(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isMvpOnTop()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$200(Lcom/android/internal/policy/impl/PhoneWindowManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideDisplayMode:I

    #@2
    return v0
.end method

.method static synthetic access$2002(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickclipPressed:Z

    #@2
    return p1
.end method

.method static synthetic access$202(Lcom/android/internal/policy/impl/PhoneWindowManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideDisplayMode:I

    #@2
    return p1
.end method

.method static synthetic access$2100(Lcom/android/internal/policy/impl/PhoneWindowManager;Ljava/lang/String;Ljava/lang/String;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 233
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->excuteQuickMemoHotKey(Ljava/lang/String;Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$2200(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->toggleQuickClipFromHotKey()V

    #@3
    return-void
.end method

.method static synthetic access$2300(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@2
    return v0
.end method

.method static synthetic access$2400(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@2
    return v0
.end method

.method static synthetic access$2500(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppStartedByReservedKey:Z

    #@2
    return v0
.end method

.method static synthetic access$2502(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppStartedByReservedKey:Z

    #@2
    return p1
.end method

.method static synthetic access$2600(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->is112AppInstalledProperly()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$2800(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->showNavigationBar()V

    #@3
    return-void
.end method

.method static synthetic access$2900(Lcom/android/internal/policy/impl/PhoneWindowManager;Ljava/lang/String;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendIntents(Ljava/lang/String;)V

    #@3
    return-void
.end method

.method static synthetic access$300(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->enablePointerLocation()V

    #@3
    return-void
.end method

.method static synthetic access$3002(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetIntent:Z

    #@2
    return p1
.end method

.method static synthetic access$3102(Lcom/android/internal/policy/impl/PhoneWindowManager;Ljava/lang/String;)Ljava/lang/String;
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetName:Ljava/lang/String;

    #@2
    return-object p1
.end method

.method static synthetic access$3200(Lcom/android/internal/policy/impl/PhoneWindowManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateSystemUiVisibilityLw()I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method static synthetic access$3300(Lcom/android/internal/policy/impl/PhoneWindowManager;)Ljava/lang/Runnable;
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotChordLongPress:Ljava/lang/Runnable;

    #@2
    return-object v0
.end method

.method static synthetic access$3400(Lcom/android/internal/policy/impl/PhoneWindowManager;)I
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickBTNAnswserMode:I

    #@2
    return v0
.end method

.method static synthetic access$3402(Lcom/android/internal/policy/impl/PhoneWindowManager;I)I
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickBTNAnswserMode:I

    #@2
    return p1
.end method

.method static synthetic access$3500(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/os/IBinder;Landroid/view/WindowManagerPolicy$ScreenOnListener;)V
    .registers 3
    .parameter "x0"
    .parameter "x1"
    .parameter "x2"

    #@0
    .prologue
    .line 233
    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->waitForKeyguardWindowDrawn(Landroid/os/IBinder;Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    #@3
    return-void
.end method

.method static synthetic access$3600(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/view/WindowManagerPolicy$ScreenOnListener;)V
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->finishScreenTurningOn(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    #@3
    return-void
.end method

.method static synthetic access$3702(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIsExpanded:Z

    #@2
    return p1
.end method

.method static synthetic access$3802(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCameraCoverOpened:Z

    #@2
    return p1
.end method

.method static synthetic access$3900(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorEnabled:Z

    #@2
    return v0
.end method

.method static synthetic access$400(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->disablePointerLocation()V

    #@3
    return-void
.end method

.method static synthetic access$4002(Lcom/android/internal/policy/impl/PhoneWindowManager;Z)Z
    .registers 2
    .parameter "x0"
    .parameter "x1"

    #@0
    .prologue
    .line 233
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAATIsRunning:Z

    #@2
    return p1
.end method

.method static synthetic access$500(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->CAPP_TOUCH_PALM_COVER:Z

    #@2
    return v0
.end method

.method static synthetic access$600(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->enableLgeInputEventMonitor()V

    #@3
    return-void
.end method

.method static synthetic access$700(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->disableLgeInputEventMonitor()V

    #@3
    return-void
.end method

.method static synthetic access$800(Lcom/android/internal/policy/impl/PhoneWindowManager;)Z
    .registers 2
    .parameter "x0"

    #@0
    .prologue
    .line 233
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->CAPP_TOUCH_PALM_SWIPE:Z

    #@2
    return v0
.end method

.method static synthetic access$900(Lcom/android/internal/policy/impl/PhoneWindowManager;)V
    .registers 1
    .parameter "x0"

    #@0
    .prologue
    .line 233
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->performAuditoryFeedbackForAccessibilityIfNeed()V

    #@3
    return-void
.end method

.method private applyLidSwitchState()V
    .registers 4

    #@0
    .prologue
    .line 7644
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidState:I

    #@2
    if-nez v0, :cond_11

    #@4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidControlsSleep:Z

    #@6
    if-eqz v0, :cond_11

    #@8
    .line 7645
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@d
    move-result-wide v1

    #@e
    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->goToSleep(J)V

    #@11
    .line 7647
    :cond_11
    return-void
.end method

.method private applyStableConstraints(IILandroid/graphics/Rect;)V
    .registers 6
    .parameter "sysui"
    .parameter "fl"
    .parameter "r"

    #@0
    .prologue
    .line 4833
    and-int/lit16 v0, p1, 0x100

    #@2
    if-eqz v0, :cond_30

    #@4
    .line 4836
    and-int/lit16 v0, p2, 0x400

    #@6
    if-eqz v0, :cond_31

    #@8
    .line 4837
    iget v0, p3, Landroid/graphics/Rect;->left:I

    #@a
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenLeft:I

    #@c
    if-ge v0, v1, :cond_12

    #@e
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenLeft:I

    #@10
    iput v0, p3, Landroid/graphics/Rect;->left:I

    #@12
    .line 4838
    :cond_12
    iget v0, p3, Landroid/graphics/Rect;->top:I

    #@14
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenTop:I

    #@16
    if-ge v0, v1, :cond_1c

    #@18
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenTop:I

    #@1a
    iput v0, p3, Landroid/graphics/Rect;->top:I

    #@1c
    .line 4839
    :cond_1c
    iget v0, p3, Landroid/graphics/Rect;->right:I

    #@1e
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenRight:I

    #@20
    if-le v0, v1, :cond_26

    #@22
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenRight:I

    #@24
    iput v0, p3, Landroid/graphics/Rect;->right:I

    #@26
    .line 4840
    :cond_26
    iget v0, p3, Landroid/graphics/Rect;->bottom:I

    #@28
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenBottom:I

    #@2a
    if-le v0, v1, :cond_30

    #@2c
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenBottom:I

    #@2e
    iput v0, p3, Landroid/graphics/Rect;->bottom:I

    #@30
    .line 4848
    :cond_30
    :goto_30
    return-void

    #@31
    .line 4842
    :cond_31
    iget v0, p3, Landroid/graphics/Rect;->left:I

    #@33
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableLeft:I

    #@35
    if-ge v0, v1, :cond_3b

    #@37
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableLeft:I

    #@39
    iput v0, p3, Landroid/graphics/Rect;->left:I

    #@3b
    .line 4843
    :cond_3b
    iget v0, p3, Landroid/graphics/Rect;->top:I

    #@3d
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableTop:I

    #@3f
    if-ge v0, v1, :cond_45

    #@41
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableTop:I

    #@43
    iput v0, p3, Landroid/graphics/Rect;->top:I

    #@45
    .line 4844
    :cond_45
    iget v0, p3, Landroid/graphics/Rect;->right:I

    #@47
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableRight:I

    #@49
    if-le v0, v1, :cond_4f

    #@4b
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableRight:I

    #@4d
    iput v0, p3, Landroid/graphics/Rect;->right:I

    #@4f
    .line 4845
    :cond_4f
    iget v0, p3, Landroid/graphics/Rect;->bottom:I

    #@51
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableBottom:I

    #@53
    if-le v0, v1, :cond_30

    #@55
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableBottom:I

    #@57
    iput v0, p3, Landroid/graphics/Rect;->bottom:I

    #@59
    goto :goto_30
.end method

.method private cancelPending112AppChordAction()V
    .registers 3

    #@0
    .prologue
    .line 1662
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppChordLongPress:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 1663
    return-void
.end method

.method private cancelPendingEasyAccessEntryAction()V
    .registers 3

    #@0
    .prologue
    .line 1196
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEasyAccessEntry:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 1197
    return-void
.end method

.method private cancelPendingPowerKeyAction()V
    .registers 3

    #@0
    .prologue
    .line 1139
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyHandled:Z

    #@2
    if-nez v0, :cond_b

    #@4
    .line 1140
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerLongPress:Ljava/lang/Runnable;

    #@8
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@b
    .line 1142
    :cond_b
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@d
    if-eqz v0, :cond_12

    #@f
    .line 1143
    const/4 v0, 0x1

    #@10
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPendingPowerKeyUpCanceled:Z

    #@12
    .line 1145
    :cond_12
    return-void
.end method

.method private cancelPendingQuickClipChordAction()V
    .registers 3

    #@0
    .prologue
    .line 1450
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipChordLongPress:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 1451
    return-void
.end method

.method private cancelPendingScreenshotChordAction()V
    .registers 3

    #@0
    .prologue
    .line 1175
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotChordLongPress:Ljava/lang/Runnable;

    #@4
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7
    .line 1176
    return-void
.end method

.method private cancelPendingVolumeLongKeyAction(II)V
    .registers 6
    .parameter "keycode"
    .parameter "cancelReason"

    #@0
    .prologue
    .line 1235
    const-string v0, "WindowManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "cancelPendingVolumeLongKeyAction:cancelReason="

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@14
    move-result-object v1

    #@15
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 1237
    packed-switch p1, :pswitch_data_4a

    #@1b
    .line 1255
    :cond_1b
    :goto_1b
    return-void

    #@1c
    .line 1239
    :pswitch_1c
    iput p2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpLongKeyCancelReason:I

    #@1e
    .line 1240
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@20
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpLongPress:Ljava/lang/Runnable;

    #@22
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@25
    .line 1241
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@27
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@2a
    move-result v0

    #@2b
    if-eqz v0, :cond_1b

    #@2d
    .line 1242
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2f
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@32
    goto :goto_1b

    #@33
    .line 1246
    :pswitch_33
    iput p2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownLongKeyCancelReason:I

    #@35
    .line 1247
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@37
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownLongPress:Ljava/lang/Runnable;

    #@39
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@3c
    .line 1248
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@3e
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@41
    move-result v0

    #@42
    if-eqz v0, :cond_1b

    #@44
    .line 1249
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@46
    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    #@49
    goto :goto_1b

    #@4a
    .line 1237
    :pswitch_data_4a
    .packed-switch 0x18
        :pswitch_1c
        :pswitch_33
    .end packed-switch
.end method

.method private clearKeepHidingNavInputEventReceiver()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 4418
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@3
    if-eqz v0, :cond_26

    #@5
    .line 4419
    const-string v0, "WindowManager"

    #@7
    new-instance v1, Ljava/lang/StringBuilder;

    #@9
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@c
    const-string v2, "unregister KeepHidingNavInputEventReceiver of "

    #@e
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@11
    move-result-object v1

    #@12
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@14
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@17
    move-result-object v1

    #@18
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1b
    move-result-object v1

    #@1c
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@1f
    .line 4420
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@21
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;->dispose()V

    #@24
    .line 4421
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@26
    .line 4423
    :cond_26
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputChannel:Landroid/view/InputChannel;

    #@28
    if-eqz v0, :cond_31

    #@2a
    .line 4424
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputChannel:Landroid/view/InputChannel;

    #@2c
    invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V

    #@2f
    .line 4425
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputChannel:Landroid/view/InputChannel;

    #@31
    .line 4427
    :cond_31
    return-void
.end method

.method private disableLgeInputEventMonitor()V
    .registers 3

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 2578
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideDisplayMode:I

    #@3
    if-nez v0, :cond_23

    #@5
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTakeScreenshotMode:I

    #@7
    if-nez v0, :cond_23

    #@9
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSlideAsideMode:I

    #@b
    if-nez v0, :cond_23

    #@d
    .line 2579
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@f
    if-eqz v0, :cond_18

    #@11
    .line 2580
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;->dispose()V

    #@16
    .line 2581
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@18
    .line 2584
    :cond_18
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitorChannel:Landroid/view/InputChannel;

    #@1a
    if-eqz v0, :cond_23

    #@1c
    .line 2585
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitorChannel:Landroid/view/InputChannel;

    #@1e
    invoke-virtual {v0}, Landroid/view/InputChannel;->dispose()V

    #@21
    .line 2586
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitorChannel:Landroid/view/InputChannel;

    #@23
    .line 2589
    :cond_23
    return-void
.end method

.method private disablePointerLocation()V
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 2548
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;

    #@3
    if-eqz v1, :cond_c

    #@5
    .line 2549
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;

    #@7
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;->dispose()V

    #@a
    .line 2550
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;

    #@c
    .line 2553
    :cond_c
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationInputChannel:Landroid/view/InputChannel;

    #@e
    if-eqz v1, :cond_17

    #@10
    .line 2554
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationInputChannel:Landroid/view/InputChannel;

    #@12
    invoke-virtual {v1}, Landroid/view/InputChannel;->dispose()V

    #@15
    .line 2555
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationInputChannel:Landroid/view/InputChannel;

    #@17
    .line 2558
    :cond_17
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationView:Lcom/android/internal/widget/PointerLocationView;

    #@19
    if-eqz v1, :cond_2c

    #@1b
    .line 2559
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1d
    const-string v2, "window"

    #@1f
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@22
    move-result-object v0

    #@23
    check-cast v0, Landroid/view/WindowManager;

    #@25
    .line 2561
    .local v0, wm:Landroid/view/WindowManager;
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationView:Lcom/android/internal/widget/PointerLocationView;

    #@27
    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    #@2a
    .line 2562
    iput-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationView:Lcom/android/internal/widget/PointerLocationView;

    #@2c
    .line 2564
    .end local v0           #wm:Landroid/view/WindowManager;
    :cond_2c
    return-void
.end method

.method private enableLgeInputEventMonitor()V
    .registers 4

    #@0
    .prologue
    .line 2568
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitorChannel:Landroid/view/InputChannel;

    #@2
    if-nez v0, :cond_1f

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@6
    if-nez v0, :cond_1f

    #@8
    .line 2569
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@a
    const-string v1, "LGInputEventMonitor"

    #@c
    invoke-interface {v0, v1}, Landroid/view/WindowManagerPolicy$WindowManagerFuncs;->monitorInput(Ljava/lang/String;)Landroid/view/InputChannel;

    #@f
    move-result-object v0

    #@10
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitorChannel:Landroid/view/InputChannel;

    #@12
    .line 2571
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@14
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitorChannel:Landroid/view/InputChannel;

    #@16
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@19
    move-result-object v2

    #@1a
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@1d
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLgeInputEventMonitor:Lcom/android/internal/policy/impl/PhoneWindowManager$LgeInputEventMonitor;

    #@1f
    .line 2575
    :cond_1f
    return-void
.end method

.method private enablePointerLocation()V
    .registers 7

    #@0
    .prologue
    const/4 v4, -0x1

    #@1
    .line 2515
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationView:Lcom/android/internal/widget/PointerLocationView;

    #@3
    if-nez v2, :cond_6a

    #@5
    .line 2516
    new-instance v2, Lcom/android/internal/widget/PointerLocationView;

    #@7
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@9
    invoke-direct {v2, v3}, Lcom/android/internal/widget/PointerLocationView;-><init>(Landroid/content/Context;)V

    #@c
    iput-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationView:Lcom/android/internal/widget/PointerLocationView;

    #@e
    .line 2517
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationView:Lcom/android/internal/widget/PointerLocationView;

    #@10
    const/4 v3, 0x0

    #@11
    invoke-virtual {v2, v3}, Lcom/android/internal/widget/PointerLocationView;->setPrintCoords(Z)V

    #@14
    .line 2519
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    #@16
    invoke-direct {v0, v4, v4}, Landroid/view/WindowManager$LayoutParams;-><init>(II)V

    #@19
    .line 2522
    .local v0, lp:Landroid/view/WindowManager$LayoutParams;
    const/16 v2, 0x7df

    #@1b
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@1d
    .line 2523
    const/16 v2, 0x518

    #@1f
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@21
    .line 2527
    invoke-static {}, Landroid/app/ActivityManager;->isHighEndGfx()Z

    #@24
    move-result v2

    #@25
    if-eqz v2, :cond_34

    #@27
    .line 2528
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@29
    const/high16 v3, 0x100

    #@2b
    or-int/2addr v2, v3

    #@2c
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@2e
    .line 2529
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@30
    or-int/lit8 v2, v2, 0x2

    #@32
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@34
    .line 2532
    :cond_34
    const/4 v2, -0x3

    #@35
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    #@37
    .line 2533
    const-string v2, "PointerLocation"

    #@39
    invoke-virtual {v0, v2}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@3c
    .line 2534
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@3e
    const-string v3, "window"

    #@40
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@43
    move-result-object v1

    #@44
    check-cast v1, Landroid/view/WindowManager;

    #@46
    .line 2536
    .local v1, wm:Landroid/view/WindowManager;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@48
    or-int/lit8 v2, v2, 0x2

    #@4a
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->inputFeatures:I

    #@4c
    .line 2537
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationView:Lcom/android/internal/widget/PointerLocationView;

    #@4e
    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@51
    .line 2539
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@53
    const-string v3, "PointerLocationView"

    #@55
    invoke-interface {v2, v3}, Landroid/view/WindowManagerPolicy$WindowManagerFuncs;->monitorInput(Ljava/lang/String;)Landroid/view/InputChannel;

    #@58
    move-result-object v2

    #@59
    iput-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationInputChannel:Landroid/view/InputChannel;

    #@5b
    .line 2541
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;

    #@5d
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationInputChannel:Landroid/view/InputChannel;

    #@5f
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@62
    move-result-object v4

    #@63
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationView:Lcom/android/internal/widget/PointerLocationView;

    #@65
    invoke-direct {v2, v3, v4, v5}, Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;-><init>(Landroid/view/InputChannel;Landroid/os/Looper;Lcom/android/internal/widget/PointerLocationView;)V

    #@68
    iput-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$PointerLocationInputEventReceiver;

    #@6a
    .line 2545
    .end local v0           #lp:Landroid/view/WindowManager$LayoutParams;
    .end local v1           #wm:Landroid/view/WindowManager;
    :cond_6a
    return-void
.end method

.method private excuteQuickMemoHotKey(Ljava/lang/String;Ljava/lang/String;)V
    .registers 12
    .parameter "PkgName"
    .parameter "PkgClass"

    #@0
    .prologue
    const/4 v8, 0x0

    #@1
    .line 1556
    if-eqz p1, :cond_16

    #@3
    const-string v6, "none"

    #@5
    invoke-virtual {p1, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@8
    move-result v6

    #@9
    if-nez v6, :cond_16

    #@b
    .line 1557
    const-string v6, "com.lge.QuickClip"

    #@d
    invoke-virtual {p1, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_17

    #@13
    .line 1558
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->toggleQuickClipFromHotKey()V

    #@16
    .line 1625
    :cond_16
    :goto_16
    return-void

    #@17
    .line 1559
    :cond_17
    const-string v6, "com.lge.pa"

    #@19
    invoke-virtual {p1, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@1c
    move-result v6

    #@1d
    if-eqz v6, :cond_23

    #@1f
    .line 1560
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->excuteQuickVoice()V

    #@22
    goto :goto_16

    #@23
    .line 1561
    :cond_23
    const-string v6, "com.android.settings"

    #@25
    invoke-virtual {p1, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@28
    move-result v6

    #@29
    if-eqz v6, :cond_6d

    #@2b
    const-string v6, "com.android.settings.Settings"

    #@2d
    invoke-virtual {p2, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@30
    move-result v6

    #@31
    if-eqz v6, :cond_6d

    #@33
    .line 1562
    new-instance v4, Landroid/content/Intent;

    #@35
    const-string v6, "android.settings.SETTINGS"

    #@37
    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@3a
    .line 1563
    .local v4, settings:Landroid/content/Intent;
    const/high16 v6, 0x1020

    #@3c
    invoke-virtual {v4, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@3f
    .line 1566
    :try_start_3f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@42
    move-result-object v5

    #@43
    .line 1567
    .local v5, statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v5, :cond_48

    #@45
    .line 1568
    invoke-interface {v5}, Lcom/android/internal/statusbar/IStatusBarService;->collapsePanels()V
    :try_end_48
    .catch Landroid/os/RemoteException; {:try_start_3f .. :try_end_48} :catch_68
    .catch Landroid/content/ActivityNotFoundException; {:try_start_3f .. :try_end_48} :catch_4e

    #@48
    .line 1574
    .end local v5           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_48
    :goto_48
    :try_start_48
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@4a
    invoke-virtual {v6, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_4d
    .catch Landroid/content/ActivityNotFoundException; {:try_start_48 .. :try_end_4d} :catch_4e

    #@4d
    goto :goto_16

    #@4e
    .line 1575
    :catch_4e
    move-exception v2

    #@4f
    .line 1576
    .local v2, ex:Landroid/content/ActivityNotFoundException;
    const-string v6, "WindowManager"

    #@51
    new-instance v7, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v8, "Dropping application Settings launch because the activity to which it is registered was not found: , package="

    #@58
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v7

    #@5c
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v7

    #@60
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v7

    #@64
    invoke-static {v6, v7, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@67
    goto :goto_16

    #@68
    .line 1570
    .end local v2           #ex:Landroid/content/ActivityNotFoundException;
    :catch_68
    move-exception v2

    #@69
    .line 1572
    .local v2, ex:Landroid/os/RemoteException;
    const/4 v6, 0x0

    #@6a
    :try_start_6a
    iput-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;
    :try_end_6c
    .catch Landroid/content/ActivityNotFoundException; {:try_start_6a .. :try_end_6c} :catch_4e

    #@6c
    goto :goto_48

    #@6d
    .line 1581
    .end local v2           #ex:Landroid/os/RemoteException;
    .end local v4           #settings:Landroid/content/Intent;
    :cond_6d
    const-string v6, "com.skt.tmode"

    #@6f
    invoke-virtual {p1, v6}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    #@72
    move-result v6

    #@73
    if-eqz v6, :cond_c2

    #@75
    .line 1582
    new-instance v3, Landroid/content/Intent;

    #@77
    const-string v6, "com.skt.tmode.intent.action.TMODE_HOTKEY"

    #@79
    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7c
    .line 1583
    .local v3, intent:Landroid/content/Intent;
    const-string v6, "android.intent.category.HOME"

    #@7e
    invoke-virtual {v3, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@81
    .line 1584
    const-string v6, "android.intent.category.LAUNCHER"

    #@83
    invoke-virtual {v3, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@86
    .line 1585
    const/high16 v6, 0x3000

    #@88
    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@8b
    .line 1587
    :try_start_8b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@8e
    move-result-object v5

    #@8f
    .line 1588
    .restart local v5       #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v5, :cond_94

    #@91
    .line 1589
    invoke-interface {v5}, Lcom/android/internal/statusbar/IStatusBarService;->collapsePanels()V

    #@94
    .line 1590
    :cond_94
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@96
    invoke-virtual {v6, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_99
    .catch Landroid/content/ActivityNotFoundException; {:try_start_8b .. :try_end_99} :catch_9b
    .catch Landroid/os/RemoteException; {:try_start_8b .. :try_end_99} :catch_b6

    #@99
    goto/16 :goto_16

    #@9b
    .line 1591
    .end local v5           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_9b
    move-exception v2

    #@9c
    .line 1592
    .local v2, ex:Landroid/content/ActivityNotFoundException;
    const-string v6, "WindowManager"

    #@9e
    new-instance v7, Ljava/lang/StringBuilder;

    #@a0
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@a3
    const-string v8, "Dropping application QuickMemo key launch because the activity to which it is registered was not found: , package="

    #@a5
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a8
    move-result-object v7

    #@a9
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v7

    #@ad
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@b0
    move-result-object v7

    #@b1
    invoke-static {v6, v7, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b4
    goto/16 :goto_16

    #@b6
    .line 1595
    .end local v2           #ex:Landroid/content/ActivityNotFoundException;
    :catch_b6
    move-exception v1

    #@b7
    .line 1596
    .local v1, e:Landroid/os/RemoteException;
    const-string v6, "WindowManager"

    #@b9
    const-string v7, "RemoteException when long press simsitching key"

    #@bb
    invoke-static {v6, v7, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@be
    .line 1597
    iput-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@c0
    goto/16 :goto_16

    #@c2
    .line 1601
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v3           #intent:Landroid/content/Intent;
    :cond_c2
    new-instance v3, Landroid/content/Intent;

    #@c4
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    #@c7
    .line 1602
    .restart local v3       #intent:Landroid/content/Intent;
    const-string v6, "android.intent.action.MAIN"

    #@c9
    invoke-virtual {v3, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@cc
    .line 1603
    const-string v6, "android.intent.category.LAUNCHER"

    #@ce
    invoke-virtual {v3, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@d1
    .line 1604
    const/high16 v6, 0x3020

    #@d3
    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@d6
    .line 1605
    new-instance v0, Landroid/content/ComponentName;

    #@d8
    invoke-direct {v0, p1, p2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@db
    .line 1606
    .local v0, comp:Landroid/content/ComponentName;
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@de
    .line 1609
    :try_start_de
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@e1
    move-result-object v5

    #@e2
    .line 1610
    .restart local v5       #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v5, :cond_e7

    #@e4
    .line 1611
    invoke-interface {v5}, Lcom/android/internal/statusbar/IStatusBarService;->collapsePanels()V
    :try_end_e7
    .catch Landroid/os/RemoteException; {:try_start_de .. :try_end_e7} :catch_109
    .catch Landroid/content/ActivityNotFoundException; {:try_start_de .. :try_end_e7} :catch_ee

    #@e7
    .line 1617
    .end local v5           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_e7
    :goto_e7
    :try_start_e7
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@e9
    invoke-virtual {v6, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_ec
    .catch Landroid/content/ActivityNotFoundException; {:try_start_e7 .. :try_end_ec} :catch_ee

    #@ec
    goto/16 :goto_16

    #@ee
    .line 1618
    :catch_ee
    move-exception v2

    #@ef
    .line 1619
    .restart local v2       #ex:Landroid/content/ActivityNotFoundException;
    const-string v6, "WindowManager"

    #@f1
    new-instance v7, Ljava/lang/StringBuilder;

    #@f3
    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    #@f6
    const-string v8, "Dropping application QuickMemo key launch because the activity to which it is registered was not found: , package="

    #@f8
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fb
    move-result-object v7

    #@fc
    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ff
    move-result-object v7

    #@100
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@103
    move-result-object v7

    #@104
    invoke-static {v6, v7, v2}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@107
    goto/16 :goto_16

    #@109
    .line 1613
    .end local v2           #ex:Landroid/content/ActivityNotFoundException;
    :catch_109
    move-exception v2

    #@10a
    .line 1615
    .local v2, ex:Landroid/os/RemoteException;
    const/4 v6, 0x0

    #@10b
    :try_start_10b
    iput-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;
    :try_end_10d
    .catch Landroid/content/ActivityNotFoundException; {:try_start_10b .. :try_end_10d} :catch_ee

    #@10d
    goto :goto_e7
.end method

.method private excuteQuickVoice()V
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v7, 0x0

    #@2
    .line 8143
    sget-boolean v6, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEY_EXCEPTION:Z

    #@4
    if-eqz v6, :cond_1c

    #@6
    .line 8144
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@8
    if-eqz v6, :cond_1a

    #@a
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@c
    invoke-interface {v6}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@f
    move-result-object v0

    #@10
    .line 8145
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    :goto_10
    if-eqz v0, :cond_1c

    #@12
    .line 8146
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@14
    .line 8147
    .local v1, extend:I
    const/high16 v6, 0x10

    #@16
    and-int/2addr v6, v1

    #@17
    if-eqz v6, :cond_1c

    #@19
    .line 8171
    .end local v0           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v1           #extend:I
    :goto_19
    return-void

    #@1a
    :cond_1a
    move-object v0, v5

    #@1b
    .line 8144
    goto :goto_10

    #@1c
    .line 8153
    :cond_1c
    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    #@1f
    move-result-object v3

    #@20
    .line 8154
    .local v3, mediaState:Ljava/lang/String;
    const-string v6, "mounted"

    #@22
    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@25
    move-result v6

    #@26
    if-nez v6, :cond_4d

    #@28
    .line 8155
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2d
    move-result-object v5

    #@2e
    const v6, 0x209002c

    #@31
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    #@34
    move-result-object v4

    #@35
    .line 8156
    .local v4, msg:Ljava/lang/String;
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->nExptToast:Landroid/widget/Toast;

    #@37
    if-nez v5, :cond_47

    #@39
    .line 8157
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@3b
    invoke-static {v5, v4, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    #@3e
    move-result-object v5

    #@3f
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->nExptToast:Landroid/widget/Toast;

    #@41
    .line 8161
    :goto_41
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->nExptToast:Landroid/widget/Toast;

    #@43
    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    #@46
    goto :goto_19

    #@47
    .line 8159
    :cond_47
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->nExptToast:Landroid/widget/Toast;

    #@49
    invoke-virtual {v5, v4}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    #@4c
    goto :goto_41

    #@4d
    .line 8164
    .end local v4           #msg:Ljava/lang/String;
    :cond_4d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isKeyguardLocked()Z

    #@50
    move-result v6

    #@51
    if-eqz v6, :cond_56

    #@53
    .line 8165
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->dismissKeyguardLw()V

    #@56
    .line 8167
    :cond_56
    const/4 v6, 0x1

    #@57
    invoke-virtual {p0, v5, v7, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    #@5a
    .line 8169
    new-instance v2, Landroid/content/Intent;

    #@5c
    const-string v5, "com.lge.pa.ACTION_LAUNCH_HOME_LONG"

    #@5e
    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@61
    .line 8170
    .local v2, intent:Landroid/content/Intent;
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@63
    invoke-virtual {v5, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@66
    goto :goto_19
.end method

.method private finishScreenTurningOn(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V
    .registers 4
    .parameter "screenOnListener"

    #@0
    .prologue
    .line 7121
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 7122
    const/4 v0, 0x1

    #@4
    :try_start_4
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnFully:Z

    #@6
    .line 7123
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_4 .. :try_end_7} :catchall_13

    #@7
    .line 7126
    :try_start_7
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManager:Landroid/view/IWindowManager;

    #@9
    const/4 v1, 0x1

    #@a
    invoke-interface {v0, v1}, Landroid/view/IWindowManager;->setEventDispatching(Z)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_d} :catch_16

    #@d
    .line 7130
    :goto_d
    if-eqz p1, :cond_12

    #@f
    .line 7131
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$ScreenOnListener;->onScreenOn()V

    #@12
    .line 7133
    :cond_12
    return-void

    #@13
    .line 7123
    :catchall_13
    move-exception v0

    #@14
    :try_start_14
    monitor-exit v1
    :try_end_15
    .catchall {:try_start_14 .. :try_end_15} :catchall_13

    #@15
    throw v0

    #@16
    .line 7127
    :catch_16
    move-exception v0

    #@17
    goto :goto_d
.end method

.method static getAudioService()Landroid/media/IAudioService;
    .registers 3

    #@0
    .prologue
    .line 3202
    const-string v1, "audio"

    #@2
    invoke-static {v1}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v1

    #@6
    invoke-static {v1}, Landroid/media/IAudioService$Stub;->asInterface(Landroid/os/IBinder;)Landroid/media/IAudioService;

    #@9
    move-result-object v0

    #@a
    .line 3204
    .local v0, audioService:Landroid/media/IAudioService;
    if-nez v0, :cond_13

    #@c
    .line 3205
    const-string v1, "WindowManager"

    #@e
    const-string v2, "Unable to find IAudioService interface."

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 3207
    :cond_13
    return-object v0
.end method

.method static getLongIntArray(Landroid/content/res/Resources;I)[J
    .registers 7
    .parameter "r"
    .parameter "resid"

    #@0
    .prologue
    .line 7451
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getIntArray(I)[I

    #@3
    move-result-object v0

    #@4
    .line 7452
    .local v0, ar:[I
    if-nez v0, :cond_8

    #@6
    .line 7453
    const/4 v2, 0x0

    #@7
    .line 7459
    :cond_7
    return-object v2

    #@8
    .line 7455
    :cond_8
    array-length v3, v0

    #@9
    new-array v2, v3, [J

    #@b
    .line 7456
    .local v2, out:[J
    const/4 v1, 0x0

    #@c
    .local v1, i:I
    :goto_c
    array-length v3, v0

    #@d
    if-ge v1, v3, :cond_7

    #@f
    .line 7457
    aget v3, v0, v1

    #@11
    int-to-long v3, v3

    #@12
    aput-wide v3, v2, v1

    #@14
    .line 7456
    add-int/lit8 v1, v1, 0x1

    #@16
    goto :goto_c
.end method

.method private getScreenshotChordLongPressDelay()J
    .registers 4

    #@0
    .prologue
    .line 1165
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    if-eqz v0, :cond_16

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_16

    #@c
    .line 1167
    const/high16 v0, 0x4020

    #@e
    invoke-static {}, Landroid/view/ViewConfiguration;->getGlobalActionKeyTimeout()J

    #@11
    move-result-wide v1

    #@12
    long-to-float v1, v1

    #@13
    mul-float/2addr v0, v1

    #@14
    float-to-long v0, v0

    #@15
    .line 1170
    :goto_15
    return-wide v0

    #@16
    :cond_16
    invoke-static {}, Landroid/view/ViewConfiguration;->getGlobalActionKeyTimeout()J

    #@19
    move-result-wide v0

    #@1a
    goto :goto_15
.end method

.method private getSearchManager()Landroid/app/SearchManager;
    .registers 3

    #@0
    .prologue
    .line 4190
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSearchManager:Landroid/app/SearchManager;

    #@2
    if-nez v0, :cond_10

    #@4
    .line 4191
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@6
    const-string v1, "search"

    #@8
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b
    move-result-object v0

    #@c
    check-cast v0, Landroid/app/SearchManager;

    #@e
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSearchManager:Landroid/app/SearchManager;

    #@10
    .line 4193
    :cond_10
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSearchManager:Landroid/app/SearchManager;

    #@12
    return-object v0
.end method

.method private getSplitWindowManagerInstance()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;
    .registers 2

    #@0
    .prologue
    .line 8369
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->isSplitWindowLoaded:Z

    #@2
    if-eqz v0, :cond_7

    #@4
    .line 8370
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@6
    .line 8374
    :goto_6
    return-object v0

    #@7
    .line 8372
    :cond_7
    invoke-static {}, Lcom/lge/loader/splitwindow/SplitWindowCreatorHelper;->getPolicyService()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@a
    move-result-object v0

    #@b
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@d
    .line 8373
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@f
    if-nez v0, :cond_17

    #@11
    const/4 v0, 0x0

    #@12
    :goto_12
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->isSplitWindowLoaded:Z

    #@14
    .line 8374
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@16
    goto :goto_6

    #@17
    .line 8373
    :cond_17
    const/4 v0, 0x1

    #@18
    goto :goto_12
.end method

.method static getTelephonyService()Lcom/android/internal/telephony/ITelephony;
    .registers 1

    #@0
    .prologue
    .line 3197
    const-string v0, "phone"

    #@2
    invoke-static {v0}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@5
    move-result-object v0

    #@6
    invoke-static {v0}, Lcom/android/internal/telephony/ITelephony$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/telephony/ITelephony;

    #@9
    move-result-object v0

    #@a
    return-object v0
.end method

.method private handleLongPressOnHome()V
    .registers 12

    #@0
    .prologue
    const/4 v10, 0x0

    #@1
    const/4 v9, 0x2

    #@2
    const/4 v8, 0x1

    #@3
    const/4 v7, 0x0

    #@4
    .line 1947
    const/4 v0, 0x0

    #@5
    .line 1949
    .local v0, bCallState:I
    :try_start_5
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@8
    move-result-object v4

    #@9
    .line 1950
    .local v4, telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v4, :cond_f

    #@b
    .line 1951
    invoke-interface {v4}, Lcom/android/internal/telephony/ITelephony;->getCallState()I
    :try_end_e
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_e} :catch_1f

    #@e
    move-result v0

    #@f
    .line 1957
    .end local v4           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_f
    :goto_f
    const/16 v5, 0x64

    #@11
    if-eq v0, v5, :cond_17

    #@13
    const/16 v5, 0x65

    #@15
    if-ne v0, v5, :cond_28

    #@17
    .line 1958
    :cond_17
    const-string v5, "WindowManager"

    #@19
    const-string v6, "Ignoring LongPress HOME; there\'s a ringing incoming call."

    #@1b
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 1995
    :cond_1e
    :goto_1e
    return-void

    #@1f
    .line 1953
    :catch_1f
    move-exception v2

    #@20
    .line 1954
    .local v2, ex:Landroid/os/RemoteException;
    const-string v5, "WindowManager"

    #@22
    const-string v6, "RemoteException from getPhoneInterface()"

    #@24
    invoke-static {v5, v6, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@27
    goto :goto_f

    #@28
    .line 1963
    .end local v2           #ex:Landroid/os/RemoteException;
    :cond_28
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@2a
    if-gez v5, :cond_45

    #@2c
    .line 1964
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2e
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@31
    move-result-object v5

    #@32
    const v6, 0x10e0024

    #@35
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    #@38
    move-result v5

    #@39
    iput v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@3b
    .line 1966
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@3d
    if-ltz v5, :cond_43

    #@3f
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@41
    if-le v5, v9, :cond_45

    #@43
    .line 1968
    :cond_43
    iput v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@45
    .line 1972
    :cond_45
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@47
    if-eqz v5, :cond_53

    #@49
    .line 1973
    invoke-virtual {p0, v10, v7, v7}, Lcom/android/internal/policy/impl/PhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    #@4c
    .line 1974
    const-string v5, "recentapps"

    #@4e
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendCloseSystemWindows(Ljava/lang/String;)V

    #@51
    .line 1978
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeLongPressed:Z

    #@53
    .line 1981
    :cond_53
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@55
    if-ne v5, v8, :cond_5b

    #@57
    .line 1982
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/PhoneWindowManager;->showOrHideRecentAppsDialog(I)V

    #@5a
    goto :goto_1e

    #@5b
    .line 1983
    :cond_5b
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@5d
    if-ne v5, v9, :cond_1e

    #@5f
    .line 1985
    :try_start_5f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@62
    move-result-object v3

    #@63
    .line 1986
    .local v3, statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v3, :cond_1e

    #@65
    .line 1987
    invoke-interface {v3}, Lcom/android/internal/statusbar/IStatusBarService;->toggleRecentApps()V
    :try_end_68
    .catch Landroid/os/RemoteException; {:try_start_5f .. :try_end_68} :catch_69

    #@68
    goto :goto_1e

    #@69
    .line 1989
    .end local v3           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_69
    move-exception v1

    #@6a
    .line 1990
    .local v1, e:Landroid/os/RemoteException;
    const-string v5, "WindowManager"

    #@6c
    const-string v6, "RemoteException when showing recent apps"

    #@6e
    invoke-static {v5, v6, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@71
    .line 1992
    iput-object v10, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@73
    goto :goto_1e
.end method

.method private intercept112AppChord()V
    .registers 9

    #@0
    .prologue
    const-wide/16 v6, 0x3e8

    #@2
    .line 1635
    const-string v2, "WindowManager"

    #@4
    new-instance v3, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v4, "112APP_KEY : intercept112AppChord Started! : mVolumeUpKeyTriggered="

    #@b
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v3

    #@f
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@11
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@14
    move-result-object v3

    #@15
    const-string v4, " mPowerKeyTriggered="

    #@17
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v3

    #@1b
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@1d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@20
    move-result-object v3

    #@21
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1637
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@2a
    if-eqz v2, :cond_b4

    #@2c
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@2e
    if-eqz v2, :cond_b4

    #@30
    .line 1638
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@33
    move-result-wide v0

    #@34
    .line 1640
    .local v0, now:J
    const-string v2, "WindowManager"

    #@36
    new-instance v3, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v4, "112APP_KEY : now = "

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 1641
    const-string v2, "WindowManager"

    #@4e
    new-instance v3, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v4, "112APP_KEY : mVolumeUpKeyTime = "

    #@55
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v3

    #@59
    iget-wide v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTime:J

    #@5b
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v3

    #@5f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@62
    move-result-object v3

    #@63
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@66
    .line 1642
    const-string v2, "WindowManager"

    #@68
    new-instance v3, Ljava/lang/StringBuilder;

    #@6a
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@6d
    const-string v4, "112APP_KEY : mPowerKeyTime = "

    #@6f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@72
    move-result-object v3

    #@73
    iget-wide v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTime:J

    #@75
    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@78
    move-result-object v3

    #@79
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@7c
    move-result-object v3

    #@7d
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    .line 1644
    iget-wide v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTime:J

    #@82
    add-long/2addr v2, v6

    #@83
    cmp-long v2, v0, v2

    #@85
    if-gtz v2, :cond_b4

    #@87
    iget-wide v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTime:J

    #@89
    add-long/2addr v2, v6

    #@8a
    cmp-long v2, v0, v2

    #@8c
    if-gtz v2, :cond_b4

    #@8e
    .line 1646
    const-string v2, "WindowManager"

    #@90
    const-string v3, "112APP_KEY : mVolumeUpKeyTriggered + mPowerKeyTriggered Pressed!"

    #@92
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@95
    .line 1648
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->is112AppInstalledProperly()Z

    #@98
    move-result v2

    #@99
    if-eqz v2, :cond_b4

    #@9b
    .line 1649
    const/4 v2, 0x1

    #@9c
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedBy112App:Z

    #@9e
    .line 1650
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingPowerKeyAction()V

    #@a1
    .line 1651
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@a3
    if-eqz v2, :cond_ab

    #@a5
    .line 1652
    const/16 v2, 0x18

    #@a7
    const/4 v3, 0x6

    #@a8
    invoke-direct {p0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingVolumeLongKeyAction(II)V

    #@ab
    .line 1655
    :cond_ab
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@ad
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppChordLongPress:Ljava/lang/Runnable;

    #@af
    const-wide/16 v4, 0xbb8

    #@b1
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@b4
    .line 1659
    .end local v0           #now:J
    :cond_b4
    return-void
.end method

.method private interceptEasyAccessEntry()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const/4 v4, 0x0

    #@2
    .line 1181
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEasyAccessEnabled:Z

    #@4
    if-eqz v0, :cond_42

    #@6
    .line 1182
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@8
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@a
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@c
    array-length v2, v2

    #@d
    add-int/lit8 v2, v2, -0x1

    #@f
    invoke-static {v0, v6, v1, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@12
    .line 1183
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@14
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@16
    array-length v1, v1

    #@17
    add-int/lit8 v1, v1, -0x1

    #@19
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1c
    move-result-wide v2

    #@1d
    aput-wide v2, v0, v1

    #@1f
    .line 1185
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@21
    aget-wide v0, v0, v4

    #@23
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@26
    move-result-wide v2

    #@27
    const-wide/16 v4, 0x384

    #@29
    sub-long/2addr v2, v4

    #@2a
    cmp-long v0, v0, v2

    #@2c
    if-ltz v0, :cond_42

    #@2e
    .line 1187
    const-string v0, "WindowManager"

    #@30
    const-string v1, " Launch Accessbility"

    #@32
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@35
    .line 1188
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyConsumedByEasyAccess:Z

    #@37
    .line 1189
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@39
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEasyAccessEntry:Ljava/lang/Runnable;

    #@3b
    invoke-static {}, Landroid/view/ViewConfiguration;->getGlobalActionKeyTimeout()J

    #@3e
    move-result-wide v2

    #@3f
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@42
    .line 1193
    :cond_42
    return-void
.end method

.method private interceptFallback(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;I)Z
    .registers 10
    .parameter "win"
    .parameter "fallbackEvent"
    .parameter "policyFlags"

    #@0
    .prologue
    const/4 v3, 0x1

    #@1
    .line 4141
    invoke-virtual {p0, p2, p3, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I

    #@4
    move-result v0

    #@5
    .line 4142
    .local v0, actions:I
    and-int/lit8 v4, v0, 0x1

    #@7
    if-eqz v4, :cond_14

    #@9
    .line 4143
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptKeyBeforeDispatching(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;I)J

    #@c
    move-result-wide v1

    #@d
    .line 4145
    .local v1, delayMillis:J
    const-wide/16 v4, 0x0

    #@f
    cmp-long v4, v1, v4

    #@11
    if-nez v4, :cond_14

    #@13
    .line 4149
    .end local v1           #delayMillis:J
    :goto_13
    return v3

    #@14
    :cond_14
    const/4 v3, 0x0

    #@15
    goto :goto_13
.end method

.method private interceptPowerKeyDown(Z)V
    .registers 6
    .parameter "handled"

    #@0
    .prologue
    .line 1114
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@2
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPower8SecLongPress:Ljava/lang/Runnable;

    #@4
    const-wide/16 v2, 0xfa0

    #@6
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@9
    .line 1116
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyHandled:Z

    #@b
    .line 1117
    if-nez p1, :cond_16

    #@d
    .line 1118
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@f
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerLongPress:Ljava/lang/Runnable;

    #@11
    const-wide/16 v2, 0x5dc

    #@13
    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@16
    .line 1120
    :cond_16
    return-void
.end method

.method private interceptPowerKeyUp(Z)Z
    .registers 5
    .parameter "canceled"

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1124
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPower8SecLongPressHandled:Z

    #@3
    if-nez v1, :cond_1b

    #@5
    .line 1125
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@7
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPower8SecLongPress:Ljava/lang/Runnable;

    #@9
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@c
    .line 1131
    :goto_c
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyHandled:Z

    #@e
    if-nez v1, :cond_1a

    #@10
    .line 1132
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@12
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerLongPress:Ljava/lang/Runnable;

    #@14
    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@17
    .line 1133
    if-nez p1, :cond_1a

    #@19
    const/4 v0, 0x1

    #@1a
    .line 1135
    :cond_1a
    return v0

    #@1b
    .line 1127
    :cond_1b
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->dismissRestartActionDialog()V

    #@1e
    .line 1128
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPower8SecLongPressHandled:Z

    #@20
    goto :goto_c
.end method

.method private interceptQuickClipChord()V
    .registers 8

    #@0
    .prologue
    const/4 v6, 0x1

    #@1
    const-wide/16 v4, 0x96

    #@3
    .line 1437
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@5
    if-nez v2, :cond_b

    #@7
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@9
    if-eqz v2, :cond_38

    #@b
    .line 1438
    :cond_b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@e
    move-result-wide v0

    #@f
    .line 1439
    .local v0, now:J
    iget-wide v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTime:J

    #@11
    add-long/2addr v2, v4

    #@12
    cmp-long v2, v0, v2

    #@14
    if-gtz v2, :cond_38

    #@16
    iget-wide v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTime:J

    #@18
    add-long/2addr v2, v4

    #@19
    cmp-long v2, v0, v2

    #@1b
    if-gtz v2, :cond_38

    #@1d
    .line 1441
    const-string v2, "sys.allautotest.run"

    #@1f
    const-string v3, "false"

    #@21
    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@24
    move-result-object v2

    #@25
    const-string v3, "false"

    #@27
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2a
    move-result v2

    #@2b
    if-eqz v2, :cond_38

    #@2d
    .line 1442
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByQuickClipChord:Z

    #@2f
    .line 1443
    iput-boolean v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedByQuickClipChord:Z

    #@31
    .line 1444
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@33
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipChordLongPress:Ljava/lang/Runnable;

    #@35
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@38
    .line 1448
    .end local v0           #now:J
    :cond_38
    return-void
.end method

.method private interceptScreenshotChord()V
    .registers 7

    #@0
    .prologue
    const-wide/16 v4, 0x96

    #@2
    .line 1148
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotChordEnabled:Z

    #@4
    if-eqz v2, :cond_3f

    #@6
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@8
    if-eqz v2, :cond_3f

    #@a
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@c
    if-eqz v2, :cond_3f

    #@e
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@10
    if-nez v2, :cond_3f

    #@12
    .line 1150
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@15
    move-result-wide v0

    #@16
    .line 1151
    .local v0, now:J
    iget-wide v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTime:J

    #@18
    add-long/2addr v2, v4

    #@19
    cmp-long v2, v0, v2

    #@1b
    if-gtz v2, :cond_3f

    #@1d
    iget-wide v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTime:J

    #@1f
    add-long/2addr v2, v4

    #@20
    cmp-long v2, v0, v2

    #@22
    if-gtz v2, :cond_3f

    #@24
    .line 1153
    const/4 v2, 0x1

    #@25
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@27
    .line 1154
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingPowerKeyAction()V

    #@2a
    .line 1155
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@2c
    if-eqz v2, :cond_34

    #@2e
    .line 1156
    const/16 v2, 0x19

    #@30
    const/4 v3, 0x5

    #@31
    invoke-direct {p0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingVolumeLongKeyAction(II)V

    #@34
    .line 1159
    :cond_34
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@36
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotChordLongPress:Ljava/lang/Runnable;

    #@38
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getScreenshotChordLongPressDelay()J

    #@3b
    move-result-wide v4

    #@3c
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@3f
    .line 1162
    .end local v0           #now:J
    :cond_3f
    return-void
.end method

.method private interceptVolumeDownLongKey(Z)V
    .registers 10
    .parameter "isScreenOn"

    #@0
    .prologue
    const-wide/16 v6, 0x3e8

    #@2
    const/4 v5, 0x1

    #@3
    .line 1258
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@5
    if-eqz v2, :cond_3c

    #@7
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@9
    if-nez v2, :cond_3c

    #@b
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@d
    if-nez v2, :cond_3c

    #@f
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isVolumeLongKeyAvailable(Z)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_3c

    #@15
    .line 1260
    const-string v2, "true"

    #@17
    const-string v3, "gsm.lge.ota_is_running"

    #@19
    const-string v4, "false"

    #@1b
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v2

    #@23
    if-nez v2, :cond_35

    #@25
    const-string v2, "true"

    #@27
    const-string v3, "gsm.lge.ota_ignoreKey"

    #@29
    const-string v4, "false"

    #@2b
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_3d

    #@35
    .line 1262
    :cond_35
    const-string v2, "WindowManager"

    #@37
    const-string v3, "OTA key ignore!"

    #@39
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 1277
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 1265
    :cond_3d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@40
    move-result-wide v0

    #@41
    .line 1266
    .local v0, now:J
    iget-wide v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTime:J

    #@43
    add-long/2addr v2, v6

    #@44
    cmp-long v2, v0, v2

    #@46
    if-gtz v2, :cond_3c

    #@48
    .line 1267
    const-string v2, "WindowManager"

    #@4a
    const-string v3, "interceptVolumeDownLongKey"

    #@4c
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 1268
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@51
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@54
    .line 1269
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByLongPress:Z

    #@56
    .line 1270
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorEnabled:Z

    #@58
    if-nez v2, :cond_65

    #@5a
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverClosed:Z

    #@5c
    if-nez v2, :cond_65

    #@5e
    .line 1271
    const/4 v2, 0x0

    #@5f
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setProximitySensorEnabled(Z)V

    #@62
    .line 1272
    invoke-direct {p0, v5}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setProximitySensorEnabled(Z)V

    #@65
    .line 1274
    :cond_65
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@67
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownLongPress:Ljava/lang/Runnable;

    #@69
    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@6c
    goto :goto_3c
.end method

.method private interceptVolumeUpLongKey(Z)V
    .registers 10
    .parameter "isScreenOn"

    #@0
    .prologue
    const-wide/16 v6, 0x3e8

    #@2
    const/4 v5, 0x1

    #@3
    .line 1213
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@5
    if-eqz v2, :cond_3c

    #@7
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@9
    if-nez v2, :cond_3c

    #@b
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@d
    if-nez v2, :cond_3c

    #@f
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isVolumeLongKeyAvailable(Z)Z

    #@12
    move-result v2

    #@13
    if-eqz v2, :cond_3c

    #@15
    .line 1215
    const-string v2, "true"

    #@17
    const-string v3, "gsm.lge.ota_is_running"

    #@19
    const-string v4, "false"

    #@1b
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1e
    move-result-object v3

    #@1f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@22
    move-result v2

    #@23
    if-nez v2, :cond_35

    #@25
    const-string v2, "true"

    #@27
    const-string v3, "gsm.lge.ota_ignoreKey"

    #@29
    const-string v4, "false"

    #@2b
    invoke-static {v3, v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@2e
    move-result-object v3

    #@2f
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@32
    move-result v2

    #@33
    if-eqz v2, :cond_3d

    #@35
    .line 1217
    :cond_35
    const-string v2, "WindowManager"

    #@37
    const-string v3, "OTA key ignore!"

    #@39
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3c
    .line 1232
    :cond_3c
    :goto_3c
    return-void

    #@3d
    .line 1220
    :cond_3d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@40
    move-result-wide v0

    #@41
    .line 1221
    .local v0, now:J
    iget-wide v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTime:J

    #@43
    add-long/2addr v2, v6

    #@44
    cmp-long v2, v0, v2

    #@46
    if-gtz v2, :cond_3c

    #@48
    .line 1222
    const-string v2, "WindowManager"

    #@4a
    const-string v3, "interceptVolumeUpLongKey:"

    #@4c
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4f
    .line 1223
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@51
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@54
    .line 1224
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedByLongPress:Z

    #@56
    .line 1225
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorEnabled:Z

    #@58
    if-nez v2, :cond_65

    #@5a
    iget-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverClosed:Z

    #@5c
    if-nez v2, :cond_65

    #@5e
    .line 1226
    const/4 v2, 0x0

    #@5f
    invoke-direct {p0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setProximitySensorEnabled(Z)V

    #@62
    .line 1227
    invoke-direct {p0, v5}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setProximitySensorEnabled(Z)V

    #@65
    .line 1229
    :cond_65
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@67
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpLongPress:Ljava/lang/Runnable;

    #@69
    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@6c
    goto :goto_3c
.end method

.method private is112AppAgreedByCustomer()Z
    .registers 9

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v5, 0x0

    #@2
    const/4 v3, 0x0

    #@3
    .line 1709
    const-string v0, "content://go.police.provider.report/agree"

    #@5
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    #@8
    move-result-object v1

    #@9
    .line 1710
    .local v1, uri:Landroid/net/Uri;
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@b
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@e
    move-result-object v0

    #@f
    new-array v2, v7, [Ljava/lang/String;

    #@11
    const-string v4, "customer_agreement"

    #@13
    aput-object v4, v2, v5

    #@15
    move-object v4, v3

    #@16
    move-object v5, v3

    #@17
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    #@1a
    move-result-object v6

    #@1b
    .line 1712
    .local v6, cursor:Landroid/database/Cursor;
    if-eqz v6, :cond_43

    #@1d
    .line 1714
    :try_start_1d
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    #@20
    move-result v0

    #@21
    if-eqz v0, :cond_44

    #@23
    .line 1715
    const-string v0, "WindowManager"

    #@25
    new-instance v2, Ljava/lang/StringBuilder;

    #@27
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@2a
    const-string v3, "112APP_KEY : cursor.getString(0) : "

    #@2c
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2f
    move-result-object v2

    #@30
    const/4 v3, 0x0

    #@31
    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    #@34
    move-result-object v3

    #@35
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v2

    #@39
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3c
    move-result-object v2

    #@3d
    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_40
    .catchall {:try_start_1d .. :try_end_40} :catchall_4c

    #@40
    .line 1722
    :goto_40
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@43
    .line 1726
    :cond_43
    return v7

    #@44
    .line 1717
    :cond_44
    :try_start_44
    const-string v0, "WindowManager"

    #@46
    const-string v2, "112APP_KEY : User has not been agreed to install 112 app."

    #@48
    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4b
    .catchall {:try_start_44 .. :try_end_4b} :catchall_4c

    #@4b
    goto :goto_40

    #@4c
    .line 1722
    :catchall_4c
    move-exception v0

    #@4d
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    #@50
    throw v0
.end method

.method private is112AppInstalled()Z
    .registers 7

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 1699
    const/4 v0, 0x0

    #@2
    .line 1701
    .local v0, app:Landroid/content/pm/ApplicationInfo;
    :try_start_2
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@7
    move-result-object v3

    #@8
    const-string v4, "go.police.report"

    #@a
    const/4 v5, 0x0

    #@b
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_e} :catch_13

    #@e
    move-result-object v0

    #@f
    .line 1705
    :goto_f
    if-eqz v0, :cond_12

    #@11
    const/4 v2, 0x1

    #@12
    :cond_12
    return v2

    #@13
    .line 1702
    :catch_13
    move-exception v1

    #@14
    .line 1703
    .local v1, e:Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "WindowManager"

    #@16
    const-string v4, "112APP_KEY : 112 go.police.report is not installed"

    #@18
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    goto :goto_f
.end method

.method private is112AppInstalledProperly()Z
    .registers 2

    #@0
    .prologue
    .line 1695
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->is112AppInstalled()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_e

    #@6
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->is112AppAgreedByCustomer()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method private isASystemWindow(Landroid/view/WindowManagerPolicy$WindowState;)Z
    .registers 5
    .parameter "win"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3224
    if-nez p1, :cond_4

    #@3
    .line 3228
    :cond_3
    :goto_3
    return v1

    #@4
    .line 3227
    :cond_4
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@7
    move-result-object v2

    #@8
    iget v0, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@a
    .line 3228
    .local v0, type:I
    const/16 v2, 0x7d0

    #@c
    if-lt v0, v2, :cond_3

    #@e
    const/16 v2, 0xbb7

    #@10
    if-gt v0, v2, :cond_3

    #@12
    const/4 v1, 0x1

    #@13
    goto :goto_3
.end method

.method private isAnyPortrait(I)Z
    .registers 3
    .parameter "rotation"

    #@0
    .prologue
    .line 7406
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@2
    if-eq p1, v0, :cond_8

    #@4
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUpsideDownRotation:I

    #@6
    if-ne p1, v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private isBuiltInKeyboardVisible()Z
    .registers 2

    #@0
    .prologue
    .line 2728
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHaveBuiltInKeyboard:Z

    #@2
    if-eqz v0, :cond_e

    #@4
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidKeyboardAccessibility:I

    #@6
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isHidden(I)Z

    #@9
    move-result v0

    #@a
    if-nez v0, :cond_e

    #@c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method public static isFindMissedPhoneRegister()Z
    .registers 10

    #@0
    .prologue
    .line 8391
    const-string v0, "/persist-lg/findlostphone/MissedContents"

    #@2
    .line 8393
    .local v0, MISSEDDPHONE_FILE:Ljava/lang/String;
    const/4 v6, 0x0

    #@3
    .line 8395
    .local v6, result:Ljava/lang/String;
    const/4 v2, 0x0

    #@4
    .line 8396
    .local v2, fr:Ljava/io/FileReader;
    const/4 v4, 0x0

    #@5
    .line 8398
    .local v4, in:Ljava/io/BufferedReader;
    :try_start_5
    new-instance v3, Ljava/io/FileReader;

    #@7
    const-string v7, "/persist-lg/findlostphone/MissedContents"

    #@9
    invoke-direct {v3, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_5 .. :try_end_c} :catchall_a6
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_c} :catch_5c
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_c} :catch_81

    #@c
    .line 8399
    .end local v2           #fr:Ljava/io/FileReader;
    .local v3, fr:Ljava/io/FileReader;
    :try_start_c
    new-instance v5, Ljava/io/BufferedReader;

    #@e
    invoke-direct {v5, v3}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_11
    .catchall {:try_start_c .. :try_end_11} :catchall_c4
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_11} :catch_d2
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_11} :catch_cb

    #@11
    .line 8401
    .end local v4           #in:Ljava/io/BufferedReader;
    .local v5, in:Ljava/io/BufferedReader;
    :try_start_11
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    .line 8403
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    #@18
    .line 8404
    if-eqz v3, :cond_1d

    #@1a
    .line 8405
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_1d
    .catchall {:try_start_11 .. :try_end_1d} :catchall_c7
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_1d} :catch_d5
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_1d} :catch_ce

    #@1d
    .line 8412
    :cond_1d
    if-eqz v5, :cond_22

    #@1f
    .line 8414
    :try_start_1f
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V
    :try_end_22
    .catch Ljava/io/IOException; {:try_start_1f .. :try_end_22} :catch_48

    #@22
    .line 8419
    :cond_22
    :goto_22
    if-eqz v3, :cond_d9

    #@24
    .line 8421
    :try_start_24
    invoke-virtual {v3}, Ljava/io/FileReader;->close()V
    :try_end_27
    .catch Ljava/io/IOException; {:try_start_24 .. :try_end_27} :catch_51

    #@27
    move-object v4, v5

    #@28
    .end local v5           #in:Ljava/io/BufferedReader;
    .restart local v4       #in:Ljava/io/BufferedReader;
    move-object v2, v3

    #@29
    .line 8427
    .end local v3           #fr:Ljava/io/FileReader;
    .restart local v2       #fr:Ljava/io/FileReader;
    :cond_29
    :goto_29
    const-string v7, "SKT_FIND_PHONE"

    #@2b
    new-instance v8, Ljava/lang/StringBuilder;

    #@2d
    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    #@30
    const-string v9, "WindowManager - isFindMissedPhoneRegister  result = "

    #@32
    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@35
    move-result-object v8

    #@36
    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@39
    move-result-object v8

    #@3a
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3d
    move-result-object v8

    #@3e
    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 8429
    const-string v7, "1"

    #@43
    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@46
    move-result v7

    #@47
    return v7

    #@48
    .line 8415
    .end local v2           #fr:Ljava/io/FileReader;
    .end local v4           #in:Ljava/io/BufferedReader;
    .restart local v3       #fr:Ljava/io/FileReader;
    .restart local v5       #in:Ljava/io/BufferedReader;
    :catch_48
    move-exception v1

    #@49
    .line 8416
    .local v1, e:Ljava/io/IOException;
    const-string v7, "WindowManager"

    #@4b
    const-string v8, "[1-1]read failed: in close fail"

    #@4d
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@50
    goto :goto_22

    #@51
    .line 8422
    .end local v1           #e:Ljava/io/IOException;
    :catch_51
    move-exception v1

    #@52
    .line 8423
    .restart local v1       #e:Ljava/io/IOException;
    const-string v7, "WindowManager"

    #@54
    const-string v8, "[1-1]read failed: fr close fail"

    #@56
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@59
    move-object v4, v5

    #@5a
    .end local v5           #in:Ljava/io/BufferedReader;
    .restart local v4       #in:Ljava/io/BufferedReader;
    move-object v2, v3

    #@5b
    .line 8424
    .end local v3           #fr:Ljava/io/FileReader;
    .restart local v2       #fr:Ljava/io/FileReader;
    goto :goto_29

    #@5c
    .line 8407
    .end local v1           #e:Ljava/io/IOException;
    :catch_5c
    move-exception v1

    #@5d
    .line 8408
    .local v1, e:Ljava/io/FileNotFoundException;
    :goto_5d
    :try_start_5d
    const-string v7, "WindowManager"

    #@5f
    const-string v8, "read failed: FileNotFoundException"

    #@61
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_64
    .catchall {:try_start_5d .. :try_end_64} :catchall_a6

    #@64
    .line 8412
    if-eqz v4, :cond_69

    #@66
    .line 8414
    :try_start_66
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_69
    .catch Ljava/io/IOException; {:try_start_66 .. :try_end_69} :catch_78

    #@69
    .line 8419
    .end local v1           #e:Ljava/io/FileNotFoundException;
    :cond_69
    :goto_69
    if-eqz v2, :cond_29

    #@6b
    .line 8421
    :try_start_6b
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_6e
    .catch Ljava/io/IOException; {:try_start_6b .. :try_end_6e} :catch_6f

    #@6e
    goto :goto_29

    #@6f
    .line 8422
    :catch_6f
    move-exception v1

    #@70
    .line 8423
    .local v1, e:Ljava/io/IOException;
    const-string v7, "WindowManager"

    #@72
    const-string v8, "[1-1]read failed: fr close fail"

    #@74
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@77
    goto :goto_29

    #@78
    .line 8415
    .local v1, e:Ljava/io/FileNotFoundException;
    :catch_78
    move-exception v1

    #@79
    .line 8416
    .local v1, e:Ljava/io/IOException;
    const-string v7, "WindowManager"

    #@7b
    const-string v8, "[1-1]read failed: in close fail"

    #@7d
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@80
    goto :goto_69

    #@81
    .line 8409
    .end local v1           #e:Ljava/io/IOException;
    :catch_81
    move-exception v1

    #@82
    .line 8410
    .restart local v1       #e:Ljava/io/IOException;
    :goto_82
    :try_start_82
    const-string v7, "WindowManager"

    #@84
    const-string v8, "read failed: IOException"

    #@86
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_89
    .catchall {:try_start_82 .. :try_end_89} :catchall_a6

    #@89
    .line 8412
    if-eqz v4, :cond_8e

    #@8b
    .line 8414
    :try_start_8b
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_8e
    .catch Ljava/io/IOException; {:try_start_8b .. :try_end_8e} :catch_9d

    #@8e
    .line 8419
    :cond_8e
    :goto_8e
    if-eqz v2, :cond_29

    #@90
    .line 8421
    :try_start_90
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_93
    .catch Ljava/io/IOException; {:try_start_90 .. :try_end_93} :catch_94

    #@93
    goto :goto_29

    #@94
    .line 8422
    :catch_94
    move-exception v1

    #@95
    .line 8423
    const-string v7, "WindowManager"

    #@97
    const-string v8, "[1-1]read failed: fr close fail"

    #@99
    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@9c
    goto :goto_29

    #@9d
    .line 8415
    :catch_9d
    move-exception v1

    #@9e
    .line 8416
    const-string v7, "WindowManager"

    #@a0
    const-string v8, "[1-1]read failed: in close fail"

    #@a2
    invoke-static {v7, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@a5
    goto :goto_8e

    #@a6
    .line 8412
    .end local v1           #e:Ljava/io/IOException;
    :catchall_a6
    move-exception v7

    #@a7
    :goto_a7
    if-eqz v4, :cond_ac

    #@a9
    .line 8414
    :try_start_a9
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_ac
    .catch Ljava/io/IOException; {:try_start_a9 .. :try_end_ac} :catch_b2

    #@ac
    .line 8419
    :cond_ac
    :goto_ac
    if-eqz v2, :cond_b1

    #@ae
    .line 8421
    :try_start_ae
    invoke-virtual {v2}, Ljava/io/FileReader;->close()V
    :try_end_b1
    .catch Ljava/io/IOException; {:try_start_ae .. :try_end_b1} :catch_bb

    #@b1
    .line 8424
    :cond_b1
    :goto_b1
    throw v7

    #@b2
    .line 8415
    :catch_b2
    move-exception v1

    #@b3
    .line 8416
    .restart local v1       #e:Ljava/io/IOException;
    const-string v8, "WindowManager"

    #@b5
    const-string v9, "[1-1]read failed: in close fail"

    #@b7
    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@ba
    goto :goto_ac

    #@bb
    .line 8422
    .end local v1           #e:Ljava/io/IOException;
    :catch_bb
    move-exception v1

    #@bc
    .line 8423
    .restart local v1       #e:Ljava/io/IOException;
    const-string v8, "WindowManager"

    #@be
    const-string v9, "[1-1]read failed: fr close fail"

    #@c0
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    goto :goto_b1

    #@c4
    .line 8412
    .end local v1           #e:Ljava/io/IOException;
    .end local v2           #fr:Ljava/io/FileReader;
    .restart local v3       #fr:Ljava/io/FileReader;
    :catchall_c4
    move-exception v7

    #@c5
    move-object v2, v3

    #@c6
    .end local v3           #fr:Ljava/io/FileReader;
    .restart local v2       #fr:Ljava/io/FileReader;
    goto :goto_a7

    #@c7
    .end local v2           #fr:Ljava/io/FileReader;
    .end local v4           #in:Ljava/io/BufferedReader;
    .restart local v3       #fr:Ljava/io/FileReader;
    .restart local v5       #in:Ljava/io/BufferedReader;
    :catchall_c7
    move-exception v7

    #@c8
    move-object v4, v5

    #@c9
    .end local v5           #in:Ljava/io/BufferedReader;
    .restart local v4       #in:Ljava/io/BufferedReader;
    move-object v2, v3

    #@ca
    .end local v3           #fr:Ljava/io/FileReader;
    .restart local v2       #fr:Ljava/io/FileReader;
    goto :goto_a7

    #@cb
    .line 8409
    .end local v2           #fr:Ljava/io/FileReader;
    .restart local v3       #fr:Ljava/io/FileReader;
    :catch_cb
    move-exception v1

    #@cc
    move-object v2, v3

    #@cd
    .end local v3           #fr:Ljava/io/FileReader;
    .restart local v2       #fr:Ljava/io/FileReader;
    goto :goto_82

    #@ce
    .end local v2           #fr:Ljava/io/FileReader;
    .end local v4           #in:Ljava/io/BufferedReader;
    .restart local v3       #fr:Ljava/io/FileReader;
    .restart local v5       #in:Ljava/io/BufferedReader;
    :catch_ce
    move-exception v1

    #@cf
    move-object v4, v5

    #@d0
    .end local v5           #in:Ljava/io/BufferedReader;
    .restart local v4       #in:Ljava/io/BufferedReader;
    move-object v2, v3

    #@d1
    .end local v3           #fr:Ljava/io/FileReader;
    .restart local v2       #fr:Ljava/io/FileReader;
    goto :goto_82

    #@d2
    .line 8407
    .end local v2           #fr:Ljava/io/FileReader;
    .restart local v3       #fr:Ljava/io/FileReader;
    :catch_d2
    move-exception v1

    #@d3
    move-object v2, v3

    #@d4
    .end local v3           #fr:Ljava/io/FileReader;
    .restart local v2       #fr:Ljava/io/FileReader;
    goto :goto_5d

    #@d5
    .end local v2           #fr:Ljava/io/FileReader;
    .end local v4           #in:Ljava/io/BufferedReader;
    .restart local v3       #fr:Ljava/io/FileReader;
    .restart local v5       #in:Ljava/io/BufferedReader;
    :catch_d5
    move-exception v1

    #@d6
    move-object v4, v5

    #@d7
    .end local v5           #in:Ljava/io/BufferedReader;
    .restart local v4       #in:Ljava/io/BufferedReader;
    move-object v2, v3

    #@d8
    .end local v3           #fr:Ljava/io/FileReader;
    .restart local v2       #fr:Ljava/io/FileReader;
    goto :goto_5d

    #@d9
    .end local v2           #fr:Ljava/io/FileReader;
    .end local v4           #in:Ljava/io/BufferedReader;
    .restart local v3       #fr:Ljava/io/FileReader;
    .restart local v5       #in:Ljava/io/BufferedReader;
    :cond_d9
    move-object v4, v5

    #@da
    .end local v5           #in:Ljava/io/BufferedReader;
    .restart local v4       #in:Ljava/io/BufferedReader;
    move-object v2, v3

    #@db
    .end local v3           #fr:Ljava/io/FileReader;
    .restart local v2       #fr:Ljava/io/FileReader;
    goto/16 :goto_29
.end method

.method private isGlobalAccessibilityGestureEnabled()Z
    .registers 5

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 7788
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v2

    #@8
    const-string v3, "enable_accessibility_global_gesture_enabled"

    #@a
    invoke-static {v2, v3, v1}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d
    move-result v2

    #@e
    if-ne v2, v0, :cond_11

    #@10
    :goto_10
    return v0

    #@11
    :cond_11
    move v0, v1

    #@12
    goto :goto_10
.end method

.method private isHidden(I)Z
    .registers 5
    .parameter "accessibilityMode"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 2717
    packed-switch p1, :pswitch_data_14

    #@5
    move v0, v1

    #@6
    .line 2723
    :cond_6
    :goto_6
    return v0

    #@7
    .line 2719
    :pswitch_7
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidState:I

    #@9
    if-eqz v2, :cond_6

    #@b
    move v0, v1

    #@c
    goto :goto_6

    #@d
    .line 2721
    :pswitch_d
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidState:I

    #@f
    if-eq v2, v0, :cond_6

    #@11
    move v0, v1

    #@12
    goto :goto_6

    #@13
    .line 2717
    nop

    #@14
    :pswitch_data_14
    .packed-switch 0x1
        :pswitch_7
        :pswitch_d
    .end packed-switch
.end method

.method private isLandscapeOrSeascape(I)Z
    .registers 3
    .parameter "rotation"

    #@0
    .prologue
    .line 7402
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@2
    if-eq p1, v0, :cond_8

    #@4
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSeascapeRotation:I

    #@6
    if-ne p1, v0, :cond_a

    #@8
    :cond_8
    const/4 v0, 0x1

    #@9
    :goto_9
    return v0

    #@a
    :cond_a
    const/4 v0, 0x0

    #@b
    goto :goto_9
.end method

.method private isMvpOnTop()Z
    .registers 9

    #@0
    .prologue
    const/4 v5, 0x1

    #@1
    const/4 v6, 0x0

    #@2
    .line 8435
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@4
    const-string v7, "activity"

    #@6
    invoke-virtual {v4, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/app/ActivityManager;

    #@c
    .line 8436
    .local v0, aMgr:Landroid/app/ActivityManager;
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@e
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@11
    move-result-object v1

    #@12
    .line 8437
    .local v1, pMgr:Landroid/content/pm/PackageManager;
    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    #@15
    move-result-object v3

    #@16
    .line 8439
    .local v3, tasks:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v3, :cond_36

    #@18
    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@1b
    move-result-object v4

    #@1c
    check-cast v4, Landroid/app/ActivityManager$RunningTaskInfo;

    #@1e
    iget-object v4, v4, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    #@20
    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@23
    move-result-object v2

    #@24
    .line 8443
    .local v2, packageName:Ljava/lang/String;
    :goto_24
    const-string v4, "com.vmware.mvp.permission.GRAB_HOME_KEY"

    #@26
    invoke-virtual {v1, v4, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    move-result v4

    #@2a
    if-nez v4, :cond_39

    #@2c
    const-string v4, "com.vmware"

    #@2e
    invoke-virtual {v2, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    #@31
    move-result v4

    #@32
    if-eqz v4, :cond_39

    #@34
    move v4, v5

    #@35
    .line 8447
    :goto_35
    return v4

    #@36
    .line 8439
    .end local v2           #packageName:Ljava/lang/String;
    :cond_36
    const-string v2, ""

    #@38
    goto :goto_24

    #@39
    .restart local v2       #packageName:Ljava/lang/String;
    :cond_39
    move v4, v6

    #@3a
    .line 8447
    goto :goto_35
.end method

.method private isVolumeLongKeyAvailable(Z)Z
    .registers 5
    .parameter "isScreenOn"

    #@0
    .prologue
    .line 1201
    const/4 v0, 0x0

    #@1
    .line 1202
    .local v0, result:Z
    if-eqz p1, :cond_1b

    #@3
    if-eqz p1, :cond_32

    #@5
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->keyguardIsShowingTq()Z

    #@8
    move-result v1

    #@9
    if-eqz v1, :cond_32

    #@b
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@d
    if-eqz v1, :cond_32

    #@f
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@11
    invoke-interface {v1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@14
    move-result-object v1

    #@15
    iget v1, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@17
    const/16 v2, 0x7d4

    #@19
    if-ne v1, v2, :cond_32

    #@1b
    :cond_1b
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverClosed:Z

    #@1d
    if-nez v1, :cond_32

    #@1f
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isDeviceProvisioned()Z

    #@22
    move-result v1

    #@23
    if-eqz v1, :cond_32

    #@25
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@27
    invoke-static {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->isSimPinSecure()Z

    #@2e
    move-result v1

    #@2f
    if-nez v1, :cond_32

    #@31
    .line 1207
    const/4 v0, 0x1

    #@32
    .line 1209
    :cond_32
    return v0
.end method

.method private isWakeKeyWhenScreenOff(I)Z
    .registers 5
    .parameter "keyCode"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    const/4 v1, 0x0

    #@2
    .line 6808
    sparse-switch p1, :sswitch_data_e

    #@5
    .line 6830
    :cond_5
    :goto_5
    return v0

    #@6
    .line 6813
    :sswitch_6
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@8
    if-nez v2, :cond_5

    #@a
    move v0, v1

    #@b
    goto :goto_5

    #@c
    :sswitch_c
    move v0, v1

    #@d
    .line 6828
    goto :goto_5

    #@e
    .line 6808
    :sswitch_data_e
    .sparse-switch
        0x18 -> :sswitch_6
        0x19 -> :sswitch_6
        0x1b -> :sswitch_c
        0x4f -> :sswitch_c
        0x55 -> :sswitch_c
        0x56 -> :sswitch_c
        0x57 -> :sswitch_c
        0x58 -> :sswitch_c
        0x59 -> :sswitch_c
        0x5a -> :sswitch_c
        0x5b -> :sswitch_c
        0x7e -> :sswitch_c
        0x7f -> :sswitch_c
        0x82 -> :sswitch_c
        0xa4 -> :sswitch_6
    .end sparse-switch
.end method

.method private keyguardIsShowingTq()Z
    .registers 2

    #@0
    .prologue
    .line 7160
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 7161
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowingAndNotHidden()Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method private launchAssistAction()V
    .registers 6

    #@0
    .prologue
    const/4 v4, -0x2

    #@1
    .line 4175
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@3
    const-string v3, "search"

    #@5
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@8
    move-result-object v2

    #@9
    check-cast v2, Landroid/app/SearchManager;

    #@b
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@d
    invoke-virtual {v2, v3, v4}, Landroid/app/SearchManager;->getAssistIntent(Landroid/content/Context;I)Landroid/content/Intent;

    #@10
    move-result-object v1

    #@11
    .line 4177
    .local v1, intent:Landroid/content/Intent;
    if-eqz v1, :cond_23

    #@13
    .line 4178
    const/high16 v2, 0x3400

    #@15
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@18
    .line 4182
    :try_start_18
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1a
    new-instance v3, Landroid/os/UserHandle;

    #@1c
    const/4 v4, -0x2

    #@1d
    invoke-direct {v3, v4}, Landroid/os/UserHandle;-><init>(I)V

    #@20
    invoke-virtual {v2, v1, v3}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_23
    .catch Landroid/content/ActivityNotFoundException; {:try_start_18 .. :try_end_23} :catch_24

    #@23
    .line 4187
    :cond_23
    :goto_23
    return-void

    #@24
    .line 4183
    :catch_24
    move-exception v0

    #@25
    .line 4184
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v2, "WindowManager"

    #@27
    const-string v3, "No activity to handle assist action."

    #@29
    invoke-static {v2, v3, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@2c
    goto :goto_23
.end method

.method private launchAssistLongPressAction()V
    .registers 7

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 4153
    const/4 v3, 0x0

    #@2
    invoke-virtual {p0, v3, v4, v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    #@5
    .line 4154
    const-string v3, "assist"

    #@7
    invoke-virtual {p0, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendCloseSystemWindows(Ljava/lang/String;)V

    #@a
    .line 4157
    new-instance v1, Landroid/content/Intent;

    #@c
    const-string v3, "android.intent.action.SEARCH_LONG_PRESS"

    #@e
    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@11
    .line 4158
    .local v1, intent:Landroid/content/Intent;
    const/high16 v3, 0x1000

    #@13
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@16
    .line 4162
    :try_start_16
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getSearchManager()Landroid/app/SearchManager;

    #@19
    move-result-object v2

    #@1a
    .line 4163
    .local v2, searchManager:Landroid/app/SearchManager;
    if-eqz v2, :cond_1f

    #@1c
    .line 4164
    invoke-virtual {v2}, Landroid/app/SearchManager;->stopSearch()V

    #@1f
    .line 4166
    :cond_1f
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@21
    new-instance v4, Landroid/os/UserHandle;

    #@23
    const/4 v5, -0x2

    #@24
    invoke-direct {v4, v5}, Landroid/os/UserHandle;-><init>(I)V

    #@27
    invoke-virtual {v3, v1, v4}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_2a
    .catch Landroid/content/ActivityNotFoundException; {:try_start_16 .. :try_end_2a} :catch_2b

    #@2a
    .line 4170
    .end local v2           #searchManager:Landroid/app/SearchManager;
    :goto_2a
    return-void

    #@2b
    .line 4167
    :catch_2b
    move-exception v0

    #@2c
    .line 4168
    .local v0, e:Landroid/content/ActivityNotFoundException;
    const-string v3, "WindowManager"

    #@2e
    const-string v4, "No activity to handle assist long press action."

    #@30
    invoke-static {v3, v4, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@33
    goto :goto_2a
.end method

.method private launchEasyAccess()V
    .registers 3

    #@0
    .prologue
    .line 6107
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.lge.accessibility.action.EASY_ACCESS_SHOW_CATEGORY"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 6108
    .local v0, intent:Landroid/content/Intent;
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@9
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@c
    .line 6109
    return-void
.end method

.method private launchVolumeDownLongPressAction()V
    .registers 5

    #@0
    .prologue
    .line 1280
    const/4 v0, 0x0

    #@1
    .line 1281
    .local v0, cameraIntent:Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isKeyguardSecure()Z

    #@4
    move-result v1

    #@5
    if-eqz v1, :cond_2d

    #@7
    .line 1282
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->SECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@9
    .line 1286
    :goto_9
    const/high16 v1, 0x3400

    #@b
    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@e
    .line 1290
    const-string v1, "com.lge.camera.launchingPath"

    #@10
    const/4 v2, 0x4

    #@11
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@14
    .line 1291
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@16
    new-instance v2, Landroid/os/UserHandle;

    #@18
    const/4 v3, -0x2

    #@19
    invoke-direct {v2, v3}, Landroid/os/UserHandle;-><init>(I)V

    #@1c
    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->startActivityAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@1f
    .line 1292
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@21
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@24
    move-result v1

    #@25
    if-eqz v1, :cond_2c

    #@27
    .line 1293
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@29
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@2c
    .line 1295
    :cond_2c
    return-void

    #@2d
    .line 1284
    :cond_2d
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->INSECURE_CAMERA_INTENT:Landroid/content/Intent;

    #@2f
    goto :goto_9
.end method

.method private launchVolumeUpLongPressAction()V
    .registers 4

    #@0
    .prologue
    .line 1298
    new-instance v0, Landroid/content/Intent;

    #@2
    const-string v1, "com.lge.QuickClip.action.TOGGLE_QUICKCLIP"

    #@4
    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@7
    .line 1299
    .local v0, quickclipIntent:Landroid/content/Intent;
    const-string v1, "com.lge.QuickClip.launchingPath"

    #@9
    const/4 v2, 0x4

    #@a
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@d
    .line 1300
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@f
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@12
    .line 1301
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@14
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    #@17
    move-result v1

    #@18
    if-eqz v1, :cond_1f

    #@1a
    .line 1302
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@1c
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@1f
    .line 1304
    :cond_1f
    return-void
.end method

.method private manageFocusedMvpKeys(IIZ)J
    .registers 17
    .parameter "keyCode"
    .parameter "flags"
    .parameter "down"

    #@0
    .prologue
    .line 3277
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotChordEnabled:Z

    #@2
    if-eqz v9, :cond_82

    #@4
    and-int/lit16 v9, p2, 0x400

    #@6
    if-nez v9, :cond_82

    #@8
    .line 3278
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@a
    if-eqz v9, :cond_25

    #@c
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@e
    if-eqz v9, :cond_14

    #@10
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@12
    if-nez v9, :cond_25

    #@14
    .line 3279
    :cond_14
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@17
    move-result-wide v3

    #@18
    .line 3280
    .local v3, now:J
    iget-wide v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTime:J

    #@1a
    const-wide/16 v11, 0x96

    #@1c
    add-long v7, v9, v11

    #@1e
    .line 3281
    .local v7, timeoutTime:J
    cmp-long v9, v3, v7

    #@20
    if-gez v9, :cond_25

    #@22
    .line 3282
    sub-long v9, v7, v3

    #@24
    .line 3368
    .end local v3           #now:J
    .end local v7           #timeoutTime:J
    :goto_24
    return-wide v9

    #@25
    .line 3285
    :cond_25
    const/16 v9, 0x19

    #@27
    if-ne p1, v9, :cond_82

    #@29
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@2b
    if-eqz v9, :cond_82

    #@2d
    .line 3288
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRearSideKeyEnable:Z

    #@2f
    if-eqz v9, :cond_7a

    #@31
    .line 3289
    const/4 v2, 0x0

    #@32
    .line 3290
    .local v2, isOffhook:Z
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@35
    move-result-object v6

    #@36
    .line 3291
    .local v6, telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v6, :cond_3c

    #@38
    .line 3293
    :try_start_38
    invoke-interface {v6}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z
    :try_end_3b
    .catch Landroid/os/RemoteException; {:try_start_38 .. :try_end_3b} :catch_5f

    #@3b
    move-result v2

    #@3c
    .line 3298
    :cond_3c
    :goto_3c
    if-eqz v2, :cond_53

    #@3e
    .line 3299
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@41
    move-result-wide v3

    #@42
    .line 3300
    .restart local v3       #now:J
    iget-wide v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTime:J

    #@44
    const-wide/16 v11, 0x12c

    #@46
    add-long v7, v9, v11

    #@48
    .line 3301
    .restart local v7       #timeoutTime:J
    cmp-long v9, v3, v7

    #@4a
    if-gez v9, :cond_6b

    #@4c
    .line 3302
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@4e
    if-nez v9, :cond_68

    #@50
    .line 3303
    const/4 v9, 0x0

    #@51
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@53
    .line 3312
    .end local v3           #now:J
    .end local v7           #timeoutTime:J
    :cond_53
    :goto_53
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@55
    if-eqz v9, :cond_82

    #@57
    .line 3313
    if-nez p3, :cond_5c

    #@59
    .line 3314
    const/4 v9, 0x0

    #@5a
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@5c
    .line 3316
    :cond_5c
    const-wide/16 v9, -0x1

    #@5e
    goto :goto_24

    #@5f
    .line 3294
    :catch_5f
    move-exception v1

    #@60
    .line 3295
    .local v1, ex:Landroid/os/RemoteException;
    const-string v9, "WindowManager"

    #@62
    const-string v10, "ITelephony threw RemoteException"

    #@64
    invoke-static {v9, v10, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@67
    goto :goto_3c

    #@68
    .line 3305
    .end local v1           #ex:Landroid/os/RemoteException;
    .restart local v3       #now:J
    .restart local v7       #timeoutTime:J
    :cond_68
    sub-long v9, v7, v3

    #@6a
    goto :goto_24

    #@6b
    .line 3307
    :cond_6b
    const-wide/16 v9, 0x96

    #@6d
    add-long/2addr v9, v7

    #@6e
    cmp-long v9, v3, v9

    #@70
    if-gez v9, :cond_53

    #@72
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@74
    if-nez v9, :cond_53

    #@76
    .line 3309
    const/4 v9, 0x0

    #@77
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@79
    goto :goto_53

    #@7a
    .line 3319
    .end local v2           #isOffhook:Z
    .end local v3           #now:J
    .end local v6           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    .end local v7           #timeoutTime:J
    :cond_7a
    if-nez p3, :cond_7f

    #@7c
    .line 3320
    const/4 v9, 0x0

    #@7d
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@7f
    .line 3322
    :cond_7f
    const-wide/16 v9, -0x1

    #@81
    goto :goto_24

    #@82
    .line 3327
    :cond_82
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@84
    if-nez v9, :cond_8a

    #@86
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@88
    if-eqz v9, :cond_ca

    #@8a
    :cond_8a
    and-int/lit16 v9, p2, 0x400

    #@8c
    if-nez v9, :cond_ca

    #@8e
    .line 3328
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@90
    if-eqz v9, :cond_a8

    #@92
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@94
    if-nez v9, :cond_a8

    #@96
    .line 3329
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@99
    move-result-wide v3

    #@9a
    .line 3330
    .restart local v3       #now:J
    iget-wide v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTime:J

    #@9c
    const-wide/16 v11, 0x96

    #@9e
    add-long v7, v9, v11

    #@a0
    .line 3331
    .restart local v7       #timeoutTime:J
    cmp-long v9, v3, v7

    #@a2
    if-gez v9, :cond_a8

    #@a4
    .line 3332
    sub-long v9, v7, v3

    #@a6
    goto/16 :goto_24

    #@a8
    .line 3335
    .end local v3           #now:J
    .end local v7           #timeoutTime:J
    :cond_a8
    const/16 v9, 0x19

    #@aa
    if-ne p1, v9, :cond_b9

    #@ac
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByQuickClipChord:Z

    #@ae
    if-eqz v9, :cond_b9

    #@b0
    .line 3337
    if-nez p3, :cond_b5

    #@b2
    .line 3338
    const/4 v9, 0x0

    #@b3
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByQuickClipChord:Z

    #@b5
    .line 3340
    :cond_b5
    const-wide/16 v9, -0x1

    #@b7
    goto/16 :goto_24

    #@b9
    .line 3342
    :cond_b9
    const/16 v9, 0x18

    #@bb
    if-ne p1, v9, :cond_ca

    #@bd
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedByQuickClipChord:Z

    #@bf
    if-eqz v9, :cond_ca

    #@c1
    .line 3344
    if-nez p3, :cond_c6

    #@c3
    .line 3345
    const/4 v9, 0x0

    #@c4
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedByQuickClipChord:Z

    #@c6
    .line 3347
    :cond_c6
    const-wide/16 v9, -0x1

    #@c8
    goto/16 :goto_24

    #@ca
    .line 3351
    :cond_ca
    const/16 v9, 0x53

    #@cc
    if-ne p1, v9, :cond_dd

    #@ce
    .line 3352
    if-eqz p3, :cond_dd

    #@d0
    .line 3353
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@d3
    move-result-object v5

    #@d4
    .line 3354
    .local v5, sbs:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v5, :cond_dd

    #@d6
    .line 3356
    :try_start_d6
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIsExpanded:Z

    #@d8
    if-nez v9, :cond_e1

    #@da
    .line 3357
    invoke-interface {v5}, Lcom/android/internal/statusbar/IStatusBarService;->expandNotificationsPanel()V

    #@dd
    .line 3368
    .end local v5           #sbs:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_dd
    :goto_dd
    const-wide/16 v9, 0x0

    #@df
    goto/16 :goto_24

    #@e1
    .line 3359
    .restart local v5       #sbs:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_e1
    invoke-interface {v5}, Lcom/android/internal/statusbar/IStatusBarService;->collapsePanels()V
    :try_end_e4
    .catch Landroid/os/RemoteException; {:try_start_d6 .. :try_end_e4} :catch_e5

    #@e4
    goto :goto_dd

    #@e5
    .line 3361
    :catch_e5
    move-exception v0

    #@e6
    .line 3362
    .local v0, e:Landroid/os/RemoteException;
    new-instance v9, Ljava/lang/RuntimeException;

    #@e8
    invoke-direct {v9, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@eb
    throw v9
.end method

.method private manageMvpKeys(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;Ljava/lang/String;)I
    .registers 11
    .parameter "win"
    .parameter "event"
    .parameter "reason"

    #@0
    .prologue
    const/4 v2, -0x1

    #@1
    .line 3241
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isASystemWindow(Landroid/view/WindowManagerPolicy$WindowState;)Z

    #@4
    move-result v3

    #@5
    if-eqz v3, :cond_3a

    #@7
    .line 3242
    sget-object v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->WINDOW_TYPES_WHERE_HOME_DOESNT_WORK:[I

    #@9
    array-length v1, v3

    #@a
    .line 3243
    .local v1, typeCount:I
    const/4 v0, 0x0

    #@b
    .local v0, i:I
    :goto_b
    if-ge v0, v1, :cond_1d

    #@d
    .line 3244
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@10
    move-result-object v3

    #@11
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@13
    sget-object v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->WINDOW_TYPES_WHERE_HOME_DOESNT_WORK:[I

    #@15
    aget v4, v4, v0

    #@17
    if-ne v3, v4, :cond_1a

    #@19
    .line 3262
    .end local v0           #i:I
    .end local v1           #typeCount:I
    :goto_19
    return v2

    #@1a
    .line 3243
    .restart local v0       #i:I
    .restart local v1       #typeCount:I
    :cond_1a
    add-int/lit8 v0, v0, 0x1

    #@1c
    goto :goto_b

    #@1d
    .line 3251
    :cond_1d
    invoke-virtual {p0, p3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendCloseSystemWindows(Ljava/lang/String;)V

    #@20
    .line 3253
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@23
    move-result-wide v3

    #@24
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDownTime()J

    #@27
    move-result-wide v5

    #@28
    sub-long/2addr v3, v5

    #@29
    const-wide/16 v5, 0x258

    #@2b
    cmp-long v3, v3, v5

    #@2d
    if-lez v3, :cond_37

    #@2f
    .line 3254
    const-string v3, "WindowManager"

    #@31
    const-string v4, "Unable to close a system window hiding MVP"

    #@33
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@36
    goto :goto_19

    #@37
    .line 3259
    :cond_37
    const/16 v2, 0x64

    #@39
    goto :goto_19

    #@3a
    .line 3261
    .end local v0           #i:I
    .end local v1           #typeCount:I
    :cond_3a
    const-string v2, "WindowManager"

    #@3c
    const-string v3, "MVP flag is set but it\'s not a system window. Send key to focused window"

    #@3e
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@41
    .line 3262
    const/4 v2, 0x0

    #@42
    goto :goto_19
.end method

.method private modifyLayoutForSplitWindow(Landroid/view/WindowManagerPolicy$WindowState;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/WindowManager$LayoutParams;)V
    .registers 20
    .parameter "win"
    .parameter "parentFrm"
    .parameter "displayFrm"
    .parameter "contentFrm"
    .parameter "visibleFrm"
    .parameter "attrs"

    #@0
    .prologue
    .line 4906
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getSplitScreenId()I

    #@3
    move-result v9

    #@4
    .line 4907
    .local v9, screenId:I
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isSplitFullScreen()Z

    #@7
    move-result v6

    #@8
    .line 4909
    .local v6, isFullScreenMode:Z
    if-eqz v9, :cond_d

    #@a
    const/4 v10, 0x1

    #@b
    if-ne v6, v10, :cond_e

    #@d
    .line 5052
    :cond_d
    :goto_d
    return-void

    #@e
    .line 4912
    :cond_e
    const/4 v8, 0x0

    #@f
    .line 4915
    .local v8, scrInfo:Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;
    iget-object v10, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@11
    if-nez v10, :cond_16

    #@13
    .line 4916
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getSplitWindowManagerInstance()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@16
    .line 4919
    :cond_16
    :try_start_16
    iget-object v10, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@18
    invoke-interface {v10, v9}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getScreenInfo(I)Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;

    #@1b
    move-result-object v8

    #@1c
    .line 4920
    iget-object v10, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@1e
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@21
    move-result-object v11

    #@22
    iget-object v11, v11, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@24
    invoke-interface {v10, v11}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->getMinimumScreenSize(Ljava/lang/String;)Landroid/graphics/Rect;
    :try_end_27
    .catchall {:try_start_16 .. :try_end_27} :catchall_83
    .catch Landroid/os/RemoteException; {:try_start_16 .. :try_end_27} :catch_6d
    .catch Ljava/lang/NullPointerException; {:try_start_16 .. :try_end_27} :catch_78

    #@27
    move-result-object v2

    #@28
    .line 4930
    .local v2, MinAppRect:Landroid/graphics/Rect;
    if-eqz v8, :cond_d

    #@2a
    .line 4934
    invoke-interface {v8}, Lcom/lge/loader/splitwindow/ISplitWindow$IScreenInfo;->getScreenRect()Landroid/graphics/Rect;

    #@2d
    move-result-object v3

    #@2e
    .line 4936
    .local v3, ScrRect:Landroid/graphics/Rect;
    move-object/from16 v0, p6

    #@30
    iget v10, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@32
    const/high16 v11, 0x1000

    #@34
    and-int/2addr v10, v11

    #@35
    if-nez v10, :cond_47

    #@37
    invoke-virtual/range {p6 .. p6}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@3a
    move-result-object v10

    #@3b
    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    #@3e
    move-result-object v10

    #@3f
    const-string v11, "com.android.internal.app"

    #@41
    invoke-virtual {v10, v11}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    #@44
    move-result v10

    #@45
    if-eqz v10, :cond_87

    #@47
    :cond_47
    const/4 v7, 0x1

    #@48
    .line 4938
    .local v7, layOverSplitBarNeeded:Z
    :goto_48
    if-eqz v7, :cond_b1

    #@4a
    .line 4939
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getFrameLw()Landroid/graphics/Rect;

    #@4d
    move-result-object v5

    #@4e
    .line 4940
    .local v5, frameRect:Landroid/graphics/Rect;
    iget v10, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenWidth:I

    #@50
    iget v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenHeight:I

    #@52
    if-ge v10, v11, :cond_91

    #@54
    .line 4943
    iget v10, v5, Landroid/graphics/Rect;->bottom:I

    #@56
    iget v11, v5, Landroid/graphics/Rect;->top:I

    #@58
    sub-int/2addr v10, v11

    #@59
    iget v11, v3, Landroid/graphics/Rect;->bottom:I

    #@5b
    iget v12, v3, Landroid/graphics/Rect;->top:I

    #@5d
    sub-int/2addr v11, v12

    #@5e
    if-lt v10, v11, :cond_b1

    #@60
    .line 4945
    iget v10, p2, Landroid/graphics/Rect;->top:I

    #@62
    iget v11, v3, Landroid/graphics/Rect;->top:I

    #@64
    if-ne v10, v11, :cond_89

    #@66
    .line 4947
    const/16 v10, 0x30

    #@68
    move-object/from16 v0, p6

    #@6a
    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@6c
    goto :goto_d

    #@6d
    .line 4921
    .end local v2           #MinAppRect:Landroid/graphics/Rect;
    .end local v3           #ScrRect:Landroid/graphics/Rect;
    .end local v5           #frameRect:Landroid/graphics/Rect;
    .end local v7           #layOverSplitBarNeeded:Z
    :catch_6d
    move-exception v4

    #@6e
    .line 4922
    .local v4, e:Landroid/os/RemoteException;
    :try_start_6e
    const-string v10, "SplitWindow"

    #@70
    const-string v11, "Binder service (SplitWindowPolicyService) is not available"

    #@72
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 4930
    if-nez v8, :cond_d

    #@77
    goto :goto_d

    #@78
    .line 4925
    .end local v4           #e:Landroid/os/RemoteException;
    :catch_78
    move-exception v4

    #@79
    .line 4926
    .local v4, e:Ljava/lang/NullPointerException;
    const-string v10, "SplitWindow"

    #@7b
    const-string v11, "SplitWindowPolicyService is not created well.. check service routine"

    #@7d
    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_80
    .catchall {:try_start_6e .. :try_end_80} :catchall_83

    #@80
    .line 4930
    if-nez v8, :cond_d

    #@82
    goto :goto_d

    #@83
    .end local v4           #e:Ljava/lang/NullPointerException;
    :catchall_83
    move-exception v10

    #@84
    if-eqz v8, :cond_d

    #@86
    .line 4931
    throw v10

    #@87
    .line 4936
    .restart local v2       #MinAppRect:Landroid/graphics/Rect;
    .restart local v3       #ScrRect:Landroid/graphics/Rect;
    :cond_87
    const/4 v7, 0x0

    #@88
    goto :goto_48

    #@89
    .line 4949
    .restart local v5       #frameRect:Landroid/graphics/Rect;
    .restart local v7       #layOverSplitBarNeeded:Z
    :cond_89
    const/16 v10, 0x50

    #@8b
    move-object/from16 v0, p6

    #@8d
    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@8f
    goto/16 :goto_d

    #@91
    .line 4956
    :cond_91
    iget v10, v5, Landroid/graphics/Rect;->right:I

    #@93
    iget v11, v5, Landroid/graphics/Rect;->left:I

    #@95
    sub-int/2addr v10, v11

    #@96
    iget v11, v3, Landroid/graphics/Rect;->right:I

    #@98
    iget v12, v3, Landroid/graphics/Rect;->left:I

    #@9a
    sub-int/2addr v11, v12

    #@9b
    if-lt v10, v11, :cond_b1

    #@9d
    .line 4958
    iget v10, p2, Landroid/graphics/Rect;->left:I

    #@9f
    iget v11, v3, Landroid/graphics/Rect;->left:I

    #@a1
    if-ne v10, v11, :cond_aa

    #@a3
    .line 4960
    const/4 v10, 0x3

    #@a4
    move-object/from16 v0, p6

    #@a6
    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@a8
    goto/16 :goto_d

    #@aa
    .line 4962
    :cond_aa
    const/4 v10, 0x5

    #@ab
    move-object/from16 v0, p6

    #@ad
    iput v10, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@af
    goto/16 :goto_d

    #@b1
    .line 4971
    .end local v5           #frameRect:Landroid/graphics/Rect;
    :cond_b1
    iget v10, p2, Landroid/graphics/Rect;->bottom:I

    #@b3
    iget v11, p2, Landroid/graphics/Rect;->right:I

    #@b5
    if-le v10, v11, :cond_110

    #@b7
    .line 4974
    invoke-virtual {p2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@ba
    .line 4975
    move-object/from16 v0, p4

    #@bc
    invoke-virtual {v0, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@bf
    .line 4976
    move-object/from16 v0, p3

    #@c1
    invoke-virtual {v0, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@c4
    .line 4979
    iget v10, p2, Landroid/graphics/Rect;->top:I

    #@c6
    iget v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@c8
    if-ne v10, v11, :cond_f1

    #@ca
    .line 4981
    if-eqz v2, :cond_e8

    #@cc
    iget v10, v2, Landroid/graphics/Rect;->bottom:I

    #@ce
    iget v11, p2, Landroid/graphics/Rect;->bottom:I

    #@d0
    iget v12, p2, Landroid/graphics/Rect;->top:I

    #@d2
    sub-int/2addr v11, v12

    #@d3
    if-le v10, v11, :cond_e8

    #@d5
    .line 4992
    iget v10, p2, Landroid/graphics/Rect;->bottom:I

    #@d7
    iget v11, v2, Landroid/graphics/Rect;->bottom:I

    #@d9
    sub-int/2addr v10, v11

    #@da
    move-object/from16 v0, p4

    #@dc
    iput v10, v0, Landroid/graphics/Rect;->top:I

    #@de
    .line 4993
    move-object/from16 v0, p4

    #@e0
    iget v10, v0, Landroid/graphics/Rect;->top:I

    #@e2
    move-object/from16 v0, p3

    #@e4
    iput v10, v0, Landroid/graphics/Rect;->top:I

    #@e6
    iput v10, p2, Landroid/graphics/Rect;->top:I

    #@e8
    .line 5051
    :cond_e8
    :goto_e8
    move-object/from16 v0, p5

    #@ea
    move-object/from16 v1, p4

    #@ec
    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@ef
    goto/16 :goto_d

    #@f1
    .line 4998
    :cond_f1
    if-eqz v2, :cond_e8

    #@f3
    iget v10, v2, Landroid/graphics/Rect;->bottom:I

    #@f5
    iget v11, p2, Landroid/graphics/Rect;->bottom:I

    #@f7
    iget v12, p2, Landroid/graphics/Rect;->top:I

    #@f9
    sub-int/2addr v11, v12

    #@fa
    if-le v10, v11, :cond_e8

    #@fc
    .line 5007
    iget v10, p2, Landroid/graphics/Rect;->top:I

    #@fe
    iget v11, v2, Landroid/graphics/Rect;->bottom:I

    #@100
    add-int/2addr v10, v11

    #@101
    move-object/from16 v0, p4

    #@103
    iput v10, v0, Landroid/graphics/Rect;->bottom:I

    #@105
    .line 5008
    move-object/from16 v0, p4

    #@107
    iget v10, v0, Landroid/graphics/Rect;->bottom:I

    #@109
    move-object/from16 v0, p3

    #@10b
    iput v10, v0, Landroid/graphics/Rect;->bottom:I

    #@10d
    iput v10, p2, Landroid/graphics/Rect;->bottom:I

    #@10f
    goto :goto_e8

    #@110
    .line 5014
    :cond_110
    invoke-virtual {p2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@113
    .line 5015
    move-object/from16 v0, p4

    #@115
    invoke-virtual {v0, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@118
    .line 5016
    move-object/from16 v0, p3

    #@11a
    invoke-virtual {v0, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@11d
    .line 5019
    iget v10, p2, Landroid/graphics/Rect;->left:I

    #@11f
    iget v11, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@121
    if-ne v10, v11, :cond_149

    #@123
    .line 5021
    if-eqz v2, :cond_e8

    #@125
    iget v10, v2, Landroid/graphics/Rect;->right:I

    #@127
    iget v11, p2, Landroid/graphics/Rect;->right:I

    #@129
    iget v12, p2, Landroid/graphics/Rect;->left:I

    #@12b
    sub-int/2addr v11, v12

    #@12c
    if-le v10, v11, :cond_e8

    #@12e
    .line 5029
    const-string v10, "SplitWindow"

    #@130
    const-string v11, "This is push-away case1.."

    #@132
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@135
    .line 5030
    iget v10, p2, Landroid/graphics/Rect;->right:I

    #@137
    iget v11, v2, Landroid/graphics/Rect;->right:I

    #@139
    sub-int/2addr v10, v11

    #@13a
    move-object/from16 v0, p4

    #@13c
    iput v10, v0, Landroid/graphics/Rect;->left:I

    #@13e
    .line 5031
    move-object/from16 v0, p4

    #@140
    iget v10, v0, Landroid/graphics/Rect;->left:I

    #@142
    move-object/from16 v0, p3

    #@144
    iput v10, v0, Landroid/graphics/Rect;->left:I

    #@146
    iput v10, p2, Landroid/graphics/Rect;->left:I

    #@148
    goto :goto_e8

    #@149
    .line 5036
    :cond_149
    if-eqz v2, :cond_e8

    #@14b
    iget v10, v2, Landroid/graphics/Rect;->right:I

    #@14d
    iget v11, p2, Landroid/graphics/Rect;->right:I

    #@14f
    iget v12, p2, Landroid/graphics/Rect;->left:I

    #@151
    sub-int/2addr v11, v12

    #@152
    if-le v10, v11, :cond_e8

    #@154
    .line 5044
    const-string v10, "SplitWindow"

    #@156
    const-string v11, "This is push-away case2.."

    #@158
    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@15b
    .line 5045
    iget v10, p2, Landroid/graphics/Rect;->left:I

    #@15d
    iget v11, v2, Landroid/graphics/Rect;->right:I

    #@15f
    add-int/2addr v10, v11

    #@160
    move-object/from16 v0, p4

    #@162
    iput v10, v0, Landroid/graphics/Rect;->right:I

    #@164
    .line 5046
    move-object/from16 v0, p4

    #@166
    iget v10, v0, Landroid/graphics/Rect;->right:I

    #@168
    move-object/from16 v0, p3

    #@16a
    iput v10, v0, Landroid/graphics/Rect;->right:I

    #@16c
    iput v10, p2, Landroid/graphics/Rect;->right:I

    #@16e
    goto/16 :goto_e8
.end method

.method private modifySubPanelLayoutForSplitWindow(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 17
    .parameter "win"
    .parameter "attached"
    .parameter "attrs"
    .parameter "parentFrm"
    .parameter "displayFrm"
    .parameter "contentFrm"
    .parameter "visibleFrm"

    #@0
    .prologue
    .line 4854
    invoke-interface {p2}, Landroid/view/WindowManagerPolicy$WindowState;->getSplitScreenId()I

    #@3
    move-result v8

    #@4
    .line 4855
    .local v8, screenId:I
    invoke-interface {p2}, Landroid/view/WindowManagerPolicy$WindowState;->isSplitFullScreen()Z

    #@7
    move-result v7

    #@8
    .line 4857
    .local v7, isFullScreenMode:Z
    if-eqz v8, :cond_d

    #@a
    const/4 v0, 0x1

    #@b
    if-ne v7, v0, :cond_e

    #@d
    .line 4903
    :cond_d
    :goto_d
    return-void

    #@e
    .line 4860
    :cond_e
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@10
    const/16 v1, 0x3e9

    #@12
    if-eq v0, v1, :cond_d

    #@14
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@16
    const/16 v1, 0x3ec

    #@18
    if-eq v0, v1, :cond_d

    #@1a
    .line 4871
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@1c
    const/high16 v1, 0x1000

    #@1e
    and-int/2addr v0, v1

    #@1f
    if-eqz v0, :cond_53

    #@21
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@23
    const/16 v1, 0x3eb

    #@25
    if-ne v0, v1, :cond_53

    #@27
    .line 4875
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@29
    iput v0, p6, Landroid/graphics/Rect;->left:I

    #@2b
    iput v0, p5, Landroid/graphics/Rect;->left:I

    #@2d
    iput v0, p4, Landroid/graphics/Rect;->left:I

    #@2f
    .line 4876
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@31
    iput v0, p6, Landroid/graphics/Rect;->top:I

    #@33
    iput v0, p5, Landroid/graphics/Rect;->top:I

    #@35
    iput v0, p4, Landroid/graphics/Rect;->top:I

    #@37
    .line 4877
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@39
    iput v0, p6, Landroid/graphics/Rect;->right:I

    #@3b
    iput v0, p5, Landroid/graphics/Rect;->right:I

    #@3d
    iput v0, p4, Landroid/graphics/Rect;->right:I

    #@3f
    .line 4878
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@41
    iput v0, p6, Landroid/graphics/Rect;->bottom:I

    #@43
    iput v0, p5, Landroid/graphics/Rect;->bottom:I

    #@45
    iput v0, p4, Landroid/graphics/Rect;->bottom:I

    #@47
    move-object v0, p0

    #@48
    move-object v1, p1

    #@49
    move-object v2, p4

    #@4a
    move-object v3, p5

    #@4b
    move-object v4, p6

    #@4c
    move-object/from16 v5, p7

    #@4e
    move-object v6, p3

    #@4f
    .line 4879
    invoke-direct/range {v0 .. v6}, Lcom/android/internal/policy/impl/PhoneWindowManager;->modifyLayoutForSplitWindow(Landroid/view/WindowManagerPolicy$WindowState;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/WindowManager$LayoutParams;)V

    #@52
    goto :goto_d

    #@53
    .line 4883
    :cond_53
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@55
    const/16 v1, 0x3e8

    #@57
    if-eq v0, v1, :cond_65

    #@59
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@5b
    const/16 v1, 0x3ea

    #@5d
    if-eq v0, v1, :cond_65

    #@5f
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@61
    const/16 v1, 0x3eb

    #@63
    if-ne v0, v1, :cond_d

    #@65
    .line 4889
    :cond_65
    iget v0, p4, Landroid/graphics/Rect;->bottom:I

    #@67
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@69
    if-ne v0, v1, :cond_79

    #@6b
    .line 4890
    iget v0, p4, Landroid/graphics/Rect;->top:I

    #@6d
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@6f
    if-eq v0, v1, :cond_79

    #@71
    .line 4891
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@73
    iput v0, p6, Landroid/graphics/Rect;->top:I

    #@75
    iput v0, p5, Landroid/graphics/Rect;->top:I

    #@77
    iput v0, p4, Landroid/graphics/Rect;->top:I

    #@79
    .line 4896
    :cond_79
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@7b
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@7d
    sub-int/2addr v0, v1

    #@7e
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@80
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@82
    sub-int/2addr v1, v2

    #@83
    if-le v0, v1, :cond_d

    #@85
    iget v0, p3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@87
    const/16 v1, 0x3eb

    #@89
    if-eq v0, v1, :cond_d

    #@8b
    .line 4897
    iget v0, p4, Landroid/graphics/Rect;->left:I

    #@8d
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@8f
    if-ne v0, v1, :cond_9b

    #@91
    .line 4898
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@93
    iput v0, p6, Landroid/graphics/Rect;->right:I

    #@95
    iput v0, p5, Landroid/graphics/Rect;->right:I

    #@97
    iput v0, p4, Landroid/graphics/Rect;->right:I

    #@99
    goto/16 :goto_d

    #@9b
    .line 4899
    :cond_9b
    iget v0, p4, Landroid/graphics/Rect;->right:I

    #@9d
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@9f
    if-ne v0, v1, :cond_d

    #@a1
    .line 4900
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@a3
    iput v0, p6, Landroid/graphics/Rect;->left:I

    #@a5
    iput v0, p5, Landroid/graphics/Rect;->left:I

    #@a7
    iput v0, p4, Landroid/graphics/Rect;->left:I

    #@a9
    goto/16 :goto_d
.end method

.method private offsetInputMethodWindowLw(Landroid/view/WindowManagerPolicy$WindowState;)V
    .registers 4
    .parameter "win"

    #@0
    .prologue
    .line 5429
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getContentFrameLw()Landroid/graphics/Rect;

    #@3
    move-result-object v1

    #@4
    iget v0, v1, Landroid/graphics/Rect;->top:I

    #@6
    .line 5430
    .local v0, top:I
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getGivenContentInsetsLw()Landroid/graphics/Rect;

    #@9
    move-result-object v1

    #@a
    iget v1, v1, Landroid/graphics/Rect;->top:I

    #@c
    add-int/2addr v0, v1

    #@d
    .line 5431
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentBottom:I

    #@f
    if-le v1, v0, :cond_13

    #@11
    .line 5432
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentBottom:I

    #@13
    .line 5434
    :cond_13
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getVisibleFrameLw()Landroid/graphics/Rect;

    #@16
    move-result-object v1

    #@17
    iget v0, v1, Landroid/graphics/Rect;->top:I

    #@19
    .line 5435
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getGivenVisibleInsetsLw()Landroid/graphics/Rect;

    #@1c
    move-result-object v1

    #@1d
    iget v1, v1, Landroid/graphics/Rect;->top:I

    #@1f
    add-int/2addr v0, v1

    #@20
    .line 5436
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurBottom:I

    #@22
    if-le v1, v0, :cond_26

    #@24
    .line 5437
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurBottom:I

    #@26
    .line 5442
    :cond_26
    return-void
.end method

.method private performAuditoryFeedbackForAccessibilityIfNeed()V
    .registers 5

    #@0
    .prologue
    .line 7774
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isGlobalAccessibilityGestureEnabled()Z

    #@3
    move-result v2

    #@4
    if-nez v2, :cond_7

    #@6
    .line 7786
    :cond_6
    :goto_6
    return-void

    #@7
    .line 7777
    :cond_7
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@9
    const-string v3, "audio"

    #@b
    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@e
    move-result-object v0

    #@f
    check-cast v0, Landroid/media/AudioManager;

    #@11
    .line 7779
    .local v0, audioManager:Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isSilentMode()Z

    #@14
    move-result v2

    #@15
    if-nez v2, :cond_6

    #@17
    .line 7782
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@19
    sget-object v3, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    #@1b
    invoke-static {v2, v3}, Landroid/media/RingtoneManager;->getRingtone(Landroid/content/Context;Landroid/net/Uri;)Landroid/media/Ringtone;

    #@1e
    move-result-object v1

    #@1f
    .line 7784
    .local v1, ringTone:Landroid/media/Ringtone;
    const/4 v2, 0x3

    #@20
    invoke-virtual {v1, v2}, Landroid/media/Ringtone;->setStreamType(I)V

    #@23
    .line 7785
    invoke-virtual {v1}, Landroid/media/Ringtone;->play()V

    #@26
    goto :goto_6
.end method

.method private readRotation(I)I
    .registers 4
    .parameter "resID"

    #@0
    .prologue
    .line 2594
    :try_start_0
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5
    move-result-object v1

    #@6
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_9
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_9} :catch_17

    #@9
    move-result v0

    #@a
    .line 2595
    .local v0, rotation:I
    sparse-switch v0, :sswitch_data_1a

    #@d
    .line 2608
    .end local v0           #rotation:I
    :goto_d
    const/4 v1, -0x1

    #@e
    :goto_e
    return v1

    #@f
    .line 2597
    .restart local v0       #rotation:I
    :sswitch_f
    const/4 v1, 0x0

    #@10
    goto :goto_e

    #@11
    .line 2599
    :sswitch_11
    const/4 v1, 0x1

    #@12
    goto :goto_e

    #@13
    .line 2601
    :sswitch_13
    const/4 v1, 0x2

    #@14
    goto :goto_e

    #@15
    .line 2603
    :sswitch_15
    const/4 v1, 0x3

    #@16
    goto :goto_e

    #@17
    .line 2605
    .end local v0           #rotation:I
    :catch_17
    move-exception v1

    #@18
    goto :goto_d

    #@19
    .line 2595
    nop

    #@1a
    :sswitch_data_1a
    .sparse-switch
        0x0 -> :sswitch_f
        0x5a -> :sswitch_11
        0xb4 -> :sswitch_13
        0x10e -> :sswitch_15
    .end sparse-switch
.end method

.method private registerKeepHidingNavInputEventReceiver()V
    .registers 4

    #@0
    .prologue
    .line 4408
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@2
    if-eqz v0, :cond_25

    #@4
    .line 4409
    const-string v0, "WindowManager"

    #@6
    new-instance v1, Ljava/lang/StringBuilder;

    #@8
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@b
    const-string v2, "receiver is not null("

    #@d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    const-string v2, ").. skip creating a new one"

    #@19
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v1

    #@1d
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v1

    #@21
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    .line 4415
    :goto_24
    return-void

    #@25
    .line 4412
    :cond_25
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@27
    const-string v1, "KeepHidingNavInputEventReceiver"

    #@29
    invoke-interface {v0, v1}, Landroid/view/WindowManagerPolicy$WindowManagerFuncs;->monitorInput(Ljava/lang/String;)Landroid/view/InputChannel;

    #@2c
    move-result-object v0

    #@2d
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputChannel:Landroid/view/InputChannel;

    #@2f
    .line 4413
    new-instance v0, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@31
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputChannel:Landroid/view/InputChannel;

    #@33
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    #@36
    move-result-object v2

    #@37
    invoke-direct {v0, p0, v1, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/view/InputChannel;Landroid/os/Looper;)V

    #@3a
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@3c
    .line 4414
    const-string v0, "WindowManager"

    #@3e
    new-instance v1, Ljava/lang/StringBuilder;

    #@40
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@43
    const-string v2, "registered KeepHidingNavInputEventReceiver of "

    #@45
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@48
    move-result-object v1

    #@49
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeepHidingNavInputEventReceiver:Lcom/android/internal/policy/impl/PhoneWindowManager$KeepHidingNavInputEventReceiver;

    #@4b
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@4e
    move-result-object v1

    #@4f
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@52
    move-result-object v1

    #@53
    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@56
    goto :goto_24
.end method

.method static sendCloseSystemWindows(Landroid/content/Context;Ljava/lang/String;)V
    .registers 3
    .parameter "context"
    .parameter "reason"

    #@0
    .prologue
    .line 7207
    invoke-static {}, Landroid/app/ActivityManagerNative;->isSystemReady()Z

    #@3
    move-result v0

    #@4
    if-eqz v0, :cond_d

    #@6
    .line 7209
    :try_start_6
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@9
    move-result-object v0

    #@a
    invoke-interface {v0, p1}, Landroid/app/IActivityManager;->closeSystemDialogs(Ljava/lang/String;)V
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_d} :catch_e

    #@d
    .line 7213
    :cond_d
    :goto_d
    return-void

    #@e
    .line 7210
    :catch_e
    move-exception v0

    #@f
    goto :goto_d
.end method

.method private final sendIntent(IIILjava/lang/String;)V
    .registers 11
    .parameter "headset"
    .parameter "headsetState"
    .parameter "prevHeadsetState"
    .parameter "headsetName"

    #@0
    .prologue
    .line 5895
    and-int v3, p2, p1

    #@2
    and-int v4, p3, p1

    #@4
    if-eq v3, v4, :cond_29

    #@6
    .line 5897
    const/4 v2, 0x0

    #@7
    .line 5898
    .local v2, state:I
    const/4 v1, 0x0

    #@8
    .line 5901
    .local v1, device:I
    and-int/lit8 v3, p1, 0x1

    #@a
    if-eqz v3, :cond_2a

    #@c
    .line 5902
    const/4 v1, 0x4

    #@d
    .line 5910
    :goto_d
    and-int v3, p2, p1

    #@f
    if-eqz v3, :cond_4a

    #@11
    .line 5911
    const/4 v2, 0x1

    #@12
    .line 5916
    :goto_12
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemBooted:Z

    #@14
    if-eqz v3, :cond_72

    #@16
    .line 5917
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@18
    const-string v4, "audio"

    #@1a
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@1d
    move-result-object v0

    #@1e
    check-cast v0, Landroid/media/AudioManager;

    #@20
    .line 5918
    .local v0, am:Landroid/media/AudioManager;
    if-nez v0, :cond_4c

    #@22
    .line 5919
    const-string v3, "WindowManager"

    #@24
    const-string v4, "sendIntent: couldn\'t get AudioManager reference"

    #@26
    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    .line 5928
    .end local v0           #am:Landroid/media/AudioManager;
    .end local v1           #device:I
    .end local v2           #state:I
    :cond_29
    :goto_29
    return-void

    #@2a
    .line 5903
    .restart local v1       #device:I
    .restart local v2       #state:I
    :cond_2a
    and-int/lit8 v3, p1, 0x2

    #@2c
    if-eqz v3, :cond_31

    #@2e
    .line 5904
    const/16 v1, 0x8

    #@30
    goto :goto_d

    #@31
    .line 5906
    :cond_31
    const-string v3, "WindowManager"

    #@33
    new-instance v4, Ljava/lang/StringBuilder;

    #@35
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@38
    const-string v5, "setDeviceState() invalid headset type: "

    #@3a
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@3d
    move-result-object v4

    #@3e
    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@41
    move-result-object v4

    #@42
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    invoke-static {v3, v4}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@49
    goto :goto_29

    #@4a
    .line 5913
    :cond_4a
    const/4 v2, 0x0

    #@4b
    goto :goto_12

    #@4c
    .line 5922
    .restart local v0       #am:Landroid/media/AudioManager;
    :cond_4c
    const-string v3, "WindowManager"

    #@4e
    new-instance v4, Ljava/lang/StringBuilder;

    #@50
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@53
    const-string v5, "sendIntent(): device "

    #@55
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@58
    move-result-object v4

    #@59
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v4

    #@5d
    const-string v5, " state "

    #@5f
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@62
    move-result-object v4

    #@63
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v4

    #@67
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@6a
    move-result-object v4

    #@6b
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@6e
    .line 5923
    invoke-virtual {v0, v1, v2, p4}, Landroid/media/AudioManager;->setWiredDeviceConnectionState(IILjava/lang/String;)V

    #@71
    goto :goto_29

    #@72
    .line 5925
    .end local v0           #am:Landroid/media/AudioManager;
    :cond_72
    const-string v3, "WindowManager"

    #@74
    const-string v4, "system not booted yet, call setWiredDeviceConnectionState later"

    #@76
    invoke-static {v3, v4}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@79
    goto :goto_29
.end method

.method private final declared-synchronized sendIntents(Ljava/lang/String;)V
    .registers 10
    .parameter "headsetName"

    #@0
    .prologue
    const/4 v7, 0x3

    #@1
    .line 5854
    monitor-enter p0

    #@2
    :try_start_2
    const-string v4, "WindowManager"

    #@4
    new-instance v5, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v6, "sendIntents(): mHeadsetJackState "

    #@b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v5

    #@f
    sget v6, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetJackState:I

    #@11
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@14
    move-result-object v5

    #@15
    const-string v6, " mCurHeadsetState "

    #@17
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v5

    #@1b
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurHeadsetState:I

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    const-string v6, " mPrevHeadsetState "

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPrevHeadsetState:I

    #@29
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2c
    move-result-object v5

    #@2d
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@30
    move-result-object v5

    #@31
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@34
    .line 5857
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetLock:Ljava/lang/Object;

    #@36
    monitor-enter v5
    :try_end_37
    .catchall {:try_start_2 .. :try_end_37} :catchall_8c

    #@37
    .line 5858
    :try_start_37
    sget v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetJackState:I

    #@39
    and-int/lit8 v3, v4, 0x3

    #@3b
    .line 5859
    .local v3, headsetState:I
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurHeadsetState:I

    #@3d
    if-ne v4, v3, :cond_66

    #@3f
    .line 5860
    const-string v4, "WindowManager"

    #@41
    new-instance v6, Ljava/lang/StringBuilder;

    #@43
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@46
    const-string v7, "sendIntents(): mCurHeadsetState "

    #@48
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4b
    move-result-object v6

    #@4c
    iget v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurHeadsetState:I

    #@4e
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@51
    move-result-object v6

    #@52
    const-string v7, " headsetState "

    #@54
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@57
    move-result-object v6

    #@58
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v6

    #@5c
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5f
    move-result-object v6

    #@60
    invoke-static {v4, v6}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@63
    .line 5861
    monitor-exit v5
    :try_end_64
    .catchall {:try_start_37 .. :try_end_64} :catchall_89

    #@64
    .line 5891
    :cond_64
    monitor-exit p0

    #@65
    return-void

    #@66
    .line 5863
    :cond_66
    :try_start_66
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurHeadsetState:I

    #@68
    iput v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPrevHeadsetState:I

    #@6a
    .line 5864
    iput v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurHeadsetState:I

    #@6c
    .line 5865
    monitor-exit v5
    :try_end_6d
    .catchall {:try_start_66 .. :try_end_6d} :catchall_89

    #@6d
    .line 5866
    const/4 v0, 0x3

    #@6e
    .line 5868
    .local v0, allHeadsets:I
    const/4 v1, 0x1

    #@6f
    .local v1, curHeadset:I
    :goto_6f
    if-ge v1, v7, :cond_8f

    #@71
    .line 5869
    and-int v4, v3, v1

    #@73
    if-ne v4, v1, :cond_86

    #@75
    :try_start_75
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPrevHeadsetState:I

    #@77
    and-int/2addr v4, v1

    #@78
    if-nez v4, :cond_86

    #@7a
    .line 5870
    and-int v4, v1, v0

    #@7c
    if-eqz v4, :cond_86

    #@7e
    .line 5871
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPrevHeadsetState:I

    #@80
    invoke-direct {p0, v1, v3, v4, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendIntent(IIILjava/lang/String;)V
    :try_end_83
    .catchall {:try_start_75 .. :try_end_83} :catchall_8c

    #@83
    .line 5872
    xor-int/lit8 v4, v1, -0x1

    #@85
    and-int/2addr v0, v4

    #@86
    .line 5868
    :cond_86
    shl-int/lit8 v1, v1, 0x1

    #@88
    goto :goto_6f

    #@89
    .line 5865
    .end local v0           #allHeadsets:I
    .end local v1           #curHeadset:I
    .end local v3           #headsetState:I
    :catchall_89
    move-exception v4

    #@8a
    :try_start_8a
    monitor-exit v5
    :try_end_8b
    .catchall {:try_start_8a .. :try_end_8b} :catchall_89

    #@8b
    :try_start_8b
    throw v4
    :try_end_8c
    .catchall {:try_start_8b .. :try_end_8c} :catchall_8c

    #@8c
    .line 5854
    :catchall_8c
    move-exception v4

    #@8d
    monitor-exit p0

    #@8e
    throw v4

    #@8f
    .line 5878
    .restart local v0       #allHeadsets:I
    .restart local v1       #curHeadset:I
    .restart local v3       #headsetState:I
    :cond_8f
    const-wide/16 v4, 0x14

    #@91
    :try_start_91
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_94
    .catchall {:try_start_91 .. :try_end_94} :catchall_8c
    .catch Ljava/lang/Exception; {:try_start_91 .. :try_end_94} :catch_af

    #@94
    .line 5883
    :goto_94
    const/4 v1, 0x1

    #@95
    :goto_95
    if-ge v1, v7, :cond_64

    #@97
    .line 5884
    and-int v4, v3, v1

    #@99
    if-nez v4, :cond_ac

    #@9b
    :try_start_9b
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPrevHeadsetState:I

    #@9d
    and-int/2addr v4, v1

    #@9e
    if-ne v4, v1, :cond_ac

    #@a0
    .line 5885
    and-int v4, v1, v0

    #@a2
    if-eqz v4, :cond_ac

    #@a4
    .line 5886
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPrevHeadsetState:I

    #@a6
    invoke-direct {p0, v1, v3, v4, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendIntent(IIILjava/lang/String;)V

    #@a9
    .line 5887
    xor-int/lit8 v4, v1, -0x1

    #@ab
    and-int/2addr v0, v4

    #@ac
    .line 5883
    :cond_ac
    shl-int/lit8 v1, v1, 0x1

    #@ae
    goto :goto_95

    #@af
    .line 5879
    :catch_af
    move-exception v2

    #@b0
    .line 5880
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_b3
    .catchall {:try_start_9b .. :try_end_b3} :catchall_8c

    #@b3
    goto :goto_94
.end method

.method private setProximitySensorEnabled(Z)V
    .registers 8
    .parameter "enable"

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    .line 8492
    if-eqz p1, :cond_27

    #@3
    .line 8493
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorEnabled:Z

    #@5
    if-nez v0, :cond_26

    #@7
    .line 8494
    const-string v0, "WindowManager"

    #@9
    const-string v1, "Proximity Regi S"

    #@b
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@e
    .line 8495
    const/4 v0, 0x1

    #@f
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorEnabled:Z

    #@11
    .line 8496
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSensorManager:Landroid/hardware/SensorManager;

    #@13
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorListener:Landroid/hardware/SensorEventListener;

    #@15
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensor:Landroid/hardware/Sensor;

    #@17
    const/4 v3, 0x3

    #@18
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@1a
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;ILandroid/os/Handler;)Z

    #@1d
    .line 8498
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximityNear:Z

    #@1f
    .line 8499
    const-string v0, "WindowManager"

    #@21
    const-string v1, "Proximity Regi E"

    #@23
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@26
    .line 8509
    :cond_26
    :goto_26
    return-void

    #@27
    .line 8502
    :cond_27
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorEnabled:Z

    #@29
    if-eqz v0, :cond_26

    #@2b
    .line 8503
    const-string v0, "WindowManager"

    #@2d
    const-string v1, "Proximity Un-Regi S"

    #@2f
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 8504
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorEnabled:Z

    #@34
    .line 8505
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSensorManager:Landroid/hardware/SensorManager;

    #@36
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorListener:Landroid/hardware/SensorEventListener;

    #@38
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    #@3b
    .line 8506
    const-string v0, "WindowManager"

    #@3d
    const-string v1, "Proximity Un-Regi E"

    #@3f
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@42
    goto :goto_26
.end method

.method private shouldKeepHidingNavOnTouchDown()Z
    .registers 4

    #@0
    .prologue
    .line 4356
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@2
    if-eqz v2, :cond_15

    #@4
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@6
    invoke-interface {v2}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@9
    move-result-object v0

    #@a
    .line 4357
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    :goto_a
    if-eqz v0, :cond_17

    #@c
    .line 4358
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@e
    .line 4359
    .local v1, extend:I
    const/high16 v2, 0x40

    #@10
    and-int/2addr v2, v1

    #@11
    if-eqz v2, :cond_17

    #@13
    .line 4360
    const/4 v2, 0x1

    #@14
    .line 4363
    .end local v1           #extend:I
    :goto_14
    return v2

    #@15
    .line 4356
    .end local v0           #attrs:Landroid/view/WindowManager$LayoutParams;
    :cond_15
    const/4 v0, 0x0

    #@16
    goto :goto_a

    #@17
    .line 4363
    .restart local v0       #attrs:Landroid/view/WindowManager$LayoutParams;
    :cond_17
    const/4 v2, 0x0

    #@18
    goto :goto_14
.end method

.method private showNavigationBar()V
    .registers 8

    #@0
    .prologue
    .line 4367
    const/4 v0, 0x0

    #@1
    .line 4368
    .local v0, changed:Z
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@3
    monitor-enter v3

    #@4
    .line 4374
    :try_start_4
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mResettingSystemUiFlags:I

    #@6
    or-int/lit8 v2, v2, 0x2

    #@8
    or-int/lit8 v2, v2, 0x1

    #@a
    or-int/lit8 v1, v2, 0x4

    #@c
    .line 4378
    .local v1, newVal:I
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mResettingSystemUiFlags:I

    #@e
    if-eq v2, v1, :cond_13

    #@10
    .line 4379
    iput v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mResettingSystemUiFlags:I

    #@12
    .line 4380
    const/4 v0, 0x1

    #@13
    .line 4385
    :cond_13
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceClearedSystemUiFlags:I

    #@15
    or-int/lit8 v1, v2, 0x2

    #@17
    .line 4387
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceClearedSystemUiFlags:I

    #@19
    if-eq v2, v1, :cond_2a

    #@1b
    .line 4388
    iput v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceClearedSystemUiFlags:I

    #@1d
    .line 4389
    const/4 v0, 0x1

    #@1e
    .line 4390
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@20
    new-instance v4, Lcom/android/internal/policy/impl/PhoneWindowManager$18;

    #@22
    invoke-direct {v4, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$18;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@25
    const-wide/16 v5, 0x3e8

    #@27
    invoke-virtual {v2, v4, v5, v6}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@2a
    .line 4401
    :cond_2a
    monitor-exit v3
    :try_end_2b
    .catchall {:try_start_4 .. :try_end_2b} :catchall_33

    #@2b
    .line 4402
    if-eqz v0, :cond_32

    #@2d
    .line 4403
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@2f
    invoke-interface {v2}, Landroid/view/WindowManagerPolicy$WindowManagerFuncs;->reevaluateStatusBarVisibility()V

    #@32
    .line 4405
    :cond_32
    return-void

    #@33
    .line 4401
    .end local v1           #newVal:I
    :catchall_33
    move-exception v2

    #@34
    :try_start_34
    monitor-exit v3
    :try_end_35
    .catchall {:try_start_34 .. :try_end_35} :catchall_33

    #@35
    throw v2
.end method

.method private takeScreenshot()V
    .registers 12

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 6017
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotLock:Ljava/lang/Object;

    #@3
    monitor-enter v7

    #@4
    .line 6019
    :try_start_4
    const-string v6, "ro.lge.b2b.vmware"

    #@6
    const/4 v8, 0x0

    #@7
    invoke-static {v6, v8}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@a
    move-result v6

    #@b
    if-eqz v6, :cond_2f

    #@d
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isMvpOnTop()Z

    #@10
    move-result v6

    #@11
    if-eqz v6, :cond_2f

    #@13
    .line 6020
    new-instance v4, Landroid/content/Intent;

    #@15
    const-string v6, "lge.intent.action.toast"

    #@17
    invoke-direct {v4, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1a
    .line 6021
    .local v4, intent:Landroid/content/Intent;
    const-string v6, "text"

    #@1c
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1e
    const v9, 0x209002d

    #@21
    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    #@24
    move-result-object v8

    #@25
    invoke-virtual {v4, v6, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@28
    .line 6022
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2a
    invoke-virtual {v6, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@2d
    .line 6023
    monitor-exit v7

    #@2e
    .line 6102
    .end local v4           #intent:Landroid/content/Intent;
    :goto_2e
    return-void

    #@2f
    .line 6028
    :cond_2f
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@32
    move-result-object v6

    #@33
    if-eqz v6, :cond_57

    #@35
    .line 6029
    const/4 v5, 0x0

    #@36
    .line 6031
    .local v5, rv:I
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@39
    move-result-object v6

    #@3a
    const/4 v8, 0x0

    #@3b
    invoke-interface {v6, v8}, Lcom/lge/cappuccino/IMdm;->checkScreenCapture(Landroid/content/ComponentName;)I

    #@3e
    move-result v5

    #@3f
    .line 6032
    if-eqz v5, :cond_57

    #@41
    .line 6033
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@44
    move-result-object v6

    #@45
    const v8, 0x209002d

    #@48
    invoke-interface {v6, v8}, Lcom/lge/cappuccino/IMdm;->sendToastMessageId(I)V

    #@4b
    .line 6035
    const-string v6, "WindowManager"

    #@4d
    const-string v8, "LGMDM Block ScreenCapture"

    #@4f
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@52
    .line 6036
    monitor-exit v7

    #@53
    goto :goto_2e

    #@54
    .line 6101
    .end local v5           #rv:I
    :catchall_54
    move-exception v6

    #@55
    monitor-exit v7
    :try_end_56
    .catchall {:try_start_4 .. :try_end_56} :catchall_54

    #@56
    throw v6

    #@57
    .line 6042
    :cond_57
    :try_start_57
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@59
    if-eqz v6, :cond_61

    #@5b
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@5d
    invoke-interface {v6}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@60
    move-result-object v0

    #@61
    .line 6043
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    :cond_61
    if-eqz v0, :cond_73

    #@63
    .line 6044
    iget v3, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@65
    .line 6045
    .local v3, extend:I
    const/high16 v6, 0x2

    #@67
    and-int/2addr v6, v3

    #@68
    if-eqz v6, :cond_73

    #@6a
    .line 6046
    const-string v6, "WindowManager"

    #@6c
    const-string v8, "Blocking ScreenCapture"

    #@6e
    invoke-static {v6, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@71
    .line 6047
    monitor-exit v7

    #@72
    goto :goto_2e

    #@73
    .line 6052
    .end local v3           #extend:I
    :cond_73
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotConnection:Landroid/content/ServiceConnection;

    #@75
    if-eqz v6, :cond_79

    #@77
    .line 6053
    monitor-exit v7

    #@78
    goto :goto_2e

    #@79
    .line 6055
    :cond_79
    new-instance v1, Landroid/content/ComponentName;

    #@7b
    const-string v6, "com.android.systemui"

    #@7d
    const-string v8, "com.android.systemui.screenshot.TakeScreenshotService"

    #@7f
    invoke-direct {v1, v6, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    #@82
    .line 6057
    .local v1, cn:Landroid/content/ComponentName;
    new-instance v4, Landroid/content/Intent;

    #@84
    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    #@87
    .line 6058
    .restart local v4       #intent:Landroid/content/Intent;
    invoke-virtual {v4, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    #@8a
    .line 6059
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindowManager$25;

    #@8c
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$25;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@8f
    .line 6096
    .local v2, conn:Landroid/content/ServiceConnection;
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@91
    const/4 v8, 0x1

    #@92
    const/4 v9, -0x2

    #@93
    invoke-virtual {v6, v4, v2, v8, v9}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;II)Z

    #@96
    move-result v6

    #@97
    if-eqz v6, :cond_a4

    #@99
    .line 6098
    iput-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotConnection:Landroid/content/ServiceConnection;

    #@9b
    .line 6099
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@9d
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotTimeout:Ljava/lang/Runnable;

    #@9f
    const-wide/16 v9, 0x2710

    #@a1
    invoke-virtual {v6, v8, v9, v10}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@a4
    .line 6101
    :cond_a4
    monitor-exit v7
    :try_end_a5
    .catchall {:try_start_57 .. :try_end_a5} :catchall_54

    #@a5
    goto :goto_2e
.end method

.method private toggleQuickClipFromHotKey()V
    .registers 11

    #@0
    .prologue
    const/4 v9, 0x0

    #@1
    .line 1497
    const-string v6, "true"

    #@3
    const-string v7, "gsm.lge.ota_is_running"

    #@5
    const-string v8, "false"

    #@7
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@a
    move-result-object v7

    #@b
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@e
    move-result v6

    #@f
    if-nez v6, :cond_21

    #@11
    const-string v6, "true"

    #@13
    const-string v7, "gsm.lge.ota_ignoreKey"

    #@15
    const-string v8, "false"

    #@17
    invoke-static {v7, v8}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@1a
    move-result-object v7

    #@1b
    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1e
    move-result v6

    #@1f
    if-eqz v6, :cond_29

    #@21
    .line 1499
    :cond_21
    const-string v6, "WindowManager"

    #@23
    const-string v7, "OTA key ignore!"

    #@25
    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@28
    .line 1516
    :goto_28
    return-void

    #@29
    .line 1502
    :cond_29
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2b
    const-string v7, "activity"

    #@2d
    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@30
    move-result-object v0

    #@31
    check-cast v0, Landroid/app/ActivityManager;

    #@33
    .line 1503
    .local v0, am:Landroid/app/ActivityManager;
    const/4 v6, 0x1

    #@34
    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    #@37
    move-result-object v3

    #@38
    .line 1504
    .local v3, tasks:Ljava/util/List;,"Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v3, :cond_84

    #@3a
    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@3d
    move-result-object v6

    #@3e
    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    #@40
    iget-object v6, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    #@42
    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    #@45
    move-result-object v4

    #@46
    .line 1505
    .local v4, topActivity:Ljava/lang/String;
    :goto_46
    if-eqz v3, :cond_87

    #@48
    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    #@4b
    move-result-object v6

    #@4c
    check-cast v6, Landroid/app/ActivityManager$RunningTaskInfo;

    #@4e
    iget-object v6, v6, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    #@50
    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    #@53
    move-result-object v5

    #@54
    .line 1507
    .local v5, topPackage:Ljava/lang/String;
    :goto_54
    const-string v6, "com.lge.camera.CameraApp"

    #@56
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@59
    move-result v6

    #@5a
    if-nez v6, :cond_74

    #@5c
    const-string v6, "com.lge.camera.CameraAppLauncher"

    #@5e
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@61
    move-result v6

    #@62
    if-nez v6, :cond_74

    #@64
    const-string v6, "com.lge.camera.Camcorder"

    #@66
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@69
    move-result v6

    #@6a
    if-nez v6, :cond_74

    #@6c
    const-string v6, "com.lge.vmemo"

    #@6e
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@71
    move-result v6

    #@72
    if-eqz v6, :cond_8a

    #@74
    .line 1509
    :cond_74
    new-instance v1, Landroid/content/Intent;

    #@76
    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    #@79
    .line 1510
    .local v1, intent:Landroid/content/Intent;
    const-string v6, "com.lge.systemui.qmemo"

    #@7b
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@7e
    .line 1511
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@80
    invoke-virtual {v6, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@83
    goto :goto_28

    #@84
    .line 1504
    .end local v1           #intent:Landroid/content/Intent;
    .end local v4           #topActivity:Ljava/lang/String;
    .end local v5           #topPackage:Ljava/lang/String;
    :cond_84
    const-string v4, ""

    #@86
    goto :goto_46

    #@87
    .line 1505
    .restart local v4       #topActivity:Ljava/lang/String;
    :cond_87
    const-string v5, ""

    #@89
    goto :goto_54

    #@8a
    .line 1513
    .restart local v5       #topPackage:Ljava/lang/String;
    :cond_8a
    new-instance v2, Landroid/content/Intent;

    #@8c
    const-string v6, "com.lge.QuickClip.action.TOGGLE_QUICKCLIP"

    #@8e
    invoke-direct {v2, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@91
    .line 1514
    .local v2, quickclipIntent:Landroid/content/Intent;
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@93
    invoke-virtual {v6, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@96
    goto :goto_28
.end method

.method private final update()V
    .registers 7

    #@0
    .prologue
    .line 5835
    sget v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetJackState:I

    #@2
    and-int/lit8 v1, v2, 0x3

    #@4
    .line 5837
    .local v1, headsetState:I
    const/16 v0, 0x64

    #@6
    .line 5841
    .local v0, delay:I
    iget v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurHeadsetState:I

    #@8
    if-ne v2, v1, :cond_2f

    #@a
    .line 5842
    const-string v2, "WindowManager"

    #@c
    new-instance v3, Ljava/lang/StringBuilder;

    #@e
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@11
    const-string v4, "update(): mCurHeadsetState "

    #@13
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v3

    #@17
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurHeadsetState:I

    #@19
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v3

    #@1d
    const-string v4, " headsetState "

    #@1f
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v3

    #@23
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@26
    move-result-object v3

    #@27
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2a
    move-result-object v3

    #@2b
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2e
    .line 5850
    :goto_2e
    return-void

    #@2f
    .line 5846
    :cond_2f
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@31
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@34
    .line 5847
    const-string v2, "WindowManager"

    #@36
    new-instance v3, Ljava/lang/StringBuilder;

    #@38
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@3b
    const-string v4, "update(): sending Message to IntentHander with delay of "

    #@3d
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@40
    move-result-object v3

    #@41
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@44
    move-result-object v3

    #@45
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@48
    move-result-object v3

    #@49
    invoke-static {v2, v3}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@4c
    .line 5848
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIntentHandler:Landroid/os/Handler;

    #@4e
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIntentHandler:Landroid/os/Handler;

    #@50
    const/4 v4, 0x0

    #@51
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetName:Ljava/lang/String;

    #@53
    invoke-virtual {v3, v4, v5}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@56
    move-result-object v3

    #@57
    int-to-long v4, v0

    #@58
    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@5b
    goto :goto_2e
.end method

.method private updateLockScreenTimeout()V
    .registers 7

    #@0
    .prologue
    .line 7620
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@2
    monitor-enter v2

    #@3
    .line 7621
    :try_start_3
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAllowLockscreenWhenOn:Z

    #@5
    if-eqz v1, :cond_2c

    #@7
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnEarly:Z

    #@9
    if-eqz v1, :cond_2c

    #@b
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@d
    if-eqz v1, :cond_2c

    #@f
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@11
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@14
    move-result v1

    #@15
    if-eqz v1, :cond_2c

    #@17
    const/4 v0, 0x1

    #@18
    .line 7623
    .local v0, enable:Z
    :goto_18
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLockScreenTimerActive:Z

    #@1a
    if-eq v1, v0, :cond_2a

    #@1c
    .line 7624
    if-eqz v0, :cond_2e

    #@1e
    .line 7626
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@20
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@22
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLockScreenTimeout:I

    #@24
    int-to-long v4, v4

    #@25
    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@28
    .line 7631
    :goto_28
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLockScreenTimerActive:Z

    #@2a
    .line 7633
    :cond_2a
    monitor-exit v2

    #@2b
    .line 7634
    return-void

    #@2c
    .line 7621
    .end local v0           #enable:Z
    :cond_2c
    const/4 v0, 0x0

    #@2d
    goto :goto_18

    #@2e
    .line 7629
    .restart local v0       #enable:Z
    :cond_2e
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@30
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@32
    invoke-virtual {v1, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@35
    goto :goto_28

    #@36
    .line 7633
    .end local v0           #enable:Z
    :catchall_36
    move-exception v1

    #@37
    monitor-exit v2
    :try_end_38
    .catchall {:try_start_3 .. :try_end_38} :catchall_36

    #@38
    throw v1
.end method

.method private updateSystemUiVisibilityLw()I
    .registers 8

    #@0
    .prologue
    const/4 v4, 0x0

    #@1
    .line 7900
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@3
    if-nez v5, :cond_7

    #@5
    move v0, v4

    #@6
    .line 7942
    :goto_6
    return v0

    #@7
    .line 7903
    :cond_7
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@9
    invoke-interface {v5}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@c
    move-result-object v5

    #@d
    iget v5, v5, Landroid/view/WindowManager$LayoutParams;->type:I

    #@f
    const/16 v6, 0x7d4

    #@11
    if-ne v5, v6, :cond_1a

    #@13
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideLockScreen:Z

    #@15
    const/4 v6, 0x1

    #@16
    if-ne v5, v6, :cond_1a

    #@18
    move v0, v4

    #@19
    .line 7910
    goto :goto_6

    #@1a
    .line 7912
    :cond_1a
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@1c
    invoke-interface {v5}, Landroid/view/WindowManagerPolicy$WindowState;->getSystemUiVisibility()I

    #@1f
    move-result v5

    #@20
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mResettingSystemUiFlags:I

    #@22
    xor-int/lit8 v6, v6, -0x1

    #@24
    and-int/2addr v5, v6

    #@25
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceClearedSystemUiFlags:I

    #@27
    xor-int/lit8 v6, v6, -0x1

    #@29
    and-int v2, v5, v6

    #@2b
    .line 7915
    .local v2, tmpVisibility:I
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForcingShowNavBar:Z

    #@2d
    if-eqz v5, :cond_3b

    #@2f
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@31
    invoke-interface {v5}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    #@34
    move-result v5

    #@35
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForcingShowNavBarLayer:I

    #@37
    if-ge v5, v6, :cond_3b

    #@39
    .line 7916
    and-int/lit8 v2, v2, -0x10

    #@3b
    .line 7918
    :cond_3b
    move v3, v2

    #@3c
    .line 7919
    .local v3, visibility:I
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastSystemUiFlags:I

    #@3e
    xor-int v0, v3, v5

    #@40
    .line 7920
    .local v0, diff:I
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@42
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@44
    invoke-interface {v5, v6}, Landroid/view/WindowManagerPolicy$WindowState;->getNeedsMenuLw(Landroid/view/WindowManagerPolicy$WindowState;)Z

    #@47
    move-result v1

    #@48
    .line 7921
    .local v1, needsMenu:Z
    if-nez v0, :cond_5a

    #@4a
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastFocusNeedsMenu:Z

    #@4c
    if-ne v5, v1, :cond_5a

    #@4e
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedApp:Landroid/view/IApplicationToken;

    #@50
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@52
    invoke-interface {v6}, Landroid/view/WindowManagerPolicy$WindowState;->getAppToken()Landroid/view/IApplicationToken;

    #@55
    move-result-object v6

    #@56
    if-ne v5, v6, :cond_5a

    #@58
    move v0, v4

    #@59
    .line 7923
    goto :goto_6

    #@5a
    .line 7925
    :cond_5a
    iput v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastSystemUiFlags:I

    #@5c
    .line 7926
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastFocusNeedsMenu:Z

    #@5e
    .line 7927
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@60
    invoke-interface {v4}, Landroid/view/WindowManagerPolicy$WindowState;->getAppToken()Landroid/view/IApplicationToken;

    #@63
    move-result-object v4

    #@64
    iput-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedApp:Landroid/view/IApplicationToken;

    #@66
    .line 7928
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@68
    new-instance v5, Lcom/android/internal/policy/impl/PhoneWindowManager$40;

    #@6a
    invoke-direct {v5, p0, v3, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$40;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;IZ)V

    #@6d
    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@70
    goto :goto_6
.end method

.method private waitForKeyguard(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V
    .registers 4
    .parameter "screenOnListener"

    #@0
    .prologue
    .line 7079
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    if-eqz v0, :cond_1b

    #@4
    .line 7080
    if-eqz p1, :cond_11

    #@6
    .line 7081
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@8
    new-instance v1, Lcom/android/internal/policy/impl/PhoneWindowManager$34;

    #@a
    invoke-direct {v1, p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager$34;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    #@d
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->onScreenTurnedOn(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V

    #@10
    .line 7095
    :goto_10
    return-void

    #@11
    .line 7089
    :cond_11
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@13
    const/4 v1, 0x0

    #@14
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->onScreenTurnedOn(Lcom/android/internal/policy/impl/keyguard/KeyguardViewManager$ShowListener;)V

    #@17
    .line 7094
    :goto_17
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->finishScreenTurningOn(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    #@1a
    goto :goto_10

    #@1b
    .line 7092
    :cond_1b
    const-string v0, "WindowManager"

    #@1d
    const-string v1, "No keyguard mediator!"

    #@1f
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    goto :goto_17
.end method

.method private waitForKeyguardWindowDrawn(Landroid/os/IBinder;Landroid/view/WindowManagerPolicy$ScreenOnListener;)V
    .registers 5
    .parameter "windowToken"
    .parameter "screenOnListener"

    #@0
    .prologue
    .line 7099
    if-eqz p1, :cond_11

    #@2
    .line 7101
    :try_start_2
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManager:Landroid/view/IWindowManager;

    #@4
    new-instance v1, Lcom/android/internal/policy/impl/PhoneWindowManager$35;

    #@6
    invoke-direct {v1, p0, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager$35;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    #@9
    invoke-interface {v0, p1, v1}, Landroid/view/IWindowManager;->waitForWindowDrawn(Landroid/os/IBinder;Landroid/os/IRemoteCallback;)Z
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_c} :catch_10

    #@c
    move-result v0

    #@d
    if-eqz v0, :cond_11

    #@f
    .line 7118
    :goto_f
    return-void

    #@10
    .line 7111
    :catch_10
    move-exception v0

    #@11
    .line 7116
    :cond_11
    const-string v0, "WindowManager"

    #@13
    const-string v1, "No lock screen!"

    #@15
    invoke-static {v0, v1}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@18
    .line 7117
    invoke-direct {p0, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->finishScreenTurningOn(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    #@1b
    goto :goto_f
.end method


# virtual methods
.method public addStartingWindow(Landroid/os/IBinder;Ljava/lang/String;ILandroid/content/res/CompatibilityInfo;Ljava/lang/CharSequence;III)Landroid/view/View;
    .registers 25
    .parameter "appToken"
    .parameter "packageName"
    .parameter "theme"
    .parameter "compatInfo"
    .parameter "nonLocalizedLabel"
    .parameter "labelRes"
    .parameter "icon"
    .parameter "windowFlags"

    #@0
    .prologue
    .line 2943
    if-nez p2, :cond_4

    #@2
    .line 2944
    const/4 v13, 0x0

    #@3
    .line 3053
    :cond_3
    :goto_3
    return-object v13

    #@4
    .line 2948
    :cond_4
    :try_start_4
    move-object/from16 v0, p0

    #@6
    iget-object v8, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@8
    .line 2952
    .local v8, context:Landroid/content/Context;
    invoke-virtual {v8}, Landroid/content/Context;->getThemeResId()I
    :try_end_b
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_4 .. :try_end_b} :catch_113
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_b} :catch_131

    #@b
    move-result v3

    #@c
    move/from16 v0, p3

    #@e
    if-ne v0, v3, :cond_12

    #@10
    if-eqz p6, :cond_1e

    #@12
    .line 2954
    :cond_12
    const/4 v3, 0x0

    #@13
    :try_start_13
    move-object/from16 v0, p2

    #@15
    invoke-virtual {v8, v0, v3}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    #@18
    move-result-object v8

    #@19
    .line 2955
    move/from16 v0, p3

    #@1b
    invoke-virtual {v8, v0}, Landroid/content/Context;->setTheme(I)V
    :try_end_1e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_13 .. :try_end_1e} :catch_14d
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_13 .. :try_end_1e} :catch_113
    .catch Ljava/lang/RuntimeException; {:try_start_13 .. :try_end_1e} :catch_131

    #@1e
    .line 2961
    :cond_1e
    :goto_1e
    :try_start_1e
    invoke-static {v8}, Lcom/android/internal/policy/PolicyManager;->makeNewWindow(Landroid/content/Context;)Landroid/view/Window;

    #@21
    move-result-object v14

    #@22
    .line 2962
    .local v14, win:Landroid/view/Window;
    invoke-virtual {v14}, Landroid/view/Window;->getWindowStyle()Landroid/content/res/TypedArray;

    #@25
    move-result-object v12

    #@26
    .line 2963
    .local v12, ta:Landroid/content/res/TypedArray;
    const/16 v3, 0xc

    #@28
    const/4 v4, 0x0

    #@29
    invoke-virtual {v12, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@2c
    move-result v3

    #@2d
    if-nez v3, :cond_38

    #@2f
    const/16 v3, 0xe

    #@31
    const/4 v4, 0x0

    #@32
    invoke-virtual {v12, v3, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    #@35
    move-result v3

    #@36
    if-eqz v3, :cond_3a

    #@38
    .line 2967
    :cond_38
    const/4 v13, 0x0

    #@39
    goto :goto_3

    #@3a
    .line 2970
    :cond_3a
    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3d
    move-result-object v11

    #@3e
    .line 2971
    .local v11, r:Landroid/content/res/Resources;
    move/from16 v0, p6

    #@40
    move-object/from16 v1, p5

    #@42
    invoke-virtual {v11, v0, v1}, Landroid/content/res/Resources;->getText(ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    #@45
    move-result-object v3

    #@46
    invoke-virtual {v14, v3}, Landroid/view/Window;->setTitle(Ljava/lang/CharSequence;)V

    #@49
    .line 2973
    const/4 v3, 0x3

    #@4a
    invoke-virtual {v14, v3}, Landroid/view/Window;->setType(I)V

    #@4d
    .line 2979
    or-int/lit8 v3, p8, 0x10

    #@4f
    or-int/lit8 v3, v3, 0x8

    #@51
    const/high16 v4, 0x2

    #@53
    or-int/2addr v3, v4

    #@54
    or-int/lit8 v4, p8, 0x10

    #@56
    or-int/lit8 v4, v4, 0x8

    #@58
    const/high16 v5, 0x2

    #@5a
    or-int/2addr v4, v5

    #@5b
    invoke-virtual {v14, v3, v4}, Landroid/view/Window;->setFlags(II)V

    #@5e
    .line 2989
    invoke-virtual/range {p4 .. p4}, Landroid/content/res/CompatibilityInfo;->supportsScreen()Z

    #@61
    move-result v3

    #@62
    if-nez v3, :cond_69

    #@64
    .line 2990
    const/high16 v3, 0x2000

    #@66
    invoke-virtual {v14, v3}, Landroid/view/Window;->addFlags(I)V

    #@69
    .line 2993
    :cond_69
    const/4 v3, -0x1

    #@6a
    const/4 v4, -0x1

    #@6b
    invoke-virtual {v14, v3, v4}, Landroid/view/Window;->setLayout(II)V

    #@6e
    .line 2996
    invoke-virtual {v14}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    #@71
    move-result-object v10

    #@72
    .line 2997
    .local v10, params:Landroid/view/WindowManager$LayoutParams;
    move-object/from16 v0, p1

    #@74
    iput-object v0, v10, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    #@76
    .line 2998
    move-object/from16 v0, p2

    #@78
    iput-object v0, v10, Landroid/view/WindowManager$LayoutParams;->packageName:Ljava/lang/String;

    #@7a
    .line 2999
    invoke-virtual {v14}, Landroid/view/Window;->getWindowStyle()Landroid/content/res/TypedArray;

    #@7d
    move-result-object v3

    #@7e
    const/16 v4, 0x8

    #@80
    const/4 v5, 0x0

    #@81
    invoke-virtual {v3, v4, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    #@84
    move-result v3

    #@85
    iput v3, v10, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    #@87
    .line 3001
    iget v3, v10, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@89
    or-int/lit8 v3, v3, 0x1

    #@8b
    iput v3, v10, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@8d
    .line 3003
    iget v3, v10, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@8f
    or-int/lit8 v3, v3, 0x10

    #@91
    iput v3, v10, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@93
    .line 3004
    new-instance v3, Ljava/lang/StringBuilder;

    #@95
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@98
    const-string v4, "Starting "

    #@9a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9d
    move-result-object v3

    #@9e
    move-object/from16 v0, p2

    #@a0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@a3
    move-result-object v3

    #@a4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@a7
    move-result-object v3

    #@a8
    invoke-virtual {v10, v3}, Landroid/view/WindowManager$LayoutParams;->setTitle(Ljava/lang/CharSequence;)V

    #@ab
    .line 3006
    const-string v3, "window"

    #@ad
    invoke-virtual {v8, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@b0
    move-result-object v15

    #@b1
    check-cast v15, Landroid/view/WindowManager;

    #@b3
    .line 3007
    .local v15, wm:Landroid/view/WindowManager;
    invoke-virtual {v14}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    #@b6
    move-result-object v13

    #@b7
    .line 3009
    .local v13, view:Landroid/view/View;
    invoke-virtual {v14}, Landroid/view/Window;->isFloating()Z

    #@ba
    move-result v3

    #@bb
    if-eqz v3, :cond_c0

    #@bd
    .line 3016
    const/4 v13, 0x0

    #@be
    goto/16 :goto_3

    #@c0
    .line 3020
    :cond_c0
    move-object/from16 v0, p0

    #@c2
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@c4
    const-string v4, "statusbar"

    #@c6
    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@c9
    move-result-object v2

    #@ca
    check-cast v2, Landroid/app/StatusBarManager;

    #@cc
    .line 3021
    .local v2, statusBarManager:Landroid/app/StatusBarManager;
    if-eqz v2, :cond_e8

    #@ce
    const-string v3, "com.lge.camera"

    #@d0
    move-object/from16 v0, p2

    #@d2
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d5
    move-result v3

    #@d6
    if-nez v3, :cond_e8

    #@d8
    .line 3022
    const-string v3, "WindowManager"

    #@da
    const-string v4, "setStatusBarOpaque sent"

    #@dc
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@df
    .line 3023
    const-string v3, "setStatusBarOpaque"

    #@e1
    const/4 v4, 0x0

    #@e2
    const/4 v5, 0x0

    #@e3
    const/4 v6, 0x0

    #@e4
    const/4 v7, 0x0

    #@e5
    invoke-virtual/range {v2 .. v7}, Landroid/app/StatusBarManager;->setNavigationBackground(Ljava/lang/String;IIII)V

    #@e8
    .line 3026
    :cond_e8
    const-string v3, "com.lge.launcher2"

    #@ea
    move-object/from16 v0, p2

    #@ec
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ef
    move-result v3

    #@f0
    if-nez v3, :cond_107

    #@f2
    const-string v3, "com.lge.camera"

    #@f4
    move-object/from16 v0, p2

    #@f6
    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f9
    move-result v3

    #@fa
    if-nez v3, :cond_107

    #@fc
    .line 3027
    if-eqz v2, :cond_107

    #@fe
    .line 3028
    const-string v3, ""

    #@100
    const/4 v4, 0x0

    #@101
    const/4 v5, 0x0

    #@102
    const/4 v6, 0x0

    #@103
    const/4 v7, 0x0

    #@104
    invoke-virtual/range {v2 .. v7}, Landroid/app/StatusBarManager;->setNavigationBackground(Ljava/lang/String;IIII)V

    #@107
    .line 3038
    :cond_107
    invoke-interface {v15, v13, v10}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    #@10a
    .line 3042
    invoke-virtual {v13}, Landroid/view/View;->getParent()Landroid/view/ViewParent;
    :try_end_10d
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_1e .. :try_end_10d} :catch_113
    .catch Ljava/lang/RuntimeException; {:try_start_1e .. :try_end_10d} :catch_131

    #@10d
    move-result-object v3

    #@10e
    if-nez v3, :cond_3

    #@110
    const/4 v13, 0x0

    #@111
    goto/16 :goto_3

    #@113
    .line 3043
    .end local v2           #statusBarManager:Landroid/app/StatusBarManager;
    .end local v8           #context:Landroid/content/Context;
    .end local v10           #params:Landroid/view/WindowManager$LayoutParams;
    .end local v11           #r:Landroid/content/res/Resources;
    .end local v12           #ta:Landroid/content/res/TypedArray;
    .end local v13           #view:Landroid/view/View;
    .end local v14           #win:Landroid/view/Window;
    .end local v15           #wm:Landroid/view/WindowManager;
    :catch_113
    move-exception v9

    #@114
    .line 3045
    .local v9, e:Landroid/view/WindowManager$BadTokenException;
    const-string v3, "WindowManager"

    #@116
    new-instance v4, Ljava/lang/StringBuilder;

    #@118
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@11b
    move-object/from16 v0, p1

    #@11d
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@120
    move-result-object v4

    #@121
    const-string v5, " already running, starting window not displayed"

    #@123
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@126
    move-result-object v4

    #@127
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@12a
    move-result-object v4

    #@12b
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@12e
    .line 3053
    .end local v9           #e:Landroid/view/WindowManager$BadTokenException;
    :goto_12e
    const/4 v13, 0x0

    #@12f
    goto/16 :goto_3

    #@131
    .line 3046
    :catch_131
    move-exception v9

    #@132
    .line 3050
    .local v9, e:Ljava/lang/RuntimeException;
    const-string v3, "WindowManager"

    #@134
    new-instance v4, Ljava/lang/StringBuilder;

    #@136
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@139
    move-object/from16 v0, p1

    #@13b
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@13e
    move-result-object v4

    #@13f
    const-string v5, " failed creating starting window"

    #@141
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@144
    move-result-object v4

    #@145
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@148
    move-result-object v4

    #@149
    invoke-static {v3, v4, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@14c
    goto :goto_12e

    #@14d
    .line 2956
    .end local v9           #e:Ljava/lang/RuntimeException;
    .restart local v8       #context:Landroid/content/Context;
    :catch_14d
    move-exception v3

    #@14e
    goto/16 :goto_1e
.end method

.method public adjustConfigurationLw(Landroid/content/res/Configuration;II)V
    .registers 7
    .parameter "config"
    .parameter "keyboardPresence"
    .parameter "navigationPresence"

    #@0
    .prologue
    const/4 v2, 0x2

    #@1
    const/4 v1, 0x1

    #@2
    .line 2734
    and-int/lit8 v0, p2, 0x1

    #@4
    if-eqz v0, :cond_36

    #@6
    move v0, v1

    #@7
    :goto_7
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHaveBuiltInKeyboard:Z

    #@9
    .line 2736
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->readLidState()V

    #@c
    .line 2737
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->applyLidSwitchState()V

    #@f
    .line 2739
    iget v0, p1, Landroid/content/res/Configuration;->keyboard:I

    #@11
    if-eq v0, v1, :cond_1d

    #@13
    if-ne p2, v1, :cond_25

    #@15
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidKeyboardAccessibility:I

    #@17
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isHidden(I)Z

    #@1a
    move-result v0

    #@1b
    if-eqz v0, :cond_25

    #@1d
    .line 2742
    :cond_1d
    iput v2, p1, Landroid/content/res/Configuration;->hardKeyboardHidden:I

    #@1f
    .line 2743
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSoftInput:Z

    #@21
    if-nez v0, :cond_25

    #@23
    .line 2744
    iput v2, p1, Landroid/content/res/Configuration;->keyboardHidden:I

    #@25
    .line 2748
    :cond_25
    iget v0, p1, Landroid/content/res/Configuration;->navigation:I

    #@27
    if-eq v0, v1, :cond_33

    #@29
    if-ne p3, v1, :cond_35

    #@2b
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidNavigationAccessibility:I

    #@2d
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isHidden(I)Z

    #@30
    move-result v0

    #@31
    if-eqz v0, :cond_35

    #@33
    .line 2751
    :cond_33
    iput v2, p1, Landroid/content/res/Configuration;->navigationHidden:I

    #@35
    .line 2753
    :cond_35
    return-void

    #@36
    .line 2734
    :cond_36
    const/4 v0, 0x0

    #@37
    goto :goto_7
.end method

.method public adjustSystemUiVisibilityLw(I)I
    .registers 4
    .parameter "visibility"

    #@0
    .prologue
    .line 4469
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mResettingSystemUiFlags:I

    #@2
    and-int/2addr v0, p1

    #@3
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mResettingSystemUiFlags:I

    #@5
    .line 4472
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mResettingSystemUiFlags:I

    #@7
    xor-int/lit8 v0, v0, -0x1

    #@9
    and-int/2addr v0, p1

    #@a
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceClearedSystemUiFlags:I

    #@c
    xor-int/lit8 v1, v1, -0x1

    #@e
    and-int/2addr v0, v1

    #@f
    return v0
.end method

.method public adjustWindowParamsLw(Landroid/view/WindowManager$LayoutParams;)V
    .registers 4
    .parameter "attrs"

    #@0
    .prologue
    .line 2700
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2
    sparse-switch v0, :sswitch_data_16

    #@5
    .line 2710
    :goto_5
    return-void

    #@6
    .line 2705
    :sswitch_6
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@8
    or-int/lit8 v0, v0, 0x18

    #@a
    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@c
    .line 2707
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@e
    const v1, -0x40001

    #@11
    and-int/2addr v0, v1

    #@12
    iput v0, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@14
    goto :goto_5

    #@15
    .line 2700
    nop

    #@16
    :sswitch_data_16
    .sparse-switch
        0x7d5 -> :sswitch_6
        0x7d6 -> :sswitch_6
        0x7df -> :sswitch_6
    .end sparse-switch
.end method

.method public allowAppAnimationsLw()Z
    .registers 2

    #@0
    .prologue
    .line 5705
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@2
    if-eqz v0, :cond_16

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@6
    invoke-interface {v0}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_16

    #@c
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@e
    invoke-interface {v0}, Landroid/view/WindowManagerPolicy$WindowState;->isAnimatingLw()Z

    #@11
    move-result v0

    #@12
    if-nez v0, :cond_16

    #@14
    .line 5708
    const/4 v0, 0x0

    #@15
    .line 5710
    :goto_15
    return v0

    #@16
    :cond_16
    const/4 v0, 0x1

    #@17
    goto :goto_15
.end method

.method public applyPostLayoutPolicyLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)V
    .registers 9
    .parameter "win"
    .parameter "attrs"

    #@0
    .prologue
    const/16 v4, 0x7d4

    #@2
    const/4 v5, -0x1

    #@3
    const/4 v2, 0x0

    #@4
    const/4 v1, 0x1

    #@5
    .line 5472
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@7
    if-nez v3, :cond_1f

    #@9
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@c
    move-result-object v3

    #@d
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@f
    and-int/lit8 v3, v3, 0x20

    #@11
    if-eqz v3, :cond_1f

    #@13
    .line 5474
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForcingShowNavBarLayer:I

    #@15
    if-gez v3, :cond_1f

    #@17
    .line 5475
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForcingShowNavBar:Z

    #@19
    .line 5476
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    #@1c
    move-result v3

    #@1d
    iput v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForcingShowNavBarLayer:I

    #@1f
    .line 5479
    :cond_1f
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@21
    if-nez v3, :cond_a2

    #@23
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleOrBehindKeyguardLw()Z

    #@26
    move-result v3

    #@27
    if-eqz v3, :cond_a2

    #@29
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isGoneForLayoutLw()Z

    #@2c
    move-result v3

    #@2d
    if-nez v3, :cond_a2

    #@2f
    .line 5481
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@31
    and-int/lit16 v3, v3, 0x800

    #@33
    if-eqz v3, :cond_3b

    #@35
    .line 5482
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@37
    if-ne v3, v4, :cond_a3

    #@39
    .line 5483
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceStatusBarFromKeyguard:Z

    #@3b
    .line 5488
    :cond_3b
    :goto_3b
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@3d
    if-ne v3, v4, :cond_41

    #@3f
    .line 5489
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShowingLockscreen:Z

    #@41
    .line 5491
    :cond_41
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@43
    if-lt v3, v1, :cond_a6

    #@45
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@47
    const/16 v4, 0x63

    #@49
    if-gt v3, v4, :cond_a6

    #@4b
    move v0, v1

    #@4c
    .line 5493
    .local v0, applyWindow:Z
    :goto_4c
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@4e
    const/16 v4, 0x7e7

    #@50
    if-ne v3, v4, :cond_65

    #@52
    .line 5496
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDreamingLockscreen:Z

    #@54
    if-eqz v3, :cond_62

    #@56
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    #@59
    move-result v3

    #@5a
    if-eqz v3, :cond_65

    #@5c
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->hasDrawnLw()Z

    #@5f
    move-result v3

    #@60
    if-eqz v3, :cond_65

    #@62
    .line 5498
    :cond_62
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShowingDream:Z

    #@64
    .line 5499
    const/4 v0, 0x1

    #@65
    .line 5502
    :cond_65
    if-eqz v0, :cond_a2

    #@67
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->x:I

    #@69
    if-nez v3, :cond_a2

    #@6b
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->y:I

    #@6d
    if-nez v3, :cond_a2

    #@6f
    iget v3, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    #@71
    if-ne v3, v5, :cond_a2

    #@73
    iget v3, p2, Landroid/view/ViewGroup$LayoutParams;->height:I

    #@75
    if-ne v3, v5, :cond_a2

    #@77
    .line 5507
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@79
    .line 5508
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@7b
    const/high16 v4, 0x8

    #@7d
    and-int/2addr v3, v4

    #@7e
    if-eqz v3, :cond_84

    #@80
    .line 5510
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideLockScreen:Z

    #@82
    .line 5511
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceStatusBarFromKeyguard:Z

    #@84
    .line 5513
    :cond_84
    iget v3, p2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@86
    const/high16 v4, 0x40

    #@88
    and-int/2addr v3, v4

    #@89
    if-eqz v3, :cond_9a

    #@8b
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDismissKeyguard:I

    #@8d
    if-nez v3, :cond_9a

    #@8f
    .line 5516
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWinDismissingKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@91
    if-ne v3, p1, :cond_a8

    #@93
    const/4 v3, 0x2

    #@94
    :goto_94
    iput v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDismissKeyguard:I

    #@96
    .line 5518
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWinDismissingKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@98
    .line 5519
    iput-boolean v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceStatusBarFromKeyguard:Z

    #@9a
    .line 5521
    :cond_9a
    iget v2, p2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@9c
    and-int/lit8 v2, v2, 0x1

    #@9e
    if-eqz v2, :cond_a2

    #@a0
    .line 5522
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAllowLockscreenWhenOn:Z

    #@a2
    .line 5526
    .end local v0           #applyWindow:Z
    :cond_a2
    return-void

    #@a3
    .line 5485
    :cond_a3
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceStatusBar:Z

    #@a5
    goto :goto_3b

    #@a6
    :cond_a6
    move v0, v2

    #@a7
    .line 5491
    goto :goto_4c

    #@a8
    .restart local v0       #applyWindow:Z
    :cond_a8
    move v3, v1

    #@a9
    .line 5516
    goto :goto_94
.end method

.method public beginLayoutLw(ZIII)V
    .registers 25
    .parameter "isDefaultDisplay"
    .parameter "displayWidth"
    .parameter "displayHeight"
    .parameter "displayRotation"

    #@0
    .prologue
    .line 4520
    const/4 v3, 0x0

    #@1
    move-object/from16 v0, p0

    #@3
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@5
    move-object/from16 v0, p0

    #@7
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@9
    .line 4521
    move/from16 v0, p2

    #@b
    move-object/from16 v1, p0

    #@d
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@f
    .line 4522
    move/from16 v0, p3

    #@11
    move-object/from16 v1, p0

    #@13
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@15
    .line 4523
    const/4 v3, 0x0

    #@16
    move-object/from16 v0, p0

    #@18
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@1a
    move-object/from16 v0, p0

    #@1c
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@1e
    .line 4524
    move/from16 v0, p2

    #@20
    move-object/from16 v1, p0

    #@22
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenWidth:I

    #@24
    .line 4525
    move/from16 v0, p3

    #@26
    move-object/from16 v1, p0

    #@28
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenHeight:I

    #@2a
    .line 4526
    const/4 v3, 0x0

    #@2b
    move-object/from16 v0, p0

    #@2d
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurLeft:I

    #@2f
    move-object/from16 v0, p0

    #@31
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemLeft:I

    #@33
    move-object/from16 v0, p0

    #@35
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenLeft:I

    #@37
    move-object/from16 v0, p0

    #@39
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableLeft:I

    #@3b
    move-object/from16 v0, p0

    #@3d
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentLeft:I

    #@3f
    move-object/from16 v0, p0

    #@41
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@43
    .line 4528
    const/4 v3, 0x0

    #@44
    move-object/from16 v0, p0

    #@46
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurTop:I

    #@48
    move-object/from16 v0, p0

    #@4a
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemTop:I

    #@4c
    move-object/from16 v0, p0

    #@4e
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenTop:I

    #@50
    move-object/from16 v0, p0

    #@52
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableTop:I

    #@54
    move-object/from16 v0, p0

    #@56
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentTop:I

    #@58
    move-object/from16 v0, p0

    #@5a
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@5c
    .line 4530
    move/from16 v0, p2

    #@5e
    move-object/from16 v1, p0

    #@60
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurRight:I

    #@62
    move/from16 v0, p2

    #@64
    move-object/from16 v1, p0

    #@66
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemRight:I

    #@68
    move/from16 v0, p2

    #@6a
    move-object/from16 v1, p0

    #@6c
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenRight:I

    #@6e
    move/from16 v0, p2

    #@70
    move-object/from16 v1, p0

    #@72
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableRight:I

    #@74
    move/from16 v0, p2

    #@76
    move-object/from16 v1, p0

    #@78
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentRight:I

    #@7a
    move/from16 v0, p2

    #@7c
    move-object/from16 v1, p0

    #@7e
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@80
    .line 4532
    move/from16 v0, p3

    #@82
    move-object/from16 v1, p0

    #@84
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurBottom:I

    #@86
    move/from16 v0, p3

    #@88
    move-object/from16 v1, p0

    #@8a
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemBottom:I

    #@8c
    move/from16 v0, p3

    #@8e
    move-object/from16 v1, p0

    #@90
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenBottom:I

    #@92
    move/from16 v0, p3

    #@94
    move-object/from16 v1, p0

    #@96
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableBottom:I

    #@98
    move/from16 v0, p3

    #@9a
    move-object/from16 v1, p0

    #@9c
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentBottom:I

    #@9e
    move/from16 v0, p3

    #@a0
    move-object/from16 v1, p0

    #@a2
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@a4
    .line 4534
    const/high16 v3, 0x1000

    #@a6
    move-object/from16 v0, p0

    #@a8
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLayer:I

    #@aa
    .line 4535
    const/4 v3, -0x1

    #@ab
    move-object/from16 v0, p0

    #@ad
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarLayer:I

    #@af
    .line 4538
    sget-object v17, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpParentFrame:Landroid/graphics/Rect;

    #@b1
    .line 4539
    .local v17, pf:Landroid/graphics/Rect;
    sget-object v12, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpDisplayFrame:Landroid/graphics/Rect;

    #@b3
    .line 4540
    .local v12, df:Landroid/graphics/Rect;
    sget-object v19, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpVisibleFrame:Landroid/graphics/Rect;

    #@b5
    .line 4541
    .local v19, vf:Landroid/graphics/Rect;
    move-object/from16 v0, p0

    #@b7
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@b9
    move-object/from16 v0, v19

    #@bb
    iput v3, v0, Landroid/graphics/Rect;->left:I

    #@bd
    iput v3, v12, Landroid/graphics/Rect;->left:I

    #@bf
    move-object/from16 v0, v17

    #@c1
    iput v3, v0, Landroid/graphics/Rect;->left:I

    #@c3
    .line 4542
    move-object/from16 v0, p0

    #@c5
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@c7
    move-object/from16 v0, v19

    #@c9
    iput v3, v0, Landroid/graphics/Rect;->top:I

    #@cb
    iput v3, v12, Landroid/graphics/Rect;->top:I

    #@cd
    move-object/from16 v0, v17

    #@cf
    iput v3, v0, Landroid/graphics/Rect;->top:I

    #@d1
    .line 4543
    move-object/from16 v0, p0

    #@d3
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@d5
    move-object/from16 v0, v19

    #@d7
    iput v3, v0, Landroid/graphics/Rect;->right:I

    #@d9
    iput v3, v12, Landroid/graphics/Rect;->right:I

    #@db
    move-object/from16 v0, v17

    #@dd
    iput v3, v0, Landroid/graphics/Rect;->right:I

    #@df
    .line 4544
    move-object/from16 v0, p0

    #@e1
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@e3
    move-object/from16 v0, v19

    #@e5
    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    #@e7
    iput v3, v12, Landroid/graphics/Rect;->bottom:I

    #@e9
    move-object/from16 v0, v17

    #@eb
    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    #@ed
    .line 4546
    if-eqz p1, :cond_2e9

    #@ef
    .line 4549
    move-object/from16 v0, p0

    #@f1
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastSystemUiFlags:I

    #@f3
    and-int/lit8 v3, v3, 0x2

    #@f5
    if-nez v3, :cond_2fa

    #@f7
    const/16 v16, 0x1

    #@f9
    .line 4552
    .local v16, navVisible:Z
    :goto_f9
    move-object/from16 v0, p0

    #@fb
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastSystemUiFlags:I

    #@fd
    and-int/lit8 v3, v3, 0x8

    #@ff
    const/16 v4, 0x8

    #@101
    if-ne v3, v4, :cond_2fe

    #@103
    const/4 v15, 0x1

    #@104
    .line 4556
    .local v15, navHideForce:Z
    :goto_104
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->shouldKeepHidingNavOnTouchDown()Z

    #@107
    move-result v13

    #@108
    .line 4567
    .local v13, keepHidingNav:Z
    move-object/from16 v0, p0

    #@10a
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@10c
    if-eqz v3, :cond_127

    #@10e
    move-object/from16 v0, p0

    #@110
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@112
    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@115
    move-result-object v3

    #@116
    iget v3, v3, Landroid/view/WindowManager$LayoutParams;->type:I

    #@118
    const/16 v4, 0x7d4

    #@11a
    if-ne v3, v4, :cond_127

    #@11c
    move-object/from16 v0, p0

    #@11e
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@120
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isNavigationHidden()Z

    #@123
    move-result v3

    #@124
    if-eqz v3, :cond_127

    #@126
    .line 4569
    const/4 v15, 0x1

    #@127
    .line 4573
    :cond_127
    move-object/from16 v0, p0

    #@129
    iget-boolean v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAATIsRunning:Z

    #@12b
    if-eqz v3, :cond_135

    #@12d
    .line 4574
    const/4 v15, 0x1

    #@12e
    .line 4575
    const-string v3, "WindowManager"

    #@130
    const-string v4, "NavigationBar will be hide "

    #@132
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@135
    .line 4579
    :cond_135
    if-nez v16, :cond_139

    #@137
    if-eqz v15, :cond_301

    #@139
    .line 4580
    :cond_139
    move-object/from16 v0, p0

    #@13b
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideNavFakeWindow:Landroid/view/WindowManagerPolicy$FakeWindow;

    #@13d
    if-eqz v3, :cond_14b

    #@13f
    .line 4581
    move-object/from16 v0, p0

    #@141
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideNavFakeWindow:Landroid/view/WindowManagerPolicy$FakeWindow;

    #@143
    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$FakeWindow;->dismiss()V

    #@146
    .line 4582
    const/4 v3, 0x0

    #@147
    move-object/from16 v0, p0

    #@149
    iput-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideNavFakeWindow:Landroid/view/WindowManagerPolicy$FakeWindow;

    #@14b
    .line 4599
    :cond_14b
    :goto_14b
    if-nez v16, :cond_32b

    #@14d
    if-eqz v13, :cond_32b

    #@14f
    .line 4601
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->registerKeepHidingNavInputEventReceiver()V

    #@152
    .line 4612
    :goto_152
    move-object/from16 v0, p0

    #@154
    iget-boolean v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCanHideNavigationBar:Z

    #@156
    if-nez v3, :cond_330

    #@158
    const/4 v3, 0x1

    #@159
    :goto_159
    or-int v16, v16, v3

    #@15b
    .line 4614
    move-object/from16 v0, p0

    #@15d
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@15f
    if-eqz v3, :cond_203

    #@161
    .line 4619
    move-object/from16 v0, p0

    #@163
    iget-boolean v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarCanMove:Z

    #@165
    if-eqz v3, :cond_16d

    #@167
    move/from16 v0, p2

    #@169
    move/from16 v1, p3

    #@16b
    if-ge v0, v1, :cond_333

    #@16d
    :cond_16d
    const/4 v3, 0x1

    #@16e
    :goto_16e
    move-object/from16 v0, p0

    #@170
    iput-boolean v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarOnBottom:Z

    #@172
    .line 4620
    move-object/from16 v0, p0

    #@174
    iget-boolean v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarOnBottom:Z

    #@176
    if-eqz v3, :cond_361

    #@178
    .line 4622
    move-object/from16 v0, p0

    #@17a
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarHeightForRotation:[I

    #@17c
    aget v3, v3, p4

    #@17e
    sub-int v18, p3, v3

    #@180
    .line 4623
    .local v18, top:I
    sget-object v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@182
    const/4 v4, 0x0

    #@183
    move/from16 v0, v18

    #@185
    move/from16 v1, p2

    #@187
    move/from16 v2, p3

    #@189
    invoke-virtual {v3, v4, v0, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    #@18c
    .line 4624
    sget-object v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@18e
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@190
    move-object/from16 v0, p0

    #@192
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenBottom:I

    #@194
    move-object/from16 v0, p0

    #@196
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableBottom:I

    #@198
    .line 4626
    if-eqz v15, :cond_336

    #@19a
    .line 4628
    move-object/from16 v0, p0

    #@19c
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@19e
    const/4 v4, 0x1

    #@19f
    invoke-interface {v3, v4}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    #@1a2
    .line 4640
    :goto_1a2
    if-nez v15, :cond_1b8

    #@1a4
    .line 4641
    if-eqz v16, :cond_1b8

    #@1a6
    move-object/from16 v0, p0

    #@1a8
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@1aa
    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->isAnimatingLw()Z

    #@1ad
    move-result v3

    #@1ae
    if-nez v3, :cond_1b8

    #@1b0
    .line 4645
    sget-object v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@1b2
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@1b4
    move-object/from16 v0, p0

    #@1b6
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemBottom:I

    #@1b8
    .line 4681
    .end local v18           #top:I
    :cond_1b8
    :goto_1b8
    move-object/from16 v0, p0

    #@1ba
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@1bc
    move-object/from16 v0, p0

    #@1be
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurTop:I

    #@1c0
    move-object/from16 v0, p0

    #@1c2
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentTop:I

    #@1c4
    .line 4682
    move-object/from16 v0, p0

    #@1c6
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@1c8
    move-object/from16 v0, p0

    #@1ca
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurBottom:I

    #@1cc
    move-object/from16 v0, p0

    #@1ce
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentBottom:I

    #@1d0
    .line 4683
    move-object/from16 v0, p0

    #@1d2
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@1d4
    move-object/from16 v0, p0

    #@1d6
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurLeft:I

    #@1d8
    move-object/from16 v0, p0

    #@1da
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentLeft:I

    #@1dc
    .line 4684
    move-object/from16 v0, p0

    #@1de
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@1e0
    move-object/from16 v0, p0

    #@1e2
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurRight:I

    #@1e4
    move-object/from16 v0, p0

    #@1e6
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentRight:I

    #@1e8
    .line 4685
    move-object/from16 v0, p0

    #@1ea
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@1ec
    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    #@1ef
    move-result v3

    #@1f0
    move-object/from16 v0, p0

    #@1f2
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarLayer:I

    #@1f4
    .line 4687
    move-object/from16 v0, p0

    #@1f6
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@1f8
    sget-object v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@1fa
    sget-object v5, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@1fc
    sget-object v6, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@1fe
    sget-object v7, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@200
    invoke-interface {v3, v4, v5, v6, v7}, Landroid/view/WindowManagerPolicy$WindowState;->computeFrameLw(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@203
    .line 4695
    :cond_203
    move-object/from16 v0, p0

    #@205
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@207
    if-eqz v3, :cond_2e9

    #@209
    .line 4697
    move-object/from16 v0, p0

    #@20b
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@20d
    iput v3, v12, Landroid/graphics/Rect;->left:I

    #@20f
    move-object/from16 v0, v17

    #@211
    iput v3, v0, Landroid/graphics/Rect;->left:I

    #@213
    .line 4698
    move-object/from16 v0, p0

    #@215
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@217
    iput v3, v12, Landroid/graphics/Rect;->top:I

    #@219
    move-object/from16 v0, v17

    #@21b
    iput v3, v0, Landroid/graphics/Rect;->top:I

    #@21d
    .line 4699
    move-object/from16 v0, p0

    #@21f
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@221
    move-object/from16 v0, p0

    #@223
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@225
    sub-int/2addr v3, v4

    #@226
    iput v3, v12, Landroid/graphics/Rect;->right:I

    #@228
    move-object/from16 v0, v17

    #@22a
    iput v3, v0, Landroid/graphics/Rect;->right:I

    #@22c
    .line 4700
    move-object/from16 v0, p0

    #@22e
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@230
    move-object/from16 v0, p0

    #@232
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@234
    sub-int/2addr v3, v4

    #@235
    iput v3, v12, Landroid/graphics/Rect;->bottom:I

    #@237
    move-object/from16 v0, v17

    #@239
    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    #@23b
    .line 4701
    move-object/from16 v0, p0

    #@23d
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableLeft:I

    #@23f
    move-object/from16 v0, v19

    #@241
    iput v3, v0, Landroid/graphics/Rect;->left:I

    #@243
    .line 4702
    move-object/from16 v0, p0

    #@245
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableTop:I

    #@247
    move-object/from16 v0, v19

    #@249
    iput v3, v0, Landroid/graphics/Rect;->top:I

    #@24b
    .line 4703
    move-object/from16 v0, p0

    #@24d
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableRight:I

    #@24f
    move-object/from16 v0, v19

    #@251
    iput v3, v0, Landroid/graphics/Rect;->right:I

    #@253
    .line 4704
    move-object/from16 v0, p0

    #@255
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableBottom:I

    #@257
    move-object/from16 v0, v19

    #@259
    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    #@25b
    .line 4706
    move-object/from16 v0, p0

    #@25d
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@25f
    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    #@262
    move-result v3

    #@263
    move-object/from16 v0, p0

    #@265
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarLayer:I

    #@267
    .line 4709
    move-object/from16 v0, p0

    #@269
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@26b
    move-object/from16 v0, v17

    #@26d
    move-object/from16 v1, v19

    #@26f
    move-object/from16 v2, v19

    #@271
    invoke-interface {v3, v0, v12, v1, v2}, Landroid/view/WindowManagerPolicy$WindowState;->computeFrameLw(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@274
    .line 4712
    move-object/from16 v0, p0

    #@276
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@278
    move-object/from16 v0, p0

    #@27a
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarHeight:I

    #@27c
    add-int/2addr v3, v4

    #@27d
    move-object/from16 v0, p0

    #@27f
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableTop:I

    #@281
    .line 4716
    move-object/from16 v0, p0

    #@283
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@285
    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    #@288
    move-result v3

    #@289
    if-eqz v3, :cond_2c8

    #@28b
    .line 4720
    move-object/from16 v0, p0

    #@28d
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@28f
    move-object/from16 v0, p0

    #@291
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarHeight:I

    #@293
    add-int/2addr v3, v4

    #@294
    move-object/from16 v0, p0

    #@296
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@298
    .line 4722
    move-object/from16 v0, p0

    #@29a
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@29c
    move-object/from16 v0, p0

    #@29e
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurTop:I

    #@2a0
    move-object/from16 v0, p0

    #@2a2
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentTop:I

    #@2a4
    .line 4723
    move-object/from16 v0, p0

    #@2a6
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@2a8
    move-object/from16 v0, p0

    #@2aa
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurBottom:I

    #@2ac
    move-object/from16 v0, p0

    #@2ae
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentBottom:I

    #@2b0
    .line 4724
    move-object/from16 v0, p0

    #@2b2
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@2b4
    move-object/from16 v0, p0

    #@2b6
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurLeft:I

    #@2b8
    move-object/from16 v0, p0

    #@2ba
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentLeft:I

    #@2bc
    .line 4725
    move-object/from16 v0, p0

    #@2be
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@2c0
    move-object/from16 v0, p0

    #@2c2
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurRight:I

    #@2c4
    move-object/from16 v0, p0

    #@2c6
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentRight:I

    #@2c8
    .line 4734
    :cond_2c8
    move-object/from16 v0, p0

    #@2ca
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@2cc
    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    #@2cf
    move-result v3

    #@2d0
    if-eqz v3, :cond_2e9

    #@2d2
    move-object/from16 v0, p0

    #@2d4
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@2d6
    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->isAnimatingLw()Z

    #@2d9
    move-result v3

    #@2da
    if-nez v3, :cond_2e9

    #@2dc
    .line 4738
    move-object/from16 v0, p0

    #@2de
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@2e0
    move-object/from16 v0, p0

    #@2e2
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarHeight:I

    #@2e4
    add-int/2addr v3, v4

    #@2e5
    move-object/from16 v0, p0

    #@2e7
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemTop:I

    #@2e9
    .line 4743
    .end local v13           #keepHidingNav:Z
    .end local v15           #navHideForce:Z
    .end local v16           #navVisible:Z
    :cond_2e9
    move-object/from16 v0, p0

    #@2eb
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurBottom:I

    #@2ed
    move-object/from16 v0, p0

    #@2ef
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mInitialCurBottom:I

    #@2f1
    .line 4744
    move-object/from16 v0, p0

    #@2f3
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentBottom:I

    #@2f5
    move-object/from16 v0, p0

    #@2f7
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mInitialContentBottom:I

    #@2f9
    .line 4745
    return-void

    #@2fa
    .line 4549
    :cond_2fa
    const/16 v16, 0x0

    #@2fc
    goto/16 :goto_f9

    #@2fe
    .line 4552
    .restart local v16       #navVisible:Z
    :cond_2fe
    const/4 v15, 0x0

    #@2ff
    goto/16 :goto_104

    #@301
    .line 4592
    .restart local v13       #keepHidingNav:Z
    .restart local v15       #navHideForce:Z
    :cond_301
    move-object/from16 v0, p0

    #@303
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideNavFakeWindow:Landroid/view/WindowManagerPolicy$FakeWindow;

    #@305
    if-nez v3, :cond_14b

    #@307
    if-nez v13, :cond_14b

    #@309
    .line 4593
    move-object/from16 v0, p0

    #@30b
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@30d
    move-object/from16 v0, p0

    #@30f
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@311
    invoke-virtual {v4}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@314
    move-result-object v4

    #@315
    move-object/from16 v0, p0

    #@317
    iget-object v5, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideNavInputEventReceiverFactory:Landroid/view/InputEventReceiver$Factory;

    #@319
    const-string v6, "hidden nav"

    #@31b
    const/16 v7, 0x7e6

    #@31d
    const/4 v8, 0x0

    #@31e
    const/4 v9, 0x0

    #@31f
    const/4 v10, 0x0

    #@320
    const/4 v11, 0x1

    #@321
    invoke-interface/range {v3 .. v11}, Landroid/view/WindowManagerPolicy$WindowManagerFuncs;->addFakeWindow(Landroid/os/Looper;Landroid/view/InputEventReceiver$Factory;Ljava/lang/String;IIZZZ)Landroid/view/WindowManagerPolicy$FakeWindow;

    #@324
    move-result-object v3

    #@325
    move-object/from16 v0, p0

    #@327
    iput-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideNavFakeWindow:Landroid/view/WindowManagerPolicy$FakeWindow;

    #@329
    goto/16 :goto_14b

    #@32b
    .line 4605
    :cond_32b
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->clearKeepHidingNavInputEventReceiver()V

    #@32e
    goto/16 :goto_152

    #@330
    .line 4612
    :cond_330
    const/4 v3, 0x0

    #@331
    goto/16 :goto_159

    #@333
    .line 4619
    :cond_333
    const/4 v3, 0x0

    #@334
    goto/16 :goto_16e

    #@336
    .line 4631
    .restart local v18       #top:I
    :cond_336
    if-eqz v16, :cond_357

    #@338
    .line 4632
    move-object/from16 v0, p0

    #@33a
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@33c
    const/4 v4, 0x1

    #@33d
    invoke-interface {v3, v4}, Landroid/view/WindowManagerPolicy$WindowState;->showLw(Z)Z

    #@340
    .line 4633
    sget-object v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@342
    iget v3, v3, Landroid/graphics/Rect;->top:I

    #@344
    move-object/from16 v0, p0

    #@346
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@348
    .line 4634
    move-object/from16 v0, p0

    #@34a
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@34c
    move-object/from16 v0, p0

    #@34e
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@350
    sub-int/2addr v3, v4

    #@351
    move-object/from16 v0, p0

    #@353
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenHeight:I

    #@355
    goto/16 :goto_1a2

    #@357
    .line 4637
    :cond_357
    move-object/from16 v0, p0

    #@359
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@35b
    const/4 v4, 0x1

    #@35c
    invoke-interface {v3, v4}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    #@35f
    goto/16 :goto_1a2

    #@361
    .line 4651
    .end local v18           #top:I
    :cond_361
    move-object/from16 v0, p0

    #@363
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarWidthForRotation:[I

    #@365
    aget v3, v3, p4

    #@367
    sub-int v14, p2, v3

    #@369
    .line 4652
    .local v14, left:I
    sget-object v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@36b
    const/4 v4, 0x0

    #@36c
    move/from16 v0, p2

    #@36e
    move/from16 v1, p3

    #@370
    invoke-virtual {v3, v14, v4, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    #@373
    .line 4653
    sget-object v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@375
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@377
    move-object/from16 v0, p0

    #@379
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenRight:I

    #@37b
    move-object/from16 v0, p0

    #@37d
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableRight:I

    #@37f
    .line 4655
    if-eqz v15, :cond_3a1

    #@381
    .line 4657
    move-object/from16 v0, p0

    #@383
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@385
    const/4 v4, 0x1

    #@386
    invoke-interface {v3, v4}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    #@389
    .line 4669
    :goto_389
    if-nez v15, :cond_1b8

    #@38b
    .line 4670
    if-eqz v16, :cond_1b8

    #@38d
    move-object/from16 v0, p0

    #@38f
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@391
    invoke-interface {v3}, Landroid/view/WindowManagerPolicy$WindowState;->isAnimatingLw()Z

    #@394
    move-result v3

    #@395
    if-nez v3, :cond_1b8

    #@397
    .line 4674
    sget-object v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@399
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@39b
    move-object/from16 v0, p0

    #@39d
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemRight:I

    #@39f
    goto/16 :goto_1b8

    #@3a1
    .line 4660
    :cond_3a1
    if-eqz v16, :cond_3c1

    #@3a3
    .line 4661
    move-object/from16 v0, p0

    #@3a5
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@3a7
    const/4 v4, 0x1

    #@3a8
    invoke-interface {v3, v4}, Landroid/view/WindowManagerPolicy$WindowState;->showLw(Z)Z

    #@3ab
    .line 4662
    sget-object v3, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpNavigationFrame:Landroid/graphics/Rect;

    #@3ad
    iget v3, v3, Landroid/graphics/Rect;->left:I

    #@3af
    move-object/from16 v0, p0

    #@3b1
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@3b3
    .line 4663
    move-object/from16 v0, p0

    #@3b5
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@3b7
    move-object/from16 v0, p0

    #@3b9
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@3bb
    sub-int/2addr v3, v4

    #@3bc
    move-object/from16 v0, p0

    #@3be
    iput v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenWidth:I

    #@3c0
    goto :goto_389

    #@3c1
    .line 4666
    :cond_3c1
    move-object/from16 v0, p0

    #@3c3
    iget-object v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@3c5
    const/4 v4, 0x1

    #@3c6
    invoke-interface {v3, v4}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    #@3c9
    goto :goto_389
.end method

.method public beginPostLayoutPolicyLw(II)V
    .registers 5
    .parameter "displayWidth"
    .parameter "displayHeight"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 5453
    const/4 v0, 0x0

    #@2
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@4
    .line 5454
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceStatusBar:Z

    #@6
    .line 5455
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceStatusBarFromKeyguard:Z

    #@8
    .line 5456
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForcingShowNavBar:Z

    #@a
    .line 5457
    const/4 v0, -0x1

    #@b
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForcingShowNavBarLayer:I

    #@d
    .line 5459
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideLockScreen:Z

    #@f
    .line 5460
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAllowLockscreenWhenOn:Z

    #@11
    .line 5461
    iput v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDismissKeyguard:I

    #@13
    .line 5462
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShowingLockscreen:Z

    #@15
    .line 5463
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShowingDream:Z

    #@17
    .line 5464
    return-void
.end method

.method public canBeForceHidden(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z
    .registers 4
    .parameter "win"
    .parameter "attrs"

    #@0
    .prologue
    .line 2922
    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2
    sparse-switch v0, :sswitch_data_a

    #@5
    .line 2931
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    .line 2929
    :sswitch_7
    const/4 v0, 0x0

    #@8
    goto :goto_6

    #@9
    .line 2922
    nop

    #@a
    :sswitch_data_a
    .sparse-switch
        0x7d0 -> :sswitch_7
        0x7d4 -> :sswitch_7
        0x7dd -> :sswitch_7
        0x7e3 -> :sswitch_7
        0x7e7 -> :sswitch_7
        0x7e9 -> :sswitch_7
    .end sparse-switch
.end method

.method public canMagnifyWindowLw(Landroid/view/WindowManager$LayoutParams;)Z
    .registers 3
    .parameter "attrs"

    #@0
    .prologue
    .line 7959
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2
    sparse-switch v0, :sswitch_data_a

    #@5
    .line 7967
    const/4 v0, 0x1

    #@6
    :goto_6
    return v0

    #@7
    .line 7964
    :sswitch_7
    const/4 v0, 0x0

    #@8
    goto :goto_6

    #@9
    .line 7959
    nop

    #@a
    :sswitch_data_a
    .sparse-switch
        0x7db -> :sswitch_7
        0x7dc -> :sswitch_7
        0x7e3 -> :sswitch_7
        0x7eb -> :sswitch_7
    .end sparse-switch
.end method

.method public checkAddPermission(Landroid/view/WindowManager$LayoutParams;)I
    .registers 6
    .parameter "attrs"

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 2614
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@3
    .line 2616
    .local v1, type:I
    const/16 v3, 0x7d0

    #@5
    if-lt v1, v3, :cond_b

    #@7
    const/16 v3, 0xbb7

    #@9
    if-le v1, v3, :cond_c

    #@b
    .line 2648
    :cond_b
    :goto_b
    return v2

    #@c
    .line 2620
    :cond_c
    const/4 v0, 0x0

    #@d
    .line 2621
    .local v0, permission:Ljava/lang/String;
    sparse-switch v1, :sswitch_data_22

    #@10
    .line 2640
    const-string v0, "android.permission.INTERNAL_SYSTEM_WINDOW"

    #@12
    .line 2642
    :goto_12
    :sswitch_12
    if-eqz v0, :cond_b

    #@14
    .line 2643
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@16
    invoke-virtual {v3, v0}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@19
    move-result v3

    #@1a
    if-eqz v3, :cond_b

    #@1c
    .line 2645
    const/4 v2, -0x8

    #@1d
    goto :goto_b

    #@1e
    .line 2637
    :sswitch_1e
    const-string v0, "android.permission.SYSTEM_ALERT_WINDOW"

    #@20
    .line 2638
    goto :goto_12

    #@21
    .line 2621
    nop

    #@22
    :sswitch_data_22
    .sparse-switch
        0x7d2 -> :sswitch_1e
        0x7d3 -> :sswitch_1e
        0x7d5 -> :sswitch_12
        0x7d6 -> :sswitch_1e
        0x7d7 -> :sswitch_1e
        0x7da -> :sswitch_1e
        0x7db -> :sswitch_12
        0x7dd -> :sswitch_12
        0x7e7 -> :sswitch_12
    .end sparse-switch
.end method

.method public checkShowToOwnerOnly(Landroid/view/WindowManager$LayoutParams;)Z
    .registers 5
    .parameter "attrs"

    #@0
    .prologue
    const/4 v0, 0x1

    #@1
    .line 2656
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->type:I

    #@3
    sparse-switch v1, :sswitch_data_1a

    #@6
    .line 2663
    iget v1, p1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    #@8
    and-int/lit8 v1, v1, 0x10

    #@a
    if-nez v1, :cond_d

    #@c
    .line 2694
    :cond_c
    :goto_c
    return v0

    #@d
    :cond_d
    :sswitch_d
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@f
    const-string v2, "android.permission.INTERNAL_SYSTEM_WINDOW"

    #@11
    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    #@14
    move-result v1

    #@15
    if-nez v1, :cond_c

    #@17
    const/4 v0, 0x0

    #@18
    goto :goto_c

    #@19
    .line 2656
    nop

    #@1a
    :sswitch_data_1a
    .sparse-switch
        0x3 -> :sswitch_d
        0x7d0 -> :sswitch_d
        0x7d1 -> :sswitch_d
        0x7d2 -> :sswitch_d
        0x7d4 -> :sswitch_d
        0x7d7 -> :sswitch_d
        0x7d8 -> :sswitch_d
        0x7d9 -> :sswitch_d
        0x7de -> :sswitch_d
        0x7e1 -> :sswitch_d
        0x7e2 -> :sswitch_d
        0x7e3 -> :sswitch_d
        0x7e4 -> :sswitch_d
        0x7e5 -> :sswitch_d
        0x7e6 -> :sswitch_d
        0x7e8 -> :sswitch_d
        0x7e9 -> :sswitch_d
        0x7ea -> :sswitch_d
        0x7eb -> :sswitch_d
        0x7ec -> :sswitch_d
    .end sparse-switch
.end method

.method public createForceHideEnterAnimation(Z)Landroid/view/animation/Animation;
    .registers 4
    .parameter "onWallpaper"

    #@0
    .prologue
    .line 3191
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2
    if-eqz p1, :cond_c

    #@4
    const v0, 0x10a0030

    #@7
    :goto_7
    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    #@a
    move-result-object v0

    #@b
    return-object v0

    #@c
    :cond_c
    const v0, 0x10a002d

    #@f
    goto :goto_7
.end method

.method createHomeDockIntent()Landroid/content/Intent;
    .registers 7

    #@0
    .prologue
    const/4 v3, 0x0

    #@1
    .line 7668
    const/4 v1, 0x0

    #@2
    .line 7673
    .local v1, intent:Landroid/content/Intent;
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUiMode:I

    #@4
    const/4 v5, 0x3

    #@5
    if-ne v4, v5, :cond_e

    #@7
    .line 7675
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockIntent:Landroid/content/Intent;

    #@9
    move-object v2, v1

    #@a
    .line 7682
    .end local v1           #intent:Landroid/content/Intent;
    .local v2, intent:Landroid/content/Intent;
    :goto_a
    if-nez v2, :cond_15

    #@c
    move-object v1, v2

    #@d
    .line 7695
    .end local v2           #intent:Landroid/content/Intent;
    .restart local v1       #intent:Landroid/content/Intent;
    :goto_d
    return-object v3

    #@e
    .line 7677
    :cond_e
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUiMode:I

    #@10
    const/4 v5, 0x2

    #@11
    if-ne v4, v5, :cond_13

    #@13
    :cond_13
    move-object v2, v1

    #@14
    .end local v1           #intent:Landroid/content/Intent;
    .restart local v2       #intent:Landroid/content/Intent;
    goto :goto_a

    #@15
    .line 7685
    :cond_15
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@17
    invoke-virtual {v4}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    #@1a
    move-result-object v4

    #@1b
    const/16 v5, 0x80

    #@1d
    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->resolveActivityInfo(Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;

    #@20
    move-result-object v0

    #@21
    .line 7687
    .local v0, ai:Landroid/content/pm/ActivityInfo;
    if-nez v0, :cond_25

    #@23
    move-object v1, v2

    #@24
    .line 7688
    .end local v2           #intent:Landroid/content/Intent;
    .restart local v1       #intent:Landroid/content/Intent;
    goto :goto_d

    #@25
    .line 7690
    .end local v1           #intent:Landroid/content/Intent;
    .restart local v2       #intent:Landroid/content/Intent;
    :cond_25
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    #@27
    if-eqz v4, :cond_41

    #@29
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->metaData:Landroid/os/Bundle;

    #@2b
    const-string v5, "android.dock_home"

    #@2d
    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    #@30
    move-result v4

    #@31
    if-eqz v4, :cond_41

    #@33
    .line 7691
    new-instance v1, Landroid/content/Intent;

    #@35
    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    #@38
    .line 7692
    .end local v2           #intent:Landroid/content/Intent;
    .restart local v1       #intent:Landroid/content/Intent;
    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    #@3a
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    #@3c
    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@3f
    move-object v3, v1

    #@40
    .line 7693
    goto :goto_d

    #@41
    .end local v1           #intent:Landroid/content/Intent;
    .restart local v2       #intent:Landroid/content/Intent;
    :cond_41
    move-object v1, v2

    #@42
    .line 7695
    .end local v2           #intent:Landroid/content/Intent;
    .restart local v1       #intent:Landroid/content/Intent;
    goto :goto_d
.end method

.method public dismissKeyguardLw()V
    .registers 3

    #@0
    .prologue
    .line 7183
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@5
    move-result v0

    #@6
    if-eqz v0, :cond_12

    #@8
    .line 7184
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@a
    new-instance v1, Lcom/android/internal/policy/impl/PhoneWindowManager$36;

    #@c
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$36;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@f
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@12
    .line 7196
    :cond_12
    return-void
.end method

.method dismissRestartActionDialog()V
    .registers 2

    #@0
    .prologue
    .line 1918
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestartAction:Lcom/android/internal/policy/impl/RestartAction;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 1919
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestartAction:Lcom/android/internal/policy/impl/RestartAction;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/RestartAction;->dismissDialog()V

    #@9
    .line 1921
    :cond_9
    return-void
.end method

.method dispatchMediaKeyRepeatWithWakeLock(Landroid/view/KeyEvent;)V
    .registers 7
    .parameter "event"

    #@0
    .prologue
    .line 6884
    const/4 v1, 0x0

    #@1
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHavePendingMediaKeyRepeatWithWakeLock:Z

    #@3
    .line 6886
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@6
    move-result-wide v1

    #@7
    const/4 v3, 0x1

    #@8
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    #@b
    move-result v4

    #@c
    or-int/lit16 v4, v4, 0x80

    #@e
    invoke-static {p1, v1, v2, v3, v4}, Landroid/view/KeyEvent;->changeTimeRepeat(Landroid/view/KeyEvent;JII)Landroid/view/KeyEvent;

    #@11
    move-result-object v0

    #@12
    .line 6889
    .local v0, repeatEvent:Landroid/view/KeyEvent;
    const-string v1, "WindowManager"

    #@14
    new-instance v2, Ljava/lang/StringBuilder;

    #@16
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@19
    const-string v3, "dispatchMediaKeyRepeatWithWakeLock: "

    #@1b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1e
    move-result-object v2

    #@1f
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@22
    move-result-object v2

    #@23
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@26
    move-result-object v2

    #@27
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@2a
    .line 6892
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->dispatchMediaKeyWithWakeLockToAudioService(Landroid/view/KeyEvent;)V

    #@2d
    .line 6893
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2f
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@32
    .line 6894
    return-void
.end method

.method dispatchMediaKeyWithWakeLock(Landroid/view/KeyEvent;)V
    .registers 8
    .parameter "event"

    #@0
    .prologue
    const/4 v5, 0x4

    #@1
    const/4 v4, 0x1

    #@2
    .line 6855
    const-string v1, "WindowManager"

    #@4
    new-instance v2, Ljava/lang/StringBuilder;

    #@6
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@9
    const-string v3, "dispatchMediaKeyWithWakeLock: "

    #@b
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e
    move-result-object v2

    #@f
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16
    move-result-object v2

    #@17
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1a
    .line 6858
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHavePendingMediaKeyRepeatWithWakeLock:Z

    #@1c
    if-eqz v1, :cond_32

    #@1e
    .line 6860
    const-string v1, "WindowManager"

    #@20
    const-string v2, "dispatchMediaKeyWithWakeLock: canceled repeat"

    #@22
    invoke-static {v1, v2}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@25
    .line 6863
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@27
    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    #@2a
    .line 6864
    const/4 v1, 0x0

    #@2b
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHavePendingMediaKeyRepeatWithWakeLock:Z

    #@2d
    .line 6865
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2f
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@32
    .line 6868
    :cond_32
    invoke-virtual {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->dispatchMediaKeyWithWakeLockToAudioService(Landroid/view/KeyEvent;)V

    #@35
    .line 6870
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    #@38
    move-result v1

    #@39
    if-nez v1, :cond_57

    #@3b
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@3e
    move-result v1

    #@3f
    if-nez v1, :cond_57

    #@41
    .line 6872
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHavePendingMediaKeyRepeatWithWakeLock:Z

    #@43
    .line 6874
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@45
    invoke-virtual {v1, v5, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@48
    move-result-object v0

    #@49
    .line 6876
    .local v0, msg:Landroid/os/Message;
    invoke-virtual {v0, v4}, Landroid/os/Message;->setAsynchronous(Z)V

    #@4c
    .line 6877
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@4e
    invoke-static {}, Landroid/view/ViewConfiguration;->getKeyRepeatTimeout()I

    #@51
    move-result v2

    #@52
    int-to-long v2, v2

    #@53
    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    #@56
    .line 6881
    .end local v0           #msg:Landroid/os/Message;
    :goto_56
    return-void

    #@57
    .line 6879
    :cond_57
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@59
    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    #@5c
    goto :goto_56
.end method

.method dispatchMediaKeyWithWakeLockToAudioService(Landroid/view/KeyEvent;)V
    .registers 7
    .parameter "event"

    #@0
    .prologue
    .line 6897
    invoke-static {}, Landroid/app/ActivityManagerNative;->isSystemReady()Z

    #@3
    move-result v2

    #@4
    if-eqz v2, :cond_f

    #@6
    .line 6898
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getAudioService()Landroid/media/IAudioService;

    #@9
    move-result-object v0

    #@a
    .line 6899
    .local v0, audioService:Landroid/media/IAudioService;
    if-eqz v0, :cond_f

    #@c
    .line 6901
    :try_start_c
    invoke-interface {v0, p1}, Landroid/media/IAudioService;->dispatchMediaKeyEventUnderWakelock(Landroid/view/KeyEvent;)V
    :try_end_f
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_f} :catch_10

    #@f
    .line 6907
    .end local v0           #audioService:Landroid/media/IAudioService;
    :cond_f
    :goto_f
    return-void

    #@10
    .line 6902
    .restart local v0       #audioService:Landroid/media/IAudioService;
    :catch_10
    move-exception v1

    #@11
    .line 6903
    .local v1, e:Landroid/os/RemoteException;
    const-string v2, "WindowManager"

    #@13
    new-instance v3, Ljava/lang/StringBuilder;

    #@15
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@18
    const-string v4, "dispatchMediaKeyEvent threw exception "

    #@1a
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1d
    move-result-object v3

    #@1e
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@21
    move-result-object v3

    #@22
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@25
    move-result-object v3

    #@26
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@29
    goto :goto_f
.end method

.method public dispatchUnhandledKey(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;
    .registers 27
    .parameter "win"
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 4077
    const-string v4, "WindowManager"

    #@2
    new-instance v5, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v6, "Unhandled key: win="

    #@9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v5

    #@d
    move-object/from16 v0, p1

    #@f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v5

    #@13
    const-string v6, ", action="

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v5

    #@19
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    #@1c
    move-result v6

    #@1d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@20
    move-result-object v5

    #@21
    const-string v6, ", flags="

    #@23
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@26
    move-result-object v5

    #@27
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    #@2a
    move-result v6

    #@2b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@2e
    move-result-object v5

    #@2f
    const-string v6, ", keyCode="

    #@31
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@34
    move-result-object v5

    #@35
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getKeyCode()I

    #@38
    move-result v6

    #@39
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3c
    move-result-object v5

    #@3d
    const-string v6, ", scanCode="

    #@3f
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@42
    move-result-object v5

    #@43
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getScanCode()I

    #@46
    move-result v6

    #@47
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v5

    #@4b
    const-string v6, ", metaState="

    #@4d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v5

    #@51
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getMetaState()I

    #@54
    move-result v6

    #@55
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@58
    move-result-object v5

    #@59
    const-string v6, ", repeatCount="

    #@5b
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5e
    move-result-object v5

    #@5f
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@62
    move-result v6

    #@63
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@66
    move-result-object v5

    #@67
    const-string v6, ", policyFlags="

    #@69
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@6c
    move-result-object v5

    #@6d
    move/from16 v0, p3

    #@6f
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@72
    move-result-object v5

    #@73
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@76
    move-result-object v5

    #@77
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7a
    .line 4086
    const/16 v18, 0x0

    #@7c
    .line 4087
    .local v18, fallbackEvent:Landroid/view/KeyEvent;
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    #@7f
    move-result v4

    #@80
    and-int/lit16 v4, v4, 0x400

    #@82
    if-nez v4, :cond_124

    #@84
    .line 4088
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@87
    move-result-object v20

    #@88
    .line 4089
    .local v20, kcm:Landroid/view/KeyCharacterMap;
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getKeyCode()I

    #@8b
    move-result v21

    #@8c
    .line 4090
    .local v21, keyCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getMetaState()I

    #@8f
    move-result v22

    #@90
    .line 4091
    .local v22, metaState:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    #@93
    move-result v4

    #@94
    if-nez v4, :cond_12e

    #@96
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@99
    move-result v4

    #@9a
    if-nez v4, :cond_12e

    #@9c
    const/16 v19, 0x1

    #@9e
    .line 4096
    .local v19, initialDown:Z
    :goto_9e
    if-eqz v19, :cond_132

    #@a0
    .line 4097
    invoke-virtual/range {v20 .. v22}, Landroid/view/KeyCharacterMap;->getFallbackAction(II)Landroid/view/KeyCharacterMap$FallbackAction;

    #@a3
    move-result-object v17

    #@a4
    .line 4102
    .local v17, fallbackAction:Landroid/view/KeyCharacterMap$FallbackAction;
    :goto_a4
    if-eqz v17, :cond_124

    #@a6
    .line 4104
    const-string v4, "WindowManager"

    #@a8
    new-instance v5, Ljava/lang/StringBuilder;

    #@aa
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@ad
    const-string v6, "Fallback: keyCode="

    #@af
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b2
    move-result-object v5

    #@b3
    move-object/from16 v0, v17

    #@b5
    iget v6, v0, Landroid/view/KeyCharacterMap$FallbackAction;->keyCode:I

    #@b7
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ba
    move-result-object v5

    #@bb
    const-string v6, " metaState="

    #@bd
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c0
    move-result-object v5

    #@c1
    move-object/from16 v0, v17

    #@c3
    iget v6, v0, Landroid/view/KeyCharacterMap$FallbackAction;->metaState:I

    #@c5
    invoke-static {v6}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@c8
    move-result-object v6

    #@c9
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@cc
    move-result-object v5

    #@cd
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@d0
    move-result-object v5

    #@d1
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d4
    .line 4108
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    #@d7
    move-result v4

    #@d8
    or-int/lit16 v14, v4, 0x400

    #@da
    .line 4109
    .local v14, flags:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getDownTime()J

    #@dd
    move-result-wide v4

    #@de
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getEventTime()J

    #@e1
    move-result-wide v6

    #@e2
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    #@e5
    move-result v8

    #@e6
    move-object/from16 v0, v17

    #@e8
    iget v9, v0, Landroid/view/KeyCharacterMap$FallbackAction;->keyCode:I

    #@ea
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@ed
    move-result v10

    #@ee
    move-object/from16 v0, v17

    #@f0
    iget v11, v0, Landroid/view/KeyCharacterMap$FallbackAction;->metaState:I

    #@f2
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getDeviceId()I

    #@f5
    move-result v12

    #@f6
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getScanCode()I

    #@f9
    move-result v13

    #@fa
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getSource()I

    #@fd
    move-result v15

    #@fe
    const/16 v16, 0x0

    #@100
    invoke-static/range {v4 .. v16}, Landroid/view/KeyEvent;->obtain(JJIIIIIIIILjava/lang/String;)Landroid/view/KeyEvent;

    #@103
    move-result-object v18

    #@104
    .line 4116
    move-object/from16 v0, p0

    #@106
    move-object/from16 v1, p1

    #@108
    move-object/from16 v2, v18

    #@10a
    move/from16 v3, p3

    #@10c
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptFallback(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;I)Z

    #@10f
    move-result v4

    #@110
    if-nez v4, :cond_117

    #@112
    .line 4117
    invoke-virtual/range {v18 .. v18}, Landroid/view/KeyEvent;->recycle()V

    #@115
    .line 4118
    const/16 v18, 0x0

    #@117
    .line 4121
    :cond_117
    if-eqz v19, :cond_140

    #@119
    .line 4122
    move-object/from16 v0, p0

    #@11b
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFallbackActions:Landroid/util/SparseArray;

    #@11d
    move/from16 v0, v21

    #@11f
    move-object/from16 v1, v17

    #@121
    invoke-virtual {v4, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    #@124
    .line 4131
    .end local v14           #flags:I
    .end local v17           #fallbackAction:Landroid/view/KeyCharacterMap$FallbackAction;
    .end local v19           #initialDown:Z
    .end local v20           #kcm:Landroid/view/KeyCharacterMap;
    .end local v21           #keyCode:I
    .end local v22           #metaState:I
    :cond_124
    :goto_124
    if-nez v18, :cond_154

    #@126
    .line 4132
    const-string v4, "WindowManager"

    #@128
    const-string v5, "No fallback."

    #@12a
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12d
    .line 4137
    :goto_12d
    return-object v18

    #@12e
    .line 4091
    .restart local v20       #kcm:Landroid/view/KeyCharacterMap;
    .restart local v21       #keyCode:I
    .restart local v22       #metaState:I
    :cond_12e
    const/16 v19, 0x0

    #@130
    goto/16 :goto_9e

    #@132
    .line 4099
    .restart local v19       #initialDown:Z
    :cond_132
    move-object/from16 v0, p0

    #@134
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFallbackActions:Landroid/util/SparseArray;

    #@136
    move/from16 v0, v21

    #@138
    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@13b
    move-result-object v17

    #@13c
    check-cast v17, Landroid/view/KeyCharacterMap$FallbackAction;

    #@13e
    .restart local v17       #fallbackAction:Landroid/view/KeyCharacterMap$FallbackAction;
    goto/16 :goto_a4

    #@140
    .line 4123
    .restart local v14       #flags:I
    :cond_140
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    #@143
    move-result v4

    #@144
    const/4 v5, 0x1

    #@145
    if-ne v4, v5, :cond_124

    #@147
    .line 4124
    move-object/from16 v0, p0

    #@149
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFallbackActions:Landroid/util/SparseArray;

    #@14b
    move/from16 v0, v21

    #@14d
    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->remove(I)V

    #@150
    .line 4125
    invoke-virtual/range {v17 .. v17}, Landroid/view/KeyCharacterMap$FallbackAction;->recycle()V

    #@153
    goto :goto_124

    #@154
    .line 4134
    .end local v14           #flags:I
    .end local v17           #fallbackAction:Landroid/view/KeyCharacterMap$FallbackAction;
    .end local v19           #initialDown:Z
    .end local v20           #kcm:Landroid/view/KeyCharacterMap;
    .end local v21           #keyCode:I
    .end local v22           #metaState:I
    :cond_154
    const-string v4, "WindowManager"

    #@156
    new-instance v5, Ljava/lang/StringBuilder;

    #@158
    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    #@15b
    const-string v6, "Performing fallback: "

    #@15d
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@160
    move-result-object v5

    #@161
    move-object/from16 v0, v18

    #@163
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@166
    move-result-object v5

    #@167
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@16a
    move-result-object v5

    #@16b
    invoke-static {v4, v5}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@16e
    goto :goto_12d
.end method

.method public doesForceHide(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)Z
    .registers 5
    .parameter "win"
    .parameter "attrs"

    #@0
    .prologue
    .line 2917
    iget v0, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2
    const/16 v1, 0x7d4

    #@4
    if-ne v0, v1, :cond_8

    #@6
    const/4 v0, 0x1

    #@7
    :goto_7
    return v0

    #@8
    :cond_8
    const/4 v0, 0x0

    #@9
    goto :goto_7
.end method

.method public dump(Ljava/lang/String;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .registers 5
    .parameter "prefix"
    .parameter "pw"
    .parameter "args"

    #@0
    .prologue
    .line 7992
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3
    const-string v0, "mSafeMode="

    #@5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSafeMode:Z

    #@a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@d
    .line 7993
    const-string v0, " mSystemReady="

    #@f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemReady:Z

    #@14
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@17
    .line 7994
    const-string v0, " mSystemBooted="

    #@19
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemBooted:Z

    #@1e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@21
    .line 7995
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24
    const-string v0, "mLidState="

    #@26
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@29
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidState:I

    #@2b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@2e
    .line 7996
    const-string v0, " mLidOpenRotation="

    #@30
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@33
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidOpenRotation:I

    #@35
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@38
    .line 7997
    const-string v0, " mHdmiPlugged="

    #@3a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3d
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiPlugged:Z

    #@3f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@42
    .line 7998
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastSystemUiFlags:I

    #@44
    if-nez v0, :cond_4e

    #@46
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mResettingSystemUiFlags:I

    #@48
    if-nez v0, :cond_4e

    #@4a
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceClearedSystemUiFlags:I

    #@4c
    if-eqz v0, :cond_7b

    #@4e
    .line 8000
    :cond_4e
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@51
    const-string v0, "mLastSystemUiFlags=0x"

    #@53
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@56
    .line 8001
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastSystemUiFlags:I

    #@58
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@5b
    move-result-object v0

    #@5c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@5f
    .line 8002
    const-string v0, " mResettingSystemUiFlags=0x"

    #@61
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@64
    .line 8003
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mResettingSystemUiFlags:I

    #@66
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@69
    move-result-object v0

    #@6a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@6d
    .line 8004
    const-string v0, " mForceClearedSystemUiFlags=0x"

    #@6f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@72
    .line 8005
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceClearedSystemUiFlags:I

    #@74
    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@77
    move-result-object v0

    #@78
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@7b
    .line 8007
    :cond_7b
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastFocusNeedsMenu:Z

    #@7d
    if-eqz v0, :cond_8c

    #@7f
    .line 8008
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@82
    const-string v0, "mLastFocusNeedsMenu="

    #@84
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@87
    .line 8009
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastFocusNeedsMenu:Z

    #@89
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@8c
    .line 8011
    :cond_8c
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@8f
    const-string v0, "mDockMode="

    #@91
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@94
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@96
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@99
    .line 8012
    const-string v0, " mCarDockRotation="

    #@9b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@9e
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockRotation:I

    #@a0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@a3
    .line 8013
    const-string v0, " mDeskDockRotation="

    #@a5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@a8
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockRotation:I

    #@aa
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@ad
    .line 8014
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b0
    const-string v0, "mUserRotationMode="

    #@b2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@b5
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotationMode:I

    #@b7
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@ba
    .line 8015
    const-string v0, " mUserRotation="

    #@bc
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@bf
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotation:I

    #@c1
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@c4
    .line 8016
    const-string v0, " mAllowAllRotations="

    #@c6
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@c9
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAllowAllRotations:I

    #@cb
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@ce
    .line 8017
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d1
    const-string v0, "mCurrentAppOrientation="

    #@d3
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@d6
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurrentAppOrientation:I

    #@d8
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@db
    .line 8018
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@de
    const-string v0, "mCarDockEnablesAccelerometer="

    #@e0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@e3
    .line 8019
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockEnablesAccelerometer:Z

    #@e5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@e8
    .line 8020
    const-string v0, " mDeskDockEnablesAccelerometer="

    #@ea
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@ed
    .line 8021
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockEnablesAccelerometer:Z

    #@ef
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@f2
    .line 8022
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@f5
    const-string v0, "mLidKeyboardAccessibility="

    #@f7
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@fa
    .line 8023
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidKeyboardAccessibility:I

    #@fc
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@ff
    .line 8024
    const-string v0, " mLidNavigationAccessibility="

    #@101
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@104
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidNavigationAccessibility:I

    #@106
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@109
    .line 8025
    const-string v0, " mLidControlsSleep="

    #@10b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@10e
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidControlsSleep:Z

    #@110
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@113
    .line 8026
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@116
    const-string v0, "mLongPressOnPowerBehavior="

    #@118
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@11b
    .line 8027
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnPowerBehavior:I

    #@11d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@120
    .line 8028
    const-string v0, " mHasSoftInput="

    #@122
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@125
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSoftInput:Z

    #@127
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@12a
    .line 8029
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@12d
    const-string v0, "mScreenOnEarly="

    #@12f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@132
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnEarly:Z

    #@134
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@137
    .line 8030
    const-string v0, " mScreenOnFully="

    #@139
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@13c
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnFully:Z

    #@13e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@141
    .line 8031
    const-string v0, " mOrientationSensorEnabled="

    #@143
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@146
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationSensorEnabled:Z

    #@148
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@14b
    .line 8032
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@14e
    const-string v0, "mUnrestrictedScreen=("

    #@150
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@153
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@155
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@158
    .line 8033
    const-string v0, ","

    #@15a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@15d
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@15f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@162
    .line 8034
    const-string v0, ") "

    #@164
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@167
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@169
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@16c
    .line 8035
    const-string v0, "x"

    #@16e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@171
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@173
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@176
    .line 8036
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@179
    const-string v0, "mRestrictedScreen=("

    #@17b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@17e
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@180
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@183
    .line 8037
    const-string v0, ","

    #@185
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@188
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@18a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@18d
    .line 8038
    const-string v0, ") "

    #@18f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@192
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenWidth:I

    #@194
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@197
    .line 8039
    const-string v0, "x"

    #@199
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@19c
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenHeight:I

    #@19e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@1a1
    .line 8040
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a4
    const-string v0, "mStableFullscreen=("

    #@1a6
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1a9
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenLeft:I

    #@1ab
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1ae
    .line 8041
    const-string v0, ","

    #@1b0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1b3
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenTop:I

    #@1b5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1b8
    .line 8042
    const-string v0, ")-("

    #@1ba
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1bd
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenRight:I

    #@1bf
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1c2
    .line 8043
    const-string v0, ","

    #@1c4
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1c7
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenBottom:I

    #@1c9
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1cc
    const-string v0, ")"

    #@1ce
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@1d1
    .line 8044
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d4
    const-string v0, "mStable=("

    #@1d6
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1d9
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableLeft:I

    #@1db
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1de
    .line 8045
    const-string v0, ","

    #@1e0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1e3
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableTop:I

    #@1e5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1e8
    .line 8046
    const-string v0, ")-("

    #@1ea
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1ed
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableRight:I

    #@1ef
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1f2
    .line 8047
    const-string v0, ","

    #@1f4
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@1f7
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableBottom:I

    #@1f9
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@1fc
    const-string v0, ")"

    #@1fe
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@201
    .line 8048
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@204
    const-string v0, "mSystem=("

    #@206
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@209
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemLeft:I

    #@20b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@20e
    .line 8049
    const-string v0, ","

    #@210
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@213
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemTop:I

    #@215
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@218
    .line 8050
    const-string v0, ")-("

    #@21a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@21d
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemRight:I

    #@21f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@222
    .line 8051
    const-string v0, ","

    #@224
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@227
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemBottom:I

    #@229
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@22c
    const-string v0, ")"

    #@22e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@231
    .line 8052
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@234
    const-string v0, "mCur=("

    #@236
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@239
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurLeft:I

    #@23b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@23e
    .line 8053
    const-string v0, ","

    #@240
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@243
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurTop:I

    #@245
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@248
    .line 8054
    const-string v0, ")-("

    #@24a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@24d
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurRight:I

    #@24f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@252
    .line 8055
    const-string v0, ","

    #@254
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@257
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurBottom:I

    #@259
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@25c
    const-string v0, ")"

    #@25e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@261
    .line 8056
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@264
    const-string v0, "mContent=("

    #@266
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@269
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentLeft:I

    #@26b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@26e
    .line 8057
    const-string v0, ","

    #@270
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@273
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentTop:I

    #@275
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@278
    .line 8058
    const-string v0, ")-("

    #@27a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@27d
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentRight:I

    #@27f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@282
    .line 8059
    const-string v0, ","

    #@284
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@287
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentBottom:I

    #@289
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@28c
    const-string v0, ")"

    #@28e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@291
    .line 8060
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@294
    const-string v0, "mDock=("

    #@296
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@299
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@29b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@29e
    .line 8061
    const-string v0, ","

    #@2a0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2a3
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@2a5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@2a8
    .line 8062
    const-string v0, ")-("

    #@2aa
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2ad
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@2af
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@2b2
    .line 8063
    const-string v0, ","

    #@2b4
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2b7
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@2b9
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@2bc
    const-string v0, ")"

    #@2be
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    #@2c1
    .line 8064
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c4
    const-string v0, "mDockLayer="

    #@2c6
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2c9
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLayer:I

    #@2cb
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@2ce
    .line 8065
    const-string v0, " mStatusBarLayer="

    #@2d0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2d3
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarLayer:I

    #@2d5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@2d8
    .line 8066
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2db
    const-string v0, "mShowingLockscreen="

    #@2dd
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2e0
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShowingLockscreen:Z

    #@2e2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@2e5
    .line 8067
    const-string v0, " mShowingDream="

    #@2e7
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2ea
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShowingDream:Z

    #@2ec
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@2ef
    .line 8068
    const-string v0, " mDreamingLockscreen="

    #@2f1
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@2f4
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDreamingLockscreen:Z

    #@2f6
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@2f9
    .line 8069
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@2fb
    if-eqz v0, :cond_30a

    #@2fd
    .line 8070
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@300
    const-string v0, "mLastInputMethodWindow="

    #@302
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@305
    .line 8071
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@307
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@30a
    .line 8073
    :cond_30a
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodTargetWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@30c
    if-eqz v0, :cond_31b

    #@30e
    .line 8074
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@311
    const-string v0, "mLastInputMethodTargetWindow="

    #@313
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@316
    .line 8075
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodTargetWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@318
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@31b
    .line 8077
    :cond_31b
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@31d
    if-eqz v0, :cond_32c

    #@31f
    .line 8078
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@322
    const-string v0, "mStatusBar="

    #@324
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@327
    .line 8079
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@329
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@32c
    .line 8081
    :cond_32c
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@32e
    if-eqz v0, :cond_33d

    #@330
    .line 8082
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@333
    const-string v0, "mNavigationBar="

    #@335
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@338
    .line 8083
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@33a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@33d
    .line 8085
    :cond_33d
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@33f
    if-eqz v0, :cond_34e

    #@341
    .line 8086
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@344
    const-string v0, "mKeyguard="

    #@346
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@349
    .line 8087
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@34b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@34e
    .line 8089
    :cond_34e
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@350
    if-eqz v0, :cond_35f

    #@352
    .line 8090
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@355
    const-string v0, "mFocusedWindow="

    #@357
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@35a
    .line 8091
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@35c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@35f
    .line 8093
    :cond_35f
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedApp:Landroid/view/IApplicationToken;

    #@361
    if-eqz v0, :cond_370

    #@363
    .line 8094
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@366
    const-string v0, "mFocusedApp="

    #@368
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@36b
    .line 8095
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedApp:Landroid/view/IApplicationToken;

    #@36d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@370
    .line 8097
    :cond_370
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWinDismissingKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@372
    if-eqz v0, :cond_381

    #@374
    .line 8098
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@377
    const-string v0, "mWinDismissingKeyguard="

    #@379
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@37c
    .line 8099
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWinDismissingKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@37e
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@381
    .line 8101
    :cond_381
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@383
    if-eqz v0, :cond_392

    #@385
    .line 8102
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@388
    const-string v0, "mTopFullscreenOpaqueWindowState="

    #@38a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@38d
    .line 8103
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@38f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    #@392
    .line 8105
    :cond_392
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForcingShowNavBar:Z

    #@394
    if-eqz v0, :cond_3ad

    #@396
    .line 8106
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@399
    const-string v0, "mForcingShowNavBar="

    #@39b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@39e
    .line 8107
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForcingShowNavBar:Z

    #@3a0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@3a3
    const-string v0, "mForcingShowNavBarLayer="

    #@3a5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3a8
    .line 8108
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForcingShowNavBarLayer:I

    #@3aa
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@3ad
    .line 8110
    :cond_3ad
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3b0
    const-string v0, "mTopIsFullscreen="

    #@3b2
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3b5
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopIsFullscreen:Z

    #@3b7
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@3ba
    .line 8111
    const-string v0, " mHideLockScreen="

    #@3bc
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3bf
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideLockScreen:Z

    #@3c1
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@3c4
    .line 8112
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3c7
    const-string v0, "mForceStatusBar="

    #@3c9
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3cc
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceStatusBar:Z

    #@3ce
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@3d1
    .line 8113
    const-string v0, " mForceStatusBarFromKeyguard="

    #@3d3
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3d6
    .line 8114
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceStatusBarFromKeyguard:Z

    #@3d8
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@3db
    .line 8115
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3de
    const-string v0, "mDismissKeyguard="

    #@3e0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3e3
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDismissKeyguard:I

    #@3e5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@3e8
    .line 8116
    const-string v0, " mWinDismissingKeyguard="

    #@3ea
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3ed
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWinDismissingKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@3ef
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    #@3f2
    .line 8117
    const-string v0, " mHomePressed="

    #@3f4
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3f7
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomePressed:Z

    #@3f9
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@3fc
    .line 8118
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@3ff
    const-string v0, "mAllowLockscreenWhenOn="

    #@401
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@404
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAllowLockscreenWhenOn:Z

    #@406
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Z)V

    #@409
    .line 8119
    const-string v0, " mLockScreenTimeout="

    #@40b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@40e
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLockScreenTimeout:I

    #@410
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@413
    .line 8120
    const-string v0, " mLockScreenTimerActive="

    #@415
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@418
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLockScreenTimerActive:Z

    #@41a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@41d
    .line 8121
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@420
    const-string v0, "mEndcallBehavior="

    #@422
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@425
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEndcallBehavior:I

    #@427
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@42a
    .line 8122
    const-string v0, " mIncallPowerBehavior="

    #@42c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@42f
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIncallPowerBehavior:I

    #@431
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@434
    .line 8123
    const-string v0, " mLongPressOnHomeBehavior="

    #@436
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@439
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@43b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@43e
    .line 8124
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@441
    const-string v0, "mLandscapeRotation="

    #@443
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@446
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@448
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@44b
    .line 8125
    const-string v0, " mSeascapeRotation="

    #@44d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@450
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSeascapeRotation:I

    #@452
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@455
    .line 8126
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@458
    const-string v0, "mPortraitRotation="

    #@45a
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@45d
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@45f
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@462
    .line 8127
    const-string v0, " mUpsideDownRotation="

    #@464
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@467
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUpsideDownRotation:I

    #@469
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(I)V

    #@46c
    .line 8128
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@46f
    const-string v0, "mHdmiRotation="

    #@471
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@474
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiRotation:I

    #@476
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    #@479
    .line 8129
    const-string v0, " mHdmiRotationLock="

    #@47b
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@47e
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiRotationLock:Z

    #@480
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@483
    .line 8130
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@486
    const-string v0, " mMenuLongEnabled="

    #@488
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@48b
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenulongEnabled:Z

    #@48d
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@490
    .line 8131
    const-string v0, " mMenulongToSearch="

    #@492
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@495
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenulongToSearch:Z

    #@497
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@49a
    .line 8132
    const-string v0, " mQuickClipHotKeyDisable="

    #@49c
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@49f
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@4a1
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@4a4
    .line 8133
    const-string v0, " mHotKeyCustomizing="

    #@4a6
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4a9
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHotKeyCustomizing:Z

    #@4ab
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@4ae
    .line 8134
    const-string v0, " mRearSideKey="

    #@4b0
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    #@4b3
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRearSideKeyEnable:Z

    #@4b5
    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    #@4b8
    .line 8135
    return-void
.end method

.method public enableKeyguard(Z)V
    .registers 3
    .parameter "enabled"

    #@0
    .prologue
    .line 7147
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 7148
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setKeyguardEnabled(Z)V

    #@9
    .line 7150
    :cond_9
    return-void
.end method

.method public enableScreenAfterBoot()V
    .registers 2

    #@0
    .prologue
    .line 7638
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->readLidState()V

    #@3
    .line 7639
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->applyLidSwitchState()V

    #@6
    .line 7640
    const/4 v0, 0x1

    #@7
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateRotation(Z)V

    #@a
    .line 7641
    return-void
.end method

.method public enableTranslucentImeMode(Z)V
    .registers 2
    .parameter "enabled"

    #@0
    .prologue
    .line 8138
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTranslucentImeMode:Z

    #@2
    .line 8139
    return-void
.end method

.method public exitKeyguardSecurely(Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;)V
    .registers 3
    .parameter "callback"

    #@0
    .prologue
    .line 7154
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 7155
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@6
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->verifyUnlock(Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;)V

    #@9
    .line 7157
    :cond_9
    return-void
.end method

.method public finishLayoutLw()V
    .registers 1

    #@0
    .prologue
    .line 5447
    return-void
.end method

.method public finishPostLayoutPolicyLw()I
    .registers 11

    #@0
    .prologue
    const/4 v5, 0x0

    #@1
    const/4 v6, 0x0

    #@2
    const/4 v7, 0x1

    #@3
    .line 5531
    const/4 v0, 0x0

    #@4
    .line 5532
    .local v0, changes:I
    const/4 v4, 0x0

    #@5
    .line 5534
    .local v4, topIsFullscreen:Z
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@7
    if-eqz v8, :cond_a9

    #@9
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@b
    invoke-interface {v8}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@e
    move-result-object v2

    #@f
    .line 5542
    .local v2, lp:Landroid/view/WindowManager$LayoutParams;
    :goto_f
    iget-boolean v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShowingDream:Z

    #@11
    if-nez v8, :cond_17

    #@13
    .line 5543
    iget-boolean v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShowingLockscreen:Z

    #@15
    iput-boolean v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDreamingLockscreen:Z

    #@17
    .line 5546
    :cond_17
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@19
    if-eqz v8, :cond_37

    #@1b
    .line 5550
    iget-boolean v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceStatusBar:Z

    #@1d
    if-nez v8, :cond_23

    #@1f
    iget-boolean v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mForceStatusBarFromKeyguard:Z

    #@21
    if-eqz v8, :cond_b5

    #@23
    .line 5552
    :cond_23
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@25
    invoke-interface {v8, v7}, Landroid/view/WindowManagerPolicy$WindowState;->showLw(Z)Z

    #@28
    move-result v8

    #@29
    if-eqz v8, :cond_37

    #@2b
    .line 5553
    or-int/lit8 v0, v0, 0x1

    #@2d
    .line 5555
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@30
    move-result-object v3

    #@31
    .line 5556
    .local v3, statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v3, :cond_37

    #@33
    .line 5558
    const/4 v8, 0x0

    #@34
    :try_start_34
    invoke-interface {v3, v8}, Lcom/android/internal/statusbar/IStatusBarService;->disableTouch(Z)V
    :try_end_37
    .catch Landroid/os/RemoteException; {:try_start_34 .. :try_end_37} :catch_ac

    #@37
    .line 5628
    .end local v3           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_37
    :goto_37
    iput-boolean v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopIsFullscreen:Z

    #@39
    .line 5632
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@3b
    if-eqz v8, :cond_9b

    #@3d
    .line 5636
    iget v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDismissKeyguard:I

    #@3f
    if-eqz v8, :cond_117

    #@41
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@43
    invoke-virtual {v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@46
    move-result v8

    #@47
    if-nez v8, :cond_117

    #@49
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@4b
    invoke-virtual {v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isLockoutMode()Z

    #@4e
    move-result v8

    #@4f
    if-nez v8, :cond_117

    #@51
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@53
    invoke-virtual {v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isDisabledUnlock()Z

    #@56
    move-result v8

    #@57
    if-nez v8, :cond_117

    #@59
    .line 5640
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@5b
    invoke-interface {v5, v7}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    #@5e
    move-result v5

    #@5f
    if-eqz v5, :cond_63

    #@61
    .line 5641
    or-int/lit8 v0, v0, 0x7

    #@63
    .line 5645
    :cond_63
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@65
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@68
    move-result v5

    #@69
    if-eqz v5, :cond_9b

    #@6b
    .line 5647
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@6d
    if-eqz v5, :cond_91

    #@6f
    .line 5648
    const-string v5, "WindowManager"

    #@71
    new-instance v6, Ljava/lang/StringBuilder;

    #@73
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@76
    const-string v7, "KeyguardDone From FLAG_DISMISS_KEYGUARD : "

    #@78
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7b
    move-result-object v6

    #@7c
    iget-object v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@7e
    invoke-interface {v7}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@81
    move-result-object v7

    #@82
    invoke-virtual {v7}, Landroid/view/WindowManager$LayoutParams;->getTitle()Ljava/lang/CharSequence;

    #@85
    move-result-object v7

    #@86
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@89
    move-result-object v6

    #@8a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8d
    move-result-object v6

    #@8e
    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@91
    .line 5651
    :cond_91
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@93
    new-instance v6, Lcom/android/internal/policy/impl/PhoneWindowManager$21;

    #@95
    invoke-direct {v6, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$21;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@98
    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@9b
    .line 5693
    :cond_9b
    :goto_9b
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateSystemUiVisibilityLw()I

    #@9e
    move-result v5

    #@9f
    and-int/lit8 v5, v5, 0xe

    #@a1
    if-eqz v5, :cond_a5

    #@a3
    .line 5696
    or-int/lit8 v0, v0, 0x1

    #@a5
    .line 5700
    :cond_a5
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateLockScreenTimeout()V

    #@a8
    .line 5701
    return v0

    #@a9
    .end local v2           #lp:Landroid/view/WindowManager$LayoutParams;
    :cond_a9
    move-object v2, v5

    #@aa
    .line 5534
    goto/16 :goto_f

    #@ac
    .line 5559
    .restart local v2       #lp:Landroid/view/WindowManager$LayoutParams;
    .restart local v3       #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_ac
    move-exception v1

    #@ad
    .line 5560
    .local v1, e:Landroid/os/RemoteException;
    const-string v8, "WindowManager"

    #@af
    const-string v9, "RemoteException======"

    #@b1
    invoke-static {v8, v9, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@b4
    goto :goto_37

    #@b5
    .line 5565
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v3           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_b5
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@b7
    if-eqz v8, :cond_37

    #@b9
    .line 5572
    iget v8, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@bb
    and-int/lit16 v8, v8, 0x400

    #@bd
    if-nez v8, :cond_c5

    #@bf
    iget v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastSystemUiFlags:I

    #@c1
    and-int/lit8 v8, v8, 0x4

    #@c3
    if-eqz v8, :cond_e8

    #@c5
    :cond_c5
    move v4, v7

    #@c6
    .line 5578
    :goto_c6
    if-eqz v4, :cond_f3

    #@c8
    .line 5580
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@ca
    invoke-interface {v8, v7}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    #@cd
    move-result v8

    #@ce
    if-eqz v8, :cond_37

    #@d0
    .line 5581
    or-int/lit8 v0, v0, 0x1

    #@d2
    .line 5584
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@d5
    move-result-object v3

    #@d6
    .line 5585
    .restart local v3       #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v3, :cond_dc

    #@d8
    .line 5587
    const/4 v8, 0x1

    #@d9
    :try_start_d9
    invoke-interface {v3, v8}, Lcom/android/internal/statusbar/IStatusBarService;->disableTouch(Z)V
    :try_end_dc
    .catch Landroid/os/RemoteException; {:try_start_d9 .. :try_end_dc} :catch_ea

    #@dc
    .line 5593
    :cond_dc
    :goto_dc
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@de
    new-instance v9, Lcom/android/internal/policy/impl/PhoneWindowManager$20;

    #@e0
    invoke-direct {v9, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$20;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@e3
    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@e6
    goto/16 :goto_37

    #@e8
    .end local v3           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_e8
    move v4, v6

    #@e9
    .line 5572
    goto :goto_c6

    #@ea
    .line 5588
    .restart local v3       #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :catch_ea
    move-exception v1

    #@eb
    .line 5589
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v8, "WindowManager"

    #@ed
    const-string v9, "RemoteException======"

    #@ef
    invoke-static {v8, v9, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@f2
    goto :goto_dc

    #@f3
    .line 5611
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v3           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_f3
    iget-boolean v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverClosed:Z

    #@f5
    if-nez v8, :cond_101

    #@f7
    .line 5612
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@f9
    invoke-interface {v8, v7}, Landroid/view/WindowManagerPolicy$WindowState;->showLw(Z)Z

    #@fc
    move-result v8

    #@fd
    if-eqz v8, :cond_101

    #@ff
    or-int/lit8 v0, v0, 0x1

    #@101
    .line 5615
    :cond_101
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@104
    move-result-object v3

    #@105
    .line 5616
    .restart local v3       #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v3, :cond_37

    #@107
    .line 5618
    const/4 v8, 0x0

    #@108
    :try_start_108
    invoke-interface {v3, v8}, Lcom/android/internal/statusbar/IStatusBarService;->disableTouch(Z)V
    :try_end_10b
    .catch Landroid/os/RemoteException; {:try_start_108 .. :try_end_10b} :catch_10d

    #@10b
    goto/16 :goto_37

    #@10d
    .line 5619
    :catch_10d
    move-exception v1

    #@10e
    .line 5620
    .restart local v1       #e:Landroid/os/RemoteException;
    const-string v8, "WindowManager"

    #@110
    const-string v9, "RemoteException======"

    #@112
    invoke-static {v8, v9, v1}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@115
    goto/16 :goto_37

    #@117
    .line 5658
    .end local v1           #e:Landroid/os/RemoteException;
    .end local v3           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_117
    iget-boolean v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideLockScreen:Z

    #@119
    if-eqz v8, :cond_12c

    #@11b
    .line 5659
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@11d
    invoke-interface {v5, v7}, Landroid/view/WindowManagerPolicy$WindowState;->hideLw(Z)Z

    #@120
    move-result v5

    #@121
    if-eqz v5, :cond_125

    #@123
    .line 5660
    or-int/lit8 v0, v0, 0x7

    #@125
    .line 5664
    :cond_125
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@127
    invoke-virtual {v5, v7}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setHidden(Z)V

    #@12a
    goto/16 :goto_9b

    #@12c
    .line 5665
    :cond_12c
    iget v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDismissKeyguard:I

    #@12e
    if-eqz v8, :cond_14f

    #@130
    .line 5667
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDismissKeyguard:I

    #@132
    if-ne v5, v7, :cond_9b

    #@134
    .line 5669
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@136
    invoke-interface {v5, v7}, Landroid/view/WindowManagerPolicy$WindowState;->showLw(Z)Z

    #@139
    move-result v5

    #@13a
    if-eqz v5, :cond_13e

    #@13c
    .line 5670
    or-int/lit8 v0, v0, 0x7

    #@13e
    .line 5674
    :cond_13e
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@140
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setHidden(Z)V

    #@143
    .line 5675
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@145
    new-instance v6, Lcom/android/internal/policy/impl/PhoneWindowManager$22;

    #@147
    invoke-direct {v6, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$22;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@14a
    invoke-virtual {v5, v6}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@14d
    goto/16 :goto_9b

    #@14f
    .line 5683
    :cond_14f
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWinDismissingKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@151
    .line 5684
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@153
    invoke-interface {v5, v7}, Landroid/view/WindowManagerPolicy$WindowState;->showLw(Z)Z

    #@156
    move-result v5

    #@157
    if-eqz v5, :cond_15b

    #@159
    .line 5685
    or-int/lit8 v0, v0, 0x7

    #@15b
    .line 5689
    :cond_15b
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@15d
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setHidden(Z)V

    #@160
    goto/16 :goto_9b
.end method

.method public focusChangedLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManagerPolicy$WindowState;)I
    .registers 4
    .parameter "lastFocus"
    .parameter "newFocus"

    #@0
    .prologue
    .line 5714
    iput-object p2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@2
    .line 5715
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateSystemUiVisibilityLw()I

    #@5
    move-result v0

    #@6
    and-int/lit8 v0, v0, 0xe

    #@8
    if-eqz v0, :cond_c

    #@a
    .line 5718
    const/4 v0, 0x1

    #@b
    .line 5720
    :goto_b
    return v0

    #@c
    :cond_c
    const/4 v0, 0x0

    #@d
    goto :goto_b
.end method

.method public getAboveUniverseLayer()I
    .registers 2

    #@0
    .prologue
    .line 2866
    const/16 v0, 0x7da

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->windowTypeToLayerLw(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getConfigDisplayHeight(III)I
    .registers 6
    .parameter "fullWidth"
    .parameter "fullHeight"
    .parameter "rotation"

    #@0
    .prologue
    .line 2909
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSystemNavBar:Z

    #@2
    if-nez v0, :cond_c

    #@4
    .line 2910
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getNonDecorDisplayHeight(III)I

    #@7
    move-result v0

    #@8
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarHeight:I

    #@a
    sub-int/2addr v0, v1

    #@b
    .line 2912
    :goto_b
    return v0

    #@c
    :cond_c
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getNonDecorDisplayHeight(III)I

    #@f
    move-result v0

    #@10
    goto :goto_b
.end method

.method public getConfigDisplayWidth(III)I
    .registers 5
    .parameter "fullWidth"
    .parameter "fullHeight"
    .parameter "rotation"

    #@0
    .prologue
    .line 2900
    invoke-virtual {p0, p1, p2, p3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getNonDecorDisplayWidth(III)I

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public getContentInsetHintLw(Landroid/view/WindowManager$LayoutParams;Landroid/graphics/Rect;)V
    .registers 11
    .parameter "attrs"
    .parameter "contentInset"

    #@0
    .prologue
    const v6, 0x10100

    #@3
    .line 4478
    iget v2, p1, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@5
    .line 4479
    .local v2, fl:I
    iget v4, p1, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    #@7
    iget v5, p1, Landroid/view/WindowManager$LayoutParams;->subtreeSystemUiVisibility:I

    #@9
    or-int v3, v4, v5

    #@b
    .line 4481
    .local v3, systemUiVisibility:I
    and-int v4, v2, v6

    #@d
    if-ne v4, v6, :cond_84

    #@f
    .line 4484
    iget-boolean v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCanHideNavigationBar:Z

    #@11
    if-eqz v4, :cond_3b

    #@13
    and-int/lit16 v4, v3, 0x200

    #@15
    if-eqz v4, :cond_3b

    #@17
    .line 4486
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@19
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@1b
    add-int v1, v4, v5

    #@1d
    .line 4487
    .local v1, availRight:I
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@1f
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@21
    add-int v0, v4, v5

    #@23
    .line 4492
    .local v0, availBottom:I
    :goto_23
    and-int/lit16 v4, v3, 0x100

    #@25
    if-eqz v4, :cond_58

    #@27
    .line 4493
    and-int/lit16 v4, v2, 0x400

    #@29
    if-eqz v4, :cond_48

    #@2b
    .line 4494
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenLeft:I

    #@2d
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenTop:I

    #@2f
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenRight:I

    #@31
    sub-int v6, v1, v6

    #@33
    iget v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableFullscreenBottom:I

    #@35
    sub-int v7, v0, v7

    #@37
    invoke-virtual {p2, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    #@3a
    .line 4514
    .end local v0           #availBottom:I
    .end local v1           #availRight:I
    :goto_3a
    return-void

    #@3b
    .line 4489
    :cond_3b
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@3d
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenWidth:I

    #@3f
    add-int v1, v4, v5

    #@41
    .line 4490
    .restart local v1       #availRight:I
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@43
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenHeight:I

    #@45
    add-int v0, v4, v5

    #@47
    .restart local v0       #availBottom:I
    goto :goto_23

    #@48
    .line 4498
    :cond_48
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableLeft:I

    #@4a
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableTop:I

    #@4c
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableRight:I

    #@4e
    sub-int v6, v1, v6

    #@50
    iget v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStableBottom:I

    #@52
    sub-int v7, v0, v7

    #@54
    invoke-virtual {p2, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    #@57
    goto :goto_3a

    #@58
    .line 4501
    :cond_58
    and-int/lit16 v4, v2, 0x400

    #@5a
    if-eqz v4, :cond_60

    #@5c
    .line 4502
    invoke-virtual {p2}, Landroid/graphics/Rect;->setEmpty()V

    #@5f
    goto :goto_3a

    #@60
    .line 4503
    :cond_60
    and-int/lit16 v4, v3, 0x404

    #@62
    if-nez v4, :cond_74

    #@64
    .line 4505
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurLeft:I

    #@66
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurTop:I

    #@68
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurRight:I

    #@6a
    sub-int v6, v1, v6

    #@6c
    iget v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurBottom:I

    #@6e
    sub-int v7, v0, v7

    #@70
    invoke-virtual {p2, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    #@73
    goto :goto_3a

    #@74
    .line 4508
    :cond_74
    iget v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurLeft:I

    #@76
    iget v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurTop:I

    #@78
    iget v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurRight:I

    #@7a
    sub-int v6, v1, v6

    #@7c
    iget v7, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurBottom:I

    #@7e
    sub-int v7, v0, v7

    #@80
    invoke-virtual {p2, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    #@83
    goto :goto_3a

    #@84
    .line 4513
    .end local v0           #availBottom:I
    .end local v1           #availRight:I
    :cond_84
    invoke-virtual {p2}, Landroid/graphics/Rect;->setEmpty()V

    #@87
    goto :goto_3a
.end method

.method public getMaxWallpaperLayer()I
    .registers 2

    #@0
    .prologue
    .line 2862
    const/16 v0, 0x7d0

    #@2
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->windowTypeToLayerLw(I)I

    #@5
    move-result v0

    #@6
    return v0
.end method

.method public getNonDecorDisplayHeight(III)I
    .registers 5
    .parameter "fullWidth"
    .parameter "fullHeight"
    .parameter "rotation"

    #@0
    .prologue
    .line 2885
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSystemNavBar:Z

    #@2
    if-eqz v0, :cond_a

    #@4
    .line 2887
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarHeightForRotation:[I

    #@6
    aget v0, v0, p3

    #@8
    sub-int/2addr p2, v0

    #@9
    .line 2896
    .end local p2
    :cond_9
    :goto_9
    return p2

    #@a
    .line 2889
    .restart local p2
    :cond_a
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasNavigationBar:Z

    #@c
    if-eqz v0, :cond_9

    #@e
    .line 2892
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarCanMove:Z

    #@10
    if-eqz v0, :cond_14

    #@12
    if-ge p1, p2, :cond_9

    #@14
    .line 2893
    :cond_14
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarHeightForRotation:[I

    #@16
    aget v0, v0, p3

    #@18
    sub-int/2addr p2, v0

    #@19
    goto :goto_9
.end method

.method public getNonDecorDisplayWidth(III)I
    .registers 5
    .parameter "fullWidth"
    .parameter "fullHeight"
    .parameter "rotation"

    #@0
    .prologue
    .line 2874
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasNavigationBar:Z

    #@2
    if-eqz v0, :cond_f

    #@4
    .line 2877
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarCanMove:Z

    #@6
    if-eqz v0, :cond_f

    #@8
    if-le p1, p2, :cond_f

    #@a
    .line 2878
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarWidthForRotation:[I

    #@c
    aget v0, v0, p3

    #@e
    sub-int/2addr p1, v0

    #@f
    .line 2881
    .end local p1
    :cond_f
    return p1
.end method

.method public getSKTLockState()Z
    .registers 3

    #@0
    .prologue
    .line 7892
    const-string v0, "WindowManager"

    #@2
    const-string v1, "[SKT Lock&Wipe] getSKTLockState()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 7893
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@c
    move-result v0

    #@d
    return v0
.end method

.method getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;
    .registers 3

    #@0
    .prologue
    .line 1028
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mServiceAquireLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 1029
    :try_start_3
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@5
    if-nez v0, :cond_13

    #@7
    .line 1030
    const-string v0, "statusbar"

    #@9
    invoke-static {v0}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    #@c
    move-result-object v0

    #@d
    invoke-static {v0}, Lcom/android/internal/statusbar/IStatusBarService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/android/internal/statusbar/IStatusBarService;

    #@10
    move-result-object v0

    #@11
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@13
    .line 1033
    :cond_13
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@15
    monitor-exit v1

    #@16
    return-object v0

    #@17
    .line 1034
    :catchall_17
    move-exception v0

    #@18
    monitor-exit v1
    :try_end_19
    .catchall {:try_start_3 .. :try_end_19} :catchall_17

    #@19
    throw v0
.end method

.method public getSystemDecorRectLw(Landroid/graphics/Rect;)I
    .registers 3
    .parameter "systemRect"

    #@0
    .prologue
    .line 4749
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemLeft:I

    #@2
    iput v0, p1, Landroid/graphics/Rect;->left:I

    #@4
    .line 4750
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemTop:I

    #@6
    iput v0, p1, Landroid/graphics/Rect;->top:I

    #@8
    .line 4751
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemRight:I

    #@a
    iput v0, p1, Landroid/graphics/Rect;->right:I

    #@c
    .line 4752
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemBottom:I

    #@e
    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    #@10
    .line 4768
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@12
    if-eqz v0, :cond_1b

    #@14
    .line 4769
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@16
    invoke-interface {v0}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    #@19
    move-result v0

    #@1a
    .line 4776
    :goto_1a
    return v0

    #@1b
    .line 4772
    :cond_1b
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@1d
    if-eqz v0, :cond_26

    #@1f
    .line 4773
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@21
    invoke-interface {v0}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    #@24
    move-result v0

    #@25
    goto :goto_1a

    #@26
    .line 4776
    :cond_26
    const/4 v0, 0x0

    #@27
    goto :goto_1a
.end method

.method goHome()Z
    .registers 16

    #@0
    .prologue
    const/4 v13, 0x0

    #@1
    const/4 v14, 0x1

    #@2
    .line 7741
    :try_start_2
    const-string v0, "persist.sys.uts-test-mode"

    #@4
    const/4 v1, 0x0

    #@5
    invoke-static {v0, v1}, Landroid/os/SystemProperties;->getInt(Ljava/lang/String;I)I

    #@8
    move-result v0

    #@9
    if-ne v0, v14, :cond_35

    #@b
    .line 7743
    const-string v0, "WindowManager"

    #@d
    const-string v1, "UTS-TEST-MODE"

    #@f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@12
    .line 7748
    :goto_12
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@15
    move-result-object v0

    #@16
    const/4 v1, 0x0

    #@17
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeIntent:Landroid/content/Intent;

    #@19
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeIntent:Landroid/content/Intent;

    #@1b
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1d
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@20
    move-result-object v4

    #@21
    invoke-virtual {v3, v4}, Landroid/content/Intent;->resolveTypeIfNeeded(Landroid/content/ContentResolver;)Ljava/lang/String;

    #@24
    move-result-object v3

    #@25
    const/4 v4, 0x0

    #@26
    const/4 v5, 0x0

    #@27
    const/4 v6, 0x0

    #@28
    const/4 v7, 0x1

    #@29
    const/4 v8, 0x0

    #@2a
    const/4 v9, 0x0

    #@2b
    const/4 v10, 0x0

    #@2c
    const/4 v11, -0x2

    #@2d
    invoke-interface/range {v0 .. v11}, Landroid/app/IActivityManager;->startActivityAsUser(Landroid/app/IApplicationThread;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IILjava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;I)I

    #@30
    move-result v12

    #@31
    .line 7754
    .local v12, result:I
    if-ne v12, v14, :cond_41

    #@33
    move v0, v13

    #@34
    .line 7761
    .end local v12           #result:I
    :goto_34
    return v0

    #@35
    .line 7745
    :cond_35
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@38
    move-result-object v0

    #@39
    invoke-interface {v0}, Landroid/app/IActivityManager;->stopAppSwitches()V

    #@3c
    .line 7746
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendCloseSystemWindows()V
    :try_end_3f
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_3f} :catch_40

    #@3f
    goto :goto_12

    #@40
    .line 7757
    :catch_40
    move-exception v0

    #@41
    :cond_41
    move v0, v14

    #@42
    .line 7761
    goto :goto_34
.end method

.method handleVolumeKey(II)V
    .registers 8
    .parameter "stream"
    .parameter "keycode"

    #@0
    .prologue
    .line 5974
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getAudioService()Landroid/media/IAudioService;

    #@3
    move-result-object v0

    #@4
    .line 5975
    .local v0, audioService:Landroid/media/IAudioService;
    if-nez v0, :cond_7

    #@6
    .line 5999
    :goto_6
    return-void

    #@7
    .line 5983
    :cond_7
    :try_start_7
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@9
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@c
    .line 5985
    if-nez p1, :cond_15

    #@e
    invoke-interface {v0}, Landroid/media/IAudioService;->isBluetoothScoOn()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_15

    #@14
    .line 5986
    const/4 p1, 0x6

    #@15
    .line 5989
    :cond_15
    const/16 v2, 0x18

    #@17
    if-ne p2, v2, :cond_24

    #@19
    const/4 v2, 0x1

    #@1a
    :goto_1a
    const/4 v3, 0x0

    #@1b
    invoke-interface {v0, p1, v2, v3}, Landroid/media/IAudioService;->adjustStreamVolume(III)V
    :try_end_1e
    .catchall {:try_start_7 .. :try_end_1e} :catchall_45
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_1e} :catch_26

    #@1e
    .line 5997
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@20
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@23
    goto :goto_6

    #@24
    .line 5989
    :cond_24
    const/4 v2, -0x1

    #@25
    goto :goto_1a

    #@26
    .line 5994
    :catch_26
    move-exception v1

    #@27
    .line 5995
    .local v1, e:Landroid/os/RemoteException;
    :try_start_27
    const-string v2, "WindowManager"

    #@29
    new-instance v3, Ljava/lang/StringBuilder;

    #@2b
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@2e
    const-string v4, "IAudioService.adjustStreamVolume() threw RemoteException "

    #@30
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@33
    move-result-object v3

    #@34
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@37
    move-result-object v3

    #@38
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@3b
    move-result-object v3

    #@3c
    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3f
    .catchall {:try_start_27 .. :try_end_3f} :catchall_45

    #@3f
    .line 5997
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@41
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    #@44
    goto :goto_6

    #@45
    .end local v1           #e:Landroid/os/RemoteException;
    :catchall_45
    move-exception v2

    #@46
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@48
    invoke-virtual {v3}, Landroid/os/PowerManager$WakeLock;->release()V

    #@4b
    throw v2
.end method

.method public hasNavigationBar()Z
    .registers 2

    #@0
    .prologue
    .line 7948
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasNavigationBar:Z

    #@2
    return v0
.end method

.method public hasSystemNavBar()Z
    .registers 2

    #@0
    .prologue
    .line 2870
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSystemNavBar:Z

    #@2
    return v0
.end method

.method public hideBootMessages()V
    .registers 3

    #@0
    .prologue
    .line 7554
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v1, Lcom/android/internal/policy/impl/PhoneWindowManager$39;

    #@4
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$39;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 7562
    return-void
.end method

.method public hideSKTLocked()V
    .registers 3

    #@0
    .prologue
    .line 7887
    const-string v0, "WindowManager"

    #@2
    const-string v1, "[SKT Lock&Wipe] hideSKTLocked()"

    #@4
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7
    .line 7888
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@9
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->hideSKTLocked()V

    #@c
    .line 7889
    return-void
.end method

.method public inKeyguardRestrictedKeyInputMode()Z
    .registers 2

    #@0
    .prologue
    .line 7178
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 7179
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isInputRestricted()Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public init(Landroid/content/Context;Landroid/view/IWindowManager;Landroid/view/WindowManagerPolicy$WindowManagerFuncs;)V
    .registers 16
    .parameter "context"
    .parameter "windowManager"
    .parameter "windowManagerFuncs"

    #@0
    .prologue
    const/high16 v11, 0x1020

    #@2
    const/4 v10, 0x0

    #@3
    const/4 v9, 0x1

    #@4
    const/4 v8, 0x0

    #@5
    .line 2046
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@7
    .line 2047
    iput-object p2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManager:Landroid/view/IWindowManager;

    #@9
    .line 2048
    iput-object p3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@b
    .line 2049
    const-string v5, "1"

    #@d
    const-string v6, "ro.config.headless"

    #@f
    const-string v7, "0"

    #@11
    invoke-static {v6, v7}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@14
    move-result-object v6

    #@15
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@18
    move-result v5

    #@19
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadless:Z

    #@1b
    .line 2050
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadless:Z

    #@1d
    if-nez v5, :cond_26

    #@1f
    .line 2052
    new-instance v5, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@21
    invoke-direct {v5, p1, v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;-><init>(Landroid/content/Context;Lcom/android/internal/widget/LockPatternUtils;)V

    #@24
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@26
    .line 2055
    :cond_26
    const-string v5, "ro.lge.lcdbreak_mode"

    #@28
    invoke-static {v5, v10}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@2b
    move-result v5

    #@2c
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@2e
    .line 2058
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@30
    if-eqz v5, :cond_390

    #@32
    .line 2059
    const-string v5, "lge.touchcrack_mode"

    #@34
    const-string v6, "1"

    #@36
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@39
    .line 2060
    iput v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTouchCrackMode:I

    #@3b
    .line 2066
    :goto_3b
    new-instance v5, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;

    #@3d
    invoke-direct {v5, p0, v8}, Lcom/android/internal/policy/impl/PhoneWindowManager$PolicyHandler;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Lcom/android/internal/policy/impl/PhoneWindowManager$1;)V

    #@40
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@42
    .line 2067
    new-instance v5, Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;

    #@44
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@46
    invoke-direct {v5, p0, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/content/Context;)V

    #@49
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationListener:Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;

    #@4b
    .line 2069
    :try_start_4b
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationListener:Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;

    #@4d
    invoke-interface {p2}, Landroid/view/IWindowManager;->getRotation()I

    #@50
    move-result v6

    #@51
    invoke-virtual {v5, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;->setCurrentRotation(I)V
    :try_end_54
    .catch Landroid/os/RemoteException; {:try_start_4b .. :try_end_54} :catch_3ac

    #@54
    .line 2071
    :goto_54
    new-instance v5, Lcom/android/internal/policy/impl/PhoneWindowManager$SettingsObserver;

    #@56
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@58
    invoke-direct {v5, p0, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager$SettingsObserver;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Landroid/os/Handler;)V

    #@5b
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSettingsObserver:Lcom/android/internal/policy/impl/PhoneWindowManager$SettingsObserver;

    #@5d
    .line 2072
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@5f
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@62
    move-result-object v5

    #@63
    const v6, 0x2060014

    #@66
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@69
    move-result v5

    #@6a
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHotKeyCustomizing:Z

    #@6c
    .line 2084
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSettingsObserver:Lcom/android/internal/policy/impl/PhoneWindowManager$SettingsObserver;

    #@6e
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/PhoneWindowManager$SettingsObserver;->observe()V

    #@71
    .line 2085
    new-instance v5, Lcom/android/internal/policy/impl/ShortcutManager;

    #@73
    iget-object v6, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@75
    invoke-direct {v5, p1, v6}, Lcom/android/internal/policy/impl/ShortcutManager;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    #@78
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShortcutManager:Lcom/android/internal/policy/impl/ShortcutManager;

    #@7a
    .line 2086
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShortcutManager:Lcom/android/internal/policy/impl/ShortcutManager;

    #@7c
    invoke-virtual {v5}, Lcom/android/internal/policy/impl/ShortcutManager;->observe()V

    #@7f
    .line 2087
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@82
    move-result-object v5

    #@83
    const v6, 0x10e0013

    #@86
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    #@89
    move-result v5

    #@8a
    iput v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUiMode:I

    #@8c
    .line 2088
    new-instance v5, Landroid/content/Intent;

    #@8e
    const-string v6, "android.intent.action.MAIN"

    #@90
    invoke-direct {v5, v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@93
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeIntent:Landroid/content/Intent;

    #@95
    .line 2089
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeIntent:Landroid/content/Intent;

    #@97
    const-string v6, "android.intent.category.HOME"

    #@99
    invoke-virtual {v5, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@9c
    .line 2090
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeIntent:Landroid/content/Intent;

    #@9e
    invoke-virtual {v5, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@a1
    .line 2092
    new-instance v5, Landroid/content/Intent;

    #@a3
    const-string v6, "android.intent.action.MAIN"

    #@a5
    invoke-direct {v5, v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@a8
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockIntent:Landroid/content/Intent;

    #@aa
    .line 2093
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockIntent:Landroid/content/Intent;

    #@ac
    const-string v6, "android.intent.category.CAR_DOCK"

    #@ae
    invoke-virtual {v5, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@b1
    .line 2094
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockIntent:Landroid/content/Intent;

    #@b3
    invoke-virtual {v5, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@b6
    .line 2096
    new-instance v5, Landroid/content/Intent;

    #@b8
    const-string v6, "android.intent.action.MAIN"

    #@ba
    invoke-direct {v5, v6, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    #@bd
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockIntent:Landroid/content/Intent;

    #@bf
    .line 2097
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockIntent:Landroid/content/Intent;

    #@c1
    const-string v6, "android.intent.category.DESK_DOCK"

    #@c3
    invoke-virtual {v5, v6}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    #@c6
    .line 2098
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockIntent:Landroid/content/Intent;

    #@c8
    invoke-virtual {v5, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@cb
    .line 2101
    new-instance v5, Landroid/content/Intent;

    #@cd
    const-string v6, "com.lge.safetycare.action.KEY_EVENT"

    #@cf
    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@d2
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSafetyCareIntent:Landroid/content/Intent;

    #@d4
    .line 2103
    const-string v5, "power"

    #@d6
    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@d9
    move-result-object v5

    #@da
    check-cast v5, Landroid/os/PowerManager;

    #@dc
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@de
    .line 2104
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@e0
    const-string v6, "PhoneWindowManager.mBroadcastWakeLock"

    #@e2
    invoke-virtual {v5, v9, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@e5
    move-result-object v5

    #@e6
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@e8
    .line 2106
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@ea
    const-string v6, "PhoneWindowManager.mVolumeLongKeyWakeLock"

    #@ec
    invoke-virtual {v5, v9, v6}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    #@ef
    move-result-object v5

    #@f0
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeLongKeyWakeLock:Landroid/os/PowerManager$WakeLock;

    #@f2
    .line 2108
    const-string v5, "1"

    #@f4
    const-string v6, "ro.debuggable"

    #@f6
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f9
    move-result-object v6

    #@fa
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@fd
    move-result v5

    #@fe
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEnableShiftMenuBugReports:Z

    #@100
    .line 2109
    const v5, 0x10e0010

    #@103
    invoke-direct {p0, v5}, Lcom/android/internal/policy/impl/PhoneWindowManager;->readRotation(I)I

    #@106
    move-result v5

    #@107
    iput v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidOpenRotation:I

    #@109
    .line 2111
    const v5, 0x10e0012

    #@10c
    invoke-direct {p0, v5}, Lcom/android/internal/policy/impl/PhoneWindowManager;->readRotation(I)I

    #@10f
    move-result v5

    #@110
    iput v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockRotation:I

    #@112
    .line 2113
    const v5, 0x10e0011

    #@115
    invoke-direct {p0, v5}, Lcom/android/internal/policy/impl/PhoneWindowManager;->readRotation(I)I

    #@118
    move-result v5

    #@119
    iput v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockRotation:I

    #@11b
    .line 2115
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@11d
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@120
    move-result-object v5

    #@121
    const v6, 0x1110020

    #@124
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@127
    move-result v5

    #@128
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockEnablesAccelerometer:Z

    #@12a
    .line 2117
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@12c
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@12f
    move-result-object v5

    #@130
    const v6, 0x111001f

    #@133
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@136
    move-result v5

    #@137
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockEnablesAccelerometer:Z

    #@139
    .line 2119
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@13b
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@13e
    move-result-object v5

    #@13f
    const v6, 0x10e0016

    #@142
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    #@145
    move-result v5

    #@146
    iput v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidKeyboardAccessibility:I

    #@148
    .line 2121
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@14a
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@14d
    move-result-object v5

    #@14e
    const v6, 0x10e0017

    #@151
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    #@154
    move-result v5

    #@155
    iput v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidNavigationAccessibility:I

    #@157
    .line 2123
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@159
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@15c
    move-result-object v5

    #@15d
    const v6, 0x1110021

    #@160
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@163
    move-result v5

    #@164
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidControlsSleep:Z

    #@166
    .line 2125
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@168
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@16b
    move-result-object v5

    #@16c
    const v6, 0x206000c

    #@16f
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@172
    move-result v5

    #@173
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenulongEnabled:Z

    #@175
    .line 2127
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@177
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@17a
    move-result-object v5

    #@17b
    const v6, 0x206000d

    #@17e
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@181
    move-result v5

    #@182
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenulongToSearch:Z

    #@184
    .line 2129
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@186
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@189
    move-result-object v5

    #@18a
    const v6, 0x2060013

    #@18d
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@190
    move-result v5

    #@191
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@193
    .line 2131
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@195
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@198
    move-result-object v5

    #@199
    const v6, 0x2060021

    #@19c
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@19f
    move-result v5

    #@1a0
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppAvailable:Z

    #@1a2
    .line 2133
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1a4
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1a7
    move-result-object v5

    #@1a8
    const v6, 0x2060023

    #@1ab
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@1ae
    move-result v5

    #@1af
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRearSideKeyEnable:Z

    #@1b1
    .line 2136
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1b3
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@1b6
    move-result-object v5

    #@1b7
    const v6, 0x2060036

    #@1ba
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@1bd
    move-result v5

    #@1be
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@1c0
    .line 2140
    new-instance v3, Landroid/content/IntentFilter;

    #@1c2
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@1c5
    .line 2141
    .local v3, filter:Landroid/content/IntentFilter;
    sget-object v5, Landroid/app/UiModeManager;->ACTION_ENTER_CAR_MODE:Ljava/lang/String;

    #@1c7
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1ca
    .line 2142
    sget-object v5, Landroid/app/UiModeManager;->ACTION_EXIT_CAR_MODE:Ljava/lang/String;

    #@1cc
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1cf
    .line 2143
    sget-object v5, Landroid/app/UiModeManager;->ACTION_ENTER_DESK_MODE:Ljava/lang/String;

    #@1d1
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d4
    .line 2144
    sget-object v5, Landroid/app/UiModeManager;->ACTION_EXIT_DESK_MODE:Ljava/lang/String;

    #@1d6
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1d9
    .line 2145
    const-string v5, "android.intent.action.DOCK_EVENT"

    #@1db
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1de
    .line 2146
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockReceiver:Landroid/content/BroadcastReceiver;

    #@1e0
    invoke-virtual {p1, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@1e3
    move-result-object v4

    #@1e4
    .line 2147
    .local v4, intent:Landroid/content/Intent;
    if-eqz v4, :cond_1ee

    #@1e6
    .line 2149
    const-string v5, "android.intent.extra.DOCK_STATE"

    #@1e8
    invoke-virtual {v4, v5, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    #@1eb
    move-result v5

    #@1ec
    iput v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@1ee
    .line 2154
    :cond_1ee
    new-instance v3, Landroid/content/IntentFilter;

    #@1f0
    .end local v3           #filter:Landroid/content/IntentFilter;
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@1f3
    .line 2155
    .restart local v3       #filter:Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.DREAMING_STARTED"

    #@1f5
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1f8
    .line 2156
    const-string v5, "android.intent.action.DREAMING_STOPPED"

    #@1fa
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@1fd
    .line 2157
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDreamReceiver:Landroid/content/BroadcastReceiver;

    #@1ff
    invoke-virtual {p1, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@202
    .line 2160
    new-instance v3, Landroid/content/IntentFilter;

    #@204
    .end local v3           #filter:Landroid/content/IntentFilter;
    const-string v5, "android.intent.action.USER_SWITCHED"

    #@206
    invoke-direct {v3, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@209
    .line 2161
    .restart local v3       #filter:Landroid/content/IntentFilter;
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMultiuserReceiver:Landroid/content/BroadcastReceiver;

    #@20b
    invoke-virtual {p1, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@20e
    .line 2165
    new-instance v3, Landroid/content/IntentFilter;

    #@210
    .end local v3           #filter:Landroid/content/IntentFilter;
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@213
    .line 2166
    .restart local v3       #filter:Landroid/content/IntentFilter;
    const-string v5, "com.lge.android.intent.action.TOUCH_PALM_COVER"

    #@215
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@218
    .line 2167
    const-string v5, "com.lge.android.intent.action.TOUCH_PALM_SWIPE_UP"

    #@21a
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@21d
    .line 2168
    const-string v5, "com.lge.android.intent.action.TOUCH_PALM_SWIPE_DOWN"

    #@21f
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@222
    .line 2169
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPalmGestureReceiver:Landroid/content/BroadcastReceiver;

    #@224
    invoke-virtual {p1, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@227
    .line 2173
    new-instance v3, Landroid/content/IntentFilter;

    #@229
    .end local v3           #filter:Landroid/content/IntentFilter;
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@22c
    .line 2174
    .restart local v3       #filter:Landroid/content/IntentFilter;
    const-string v5, "com.lge.statusbar.expanded"

    #@22e
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@231
    .line 2175
    const-string v5, "com.lge.statusbar.collapsed"

    #@233
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@236
    .line 2176
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarReceiver:Landroid/content/BroadcastReceiver;

    #@238
    invoke-virtual {p1, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@23b
    .line 2179
    new-instance v3, Landroid/content/IntentFilter;

    #@23d
    .end local v3           #filter:Landroid/content/IntentFilter;
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@240
    .line 2180
    .restart local v3       #filter:Landroid/content/IntentFilter;
    const-string v5, "com.lge.android.intent.action.ACCESSORY_EVENT"

    #@242
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@245
    .line 2181
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSmartCoverReceiver:Landroid/content/BroadcastReceiver;

    #@247
    invoke-virtual {p1, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@24a
    .line 2183
    new-instance v3, Landroid/content/IntentFilter;

    #@24c
    .end local v3           #filter:Landroid/content/IntentFilter;
    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    #@24f
    .line 2184
    .restart local v3       #filter:Landroid/content/IntentFilter;
    const-string v5, "com.lge.aat.navigation.hide"

    #@251
    invoke-virtual {v3, v5}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    #@254
    .line 2185
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNaviReceiver:Landroid/content/BroadcastReceiver;

    #@256
    invoke-virtual {p1, v5, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@259
    .line 2188
    :try_start_259
    sget-object v5, Lcom/lge/loader/RuntimeLibraryLoader;->VOLUME_MANAGER:Ljava/lang/String;

    #@25b
    invoke-static {v5}, Lcom/lge/loader/RuntimeLibraryLoader;->getCreator(Ljava/lang/String;)Lcom/lge/loader/InstanceCreator;

    #@25e
    move-result-object v5

    #@25f
    const/4 v6, 0x0

    #@260
    invoke-virtual {v5, v6}, Lcom/lge/loader/InstanceCreator;->newInstance(Ljava/lang/Object;)Ljava/lang/Object;

    #@263
    move-result-object v5

    #@264
    check-cast v5, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@266
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;
    :try_end_268
    .catch Ljava/lang/NullPointerException; {:try_start_259 .. :try_end_268} :catch_39b
    .catch Ljava/lang/Exception; {:try_start_259 .. :try_end_268} :catch_3a1

    #@268
    .line 2207
    :goto_268
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@26a
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@26d
    move-result-object v5

    #@26e
    const v6, 0x107002b

    #@271
    invoke-static {v5, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getLongIntArray(Landroid/content/res/Resources;I)[J

    #@274
    move-result-object v5

    #@275
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressVibePattern:[J

    #@277
    .line 2209
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@279
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@27c
    move-result-object v5

    #@27d
    const v6, 0x107002c

    #@280
    invoke-static {v5, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getLongIntArray(Landroid/content/res/Resources;I)[J

    #@283
    move-result-object v5

    #@284
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVirtualKeyVibePattern:[J

    #@286
    .line 2211
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@288
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@28b
    move-result-object v5

    #@28c
    const v6, 0x107002d

    #@28f
    invoke-static {v5, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getLongIntArray(Landroid/content/res/Resources;I)[J

    #@292
    move-result-object v5

    #@293
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyboardTapVibePattern:[J

    #@295
    .line 2213
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@297
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@29a
    move-result-object v5

    #@29b
    const v6, 0x107002e

    #@29e
    invoke-static {v5, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getLongIntArray(Landroid/content/res/Resources;I)[J

    #@2a1
    move-result-object v5

    #@2a2
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSafeModeDisabledVibePattern:[J

    #@2a4
    .line 2215
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2a6
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2a9
    move-result-object v5

    #@2aa
    const v6, 0x107002f

    #@2ad
    invoke-static {v5, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getLongIntArray(Landroid/content/res/Resources;I)[J

    #@2b0
    move-result-object v5

    #@2b1
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSafeModeEnabledVibePattern:[J

    #@2b3
    .line 2218
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2b5
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@2b8
    move-result-object v5

    #@2b9
    const v6, 0x111001c

    #@2bc
    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@2bf
    move-result v5

    #@2c0
    iput-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotChordEnabled:Z

    #@2c2
    .line 2221
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAutocallReceiver:Landroid/content/BroadcastReceiver;

    #@2c4
    new-instance v6, Landroid/content/IntentFilter;

    #@2c6
    const-string v7, "android.intent.action.AUTOCALL_ENABLE"

    #@2c8
    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2cb
    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2ce
    .line 2224
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTouchCrackModeReceiver:Landroid/content/BroadcastReceiver;

    #@2d0
    new-instance v6, Landroid/content/IntentFilter;

    #@2d2
    const-string v7, "com.lge.android.intent.action.TOUCHCRACK_MODE_EVENT"

    #@2d4
    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2d7
    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2da
    .line 2225
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTouchCrackModeObserver:Landroid/os/UEventObserver;

    #@2dc
    const-string v6, "DEVPATH=/devices/system/lge_touch/lge_touch0"

    #@2de
    invoke-virtual {v5, v6}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@2e1
    .line 2228
    iget-boolean v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppAvailable:Z

    #@2e3
    if-eqz v5, :cond_2fd

    #@2e5
    .line 2229
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppReceiver:Landroid/content/BroadcastReceiver;

    #@2e7
    new-instance v6, Landroid/content/IntentFilter;

    #@2e9
    const-string v7, "android.intent.action.NEW_OUTGOING_CALL"

    #@2eb
    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2ee
    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2f1
    .line 2230
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppReceiver:Landroid/content/BroadcastReceiver;

    #@2f3
    new-instance v6, Landroid/content/IntentFilter;

    #@2f5
    const-string v7, "android.intent.KOR_GO_POLICE_REPORT"

    #@2f7
    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@2fa
    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@2fd
    .line 2234
    :cond_2fd
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGlobalActionReceiver:Landroid/content/BroadcastReceiver;

    #@2ff
    new-instance v6, Landroid/content/IntentFilter;

    #@301
    const-string v7, "com.lge.android.intent.action.GLOBAL_ACTION_SHOW"

    #@303
    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@306
    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@309
    .line 2235
    const-string v5, "ro.build.target_operator"

    #@30b
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@30e
    move-result-object v5

    #@30f
    const-string v6, "SKT"

    #@311
    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@314
    move-result v5

    #@315
    if-eqz v5, :cond_331

    #@317
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isDeviceProvisioned()Z

    #@31a
    move-result v5

    #@31b
    if-nez v5, :cond_331

    #@31d
    .line 2236
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@31f
    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@322
    move-result-object v5

    #@323
    const-string v6, "t_entry_mode"

    #@325
    const-string v7, "false"

    #@327
    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    #@32a
    .line 2237
    const-string v5, "PackageManager"

    #@32c
    const-string v6, "Setting t_entry_mode DB -> false"

    #@32e
    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@331
    .line 2240
    :cond_331
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQucikBTNAnswerSettingReceiver:Landroid/content/BroadcastReceiver;

    #@333
    new-instance v6, Landroid/content/IntentFilter;

    #@335
    const-string v7, "com.lge.phone.action.QUICK_BUTTON_SETTING"

    #@337
    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@33a
    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@33d
    .line 2243
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->initializeHdmiState()V

    #@340
    .line 2246
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@342
    invoke-virtual {v5}, Landroid/os/PowerManager;->isScreenOn()Z

    #@345
    move-result v5

    #@346
    if-eqz v5, :cond_3a7

    #@348
    .line 2247
    invoke-virtual {p0, v8}, Lcom/android/internal/policy/impl/PhoneWindowManager;->screenTurningOn(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    #@34b
    .line 2254
    :goto_34b
    const-string v5, "ro.build.target_operator"

    #@34d
    const-string v6, ""

    #@34f
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@352
    move-result-object v0

    #@353
    .line 2255
    .local v0, carrier:Ljava/lang/String;
    const-string v5, "ro.build.target_country"

    #@355
    const-string v6, ""

    #@357
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@35a
    move-result-object v1

    #@35b
    .line 2256
    .local v1, country:Ljava/lang/String;
    if-eqz v0, :cond_371

    #@35d
    if-eqz v1, :cond_371

    #@35f
    const-string v5, "TMO"

    #@361
    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@364
    move-result v5

    #@365
    if-eqz v5, :cond_371

    #@367
    const-string v5, "US"

    #@369
    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@36c
    move-result v5

    #@36d
    if-eqz v5, :cond_371

    #@36f
    .line 2257
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIsTargetTmoUs:Z

    #@371
    .line 2262
    :cond_371
    const-string v5, "ro.build.target_operator"

    #@373
    invoke-static {v5}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@376
    move-result-object v5

    #@377
    iput-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOperator:Ljava/lang/String;

    #@379
    .line 2263
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOperator:Ljava/lang/String;

    #@37b
    const-string v6, "VZW"

    #@37d
    invoke-virtual {v5, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@380
    move-result v5

    #@381
    if-eqz v5, :cond_38f

    #@383
    .line 2264
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->spcErrReceiver:Landroid/content/BroadcastReceiver;

    #@385
    new-instance v6, Landroid/content/IntentFilter;

    #@387
    const-string v7, "com.lge.intent.action.EXCESS_SPC_FAIL_EVENT"

    #@389
    invoke-direct {v6, v7}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    #@38c
    invoke-virtual {p1, v5, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    #@38f
    .line 2267
    :cond_38f
    return-void

    #@390
    .line 2062
    .end local v0           #carrier:Ljava/lang/String;
    .end local v1           #country:Ljava/lang/String;
    .end local v3           #filter:Landroid/content/IntentFilter;
    .end local v4           #intent:Landroid/content/Intent;
    :cond_390
    const-string v5, "lge.touchcrack_mode"

    #@392
    const-string v6, "0"

    #@394
    invoke-static {v5, v6}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    #@397
    .line 2063
    iput v10, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTouchCrackMode:I

    #@399
    goto/16 :goto_3b

    #@39b
    .line 2192
    .restart local v3       #filter:Landroid/content/IntentFilter;
    .restart local v4       #intent:Landroid/content/Intent;
    :catch_39b
    move-exception v2

    #@39c
    .line 2193
    .local v2, e:Ljava/lang/NullPointerException;
    invoke-virtual {v2}, Ljava/lang/NullPointerException;->printStackTrace()V

    #@39f
    goto/16 :goto_268

    #@3a1
    .line 2194
    .end local v2           #e:Ljava/lang/NullPointerException;
    :catch_3a1
    move-exception v2

    #@3a2
    .line 2195
    .local v2, e:Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    #@3a5
    goto/16 :goto_268

    #@3a7
    .line 2249
    .end local v2           #e:Ljava/lang/Exception;
    :cond_3a7
    const/4 v5, 0x2

    #@3a8
    invoke-virtual {p0, v5}, Lcom/android/internal/policy/impl/PhoneWindowManager;->screenTurnedOff(I)V

    #@3ab
    goto :goto_34b

    #@3ac
    .line 2070
    .end local v3           #filter:Landroid/content/IntentFilter;
    .end local v4           #intent:Landroid/content/Intent;
    :catch_3ac
    move-exception v5

    #@3ad
    goto/16 :goto_54
.end method

.method initializeHdmiState()V
    .registers 13

    #@0
    .prologue
    const/4 v7, 0x1

    #@1
    const/4 v8, 0x0

    #@2
    .line 5768
    const/4 v4, 0x0

    #@3
    .line 5770
    .local v4, plugged:Z
    new-instance v9, Ljava/io/File;

    #@5
    const-string v10, "/sys/devices/virtual/switch/hdmi/state"

    #@7
    invoke-direct {v9, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    #@a
    invoke-virtual {v9}, Ljava/io/File;->exists()Z

    #@d
    move-result v9

    #@e
    if-eqz v9, :cond_3f

    #@10
    .line 5771
    iget-object v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHDMIObserver:Landroid/os/UEventObserver;

    #@12
    const-string v10, "DEVPATH=/devices/virtual/switch/hdmi"

    #@14
    invoke-virtual {v9, v10}, Landroid/os/UEventObserver;->startObserving(Ljava/lang/String;)V

    #@17
    .line 5773
    const-string v2, "/sys/class/switch/hdmi/state"

    #@19
    .line 5774
    .local v2, filename:Ljava/lang/String;
    const/4 v5, 0x0

    #@1a
    .line 5776
    .local v5, reader:Ljava/io/FileReader;
    :try_start_1a
    new-instance v6, Ljava/io/FileReader;

    #@1c
    const-string v9, "/sys/class/switch/hdmi/state"

    #@1e
    invoke-direct {v6, v9}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V
    :try_end_21
    .catchall {:try_start_1a .. :try_end_21} :catchall_90
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_21} :catch_4e
    .catch Ljava/lang/NumberFormatException; {:try_start_1a .. :try_end_21} :catch_6f

    #@21
    .line 5777
    .end local v5           #reader:Ljava/io/FileReader;
    .local v6, reader:Ljava/io/FileReader;
    const/16 v9, 0xf

    #@23
    :try_start_23
    new-array v0, v9, [C

    #@25
    .line 5778
    .local v0, buf:[C
    invoke-virtual {v6, v0}, Ljava/io/FileReader;->read([C)I

    #@28
    move-result v3

    #@29
    .line 5779
    .local v3, n:I
    if-le v3, v7, :cond_3a

    #@2b
    .line 5780
    new-instance v9, Ljava/lang/String;

    #@2d
    const/4 v10, 0x0

    #@2e
    add-int/lit8 v11, v3, -0x1

    #@30
    invoke-direct {v9, v0, v10, v11}, Ljava/lang/String;-><init>([CII)V

    #@33
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_36
    .catchall {:try_start_23 .. :try_end_36} :catchall_9f
    .catch Ljava/io/IOException; {:try_start_23 .. :try_end_36} :catch_a5
    .catch Ljava/lang/NumberFormatException; {:try_start_23 .. :try_end_36} :catch_a2

    #@36
    move-result v9

    #@37
    if-eqz v9, :cond_4c

    #@39
    move v4, v7

    #@3a
    .line 5787
    :cond_3a
    :goto_3a
    if-eqz v6, :cond_3f

    #@3c
    .line 5789
    :try_start_3c
    invoke-virtual {v6}, Ljava/io/FileReader;->close()V
    :try_end_3f
    .catch Ljava/io/IOException; {:try_start_3c .. :try_end_3f} :catch_9b

    #@3f
    .line 5797
    .end local v0           #buf:[C
    .end local v2           #filename:Ljava/lang/String;
    .end local v3           #n:I
    .end local v6           #reader:Ljava/io/FileReader;
    :cond_3f
    :goto_3f
    if-nez v4, :cond_97

    #@41
    move v9, v7

    #@42
    :goto_42
    iput-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiPlugged:Z

    #@44
    .line 5798
    iget-boolean v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiPlugged:Z

    #@46
    if-nez v9, :cond_99

    #@48
    :goto_48
    invoke-virtual {p0, v7}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setHdmiPlugged(Z)V

    #@4b
    .line 5799
    return-void

    #@4c
    .restart local v0       #buf:[C
    .restart local v2       #filename:Ljava/lang/String;
    .restart local v3       #n:I
    .restart local v6       #reader:Ljava/io/FileReader;
    :cond_4c
    move v4, v8

    #@4d
    .line 5780
    goto :goto_3a

    #@4e
    .line 5782
    .end local v0           #buf:[C
    .end local v3           #n:I
    .end local v6           #reader:Ljava/io/FileReader;
    .restart local v5       #reader:Ljava/io/FileReader;
    :catch_4e
    move-exception v1

    #@4f
    .line 5783
    .local v1, ex:Ljava/io/IOException;
    :goto_4f
    :try_start_4f
    const-string v9, "WindowManager"

    #@51
    new-instance v10, Ljava/lang/StringBuilder;

    #@53
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@56
    const-string v11, "Couldn\'t read hdmi state from /sys/class/switch/hdmi/state: "

    #@58
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5b
    move-result-object v10

    #@5c
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@5f
    move-result-object v10

    #@60
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@63
    move-result-object v10

    #@64
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_67
    .catchall {:try_start_4f .. :try_end_67} :catchall_90

    #@67
    .line 5787
    if-eqz v5, :cond_3f

    #@69
    .line 5789
    :try_start_69
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_6c
    .catch Ljava/io/IOException; {:try_start_69 .. :try_end_6c} :catch_6d

    #@6c
    goto :goto_3f

    #@6d
    .line 5790
    :catch_6d
    move-exception v9

    #@6e
    goto :goto_3f

    #@6f
    .line 5784
    .end local v1           #ex:Ljava/io/IOException;
    :catch_6f
    move-exception v1

    #@70
    .line 5785
    .local v1, ex:Ljava/lang/NumberFormatException;
    :goto_70
    :try_start_70
    const-string v9, "WindowManager"

    #@72
    new-instance v10, Ljava/lang/StringBuilder;

    #@74
    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    #@77
    const-string v11, "Couldn\'t read hdmi state from /sys/class/switch/hdmi/state: "

    #@79
    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v10

    #@7d
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@80
    move-result-object v10

    #@81
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@84
    move-result-object v10

    #@85
    invoke-static {v9, v10}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_88
    .catchall {:try_start_70 .. :try_end_88} :catchall_90

    #@88
    .line 5787
    if-eqz v5, :cond_3f

    #@8a
    .line 5789
    :try_start_8a
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_8d
    .catch Ljava/io/IOException; {:try_start_8a .. :try_end_8d} :catch_8e

    #@8d
    goto :goto_3f

    #@8e
    .line 5790
    :catch_8e
    move-exception v9

    #@8f
    goto :goto_3f

    #@90
    .line 5787
    .end local v1           #ex:Ljava/lang/NumberFormatException;
    :catchall_90
    move-exception v7

    #@91
    :goto_91
    if-eqz v5, :cond_96

    #@93
    .line 5789
    :try_start_93
    invoke-virtual {v5}, Ljava/io/FileReader;->close()V
    :try_end_96
    .catch Ljava/io/IOException; {:try_start_93 .. :try_end_96} :catch_9d

    #@96
    .line 5791
    :cond_96
    :goto_96
    throw v7

    #@97
    .end local v2           #filename:Ljava/lang/String;
    .end local v5           #reader:Ljava/io/FileReader;
    :cond_97
    move v9, v8

    #@98
    .line 5797
    goto :goto_42

    #@99
    :cond_99
    move v7, v8

    #@9a
    .line 5798
    goto :goto_48

    #@9b
    .line 5790
    .restart local v0       #buf:[C
    .restart local v2       #filename:Ljava/lang/String;
    .restart local v3       #n:I
    .restart local v6       #reader:Ljava/io/FileReader;
    :catch_9b
    move-exception v9

    #@9c
    goto :goto_3f

    #@9d
    .end local v0           #buf:[C
    .end local v3           #n:I
    .end local v6           #reader:Ljava/io/FileReader;
    .restart local v5       #reader:Ljava/io/FileReader;
    :catch_9d
    move-exception v8

    #@9e
    goto :goto_96

    #@9f
    .line 5787
    .end local v5           #reader:Ljava/io/FileReader;
    .restart local v6       #reader:Ljava/io/FileReader;
    :catchall_9f
    move-exception v7

    #@a0
    move-object v5, v6

    #@a1
    .end local v6           #reader:Ljava/io/FileReader;
    .restart local v5       #reader:Ljava/io/FileReader;
    goto :goto_91

    #@a2
    .line 5784
    .end local v5           #reader:Ljava/io/FileReader;
    .restart local v6       #reader:Ljava/io/FileReader;
    :catch_a2
    move-exception v1

    #@a3
    move-object v5, v6

    #@a4
    .end local v6           #reader:Ljava/io/FileReader;
    .restart local v5       #reader:Ljava/io/FileReader;
    goto :goto_70

    #@a5
    .line 5782
    .end local v5           #reader:Ljava/io/FileReader;
    .restart local v6       #reader:Ljava/io/FileReader;
    :catch_a5
    move-exception v1

    #@a6
    move-object v5, v6

    #@a7
    .end local v6           #reader:Ljava/io/FileReader;
    .restart local v5       #reader:Ljava/io/FileReader;
    goto :goto_4f
.end method

.method public interceptKeyBeforeDispatching(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;I)J
    .registers 52
    .parameter "win"
    .parameter "event"
    .parameter "policyFlags"

    #@0
    .prologue
    .line 3376
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->keyguardOn()Z

    #@3
    move-result v31

    #@4
    .line 3377
    .local v31, keyguardOn:Z
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getKeyCode()I

    #@7
    move-result v30

    #@8
    .line 3378
    .local v30, keyCode:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@b
    move-result v36

    #@c
    .line 3379
    .local v36, repeatCount:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getMetaState()I

    #@f
    move-result v33

    #@10
    .line 3380
    .local v33, metaState:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    #@13
    move-result v25

    #@14
    .line 3381
    .local v25, flags:I
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    #@17
    move-result v4

    #@18
    if-nez v4, :cond_98

    #@1a
    const/16 v21, 0x1

    #@1c
    .line 3382
    .local v21, down:Z
    :goto_1c
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->isCanceled()Z

    #@1f
    move-result v17

    #@20
    .line 3383
    .local v17, canceled:Z
    const-string v4, "kids"

    #@22
    const-string v6, "service.plushome.currenthome"

    #@24
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v6

    #@28
    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@2b
    move-result v32

    #@2c
    .line 3386
    .local v32, kidsModeOn:Z
    const-string v4, "WindowManager"

    #@2e
    new-instance v6, Ljava/lang/StringBuilder;

    #@30
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@33
    const-string v7, "interceptKeyTi keyCode="

    #@35
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@38
    move-result-object v6

    #@39
    move/from16 v0, v30

    #@3b
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@3e
    move-result-object v6

    #@3f
    const-string v7, " down="

    #@41
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@44
    move-result-object v6

    #@45
    move/from16 v0, v21

    #@47
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@4a
    move-result-object v6

    #@4b
    const-string v7, " repeatCount="

    #@4d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@50
    move-result-object v6

    #@51
    move/from16 v0, v36

    #@53
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@56
    move-result-object v6

    #@57
    const-string v7, " keyguardOn="

    #@59
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5c
    move-result-object v6

    #@5d
    move/from16 v0, v31

    #@5f
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@62
    move-result-object v6

    #@63
    const-string v7, " mHomePressed="

    #@65
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@68
    move-result-object v6

    #@69
    move-object/from16 v0, p0

    #@6b
    iget-boolean v7, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomePressed:Z

    #@6d
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@70
    move-result-object v6

    #@71
    const-string v7, " canceled="

    #@73
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76
    move-result-object v6

    #@77
    move/from16 v0, v17

    #@79
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@7c
    move-result-object v6

    #@7d
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@80
    move-result-object v6

    #@81
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@84
    .line 3392
    move-object/from16 v0, p0

    #@86
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOperator:Ljava/lang/String;

    #@88
    const-string v6, "VZW"

    #@8a
    invoke-virtual {v4, v6}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@8d
    move-result v4

    #@8e
    if-eqz v4, :cond_9b

    #@90
    .line 3393
    sget-boolean v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->SPC_ERR_FREEZE:Z

    #@92
    const/4 v6, 0x1

    #@93
    if-ne v4, v6, :cond_9b

    #@95
    .line 3394
    const-wide/16 v6, -0x1

    #@97
    .line 4069
    :goto_97
    return-wide v6

    #@98
    .line 3381
    .end local v17           #canceled:Z
    .end local v21           #down:Z
    .end local v32           #kidsModeOn:Z
    :cond_98
    const/16 v21, 0x0

    #@9a
    goto :goto_1c

    #@9b
    .line 3401
    .restart local v17       #canceled:Z
    .restart local v21       #down:Z
    .restart local v32       #kidsModeOn:Z
    :cond_9b
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@9e
    move-result-object v4

    #@9f
    if-eqz v4, :cond_d1

    #@a1
    .line 3402
    const/4 v4, 0x3

    #@a2
    move/from16 v0, v30

    #@a4
    if-eq v0, v4, :cond_b7

    #@a6
    const/4 v4, 0x4

    #@a7
    move/from16 v0, v30

    #@a9
    if-eq v0, v4, :cond_b7

    #@ab
    const/16 v4, 0x52

    #@ad
    move/from16 v0, v30

    #@af
    if-eq v0, v4, :cond_b7

    #@b1
    const/16 v4, 0xbb

    #@b3
    move/from16 v0, v30

    #@b5
    if-ne v0, v4, :cond_d1

    #@b7
    .line 3404
    :cond_b7
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@ba
    move-result-object v4

    #@bb
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    #@be
    move-result v6

    #@bf
    move/from16 v0, v30

    #@c1
    invoke-interface {v4, v0, v6}, Lcom/lge/cappuccino/IMdm;->getAllowSpecificKey(II)Z

    #@c4
    move-result v4

    #@c5
    if-nez v4, :cond_d1

    #@c7
    .line 3406
    const-string v4, "WindowManager"

    #@c9
    const-string v6, "LGMDM:Navigation key is locked!!"

    #@cb
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@ce
    .line 3407
    const-wide/16 v6, -0x1

    #@d0
    goto :goto_97

    #@d1
    .line 3414
    :cond_d1
    const-string v4, "ro.lge.b2b.vmware"

    #@d3
    const/4 v6, 0x0

    #@d4
    invoke-static {v4, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@d7
    move-result v4

    #@d8
    if-eqz v4, :cond_ed

    #@da
    const/high16 v4, 0x80

    #@dc
    and-int v4, v4, p3

    #@de
    if-eqz v4, :cond_ed

    #@e0
    .line 3416
    move-object/from16 v0, p0

    #@e2
    move/from16 v1, v30

    #@e4
    move/from16 v2, v25

    #@e6
    move/from16 v3, v21

    #@e8
    invoke-direct {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->manageFocusedMvpKeys(IIZ)J

    #@eb
    move-result-wide v6

    #@ec
    goto :goto_97

    #@ed
    .line 3424
    :cond_ed
    move-object/from16 v0, p0

    #@ef
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenshotChordEnabled:Z

    #@f1
    if-eqz v4, :cond_199

    #@f3
    move/from16 v0, v25

    #@f5
    and-int/lit16 v4, v0, 0x400

    #@f7
    if-nez v4, :cond_199

    #@f9
    .line 3425
    move-object/from16 v0, p0

    #@fb
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@fd
    if-eqz v4, :cond_11f

    #@ff
    move-object/from16 v0, p0

    #@101
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@103
    if-eqz v4, :cond_10b

    #@105
    move-object/from16 v0, p0

    #@107
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@109
    if-nez v4, :cond_11f

    #@10b
    .line 3426
    :cond_10b
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@10e
    move-result-wide v34

    #@10f
    .line 3427
    .local v34, now:J
    move-object/from16 v0, p0

    #@111
    iget-wide v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTime:J

    #@113
    const-wide/16 v8, 0x96

    #@115
    add-long v42, v6, v8

    #@117
    .line 3428
    .local v42, timeoutTime:J
    cmp-long v4, v34, v42

    #@119
    if-gez v4, :cond_11f

    #@11b
    .line 3429
    sub-long v6, v42, v34

    #@11d
    goto/16 :goto_97

    #@11f
    .line 3432
    .end local v34           #now:J
    .end local v42           #timeoutTime:J
    :cond_11f
    const/16 v4, 0x19

    #@121
    move/from16 v0, v30

    #@123
    if-ne v0, v4, :cond_199

    #@125
    move-object/from16 v0, p0

    #@127
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@129
    if-eqz v4, :cond_199

    #@12b
    .line 3435
    move-object/from16 v0, p0

    #@12d
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRearSideKeyEnable:Z

    #@12f
    if-eqz v4, :cond_18e

    #@131
    .line 3436
    const/16 v28, 0x0

    #@133
    .line 3437
    .local v28, isOffhook:Z
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@136
    move-result-object v41

    #@137
    .line 3438
    .local v41, telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v41, :cond_13d

    #@139
    .line 3440
    :try_start_139
    invoke-interface/range {v41 .. v41}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z
    :try_end_13c
    .catch Landroid/os/RemoteException; {:try_start_139 .. :try_end_13c} :catch_16b

    #@13c
    move-result v28

    #@13d
    .line 3445
    :cond_13d
    :goto_13d
    if-eqz v28, :cond_15a

    #@13f
    .line 3446
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@142
    move-result-wide v34

    #@143
    .line 3447
    .restart local v34       #now:J
    move-object/from16 v0, p0

    #@145
    iget-wide v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTime:J

    #@147
    const-wide/16 v8, 0x12c

    #@149
    add-long v42, v6, v8

    #@14b
    .line 3448
    .restart local v42       #timeoutTime:J
    cmp-long v4, v34, v42

    #@14d
    if-gez v4, :cond_17a

    #@14f
    .line 3449
    move-object/from16 v0, p0

    #@151
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@153
    if-nez v4, :cond_176

    #@155
    .line 3450
    const/4 v4, 0x0

    #@156
    move-object/from16 v0, p0

    #@158
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@15a
    .line 3459
    .end local v34           #now:J
    .end local v42           #timeoutTime:J
    :cond_15a
    :goto_15a
    move-object/from16 v0, p0

    #@15c
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@15e
    if-eqz v4, :cond_199

    #@160
    .line 3460
    if-nez v21, :cond_167

    #@162
    .line 3461
    const/4 v4, 0x0

    #@163
    move-object/from16 v0, p0

    #@165
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@167
    .line 3463
    :cond_167
    const-wide/16 v6, -0x1

    #@169
    goto/16 :goto_97

    #@16b
    .line 3441
    :catch_16b
    move-exception v23

    #@16c
    .line 3442
    .local v23, ex:Landroid/os/RemoteException;
    const-string v4, "WindowManager"

    #@16e
    const-string v6, "ITelephony threw RemoteException"

    #@170
    move-object/from16 v0, v23

    #@172
    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@175
    goto :goto_13d

    #@176
    .line 3452
    .end local v23           #ex:Landroid/os/RemoteException;
    .restart local v34       #now:J
    .restart local v42       #timeoutTime:J
    :cond_176
    sub-long v6, v42, v34

    #@178
    goto/16 :goto_97

    #@17a
    .line 3454
    :cond_17a
    const-wide/16 v6, 0x96

    #@17c
    add-long v6, v6, v42

    #@17e
    cmp-long v4, v34, v6

    #@180
    if-gez v4, :cond_15a

    #@182
    move-object/from16 v0, p0

    #@184
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@186
    if-nez v4, :cond_15a

    #@188
    .line 3456
    const/4 v4, 0x0

    #@189
    move-object/from16 v0, p0

    #@18b
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@18d
    goto :goto_15a

    #@18e
    .line 3466
    .end local v28           #isOffhook:Z
    .end local v34           #now:J
    .end local v41           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    .end local v42           #timeoutTime:J
    :cond_18e
    if-nez v21, :cond_195

    #@190
    .line 3467
    const/4 v4, 0x0

    #@191
    move-object/from16 v0, p0

    #@193
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@195
    .line 3469
    :cond_195
    const-wide/16 v6, -0x1

    #@197
    goto/16 :goto_97

    #@199
    .line 3476
    :cond_199
    move-object/from16 v0, p0

    #@19b
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEasyAccessEnabled:Z

    #@19d
    if-eqz v4, :cond_24a

    #@19f
    move/from16 v0, v25

    #@1a1
    and-int/lit16 v4, v0, 0x400

    #@1a3
    if-nez v4, :cond_24a

    #@1a5
    if-nez v21, :cond_24a

    #@1a7
    .line 3477
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@1aa
    move-result-wide v34

    #@1ab
    .line 3478
    .restart local v34       #now:J
    move-object/from16 v0, p0

    #@1ad
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@1af
    move-object/from16 v0, p0

    #@1b1
    iget-object v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@1b3
    array-length v6, v6

    #@1b4
    add-int/lit8 v6, v6, -0x1

    #@1b6
    aget-wide v6, v4, v6

    #@1b8
    const-wide/16 v8, 0x12c

    #@1ba
    add-long v42, v6, v8

    #@1bc
    .line 3481
    .restart local v42       #timeoutTime:J
    const-string v4, "WindowManager"

    #@1be
    new-instance v6, Ljava/lang/StringBuilder;

    #@1c0
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@1c3
    const-string v7, "interceptKeyTi  last home key up  time ="

    #@1c5
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1c8
    move-result-object v6

    #@1c9
    move-object/from16 v0, p0

    #@1cb
    iget-object v7, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@1cd
    move-object/from16 v0, p0

    #@1cf
    iget-object v8, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@1d1
    array-length v8, v8

    #@1d2
    add-int/lit8 v8, v8, -0x1

    #@1d4
    aget-wide v7, v7, v8

    #@1d6
    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1d9
    move-result-object v6

    #@1da
    const-string v7, " time out "

    #@1dc
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1df
    move-result-object v6

    #@1e0
    move-wide/from16 v0, v42

    #@1e2
    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1e5
    move-result-object v6

    #@1e6
    const-string v7, " now "

    #@1e8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1eb
    move-result-object v6

    #@1ec
    move-wide/from16 v0, v34

    #@1ee
    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@1f1
    move-result-object v6

    #@1f2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1f5
    move-result-object v6

    #@1f6
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1f9
    .line 3484
    cmp-long v4, v34, v42

    #@1fb
    if-gez v4, :cond_201

    #@1fd
    .line 3485
    sub-long v6, v42, v34

    #@1ff
    goto/16 :goto_97

    #@201
    .line 3488
    :cond_201
    const/4 v4, 0x3

    #@202
    move/from16 v0, v30

    #@204
    if-ne v0, v4, :cond_247

    #@206
    move-object/from16 v0, p0

    #@208
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyConsumedByEasyAccess:Z

    #@20a
    if-eqz v4, :cond_247

    #@20c
    .line 3490
    move-object/from16 v0, p0

    #@20e
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@210
    const/4 v6, 0x1

    #@211
    move-object/from16 v0, p0

    #@213
    iget-object v7, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@215
    const/4 v8, 0x0

    #@216
    move-object/from16 v0, p0

    #@218
    iget-object v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@21a
    array-length v9, v9

    #@21b
    add-int/lit8 v9, v9, -0x1

    #@21d
    invoke-static {v4, v6, v7, v8, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    #@220
    .line 3491
    move-object/from16 v0, p0

    #@222
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@224
    move-object/from16 v0, p0

    #@226
    iget-object v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@228
    array-length v6, v6

    #@229
    add-int/lit8 v6, v6, -0x1

    #@22b
    const-wide/16 v7, 0x0

    #@22d
    aput-wide v7, v4, v6

    #@22f
    .line 3492
    move-object/from16 v0, p0

    #@231
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyTime:[J

    #@233
    const/4 v6, 0x0

    #@234
    aget-wide v6, v4, v6

    #@236
    const-wide/16 v8, 0x0

    #@238
    cmp-long v4, v6, v8

    #@23a
    if-eqz v4, :cond_245

    #@23c
    const/4 v4, 0x1

    #@23d
    :goto_23d
    move-object/from16 v0, p0

    #@23f
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyConsumedByEasyAccess:Z

    #@241
    .line 3493
    const-wide/16 v6, -0x1

    #@243
    goto/16 :goto_97

    #@245
    .line 3492
    :cond_245
    const/4 v4, 0x0

    #@246
    goto :goto_23d

    #@247
    .line 3496
    :cond_247
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingEasyAccessEntryAction()V

    #@24a
    .line 3500
    .end local v34           #now:J
    .end local v42           #timeoutTime:J
    :cond_24a
    move-object/from16 v0, p0

    #@24c
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@24e
    if-nez v4, :cond_256

    #@250
    move-object/from16 v0, p0

    #@252
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@254
    if-eqz v4, :cond_2aa

    #@256
    :cond_256
    move/from16 v0, v25

    #@258
    and-int/lit16 v4, v0, 0x400

    #@25a
    if-nez v4, :cond_2aa

    #@25c
    .line 3501
    move-object/from16 v0, p0

    #@25e
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@260
    if-eqz v4, :cond_27c

    #@262
    move-object/from16 v0, p0

    #@264
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@266
    if-nez v4, :cond_27c

    #@268
    .line 3502
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@26b
    move-result-wide v34

    #@26c
    .line 3503
    .restart local v34       #now:J
    move-object/from16 v0, p0

    #@26e
    iget-wide v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTime:J

    #@270
    const-wide/16 v8, 0x96

    #@272
    add-long v42, v6, v8

    #@274
    .line 3504
    .restart local v42       #timeoutTime:J
    cmp-long v4, v34, v42

    #@276
    if-gez v4, :cond_27c

    #@278
    .line 3505
    sub-long v6, v42, v34

    #@27a
    goto/16 :goto_97

    #@27c
    .line 3508
    .end local v34           #now:J
    .end local v42           #timeoutTime:J
    :cond_27c
    const/16 v4, 0x19

    #@27e
    move/from16 v0, v30

    #@280
    if-ne v0, v4, :cond_293

    #@282
    move-object/from16 v0, p0

    #@284
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByQuickClipChord:Z

    #@286
    if-eqz v4, :cond_293

    #@288
    .line 3510
    if-nez v21, :cond_28f

    #@28a
    .line 3511
    const/4 v4, 0x0

    #@28b
    move-object/from16 v0, p0

    #@28d
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByQuickClipChord:Z

    #@28f
    .line 3513
    :cond_28f
    const-wide/16 v6, -0x1

    #@291
    goto/16 :goto_97

    #@293
    .line 3515
    :cond_293
    const/16 v4, 0x18

    #@295
    move/from16 v0, v30

    #@297
    if-ne v0, v4, :cond_2aa

    #@299
    move-object/from16 v0, p0

    #@29b
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedByQuickClipChord:Z

    #@29d
    if-eqz v4, :cond_2aa

    #@29f
    .line 3517
    if-nez v21, :cond_2a6

    #@2a1
    .line 3518
    const/4 v4, 0x0

    #@2a2
    move-object/from16 v0, p0

    #@2a4
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedByQuickClipChord:Z

    #@2a6
    .line 3520
    :cond_2a6
    const-wide/16 v6, -0x1

    #@2a8
    goto/16 :goto_97

    #@2aa
    .line 3525
    :cond_2aa
    move-object/from16 v0, p0

    #@2ac
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@2ae
    if-eqz v4, :cond_2fe

    #@2b0
    move/from16 v0, v25

    #@2b2
    and-int/lit16 v4, v0, 0x400

    #@2b4
    if-nez v4, :cond_2fe

    #@2b6
    .line 3526
    const/16 v4, 0x19

    #@2b8
    move/from16 v0, v30

    #@2ba
    if-ne v0, v4, :cond_2da

    #@2bc
    move-object/from16 v0, p0

    #@2be
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByLongPress:Z

    #@2c0
    if-eqz v4, :cond_2da

    #@2c2
    move-object/from16 v0, p0

    #@2c4
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownLongKeyCancelReason:I

    #@2c6
    if-eqz v4, :cond_2cf

    #@2c8
    move-object/from16 v0, p0

    #@2ca
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownLongKeyCancelReason:I

    #@2cc
    const/4 v6, 0x1

    #@2cd
    if-ne v4, v6, :cond_2da

    #@2cf
    .line 3529
    :cond_2cf
    if-nez v21, :cond_2d6

    #@2d1
    .line 3530
    const/4 v4, 0x0

    #@2d2
    move-object/from16 v0, p0

    #@2d4
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByLongPress:Z

    #@2d6
    .line 3532
    :cond_2d6
    const-wide/16 v6, -0x1

    #@2d8
    goto/16 :goto_97

    #@2da
    .line 3534
    :cond_2da
    const/16 v4, 0x18

    #@2dc
    move/from16 v0, v30

    #@2de
    if-ne v0, v4, :cond_2fe

    #@2e0
    move-object/from16 v0, p0

    #@2e2
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedByLongPress:Z

    #@2e4
    if-eqz v4, :cond_2fe

    #@2e6
    move-object/from16 v0, p0

    #@2e8
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpLongKeyCancelReason:I

    #@2ea
    if-eqz v4, :cond_2f3

    #@2ec
    move-object/from16 v0, p0

    #@2ee
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpLongKeyCancelReason:I

    #@2f0
    const/4 v6, 0x1

    #@2f1
    if-ne v4, v6, :cond_2fe

    #@2f3
    .line 3537
    :cond_2f3
    if-nez v21, :cond_2fa

    #@2f5
    .line 3538
    const/4 v4, 0x0

    #@2f6
    move-object/from16 v0, p0

    #@2f8
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedByLongPress:Z

    #@2fa
    .line 3540
    :cond_2fa
    const-wide/16 v6, -0x1

    #@2fc
    goto/16 :goto_97

    #@2fe
    .line 3545
    :cond_2fe
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEY_EXCEPTION:Z

    #@300
    if-eqz v4, :cond_32d

    #@302
    .line 3546
    move-object/from16 v0, p0

    #@304
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@306
    if-eqz v4, :cond_32b

    #@308
    move-object/from16 v0, p0

    #@30a
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@30c
    invoke-interface {v4}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@30f
    move-result-object v14

    #@310
    .line 3547
    .local v14, attrs:Landroid/view/WindowManager$LayoutParams;
    :goto_310
    if-eqz v14, :cond_32d

    #@312
    .line 3548
    iget v0, v14, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@314
    move/from16 v24, v0

    #@316
    .line 3549
    .local v24, extend:I
    const/high16 v4, 0x8

    #@318
    and-int v4, v4, v24

    #@31a
    if-eqz v4, :cond_32d

    #@31c
    move/from16 v0, p3

    #@31e
    and-int/lit16 v4, v0, 0x100

    #@320
    if-nez v4, :cond_327

    #@322
    const/4 v4, 0x3

    #@323
    move/from16 v0, v30

    #@325
    if-ne v0, v4, :cond_32d

    #@327
    .line 3551
    :cond_327
    const-wide/16 v6, 0x0

    #@329
    goto/16 :goto_97

    #@32b
    .line 3546
    .end local v14           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v24           #extend:I
    :cond_32b
    const/4 v14, 0x0

    #@32c
    goto :goto_310

    #@32d
    .line 3556
    :cond_32d
    move-object/from16 v0, p0

    #@32f
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppAvailable:Z

    #@331
    if-eqz v4, :cond_350

    #@333
    .line 3557
    move/from16 v0, v25

    #@335
    and-int/lit16 v4, v0, 0x400

    #@337
    if-nez v4, :cond_350

    #@339
    .line 3558
    const/16 v4, 0x18

    #@33b
    move/from16 v0, v30

    #@33d
    if-ne v0, v4, :cond_350

    #@33f
    move-object/from16 v0, p0

    #@341
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedBy112App:Z

    #@343
    if-eqz v4, :cond_350

    #@345
    .line 3559
    if-nez v21, :cond_34c

    #@347
    .line 3560
    const/4 v4, 0x0

    #@348
    move-object/from16 v0, p0

    #@34a
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedBy112App:Z

    #@34c
    .line 3562
    :cond_34c
    const-wide/16 v6, -0x1

    #@34e
    goto/16 :goto_97

    #@350
    .line 3567
    :cond_350
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isMulticallActive()Z

    #@353
    move-result v4

    #@354
    if-eqz v4, :cond_38e

    #@356
    .line 3568
    const/4 v4, 0x3

    #@357
    move/from16 v0, v30

    #@359
    if-eq v0, v4, :cond_36c

    #@35b
    const/4 v4, 0x4

    #@35c
    move/from16 v0, v30

    #@35e
    if-eq v0, v4, :cond_36c

    #@360
    const/16 v4, 0xbb

    #@362
    move/from16 v0, v30

    #@364
    if-eq v0, v4, :cond_36c

    #@366
    const/16 v4, 0x52

    #@368
    move/from16 v0, v30

    #@36a
    if-ne v0, v4, :cond_38e

    #@36c
    .line 3571
    :cond_36c
    new-instance v13, Landroid/content/Intent;

    #@36e
    const-string v4, "com.lge.call.action.SENDKEY"

    #@370
    invoke-direct {v13, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@373
    .line 3572
    .local v13, KeyIntent:Landroid/content/Intent;
    const-string v4, "com.lge.call.KeyValue"

    #@375
    move/from16 v0, v30

    #@377
    invoke-virtual {v13, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@37a
    .line 3573
    const-string v4, "com.lge.call.KeyAction"

    #@37c
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getAction()I

    #@37f
    move-result v6

    #@380
    invoke-virtual {v13, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@383
    .line 3574
    move-object/from16 v0, p0

    #@385
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@387
    invoke-virtual {v4, v13}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@38a
    .line 3575
    const-wide/16 v6, -0x1

    #@38c
    goto/16 :goto_97

    #@38e
    .line 3583
    .end local v13           #KeyIntent:Landroid/content/Intent;
    :cond_38e
    const/4 v4, 0x3

    #@38f
    move/from16 v0, v30

    #@391
    if-ne v0, v4, :cond_53e

    #@393
    .line 3585
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@396
    move-result-object v4

    #@397
    if-eqz v4, :cond_3ae

    #@399
    .line 3586
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@39c
    move-result-object v4

    #@39d
    invoke-interface {v4}, Lcom/lge/cappuccino/IMdm;->getEnforcePasswordChange()Z

    #@3a0
    move-result v4

    #@3a1
    if-eqz v4, :cond_3ae

    #@3a3
    .line 3587
    const-string v4, "WindowManager"

    #@3a5
    const-string v6, "LGMDM:HOME is locked!!"

    #@3a7
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@3aa
    .line 3588
    const-wide/16 v6, -0x1

    #@3ac
    goto/16 :goto_97

    #@3ae
    .line 3594
    :cond_3ae
    const-string v4, "ro.lge.b2b.vmware"

    #@3b0
    const/4 v6, 0x0

    #@3b1
    invoke-static {v4, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@3b4
    move-result v4

    #@3b5
    if-eqz v4, :cond_3cc

    #@3b7
    const/high16 v4, -0x8000

    #@3b9
    and-int v4, v4, p3

    #@3bb
    if-eqz v4, :cond_3cc

    #@3bd
    .line 3596
    const-string v4, "homekey"

    #@3bf
    move-object/from16 v0, p0

    #@3c1
    move-object/from16 v1, p1

    #@3c3
    move-object/from16 v2, p2

    #@3c5
    invoke-direct {v0, v1, v2, v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->manageMvpKeys(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;Ljava/lang/String;)I

    #@3c8
    move-result v4

    #@3c9
    int-to-long v6, v4

    #@3ca
    goto/16 :goto_97

    #@3cc
    .line 3600
    :cond_3cc
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEY_EXCEPTION:Z

    #@3ce
    if-eqz v4, :cond_3fd

    #@3d0
    .line 3601
    move-object/from16 v0, p0

    #@3d2
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@3d4
    if-eqz v4, :cond_3f0

    #@3d6
    move-object/from16 v0, p0

    #@3d8
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@3da
    invoke-interface {v4}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@3dd
    move-result-object v44

    #@3de
    .line 3604
    .local v44, topWindowAttrs:Landroid/view/WindowManager$LayoutParams;
    :goto_3de
    if-eqz v44, :cond_3fd

    #@3e0
    .line 3605
    move-object/from16 v0, v44

    #@3e2
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@3e4
    move/from16 v24, v0

    #@3e6
    .line 3606
    .restart local v24       #extend:I
    move/from16 v0, v24

    #@3e8
    and-int/lit16 v4, v0, 0x100

    #@3ea
    if-eqz v4, :cond_3f3

    #@3ec
    .line 3607
    const-wide/16 v6, -0x1

    #@3ee
    goto/16 :goto_97

    #@3f0
    .line 3601
    .end local v24           #extend:I
    .end local v44           #topWindowAttrs:Landroid/view/WindowManager$LayoutParams;
    :cond_3f0
    const/16 v44, 0x0

    #@3f2
    goto :goto_3de

    #@3f3
    .line 3608
    .restart local v24       #extend:I
    .restart local v44       #topWindowAttrs:Landroid/view/WindowManager$LayoutParams;
    :cond_3f3
    move/from16 v0, v24

    #@3f5
    and-int/lit16 v4, v0, 0x200

    #@3f7
    if-eqz v4, :cond_3fd

    #@3f9
    .line 3609
    const-wide/16 v6, 0x0

    #@3fb
    goto/16 :goto_97

    #@3fd
    .line 3616
    .end local v24           #extend:I
    .end local v44           #topWindowAttrs:Landroid/view/WindowManager$LayoutParams;
    :cond_3fd
    move-object/from16 v0, p0

    #@3ff
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomePressed:Z

    #@401
    if-eqz v4, :cond_4b8

    #@403
    if-nez v21, :cond_4b8

    #@405
    .line 3617
    move-object/from16 v0, p0

    #@407
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeLongPressed:Z

    #@409
    move/from16 v26, v0

    #@40b
    .line 3618
    .local v26, homeWasLongPressed:Z
    const/4 v4, 0x0

    #@40c
    move-object/from16 v0, p0

    #@40e
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomePressed:Z

    #@410
    .line 3619
    const/4 v4, 0x0

    #@411
    move-object/from16 v0, p0

    #@413
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeLongPressed:Z

    #@415
    .line 3620
    if-nez v26, :cond_4b8

    #@417
    .line 3621
    move-object/from16 v0, p0

    #@419
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@41b
    const/4 v6, 0x2

    #@41c
    if-ne v4, v6, :cond_427

    #@41e
    .line 3623
    :try_start_41e
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@421
    move-result-object v40

    #@422
    .line 3624
    .local v40, statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v40, :cond_427

    #@424
    .line 3625
    invoke-interface/range {v40 .. v40}, Lcom/android/internal/statusbar/IStatusBarService;->cancelPreloadRecentApps()V
    :try_end_427
    .catch Landroid/os/RemoteException; {:try_start_41e .. :try_end_427} :catch_45a

    #@427
    .line 3634
    .end local v40           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_427
    :goto_427
    const/4 v4, 0x0

    #@428
    move-object/from16 v0, p0

    #@42a
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomePressed:Z

    #@42c
    .line 3635
    if-nez v17, :cond_4b0

    #@42e
    .line 3640
    move-object/from16 v0, p0

    #@430
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@432
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@435
    move-result-object v4

    #@436
    const v6, 0x2060006

    #@439
    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@43c
    move-result v4

    #@43d
    if-eqz v4, :cond_46a

    #@43f
    .line 3641
    move-object/from16 v0, p0

    #@441
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@443
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@446
    move-result-object v4

    #@447
    const-string v6, "go_home"

    #@449
    const/4 v7, 0x0

    #@44a
    invoke-static {v4, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@44d
    move-result v47

    #@44e
    .line 3642
    .local v47, value:I
    const/4 v4, 0x1

    #@44f
    move/from16 v0, v47

    #@451
    if-ne v0, v4, :cond_46a

    #@453
    .line 3644
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->showGoHomeDialog()V

    #@456
    .line 3645
    const-wide/16 v6, -0x1

    #@458
    goto/16 :goto_97

    #@45a
    .line 3627
    .end local v47           #value:I
    :catch_45a
    move-exception v22

    #@45b
    .line 3628
    .local v22, e:Landroid/os/RemoteException;
    const-string v4, "WindowManager"

    #@45d
    const-string v6, "RemoteException when showing recent apps"

    #@45f
    move-object/from16 v0, v22

    #@461
    invoke-static {v4, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@464
    .line 3630
    const/4 v4, 0x0

    #@465
    move-object/from16 v0, p0

    #@467
    iput-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@469
    goto :goto_427

    #@46a
    .line 3663
    .end local v22           #e:Landroid/os/RemoteException;
    :cond_46a
    const/16 v16, 0x0

    #@46c
    .line 3665
    .local v16, bCallState:I
    :try_start_46c
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@46f
    move-result-object v41

    #@470
    .line 3666
    .restart local v41       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v41, :cond_476

    #@472
    .line 3667
    invoke-interface/range {v41 .. v41}, Lcom/android/internal/telephony/ITelephony;->getCallState()I
    :try_end_475
    .catch Landroid/os/RemoteException; {:try_start_46c .. :try_end_475} :catch_493

    #@475
    move-result v16

    #@476
    .line 3672
    .end local v41           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_476
    :goto_476
    const/16 v4, 0x64

    #@478
    move/from16 v0, v16

    #@47a
    if-eq v0, v4, :cond_482

    #@47c
    const/16 v4, 0x65

    #@47e
    move/from16 v0, v16

    #@480
    if-ne v0, v4, :cond_4a9

    #@482
    .line 3673
    :cond_482
    move/from16 v0, p3

    #@484
    and-int/lit16 v4, v0, 0x100

    #@486
    if-nez v4, :cond_49e

    #@488
    .line 3674
    const-string v4, "WindowManager"

    #@48a
    const-string v6, "bypass HOME to phone apk: there\'s a ringing incoming call."

    #@48c
    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@48f
    .line 3675
    const-wide/16 v6, 0x0

    #@491
    goto/16 :goto_97

    #@493
    .line 3669
    :catch_493
    move-exception v23

    #@494
    .line 3670
    .restart local v23       #ex:Landroid/os/RemoteException;
    const-string v4, "WindowManager"

    #@496
    const-string v6, "RemoteException from getPhoneInterface()"

    #@498
    move-object/from16 v0, v23

    #@49a
    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@49d
    goto :goto_476

    #@49e
    .line 3677
    .end local v23           #ex:Landroid/os/RemoteException;
    :cond_49e
    const-string v4, "WindowManager"

    #@4a0
    const-string v6, "Ignoring HOME; there\'s a ringing incoming call."

    #@4a2
    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4a5
    .line 3678
    const-wide/16 v6, -0x1

    #@4a7
    goto/16 :goto_97

    #@4a9
    .line 3681
    :cond_4a9
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->launchHomeFromHotKey()V

    #@4ac
    .line 3688
    .end local v16           #bCallState:I
    :goto_4ac
    const-wide/16 v6, -0x1

    #@4ae
    goto/16 :goto_97

    #@4b0
    .line 3686
    :cond_4b0
    const-string v4, "WindowManager"

    #@4b2
    const-string v6, "Ignoring HOME; event canceled."

    #@4b4
    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@4b7
    goto :goto_4ac

    #@4b8
    .line 3694
    .end local v26           #homeWasLongPressed:Z
    :cond_4b8
    if-eqz p1, :cond_4d4

    #@4ba
    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@4bd
    move-result-object v14

    #@4be
    .line 3695
    .restart local v14       #attrs:Landroid/view/WindowManager$LayoutParams;
    :goto_4be
    if-eqz v14, :cond_4f2

    #@4c0
    .line 3696
    iget v0, v14, Landroid/view/WindowManager$LayoutParams;->type:I

    #@4c2
    move/from16 v45, v0

    #@4c4
    .line 3697
    .local v45, type:I
    const/16 v4, 0x7d4

    #@4c6
    move/from16 v0, v45

    #@4c8
    if-eq v0, v4, :cond_4d0

    #@4ca
    const/16 v4, 0x7d9

    #@4cc
    move/from16 v0, v45

    #@4ce
    if-ne v0, v4, :cond_4d6

    #@4d0
    .line 3700
    :cond_4d0
    const-wide/16 v6, 0x0

    #@4d2
    goto/16 :goto_97

    #@4d4
    .line 3694
    .end local v14           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v45           #type:I
    :cond_4d4
    const/4 v14, 0x0

    #@4d5
    goto :goto_4be

    #@4d6
    .line 3702
    .restart local v14       #attrs:Landroid/view/WindowManager$LayoutParams;
    .restart local v45       #type:I
    :cond_4d6
    sget-object v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->WINDOW_TYPES_WHERE_HOME_DOESNT_WORK:[I

    #@4d8
    array-length v0, v4

    #@4d9
    move/from16 v46, v0

    #@4db
    .line 3703
    .local v46, typeCount:I
    const/16 v27, 0x0

    #@4dd
    .local v27, i:I
    :goto_4dd
    move/from16 v0, v27

    #@4df
    move/from16 v1, v46

    #@4e1
    if-ge v0, v1, :cond_4f2

    #@4e3
    .line 3704
    sget-object v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->WINDOW_TYPES_WHERE_HOME_DOESNT_WORK:[I

    #@4e5
    aget v4, v4, v27

    #@4e7
    move/from16 v0, v45

    #@4e9
    if-ne v0, v4, :cond_4ef

    #@4eb
    .line 3706
    const-wide/16 v6, -0x1

    #@4ed
    goto/16 :goto_97

    #@4ef
    .line 3703
    :cond_4ef
    add-int/lit8 v27, v27, 0x1

    #@4f1
    goto :goto_4dd

    #@4f2
    .line 3710
    .end local v27           #i:I
    .end local v45           #type:I
    .end local v46           #typeCount:I
    :cond_4f2
    if-eqz v21, :cond_53a

    #@4f4
    .line 3711
    move-object/from16 v0, p0

    #@4f6
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomePressed:Z

    #@4f8
    if-nez v4, :cond_50a

    #@4fa
    move-object/from16 v0, p0

    #@4fc
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressOnHomeBehavior:I

    #@4fe
    const/4 v6, 0x2

    #@4ff
    if-ne v4, v6, :cond_50a

    #@501
    .line 3713
    :try_start_501
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@504
    move-result-object v40

    #@505
    .line 3714
    .restart local v40       #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v40, :cond_50a

    #@507
    .line 3715
    invoke-interface/range {v40 .. v40}, Lcom/android/internal/statusbar/IStatusBarService;->preloadRecentApps()V
    :try_end_50a
    .catch Landroid/os/RemoteException; {:try_start_501 .. :try_end_50a} :catch_51b

    #@50a
    .line 3723
    .end local v40           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_50a
    :goto_50a
    if-nez v36, :cond_52b

    #@50c
    .line 3724
    const/4 v4, 0x1

    #@50d
    move-object/from16 v0, p0

    #@50f
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomePressed:Z

    #@511
    .line 3726
    move-object/from16 v0, p0

    #@513
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShowingDream:Z

    #@515
    if-eqz v4, :cond_53a

    #@517
    .line 3727
    const-wide/16 v6, 0x0

    #@519
    goto/16 :goto_97

    #@51b
    .line 3717
    :catch_51b
    move-exception v22

    #@51c
    .line 3718
    .restart local v22       #e:Landroid/os/RemoteException;
    const-string v4, "WindowManager"

    #@51e
    const-string v6, "RemoteException when preloading recent apps"

    #@520
    move-object/from16 v0, v22

    #@522
    invoke-static {v4, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@525
    .line 3720
    const/4 v4, 0x0

    #@526
    move-object/from16 v0, p0

    #@528
    iput-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@52a
    goto :goto_50a

    #@52b
    .line 3729
    .end local v22           #e:Landroid/os/RemoteException;
    :cond_52b
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    #@52e
    move-result v4

    #@52f
    and-int/lit16 v4, v4, 0x80

    #@531
    if-eqz v4, :cond_53a

    #@533
    .line 3730
    if-nez v32, :cond_53a

    #@535
    .line 3731
    if-nez v31, :cond_53a

    #@537
    .line 3732
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->handleLongPressOnHome()V

    #@53a
    .line 3737
    :cond_53a
    const-wide/16 v6, -0x1

    #@53c
    goto/16 :goto_97

    #@53e
    .line 3738
    .end local v14           #attrs:Landroid/view/WindowManager$LayoutParams;
    :cond_53e
    const/16 v4, 0x52

    #@540
    move/from16 v0, v30

    #@542
    if-ne v0, v4, :cond_5ec

    #@544
    .line 3740
    const/16 v19, 0x1

    #@546
    .line 3742
    .local v19, chordBug:I
    if-eqz v21, :cond_5cd

    #@548
    if-nez v36, :cond_5cd

    #@54a
    .line 3743
    move-object/from16 v0, p0

    #@54c
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEnableShiftMenuBugReports:Z

    #@54e
    if-eqz v4, :cond_56f

    #@550
    and-int/lit8 v4, v33, 0x1

    #@552
    const/4 v6, 0x1

    #@553
    if-ne v4, v6, :cond_56f

    #@555
    .line 3744
    new-instance v5, Landroid/content/Intent;

    #@557
    const-string v4, "android.intent.action.BUG_REPORT"

    #@559
    invoke-direct {v5, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@55c
    .line 3745
    .local v5, intent:Landroid/content/Intent;
    move-object/from16 v0, p0

    #@55e
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@560
    sget-object v6, Landroid/os/UserHandle;->CURRENT:Landroid/os/UserHandle;

    #@562
    const/4 v7, 0x0

    #@563
    const/4 v8, 0x0

    #@564
    const/4 v9, 0x0

    #@565
    const/4 v10, 0x0

    #@566
    const/4 v11, 0x0

    #@567
    const/4 v12, 0x0

    #@568
    invoke-virtual/range {v4 .. v12}, Landroid/content/Context;->sendOrderedBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;ILjava/lang/String;Landroid/os/Bundle;)V

    #@56b
    .line 3747
    const-wide/16 v6, -0x1

    #@56d
    goto/16 :goto_97

    #@56f
    .line 3763
    .end local v5           #intent:Landroid/content/Intent;
    :cond_56f
    if-nez v31, :cond_589

    #@571
    .line 3764
    move-object/from16 v0, p0

    #@573
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenulongEnabled:Z

    #@575
    if-eqz v4, :cond_589

    #@577
    .line 3765
    move-object/from16 v0, p0

    #@579
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@57b
    move-object/from16 v0, p0

    #@57d
    iget-object v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenuLongPress:Ljava/lang/Runnable;

    #@57f
    const-wide/16 v7, 0x3e8

    #@581
    invoke-virtual {v4, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@584
    .line 3766
    const/4 v4, 0x1

    #@585
    move-object/from16 v0, p0

    #@587
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenuLongPressing:Z

    #@589
    .line 3969
    .end local v19           #chordBug:I
    :cond_589
    :goto_589
    move-object/from16 v0, p0

    #@58b
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSearchKeyShortcutPending:Z

    #@58d
    if-eqz v4, :cond_889

    #@58f
    .line 3970
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@592
    move-result-object v29

    #@593
    .line 3971
    .local v29, kcm:Landroid/view/KeyCharacterMap;
    invoke-virtual/range {v29 .. v30}, Landroid/view/KeyCharacterMap;->isPrintingKey(I)Z

    #@596
    move-result v4

    #@597
    if-eqz v4, :cond_889

    #@599
    .line 3972
    const/4 v4, 0x1

    #@59a
    move-object/from16 v0, p0

    #@59c
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mConsumeSearchKeyUp:Z

    #@59e
    .line 3973
    const/4 v4, 0x0

    #@59f
    move-object/from16 v0, p0

    #@5a1
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSearchKeyShortcutPending:Z

    #@5a3
    .line 3974
    if-eqz v21, :cond_5c9

    #@5a5
    if-nez v36, :cond_5c9

    #@5a7
    if-nez v31, :cond_5c9

    #@5a9
    .line 3975
    move-object/from16 v0, p0

    #@5ab
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShortcutManager:Lcom/android/internal/policy/impl/ShortcutManager;

    #@5ad
    move-object/from16 v0, v29

    #@5af
    move/from16 v1, v30

    #@5b1
    move/from16 v2, v33

    #@5b3
    invoke-virtual {v4, v0, v1, v2}, Lcom/android/internal/policy/impl/ShortcutManager;->getIntent(Landroid/view/KeyCharacterMap;II)Landroid/content/Intent;

    #@5b6
    move-result-object v39

    #@5b7
    .line 3976
    .local v39, shortcutIntent:Landroid/content/Intent;
    if-eqz v39, :cond_86b

    #@5b9
    .line 3977
    const/high16 v4, 0x1000

    #@5bb
    move-object/from16 v0, v39

    #@5bd
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@5c0
    .line 3979
    :try_start_5c0
    move-object/from16 v0, p0

    #@5c2
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@5c4
    move-object/from16 v0, v39

    #@5c6
    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_5c9
    .catch Landroid/content/ActivityNotFoundException; {:try_start_5c0 .. :try_end_5c9} :catch_84a

    #@5c9
    .line 3990
    .end local v39           #shortcutIntent:Landroid/content/Intent;
    :cond_5c9
    :goto_5c9
    const-wide/16 v6, -0x1

    #@5cb
    goto/16 :goto_97

    #@5cd
    .line 3769
    .end local v29           #kcm:Landroid/view/KeyCharacterMap;
    .restart local v19       #chordBug:I
    :cond_5cd
    if-nez v21, :cond_589

    #@5cf
    move-object/from16 v0, p0

    #@5d1
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenuLongPressing:Z

    #@5d3
    if-eqz v4, :cond_589

    #@5d5
    .line 3770
    move-object/from16 v0, p0

    #@5d7
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenulongEnabled:Z

    #@5d9
    if-eqz v4, :cond_589

    #@5db
    .line 3771
    const/4 v4, 0x0

    #@5dc
    move-object/from16 v0, p0

    #@5de
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenuLongPressing:Z

    #@5e0
    .line 3772
    move-object/from16 v0, p0

    #@5e2
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@5e4
    move-object/from16 v0, p0

    #@5e6
    iget-object v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mMenuLongPress:Ljava/lang/Runnable;

    #@5e8
    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@5eb
    goto :goto_589

    #@5ec
    .line 3775
    .end local v19           #chordBug:I
    :cond_5ec
    const/16 v4, 0x54

    #@5ee
    move/from16 v0, v30

    #@5f0
    if-ne v0, v4, :cond_636

    #@5f2
    .line 3777
    const-string v4, "ro.lge.b2b.vmware"

    #@5f4
    const/4 v6, 0x0

    #@5f5
    invoke-static {v4, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@5f8
    move-result v4

    #@5f9
    if-eqz v4, :cond_610

    #@5fb
    const/high16 v4, -0x8000

    #@5fd
    and-int v4, v4, p3

    #@5ff
    if-eqz v4, :cond_610

    #@601
    .line 3779
    const-string v4, "globalactions"

    #@603
    move-object/from16 v0, p0

    #@605
    move-object/from16 v1, p1

    #@607
    move-object/from16 v2, p2

    #@609
    invoke-direct {v0, v1, v2, v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->manageMvpKeys(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;Ljava/lang/String;)I

    #@60c
    move-result v4

    #@60d
    int-to-long v6, v4

    #@60e
    goto/16 :goto_97

    #@610
    .line 3783
    :cond_610
    if-eqz v21, :cond_622

    #@612
    .line 3784
    if-nez v36, :cond_61e

    #@614
    .line 3785
    const/4 v4, 0x1

    #@615
    move-object/from16 v0, p0

    #@617
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSearchKeyShortcutPending:Z

    #@619
    .line 3786
    const/4 v4, 0x0

    #@61a
    move-object/from16 v0, p0

    #@61c
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mConsumeSearchKeyUp:Z

    #@61e
    .line 3795
    :cond_61e
    const-wide/16 v6, 0x0

    #@620
    goto/16 :goto_97

    #@622
    .line 3789
    :cond_622
    const/4 v4, 0x0

    #@623
    move-object/from16 v0, p0

    #@625
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSearchKeyShortcutPending:Z

    #@627
    .line 3790
    move-object/from16 v0, p0

    #@629
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mConsumeSearchKeyUp:Z

    #@62b
    if-eqz v4, :cond_61e

    #@62d
    .line 3791
    const/4 v4, 0x0

    #@62e
    move-object/from16 v0, p0

    #@630
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mConsumeSearchKeyUp:Z

    #@632
    .line 3792
    const-wide/16 v6, -0x1

    #@634
    goto/16 :goto_97

    #@636
    .line 3796
    :cond_636
    const/16 v4, 0xbb

    #@638
    move/from16 v0, v30

    #@63a
    if-ne v0, v4, :cond_6d8

    #@63c
    .line 3798
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@63f
    move-result-object v4

    #@640
    if-eqz v4, :cond_657

    #@642
    .line 3799
    invoke-static {}, Lcom/lge/cappuccino/Mdm;->getInstance()Lcom/lge/cappuccino/IMdm;

    #@645
    move-result-object v4

    #@646
    invoke-interface {v4}, Lcom/lge/cappuccino/IMdm;->getEnforcePasswordChange()Z

    #@649
    move-result v4

    #@64a
    if-eqz v4, :cond_657

    #@64c
    .line 3800
    const-string v4, "WindowManager"

    #@64e
    const-string v6, "LGMDM:app switch key is locked!!"

    #@650
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@653
    .line 3801
    const-wide/16 v6, -0x1

    #@655
    goto/16 :goto_97

    #@657
    .line 3807
    :cond_657
    const-string v4, "ro.lge.b2b.vmware"

    #@659
    const/4 v6, 0x0

    #@65a
    invoke-static {v4, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@65d
    move-result v4

    #@65e
    if-eqz v4, :cond_675

    #@660
    const/high16 v4, -0x8000

    #@662
    and-int v4, v4, p3

    #@664
    if-eqz v4, :cond_675

    #@666
    .line 3809
    const-string v4, "recentapps"

    #@668
    move-object/from16 v0, p0

    #@66a
    move-object/from16 v1, p1

    #@66c
    move-object/from16 v2, p2

    #@66e
    invoke-direct {v0, v1, v2, v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->manageMvpKeys(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/KeyEvent;Ljava/lang/String;)I

    #@671
    move-result v4

    #@672
    int-to-long v6, v4

    #@673
    goto/16 :goto_97

    #@675
    .line 3813
    :cond_675
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEY_EXCEPTION:Z

    #@677
    if-eqz v4, :cond_6a6

    #@679
    .line 3814
    move-object/from16 v0, p0

    #@67b
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@67d
    if-eqz v4, :cond_699

    #@67f
    move-object/from16 v0, p0

    #@681
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@683
    invoke-interface {v4}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@686
    move-result-object v44

    #@687
    .line 3815
    .restart local v44       #topWindowAttrs:Landroid/view/WindowManager$LayoutParams;
    :goto_687
    if-eqz v44, :cond_6a6

    #@689
    .line 3816
    move-object/from16 v0, v44

    #@68b
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@68d
    move/from16 v24, v0

    #@68f
    .line 3817
    .restart local v24       #extend:I
    move/from16 v0, v24

    #@691
    and-int/lit16 v4, v0, 0x2000

    #@693
    if-eqz v4, :cond_69c

    #@695
    .line 3818
    const-wide/16 v6, -0x1

    #@697
    goto/16 :goto_97

    #@699
    .line 3814
    .end local v24           #extend:I
    .end local v44           #topWindowAttrs:Landroid/view/WindowManager$LayoutParams;
    :cond_699
    const/16 v44, 0x0

    #@69b
    goto :goto_687

    #@69c
    .line 3819
    .restart local v24       #extend:I
    .restart local v44       #topWindowAttrs:Landroid/view/WindowManager$LayoutParams;
    :cond_69c
    move/from16 v0, v24

    #@69e
    and-int/lit16 v4, v0, 0x4000

    #@6a0
    if-eqz v4, :cond_6a6

    #@6a2
    .line 3820
    const-wide/16 v6, 0x0

    #@6a4
    goto/16 :goto_97

    #@6a6
    .line 3825
    .end local v24           #extend:I
    .end local v44           #topWindowAttrs:Landroid/view/WindowManager$LayoutParams;
    :cond_6a6
    if-eqz v21, :cond_6c4

    #@6a8
    if-nez v36, :cond_6c4

    #@6aa
    if-nez v31, :cond_6c4

    #@6ac
    .line 3826
    const/4 v4, 0x0

    #@6ad
    move-object/from16 v0, p0

    #@6af
    invoke-virtual {v0, v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->showOrHideRecentAppsDialog(I)V

    #@6b2
    .line 3828
    const-string v4, "ro.lge.b2b.vmware"

    #@6b4
    const/4 v6, 0x0

    #@6b5
    invoke-static {v4, v6}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@6b8
    move-result v4

    #@6b9
    if-eqz v4, :cond_6c4

    #@6bb
    .line 3830
    :try_start_6bb
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@6be
    move-result-object v40

    #@6bf
    .line 3831
    .restart local v40       #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v40, :cond_6c4

    #@6c1
    .line 3832
    invoke-interface/range {v40 .. v40}, Lcom/android/internal/statusbar/IStatusBarService;->toggleRecentApps()V
    :try_end_6c4
    .catch Landroid/os/RemoteException; {:try_start_6bb .. :try_end_6c4} :catch_6c8

    #@6c4
    .line 3842
    .end local v40           #statusbar:Lcom/android/internal/statusbar/IStatusBarService;
    :cond_6c4
    :goto_6c4
    const-wide/16 v6, -0x1

    #@6c6
    goto/16 :goto_97

    #@6c8
    .line 3834
    :catch_6c8
    move-exception v22

    #@6c9
    .line 3835
    .restart local v22       #e:Landroid/os/RemoteException;
    const-string v4, "WindowManager"

    #@6cb
    const-string v6, "RemoteException when showing recent apps"

    #@6cd
    move-object/from16 v0, v22

    #@6cf
    invoke-static {v4, v6, v0}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@6d2
    .line 3837
    const/4 v4, 0x0

    #@6d3
    move-object/from16 v0, p0

    #@6d5
    iput-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@6d7
    goto :goto_6c4

    #@6d8
    .line 3843
    .end local v22           #e:Landroid/os/RemoteException;
    :cond_6d8
    const/16 v4, 0xdb

    #@6da
    move/from16 v0, v30

    #@6dc
    if-ne v0, v4, :cond_70d

    #@6de
    .line 3844
    if-eqz v21, :cond_6fb

    #@6e0
    .line 3845
    if-nez v36, :cond_6eb

    #@6e2
    .line 3846
    const/4 v4, 0x0

    #@6e3
    move-object/from16 v0, p0

    #@6e5
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAssistKeyLongPressed:Z

    #@6e7
    .line 3862
    :cond_6e7
    :goto_6e7
    const-wide/16 v6, -0x1

    #@6e9
    goto/16 :goto_97

    #@6eb
    .line 3847
    :cond_6eb
    const/4 v4, 0x1

    #@6ec
    move/from16 v0, v36

    #@6ee
    if-ne v0, v4, :cond_6e7

    #@6f0
    .line 3848
    const/4 v4, 0x1

    #@6f1
    move-object/from16 v0, p0

    #@6f3
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAssistKeyLongPressed:Z

    #@6f5
    .line 3849
    if-nez v31, :cond_6e7

    #@6f7
    .line 3850
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->launchAssistLongPressAction()V

    #@6fa
    goto :goto_6e7

    #@6fb
    .line 3854
    :cond_6fb
    move-object/from16 v0, p0

    #@6fd
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAssistKeyLongPressed:Z

    #@6ff
    if-eqz v4, :cond_707

    #@701
    .line 3855
    const/4 v4, 0x0

    #@702
    move-object/from16 v0, p0

    #@704
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAssistKeyLongPressed:Z

    #@706
    goto :goto_6e7

    #@707
    .line 3857
    :cond_707
    if-nez v31, :cond_6e7

    #@709
    .line 3858
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->launchAssistAction()V

    #@70c
    goto :goto_6e7

    #@70d
    .line 3864
    :cond_70d
    const/16 v4, 0xe1

    #@70f
    move/from16 v0, v30

    #@711
    if-ne v0, v4, :cond_7cd

    #@713
    .line 3865
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@716
    move-result-object v41

    #@717
    .line 3866
    .restart local v41       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v41, :cond_755

    #@719
    .line 3868
    :try_start_719
    invoke-interface/range {v41 .. v41}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    #@71c
    move-result v4

    #@71d
    if-eqz v4, :cond_755

    #@71f
    .line 3871
    move-object/from16 v0, p0

    #@721
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@723
    if-nez v4, :cond_72c

    #@725
    move-object/from16 v0, p0

    #@727
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickBTNAnswserMode:I

    #@729
    const/4 v6, 0x1

    #@72a
    if-ne v4, v6, :cond_73a

    #@72c
    .line 3876
    :cond_72c
    const-string v4, "WindowManager"

    #@72e
    const-string v6, "mQuickClipChordLongPress: CALL key-down while ringing: Answer the call!"

    #@730
    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@733
    .line 3878
    invoke-interface/range {v41 .. v41}, Lcom/android/internal/telephony/ITelephony;->answerRingingCall()V

    #@736
    .line 3879
    const-wide/16 v6, -0x1

    #@738
    goto/16 :goto_97

    #@73a
    .line 3882
    :cond_73a
    move-object/from16 v0, p0

    #@73c
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@73e
    move-object/from16 v0, p0

    #@740
    iget-object v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mToastIsRinging:Ljava/lang/Runnable;

    #@742
    const-wide/16 v7, 0x64

    #@744
    invoke-virtual {v4, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_747
    .catch Landroid/os/RemoteException; {:try_start_719 .. :try_end_747} :catch_74b

    #@747
    .line 3883
    const-wide/16 v6, -0x1

    #@749
    goto/16 :goto_97

    #@74b
    .line 3885
    :catch_74b
    move-exception v23

    #@74c
    .line 3886
    .restart local v23       #ex:Landroid/os/RemoteException;
    const-string v4, "WindowManager"

    #@74e
    const-string v6, "ITelephony threw RemoteException"

    #@750
    move-object/from16 v0, v23

    #@752
    invoke-static {v4, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@755
    .line 3890
    .end local v23           #ex:Landroid/os/RemoteException;
    :cond_755
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEY_EXCEPTION:Z

    #@757
    if-eqz v4, :cond_77d

    #@759
    .line 3891
    move-object/from16 v0, p0

    #@75b
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@75d
    if-eqz v4, :cond_797

    #@75f
    move-object/from16 v0, p0

    #@761
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@763
    invoke-interface {v4}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@766
    move-result-object v15

    #@767
    .line 3892
    .local v15, attrss:Landroid/view/WindowManager$LayoutParams;
    :goto_767
    if-eqz v15, :cond_77d

    #@769
    .line 3893
    iget v0, v15, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@76b
    move/from16 v24, v0

    #@76d
    .line 3894
    .restart local v24       #extend:I
    const/high16 v4, 0x4

    #@76f
    and-int v4, v4, v24

    #@771
    if-eqz v4, :cond_77d

    #@773
    .line 3895
    const-string v4, "WindowManager"

    #@775
    const-string v6, "Blocking mBlockingQuickclip"

    #@777
    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@77a
    .line 3896
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->toggleQuickClipFromHotKey()V

    #@77d
    .line 3902
    .end local v15           #attrss:Landroid/view/WindowManager$LayoutParams;
    .end local v24           #extend:I
    :cond_77d
    if-eqz v21, :cond_799

    #@77f
    if-nez v36, :cond_799

    #@781
    .line 3903
    move-object/from16 v0, p0

    #@783
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@785
    move-object/from16 v0, p0

    #@787
    iget-object v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipLongPress:Ljava/lang/Runnable;

    #@789
    invoke-static {}, Landroid/view/ViewConfiguration;->getGlobalActionKeyTimeout()J

    #@78c
    move-result-wide v7

    #@78d
    invoke-virtual {v4, v6, v7, v8}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@790
    .line 3904
    const/4 v4, 0x1

    #@791
    move-object/from16 v0, p0

    #@793
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickclipPressed:Z

    #@795
    goto/16 :goto_589

    #@797
    .line 3891
    :cond_797
    const/4 v15, 0x0

    #@798
    goto :goto_767

    #@799
    .line 3905
    :cond_799
    if-nez v21, :cond_589

    #@79b
    move-object/from16 v0, p0

    #@79d
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickclipPressed:Z

    #@79f
    if-eqz v4, :cond_589

    #@7a1
    .line 3906
    const/4 v4, 0x0

    #@7a2
    move-object/from16 v0, p0

    #@7a4
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickclipPressed:Z

    #@7a6
    .line 3907
    move-object/from16 v0, p0

    #@7a8
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@7aa
    move-object/from16 v0, p0

    #@7ac
    iget-object v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipLongPress:Ljava/lang/Runnable;

    #@7ae
    invoke-virtual {v4, v6}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@7b1
    .line 3908
    if-nez v31, :cond_589

    #@7b3
    .line 3909
    move-object/from16 v0, p0

    #@7b5
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHotKeyCustomizing:Z

    #@7b7
    if-eqz v4, :cond_7c8

    #@7b9
    .line 3910
    move-object/from16 v0, p0

    #@7bb
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sShortKeyPkg:Ljava/lang/String;

    #@7bd
    move-object/from16 v0, p0

    #@7bf
    iget-object v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sShortKeyClass:Ljava/lang/String;

    #@7c1
    move-object/from16 v0, p0

    #@7c3
    invoke-direct {v0, v4, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager;->excuteQuickMemoHotKey(Ljava/lang/String;Ljava/lang/String;)V

    #@7c6
    goto/16 :goto_589

    #@7c8
    .line 3912
    :cond_7c8
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->toggleQuickClipFromHotKey()V

    #@7cb
    goto/16 :goto_589

    #@7cd
    .line 3917
    .end local v41           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_7cd
    sget-boolean v4, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@7cf
    if-eqz v4, :cond_823

    #@7d1
    const/4 v4, 0x4

    #@7d2
    move/from16 v0, v30

    #@7d4
    if-ne v0, v4, :cond_823

    #@7d6
    .line 3920
    if-eqz p1, :cond_7f2

    #@7d8
    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@7db
    move-result-object v14

    #@7dc
    .line 3921
    .restart local v14       #attrs:Landroid/view/WindowManager$LayoutParams;
    :goto_7dc
    if-eqz v14, :cond_810

    #@7de
    .line 3922
    iget v0, v14, Landroid/view/WindowManager$LayoutParams;->type:I

    #@7e0
    move/from16 v45, v0

    #@7e2
    .line 3923
    .restart local v45       #type:I
    const/16 v4, 0x7d4

    #@7e4
    move/from16 v0, v45

    #@7e6
    if-eq v0, v4, :cond_7ee

    #@7e8
    const/16 v4, 0x7d9

    #@7ea
    move/from16 v0, v45

    #@7ec
    if-ne v0, v4, :cond_7f4

    #@7ee
    .line 3926
    :cond_7ee
    const-wide/16 v6, 0x0

    #@7f0
    goto/16 :goto_97

    #@7f2
    .line 3920
    .end local v14           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v45           #type:I
    :cond_7f2
    const/4 v14, 0x0

    #@7f3
    goto :goto_7dc

    #@7f4
    .line 3928
    .restart local v14       #attrs:Landroid/view/WindowManager$LayoutParams;
    .restart local v45       #type:I
    :cond_7f4
    sget-object v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->WINDOW_TYPES_WHERE_HOME_DOESNT_WORK:[I

    #@7f6
    array-length v0, v4

    #@7f7
    move/from16 v46, v0

    #@7f9
    .line 3929
    .restart local v46       #typeCount:I
    const/16 v27, 0x0

    #@7fb
    .restart local v27       #i:I
    :goto_7fb
    move/from16 v0, v27

    #@7fd
    move/from16 v1, v46

    #@7ff
    if-ge v0, v1, :cond_810

    #@801
    .line 3930
    sget-object v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->WINDOW_TYPES_WHERE_HOME_DOESNT_WORK:[I

    #@803
    aget v4, v4, v27

    #@805
    move/from16 v0, v45

    #@807
    if-ne v0, v4, :cond_80d

    #@809
    .line 3932
    const-wide/16 v6, -0x1

    #@80b
    goto/16 :goto_97

    #@80d
    .line 3929
    :cond_80d
    add-int/lit8 v27, v27, 0x1

    #@80f
    goto :goto_7fb

    #@810
    .line 3936
    .end local v27           #i:I
    .end local v45           #type:I
    .end local v46           #typeCount:I
    :cond_810
    if-eqz v21, :cond_589

    #@812
    .line 3937
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getFlags()I

    #@815
    move-result v4

    #@816
    and-int/lit16 v4, v4, 0x80

    #@818
    if-eqz v4, :cond_589

    #@81a
    .line 3938
    if-nez v31, :cond_589

    #@81c
    .line 3939
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->launchSplitWindowUI()V

    #@81f
    .line 3940
    const-wide/16 v6, -0x1

    #@821
    goto/16 :goto_97

    #@823
    .line 3946
    .end local v14           #attrs:Landroid/view/WindowManager$LayoutParams;
    :cond_823
    const/16 v4, 0x53

    #@825
    move/from16 v0, v30

    #@827
    if-ne v0, v4, :cond_589

    #@829
    .line 3947
    if-eqz v21, :cond_589

    #@82b
    .line 3948
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getStatusBarService()Lcom/android/internal/statusbar/IStatusBarService;

    #@82e
    move-result-object v37

    #@82f
    .line 3949
    .local v37, sbs:Lcom/android/internal/statusbar/IStatusBarService;
    if-eqz v37, :cond_589

    #@831
    .line 3951
    :try_start_831
    move-object/from16 v0, p0

    #@833
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIsExpanded:Z

    #@835
    if-nez v4, :cond_845

    #@837
    .line 3952
    invoke-interface/range {v37 .. v37}, Lcom/android/internal/statusbar/IStatusBarService;->expandNotificationsPanel()V
    :try_end_83a
    .catch Landroid/os/RemoteException; {:try_start_831 .. :try_end_83a} :catch_83c

    #@83a
    goto/16 :goto_589

    #@83c
    .line 3956
    :catch_83c
    move-exception v22

    #@83d
    .line 3957
    .restart local v22       #e:Landroid/os/RemoteException;
    new-instance v4, Ljava/lang/RuntimeException;

    #@83f
    move-object/from16 v0, v22

    #@841
    invoke-direct {v4, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    #@844
    throw v4

    #@845
    .line 3954
    .end local v22           #e:Landroid/os/RemoteException;
    :cond_845
    :try_start_845
    invoke-interface/range {v37 .. v37}, Lcom/android/internal/statusbar/IStatusBarService;->collapsePanels()V
    :try_end_848
    .catch Landroid/os/RemoteException; {:try_start_845 .. :try_end_848} :catch_83c

    #@848
    goto/16 :goto_589

    #@84a
    .line 3980
    .end local v37           #sbs:Lcom/android/internal/statusbar/IStatusBarService;
    .restart local v29       #kcm:Landroid/view/KeyCharacterMap;
    .restart local v39       #shortcutIntent:Landroid/content/Intent;
    :catch_84a
    move-exception v23

    #@84b
    .line 3981
    .local v23, ex:Landroid/content/ActivityNotFoundException;
    const-string v4, "WindowManager"

    #@84d
    new-instance v6, Ljava/lang/StringBuilder;

    #@84f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@852
    const-string v7, "Dropping shortcut key combination because the activity to which it is registered was not found: SEARCH+"

    #@854
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@857
    move-result-object v6

    #@858
    invoke-static/range {v30 .. v30}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    #@85b
    move-result-object v7

    #@85c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@85f
    move-result-object v6

    #@860
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@863
    move-result-object v6

    #@864
    move-object/from16 v0, v23

    #@866
    invoke-static {v4, v6, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@869
    goto/16 :goto_5c9

    #@86b
    .line 3986
    .end local v23           #ex:Landroid/content/ActivityNotFoundException;
    :cond_86b
    const-string v4, "WindowManager"

    #@86d
    new-instance v6, Ljava/lang/StringBuilder;

    #@86f
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@872
    const-string v7, "Dropping unregistered shortcut key combination: SEARCH+"

    #@874
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@877
    move-result-object v6

    #@878
    invoke-static/range {v30 .. v30}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    #@87b
    move-result-object v7

    #@87c
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@87f
    move-result-object v6

    #@880
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@883
    move-result-object v6

    #@884
    invoke-static {v4, v6}, Landroid/util/Slog;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@887
    goto/16 :goto_5c9

    #@889
    .line 3995
    .end local v29           #kcm:Landroid/view/KeyCharacterMap;
    .end local v39           #shortcutIntent:Landroid/content/Intent;
    :cond_889
    if-eqz v21, :cond_8e6

    #@88b
    if-nez v36, :cond_8e6

    #@88d
    if-nez v31, :cond_8e6

    #@88f
    const/high16 v4, 0x1

    #@891
    and-int v4, v4, v33

    #@893
    if-eqz v4, :cond_8e6

    #@895
    .line 3997
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    #@898
    move-result-object v29

    #@899
    .line 3998
    .restart local v29       #kcm:Landroid/view/KeyCharacterMap;
    invoke-virtual/range {v29 .. v30}, Landroid/view/KeyCharacterMap;->isPrintingKey(I)Z

    #@89c
    move-result v4

    #@89d
    if-eqz v4, :cond_8e6

    #@89f
    .line 3999
    move-object/from16 v0, p0

    #@8a1
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mShortcutManager:Lcom/android/internal/policy/impl/ShortcutManager;

    #@8a3
    const v6, -0x70001

    #@8a6
    and-int v6, v6, v33

    #@8a8
    move-object/from16 v0, v29

    #@8aa
    move/from16 v1, v30

    #@8ac
    invoke-virtual {v4, v0, v1, v6}, Lcom/android/internal/policy/impl/ShortcutManager;->getIntent(Landroid/view/KeyCharacterMap;II)Landroid/content/Intent;

    #@8af
    move-result-object v39

    #@8b0
    .line 4002
    .restart local v39       #shortcutIntent:Landroid/content/Intent;
    if-eqz v39, :cond_8e6

    #@8b2
    .line 4003
    const/high16 v4, 0x1000

    #@8b4
    move-object/from16 v0, v39

    #@8b6
    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@8b9
    .line 4005
    :try_start_8b9
    move-object/from16 v0, p0

    #@8bb
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@8bd
    move-object/from16 v0, v39

    #@8bf
    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_8c2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_8b9 .. :try_end_8c2} :catch_8c6

    #@8c2
    .line 4011
    :goto_8c2
    const-wide/16 v6, -0x1

    #@8c4
    goto/16 :goto_97

    #@8c6
    .line 4006
    :catch_8c6
    move-exception v23

    #@8c7
    .line 4007
    .restart local v23       #ex:Landroid/content/ActivityNotFoundException;
    const-string v4, "WindowManager"

    #@8c9
    new-instance v6, Ljava/lang/StringBuilder;

    #@8cb
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@8ce
    const-string v7, "Dropping shortcut key combination because the activity to which it is registered was not found: META+"

    #@8d0
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8d3
    move-result-object v6

    #@8d4
    invoke-static/range {v30 .. v30}, Landroid/view/KeyEvent;->keyCodeToString(I)Ljava/lang/String;

    #@8d7
    move-result-object v7

    #@8d8
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@8db
    move-result-object v6

    #@8dc
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@8df
    move-result-object v6

    #@8e0
    move-object/from16 v0, v23

    #@8e2
    invoke-static {v4, v6, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8e5
    goto :goto_8c2

    #@8e6
    .line 4017
    .end local v23           #ex:Landroid/content/ActivityNotFoundException;
    .end local v29           #kcm:Landroid/view/KeyCharacterMap;
    .end local v39           #shortcutIntent:Landroid/content/Intent;
    :cond_8e6
    if-eqz v21, :cond_93a

    #@8e8
    if-nez v36, :cond_93a

    #@8ea
    if-nez v31, :cond_93a

    #@8ec
    .line 4018
    sget-object v4, Lcom/android/internal/policy/impl/PhoneWindowManager;->sApplicationLaunchKeyCategories:Landroid/util/SparseArray;

    #@8ee
    move/from16 v0, v30

    #@8f0
    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    #@8f3
    move-result-object v18

    #@8f4
    check-cast v18, Ljava/lang/String;

    #@8f6
    .line 4019
    .local v18, category:Ljava/lang/String;
    if-eqz v18, :cond_93a

    #@8f8
    .line 4020
    const-string v4, "android.intent.action.MAIN"

    #@8fa
    move-object/from16 v0, v18

    #@8fc
    invoke-static {v4, v0}, Landroid/content/Intent;->makeMainSelectorActivity(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@8ff
    move-result-object v5

    #@900
    .line 4021
    .restart local v5       #intent:Landroid/content/Intent;
    const/high16 v4, 0x1000

    #@902
    invoke-virtual {v5, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@905
    .line 4023
    :try_start_905
    move-object/from16 v0, p0

    #@907
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@909
    invoke-virtual {v4, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_90c
    .catch Landroid/content/ActivityNotFoundException; {:try_start_905 .. :try_end_90c} :catch_910

    #@90c
    .line 4029
    :goto_90c
    const-wide/16 v6, -0x1

    #@90e
    goto/16 :goto_97

    #@910
    .line 4024
    :catch_910
    move-exception v23

    #@911
    .line 4025
    .restart local v23       #ex:Landroid/content/ActivityNotFoundException;
    const-string v4, "WindowManager"

    #@913
    new-instance v6, Ljava/lang/StringBuilder;

    #@915
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@918
    const-string v7, "Dropping application launch key because the activity to which it is registered was not found: keyCode="

    #@91a
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@91d
    move-result-object v6

    #@91e
    move/from16 v0, v30

    #@920
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@923
    move-result-object v6

    #@924
    const-string v7, ", category="

    #@926
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@929
    move-result-object v6

    #@92a
    move-object/from16 v0, v18

    #@92c
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@92f
    move-result-object v6

    #@930
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@933
    move-result-object v6

    #@934
    move-object/from16 v0, v23

    #@936
    invoke-static {v4, v6, v0}, Landroid/util/Slog;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@939
    goto :goto_90c

    #@93a
    .line 4034
    .end local v5           #intent:Landroid/content/Intent;
    .end local v18           #category:Ljava/lang/String;
    .end local v23           #ex:Landroid/content/ActivityNotFoundException;
    :cond_93a
    if-eqz v21, :cond_977

    #@93c
    if-nez v36, :cond_977

    #@93e
    const/16 v4, 0x3d

    #@940
    move/from16 v0, v30

    #@942
    if-ne v0, v4, :cond_977

    #@944
    .line 4035
    move-object/from16 v0, p0

    #@946
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRecentAppsDialogHeldModifiers:I

    #@948
    if-nez v4, :cond_994

    #@94a
    if-nez v31, :cond_994

    #@94c
    .line 4036
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getModifiers()I

    #@94f
    move-result v4

    #@950
    and-int/lit16 v0, v4, -0xc2

    #@952
    move/from16 v38, v0

    #@954
    .line 4037
    .local v38, shiftlessModifiers:I
    const/4 v4, 0x2

    #@955
    move/from16 v0, v38

    #@957
    invoke-static {v0, v4}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@95a
    move-result v4

    #@95b
    if-nez v4, :cond_967

    #@95d
    const/high16 v4, 0x1

    #@95f
    move/from16 v0, v38

    #@961
    invoke-static {v0, v4}, Landroid/view/KeyEvent;->metaStateHasModifiers(II)Z

    #@964
    move-result v4

    #@965
    if-eqz v4, :cond_994

    #@967
    .line 4040
    :cond_967
    move/from16 v0, v38

    #@969
    move-object/from16 v1, p0

    #@96b
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRecentAppsDialogHeldModifiers:I

    #@96d
    .line 4041
    const/4 v4, 0x1

    #@96e
    move-object/from16 v0, p0

    #@970
    invoke-virtual {v0, v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->showOrHideRecentAppsDialog(I)V

    #@973
    .line 4042
    const-wide/16 v6, -0x1

    #@975
    goto/16 :goto_97

    #@977
    .line 4045
    .end local v38           #shiftlessModifiers:I
    :cond_977
    if-nez v21, :cond_994

    #@979
    move-object/from16 v0, p0

    #@97b
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRecentAppsDialogHeldModifiers:I

    #@97d
    if-eqz v4, :cond_994

    #@97f
    move-object/from16 v0, p0

    #@981
    iget v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRecentAppsDialogHeldModifiers:I

    #@983
    and-int v4, v4, v33

    #@985
    if-nez v4, :cond_994

    #@987
    .line 4047
    const/4 v4, 0x0

    #@988
    move-object/from16 v0, p0

    #@98a
    iput v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRecentAppsDialogHeldModifiers:I

    #@98c
    .line 4048
    if-eqz v31, :cond_9dc

    #@98e
    const/4 v4, 0x2

    #@98f
    :goto_98f
    move-object/from16 v0, p0

    #@991
    invoke-virtual {v0, v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->showOrHideRecentAppsDialog(I)V

    #@994
    .line 4053
    :cond_994
    if-eqz v21, :cond_9bf

    #@996
    if-nez v36, :cond_9bf

    #@998
    const/16 v4, 0xcc

    #@99a
    move/from16 v0, v30

    #@99c
    if-eq v0, v4, :cond_9aa

    #@99e
    const/16 v4, 0x3e

    #@9a0
    move/from16 v0, v30

    #@9a2
    if-ne v0, v4, :cond_9bf

    #@9a4
    move/from16 v0, v33

    #@9a6
    and-int/lit16 v4, v0, 0x7000

    #@9a8
    if-eqz v4, :cond_9bf

    #@9aa
    .line 4057
    :cond_9aa
    move/from16 v0, v33

    #@9ac
    and-int/lit16 v4, v0, 0xc1

    #@9ae
    if-eqz v4, :cond_9de

    #@9b0
    const/16 v20, -0x1

    #@9b2
    .line 4058
    .local v20, direction:I
    :goto_9b2
    move-object/from16 v0, p0

    #@9b4
    iget-object v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@9b6
    invoke-virtual/range {p2 .. p2}, Landroid/view/KeyEvent;->getDeviceId()I

    #@9b9
    move-result v6

    #@9ba
    move/from16 v0, v20

    #@9bc
    invoke-interface {v4, v6, v0}, Landroid/view/WindowManagerPolicy$WindowManagerFuncs;->switchKeyboardLayout(II)V

    #@9bf
    .line 4061
    .end local v20           #direction:I
    :cond_9bf
    move-object/from16 v0, p0

    #@9c1
    iget-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLanguageSwitchKeyPressed:Z

    #@9c3
    if-eqz v4, :cond_9e1

    #@9c5
    if-nez v21, :cond_9e1

    #@9c7
    const/16 v4, 0xcc

    #@9c9
    move/from16 v0, v30

    #@9cb
    if-eq v0, v4, :cond_9d3

    #@9cd
    const/16 v4, 0x3e

    #@9cf
    move/from16 v0, v30

    #@9d1
    if-ne v0, v4, :cond_9e1

    #@9d3
    .line 4064
    :cond_9d3
    const/4 v4, 0x0

    #@9d4
    move-object/from16 v0, p0

    #@9d6
    iput-boolean v4, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLanguageSwitchKeyPressed:Z

    #@9d8
    .line 4065
    const-wide/16 v6, -0x1

    #@9da
    goto/16 :goto_97

    #@9dc
    .line 4048
    :cond_9dc
    const/4 v4, 0x3

    #@9dd
    goto :goto_98f

    #@9de
    .line 4057
    :cond_9de
    const/16 v20, 0x1

    #@9e0
    goto :goto_9b2

    #@9e1
    .line 4069
    :cond_9e1
    const-wide/16 v6, 0x0

    #@9e3
    goto/16 :goto_97
.end method

.method public interceptKeyBeforeQueueing(Landroid/view/KeyEvent;IZ)I
    .registers 29
    .parameter "event"
    .parameter "policyFlags"
    .parameter "isScreenOn"

    #@0
    .prologue
    .line 6115
    move-object/from16 v0, p0

    #@2
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemBooted:Z

    #@4
    move/from16 v21, v0

    #@6
    if-nez v21, :cond_b

    #@8
    .line 6117
    const/16 v17, 0x0

    #@a
    .line 6797
    :cond_a
    :goto_a
    return v17

    #@b
    .line 6120
    :cond_b
    move-object/from16 v0, p0

    #@d
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@f
    move-object/from16 v21, v0

    #@11
    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManagerPolicy$WindowManagerFuncs;->isProgressingExit()Z

    #@14
    move-result v21

    #@15
    if-eqz v21, :cond_1a

    #@17
    .line 6122
    const/16 v17, 0x0

    #@19
    goto :goto_a

    #@1a
    .line 6125
    :cond_1a
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getAction()I

    #@1d
    move-result v21

    #@1e
    if-nez v21, :cond_6a

    #@20
    const/4 v7, 0x1

    #@21
    .line 6126
    .local v7, down:Z
    :goto_21
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->isCanceled()Z

    #@24
    move-result v6

    #@25
    .line 6127
    .local v6, canceled:Z
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@28
    move-result v13

    #@29
    .line 6129
    .local v13, keyCode:I
    const/high16 v21, 0x100

    #@2b
    and-int v21, v21, p2

    #@2d
    if-eqz v21, :cond_6c

    #@2f
    const/4 v11, 0x1

    #@30
    .line 6133
    .local v11, isInjected:Z
    :goto_30
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getKeyCode()I

    #@33
    move-result v21

    #@34
    const/16 v22, 0xdc

    #@36
    move/from16 v0, v21

    #@38
    move/from16 v1, v22

    #@3a
    if-ne v0, v1, :cond_4f

    #@3c
    .line 6134
    const-string v21, "WindowManager"

    #@3e
    const-string v22, "KEY_TESTMODE_UNLOCK : Clear pattern lock"

    #@40
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@43
    .line 6135
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->systemReady()V

    #@46
    .line 6136
    move-object/from16 v0, p0

    #@48
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@4a
    move-object/from16 v21, v0

    #@4c
    invoke-virtual/range {v21 .. v21}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->clearLockPattern()V

    #@4f
    .line 6140
    :cond_4f
    move-object/from16 v0, p0

    #@51
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOperator:Ljava/lang/String;

    #@53
    move-object/from16 v21, v0

    #@55
    const-string v22, "VZW"

    #@57
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@5a
    move-result v21

    #@5b
    if-eqz v21, :cond_6e

    #@5d
    .line 6141
    sget-boolean v21, Lcom/android/internal/policy/impl/PhoneWindowManager;->SPC_ERR_FREEZE:Z

    #@5f
    const/16 v22, 0x1

    #@61
    move/from16 v0, v21

    #@63
    move/from16 v1, v22

    #@65
    if-ne v0, v1, :cond_6e

    #@67
    .line 6142
    const/16 v17, 0x2

    #@69
    goto :goto_a

    #@6a
    .line 6125
    .end local v6           #canceled:Z
    .end local v7           #down:Z
    .end local v11           #isInjected:Z
    .end local v13           #keyCode:I
    :cond_6a
    const/4 v7, 0x0

    #@6b
    goto :goto_21

    #@6c
    .line 6129
    .restart local v6       #canceled:Z
    .restart local v7       #down:Z
    .restart local v13       #keyCode:I
    :cond_6c
    const/4 v11, 0x0

    #@6d
    goto :goto_30

    #@6e
    .line 6151
    .restart local v11       #isInjected:Z
    :cond_6e
    move-object/from16 v0, p0

    #@70
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@72
    move-object/from16 v21, v0

    #@74
    if-nez v21, :cond_170

    #@76
    const/4 v14, 0x0

    #@77
    .line 6156
    .local v14, keyguardActive:Z
    :goto_77
    const/16 v21, 0x1a

    #@79
    move/from16 v0, v21

    #@7b
    if-ne v13, v0, :cond_7f

    #@7d
    .line 6157
    or-int/lit8 p2, p2, 0x1

    #@7f
    .line 6159
    :cond_7f
    and-int/lit8 v21, p2, 0x3

    #@81
    if-eqz v21, :cond_18a

    #@83
    const/4 v12, 0x1

    #@84
    .line 6163
    .local v12, isWakeKey:Z
    :goto_84
    const-string v21, "WindowManager"

    #@86
    new-instance v22, Ljava/lang/StringBuilder;

    #@88
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@8b
    const-string v23, "interceptKeyTq keycode="

    #@8d
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@90
    move-result-object v22

    #@91
    move-object/from16 v0, v22

    #@93
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@96
    move-result-object v22

    #@97
    const-string v23, " screenIsOn="

    #@99
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@9c
    move-result-object v22

    #@9d
    move-object/from16 v0, v22

    #@9f
    move/from16 v1, p3

    #@a1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@a4
    move-result-object v22

    #@a5
    const-string v23, " keyguardActive="

    #@a7
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@aa
    move-result-object v22

    #@ab
    move-object/from16 v0, v22

    #@ad
    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@b0
    move-result-object v22

    #@b1
    const-string v23, " policyFlags="

    #@b3
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b6
    move-result-object v22

    #@b7
    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    #@ba
    move-result-object v23

    #@bb
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@be
    move-result-object v22

    #@bf
    const-string v23, " isWakeKey="

    #@c1
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c4
    move-result-object v22

    #@c5
    move-object/from16 v0, v22

    #@c7
    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@ca
    move-result-object v22

    #@cb
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@ce
    move-result-object v22

    #@cf
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    .line 6169
    if-eqz v7, :cond_f3

    #@d4
    move/from16 v0, p2

    #@d6
    and-int/lit16 v0, v0, 0x100

    #@d8
    move/from16 v21, v0

    #@da
    if-eqz v21, :cond_f3

    #@dc
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getRepeatCount()I

    #@df
    move-result v21

    #@e0
    if-nez v21, :cond_f3

    #@e2
    .line 6171
    const/16 v21, 0x0

    #@e4
    const/16 v22, 0x1

    #@e6
    const/16 v23, 0x0

    #@e8
    move-object/from16 v0, p0

    #@ea
    move-object/from16 v1, v21

    #@ec
    move/from16 v2, v22

    #@ee
    move/from16 v3, v23

    #@f0
    invoke-virtual {v0, v1, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    #@f3
    .line 6175
    :cond_f3
    const/16 v21, 0x1a

    #@f5
    move/from16 v0, v21

    #@f7
    if-ne v13, v0, :cond_134

    #@f9
    if-nez v7, :cond_134

    #@fb
    move-object/from16 v0, p0

    #@fd
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverClosed:Z

    #@ff
    move/from16 v21, v0

    #@101
    if-nez v21, :cond_134

    #@103
    .line 6176
    move-object/from16 v0, p0

    #@105
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensorEnabled:Z

    #@107
    move/from16 v21, v0

    #@109
    if-nez v21, :cond_134

    #@10b
    move-object/from16 v0, p0

    #@10d
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyScreenOn:Z

    #@10f
    move/from16 v21, v0

    #@111
    if-nez v21, :cond_134

    #@113
    .line 6177
    move-object/from16 v0, p0

    #@115
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@117
    move-object/from16 v21, v0

    #@119
    move-object/from16 v0, p0

    #@11b
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyPress:Ljava/lang/Runnable;

    #@11d
    move-object/from16 v22, v0

    #@11f
    invoke-virtual/range {v21 .. v22}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@122
    .line 6178
    const/16 v21, 0x0

    #@124
    move-object/from16 v0, p0

    #@126
    move/from16 v1, v21

    #@128
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setProximitySensorEnabled(Z)V

    #@12b
    .line 6179
    const/16 v21, 0x1

    #@12d
    move-object/from16 v0, p0

    #@12f
    move/from16 v1, v21

    #@131
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setProximitySensorEnabled(Z)V

    #@134
    .line 6193
    :cond_134
    if-eqz p3, :cond_13e

    #@136
    move-object/from16 v0, p0

    #@138
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadless:Z

    #@13a
    move/from16 v21, v0

    #@13c
    if-eqz v21, :cond_142

    #@13e
    :cond_13e
    if-eqz v11, :cond_18d

    #@140
    if-nez v12, :cond_18d

    #@142
    .line 6195
    :cond_142
    const/16 v17, 0x1

    #@144
    .line 6215
    .local v17, result:I
    :cond_144
    :goto_144
    sparse-switch v13, :sswitch_data_a28

    #@147
    goto/16 :goto_a

    #@149
    .line 6217
    :sswitch_149
    if-nez v7, :cond_a

    #@14b
    .line 6218
    if-eqz p3, :cond_a

    #@14d
    if-nez v14, :cond_a

    #@14f
    move-object/from16 v0, p0

    #@151
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEasyAccessEnabled:Z

    #@153
    move/from16 v21, v0

    #@155
    if-eqz v21, :cond_a

    #@157
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    #@15a
    move-result v21

    #@15b
    move/from16 v0, v21

    #@15d
    and-int/lit16 v0, v0, 0x400

    #@15f
    move/from16 v21, v0

    #@161
    if-nez v21, :cond_a

    #@163
    .line 6222
    const/16 v21, 0x0

    #@165
    move/from16 v0, v21

    #@167
    move-object/from16 v1, p0

    #@169
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeUpKeyConsumedByEasyAccess:Z

    #@16b
    .line 6223
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptEasyAccessEntry()V

    #@16e
    goto/16 :goto_a

    #@170
    .line 6151
    .end local v12           #isWakeKey:Z
    .end local v14           #keyguardActive:Z
    .end local v17           #result:I
    :cond_170
    if-eqz p3, :cond_17e

    #@172
    move-object/from16 v0, p0

    #@174
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@176
    move-object/from16 v21, v0

    #@178
    invoke-virtual/range {v21 .. v21}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowingAndNotHidden()Z

    #@17b
    move-result v14

    #@17c
    goto/16 :goto_77

    #@17e
    :cond_17e
    move-object/from16 v0, p0

    #@180
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@182
    move-object/from16 v21, v0

    #@184
    invoke-virtual/range {v21 .. v21}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@187
    move-result v14

    #@188
    goto/16 :goto_77

    #@18a
    .line 6159
    .restart local v14       #keyguardActive:Z
    :cond_18a
    const/4 v12, 0x0

    #@18b
    goto/16 :goto_84

    #@18d
    .line 6199
    .restart local v12       #isWakeKey:Z
    :cond_18d
    const/16 v17, 0x0

    #@18f
    .line 6200
    .restart local v17       #result:I
    if-eqz v7, :cond_1a0

    #@191
    .line 6201
    move-object/from16 v0, p0

    #@193
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@195
    move-object/from16 v21, v0

    #@197
    move-object/from16 v0, p0

    #@199
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSafetyCareIntent:Landroid/content/Intent;

    #@19b
    move-object/from16 v22, v0

    #@19d
    invoke-virtual/range {v21 .. v22}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@1a0
    .line 6203
    :cond_1a0
    if-eqz v7, :cond_144

    #@1a2
    if-eqz v12, :cond_144

    #@1a4
    move-object/from16 v0, p0

    #@1a6
    invoke-direct {v0, v13}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isWakeKeyWhenScreenOff(I)Z

    #@1a9
    move-result v21

    #@1aa
    if-eqz v21, :cond_144

    #@1ac
    .line 6204
    if-eqz v14, :cond_1ba

    #@1ae
    .line 6206
    move-object/from16 v0, p0

    #@1b0
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@1b2
    move-object/from16 v21, v0

    #@1b4
    move-object/from16 v0, v21

    #@1b6
    invoke-virtual {v0, v13}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->onWakeKeyWhenKeyguardShowingTq(I)V

    #@1b9
    goto :goto_144

    #@1ba
    .line 6209
    :cond_1ba
    or-int/lit8 v17, v17, 0x2

    #@1bc
    goto :goto_144

    #@1bd
    .line 6232
    :sswitch_1bd
    if-nez p3, :cond_1f0

    #@1bf
    if-eqz v7, :cond_1f0

    #@1c1
    .line 6234
    :try_start_1c1
    move-object/from16 v0, p0

    #@1c3
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManager:Landroid/view/IWindowManager;

    #@1c5
    move-object/from16 v21, v0

    #@1c7
    invoke-interface/range {v21 .. v21}, Landroid/view/IWindowManager;->getDsdrStatus()I

    #@1ca
    move-result v21

    #@1cb
    const/16 v22, 0x3

    #@1cd
    move/from16 v0, v21

    #@1cf
    move/from16 v1, v22

    #@1d1
    if-ne v0, v1, :cond_1f0

    #@1d3
    .line 6235
    new-instance v20, Landroid/content/Intent;

    #@1d5
    const-string v21, "com.lge.lcdoff.volumekey"

    #@1d7
    invoke-direct/range {v20 .. v21}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@1da
    .line 6236
    .local v20, volumeKeyIntent:Landroid/content/Intent;
    const-string v21, "keycode"

    #@1dc
    move-object/from16 v0, v20

    #@1de
    move-object/from16 v1, v21

    #@1e0
    invoke-virtual {v0, v1, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    #@1e3
    .line 6237
    move-object/from16 v0, p0

    #@1e5
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1e7
    move-object/from16 v21, v0

    #@1e9
    move-object/from16 v0, v21

    #@1eb
    move-object/from16 v1, v20

    #@1ed
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_1f0
    .catch Landroid/os/RemoteException; {:try_start_1c1 .. :try_end_1f0} :catch_a24

    #@1f0
    .line 6244
    .end local v20           #volumeKeyIntent:Landroid/content/Intent;
    :cond_1f0
    :goto_1f0
    :sswitch_1f0
    const/16 v21, 0x19

    #@1f2
    move/from16 v0, v21

    #@1f4
    if-ne v13, v0, :cond_3ab

    #@1f6
    .line 6245
    if-eqz v7, :cond_37a

    #@1f8
    .line 6246
    if-eqz p3, :cond_250

    #@1fa
    move-object/from16 v0, p0

    #@1fc
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@1fe
    move/from16 v21, v0

    #@200
    if-nez v21, :cond_250

    #@202
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    #@205
    move-result v21

    #@206
    move/from16 v0, v21

    #@208
    and-int/lit16 v0, v0, 0x400

    #@20a
    move/from16 v21, v0

    #@20c
    if-nez v21, :cond_250

    #@20e
    .line 6248
    const/16 v21, 0x1

    #@210
    move/from16 v0, v21

    #@212
    move-object/from16 v1, p0

    #@214
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@216
    .line 6249
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    #@219
    move-result-wide v21

    #@21a
    move-wide/from16 v0, v21

    #@21c
    move-object/from16 v2, p0

    #@21e
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTime:J

    #@220
    .line 6250
    const/16 v21, 0x0

    #@222
    move/from16 v0, v21

    #@224
    move-object/from16 v1, p0

    #@226
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByScreenshotChord:Z

    #@228
    .line 6251
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingPowerKeyAction()V

    #@22b
    .line 6252
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptScreenshotChord()V

    #@22e
    .line 6253
    move-object/from16 v0, p0

    #@230
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@232
    move/from16 v21, v0

    #@234
    if-nez v21, :cond_23e

    #@236
    move-object/from16 v0, p0

    #@238
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@23a
    move/from16 v21, v0

    #@23c
    if-eqz v21, :cond_250

    #@23e
    .line 6254
    :cond_23e
    const-string v21, "WindowManager"

    #@240
    const-string v22, "BeforeQueueing ==> KEYCODE_VOLUME_DOWN"

    #@242
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@245
    .line 6255
    const/16 v21, 0x0

    #@247
    move/from16 v0, v21

    #@249
    move-object/from16 v1, p0

    #@24b
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByQuickClipChord:Z

    #@24d
    .line 6256
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptQuickClipChord()V

    #@250
    .line 6260
    :cond_250
    move-object/from16 v0, p0

    #@252
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@254
    move/from16 v21, v0

    #@256
    if-eqz v21, :cond_28d

    #@258
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    #@25b
    move-result v21

    #@25c
    move/from16 v0, v21

    #@25e
    and-int/lit16 v0, v0, 0x400

    #@260
    move/from16 v21, v0

    #@262
    if-nez v21, :cond_28d

    #@264
    .line 6262
    const/16 v21, 0x1

    #@266
    move/from16 v0, v21

    #@268
    move-object/from16 v1, p0

    #@26a
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@26c
    .line 6263
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    #@26f
    move-result-wide v21

    #@270
    move-wide/from16 v0, v21

    #@272
    move-object/from16 v2, p0

    #@274
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTime:J

    #@276
    .line 6264
    const/16 v21, 0x0

    #@278
    move/from16 v0, v21

    #@27a
    move-object/from16 v1, p0

    #@27c
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyConsumedByLongPress:Z

    #@27e
    .line 6265
    const/16 v21, 0x0

    #@280
    move/from16 v0, v21

    #@282
    move-object/from16 v1, p0

    #@284
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownLongKeyCancelReason:I

    #@286
    .line 6266
    move-object/from16 v0, p0

    #@288
    move/from16 v1, p3

    #@28a
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptVolumeDownLongKey(Z)V

    #@28d
    .line 6324
    :cond_28d
    :goto_28d
    if-eqz v7, :cond_54a

    #@28f
    .line 6326
    const/16 v21, 0x5

    #@291
    const/16 v22, 0x0

    #@293
    invoke-static/range {v21 .. v22}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@296
    move-result v21

    #@297
    if-eqz v21, :cond_2af

    #@299
    .line 6327
    const-string v21, "WindowManager"

    #@29b
    const-string v22, "interceptKeyBeforeQueueing: Stop notification sound"

    #@29d
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2a0
    .line 6328
    const-string v21, "notification"

    #@2a2
    invoke-static/range {v21 .. v21}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@2a5
    move-result-object v21

    #@2a6
    invoke-static/range {v21 .. v21}, Landroid/app/INotificationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/INotificationManager;

    #@2a9
    move-result-object v16

    #@2aa
    .line 6330
    .local v16, notificationManagerService:Landroid/app/INotificationManager;
    if-eqz v16, :cond_2af

    #@2ac
    .line 6332
    :try_start_2ac
    invoke-interface/range {v16 .. v16}, Landroid/app/INotificationManager;->stopSound()V
    :try_end_2af
    .catch Landroid/os/RemoteException; {:try_start_2ac .. :try_end_2af} :catch_4a3

    #@2af
    .line 6339
    .end local v16           #notificationManagerService:Landroid/app/INotificationManager;
    :cond_2af
    :goto_2af
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@2b2
    move-result-object v19

    #@2b3
    .line 6340
    .local v19, telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v19, :cond_348

    #@2b5
    .line 6343
    :try_start_2b5
    move-object/from16 v0, p0

    #@2b7
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@2b9
    move/from16 v21, v0

    #@2bb
    if-nez v21, :cond_4b5

    #@2bd
    .line 6345
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    #@2c0
    move-result v21

    #@2c1
    if-eqz v21, :cond_50f

    #@2c3
    .line 6352
    const-string v21, "WindowManager"

    #@2c5
    const-string v22, "interceptKeyBeforeQueueing: VOLUME key-down while ringing: Silence ringer!"

    #@2c7
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@2ca
    .line 6356
    move-object/from16 v0, p0

    #@2cc
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIsTargetTmoUs:Z

    #@2ce
    move/from16 v21, v0

    #@2d0
    if-eqz v21, :cond_31e

    #@2d2
    const/16 v21, 0x19

    #@2d4
    move/from16 v0, v21

    #@2d6
    if-eq v13, v0, :cond_2de

    #@2d8
    const/16 v21, 0x18

    #@2da
    move/from16 v0, v21

    #@2dc
    if-ne v13, v0, :cond_31e

    #@2de
    .line 6358
    :cond_2de
    move-object/from16 v0, p0

    #@2e0
    iget-wide v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@2e2
    move-wide/from16 v21, v0

    #@2e4
    const-wide/16 v23, 0x0

    #@2e6
    cmp-long v21, v21, v23

    #@2e8
    if-nez v21, :cond_31e

    #@2ea
    .line 6359
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@2ed
    move-result-wide v21

    #@2ee
    move-wide/from16 v0, v21

    #@2f0
    move-object/from16 v2, p0

    #@2f2
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@2f4
    .line 6360
    const-string v21, "WindowManager"

    #@2f6
    new-instance v22, Ljava/lang/StringBuilder;

    #@2f8
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@2fb
    const-string v23, "[UDUB] keyCode = "

    #@2fd
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@300
    move-result-object v22

    #@301
    move-object/from16 v0, v22

    #@303
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@306
    move-result-object v22

    #@307
    const-string v23, " down at mUDUBLongPressedStartTime="

    #@309
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@30c
    move-result-object v22

    #@30d
    move-object/from16 v0, p0

    #@30f
    iget-wide v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@311
    move-wide/from16 v23, v0

    #@313
    invoke-virtual/range {v22 .. v24}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@316
    move-result-object v22

    #@317
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@31a
    move-result-object v22

    #@31b
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@31e
    .line 6367
    :cond_31e
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isReservedCall()Z

    #@321
    move-result v21

    #@322
    if-nez v21, :cond_4b1

    #@324
    .line 6371
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->silenceRinger()V

    #@327
    .line 6375
    and-int/lit8 v17, v17, -0x2

    #@329
    .line 6381
    :goto_329
    move-object/from16 v0, p0

    #@32b
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@32d
    move/from16 v21, v0

    #@32f
    if-eqz v21, :cond_a

    #@331
    .line 6382
    const/16 v21, 0x3

    #@333
    move-object/from16 v0, p0

    #@335
    move/from16 v1, v21

    #@337
    invoke-direct {v0, v13, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingVolumeLongKeyAction(II)V
    :try_end_33a
    .catch Landroid/os/RemoteException; {:try_start_2b5 .. :try_end_33a} :catch_33c

    #@33a
    goto/16 :goto_a

    #@33c
    .line 6411
    :catch_33c
    move-exception v8

    #@33d
    .line 6412
    .local v8, ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@33f
    const-string v22, "ITelephony threw RemoteException"

    #@341
    move-object/from16 v0, v21

    #@343
    move-object/from16 v1, v22

    #@345
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@348
    .line 6416
    .end local v8           #ex:Landroid/os/RemoteException;
    :cond_348
    move-object/from16 v0, p0

    #@34a
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@34c
    move/from16 v21, v0

    #@34e
    if-eqz v21, :cond_365

    #@350
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isMusicActive()Z

    #@353
    move-result v21

    #@354
    if-nez v21, :cond_35c

    #@356
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isFMActive()Z

    #@359
    move-result v21

    #@35a
    if-eqz v21, :cond_365

    #@35c
    .line 6418
    :cond_35c
    const/16 v21, 0x2

    #@35e
    move-object/from16 v0, p0

    #@360
    move/from16 v1, v21

    #@362
    invoke-direct {v0, v13, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingVolumeLongKeyAction(II)V

    #@365
    .line 6421
    :cond_365
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isMusicActive()Z

    #@368
    move-result v21

    #@369
    if-eqz v21, :cond_535

    #@36b
    and-int/lit8 v21, v17, 0x1

    #@36d
    if-nez v21, :cond_535

    #@36f
    .line 6424
    const/16 v21, 0x3

    #@371
    move-object/from16 v0, p0

    #@373
    move/from16 v1, v21

    #@375
    invoke-virtual {v0, v1, v13}, Lcom/android/internal/policy/impl/PhoneWindowManager;->handleVolumeKey(II)V

    #@378
    goto/16 :goto_a

    #@37a
    .line 6269
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_37a
    const/16 v21, 0x0

    #@37c
    move/from16 v0, v21

    #@37e
    move-object/from16 v1, p0

    #@380
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@382
    .line 6270
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingScreenshotChordAction()V

    #@385
    .line 6271
    move-object/from16 v0, p0

    #@387
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@389
    move/from16 v21, v0

    #@38b
    if-nez v21, :cond_395

    #@38d
    move-object/from16 v0, p0

    #@38f
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@391
    move/from16 v21, v0

    #@393
    if-eqz v21, :cond_398

    #@395
    .line 6272
    :cond_395
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingQuickClipChordAction()V

    #@398
    .line 6275
    :cond_398
    move-object/from16 v0, p0

    #@39a
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@39c
    move/from16 v21, v0

    #@39e
    if-eqz v21, :cond_28d

    #@3a0
    .line 6276
    const/16 v21, 0x1

    #@3a2
    move-object/from16 v0, p0

    #@3a4
    move/from16 v1, v21

    #@3a6
    invoke-direct {v0, v13, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingVolumeLongKeyAction(II)V

    #@3a9
    goto/16 :goto_28d

    #@3ab
    .line 6279
    :cond_3ab
    const/16 v21, 0x18

    #@3ad
    move/from16 v0, v21

    #@3af
    if-ne v13, v0, :cond_28d

    #@3b1
    .line 6280
    if-eqz v7, :cond_467

    #@3b3
    .line 6281
    if-eqz p3, :cond_403

    #@3b5
    move-object/from16 v0, p0

    #@3b7
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@3b9
    move/from16 v21, v0

    #@3bb
    if-nez v21, :cond_403

    #@3bd
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    #@3c0
    move-result v21

    #@3c1
    move/from16 v0, v21

    #@3c3
    and-int/lit16 v0, v0, 0x400

    #@3c5
    move/from16 v21, v0

    #@3c7
    if-nez v21, :cond_403

    #@3c9
    .line 6283
    const/16 v21, 0x1

    #@3cb
    move/from16 v0, v21

    #@3cd
    move-object/from16 v1, p0

    #@3cf
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@3d1
    .line 6284
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingPowerKeyAction()V

    #@3d4
    .line 6285
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingScreenshotChordAction()V

    #@3d7
    .line 6286
    move-object/from16 v0, p0

    #@3d9
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@3db
    move/from16 v21, v0

    #@3dd
    if-nez v21, :cond_3e7

    #@3df
    move-object/from16 v0, p0

    #@3e1
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@3e3
    move/from16 v21, v0

    #@3e5
    if-eqz v21, :cond_403

    #@3e7
    .line 6287
    :cond_3e7
    const-string v21, "WindowManager"

    #@3e9
    const-string v22, "BeforeQueueing ==> KEYCODE_VOLUME_UP"

    #@3eb
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@3ee
    .line 6288
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    #@3f1
    move-result-wide v21

    #@3f2
    move-wide/from16 v0, v21

    #@3f4
    move-object/from16 v2, p0

    #@3f6
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTime:J

    #@3f8
    .line 6289
    const/16 v21, 0x0

    #@3fa
    move/from16 v0, v21

    #@3fc
    move-object/from16 v1, p0

    #@3fe
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedByQuickClipChord:Z

    #@400
    .line 6290
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptQuickClipChord()V

    #@403
    .line 6294
    :cond_403
    move-object/from16 v0, p0

    #@405
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppAvailable:Z

    #@407
    move/from16 v21, v0

    #@409
    if-eqz v21, :cond_428

    #@40b
    .line 6295
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    #@40e
    move-result-wide v21

    #@40f
    move-wide/from16 v0, v21

    #@411
    move-object/from16 v2, p0

    #@413
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTime:J

    #@415
    .line 6296
    const/16 v21, 0x0

    #@417
    move/from16 v0, v21

    #@419
    move-object/from16 v1, p0

    #@41b
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedBy112App:Z

    #@41d
    .line 6297
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->intercept112AppChord()V

    #@420
    .line 6298
    const/16 v21, 0x1

    #@422
    move/from16 v0, v21

    #@424
    move-object/from16 v1, p0

    #@426
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@428
    .line 6301
    :cond_428
    move-object/from16 v0, p0

    #@42a
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@42c
    move/from16 v21, v0

    #@42e
    if-eqz v21, :cond_28d

    #@430
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    #@433
    move-result v21

    #@434
    move/from16 v0, v21

    #@436
    and-int/lit16 v0, v0, 0x400

    #@438
    move/from16 v21, v0

    #@43a
    if-nez v21, :cond_28d

    #@43c
    .line 6303
    const/16 v21, 0x1

    #@43e
    move/from16 v0, v21

    #@440
    move-object/from16 v1, p0

    #@442
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@444
    .line 6304
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    #@447
    move-result-wide v21

    #@448
    move-wide/from16 v0, v21

    #@44a
    move-object/from16 v2, p0

    #@44c
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTime:J

    #@44e
    .line 6305
    const/16 v21, 0x0

    #@450
    move/from16 v0, v21

    #@452
    move-object/from16 v1, p0

    #@454
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyConsumedByLongPress:Z

    #@456
    .line 6306
    const/16 v21, 0x0

    #@458
    move/from16 v0, v21

    #@45a
    move-object/from16 v1, p0

    #@45c
    iput v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpLongKeyCancelReason:I

    #@45e
    .line 6307
    move-object/from16 v0, p0

    #@460
    move/from16 v1, p3

    #@462
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptVolumeUpLongKey(Z)V

    #@465
    goto/16 :goto_28d

    #@467
    .line 6310
    :cond_467
    const/16 v21, 0x0

    #@469
    move/from16 v0, v21

    #@46b
    move-object/from16 v1, p0

    #@46d
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@46f
    .line 6311
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingScreenshotChordAction()V

    #@472
    .line 6312
    move-object/from16 v0, p0

    #@474
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@476
    move/from16 v21, v0

    #@478
    if-nez v21, :cond_482

    #@47a
    move-object/from16 v0, p0

    #@47c
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@47e
    move/from16 v21, v0

    #@480
    if-eqz v21, :cond_485

    #@482
    .line 6313
    :cond_482
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingQuickClipChordAction()V

    #@485
    .line 6315
    :cond_485
    move-object/from16 v0, p0

    #@487
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppAvailable:Z

    #@489
    move/from16 v21, v0

    #@48b
    if-eqz v21, :cond_490

    #@48d
    .line 6316
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPending112AppChordAction()V

    #@490
    .line 6319
    :cond_490
    move-object/from16 v0, p0

    #@492
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@494
    move/from16 v21, v0

    #@496
    if-eqz v21, :cond_28d

    #@498
    .line 6320
    const/16 v21, 0x1

    #@49a
    move-object/from16 v0, p0

    #@49c
    move/from16 v1, v21

    #@49e
    invoke-direct {v0, v13, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingVolumeLongKeyAction(II)V

    #@4a1
    goto/16 :goto_28d

    #@4a3
    .line 6333
    .restart local v16       #notificationManagerService:Landroid/app/INotificationManager;
    :catch_4a3
    move-exception v8

    #@4a4
    .line 6334
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@4a6
    const-string v22, "INotificationManager threw RemoteException"

    #@4a8
    move-object/from16 v0, v21

    #@4aa
    move-object/from16 v1, v22

    #@4ac
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@4af
    goto/16 :goto_2af

    #@4b1
    .line 6378
    .end local v8           #ex:Landroid/os/RemoteException;
    .end local v16           #notificationManagerService:Landroid/app/INotificationManager;
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_4b1
    and-int/lit8 v17, v17, 0x1

    #@4b3
    goto/16 :goto_329

    #@4b5
    .line 6388
    :cond_4b5
    :try_start_4b5
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    #@4b8
    move-result v21

    #@4b9
    if-eqz v21, :cond_50f

    #@4bb
    .line 6389
    move-object/from16 v0, p0

    #@4bd
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIsTargetTmoUs:Z

    #@4bf
    move/from16 v21, v0

    #@4c1
    if-eqz v21, :cond_50f

    #@4c3
    const/16 v21, 0x19

    #@4c5
    move/from16 v0, v21

    #@4c7
    if-eq v13, v0, :cond_4cf

    #@4c9
    const/16 v21, 0x18

    #@4cb
    move/from16 v0, v21

    #@4cd
    if-ne v13, v0, :cond_50f

    #@4cf
    .line 6391
    :cond_4cf
    move-object/from16 v0, p0

    #@4d1
    iget-wide v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@4d3
    move-wide/from16 v21, v0

    #@4d5
    const-wide/16 v23, 0x0

    #@4d7
    cmp-long v21, v21, v23

    #@4d9
    if-nez v21, :cond_50f

    #@4db
    .line 6392
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@4de
    move-result-wide v21

    #@4df
    move-wide/from16 v0, v21

    #@4e1
    move-object/from16 v2, p0

    #@4e3
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@4e5
    .line 6393
    const-string v21, "WindowManager"

    #@4e7
    new-instance v22, Ljava/lang/StringBuilder;

    #@4e9
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@4ec
    const-string v23, "[UDUB] keyCode = "

    #@4ee
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4f1
    move-result-object v22

    #@4f2
    move-object/from16 v0, v22

    #@4f4
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@4f7
    move-result-object v22

    #@4f8
    const-string v23, " down at mUDUBLongPressedStartTime="

    #@4fa
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@4fd
    move-result-object v22

    #@4fe
    move-object/from16 v0, p0

    #@500
    iget-wide v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@502
    move-wide/from16 v23, v0

    #@504
    invoke-virtual/range {v22 .. v24}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@507
    move-result-object v22

    #@508
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@50b
    move-result-object v22

    #@50c
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@50f
    .line 6400
    :cond_50f
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z

    #@512
    move-result v21

    #@513
    if-eqz v21, :cond_348

    #@515
    and-int/lit8 v21, v17, 0x1

    #@517
    if-nez v21, :cond_348

    #@519
    .line 6404
    const/16 v21, 0x0

    #@51b
    move-object/from16 v0, p0

    #@51d
    move/from16 v1, v21

    #@51f
    invoke-virtual {v0, v1, v13}, Lcom/android/internal/policy/impl/PhoneWindowManager;->handleVolumeKey(II)V

    #@522
    .line 6406
    move-object/from16 v0, p0

    #@524
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeKeyLongPressEnabled:Z

    #@526
    move/from16 v21, v0

    #@528
    if-eqz v21, :cond_a

    #@52a
    .line 6407
    const/16 v21, 0x4

    #@52c
    move-object/from16 v0, p0

    #@52e
    move/from16 v1, v21

    #@530
    invoke-direct {v0, v13, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingVolumeLongKeyAction(II)V
    :try_end_533
    .catch Landroid/os/RemoteException; {:try_start_4b5 .. :try_end_533} :catch_33c

    #@533
    goto/16 :goto_a

    #@535
    .line 6426
    :cond_535
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isFMActive()Z

    #@538
    move-result v21

    #@539
    if-eqz v21, :cond_a

    #@53b
    and-int/lit8 v21, v17, 0x1

    #@53d
    if-nez v21, :cond_a

    #@53f
    .line 6427
    const/16 v21, 0xa

    #@541
    move-object/from16 v0, p0

    #@543
    move/from16 v1, v21

    #@545
    invoke-virtual {v0, v1, v13}, Lcom/android/internal/policy/impl/PhoneWindowManager;->handleVolumeKey(II)V

    #@548
    goto/16 :goto_a

    #@54a
    .line 6431
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_54a
    move-object/from16 v0, p0

    #@54c
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIsTargetTmoUs:Z

    #@54e
    move/from16 v21, v0

    #@550
    if-eqz v21, :cond_566

    #@552
    .line 6432
    move-object/from16 v0, p0

    #@554
    iget-wide v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@556
    move-wide/from16 v21, v0

    #@558
    const-wide/16 v23, 0x0

    #@55a
    cmp-long v21, v21, v23

    #@55c
    if-nez v21, :cond_588

    #@55e
    .line 6433
    const/16 v21, 0x0

    #@560
    move/from16 v0, v21

    #@562
    move-object/from16 v1, p0

    #@564
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSkipUdub:Z

    #@566
    .line 6461
    :cond_566
    :goto_566
    move-object/from16 v0, p0

    #@568
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@56a
    move/from16 v21, v0

    #@56c
    if-eqz v21, :cond_a

    #@56e
    .line 6462
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@571
    move-result-object v19

    #@572
    .line 6463
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v19, :cond_a

    #@574
    .line 6465
    :try_start_574
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    #@577
    move-result v21

    #@578
    if-eqz v21, :cond_a

    #@57a
    .line 6472
    const-string v21, "WindowManager"

    #@57c
    const-string v22, "interceptKeyBeforeQueueing: VOLUME key-down while ringing: Silence ringer!"

    #@57e
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@581
    .line 6478
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->silenceRinger()V
    :try_end_584
    .catch Landroid/os/RemoteException; {:try_start_574 .. :try_end_584} :catch_62d

    #@584
    .line 6482
    and-int/lit8 v17, v17, -0x2

    #@586
    goto/16 :goto_a

    #@588
    .line 6434
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_588
    const/16 v21, 0x19

    #@58a
    move/from16 v0, v21

    #@58c
    if-eq v13, v0, :cond_594

    #@58e
    const/16 v21, 0x18

    #@590
    move/from16 v0, v21

    #@592
    if-ne v13, v0, :cond_566

    #@594
    .line 6435
    :cond_594
    const-string v21, "WindowManager"

    #@596
    new-instance v22, Ljava/lang/StringBuilder;

    #@598
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@59b
    const-string v23, "[UDUB] mIsTargetTmoUs keyCode= "

    #@59d
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5a0
    move-result-object v22

    #@5a1
    move-object/from16 v0, v22

    #@5a3
    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@5a6
    move-result-object v22

    #@5a7
    const-string v23, ", mUDUBLongPressedStartTime = "

    #@5a9
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5ac
    move-result-object v22

    #@5ad
    move-object/from16 v0, p0

    #@5af
    iget-wide v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@5b1
    move-wide/from16 v23, v0

    #@5b3
    invoke-virtual/range {v22 .. v24}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    #@5b6
    move-result-object v22

    #@5b7
    const-string v23, ", mSkipUdub="

    #@5b9
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@5bc
    move-result-object v22

    #@5bd
    move-object/from16 v0, p0

    #@5bf
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSkipUdub:Z

    #@5c1
    move/from16 v23, v0

    #@5c3
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@5c6
    move-result-object v22

    #@5c7
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@5ca
    move-result-object v22

    #@5cb
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5ce
    .line 6438
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@5d1
    move-result-object v19

    #@5d2
    .line 6439
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v19, :cond_5fe

    #@5d4
    .line 6442
    :try_start_5d4
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    #@5d7
    move-result-wide v21

    #@5d8
    move-object/from16 v0, p0

    #@5da
    iget-wide v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@5dc
    move-wide/from16 v23, v0

    #@5de
    sub-long v21, v21, v23

    #@5e0
    const-wide/16 v23, 0x7cf

    #@5e2
    cmp-long v21, v21, v23

    #@5e4
    if-lez v21, :cond_5fe

    #@5e6
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    #@5e9
    move-result v21

    #@5ea
    if-eqz v21, :cond_5fe

    #@5ec
    .line 6444
    move-object/from16 v0, p0

    #@5ee
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSkipUdub:Z

    #@5f0
    move/from16 v21, v0

    #@5f2
    if-nez v21, :cond_5fe

    #@5f4
    .line 6445
    const-string v21, "WindowManager"

    #@5f6
    const-string v22, "[UDUB] call endCall"

    #@5f8
    invoke-static/range {v21 .. v22}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@5fb
    .line 6446
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->endCall()Z
    :try_end_5fe
    .catch Landroid/os/RemoteException; {:try_start_5d4 .. :try_end_5fe} :catch_610

    #@5fe
    .line 6455
    :cond_5fe
    :goto_5fe
    const-wide/16 v21, 0x0

    #@600
    move-wide/from16 v0, v21

    #@602
    move-object/from16 v2, p0

    #@604
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@606
    .line 6456
    const/16 v21, 0x0

    #@608
    move/from16 v0, v21

    #@60a
    move-object/from16 v1, p0

    #@60c
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSkipUdub:Z

    #@60e
    goto/16 :goto_566

    #@610
    .line 6449
    :catch_610
    move-exception v8

    #@611
    .line 6450
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-wide/16 v21, 0x0

    #@613
    move-wide/from16 v0, v21

    #@615
    move-object/from16 v2, p0

    #@617
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUDUBLongPressedStartTime:J

    #@619
    .line 6451
    const/16 v21, 0x0

    #@61b
    move/from16 v0, v21

    #@61d
    move-object/from16 v1, p0

    #@61f
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSkipUdub:Z

    #@621
    .line 6452
    const-string v21, "WindowManager"

    #@623
    const-string v22, "ITelephony threw RemoteException"

    #@625
    move-object/from16 v0, v21

    #@627
    move-object/from16 v1, v22

    #@629
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@62c
    goto :goto_5fe

    #@62d
    .line 6485
    .end local v8           #ex:Landroid/os/RemoteException;
    :catch_62d
    move-exception v8

    #@62e
    .line 6486
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@630
    const-string v22, "ITelephony threw RemoteException"

    #@632
    move-object/from16 v0, v21

    #@634
    move-object/from16 v1, v22

    #@636
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@639
    goto/16 :goto_a

    #@63b
    .line 6496
    .end local v8           #ex:Landroid/os/RemoteException;
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :sswitch_63b
    sget-boolean v21, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEY_EXCEPTION:Z

    #@63d
    if-eqz v21, :cond_66b

    #@63f
    .line 6497
    move-object/from16 v0, p0

    #@641
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@643
    move-object/from16 v21, v0

    #@645
    if-eqz v21, :cond_65f

    #@647
    move-object/from16 v0, p0

    #@649
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@64b
    move-object/from16 v21, v0

    #@64d
    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@650
    move-result-object v4

    #@651
    .line 6498
    .local v4, attrs:Landroid/view/WindowManager$LayoutParams;
    :goto_651
    if-eqz v4, :cond_66b

    #@653
    .line 6499
    iget v9, v4, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@655
    .line 6500
    .local v9, extend:I
    and-int/lit16 v0, v9, 0x800

    #@657
    move/from16 v21, v0

    #@659
    if-eqz v21, :cond_661

    #@65b
    .line 6501
    or-int/lit8 v17, v17, 0x1

    #@65d
    .line 6502
    goto/16 :goto_a

    #@65f
    .line 6497
    .end local v4           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v9           #extend:I
    :cond_65f
    const/4 v4, 0x0

    #@660
    goto :goto_651

    #@661
    .line 6503
    .restart local v4       #attrs:Landroid/view/WindowManager$LayoutParams;
    .restart local v9       #extend:I
    :cond_661
    and-int/lit16 v0, v9, 0x400

    #@663
    move/from16 v21, v0

    #@665
    if-eqz v21, :cond_66b

    #@667
    .line 6504
    and-int/lit8 v17, v17, -0x2

    #@669
    .line 6505
    goto/16 :goto_a

    #@66b
    .line 6509
    .end local v4           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v9           #extend:I
    :cond_66b
    and-int/lit8 v17, v17, -0x2

    #@66d
    .line 6510
    if-eqz v7, :cond_699

    #@66f
    .line 6511
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@672
    move-result-object v19

    #@673
    .line 6512
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    const/4 v10, 0x0

    #@674
    .line 6513
    .local v10, hungUp:Z
    if-eqz v19, :cond_67a

    #@676
    .line 6515
    :try_start_676
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->endCall()Z
    :try_end_679
    .catch Landroid/os/RemoteException; {:try_start_676 .. :try_end_679} :catch_689

    #@679
    move-result v10

    #@67a
    .line 6520
    :cond_67a
    :goto_67a
    if-eqz p3, :cond_67e

    #@67c
    if-eqz v10, :cond_696

    #@67e
    :cond_67e
    const/16 v21, 0x1

    #@680
    :goto_680
    move-object/from16 v0, p0

    #@682
    move/from16 v1, v21

    #@684
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptPowerKeyDown(Z)V

    #@687
    goto/16 :goto_a

    #@689
    .line 6516
    :catch_689
    move-exception v8

    #@68a
    .line 6517
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@68c
    const-string v22, "ITelephony threw RemoteException"

    #@68e
    move-object/from16 v0, v21

    #@690
    move-object/from16 v1, v22

    #@692
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@695
    goto :goto_67a

    #@696
    .line 6520
    .end local v8           #ex:Landroid/os/RemoteException;
    :cond_696
    const/16 v21, 0x0

    #@698
    goto :goto_680

    #@699
    .line 6522
    .end local v10           #hungUp:Z
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_699
    move-object/from16 v0, p0

    #@69b
    invoke-direct {v0, v6}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptPowerKeyUp(Z)Z

    #@69e
    move-result v21

    #@69f
    if-eqz v21, :cond_a

    #@6a1
    .line 6523
    move-object/from16 v0, p0

    #@6a3
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEndcallBehavior:I

    #@6a5
    move/from16 v21, v0

    #@6a7
    and-int/lit8 v21, v21, 0x1

    #@6a9
    if-eqz v21, :cond_6b1

    #@6ab
    .line 6525
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->goHome()Z

    #@6ae
    move-result v21

    #@6af
    if-nez v21, :cond_a

    #@6b1
    .line 6529
    :cond_6b1
    move-object/from16 v0, p0

    #@6b3
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEndcallBehavior:I

    #@6b5
    move/from16 v21, v0

    #@6b7
    and-int/lit8 v21, v21, 0x2

    #@6b9
    if-eqz v21, :cond_a

    #@6bb
    .line 6531
    and-int/lit8 v21, v17, -0x3

    #@6bd
    or-int/lit8 v17, v21, 0x4

    #@6bf
    goto/16 :goto_a

    #@6c1
    .line 6539
    :sswitch_6c1
    sget-boolean v21, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEY_EXCEPTION:Z

    #@6c3
    if-eqz v21, :cond_701

    #@6c5
    .line 6540
    move-object/from16 v0, p0

    #@6c7
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@6c9
    move-object/from16 v21, v0

    #@6cb
    if-eqz v21, :cond_6e5

    #@6cd
    move-object/from16 v0, p0

    #@6cf
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@6d1
    move-object/from16 v21, v0

    #@6d3
    invoke-interface/range {v21 .. v21}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@6d6
    move-result-object v4

    #@6d7
    .line 6541
    .restart local v4       #attrs:Landroid/view/WindowManager$LayoutParams;
    :goto_6d7
    if-eqz v4, :cond_701

    #@6d9
    .line 6542
    iget v9, v4, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@6db
    .line 6543
    .restart local v9       #extend:I
    and-int/lit16 v0, v9, 0x800

    #@6dd
    move/from16 v21, v0

    #@6df
    if-eqz v21, :cond_6e7

    #@6e1
    .line 6544
    or-int/lit8 v17, v17, 0x1

    #@6e3
    .line 6545
    goto/16 :goto_a

    #@6e5
    .line 6540
    .end local v4           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v9           #extend:I
    :cond_6e5
    const/4 v4, 0x0

    #@6e6
    goto :goto_6d7

    #@6e7
    .line 6546
    .restart local v4       #attrs:Landroid/view/WindowManager$LayoutParams;
    .restart local v9       #extend:I
    :cond_6e7
    and-int/lit16 v0, v9, 0x400

    #@6e9
    move/from16 v21, v0

    #@6eb
    if-eqz v21, :cond_6f1

    #@6ed
    .line 6547
    and-int/lit8 v17, v17, -0x2

    #@6ef
    .line 6548
    goto/16 :goto_a

    #@6f1
    .line 6550
    :cond_6f1
    const/high16 v21, 0x1

    #@6f3
    and-int v21, v21, v9

    #@6f5
    if-eqz v21, :cond_701

    #@6f7
    .line 6551
    or-int/lit8 v17, v17, 0x1

    #@6f9
    .line 6552
    const/16 v21, 0x1

    #@6fb
    move/from16 v0, v21

    #@6fd
    move-object/from16 v1, p0

    #@6ff
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBlockingLcdoff:Z

    #@701
    .line 6556
    .end local v4           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v9           #extend:I
    :cond_701
    and-int/lit8 v17, v17, -0x2

    #@703
    .line 6557
    if-eqz v7, :cond_8ce

    #@705
    .line 6559
    const/16 v21, 0x5

    #@707
    const/16 v22, 0x0

    #@709
    invoke-static/range {v21 .. v22}, Landroid/media/AudioSystem;->isStreamActive(II)Z

    #@70c
    move-result v21

    #@70d
    if-eqz v21, :cond_725

    #@70f
    .line 6560
    const-string v21, "WindowManager"

    #@711
    const-string v22, "interceptKeyBeforeQueueing: Stop notification sound"

    #@713
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@716
    .line 6561
    const-string v21, "notification"

    #@718
    invoke-static/range {v21 .. v21}, Landroid/os/ServiceManager;->checkService(Ljava/lang/String;)Landroid/os/IBinder;

    #@71b
    move-result-object v21

    #@71c
    invoke-static/range {v21 .. v21}, Landroid/app/INotificationManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/app/INotificationManager;

    #@71f
    move-result-object v16

    #@720
    .line 6563
    .restart local v16       #notificationManagerService:Landroid/app/INotificationManager;
    if-eqz v16, :cond_725

    #@722
    .line 6565
    :try_start_722
    invoke-interface/range {v16 .. v16}, Landroid/app/INotificationManager;->stopSound()V
    :try_end_725
    .catch Landroid/os/RemoteException; {:try_start_722 .. :try_end_725} :catch_804

    #@725
    .line 6572
    .end local v16           #notificationManagerService:Landroid/app/INotificationManager;
    :cond_725
    :goto_725
    if-eqz p3, :cond_750

    #@727
    move-object/from16 v0, p0

    #@729
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@72b
    move/from16 v21, v0

    #@72d
    if-nez v21, :cond_750

    #@72f
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getFlags()I

    #@732
    move-result v21

    #@733
    move/from16 v0, v21

    #@735
    and-int/lit16 v0, v0, 0x400

    #@737
    move/from16 v21, v0

    #@739
    if-nez v21, :cond_750

    #@73b
    .line 6574
    const/16 v21, 0x1

    #@73d
    move/from16 v0, v21

    #@73f
    move-object/from16 v1, p0

    #@741
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@743
    .line 6575
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    #@746
    move-result-wide v21

    #@747
    move-wide/from16 v0, v21

    #@749
    move-object/from16 v2, p0

    #@74b
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTime:J

    #@74d
    .line 6576
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptScreenshotChord()V

    #@750
    .line 6579
    :cond_750
    move-object/from16 v0, p0

    #@752
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppAvailable:Z

    #@754
    move/from16 v21, v0

    #@756
    if-eqz v21, :cond_78b

    #@758
    .line 6580
    const/16 v21, 0x1

    #@75a
    move/from16 v0, v21

    #@75c
    move-object/from16 v1, p0

    #@75e
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@760
    .line 6581
    const-string v21, "WindowManager"

    #@762
    new-instance v22, Ljava/lang/StringBuilder;

    #@764
    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    #@767
    const-string v23, "112APP_KEY : down! mPowerKeyTriggered :"

    #@769
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@76c
    move-result-object v22

    #@76d
    move-object/from16 v0, p0

    #@76f
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@771
    move/from16 v23, v0

    #@773
    invoke-virtual/range {v22 .. v23}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@776
    move-result-object v22

    #@777
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@77a
    move-result-object v22

    #@77b
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@77e
    .line 6582
    invoke-virtual/range {p1 .. p1}, Landroid/view/KeyEvent;->getDownTime()J

    #@781
    move-result-wide v21

    #@782
    move-wide/from16 v0, v21

    #@784
    move-object/from16 v2, p0

    #@786
    iput-wide v0, v2, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTime:J

    #@788
    .line 6583
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->intercept112AppChord()V

    #@78b
    .line 6585
    :cond_78b
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@78e
    move-result-object v19

    #@78f
    .line 6586
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    const/4 v10, 0x0

    #@790
    .line 6588
    .restart local v10       #hungUp:Z
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getAudioService()Landroid/media/IAudioService;

    #@793
    move-result-object v5

    #@794
    .line 6589
    .local v5, audioService:Landroid/media/IAudioService;
    const/16 v18, 0x0

    #@796
    .line 6591
    .local v18, silenceMode:Z
    if-eqz v19, :cond_7b8

    #@798
    if-eqz v5, :cond_7b8

    #@79a
    .line 6593
    :try_start_79a
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    #@79d
    move-result v21

    #@79e
    if-eqz v21, :cond_83d

    #@7a0
    .line 6597
    move-object/from16 v0, p0

    #@7a2
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@7a4
    move/from16 v21, v0

    #@7a6
    if-eqz v21, :cond_812

    #@7a8
    move-object/from16 v0, p0

    #@7aa
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@7ac
    move/from16 v21, v0

    #@7ae
    if-eqz v21, :cond_812

    #@7b0
    move-object/from16 v0, p0

    #@7b2
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@7b4
    move/from16 v21, v0
    :try_end_7b6
    .catch Landroid/os/RemoteException; {:try_start_79a .. :try_end_7b6} :catch_8ae

    #@7b6
    if-eqz v21, :cond_812

    #@7b8
    .line 6639
    :cond_7b8
    :goto_7b8
    if-eqz p3, :cond_7c4

    #@7ba
    move-object/from16 v0, p0

    #@7bc
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIncallPowerBehavior:I

    #@7be
    move/from16 v21, v0

    #@7c0
    and-int/lit8 v21, v21, 0x2

    #@7c2
    if-nez v21, :cond_7cc

    #@7c4
    :cond_7c4
    move-object/from16 v0, p0

    #@7c6
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@7c8
    move/from16 v21, v0

    #@7ca
    if-eqz v21, :cond_7df

    #@7cc
    :cond_7cc
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isInCallLTEVT()Z

    #@7cf
    move-result v21

    #@7d0
    if-eqz v21, :cond_7df

    #@7d2
    .line 6642
    const-string v21, "WindowManager"

    #@7d4
    const-string v22, "VT Call end by power key in call"

    #@7d6
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@7d9
    .line 6644
    if-eqz v19, :cond_7df

    #@7db
    .line 6645
    :try_start_7db
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->endCall()Z
    :try_end_7de
    .catch Landroid/os/RemoteException; {:try_start_7db .. :try_end_7de} :catch_8bc

    #@7de
    move-result v10

    #@7df
    .line 6653
    :cond_7df
    :goto_7df
    move/from16 v0, p3

    #@7e1
    move-object/from16 v1, p0

    #@7e3
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyScreenOn:Z

    #@7e5
    .line 6654
    if-nez v10, :cond_7f9

    #@7e7
    if-nez v18, :cond_7f9

    #@7e9
    move-object/from16 v0, p0

    #@7eb
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@7ed
    move/from16 v21, v0

    #@7ef
    if-nez v21, :cond_7f9

    #@7f1
    move-object/from16 v0, p0

    #@7f3
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@7f5
    move/from16 v21, v0

    #@7f7
    if-eqz v21, :cond_8ca

    #@7f9
    :cond_7f9
    const/16 v21, 0x1

    #@7fb
    :goto_7fb
    move-object/from16 v0, p0

    #@7fd
    move/from16 v1, v21

    #@7ff
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptPowerKeyDown(Z)V

    #@802
    goto/16 :goto_a

    #@804
    .line 6566
    .end local v5           #audioService:Landroid/media/IAudioService;
    .end local v10           #hungUp:Z
    .end local v18           #silenceMode:Z
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    .restart local v16       #notificationManagerService:Landroid/app/INotificationManager;
    :catch_804
    move-exception v8

    #@805
    .line 6567
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@807
    const-string v22, "INotificationManager threw RemoteException"

    #@809
    move-object/from16 v0, v21

    #@80b
    move-object/from16 v1, v22

    #@80d
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@810
    goto/16 :goto_725

    #@812
    .line 6601
    .end local v8           #ex:Landroid/os/RemoteException;
    .end local v16           #notificationManagerService:Landroid/app/INotificationManager;
    .restart local v5       #audioService:Landroid/media/IAudioService;
    .restart local v10       #hungUp:Z
    .restart local v18       #silenceMode:Z
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_812
    :try_start_812
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isReservedCall()Z

    #@815
    move-result v21

    #@816
    if-nez v21, :cond_825

    #@818
    .line 6602
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->silenceRinger()V

    #@81b
    .line 6603
    const-string v21, "WindowManager"

    #@81d
    const-string v22, "telephonyService.isRinging())"

    #@81f
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@822
    .line 6604
    const/16 v18, 0x1

    #@824
    goto :goto_7b8

    #@825
    .line 6606
    :cond_825
    move-object/from16 v0, p0

    #@827
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIncallPowerBehavior:I

    #@829
    move/from16 v21, v0

    #@82b
    and-int/lit8 v21, v21, 0x2

    #@82d
    if-nez v21, :cond_837

    #@82f
    move-object/from16 v0, p0

    #@831
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@833
    move/from16 v21, v0

    #@835
    if-eqz v21, :cond_7b8

    #@837
    .line 6607
    :cond_837
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->endCall()Z

    #@83a
    move-result v10

    #@83b
    goto/16 :goto_7b8

    #@83d
    .line 6611
    :cond_83d
    move-object/from16 v0, p0

    #@83f
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIncallPowerBehavior:I

    #@841
    move/from16 v21, v0

    #@843
    and-int/lit8 v21, v21, 0x2

    #@845
    if-nez v21, :cond_84f

    #@847
    move-object/from16 v0, p0

    #@849
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@84b
    move/from16 v21, v0

    #@84d
    if-eqz v21, :cond_7b8

    #@84f
    :cond_84f
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z

    #@852
    move-result v21

    #@853
    if-eqz v21, :cond_7b8

    #@855
    move-object/from16 v0, p0

    #@857
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@859
    move-object/from16 v21, v0

    #@85b
    invoke-virtual/range {v21 .. v21}, Landroid/os/PowerManager;->isLcdOn()Z

    #@85e
    move-result v21

    #@85f
    if-nez v21, :cond_871

    #@861
    move-object/from16 v0, p0

    #@863
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverEnabled:Z

    #@865
    move/from16 v21, v0

    #@867
    if-eqz v21, :cond_7b8

    #@869
    move-object/from16 v0, p0

    #@86b
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverClosed:Z

    #@86d
    move/from16 v21, v0

    #@86f
    if-eqz v21, :cond_7b8

    #@871
    .line 6619
    :cond_871
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isHeadsetPlugged()Z

    #@874
    move-result v21

    #@875
    if-nez v21, :cond_899

    #@877
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isBluetoothAudioOn()Z

    #@87a
    move-result v21

    #@87b
    if-nez v21, :cond_899

    #@87d
    invoke-interface {v5}, Landroid/media/IAudioService;->isSpeakerphoneOn()Z

    #@880
    move-result v21

    #@881
    if-nez v21, :cond_899

    #@883
    move-object/from16 v0, p0

    #@885
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeDownKeyTriggered:Z

    #@887
    move/from16 v21, v0

    #@889
    if-nez v21, :cond_899

    #@88b
    move-object/from16 v0, p0

    #@88d
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeUpKeyTriggered:Z

    #@88f
    move/from16 v21, v0

    #@891
    if-nez v21, :cond_899

    #@893
    .line 6624
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->endCall()Z

    #@896
    move-result v10

    #@897
    goto/16 :goto_7b8

    #@899
    .line 6626
    :cond_899
    move-object/from16 v0, p0

    #@89b
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAutocallTest:Z

    #@89d
    move/from16 v21, v0

    #@89f
    if-eqz v21, :cond_7b8

    #@8a1
    .line 6627
    const-string v21, "WindowManager"

    #@8a3
    const-string v22, "Auto Test call end"

    #@8a5
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@8a8
    .line 6628
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->endCall()Z
    :try_end_8ab
    .catch Landroid/os/RemoteException; {:try_start_812 .. :try_end_8ab} :catch_8ae

    #@8ab
    move-result v10

    #@8ac
    goto/16 :goto_7b8

    #@8ae
    .line 6633
    :catch_8ae
    move-exception v8

    #@8af
    .line 6634
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@8b1
    const-string v22, "ITelephony threw RemoteException"

    #@8b3
    move-object/from16 v0, v21

    #@8b5
    move-object/from16 v1, v22

    #@8b7
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8ba
    goto/16 :goto_7b8

    #@8bc
    .line 6647
    .end local v8           #ex:Landroid/os/RemoteException;
    :catch_8bc
    move-exception v8

    #@8bd
    .line 6648
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@8bf
    const-string v22, "ITelephony threw RemoteException"

    #@8c1
    move-object/from16 v0, v21

    #@8c3
    move-object/from16 v1, v22

    #@8c5
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@8c8
    goto/16 :goto_7df

    #@8ca
    .line 6654
    .end local v8           #ex:Landroid/os/RemoteException;
    :cond_8ca
    const/16 v21, 0x0

    #@8cc
    goto/16 :goto_7fb

    #@8ce
    .line 6660
    .end local v5           #audioService:Landroid/media/IAudioService;
    .end local v10           #hungUp:Z
    .end local v18           #silenceMode:Z
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_8ce
    const/16 v21, 0x0

    #@8d0
    move/from16 v0, v21

    #@8d2
    move-object/from16 v1, p0

    #@8d4
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyTriggered:Z

    #@8d6
    .line 6661
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingScreenshotChordAction()V

    #@8d9
    .line 6662
    move-object/from16 v0, p0

    #@8db
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mQuickClipHotKeyDisable:Z

    #@8dd
    move/from16 v21, v0

    #@8df
    if-nez v21, :cond_8e9

    #@8e1
    move-object/from16 v0, p0

    #@8e3
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHWKeyControlMode:Z

    #@8e5
    move/from16 v21, v0

    #@8e7
    if-eqz v21, :cond_8ec

    #@8e9
    .line 6663
    :cond_8e9
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPendingQuickClipChordAction()V

    #@8ec
    .line 6666
    :cond_8ec
    move-object/from16 v0, p0

    #@8ee
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->m112AppAvailable:Z

    #@8f0
    move/from16 v21, v0

    #@8f2
    if-eqz v21, :cond_8f7

    #@8f4
    .line 6667
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->cancelPending112AppChordAction()V

    #@8f7
    .line 6670
    :cond_8f7
    if-nez v6, :cond_909

    #@8f9
    move-object/from16 v0, p0

    #@8fb
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPendingPowerKeyUpCanceled:Z

    #@8fd
    move/from16 v21, v0

    #@8ff
    if-nez v21, :cond_909

    #@901
    move-object/from16 v0, p0

    #@903
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerKeyScreenOn:Z

    #@905
    move/from16 v21, v0

    #@907
    if-nez v21, :cond_95d

    #@909
    :cond_909
    const/16 v21, 0x1

    #@90b
    :goto_90b
    move-object/from16 v0, p0

    #@90d
    move/from16 v1, v21

    #@90f
    invoke-direct {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->interceptPowerKeyUp(Z)Z

    #@912
    move-result v21

    #@913
    if-eqz v21, :cond_919

    #@915
    .line 6671
    and-int/lit8 v21, v17, -0x3

    #@917
    or-int/lit8 v17, v21, 0x4

    #@919
    .line 6675
    :cond_919
    move-object/from16 v0, p0

    #@91b
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBlockingLcdoff:Z

    #@91d
    move/from16 v21, v0

    #@91f
    const/16 v22, 0x1

    #@921
    move/from16 v0, v21

    #@923
    move/from16 v1, v22

    #@925
    if-ne v0, v1, :cond_931

    #@927
    .line 6676
    and-int/lit8 v17, v17, -0x5

    #@929
    .line 6677
    const/16 v21, 0x0

    #@92b
    move/from16 v0, v21

    #@92d
    move-object/from16 v1, p0

    #@92f
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBlockingLcdoff:Z

    #@931
    .line 6680
    :cond_931
    move-object/from16 v0, p0

    #@933
    iget-boolean v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverClosed:Z

    #@935
    move/from16 v21, v0

    #@937
    if-eqz v21, :cond_953

    #@939
    .line 6681
    sget-object v21, Lcom/android/internal/policy/impl/PhoneWindowManager;->DEVICE_NAME:Ljava/lang/String;

    #@93b
    const-string v22, "g2"

    #@93d
    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@940
    move-result v21

    #@941
    if-nez v21, :cond_953

    #@943
    .line 6682
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@946
    move-result-object v19

    #@947
    .line 6683
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v19, :cond_953

    #@949
    .line 6685
    :try_start_949
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isOffhook()Z
    :try_end_94c
    .catch Landroid/os/RemoteException; {:try_start_949 .. :try_end_94c} :catch_960

    #@94c
    move-result v21

    #@94d
    if-eqz v21, :cond_953

    #@94f
    .line 6686
    and-int/lit8 v21, v17, -0x3

    #@951
    or-int/lit8 v17, v21, 0x4

    #@953
    .line 6694
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_953
    :goto_953
    const/16 v21, 0x0

    #@955
    move/from16 v0, v21

    #@957
    move-object/from16 v1, p0

    #@959
    iput-boolean v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPendingPowerKeyUpCanceled:Z

    #@95b
    goto/16 :goto_a

    #@95d
    .line 6670
    :cond_95d
    const/16 v21, 0x0

    #@95f
    goto :goto_90b

    #@960
    .line 6688
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :catch_960
    move-exception v8

    #@961
    .line 6689
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@963
    const-string v22, "ITelephony threw RemoteException"

    #@965
    move-object/from16 v0, v21

    #@967
    move-object/from16 v1, v22

    #@969
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@96c
    goto :goto_953

    #@96d
    .line 6702
    .end local v8           #ex:Landroid/os/RemoteException;
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :sswitch_96d
    if-eqz v7, :cond_97b

    #@96f
    .line 6703
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@972
    move-result-object v19

    #@973
    .line 6704
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v19, :cond_97b

    #@975
    .line 6706
    :try_start_975
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isIdle()Z
    :try_end_978
    .catch Landroid/os/RemoteException; {:try_start_975 .. :try_end_978} :catch_9a9

    #@978
    move-result v21

    #@979
    if-eqz v21, :cond_a

    #@97b
    .line 6724
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :cond_97b
    :goto_97b
    :sswitch_97b
    and-int/lit8 v21, v17, 0x1

    #@97d
    if-nez v21, :cond_a

    #@97f
    .line 6730
    move-object/from16 v0, p0

    #@981
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@983
    move-object/from16 v21, v0

    #@985
    invoke-virtual/range {v21 .. v21}, Landroid/os/PowerManager$WakeLock;->acquire()V

    #@988
    .line 6731
    move-object/from16 v0, p0

    #@98a
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@98c
    move-object/from16 v21, v0

    #@98e
    const/16 v22, 0x3

    #@990
    new-instance v23, Landroid/view/KeyEvent;

    #@992
    move-object/from16 v0, v23

    #@994
    move-object/from16 v1, p1

    #@996
    invoke-direct {v0, v1}, Landroid/view/KeyEvent;-><init>(Landroid/view/KeyEvent;)V

    #@999
    invoke-virtual/range {v21 .. v23}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    #@99c
    move-result-object v15

    #@99d
    .line 6733
    .local v15, msg:Landroid/os/Message;
    const/16 v21, 0x1

    #@99f
    move/from16 v0, v21

    #@9a1
    invoke-virtual {v15, v0}, Landroid/os/Message;->setAsynchronous(Z)V

    #@9a4
    .line 6734
    invoke-virtual {v15}, Landroid/os/Message;->sendToTarget()V

    #@9a7
    goto/16 :goto_a

    #@9a9
    .line 6711
    .end local v15           #msg:Landroid/os/Message;
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :catch_9a9
    move-exception v8

    #@9aa
    .line 6712
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@9ac
    const-string v22, "ITelephony threw RemoteException"

    #@9ae
    move-object/from16 v0, v21

    #@9b0
    move-object/from16 v1, v22

    #@9b2
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9b5
    goto :goto_97b

    #@9b6
    .line 6740
    .end local v8           #ex:Landroid/os/RemoteException;
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :sswitch_9b6
    if-eqz v7, :cond_a

    #@9b8
    .line 6741
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@9bb
    move-result-object v19

    #@9bc
    .line 6742
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v19, :cond_a

    #@9be
    .line 6744
    :try_start_9be
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->isRinging()Z

    #@9c1
    move-result v21

    #@9c2
    if-eqz v21, :cond_a

    #@9c4
    .line 6745
    const-string v21, "WindowManager"

    #@9c6
    const-string v22, "interceptKeyBeforeQueueing: CALL key-down while ringing: Answer the call!"

    #@9c8
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9cb
    .line 6747
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->answerRingingCall()V
    :try_end_9ce
    .catch Landroid/os/RemoteException; {:try_start_9be .. :try_end_9ce} :catch_9d2

    #@9ce
    .line 6751
    and-int/lit8 v17, v17, -0x2

    #@9d0
    goto/16 :goto_a

    #@9d2
    .line 6753
    :catch_9d2
    move-exception v8

    #@9d3
    .line 6754
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@9d5
    const-string v22, "ITelephony threw RemoteException"

    #@9d7
    move-object/from16 v0, v21

    #@9d9
    move-object/from16 v1, v22

    #@9db
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@9de
    goto/16 :goto_a

    #@9e0
    .line 6764
    .end local v8           #ex:Landroid/os/RemoteException;
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :sswitch_9e0
    if-eqz v7, :cond_a

    #@9e2
    .line 6766
    const-string v21, "WindowManager"

    #@9e4
    const-string v22, "interceptKeyBeforeQueueing:VT CALL_END key-down"

    #@9e6
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9e9
    .line 6767
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@9ec
    move-result-object v19

    #@9ed
    .line 6769
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v19, :cond_a

    #@9ef
    .line 6770
    :try_start_9ef
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->endCall()Z
    :try_end_9f2
    .catch Landroid/os/RemoteException; {:try_start_9ef .. :try_end_9f2} :catch_9f4

    #@9f2
    goto/16 :goto_a

    #@9f4
    .line 6772
    :catch_9f4
    move-exception v8

    #@9f5
    .line 6773
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@9f7
    const-string v22, "ITelephony threw RemoteException"

    #@9f9
    move-object/from16 v0, v21

    #@9fb
    move-object/from16 v1, v22

    #@9fd
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a00
    goto/16 :goto_a

    #@a02
    .line 6781
    .end local v8           #ex:Landroid/os/RemoteException;
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :sswitch_a02
    if-eqz v7, :cond_a

    #@a04
    .line 6783
    const-string v21, "WindowManager"

    #@a06
    const-string v22, "interceptKeyBeforeQueueing:VT CALL_RECEIVE key-down"

    #@a08
    invoke-static/range {v21 .. v22}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@a0b
    .line 6784
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@a0e
    move-result-object v19

    #@a0f
    .line 6786
    .restart local v19       #telephonyService:Lcom/android/internal/telephony/ITelephony;
    if-eqz v19, :cond_a

    #@a11
    .line 6787
    :try_start_a11
    invoke-interface/range {v19 .. v19}, Lcom/android/internal/telephony/ITelephony;->answerRingingCall()V
    :try_end_a14
    .catch Landroid/os/RemoteException; {:try_start_a11 .. :try_end_a14} :catch_a16

    #@a14
    goto/16 :goto_a

    #@a16
    .line 6789
    :catch_a16
    move-exception v8

    #@a17
    .line 6790
    .restart local v8       #ex:Landroid/os/RemoteException;
    const-string v21, "WindowManager"

    #@a19
    const-string v22, "ITelephony threw RemoteException"

    #@a1b
    move-object/from16 v0, v21

    #@a1d
    move-object/from16 v1, v22

    #@a1f
    invoke-static {v0, v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@a22
    goto/16 :goto_a

    #@a24
    .line 6239
    .end local v8           #ex:Landroid/os/RemoteException;
    .end local v19           #telephonyService:Lcom/android/internal/telephony/ITelephony;
    :catch_a24
    move-exception v21

    #@a25
    goto/16 :goto_1f0

    #@a27
    .line 6215
    nop

    #@a28
    :sswitch_data_a28
    .sparse-switch
        0x3 -> :sswitch_149
        0x5 -> :sswitch_9b6
        0x6 -> :sswitch_63b
        0x18 -> :sswitch_1bd
        0x19 -> :sswitch_1bd
        0x1a -> :sswitch_6c1
        0x4f -> :sswitch_97b
        0x55 -> :sswitch_96d
        0x56 -> :sswitch_97b
        0x57 -> :sswitch_97b
        0x58 -> :sswitch_97b
        0x59 -> :sswitch_97b
        0x5a -> :sswitch_97b
        0x5b -> :sswitch_97b
        0x7e -> :sswitch_96d
        0x7f -> :sswitch_96d
        0x82 -> :sswitch_97b
        0xa4 -> :sswitch_1f0
        0xdf -> :sswitch_a02
        0xe0 -> :sswitch_9e0
    .end sparse-switch
.end method

.method public interceptMotionBeforeQueueingWhenScreenOff(I)I
    .registers 5
    .parameter "policyFlags"

    #@0
    .prologue
    .line 6837
    const/4 v1, 0x0

    #@1
    .line 6839
    .local v1, result:I
    and-int/lit8 v2, p1, 0x3

    #@3
    if-eqz v2, :cond_1a

    #@5
    const/4 v0, 0x1

    #@6
    .line 6841
    .local v0, isWakeMotion:Z
    :goto_6
    if-eqz v0, :cond_19

    #@8
    .line 6842
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@a
    if-eqz v2, :cond_1c

    #@c
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@e
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowing()Z

    #@11
    move-result v2

    #@12
    if-eqz v2, :cond_1c

    #@14
    .line 6844
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@16
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->onWakeMotionWhenKeyguardShowingTq()V

    #@19
    .line 6850
    :cond_19
    :goto_19
    return v1

    #@1a
    .line 6839
    .end local v0           #isWakeMotion:Z
    :cond_1a
    const/4 v0, 0x0

    #@1b
    goto :goto_6

    #@1c
    .line 6847
    .restart local v0       #isWakeMotion:Z
    :cond_1c
    or-int/lit8 v1, v1, 0x2

    #@1e
    goto :goto_19
.end method

.method isDeviceProvisioned()Z
    .registers 4

    #@0
    .prologue
    const/4 v0, 0x0

    #@1
    .line 1941
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@3
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@6
    move-result-object v1

    #@7
    const-string v2, "device_provisioned"

    #@9
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@c
    move-result v1

    #@d
    if-eqz v1, :cond_10

    #@f
    const/4 v0, 0x1

    #@10
    :cond_10
    return v0
.end method

.method isFMActive()Z
    .registers 4

    #@0
    .prologue
    .line 5961
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "audio"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/media/AudioManager;

    #@a
    .line 5962
    .local v0, am:Landroid/media/AudioManager;
    if-nez v0, :cond_15

    #@c
    .line 5963
    const-string v1, "WindowManager"

    #@e
    const-string v2, "isFMActive: couldn\'t get AudioManager reference"

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 5964
    const/4 v1, 0x0

    #@14
    .line 5966
    :goto_14
    return v1

    #@15
    :cond_15
    invoke-virtual {v0}, Landroid/media/AudioManager;->isFMActive()Z

    #@18
    move-result v1

    #@19
    goto :goto_14
.end method

.method public isInCallLTEVT()Z
    .registers 6

    #@0
    .prologue
    .line 8340
    const/4 v2, 0x0

    #@1
    .line 8343
    .local v2, vtCallState:I
    :try_start_1
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getTelephonyService()Lcom/android/internal/telephony/ITelephony;

    #@4
    move-result-object v1

    #@5
    .line 8344
    .local v1, phoneServ:Lcom/android/internal/telephony/ITelephony;
    if-eqz v1, :cond_11

    #@7
    .line 8345
    invoke-interface {v1}, Lcom/android/internal/telephony/ITelephony;->getCallState()I

    #@a
    move-result v2

    #@b
    .line 8355
    .end local v1           #phoneServ:Lcom/android/internal/telephony/ITelephony;
    :goto_b
    const/16 v3, 0x66

    #@d
    if-ne v2, v3, :cond_22

    #@f
    .line 8356
    const/4 v3, 0x1

    #@10
    .line 8358
    :goto_10
    return v3

    #@11
    .line 8349
    .restart local v1       #phoneServ:Lcom/android/internal/telephony/ITelephony;
    :cond_11
    const-string v3, "WindowManager"

    #@13
    const-string v4, "Unable to find ITelephony interface"

    #@15
    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_18
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_18} :catch_19

    #@18
    goto :goto_b

    #@19
    .line 8351
    .end local v1           #phoneServ:Lcom/android/internal/telephony/ITelephony;
    :catch_19
    move-exception v0

    #@1a
    .line 8352
    .local v0, ex:Landroid/os/RemoteException;
    const-string v3, "WindowManager"

    #@1c
    const-string v4, "RemoteException from getPhoneInterface()"

    #@1e
    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    #@21
    goto :goto_b

    #@22
    .line 8358
    .end local v0           #ex:Landroid/os/RemoteException;
    :cond_22
    const/4 v3, 0x0

    #@23
    goto :goto_10
.end method

.method public isKeyguardLocked()Z
    .registers 2

    #@0
    .prologue
    .line 7167
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->keyguardOn()Z

    #@3
    move-result v0

    #@4
    return v0
.end method

.method public isKeyguardSecure()Z
    .registers 2

    #@0
    .prologue
    .line 7172
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    if-nez v0, :cond_6

    #@4
    const/4 v0, 0x0

    #@5
    .line 7173
    :goto_5
    return v0

    #@6
    :cond_6
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@8
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isSecure()Z

    #@b
    move-result v0

    #@c
    goto :goto_5
.end method

.method public isMulticallActive()Z
    .registers 5

    #@0
    .prologue
    .line 8207
    const-string v1, "persist.radio.keyBlockByCall"

    #@2
    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@5
    move-result-object v0

    #@6
    .line 8208
    .local v0, result:Ljava/lang/String;
    const-string v1, "WindowManager"

    #@8
    new-instance v2, Ljava/lang/StringBuilder;

    #@a
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@d
    const-string v3, "isMulticallActive() result = "

    #@f
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@12
    move-result-object v2

    #@13
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v2

    #@17
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1a
    move-result-object v2

    #@1b
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@1e
    .line 8210
    const-string v1, "1"

    #@20
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@23
    move-result v1

    #@24
    if-eqz v1, :cond_28

    #@26
    .line 8211
    const/4 v1, 0x1

    #@27
    .line 8214
    :goto_27
    return v1

    #@28
    :cond_28
    const/4 v1, 0x0

    #@29
    goto :goto_27
.end method

.method isMusicActive()Z
    .registers 4

    #@0
    .prologue
    .line 5949
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2
    const-string v2, "audio"

    #@4
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@7
    move-result-object v0

    #@8
    check-cast v0, Landroid/media/AudioManager;

    #@a
    .line 5950
    .local v0, am:Landroid/media/AudioManager;
    if-nez v0, :cond_15

    #@c
    .line 5951
    const-string v1, "WindowManager"

    #@e
    const-string v2, "isMusicActive: couldn\'t get AudioManager reference"

    #@10
    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    #@13
    .line 5952
    const/4 v1, 0x0

    #@14
    .line 5954
    :goto_14
    return v1

    #@15
    :cond_15
    invoke-virtual {v0}, Landroid/media/AudioManager;->isMusicActive()Z

    #@18
    move-result v1

    #@19
    goto :goto_14
.end method

.method public isScreenOnEarly()Z
    .registers 2

    #@0
    .prologue
    .line 7137
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnEarly:Z

    #@2
    return v0
.end method

.method public isScreenOnFully()Z
    .registers 2

    #@0
    .prologue
    .line 7142
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnFully:Z

    #@2
    return v0
.end method

.method public keepScreenOnStartedLw()V
    .registers 1

    #@0
    .prologue
    .line 7870
    return-void
.end method

.method public keepScreenOnStoppedLw()V
    .registers 5

    #@0
    .prologue
    .line 7874
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    if-eqz v2, :cond_16

    #@4
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@6
    invoke-virtual {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowingAndNotHidden()Z

    #@9
    move-result v2

    #@a
    if-nez v2, :cond_16

    #@c
    .line 7875
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@f
    move-result-wide v0

    #@10
    .line 7876
    .local v0, curTime:J
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@12
    const/4 v3, 0x0

    #@13
    invoke-virtual {v2, v0, v1, v3}, Landroid/os/PowerManager;->userActivity(JZ)V

    #@16
    .line 7878
    .end local v0           #curTime:J
    :cond_16
    return-void
.end method

.method keyguardOn()Z
    .registers 2

    #@0
    .prologue
    .line 3211
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->keyguardIsShowingTq()Z

    #@3
    move-result v0

    #@4
    if-nez v0, :cond_c

    #@6
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->inKeyguardRestrictedKeyInputMode()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_e

    #@c
    :cond_c
    const/4 v0, 0x1

    #@d
    :goto_d
    return v0

    #@e
    :cond_e
    const/4 v0, 0x0

    #@f
    goto :goto_d
.end method

.method launchHomeFromHotKey()V
    .registers 4

    #@0
    .prologue
    .line 4201
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    if-eqz v0, :cond_d

    #@4
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowingAndNotHidden()Z

    #@9
    move-result v0

    #@a
    if-eqz v0, :cond_d

    #@c
    .line 4232
    :goto_c
    return-void

    #@d
    .line 4203
    :cond_d
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideLockScreen:Z

    #@f
    if-nez v0, :cond_24

    #@11
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@13
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isInputRestricted()Z

    #@16
    move-result v0

    #@17
    if-eqz v0, :cond_24

    #@19
    .line 4206
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@1b
    new-instance v1, Lcom/android/internal/policy/impl/PhoneWindowManager$16;

    #@1d
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$16;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@20
    invoke-virtual {v0, v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->verifyUnlock(Landroid/view/WindowManagerPolicy$OnKeyguardExitResult;)V

    #@23
    goto :goto_c

    #@24
    .line 4221
    :cond_24
    :try_start_24
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    #@27
    move-result-object v0

    #@28
    invoke-interface {v0}, Landroid/app/IActivityManager;->stopAppSwitches()V
    :try_end_2b
    .catch Landroid/os/RemoteException; {:try_start_24 .. :try_end_2b} :catch_3d

    #@2b
    .line 4227
    :goto_2b
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2d
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeKeyintent:Landroid/content/Intent;

    #@2f
    const-string v2, "com.lge.permission.SlideAside"

    #@31
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    #@34
    .line 4229
    const-string v0, "homekey"

    #@36
    invoke-virtual {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendCloseSystemWindows(Ljava/lang/String;)V

    #@39
    .line 4230
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->startDockOrHome()V

    #@3c
    goto :goto_c

    #@3d
    .line 4222
    :catch_3d
    move-exception v0

    #@3e
    goto :goto_2b
.end method

.method launchSplitWindowUI()V
    .registers 4

    #@0
    .prologue
    const/4 v2, 0x0

    #@1
    .line 5055
    const/4 v1, 0x0

    #@2
    invoke-virtual {p0, v1, v2, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    #@5
    .line 5056
    new-instance v0, Landroid/content/Intent;

    #@7
    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    #@a
    .line 5057
    .local v0, intent:Landroid/content/Intent;
    const-string v1, "com.lge.splitwindow.LAUNCH"

    #@c
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    #@f
    .line 5058
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@11
    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    #@14
    .line 5059
    return-void
.end method

.method public layoutWindowLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Landroid/view/WindowManagerPolicy$WindowState;)V
    .registers 35
    .parameter "win"
    .parameter "attrs"
    .parameter "attached"

    #@0
    .prologue
    .line 5071
    move-object/from16 v0, p0

    #@2
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@4
    move-object/from16 v0, p1

    #@6
    if-eq v0, v2, :cond_10

    #@8
    move-object/from16 v0, p0

    #@a
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@c
    move-object/from16 v0, p1

    #@e
    if-ne v0, v2, :cond_11

    #@10
    .line 5426
    :cond_10
    :goto_10
    return-void

    #@11
    .line 5074
    :cond_11
    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->isDefaultDisplay()Z

    #@14
    move-result v24

    #@15
    .line 5075
    .local v24, isDefaultDisplay:Z
    if-eqz v24, :cond_153

    #@17
    move-object/from16 v0, p0

    #@19
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodTargetWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@1b
    move-object/from16 v0, p1

    #@1d
    if-ne v0, v2, :cond_153

    #@1f
    move-object/from16 v0, p0

    #@21
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@23
    if-eqz v2, :cond_153

    #@25
    const/16 v27, 0x1

    #@27
    .line 5077
    .local v27, needsToOffsetInputMethodTarget:Z
    :goto_27
    if-eqz v27, :cond_32

    #@29
    .line 5081
    move-object/from16 v0, p0

    #@2b
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@2d
    move-object/from16 v0, p0

    #@2f
    invoke-direct {v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->offsetInputMethodWindowLw(Landroid/view/WindowManagerPolicy$WindowState;)V

    #@32
    .line 5084
    :cond_32
    move-object/from16 v0, p2

    #@34
    iget v4, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    #@36
    .line 5085
    .local v4, fl:I
    move-object/from16 v0, p2

    #@38
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->softInputMode:I

    #@3a
    move/from16 v29, v0

    #@3c
    .line 5086
    .local v29, sim:I
    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->getSystemUiVisibility()I

    #@3f
    move-result v30

    #@40
    .line 5088
    .local v30, sysUiFl:I
    sget-object v8, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpParentFrame:Landroid/graphics/Rect;

    #@42
    .line 5089
    .local v8, pf:Landroid/graphics/Rect;
    sget-object v9, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpDisplayFrame:Landroid/graphics/Rect;

    #@44
    .line 5090
    .local v9, df:Landroid/graphics/Rect;
    sget-object v10, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpContentFrame:Landroid/graphics/Rect;

    #@46
    .line 5091
    .local v10, cf:Landroid/graphics/Rect;
    sget-object v11, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTmpVisibleFrame:Landroid/graphics/Rect;

    #@48
    .line 5093
    .local v11, vf:Landroid/graphics/Rect;
    if-eqz v24, :cond_157

    #@4a
    move-object/from16 v0, p0

    #@4c
    iget-boolean v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasNavigationBar:Z

    #@4e
    if-eqz v2, :cond_157

    #@50
    move-object/from16 v0, p0

    #@52
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@54
    if-eqz v2, :cond_157

    #@56
    move-object/from16 v0, p0

    #@58
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@5a
    invoke-interface {v2}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleLw()Z

    #@5d
    move-result v2

    #@5e
    if-eqz v2, :cond_157

    #@60
    const/16 v23, 0x1

    #@62
    .line 5096
    .local v23, hasNavBar:Z
    :goto_62
    move/from16 v0, v29

    #@64
    and-int/lit16 v5, v0, 0xf0

    #@66
    .line 5098
    .local v5, adjust:I
    move-object/from16 v0, p0

    #@68
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurBottom:I

    #@6a
    move/from16 v21, v0

    #@6c
    .line 5099
    .local v21, curBottom:I
    move-object/from16 v0, p0

    #@6e
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentBottom:I

    #@70
    move/from16 v20, v0

    #@72
    .line 5100
    .local v20, contentBottom:I
    move-object/from16 v0, p0

    #@74
    iget-boolean v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTranslucentImeMode:Z

    #@76
    if-eqz v2, :cond_8c

    #@78
    move-object/from16 v0, p0

    #@7a
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mFocusedWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@7c
    move-object/from16 v0, p1

    #@7e
    if-eq v0, v2, :cond_8c

    #@80
    .line 5101
    move-object/from16 v0, p0

    #@82
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mInitialCurBottom:I

    #@84
    move/from16 v21, v0

    #@86
    .line 5102
    move-object/from16 v0, p0

    #@88
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mInitialContentBottom:I

    #@8a
    move/from16 v20, v0

    #@8c
    .line 5105
    :cond_8c
    if-nez v24, :cond_18f

    #@8e
    .line 5106
    if-eqz p3, :cond_15b

    #@90
    .line 5109
    const/4 v7, 0x1

    #@91
    move-object/from16 v2, p0

    #@93
    move-object/from16 v3, p1

    #@95
    move-object/from16 v6, p3

    #@97
    invoke-virtual/range {v2 .. v11}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setAttachedWindowFrames(Landroid/view/WindowManagerPolicy$WindowState;IILandroid/view/WindowManagerPolicy$WindowState;ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@9a
    .line 5387
    :cond_9a
    :goto_9a
    const/16 v26, 0x0

    #@9c
    .line 5389
    .local v26, isSplitMediaLayoutAttached:Z
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@9e
    if-eqz v2, :cond_10c

    #@a0
    .line 5390
    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->getSplitScreenId()I

    #@a3
    move-result v28

    #@a4
    .line 5391
    .local v28, screenId:I
    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->isSplitFullScreen()Z

    #@a7
    move-result v25

    #@a8
    .line 5393
    .local v25, isFullScreenMode:Z
    if-eqz p3, :cond_691

    #@aa
    invoke-interface/range {p3 .. p3}, Landroid/view/WindowManagerPolicy$WindowState;->getSplitScreenId()I

    #@ad
    move-result v2

    #@ae
    if-eqz v2, :cond_691

    #@b0
    invoke-interface/range {p3 .. p3}, Landroid/view/WindowManagerPolicy$WindowState;->isSplitFullScreen()Z

    #@b3
    move-result v2

    #@b4
    if-nez v2, :cond_691

    #@b6
    move-object/from16 v0, p2

    #@b8
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@ba
    const/16 v3, 0x3e9

    #@bc
    if-eq v2, v3, :cond_c6

    #@be
    move-object/from16 v0, p2

    #@c0
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@c2
    const/16 v3, 0x3e9

    #@c4
    if-ne v2, v3, :cond_691

    #@c6
    :cond_c6
    const/16 v26, 0x1

    #@c8
    .line 5398
    :goto_c8
    if-eqz v26, :cond_10c

    #@ca
    .line 5399
    const-string v2, "SplitWindow"

    #@cc
    new-instance v3, Ljava/lang/StringBuilder;

    #@ce
    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    #@d1
    const-string v6, "SurfaceView check.. layoutAttached  = "

    #@d3
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@d6
    move-result-object v3

    #@d7
    move/from16 v0, v26

    #@d9
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@dc
    move-result-object v3

    #@dd
    const-string v6, ", screenId = "

    #@df
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@e2
    move-result-object v3

    #@e3
    invoke-interface/range {p3 .. p3}, Landroid/view/WindowManagerPolicy$WindowState;->getSplitScreenId()I

    #@e6
    move-result v6

    #@e7
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@ea
    move-result-object v3

    #@eb
    const-string v6, ", isFullscreen = "

    #@ed
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f0
    move-result-object v3

    #@f1
    invoke-interface/range {p3 .. p3}, Landroid/view/WindowManagerPolicy$WindowState;->isSplitFullScreen()Z

    #@f4
    move-result v6

    #@f5
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@f8
    move-result-object v3

    #@f9
    const-string v6, " of win = "

    #@fb
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@fe
    move-result-object v3

    #@ff
    move-object/from16 v0, p1

    #@101
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    #@104
    move-result-object v3

    #@105
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@108
    move-result-object v3

    #@109
    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@10c
    .line 5404
    .end local v25           #isFullScreenMode:Z
    .end local v28           #screenId:I
    :cond_10c
    and-int/lit16 v2, v4, 0x200

    #@10e
    if-eqz v2, :cond_12e

    #@110
    if-nez v26, :cond_12e

    #@112
    .line 5405
    const/16 v2, -0x2710

    #@114
    iput v2, v11, Landroid/graphics/Rect;->top:I

    #@116
    iput v2, v11, Landroid/graphics/Rect;->left:I

    #@118
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@11a
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@11c
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@11e
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@120
    .line 5406
    const/16 v2, 0x2710

    #@122
    iput v2, v11, Landroid/graphics/Rect;->bottom:I

    #@124
    iput v2, v11, Landroid/graphics/Rect;->right:I

    #@126
    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    #@128
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@12a
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@12c
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@12e
    .line 5417
    :cond_12e
    move-object/from16 v0, p1

    #@130
    invoke-interface {v0, v8, v9, v10, v11}, Landroid/view/WindowManagerPolicy$WindowState;->computeFrameLw(Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@133
    .line 5421
    move-object/from16 v0, p2

    #@135
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@137
    const/16 v3, 0x7db

    #@139
    if-ne v2, v3, :cond_10

    #@13b
    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->isVisibleOrBehindKeyguardLw()Z

    #@13e
    move-result v2

    #@13f
    if-eqz v2, :cond_10

    #@141
    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->getGivenInsetsPendingLw()Z

    #@144
    move-result v2

    #@145
    if-nez v2, :cond_10

    #@147
    .line 5423
    const/4 v2, 0x0

    #@148
    const/4 v3, 0x0

    #@149
    move-object/from16 v0, p0

    #@14b
    invoke-virtual {v0, v2, v3}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setLastInputMethodWindowLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManagerPolicy$WindowState;)V

    #@14e
    .line 5424
    invoke-direct/range {p0 .. p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->offsetInputMethodWindowLw(Landroid/view/WindowManagerPolicy$WindowState;)V

    #@151
    goto/16 :goto_10

    #@153
    .line 5075
    .end local v4           #fl:I
    .end local v5           #adjust:I
    .end local v8           #pf:Landroid/graphics/Rect;
    .end local v9           #df:Landroid/graphics/Rect;
    .end local v10           #cf:Landroid/graphics/Rect;
    .end local v11           #vf:Landroid/graphics/Rect;
    .end local v20           #contentBottom:I
    .end local v21           #curBottom:I
    .end local v23           #hasNavBar:Z
    .end local v26           #isSplitMediaLayoutAttached:Z
    .end local v27           #needsToOffsetInputMethodTarget:Z
    .end local v29           #sim:I
    .end local v30           #sysUiFl:I
    :cond_153
    const/16 v27, 0x0

    #@155
    goto/16 :goto_27

    #@157
    .line 5093
    .restart local v4       #fl:I
    .restart local v8       #pf:Landroid/graphics/Rect;
    .restart local v9       #df:Landroid/graphics/Rect;
    .restart local v10       #cf:Landroid/graphics/Rect;
    .restart local v11       #vf:Landroid/graphics/Rect;
    .restart local v27       #needsToOffsetInputMethodTarget:Z
    .restart local v29       #sim:I
    .restart local v30       #sysUiFl:I
    :cond_157
    const/16 v23, 0x0

    #@159
    goto/16 :goto_62

    #@15b
    .line 5112
    .restart local v5       #adjust:I
    .restart local v20       #contentBottom:I
    .restart local v21       #curBottom:I
    .restart local v23       #hasNavBar:Z
    :cond_15b
    move-object/from16 v0, p0

    #@15d
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@15f
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@161
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@163
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@165
    .line 5113
    move-object/from16 v0, p0

    #@167
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@169
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@16b
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@16d
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@16f
    .line 5114
    move-object/from16 v0, p0

    #@171
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@173
    move-object/from16 v0, p0

    #@175
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@177
    add-int/2addr v2, v3

    #@178
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@17a
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@17c
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@17e
    .line 5116
    move-object/from16 v0, p0

    #@180
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@182
    move-object/from16 v0, p0

    #@184
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@186
    add-int/2addr v2, v3

    #@187
    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    #@189
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@18b
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@18d
    goto/16 :goto_9a

    #@18f
    .line 5119
    :cond_18f
    move-object/from16 v0, p2

    #@191
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@193
    const/16 v3, 0x7db

    #@195
    if-ne v2, v3, :cond_1d7

    #@197
    .line 5120
    move-object/from16 v0, p0

    #@199
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@19b
    iput v2, v11, Landroid/graphics/Rect;->left:I

    #@19d
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@19f
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@1a1
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@1a3
    .line 5121
    move-object/from16 v0, p0

    #@1a5
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@1a7
    iput v2, v11, Landroid/graphics/Rect;->top:I

    #@1a9
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@1ab
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@1ad
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@1af
    .line 5122
    move-object/from16 v0, p0

    #@1b1
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@1b3
    iput v2, v11, Landroid/graphics/Rect;->right:I

    #@1b5
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@1b7
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@1b9
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@1bb
    .line 5123
    move-object/from16 v0, p0

    #@1bd
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@1bf
    iput v2, v11, Landroid/graphics/Rect;->bottom:I

    #@1c1
    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    #@1c3
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@1c5
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@1c7
    .line 5125
    const/16 v2, 0x50

    #@1c9
    move-object/from16 v0, p2

    #@1cb
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    #@1cd
    .line 5126
    invoke-interface/range {p1 .. p1}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    #@1d0
    move-result v2

    #@1d1
    move-object/from16 v0, p0

    #@1d3
    iput v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLayer:I

    #@1d5
    goto/16 :goto_9a

    #@1d7
    .line 5128
    :cond_1d7
    const v2, 0x10500

    #@1da
    and-int/2addr v2, v4

    #@1db
    const v3, 0x10100

    #@1de
    if-ne v2, v3, :cond_379

    #@1e0
    and-int/lit8 v2, v30, 0x4

    #@1e2
    if-nez v2, :cond_379

    #@1e4
    .line 5138
    if-eqz p3, :cond_209

    #@1e6
    .line 5141
    const/4 v7, 0x1

    #@1e7
    move-object/from16 v2, p0

    #@1e9
    move-object/from16 v3, p1

    #@1eb
    move-object/from16 v6, p3

    #@1ed
    invoke-virtual/range {v2 .. v11}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setAttachedWindowFrames(Landroid/view/WindowManagerPolicy$WindowState;IILandroid/view/WindowManagerPolicy$WindowState;ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@1f0
    .line 5142
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@1f2
    if-eqz v2, :cond_9a

    #@1f4
    move-object/from16 v12, p0

    #@1f6
    move-object/from16 v13, p1

    #@1f8
    move-object/from16 v14, p3

    #@1fa
    move-object/from16 v15, p2

    #@1fc
    move-object/from16 v16, v8

    #@1fe
    move-object/from16 v17, v9

    #@200
    move-object/from16 v18, v10

    #@202
    move-object/from16 v19, v11

    #@204
    .line 5143
    invoke-direct/range {v12 .. v19}, Lcom/android/internal/policy/impl/PhoneWindowManager;->modifySubPanelLayoutForSplitWindow(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@207
    goto/16 :goto_9a

    #@209
    .line 5146
    :cond_209
    move-object/from16 v0, p2

    #@20b
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@20d
    const/16 v3, 0x7de

    #@20f
    if-eq v2, v3, :cond_219

    #@211
    move-object/from16 v0, p2

    #@213
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@215
    const/16 v3, 0x7e1

    #@217
    if-ne v2, v3, :cond_2d7

    #@219
    .line 5155
    :cond_219
    if-eqz v23, :cond_2bb

    #@21b
    move-object/from16 v0, p0

    #@21d
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@21f
    :goto_21f
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@221
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@223
    .line 5156
    move-object/from16 v0, p0

    #@225
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@227
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@229
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@22b
    .line 5157
    if-eqz v23, :cond_2c1

    #@22d
    move-object/from16 v0, p0

    #@22f
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@231
    move-object/from16 v0, p0

    #@233
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenWidth:I

    #@235
    add-int/2addr v2, v3

    #@236
    :goto_236
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@238
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@23a
    .line 5160
    if-eqz v23, :cond_2cc

    #@23c
    move-object/from16 v0, p0

    #@23e
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@240
    move-object/from16 v0, p0

    #@242
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenHeight:I

    #@244
    add-int/2addr v2, v3

    #@245
    :goto_245
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@247
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@249
    .line 5190
    :goto_249
    const/16 v2, 0x10

    #@24b
    if-eq v5, v2, :cond_352

    #@24d
    .line 5191
    move-object/from16 v0, p0

    #@24f
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@251
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@253
    .line 5192
    move-object/from16 v0, p0

    #@255
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@257
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@259
    .line 5193
    move-object/from16 v0, p0

    #@25b
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@25d
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@25f
    .line 5194
    move-object/from16 v0, p0

    #@261
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@263
    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    #@265
    .line 5202
    :goto_265
    move-object/from16 v0, p0

    #@267
    move/from16 v1, v30

    #@269
    invoke-direct {v0, v1, v4, v10}, Lcom/android/internal/policy/impl/PhoneWindowManager;->applyStableConstraints(IILandroid/graphics/Rect;)V

    #@26c
    .line 5203
    const/16 v2, 0x30

    #@26e
    if-eq v5, v2, :cond_36a

    #@270
    .line 5204
    move-object/from16 v0, p0

    #@272
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurLeft:I

    #@274
    iput v2, v11, Landroid/graphics/Rect;->left:I

    #@276
    .line 5205
    move-object/from16 v0, p0

    #@278
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurTop:I

    #@27a
    iput v2, v11, Landroid/graphics/Rect;->top:I

    #@27c
    .line 5206
    move-object/from16 v0, p0

    #@27e
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurRight:I

    #@280
    iput v2, v11, Landroid/graphics/Rect;->right:I

    #@282
    .line 5207
    move/from16 v0, v21

    #@284
    iput v0, v11, Landroid/graphics/Rect;->bottom:I

    #@286
    .line 5212
    :goto_286
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@288
    if-eqz v2, :cond_9a

    #@28a
    move-object/from16 v6, p0

    #@28c
    move-object/from16 v7, p1

    #@28e
    move-object/from16 v12, p2

    #@290
    .line 5213
    invoke-direct/range {v6 .. v12}, Lcom/android/internal/policy/impl/PhoneWindowManager;->modifyLayoutForSplitWindow(Landroid/view/WindowManagerPolicy$WindowState;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/WindowManager$LayoutParams;)V

    #@293
    .line 5215
    move-object/from16 v0, p0

    #@295
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@297
    if-nez v2, :cond_29c

    #@299
    .line 5216
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getSplitWindowManagerInstance()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@29c
    .line 5219
    :cond_29c
    :try_start_29c
    move-object/from16 v0, p0

    #@29e
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@2a0
    if-eqz v2, :cond_9a

    #@2a2
    move-object/from16 v0, p0

    #@2a4
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@2a6
    invoke-interface {v2}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->isSplitMode()Z

    #@2a9
    move-result v2

    #@2aa
    if-eqz v2, :cond_9a

    #@2ac
    .line 5220
    invoke-virtual {v11, v10}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
    :try_end_2af
    .catch Landroid/os/RemoteException; {:try_start_29c .. :try_end_2af} :catch_2b1
    .catch Ljava/lang/NullPointerException; {:try_start_29c .. :try_end_2af} :catch_36f

    #@2af
    goto/16 :goto_9a

    #@2b1
    .line 5221
    :catch_2b1
    move-exception v22

    #@2b2
    .line 5222
    .local v22, e:Landroid/os/RemoteException;
    const-string v2, "SplitWindow"

    #@2b4
    const-string v3, "Binder service (SplitWindowPolicyService) is not available"

    #@2b6
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@2b9
    goto/16 :goto_9a

    #@2bb
    .line 5155
    .end local v22           #e:Landroid/os/RemoteException;
    :cond_2bb
    move-object/from16 v0, p0

    #@2bd
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@2bf
    goto/16 :goto_21f

    #@2c1
    .line 5157
    :cond_2c1
    move-object/from16 v0, p0

    #@2c3
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@2c5
    move-object/from16 v0, p0

    #@2c7
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@2c9
    add-int/2addr v2, v3

    #@2ca
    goto/16 :goto_236

    #@2cc
    .line 5160
    :cond_2cc
    move-object/from16 v0, p0

    #@2ce
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@2d0
    move-object/from16 v0, p0

    #@2d2
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@2d4
    add-int/2addr v2, v3

    #@2d5
    goto/16 :goto_245

    #@2d7
    .line 5169
    :cond_2d7
    move-object/from16 v0, p0

    #@2d9
    iget-boolean v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCanHideNavigationBar:Z

    #@2db
    if-eqz v2, :cond_326

    #@2dd
    move/from16 v0, v30

    #@2df
    and-int/lit16 v2, v0, 0x200

    #@2e1
    if-eqz v2, :cond_326

    #@2e3
    move-object/from16 v0, p2

    #@2e5
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2e7
    const/4 v3, 0x1

    #@2e8
    if-lt v2, v3, :cond_326

    #@2ea
    move-object/from16 v0, p2

    #@2ec
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2ee
    const/16 v3, 0x7cf

    #@2f0
    if-le v2, v3, :cond_2fa

    #@2f2
    move-object/from16 v0, p2

    #@2f4
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@2f6
    const/16 v3, 0x7d4

    #@2f8
    if-ne v2, v3, :cond_326

    #@2fa
    .line 5179
    :cond_2fa
    move-object/from16 v0, p0

    #@2fc
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@2fe
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@300
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@302
    .line 5180
    move-object/from16 v0, p0

    #@304
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@306
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@308
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@30a
    .line 5181
    move-object/from16 v0, p0

    #@30c
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@30e
    move-object/from16 v0, p0

    #@310
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@312
    add-int/2addr v2, v3

    #@313
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@315
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@317
    .line 5182
    move-object/from16 v0, p0

    #@319
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@31b
    move-object/from16 v0, p0

    #@31d
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@31f
    add-int/2addr v2, v3

    #@320
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@322
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@324
    goto/16 :goto_249

    #@326
    .line 5184
    :cond_326
    move-object/from16 v0, p0

    #@328
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@32a
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@32c
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@32e
    .line 5185
    move-object/from16 v0, p0

    #@330
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@332
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@334
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@336
    .line 5186
    move-object/from16 v0, p0

    #@338
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@33a
    move-object/from16 v0, p0

    #@33c
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenWidth:I

    #@33e
    add-int/2addr v2, v3

    #@33f
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@341
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@343
    .line 5187
    move-object/from16 v0, p0

    #@345
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@347
    move-object/from16 v0, p0

    #@349
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenHeight:I

    #@34b
    add-int/2addr v2, v3

    #@34c
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@34e
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@350
    goto/16 :goto_249

    #@352
    .line 5196
    :cond_352
    move-object/from16 v0, p0

    #@354
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentLeft:I

    #@356
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@358
    .line 5197
    move-object/from16 v0, p0

    #@35a
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentTop:I

    #@35c
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@35e
    .line 5198
    move-object/from16 v0, p0

    #@360
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentRight:I

    #@362
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@364
    .line 5199
    move/from16 v0, v20

    #@366
    iput v0, v10, Landroid/graphics/Rect;->bottom:I

    #@368
    goto/16 :goto_265

    #@36a
    .line 5209
    :cond_36a
    invoke-virtual {v11, v10}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@36d
    goto/16 :goto_286

    #@36f
    .line 5223
    :catch_36f
    move-exception v22

    #@370
    .line 5224
    .local v22, e:Ljava/lang/NullPointerException;
    const-string v2, "SplitWindow"

    #@372
    const-string v3, "Binder service (SplitWindowPolicyService) is not available"

    #@374
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@377
    goto/16 :goto_9a

    #@379
    .line 5228
    .end local v22           #e:Ljava/lang/NullPointerException;
    :cond_379
    and-int/lit16 v2, v4, 0x100

    #@37b
    if-nez v2, :cond_383

    #@37d
    move/from16 v0, v30

    #@37f
    and-int/lit16 v2, v0, 0x600

    #@381
    if-eqz v2, :cond_578

    #@383
    .line 5235
    :cond_383
    move-object/from16 v0, p2

    #@385
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@387
    const/16 v3, 0x7de

    #@389
    if-eq v2, v3, :cond_393

    #@38b
    move-object/from16 v0, p2

    #@38d
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@38f
    const/16 v3, 0x7e1

    #@391
    if-ne v2, v3, :cond_407

    #@393
    .line 5237
    :cond_393
    if-eqz v23, :cond_3ee

    #@395
    move-object/from16 v0, p0

    #@397
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@399
    :goto_399
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@39b
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@39d
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@39f
    .line 5238
    move-object/from16 v0, p0

    #@3a1
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@3a3
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@3a5
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@3a7
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@3a9
    .line 5239
    if-eqz v23, :cond_3f3

    #@3ab
    move-object/from16 v0, p0

    #@3ad
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@3af
    move-object/from16 v0, p0

    #@3b1
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenWidth:I

    #@3b3
    add-int/2addr v2, v3

    #@3b4
    :goto_3b4
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@3b6
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@3b8
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@3ba
    .line 5242
    if-eqz v23, :cond_3fd

    #@3bc
    move-object/from16 v0, p0

    #@3be
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@3c0
    move-object/from16 v0, p0

    #@3c2
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenHeight:I

    #@3c4
    add-int/2addr v2, v3

    #@3c5
    :goto_3c5
    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    #@3c7
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@3c9
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@3cb
    .line 5309
    :cond_3cb
    :goto_3cb
    move-object/from16 v0, p0

    #@3cd
    move/from16 v1, v30

    #@3cf
    invoke-direct {v0, v1, v4, v10}, Lcom/android/internal/policy/impl/PhoneWindowManager;->applyStableConstraints(IILandroid/graphics/Rect;)V

    #@3d2
    .line 5311
    const/16 v2, 0x30

    #@3d4
    if-eq v5, v2, :cond_573

    #@3d6
    .line 5312
    move-object/from16 v0, p0

    #@3d8
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurLeft:I

    #@3da
    iput v2, v11, Landroid/graphics/Rect;->left:I

    #@3dc
    .line 5313
    move-object/from16 v0, p0

    #@3de
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurTop:I

    #@3e0
    iput v2, v11, Landroid/graphics/Rect;->top:I

    #@3e2
    .line 5314
    move-object/from16 v0, p0

    #@3e4
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurRight:I

    #@3e6
    iput v2, v11, Landroid/graphics/Rect;->right:I

    #@3e8
    .line 5315
    move/from16 v0, v21

    #@3ea
    iput v0, v11, Landroid/graphics/Rect;->bottom:I

    #@3ec
    goto/16 :goto_9a

    #@3ee
    .line 5237
    :cond_3ee
    move-object/from16 v0, p0

    #@3f0
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@3f2
    goto :goto_399

    #@3f3
    .line 5239
    :cond_3f3
    move-object/from16 v0, p0

    #@3f5
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@3f7
    move-object/from16 v0, p0

    #@3f9
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@3fb
    add-int/2addr v2, v3

    #@3fc
    goto :goto_3b4

    #@3fd
    .line 5242
    :cond_3fd
    move-object/from16 v0, p0

    #@3ff
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@401
    move-object/from16 v0, p0

    #@403
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@405
    add-int/2addr v2, v3

    #@406
    goto :goto_3c5

    #@407
    .line 5250
    :cond_407
    move-object/from16 v0, p2

    #@409
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@40b
    const/16 v3, 0x7e3

    #@40d
    if-eq v2, v3, :cond_417

    #@40f
    move-object/from16 v0, p2

    #@411
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@413
    const/16 v3, 0x7e8

    #@415
    if-ne v2, v3, :cond_442

    #@417
    .line 5253
    :cond_417
    move-object/from16 v0, p0

    #@419
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@41b
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@41d
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@41f
    .line 5254
    move-object/from16 v0, p0

    #@421
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@423
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@425
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@427
    .line 5255
    move-object/from16 v0, p0

    #@429
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@42b
    move-object/from16 v0, p0

    #@42d
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@42f
    add-int/2addr v2, v3

    #@430
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@432
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@434
    .line 5256
    move-object/from16 v0, p0

    #@436
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@438
    move-object/from16 v0, p0

    #@43a
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@43c
    add-int/2addr v2, v3

    #@43d
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@43f
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@441
    goto :goto_3cb

    #@442
    .line 5262
    :cond_442
    move-object/from16 v0, p2

    #@444
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@446
    const/16 v3, 0x7df

    #@448
    if-eq v2, v3, :cond_452

    #@44a
    move-object/from16 v0, p2

    #@44c
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@44e
    const/16 v3, 0x7e5

    #@450
    if-ne v2, v3, :cond_482

    #@452
    :cond_452
    and-int/lit16 v2, v4, 0x400

    #@454
    if-eqz v2, :cond_482

    #@456
    .line 5266
    move-object/from16 v0, p0

    #@458
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@45a
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@45c
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@45e
    .line 5267
    move-object/from16 v0, p0

    #@460
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@462
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@464
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@466
    .line 5268
    move-object/from16 v0, p0

    #@468
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@46a
    move-object/from16 v0, p0

    #@46c
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@46e
    add-int/2addr v2, v3

    #@46f
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@471
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@473
    .line 5269
    move-object/from16 v0, p0

    #@475
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@477
    move-object/from16 v0, p0

    #@479
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@47b
    add-int/2addr v2, v3

    #@47c
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@47e
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@480
    goto/16 :goto_3cb

    #@482
    .line 5270
    :cond_482
    move-object/from16 v0, p2

    #@484
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@486
    const/16 v3, 0x7e5

    #@488
    if-eq v2, v3, :cond_492

    #@48a
    move-object/from16 v0, p2

    #@48c
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@48e
    const/16 v3, 0x7e9

    #@490
    if-ne v2, v3, :cond_4c6

    #@492
    .line 5273
    :cond_492
    move-object/from16 v0, p0

    #@494
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@496
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@498
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@49a
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@49c
    .line 5274
    move-object/from16 v0, p0

    #@49e
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@4a0
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@4a2
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@4a4
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@4a6
    .line 5275
    move-object/from16 v0, p0

    #@4a8
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@4aa
    move-object/from16 v0, p0

    #@4ac
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@4ae
    add-int/2addr v2, v3

    #@4af
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@4b1
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@4b3
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@4b5
    .line 5276
    move-object/from16 v0, p0

    #@4b7
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@4b9
    move-object/from16 v0, p0

    #@4bb
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@4bd
    add-int/2addr v2, v3

    #@4be
    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    #@4c0
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@4c2
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@4c4
    goto/16 :goto_3cb

    #@4c6
    .line 5278
    :cond_4c6
    move-object/from16 v0, p0

    #@4c8
    iget-boolean v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCanHideNavigationBar:Z

    #@4ca
    if-eqz v2, :cond_532

    #@4cc
    move/from16 v0, v30

    #@4ce
    and-int/lit16 v2, v0, 0x200

    #@4d0
    if-eqz v2, :cond_532

    #@4d2
    move-object/from16 v0, p2

    #@4d4
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@4d6
    const/4 v3, 0x1

    #@4d7
    if-lt v2, v3, :cond_532

    #@4d9
    move-object/from16 v0, p2

    #@4db
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@4dd
    const/16 v3, 0x7cf

    #@4df
    if-le v2, v3, :cond_4f1

    #@4e1
    move-object/from16 v0, p2

    #@4e3
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@4e5
    const/16 v3, 0x7dd

    #@4e7
    if-eq v2, v3, :cond_4f1

    #@4e9
    move-object/from16 v0, p2

    #@4eb
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@4ed
    const/16 v3, 0x7d4

    #@4ef
    if-ne v2, v3, :cond_532

    #@4f1
    .line 5292
    :cond_4f1
    move-object/from16 v0, p0

    #@4f3
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@4f5
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@4f7
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@4f9
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@4fb
    .line 5293
    move-object/from16 v0, p0

    #@4fd
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@4ff
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@501
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@503
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@505
    .line 5294
    move-object/from16 v0, p0

    #@507
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenLeft:I

    #@509
    move-object/from16 v0, p0

    #@50b
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenWidth:I

    #@50d
    add-int/2addr v2, v3

    #@50e
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@510
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@512
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@514
    .line 5295
    move-object/from16 v0, p0

    #@516
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenTop:I

    #@518
    move-object/from16 v0, p0

    #@51a
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUnrestrictedScreenHeight:I

    #@51c
    add-int/2addr v2, v3

    #@51d
    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    #@51f
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@521
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@523
    .line 5297
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@525
    if-eqz v2, :cond_3cb

    #@527
    move-object/from16 v6, p0

    #@529
    move-object/from16 v7, p1

    #@52b
    move-object/from16 v12, p2

    #@52d
    .line 5298
    invoke-direct/range {v6 .. v12}, Lcom/android/internal/policy/impl/PhoneWindowManager;->modifyLayoutForSplitWindow(Landroid/view/WindowManagerPolicy$WindowState;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/WindowManager$LayoutParams;)V

    #@530
    goto/16 :goto_3cb

    #@532
    .line 5300
    :cond_532
    move-object/from16 v0, p0

    #@534
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@536
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@538
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@53a
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@53c
    .line 5301
    move-object/from16 v0, p0

    #@53e
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@540
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@542
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@544
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@546
    .line 5302
    move-object/from16 v0, p0

    #@548
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@54a
    move-object/from16 v0, p0

    #@54c
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenWidth:I

    #@54e
    add-int/2addr v2, v3

    #@54f
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@551
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@553
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@555
    .line 5303
    move-object/from16 v0, p0

    #@557
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@559
    move-object/from16 v0, p0

    #@55b
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenHeight:I

    #@55d
    add-int/2addr v2, v3

    #@55e
    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    #@560
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@562
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@564
    .line 5305
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@566
    if-eqz v2, :cond_3cb

    #@568
    move-object/from16 v6, p0

    #@56a
    move-object/from16 v7, p1

    #@56c
    move-object/from16 v12, p2

    #@56e
    .line 5306
    invoke-direct/range {v6 .. v12}, Lcom/android/internal/policy/impl/PhoneWindowManager;->modifyLayoutForSplitWindow(Landroid/view/WindowManagerPolicy$WindowState;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/WindowManager$LayoutParams;)V

    #@571
    goto/16 :goto_3cb

    #@573
    .line 5317
    :cond_573
    invoke-virtual {v11, v10}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@576
    goto/16 :goto_9a

    #@578
    .line 5319
    :cond_578
    if-eqz p3, :cond_59d

    #@57a
    .line 5324
    const/4 v7, 0x0

    #@57b
    move-object/from16 v2, p0

    #@57d
    move-object/from16 v3, p1

    #@57f
    move-object/from16 v6, p3

    #@581
    invoke-virtual/range {v2 .. v11}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setAttachedWindowFrames(Landroid/view/WindowManagerPolicy$WindowState;IILandroid/view/WindowManagerPolicy$WindowState;ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@584
    .line 5325
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@586
    if-eqz v2, :cond_9a

    #@588
    move-object/from16 v12, p0

    #@58a
    move-object/from16 v13, p1

    #@58c
    move-object/from16 v14, p3

    #@58e
    move-object/from16 v15, p2

    #@590
    move-object/from16 v16, v8

    #@592
    move-object/from16 v17, v9

    #@594
    move-object/from16 v18, v10

    #@596
    move-object/from16 v19, v11

    #@598
    .line 5326
    invoke-direct/range {v12 .. v19}, Lcom/android/internal/policy/impl/PhoneWindowManager;->modifySubPanelLayoutForSplitWindow(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    #@59b
    goto/16 :goto_9a

    #@59d
    .line 5333
    :cond_59d
    move-object/from16 v0, p2

    #@59f
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@5a1
    const/16 v3, 0x7de

    #@5a3
    if-ne v2, v3, :cond_5d9

    #@5a5
    .line 5338
    move-object/from16 v0, p0

    #@5a7
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@5a9
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@5ab
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@5ad
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@5af
    .line 5339
    move-object/from16 v0, p0

    #@5b1
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@5b3
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@5b5
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@5b7
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@5b9
    .line 5340
    move-object/from16 v0, p0

    #@5bb
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenLeft:I

    #@5bd
    move-object/from16 v0, p0

    #@5bf
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenWidth:I

    #@5c1
    add-int/2addr v2, v3

    #@5c2
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@5c4
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@5c6
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@5c8
    .line 5341
    move-object/from16 v0, p0

    #@5ca
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenTop:I

    #@5cc
    move-object/from16 v0, p0

    #@5ce
    iget v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestrictedScreenHeight:I

    #@5d0
    add-int/2addr v2, v3

    #@5d1
    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    #@5d3
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@5d5
    iput v2, v8, Landroid/graphics/Rect;->bottom:I

    #@5d7
    goto/16 :goto_9a

    #@5d9
    .line 5344
    :cond_5d9
    move-object/from16 v0, p0

    #@5db
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentLeft:I

    #@5dd
    iput v2, v8, Landroid/graphics/Rect;->left:I

    #@5df
    .line 5345
    move-object/from16 v0, p0

    #@5e1
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentTop:I

    #@5e3
    iput v2, v8, Landroid/graphics/Rect;->top:I

    #@5e5
    .line 5346
    move-object/from16 v0, p0

    #@5e7
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentRight:I

    #@5e9
    iput v2, v8, Landroid/graphics/Rect;->right:I

    #@5eb
    .line 5347
    move/from16 v0, v20

    #@5ed
    iput v0, v8, Landroid/graphics/Rect;->bottom:I

    #@5ef
    .line 5348
    const/16 v2, 0x10

    #@5f1
    if-eq v5, v2, :cond_662

    #@5f3
    .line 5349
    move-object/from16 v0, p0

    #@5f5
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@5f7
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@5f9
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@5fb
    .line 5350
    move-object/from16 v0, p0

    #@5fd
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@5ff
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@601
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@603
    .line 5351
    move-object/from16 v0, p0

    #@605
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@607
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@609
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@60b
    .line 5352
    move-object/from16 v0, p0

    #@60d
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@60f
    iput v2, v10, Landroid/graphics/Rect;->bottom:I

    #@611
    iput v2, v9, Landroid/graphics/Rect;->bottom:I

    #@613
    .line 5359
    :goto_613
    const/16 v2, 0x30

    #@615
    if-eq v5, v2, :cond_683

    #@617
    .line 5360
    move-object/from16 v0, p0

    #@619
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurLeft:I

    #@61b
    iput v2, v11, Landroid/graphics/Rect;->left:I

    #@61d
    .line 5361
    move-object/from16 v0, p0

    #@61f
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurTop:I

    #@621
    iput v2, v11, Landroid/graphics/Rect;->top:I

    #@623
    .line 5362
    move-object/from16 v0, p0

    #@625
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurRight:I

    #@627
    iput v2, v11, Landroid/graphics/Rect;->right:I

    #@629
    .line 5363
    move/from16 v0, v21

    #@62b
    iput v0, v11, Landroid/graphics/Rect;->bottom:I

    #@62d
    .line 5368
    :goto_62d
    sget-boolean v2, Lcom/lge/config/ConfigBuildFlags;->CAPP_SPLITWINDOW:Z

    #@62f
    if-eqz v2, :cond_9a

    #@631
    move-object/from16 v6, p0

    #@633
    move-object/from16 v7, p1

    #@635
    move-object/from16 v12, p2

    #@637
    .line 5369
    invoke-direct/range {v6 .. v12}, Lcom/android/internal/policy/impl/PhoneWindowManager;->modifyLayoutForSplitWindow(Landroid/view/WindowManagerPolicy$WindowState;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/view/WindowManager$LayoutParams;)V

    #@63a
    .line 5371
    move-object/from16 v0, p0

    #@63c
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@63e
    if-nez v2, :cond_643

    #@640
    .line 5372
    invoke-direct/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->getSplitWindowManagerInstance()Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@643
    .line 5375
    :cond_643
    :try_start_643
    move-object/from16 v0, p0

    #@645
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@647
    if-eqz v2, :cond_9a

    #@649
    move-object/from16 v0, p0

    #@64b
    iget-object v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSplitWindowManager:Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;

    #@64d
    invoke-interface {v2}, Lcom/lge/loader/splitwindow/ISplitWindow$ISplitWindowPolicy;->isSplitMode()Z

    #@650
    move-result v2

    #@651
    if-eqz v2, :cond_9a

    #@653
    .line 5376
    invoke-virtual {v11, v10}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V
    :try_end_656
    .catch Landroid/os/RemoteException; {:try_start_643 .. :try_end_656} :catch_658
    .catch Ljava/lang/NullPointerException; {:try_start_643 .. :try_end_656} :catch_687

    #@656
    goto/16 :goto_9a

    #@658
    .line 5377
    :catch_658
    move-exception v22

    #@659
    .line 5378
    .local v22, e:Landroid/os/RemoteException;
    const-string v2, "SplitWindow"

    #@65b
    const-string v3, "Binder service (SplitWindowPolicyService) is not available"

    #@65d
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@660
    goto/16 :goto_9a

    #@662
    .line 5354
    .end local v22           #e:Landroid/os/RemoteException;
    :cond_662
    move-object/from16 v0, p0

    #@664
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentLeft:I

    #@666
    iput v2, v10, Landroid/graphics/Rect;->left:I

    #@668
    iput v2, v9, Landroid/graphics/Rect;->left:I

    #@66a
    .line 5355
    move-object/from16 v0, p0

    #@66c
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentTop:I

    #@66e
    iput v2, v10, Landroid/graphics/Rect;->top:I

    #@670
    iput v2, v9, Landroid/graphics/Rect;->top:I

    #@672
    .line 5356
    move-object/from16 v0, p0

    #@674
    iget v2, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentRight:I

    #@676
    iput v2, v10, Landroid/graphics/Rect;->right:I

    #@678
    iput v2, v9, Landroid/graphics/Rect;->right:I

    #@67a
    .line 5357
    move/from16 v0, v20

    #@67c
    iput v0, v10, Landroid/graphics/Rect;->bottom:I

    #@67e
    move/from16 v0, v20

    #@680
    iput v0, v9, Landroid/graphics/Rect;->bottom:I

    #@682
    goto :goto_613

    #@683
    .line 5365
    :cond_683
    invoke-virtual {v11, v10}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@686
    goto :goto_62d

    #@687
    .line 5379
    :catch_687
    move-exception v22

    #@688
    .line 5380
    .local v22, e:Ljava/lang/NullPointerException;
    const-string v2, "SplitWindow"

    #@68a
    const-string v3, "Binder service (SplitWindowPolicyService) is not available"

    #@68c
    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@68f
    goto/16 :goto_9a

    #@691
    .line 5393
    .end local v22           #e:Ljava/lang/NullPointerException;
    .restart local v25       #isFullScreenMode:Z
    .restart local v26       #isSplitMediaLayoutAttached:Z
    .restart local v28       #screenId:I
    :cond_691
    const/16 v26, 0x0

    #@693
    goto/16 :goto_c8
.end method

.method public lockNow(Landroid/os/Bundle;)V
    .registers 5
    .parameter "options"

    #@0
    .prologue
    .line 7609
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2
    const-string v1, "android.permission.DEVICE_POWER"

    #@4
    const/4 v2, 0x0

    #@5
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@8
    .line 7610
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@a
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@f
    .line 7611
    if-eqz p1, :cond_16

    #@11
    .line 7614
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@13
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;->setLockOptions(Landroid/os/Bundle;)V

    #@16
    .line 7616
    :cond_16
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@18
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@1a
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@1d
    .line 7617
    return-void
.end method

.method needSensorRunningLp()Z
    .registers 5

    #@0
    .prologue
    const/4 v3, 0x4

    #@1
    const/4 v0, 0x1

    #@2
    .line 1043
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurrentAppOrientation:I

    #@4
    if-eq v1, v3, :cond_16

    #@6
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurrentAppOrientation:I

    #@8
    const/16 v2, 0xa

    #@a
    if-eq v1, v2, :cond_16

    #@c
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurrentAppOrientation:I

    #@e
    const/4 v2, 0x7

    #@f
    if-eq v1, v2, :cond_16

    #@11
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurrentAppOrientation:I

    #@13
    const/4 v2, 0x6

    #@14
    if-ne v1, v2, :cond_17

    #@16
    .line 1070
    :cond_16
    :goto_16
    return v0

    #@17
    .line 1051
    :cond_17
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockEnablesAccelerometer:Z

    #@19
    if-eqz v1, :cond_20

    #@1b
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@1d
    const/4 v2, 0x2

    #@1e
    if-eq v1, v2, :cond_16

    #@20
    :cond_20
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockEnablesAccelerometer:Z

    #@22
    if-eqz v1, :cond_31

    #@24
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@26
    if-eq v1, v0, :cond_16

    #@28
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@2a
    const/4 v2, 0x3

    #@2b
    if-eq v1, v2, :cond_16

    #@2d
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@2f
    if-eq v1, v3, :cond_16

    #@31
    .line 1059
    :cond_31
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotationMode:I

    #@33
    if-ne v1, v0, :cond_16

    #@35
    .line 1068
    const/4 v0, 0x0

    #@36
    goto :goto_16
.end method

.method public notifyJackSwitchChanged(JII)V
    .registers 8
    .parameter "whenNanos"
    .parameter "switchValues"
    .parameter "switchMask"

    #@0
    .prologue
    .line 5803
    const-string v0, "WindowManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "notifyJackSwitchChanged(): switchValues "

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, " switchMask "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@1e
    move-result-object v1

    #@1f
    invoke-static {v0, v1}, Landroid/util/Slog;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@22
    .line 5804
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetLock:Ljava/lang/Object;

    #@24
    monitor-enter v1

    #@25
    .line 5805
    :try_start_25
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSwitchValues:I

    #@27
    xor-int/lit8 v2, p4, -0x1

    #@29
    and-int/2addr v0, v2

    #@2a
    or-int/2addr v0, p3

    #@2b
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSwitchValues:I

    #@2d
    .line 5806
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSwitchValues:I

    #@2f
    and-int/lit8 v0, v0, 0x14

    #@31
    sparse-switch v0, :sswitch_data_50

    #@34
    .line 5824
    const/4 v0, 0x0

    #@35
    sput v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetJackState:I

    #@37
    .line 5828
    :goto_37
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->update()V

    #@3a
    .line 5829
    monitor-exit v1

    #@3b
    .line 5830
    return-void

    #@3c
    .line 5808
    :sswitch_3c
    const/4 v0, 0x0

    #@3d
    sput v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetJackState:I

    #@3f
    goto :goto_37

    #@40
    .line 5829
    :catchall_40
    move-exception v0

    #@41
    monitor-exit v1
    :try_end_42
    .catchall {:try_start_25 .. :try_end_42} :catchall_40

    #@42
    throw v0

    #@43
    .line 5812
    :sswitch_43
    const/4 v0, 0x2

    #@44
    :try_start_44
    sput v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetJackState:I

    #@46
    goto :goto_37

    #@47
    .line 5816
    :sswitch_47
    const/4 v0, 0x1

    #@48
    sput v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetJackState:I

    #@4a
    goto :goto_37

    #@4b
    .line 5820
    :sswitch_4b
    const/4 v0, 0x1

    #@4c
    sput v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetJackState:I
    :try_end_4e
    .catchall {:try_start_44 .. :try_end_4e} :catchall_40

    #@4e
    goto :goto_37

    #@4f
    .line 5806
    nop

    #@50
    :sswitch_data_50
    .sparse-switch
        0x0 -> :sswitch_3c
        0x4 -> :sswitch_43
        0x10 -> :sswitch_4b
        0x14 -> :sswitch_47
    .end sparse-switch
.end method

.method public notifyLidSwitchChanged(JZ)V
    .registers 9
    .parameter "whenNanos"
    .parameter "lidOpen"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v2, 0x0

    #@2
    .line 5726
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadless:Z

    #@4
    if-eqz v3, :cond_7

    #@6
    .line 5747
    :cond_6
    :goto_6
    return-void

    #@7
    .line 5729
    :cond_7
    if-eqz p3, :cond_26

    #@9
    move v0, v1

    #@a
    .line 5730
    .local v0, newLidState:I
    :goto_a
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidState:I

    #@c
    if-eq v0, v3, :cond_6

    #@e
    .line 5734
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidState:I

    #@10
    .line 5735
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->applyLidSwitchState()V

    #@13
    .line 5736
    invoke-virtual {p0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateRotation(Z)V

    #@16
    .line 5738
    if-eqz p3, :cond_32

    #@18
    .line 5739
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->keyguardIsShowingTq()Z

    #@1b
    move-result v1

    #@1c
    if-eqz v1, :cond_28

    #@1e
    .line 5740
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@20
    const/16 v2, 0x1a

    #@22
    invoke-virtual {v1, v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->onWakeKeyWhenKeyguardShowingTq(I)V

    #@25
    goto :goto_6

    #@26
    .end local v0           #newLidState:I
    :cond_26
    move v0, v2

    #@27
    .line 5729
    goto :goto_a

    #@28
    .line 5742
    .restart local v0       #newLidState:I
    :cond_28
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@2a
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@2d
    move-result-wide v2

    #@2e
    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager;->wakeUp(J)V

    #@31
    goto :goto_6

    #@32
    .line 5744
    :cond_32
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidControlsSleep:Z

    #@34
    if-nez v1, :cond_6

    #@36
    .line 5745
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPowerManager:Landroid/os/PowerManager;

    #@38
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    #@3b
    move-result-wide v3

    #@3c
    invoke-virtual {v1, v3, v4, v2}, Landroid/os/PowerManager;->userActivity(JZ)V

    #@3f
    goto :goto_6
.end method

.method public performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z
    .registers 16
    .parameter "win"
    .parameter "effectId"
    .parameter "always"

    #@0
    .prologue
    .line 7799
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2
    invoke-virtual {v8}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@5
    move-result-object v8

    #@6
    const-string v9, "haptic_feedback_enabled"

    #@8
    const/4 v10, 0x0

    #@9
    const/4 v11, -0x2

    #@a
    invoke-static {v8, v9, v10, v11}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@d
    move-result v8

    #@e
    if-nez v8, :cond_1f

    #@10
    const/4 v3, 0x1

    #@11
    .line 7801
    .local v3, hapticsDisabled:Z
    :goto_11
    if-nez p3, :cond_21

    #@13
    if-nez v3, :cond_1d

    #@15
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@17
    invoke-virtual {v8}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->isShowingAndNotHidden()Z

    #@1a
    move-result v8

    #@1b
    if-eqz v8, :cond_21

    #@1d
    .line 7802
    :cond_1d
    const/4 v8, 0x0

    #@1e
    .line 7865
    :goto_1e
    return v8

    #@1f
    .line 7799
    .end local v3           #hapticsDisabled:Z
    :cond_1f
    const/4 v3, 0x0

    #@20
    goto :goto_11

    #@21
    .line 7805
    .restart local v3       #hapticsDisabled:Z
    :cond_21
    sget-boolean v8, Lcom/lge/config/ConfigBuildFlags;->CAPP_KEY_EXCEPTION:Z

    #@23
    if-eqz v8, :cond_3f

    #@25
    .line 7806
    const/4 v8, 0x1

    #@26
    if-ne p2, v8, :cond_3f

    #@28
    .line 7807
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@2a
    if-eqz v8, :cond_3d

    #@2c
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTopFullscreenOpaqueWindowState:Landroid/view/WindowManagerPolicy$WindowState;

    #@2e
    invoke-interface {v8}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@31
    move-result-object v0

    #@32
    .line 7808
    .local v0, attrs:Landroid/view/WindowManager$LayoutParams;
    :goto_32
    if-eqz v0, :cond_3f

    #@34
    .line 7809
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->extend:I

    #@36
    .line 7810
    .local v2, extend:I
    const/high16 v8, 0x8

    #@38
    and-int/2addr v8, v2

    #@39
    if-eqz v8, :cond_3f

    #@3b
    .line 7811
    const/4 v8, 0x0

    #@3c
    goto :goto_1e

    #@3d
    .line 7807
    .end local v0           #attrs:Landroid/view/WindowManager$LayoutParams;
    .end local v2           #extend:I
    :cond_3d
    const/4 v0, 0x0

    #@3e
    goto :goto_32

    #@3f
    .line 7817
    :cond_3f
    const/4 v5, 0x0

    #@40
    .line 7818
    .local v5, pattern:[J
    sparse-switch p2, :sswitch_data_d4

    #@43
    .line 7835
    const/4 v8, 0x0

    #@44
    goto :goto_1e

    #@45
    .line 7820
    :sswitch_45
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLongPressVibePattern:[J

    #@47
    .line 7837
    :goto_47
    array-length v8, v5

    #@48
    const/4 v9, 0x1

    #@49
    if-ne v8, v9, :cond_8b

    #@4b
    .line 7840
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@4d
    iget-object v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@4f
    const/4 v9, 0x2

    #@50
    invoke-interface {v8, v9}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->getVibrateVolume(I)I

    #@53
    move-result v6

    #@54
    .line 7841
    .local v6, vibVolume:I
    const-string v8, "WindowManager"

    #@56
    new-instance v9, Ljava/lang/StringBuilder;

    #@58
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@5b
    const-string v10, "vibVolume ="

    #@5d
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@60
    move-result-object v9

    #@61
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@64
    move-result-object v9

    #@65
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@68
    move-result-object v9

    #@69
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@6c
    .line 7843
    :try_start_6c
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@6e
    const/4 v9, 0x0

    #@6f
    aget-wide v9, v5, v9

    #@71
    invoke-interface {v8, v9, v10, v6}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->vibrate(JI)V
    :try_end_74
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_6c .. :try_end_74} :catch_82

    #@74
    .line 7865
    :goto_74
    const/4 v8, 0x1

    #@75
    goto :goto_1e

    #@76
    .line 7823
    .end local v6           #vibVolume:I
    :sswitch_76
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVirtualKeyVibePattern:[J

    #@78
    .line 7824
    goto :goto_47

    #@79
    .line 7826
    :sswitch_79
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyboardTapVibePattern:[J

    #@7b
    .line 7827
    goto :goto_47

    #@7c
    .line 7829
    :sswitch_7c
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSafeModeDisabledVibePattern:[J

    #@7e
    .line 7830
    goto :goto_47

    #@7f
    .line 7832
    :sswitch_7f
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSafeModeEnabledVibePattern:[J

    #@81
    .line 7833
    goto :goto_47

    #@82
    .line 7844
    .restart local v6       #vibVolume:I
    :catch_82
    move-exception v1

    #@83
    .line 7845
    .local v1, e:Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v8, "WindowManager"

    #@85
    const-string v9, "Unexpected ArrayIndexOutOfBoundsException was thrown"

    #@87
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@8a
    goto :goto_74

    #@8b
    .line 7851
    .end local v1           #e:Ljava/lang/ArrayIndexOutOfBoundsException;
    .end local v6           #vibVolume:I
    :cond_8b
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@8d
    iget-object v9, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@8f
    const/4 v9, 0x2

    #@90
    invoke-interface {v8, v9}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->getVibrateVolume(I)I

    #@93
    move-result v6

    #@94
    .line 7852
    .restart local v6       #vibVolume:I
    array-length v8, v5

    #@95
    new-array v7, v8, [I

    #@97
    .line 7853
    .local v7, volumeIndex:[I
    const/4 v4, 0x0

    #@98
    .local v4, i:I
    :goto_98
    array-length v8, v7

    #@99
    if-ge v4, v8, :cond_a0

    #@9b
    .line 7854
    aput v6, v7, v4

    #@9d
    .line 7853
    add-int/lit8 v4, v4, 0x1

    #@9f
    goto :goto_98

    #@a0
    .line 7856
    :cond_a0
    const-string v8, "WindowManager"

    #@a2
    new-instance v9, Ljava/lang/StringBuilder;

    #@a4
    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    #@a7
    const-string v10, "mVolumeVibrator.vibrate pattern.length = "

    #@a9
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@ac
    move-result-object v9

    #@ad
    array-length v10, v5

    #@ae
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@b1
    move-result-object v9

    #@b2
    const-string v10, " vibVolume = "

    #@b4
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@b7
    move-result-object v9

    #@b8
    invoke-virtual {v9, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@bb
    move-result-object v9

    #@bc
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@bf
    move-result-object v9

    #@c0
    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    #@c3
    .line 7859
    :try_start_c3
    iget-object v8, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mVolumeVibrator:Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;

    #@c5
    const/4 v9, -0x1

    #@c6
    invoke-interface {v8, v5, v9, v7}, Lcom/lge/loader/volumevibrator/IVolumeVibratorLoader;->vibrate([JI[I)V
    :try_end_c9
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_c3 .. :try_end_c9} :catch_ca

    #@c9
    goto :goto_74

    #@ca
    .line 7860
    :catch_ca
    move-exception v1

    #@cb
    .line 7861
    .restart local v1       #e:Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v8, "WindowManager"

    #@cd
    const-string v9, "Unexpected ArrayIndexOutOfBoundsException was thrown"

    #@cf
    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@d2
    goto :goto_74

    #@d3
    .line 7818
    nop

    #@d4
    :sswitch_data_d4
    .sparse-switch
        0x0 -> :sswitch_45
        0x1 -> :sswitch_76
        0x3 -> :sswitch_79
        0x2710 -> :sswitch_7c
        0x2711 -> :sswitch_7f
    .end sparse-switch
.end method

.method public prepareAddWindowLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManager$LayoutParams;)I
    .registers 7
    .parameter "win"
    .parameter "attrs"

    #@0
    .prologue
    const/4 v0, -0x7

    #@1
    .line 3086
    iget v1, p2, Landroid/view/WindowManager$LayoutParams;->type:I

    #@3
    sparse-switch v1, :sswitch_data_5e

    #@6
    .line 3132
    :goto_6
    const/4 v0, 0x0

    #@7
    :cond_7
    return v0

    #@8
    .line 3088
    :sswitch_8
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@a
    const-string v2, "android.permission.STATUS_BAR_SERVICE"

    #@c
    const-string v3, "PhoneWindowManager"

    #@e
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@11
    .line 3091
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@13
    if-eqz v1, :cond_1d

    #@15
    .line 3092
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@17
    invoke-interface {v1}, Landroid/view/WindowManagerPolicy$WindowState;->isAlive()Z

    #@1a
    move-result v1

    #@1b
    if-nez v1, :cond_7

    #@1d
    .line 3096
    :cond_1d
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@1f
    goto :goto_6

    #@20
    .line 3099
    :sswitch_20
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@22
    const-string v2, "android.permission.STATUS_BAR_SERVICE"

    #@24
    const-string v3, "PhoneWindowManager"

    #@26
    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@29
    .line 3102
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@2b
    if-eqz v1, :cond_35

    #@2d
    .line 3103
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@2f
    invoke-interface {v1}, Landroid/view/WindowManagerPolicy$WindowState;->isAlive()Z

    #@32
    move-result v1

    #@33
    if-nez v1, :cond_7

    #@35
    .line 3107
    :cond_35
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@37
    goto :goto_6

    #@38
    .line 3111
    :sswitch_38
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@3a
    const-string v1, "android.permission.STATUS_BAR_SERVICE"

    #@3c
    const-string v2, "PhoneWindowManager"

    #@3e
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@41
    goto :goto_6

    #@42
    .line 3116
    :sswitch_42
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@44
    const-string v1, "android.permission.STATUS_BAR_SERVICE"

    #@46
    const-string v2, "PhoneWindowManager"

    #@48
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@4b
    goto :goto_6

    #@4c
    .line 3121
    :sswitch_4c
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@4e
    const-string v1, "android.permission.STATUS_BAR_SERVICE"

    #@50
    const-string v2, "PhoneWindowManager"

    #@52
    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->enforceCallingOrSelfPermission(Ljava/lang/String;Ljava/lang/String;)V

    #@55
    goto :goto_6

    #@56
    .line 3126
    :sswitch_56
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@58
    if-nez v1, :cond_7

    #@5a
    .line 3129
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@5c
    goto :goto_6

    #@5d
    .line 3086
    nop

    #@5e
    :sswitch_data_5e
    .sparse-switch
        0x7d0 -> :sswitch_8
        0x7d4 -> :sswitch_56
        0x7de -> :sswitch_42
        0x7e1 -> :sswitch_4c
        0x7e3 -> :sswitch_20
        0x7e8 -> :sswitch_38
    .end sparse-switch
.end method

.method readLidState()V
    .registers 2

    #@0
    .prologue
    .line 2713
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@2
    invoke-interface {v0}, Landroid/view/WindowManagerPolicy$WindowManagerFuncs;->getLidState()I

    #@5
    move-result v0

    #@6
    iput v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidState:I

    #@8
    .line 2714
    return-void
.end method

.method public removeStartingWindow(Landroid/os/IBinder;Landroid/view/View;)V
    .registers 6
    .parameter "appToken"
    .parameter "window"

    #@0
    .prologue
    .line 3064
    if-eqz p2, :cond_f

    #@2
    .line 3065
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@4
    const-string v2, "window"

    #@6
    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    #@9
    move-result-object v0

    #@a
    check-cast v0, Landroid/view/WindowManager;

    #@c
    .line 3066
    .local v0, wm:Landroid/view/WindowManager;
    invoke-interface {v0, p2}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    #@f
    .line 3068
    .end local v0           #wm:Landroid/view/WindowManager;
    :cond_f
    return-void
.end method

.method public removeWindowLw(Landroid/view/WindowManagerPolicy$WindowState;)V
    .registers 4
    .parameter "win"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 3137
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@3
    if-ne v0, p1, :cond_8

    #@5
    .line 3138
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@7
    .line 3144
    :cond_7
    :goto_7
    return-void

    #@8
    .line 3139
    :cond_8
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@a
    if-ne v0, p1, :cond_f

    #@c
    .line 3140
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguard:Landroid/view/WindowManagerPolicy$WindowState;

    #@e
    goto :goto_7

    #@f
    .line 3141
    :cond_f
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@11
    if-ne v0, p1, :cond_7

    #@13
    .line 3142
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@15
    goto :goto_7
.end method

.method public rotationForOrientationLw(II)I
    .registers 14
    .parameter "orientation"
    .parameter "lastRotation"

    #@0
    .prologue
    const/4 v10, 0x3

    #@1
    const/4 v2, 0x0

    #@2
    const/4 v9, -0x1

    #@3
    const/4 v8, 0x2

    #@4
    const/4 v4, 0x1

    #@5
    .line 7226
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@7
    monitor-enter v5

    #@8
    .line 7227
    :try_start_8
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationListener:Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;

    #@a
    invoke-virtual {v3}, Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;->getProposedRotation()I

    #@d
    move-result v1

    #@e
    .line 7228
    .local v1, sensorRotation:I
    if-gez v1, :cond_11

    #@10
    .line 7229
    move v1, p2

    #@11
    .line 7233
    :cond_11
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidState:I

    #@13
    if-ne v3, v4, :cond_22

    #@15
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidOpenRotation:I

    #@17
    if-ltz v3, :cond_22

    #@19
    .line 7235
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLidOpenRotation:I

    #@1b
    .line 7318
    .local v0, preferredRotation:I
    :goto_1b
    packed-switch p1, :pswitch_data_172

    #@1e
    .line 7370
    :pswitch_1e
    if-ltz v0, :cond_16e

    #@20
    .line 7371
    monitor-exit v5

    #@21
    .line 7373
    .end local v0           #preferredRotation:I
    :goto_21
    return v0

    #@22
    .line 7236
    :cond_22
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@24
    if-ne v3, v8, :cond_37

    #@26
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockEnablesAccelerometer:Z

    #@28
    if-nez v3, :cond_2e

    #@2a
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockRotation:I

    #@2c
    if-ltz v3, :cond_37

    #@2e
    .line 7241
    :cond_2e
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockEnablesAccelerometer:Z

    #@30
    if-eqz v3, :cond_34

    #@32
    move v0, v1

    #@33
    .restart local v0       #preferredRotation:I
    :goto_33
    goto :goto_1b

    #@34
    .end local v0           #preferredRotation:I
    :cond_34
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCarDockRotation:I

    #@36
    goto :goto_33

    #@37
    .line 7243
    :cond_37
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@39
    if-eq v3, v4, :cond_44

    #@3b
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@3d
    if-eq v3, v10, :cond_44

    #@3f
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockMode:I

    #@41
    const/4 v6, 0x4

    #@42
    if-ne v3, v6, :cond_55

    #@44
    :cond_44
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockEnablesAccelerometer:Z

    #@46
    if-nez v3, :cond_4c

    #@48
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockRotation:I

    #@4a
    if-ltz v3, :cond_55

    #@4c
    .line 7250
    :cond_4c
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockEnablesAccelerometer:Z

    #@4e
    if-eqz v3, :cond_52

    #@50
    move v0, v1

    #@51
    .restart local v0       #preferredRotation:I
    :goto_51
    goto :goto_1b

    #@52
    .end local v0           #preferredRotation:I
    :cond_52
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDeskDockRotation:I

    #@54
    goto :goto_51

    #@55
    .line 7253
    :cond_55
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiPlugged:Z

    #@57
    if-eqz v3, :cond_a2

    #@59
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiRotationLock:Z

    #@5b
    if-eqz v3, :cond_a2

    #@5d
    .line 7261
    const-string v3, "WindowManager"

    #@5f
    new-instance v6, Ljava/lang/StringBuilder;

    #@61
    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    #@64
    const-string v7, "orientation="

    #@66
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@69
    move-result-object v6

    #@6a
    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@6d
    move-result-object v6

    #@6e
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@71
    move-result-object v6

    #@72
    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@75
    .line 7262
    const/4 v3, 0x5

    #@76
    if-eq p1, v3, :cond_80

    #@78
    if-eq p1, v9, :cond_80

    #@7a
    if-eq p1, v4, :cond_80

    #@7c
    if-eq p1, v8, :cond_80

    #@7e
    if-ne p1, v10, :cond_87

    #@80
    :cond_80
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotationMode:I

    #@82
    if-ne v3, v4, :cond_87

    #@84
    .line 7268
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@86
    .restart local v0       #preferredRotation:I
    goto :goto_1b

    #@87
    .line 7271
    .end local v0           #preferredRotation:I
    :cond_87
    const-string v3, "WindowManager"

    #@89
    new-instance v4, Ljava/lang/StringBuilder;

    #@8b
    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    #@8e
    const-string v6, "sensorRotation="

    #@90
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@93
    move-result-object v4

    #@94
    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@97
    move-result-object v4

    #@98
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@9b
    move-result-object v4

    #@9c
    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    #@9f
    .line 7272
    move v0, v1

    #@a0
    .restart local v0       #preferredRotation:I
    goto/16 :goto_1b

    #@a2
    .line 7275
    .end local v0           #preferredRotation:I
    :cond_a2
    const-string v3, "LGU"

    #@a4
    const-string v6, "ro.build.target_operator"

    #@a6
    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@a9
    move-result-object v6

    #@aa
    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ad
    move-result v3

    #@ae
    if-eqz v3, :cond_b8

    #@b0
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotationMode:I

    #@b2
    if-ne v3, v8, :cond_b8

    #@b4
    .line 7277
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotation:I

    #@b6
    .restart local v0       #preferredRotation:I
    goto/16 :goto_1b

    #@b8
    .line 7279
    .end local v0           #preferredRotation:I
    :cond_b8
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotationMode:I

    #@ba
    if-nez v3, :cond_c0

    #@bc
    if-eq p1, v8, :cond_cd

    #@be
    if-eq p1, v9, :cond_cd

    #@c0
    :cond_c0
    const/4 v3, 0x4

    #@c1
    if-eq p1, v3, :cond_cd

    #@c3
    const/16 v3, 0xa

    #@c5
    if-eq p1, v3, :cond_cd

    #@c7
    const/4 v3, 0x6

    #@c8
    if-eq p1, v3, :cond_cd

    #@ca
    const/4 v3, 0x7

    #@cb
    if-ne p1, v3, :cond_f5

    #@cd
    .line 7288
    :cond_cd
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAllowAllRotations:I

    #@cf
    if-gez v3, :cond_e3

    #@d1
    .line 7292
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@d3
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@d6
    move-result-object v3

    #@d7
    const v6, 0x111001d

    #@da
    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@dd
    move-result v3

    #@de
    if-eqz v3, :cond_f0

    #@e0
    move v3, v4

    #@e1
    :goto_e1
    iput v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAllowAllRotations:I

    #@e3
    .line 7295
    :cond_e3
    if-ne v1, v8, :cond_ed

    #@e5
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mAllowAllRotations:I

    #@e7
    if-eq v3, v4, :cond_ed

    #@e9
    const/16 v3, 0xa

    #@eb
    if-ne p1, v3, :cond_f2

    #@ed
    .line 7298
    :cond_ed
    move v0, v1

    #@ee
    .restart local v0       #preferredRotation:I
    goto/16 :goto_1b

    #@f0
    .end local v0           #preferredRotation:I
    :cond_f0
    move v3, v2

    #@f1
    .line 7292
    goto :goto_e1

    #@f2
    .line 7300
    :cond_f2
    move v0, p2

    #@f3
    .restart local v0       #preferredRotation:I
    goto/16 :goto_1b

    #@f5
    .line 7302
    .end local v0           #preferredRotation:I
    :cond_f5
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotationMode:I

    #@f7
    if-ne v3, v4, :cond_100

    #@f9
    const/4 v3, 0x5

    #@fa
    if-eq p1, v3, :cond_100

    #@fc
    .line 7308
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotation:I

    #@fe
    .restart local v0       #preferredRotation:I
    goto/16 :goto_1b

    #@100
    .line 7312
    .end local v0           #preferredRotation:I
    :cond_100
    const/4 v0, -0x1

    #@101
    .restart local v0       #preferredRotation:I
    goto/16 :goto_1b

    #@103
    .line 7321
    :pswitch_103
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isAnyPortrait(I)Z

    #@106
    move-result v2

    #@107
    if-eqz v2, :cond_10f

    #@109
    .line 7322
    monitor-exit v5

    #@10a
    goto/16 :goto_21

    #@10c
    .line 7375
    .end local v0           #preferredRotation:I
    .end local v1           #sensorRotation:I
    :catchall_10c
    move-exception v2

    #@10d
    monitor-exit v5
    :try_end_10e
    .catchall {:try_start_8 .. :try_end_10e} :catchall_10c

    #@10e
    throw v2

    #@10f
    .line 7324
    .restart local v0       #preferredRotation:I
    .restart local v1       #sensorRotation:I
    :cond_10f
    :try_start_10f
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@111
    .end local v0           #preferredRotation:I
    monitor-exit v5

    #@112
    goto/16 :goto_21

    #@114
    .line 7328
    .restart local v0       #preferredRotation:I
    :pswitch_114
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isLandscapeOrSeascape(I)Z

    #@117
    move-result v2

    #@118
    if-eqz v2, :cond_11d

    #@11a
    .line 7329
    monitor-exit v5

    #@11b
    goto/16 :goto_21

    #@11d
    .line 7331
    :cond_11d
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@11f
    .end local v0           #preferredRotation:I
    monitor-exit v5

    #@120
    goto/16 :goto_21

    #@122
    .line 7335
    .restart local v0       #preferredRotation:I
    :pswitch_122
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isAnyPortrait(I)Z

    #@125
    move-result v2

    #@126
    if-eqz v2, :cond_12b

    #@128
    .line 7336
    monitor-exit v5

    #@129
    goto/16 :goto_21

    #@12b
    .line 7338
    :cond_12b
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUpsideDownRotation:I

    #@12d
    .end local v0           #preferredRotation:I
    monitor-exit v5

    #@12e
    goto/16 :goto_21

    #@130
    .line 7342
    .restart local v0       #preferredRotation:I
    :pswitch_130
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isLandscapeOrSeascape(I)Z

    #@133
    move-result v2

    #@134
    if-eqz v2, :cond_139

    #@136
    .line 7343
    monitor-exit v5

    #@137
    goto/16 :goto_21

    #@139
    .line 7345
    :cond_139
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSeascapeRotation:I

    #@13b
    .end local v0           #preferredRotation:I
    monitor-exit v5

    #@13c
    goto/16 :goto_21

    #@13e
    .line 7349
    .restart local v0       #preferredRotation:I
    :pswitch_13e
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isLandscapeOrSeascape(I)Z

    #@141
    move-result v2

    #@142
    if-eqz v2, :cond_147

    #@144
    .line 7350
    monitor-exit v5

    #@145
    goto/16 :goto_21

    #@147
    .line 7352
    :cond_147
    invoke-direct {p0, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isLandscapeOrSeascape(I)Z

    #@14a
    move-result v2

    #@14b
    if-eqz v2, :cond_151

    #@14d
    .line 7353
    monitor-exit v5

    #@14e
    move v0, p2

    #@14f
    goto/16 :goto_21

    #@151
    .line 7355
    :cond_151
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@153
    .end local v0           #preferredRotation:I
    monitor-exit v5

    #@154
    goto/16 :goto_21

    #@156
    .line 7359
    .restart local v0       #preferredRotation:I
    :pswitch_156
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isAnyPortrait(I)Z

    #@159
    move-result v2

    #@15a
    if-eqz v2, :cond_15f

    #@15c
    .line 7360
    monitor-exit v5

    #@15d
    goto/16 :goto_21

    #@15f
    .line 7362
    :cond_15f
    invoke-direct {p0, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isAnyPortrait(I)Z

    #@162
    move-result v2

    #@163
    if-eqz v2, :cond_169

    #@165
    .line 7363
    monitor-exit v5

    #@166
    move v0, p2

    #@167
    goto/16 :goto_21

    #@169
    .line 7365
    :cond_169
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@16b
    .end local v0           #preferredRotation:I
    monitor-exit v5

    #@16c
    goto/16 :goto_21

    #@16e
    .line 7373
    .restart local v0       #preferredRotation:I
    :cond_16e
    monitor-exit v5
    :try_end_16f
    .catchall {:try_start_10f .. :try_end_16f} :catchall_10c

    #@16f
    move v0, v2

    #@170
    goto/16 :goto_21

    #@172
    .line 7318
    :pswitch_data_172
    .packed-switch 0x0
        :pswitch_114
        :pswitch_103
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_13e
        :pswitch_156
        :pswitch_130
        :pswitch_122
    .end packed-switch
.end method

.method public rotationHasCompatibleMetricsLw(II)Z
    .registers 4
    .parameter "orientation"
    .parameter "rotation"

    #@0
    .prologue
    .line 7380
    packed-switch p1, :pswitch_data_10

    #@3
    .line 7392
    :pswitch_3
    const/4 v0, 0x1

    #@4
    :goto_4
    return v0

    #@5
    .line 7384
    :pswitch_5
    invoke-direct {p0, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isAnyPortrait(I)Z

    #@8
    move-result v0

    #@9
    goto :goto_4

    #@a
    .line 7389
    :pswitch_a
    invoke-direct {p0, p2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isLandscapeOrSeascape(I)Z

    #@d
    move-result v0

    #@e
    goto :goto_4

    #@f
    .line 7380
    nop

    #@10
    :pswitch_data_10
    .packed-switch 0x0
        :pswitch_a
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_a
        :pswitch_5
        :pswitch_a
        :pswitch_5
    .end packed-switch
.end method

.method public screenTurnedOff(I)V
    .registers 4
    .parameter "why"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 7046
    const v0, 0x11170

    #@4
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(II)I

    #@7
    .line 7047
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@9
    monitor-enter v1

    #@a
    .line 7048
    const/4 v0, 0x0

    #@b
    :try_start_b
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnEarly:Z

    #@d
    .line 7049
    const/4 v0, 0x0

    #@e
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnFully:Z

    #@10
    .line 7050
    monitor-exit v1
    :try_end_11
    .catchall {:try_start_b .. :try_end_11} :catchall_25

    #@11
    .line 7051
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@13
    if-eqz v0, :cond_1a

    #@15
    .line 7052
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@17
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->onScreenTurnedOff(I)V

    #@1a
    .line 7054
    :cond_1a
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@1c
    monitor-enter v1

    #@1d
    .line 7055
    :try_start_1d
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateOrientationListenerLp()V

    #@20
    .line 7056
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateLockScreenTimeout()V

    #@23
    .line 7057
    monitor-exit v1
    :try_end_24
    .catchall {:try_start_1d .. :try_end_24} :catchall_28

    #@24
    .line 7058
    return-void

    #@25
    .line 7050
    :catchall_25
    move-exception v0

    #@26
    :try_start_26
    monitor-exit v1
    :try_end_27
    .catchall {:try_start_26 .. :try_end_27} :catchall_25

    #@27
    throw v0

    #@28
    .line 7057
    :catchall_28
    move-exception v0

    #@29
    :try_start_29
    monitor-exit v1
    :try_end_2a
    .catchall {:try_start_29 .. :try_end_2a} :catchall_28

    #@2a
    throw v0
.end method

.method public screenTurningOn(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V
    .registers 4
    .parameter "screenOnListener"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    .line 7062
    const v0, 0x11170

    #@4
    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(II)I

    #@7
    .line 7069
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@9
    monitor-enter v1

    #@a
    .line 7070
    const/4 v0, 0x1

    #@b
    :try_start_b
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnEarly:Z

    #@d
    .line 7071
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateOrientationListenerLp()V

    #@10
    .line 7072
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateLockScreenTimeout()V

    #@13
    .line 7073
    monitor-exit v1
    :try_end_14
    .catchall {:try_start_b .. :try_end_14} :catchall_18

    #@14
    .line 7075
    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->waitForKeyguard(Landroid/view/WindowManagerPolicy$ScreenOnListener;)V

    #@17
    .line 7076
    return-void

    #@18
    .line 7073
    :catchall_18
    move-exception v0

    #@19
    :try_start_19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_19 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public selectAnimationLw(Landroid/view/WindowManagerPolicy$WindowState;I)I
    .registers 8
    .parameter "win"
    .parameter "transit"

    #@0
    .prologue
    const/16 v4, 0x2004

    #@2
    const/16 v3, 0x2002

    #@4
    const/16 v1, 0x1003

    #@6
    const/16 v2, 0x1001

    #@8
    .line 3152
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@a
    if-ne p1, v0, :cond_1c

    #@c
    .line 3153
    if-eq p2, v3, :cond_10

    #@e
    if-ne p2, v4, :cond_14

    #@10
    .line 3154
    :cond_10
    const v0, 0x10a001b

    #@13
    .line 3187
    :goto_13
    return v0

    #@14
    .line 3155
    :cond_14
    if-eq p2, v2, :cond_18

    #@16
    if-ne p2, v1, :cond_44

    #@18
    .line 3156
    :cond_18
    const v0, 0x10a001a

    #@1b
    goto :goto_13

    #@1c
    .line 3158
    :cond_1c
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBar:Landroid/view/WindowManagerPolicy$WindowState;

    #@1e
    if-ne p1, v0, :cond_44

    #@20
    .line 3160
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarOnBottom:Z

    #@22
    if-eqz v0, :cond_34

    #@24
    .line 3161
    if-eq p2, v3, :cond_28

    #@26
    if-ne p2, v4, :cond_2c

    #@28
    .line 3162
    :cond_28
    const v0, 0x10a0015

    #@2b
    goto :goto_13

    #@2c
    .line 3163
    :cond_2c
    if-eq p2, v2, :cond_30

    #@2e
    if-ne p2, v1, :cond_44

    #@30
    .line 3164
    :cond_30
    const v0, 0x10a0014

    #@33
    goto :goto_13

    #@34
    .line 3167
    :cond_34
    if-eq p2, v3, :cond_38

    #@36
    if-ne p2, v4, :cond_3c

    #@38
    .line 3168
    :cond_38
    const v0, 0x10a0019

    #@3b
    goto :goto_13

    #@3c
    .line 3169
    :cond_3c
    if-eq p2, v2, :cond_40

    #@3e
    if-ne p2, v1, :cond_44

    #@40
    .line 3170
    :cond_40
    const v0, 0x10a0018

    #@43
    goto :goto_13

    #@44
    .line 3173
    :cond_44
    const/4 v0, 0x5

    #@45
    if-ne p2, v0, :cond_51

    #@47
    .line 3174
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->hasAppShownWindows()Z

    #@4a
    move-result v0

    #@4b
    if-eqz v0, :cond_63

    #@4d
    .line 3176
    const v0, 0x10a0011

    #@50
    goto :goto_13

    #@51
    .line 3178
    :cond_51
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getAttrs()Landroid/view/WindowManager$LayoutParams;

    #@54
    move-result-object v0

    #@55
    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    #@57
    const/16 v1, 0x7e7

    #@59
    if-ne v0, v1, :cond_63

    #@5b
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDreamingLockscreen:Z

    #@5d
    if-eqz v0, :cond_63

    #@5f
    if-ne p2, v2, :cond_63

    #@61
    .line 3184
    const/4 v0, -0x1

    #@62
    goto :goto_13

    #@63
    .line 3187
    :cond_63
    const/4 v0, 0x0

    #@64
    goto :goto_13
.end method

.method sendCloseSystemWindows()V
    .registers 3

    #@0
    .prologue
    .line 7199
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-static {v0, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendCloseSystemWindows(Landroid/content/Context;Ljava/lang/String;)V

    #@6
    .line 7200
    return-void
.end method

.method sendCloseSystemWindows(Ljava/lang/String;)V
    .registers 3
    .parameter "reason"

    #@0
    .prologue
    .line 7203
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2
    invoke-static {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendCloseSystemWindows(Landroid/content/Context;Ljava/lang/String;)V

    #@5
    .line 7204
    return-void
.end method

.method setAttachedWindowFrames(Landroid/view/WindowManagerPolicy$WindowState;IILandroid/view/WindowManagerPolicy$WindowState;ZLandroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .registers 12
    .parameter "win"
    .parameter "fl"
    .parameter "adjust"
    .parameter "attached"
    .parameter "insetDecors"
    .parameter "pf"
    .parameter "df"
    .parameter "cf"
    .parameter "vf"

    #@0
    .prologue
    .line 4781
    invoke-interface {p1}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    #@3
    move-result v0

    #@4
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLayer:I

    #@6
    if-le v0, v1, :cond_3c

    #@8
    invoke-interface {p4}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    #@b
    move-result v0

    #@c
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLayer:I

    #@e
    if-ge v0, v1, :cond_3c

    #@10
    .line 4789
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLeft:I

    #@12
    iput v0, p9, Landroid/graphics/Rect;->left:I

    #@14
    iput v0, p8, Landroid/graphics/Rect;->left:I

    #@16
    iput v0, p7, Landroid/graphics/Rect;->left:I

    #@18
    .line 4790
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockTop:I

    #@1a
    iput v0, p9, Landroid/graphics/Rect;->top:I

    #@1c
    iput v0, p8, Landroid/graphics/Rect;->top:I

    #@1e
    iput v0, p7, Landroid/graphics/Rect;->top:I

    #@20
    .line 4791
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockRight:I

    #@22
    iput v0, p9, Landroid/graphics/Rect;->right:I

    #@24
    iput v0, p8, Landroid/graphics/Rect;->right:I

    #@26
    iput v0, p7, Landroid/graphics/Rect;->right:I

    #@28
    .line 4792
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockBottom:I

    #@2a
    iput v0, p9, Landroid/graphics/Rect;->bottom:I

    #@2c
    iput v0, p8, Landroid/graphics/Rect;->bottom:I

    #@2e
    iput v0, p7, Landroid/graphics/Rect;->bottom:I

    #@30
    .line 4823
    .end local p8
    :goto_30
    and-int/lit16 v0, p2, 0x100

    #@32
    if-nez v0, :cond_38

    #@34
    invoke-interface {p4}, Landroid/view/WindowManagerPolicy$WindowState;->getFrameLw()Landroid/graphics/Rect;

    #@37
    move-result-object p7

    #@38
    .end local p7
    :cond_38
    invoke-virtual {p6, p7}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@3b
    .line 4830
    return-void

    #@3c
    .line 4800
    .restart local p7
    .restart local p8
    :cond_3c
    const/16 v0, 0x10

    #@3e
    if-eq p3, v0, :cond_58

    #@40
    .line 4801
    invoke-interface {p4}, Landroid/view/WindowManagerPolicy$WindowState;->getDisplayFrameLw()Landroid/graphics/Rect;

    #@43
    move-result-object v0

    #@44
    invoke-virtual {p8, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@47
    .line 4817
    :cond_47
    :goto_47
    if-eqz p5, :cond_4d

    #@49
    invoke-interface {p4}, Landroid/view/WindowManagerPolicy$WindowState;->getDisplayFrameLw()Landroid/graphics/Rect;

    #@4c
    move-result-object p8

    #@4d
    .end local p8
    :cond_4d
    invoke-virtual {p7, p8}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@50
    .line 4818
    invoke-interface {p4}, Landroid/view/WindowManagerPolicy$WindowState;->getVisibleFrameLw()Landroid/graphics/Rect;

    #@53
    move-result-object v0

    #@54
    invoke-virtual {p9, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@57
    goto :goto_30

    #@58
    .line 4809
    .restart local p8
    :cond_58
    invoke-interface {p4}, Landroid/view/WindowManagerPolicy$WindowState;->getContentFrameLw()Landroid/graphics/Rect;

    #@5b
    move-result-object v0

    #@5c
    invoke-virtual {p8, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    #@5f
    .line 4810
    invoke-interface {p4}, Landroid/view/WindowManagerPolicy$WindowState;->getSurfaceLayer()I

    #@62
    move-result v0

    #@63
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDockLayer:I

    #@65
    if-ge v0, v1, :cond_47

    #@67
    .line 4811
    iget v0, p8, Landroid/graphics/Rect;->left:I

    #@69
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentLeft:I

    #@6b
    if-ge v0, v1, :cond_71

    #@6d
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentLeft:I

    #@6f
    iput v0, p8, Landroid/graphics/Rect;->left:I

    #@71
    .line 4812
    :cond_71
    iget v0, p8, Landroid/graphics/Rect;->top:I

    #@73
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentTop:I

    #@75
    if-ge v0, v1, :cond_7b

    #@77
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentTop:I

    #@79
    iput v0, p8, Landroid/graphics/Rect;->top:I

    #@7b
    .line 4813
    :cond_7b
    iget v0, p8, Landroid/graphics/Rect;->right:I

    #@7d
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentRight:I

    #@7f
    if-le v0, v1, :cond_85

    #@81
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentRight:I

    #@83
    iput v0, p8, Landroid/graphics/Rect;->right:I

    #@85
    .line 4814
    :cond_85
    iget v0, p8, Landroid/graphics/Rect;->bottom:I

    #@87
    iget v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentBottom:I

    #@89
    if-le v0, v1, :cond_47

    #@8b
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContentBottom:I

    #@8d
    iput v0, p8, Landroid/graphics/Rect;->bottom:I

    #@8f
    goto :goto_47
.end method

.method public setCurrentOrientationLw(I)V
    .registers 4
    .parameter "newOrientation"

    #@0
    .prologue
    .line 7765
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 7766
    :try_start_3
    iget v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurrentAppOrientation:I

    #@5
    if-eq p1, v0, :cond_c

    #@7
    .line 7767
    iput p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCurrentAppOrientation:I

    #@9
    .line 7768
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateOrientationListenerLp()V

    #@c
    .line 7770
    :cond_c
    monitor-exit v1

    #@d
    .line 7771
    return-void

    #@e
    .line 7770
    :catchall_e
    move-exception v0

    #@f
    monitor-exit v1
    :try_end_10
    .catchall {:try_start_3 .. :try_end_10} :catchall_e

    #@10
    throw v0
.end method

.method public setCurrentUserLw(I)V
    .registers 4
    .parameter "newUserId"

    #@0
    .prologue
    const/4 v1, 0x0

    #@1
    .line 7972
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@3
    if-eqz v0, :cond_a

    #@5
    .line 7973
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@7
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->setCurrentUser(I)V

    #@a
    .line 7975
    :cond_a
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@c
    if-eqz v0, :cond_13

    #@e
    .line 7977
    :try_start_e
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarService:Lcom/android/internal/statusbar/IStatusBarService;

    #@10
    invoke-interface {v0, p1}, Lcom/android/internal/statusbar/IStatusBarService;->setCurrentUser(I)V
    :try_end_13
    .catch Landroid/os/RemoteException; {:try_start_e .. :try_end_13} :catch_17

    #@13
    .line 7982
    :cond_13
    :goto_13
    invoke-virtual {p0, v1, v1}, Lcom/android/internal/policy/impl/PhoneWindowManager;->setLastInputMethodWindowLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManagerPolicy$WindowState;)V

    #@16
    .line 7983
    return-void

    #@17
    .line 7978
    :catch_17
    move-exception v0

    #@18
    goto :goto_13
.end method

.method setHdmiPlugged(Z)V
    .registers 8
    .parameter "plugged"

    #@0
    .prologue
    const/high16 v5, 0x800

    #@2
    const/4 v4, 0x1

    #@3
    .line 5750
    iget-boolean v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiPlugged:Z

    #@5
    if-eq v3, p1, :cond_46

    #@7
    .line 5751
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiPlugged:Z

    #@9
    .line 5752
    invoke-virtual {p0, v4, v4}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateRotation(ZZ)V

    #@c
    .line 5753
    new-instance v0, Landroid/content/Intent;

    #@e
    const-string v3, "android.intent.action.HDMI_PLUGGED"

    #@10
    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@13
    .line 5754
    .local v0, intent:Landroid/content/Intent;
    invoke-virtual {v0, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@16
    .line 5755
    const-string v3, "state"

    #@18
    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@1b
    .line 5756
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1d
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@1f
    invoke-virtual {v3, v0, v4}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@22
    .line 5757
    const-string v3, "ro.build.target_operator"

    #@24
    invoke-static {v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@27
    move-result-object v1

    #@28
    .line 5758
    .local v1, operator:Ljava/lang/String;
    const-string v3, "VZW"

    #@2a
    invoke-virtual {v1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    #@2d
    move-result v3

    #@2e
    if-eqz v3, :cond_46

    #@30
    .line 5759
    new-instance v2, Landroid/content/Intent;

    #@32
    const-string v3, "verizon.intent.action.HDMI_PLUG"

    #@34
    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    #@37
    .line 5760
    .local v2, vzw_intent:Landroid/content/Intent;
    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    #@3a
    .line 5761
    const-string v3, "state"

    #@3c
    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    #@3f
    .line 5762
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@41
    sget-object v4, Landroid/os/UserHandle;->ALL:Landroid/os/UserHandle;

    #@43
    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->sendStickyBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    #@46
    .line 5765
    .end local v0           #intent:Landroid/content/Intent;
    .end local v1           #operator:Ljava/lang/String;
    .end local v2           #vzw_intent:Landroid/content/Intent;
    :cond_46
    return-void
.end method

.method public setInitialDisplaySize(Landroid/view/Display;III)V
    .registers 24
    .parameter "display"
    .parameter "width"
    .parameter "height"
    .parameter "density"

    #@0
    .prologue
    .line 2270
    move-object/from16 v0, p1

    #@2
    move-object/from16 v1, p0

    #@4
    iput-object v0, v1, Lcom/android/internal/policy/impl/PhoneWindowManager;->mDisplay:Landroid/view/Display;

    #@6
    .line 2273
    move/from16 v0, p2

    #@8
    move/from16 v1, p3

    #@a
    if-le v0, v1, :cond_172

    #@c
    .line 2274
    move/from16 v7, p3

    #@e
    .line 2275
    .local v7, shortSize:I
    move/from16 v4, p2

    #@10
    .line 2276
    .local v4, longSize:I
    const/4 v9, 0x0

    #@11
    move-object/from16 v0, p0

    #@13
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@15
    .line 2277
    const/4 v9, 0x2

    #@16
    move-object/from16 v0, p0

    #@18
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSeascapeRotation:I

    #@1a
    .line 2278
    move-object/from16 v0, p0

    #@1c
    iget-object v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@1e
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@21
    move-result-object v9

    #@22
    const v10, 0x111001e

    #@25
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@28
    move-result v9

    #@29
    if-eqz v9, :cond_166

    #@2b
    .line 2280
    const/4 v9, 0x1

    #@2c
    move-object/from16 v0, p0

    #@2e
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@30
    .line 2281
    const/4 v9, 0x3

    #@31
    move-object/from16 v0, p0

    #@33
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUpsideDownRotation:I

    #@35
    .line 2301
    :goto_35
    move-object/from16 v0, p0

    #@37
    iget-object v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@39
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@3c
    move-result-object v9

    #@3d
    const v10, 0x105000c

    #@40
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@43
    move-result v9

    #@44
    move-object/from16 v0, p0

    #@46
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mStatusBarHeight:I

    #@48
    .line 2305
    move-object/from16 v0, p0

    #@4a
    iget-object v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarHeightForRotation:[I

    #@4c
    move-object/from16 v0, p0

    #@4e
    iget v10, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@50
    move-object/from16 v0, p0

    #@52
    iget-object v11, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarHeightForRotation:[I

    #@54
    move-object/from16 v0, p0

    #@56
    iget v12, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUpsideDownRotation:I

    #@58
    move-object/from16 v0, p0

    #@5a
    iget-object v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@5c
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@5f
    move-result-object v13

    #@60
    const v14, 0x105000d

    #@63
    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@66
    move-result v13

    #@67
    aput v13, v11, v12

    #@69
    aput v13, v9, v10

    #@6b
    .line 2309
    move-object/from16 v0, p0

    #@6d
    iget-object v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarHeightForRotation:[I

    #@6f
    move-object/from16 v0, p0

    #@71
    iget v10, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@73
    move-object/from16 v0, p0

    #@75
    iget-object v11, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarHeightForRotation:[I

    #@77
    move-object/from16 v0, p0

    #@79
    iget v12, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSeascapeRotation:I

    #@7b
    move-object/from16 v0, p0

    #@7d
    iget-object v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@7f
    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@82
    move-result-object v13

    #@83
    const v14, 0x105000e

    #@86
    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@89
    move-result v13

    #@8a
    aput v13, v11, v12

    #@8c
    aput v13, v9, v10

    #@8e
    .line 2315
    move-object/from16 v0, p0

    #@90
    iget-object v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarWidthForRotation:[I

    #@92
    move-object/from16 v0, p0

    #@94
    iget v10, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@96
    move-object/from16 v0, p0

    #@98
    iget-object v11, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarWidthForRotation:[I

    #@9a
    move-object/from16 v0, p0

    #@9c
    iget v12, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUpsideDownRotation:I

    #@9e
    move-object/from16 v0, p0

    #@a0
    iget-object v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarWidthForRotation:[I

    #@a2
    move-object/from16 v0, p0

    #@a4
    iget v14, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@a6
    move-object/from16 v0, p0

    #@a8
    iget-object v15, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarWidthForRotation:[I

    #@aa
    move-object/from16 v0, p0

    #@ac
    iget v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSeascapeRotation:I

    #@ae
    move/from16 v16, v0

    #@b0
    move-object/from16 v0, p0

    #@b2
    iget-object v0, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@b4
    move-object/from16 v17, v0

    #@b6
    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@b9
    move-result-object v17

    #@ba
    const v18, 0x105000f

    #@bd
    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    #@c0
    move-result v17

    #@c1
    aput v17, v15, v16

    #@c3
    aput v17, v13, v14

    #@c5
    aput v17, v11, v12

    #@c7
    aput v17, v9, v10

    #@c9
    .line 2323
    mul-int/lit16 v9, v7, 0xa0

    #@cb
    div-int v8, v9, p4

    #@cd
    .line 2325
    .local v8, shortSizeDp:I
    const/16 v9, 0x258

    #@cf
    if-ge v8, v9, :cond_1a9

    #@d1
    .line 2327
    const/4 v9, 0x0

    #@d2
    move-object/from16 v0, p0

    #@d4
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSystemNavBar:Z

    #@d6
    .line 2328
    const/4 v9, 0x1

    #@d7
    move-object/from16 v0, p0

    #@d9
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarCanMove:Z

    #@db
    .line 2335
    :cond_db
    :goto_db
    move-object/from16 v0, p0

    #@dd
    iget-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSystemNavBar:Z

    #@df
    if-nez v9, :cond_1c8

    #@e1
    .line 2336
    move-object/from16 v0, p0

    #@e3
    iget-object v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@e5
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@e8
    move-result-object v9

    #@e9
    const v10, 0x111003c

    #@ec
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@ef
    move-result v9

    #@f0
    move-object/from16 v0, p0

    #@f2
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasNavigationBar:Z

    #@f4
    .line 2341
    const-string v9, "qemu.hw.mainkeys"

    #@f6
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@f9
    move-result-object v6

    #@fa
    .line 2342
    .local v6, navBarOverride:Ljava/lang/String;
    const-string v9, ""

    #@fc
    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@ff
    move-result v9

    #@100
    if-eqz v9, :cond_108

    #@102
    .line 2343
    const-string v9, "ro.hw.nav_keys"

    #@104
    invoke-static {v9}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@107
    move-result-object v6

    #@108
    .line 2345
    :cond_108
    const-string v9, ""

    #@10a
    invoke-virtual {v9, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@10d
    move-result v9

    #@10e
    if-nez v9, :cond_11d

    #@110
    .line 2346
    const-string v9, "1"

    #@112
    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@115
    move-result v9

    #@116
    if-eqz v9, :cond_1b9

    #@118
    const/4 v9, 0x0

    #@119
    move-object/from16 v0, p0

    #@11b
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasNavigationBar:Z

    #@11d
    .line 2353
    .end local v6           #navBarOverride:Ljava/lang/String;
    :cond_11d
    :goto_11d
    move-object/from16 v0, p0

    #@11f
    iget-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSystemNavBar:Z

    #@121
    if-eqz v9, :cond_1d2

    #@123
    .line 2357
    mul-int/lit16 v9, v4, 0xa0

    #@125
    div-int v5, v9, p4

    #@127
    .line 2358
    .local v5, longSizeDp:I
    move-object/from16 v0, p0

    #@129
    iget-object v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarHeightForRotation:[I

    #@12b
    move-object/from16 v0, p0

    #@12d
    iget v10, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@12f
    aget v9, v9, v10

    #@131
    mul-int/lit16 v9, v9, 0xa0

    #@133
    div-int v3, v9, p4

    #@135
    .line 2360
    .local v3, barHeightDp:I
    sub-int v9, v8, v3

    #@137
    mul-int/lit8 v9, v9, 0x10

    #@139
    div-int v2, v9, v5

    #@13b
    .line 2365
    .local v2, aspect:I
    const/16 v9, 0x9

    #@13d
    if-ge v2, v9, :cond_1cf

    #@13f
    const/4 v9, 0x1

    #@140
    :goto_140
    move-object/from16 v0, p0

    #@142
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCanHideNavigationBar:Z

    #@144
    .line 2376
    .end local v2           #aspect:I
    .end local v3           #barHeightDp:I
    .end local v5           #longSizeDp:I
    :goto_144
    const-string v9, "portrait"

    #@146
    const-string v10, "persist.demo.hdmirotation"

    #@148
    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@14b
    move-result-object v10

    #@14c
    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@14f
    move-result v9

    #@150
    if-eqz v9, :cond_1e6

    #@152
    .line 2377
    move-object/from16 v0, p0

    #@154
    iget v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@156
    move-object/from16 v0, p0

    #@158
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiRotation:I

    #@15a
    .line 2381
    :goto_15a
    const-string v9, "persist.demo.hdmirotationlock"

    #@15c
    const/4 v10, 0x1

    #@15d
    invoke-static {v9, v10}, Landroid/os/SystemProperties;->getBoolean(Ljava/lang/String;Z)Z

    #@160
    move-result v9

    #@161
    move-object/from16 v0, p0

    #@163
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiRotationLock:Z

    #@165
    .line 2382
    return-void

    #@166
    .line 2283
    .end local v8           #shortSizeDp:I
    :cond_166
    const/4 v9, 0x3

    #@167
    move-object/from16 v0, p0

    #@169
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@16b
    .line 2284
    const/4 v9, 0x1

    #@16c
    move-object/from16 v0, p0

    #@16e
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUpsideDownRotation:I

    #@170
    goto/16 :goto_35

    #@172
    .line 2287
    .end local v4           #longSize:I
    .end local v7           #shortSize:I
    :cond_172
    move/from16 v7, p2

    #@174
    .line 2288
    .restart local v7       #shortSize:I
    move/from16 v4, p3

    #@176
    .line 2289
    .restart local v4       #longSize:I
    const/4 v9, 0x0

    #@177
    move-object/from16 v0, p0

    #@179
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPortraitRotation:I

    #@17b
    .line 2290
    const/4 v9, 0x2

    #@17c
    move-object/from16 v0, p0

    #@17e
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUpsideDownRotation:I

    #@180
    .line 2291
    move-object/from16 v0, p0

    #@182
    iget-object v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@184
    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    #@187
    move-result-object v9

    #@188
    const v10, 0x111001e

    #@18b
    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getBoolean(I)Z

    #@18e
    move-result v9

    #@18f
    if-eqz v9, :cond_19d

    #@191
    .line 2293
    const/4 v9, 0x3

    #@192
    move-object/from16 v0, p0

    #@194
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@196
    .line 2294
    const/4 v9, 0x1

    #@197
    move-object/from16 v0, p0

    #@199
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSeascapeRotation:I

    #@19b
    goto/16 :goto_35

    #@19d
    .line 2296
    :cond_19d
    const/4 v9, 0x1

    #@19e
    move-object/from16 v0, p0

    #@1a0
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@1a2
    .line 2297
    const/4 v9, 0x3

    #@1a3
    move-object/from16 v0, p0

    #@1a5
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSeascapeRotation:I

    #@1a7
    goto/16 :goto_35

    #@1a9
    .line 2329
    .restart local v8       #shortSizeDp:I
    :cond_1a9
    const/16 v9, 0x2d0

    #@1ab
    if-ge v8, v9, :cond_db

    #@1ad
    .line 2331
    const/4 v9, 0x0

    #@1ae
    move-object/from16 v0, p0

    #@1b0
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSystemNavBar:Z

    #@1b2
    .line 2332
    const/4 v9, 0x0

    #@1b3
    move-object/from16 v0, p0

    #@1b5
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mNavigationBarCanMove:Z

    #@1b7
    goto/16 :goto_db

    #@1b9
    .line 2347
    .restart local v6       #navBarOverride:Ljava/lang/String;
    :cond_1b9
    const-string v9, "0"

    #@1bb
    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@1be
    move-result v9

    #@1bf
    if-eqz v9, :cond_11d

    #@1c1
    const/4 v9, 0x1

    #@1c2
    move-object/from16 v0, p0

    #@1c4
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasNavigationBar:Z

    #@1c6
    goto/16 :goto_11d

    #@1c8
    .line 2350
    .end local v6           #navBarOverride:Ljava/lang/String;
    :cond_1c8
    const/4 v9, 0x0

    #@1c9
    move-object/from16 v0, p0

    #@1cb
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasNavigationBar:Z

    #@1cd
    goto/16 :goto_11d

    #@1cf
    .line 2365
    .restart local v2       #aspect:I
    .restart local v3       #barHeightDp:I
    .restart local v5       #longSizeDp:I
    :cond_1cf
    const/4 v9, 0x0

    #@1d0
    goto/16 :goto_140

    #@1d2
    .line 2366
    .end local v2           #aspect:I
    .end local v3           #barHeightDp:I
    .end local v5           #longSizeDp:I
    :cond_1d2
    move-object/from16 v0, p0

    #@1d4
    iget-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasNavigationBar:Z

    #@1d6
    if-eqz v9, :cond_1df

    #@1d8
    .line 2369
    const/4 v9, 0x1

    #@1d9
    move-object/from16 v0, p0

    #@1db
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCanHideNavigationBar:Z

    #@1dd
    goto/16 :goto_144

    #@1df
    .line 2371
    :cond_1df
    const/4 v9, 0x0

    #@1e0
    move-object/from16 v0, p0

    #@1e2
    iput-boolean v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mCanHideNavigationBar:Z

    #@1e4
    goto/16 :goto_144

    #@1e6
    .line 2379
    :cond_1e6
    move-object/from16 v0, p0

    #@1e8
    iget v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLandscapeRotation:I

    #@1ea
    move-object/from16 v0, p0

    #@1ec
    iput v9, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHdmiRotation:I

    #@1ee
    goto/16 :goto_15a
.end method

.method public setLastInputMethodWindowLw(Landroid/view/WindowManagerPolicy$WindowState;Landroid/view/WindowManagerPolicy$WindowState;)V
    .registers 3
    .parameter "ime"
    .parameter "target"

    #@0
    .prologue
    .line 7953
    iput-object p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@2
    .line 7954
    iput-object p2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLastInputMethodTargetWindow:Landroid/view/WindowManagerPolicy$WindowState;

    #@4
    .line 7955
    return-void
.end method

.method public setRotationLw(I)V
    .registers 3
    .parameter "rotation"

    #@0
    .prologue
    .line 7398
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationListener:Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;

    #@2
    invoke-virtual {v0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;->setCurrentRotation(I)V

    #@5
    .line 7399
    return-void
.end method

.method public setSafeMode(Z)V
    .registers 5
    .parameter "safeMode"

    #@0
    .prologue
    .line 7444
    iput-boolean p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSafeMode:Z

    #@2
    .line 7445
    const/4 v1, 0x0

    #@3
    if-eqz p1, :cond_c

    #@5
    const/16 v0, 0x2711

    #@7
    :goto_7
    const/4 v2, 0x1

    #@8
    invoke-virtual {p0, v1, v0, v2}, Lcom/android/internal/policy/impl/PhoneWindowManager;->performHapticFeedbackLw(Landroid/view/WindowManagerPolicy$WindowState;IZ)Z

    #@b
    .line 7448
    return-void

    #@c
    .line 7445
    :cond_c
    const/16 v0, 0x2710

    #@e
    goto :goto_7
.end method

.method public setSystemBarType(I)V
    .registers 2
    .parameter "type"

    #@0
    .prologue
    .line 8363
    iput p1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemBarType:I

    #@2
    .line 8364
    return-void
.end method

.method public setUserRotationMode(II)V
    .registers 8
    .parameter "mode"
    .parameter "rot"

    #@0
    .prologue
    const/4 v4, 0x2

    #@1
    const/4 v2, 0x1

    #@2
    const/4 v3, -0x2

    #@3
    .line 7412
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@5
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@8
    move-result-object v0

    #@9
    .line 7415
    .local v0, res:Landroid/content/ContentResolver;
    if-ne p1, v2, :cond_17

    #@b
    .line 7416
    const-string v1, "user_rotation"

    #@d
    invoke-static {v0, v1, p2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@10
    .line 7420
    const-string v1, "accelerometer_rotation"

    #@12
    const/4 v2, 0x0

    #@13
    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@16
    .line 7441
    :goto_16
    return-void

    #@17
    .line 7425
    :cond_17
    if-ne p1, v4, :cond_24

    #@19
    .line 7426
    const-string v1, "user_rotation"

    #@1b
    invoke-static {v0, v1, p2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@1e
    .line 7430
    const-string v1, "accelerometer_rotation"

    #@20
    invoke-static {v0, v1, v4, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@23
    goto :goto_16

    #@24
    .line 7436
    :cond_24
    const-string v1, "accelerometer_rotation"

    #@26
    invoke-static {v0, v1, v2, v3}, Landroid/provider/Settings$System;->putIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)Z

    #@29
    goto :goto_16
.end method

.method public showAssistant()V
    .registers 2

    #@0
    .prologue
    .line 7987
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->showAssistant()V

    #@5
    .line 7988
    return-void
.end method

.method public showBootMessage(Ljava/lang/CharSequence;Z)V
    .registers 5
    .parameter "msg"
    .parameter "always"

    #@0
    .prologue
    .line 7505
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadless:Z

    #@2
    if-eqz v0, :cond_5

    #@4
    .line 7550
    :goto_4
    return-void

    #@5
    .line 7506
    :cond_5
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@7
    new-instance v1, Lcom/android/internal/policy/impl/PhoneWindowManager$38;

    #@9
    invoke-direct {v1, p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager$38;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;Ljava/lang/CharSequence;)V

    #@c
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@f
    goto :goto_4
.end method

.method showGlobalActionsDialog()V
    .registers 5

    #@0
    .prologue
    .line 1924
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGlobalActions:Lcom/android/internal/policy/impl/GlobalActions;

    #@2
    if-nez v1, :cond_f

    #@4
    .line 1925
    new-instance v1, Lcom/android/internal/policy/impl/GlobalActions;

    #@6
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@8
    iget-object v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManagerFuncs:Landroid/view/WindowManagerPolicy$WindowManagerFuncs;

    #@a
    invoke-direct {v1, v2, v3}, Lcom/android/internal/policy/impl/GlobalActions;-><init>(Landroid/content/Context;Landroid/view/WindowManagerPolicy$WindowManagerFuncs;)V

    #@d
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGlobalActions:Lcom/android/internal/policy/impl/GlobalActions;

    #@f
    .line 1930
    :cond_f
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->keyguardIsShowingTq()Z

    #@12
    move-result v1

    #@13
    if-nez v1, :cond_1b

    #@15
    invoke-static {}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isFindMissedPhoneRegister()Z

    #@18
    move-result v1

    #@19
    if-eqz v1, :cond_2d

    #@1b
    :cond_1b
    const/4 v0, 0x1

    #@1c
    .line 1932
    .local v0, keyguardShowing:Z
    :goto_1c
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGlobalActions:Lcom/android/internal/policy/impl/GlobalActions;

    #@1e
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->isDeviceProvisioned()Z

    #@21
    move-result v2

    #@22
    invoke-virtual {v1, v0, v2}, Lcom/android/internal/policy/impl/GlobalActions;->showDialog(ZZ)V

    #@25
    .line 1933
    if-eqz v0, :cond_2c

    #@27
    .line 1936
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@29
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->userActivity()V

    #@2c
    .line 1938
    :cond_2c
    return-void

    #@2d
    .line 1930
    .end local v0           #keyguardShowing:Z
    :cond_2d
    const/4 v0, 0x0

    #@2e
    goto :goto_1c
.end method

.method showGoHomeDialog()V
    .registers 3

    #@0
    .prologue
    .line 8221
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v1, Lcom/android/internal/policy/impl/PhoneWindowManager$42;

    #@4
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$42;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 8265
    return-void
.end method

.method showGoHomeDialog2()V
    .registers 4

    #@0
    .prologue
    .line 8267
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog2:Landroid/app/AlertDialog;

    #@2
    if-nez v1, :cond_42

    #@4
    .line 8268
    new-instance v0, Landroid/app/AlertDialog$Builder;

    #@6
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@8
    const/4 v2, 0x2

    #@9
    invoke-direct {v0, v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    #@c
    .line 8270
    .local v0, ab:Landroid/app/AlertDialog$Builder;
    const v1, 0x2090263

    #@f
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    #@12
    .line 8271
    const v1, 0x2090266

    #@15
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    #@18
    .line 8272
    const v1, 0x1080027

    #@1b
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    #@1e
    .line 8273
    const v1, 0x104000a

    #@21
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindowManager$43;

    #@23
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$43;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@26
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    #@29
    .line 8325
    new-instance v1, Lcom/android/internal/policy/impl/PhoneWindowManager$44;

    #@2b
    invoke-direct {v1, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$44;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@2e
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    #@31
    .line 8331
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    #@34
    move-result-object v1

    #@35
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog2:Landroid/app/AlertDialog;

    #@37
    .line 8332
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog2:Landroid/app/AlertDialog;

    #@39
    invoke-virtual {v1}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    #@3c
    move-result-object v1

    #@3d
    const/16 v2, 0x7d8

    #@3f
    invoke-virtual {v1, v2}, Landroid/view/Window;->setType(I)V

    #@42
    .line 8334
    .end local v0           #ab:Landroid/app/AlertDialog$Builder;
    :cond_42
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mGoHomeDialog2:Landroid/app/AlertDialog;

    #@44
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    #@47
    .line 8335
    return-void
.end method

.method showOrHideRecentAppsDialog(I)V
    .registers 4
    .parameter "behavior"

    #@0
    .prologue
    .line 2002
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@2
    new-instance v1, Lcom/android/internal/policy/impl/PhoneWindowManager$15;

    #@4
    invoke-direct {v1, p0, p1}, Lcom/android/internal/policy/impl/PhoneWindowManager$15;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;I)V

    #@7
    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@a
    .line 2041
    return-void
.end method

.method showRestartActionDialog(I)V
    .registers 6
    .parameter "restartCount"

    #@0
    .prologue
    .line 1899
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestartAction:Lcom/android/internal/policy/impl/RestartAction;

    #@2
    if-nez v1, :cond_d

    #@4
    .line 1900
    new-instance v1, Lcom/android/internal/policy/impl/RestartAction;

    #@6
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@8
    invoke-direct {v1, v2, p1}, Lcom/android/internal/policy/impl/RestartAction;-><init>(Landroid/content/Context;I)V

    #@b
    iput-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestartAction:Lcom/android/internal/policy/impl/RestartAction;

    #@d
    .line 1903
    :cond_d
    invoke-direct {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->keyguardIsShowingTq()Z

    #@10
    move-result v0

    #@11
    .line 1905
    .local v0, keyguardShowing:Z
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestartAction:Lcom/android/internal/policy/impl/RestartAction;

    #@13
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/RestartAction;->isShowing()Z

    #@16
    move-result v1

    #@17
    if-eqz v1, :cond_21

    #@19
    .line 1906
    const-string v1, "WindowManager"

    #@1b
    const-string v2, "RestartActionDialog is showing. Ignore showRestartActionDialog"

    #@1d
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@20
    .line 1915
    :cond_20
    :goto_20
    return-void

    #@21
    .line 1910
    :cond_21
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mRestartAction:Lcom/android/internal/policy/impl/RestartAction;

    #@23
    invoke-virtual {v1, v0}, Lcom/android/internal/policy/impl/RestartAction;->showDialog(Z)V

    #@26
    .line 1912
    if-eqz v0, :cond_20

    #@28
    .line 1913
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mBroadcastWakeLock:Landroid/os/PowerManager$WakeLock;

    #@2a
    const-wide/16 v2, 0xbb8

    #@2c
    invoke-virtual {v1, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    #@2f
    goto :goto_20
.end method

.method public showSKTLocked(ZLjava/lang/String;Ljava/lang/String;)V
    .registers 7
    .parameter "enableUserUnlock"
    .parameter "strPassword"
    .parameter "userMsg"

    #@0
    .prologue
    .line 7882
    const-string v0, "WindowManager"

    #@2
    new-instance v1, Ljava/lang/StringBuilder;

    #@4
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@7
    const-string v2, "[SKT Lock&Wipe] ShowSKTLocked("

    #@9
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@c
    move-result-object v1

    #@d
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    #@10
    move-result-object v1

    #@11
    const-string v2, ", "

    #@13
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@16
    move-result-object v1

    #@17
    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@1a
    move-result-object v1

    #@1b
    const-string v2, ", "

    #@1d
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@20
    move-result-object v1

    #@21
    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@24
    move-result-object v1

    #@25
    const-string v2, ")"

    #@27
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@2a
    move-result-object v1

    #@2b
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@2e
    move-result-object v1

    #@2f
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    #@32
    .line 7883
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@34
    invoke-virtual {v0, p1, p2, p3}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->showSKTLocked(ZLjava/lang/String;Ljava/lang/String;)V

    #@37
    .line 7884
    return-void
.end method

.method startDockOrHome()V
    .registers 7

    #@0
    .prologue
    .line 7699
    const-string v2, ""

    #@2
    .line 7700
    .local v2, homeprop:Ljava/lang/String;
    const-string v4, "service.plushome.currenthome"

    #@4
    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@7
    move-result-object v2

    #@8
    .line 7702
    const-string v4, "kids"

    #@a
    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@d
    move-result v4

    #@e
    if-eqz v4, :cond_30

    #@10
    .line 7704
    :try_start_10
    new-instance v3, Landroid/content/Intent;

    #@12
    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    #@15
    .line 7705
    .local v3, intent:Landroid/content/Intent;
    const-string v4, "com.lge.launcher2"

    #@17
    const-string v5, "com.lge.launcher2.plushome.kidshome.LGKidsHome"

    #@19
    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    #@1c
    .line 7706
    const/high16 v4, 0x1400

    #@1e
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    #@21
    .line 7707
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@23
    invoke-virtual {v4, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_26
    .catch Landroid/content/ActivityNotFoundException; {:try_start_10 .. :try_end_26} :catch_27

    #@26
    .line 7723
    .end local v3           #intent:Landroid/content/Intent;
    :goto_26
    return-void

    #@27
    .line 7708
    :catch_27
    move-exception v1

    #@28
    .line 7709
    .local v1, e:Landroid/content/ActivityNotFoundException;
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@2a
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeIntent:Landroid/content/Intent;

    #@2c
    invoke-virtual {v4, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@2f
    goto :goto_26

    #@30
    .line 7713
    .end local v1           #e:Landroid/content/ActivityNotFoundException;
    :cond_30
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->createHomeDockIntent()Landroid/content/Intent;

    #@33
    move-result-object v0

    #@34
    .line 7714
    .local v0, dock:Landroid/content/Intent;
    if-eqz v0, :cond_3d

    #@36
    .line 7716
    :try_start_36
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@38
    invoke-virtual {v4, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_3b
    .catch Landroid/content/ActivityNotFoundException; {:try_start_36 .. :try_end_3b} :catch_3c

    #@3b
    goto :goto_26

    #@3c
    .line 7718
    :catch_3c
    move-exception v4

    #@3d
    .line 7721
    :cond_3d
    iget-object v4, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@3f
    iget-object v5, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHomeIntent:Landroid/content/Intent;

    #@41
    invoke-virtual {v4, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    #@44
    goto :goto_26
.end method

.method public subWindowTypeToLayerLw(I)I
    .registers 5
    .parameter "type"

    #@0
    .prologue
    .line 2846
    packed-switch p1, :pswitch_data_26

    #@3
    .line 2857
    const-string v0, "WindowManager"

    #@5
    new-instance v1, Ljava/lang/StringBuilder;

    #@7
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    #@a
    const-string v2, "Unknown sub-window type: "

    #@c
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@f
    move-result-object v1

    #@10
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@13
    move-result-object v1

    #@14
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@17
    move-result-object v1

    #@18
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@1b
    .line 2858
    const/4 v0, 0x0

    #@1c
    :goto_1c
    return v0

    #@1d
    .line 2849
    :pswitch_1d
    const/4 v0, 0x1

    #@1e
    goto :goto_1c

    #@1f
    .line 2851
    :pswitch_1f
    const/4 v0, -0x2

    #@20
    goto :goto_1c

    #@21
    .line 2853
    :pswitch_21
    const/4 v0, -0x1

    #@22
    goto :goto_1c

    #@23
    .line 2855
    :pswitch_23
    const/4 v0, 0x2

    #@24
    goto :goto_1c

    #@25
    .line 2846
    nop

    #@26
    :pswitch_data_26
    .packed-switch 0x3e8
        :pswitch_1d
        :pswitch_1f
        :pswitch_23
        :pswitch_1d
        :pswitch_21
    .end packed-switch
.end method

.method public systemBooted()V
    .registers 4

    #@0
    .prologue
    .line 7492
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@2
    monitor-enter v1

    #@3
    .line 7493
    const/4 v0, 0x1

    #@4
    :try_start_4
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemBooted:Z

    #@6
    .line 7494
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetIntent:Z

    #@8
    if-eqz v0, :cond_16

    #@a
    .line 7495
    const-string v0, "WindowManager"

    #@c
    const-string v2, "send headset intent after system booted"

    #@e
    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@11
    .line 7496
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHeadsetName:Ljava/lang/String;

    #@13
    invoke-direct {p0, v0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->sendIntents(Ljava/lang/String;)V

    #@16
    .line 7498
    :cond_16
    monitor-exit v1

    #@17
    .line 7499
    return-void

    #@18
    .line 7498
    :catchall_18
    move-exception v0

    #@19
    monitor-exit v1
    :try_end_1a
    .catchall {:try_start_4 .. :try_end_1a} :catchall_18

    #@1a
    throw v0
.end method

.method public systemReady()V
    .registers 4

    #@0
    .prologue
    .line 7464
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@2
    if-eqz v0, :cond_9

    #@4
    .line 7466
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mKeyguardMediator:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;

    #@6
    invoke-virtual {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator;->onSystemReady()V

    #@9
    .line 7468
    :cond_9
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@b
    monitor-enter v1

    #@c
    .line 7469
    :try_start_c
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateOrientationListenerLp()V

    #@f
    .line 7470
    const/4 v0, 0x1

    #@10
    iput-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemReady:Z

    #@12
    .line 7471
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@14
    new-instance v2, Lcom/android/internal/policy/impl/PhoneWindowManager$37;

    #@16
    invoke-direct {v2, p0}, Lcom/android/internal/policy/impl/PhoneWindowManager$37;-><init>(Lcom/android/internal/policy/impl/PhoneWindowManager;)V

    #@19
    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    #@1c
    .line 7483
    monitor-exit v1
    :try_end_1d
    .catchall {:try_start_c .. :try_end_1d} :catchall_35

    #@1d
    .line 7485
    new-instance v0, Landroid/hardware/SystemSensorManager;

    #@1f
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@21
    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    #@24
    move-result-object v1

    #@25
    invoke-direct {v0, v1}, Landroid/hardware/SystemSensorManager;-><init>(Landroid/os/Looper;)V

    #@28
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSensorManager:Landroid/hardware/SensorManager;

    #@2a
    .line 7486
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSensorManager:Landroid/hardware/SensorManager;

    #@2c
    const/16 v1, 0x8

    #@2e
    invoke-virtual {v0, v1}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    #@31
    move-result-object v0

    #@32
    iput-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mProximitySensor:Landroid/hardware/Sensor;

    #@34
    .line 7488
    return-void

    #@35
    .line 7483
    :catchall_35
    move-exception v0

    #@36
    :try_start_36
    monitor-exit v1
    :try_end_37
    .catchall {:try_start_36 .. :try_end_37} :catchall_35

    #@37
    throw v0
.end method

.method updateOrientationListenerLp()V
    .registers 3

    #@0
    .prologue
    .line 1084
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationListener:Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;

    #@2
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;->canDetectOrientation()Z

    #@5
    move-result v1

    #@6
    if-nez v1, :cond_9

    #@8
    .line 1111
    :cond_8
    :goto_8
    return-void

    #@9
    .line 1093
    :cond_9
    const/4 v0, 0x1

    #@a
    .line 1094
    .local v0, disable:Z
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenOnEarly:Z

    #@c
    if-eqz v1, :cond_21

    #@e
    .line 1095
    invoke-virtual {p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->needSensorRunningLp()Z

    #@11
    move-result v1

    #@12
    if-eqz v1, :cond_21

    #@14
    .line 1096
    const/4 v0, 0x0

    #@15
    .line 1098
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationSensorEnabled:Z

    #@17
    if-nez v1, :cond_21

    #@19
    .line 1099
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationListener:Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;

    #@1b
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;->enable()V

    #@1e
    .line 1101
    const/4 v1, 0x1

    #@1f
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationSensorEnabled:Z

    #@21
    .line 1106
    :cond_21
    if-eqz v0, :cond_8

    #@23
    iget-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationSensorEnabled:Z

    #@25
    if-eqz v1, :cond_8

    #@27
    .line 1107
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationListener:Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;

    #@29
    invoke-virtual {v1}, Lcom/android/internal/policy/impl/PhoneWindowManager$MyOrientationListener;->disable()V

    #@2c
    .line 1109
    const/4 v1, 0x0

    #@2d
    iput-boolean v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mOrientationSensorEnabled:Z

    #@2f
    goto :goto_8
.end method

.method updateRotation(Z)V
    .registers 4
    .parameter "alwaysSendConfiguration"

    #@0
    .prologue
    .line 7652
    :try_start_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManager:Landroid/view/IWindowManager;

    #@2
    const/4 v1, 0x0

    #@3
    invoke-interface {v0, p1, v1}, Landroid/view/IWindowManager;->updateRotation(ZZ)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_6} :catch_7

    #@6
    .line 7656
    :goto_6
    return-void

    #@7
    .line 7653
    :catch_7
    move-exception v0

    #@8
    goto :goto_6
.end method

.method updateRotation(ZZ)V
    .registers 4
    .parameter "alwaysSendConfiguration"
    .parameter "forceRelayout"

    #@0
    .prologue
    .line 7661
    :try_start_0
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mWindowManager:Landroid/view/IWindowManager;

    #@2
    invoke-interface {v0, p1, p2}, Landroid/view/IWindowManager;->updateRotation(ZZ)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_5} :catch_6

    #@5
    .line 7665
    :goto_5
    return-void

    #@6
    .line 7662
    :catch_6
    move-exception v0

    #@7
    goto :goto_5
.end method

.method public updateSettings()V
    .registers 18

    #@0
    .prologue
    .line 2385
    move-object/from16 v0, p0

    #@2
    iget-object v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mContext:Landroid/content/Context;

    #@4
    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    #@7
    move-result-object v7

    #@8
    .line 2386
    .local v7, resolver:Landroid/content/ContentResolver;
    const/4 v10, 0x0

    #@9
    .line 2387
    .local v10, updateRotation:Z
    move-object/from16 v0, p0

    #@b
    iget-object v14, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLock:Ljava/lang/Object;

    #@d
    monitor-enter v14

    #@e
    .line 2388
    :try_start_e
    const-string v13, "end_button_behavior"

    #@10
    const/4 v15, 0x2

    #@11
    const/16 v16, -0x2

    #@13
    move/from16 v0, v16

    #@15
    invoke-static {v7, v13, v15, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@18
    move-result v13

    #@19
    move-object/from16 v0, p0

    #@1b
    iput v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEndcallBehavior:I

    #@1d
    .line 2392
    const-string v13, "incall_power_button_behavior"

    #@1f
    const/4 v15, 0x1

    #@20
    const/16 v16, -0x2

    #@22
    move/from16 v0, v16

    #@24
    invoke-static {v7, v13, v15, v0}, Landroid/provider/Settings$Secure;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@27
    move-result v13

    #@28
    move-object/from16 v0, p0

    #@2a
    iput v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mIncallPowerBehavior:I

    #@2c
    .line 2398
    const-string v13, "user_rotation"

    #@2e
    const/4 v15, 0x0

    #@2f
    const/16 v16, -0x2

    #@31
    move/from16 v0, v16

    #@33
    invoke-static {v7, v13, v15, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@36
    move-result v11

    #@37
    .line 2401
    .local v11, userRotation:I
    move-object/from16 v0, p0

    #@39
    iget v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotation:I

    #@3b
    if-eq v13, v11, :cond_42

    #@3d
    .line 2402
    move-object/from16 v0, p0

    #@3f
    iput v11, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotation:I

    #@41
    .line 2403
    const/4 v10, 0x1

    #@42
    .line 2407
    :cond_42
    const-string v13, "accelerometer_rotation"

    #@44
    const/4 v15, 0x0

    #@45
    const/16 v16, -0x2

    #@47
    move/from16 v0, v16

    #@49
    invoke-static {v7, v13, v15, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@4c
    move-result v1

    #@4d
    .line 2410
    .local v1, accelerometerDefault:I
    if-eqz v1, :cond_187

    #@4f
    const/4 v12, 0x0

    #@50
    .line 2414
    .local v12, userRotationMode:I
    :goto_50
    const/4 v13, 0x2

    #@51
    if-ne v1, v13, :cond_54

    #@53
    .line 2415
    const/4 v12, 0x2

    #@54
    .line 2423
    :cond_54
    move-object/from16 v0, p0

    #@56
    iget v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotationMode:I

    #@58
    if-eq v13, v12, :cond_62

    #@5a
    .line 2424
    move-object/from16 v0, p0

    #@5c
    iput v12, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mUserRotationMode:I

    #@5e
    .line 2425
    const/4 v10, 0x1

    #@5f
    .line 2426
    invoke-virtual/range {p0 .. p0}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateOrientationListenerLp()V

    #@62
    .line 2429
    :cond_62
    move-object/from16 v0, p0

    #@64
    iget-boolean v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSystemReady:Z

    #@66
    if-eqz v13, :cond_117

    #@68
    .line 2430
    const-string v13, "pointer_location"

    #@6a
    const/4 v15, 0x0

    #@6b
    const/16 v16, -0x2

    #@6d
    move/from16 v0, v16

    #@6f
    invoke-static {v7, v13, v15, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@72
    move-result v6

    #@73
    .line 2432
    .local v6, pointerLocation:I
    move-object/from16 v0, p0

    #@75
    iget v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationMode:I

    #@77
    if-eq v13, v6, :cond_87

    #@79
    .line 2433
    move-object/from16 v0, p0

    #@7b
    iput v6, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mPointerLocationMode:I

    #@7d
    .line 2434
    move-object/from16 v0, p0

    #@7f
    iget-object v15, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@81
    if-eqz v6, :cond_18a

    #@83
    const/4 v13, 0x1

    #@84
    :goto_84
    invoke-virtual {v15, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@87
    .line 2439
    :cond_87
    move-object/from16 v0, p0

    #@89
    iget-boolean v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->CAPP_TOUCH_PALM_COVER:Z

    #@8b
    if-eqz v13, :cond_b5

    #@8d
    const-string v13, "1"

    #@8f
    const-string v15, "ro.factorytest"

    #@91
    const-string v16, "0"

    #@93
    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@96
    move-result-object v15

    #@97
    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@9a
    move-result v13

    #@9b
    if-nez v13, :cond_b5

    #@9d
    .line 2440
    const-string v13, "hide_display"

    #@9f
    const/4 v15, 0x0

    #@a0
    invoke-static {v7, v13, v15}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@a3
    move-result v4

    #@a4
    .line 2442
    .local v4, hideDisplay:I
    move-object/from16 v0, p0

    #@a6
    iget v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHideDisplayMode:I

    #@a8
    if-eq v13, v4, :cond_b5

    #@aa
    .line 2443
    move-object/from16 v0, p0

    #@ac
    iget-object v15, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@ae
    if-eqz v4, :cond_18d

    #@b0
    const/16 v13, 0x65

    #@b2
    :goto_b2
    invoke-virtual {v15, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@b5
    .line 2450
    .end local v4           #hideDisplay:I
    :cond_b5
    move-object/from16 v0, p0

    #@b7
    iget-boolean v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->CAPP_TOUCH_PALM_SWIPE:Z

    #@b9
    if-eqz v13, :cond_e3

    #@bb
    const-string v13, "1"

    #@bd
    const-string v15, "ro.factorytest"

    #@bf
    const-string v16, "0"

    #@c1
    invoke-static/range {v15 .. v16}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    #@c4
    move-result-object v15

    #@c5
    invoke-virtual {v13, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@c8
    move-result v13

    #@c9
    if-nez v13, :cond_e3

    #@cb
    .line 2451
    const-string v13, "take_screenshot"

    #@cd
    const/4 v15, 0x0

    #@ce
    invoke-static {v7, v13, v15}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@d1
    move-result v9

    #@d2
    .line 2453
    .local v9, takeScreenshot:I
    move-object/from16 v0, p0

    #@d4
    iget v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTakeScreenshotMode:I

    #@d6
    if-eq v13, v9, :cond_e3

    #@d8
    .line 2454
    move-object/from16 v0, p0

    #@da
    iget-object v15, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@dc
    if-eqz v9, :cond_191

    #@de
    const/16 v13, 0x67

    #@e0
    :goto_e0
    invoke-virtual {v15, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@e3
    .line 2462
    .end local v9           #takeScreenshot:I
    :cond_e3
    sget-boolean v13, Lcom/lge/config/ConfigBuildFlags;->CAPP_SLIDEASIDE:Z

    #@e5
    if-eqz v13, :cond_108

    #@e7
    .line 2463
    const/4 v8, 0x0

    #@e8
    .line 2464
    .local v8, slideAside:I
    const-string v13, "sys.factory.qem"

    #@ea
    invoke-static {v13}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    #@ed
    move-result-object v2

    #@ee
    .line 2466
    .local v2, factoryTestStr:Ljava/lang/String;
    const-string v13, "1"

    #@f0
    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    #@f3
    move-result v13

    #@f4
    if-eqz v13, :cond_195

    #@f6
    .line 2467
    const/4 v8, 0x0

    #@f7
    .line 2472
    :goto_f7
    move-object/from16 v0, p0

    #@f9
    iget v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mSlideAsideMode:I

    #@fb
    if-eq v13, v8, :cond_108

    #@fd
    .line 2473
    move-object/from16 v0, p0

    #@ff
    iget-object v15, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@101
    if-eqz v8, :cond_19e

    #@103
    const/16 v13, 0x69

    #@105
    :goto_105
    invoke-virtual {v15, v13}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    #@108
    .line 2480
    .end local v2           #factoryTestStr:Ljava/lang/String;
    .end local v8           #slideAside:I
    :cond_108
    const-string v13, "navigation_bar_option"

    #@10a
    const/4 v15, 0x1

    #@10b
    invoke-static {v7, v13, v15}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@10e
    move-result v13

    #@10f
    const/4 v15, 0x1

    #@110
    if-ne v13, v15, :cond_1a2

    #@112
    const/4 v13, 0x1

    #@113
    :goto_113
    move-object/from16 v0, p0

    #@115
    iput-boolean v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mTransparentSystemBar:Z

    #@117
    .line 2484
    .end local v6           #pointerLocation:I
    :cond_117
    const-string v13, "screen_off_timeout"

    #@119
    const/4 v15, 0x0

    #@11a
    const/16 v16, -0x2

    #@11c
    move/from16 v0, v16

    #@11e
    invoke-static {v7, v13, v15, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@121
    move-result v13

    #@122
    move-object/from16 v0, p0

    #@124
    iput v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLockScreenTimeout:I

    #@126
    .line 2486
    const-string v13, "default_input_method"

    #@128
    const/4 v15, -0x2

    #@129
    invoke-static {v7, v13, v15}, Landroid/provider/Settings$Secure;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@12c
    move-result-object v5

    #@12d
    .line 2490
    .local v5, imId:Ljava/lang/String;
    const-string v13, "accessibility_easy_access_enabled_category"

    #@12f
    const/4 v15, 0x0

    #@130
    invoke-static {v7, v13, v15}, Lcom/lge/provider/SettingsEx$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@133
    move-result v13

    #@134
    if-eqz v13, :cond_1a5

    #@136
    const/4 v13, 0x1

    #@137
    :goto_137
    move-object/from16 v0, p0

    #@139
    iput-boolean v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mEasyAccessEnabled:Z

    #@13b
    .line 2494
    if-eqz v5, :cond_1a7

    #@13d
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    #@140
    move-result v13

    #@141
    if-lez v13, :cond_1a7

    #@143
    const/4 v3, 0x1

    #@144
    .line 2495
    .local v3, hasSoftInput:Z
    :goto_144
    move-object/from16 v0, p0

    #@146
    iget-boolean v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSoftInput:Z

    #@148
    if-eq v13, v3, :cond_14f

    #@14a
    .line 2496
    move-object/from16 v0, p0

    #@14c
    iput-boolean v3, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHasSoftInput:Z

    #@14e
    .line 2497
    const/4 v10, 0x1

    #@14f
    .line 2499
    :cond_14f
    move-object/from16 v0, p0

    #@151
    iget-boolean v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHotKeyCustomizing:Z

    #@153
    if-eqz v13, :cond_16b

    #@155
    .line 2500
    const-string v13, "hotkey_short_package"

    #@157
    const/4 v15, -0x2

    #@158
    invoke-static {v7, v13, v15}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@15b
    move-result-object v13

    #@15c
    move-object/from16 v0, p0

    #@15e
    iput-object v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sShortKeyPkg:Ljava/lang/String;

    #@160
    .line 2501
    const-string v13, "hotkey_short_class"

    #@162
    const/4 v15, -0x2

    #@163
    invoke-static {v7, v13, v15}, Landroid/provider/Settings$System;->getStringForUser(Landroid/content/ContentResolver;Ljava/lang/String;I)Ljava/lang/String;

    #@166
    move-result-object v13

    #@167
    move-object/from16 v0, p0

    #@169
    iput-object v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->sShortKeyClass:Ljava/lang/String;

    #@16b
    .line 2504
    :cond_16b
    const-string v13, "quick_view_enable"

    #@16d
    const/4 v15, 0x0

    #@16e
    const/16 v16, -0x2

    #@170
    move/from16 v0, v16

    #@172
    invoke-static {v7, v13, v15, v0}, Landroid/provider/Settings$System;->getIntForUser(Landroid/content/ContentResolver;Ljava/lang/String;II)I

    #@175
    move-result v13

    #@176
    if-eqz v13, :cond_1a9

    #@178
    const/4 v13, 0x1

    #@179
    :goto_179
    move-object/from16 v0, p0

    #@17b
    iput-boolean v13, v0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mViewCoverEnabled:Z

    #@17d
    .line 2508
    monitor-exit v14
    :try_end_17e
    .catchall {:try_start_e .. :try_end_17e} :catchall_1ab

    #@17e
    .line 2509
    if-eqz v10, :cond_186

    #@180
    .line 2510
    const/4 v13, 0x1

    #@181
    move-object/from16 v0, p0

    #@183
    invoke-virtual {v0, v13}, Lcom/android/internal/policy/impl/PhoneWindowManager;->updateRotation(Z)V

    #@186
    .line 2512
    :cond_186
    return-void

    #@187
    .line 2410
    .end local v3           #hasSoftInput:Z
    .end local v5           #imId:Ljava/lang/String;
    .end local v12           #userRotationMode:I
    :cond_187
    const/4 v12, 0x1

    #@188
    goto/16 :goto_50

    #@18a
    .line 2434
    .restart local v6       #pointerLocation:I
    .restart local v12       #userRotationMode:I
    :cond_18a
    const/4 v13, 0x2

    #@18b
    goto/16 :goto_84

    #@18d
    .line 2443
    .restart local v4       #hideDisplay:I
    :cond_18d
    const/16 v13, 0x66

    #@18f
    goto/16 :goto_b2

    #@191
    .line 2454
    .end local v4           #hideDisplay:I
    .restart local v9       #takeScreenshot:I
    :cond_191
    const/16 v13, 0x68

    #@193
    goto/16 :goto_e0

    #@195
    .line 2469
    .end local v9           #takeScreenshot:I
    .restart local v2       #factoryTestStr:Ljava/lang/String;
    .restart local v8       #slideAside:I
    :cond_195
    :try_start_195
    const-string v13, "multitasking_slide_aside"

    #@197
    const/4 v15, 0x0

    #@198
    invoke-static {v7, v13, v15}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    #@19b
    move-result v8

    #@19c
    goto/16 :goto_f7

    #@19e
    .line 2473
    :cond_19e
    const/16 v13, 0x6a

    #@1a0
    goto/16 :goto_105

    #@1a2
    .line 2480
    .end local v2           #factoryTestStr:Ljava/lang/String;
    .end local v8           #slideAside:I
    :cond_1a2
    const/4 v13, 0x0

    #@1a3
    goto/16 :goto_113

    #@1a5
    .line 2490
    .end local v6           #pointerLocation:I
    .restart local v5       #imId:Ljava/lang/String;
    :cond_1a5
    const/4 v13, 0x0

    #@1a6
    goto :goto_137

    #@1a7
    .line 2494
    :cond_1a7
    const/4 v3, 0x0

    #@1a8
    goto :goto_144

    #@1a9
    .line 2504
    .restart local v3       #hasSoftInput:Z
    :cond_1a9
    const/4 v13, 0x0

    #@1aa
    goto :goto_179

    #@1ab
    .line 2508
    .end local v1           #accelerometerDefault:I
    .end local v3           #hasSoftInput:Z
    .end local v5           #imId:Ljava/lang/String;
    .end local v11           #userRotation:I
    .end local v12           #userRotationMode:I
    :catchall_1ab
    move-exception v13

    #@1ac
    monitor-exit v14
    :try_end_1ad
    .catchall {:try_start_195 .. :try_end_1ad} :catchall_1ab

    #@1ad
    throw v13
.end method

.method public userActivity()V
    .registers 6

    #@0
    .prologue
    .line 7577
    iget-object v1, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@2
    monitor-enter v1

    #@3
    .line 7578
    :try_start_3
    iget-boolean v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLockScreenTimerActive:Z

    #@5
    if-eqz v0, :cond_18

    #@7
    .line 7580
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@9
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@b
    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    #@e
    .line 7581
    iget-object v0, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mHandler:Landroid/os/Handler;

    #@10
    iget-object v2, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mScreenLockTimeout:Lcom/android/internal/policy/impl/PhoneWindowManager$ScreenLockTimeout;

    #@12
    iget v3, p0, Lcom/android/internal/policy/impl/PhoneWindowManager;->mLockScreenTimeout:I

    #@14
    int-to-long v3, v3

    #@15
    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    #@18
    .line 7583
    :cond_18
    monitor-exit v1

    #@19
    .line 7584
    return-void

    #@1a
    .line 7583
    :catchall_1a
    move-exception v0

    #@1b
    monitor-exit v1
    :try_end_1c
    .catchall {:try_start_3 .. :try_end_1c} :catchall_1a

    #@1c
    throw v0
.end method

.method public windowTypeToLayerLw(I)I
    .registers 6
    .parameter "type"

    #@0
    .prologue
    const/4 v1, 0x1

    #@1
    const/4 v0, 0x2

    #@2
    .line 2757
    if-lt p1, v1, :cond_9

    #@4
    const/16 v2, 0x63

    #@6
    if-gt p1, v2, :cond_9

    #@8
    .line 2841
    :goto_8
    :pswitch_8
    return v0

    #@9
    .line 2760
    :cond_9
    packed-switch p1, :pswitch_data_70

    #@c
    .line 2840
    const-string v1, "WindowManager"

    #@e
    new-instance v2, Ljava/lang/StringBuilder;

    #@10
    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    #@13
    const-string v3, "Unknown window type: "

    #@15
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    #@18
    move-result-object v2

    #@19
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    #@1c
    move-result-object v2

    #@1d
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    #@20
    move-result-object v2

    #@21
    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    #@24
    goto :goto_8

    #@25
    :pswitch_25
    move v0, v1

    #@26
    .line 2762
    goto :goto_8

    #@27
    .line 2767
    :pswitch_27
    const/4 v0, 0x3

    #@28
    goto :goto_8

    #@29
    .line 2769
    :pswitch_29
    const/4 v0, 0x4

    #@2a
    goto :goto_8

    #@2b
    .line 2772
    :pswitch_2b
    const/4 v0, 0x5

    #@2c
    goto :goto_8

    #@2d
    .line 2775
    :pswitch_2d
    const/4 v0, 0x6

    #@2e
    goto :goto_8

    #@2f
    .line 2778
    :pswitch_2f
    const/4 v0, 0x7

    #@30
    goto :goto_8

    #@31
    .line 2781
    :pswitch_31
    const/16 v0, 0x8

    #@33
    goto :goto_8

    #@34
    .line 2784
    :pswitch_34
    const/16 v0, 0x9

    #@36
    goto :goto_8

    #@37
    .line 2787
    :pswitch_37
    const/16 v0, 0xa

    #@39
    goto :goto_8

    #@3a
    .line 2790
    :pswitch_3a
    const/16 v0, 0xb

    #@3c
    goto :goto_8

    #@3d
    .line 2794
    :pswitch_3d
    const/16 v0, 0xc

    #@3f
    goto :goto_8

    #@40
    .line 2796
    :pswitch_40
    const/16 v0, 0xd

    #@42
    goto :goto_8

    #@43
    .line 2798
    :pswitch_43
    const/16 v0, 0xe

    #@45
    goto :goto_8

    #@46
    .line 2800
    :pswitch_46
    const/16 v0, 0xf

    #@48
    goto :goto_8

    #@49
    .line 2802
    :pswitch_49
    const/16 v0, 0x10

    #@4b
    goto :goto_8

    #@4c
    .line 2806
    :pswitch_4c
    const/16 v0, 0x11

    #@4e
    goto :goto_8

    #@4f
    .line 2810
    :pswitch_4f
    const/16 v0, 0x12

    #@51
    goto :goto_8

    #@52
    .line 2813
    :pswitch_52
    const/16 v0, 0x13

    #@54
    goto :goto_8

    #@55
    .line 2816
    :pswitch_55
    const/16 v0, 0x14

    #@57
    goto :goto_8

    #@58
    .line 2819
    :pswitch_58
    const/16 v0, 0x15

    #@5a
    goto :goto_8

    #@5b
    .line 2822
    :pswitch_5b
    const/16 v0, 0x16

    #@5d
    goto :goto_8

    #@5e
    .line 2825
    :pswitch_5e
    const/16 v0, 0x17

    #@60
    goto :goto_8

    #@61
    .line 2829
    :pswitch_61
    const/16 v0, 0x18

    #@63
    goto :goto_8

    #@64
    .line 2831
    :pswitch_64
    const/16 v0, 0x19

    #@66
    goto :goto_8

    #@67
    .line 2833
    :pswitch_67
    const/16 v0, 0x1a

    #@69
    goto :goto_8

    #@6a
    .line 2836
    :pswitch_6a
    const/16 v0, 0x1b

    #@6c
    goto :goto_8

    #@6d
    .line 2838
    :pswitch_6d
    const/16 v0, 0x1c

    #@6f
    goto :goto_8

    #@70
    .line 2760
    :pswitch_data_70
    .packed-switch 0x7d0
        :pswitch_46
        :pswitch_29
        :pswitch_27
        :pswitch_34
        :pswitch_3d
        :pswitch_2d
        :pswitch_4f
        :pswitch_2f
        :pswitch_2b
        :pswitch_40
        :pswitch_58
        :pswitch_37
        :pswitch_3a
        :pswitch_8
        :pswitch_49
        :pswitch_64
        :pswitch_61
        :pswitch_43
        :pswitch_6a
        :pswitch_52
        :pswitch_4c
        :pswitch_67
        :pswitch_6d
        :pswitch_31
        :pswitch_55
        :pswitch_25
        :pswitch_5e
        :pswitch_5b
        :pswitch_2b
    .end packed-switch
.end method
